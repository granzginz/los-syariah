﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APDisbInquiryInProcess.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.APDisbInquiryInProcess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AP DisbInquiry In Process</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />    
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';	


        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
            window.open(ServerName + App + '/Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&InsCoBranchID=' + pInsCoBranchID + '&Style=' + pStyle, 'InsuranceCo', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1')
        }		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
       onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PERMINTAAN PERMBAYARAN DALAM PROSES
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <asp:Panel ID="pnlSearch" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jenis Pembayaran/Pencairan
                    </label>
                    <asp:DropDownList ID="cmbAPType" runat="server">
                        <asp:ListItem Value="0">Select One</asp:ListItem>
                        <asp:ListItem Value="SPPL">PO Supplier</asp:ListItem>
                        <asp:ListItem Value="INSR">Asuransi</asp:ListItem>
                        <asp:ListItem Value="INCS">Incentive Supplier</asp:ListItem>
                        <asp:ListItem Value="INCE">Incentive Karyawan</asp:ListItem>
                        <asp:ListItem Value="STNK">Birojasa STNK</asp:ListItem>
                        <asp:ListItem Value="RSPL">Refund Supplier</asp:ListItem>
                        <asp:ListItem Value="RCST">Refund Customer</asp:ListItem>
                        <asp:ListItem Value="RADV">Repossession Expense</asp:ListItem>
                        <asp:ListItem Value="ASRP">PO untuk Ganti Asset</asp:ListItem>
                        <asp:ListItem Value="RCAN">Refund Kontrak Batal</asp:ListItem>
                        <asp:ListItem Value="AFTN">Fiducia Notaris</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchAP" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFVAPType" runat="server" Display="Dynamic" InitialValue="0"
                        ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Status Cetak
                    </label>
                    <asp:DropDownList ID="cboPrinted" runat="server">
                        <asp:ListItem Value="0">No</asp:ListItem>
                        <asp:ListItem Value="1">Yes</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        PERMINTAAN PEMBAYARAN DALAM PROSES</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                            ShowFooter="True" AllowSorting="True" AutoGenerateColumns="False" BorderWidth="0"
                            DataKeyField="AccountPayableNo">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                        </asp:CheckBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APTo" HeaderText="BAYAR KEPADA">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        Total
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Description" HeaderText="DETAIL PEMBAYARAN/PENCAIRAN">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APAmount" HeaderText="JUMLAH">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APAmount") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO INVOICE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceDate" HeaderText="TGL INVOICE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoiceDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APDate" HeaderText="TGL DUE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAPDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentLocation" HeaderText="LOKASI BAYAR">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPaymentLocation" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentLocation") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Status" HeaderText="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AccountPayableNo" HeaderText="AccountPayableNo"
                                    Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAccountPayableNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="ApplicationID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InsuranceBranchID" HeaderText="InsuranceBranchID"
                                    Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInsuranceBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="MaskAssID" HeaderText="MaskAssID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblAssetRepSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblReffNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblMaskAssID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonPrint" runat="server" CssClass="small button blue" Text="Print">
                </asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
