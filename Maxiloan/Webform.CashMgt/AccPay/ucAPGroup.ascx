﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucAPGroup.ascx.vb"
    Inherits="Maxiloan.Webform.CashMgt.ucAPGroup" %>
<%@ Register src="../../webform.UserController/ucNumberFormat.ascx" tagname="ucNumberFormat" tagprefix="uc1" %>
<% 
    Dim AppInfo As String
    Dim App As String
    AppInfo = Request.ServerVariables("PATH_INFO")
    App = AppInfo.Substring(1, AppInfo.IndexOf("/", 1) - 1)
%>
<script language="javascript" type="text/javascript">
    var x = screen.width;
    var y = screen.height - 100;

    var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
    var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
    function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
        window.open(ServerName + App + '/Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&InsCoBranchID=' + pInsCoBranchID + '&Style=' + pStyle, 'InsuranceCo', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1')
    }		
</script>
<script src="../../Maxiloan.js" type="text/javascript"></script>
<asp:Panel ID="pnlRequest" runat="server">
    <br />
    <asp:TextBox ID="txtApplicationID" runat="server" Visible="False"></asp:TextBox>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                PAYMENT VOUCHER REQUEST
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Supplier
            </label>
            <asp:HyperLink ID="lblAPTo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Tanggal Rencana Bayar
            </label>
            <asp:Label ID="lblDueDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Rekening
            </label>
            <asp:Label ID="lblNamaRek" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Rekening
            </label>
            <asp:Label ID="lblNoRek" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                <asp:HiddenField ID="hdnBankId" runat="server" />
                Nama Bank
            </label>
            <asp:Label ID="lblNamaBank" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
            <asp:HiddenField ID="hdnBankCabankId" runat="server" />
                Nama Cabang
            </label>
            <asp:Label ID="lblNamaCabang" runat="server"></asp:Label>
        </div>
    </div>
    <%--

                <asp:DropDownList ID="cboSupplierAccount" runat="server" Width="100%" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cboSupplierAccount"
                    InitialValue="0" Display="Dynamic" ErrorMessage="Please choose A/P to Account!"></asp:RequiredFieldValidator>
    --%>
    <asp:Label ID="lblAccountNoTo" runat="server"></asp:Label>
    <asp:Label ID="lblAccountNameTo" runat="server"></asp:Label>
    <asp:Label ID="lblBankNameTo" runat="server"></asp:Label>
    <asp:Label ID="lblBankBranchTo" runat="server"></asp:Label>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgAPGroup" runat="server" Width="100%" CssClass="grid_general"
                    BorderStyle="None" BorderWidth="0" AutoGenerateColumns="False">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="AP DETAIL">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAPNo" Text='<%# Container.DataItem("description")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotPaymentAmount" runat="server" Text="TOTAL"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SALDO PEMBAYARAN/PENCAIRAN">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAPBalance" Text='<%# FormatNumber(Container.DataItem("APBalance"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotAPBalance" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH">
                            <ItemTemplate>                                
                                <uc1:ucNumberFormat ID="txtPaymentAmount" runat="server" Enabled = '<%# DataBinder.Eval(Container, "DataItem.StsEnabled") %>'  />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="INVOICE NO.">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblInvoiceNo" Text='<%# Container.DataItem("InvoiceNo")%>' >
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="INVOICE DATE">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblInvoiceDate" Text='<%# Container.DataItem("InvoiceDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ACCOUNTPAYABLENO">
                            <ItemTemplate>
                                <asp:Label ID="lblAssetSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblAssetRepSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblReffNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                </asp:Label>
                                <asp:Label runat="server" ID="lblAccountPayableNo" Text='<%# Container.DataItem("AccountPayableNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
</asp:Panel>
