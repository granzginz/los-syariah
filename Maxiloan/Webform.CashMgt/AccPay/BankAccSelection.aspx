﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BankAccSelection.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.BankAccSelection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
     <title></title> 
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
</head>
<body>
<script  type="text/javascript">

    function checkAll(objRef) {
        var GridView = objRef.parentNode.parentNode.parentNode;
        var inputList = GridView.getElementsByTagName("input");
        for (var i = 0; i < inputList.length; i++) {
            //Get the Cell To find out ColumnIndex
            var row = inputList[i].parentNode.parentNode;
            if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                if (objRef.checked) {
                    //If the header checkbox is checked
                    //check all checkboxes
                    inputList[i].checked = true;
                }
                else {
                    //If the header checkbox is checked
                    //uncheck all checkboxes
                    inputList[i].checked = false;
                }
            }
        }
    }
    </script>
    <form id="form1" runat="server">
        
     <asp:ScriptManager ID="ScriptManager1" runat="server" />
     <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
      <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>PEMILIHAN REKENING BANK </h3>
            </div>
        </div>
 <asp:Panel ID="pnlMain" runat="server" Visible="true">
    <asp:Panel ID="pnlsearch" runat="server">
       

        <div class="form_box">
            <div class="form_left">
                <label> Cabang</label>
                <uc1:ucbranchall id="oBranch" runat="server" />
            </div>
            <div class="form_right">
                <label>Cara Pembayaran</label>
                    <asp:DropDownList ID="ddlCaraBayar" runat="server" style="width:200px" autopostback="true"> 
                     <asp:ListItem Value="0">Select One</asp:ListItem>
                        <asp:ListItem Value="GT">e-Banking</asp:ListItem>
                        <%--E Banking dilakukan di Persiapan Data Ebanking--%>
                        <asp:ListItem Value="BA">Bank</asp:ListItem>
                        <asp:ListItem Value="CA">Cash</asp:ListItem>
                        <asp:ListItem Value="NCA">Non Cash</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                        ErrorMessage="Harap pilih Cara Pembayaran" ControlToValidate="ddlCaraBayar"
                        InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
         <div class="form_box">
             <div class="form_left">
                 <label> Jenis Pembayaran/Pencairan</label>
                 <asp:DropDownList ID="cmbAPType" runat="server"  style="width:167px" />
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                     ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType"
                     InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
             </div>
             <div class="form_right">
                 <label> Rekening Bank</label>
                    <asp:DropDownList ID="ddlRekeningBank" runat="server"  style="width:1200px" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" ControlToValidate="ddlRekeningBank"
                        InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
             </div>
         </div>

        <%--<div class="form_box">
            <div class="form_left">
                <label> Tanggal Rencana Bayar</label>
                <uc1:ucDateCE id="txtTglPV" runat="server"></uc1:ucDateCE>                          
            </div>
            <div class="form_right">
                     <label> Tanggal Bayar</label>
                     <uc1:ucDateCE id="txtTglByr" runat="server"></uc1:ucDateCE>                          

            </div>
        </div>--%>
         <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue" />
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
        </asp:Panel>
 
    <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR PAYMENT VOUCHER</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general"   DataKeyField="PaymentVoucherNo" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" ShowFooter="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                             <Columns>
                              <asp:TemplateColumn>
                                <HeaderTemplate>
                                 <input id="Checkbox2" type="checkbox"  runat="server"  onclick = "checkAll(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <asp:CheckBox ID="ItemCheckBox" runat="server"  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' />
                                </ItemTemplate>
                               </asp:TemplateColumn>
                                
                                  <asp:TemplateColumn  HeaderText="JENIS">
                                <ItemTemplate> <asp:Label ID="lbljns" runat="server" Text= <%#Eval("APType")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn  HeaderText="CAB">
                                <ItemTemplate> <asp:Label ID="lblcab" runat="server" Text= <%#Eval("CAB")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>
                                 
                                <asp:TemplateColumn  HeaderText="BENEFICIARY">
                                 <ItemTemplate><asp:Label ID="lblBenef" runat="server" Text= <%#Eval("ApTo")  %>   visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                  <FooterTemplate>
                                        <strong>Grand Total</strong>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                               
                                 <asp:TemplateColumn  HeaderText="JUMLAH">
                                    <ItemTemplate>   <div  style="TEXT-ALIGN:RIGHT;">
                                    <asp:Label ID="lblPVAmount" runat="server" Text=<%# formatnumber(Eval("PVAmount"),0) %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /> </div></ItemTemplate>  
                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server" />
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                  <asp:TemplateColumn  HeaderText="JENIS TRANSFER">
                                  <ItemTemplate>
                                        <asp:Label ID="lblTrnsfPB" runat="server" Visible="False" Text='PB' />
                                         <asp:DropDownList ID="ddlTransfPB" runat="server" Visible="False" >  
                                                <asp:ListItem Value="SKN">SKN</asp:ListItem>
                                                <asp:ListItem Value="RTGS">RTGS</asp:ListItem> 
                                                <asp:ListItem Value="GT">InHouse</asp:ListItem> 
                                            </asp:DropDownList>
                                  </ItemTemplate>

                                  </asp:TemplateColumn>

                                 <asp:TemplateColumn  HeaderText="BANK">
                                <ItemTemplate> <asp:Label ID="lblBANK" runat="server" Text= <%#Eval("BankNameTo")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn  HeaderText="NO REKENING">
                                <ItemTemplate> <asp:Label ID="lblBankAccountTo" runat="server" Text= <%#Eval("BankAccountTo")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn> 

                                   <asp:TemplateColumn  HeaderText="NAMA REKENING">
                                <ItemTemplate> <asp:Label ID="lblAccountNameTo" runat="server" Text= <%#Eval("AccountNameTo")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn SortExpression="AgreementNoCustomer" HeaderText="NO KONTRAK-CUSTOMER">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# Eval("AgreementNoCustomer") %>'  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=0, true ,false) %>' /> 
                                    <asp:Label ID="lnkAPDeta" runat="server" Visible="False" Text='<%# Eval("AccountPayableNo") %>' /> 
                                    <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Visible="False" Text='<%# Eval( "PaymentVoucherBranchID") %>'/> 
                                    <asp:Label ID="lblAssetName" runat="server" Visible="False" Text='<%# Eval( "AssetName") %>'/> 
                                    <asp:Label ID="lblCustName" runat="server" Visible="False" Text='<%# Eval( "CustomerName") %>'/> 
                                    <asp:Label ID="lblBankId" runat="server" Visible="False" Text='<%# Eval( "BankToId") %>'/> 
                                    <asp:Label ID="lblJnsTrans" runat="server" Visible="False" Text='<%# Eval( "JenisTransfer") %>'/>
                                    <asp:Label ID="lblApplicationID" runat="server" Visible="False" Text='<%# Eval( "ApplicationID") %>'/>
                                </ItemTemplate>
                                </asp:TemplateColumn> 

                                <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NO VOUCHER">
                                <ItemTemplate> 
                                    <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# Eval("PaymentVoucherNo") %>'  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=0, true ,false) %>' /> 
                                </ItemTemplate>
                                </asp:TemplateColumn>                                 
                             </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
             <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
        </div>
            
        </asp:Panel>

</asp:Panel>
    </form>
</body>
</html>
