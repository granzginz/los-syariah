﻿
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine

Public Class PrintPermintaanPembayaran2
    Inherits AbsApDisbApp


#Region "Property"
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private recordCount As Int64 = 1
    Public Property PaymentVoucherNo As String
    Public Property GM1 As String
    Public Property GM2 As String
    Public Property GM3 As String
    Public Property Direksi As String
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "PRINTPBYRAN2"
            If Request.QueryString("strFileLocation") <> "" Then
                Response.Write(String.Format("<script language = javascript>{0} var x = screen.width;{0} var y = screen.height;{0} window.open('../../XML/{1}.pdf','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') {0} </script>", vbCrLf, Request.QueryString("strFileLocation")))
            End If

            pnlsearch.Visible = True
            pnlDatagrid.Visible = False
            txtTglPV.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            fillCombo()
        End If
    End Sub

    Public Function CetakBuktiBayar(ByVal CustomClass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim oDataTable As New DataTable
        Dim intloop As Integer
        Try
            Dim params() As SqlParameter = New SqlParameter(0) {}
            oDataTable = CustomClass.ListAPAppr
            For intloop = 0 To oDataTable.Rows.Count - 1
                params(0) = New SqlParameter("@PaymentVoucherNo", SqlDbType.Char, 20)
                params(0).Value = oDataTable.Rows(intloop)("PaymentVoucherNo")
            Next
            CustomClass.hasil = 1
            Return CustomClass

            CustomClass.listDataReport = SqlHelper.ExecuteDataset(CustomClass.strConnection, CommandType.StoredProcedure, "spAPDisbAppMultipleUpdate_22", params).Tables(0)
            Return CustomClass
        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try


    End Function

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles Buttonsearch.Click
        Dim strSearch As New StringBuilder
        If txtTglPV.Text <> "" Or IsDate(ConvertDate2(txtTglPV.Text)) Then
            APType = cmbAPType.SelectedItem.Value
            'Me.SearchBy = String.Format("pv.pvDueDate ='{0}'  and pv.apBranchID = '{1}' and pv.APType='{2}' ", ConvertDate2(txtTglPV.Text), oBranch.BranchID, cmbAPType.SelectedItem.Value)
            Me.SearchBy = String.Format("pv.pvDueDate <='{0}'  and pv.apBranchID = '{1}' and pv.APType='{2}' and pv.PVStatus <> 'D' ", ConvertDate2(txtTglPV.Text), oBranch.BranchID, cmbAPType.SelectedItem.Value)
            DoBind()
        Else
            ShowMessage(lblMessage, "Harap isi Tanggal Rencana Bayar", True)
        End If
    End Sub
#Region "BindGridEntity"
    Sub BindGridEntity()
        Dim dtEntity As New DataTable
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer = 0
        Dim DataList As New List(Of Parameter.APDisbApp)
        Dim custom As New Parameter.APDisbApp

        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.APDisbApp
                custom.PaymentVoucherNo = TempDataTable.Rows(index).Item("lnkPVNo").ToString.Trim
                DataList.Add(custom)
            Next
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            '.CurrentPage = currentPage
            '.PageSize = pageSize
            '.SortBy = Me.Sort
        End With

        oCustomClass = CetakBuktiBayar(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listDataReport
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        dtgEntity.DataSource = dtEntity.DefaultView
        dtgEntity.CurrentPageIndex = 0
        dtgEntity.DataBind()
        'PagingFooter()

        pnlDatagrid.Visible = True
        pnlsearch.Visible = True

        For intLoopGrid = 0 To dtgEntity.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgEntity.Items(intLoopGrid).FindControl("lblxx"), CheckBox)
            Dim PaymentVoucherNo As String = CType(dtgEntity.Items(intLoopGrid).FindControl("lnkPVNo"), HyperLink).Text.Trim

            Me.PaymentVoucherNo = PaymentVoucherNo

            Dim query As New Parameter.APDisbApp
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next
    End Sub

    Private Function PredicateFunction(obj As Parameter.APDisbApp) As Boolean
        Return obj.ApplicationID = obj.PaymentVoucherNo = Me.PaymentVoucherNo
    End Function
#End Region

    Function createDataTable() As DataTable

        Dim dt As DataTable = New DataTable

        dt.Columns.Add("NoKontrak", GetType(String))
        dt.Columns.Add("AtasNama", GetType(String))
        dt.Columns.Add("AssetName", GetType(String))
        dt.Columns.Add("BayarKepada", GetType(String))
        dt.Columns.Add("Bank", GetType(String))
        dt.Columns.Add("NoRekening", GetType(String))
        dt.Columns.Add("Jumlah", GetType(Decimal))


        Return dt

    End Function

    Sub BindDataPPK()
        Dim DataList As New List(Of Parameter.APDisbApp)
        Dim Application As New Parameter.APDisbApp
        Dim chck As Integer = 0
        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("PaymentVoucherNo", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                Application = New Parameter.APDisbApp

                Application.PaymentVoucherNo = TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim

                DataList.Add(Application)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To dtgEntity.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgEntity.Items(intLoopGrid).FindControl("ItemCheckBox"), CheckBox)
            Dim PaymentVoucherNo As String = CType(dtgEntity.Items(intLoopGrid).FindControl("lnkPVNo"), HyperLink).Text.Trim
            chck += 1


            Me.PaymentVoucherNo = PaymentVoucherNo

            Dim query As New Parameter.APDisbApp
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If
            If (chck <= 0) Then
                ShowMessage(lblMessage, "Silahkan Pilih Voucher", True)
                Exit Sub
            End If

            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("PaymentVoucherNo") = PaymentVoucherNo
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClearPPK()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("PaymentVoucherNo", GetType(String)))
        End With
    End Sub
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer = 0
            Dim hasil As Integer = 0
            Dim cmdwhere As String = ""

            If TempDataTable Is Nothing Then
                BindDataPPK()
            Else
                If TempDataTable.Rows.Count = 0 Then
                    BindDataPPK()
                End If
            End If

            With oDataTable
                .Columns.Add(New DataColumn("PaymentVoucherNo", GetType(String)))
            End With

            If TempDataTable.Rows.Count = 0 Then
                ShowMessage(lblMessage, "Harap periksa item", True)
                Exit Sub
            End If

            For index = 0 To TempDataTable.Rows.Count - 1
                oRow = oDataTable.NewRow
                oRow("PaymentVoucherNo") = TempDataTable.Rows(index).Item("PaymentVoucherNo").ToString.Trim
                oDataTable.Rows.Add(oRow)
            Next

            Me.PaymentVoucherNo = TempDataTable.Rows(0).Item("PaymentVoucherNo").ToString.Trim

            With oCustomClass
                .strConnection = GetConnectionString()
                .ListAPAppr = oDataTable
            End With
            oCustomClass = CetakBuktiBayar(oCustomClass)

            Dim cookie As HttpCookie = Request.Cookies("RptBuktiPembayaranSPPL")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                cookie.Values("BusinessDate") = Me.BusinessDate.ToString("dd/MM/yyyy")
                cookie.Values("PaymentVoucherNo") = Me.PaymentVoucherNo
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptBuktiPembayaranSPPL")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                cookieNew.Values.Add("BusinessDate", Me.BusinessDate.ToString("dd/MM/yyyy"))
                cookieNew.Values.Add("PaymentVoucherNo", Me.PaymentVoucherNo)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("BuktiPembayaranSPPLViewer.aspx")
        End If


    End Sub


End Class