﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APDisbSelec.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.APDisbSelec" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>APDisbSelec</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="javascript" src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
            window.open(ServerName + App + '/Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&InsCoBranchID=' + pInsCoBranchID + '&Style=' + pStyle, 'InsuranceCo', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1')
        }		
    </script>
</head>
<body>    
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PILIH PENCAIRAN/PEMBAYARAN YANG MAU DIBAYAR
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlMain" runat="server">
        <asp:Panel ID="pnlSearch" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jenis Pembayaran
                    </label>
                    <asp:DropDownList ID="cmbAPType" runat="server">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtAPType" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                        Display="Dynamic" ErrorMessage="Harap Pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cari Berdasarkan
                    </label>
                    <asp:DropDownList ID="cmbSearch" runat="server">
                        <asp:ListItem Value="0">Select One</asp:ListItem>
                        <asp:ListItem Value="InvoiceNo">No Invoice</asp:ListItem>
                        <asp:ListItem Value="AccountPayableNo">Detail Pembayaran/Pencairan</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Tanggal Rencana Bayar <= </label>
                    <uc1:ucDateCE id="txtTglJatuhTempo" runat="server"></uc1:ucDateCE>                         
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Cabang
                        </label>
                        <asp:DropDownList ID="oBranch" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
                            Display="Dynamic" ErrorMessage="Harap Pilih Cabang" ControlToValidate="oBranch"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Invoice
                        </label>
                        <uc1:ucDateCE id="txtTglInvoice" runat="server"></uc1:ucDateCE>                        
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search">
                </asp:Button>
                <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDataGrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR PEMBAYARAN/PENCAIRAN</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgAP" runat="server" CssClass="grid_general" BorderWidth="0" BorderStyle="None"
                            AutoGenerateColumns="False" AllowSorting="True" OnSortCommand="SortGrid" ShowFooter="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                        </asp:CheckBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APTo" HeaderText="PENERIMA">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        Total
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APID" HeaderText="AP ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lnkAPID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn SortExpression="AccountPayableNo" HeaderText="AP NO/DETAIL PEMBAYARAN/PENCAIRAN">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPDetailDescription" Visible="True" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                        </asp:HyperLink>
                                        <asp:HyperLink ID="lnkAPDetail" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAgreementNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APAmount" HeaderText="JUMLAH">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.APAmount"),2) %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="DueDate" HeaderText="RENCANA BYR">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DueDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO INVOICE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceDate" HeaderText="TGL INV">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false" SortExpression="APStatusDate" HeaderText="STATUS DUE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Visible="True"></asp:Label>
                                        <asp:Label ID="lblStatusDue" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.APStatusDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblAssetRepSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblReffNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblApplicationID" runat="server" Visible="false" Text='<%#Container.DataItem("ApplicationID")%>'>
                                        </asp:Label>
                                        <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblSupplierID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonNext" runat="server" CssClass="small button green" Text="Next">
                </asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
