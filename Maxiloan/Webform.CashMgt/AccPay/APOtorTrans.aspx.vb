﻿
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class APOtorTrans
    Inherits Maxiloan.Webform.WebBased
     
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1 
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController

    Protected WithEvents txtTglPVFr As ucDateCE
    Protected WithEvents txtTglPVTo As ucDateCE
    Protected WithEvents GridNavigator As ucGridNav
    Private m_Appcontroller As New ApplicationController
    Private time As String
#Region "Property"
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "APOTORTRANS"

            If Not Me.IsHoBranch Then
                Response.Redirect("../../error_notauthorized.aspx")
            End If

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                'Modify by Wira 20171023
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", " DESC"))
    End Sub
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
    Sub DoBind(Optional isFrNav As Boolean = False)
        oCustomClass =
            New Parameter.APDisbApp With
            {
                .strConnection = GetConnectionString(),
                .WhereCond = Me.SearchBy,
                .CurrentPage = currentPage,
                .PageSize = pageSize,
                .SortBy = Me.SortBy
            }

        oCustomClass = oController.ListApOtorTrans(oCustomClass)
        recordCount = oCustomClass.TotalRecord
        dtgEntity.DataSource = oCustomClass.ListAPAppr.DefaultView

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
    End Sub
    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        txtTglPVFr.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtTglPVTo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = New GeneralPagingController().GetGeneralPaging(New Parameter.GeneralPaging With {.strConnection = GetConnectionString(), .WhereCond = "", .CurrentPage = 1, .PageSize = 100, .SortBy = "", .SpName = "spTblAPType"}).ListData
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With

        cboBank.DataTextField = "Name"
        cboBank.DataValueField = "ID"
        cboBank.DataSource = New DataUserControlController().GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), "BA", "")
        cboBank.DataBind()

        cboBank.Items.Insert(0, "ALL")
        cboBank.Items(0).Value = "0"

    End Sub

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder
        If String.IsNullOrEmpty(txtTglPVFr.Text) Or String.IsNullOrEmpty(txtTglPVTo.Text) Then
            ShowMessage(lblMessage, "Tanggal posting tidak valid", True)
            Return
        End If

        If (ConvertDate2(txtTglPVTo.Text) < ConvertDate2(txtTglPVFr.Text)) Then
            ShowMessage(lblMessage, "Range Tanggal Salah", True)
            Return
        End If
        strSearch.Append(String.Format(" tbd.Status='N' And tbd.BranchID = '{0}' and tbd.Aptype='{1}'  and tbd.PostingDate between '{2}' and '{3}' ",
                               Me.sesBranchId.Replace("'", ""), cmbAPType.SelectedItem.Value, ConvertDate2(txtTglPVFr.Text).ToString("yyyyMMdd"), ConvertDate2(txtTglPVTo.Text).ToString("yyyyMMdd")))

        If (cboBank.SelectedItem.Value <> "0") Then
            strSearch.Append(String.Format(" and tbd.BankAccountId='{0}' ", cboBank.SelectedItem.Value))
        End If

        Me.SearchBy = strSearch.ToString
        Me.SortBy = ""
        DoBind()

    End Sub
    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        Response.Redirect("APOtorTrans.aspx")
    End Sub

    Private Sub dtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lnkAgreementNo.Text.Trim) & "')"
            End If
        End If
    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Response.Redirect("APOtorTrans.aspx")
    End Sub
    Private Function getChecked() As IList(Of String)
        Dim chkDtList As CheckBox
        Dim selectedPV As IList(Of String) = New List(Of String)
        For Each item In dtgEntity.Items
            chkDtList = CType(item.FindControl("chkItem"), CheckBox)
            If (Not Object.ReferenceEquals(chkDtList, Nothing)) Then
                If chkDtList.Checked Then
                    selectedPV.Add(CType(item.FindControl("lblAPType"), Label).Text.Trim)
                End If
            End If
        Next

        Return selectedPV
    End Function
    Private Sub imgReject_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReject.Click
        Dim selectedPV As IList(Of String) = getChecked()
        If selectedPV.Count <= 0 Then
            ShowMessage(lblMessage, "silahkan pilih data terlebih dahulu", True)
            Return
        End If

        Try

            oCustomClass.strConnection = GetConnectionString()
            oCustomClass.OtorPvNoList = selectedPV
            oCustomClass.Flag = "R"
            oController.UpdateAPDisbCash(oCustomClass, False)
            imgCancel_Click(Nothing, Nothing)
            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        If CheckFeature(Me.Loginid, Me.FormID, "OTOR", Me.AppId) Then
            'Modify by WIra 20171024
            Dim dt As DataTable = createNewDT()

            Dim selectedPV As IList(Of String) = getChecked()
            If selectedPV.Count <= 0 Then
                ShowMessage(lblMessage, "silahkan pilih data terlebih dahulu", True)
                Return
            End If

            Try
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass.OtorPvNoList = selectedPV
                oCustomClass.Flag = "S"
                oController.UpdateAPDisbCash(oCustomClass, False)
                'Modify by Wira 20171023
                'Tambah log
                Dim chkDtList As CheckBox
                For Each item In dtgEntity.Items
                    chkDtList = CType(item.FindControl("chkItem"), CheckBox)
                    If (Not Object.ReferenceEquals(chkDtList, Nothing)) Then
                        If chkDtList.Checked Then
                            Dim i = 0
                            dt.Rows.Add((i + 1).ToString, CType(item.FindControl("lblApplicationID"), Label).Text.Trim, CType(item.FindControl("lblApBranchID"), Label).Text.Trim)
                        End If
                    End If
                Next

                DisbursementLog(dt)

                imgCancel_Click(Nothing, Nothing)
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If
    End Sub

#Region "Disbursement Log"
    Sub DisbursementLog(dt As DataTable)
        Dim row As DataRow

        For Each row In dt.Rows
            Me.BranchID = row("value2")
            Me.ApplicationID = row("value1")

            If (cmbAPType.SelectedItem.Value.ToUpper.Trim = "SPPL") Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateEnd = Me.BusinessDate + " " + time

                Dim oApplication As New Parameter.Application
                oApplication.strConnection = GetConnectionString()
                oApplication.BranchId = Replace(Me.BranchID.Trim, "'", "").ToString
                oApplication.ApplicationID = Me.ApplicationID.Trim
                oApplication.ActivityType = "OTO"
                oApplication.ActivityDateStart = Me.ActivityDateStart
                oApplication.ActivityDateEnd = Me.ActivityDateEnd
                oApplication.ActivityUser = Me.Loginid
                oApplication.ActivitySeqNo = 26

                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Application

                oReturn = m_Appcontroller.DisburseLogSave(oApplication)

                If oReturn.Err <> "" Then
                    ShowMessage(lblMessage, ErrorMessage, True)
                    Exit Sub
                End If

            End If
        Next row
    End Sub
#End Region

    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")

        Return dt
    End Function
End Class