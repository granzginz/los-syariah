﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Webform.CashMgt
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
Imports CrystalDecisions.Web
Imports Maxiloan.General.CommonCookiesHelper
#End Region


Public Class CetakPettyCash
    Inherits Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
    Private Property GM1() As String
        Get
            Return CType(ViewState("GM1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM1") = Value
        End Set
    End Property
    Private Property GM2() As String
        Get
            Return CType(ViewState("GM2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM2") = Value
        End Set
    End Property
    Private Property GM3() As String
        Get
            Return CType(ViewState("GM3"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GM3") = Value
        End Set
    End Property
    Private Property Direksi() As String
        Get
            Return CType(ViewState("Direksi"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Direksi") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oController As New APDisbAppController
    Private oParameter As New Parameter.APDisbApp
    Friend Shared ReportSource As PmtPembayaranSPPL
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        bindReport()
    End Sub
#End Region

#Region "BindReport"
    Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oDataTable As New DataTable
        Dim oReport As PmtPembayaranSPPL = New PmtPembayaranSPPL
        Dim dtEntity As New DataTable


        If Not oParameter Is Nothing Then
            oDataTable = oParameter.RequestNoTemp
        End If
        With oParameter
            .strConnection = GetConnectionString()
            .RequestNo = Me.MultiApplicationID
            '.WhereCond = Me.CmdWhere
            .SortBy = Me.SortBy
            .GM1 = Me.GM1
            .GM2 = Me.GM2
            .GM3 = Me.GM3
            .Direksi = Me.Direksi
        End With

        oParameter = CetakBuktiBayar(oParameter)
        oDataSet = oParameter.ListReport

        oParameter.strConnection = GetConnectionString()
        'oParameter.WhereCond = Me.CmdWhere
        oParameter.BranchId = Me.sesBranchId.Replace("'", "")
        oParameter.LoginId = Me.Loginid
        oParameter = CetakBuktiBayar(oParameter)
        oDataSet = oParameter.ListReport

        oReport.SetDataSource(oDataSet.Tables(0))

        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_PmtPembayaranSPPL.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions =
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("PrintPettyCashReimburse.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_PmtPembayaranSPPL")

    End Sub
    Public Function CetakBuktiBayar(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(4) {}
        params(0) = New SqlParameter("@RequestNo", SqlDbType.VarChar, 30)
        params(0).Value = customclass.RequestNo
        params(1) = New SqlParameter("@GM1", SqlDbType.Char, 50)
        params(1).Value = customclass.GM1
        params(2) = New SqlParameter("@GM2", SqlDbType.Char, 50)
        params(2).Value = customclass.GM2
        params(3) = New SqlParameter("@GM3", SqlDbType.Char, 50)
        params(3).Value = customclass.GM3
        params(4) = New SqlParameter("@Direksi", SqlDbType.Char, 50)
        params(4).Value = customclass.Direksi


        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "sppettycashprint", params)
        Return customclass
    End Function
#End Region

#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptPmtPembayaranSPPL")
        'Me.CmdWhere = cookie.Values("cmdwhere")
        Me.MultiApplicationID = cookie.Values("RequestNo")
        'Me.SortBy = cookie.Values("sortby")
        Me.GM1 = cookie.Values("GM1")
        Me.GM2 = cookie.Values("GM2")
        Me.GM3 = cookie.Values("GM3")
        Me.Direksi = cookie.Values("Direksi")
    End Sub
#End Region

End Class