﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class APEBankEdit
    Inherits Maxiloan.Webform.WebBased

    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1    
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Private EBankController As New EBankTransferController
    Protected WithEvents txtJumlah As ucNumberFormat

    Dim Total As Double

#Region "properties"
    Private Property PaymentVoucherBranchID() As String
        Get
            Return (CType(ViewState("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherBranchID") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(ViewState("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVStatus") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(ViewState("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(ViewState("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(ViewState("APType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(ViewState("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVDate") = Value
        End Set
    End Property
    Private Property APTo() As String
        Get
            Return (CType(ViewState("APTo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APTo") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then

            If Request.QueryString("Request") = "RequestH" Then
                InitialDefaultPanelRequestH()
                Me.FormID = "RQEDIT"
            ElseIf Request.QueryString("Request") = "ApprovalH" Then
                InitialDefaultPanelApprovalH()
                Me.FormID = "APEDIT"
            Else
                InitialDefaultPanel()
                Me.FormID = "EBEDIT"
            End If

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If        
    End Sub
    Private Sub InitialDefaultPanelRequestH()
        pnlDetail.Visible = False
        pnlRequestH.Visible = True
        pnlRequestD.Visible = False
        pnlSearch.Visible = False
        pnlDatagrid.Visible = False
        pnlApprovalD.Visible = False
        pnlApprovalH.Visible = False

        HBind(True)
    End Sub
    Private Sub InitialDefaultPanelRequestD()
        pnlDetail.Visible = False
        pnlRequestH.Visible = False
        pnlRequestD.Visible = True
        pnlSearch.Visible = False
        pnlDatagrid.Visible = False
        pnlApprovalD.Visible = False
        pnlApprovalH.Visible = False

        DBind(True)
    End Sub
    Private Sub InitialDefaultPanelApprovalH()
        pnlDetail.Visible = False
        pnlRequestH.Visible = False
        pnlRequestD.Visible = False
        pnlSearch.Visible = False
        pnlDatagrid.Visible = False
        pnlApprovalD.Visible = False
        pnlApprovalH.Visible = True

        HBind(False)
    End Sub
    Private Sub InitialDefaultPanelApprovalD()
        pnlDetail.Visible = False
        pnlRequestH.Visible = False
        pnlRequestD.Visible = False
        pnlSearch.Visible = False
        pnlDatagrid.Visible = False
        pnlApprovalH.Visible = False
        pnlApprovalD.Visible = True

        DBind(False)
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDetail.Visible = False
        pnlRequestH.Visible = False
        pnlRequestD.Visible = False
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        pnlApprovalD.Visible = False
        pnlApprovalH.Visible = False        
        txtTglJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")        
    End Sub
    Sub HBind(ByVal request As Boolean)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController
        Dim dtPVno As DataTable = createNewPVnoDT()
        dtPVno.Rows.Add(1, String.Empty, String.Empty)
        With EBankData
            .strConnection = GetConnectionString()

            If request Then
                .WhereCond1 = " where pv.APBranchId=" + sesBranchId + ""
            Else
                .WhereCond1 = " where pv.BranchID=" + sesBranchId + ""
            End If
            '.WhereCond2 = " where TransferFundTransaction.BranchIdFrom=" + sesBranchId + ""
            '.WhereCond3 = " where a.BankBranchID=" + sesBranchId + ""
        End With

        EBankData = EBankController.GetRequestEdit(EBankData)

        If EBankData.ListAPAppr.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data tidak ada ...", True)
        End If

        If request Then
            DtUserList = EBankData.ListAPAppr
            DvUserList = DtUserList.DefaultView
            dgRequsetH.DataSource = DvUserList

            Try
                dgRequsetH.DataBind()
            Catch ex As Exception
                dgRequsetH.CurrentPageIndex = 0
                dgRequsetH.DataBind()
            End Try
        Else
            DtUserList = EBankData.ListAPAppr
            DvUserList = DtUserList.DefaultView
            dgApprovalH.DataSource = DvUserList

            Try
                dgApprovalH.DataBind()
            Catch ex As Exception
                dgApprovalH.CurrentPageIndex = 0
                dgApprovalH.DataBind()
            End Try
        End If
    End Sub
    Sub DBind(ByVal request As Boolean)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView


        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController
        Dim dtPVno As DataTable = createNewPVnoDT()
        dtPVno.Rows.Add(1, String.Empty, String.Empty)
        With EBankData
            .strConnection = GetConnectionString()

            If request Then
                .WhereCond = " pv.APTo = '" + APTo + "' and pv.APBranchId = '" + sesBranchId.Replace("'", "") + "'"
                .WhereCond1 = " BankAccount.AccountName = '" + APTo + "' and TransferFundTransaction.BranchId = '" + sesBranchId.Replace("'", "") + "'"
                .WhereCond2 = "a.Status = 'D' and a.AccountNameTo = '" + APTo + "' and a.BranchId = '" + sesBranchId.Replace("'", "") + "'"
            Else
                .WhereCond = " pv.APTo = '" + APTo + "' and pv.BranchID = '" + sesBranchId.Replace("'", "") + "'"
                .WhereCond1 = "BankAccount.AccountName = '" + APTo + "' and TransferFundTransaction.BranchId = '" + sesBranchId.Replace("'", "") + "'"
                .WhereCond2 = "a.Status = 'RE' and a.AccountNameTo = '" + APTo + "' and a.BranchId = '" + sesBranchId.Replace("'", "") + "'"
            End If

            .BusinessDate = Me.BusinessDate
            .BranchId = Me.sesBranchId.Replace("'", "")
            .PVnoDT = dtPVno
            .SortBy = ""
        End With

        EBankData = EBankController.EBankTransferList2(EBankData)


        If request Then
            DtUserList = EBankData.ListAPAppr

            If DtUserList.Rows.Count > 0 Then
                lblAccountName.Text = DtUserList.Rows(0).Item("AccountNameTo").ToString
                lblAccountNo.Text = DtUserList.Rows(0).Item("BankAccountTo").ToString
                lnkSupplier.Text = DtUserList.Rows(0).Item("APTo").ToString

                Me.PaymentVoucherBranchID = DtUserList.Rows(0).Item("PaymentVoucherBranchID").ToString()

                lnkSupplier.NavigateUrl = LinkSupplierTo(DtUserList.Rows(0).Item("SupplierID").ToString.Trim, "Marketing")
                Select Case DtUserList.Rows(0).Item("APType").ToString.Trim
                    Case "SPPL"
                        lnkSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(DtUserList.Rows(0).Item("SupplierID").ToString.Trim) & "')"
                    Case "INSR"
                        lnkSupplier.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(DtUserList.Rows(0).Item("MaskAssID").ToString.Trim) & "','" & Server.UrlEncode(DtUserList.Rows(0).Item("InsuranceBranchID").ToString.Trim) & "','" & "Finance" & "')"
                    Case "RCST"
                        lnkSupplier.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(DtUserList.Rows(0).Item("CustomerID").ToString.Trim) & "')"
                    Case "RADV"
                        lnkSupplier.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(DtUserList.Rows(0).Item("ReferenceNo").ToString.Trim) & "','" & "Finance" & "')"
                    Case "ASRP"
                        lnkSupplier.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(DtUserList.Rows(0).Item("ApplicationId").ToString.Trim) & "' , '" & Server.UrlEncode(DtUserList.Rows(0).Item("AssetRepSeqNo").ToString.Trim) & "','" & Server.UrlEncode(DtUserList.Rows(0).Item("AssetSeqNo").ToString.Trim) & "')"
                End Select

                lblBranch.Text = DtUserList.Rows(0).Item("BranchInitialName").ToString
                lblJenisPembayaran.Text = DtUserList.Rows(0).Item("APType").ToString
            Else
                ShowMessage(lblMessage, "Data tidak ada ...", True)
            End If

            DvUserList = DtUserList.DefaultView
            dgRequsetD.DataSource = DvUserList

            Try
                dgRequsetD.DataBind()
            Catch ex As Exception
                dgRequsetD.CurrentPageIndex = 0
                dgRequsetD.DataBind()
            End Try
        Else
            DtUserList = EBankData.ListAPAppr

            If DtUserList.Rows.Count > 0 Then
                lblAccountNameApproval.Text = DtUserList.Rows(0).Item("AccountNameTo").ToString
                lblAccountNoApproval.Text = DtUserList.Rows(0).Item("BankAccountTo").ToString
                lnkSupplierApproval.Text = DtUserList.Rows(0).Item("APTo").ToString

                Me.PaymentVoucherBranchID = DtUserList.Rows(0).Item("PaymentVoucherBranchID").ToString()

                lnkSupplierApproval.NavigateUrl = LinkSupplierTo(DtUserList.Rows(0).Item("SupplierID").ToString.Trim, "Marketing")
                Select Case DtUserList.Rows(0).Item("APType").ToString.Trim
                    Case "SPPL"
                        lnkSupplierApproval.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(DtUserList.Rows(0).Item("SupplierID").ToString.Trim) & "')"
                    Case "INSR"
                        lnkSupplierApproval.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(DtUserList.Rows(0).Item("MaskAssID").ToString.Trim) & "','" & Server.UrlEncode(DtUserList.Rows(0).Item("InsuranceBranchID").ToString.Trim) & "','" & "Finance" & "')"
                    Case "RCST"
                        lnkSupplierApproval.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(DtUserList.Rows(0).Item("CustomerID").ToString.Trim) & "')"
                    Case "RADV"
                        lnkSupplierApproval.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(DtUserList.Rows(0).Item("ReferenceNo").ToString.Trim) & "','" & "Finance" & "')"
                    Case "ASRP"
                        lnkSupplierApproval.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(DtUserList.Rows(0).Item("ApplicationId").ToString.Trim) & "' , '" & Server.UrlEncode(DtUserList.Rows(0).Item("AssetRepSeqNo").ToString.Trim) & "','" & Server.UrlEncode(DtUserList.Rows(0).Item("AssetSeqNo").ToString.Trim) & "')"
                End Select

                lblBranchApproval.Text = DtUserList.Rows(0).Item("BranchInitialName").ToString
                lblJenisPembayaranApproval.Text = DtUserList.Rows(0).Item("APType").ToString
            Else
                ShowMessage(lblMessage, "Data tidak ada ...", True)
            End If

            DvUserList = DtUserList.DefaultView
            dgApprovalD.DataSource = DvUserList

            Try
                dgApprovalD.DataBind()
            Catch ex As Exception
                dgApprovalD.CurrentPageIndex = 0
                dgApprovalD.DataBind()
            End Try
        End If
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView


        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController
        Dim dtPVno As DataTable = createNewPVnoDT()
        dtPVno.Rows.Add(1, String.Empty, String.Empty)
        With EBankData
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.sesBranchId.Replace("'", "")
            .PVnoDT = dtPVno
            .SortBy = SortBy
        End With

        EBankData = EBankController.EBankTransferList2(EBankData)


        DtUserList = EBankData.ListAPAppr
        DvUserList = DtUserList.DefaultView
        recordCount = EBankData.TotalRecord
        DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch ex As Exception
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
        pnlDetail.Visible = False
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        Me.PageState = currentPage
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Branch_ID & "','" & style & "')"
    End Function
    Function LinkSupplierTo(ByVal pSupplierID As String, ByVal style As String) As String
        Return "javascript:OpenWinSupplier('" & style & "', '" & pSupplierID & "')"
    End Function
#End Region
#Region "Datagrid Command"
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub
    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
                lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            End If

            If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
                lnkAPTO.NavigateUrl = LinkSupplierTo(lblSupplierID.Text.Trim, "Marketing")
                Select Case cmbAPType.SelectedItem.Value
                    Case "SPPL"
                        lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                    Case "INSR"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                    Case "RCST"
                        lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

                    Case "RADV"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
                    Case "ASRP"
                        lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
                End Select

            End If


            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If

    End Sub
    Private Sub DtgEntity_ItemCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        Select Case e.CommandName
            Case "approve"
                If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
                    pnlDetail.Visible = True
                    pnlDatagrid.Visible = False
                    pnlSearch.Visible = False
                    GridToDetail(e.Item)
                End If

        End Select
    End Sub
    Private Sub dgRequsetH_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRequsetH.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim controller As New BranchController()
            Dim obranch As New Parameter.Branch
            Dim lnkSupplier As New HyperLink

            obranch.strConnection = GetConnectionString()
            obranch.BranchId = e.Item.Cells(2).Text.ToString.Trim
            obranch = controller.BranchByID(obranch)
            e.Item.Cells(2).Text = obranch.ListData.Rows(0).Item("BranchInitialName").ToString.Trim

            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), HyperLink)

            lnkSupplier.NavigateUrl = LinkSupplierTo(e.Item.Cells(4).Text.Trim, "Marketing")
        End If
    End Sub
    Private Sub dgApprovalH_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgApprovalH.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim controller As New BranchController()
            Dim obranch As New Parameter.Branch
            Dim lnkSupplier As New HyperLink

            obranch.strConnection = GetConnectionString()
            obranch.BranchId = e.Item.Cells(2).Text.ToString.Trim
            obranch = controller.BranchByID(obranch)
            e.Item.Cells(2).Text = obranch.ListData.Rows(0).Item("BranchInitialName").ToString.Trim

            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), HyperLink)

            lnkSupplier.NavigateUrl = LinkSupplierTo(e.Item.Cells(4).Text.Trim, "Marketing")
        End If
    End Sub
    Private Sub dgRequsetH_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgRequsetH.ItemCommand
        Dim lnkSupplier As New HyperLink
        If e.CommandName = "request" Then
            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), HyperLink)

            Me.APTo = lnkSupplier.Text.Trim
            InitialDefaultPanelRequestD()
        End If
    End Sub
    Private Sub dgApprovalH_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgApprovalH.ItemCommand
        Dim lnkSupplier As New HyperLink
        If e.CommandName = "approval" Then
            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), HyperLink)

            Me.APTo = lnkSupplier.Text.Trim
            InitialDefaultPanelApprovalD()
        End If
    End Sub
    Private Sub dgRequsetD_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgRequsetD.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblPaymentVoucherBranchID As New Label
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")

            lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")

            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If
    End Sub
    Private Sub dgApprovalD_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgApprovalD.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblPaymentVoucherBranchID As New Label
        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")

            lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")

            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If
    End Sub
#End Region
    Private Sub GridToDetail(ByVal dgitem As DataGridItem)
        Dim lnkAPTO As HyperLink = CType(dgitem.FindControl("lnkAPTO"), HyperLink)
        Dim lblBankCode As Label = CType(dgitem.FindControl("lblBankCode"), Label)
        Dim strSearch As New StringBuilder

        If IsDate(ConvertDate2(txtTglJatuhTempo.Text)) Then
            Me.APType = cmbAPType.SelectedItem.Value
            Me.PVDate = txtTglJatuhTempo.Text

            strSearch.Append(" pv.APType='" & Me.APType & "'")
            strSearch.Append(" and pv.PaymentVoucherDate<='" & ConvertDate2(Me.PVDate) & "'")
            strSearch.Append(" and pv.APBranchId = '" & Me.sesBranchId.Replace("'", "") & "'")
            strSearch.Append(" and pv.APTo='" & lnkAPTO.Text & "'")
            Me.SortBy = ""
            Dim DtUserList As New DataTable
            Dim DvUserList As New DataView

            Dim EBankData As New Parameter.EBankTransfer
            Dim EBankController As New EBankTransferController
            Dim dtPVno As DataTable = createNewPVnoDT()
            dtPVno.Rows.Add(1, String.Empty, String.Empty)
            With EBankData
                .strConnection = GetConnectionString()
                .WhereCond = strSearch.ToString
                .WhereCond1 = "BankAccount.AccountName='" & lnkAPTO.Text & "'"
                .BusinessDate = Me.BusinessDate
                .BranchId = Me.sesBranchId.Replace("'", "")
                .PVnoDT = dtPVno
                .SortBy = SortBy
            End With

            EBankData = EBankController.EBankTransferList2(EBankData)

            DtUserList = EBankData.ListAPAppr
            DvUserList = DtUserList.DefaultView

            dtgDetailEditBank.DataSource = DvUserList
            dtgDetailEditBank.DataBind()

            pnlDetail.Visible = True
            pnlRequestH.Visible = False
            pnlRequestD.Visible = False
            pnlSearch.Visible = False
            pnlDatagrid.Visible = False
            pnlApprovalD.Visible = False
            pnlApprovalH.Visible = False


            Dim oSupplier As New Parameter.Supplier
            Dim m_Supplier As New SupplierController
            With oSupplier
                .strConnection = GetConnectionString()
                .SupplierName = lnkAPTO.Text.Trim
                .BankCode = lblBankCode.Text.Trim
            End With
            oSupplier = m_Supplier.GetSupplierAccountbySupplierID(oSupplier)


            If oSupplier.ListData.Rows.Count > 0 Then
                lblSupplier.Text = oSupplier.ListData.Rows(0).Item("SupplierName")
                lblNmBank.Text = oSupplier.ListData.Rows(0).Item("BankName")

                Dim sandiBank As String = ""
                If oSupplier.ListData.Rows(0).Item("BankCode").ToString.Length > 3 Then
                    sandiBank = oSupplier.ListData.Rows(0).Item("BankCode").ToString.Substring(0, 3)
                End If

                lblSandiBank.Text = sandiBank
                lblNmCabangBank.Text = oSupplier.ListData.Rows(0).Item("SupplierBankBranch")
                lblSandiCabang.Text = oSupplier.ListData.Rows(0).Item("BankCode")
                lblKota.Text = oSupplier.ListData.Rows(0).Item("SupplierCity")
                lblNoRekening.Text = oSupplier.ListData.Rows(0).Item("SupplierAccountNo")
                lblNamaRekening.Text = oSupplier.ListData.Rows(0).Item("SupplierAccountName")
                chekDefault.Checked = CBool(oSupplier.ListData.Rows(0).Item("DefaultAccount"))
                lblUntukPembayaran.Text = oSupplier.ListData.Rows(0).Item("UntukBayar")
                lblEfektifTanggal.Text = CDate(oSupplier.ListData.Rows(0).Item("TanggalEfektif")).ToString("dd/MM/yyyy")
                hdnBankID.Value = oSupplier.ListData.Rows(0).Item("SupplierBankID")
                hdnBankBranchID.Value = oSupplier.ListData.Rows(0).Item("SupplierBankBranchID")
            Else
                lblSupplier.Text = ""
                lblNmBank.Text = ""
                lblSandiBank.Text = ""
                lblNmCabangBank.Text = ""
                lblSandiCabang.Text = ""
                lblKota.Text = ""
                lblNoRekening.Text = ""
                chekDefault.Checked = False
                lblUntukPembayaran.Text = ""
                lblEfektifTanggal.Text = ""
            End If

        Else
            ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
        End If
    End Sub
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click

        Dim strSearch As New StringBuilder
        If txtTglJatuhTempo.Text <> "" Then
            If IsDate(ConvertDate2(txtTglJatuhTempo.Text)) Then

                Me.APType = cmbAPType.SelectedItem.Value
                Me.PVDate = txtTglJatuhTempo.Text
                strSearch.Append(" pv.APType='" & Me.APType & "'")
                strSearch.Append(" and pv.PaymentVoucherDate<='" & ConvertDate2(Me.PVDate) & "'")
                If txtSearch.Text.Trim <> "" Then
                    If Right(txtSearch.Text.Trim, 1) = "%" Then
                        strSearch.Append(" and pv.APTo like '" & txtSearch.Text & "'")
                    Else
                        strSearch.Append(" and pv.APTo='" & txtSearch.Text & "'")
                    End If
                End If
                strSearch.Append(" and pv.APBranchId = '" & Me.sesBranchId.Replace("'", "") & "'")
                Me.SearchBy = strSearch.ToString
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            Else
                ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
            End If
        Else
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub
    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        txtTglJatuhTempo.Text = ""
        cmbAPType.SelectedIndex = 0
        txtSearch.Text = ""
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
    End Sub
    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        If Not CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
            ShowMessage(lblMessage, "Tidak memiliki akses untuk Execute EBankTransfer", True)
            Exit Sub
        End If
        Dim listdata As New ArrayList
        Dim dtPVno As DataTable = createNewPVnoDT()


        For index = 0 To dtgDetailEditBank.Items.Count - 1
            Dim lblAccountPayableno As Label = CType(dtgDetailEditBank.Items(index).FindControl("lblAccountPayableno"), Label)
            Dim lblAccountPayableBranchID As Label = CType(dtgDetailEditBank.Items(index).FindControl("lblAccountPayableBranchID"), Label)
            Dim lnkPVNo As HyperLink = CType(dtgDetailEditBank.Items(index).FindControl("lnkPVNo"), HyperLink)
            Dim Chek As CheckBox = CType(dtgDetailEditBank.Items(index).FindControl("Chek"), CheckBox)

            If Chek.Checked Then
                Dim data As New Parameter.EBankTransfer
                With data
                    .AccountNameTo = lblNamaRekening.Text
                    .AccountNoTo = lblNoRekening.Text
                    .BankNameTo = lblNmBank.Text
                    .BankBranchTo = lblNmCabangBank.Text
                    .BankIdTo = hdnBankID.Value
                    .BankBranchID = CInt(IIf(hdnBankBranchID.Value = "", 0, hdnBankBranchID.Value))
                    .AccountPayAbleno = lblAccountPayableno.Text.Trim
                    .AccountPayAbleBranchID = lblAccountPayableBranchID.Text.Trim
                    .TransactionRefNo = lnkPVNo.Text.Trim
                    .BeneficiaryName = lblNamaRekening.Text
                    .BeneficiaryAccNo = lblNoRekening.Text
                    .ValueDate = ConvertDate2(lblEfektifTanggal.Text)
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .strConnection = GetConnectionString()
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                    .StatusFlag = "F"
                End With
                listdata.Add(data)
                dtPVno.Rows.Add(index + 1, lnkPVNo.Text.Trim, String.Empty)
            End If
        Next

        If listdata.Count = 0 Then
            ShowMessage(lblMessage, "silahkan pilih data terlebih dahulu", True)
            Return
        End If

        With oCustomClass
            .BranchId = Me.sesBranchId.Replace("'", "")
            .paymentvoucherno_DT = dtPVno
            .Notes = String.Empty
            .LoginId = Me.Loginid
            .PVStatus = "F"
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
            .UpdateMode = "F"
        End With
        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController
        Dim err As String = EBankController.EBankTransferSave(listdata, oCustomClass)

        If err <> "" Then
            ShowMessage(lblMessage, err, True)
        Else
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Edit Berhasil", False)
        End If
    End Sub
    Private Function DGItemToData(ByVal dgitem As DataGridItem) As Parameter.APDisbApp
        Dim data As New Parameter.APDisbApp
        With data
            Dim lnkPVNo As HyperLink = CType(dgitem.FindControl("lnkPVNo"), HyperLink)
            Dim lblPVAmount As Label = CType(dgitem.FindControl("lblPVAmount"), Label)
            Dim lblCompanyFullname As Label = CType(dgitem.FindControl("lblCompanyFullname"), Label)
            Dim lblCompanyAddress As Label = CType(dgitem.FindControl("lblCompanyAddress"), Label)
            Dim lblSandiKliringPusat As Label = CType(dgitem.FindControl("lblSandiKliringPusat"), Label)
            Dim lblBankNameTo As Label = CType(dgitem.FindControl("lblBankNameTo"), Label)
            Dim lblBankBranchTo As Label = CType(dgitem.FindControl("lblBankBranchTo"), Label)
            Dim lblBankAccountTo As Label = CType(dgitem.FindControl("lblBankAccountTo"), Label)
            Dim lblAccountNameTo As Label = CType(dgitem.FindControl("lblAccountNameTo"), Label)
            Dim lblPaymentNote As Label = CType(dgitem.FindControl("lblPaymentNote"), Label)
            Dim lblJenisTransfer As Label = CType(dgitem.FindControl("lblJenisTransfer"), Label)
            Dim rblJenisTransfer As RadioButtonList = CType(dgitem.FindControl("rblJenisTransfer"), RadioButtonList)
            Dim lblPVDate As Label = CType(dgitem.FindControl("lblPVDate"), Label)
            Dim lblApplicationID As Label = CType(dgitem.FindControl("lblApplicationID"), Label)
            Dim lblBankIdTo As Label = CType(dgitem.FindControl("lblBankIdTo"), Label)
            Dim lblCustomerName As Label = CType(dgitem.FindControl("lblCustomerName"), Label)

            .PaymentVoucherNo = lnkPVNo.Text
            .BankAccountID = lblBankIdTo.Text
            .BGNo = String.Empty
            .PVStatus = "P"
            .RecipientName = lblCustomerName.Text
            .PaymentNote = lblPaymentNote.Text
            .approvalBy = Loginid
            .WOP = "GT"
            .PVAmount = lblPVAmount.Text
            .APType = cmbAPType.SelectedValue
            .BusinessDate = BusinessDate
            .PVDate = lblPVDate.Text
            .ApplicationID = lblApplicationID.Text
            .ReferenceNo = lnkPVNo.Text

        End With

        Return data
    End Function
    Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitialDefaultPanel()
    End Sub
    Private Function createNewPVnoDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")
        Return dt
    End Function
    Protected Sub ButtonSaveRequestD_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSaveRequestD.Click
        Dim ListEbank As New List(Of String)
        Dim eBank As New Parameter.EBankTransfer
        Dim controller As New EBankTransferController

        If dgRequsetD.Items.Count > 0 Then
            Dim chek As New CheckBox

            For index = 0 To dgRequsetD.Items.Count - 1
                chek = CType(dgRequsetD.Items(0).FindControl("chekID"), CheckBox)

                If chek.Checked Then
                    ListEbank.Add(dgRequsetD.DataKeys(index).ToString.Trim)
                End If

            Next


            If ListEbank.Count > 0 Then

                For index = 0 To ListEbank.Count - 1
                    Dim dtPVno As DataTable = createNewPVnoDT()
                    dtPVno.Rows.Add((1).ToString, dgRequsetD.DataKeys(index).ToString.Trim, "")

                    eBank = New Parameter.EBankTransfer
                    eBank.strConnection = GetConnectionString()
                    eBank.WhereCond = ""
                    eBank.PVnoDT = dtPVno
                    eBank.StatusFlag = "RE"
                    eBank.BusinessDate = Me.BusinessDate
                    eBank.PaidDate = BusinessDate
                    eBank.GenerateDate = BusinessDate
                    eBank.EditedDate = BusinessDate
                    eBank.DeleteDate = BusinessDate
                    eBank.RejectDate = BusinessDate

                    controller.EBankTransferChangeStatus(eBank)
                Next
                InitialDefaultPanelRequestH()
                ShowMessage(lblMessage, "Data berhasil di request", False)
            Else
                ShowMessage(lblMessage, "List data belum anda cheked", True)
            End If


        Else
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        End If

    End Sub
    Protected Sub ButtonCancelRequestD_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancelRequestD.Click
        InitialDefaultPanelRequestH()
    End Sub
    Protected Sub ButtonSaveApproval_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSaveApproval.Click
        Dim ListEbank As New List(Of String)
        Dim eBank As New Parameter.EBankTransfer
        Dim controller As New EBankTransferController

        If dgApprovalD.Items.Count > 0 Then
            Dim chek As New CheckBox

            For index = 0 To dgApprovalD.Items.Count - 1
                chek = CType(dgApprovalD.Items(0).FindControl("chekID"), CheckBox)

                If chek.Checked Then
                    ListEbank.Add(dgApprovalD.DataKeys(index).ToString.Trim)
                End If

            Next


            If ListEbank.Count > 0 Then

                For index = 0 To ListEbank.Count - 1
                    Dim dtPVno As DataTable = createNewPVnoDT()
                    dtPVno.Rows.Add((1).ToString, dgApprovalD.DataKeys(index).ToString.Trim, "")

                    eBank = New Parameter.EBankTransfer
                    eBank.strConnection = GetConnectionString()
                    eBank.WhereCond = ""
                    eBank.PVnoDT = dtPVno
                    eBank.StatusFlag = "AE"
                    eBank.BusinessDate = Me.BusinessDate
                    eBank.PaidDate = BusinessDate
                    eBank.GenerateDate = BusinessDate
                    eBank.EditedDate = BusinessDate
                    eBank.DeleteDate = BusinessDate
                    eBank.RejectDate = BusinessDate

                    controller.EBankTransferChangeStatus(eBank)
                Next
                InitialDefaultPanelApprovalH()
                ShowMessage(lblMessage, "Data berhasil di approved", False)
            Else
                ShowMessage(lblMessage, "List data belum anda cheked", True)
            End If
        Else
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
        End If
    End Sub
    Protected Sub ButtonCancelApproval_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancelApproval.Click
        InitialDefaultPanelApprovalH()
    End Sub
End Class