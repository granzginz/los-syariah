﻿Imports Maxiloan.Webform.UserController
'Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports System.IO
Imports Maxiloan.General.CommonCookiesHelper
Imports System.Web
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions

Public Class EBankingPreparation
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtTglJatuhTempo, txtTglValuta As ucDateCE

    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    'Protected WithEvents oPVDueDate As ValidDate
    'Protected WithEvents oPVDateValuta As ValidDate
    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Dim Total As Double

    Private eBankController As New EBankTransferController
    Private m_controller As New DataUserControlController


    Protected WithEvents oListApNew As New ucAPTypeNew
    Private dController As New GeneralPagingController

#Region "properties"
    Private Property PaymentVoucherBranchID() As String
        Get
            Return (CType(ViewState("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherBranchID") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(ViewState("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVStatus") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(ViewState("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(ViewState("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(ViewState("APType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(ViewState("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVDate") = Value
        End Set
    End Property
    Private Property Branch() As String
        Get
            Return (CType(ViewState("Branch"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch") = Value
        End Set
    End Property
    Private Property cmdWhere() As String
        Get
            Return (CType(ViewState("cmdWhere"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("cmdWhere") = Value
        End Set
    End Property
    Private Property cmdWhere1() As String
        Get
            Return (CType(ViewState("cmdWhere1"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("cmdWhere1") = Value
        End Set
    End Property
    Private Property cmdWhere2() As String
        Get
            Return (CType(ViewState("cmdWhere2"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("cmdWhere2") = Value
        End Set
    End Property
    Private Property SortBy() As String
        Get
            Return (CType(ViewState("SortBy"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SortBy") = Value
        End Set
    End Property
    Private Property SortBy1() As String
        Get
            Return (CType(ViewState("SortBy1"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SortBy1") = Value
        End Set
    End Property

    Private Property SortBy2() As String
        Get
            Return (CType(ViewState("SortBy2"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SortBy2") = Value
        End Set
    End Property
    Private Property SKNAmount() As Double
        Get
            Return (CType(ViewState("SKNAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("SKNAmount") = Value
        End Set
    End Property
    Private Property RTGSAmount() As Double
        Get
            Return (CType(ViewState("RTGSAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("RTGSAmount") = Value
        End Set
    End Property
    Private Property InHouseAmount() As Double
        Get
            Return (CType(ViewState("InHouseAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("InHouseAmount") = Value
        End Set
    End Property
    Private Property TransferAmount() As Double
        Get
            Return (CType(ViewState("TransferAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TransferAmount") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            BindBankAccount()
            GenerateCombo(cboBranch, "spCboBranch", "Description", "ID")


            'With oBranch
            '    If Me.IsHoBranch Then
            '        .DataSource = m_controller.GetBranchAll(GetConnectionString)
            '        .DataValueField = "ID"
            '        .DataTextField = "Name"
            '        .DataBind()
            '    Else
            '        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
            '        .DataValueField = "ID"
            '        .DataTextField = "Name"
            '        .DataBind()
            '    End If
            '    .Items.Insert(0, "Select One")
            '    .Items(0).Value = "0"
            'End With


            Me.FormID = "EBPREP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
        lblMessage.Visible = False
    End Sub
    Private Sub GenerateCombo(ByVal cbogenerate As System.Web.UI.WebControls.DropDownList, ByVal spName As String, ByVal TextField As String, ByVal ValueField As String)
        Dim PGeneral As New Parameter.GeneralPaging
        Dim CGeneral As New GeneralPagingController
        Dim dtTemp As New DataTable


        With PGeneral
            .SpName = spName
            .strConnection = GetConnectionString()
            .CurrentPage = 1
            .PageSize = 100
            .WhereCond = ""
            .SortBy = ""
        End With

        dtTemp = CGeneral.GetGeneralPaging(PGeneral).ListData

        With cbogenerate
            .DataSource = dtTemp
            .DataTextField = TextField
            .DataValueField = ValueField
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .Items.Insert(1, "ALL")
            .Items(1).Value = "ALL"
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        pnlDetail.Visible = False
        'With oPVDueDate
        '    .FillRequired = True
        '    .FieldRequiredMessage = "Harap isi Tanggal Payment Voucher"
        '    .Display = "Dynamic"
        '    .ValidationErrMessage = "Harap isi dengan format dd/MM/yyyy"
        '    .isCalendarPostBack = False
        '    .dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")
        'End With
        txtTglJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

    End Sub

    'unresolvedmergeconflict Posisi akan di approve(RE) tidak boleh muncul di prepare
    Sub DoBind()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .WhereCond1 = cmdWhere1
            .WhereCond2 = cmdWhere2
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SortBy1 = SortBy1
            .SortBy2 = SortBy2
            .Flags = "App"
            .BranchId = cboBranch.SelectedValue.Trim
            .TglBayar = ConvertDate2(txtTglJatuhTempo.Text)
            .PayID = oListApNew.selectedAPTypeID
        End With

        oCustomClass = oController.APDisbAppList4(oCustomClass)

        DtUserList = oCustomClass.ListAPAppr
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        'DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList
        dtgEntity.DataBind()
        'Try
        '    dtgEntity.DataBind()
        'Catch ex As Exception
        '    dtgEntity.CurrentPageIndex = 0
        '    dtgEntity.DataBind()
        'End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
        pnlDetail.Visible = False
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        Me.PageState = currentPage
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Me.PaymentVoucherBranchID & "','" & style & "')"
    End Function
#End Region

#Region "Datagrid Command"

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub

    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblInsuranceCoyID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        Dim rblJenisTransfer As New RadioButtonList
        Dim lblJenisTransfer As New Label

        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lnkAgreementNoCustomer As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblInsuranceCoyID = CType(e.Item.FindControl("lblInsuranceCoyID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            rblJenisTransfer = CType(e.Item.FindControl("rblJenisTransfer"), RadioButtonList)
            lblJenisTransfer = CType(e.Item.FindControl("lblJenisTransfer"), Label)
            lnkAgreementNoCustomer = CType(e.Item.FindControl("lnkAgreementNoCustomer"), HyperLink)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            'If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
            lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            'End If
            lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"

            lnkAgreementNoCustomer.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
            'If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
            '    'lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
            '    'Select Case cmbAPType.SelectedItem.Value
            '    '    Case "SPPL"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
            '    '    Case "INSR"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblInsuranceCoyID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
            '    '    Case "RCST"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

            '    '    Case "RADV"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
            '    '    Case "ASRP"
            '    '        lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            '    'End Select

            'End If


            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 0)
            rblJenisTransfer.SelectedValue = lblJenisTransfer.Text
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 0)
        End If

    End Sub

#End Region

    Private Sub BindBankAccount()
        'Dim dtBankAccount As New DataTable
        'Dim oDataUserControlController As New DataUserControlController

        'dtBankAccount = oDataUserControlController.GetBankAccount(GetConnectionString, Me.sesBranchId.Replace("'", ""), "BA", "EB")

        'cmbRekening.DataTextField = "Name"
        'cmbRekening.DataValueField = "ID"
        'cmbRekening.DataSource = dtBankAccount
        'cmbRekening.DataBind()

        'cmbRekening.Items.Insert(0, "Select One")
        'cmbRekening.Items(0).Value = "0"

        Dim oEntities As New Parameter.GeneralPaging
        Dim strCacheName As String
        'strCacheName = CACHE_BANK_ACCOUNT_NO_CONDITION & Me.sesBranchId.Replace("'", "")
        Dim dtBankAccount As New DataSet

        With oEntities
            .SpName = "spGetBankAccountNoCondition"
            .WhereCond = " And IsEBanking = 1 and IsActive = 1  "
            .strConnection = GetConnectionString()
        End With

        oEntities = dController.GetReportWithParameterWhereCond(oEntities)
        dtBankAccount = oEntities.ListDataReport

        'Me.Cache.Insert(strCacheName, dtBankAccountCache, Nothing, DateTime.Now.AddHours(DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
        'dtBankAccount = CType(Me.Cache.Item(strCacheName), DataSet)



        cmbRekening.DataTextField = "Name"
        cmbRekening.DataValueField = "ID"
        cmbRekening.DataSource = dtBankAccount
        cmbRekening.DataBind()


        cmbRekening.Items.Insert(0, "Select One")
        cmbRekening.Items(0).Value = "0"
    End Sub

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder
        Dim strSearch1 As New StringBuilder
        Dim strSearch2 As New StringBuilder

        'strSearch.Append(" pv.PVstatus in ('F')  ")
        'Modify by Wira 20180210  ambil data yang sudah diapprove dan pemilihan rekening bank
        strSearch.Append(" pv.PVstatus in ('B')  ")


        If cboBranch.SelectedValue = "ALL" Then
            If oListApNew.selectedAPTypeID = "ALL" Then
                'Strsearch not used
            Else

                'Branch 'ALL' & APtype Specified
                strSearch.Append(" AND pv.APType = '" & oListApNew.selectedAPTypeID & "'")
                strSearch1.Append("TransferFundTransaction.TransferType = '" & oListApNew.selectedAPTypeID & "' AND ")
                strSearch2.Append("APType = '" & oListApNew.selectedAPTypeID & "' AND ")
            End If
        Else
            'Branch Specified & APtype 'ALL'
            If oListApNew.selectedAPTypeID = "ALL" Then
                strSearch.Append(" AND pv.APBranchID = '" & cboBranch.SelectedValue.ToString.Trim & "'")
                strSearch1.Append("TransferFundTransaction.BranchIDTo = '" & cboBranch.SelectedValue.ToString.Trim & "' AND ")
                strSearch2.Append("a.BranchIDTo = '" & cboBranch.SelectedValue.ToString.Trim & "' AND ")
            Else
                'Branch Specified & APtype Specified
                strSearch.Append(" AND pv.APBranchID = '" & cboBranch.SelectedValue.ToString.Trim & "'")
                strSearch1.Append("TransferFundTransaction.BranchIDTo = '" & cboBranch.SelectedValue.ToString.Trim & "' AND ")
                strSearch2.Append("a.BranchIDTo = '" & cboBranch.SelectedValue.ToString.Trim & "' AND ")

                strSearch.Append(" AND pv.APType = '" & oListApNew.selectedAPTypeID & "'")
                strSearch1.Append("TransferFundTransaction.TransferType = '" & oListApNew.selectedAPTypeID & "' AND ")
                strSearch2.Append("APType = '" & oListApNew.selectedAPTypeID & "' AND ")
            End If


        End If


        If txtTglJatuhTempo.Text <> "" Then
            If IsDate(ConvertDate2(txtTglJatuhTempo.Text)) Then
                'Me.APType = cmbAPType.SelectedItem.Value
                Me.PVDate = txtTglJatuhTempo.Text
                'Me.Branch = oBranch.SelectedValue

                'strSearch.Append(" pv.APType='" & Me.APType & "'")
                strSearch.Append(" AND pv.pvDueDate='" & ConvertDate2(Me.PVDate) & "'")
                'If txtSearch.Text.Trim <> "" Then
                '    If Right(txtSearch.Text.Trim, 1) = "%" Then
                '        strSearch.Append(" and pv.APTo Like '" & txtSearch.Text & "'")
                '    Else
                '        strSearch.Append(" and pv.APTo='" & txtSearch.Text & "'")
                '    End If
                'End If
                'strSearch.Append(" and pv.BranchID = '" & Me.Branch & "'")

                strSearch1.Append(" TransferFundTransaction.PostingDate='" & ConvertDate2(Me.PVDate) & "'")
                strSearch2.Append(" a.PostingDate='" & ConvertDate2(Me.PVDate) & "'")

                Me.cmdWhere = strSearch.ToString
                Me.cmdWhere1 = strSearch1.ToString
                Me.cmdWhere2 = strSearch1.ToString
                Me.SortBy = ""
                Me.SortBy1 = ""
                Me.SortBy2 = ""
                DoBind()
            Else
                ShowMessage(lblMessage, "Format tanggal salah", True)
            End If
        Else
            ShowMessage(lblMessage, "Harap isi tanggal", True)
        End If



    End Sub

    Private Sub imgNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonNext.Click
        If CheckFeature(Me.Loginid, Me.FormID, "NEXT", Me.AppId) Then
            Dim chkDtList As CheckBox
            Dim lBlnValid As Boolean
            Dim i As Integer
            For i = 0 To dtgEntity.Items.Count - 1
                chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
                If Not IsNothing(chkDtList) Then
                    If chkDtList.Checked Then
                        lBlnValid = True
                        Exit For
                    End If
                End If
            Next
            If Not lBlnValid Then
                ShowMessage(lblMessage, "Harap pilih pembayaran", True)
                Exit Sub
            Else
                pnlDetail.Visible = True
                pnlDatagrid.Visible = False
                pnlSearch.Visible = False

                txtTglValuta.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            End If
        End If
    End Sub

    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
        pnlDetail.Visible = False
        DoBind()
    End Sub

    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        Response.Redirect("EBankingPreparation.aspx")
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSave.Click
        Dim chkDtList As CheckBox
        Dim listdata As New ArrayList
        Dim data As New Parameter.EBankTransfer
        Dim dtPVno As DataTable = createNewPVnoDT()
        For i = 0 To dtgEntity.Items.Count - 1
            chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then

                    data = DGItemToData(dtgEntity.Items(i))
                    listdata.Add(data)
                    'SaveToTable(dtgEntity.Items(i))
                    dtPVno.Rows.Add((i + 1).ToString, data.TransactionRefNo, String.Empty)
                End If
            End If
        Next

        If listdata.Count = 0 Then
            ShowMessage(lblMessage, "silahkan pilih data terlebih dahulu", True)
            Return
        End If

        With oCustomClass
            .BranchId = Me.sesBranchId.Replace("'", "")
            .paymentvoucherno_DT = dtPVno
            .Notes = String.Empty
            .LoginId = Me.Loginid
            .PVStatus = "G"
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
            .UpdateMode = "G"
        End With

        'Select Case data.APType
        '    Case "PYREQ", "PCREI"
        '        lblMessage.Text = ""
        '    Case Else
        '        lblMessage.Text = eBankController.EBankTransferSave(listdata, oCustomClass)
        'End Select

        'If Not data.APType = "PYREQ" And Not data.APType = "PCREI" Then
        '    lblMessage.Text = eBankController.EBankTransferSave(listdata, oCustomClass)
        'Else
        '    lblMessage.Text = ""
        'End If

        'DoChangeStatus("G")


        Try
            lblMessage.Text = eBankController.EBankTransferSave(listdata, oCustomClass)

            If lblMessage.Text = "" Then
                'generateCityDirect()
                DoChangeStatus("G")
                'generateBNIDirect(dtPVno)
                'Modify by Wira 20180408, ada permintaan format baru csv. RTGS, SKN dan LLG disatukan
                generateBNIDirectNewFormat(dtPVno)
                InitialDefaultPanel()
                DoBind()

                ShowMessage(lblMessage, "Data has been saved", False)
            Else
                ShowMessage(lblMessage, lblMessage.Text, True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


    End Sub

    Private Sub DoChangeStatus(ByVal status As String)
        Dim chkDtList As CheckBox
        Dim dtPVno As DataTable = createNewPVnoDT()
        Dim lnkPVNo As HyperLink

        For i As Integer = 0 To dtgEntity.Items.Count - 1
            chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lnkPVNo = CType(dtgEntity.Items(i).FindControl("lnkPVNo"), HyperLink)
                    dtPVno.Rows.Add((i + 1).ToString, lnkPVNo.Text.Trim, String.Empty)
                End If
            End If
        Next

        With oCustomClass
            .BranchId = Me.sesBranchId.Replace("'", "")
            .paymentvoucherno_DT = dtPVno
            .Notes = String.Empty
            .LoginId = Me.Loginid
            .PVStatus = status
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
            .UpdateMode = status
        End With
        lblMessage.Text = oController.UpdateAPDisbApp2(oCustomClass)
        If lblMessage.Text <> "" Then
            ShowMessage(lblMessage, lblMessage.Text, True)
        End If
    End Sub

    Private Function createNewPVnoDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")
        Return dt
    End Function

    'Private Sub SaveToTable(ByVal dgitem As DataGridItem)
    '    Dim data As Parameter.EBankTransfer = DGItemToData(dgitem)

    '    With data
    '        .BranchId = Me.sesBranchId.Replace("'", "")
    '        .strConnection = GetConnectionString()
    '        .LoginId = Me.Loginid
    '        .BusinessDate = Me.BusinessDate
    '    End With

    '    lblMessage.Text = eBankController.EBankTransferSave(data)
    '    If lblMessage.Text <> "" Then
    '        ShowMessage(lblMessage, lblMessage.Text, False)
    '    End If
    'End Sub

    Private Function DGItemToData(ByVal dgitem As DataGridItem) As Parameter.EBankTransfer
        Dim data As New Parameter.EBankTransfer
        With data
            Dim lnkPVNo As HyperLink = CType(dgitem.FindControl("lnkPVNo"), HyperLink)
            Dim lblPVAmount As Label = CType(dgitem.FindControl("lblPVAmount"), Label)
            Dim lblCompanyFullname As Label = CType(dgitem.FindControl("lblCompanyFullname"), Label)
            Dim lblCompanyAddress As Label = CType(dgitem.FindControl("lblCompanyAddress"), Label)
            Dim lblSandiKliringPusat As Label = CType(dgitem.FindControl("lblSandiKliringPusat"), Label)
            Dim lblBankNameTo As Label = CType(dgitem.FindControl("lblBankNameTo"), Label)
            Dim lblBankBranchTo As Label = CType(dgitem.FindControl("lblBankBranchTo"), Label)
            Dim lblBankAccountTo As Label = CType(dgitem.FindControl("lblBankAccountTo"), Label)
            Dim lblAccountNameTo As Label = CType(dgitem.FindControl("lblAccountNameTo"), Label)
            Dim lblPaymentNote As Label = CType(dgitem.FindControl("lblPaymentNote"), Label)
            Dim lblJenisTransfer As Label = CType(dgitem.FindControl("lblJenisTransfer"), Label)
            Dim rblJenisTransfer As RadioButtonList = CType(dgitem.FindControl("rblJenisTransfer"), RadioButtonList)
            Dim lblBeneficiaryBankAddress As Label = CType(dgitem.FindControl("lblBeneficiaryBankAddress"), Label)
            Dim lblAPType As Label = CType(dgitem.FindControl("lblAPType"), Label)
            Dim lblBranchIdAP As Label = CType(dgitem.FindControl("lblBranchIdAP"), Label)

            .TransactionRefNo = lnkPVNo.Text
            '.ValueDate = CDate(txtTglValuta.Text).ToString("yyyyMMdd")
            .ValueDate = Format(ConvertDate2(txtTglValuta.Text), "yyyyMMdd")

            .Currency = "IDR"
            .Amount = lblPVAmount.Text
            .OrderingParty = lblCompanyFullname.Text
            .OrderingPartyAddress = lblCompanyAddress.Text
            .BankAccountId = cmbRekening.SelectedValue
            .BankRoutingMethod = "I2"
            .BankCode = lblSandiKliringPusat.Text
            .BeneficiaryBankName = lblBankNameTo.Text
            .BeneficiaryAccNo = lblBankAccountTo.Text
            .BeneficiaryName = lblAccountNameTo.Text
            .BeneficiaryAddress1 = "SKN/1/0"
            .BeneficiaryAddress2 = lblBankBranchTo.Text
            .BeneficiaryAddress3 = lblBankBranchTo.Text
            .InterBankRoutingMethod = String.Empty
            .InterBankSwiftCode = String.Empty
            .IntermediaryBankName = String.Empty
            .PaymentDetail1 = lblPaymentNote.Text
            .PaymentDetail2 = String.Empty
            .PaymentDetail3 = String.Empty
            .PaymentDetail4 = String.Empty
            .BtoBInfoCitibank = IIf(rblJenisTransfer.SelectedValue = "RTGS", "/ACC/RTGS", String.Empty)
            .BtoBInfoBeneBank = String.Empty
            .BtoBInfoInterBank = String.Empty
            .Charges = "OUR"
            .EndFile = "END"
            .DtmUpdate = BusinessDate
            .UserUpdate = UserID
            .BeneficiaryBankAddress = lblBeneficiaryBankAddress.Text
            .APType = lblAPType.Text.Trim

            '.BranchId = Me.sesBranchId.Replace("'", "")
            .BranchId = lblBranchIdAP.Text.Trim()
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate

            'Modify by Wira 20180223 add jenis transfer
            .JenisTransfer = rblJenisTransfer.SelectedValue
        End With

        Return data
    End Function

    'this does all the work to export  excel
    Public Sub ExportTableData(ByVal dtdata As DataTable)
        Dim attach As String = "attachment;filename=journal.xls"
        Response.ClearContent()
        Response.AddHeader("content-disposition", attach)
        Response.ContentType = "application/ms-excel"
        If (Not dtdata Is Nothing) Then
            For Each dc As DataColumn In dtdata.Columns
                Response.Write(dc.ColumnName + "\t")
                'sep = ";";
            Next

            Response.Write(System.Environment.NewLine)
            For Each dr As DataRow In dtdata.Rows
                For i = 0 To dtdata.Columns.Count - 1
                    Response.Write(dr(i).ToString() + "\t")
                Next
                Response.Write("\n")
            Next

            Response.End()
        End If



    End Sub

    'Private Sub imgExport_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgExport.Click
    '    With oCustomClass
    '        .strConnection = GetConnectionString()
    '        .WhereCond = String.Empty
    '        .CurrentPage = currentPage
    '        .PageSize = pageSize
    '        .SortBy = SortBy
    '        .Flags = "App"
    '    End With

    '    oCustomClass = oController.APDisbAppList3(oCustomClass)
    '    Dim DtUserList As DataTable = oCustomClass.ListAPAppr



    'End Sub



#Region "Generate Citidirect"
    'Private Sub generateCityDirect()
    '    Dim chkDtList As CheckBox
    '    Dim dtPVno As DataTable = createNewPVnoDT()
    '    Dim lnkPVNo As HyperLink

    '    For i As Integer = 0 To dtgEntity.Items.Count - 1

    '        chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
    '        If Not IsNothing(chkDtList) Then
    '            If chkDtList.Checked Then
    '                lnkPVNo = CType(dtgEntity.Items(i).FindControl("lnkPVNo"), HyperLink)
    '                dtPVno.Rows.Add((i + 1).ToString, lnkPVNo.Text.Trim, String.Empty)
    '            End If
    '        End If

    '    Next

    '    Dim EBankData As New Parameter.EBankTransfer
    '    Dim EBankController As New EBankTransferController

    '    With EBankData
    '        .strConnection = GetConnectionString()
    '        .WhereCond = String.Empty
    '        .BusinessDate = Me.BusinessDate
    '        .BranchId = Me.sesBranchId.Replace("'", "")
    '        .PVnoDT = dtPVno
    '        .SortBy = SortBy
    '    End With

    '    EBankData = EBankController.EBankTransferList(EBankData)

    '    'change status to G
    '    EBankData.StatusFlag = "G"
    '    EBankData.GenerateBy = Me.Loginid
    '    EBankData.GenerateDate = BusinessDate
    '    EBankData.RejectDate = BusinessDate
    '    EBankData.PaidDate = BusinessDate
    '    EBankData.DeleteDate = BusinessDate
    '    EBankData.EditedDate = BusinessDate

    '    Dim err As String = EBankController.EBankTransferChangeStatus(EBankData)
    '    Dim dtResult As DataTable = EBankData.ListAPAppr

    '    Convert(dtResult, Response, "ebanking" & Me.BusinessDate.ToString("ddMMyyyy"))
    'End Sub

    Public Sub Convert(ByVal dt As DataTable, ByVal response As HttpResponse, ByVal strFileName As String, ByVal csvFileName As String, ByVal csvFileCreation As String, ByVal csvSeq As String,
                       ByVal csvFileCreation2 As String, ByVal csvRowsTotal As Integer, ByVal csvBankAccountTo As String, ByVal csvTotAmount As Double)

        Dim FileName As String
        FileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        FileName += strFileName + ".csv"
        response.Clear()
        Dim sw As New StreamWriter(FileName + "", False)

        'write header
        Dim iColCount As Integer = dt.Columns.Count
        'For i As Integer = 0 To iColCount - 1
        '    sw.Write(dt.Columns(i))
        '    If i < iColCount - 1 Then
        '        sw.Write(",")
        '    End If
        'Next
        'sw.Write(sw.NewLine)

        sw.Write(csvFileCreation & "," & csvSeq & "," & csvFileName & ",,,,,,,,,,,,,,,,,")
        sw.Write(sw.NewLine)
        sw.Write("P" & "," & csvFileCreation2 & "," & csvBankAccountTo.Trim & "," & csvRowsTotal & "," & csvTotAmount & ",,,,,,,,,,,,,,,,,")
        sw.Write(sw.NewLine)

        'Write rows
        For Each dr As DataRow In dt.Rows
            For i As Integer = 0 To iColCount - 1
                If Not IsDBNull(dr(i)) Then
                    'maintain leading zero untuk nomor rekening dan bank code
                    'If i = 6 Or i = 8 Or i = 11 Then
                    '    sw.Write("=""" + dr(i).ToString() + """")
                    'Else
                    '    sw.Write(dr(i).ToString().Trim.Replace(",", " ").Replace("'", ""))
                    'End If
                    sw.Write(dr(i).ToString().Trim.Replace(",", " ").Replace("'", ""))

                End If
                If i < iColCount - 1 Then
                    sw.Write(",")
                End If
            Next
            sw.Write(sw.NewLine)
        Next
        sw.Close()

        'response.Clear()
        'response.ContentType = "application/csv"
        'response.AddHeader("Content-Disposition", "attachment; filename=" & strFileName + ".csv" & "")
        'response.WriteFile(FileName)
        'response.Flush()
        'response.End()

        Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
        Dim strNameServer = Request.ServerVariables("SERVER_NAME")
        Dim StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Dim strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & strFileName & ".csv"
        response.Write("<script language = javascript> var x = 10; var y =  10; window.open('" & strFileLocation & "','accacq', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0')   </script>")
    End Sub

    Private Sub generateBNIDirect(dtPVnox As DataTable)
        'update EbankFormat------------------------------------------------------------------------------'
        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController

        With EBankData
            .strConnection = GetConnectionString()
            .WhereCond = String.Empty
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.sesBranchId.Replace("'", "")
            .PVnoDT = dtPVnox
            .SortBy = SortBy
        End With

        EBankData = EBankController.EBankTransferList(EBankData)

        'change status to G
        EBankData.StatusFlag = "G"
        EBankData.GenerateBy = Me.Loginid
        EBankData.GenerateDate = BusinessDate
        EBankData.RejectDate = BusinessDate
        EBankData.PaidDate = BusinessDate
        EBankData.DeleteDate = BusinessDate
        EBankData.EditedDate = BusinessDate

        Dim err As String = EBankController.EBankTransferChangeStatus(EBankData)
        '------------------------------------------------------------------------------'
        Dim chkDtList As CheckBox
        Dim JenisTransfer As Label
        Dim dtPVnoSKN As DataTable = createNewPVnoDT()
        Dim dtPVnoRTGS As DataTable = createNewPVnoDT()
        Dim dtPVnoInHouse As DataTable = createNewPVnoDT()
        Dim lnkPVNo As HyperLink

        Dim csvFileName As String
        Dim csvJenisTransfer As String
        Dim csvTime As String
        Dim csvFileCreation As String
        Dim csvSeq As String
        Dim csvFileCreation2 As String
        Dim csvRowsTotal As Integer
        Dim csvBankAccountTo As String
        Dim lblPVAmount As Label
        csvBankAccountTo = cmbRekening.SelectedValue
        Me.SKNAmount = 0
        Me.RTGSAmount = 0
        Me.InHouseAmount = 0

        For i As Integer = 0 To dtgEntity.Items.Count - 1

            chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
            JenisTransfer = CType(dtgEntity.Items(i).FindControl("lblJenisTransfer"), Label)

            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lnkPVNo = CType(dtgEntity.Items(i).FindControl("lnkPVNo"), HyperLink)

                    lblPVAmount = CType(dtgEntity.Items(i).FindControl("lblPVAmount"), Label)
                    If JenisTransfer.Text = "SKN" Then
                        dtPVnoSKN.Rows.Add((i + 1).ToString, lnkPVNo.Text.Trim, String.Empty)
                        Me.SKNAmount = Me.SKNAmount + lblPVAmount.Text.Trim
                    End If

                    If JenisTransfer.Text = "RTGS" Then
                        dtPVnoRTGS.Rows.Add((i + 1).ToString, lnkPVNo.Text.Trim, String.Empty)
                        Me.RTGSAmount = Me.RTGSAmount + lblPVAmount.Text.Trim
                    End If

                    If JenisTransfer.Text = "INHO" Or JenisTransfer.Text = "InHo" Or JenisTransfer.Text = "GT" Then
                        dtPVnoInHouse.Rows.Add((i + 1).ToString, lnkPVNo.Text.Trim, String.Empty)
                        Me.InHouseAmount = Me.InHouseAmount + lblPVAmount.Text.Trim
                    End If

                End If
            End If

        Next

        'Dim EBankData As New Parameter.EBankTransfer
        'Dim EBankController As New EBankTransferController

        'CSV untuk SKN
        If dtPVnoSKN.Rows.Count > 0 Then
            With EBankData
                .strConnection = GetConnectionString()
                .WhereCond = String.Empty
                .BusinessDate = Me.BusinessDate
                .BranchId = Me.sesBranchId.Replace("'", "")
                .PVnoDT = dtPVnoSKN
                .SortBy = SortBy
            End With

            EBankData = EBankController.EBankTransferList(EBankData)

            Dim dtResult As DataTable = EBankData.ListAPAppr

            csvFileName = "BNIMF-310118-4"
            csvJenisTransfer = "_LLG_"
            csvTime = "_" & DateTime.Now.ToString("HHmmss")
            csvFileCreation = Date.Now.ToString("yyyy/mm/dd") & "_" & DateTime.Now.ToString("HH:mm:ss")
            csvSeq = "4"
            csvFileCreation2 = Date.Now.ToString("yyyyMMdd")
            csvRowsTotal = dtPVnoSKN.Rows.Count

            Convert(dtResult, Response, csvFileName & csvJenisTransfer & Me.BusinessDate.ToString("yyyyMMdd") & csvTime, csvFileName, csvFileCreation, csvSeq, csvFileCreation2, csvRowsTotal, csvBankAccountTo, Me.SKNAmount)
        End If

        'CSV untuk RTGS
        If dtPVnoRTGS.Rows.Count > 0 Then
            With EBankData
                .strConnection = GetConnectionString()
                .WhereCond = String.Empty
                .BusinessDate = Me.BusinessDate
                .BranchId = Me.sesBranchId.Replace("'", "")
                .PVnoDT = dtPVnoRTGS
                .SortBy = SortBy
            End With

            EBankData = EBankController.EBankTransferList(EBankData)

            Dim dtResult As DataTable = EBankData.ListAPAppr

            csvFileName = "BNIMF-300118-10"
            csvJenisTransfer = "_RTGS_"
            csvTime = "_" & DateTime.Now.ToString("HHmmss")
            csvFileCreation = Date.Now.ToString("yyyy/mm/dd") & "_" & DateTime.Now.ToString("HH:mm:ss")
            csvSeq = "3"
            csvFileCreation2 = Date.Now.ToString("yyyyMMdd")
            csvRowsTotal = dtPVnoRTGS.Rows.Count

            Convert(dtResult, Response, csvFileName & csvJenisTransfer & Me.BusinessDate.ToString("yyyyMMdd") & csvTime, csvFileName, csvFileCreation, csvSeq, csvFileCreation2, csvRowsTotal, csvBankAccountTo, Me.RTGSAmount)
        End If

        'CSV untuk InHouse
        If dtPVnoInHouse.Rows.Count > 0 Then
            With EBankData
                .strConnection = GetConnectionString()
                .WhereCond = String.Empty
                .BusinessDate = Me.BusinessDate
                .BranchId = Me.sesBranchId.Replace("'", "")
                .PVnoDT = dtPVnoInHouse
                .SortBy = SortBy
            End With

            EBankData = EBankController.EBankTransferList(EBankData)

            Dim dtResult As DataTable = EBankData.ListAPAppr

            csvFileName = "BNIMF-310118-3"
            csvJenisTransfer = "_IH_"
            csvTime = "_" & DateTime.Now.ToString("HHmmss")
            csvFileCreation = Date.Now.ToString("yyyy/mm/dd") & "_" & DateTime.Now.ToString("HH:mm:ss")
            csvSeq = "3"
            csvFileCreation2 = Date.Now.ToString("yyyyMMdd")
            csvRowsTotal = dtPVnoInHouse.Rows.Count

            Convert(dtResult, Response, csvFileName & csvJenisTransfer & Me.BusinessDate.ToString("yyyyMMdd") & csvTime, csvFileName, csvFileCreation, csvSeq, csvFileCreation2, csvRowsTotal, csvBankAccountTo, Me.InHouseAmount)
        End If
    End Sub

    Private Sub generateBNIDirectNewFormat(dtPVnox As DataTable)
        'update EbankFormat------------------------------------------------------------------------------'
        Dim EBankData As New Parameter.EBankTransfer
        Dim EBankController As New EBankTransferController

        With EBankData
            .strConnection = GetConnectionString()
            .WhereCond = String.Empty
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.sesBranchId.Replace("'", "")
            .PVnoDT = dtPVnox
            .SortBy = SortBy
        End With

        EBankData = EBankController.EBankTransferList(EBankData)

        'change status to G
        EBankData.StatusFlag = "G"
        EBankData.GenerateBy = Me.Loginid
        EBankData.GenerateDate = BusinessDate
        EBankData.RejectDate = BusinessDate
        EBankData.PaidDate = BusinessDate
        EBankData.DeleteDate = BusinessDate
        EBankData.EditedDate = BusinessDate

        Dim err As String = EBankController.EBankTransferChangeStatus(EBankData)
        '------------------------------------------------------------------------------'
        Dim chkDtList As CheckBox
        Dim JenisTransfer As Label
        Dim lnkPVNo As HyperLink

        Dim csvFileName As String
        Dim csvTime As String
        Dim csvFileCreation As String
        Dim csvSeq As String
        Dim csvFileCreation2 As String
        Dim csvRowsTotal As Integer
        Dim csvBankAccountTo As String
        Dim lblPVAmount As Label
        csvBankAccountTo = cmbRekening.SelectedValue
        Me.TransferAmount = 0

        For i As Integer = 0 To dtgEntity.Items.Count - 1

            chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
            JenisTransfer = CType(dtgEntity.Items(i).FindControl("lblJenisTransfer"), Label)

            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lnkPVNo = CType(dtgEntity.Items(i).FindControl("lnkPVNo"), HyperLink)

                    lblPVAmount = CType(dtgEntity.Items(i).FindControl("lblPVAmount"), Label)
                    Me.TransferAmount = Me.TransferAmount + lblPVAmount.Text.Trim

                End If
            End If

        Next

        'Dim EBankData As New Parameter.EBankTransfer
        'Dim EBankController As New EBankTransferController

        'CSV untuk all
        If dtPVnox.Rows.Count > 0 Then
            With EBankData
                .strConnection = GetConnectionString()
                .WhereCond = String.Empty
                .BusinessDate = Me.BusinessDate
                .BranchId = Me.sesBranchId.Replace("'", "")
                .PVnoDT = dtPVnox
                .SortBy = SortBy
            End With

            EBankData = EBankController.EBankTransferList(EBankData)

            Dim dtResult As DataTable = EBankData.ListAPAppr

            csvFileName = "UploadFile_"
            csvTime = "_" & DateTime.Now.ToString("HHmmss")
            csvFileCreation = Date.Now.ToString("yyyy/mm/dd") & "_" & DateTime.Now.ToString("HH:mm:ss")
            csvSeq = "4"
            csvFileCreation2 = Date.Now.ToString("yyyyMMdd")
            csvRowsTotal = dtPVnox.Rows.Count

            ConvertNewFomat(dtResult, Response, csvFileName & Me.BusinessDate.ToString("yyyyMMdd") & csvTime, Me.TransferAmount)
        End If

    End Sub
    Public Sub ConvertNewFomat(ByVal dt As DataTable, ByVal response As HttpResponse, ByVal strFileName As String, ByVal csvTotAmount As Double)

        Dim FileName As String
        FileName = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        FileName += strFileName + ".txt"
        response.Clear()
        Dim sw As New StreamWriter(FileName + "", False)

        'write header
        Dim iColCount As Integer = dt.Columns.Count
        'For i As Integer = 0 To iColCount - 1
        '    sw.Write(dt.Columns(i))
        '    If i < iColCount - 1 Then
        '        sw.Write(",")
        '    End If
        'Next
        'sw.Write(sw.NewLine)

        'sw.Write(csvFileCreation & "," & csvSeq & "," & csvFileName & ",,,,,,,,,,,,,,,,,")
        'sw.Write(sw.NewLine)
        'sw.Write("P" & "," & csvFileCreation2 & "," & csvBankAccountTo.Trim & "," & csvRowsTotal & "," & csvTotAmount & ",,,,,,,,,,,,,,,,,")
        'sw.Write(sw.NewLine)

        'Write rows
        For Each dr As DataRow In dt.Rows
            For i As Integer = 0 To iColCount - 1
                If Not IsDBNull(dr(i)) Then
                    'maintain leading zero untuk nomor rekening dan bank code
                    'If i = 6 Or i = 8 Or i = 11 Then
                    '    sw.Write("=""" + dr(i).ToString() + """")
                    'Else
                    '    sw.Write(dr(i).ToString().Trim.Replace(",", " ").Replace("'", ""))
                    'End If
                    sw.Write(dr(i).ToString().Trim.Replace(",", " ").Replace("'", ""))

                End If
                If i < iColCount - 1 Then
                    sw.Write(",")
                End If
            Next
            sw.Write(sw.NewLine)
        Next
        sw.Close()

        'response.Clear()
        'response.ContentType = "application/csv"
        'response.AddHeader("Content-Disposition", "attachment; filename=" & strFileName + ".csv" & "")
        'response.WriteFile(FileName)
        'response.Flush()
        'response.End()

        Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
        Dim strNameServer = Request.ServerVariables("SERVER_NAME")
        Dim StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Dim strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & strFileName & ".txt"
        response.Write("<script language = javascript> var x = 10; var y =  10; window.open('" & strFileLocation & "','accacq', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0')   </script>")
    End Sub
#End Region

End Class
