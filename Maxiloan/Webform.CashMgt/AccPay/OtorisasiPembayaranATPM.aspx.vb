﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class OtorisasiPembayaranATPM
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oTobankAccount As ucBankAccountBranch

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 6
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.Invoice
    Private oController As New InvoiceController
    Private m_Controller As New TransferHOBranchController
#End Region

    Property VoucherNo As String
        Get
            Return CType(ViewState("VoucherNo"), String)
        End Get
        Set(value As String)
            ViewState("VoucherNo") = value
        End Set
    End Property

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If Request.QueryString("Message") = MessageHelper.MESSAGE_UPDATE_SUCCESS Then _
                ShowMessage(lblMessage, Request.QueryString("Message"), False)

            Me.FormID = "ATPMOTORLIST"
            oSearchBy.ListData = "Name, Nama-Agreementno, No. Kontrak-Address, Alamat-InstallmentAmount, Angsuran"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim dtEntity As DataTable = Nothing
        Dim DvUserList As New DataView
     
        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ReceiveATPMList(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListATPM
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        DvUserList = dtEntity.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgree.DataSource = DvUserList

        Try
            DtgAgree.DataBind()
        Catch
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
        End Try
        pnlList.Visible = True
        pnlDatagrid.Visible = True

        lblMessage.Text = ""
    End Sub

#End Region
#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Response.Redirect("OtorisasiPembayaranATPM.aspx")
    End Sub
#End Region

#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "' "
        'Me.SearchBy = Me.SearchBy & "and AgreementNo = '030112150050'"
        Me.SearchBy = Me.SearchBy & " and Flag = 'R'"

        If oSearchBy.Text.Trim <> "" Then
            If oSearchBy.ValueID = "SupplierID" And Not (IsNumeric(oSearchBy.Text.Trim)) Then
                Exit Sub
            End If
            Me.SearchBy = String.Format("{0} and {1} like '%{2}%'", Me.SearchBy, oSearchBy.ValueID, oSearchBy.Text.Trim.Replace("'", "''"))

        End If
        lblMessage.Visible = False
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlList.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "imbSave"
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim oATPMController As New InvoiceController
            Dim oEntities As New Parameter.Invoice
            oEntities.strConnection = GetConnectionString()
            For n As Integer = 0 To DtgAgree.DataKeys.Count - 1
                Dim chkPilih As CheckBox = CType(DtgAgree.Items(n).FindControl("chkPilih"), CheckBox)
                If chkPilih.Checked = True Then
                    Dim lblSupplierATPM As HyperLink = CType(DtgAgree.Items(n).FindControl("lblSupplierATPM"), HyperLink)
                    oEntities.SupplierATPM = lblSupplierATPM.Text.Trim
                    oEntities.LoginId = Me.Loginid
                    oATPMController.SaveATPMOtorisasi(oEntities)
                    ShowMessage(lblMessage, "Data Berhasil di simpan", False)
                End If
            Next
            DoBind(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Pilih Daftar Kontrak yang mau di simpan!!!", True)
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        With oCustomClass
            .strConnection = GetConnectionString()
            .VoucherNO = Me.VoucherNo
        End With
        oController.EditOtorisasi(oCustomClass)
        Response.Redirect("OtorisasiPembayaranATPM.aspx?BranchID=" & Me.BranchID.Trim & " &VoucherNo=" & Me.VoucherNo.Trim)
    End Sub
    Private Sub imbReject_Click(sender As Object, e As System.EventArgs) Handles btnReject.Click
        With oCustomClass
            .strConnection = GetConnectionString()
            .VoucherNO = Me.VoucherNo
        End With
        oController.RejectOtorisasi(oCustomClass)
        Response.Redirect("OtorisasiPembayaranATPM.aspx")
    End Sub
#End Region

End Class