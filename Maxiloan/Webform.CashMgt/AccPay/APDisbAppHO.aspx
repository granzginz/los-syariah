﻿<%@ Page Language="vb" AutoEventWireup="false"  CodeBehind="APDisbAppHO.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.APDisbAppHO" %>
 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="ucAPBeneficiary" Src="../../Webform.UserController/ucAPBeneficiary.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAPDetailItem" Src="../../Webform.UserController/ucAPDetailItem.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAPPVDetail" Src="../../Webform.UserController/ucAPPVDetail.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Invoice Verification</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
</head>
<body>
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VERIFIKASI PEMBAYARAN/PENCAIRAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <asp:Panel ID="pnlSearch" runat="server">

          <div class="form_box">
            <div class="form_single">
                <label> Cabang</label>
                <uc1:ucbranchall id="oBranch" runat="server" />
            </div>
        </div>

            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jenis Pembayaran/Pencairan</label>
                         <asp:DropDownList ID="cmbAPType" runat="server"></asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                        ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType"
                        InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Tanggal Rencana Bayar</label>
                    <uc1:ucDateCE id="txtTglPV" runat="server"></uc1:ucDateCE>                          
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search" />
                <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR PEMBAYARAN/PENCAIRAN</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                            DataKeyField="PaymentVoucherNo" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True"
                            ShowFooter="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkApp" runat="server" CommandName="approve"  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' >APPROVE</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APTo" HeaderText="SUPPLIER">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPTO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BranchFullName" HeaderText="CABANG">
                                    <ItemTemplate>
                                        <asp:label ID="lblBranchFullName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchFullName") %>'></asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn SortExpression="AgreementNoCustomer" HeaderText="NO KONTRAK-CUSTOMER">
                                    <ItemTemplate>
                                      <asp:label ID="lblAgreementNoCustomer" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>'/>
                                        <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server"  Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNoCustomer") %>' />  
                                        <asp:Label ID="lnkAPDeta" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>' /> 
                                    </ItemTemplate> 
                                </asp:TemplateColumn>


                                <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NO PAYMENT-VOUCHER">
                                    <ItemTemplate> 
                                        <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'/> 
                                    </ItemTemplate>

                                    <FooterTemplate>
                                        Grand Total
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="ManufacturingYear" HeaderText="TAHUN">
                                    <ItemTemplate>
                                        <asp:label ID="lnkThnKend" runat="server"  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=0, true ,false) %>' Text='<%# databinder.eval(container,"dataitem.ManufacturingYear") %>'></asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AssetName" HeaderText="NAMA ASSET">
                                    <ItemTemplate>
                                        <asp:label ID="lnkAssetName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetName") %>'>
                                        </asp:label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="POKOK HUTANG" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkPokokHutang" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PokokHutang") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="DownPayment" HeaderText="DP" Visible="false">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkNmAsset" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DownPayment") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVAmount"   HeaderText="JUMLAH">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVAmount" runat="server" visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn SortExpression="DetailAmount" HeaderText="JML DETAIL">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDetailAmount" runat="server" visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=0, true ,false) %>' Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.DetailAmount"),0) %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="TGL JT" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SupplierID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'/> 
                                        <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'/> 
                                        <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'/> 
                                        <asp:Label ID="lblReferenceNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'/> 
                                        <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'/> 
                                        <asp:Label ID="lblAssetRepSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'/> 
                                        <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'/> 
                                        <asp:Label ID="lblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'/> 
                                        <asp:Label ID="lblBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchID") %>'/> 
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                         <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                    </div>
                </div>
            </div>
        </asp:Panel>
       
        <asp:Panel ID="pnlDetail" runat="server" Visible="False">
            <uc1:ucapdetailitem id="oDetailAP" runat="server"></uc1:ucapdetailitem>
            <uc1:ucapbeneficiary id="oBeneficiary" runat="server"></uc1:ucapbeneficiary>
            <uc1:ucappvdetail id="oPVDetail" runat="server"></uc1:ucappvdetail>
            <div class="form_box_header">
                <div class="form_single">
                <label>
                    Akan diSetujui Oleh
                </label>
                <asp:DropDownList ID="cboApprovedBy" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Akan disetujui Oleh"
                    Display="Dynamic" InitialValue="0" ControlToValidate="cboApprovedBy" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonApp" runat="server" CssClass="small button blue" Text="Approve">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonDecline" runat="server" CssClass="small button red" Text="Decline">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonCancel" runat="server" CssClass="small button gray" Text="Cancel"
                    CausesValidation="False"></asp:Button>
            </div>
             
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
