﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Webform.CashMgt
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports Maxiloan.Framework.SQLEngine
#End Region


Public Class BuktiPembayaranSPPLViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oController As New APDisbAppController
    Private oParameter As New Parameter.APDisbApp
    Friend Shared ReportSource As BuktiPembayaranSPPL
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        bindReport()
    End Sub
#End Region

#Region "BindReport"
    Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oDataTable As New DataTable
        Dim oReport As BuktiPembayaranSPPL = New BuktiPembayaranSPPL

        With oParameter
            .strConnection = GetConnectionString()
            .PaymentVoucherNo = Me.MultiApplicationID
            .WhereCond = Me.CmdWhere
            .SortBy = Me.SortBy
        End With

        oParameter = CetakBuktiBayar(oParameter)
        oDataSet = oParameter.ListReport

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = Me.CmdWhere
        oParameter.BranchId = Me.sesBranchId.Replace("'", "")
        oParameter.LoginId = Me.Loginid
        oParameter = CetakBuktiBayar(oParameter)
        oDataSet = oParameter.ListReport

        oReport.SetDataSource(oDataSet.Tables(0))

        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_BuktiPembayaranSPPL.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions =
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("PrintPermintaanPembayaran2.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_BuktiPembayaranSPPL")

    End Sub
    Public Function CetakBuktiBayar(ByVal customclass As Parameter.APDisbApp) As Parameter.APDisbApp
        Dim params() As SqlParameter = New SqlParameter(0) {}
        params(0) = New SqlParameter("@PaymentVoucherNo", SqlDbType.VarChar, 30)
        params(0).Value = customclass.PaymentVoucherNo


        customclass.ListReport = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spBuktiBayar", params)
        Return customclass
    End Function
    'Function GetCompanyName() As String
    '    Dim result As String
    '    Dim oConInst As New InstallRcvController
    '    Dim oInstal As New Parameter.InstallRcv
    '    oInstal.strConnection = GetConnectionString()
    '    oInstal.SPName = "spAPDisbAppMultipleUpdate_22"
    '    oInstal.WhereCond = ""
    '    If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
    '        result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
    '    End If
    '    Return result
    'End Function
#End Region

#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptBuktiPembayaranSPPL")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.MultiApplicationID = cookie.Values("PaymentVoucherNo")
        'Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region

End Class