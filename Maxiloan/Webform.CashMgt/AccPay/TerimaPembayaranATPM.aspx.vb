﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class TerimaPembayaranATPM
        Inherits Maxiloan.Webform.WebBased
        Protected WithEvents oSearchBy As UcSearchBy
        Protected WithEvents oBranch As ucBranchAll
        Protected WithEvents oTobankAccount As ucBankAccountBranch

#Region "Constanta"
        Private currentPage As Int32 = 1
        Private pageSize As Int16 = 6
        Private currentPageNumber As Int16 = 1
        Private totalPages As Double = 1
        Private recordCount As Int64 = 1

        Private oCustomClass As New Parameter.AgreementList
        Private oController As New AgreementListController
        'Private oCustomClass As New Parameter.Invoice
        'Private oController As New InvoiceController
#End Region

#Region "Page Load"
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If SessionInvalid() Then
                Exit Sub
            End If
            If Not IsPostBack Then
                If Request.QueryString("filekwitansi") <> "" Then
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String

                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; " & vbCrLf _
                    & "var y = screen.height; " & vbCrLf _
                    & "window.open('" & strFileLocation & "','Installment', 'left=0, top=0, width=' + x + ', height=' + y + ' , menubar=0, scrollbars=yes') " & vbCrLf _
                    & "</script>")
                End If

                If Request.QueryString("Message") = MessageHelper.MESSAGE_UPDATE_SUCCESS Then _
                    ShowMessage(lblMessage, Request.QueryString("Message"), False)

                Me.FormID = "ATPMRECEIVELIST"
                oSearchBy.ListData = "Name, Nama-Agreementno, No. Kontrak-Address, Alamat-InstallmentAmount, Angsuran"
                oSearchBy.BindData()

                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""
                    oTobankAccount.BindBankAccountBranch("000")
                End If
            End If
        End Sub

        Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
            Dim DtUserList As New DataTable
            Dim DvUserList As New DataView
            Dim intloop As Integer
            Dim hypID As HyperLink

            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = cmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = SortBy
            End With

            oCustomClass = oController.AgreementList(oCustomClass)

            DtUserList = oCustomClass.ListAgreement
            DvUserList = DtUserList.DefaultView
            recordCount = oCustomClass.TotalRecord
            DvUserList.Sort = Me.SortBy
            DtgAgree.DataSource = DvUserList

            Try
                DtgAgree.DataBind()
            Catch
                DtgAgree.CurrentPageIndex = 0
                DtgAgree.DataBind()
            End Try
            PagingFooter()
            pnlList.Visible = True
            pnlDatagrid.Visible = True
            PnlReceive.Visible = False

            lblMessage.Text = ""
        End Sub

#End Region

#Region "Navigation"
        Private Sub PagingFooter()
            lblPage.Text = currentPage.ToString()
            totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
            If totalPages = 0 Then

                'ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
                lblTotPage.Text = "1"
                rgvGo.MaximumValue = "1"
            Else
                lblTotPage.Text = CType(totalPages, String)
                rgvGo.MaximumValue = CType(totalPages, String)
            End If
            lblrecord.Text = CType(recordCount, String)

            If currentPage = 1 Then
                imbPrevPage.Enabled = False
                imbFirstPage.Enabled = False
                If totalPages > 1 Then
                    imbNextPage.Enabled = True
                    imbLastPage.Enabled = True
                Else
                    imbPrevPage.Enabled = False
                    imbNextPage.Enabled = False
                    imbLastPage.Enabled = False
                    imbFirstPage.Enabled = False
                End If
            Else
                imbPrevPage.Enabled = True
                imbFirstPage.Enabled = True
                If currentPage = totalPages Then
                    imbNextPage.Enabled = False
                    imbLastPage.Enabled = False
                Else
                    imbLastPage.Enabled = True
                    imbNextPage.Enabled = True
                End If
            End If
        End Sub

        Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
            Select Case e.CommandName
                Case "First" : currentPage = 1
                Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
                Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
                Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
            End Select
            If Me.SortBy Is Nothing Then
                Me.SortBy = ""
            End If
            DoBind(Me.SearchBy, Me.SortBy)
        End Sub

        Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int16)
                    If Me.SortBy Is Nothing Then
                        Me.SortBy = ""
                    End If
                    DoBind(Me.SearchBy, Me.SortBy)
                End If
            End If
        End Sub

        Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
            Dim lblTemp As Label
            Dim hyTemp As HyperLink

            If SessionInvalid() Then
                Exit Sub
            End If
            Dim m As Int32
            Dim hypReceive As HyperLink
            Dim hyApplicationId As HyperLink
            If e.Item.ItemIndex >= 0 Then

                lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
                hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
                hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
                hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
                '*** Agreement No link
                hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
                hyApplicationId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(hyApplicationId.Text.Trim) & "')"
                '*** ApplicationId link
                hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
                hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

            End If
        End Sub
        Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
            If e.CommandName = "Select" Then
                pnlList.Visible = False
                pnlDatagrid.Visible = False
                PnlReceive.Visible = True
            lblInvoiceDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            Dim lblBranchID As Label
            'lblBranchID.Text = Me.BranchID

                ' Dim dtEntity As DataTable

                'Dim hypSupplierID As New Label
                'hypSupplierID = CType(e.Item.FindControl("hypSupplierID"), Label)

                Dim LblSupplierName As New HyperLink
                LblSupplierName = CType(e.Item.FindControl("hyCustomerName"), HyperLink)

            'Dim lblBranchID As New HyperLink
            'lblBranchID = CType(e.Item.FindControl("hyApplicationId"), HyperLink)

                Dim LblNoFaktur As New HyperLink
                LblNoFaktur = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)


                Dim lblPPn As New Label
                lblPPn = CType(e.Item.FindControl("lblAddress"), Label)

                PnlReceive.Visible = True

                LblSupplierName.Text = LblSupplierName.Text
                lblBranchID.Text = lblBranchID.Text
                LblNoFaktur.Text = LblNoFaktur.Text


            End If
        End Sub
#End Region

#Region "Reset"
        Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
            If SessionInvalid() Then
                Exit Sub
            End If
            Response.Redirect("TerimaPembayaranATPM.aspx")
        End Sub
#End Region

#Region "Search"
        Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
            If SessionInvalid() Then
                Exit Sub
            End If
            Me.SearchBy = " branchid = '" & oBranch.BranchID.Trim & "' "
            Me.SearchBy = Me.SearchBy & " and applicationstep in ('PO','AKR')"
            If oSearchBy.Text.Trim <> "" Then
                If oSearchBy.ValueID = "InstallmentAmount" And Not (IsNumeric(oSearchBy.Text.Trim)) Then

                    ShowMessage(lblMessage, "Angsuran harus diisi dengan Angka", True)
                    Exit Sub
                End If
                'Me.SearchBy = Me.SearchBy & " and " & oSearchBy.ValueID & " = '" & oSearchBy.Text.Trim.Replace("'", "''") & "'" 
                Me.SearchBy = String.Format("{0} and {1} like '%{2}%'", Me.SearchBy, oSearchBy.ValueID, oSearchBy.Text.Trim.Replace("'", "''"))

            End If
            lblMessage.Visible = False
            pnlDatagrid.Visible = True
            DtgAgree.Visible = True
            pnlList.Visible = True
            PnlReceive.Visible = False
            DoBind(Me.SearchBy, Me.SortBy)
        End Sub
#End Region

#Region "Sort"
        Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
            If InStr(Me.SortBy, "DESC") > 0 Then
                Me.SortBy = e.SortExpression
            Else
                Me.SortBy = e.SortExpression + " DESC"
            End If
            DoBind(Me.SearchBy, Me.SortBy)
        End Sub
#End Region
#Region "imbCancel"
        Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            pnlDatagrid.Visible = False
            PnlReceive.Visible = False
            pnlList.Visible = True
        End Sub
        Private Sub imbCancel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel2.Click
            pnlDatagrid.Visible = True
            PnlReceive.Visible = False
            pnlList.Visible = False
        End Sub
#End Region

    End Class