﻿#Region "Imports"
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class EBankPendingReq
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Protected WithEvents txtTglRencanaCairSearch As ucDateCE
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim m_acuan As New AcuanBungaInternController
    Dim oRow As DataRow
#End Region
#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
    
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        'BindGridEntity(Me.CmdWhere)
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        'BindGridEntity(Me.CmdWhere)
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "EBANKPEND"
        If Not Me.IsPostBack Then


            If CheckForm(Me.Loginid, "EBANKPEND", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If


            initAwal()

        End If
    End Sub
    Private Sub InitAwal()
        pnlList.Visible = True
        pnlDetail.Visible = False
        GenerateCombo(cboJenisAP, "spTblAPType", "Description", "ID")
        GenerateCombo(cboBranch, "spCboBranch", "Description", "ID")

        BindGrid(Me.CmdWhere)

    End Sub
    Public Sub BindGrid(ByVal where As String)
        Dim tmp As New DataTable
        Dim oEBankPendingReq As New Parameter.PEBankPendingReq
        Dim m_BankPending As New Controller.CTEBankPendingReq

        With oEBankPendingReq
            .strConnection = GetConnectionString()
            .WhereCond = where
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oEBankPendingReq = m_BankPending.GetListing(oEBankPendingReq)

        If Not tmp Is Nothing Then
            tmp = oEBankPendingReq.ListData
            recordCount = oEBankPendingReq.RecordCount
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = tmp.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()


        PagingFooter()
    End Sub
    Private Sub GenerateCombo(ByVal cbogenerate As System.Web.UI.WebControls.DropDownList, ByVal spName As String, ByVal TextField As String, ByVal ValueField As String)
        Dim PGeneral As New Parameter.GeneralPaging
        Dim CGeneral As New GeneralPagingController
        Dim dtTemp As New DataTable


        With PGeneral
            .SpName = spName
            .strConnection = GetConnectionString()
            .CurrentPage = 1
            .PageSize = 100
            .WhereCond = ""
            .SortBy = ""
        End With

        dtTemp = CGeneral.GetGeneralPaging(PGeneral).ListData

        With cbogenerate
            .DataSource = dtTemp
            .DataTextField = TextField
            .DataValueField = ValueField
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim tmp As New DataTable
        Dim oEBankPendingReq As New Parameter.PEBankPendingReq
        Dim m_BankPending As New Controller.CTEBankPendingReq



        If e.CommandName = "Edit" Then

            With oEBankPendingReq
                .strConnection = GetConnectionString()
                .WhereCond = " and PaymentVoucherNo = '" & CType(e.Item.FindControl("PaymentVoucherNo"), Label).Text & "'"
            End With

            oEBankPendingReq = m_BankPending.GetDetailPay(oEBankPendingReq)
            tmp = oEBankPendingReq.ListData

            lblNamaSupplier.Text = tmp.Rows(0).Item("NamaSupplier").ToString
            lblTglRencanaCair.Text = tmp.Rows(0).Item("TglRencanaCair").ToString
            lblNoKontrakCustomer.Text = tmp.Rows(0).Item("NoKontrakCustomer").ToString
            lblTglInvoice.Text = tmp.Rows(0).Item("TglInvoice").ToString
            lblNoInvoice.Text = tmp.Rows(0).Item("NoInvoice").ToString
            lblNilaiInvoice.Text = tmp.Rows(0).Item("NilaiInvoice").ToString
            lblJumlahBayar.Text = tmp.Rows(0).Item("NilaiInvoice").ToString
            lblNamaRekening.Text = tmp.Rows(0).Item("NamaRekening").ToString
            lblNoRekening.Text = tmp.Rows(0).Item("NoRekening").ToString
            lblNamaBank.Text = tmp.Rows(0).Item("NamaBank").ToString
            lblNamaCabang.Text = tmp.Rows(0).Item("NamaCabang").ToString

            lblCabang.Text = tmp.Rows(0).Item("Cabang").ToString
            lblJenisPembayaran.Text = tmp.Rows(0).Item("JenisPembayaran").ToString
            lblNoPaymentVoucher.Text = tmp.Rows(0).Item("NoPaymentVoucher").ToString
            lblTglPaymentVoucher.Text = tmp.Rows(0).Item("TglPaymentVoucher").ToString
            lblCaraPembayaran.Text = tmp.Rows(0).Item("CaraPembayaran").ToString
            lblJenisTransfer.Text = tmp.Rows(0).Item("JenisTransfer").ToString
            lblKeterangan.Text = tmp.Rows(0).Item("Keterangan").ToString
            lblDiajukanOleh.Text = tmp.Rows(0).Item("DiajukanOleh").ToString
            lblTglPengajuan.Text = tmp.Rows(0).Item("TglPengajuan").ToString

            lblTglPermintaan.Text = Me.BusinessDate.ToString("dd/MM/yyyy")


            GenerateCombo(cboAlasanPenundaan, "spTblAlasanPenundaan", "Description", "ID")

            pnlDetail.Visible = True
            pnlList.Visible = False
        End If
    End Sub
    Private Sub imbCancel_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        InitAwal()
    End Sub

    Protected Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click
        Dim err As String
        Dim oEBankPendingReq As New Parameter.PEBankPendingReq
        Dim m_BankPending As New Controller.CTEBankPendingReq

        With oEBankPendingReq
            .strConnection = Me.GetConnectionString
            .UsrUpd = Me.UserID
            .AlasanPenundaan = cboAlasanPenundaan.SelectedValue.ToString.Trim
            .PaymentVoucherNo = lblNoPaymentVoucher.Text
            .DtmUpd = Me.BusinessDate
        End With

        err = m_BankPending.SaveUpdate(oEBankPendingReq)
        If err = "" Then
            InitAwal()
            ShowMessage(lblMessage, "Update Data berhasil", False)
        End If


    End Sub

    Protected Sub imbButtonSearch_Click(sender As Object, e As EventArgs) Handles imbButtonSearch.Click
        Me.CmdWhere = ""
        If cboJenisAP.SelectedItem.ToString <> "" Then
            Me.CmdWhere = Me.CmdWhere & " and APType = '" & cboJenisAP.SelectedItem.ToString.Trim & "'"
        End If

        If cboBranch.SelectedItem.ToString <> "" Then
            Me.CmdWhere = Me.CmdWhere & " and APBranchID = " & cboBranch.SelectedItem.ToString.Trim
        End If

        If txtTglRencanaCairSearch.Text <> "" Then
            Me.CmdWhere = Me.CmdWhere & " and PVDueDate = '" & txtTglRencanaCairSearch.Text & "'"
        End If

        If txtSearch.Text <> "" And cboSearchBy.SelectedItem.ToString <> "" Then
            Me.CmdWhere = Me.CmdWhere & " and " & cboSearchBy.SelectedItem.ToString.Trim & " like '%" & txtSearch.Text.Trim & "%'"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
End Class