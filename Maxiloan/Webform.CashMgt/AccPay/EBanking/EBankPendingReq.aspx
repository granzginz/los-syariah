﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EBankPendingReq.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.EBankPendingReq" %>
<%@ Register src="../../../Webform.UserController/ucDateCE.ascx" tagname="ucDateCE" tagprefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Acuan Bunga Internal</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
     <script type="text/javascript">
         function hideMessage() {
             document.getElementById('lblMessage').style.display = 'none';
         }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
         <ContentTemplate>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage()"></asp:Label>
                  <asp:Panel ID="pnlList" runat="server" Width="100%">
               <div class="form_title">
                   <div class="title_strip">
                   </div>
                   <div class="form_single">
                       <h3>
                           PERMINTAAN PENDING PEMBAYARAN
                       </h3>
                   </div>
               </div>
             
               <div class="form_box_header">
                   <div class="form_single">
                       <div class="grid_wrapper_ns">
                           <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" OnSortCommand="Sorting"  DataKeyField="PaymentVoucherNo"
                               AutoGenerateColumns="False" AllowSorting="True">
                               <HeaderStyle CssClass="th" HorizontalAlign="Center" VerticalAlign="Middle" />
                               <ItemStyle CssClass="item_grid" HorizontalAlign="Center" />
                               <Columns>
                                   <asp:TemplateColumn HeaderText="SELECT">
                                       <ItemStyle CssClass="command_col"></ItemStyle>
                                       <ItemTemplate>
                                           <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                               CommandName="Edit"></asp:ImageButton>
                                       </ItemTemplate>
                                   </asp:TemplateColumn>
                                   <asp:TemplateColumn Visible="true" SortExpression="SupplierName" HeaderText="SUPPLIER">
                                        <ItemTemplate>
                                            <asp:Label ID="SupplierName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.SupplierName") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:TemplateColumn Visible="true" SortExpression="PaymentVoucherNo" HeaderText="PAYMENT VOUCHER">
                                        <ItemTemplate>
                                            <asp:Label ID="PaymentVoucherNo" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PaymentVoucherNo") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                   <asp:BoundColumn Visible="True" DataField="Jumlah" SortExpression="Jumlah" HeaderText="JUMLAH">
                                   </asp:BoundColumn>
                                    <asp:BoundColumn Visible="True" DataField="NoKontrak" SortExpression="NoKontrak" HeaderText="NO KONTRAK - NAMA CUSTOMER">
                                   </asp:BoundColumn>
                                   <asp:BoundColumn Visible="True" DataField="Bank" SortExpression="Bank" HeaderText="BANK">
                                   </asp:BoundColumn>
                                   <asp:BoundColumn Visible="True" DataField="Cabang" SortExpression="Cabang" HeaderText="CABANG">
                                   </asp:BoundColumn>
                                   <asp:BoundColumn Visible="True" DataField="NoRekening" SortExpression="NoRekening" HeaderText="NO REKENING">
                                   </asp:BoundColumn>
                                   <asp:BoundColumn Visible="True" DataField="NamaRekening" SortExpression="NamaRekening" HeaderText="NAMA REKENING">
                                   </asp:BoundColumn>

                                   
                               </Columns>
                           </asp:DataGrid>
                           <div class="button_gridnavigation">
                               <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                   CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                               </asp:ImageButton>
                               <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                   CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                               </asp:ImageButton>
                               <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                   CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                               </asp:ImageButton>
                               <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                   CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                               </asp:ImageButton>
                               <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                               <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                   EnableViewState="False"></asp:Button>
                               <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                   ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                               <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                   ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                           </div>
                           <div class="label_gridnavigation">
                               <asp:Label ID="lblPage" runat="server"></asp:Label>of
                               <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                               <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                           </div>
                       </div>
                   </div>
               </div>
               <div class="form_button">
                   <asp:Button ID="imbButtonAdd" runat="server" CausesValidation="False" Text="Add"
                       CssClass="small button blue"></asp:Button>
                   <asp:Button ID="imbButtonPrint" runat="server" Text="Print" CssClass="small button blue">
                   </asp:Button>
               </div>
               <div class="form_box_title">
                   <div class="form_single">
                       <h5>
                           CARI PEMBAYARAN
                       </h5>
                   </div>
               </div>
               <div class="form_box">
                    <div class="form_left">
                            <label>Jenis A/P</label>
                            <asp:DropDownList runat="server" ID="cboJenisAP"> 
                           </asp:DropDownList>
                    </div>
                    <div class="form_right">
                            <label>Tanggal Rencana Cair</label>
                            <uc1:ucDateCE ID="txtTglRencanaCair" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>Cabang</label>
                            <asp:DropDownList runat="server" ID="cboBranch"> 
                           </asp:DropDownList>
                    </div>
                    <div class="form_right">
                            <label>
                             <asp:DropDownList runat="server" ID="cboSearchBy"> 
                            <asp:ListItem Value="">Cari Berdasarkan</asp:ListItem>
                            <asp:ListItem Value="APTo">Nama Supplier</asp:ListItem>
                            <asp:ListItem Value="RecipientName">Nama Customer</asp:ListItem>
                            <asp:ListItem Value="ApplicationID">No Kontrak</asp:ListItem>
                           </asp:DropDownList></label>
                           <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                 <div class="form_button">
                   <asp:Button ID="imbButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue"
                       Text="Search"></asp:Button>
                   <asp:Button ID="imbButtonReset" runat="server" CausesValidation="False" Text="Reset"
                       CssClass="small button gray"></asp:Button>
               </div>
            </asp:Panel>
            <asp:Panel ID="pnlDetail" runat="server" Width="100%" Visible="false">
                <div class="form_title">
                   <div class="title_strip">
                   </div>
                   <div class="form_single">
                       <h3>
                           DETAIL PEMBAYARAN
                       </h3>
                   </div>
               </div>
               <div class="form_box">
                    <div class="form_left">
                            <label>Nama Supplier</label>
                            <asp:Label runat="server" ID="lblNamaSupplier" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>Tanggal Rencana Cair</label>
                            <asp:Label runat="server" ID="lblTglRencanaCair" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>No Kontrak - Customer</label>
                            <asp:Label runat="server" ID="lblNoKontrakCustomer" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>Tanggal Invoice</label>
                            <asp:Label runat="server" ID="lblTglInvoice" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>No Invoice</label>
                            <asp:Label runat="server" ID="lblNoInvoice" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>Nilai Invoice</label>
                            <asp:Label runat="server" ID="lblNilaiInvoice" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                    </div>
                    <div class="form_right">
                            <label>Jumlah dibayar</label>
                            <asp:Label runat="server" ID="lblJumlahBayar" ></asp:Label>
                    </div>
                </div>
                <div class="form_box_title">
                   <div class="form_single">
                       <h5>
                           BENEFECIARY
                       </h5>
                   </div>
               </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>Nama Rekening</label>
                            <asp:Label runat="server" ID="lblNamaRekening" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>No Rekening</label>
                            <asp:Label runat="server" ID="lblNoRekening" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>Nama Bank</label>
                            <asp:Label runat="server" ID="lblNamaBank" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>Nama Cabang</label>
                            <asp:Label runat="server" ID="lblNamaCabang" ></asp:Label>
                    </div>
                </div>
                <div class="form_box_title">
                   <div class="form_single">
                       <h5>
                           PAYMENT VOUCHER DETAIL
                       </h5>
                   </div>
               </div>
               <div class="form_box">
                    <div class="form_left">
                            <label>Cabang</label>
                            <asp:Label runat="server" ID="lblCabang" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>Jenis Pembayaran</label>
                            <asp:Label runat="server" ID="lblJenisPembayaran" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>No Payment Voucher</label>
                            <asp:Label runat="server" ID="lblNoPaymentVoucher" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>Tgl Payment Voucher</label>
                            <asp:Label runat="server" ID="lblTglPaymentVoucher" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>Cara Pembayaran</label>
                            <asp:Label runat="server" ID="lblCaraPembayaran" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>Jenis Transfer</label>
                            <asp:Label runat="server" ID="lblJenisTransfer" ></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Keterangan (Max 35)</label>
                         <asp:Label runat="server" ID="lblKeterangan"></asp:Label>
                    </div>
                   
                </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>Diajukan Oleh</label>
                            <asp:Label runat="server" ID="lblDiajukanOleh" ></asp:Label>
                    </div>
                    <div class="form_right">
                            <label>Tanggal Pengajuan</label>
                            <asp:Label runat="server" ID="lblTglPengajuan" ></asp:Label>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label>Catatan Approval</label>
                        <asp:Label runat="server" ID="lblCatatanApproval"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                            <label>Alasan Penundaan</label>
                            <asp:DropDownList runat="server" ID="cboAlasanPenundaan"> 
                           </asp:DropDownList>
                           <asp:RequiredFieldValidator ID="rgvcboAlasanPenundaan" runat="server" ControlToValidate="cboAlasanPenundaan"
                            CssClass="validator_general" ErrorMessage="Harap Pilih Alasan Penundaan"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                            <label>Tanggal Permintaan</label>
                            <asp:Label runat="server" ID="lblTglPermintaan"></asp:Label>
                    </div>
                </div>
                 <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="true"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="false"></asp:Button>&nbsp;
                </div>
            </asp:Panel>
         </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
