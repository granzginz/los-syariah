﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region
Public Class APDisbApp
    Inherits ApDisbAppBase
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "APAPPROVE"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""

                If IIf(IsNothing(Request("aptype")), "", Request("aptype")).ToUpper = "SPPL" Then
                    With cmbAPType
                        .Items.Insert(0, "PO Supplier")
                        .Items(0).Value = "SPPL"
                    End With
                Else

                    bindComboAPType()

                End If
            End If
        End If
        lblMessage.Visible = False
    End Sub
    Overrides Sub CreateoCustomClass()
        With oCustomClass
            .BranchId = Me.BranchID
            .PaymentVoucherNo = oPVDetail.PVNo
            .LoginId = Me.Loginid
            .Notes = oPVDetail.Notes
            .PVStatus = "V"
            .strConnection = GetConnectionString()
            .LoginId = Me.Loginid
            .BusinessDate = Me.BusinessDate
            .UpdateMode = "V"
        End With
    End Sub
#Region "Append"
    Overrides Sub appendSearchPVstatus(strSearch As StringBuilder)
        'strSearch.Append(" pv.PVstatus in ('R') and ")
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusInsurance(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusRefundCustomer(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusFiduciaNotaris(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusRefundSupplier(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusFactoring(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusPO(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusModalUsaha(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusRepoExpenses(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub

    Overrides Sub appendSearchPVstatusBiroJasaSTNK(strSearch As StringBuilder)
        strSearch.Append(" pv.PVstatus in ('R') and pv.apBranchID = '" & Me.sesBranchId.Replace("'", "") & "' and")
    End Sub
#End Region

    Private Sub bindComboAPType()

        Dim customClassx As New GeneralPagingController
        Dim customClass As New Parameter.GeneralPaging

        With customClass
            .strConnection = GetConnectionString()
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spTblAPType"
        End With
        customClass = customClassx.GetGeneralPaging(customClass)
        Dim dt As DataTable
        dt = customClass.ListData
        With cmbAPType
            .DataTextField = "Description"
            .DataValueField = "ID"
            .DataSource = dt
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            .SelectedIndex = 0
        End With
    End Sub
End Class