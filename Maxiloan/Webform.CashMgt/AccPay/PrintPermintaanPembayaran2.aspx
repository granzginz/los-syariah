﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintPermintaanPembayaran2.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.PrintPermintaanPembayaran2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title> 
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script src="../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
   
</head>
<body>
 <script  type="text/javascript">

     $(document).ready(function () {
         $("#dtgEntity_Checkbox2").click(function () {
              $("#<%=dtgEntity.ClientID%> input[id*='ItemCheckBox']:checkbox").attr('checked', $(this).prop('checked'));
         });

     });
     
    </script>
    <form id="form1" runat="server">
        
     <asp:ScriptManager ID="ScriptManager1" runat="server" />
     <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>  CETAK BUKTI PEMBAYARAN/PENCAIRAN</h3>
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label> Cabang</label>
                <uc1:ucbranchall id="oBranch" runat="server" />
            </div>
        </div>
         <div class="form_box">
                <div class="form_single">
                    <label>
                        Jenis Pembayaran/Pencairan</label>
                    <asp:DropDownList ID="cmbAPType" runat="server" /> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                        ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType"
                        InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>

               <div class="form_box">
            <div class="form_single">
            <label> Tanggal Rencana Bayar <= </label>
            <uc1:ucDateCE id="txtTglPV" runat="server"></uc1:ucDateCE>                          
            </div>
        </div>
         <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue" />
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
        </asp:Panel>
       <%-- onclick="CheckAll(this)"--%>
         <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR PEMBAYARAN/PENCAIRAN</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general"   DataKeyField="PaymentVoucherNo" BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" ShowFooter="True">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                             <Columns>
                              <asp:TemplateColumn>
                                <HeaderTemplate>
                                 <input id="Checkbox2" type="checkbox"  runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <asp:Label ID="lblxx" runat="server" Text="-"  visible='<%#IIf(DataBinder.Eval(Container, "DataItem.isPV") = 1, True, False) %>' />
                                 <asp:CheckBox ID="ItemCheckBox" runat="server" visible='<%#IIf(DataBinder.Eval(Container, "DataItem.isPV") = 1, True, False) %>' />
                                </ItemTemplate>
                               </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="JENIS">
                                <ItemTemplate> <asp:Label ID="lbljns" runat="server" Text= <%#Eval("APType")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn  HeaderText="CAB">
                                <ItemTemplate> <asp:Label ID="lblcab" runat="server" Text= <%#Eval("CAB")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>
                                 
                                 
                                <asp:TemplateColumn  HeaderText="BENEFICIARY">
                                 <ItemTemplate> <asp:Label ID="lblBenef" runat="server" Text= <%#Eval("ApTo")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                  <FooterTemplate>
                                        <strong>Grand Total</strong>
                                    </FooterTemplate>
                                </asp:TemplateColumn> 

                                 <asp:TemplateColumn  HeaderText="JUMLAH">
                                    <ItemTemplate>   <div  style="TEXT-ALIGN:RIGHT;">
                                    <asp:Label ID="lblPVAmount" runat="server" Text=<%# formatnumber(Eval("PVAmount"),0) %> visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /> </div></ItemTemplate>  
                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server" />
                                    </FooterTemplate> 
                                </asp:TemplateColumn>

                                 <asp:TemplateColumn  HeaderText="SKN/RTGS">
                                <ItemTemplate> <asp:Label ID="lblskn" runat="server" Text= <%#Eval("JenisTransfer")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>

                                 <asp:TemplateColumn  HeaderText="BANK">
                                <ItemTemplate> <asp:Label ID="lblBANK" runat="server" Text= <%#Eval("BankNameTo")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>
                              
                                  <asp:TemplateColumn  HeaderText="NO REKENING">
                                <ItemTemplate> <asp:Label ID="lblBankAccountTo" runat="server" Text= <%#Eval("BankAccountTo")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>
                                 
                                    <asp:TemplateColumn  HeaderText="NAMA REKENING">
                                <ItemTemplate> <asp:Label ID="lblAccountNameTo" runat="server" Text= <%#Eval("AccountNameTo")  %>  visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=1, true ,false) %>' /></ItemTemplate>
                                </asp:TemplateColumn>
                                  
                                   
                                <asp:TemplateColumn SortExpression="AgreementNoCustomer" HeaderText="NO KONTRAK-CUSTOMER">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNoCustomer" runat="server" Text='<%# Eval("AgreementNoCustomer") %>' visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=0, true ,false) %>' />  
                                    <asp:Label ID="lnkAPDeta" runat="server" Visible="False" Text='<%# Eval("AccountPayableNo") %>' /> 
                                    <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Visible="False" Text='<%# Eval( "PaymentVoucherBranchID") %>'/> 
                                    <asp:Label ID="lblAssetName" runat="server" Visible="False" Text='<%# Eval( "AssetName") %>'/> 
                                    <asp:Label ID="lblCustName" runat="server" Visible="False" Text='<%# Eval( "CustomerName") %>'/> 
                                </ItemTemplate>
                                </asp:TemplateColumn> 

                                <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NO VOUCHER">
                                <ItemTemplate> 
                                    <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# Eval("PaymentVoucherNo") %>' visible='<%#IIF(DataBinder.Eval(Container, "DataItem.isPV")=0, true ,false) %>' /> 
                                </ItemTemplate>
                                </asp:TemplateColumn> 

                                <%-- <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="RENCANA BAYAR">
                                <ItemTemplate> <asp:Label ID="lblPVDueDate" runat="server" Text= <%#Eval("PVDueDate")  %>  visible='<%#IIf(DataBinder.Eval(Container, "DataItem.isPV") = 1, True, False) %>' /> </ItemTemplate>
                                </asp:TemplateColumn> --%>

                             </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>

             <div class="form_button">
                <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="small button green" /> 
             </div>
        </asp:Panel>
         
    </form>
</body>
</html>
