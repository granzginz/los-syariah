﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APChangeLocation.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.APChangeLocation" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>APChangeLocation</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />    
</head>
<body>
<script language="javascript" type="text/javascript">    
    var x = screen.width;
    var y = screen.height - 100;

    var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
    var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
    function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
        window.open(ServerName + App + '/Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&InsCoBranchID=' + pInsCoBranchID + '&Style=' + pStyle, 'InsuranceCo', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1')
    }		
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>   
        <asp:Label ID="lblMessage" runat="server"  ></asp:Label>
        <asp:Panel ID="pnlMain" runat="server">
        <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">                    
                <h3>
                        GANTI LOKASI PEMBAYARAN 
                </h3>
                </div>
            </div>
        <asp:Panel ID="pnlSearch" runat="server">                       
           <div class="form_box">
				<div class="form_single">
				<label class ="label_req">  Jenis Pembayaran/Pencairan</label>
				  <asp:DropDownList ID="cmbAPType" runat="server">
                            <asp:ListItem Value="0">Select One</asp:ListItem>
                            <asp:ListItem Value="SPPL">PO Supplier</asp:ListItem>
                            <asp:ListItem Value="INSR">Asuransi</asp:ListItem>
                            <asp:ListItem Value="INCS">Incentive Supplier</asp:ListItem>
                            <asp:ListItem Value="INCE">Incentive Karyawan</asp:ListItem>
                            <asp:ListItem Value="STNK">Biro Jasa STNK</asp:ListItem>
                            <asp:ListItem Value="RSPL">Refund Supplier</asp:ListItem>
                            <asp:ListItem Value="RCST">Refund Customer</asp:ListItem>
                            <asp:ListItem Value="RADV">Refund Uang Muka</asp:ListItem>
                            <asp:ListItem Value="ASRP">PO Untuk Ganti Asset</asp:ListItem>
                            <asp:ListItem Value="RCAN">Refund Kontrak Batal</asp:ListItem>
                            <asp:ListItem Value="AFTN">Fiducia Notaris</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtAPType" runat="server" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                            Display="Dynamic" ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType" CssClass="validator_general"></asp:RequiredFieldValidator>
				</div>				
			</div>

            <div class="form_box">
				<div class="form_left">
				<label  class ="label_req">  Cabang Pembayaran/Pencairan</label>
				<asp:DropDownList ID="oBranch" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
                            Display="Dynamic" ErrorMessage="Harap pilih Cabang" ControlToValidate="oBranch" CssClass="validator_general"></asp:RequiredFieldValidator>
                    
				</div>
				<div class="form_right">
				<label>  Tanggal Rencana Bayar &lt;= </label>
				 <asp:TextBox ID="txtTglJatuhTempo" runat="server" />
                    <aspajax:CalendarExtender id="calExTglJatuhTempo" runat="server" TargetControlID="txtTglJatuhTempo" Format="dd/MM/yyyy" />
                    <asp:RequiredFieldValidator ID="rfvTglJatuhTempo" runat="server" ControlToValidate="txtTglJatuhTempo" 
                    ErrorMessage="Harap isi dengan tanggal rencana bayar" 
                     Display="Dynamic" CssClass="validator_general" />
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label class ="label_req">  Lokasi bayar</label>
				 <asp:DropDownList ID="cmbPaidLocation" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" InitialValue="0"
                            Display="Dynamic" ErrorMessage="Harap pilih Cabang" ControlToValidate="cmbPaidLocation" CssClass="validator_general"></asp:RequiredFieldValidator>
                    
				</div>				
			</div>
          <div class="form_box_uc">
				 <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
		  </div>			           
          <div class="form_button">
			 <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search"/>                        
             <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset" CausesValidation="False"/>
		  </div>           
        </asp:Panel>
        <asp:Panel ID="pnlDataGrid" runat="server">
           <div class="form_box_title">
                <div class="form_single">                    
                    <h4>
                         DAFTAR PEMBAYARAN/PENCAIRAN </h4>
                </div>
            </div>
            <div class="form_box_header">
            <div class="form_single">
            <div class="grid_wrapper_ns"> 				
				<asp:DataGrid ID="dtgAP" runat="server" CssClass="grid_general"
                            BorderWidth="0" BorderStyle="None" BorderColor="#CCCCCC" AutoGenerateColumns="False"
                            AllowSorting="True" OnSortCommand="SortGrid" ShowFooter="true" >
                           <HeaderStyle CssClass="th" />
		                    <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid"/>
                            <Columns>
                                <asp:TemplateColumn>
                                   
                                    <HeaderTemplate>
                                        <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                        </asp:CheckBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APTo" HeaderText="BAYAR KEPADA">
                                   
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        Total
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APID" HeaderText="AP ID" Visible="false">
                                    
                                    <ItemTemplate>
                                        <asp:Label ID="lnkAPID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AccountPayableNo" HeaderText="DETAIL PEMBAYARAN/PENCAIRAN">
                                  
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPDetailDescription" Visible="True" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:HyperLink>
                                        <asp:HyperLink ID="lnkAPDetail" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APAmount" HeaderText="JUMLAH">
                                 
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmount" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.APAmount"),2) %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="DueDate" HeaderText="TGL JT">
                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DueDate") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO INVOICE">
                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoice" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNo") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InvoiceDate" HeaderText="TGL INV">
                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblInvoDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceDate") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APStatusDate" HeaderText="STATUS JT">
                                    
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Visible="True"></asp:Label>
                                        <asp:Label ID="lblStatusDue" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.APStatusDate") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False">
                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblSupplierID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False">
                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False">
                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False">
                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                        <asp:Label ID="lblAssetRepSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                        <asp:Label ID="lblReffNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                        <asp:Label ID="lblApplicationID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                </asp:DataGrid>				
		    </div>
            </div>
            </div>
             <div class="form_button">
			  <asp:Button ID="ButtonNext" runat="server" CssClass="small button green" Text="Next"/>
			 </div>
          
        </asp:Panel>
    </asp:Panel>
        <asp:Panel ID="pnlListInfo" runat="server">
        <div class="form_box_title">
                <div class="form_single">
                    
                    <h4>
                        GANTI LOKASI PEMBAYARAN </h4>
                </div>
            </div>
        
        <div class="form_box">
				<div class="form_left">
				<label>   Jenis Pembayaran/Pencairan </label>
				 <asp:Label ID="lblAPType" runat="server"></asp:Label>
				</div>
				<div class="form_right">
				<label>  Tanggal Rencan Bayar &lt;= </label>
				 <asp:Label ID="lblDueDateInfo" runat="server"></asp:Label>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label> Cabang Pembayaran/Pencairan </label>
				<asp:Label ID="lblAPBranchInfo" runat="server"></asp:Label>
				</div>
				<div class="form_right">
				<label>  Lokasi Pembayaran </label>
				<asp:Label ID="lblPaidLocInfo" runat="server"></asp:Label>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label class ="label_req">  Lokasi Pembayaran Baru</label>
				 <asp:DropDownList ID="cboNewPaidLoc" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" InitialValue="0"
                        Display="Dynamic" ErrorMessage="Harap pilih Lokasi Pembayaran Baru" ControlToValidate="cboNewPaidLoc" CssClass="validator_general"></asp:RequiredFieldValidator>
                
				</div>
				<div class="form_right">
				
				</div>
			</div>       
        <div class="form_button">
			    <asp:Button ID="ButtonSave" runat="server" CssClass="small button blue" Text="Save" />
                <asp:Button ID="ButtonCancel" runat="server" CssClass="small button gray" Text="Cancel" CausesValidation="False"></asp:Button>
        </div>            
        <div class="form_box_title">
            <div class="form_single">                    
                    <h4>
                          DAFTAR PENCAIRAN/PEMBAYARAN
                    </h4>
                </div>
            </div>
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns"> 	
                <asp:DataGrid ID="DtgInfo" runat="server" CssClass="grid_general"
                        BorderWidth="0" BorderStyle="None" AutoGenerateColumns="False"
                        AllowSorting="True" OnSortCommand="SortGrid2" ShowFooter="True"
                        Visible="true">
                        <HeaderStyle CssClass="th" />
		                <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="APTo" HeaderText="BAYAR KEPADA">                             
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAPTo2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                                <FooterTemplate>
                                    Total
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="APID" HeaderText="AP ID" Visible="false">
                             
                                <ItemTemplate>
                                    <asp:Label ID="lnkAPID2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AccountPayableNo" HeaderText="DETAIL PEMBAYARAN/PENCAIRAN">
                            
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAPDetailDescription2" Visible="True" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                    </asp:HyperLink>
                                    <asp:HyperLink ID="lnkAPDetail2" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AccountPayableNo") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="APAmount" HeaderText="JUMLAH">
                              
                                <ItemTemplate>
                                    <asp:Label ID="lblAmount2" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.APAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="Right"></FooterStyle>
                                <FooterTemplate>
                                    <asp:Label ID="lblSum2" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DueDate" HeaderText="TGL JT">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblDueDate2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DueDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InvoiceNo" HeaderText="NO INVOICE">
                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInvoice2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceNo") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InvoiceDate" HeaderText="TGL INV">
                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInvoDate2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InvoiceDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="APStatusDate" HeaderText="STATUS JT">
                              
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus2" runat="server" Visible="True"></asp:Label>
                                    <asp:Label ID="lblStatusDue2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.APStatusDate") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                               
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierID2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInsBranchID2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                              
                                <ItemTemplate>
                                    <asp:Label ID="lblMaskAssID2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False">
                                
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetSeqNo2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblAssetRepSeqNo2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblCustID2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblReffNo2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblApplicationID2" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>               
		</div>
        </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
