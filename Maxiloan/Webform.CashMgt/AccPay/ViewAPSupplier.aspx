﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAPSupplier.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.ViewAPSupplier" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAPSupplier</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinMain(BranchID, Voucherno) {

            window.open(ServerName + App + '/Webform.LoanMnt/View/CashBankVoucher.aspx?BranchID=' + BranchID + '&Voucherno=' + Voucherno, null, 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1')
        }
        function click() {
            if (event.button == 2) {
                alert('You Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VIEW - INVOICE
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
             <label>
                Nama Customer
            </label>
            <asp:Label ID="lblCustname" runat="server"></asp:Label>
        </div>
        <div class="form_right">
             <label>
                No Kontrak
            </label>
            <asp:Label ID="lblAgreementNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Invoice
            </label>
            <asp:Label ID="lblInvoiceDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
             <label>
                Nilai Invoice
            </label>
            <asp:Label ID="lblNilaiInvoice" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
           <label>
                No. Invoice
            </label>
            <asp:Label ID="lblInvoiceNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
           <label>
                Jumlah Potongan
            </label>
            <asp:Label ID="lblJmlPotongan" runat="server"></asp:Label>
        </div>
    </div>
     <div class="form_box">
        <div class="form_left">
           <label>
               Tanggal Rencana Bayar
            </label>
            <asp:Label ID="lblTglRencanaBayar" runat="server"></asp:Label>
        </div>
        <div class="form_right">
           <label>
                Jumlah Pembayaran
            </label>
            <asp:Label ID="lblJmlPembayaran" runat="server"></asp:Label>
        </div>
    </div>
 
    <asp:DataGrid ID="DtgList" runat="server" Width="100%" BorderStyle="None" BorderWidth="0"
        AutoGenerateColumns="False" GridLines="None" FooterStyle-Wrap="False">
        <Columns>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    <div class="form_title">
                        <div class="form_single">
                            <h3>
                                DAFTAR POTONGAN
                            </h3>
                        </div>
                    </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="form_box">
                        <div class="form_left">
                            <div style="float: left; width: 230px;">
                                <asp:Label ID="lblPODesc" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                </asp:Label>
                            </div>
                            <div>
                                <asp:Label ID="lblPOAmount" runat="server" Text='<%#formatnumber(Container.DataItem("DeductionAmount"),0)%>'>   </asp:Label>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
        <FooterStyle Wrap="False"></FooterStyle>
    </asp:DataGrid>
       <div class="form_box_title">
        <div class="form_single">
            <h4>
                REKENING BANK PENERIMA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
               Dibayar Kepada
            </label>
            <asp:Label ID="lblSupplierName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Bank
            </label>
            <asp:Label ID="lblBankName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nama Cabang
            </label>
            <asp:Label ID="lblBankBranch" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Rekening
            </label>
            <asp:Label ID="lblAccountName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Rekening
            </label>
            <asp:Label ID="lblAccountNo" runat="server"></asp:Label>
        </div>
    </div>
  
  <div class="form_box">
        <div class="form_left">
            <label> Attached Invoice </label>
           <asp:LinkButton ID="btnDownload" runat="server" Text="Download Attached Invoice" OnClick="btnDownload_OnClick" />
        </div>
        <div class="form_right"> 
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
