﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class APDisbInquiryInProcess
    Inherits Maxiloan.Webform.WebBased
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 15
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.APDisbInq
    Private oController As New APDisbInqController
    Private m_controller As New DataUserControlController
    Dim Total As Double

    Private Property PageState() As Int32
        Get
            Return (CType(Viewstate("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            Viewstate("pageState") = Value
        End Set
    End Property

    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property

    Private Property Printed() As String
        Get
            Return (CType(viewstate("Printed"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Printed") = Value
        End Set
    End Property

    Private Property SelectMode() As Boolean
        Get
            Return (CType(viewstate("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("SelectMode") = Value
        End Set
    End Property

    Private Property FilterBy() As String
        Get
            Return (CType(viewstate("FilterBy"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False        
        Me.SelectMode = True
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        'Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()

            Me.FormID = "APINPROC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
        lblMessage.Visible = False
    End Sub

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Me.SearchBy = ""
        Me.APType = cmbAPType.SelectedItem.Value
        Me.SearchBy += " AP.APtype = '" & Me.APType & "'"
        Me.FilterBy &= "AP Type=" & cmbAPType.SelectedItem.Text

        Me.Printed = cboPrinted.SelectedValue
        Me.SearchBy += " and AP.Printed = '" & Me.Printed & "'"

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.APDisbInqListPrintSelection(oCustomClass)

        DtUserList = oCustomClass.ListAPInq
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try

        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
    End Sub

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If        
        Me.PageState = currentPage
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum, lblAPDate, lblInvoiceDate As Label

        Dim lnkAPTo As New HyperLink
        Dim lnkDesc As New HyperLink

        Dim lblAccountPayableNo As New Label
        Dim lblSupplierID As New Label
        Dim lblApplicationID As New Label
        Dim lblInsuranceBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim nReffNo As New Label
        Dim nCustID As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label

        If e.Item.ItemIndex >= 0 Then
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            lblAPDate = CType(e.Item.FindControl("lblAPDate"), Label)
            lblInvoiceDate = CType(e.Item.FindControl("lblInvoiceDate"), Label)
            lblAPDate.Text = CDate(lblAPDate.Text).ToString("dd/MM/yyyy")
            If lblInvoiceDate.Text.Trim <> "-" Then
                lblInvoiceDate.Text = CDate(lblInvoiceDate.Text).ToString("dd/MM/yyyy")
            End If


            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)

            lnkAPTo = CType(e.Item.FindControl("lnkAPTo"), HyperLink)
            lnkDesc = CType(e.Item.FindControl("lnkDesc"), HyperLink)

            lblAccountPayableNo = CType(e.Item.FindControl("lblAccountPayableNo"), Label)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblInsuranceBranchID = CType(e.Item.FindControl("lblInsuranceBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            nReffNo = CType(e.Item.FindControl("lblReffNo"), Label)
            nCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)

            Select Case cmbAPType.Text.ToUpper.Trim
                Case "APDST"
                    lnkAPTo.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
                Case "SPPL"
                    lnkDesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & Server.UrlEncode(lblAccountPayableNo.Text.Trim) & "','" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                Case "INSR"
                    lnkDesc.NavigateUrl = "javascript:OpenViewApInsurance('" & "Finance" & "', '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "','" & Server.UrlEncode(sesBranchId.Replace("'", "").Trim) & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsuranceBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RCST"
                    lnkDesc.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(nCustID.Text.Trim) & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(nCustID.Text.Trim) & "')"
                Case "RADV"
                    lnkDesc.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(sesBranchId.Replace("'", "").Trim) & "', '" & Server.UrlEncode(nReffNo.Text.Trim) & "','" & "Finance" & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(sesBranchId.Replace("'", "").Trim) & "', '" & Server.UrlEncode(nReffNo.Text.Trim) & "','" & "Finance" & "')"
                Case "ASRP"
                    lnkDesc.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(sesBranchId.Replace("'", "").Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(sesBranchId.Replace("'", "").Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            End Select
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If

    End Sub

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkItem As CheckBox
        Dim x As Integer
        If Me.SelectMode = True Then
            For x = 0 To dtgEntity.Items.Count - 1
                chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
                chkItem.Checked = True
            Next
            Me.SelectMode = False
        Else
            For x = 0 To dtgEntity.Items.Count - 1
                chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
                chkItem.Checked = False
            Next
            Me.SelectMode = True
        End If
    End Sub

    Private Sub imgPrint_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            Dim chkDtList As CheckBox
            Dim lBlnValid As Boolean
            Dim i As Integer
            Dim strAPNo As String
            Dim lblAccountPayableNo As Label

            For i = 0 To dtgEntity.Items.Count - 1
                chkDtList = CType(dtgEntity.Items(i).FindControl("chkItem"), CheckBox)
                lblAccountPayableNo = CType(dtgEntity.Items(i).FindControl("lblAccountPayableNo"), Label)
                If Not IsNothing(chkDtList) Then
                    If chkDtList.Checked Then
                        strAPNo &= IIf(strAPNo = "", "'" & lblAccountPayableNo.Text.Trim & "'", ",'" & lblAccountPayableNo.Text.Trim & "'").ToString
                    End If
                End If
            Next

            If strAPNo <> "" Then lBlnValid = True

            If Not lBlnValid Then
                ShowMessage(lblMessage, "Harap pilih Jenis A/P", True)
                Exit Sub
            Else
                Try
                    With oCustomClass
                        .strConnection = GetConnectionString()
                        .SelectedAPNo = strAPNo
                    End With

                    oController.savePrintSelection(oCustomClass)

                    Dim cookie As HttpCookie = Request.Cookies(COOKIES_APINQ_PRINTSELECTION)
                    If Not cookie Is Nothing Then
                        cookie.Values("where") = " a.AccountPayableNo in (" & strAPNo & ")"
                        cookie.Values("sortby") = Me.SortBy
                        cookie.Values("FilterBy") = Me.FilterBy

                        Response.AppendCookie(cookie)
                    Else
                        Dim cookieNew As New HttpCookie(COOKIES_APINQ_PRINTSELECTION)
                        cookieNew.Values.Add("where", "a.AccountPayableNo in (" & strAPNo & ")")
                        cookieNew.Values.Add("SortBy", Me.SortBy)
                        cookieNew.Values.Add("FilterBy", Me.FilterBy)
                        Response.AppendCookie(cookieNew)
                    End If

                    Response.Redirect("../Report/APDisbInqReport.aspx?CallingFrom=PrintInProcess")

                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try
            End If
        End If
    End Sub

End Class