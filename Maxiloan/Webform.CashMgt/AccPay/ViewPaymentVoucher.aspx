﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPaymentVoucher.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.ViewPaymentVoucher" %>

<%@ Register TagPrefix="uc1" TagName="ucAPBeneficiary" Src="../../Webform.UserController/ucAPBeneficiary.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAPDetailItem" Src="../../Webform.UserController/ucAPDetailItem.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewPaymentVoucher</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PAYMENT VOUCHER
            </h3>
        </div>
    </div>
    <uc1:ucapdetailitem id="oViewDetailItem" runat="server"></uc1:ucapdetailitem>
    <uc1:ucapbeneficiary id="oViewAPBeneficiary" runat="server"></uc1:ucapbeneficiary>
    <asp:Panel ID="pnlView3" runat="server">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    DETAIL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang
                </label>
                <asp:Label ID="lblBranch" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Pembayaran/Pencairan
                </label>
                <asp:Label ID="lblAPType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No. PV
                </label>
                <asp:Label ID="lblPVNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal PV
                </label>
                <asp:Label ID="lblPVDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cara Pembayaran
                </label>
                <asp:Label ID="lblWOP" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="Label1" runat="server">Bank Account</asp:Label>
                </label>
                <asp:Label ID="lblBankAccount" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No. BG
                </label>
                <asp:Label ID="lblBGNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Rencana Bayar BG
                </label>
                <asp:Label ID="lblBGDueDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Penerima
                </label>
                <asp:Label ID="lblRecipientName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan
                </label>
                <asp:Label ID="lblPaymentNotes" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlStatus" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    STATUS
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Status
                </label>
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Status
                </label>
                <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Pengajuan Oleh
                </label>
                <asp:Label ID="lblRequestBy" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   Tanggal Pengajuan
                </label>
                <asp:Label ID="lblRequestDate" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Di Approve oleh
                </label>
                <asp:Label ID="lblApprovalBy" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblJdlAccount" runat="server">Tanggal Approve</asp:Label>
                </label>
                <asp:Label ID="lblApprovalDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Catatan
                </label>
                <asp:Label ID="lblNotes" runat="server" Width="70%"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlButton" runat="server">
        <div class="form_button">
            <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
