﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Controller


Public Class APGantiCaraBayar
    Inherits Maxiloan.Webform.WebBased

    'Protected WithEvents oPVEDueDate As ValidDate
    Protected WithEvents TglJatuhTempo As ucDateCE
    Protected WithEvents ucAPType1 As ucAPType

    Private oCustomClass As New Parameter.APDisbApp
    Private oController As New APDisbAppController
    Private m_controller As New DataUserControlController
    Dim Total As Double


    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Const VAR_ALLBRANCH As String = "*"
#Region "properties"
    Private Property PaymentVoucherBranchID() As String
        Get
            Return (CType(ViewState("PaymentVoucherBranchID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherBranchID") = Value
        End Set
    End Property
    Private Property PVStatus() As String
        Get
            Return (CType(ViewState("PVStatus"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVStatus") = Value
        End Set
    End Property
    Private Property PaymentVoucherNo() As String
        Get
            Return (CType(ViewState("PaymentVoucherNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PaymentVoucherNo") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return (CType(ViewState("Branch_ID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
    Private Property PageState() As Int32
        Get
            Return (CType(ViewState("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            ViewState("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(ViewState("APType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("APType") = Value
        End Set
    End Property
    Private Property PVDate() As String
        Get
            Return (CType(ViewState("PVDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PVDate") = Value
        End Set
    End Property
    Private Property Branch() As String
        Get
            Return (CType(ViewState("Branch"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Branch") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property TempDataTableLoad() As DataTable
        Get
            Return CType(ViewState("TempDataTableLoad"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTableLoad") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "APGANTIBAYAR"
            ucAPType1.Filter = "0"
            ucAPType1.BindAPType()
            With oBranch
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                End If
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "All Branch")
                .Items(1).Value = VAR_ALLBRANCH
            End With

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If

            If Me.IsHoBranch Then                
            Else
                pnlDatagrid.Visible = False
                pnlSearch.Visible = False
                ShowMessage(lblMessage, "Harap login di Kantor Pusat!", True)
            End If
            div_1.Visible = True
            div_2.Visible = False
            div_btn1.Visible = True
            div_btn2.Visible = False
        End If
        lblMessage.Visible = False
    End Sub

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False
        'With oPVEDueDate
        '    .FillRequired = True
        '    .FieldRequiredMessage = "Harap isi Tanggal Payment Voucher"
        '    .Display = "Dynamic"
        '    .ValidationErrMessage = "Harap isi dengan format dd/MM/yyyy"
        '    .isCalendarPostBack = False
        '    .dateValue = Me.BusinessDate.ToString("dd/MM/yyyy")
        'End With
        TglJatuhTempo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim cContract As New InstallRcvController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spAPGantiCaraBayarPaging"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        TempDataTableLoad = oContract.ListData
        TempDataTable = oContract.ListData        
        recordCount = oContract.TotalRecords
        dtgEntity.DataSource = TempDataTable

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
    End Sub

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        Me.PageState = currentPage
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal PaymentVoucherNo As String, ByVal Branch_ID As String, ByVal style As String) As String
        Return "javascript:OpenWinPaymentVoucher('" & PaymentVoucherNo & "','" & Me.PaymentVoucherBranchID & "','" & style & "')"
    End Function
#End Region

#Region "Datagrid Command"

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = sender
        Dim chkItem As CheckBox
        Dim x As Integer
        For x = 0 To dtgEntity.Items.Count - 1
            chkItem = CType(dtgEntity.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub

    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblInsuranceCoyID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        Dim lnkAgreementNoCustomer As New HyperLink

        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lnkAgreementNoCustomer = CType(e.Item.FindControl("lnkAgreementNoCustomer"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblInsuranceCoyID = CType(e.Item.FindControl("lblInsuranceCoyID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            'If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
            '    lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            'End If

            'If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
            'lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
            Select Case ucAPType1.selectedAPTypeID
                Case "SPPL"
                    lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                Case "INSR"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblInsuranceCoyID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RCST"
                    lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

                Case "RADV"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
                Case "ASRP"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            End Select

            'End If
            lnkAgreementNoCustomer.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 0)
        End If

        'If e.Item.ItemType = ListItemType.Footer Then
        '    lblSum = CType(e.Item.FindControl("lblSum"), Label)
        '    lblSum.Text = FormatNumber(Total.ToString, 2)
        'End If
        ScriptManager.RegisterStartupScript(dtgEntity, GetType(DataGrid), dtgEntity.ClientID, String.Format("total('{0}'); ", dtgEntity.ClientID), True)
    End Sub

    Private Sub dtgEntity1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity1.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum As New Label
        Dim lblInsBranchID As New Label
        Dim lblInsuranceCoyID As New Label
        Dim lblPaymentVoucherBranchID As New Label
        Dim lblReferenceNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label
        Dim lblApplicationID As New Label
        Dim lblCustomerID As New Label
        Dim lnkAgreementNoCustomer As New HyperLink

        If e.Item.ItemIndex >= 0 Then
            Dim lnkPVNo As HyperLink
            Dim lnkAPTO As HyperLink
            Dim lblSupplierID As Label
            lnkPVNo = CType(e.Item.FindControl("lnkPVNo"), HyperLink)
            lnkAPTO = CType(e.Item.FindControl("lnkAPTO"), HyperLink)
            lnkAgreementNoCustomer = CType(e.Item.FindControl("lnkAgreementNoCustomer"), HyperLink)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblInsBranchID = CType(e.Item.FindControl("lblInsBranchID"), Label)
            lblInsuranceCoyID = CType(e.Item.FindControl("lblInsuranceCoyID"), Label)
            lblPaymentVoucherBranchID = CType(e.Item.FindControl("lblPaymentVoucherBranchID"), Label)
            lblReferenceNo = CType(e.Item.FindControl("lblReferenceNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            Me.PaymentVoucherNo = lnkPVNo.Text.Trim
            Me.Branch_ID = Me.sesBranchId.Replace("'", "")
            Me.PaymentVoucherBranchID = lblPaymentVoucherBranchID.Text.Trim

            'If CheckFeature(Me.Loginid, Me.FormID, "PV", Me.AppId) Then
            '    lnkPVNo.NavigateUrl = LinkTo(Me.PaymentVoucherNo, Me.PaymentVoucherBranchID, "Finance")
            'End If

            'If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then
            'lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
            Select Case ucAPType1.selectedAPTypeID
                Case "SPPL"
                    lnkAPTO.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                Case "INSR"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblInsuranceCoyID.Text.Trim) & "','" & Server.UrlEncode(lblInsBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RCST"
                    lnkAPTO.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

                Case "RADV"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(Me.Branch_ID.Trim) & "', '" & Server.UrlEncode(lblReferenceNo.Text.Trim) & "','" & "Finance" & "')"
                Case "ASRP"
                    lnkAPTO.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(Me.Branch_ID.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            End Select

            'End If
            lnkAgreementNoCustomer.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"

            lblAmount = CType(e.Item.FindControl("lblPVAmount"), Label)
            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 0)
        End If

        'If e.Item.ItemType = ListItemType.Footer Then
        '    lblSum = CType(e.Item.FindControl("lblSum"), Label)
        '    lblSum.Text = FormatNumber(Total.ToString, 2)
        'End If
        ScriptManager.RegisterStartupScript(dtgEntity1, GetType(DataGrid), dtgEntity1.ClientID, String.Format("total('{0}'); ", dtgEntity1.ClientID), True)
    End Sub
#End Region

    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim strSearch As New StringBuilder
        If TglJatuhTempo.Text <> "" Then
            If IsDate(ConvertDate2(TglJatuhTempo.Text)) Then
                'Me.APType = cmbAPType.SelectedItem.Value
                Me.APType = ucAPType1.selectedAPTypeID
                Me.PVDate = TglJatuhTempo.Text
                Me.Branch = oBranch.SelectedValue

                strSearch.Append(" pv.APType='" & Me.APType & "'")
                strSearch.Append(" and pv.PVDueDate<='" & ConvertDate2(Me.PVDate) & "'")

                If txtSearch.Text.Trim <> "" Then
                    strSearch.Append(" and pv.APTo Like '%" & txtSearch.Text & "%'")
                End If

                If Me.Branch <> VAR_ALLBRANCH Then
                    strSearch.Append(" and pv.APBranchID = '" & Me.Branch & "'")
                End If

                Me.SearchBy = strSearch.ToString
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
            Else
                ShowMessage(lblMessage, "Format Tanggal Payment Voucher salah", True)
            End If
        Else
            ShowMessage(lblMessage, "Harap isi Tanggal Payment Voucher", True)
        End If
    End Sub

    Private Function createNewPVnoDT() As DataTable
        Dim dt As New DataTable
        dt.Columns.Add("Id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")
        Return dt
    End Function

    Private Sub ButtonNext_Click(sender As Object, e As System.EventArgs) Handles ButtonNext.Click
        Try            
            TempDataTableLoad.Clear()
            Dim Status As New List(Of Integer)
            For index = 0 To dtgEntity.Items.Count - 1
                Dim chek As New CheckBox
                chek = CType(dtgEntity.Items(index).FindControl("chkItem"), CheckBox)

                If chek.Checked Then
                    Dim tempId As String = CType(dtgEntity.Items(index).FindControl("lblID"), Label).Text.Trim()
                    Dim DrResult As Array = TempDataTable.Select("ID='" & tempId.Trim & "'")

                    If DrResult.Length > 0 Then
                        TempDataTableLoad.ImportRow(DrResult(0))
                    End If
                    Status.Add(1)
                End If
            Next

            If TempDataTableLoad.Rows.Count > 0 Then
                dtgEntity1.DataSource = TempDataTableLoad
                dtgEntity1.DataBind()
            End If

            If Status.Count = 0 Then
                ShowMessage(lblMessage, "Belum dipilih!", True)
                pnlSearch.Visible = True
                pnlDatagrid.Visible = True
                div_1.Visible = True
                div_2.Visible = False
                div_btn1.Visible = True
                div_btn2.Visible = False
            Else
                pnlSearch.Visible = False
                pnlDatagrid.Visible = True
                div_1.Visible = False
                div_2.Visible = True
                div_btn1.Visible = False
                div_btn2.Visible = True
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub ButtonReset_Click(sender As Object, e As System.EventArgs) Handles ButtonReset.Click
        Response.Redirect("APGantiCaraBayar.aspx")
    End Sub

    Private Sub ButtonSave_Click(sender As Object, e As System.EventArgs) Handles ButtonSave.Click
        Try
            If cboCarabayar.SelectedValue = "" Then
                ShowMessage(lblMessage, "Pilih cara pembayaran!", True)
                Exit Sub
            Else
                Dim dtPVno As DataTable = createNewPVnoDT()

                For i As Integer = 0 To TempDataTableLoad.Rows.Count - 1
                    Dim PaymentVoucherNo As String = TempDataTableLoad.Rows(i).Item("PaymentVoucherNo").ToString.Trim
                    dtPVno.Rows.Add((i + 1).ToString, PaymentVoucherNo, "")
                Next

                With oCustomClass
                    .BranchId = Me.sesBranchId.Replace("'", "")
                    .paymentvoucherno_DT = dtPVno
                    .WOP = cboCarabayar.SelectedValue.Trim
                    .strConnection = GetConnectionString()
                End With

                lblMessage.Text = oController.APGAntiCaraBayar(oCustomClass)

                If lblMessage.Text <> "" Then
                    ShowMessage(lblMessage, lblMessage.Text, True)
                Else
                    Response.Redirect("APGantiCaraBayar.aspx")
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub ButtonBack_Click(sender As Object, e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("APGantiCaraBayar.aspx")
    End Sub
End Class