﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class APDisbInquiry
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private Baris As Integer
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.APDisbInq
    Private oController As New APDisbInqController   
    Protected WithEvents txtTglJatuhTempo As ucDateCE
    Protected WithEvents txtTglInvoice As ucDateCE
    Protected WithEvents oPaidLocation As ucPaymentLocation
    Private m_controller As New DataUserControlController
    Dim Total As Double
#End Region
#Region "Properti"
    Private Property PageState() As Int32
        Get
            Return (CType(Viewstate("pageState"), Int32))
        End Get
        Set(ByVal Value As Int32)
            Viewstate("pageState") = Value
        End Set
    End Property
    Private Property APType() As String
        Get
            Return (CType(viewstate("APType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APType") = Value
        End Set
    End Property
    Private Property PaymentLocation() As String
        Get
            Return (CType(viewstate("PaymentLocation"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PaymentLocation") = Value
        End Set
    End Property
    Private Property APDate() As String
        Get
            Return (CType(viewstate("APDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("APDate") = Value
        End Set
    End Property
    Private Property InvoiceDate() As String
        Get
            Return (CType(viewstate("InvoiceDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InvoiceDate") = Value
        End Set
    End Property
    Private Property Status() As String
        Get
            Return (CType(viewstate("Status"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Status") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return (CType(viewstate("FilterBy"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("FilterBy") = Value
        End Set
    End Property
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            InitialDefaultPanel()
            
            Me.FormID = "INQAP"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                With oBranch
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                    End If
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                End With
            End If
        End If
        lblMessage.Visible = False
    End Sub

    Private Sub InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False        
        txtTglJatuhTempo.Text = ""
        txtTglInvoice.Text = ""
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.APDisbInqList(oCustomClass)

        DtUserList = oCustomClass.ListAPInq
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = SortBy
        dtgEntity.DataSource = DvUserList

        Try
            dtgEntity.DataBind()
        Catch
            dtgEntity.CurrentPageIndex = 0
            dtgEntity.DataBind()
        End Try
        PagingFooter()
        pnlDatagrid.Visible = True
        pnlSearch.Visible = True
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If        
        Me.PageState = currentPage
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "Datagrid Command"
    Private Sub DtgEntity_ItemCommand(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgEntity.ItemCommand
        'Select Case e.CommandName
        '    Case "apto"
        '        If CheckFeature(Me.Loginid, Me.FormID, "APDST", Me.AppId) Then

        '        End If
        '    Case "apdetail"
        '        If CheckFeature(Me.Loginid, Me.FormID, "APDTL", Me.AppId) Then

        '        End If
        'End Select
    End Sub

    Private Sub DtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgEntity.ItemDataBound
        Dim lblAmount As New Label
        Dim lblSum, lblAPDate, lblInvoiceDate As Label

        Dim lnkAPTo As New HyperLink
        Dim lnkDesc As New HyperLink

        Dim lblAccountPayableNo As New Label
        Dim lblSupplierID As New Label
        Dim lblApplicationID As New Label
        Dim lblInsuranceBranchID As New Label
        Dim lblMaskAssID As New Label
        Dim nReffNo As New Label
        Dim nCustID As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblAssetRepSeqNo As New Label

        If e.Item.ItemIndex >= 0 Then
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            lblAPDate = CType(e.Item.FindControl("lblAPDate"), Label)
            lblInvoiceDate = CType(e.Item.FindControl("lblInvoiceDate"), Label)
            lblAPDate.Text = CDate(lblAPDate.Text).ToString("dd/MM/yyyy")
            If lblInvoiceDate.Text.Trim <> "-" Then
                lblInvoiceDate.Text = CDate(lblInvoiceDate.Text).ToString("dd/MM/yyyy")
            End If


            Total += CDbl(lblAmount.Text)
            lblAmount.Text = FormatNumber(CDbl(lblAmount.Text), 2)

            lnkAPTo = CType(e.Item.FindControl("lnkAPTo"), HyperLink)
            lnkDesc = CType(e.Item.FindControl("lnkDesc"), HyperLink)

            lblAccountPayableNo = CType(e.Item.FindControl("lblAccountPayableNo"), Label)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblInsuranceBranchID = CType(e.Item.FindControl("lblInsuranceBranchID"), Label)
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            nReffNo = CType(e.Item.FindControl("lblReffNo"), Label)
            nCustID = CType(e.Item.FindControl("lblCustID"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblAssetRepSeqNo = CType(e.Item.FindControl("lblAssetRepSeqNo"), Label)

            Select Case cmbAPType.Text.ToUpper.Trim
                Case "APDST"
                    lnkAPTo.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & lblSupplierID.Text.Trim & "')"
                Case "SPPL"
                    lnkDesc.NavigateUrl = "javascript:OpenViewApSupplier('" & "Finance" & "', '" & Server.UrlEncode(lblAccountPayableNo.Text.Trim) & "','" & Server.UrlEncode(Me.sesBranchId.Replace("'", "").Trim) & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenWinSupplier('" & "Finance" & "' , '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
                Case "INSR"
                    lnkDesc.NavigateUrl = "javascript:OpenViewApInsurance('" & "Finance" & "', '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "','" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Server.UrlEncode(lblMaskAssID.Text.Trim) & "','" & Server.UrlEncode(lblInsuranceBranchID.Text.Trim) & "','" & "Finance" & "')"
                Case "RCST"
                    lnkDesc.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(nCustID.Text.Trim) & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenCustomer('" & "Finance" & "', '" & Server.UrlEncode(nCustID.Text.Trim) & "')"
                Case "RADV"
                    lnkDesc.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "', '" & Server.UrlEncode(nReffNo.Text.Trim) & "','" & "Finance" & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenViewRequestNoRefund('" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "', '" & Server.UrlEncode(nReffNo.Text.Trim) & "','" & "Finance" & "')"
                Case "ASRP"
                    lnkDesc.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
                    lnkAPTo.NavigateUrl = "javascript:OpenViewAssetReplacament('" & "Finance" & "', '" & Server.UrlEncode(oBranch.SelectedItem.Value.Trim) & "' , '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "' , '" & Server.UrlEncode(lblAssetRepSeqNo.Text.Trim) & "','" & Server.UrlEncode(lblAssetSeqNo.Text.Trim) & "')"
            End Select
        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblSum = CType(e.Item.FindControl("lblSum"), Label)
            lblSum.Text = FormatNumber(Total.ToString, 2)
        End If

    End Sub

#End Region
#Region "OnClick"
    Private Sub imgSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Me.SearchBy = ""
        Me.FilterBy = ""
        If Me.BranchID <> "0" Then
            If oBranch.SelectedItem.Value <> "0" Then
                Me.BranchID = oBranch.SelectedValue
                Me.SearchBy = "ap.branchID = '" & Me.BranchID & "'"
                Me.FilterBy &= "BranchID=" & oBranch.SelectedItem.Text.Trim
            End If

            If Me.APType <> "0" Then
                Me.APType = cmbAPType.SelectedItem.Value
                Me.SearchBy += " and AP.APtype = '" & Me.APType & "'"
                Me.FilterBy += " and AP Type =" & Me.APType
            End If
        Else
            If Me.APType <> "0" Then
                Me.APType = cmbAPType.SelectedItem.Value
                Me.SearchBy += " AP.APtype = '" & Me.APType & "'"
                Me.FilterBy += " APType =" & Me.APType
            End If
        End If

        'If Me.BranchID <> "0" And Me.APType <> "0" Then
        '    Me.SearchBy = " ap.BranchID='" & Me.BranchID & _
        '        "' And ap.APType='" & Me.APType & _
        '        "'"
        '    Me.FilterBy = "BranchID=" & oBranch.BranchName & _
        '            " And APType=" & cmbAPType.SelectedItem.Text
        'End If
        If oPaidLocation.BranchID <> "0" Then
            Me.PaymentLocation = oPaidLocation.BranchID
            Me.SearchBy += " And ap.PaymentLocation='" & Me.PaymentLocation & "'"
            Me.FilterBy += " And PaymentLocation=" & oPaidLocation.BranchName
        End If

        If txtTglJatuhTempo.Text.Trim <> "" Then
            Me.APDate = txtTglJatuhTempo.Text
            Me.SearchBy += " And ap.DueDate='" & ConvertDate2(Me.APDate).ToString & "'"
            Me.FilterBy += " And DueDate=" & ConvertDate2(Me.APDate).ToString("dd/MM/yyyy") & ""
        End If
        If txtTglInvoice.Text.Trim <> "" Then
            Me.InvoiceDate = txtTglInvoice.Text
            Me.SearchBy += " And ap.InvoiceDate='" & ConvertDate2(Me.InvoiceDate) & "'"
            Me.FilterBy += " And InvoiceDate=" & ConvertDate2(Me.InvoiceDate).ToString("dd/MM/yyyy") & ""
        End If
        If ddlStatus.SelectedIndex > 0 Then
            Me.Status = ddlStatus.SelectedItem.Value
            Me.SearchBy += " And ap.Status='" & Me.Status & "'"
            Me.FilterBy += " And Status=" & ddlStatus.SelectedItem.Text & ""
        End If
        If ddlSearch.SelectedIndex > 0 Then
            Me.SearchBy += " And ap." & ddlSearch.SelectedItem.Value & "='" & txtSearchBy.Text & "'"
            Me.FilterBy += " And " & ddlSearch.SelectedItem.Text & "=" & txtSearchBy.Text & ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles ButtonReset.Click
        txtSearchAP.Text = ""
        txtSearchBy.Text = ""
        ddlStatus.SelectedIndex = 0
        ddlSearch.SelectedIndex = 0
        txtTglJatuhTempo.Text = ""
        txtTglInvoice.Text = ""
    End Sub

   
#End Region

End Class