﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="OtorisasiPembayaranATPM.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.OtorisasiPembayaranATPM" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="ucBankAccountBranch" Src="../../Webform.UserController/ucBankAccountBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ATPMOtorisasiList</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    OTORISASI PEMBAYARAN ATPM
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang Kontrak
                </label>
                <uc1:ucbranchall id="oBranch" runat="server"></uc1:ucbranchall>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" Width="100%" Visible="False" CssClass="grid_general"
                        DataKeyField="AgreementNo" AutoGenerateColumns="False" OnSortCommand="SortGrid"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                          <asp:TemplateColumn HeaderText="PILIH">
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPilih" runat="server" onclick="onCheked(this);"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierATPM" HeaderText="NO ATPM">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblSupplierATPM" runat="server" Text='<%#Container.DataItem("SupplierATPM")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO FAKTUR">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:TemplateColumn SortExpression="InvAmount" HeaderText="NILAI FAKTUR">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblInvAmount" runat="server" Text='<%#Container.DataItem("InvAmount")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="TotPPN" HeaderText="PPn">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblTotPPN" runat="server" Text='<%#Container.DataItem("TotPPN")%>'>
                                     <asp:HiddenField ID="hdfTransferNo" runat="server" />
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="TotPPh23" HeaderText="PPh23">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblTotPPh23" runat="server" Text='<%#Container.DataItem("TotPPh23")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="TotalInvAmount" HeaderText="Total Faktur">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalInvAmount" runat="server" Text='<%#Container.DataItem("TotalInvAmount")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankAccountID" HeaderText="Cara Bayar">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCaraBayar" runat="server" Text='<%#Container.DataItem("BankAccountID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="PostingDate" HeaderText="Tanggal Bayar ">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblPostingDate" runat="server" Text='<%#Container.DataItem("PostingDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="BankAccountID" HeaderText="Rekening Bank ">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountID" runat="server" Text='<%#Container.DataItem("BankAccountID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>
         <div class="form_button">
              <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Save" CssClass="small button gray" />
              <asp:Button ID="btnEdit" runat="server" CausesValidation="False" Text="Edit" CssClass="small button orange" />
              <asp:Button ID="btnReject" runat="server" CausesValidation="False" Text="Reject" CssClass="small button gray" />
              <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
        </div>
    </asp:Panel>
   </form>
</body>
</html>