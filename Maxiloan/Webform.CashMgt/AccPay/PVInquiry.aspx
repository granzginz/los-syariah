﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PVInquiry.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.PVInquiry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PVInquiry</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />    
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.servervariables("SERVER_NAME")%>/';	


        function OpenWinPaymentVoucher(pPaymentVoucherNo, pBranchID, pStyle) {
            window.open(ServerName + App + '/Webform.CashMgt/AccPay/ViewPaymentVoucher.aspx?PaymentVoucherNo=' + pPaymentVoucherNo + '&BranchID=' + pBranchID + '&Style=' + pStyle, 'UserLookUp', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1')
        }
        function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
            window.open(ServerName + App + '/Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&InsCoBranchID=' + pInsCoBranchID + '&Style=' + pStyle, 'InsuranceCo', 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0, scrollbars=1')
        }		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
       onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PAYMENT VOUCHER - INQUIRY
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <asp:Panel ID="pnlSearch" runat="server">
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Jenis Pembayaran</label>
                    <asp:DropDownList ID="cmbAPType" runat="server" />
                        <%--<asp:ListItem Value="0">Select One</asp:ListItem>
                        <asp:ListItem Value="SPPL">PO Supplier</asp:ListItem>
                        <asp:ListItem Value="INSR">Asuransi</asp:ListItem>
                        <asp:ListItem Value="INCS">Incentive Supplier</asp:ListItem>
                        <asp:ListItem Value="STNK">Birojasa STNK</asp:ListItem>
                        <asp:ListItem Value="RCST">Refund Customer</asp:ListItem>
                        <asp:ListItem Value="RADV">Eksekutor Fee</asp:ListItem>
                        <asp:ListItem Value="AFTN">Fiducia Notaris</asp:ListItem>
                        <asp:ListItem Value="KRSI">Karoseri</asp:ListItem>
                    </asp:DropDownList>--%>
                    <asp:TextBox ID="txtSearchAP" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RFVAPType" runat="server" Display="Dynamic" Visible="True"
                        Enabled="True" InitialValue="0" ErrorMessage="Harap pilih Jenis Pembayaran/Pencairan"
                        ControlToValidate="cmbAPType" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <%--<div class="form_single">--%>
                <div class="form_left">
                    <label>
                        Tanggal Rencana Bayar
                    </label>
                    <uc1:ucDateCE id="txtTglJatuhTempo" runat="server"></uc1:ucDateCE>                      
                </div>
                <div class="form_right">
                    <label>
                        No. Kontrak
                    </label>
                    <asp:TextBox ID="txtPvAgreementNo" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <%--<label class="label_req">--%>
                    <label>
                        Cabang</label>
                    <asp:DropDownList ID="oBranch" runat="server">
                    </asp:DropDownList>
<%--                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Cabang"
                        ControlToValidate="oBranch" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
--%>                </div>
                <div class="form_right">
                    <label>
                        Status
                    </label>
                    <asp:DropDownList ID="ddlStatus" runat="server">
                        <asp:ListItem Value="0">All</asp:ListItem>
                        <asp:ListItem Value="R">Request</asp:ListItem>
                        <asp:ListItem Value="V">Verified Cabang</asp:ListItem>
                        <asp:ListItem Value="C">Verified HO</asp:ListItem>
                        <asp:ListItem Value="A">Approved</asp:ListItem>
                        <asp:ListItem Value="B">Bank Selection</asp:ListItem>
                        <asp:ListItem Value="I">Input</asp:ListItem>                        
                        <asp:ListItem Value="P">Paid</asp:ListItem>
                        <asp:ListItem Value="J">Reject</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        No. PV
                    </label>
                    <asp:TextBox ID="txtPVNo" runat="server"></asp:TextBox>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal PV
                    </label>
                    <uc1:ucDateCE id="txtTglPV" runat="server"></uc1:ucDateCE>                      
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search"
                    CausesValidation="False"></asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR PAYMENT VOUCHER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general" BorderStyle="None"
                            BorderWidth="0" DataKeyField="PaymentVoucherNo" AutoGenerateColumns="False" AllowSorting="True"
                            Width="100%" OnSortCommand="SortGrid">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn SortExpression="PayMentVoucherNo" HeaderText="NO PV">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        Total
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APTo" HeaderText="BAYAR KEPADA">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPTo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAgreementNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <ItemTemplate>
                                        <asp:Label ID="lblName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVAmount" HeaderText="JUMLAH" HeaderStyle-CssClass="th_right"  ItemStyle-CssClass="th_right">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="TGL JT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVStatus" HeaderText="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVStatus") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SupplierID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssetSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblAssetRepSeqNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblCustID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblReffNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
            </div>
           
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
