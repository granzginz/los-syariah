﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="APDisbBank.aspx.vb" Inherits="Maxiloan.Webform.CashMgt.APDisbBank" %>

<%@ Register TagPrefix="uc1" TagName="ucAPDetailItem" Src="../../Webform.UserController/ucAPDetailItem.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAPBeneficiary" Src="../../Webform.UserController/ucAPBeneficiary.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="aspajax" %>
<%@ Register TagPrefix="uc1" TagName="ucBGNumber" Src="../../Webform.UserController/ucBGNumber.ascx" %>

<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AP Disburse Bank</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
</head>
<body>
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>

    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server"  ></asp:Label>
        <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">                              
                    <h3>
                        PEMBAYARAN/PENCAIRAN DENGAN BANK
                    </h3>
                </div>
        </div>
        <asp:Panel ID="pnlList" runat="server" >       
        <asp:Panel ID="pnlSearch"  runat="server">
            <div class="form_box">
				<div class="form_left">
				<label class ="label_req">  Jenis Pembayaran/Pencairan</label>
				  <asp:DropDownList ID="cmbAPType" runat="server">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            ErrorMessage="Harap Pilih Jenis Pembayaran/Pencairan" ControlToValidate="cmbAPType" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                    
				</div>
				<div class="form_right">
				<label> Tanggal Payment Voucher &lt;= </label>
				<asp:TextBox ID="txtTglPV" runat="server" />
                <aspajax:CalendarExtender id="calExTglPV" runat="server" TargetControlID="txtTglPV" Format="dd/MM/yyyy" />
                <asp:RequiredFieldValidator ID="rfvTglPV" runat="server" ControlToValidate="txtTglPV" 
                ErrorMessage="Harap isi dengan tanggal rencana bayar" enabled="false" Display="Dynamic" />
				</div>
			</div>

            <div class="form_button">
			    <asp:Button ID="ButtonSearch" runat="server" CssClass="small button blue" Text="Search">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonReset" runat="server" CssClass="small button gray" Text="Reset"
                CausesValidation="False"></asp:Button>
			 </div>
           
        </asp:Panel>
        <asp:Panel ID="pnlDatagrid"  runat="server">
            <div class="form_box_title">
                <div class="form_single">                    
                    <h4>
                        DAFTAR PAYMENT VOUCHER </h4>
                </div>
            </div>
          <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns">				
				<asp:DataGrid ID="dtgEntity" runat="server" CssClass="grid_general" OnSortCommand="SortGrid"
                            DataKeyField="PaymentVoucherNo" BorderWidth="0"
                            AutoGenerateColumns="False" AllowSorting="True" ShowFooter="True">
                            <HeaderStyle CssClass="th" />
		                    <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">                                    
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkApp" runat="server" CommandName="disburse">DISBURSE</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherNo" HeaderText="NO PV">                                   
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkPVNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherNo") %>'
                                            CommandName="id">
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        Grand Total
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="APTo" HeaderText="NAMA PEMBAYARAN/PENCAIRAN">                                  
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkAPTO" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APTo") %>'
                                            CommandName="apto">
                                        </asp:HyperLink><br />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVAmount" HeaderText="JUMLAH">                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVAmount") %>'>
                                        </asp:Label>
                                    </ItemTemplate>                                    
                                    <FooterTemplate>
                                        <asp:Label ID="lblSum" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PaymentVoucherDate" HeaderText="TGL PV">                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="PVDueDate" HeaderText="DUE DATE">                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblPVDueDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVDueDate") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SupplierID" Visible="False">                                   
                                    <ItemTemplate>
                                        <asp:Label ID="lblMaskAssID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblInsBranchID" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceBranchID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblSupplierID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SupplierID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblPaymentVoucherBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentVoucherBranchID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblReferenceNo" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.ReferenceNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblAssetRepSeqNo" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AssetRepSeqNo") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblApplicationID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationID") %>'>
                                        </asp:Label>
                                        <asp:Label ID="lblCustomerID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                </asp:DataGrid>
                   <uc2:ucGridNav id="GridNavigator" runat="server"/> 
              <%--  <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>--%>
		</div>
        </div>
        </div>            
        </asp:Panel>
        <asp:Panel ID="pnlDetail" runat="server" Visible="False">
            <div class="form_box_uc">
				<uc1:ucapdetailitem id="oDetailAP" runat="server"></uc1:ucapdetailitem>				
			</div>
            <div class="form_box_uc">
				 <uc1:ucapbeneficiary id="oBeneficiary" runat="server"></uc1:ucapbeneficiary>				
			</div>
            <div class="form_box_title">
				<div class="form_single">
				<h4>
                        PAYMENT VOUCHER DETAIL </h4>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label> Cabang </label>
				<asp:Label ID="lblBranch" runat="server"></asp:Label>
				</div>
				<div class="form_right">
				<label> Jenis Pembayaran/Pencairan </label>
				<asp:Label ID="lblAPType" runat="server"></asp:Label>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label>   No PV </label>
				 <asp:Label ID="lblPVNo" runat="server"></asp:Label>
				</div>
				<div class="form_right">
				<label> Tanggal PV </label>
				 <asp:Label ID="lblVoucDate" runat="server"></asp:Label>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label> Cara Pembayaran </label>
				<asp:Label ID="lblWOP" runat="server"></asp:Label>
				</div>
				<div class="form_right">
				
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label> Permintaan Dari </label>
				 <asp:Label ID="lblRequest" runat="server"></asp:Label>
				</div>
				<div class="form_right">
				<label> Tanggal Approval </label>
				<asp:Label ID="lblAppDate" runat="server"></asp:Label>
				</div>
			</div>            
            <div class="form_box">
                <div class="form_single">                    
                    <label class="label_general">Catatan</label>
                    <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                </div>                
            </div>
            <div class="form_box_title">
				<div class="form_single">
				<h4>PEMBAYARAN/PENCAIRAN DENGAN BANK</h4>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label>  Rekening Bank </label>
				<asp:Label ID="lblBankAccount" runat="server"></asp:Label>
                        <asp:Label ID="lblBankAccountID" runat="server"></asp:Label>
                        <asp:DropDownList ID="cboBank" runat="server" style="display:none">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="cbank" runat="server" Display="Dynamic" ErrorMessage="Harap Pilih Rekening Bank"
                            ControlToValidate="cboBank" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                    
				</div>
				<div class="form_right">
				<label> Saldo </label>
				 <asp:Label ID="lblBalance" runat="server"></asp:Label>
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label>  No Bilyet Giro </label>
				<uc1:ucbgnumber id="oBGNumber" runat="server"></uc1:ucbgnumber>
				</div>
				<div class="form_right">
				<label>  Tanggal Rencana Bayar </label>
				<asp:TextBox ID="txtTglJatuhTempo" runat="server"  />
                <aspajax:CalendarExtender id="calExTglJatuhTempo" runat="server" TargetControlID="txtTglJatuhTempo" Format="dd/MM/yyyy" />
                <asp:RequiredFieldValidator ID="rfvTglJatuhTempo" runat="server" ControlToValidate="txtTglJatuhTempo" 
                ErrorMessage="Harap isi dengan tanggal rencana bayar" Enabled="true" Display="Dynamic" />
				</div>
			</div>

            <div class="form_box">
				<div class="form_left">
				<label class ="label_req"> Nama Penerima</label>
				<asp:TextBox ID="txtName" runat="server"  style="width:350px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap Pilih Nama Penerima"
                            ControlToValidate="txtName" CssClass="validator_general"></asp:RequiredFieldValidator>
                    
				</div>
				<div class="form_right">
				<label>  No. Bukti Kas Keluar </label>
				<asp:TextBox ID="txtRefNo" runat="server"  Columns="22" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic"
                            ErrorMessage="Harap isi Bukti Kas keluar" ControlToValidate="txtRefNo" Enabled="False" CssClass="validator_general"></asp:RequiredFieldValidator>
                    
				</div>
			</div>

            <div class="form_box">
				<div class="form_single">				
                <label class="label_general">Catatan</label>
                <asp:TextBox ID="txtNoteDisburse" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>                    
				</div>				
			</div>
            <div class="form_button">
			    <asp:Button ID="ButtonDisburse" runat="server" CssClass="small button blue" Text="Save">
                </asp:Button>&nbsp;
                <asp:Button ID="ButtonCancel" runat="server" CssClass="small button gray" Text="Cancel" CausesValidation="False"></asp:Button>
			 </div>

        </asp:Panel>
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>        
    </form>
</body>
</html>
