﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BankReconcile.aspx.vb"
    Inherits="Maxiloan.Webform.CashMgt.BankReconcile" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccountID" Src="../../Webform.UserController/UcBankAccountID.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BankReconcile</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
        function OpenWinMain(BranchID, Voucherno, Style) {
            window.open(ServerName + App + '/Webform.LoanMnt/View/CashBankVoucher.aspx?BranchID=' + BranchID + '&Voucherno=' + Voucherno + '&Style=' + Style, null, 'left=0, top=0, width=' + x + ', height = ' + y + ', menubar=0,scrollbars=1')
        }
        function click() {
            if (event.button == 2) {
                alert('You Are Not Authorize!');
            }
        }
        document.onmousedown = click
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
	        <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    REKONSILIASI BANK
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlList" runat="server">   
        <div class="form_box">
	        <div class="form_single">
                <label>Rekening Bank</label>
                <uc1:UcBankAccountID id="oBankAccount" runat="server"></uc1:UcBankAccountID>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Tanggal Posting</label>                                
                <uc1:ucDateCE id="txtsdate" runat="server"></uc1:ucDateCE>  
	        </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label class ="label_req">Tanggal Valuta</label>
                <uc1:ucDateCE id="txtoValueDate" runat="server"></uc1:ucDateCE>               
	        </div>
        </div>                               
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue" CausesValidation="true"></asp:Button>&nbsp;
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"  CausesValidation="False"></asp:Button>
	    </div>
        </asp:Panel>
        <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DAFTAR VOUCHER BANK
                </h4>
            </div>
        </div>        
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns"> 
            <asp:DataGrid ID="DtgList" runat="server" Width="100%" Visible="False"  CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        DataKeyField="VoucherNo" AutoGenerateColumns="False" OnSortCommand="SortGrid"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>                                
                                <HeaderTemplate>
                                    <asp:CheckBox AutoPostBack="true" ID="ChkSelectAll" runat="server" OnCheckedChanged="SelectAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkSlct" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="branchid" HeaderText="Branch ID">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblbranchid" runat="server" Text='<%#Container.DataItem("branchid")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="Journalno" HeaderText="JOURNAL NO">                        
                                <ItemTemplate>
                                    <asp:Label ID="lblJournalNo" runat="server" Text='<%#Container.DataItem("Journalno")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="bankAccountName" HeaderText="Bank Account">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblBankAccountName" runat="server" Text='<%#Container.DataItem("bankAccountName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="VoucherNo" HeaderText="NO VOUCHER">   
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>                                     
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyVoucherNo" runat="server" Text='<%#Container.DataItem("VoucherNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="valuedate" SortExpression="valuedate" HeaderText="TGL VALUTA"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="postingdate" SortExpression="postingdate" HeaderText="TGL POSTING"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">      
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>                                  
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO REFERENCE">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblReferenceNoH" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Debitamount" HeaderText="JUMLAH DEBIT">  
                                <HeaderStyle CssClass="item_grid_right" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="10%"></ItemStyle>                              
                                <ItemTemplate>
                                    <asp:Label ID="lblPDCAmount" runat="server" Text='<%#formatnumber(Container.DataItem("Debitamount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Creditamount" HeaderText="JUMLAH Credit">  
                                 <HeaderStyle CssClass="item_grid_right" HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right" HorizontalAlign="Right" Width="10%"></ItemStyle>                              
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#formatnumber(Container.DataItem("Creditamount"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CashierID" HeaderText="KASIR">    
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="8%"></ItemStyle>                                    
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Container.DataItem("CashierID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="PaymentTypeId" HeaderText="PaymentTypeId">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblPaymentTypeId" runat="server" Text='<%#Container.DataItem("PaymentTypeId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
            </asp:DataGrid>
            <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
            <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
        </div>
        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonReconcile" runat="server"  Text="Reconcile" CssClass ="small button blue"   CausesValidation="False"></asp:Button>
	    </div>       
        </asp:Panel>    
        <asp:Panel ID="pnlInfo" runat="server">
        <div class="form_box_title">
	        <div class="form_single">
                <div class="form_left"> 
                    <label>Saldo Sebelum Rekonsiliasi</label>
                    <asp:Label ID="lblBefore" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Total Debit Belum Rekonsiliasi</label>	
                    <asp:Label ID="lblDebit" runat="server"></asp:Label>
		        </div>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <div class="form_left">
                    <label>Saldo Sekarang</label>
                    <asp:Label ID="lblCurrent" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>Total Pembiayaan Belum Rekonsiliasi</label>
                    <asp:Label ID="lblCredit" runat="server"></asp:Label>	
		        </div>
	        </div>
        </div>                    
        </asp:Panel>
        <asp:Panel ID="pnlList2" runat="server" Visible="False">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    DETAIL REKONSILIASI BANK
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Rekening Bank</label>
                <asp:Label ID="lblBankAccountDet" runat="server"></asp:Label>
	        </div>
        </div>
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns">     
            <asp:DataGrid ID="DtgDetail" runat="server" Width="100%" Visible="False"  CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        DataKeyField="VoucherNo" AutoGenerateColumns="False" OnSortCommand="SortGrid2"
                        AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="VoucherNo" HeaderText="NO VOUCHER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="HyVoucherNODet" runat="server" Text='<%#Container.DataItem("VoucherNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="valuedate" SortExpression="valuedate" HeaderText="TGL VALUE"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="postingdate" SortExpression="postingdate" HeaderText="TGL POSTING"
                                DataFormatString="{0:dd/MM/yyyy}">                                
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Description" HeaderText="KETERANGAN">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDescDet" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ReferenceNo" HeaderText="NO REFERENCE">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblReferenceNo" runat="server" Text='<%#Container.DataItem("ReferenceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Debitamount" HeaderText="JUMLAH DEBIT">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblDebitAmountDet" runat="server" Text='<%#formatnumber(Container.DataItem("Debitamount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Creditamount" HeaderText="JUMLAH PEMBIAYAAN">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCreditAmountDet" runat="server" Text='<%#formatnumber(Container.DataItem("Creditamount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CashierID" HeaderText="KASIR">                                
                                <ItemTemplate>
                                    <asp:Label ID="lblCashierIDdet" runat="server" Text='<%#Container.DataItem("CashierID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
            </asp:DataGrid>            
        </div>
        </div>
        </div>    
        <div class="form_button">
            <asp:Button ID="ButtonSave" runat="server"  Text="Save" CssClass ="small button blue"></asp:Button>
            <asp:Button ID="ButtonCancel" runat="server"  Text="Cancel" CssClass ="small button gray"  CausesValidation="False"></asp:Button>
	    </div>        
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>        
    </form>
</body>
</html>

