﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper

#End Region

Public Class BankReconcile
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBankAccount As UcBankAccountID
    Protected WithEvents oSearchBy As UcSearchBy    
    Protected WithEvents txtsdate As ucDateCE
    Protected WithEvents txtoValueDate As ucDateCE
    Dim intLoopGrid As Integer
#Region "Property"

    Private Property SelectAll2() As Double
        Get
            Return (CType(Viewstate("SelectAll"), Double))
        End Get
        Set(ByVal Value As Double)
            Viewstate("SelectAll") = Value
        End Set
    End Property

    Private Property strVoucherno() As String
        Get
            Return (CType(Viewstate("strVoucherno"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strVoucherno") = Value
        End Set
    End Property

    Private Property strJournalno() As String
        Get
            Return (CType(Viewstate("strJournalno"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("strJournalno") = Value
        End Set
    End Property
    Private Property PaymentTypeId() As String
        Get
            Return (CType(Viewstate("PaymentTypeId"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("PaymentTypeId") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return (CType(Viewstate("FilterBy"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("FilterBy") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.BankReconcile
    Private oController As New BankReconcileController
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.FormID = "BANKRECON"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'txtsdate.Text = CStr(Day(Me.BusinessDate)) + "/" + CStr(Month(Me.BusinessDate)) + "/" + CStr(Year(Me.BusinessDate))
                txtsdate.IsRequired = True
                txtoValueDate.IsRequired = True

                oBankAccount.BankPurpose = ""
                oBankAccount.IsAll = True
                oBankAccount.BankType = "B"
                oBankAccount.BindBankAccount()
                oSearchBy.ListData = "Voucherno, No Voucher-Description, Keterangan-CashierID, Kasir-ReferenceNo, No Referensi"
                oSearchBy.BindData()
                Me.SelectAll2 = 0
                Me.SearchBy = ""
                Me.SortBy = ""

            End If

        End If
        pnlInfo.Visible = False
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim lblPaymentTypeId As Label
        Dim ChkBox As CheckBox

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.BankReconList(oCustomClass)

        DtUserList = oCustomClass.listBankRecon
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgList.DataSource = DvUserList
        Try
            DtgList.DataBind()
        Catch
            DtgList.CurrentPageIndex = 0
            DtgList.DataBind()
        End Try
        PagingFooter()
        If DtgList.Items.Count > 0 Then
            ButtonReconcile.Visible = True
        Else
            ButtonReconcile.Visible = False
        End If

        For intLoopGrid = 0 To DtgList.Items.Count - 1
            ChkBox = CType(DtgList.Items(intLoopGrid).Cells(0).FindControl("ChkSlct"), CheckBox)
            lblPaymentTypeId = CType(DtgList.Items(intLoopGrid).Cells(0).FindControl("lblPaymentTypeId"), Label)
            Me.PaymentTypeId = lblPaymentTypeId.Text.Trim
            If Me.PaymentTypeId = "BA" Then
                ChkBox.Enabled = True
            Else
                ChkBox.Enabled = False
            End If
        Next

        pnlList.Visible = True
        pnlDatagrid.Visible = True

    End Sub


    Sub DoBind2(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With
        oCustomClass = oController.BankReconListDet(oCustomClass)
        DtUserList = oCustomClass.listBankReconDet
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        DtgDetail.DataSource = DvUserList

        '  Try
        DtgDetail.DataBind()
        ' Catch
        '    DtGridDeposit.DataBind()
        ' End Try

        lblBankAccountDet.Text = oBankAccount.BankAccountName
    End Sub

#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    'Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound
    '    Dim m As Int32
    '    Dim hypReceive As HyperLink
    '    Dim lblApplicationid As Label
    '    If e.Item.ItemIndex >= 0 Then
    '        lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), Label)
    '        hypReceive = CType(e.Item.FindControl("HypReceive"), HyperLink)
    '        hypReceive.NavigateUrl = "PDCReceive.aspx?applicationid=" & lblApplicationid.Text.Trim & "&branchid=" & oBranch.BranchID.Trim
    '    End If
    'End Sub
#End Region


#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        'Me.SearchBy = ""
        'oSearchBy.Text = ""
        'oSearchBy.BindData()
        Server.Transfer("BankReconcile.aspx")
        'DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        Dim oController As New BankReconcileController
        Dim oCustomClass As New Parameter.BankReconcile

        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", Me.AppId) Then
            Dim strSearch As New StringBuilder
            Dim strParam As New StringBuilder
            'Me.SearchBy = ""
            'Me.FilterBy = ""
            strSearch.Append("CashBankTransactions.branchid = '" & Me.sesBranchId.Replace("'", "") & "'")
            strSearch.Append(" and CashBankTransactions.isreconcile = 0")
            strParam.Append("BranchID  = '" & Me.BranchName & "'")
            strParam.Append(" and CashBankTransactions.isreconcile = 0")
            If oBankAccount.BankAccountID.Trim <> "" Then
                strSearch.Append(" and CashBankTransactions.BankAccountid = '" & oBankAccount.BankAccountID.Trim & "' ")
                strParam.Append(" Bank Account Name = " & oBankAccount.BankAccountName & " ")
            End If

            If txtsdate.Text <> "" Then
                strSearch.Append("  and CashBankTransactions.PostingDate <= '" & ConvertDate2(txtsdate.Text) & "'")
                strParam.Append(" and PostingDate = " & ConvertDate2(txtsdate.Text) & "")
            End If

            If txtoValueDate.Text <> "" Then
                strSearch.Append("  and CashBankTransactions.ValueDate <= '" & ConvertDate2(txtoValueDate.Text) & "'")
                strParam.Append(" and ValueDate = " & ConvertDate2(txtoValueDate.Text) & "")
            End If
            If oSearchBy.Text.Trim <> "" Then
                strSearch.Append(" and " & oSearchBy.ValueID & " Like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
                strParam.Append("  and oSearchBy.Text.Trim & " + " like %" + " & oSearchBy.ValueID & ")
            
            End If
            Me.SearchBy = strSearch.ToString
            Me.FilterBy = strParam.ToString

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .BankAccountID = oBankAccount.BankAccountID.Trim

            End With
            oCustomClass = oController.InfoReconBalance(oCustomClass)
            With oCustomClass
                lblBefore.Text = FormatNumber(.BeforeBalance, 2)
                lblCurrent.Text = FormatNumber(.CurrentBalance, 2)
                lblDebit.Text = FormatNumber(.DebitAmount, 2)
                lblCredit.Text = FormatNumber(.CreditAmount, 2)
            End With

            pnlDatagrid.Visible = True
            pnlList.Visible = True
            DoBind(Me.SearchBy, Me.SortBy)
            pnlDatagrid.Visible = True
            pnlList.Visible = True
            DtgList.Visible = True
            pnlInfo.Visible = True
        End If
    End Sub
#End Region

#Region "Select All"
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ChkSelect As CheckBox
        Dim lblPaymentTypeId As Label
        Dim x As Integer
        If Me.SelectAll2 = 0 Then
            For x = 0 To DtgList.Items.Count - 1
                ChkSelect = CType(DtgList.Items(x).FindControl("ChkSlct"), CheckBox)
                lblPaymentTypeId = CType(DtgList.Items(intLoopGrid).Cells(0).FindControl("lblPaymentTypeId"), Label)
                Me.PaymentTypeId = lblPaymentTypeId.Text.Trim
                If Me.PaymentTypeId = "BA" Then
                    ChkSelect.Checked = True
                Else
                    ChkSelect.Checked = False
                End If
            Next
            Me.SelectAll2 = 1
        Else
            For x = 0 To DtgList.Items.Count - 1
                ChkSelect = CType(DtgList.Items(x).FindControl("ChkSlct"), CheckBox)
                ChkSelect.Checked = False
            Next
            Me.SelectAll2 = 0
        End If
    End Sub
#End Region


#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Public Sub SortGrid2(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind2(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Reconcile"
    Private Sub ButtonReconcile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReconcile.Click
        If sessioninvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "RECON", Me.AppId) Then

            Dim NewchkBox As CheckBox
            Dim lblVoucherNew As HyperLink
            Dim lblJournalNew As Label
            Dim strVoucherno As New StringBuilder
            Dim strJournalno As New StringBuilder
            Dim dtPDC As New DataTable
            Dim lblPaymentTypeId As Label
            Dim flag As Boolean
            flag = True
            For intLoopGrid = 0 To DtgList.Items.Count - 1
                NewchkBox = CType(DtgList.Items(intLoopGrid).Cells(0).FindControl("ChkSlct"), CheckBox)
                lblVoucherNew = CType(DtgList.Items(intLoopGrid).Cells(0).FindControl("HyVoucherNo"), HyperLink)
                lblJournalNew = CType(DtgList.Items(intLoopGrid).Cells(0).FindControl("lblJournalno"), Label)
                lblPaymentTypeId = CType(DtgList.Items(intLoopGrid).Cells(0).FindControl("lblPaymentTypeId"), Label)
                Me.PaymentTypeId = lblPaymentTypeId.Text.Trim
                If NewchkBox.Checked = True And Me.PaymentTypeId = "BA" Then
                    'NewchkBox.Enabled = False 
                    strVoucherno.Append(CStr(IIf(strVoucherno.ToString = "", "", ",")) & "'" & lblVoucherNew.Text.Replace("'", "") & "'")
                    strJournalno.Append(CStr(IIf(strJournalno.ToString = "", "", ",")) & "'" & lblJournalNew.Text.Replace("'", "") & "'")
                    flag = True
                ElseIf NewchkBox.Checked = False And Me.PaymentTypeId <> "BA" Then
                    flag = False
                End If
            Next

            If Trim(strVoucherno.ToString) = "" And flag = True Then                
                ShowMessage(lblMessage, "Harap pilih No Voucher", True)
                Exit Sub
            ElseIf Trim(strVoucherno.ToString) = "" And flag = False Then
                Exit Sub
            End If
            With oCustomClass
                oCustomClass.GroupVoucherno = Trim(strVoucherno.ToString)
                Me.strVoucherno = oCustomClass.GroupVoucherno
                oCustomClass.GroupJournalno = Trim(strJournalno.ToString)
                Me.strJournalno = oCustomClass.GroupJournalno
                oCustomClass.BranchId = Me.BranchID
            End With
            Me.SearchBy = " Voucherno in (" & Trim(strVoucherno.ToString) & ")"

            DoBind2(Me.SearchBy, Me.SortBy)
            pnlList2.Visible = True
            pnlList.Visible = False
            DtgList.Visible = False
            pnlDatagrid.Visible = False
            DtgDetail.Visible = True
        End If
    End Sub
#End Region

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click        
        Response.Redirect("BankReconcile.aspx")
    End Sub

    Private Sub imgSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then
            With oCustomClass
                .LoginId = Me.Loginid
                .BranchId = Me.sesBranchId.Replace("'", "")
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .GroupVoucherno = Me.strVoucherno
                .GroupJournalno = Me.strJournalno
            End With
            Try
                oController.SaveBankRecon(oCustomClass)

                pnlList.Visible = True
                pnlList2.Visible = False
                DtgDetail.Visible = False                
                ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
            Catch exp As Exception                
                ShowMessage(lblMessage, exp.Message, True)
            End Try

        End If
    End Sub

    Private Sub DtgList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgList.ItemDataBound
        If sessioninvalid() Then
            Exit Sub
        End If
        If CheckFeature(Me.Loginid, Me.FormID, "VCHR", Me.AppId) Then
            Dim NlblVoucherno As HyperLink
            Dim Nlblpbranchid As Label
            If e.Item.ItemIndex >= 0 Then
                NlblVoucherno = CType(e.Item.FindControl("HyVoucherno"), HyperLink)
                Nlblpbranchid = CType(e.Item.FindControl("lblBranchid"), Label)
                NlblVoucherno.NavigateUrl = "javascript:OpenWinMain('" & Trim(Nlblpbranchid.Text) & "','" & Trim(NlblVoucherno.Text) & "', '" & "Finance" & "')"
            End If
        End If
    End Sub


End Class