﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class Agency
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBankAccount As UcBankAccount
    Protected WithEvents oCompanyAddress As ucAddress
    Protected WithEvents oContactPerson As UcContactPerson
    Protected WithEvents oViewAddress As UcViewAddress
    Protected WithEvents oViewBankAccount As ucViewBankAccount
    Protected WithEvents oViewContactPerson As UcViewContactPerson


#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.Agency
    Private oController As New AgencyController
    Private oClassAddress As New Parameter.Address
    Private oClassPersonal As New Parameter.Personal
    Private oClassBankAccount As New Parameter.BankAccount


#End Region
#Region " Properties"
    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            oCompanyAddress.Style = "AccMnt"
            oCompanyAddress.BindAddress()
            InitialDefaultPanel()

            Me.FormID = "STNKAgency"
            oSearchBy.ListData = "AgentID,ID BiroJasa-AGENTNAME,Nama BiroJasa"
            oSearchBy.BindData()
            If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
                Me.SearchBy = ""
                Me.SortBy = ""

                With oBankAccount
                    'TODO Bank Account
                    '.setValidatorLookup()
                    .Style = "Insurance"
                    .BindBankAccount()
                End With

            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlAdd.Visible = False
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        pnlView.Visible = False
        lblMessage.Visible = False
        lblMessage.Visible = False
    End Sub
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.STNKAgencyPaging(oCustomClass)

        DtUserList = oCustomClass.listdata
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAgency.DataSource = DvUserList

        Try
            DtgAgency.DataBind()
        Catch
            DtgAgency.CurrentPageIndex = 0
            DtgAgency.DataBind()
        End Try
        pnlList.Visible = True
        pnlDtGrid.Visible = True

        PagingFooter()
    End Sub
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        lblMessage.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        lblMessage.Visible = False
        If IsNumeric(txtpage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtpage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtpage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "DataGrid Command"
    Private Sub DtgAgency_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgency.ItemCommand
        Select Case e.CommandName
            Case "EDIT"
                If CheckFeature(Me.Loginid, Me.FormID, "EDIT", Me.AppId) Then
                    Me.Mode = "EDIT"
                    lblMessage.Visible = False
                    lblTitle.Text = "BIRO JASA- EDIT"
                    oBankAccount.RequiredBankName()
                    pnlList.Visible = False
                    pnlDtGrid.Visible = False
                    pnlAdd.Visible = True

                    RevAgentId.Enabled = False

                    Dim hypID As LinkButton
                    Dim dtView As New DataTable
                    Dim lblAgentName As Label
                    Dim lblStatus As Label

                    lblAgentName = CType(DtgAgency.Items(e.Item.ItemIndex).FindControl("lblAgentName"), Label)
                    hypID = CType(DtgAgency.Items(e.Item.ItemIndex).FindControl("lnkAgencyId"), LinkButton)
                    lblStatus = CType(DtgAgency.Items(e.Item.ItemIndex).FindControl("lblStatus"), Label)


                    lblBranchID.Text = Me.BranchName
                    txtAgentID.Text = hypid.Text
                    txtAgentID.Enabled = False
                    txtAgentName.Text = lblAgentName.Text
                    If lblStatus.Text = "Active" Then
                        chkStatus.Checked = True
                    Else
                        chkStatus.Checked = False

                    End If


                    dtView = oController.STNKAgencyView(GetConnectionString, Me.BranchID, hypid.Text.Trim)

                    BindEditDetailPanel(dtview)
                    dtview.Dispose()

                End If
            Case "DELETE"
                lblMessage.Visible = False
                If CheckFeature(Me.Loginid, Me.FormID, "DEL", Me.AppId) Then
                    Dim hypID As LinkButton

                    hypID = CType(DtgAgency.Items(e.Item.ItemIndex).FindControl("lnkAgencyId"), LinkButton)
                    oCustomClass.strConnection = GetConnectionString
                    oCustomClass.AgentId = hypid.Text
                    oCustomClass.BranchId = Me.BranchID
                    Try
                        oController.STNKAgencyDelete(oCustomClass)

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, True)
                        DoBind(Me.SearchBy, Me.SortBy)

                    Catch exp As Exception

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_FAILED, True)
                    End Try
                End If
            Case "ShowView"
                If CheckFeature(Me.Loginid, Me.FormID, "VIEW", Me.AppId) Then
                    lblMessage.Visible = False

                    Dim dtView As New DataTable
                    Dim hypID As LinkButton
                    Dim lblAgentName As Label
                    Dim lblStatus As Label
                    lblAgentName = CType(DtgAgency.Items(e.Item.ItemIndex).FindControl("lblAgentName"), Label)
                    hypID = CType(DtgAgency.Items(e.Item.ItemIndex).FindControl("lnkAgencyId"), LinkButton)
                    lblStatus = CType(DtgAgency.Items(e.Item.ItemIndex).FindControl("lblStatus"), Label)

                    lblViewBranch.Text = Me.BranchName
                    lblAgentID.Text = hypid.Text.Trim
                    lblViewAgentName.Text = lblagentname.Text
                    lblIsActive.Text = lblstatus.Text
                    dtView = oController.STNKAgencyView(GetConnectionString, Me.BranchID, hypid.Text.Trim)
                    pnlList.Visible = False
                    pnlDtGrid.Visible = False
                    pnlView.Visible = True
                    BindViewPanel(dtview)
                    dtview.Dispose()

                End If

        End Select
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        lblMessage.Visible = False
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgAgency_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgency.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm();")
        End If
    End Sub
#End Region
#Region "Bind Panel"
    Private Sub BindEditDetailPanel(ByVal DtInfo As DataTable)
        Try
            If DtInfo.Rows.Count > 0 Then
                With oCompanyAddress
                    .Address = CStr(DtInfo.Rows(0).Item("agentAddress")).Trim
                    .RT = CStr(DtInfo.Rows(0).Item("agentRT")).Trim
                    .RW = CStr(DtInfo.Rows(0).Item("agentRW")).Trim
                    .Kelurahan = CStr(DtInfo.Rows(0).Item("agentKelurahan")).Trim
                    .Kecamatan = CStr(DtInfo.Rows(0).Item("agentKecamatan")).Trim
                    .City = CStr(DtInfo.Rows(0).Item("agentCity")).Trim
                    .ZipCode = CStr(DtInfo.Rows(0).Item("agentZipCode")).Trim
                    .AreaPhone1 = CStr(DtInfo.Rows(0).Item("agentAreaPhone1")).Trim
                    .Phone1 = CStr(DtInfo.Rows(0).Item("agentPhone1")).Trim
                    .AreaPhone2 = CStr(DtInfo.Rows(0).Item("agentAreaPhone2")).Trim
                    .Phone2 = CStr(DtInfo.Rows(0).Item("agentPhone2")).Trim
                    .AreaFax = CStr(DtInfo.Rows(0).Item("agentAreaFax")).Trim
                    .Fax = CStr(DtInfo.Rows(0).Item("agentFax")).Trim
                    .Style = "AccMnt"
                    .BindAddress()
                End With

                With oContactPerson
                    .ContactPerson = CStr(DtInfo.Rows(0).Item("ContactPersonName")).Trim
                    .ContactPersonTitle = CStr(DtInfo.Rows(0).Item("ContactPersonTitle")).Trim
                    .Email = CStr(DtInfo.Rows(0).Item("Email")).Trim
                    .MobilePhone = CStr(DtInfo.Rows(0).Item("MobilePhone")).Trim
                    .BindContacPerson()
                End With
                With oBankAccount
                    .BankBranchId = CStr(DtInfo.Rows(0).Item("AgentBankBranchID")).Trim
                    .BindBankAccount()
                    .RequiredBankName()
                    .AccountName = CStr(DtInfo.Rows(0).Item("AgentAccountName")).Trim
                    .BankBranch = CStr(DtInfo.Rows(0).Item("AgentBankBranch")).Trim
                    .AccountNo = CStr(DtInfo.Rows(0).Item("AgentAccountNO")).Trim
                    .BankID = CStr(DtInfo.Rows(0).Item("AgentBankID")).Trim
                    'TODO Bank Account
                    '.setValidatorLookup()
                End With
            End If
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub



    Private Sub BindViewPanel(ByVal dtinfo As DataTable)
        If dtinfo.Rows.Count > 0 Then
            With (dtinfo.Rows(0))
                If CBool(dtinfo.Rows(0).Item("IsActive")) Then
                    lblIsActive.Text = "Active"
                Else
                    lblIsActive.Text = "Not Active"
                End If

                With oViewAddress
                    .Address = CStr(dtinfo.Rows(0).Item("agentAddress")).Trim
                    .RT = CStr(dtinfo.Rows(0).Item("agentRT")).Trim
                    .RW = CStr(dtinfo.Rows(0).Item("agentRW")).Trim
                    .Kelurahan = CStr(dtinfo.Rows(0).Item("agentKelurahan")).Trim
                    .Kecamatan = CStr(dtinfo.Rows(0).Item("agentKecamatan")).Trim
                    .City = CStr(dtinfo.Rows(0).Item("agentCity")).Trim
                    .ZipCode = CStr(dtinfo.Rows(0).Item("agentZipCode")).Trim
                    .AreaPhone1 = CStr(dtinfo.Rows(0).Item("agentAreaPhone1")).Trim
                    .Phone1 = CStr(dtinfo.Rows(0).Item("agentPhone1")).Trim
                    .AreaPhone2 = CStr(dtinfo.Rows(0).Item("agentAreaPhone2")).Trim
                    .Phone2 = CStr(dtinfo.Rows(0).Item("agentPhone2")).Trim
                    .AreaFax = CStr(dtinfo.Rows(0).Item("agentAreaFax")).Trim
                    .Fax = CStr(dtinfo.Rows(0).Item("agentFax")).Trim

                End With


                With oViewContactPerson
                    .ContactPerson = CStr(dtinfo.Rows(0).Item("ContactPersonName")).Trim
                    .ContactPersonTitle = CStr(dtinfo.Rows(0).Item("ContactPersonTitle")).Trim
                    .Email = CStr(dtinfo.Rows(0).Item("Email")).Trim
                    .MobilePhone = CStr(dtinfo.Rows(0).Item("MobilePhone")).Trim
                End With
                With oViewBankAccount
                    .AccountName = CStr(dtinfo.Rows(0).Item("AgentAccountName")).Trim
                    .BankBranch = CStr(dtinfo.Rows(0).Item("AgentBankBranch")).Trim
                    .AccountNo = CStr(dtinfo.Rows(0).Item("AgentAccountNO")).Trim
                    .BankName = CStr(dtinfo.Rows(0).Item("AgentBankID")).Trim
                End With
            End With
        End If
    End Sub
#End Region
#Region "Saving Process"
    Private Sub BtnAddNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAddNew.Click
        If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
            EmptyData()
            pnlAdd.Visible = True
            pnlDtGrid.Visible = False
            pnlList.Visible = False
            lblMessage.Visible = False
            Me.Mode = "ADD"
            RevAgentId.Enabled = True
            Me.BranchID = Me.sesBranchId.Replace("'", "")
            lblBranchID.Text = Me.BranchName
            lblTitle.Text = "BIRO JASA - ADD"
            oBankAccount.BindBankAccount()
            oBankAccount.RequiredBankName()
            oCompanyAddress.Style = "AccMnt"
            txtAgentID.Enabled = True
        End If
    End Sub
#End Region
    Sub EmptyData()
        txtAgentID.Text = ""
        txtAgentName.Text = ""
        oCompanyAddress.Address = ""
        oCompanyAddress.RT = ""
        oCompanyAddress.RW = ""
        oCompanyAddress.Kelurahan = ""
        oCompanyAddress.Kecamatan = ""
        oCompanyAddress.City = ""
        oCompanyAddress.ZipCode = ""
        oCompanyAddress.AreaPhone1 = ""
        oCompanyAddress.Phone1 = ""
        oCompanyAddress.Phone2 = ""
        oCompanyAddress.Fax = ""
        oCompanyAddress.AreaPhone2 = ""
        oCompanyAddress.AreaPhone2 = ""
        oCompanyAddress.AreaFax = ""
        oCompanyAddress.BindAddress()


        oContactPerson.ContactPerson = ""
        oContactPerson.ContactPersonTitle = ""
        oContactPerson.MobilePhone = ""
        oContactPerson.Email = ""
        oContactPerson.BindContacPerson()


        oBankAccount.BankBranch = ""
        oBankAccount.AccountNo = ""
        oBankAccount.AccountName = ""

        chkStatus.Checked = False

    End Sub
#Region "Search Process"
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        lblMessage.Visible = False
        Me.BranchID = Me.sesBranchId.Replace("'", "")
        Me.SearchBy = " BranchID = '" & Me.BranchID & "' "
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID & " Like '%" & oSearchBy.Text.Trim & "%' "
        End If
        pnlList.Visible = True
        pnlDtGrid.Visible = True
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        lblMessage.Visible = False
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        lblMessage.Visible = False
        If Page.IsValid Then
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .AgentId = txtAgentID.Text.Trim
                .AgentName = txtAgentName.Text.Trim
                .IsActive = chkStatus.Checked
            End With

            With oClassBankAccount
                .BankBranchId = oBankAccount.BankBranchId
                .AccountName = oBankAccount.AccountName
                .BankID = oBankAccount.BankID
                .BankBranch = oBankAccount.BankBranch
                .AccountNo = oBankAccount.AccountNo
            End With

            With oClassAddress
                .Address = oCompanyAddress.Address
                .RT = oCompanyAddress.RT
                .RW = oCompanyAddress.RW
                .Kelurahan = oCompanyAddress.Kelurahan
                .Kecamatan = oCompanyAddress.Kecamatan
                .City = oCompanyAddress.City
                .ZipCode = oCompanyAddress.ZipCode
                .AreaPhone1 = oCompanyAddress.AreaPhone1
                .Phone1 = oCompanyAddress.Phone1
                .AreaPhone2 = oCompanyAddress.AreaPhone2
                .Phone2 = oCompanyAddress.Phone2
                .AreaFax = oCompanyAddress.AreaFax
                .Fax = oCompanyAddress.Fax
            End With

            With oClassPersonal
                .PersonName = oContactPerson.ContactPerson
                .PersonTitle = oContactPerson.ContactPersonTitle
                .Email = oContactPerson.Email
                .MobilePhone = oContactPerson.MobilePhone
            End With
            Try
                Select Case Me.Mode
                    Case "ADD"
                        oController.STNKAgencySave(oCustomClass, oClassBankAccount, oClassAddress, oClassPersonal)

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                    Case "EDIT"
                        oController.STNKAgencyUpdate(oCustomClass, oClassBankAccount, oClassAddress, oClassPersonal)

                        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                End Select
                DoBind(Me.SearchBy, Me.SortBy)
                pnlList.Visible = True
                pnlDtGrid.Visible = False

                pnlAdd.Visible = False
                pnlList.Visible = True
                pnlDtGrid.Visible = False

                pnlAdd.Visible = False

                DoBind(Me.SearchBy, Me.SortBy)
            Catch exp As Exception
                ShowMessage(lblMessage, exp.Message, True)
            End Try
        End If


    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        lblMessage.Visible = False
        pnlList.Visible = True
        pnlDtGrid.Visible = False
        pnlAdd.Visible = False
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        pnlView.Visible = False
        pnlList.Visible = True
        pnlDtGrid.Visible = True

    End Sub


End Class