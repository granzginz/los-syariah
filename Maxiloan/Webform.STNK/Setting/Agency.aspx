﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Agency.aspx.vb" Inherits="Maxiloan.Webform.STNK.Agency" %>

<%@ Register TagPrefix="uc1" TagName="UcViewContactPerson" Src="../../webform.UserController/UcViewContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucViewBankAccount" Src="../../webform.UserController/ucViewBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcViewAddress" Src="../../webform.UserController/UcViewAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../webform.UserController/UcContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../webform.UserController/ucAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../webform.UserController/UcBranch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agency</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
     <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI BIRO JASA STNK
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
        <asp:Panel ID="pnlDtGrid" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR BIRO JASA STNK
                    </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgency" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="SortGrid" DataKeyField="AgentId" BorderStyle="None" BorderWidth="0"
                            CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/IconEdit.gif"
                                            CommandName="EDIT"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/IconDelete.gif"
                                            CommandName="DELETE"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgentId" HeaderText="ID BIROJASA">
                                    <ItemStyle Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkAgencyId" runat="server" Text='<%#Container.DataItem("AgentId")%>'
                                            CommandName="ShowView">
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgentName" HeaderText="NAMA BIRO JASA">
                                    <ItemStyle Width="20%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAgentName" runat="server" Text='<%#Container.DataItem("AgentName")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgentAddress" HeaderText="ALAMAT">
                                    <ItemStyle Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblAgentAddress" runat="server" Text='<%#Container.DataItem("AgentAddress")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="IsActive" HeaderText="STATUS">
                                    <ItemStyle Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%#IIF(Container.DataItem("IsActive")="True","Active","Not Active")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages">
                            </PagerStyle>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtpage" runat="server" Width="34px" MaxLength="3">1</asp:TextBox>&nbsp;
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue" />
                        <asp:RangeValidator ID="rgvGo" runat="server" Type="Integer" MaximumValue="999999999"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" MinimumValue="1" ControlToValidate="txtpage"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                            Display="Dynamic" ControlToValidate="txtpage" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server" ForeColor="#999999"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server" ForeColor="#999999"></asp:Label>, Total
                        <asp:Label ID="lblRecord" runat="server" ForeColor="#999999"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="BtnAddNew" runat="server" Text="Add" CssClass="small button blue">
                </asp:Button>
            </div>
        </asp:Panel>
       
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblBranchID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    ID Biro Jasa</label>
                <asp:TextBox ID="txtAgentID" runat="server" MaxLength="20" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RevAgentId" runat="server" Font-Size="11px" ErrorMessage="Harap isi ID Biro Jasa"
                    ControlToValidate="txtAgentID" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Biro Jasa</label>
                <asp:TextBox ID="txtAgentName" runat="server" Width="458px" MaxLength="50" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Font-Size="11px"
                    ErrorMessage="Harap isi Nama Biro Jasa" ControlToValidate="txtAgentName" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>ALAMAT</strong>
                </label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucAddress id="oCompanyAddress" runat="server"></uc1:ucAddress>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>KONTAK</strong>
                </label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccontactperson id="oContactPerson" runat="server"></uc1:uccontactperson>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>REKENING BANK</strong>
                </label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucbankaccount id="oBankAccount" runat="server"></uc1:ucbankaccount>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Active</label>
                <asp:CheckBox ID="chkStatus" runat="server"></asp:CheckBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW BIRO JASA STNK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblViewBranch" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Biro Jasa</label>
                <asp:Label ID="lblAgentID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Biro Jasa</label>
                <asp:Label ID="lblViewAgentName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>ALAMAT</strong>
                </label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucviewaddress id="oViewAddress" runat="server"></uc1:ucviewaddress>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>KONTAK</strong>
                </label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucviewcontactperson id="oViewContactPerson" runat="server"></uc1:ucviewcontactperson>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>REKENING BANK</strong>
                </label>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucviewbankaccount id="oViewBankAccount" runat="server"></uc1:ucviewbankaccount>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Aktif</label>
                <asp:Label ID="lblIsActive" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
