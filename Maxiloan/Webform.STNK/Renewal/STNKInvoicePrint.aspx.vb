﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class STNKInvoicePrint
    Inherits Maxiloan.Webform.WebBased

#Region "Deklarasi dan constanta"
    Private m_controller As New DataUserControlController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.FormID = "PrintSTNKInv"

        If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
            If Not Page.IsPostBack Then
                Dim dtBranch As New DataTable
                dtBranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With cboParent
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"

                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    End If
                End With
            End If
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If

    End Sub

    Private Sub BtnViewReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies("STNKInvoice")
        '===============
        If CheckFeature(Me.Loginid, Me.FormID, "print", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                If Not cookie Is Nothing Then
                    cookie.Values("BranchID") = cboParent.SelectedItem.Value
                    cookie.Values("BranchName") = cboParent.SelectedItem.Text
                    cookie.Values("LoginID") = Me.Loginid
                    cookie.Values("InvoiceNo") = txtInvoiceNo.Text.Trim
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("STNKInvoice")
                    cookieNew.Values.Add("BranchID", cboParent.SelectedItem.Value)
                    cookieNew.Values.Add("BranchName", cboParent.SelectedItem.Text)
                    cookieNew.Values.Add("LoginID", Me.Loginid)
                    cookieNew.Values.Add("InvoiceNo", txtInvoiceNo.Text.Trim)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("STNKInvoicePrintViewer.aspx")
            End If
        End If
    End Sub

End Class