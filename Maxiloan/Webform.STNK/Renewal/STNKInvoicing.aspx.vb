﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKInvoicing
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property RenewalSeqNo() As String
        Get
            Return CType(ViewState("RenewalSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RenewalSeqNo") = Value
        End Set
    End Property
    Private Property AssetSeqNo() As String
        Get
            Return CType(ViewState("AssetSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetSeqNo") = Value
        End Set
    End Property
    Private Property Rate() As String
        Get
            Return CType(ViewState("Rate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Rate") = Value
        End Set
    End Property
    Private Property AdmFee() As String
        Get
            Return CType(ViewState("AdmFee"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AdmFee") = Value
        End Set
    End Property
    Private Property Req() As String
        Get
            Return (CType(ViewState("Req"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Req") = Value
        End Set
    End Property
    Private Property Tax() As Date
        Get
            Return CType(ViewState("Tax"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("Tax") = Value
        End Set
    End Property
    Private Property RequestTo() As String
        Get
            Return (CType(ViewState("RequestTo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("RequestTo") = Value
        End Set
    End Property
    Private Property AgentID() As String
        Get
            Return (CType(ViewState("AgentID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgentID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property agreementNo() As String
        Get
            Return (CType(ViewState("agreementNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("agreementNo") = Value
        End Set
    End Property
    Private Property customerName() As String
        Get
            Return (CType(ViewState("customerName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("customerName") = Value
        End Set
    End Property
    Private Property customerID() As String
        Get
            Return (CType(ViewState("customerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("customerID") = Value
        End Set
    End Property
    Private Property RequestDate() As Date
        Get
            Return (CType(ViewState("RequestDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("RequestDate") = Value
        End Set
    End Property
    Private Property taxRate() As String
        Get
            Return (CType(ViewState("taxRate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("taxRate") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oCustomClassAg As New Parameter.AgreementList
    Private oCustomClass As New Parameter.STNKRenewal
    Private oCustomClassGeneral As New Parameter.GeneralSetting
    Private oController As New STNKRenewalController
    Private oCommonController As New DataUserControlController
    Private oCustomClassAgency As New Parameter.Agency
    Private oControllerAgency As New AgencyController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("Applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.agreementNo = Request.QueryString("agreementno")
            Me.customerID = Request.QueryString("customerid")
            Me.customerName = Request.QueryString("name")
            Me.Req = Request.QueryString("requestdate")
            Me.RequestTo = Request.QueryString("requestno")
            Me.RenewalSeqNo = Request.QueryString("renewalseqno")
            Me.AssetSeqNo = Request.QueryString("assetseqno")

            GetInformation()
            txtValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        End If
    End Sub
    Private Sub GetInformation()
        lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
        HypRequestNo.Text = Me.RequestTo
        Me.RequestDate = CDate(Me.Req)
        HypRequestDate.Text = CType(Me.RequestDate, String)
        lblAgreementNo.Text = Me.agreementNo
        lblCustomerName.Text = Me.customerName
        lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.customerID.Trim) & "')"
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.WhereCond = "ApplicationID='" & Me.ApplicationID & "' And   RenewalSeqNo='" & Me.RenewalSeqNo & "' And AssetSeqNo='" & Me.AssetSeqNo & "'  and RequestNo='" & Me.RequestTo & "'"
        oCustomClass = oController.GetSTNKRequest(oCustomClass)
        With oCustomClass
            lblAssetDescription.Text = .AssetDescription
            lblAssetType.Text = .AssetType
            lblChassisNo.Text = .SerialNo1
            lblEngineNo.Text = .SerialNo2
            lblManufacturing.Text = .ManufacturingYear
            lblColor.Text = .Color
            If .OwnerName = "" Then lblName.Text = "-" Else lblName.Text = .OwnerName
            If .OwnerAddress = "" Then lblAddress.Text = "-" Else lblAddress.Text = .OwnerAddress
            lblLicensePlate.Text = .LicensePlate

            If .TaxDate = "-" Then
                lblSTNKDate.Text = ""
            Else
                Me.Tax = CDate(.TaxDate)
                lblSTNKDate.Text = Me.Tax.ToString
            End If
            lblSTNKAgent.Text = .AgentName
            lblReffNo.Text = .ReffNo            

            Dim oCustomClassGeneral As New Parameter.GeneralSetting
            Dim oCommonController As New DataUserControlController
            Dim oCustomClass As New Parameter.STNKRenewal

            Dim AdmFee As String
            oCustomClass.strConnection = GetConnectionString()
            oCustomClass.BranchId = .BranchId

            If (.CustReq.ToUpper = "U") Then
                oCustomClassGeneral.GSID = "STNKADMFEE"
            ElseIf (.CustReq.ToUpper = "K") Then
                oCustomClassGeneral.GSID = "STNKKTRFEE"
            End If

            If (.CustReq.ToUpper = "-") Then
                AdmFee = "0"
            Else
                AdmFee = oCommonController.GetGeneralSetting(oCustomClass.strConnection, oCustomClassGeneral)
            End If
            lblAdminFee.Text = FormatNumber(AdmFee, 0)

            Me.Rate = CStr(.TaxRate)
        End With        

        oCustomClassGeneral.GSID = "PPH23RATE"
        Me.Rate = oCommonController.GetGeneralSetting(oCustomClass.strConnection, oCustomClassGeneral)
        hdnTax.Value = FormatNumber(Me.Rate, 2)
        lblPph.Text = FormatNumber(Me.Rate, 0)

    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click

        If txtValueDate.Text <> "" Then
            'If IsNumeric(hdnTotalAP2.Value) Then
            '    If CDbl(hdnTotalAP2.Value) <= 0 Then
            '        ShowMessage(lblMessage, "Total A/P harus > 0", True)
            '        Exit Sub
            '    End If
            'Else
            '    ShowMessage(lblMessage, "Total A/P harus > 0", True)
            '    Exit Sub
            'End If
            If IsNumeric(txtTotalAP.Text) Then
                If CDbl(txtTotalAP.Text) <= 0 Then
                    ShowMessage(lblMessage, "Total A/P harus > 0", True)
                    Exit Sub
                End If
            Else
                ShowMessage(lblMessage, "Total A/P harus > 0", True)
                Exit Sub
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId.Replace("'", "")
                .ApplicationID = Me.ApplicationID
                .BusinessDate = Me.BusinessDate
                .RenewalSeqNo = CInt(Me.RenewalSeqNo)
                .AssetSeqNo = CInt(Me.AssetSeqNo)
                .STNKFee = CDbl(txtBBN.Text)
                .OtherFee = CDbl(txtOtherFee.Text)
                .AgentFee = CDbl(txtAgentFee.Text)
                '.TotalAP = CDbl(hdnTotalAP2.Value)
                .TotalAP = CDbl(txtTotalAP.Text)
                '.TaxAmount = CDbl(hdnTax2.Value)
                .TaxAmount = CDbl(txtTax.Text)
                .InvoiceNo = txtInvoice.Text
                .InvoiceDate = ConvertDate2(txtValueDate.Text)
                .TglRencanaBayar = ConvertDate2(TxtTglRencanaBayar.Text)
                .AdmFee = CDbl(lblAdminFee.Text)
            End With
            oCustomClass = oController.SaveSTNKInvoicing(oCustomClass)
        Else
            ShowMessage(lblMessage, "Harap isi tanggal", True)
        End If
            If oCustomClass.ErrorSave = True Then
                Response.Write("<script language='javascript'>alert('Error While Saving The Data!!');</script>")
            Else
                Response.Redirect("STNKInvoicingList.aspx")
            End If
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("STNKInvoicingList.aspx")
    End Sub

End Class