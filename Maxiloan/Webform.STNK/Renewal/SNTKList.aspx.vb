﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class SNTKList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController

    Protected WithEvents txtPeriod1 As ucDateCE
    Protected WithEvents txtPeriod2 As ucDateCE
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If sessioninvalid() Then
        '    Exit Sub
        'End If
        If Not IsPostBack Then
            'Modifikasi Imron Hilangkan filter date
            'txtPeriod1.Attributes.Add("readOnly", "true")
            'txtPeriod2.Attributes.Add("readOnly", "true")
            'txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            'txtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                'strFileLocation = "../../../maxiloan.bvf.dev/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If

            Me.FormID = "STNKList"
            oSearchBy.ListData = "Name, Nama Konsumen-aa.ApplicationID,No Aplikasi-LicensePlate, No Polisi-AgreementNo,No Kontrak"
            oSearchBy.BindData()

            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            '    Me.SearchBy = ""
            '    Me.SortBy = ""
            '    Initialize()
            'End If
            pnlDatagrid.Visible = False
            DtgAgree.Visible = False
        End If
    End Sub

    Private Sub Initialize()
        pnlDatagrid.Visible = False
        DtgAgree.Visible = False
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spSTNKListPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        recordCount = oPaging.TotalRecords
        DtgAgree.DataSource = oPaging.ListData

        Try
            DtgAgree.DataBind()
        Catch exp As Exception
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()

            ShowMessage(lblMessage, exp.Message, True)
        End Try
        lblMessage.Visible = False
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("SNTKList.aspx")
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append("aa.branchid = '" & oBranch.BranchID & "' ")
        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " LIKE  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
        End If
        'Modifikasi Imron Hilangkan filter date
        'If txtPeriod1.Text.Trim <> "" And txtPeriod2.Text.Trim <> "" Then
        '    Me.SearchBy = Me.SearchBy & " and aa.taxDate between '" & ConvertDate2(txtPeriod1.Text.Trim).ToString("yyyyMMdd") & "'" & _
        '    " and '" & ConvertDate2(txtPeriod2.Text.Trim).ToString("yyyyMMdd") & "'"
        'End If

        Me.SearchBy = strSearch.ToString
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub


    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "SA"
                Dim lblApplicationid As HyperLink
                Dim lblAgreementNo As HyperLink
                Dim lblName As HyperLink
                Dim lblCustID As Label
                lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
                lblAgreementNo = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblAgreementNo"), HyperLink)
                lblName = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblName"), HyperLink)
                lblCustID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblCustID"), Label)
                oCustomClass.ApplicationID = lblApplicationid.Text
                oCustomClass.BranchId = oBranch.BranchID
                oCustomClass.Agreementno = lblAgreementNo.Text
                oCustomClass.CustomerName = lblName.Text
                oCustomClass.CustomerID = lblCustID.Text
                HttpContext.Current.Items("Receive") = oCustomClass
                Server.Transfer("STNKRequest.aspx")
        End Select
    End Sub

End Class