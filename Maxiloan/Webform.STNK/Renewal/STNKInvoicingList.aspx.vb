﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKInvoicingList
    Inherits Maxiloan.Webform.WebBased
#Region "Property"
    Private Property RequestNo() As String
        Get
            Return CType(ViewState("RequestNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RequestNo") = Value
        End Set
    End Property
    Private Property RequestTo() As String
        Get
            Return (CType(ViewState("RequestTo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("RequestTo") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property agreementNo() As String
        Get
            Return (CType(ViewState("agreementNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("agreementNo") = Value
        End Set
    End Property
    Private Property customerName() As String
        Get
            Return (CType(ViewState("customerName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("customerName") = Value
        End Set
    End Property
    Private Property customerID() As String
        Get
            Return (CType(ViewState("customerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("customerID") = Value
        End Set
    End Property
    Private Property RequestDate() As Date
        Get
            Return (CType(ViewState("RequestDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("RequestDate") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then

            Me.FormID = "STNKInvoicing"

            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            '    Me.SearchBy = ""
            '    Me.SortBy = ""
            Initialize()
            'End If
        End If
    End Sub

    Private Sub Initialize()
        pnlDatagrid.Visible = False
        DtgAgree.Visible = False
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spSTNKInvoicingPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        recordCount = oPaging.TotalRecords
        DtgAgree.DataSource = oPaging.ListData

        Try
            DtgAgree.DataBind()
        Catch exp As Exception
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
           ShowMessage(lblMessage, exp.Message, True)
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink
        Dim imbInvoicing As HyperLink
        Dim lblRequestNo As New HyperLink
        Dim lblRequestDate As New Label
        Dim lblRenewalSeqNo As New Label
        Dim lblAssetSeqNo As New Label
        Dim lblBranchID As New Label

        If e.Item.ItemIndex >= 0 Then
            imbInvoicing = CType(e.Item.FindControl("imbInvoicing"), HyperLink)
            lblRequestNo = CType(e.Item.FindControl("hyprequestno"), HyperLink)
            lblRequestDate = CType(e.Item.FindControl("lblRequestDate"), Label)
            lblRenewalSeqNo = CType(e.Item.FindControl("lblRenewalSeqNo"), Label)
            lblAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label)
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If

            With oCustomClass
                Me.RequestTo = lblRequestNo.Text
                Me.ApplicationID = lblApplicationid.Text
                Me.agreementNo = lblAgreementNo.Text
                Me.customerName = lblName.Text
                Me.customerID = lblCustID.Text
                Me.RequestDate = CType(lblRequestDate.Text, Date)
                Me.BranchID = lblBranchID.Text.Trim

            End With
            imbInvoicing.NavigateUrl = "STNKInvoicing.aspx?applicationid=" & Server.UrlEncode(lblApplicationid.Text.Trim) & _
                                       "&agreementNo=" & Server.UrlEncode(Me.agreementNo.Trim) & "&branchid=" & Me.BranchID & "&name=" & Server.UrlEncode(CStr(Me.customerName.Trim)) & _
                                       "&customerid=" & Server.UrlEncode(Me.customerID.Trim) & "&requestno=" & Server.UrlEncode(Me.RequestTo.Trim) & "&requestdate=" & CStr(Me.RequestDate) & "&renewalseqno=" & lblRenewalSeqNo.Text.Trim & "&assetseqno=" & lblAssetSeqNo.Text.Trim
            lblRequestNo.NavigateUrl = "javascript:OpenWinViewSTNKRequestNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.RequestTo.Trim) & "' ,'" & Server.UrlEncode(Me.BranchID.Trim) & "')"

        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("STNKInvoicingList.aspx")
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        pnlDatagrid.Visible = True
        If txtSearch.Text <> "" And cboSearch.SelectedItem.Value <> "0" And txtSTNKDate.Text.Trim <> "" Then
            Me.SearchBy = cboSearch.SelectedItem.Value & " LIKE '%" & _
                               txtSearch.Text & "%'" & "And aa.taxDate  =  '" & ConvertDate2(txtSTNKDate.Text.Trim).ToString("yyyyMMdd") & "'"
        ElseIf txtSearch.Text <> "" And cboSearch.SelectedItem.Value <> "0" Then
            Me.SearchBy = cboSearch.SelectedItem.Value & " LIKE '%" & _
                               txtSearch.Text & "%'"
        ElseIf txtSTNKDate.Text.Trim <> "" Then
            Me.SearchBy = "aa.taxDate  =  '" & ConvertDate2(txtSTNKDate.Text.Trim).ToString("yyyyMMdd") & "'"
        Else
            Me.SearchBy = ""
        End If
        If Me.SearchBy <> "" Then
            Me.SearchBy = Me.SearchBy & " And sr.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        Else
            Me.SearchBy = " sr.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        End If

        Me.SortBy = "aa.Applicationid"
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub


End Class