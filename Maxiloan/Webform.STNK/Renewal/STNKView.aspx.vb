﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKView
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private ReadOnly Property RequestTo() As String
        Get
            Return CType(Request.QueryString("RequestNo"), String)
        End Get
    End Property
    Private ReadOnly Property CSSStyle() As String
        Get
            Return Request("Style").ToString
        End Get
    End Property

    Private Property RenewalSeqNo() As String
        Get
            Return CType(ViewState("RenewalSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RenewalSeqNo") = Value
        End Set
    End Property
    Private Property AssetSeqNo() As String
        Get
            Return CType(ViewState("AssetSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetSeqNo") = Value
        End Set
    End Property
    Private Property Rate() As String
        Get
            Return CType(ViewState("Rate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Rate") = Value
        End Set
    End Property
    Private Property AdmFee() As String
        Get
            Return CType(ViewState("AdmFee"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AdmFee") = Value
        End Set
    End Property
    Private Property Req() As String
        Get
            Return (CType(Viewstate("Req"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("Req") = Value
        End Set
    End Property
    Private Property Tax() As Date
        Get
            Return CType(ViewState("Tax"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("Tax") = Value
        End Set
    End Property
    'Private Property RequestTo() As String
    '    Get
    '        Return (CType(Viewstate("RequestTo"), String))
    '    End Get
    '    Set(ByVal Value As String)
    '        Viewstate("RequestTo") = Value
    '    End Set
    'End Property
    Private Property AgentID() As String
        Get
            Return (CType(Viewstate("AgentID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AgentID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(Viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property agreementNo() As String
        Get
            Return (CType(Viewstate("agreementNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("agreementNo") = Value
        End Set
    End Property
    Private Property customerName() As String
        Get
            Return (CType(Viewstate("customerName"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("customerName") = Value
        End Set
    End Property
    Private Property customerID() As String
        Get
            Return (CType(Viewstate("customerID"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("customerID") = Value
        End Set
    End Property
    Private Property RequestDate() As Date
        Get
            Return (CType(Viewstate("RequestDate"), Date))
        End Get
        Set(ByVal Value As Date)
            Viewstate("RequestDate") = Value
        End Set
    End Property
    Private Property taxRate() As String
        Get
            Return (CType(Viewstate("taxRate"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("taxRate") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oCustomClassAg As New Parameter.AgreementList
    Private oCustomClass As New Parameter.STNKRenewal
    Private oCustomClassGeneral As New Parameter.GeneralSetting
    Private oController As New STNKRenewalController
    Private oCommonController As New DataUserControlController
    Private oCustomClassAgency As New Parameter.Agency
    Private oControllerAgency As New AgencyController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            ' Me.RequestTo = Request.QueryString("requestno")
            Me.BranchID = Request.QueryString("BranchID")
            GetInformation()
            
        End If
    End Sub
    Private Sub GetInformation()
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.WhereCond = "RequestNo='" & Me.RequestTo & "'"
        oCustomClass = oController.GetSTNKRequest(oCustomClass)
        With oCustomClass
            Me.agreementNo = .AgreementNo
            Me.customerName = .CustomerName
            Me.ApplicationID = .ApplicationID
            Me.customerID = .CustomerID
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            HypRequestNo.Text = Me.RequestTo
            lblAgreementNo.Text = Me.agreementNo
            lblCustomerName.Text = Me.customerName
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.customerID.Trim) & "')"

            HypRequestDate.Text = CType(.RequestDate, String)
            lblAssetDescription.Text = .AssetDescription
            lblAssetType.Text = .AssetType
            lblChassisNo.Text = .SerialNo1
            lblEngineNo.Text = .SerialNo2
            lblManufacturing.Text = .ManufacturingYear
            lblColor.Text = .Color
            If .OwnerName = "" Then lblName.Text = "-" Else lblName.Text = .OwnerName
            If .OwnerAddress = "" Then lblAddress.Text = "-" Else lblAddress.Text = .OwnerAddress
            lblLicensePlate.Text = .LicensePlate
            Me.Tax = CDate(.TaxDate)
            If .TaxDate = "-" Then lblSTNKDate.Text = "" Else lblSTNKDate.Text = CType(Me.Tax, String)
            lblSTNKAgent.Text = .AgentName
            lblReffNo.Text = .ReffNo
            lblDP.Text = FormatNumber(.DP, 2)
            lblStatus.Text = .txtSearch
            If .StatusDate = Nothing Then lblStatusDate.Text = "-" Else lblStatusDate.Text = CType(.StatusDate, String)
            If .InvoiceNo = "" Then lblInvoiceNo.Text = "-" Else lblInvoiceNo.Text = .InvoiceNo
            If .InvoiceDate = Nothing Then lblInvoiceDate.Text = "-" Else lblInvoiceDate.Text = CType(.InvoiceDate, String)
            lblSTNKFee.Text = FormatNumber(.STNKFee, 2)
            lblSTNKAgent.Text = .AgentName
            lblOtherFee.Text = FormatNumber(.OtherFee, 2)
            lblAgentFee.Text = FormatNumber(.AgentFee, 2)
            lblTaxFromAgent.Text = FormatNumber(.TaxRate, 2)
            lblTotalAP.Text = FormatNumber(.TaxAmount, 2)
            lblTotalSTNKFee.Text = FormatNumber(.APAmountToAgent, 2)
            lblValueDate.Text = CType(.StatusDate, String)
            If .RegisterNotes = "" Then lblNotes.Text = "-" Else lblNotes.Text = .RegisterNotes
            lblAdminFee.Text = FormatNumber(.AdmFee, 2)
            If .RequestNotes = "" Then lblStatusNotes.Text = "-" Else lblStatusNotes.Text = .RequestNotes

            oCustomClassGeneral.GSID = "PPH23RATE"
            Me.Rate = oCommonController.GetGeneralSetting(oCustomClass.strConnection, oCustomClassGeneral)
            lblPph.Text = FormatNumber(Me.Rate, 0)

        End With


    End Sub

End Class