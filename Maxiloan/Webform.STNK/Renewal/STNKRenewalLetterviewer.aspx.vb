﻿

#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller.DataUserControlController
#End Region

Public Class STNKRenewalLetterviewer
    Inherits Maxiloan.webform.WebBased

#Region "page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        GetCookies()
        BindReport()
    End Sub
#End Region

#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set

    End Property
    Private Property PageFormID() As String
        Get
            Return CType(ViewState("PageFormID"), String)
        End Get
        Set(ByVal PageFormID As String)
            ViewState("PageFormID") = PageFormID
        End Set

    End Property
#End Region

#Region "Sub & Function "
    Private Sub BindReport()
        GetCookies()
        Dim dsSTNKRenewalLetter As DataSet
        Dim cSTNKRenewalLetter As New GeneralPagingController
        Dim oSTNKRenewalLetter As New Parameter.GeneralPaging

        Dim objreport As STNKRenewalLetterRpt = New STNKRenewalLetterRpt
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        With oSTNKRenewalLetter
            .strConnection = GetConnectionString()
            .WhereCond = Me.cmdwhere
            .SpName = "spSTNKRenewalLetterGetPrint"
        End With
        oSTNKRenewalLetter = cSTNKRenewalLetter.GetReportWithParameterWhereCond(oSTNKRenewalLetter)
        dsSTNKRenewalLetter = oSTNKRenewalLetter.ListDataReport

        objreport.SetDataSource(dsSTNKRenewalLetter)

        CrystalReportViewer1.ReportSource = objreport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "STNKRenewalLetter.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objreport.ExportOptions.DestinationOptions = DiskOpts
        objreport.Export()
        objreport.Close()

        If PageFormID.Trim = "STNKRequest" Then
            Response.Redirect("SNTKList.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "STNKRenewalLetter")
        End If
        If PageFormID.Trim = "STNKRenewal" Then
            Response.Redirect("STNKRenewalLetter.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "STNKRenewalLetter")
        End If

    End Sub
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("STNKRenewalLetter")
        Me.cmdwhere = cookie.Values("cmdwhere")
        Me.Loginid = cookie.Values("LoginID")
        Me.PageFormID = cookie.Values("FormID")
    End Sub
#End Region

End Class
