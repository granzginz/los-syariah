﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKBillingList
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As ucBranchAll

#Region "Deklarasi dan constanta "
    Protected WithEvents PODateFrom As ValidDate
    Protected WithEvents PODateTo As ValidDate
    Private m_controller As New DataUserControlController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "REPORTBILLING"
        If CheckForm(Me.Loginid, Me.FormID, "Maxiloan") Then
            If Not IsPostBack Then
                Dim dtbranch As New DataTable
                Dim dtAssetType As New DataTable
                txtDateFrom.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtDateTo.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                dtAssetType = m_controller.GetAssetType(GetConnectionString)
                dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With cboBranch
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    End If
                End With
            End If
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub


    Private Sub BtnViewReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies("STNKBILLING")
        Me.SearchBy = "STNKRenewal.BranchID= '" & cboBranch.SelectedItem.Value & "'"
        Me.SearchBy = Me.SearchBy & " And (STNKRenewal.InvoiceDate between  '" & txtDateFrom.Text & "'" & " And " & txtDateTo.Text & "'" & " ) "
        '===============
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                If Not cookie Is Nothing Then
                    cookie.Values("PODateFrom") = ConvertDate2(txtDateFrom.Text.Trim).ToString("dd/MM/yyyy")
                    cookie.Values("PODateTo") = ConvertDate2(txtDateTo.Text.Trim).ToString("dd/MM/yyyy")
                    cookie.Values("BranchID") = cboBranch.SelectedItem.Value
                    cookie.Values("BranchName") = cboBranch.SelectedItem.Text
                    cookie.Values("LoginID") = Me.Loginid
                    cookie.Values("Filter") = Me.SearchBy
                    cookie.Values("ReportType") = "RptSTNKBilling"
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("STNKBILLING")
                    cookieNew.Values.Add("PODateFrom", txtDateFrom.Text.Trim)
                    cookieNew.Values.Add("PODateTo", txtDateTo.Text.Trim)
                    cookieNew.Values.Add("BranchID", cboBranch.SelectedItem.Value)
                    cookieNew.Values.Add("BranchName", cboBranch.SelectedItem.Text)
                    cookieNew.Values.Add("LoginID", Me.Loginid)
                    cookieNew.Values.Add("Filter", Me.SearchBy)
                    cookieNew.Values.Add("ReportType", "RptSTNKBilling")
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("STNKBillingViewer.aspx")
            End If
        End If
    End Sub

End Class