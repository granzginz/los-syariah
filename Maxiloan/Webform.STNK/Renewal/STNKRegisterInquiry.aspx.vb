﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKRegisterInquiry
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then

            Me.FormID = "STNKRequestInquiry"
            oSearchBy.ListData = "RequestNo,No Request-AgentName,BiroJasa STNK-ApplicationID,No Aplikasi-Agreementno, No Kontrak-Name, Nama Konsumen-Description,Nama Asset-LicensePlate, No Polisi"
            oSearchBy.BindData()

            Dim dtBranch As New DataTable
            dtBranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cboParent
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()

            End With
            
            pnlDatagrid.Visible = False
            DtgAgree.Visible = False
        End If
    End Sub

    Private Sub Initialize()
        pnlDatagrid.Visible = False
        DtgAgree.Visible = False
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        With oPaging
            .strConnection = GetConnectionString
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spSTNKRegisterInquiryPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        recordCount = oPaging.TotalRecords
        DtgAgree.DataSource = oPaging.ListData

        Try
            DtgAgree.DataBind()
        Catch exp As Exception
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
           ShowMessage(lblMessage, exp.Message, True)
        End Try
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ' ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink
        Dim lblRequestNo As New HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lblRequestNo = CType(e.Item.FindControl("hyprequestno"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
            If lblRequestNo.Text.Trim.Length > 0 Then
                lblRequestNo.NavigateUrl = "javascript:OpenWinViewSTNKRequestNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblRequestNo.Text.Trim) & "' ,'" & cboParent.SelectedValue & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("STNKRegisterInquiry.aspx")
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim strSearch As New StringBuilder
        If cboParent.SelectedValue.Trim <> "" Then
            strSearch.Append("aa.branchid = '" & cboParent.SelectedValue & "' ")
        End If

        If txtAging.Text.Trim <> "" Then
            strSearch.Append(" and datediff(day,sr.RequestDate,getdate()) >='" & txtAging.Text.Trim & "'")
        End If
        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append("And " & oSearchBy.ValueID & " Like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
        End If
        If txtSTNKDate.Text.Trim <> "" Then
            strSearch.Append(" and aa.taxDate = '" & ConvertDate2(txtSTNKDate.Text).ToString("yyyyMMdd") & "' ")
        End If
        If cboStatus.SelectedValue.Trim <> "0" Then
            strSearch.Append(" and status = '" & cboStatus.SelectedValue.Trim & "' ")
        End If
        Me.SearchBy = strSearch.ToString
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub


End Class