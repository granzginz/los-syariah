﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class PenambahanEstimasiBiayaSTNK
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As UcBranch

#Region "Constanta"
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClass As New Parameter.AgreementList
    Private oController As New AgreementListController

    Protected WithEvents txtPeriod1 As ucDateCE
    Protected WithEvents txtPeriod2 As ucDateCE
    Protected WithEvents txtEstimasiBiayaSTNK As ucNumberFormat
#End Region
#Region "Prop"
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal ApplicationID As String)
            ViewState("ApplicationID") = ApplicationID
        End Set
    End Property
    Private Property PageBranchId() As String
        Get
            Return CType(ViewState("PageBranchId"), String)
        End Get
        Set(ByVal PageBranchId As String)
            ViewState("PageBranchId") = PageBranchId
        End Set
    End Property
    Private Property Agreementno() As String
        Get
            Return CType(ViewState("Agreementno"), String)
        End Get
        Set(ByVal Agreementno As String)
            ViewState("Agreementno") = Agreementno
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal CustomerName As String)
            ViewState("CustomerName") = CustomerName
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal CustomerID As String)
            ViewState("CustomerID") = CustomerID
        End Set
    End Property
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ADDESTIMASISTNK"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then                
                oBranch.BranchID = sesBranchId.Replace("'", "")
                Me.FormID = "STNKList"
                oSearchBy.ListData = "agr.AgreementNo, No Kontrak-Name, Nama Konsumen-aa.ApplicationID,No Aplikasi-LicensePlate, No Polisi"
                oSearchBy.BindData()
                pnlDatagrid.Visible = False
                DtgAgree.Visible = False                
            End If
        End If
    End Sub

    Private Sub DoBind(ByVal strSort As String, ByVal strSearch As String)
        Dim intloop As Integer
        Dim hypID As HyperLink

        Dim oPaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = strSearch
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "spEstimasiBiayaSTNKPaging"
        End With

        oPaging = m_controller.GetGeneralPaging(oPaging)
        recordCount = oPaging.TotalRecords
        DtgAgree.DataSource = oPaging.ListData

        Try
            DtgAgree.DataBind()
        Catch exp As Exception
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()

            ShowMessage(lblMessage, exp.Message, True)
        End Try
        lblMessage.Visible = False
        PagingFooter()
        pnlList.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
        pnlDatagrid.Visible = True
        DtgAgree.Visible = True
    End Sub
#End Region
#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SortBy, Me.SearchBy)
            End If
        End If
    End Sub

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

        Dim lblApplicationid As HyperLink
        Dim lblCustID As Label
        Dim lblName As HyperLink
        Dim lblAgreementNo As HyperLink

        If e.Item.ItemIndex >= 0 Then
            lblApplicationid = CType(e.Item.FindControl("lblApplicationid"), HyperLink)
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            If lblAgreementNo.Text.Trim.Length > 0 Then
                lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If

            If lblApplicationid.Text.Trim.Length > 0 Then
                lblApplicationid.NavigateUrl = "javascript:OpenApplicationId('" & "AccMnt" & "', '" & Server.UrlEncode(lblApplicationid.Text.Trim) & "')"
            End If
            lblName = CType(e.Item.FindControl("lblName"), HyperLink)
            lblCustID = CType(e.Item.FindControl("lblCustID"), Label)
            If lblCustID.Text.Trim.Length > 0 Then
                lblName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustID.Text.Trim) & "')"
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
            DoBind(e.SortExpression, Me.SearchBy)
        Else
            Me.SortBy = e.SortExpression + " DESC"
            DoBind(e.SortExpression + " DESC", Me.SearchBy)
        End If

    End Sub

#End Region

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("PenambahanEstimasiBiayaSTNK.aspx")
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim strSearch As New StringBuilder
        strSearch.Append("aa.branchid = '" & oBranch.BranchID & "' ")
        If oSearchBy.Text.Trim <> "" Then
            strSearch.Append(" and " & oSearchBy.ValueID & " LIKE  '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%'")
        End If
        
        Me.SearchBy = strSearch.ToString
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        pnlDatagrid.Visible = True
        pnlList.Visible = True
        DoBind(Me.SortBy, Me.SearchBy)
    End Sub


    Private Sub DtgAgree_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgAgree.ItemCommand
        Select Case e.CommandName
            Case "ADD"
                With txtEstimasiBiayaSTNK
                    .RangeValidatorEnable = True
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorMinimumValue = "1"
                    .setErrMsg()
                End With

                Dim lblApplicationid As HyperLink
                Dim Agreementno As HyperLink
                Dim lblName As HyperLink
                Dim lblCustID As Label
                lblApplicationid = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblApplicationid"), HyperLink)
                Agreementno = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblAgreementNo"), HyperLink)
                lblName = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblName"), HyperLink)
                lblCustID = CType(DtgAgree.Items(e.Item.ItemIndex).FindControl("lblCustID"), Label)
                Me.ApplicationID = lblApplicationid.Text
                Me.PageBranchId = oBranch.BranchID
                Me.Agreementno = Agreementno.Text
                Me.CustomerName = lblName.Text
                Me.CustomerID = lblCustID.Text
                pnlAdd.Visible = True
                pnlList.Visible = False
                pnlDatagrid.Visible = False

                Dim oCustomClass As New Parameter.STNKRenewal
                Dim oCustomClassREQ As New STNKRenewalController
                lblNoKontrak.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
                lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
                oCustomClass.strConnection = GetConnectionString()
                oCustomClass.BranchId = Me.PageBranchId
                oCustomClass.WhereCond = "ApplicationID='" & Me.ApplicationID & "'"
                oCustomClass = oCustomClassREQ.GetSTNKRequest(oCustomClass)
                With oCustomClass
                    lblNoKontrak.Text = .AgreementNo
                    lblCustomerName.Text = .CustomerName
                    lblAssetDescription.Text = .AssetDescription
                    lblAssetType.Text = .AssetType
                    lblChassisNo.Text = .SerialNo1
                    lblEngineNo.Text = .SerialNo2
                    lblManufacturing.Text = .ManufacturingYear
                    lblColor.Text = .Color
                    If .OwnerName = "" Then lblName.Text = "-" Else lblName.Text = .OwnerName
                    If .OwnerAddress = "" Then lblAddress.Text = "-" Else lblAddress.Text = .OwnerAddress
                    lblLicensePlate.Text = .LicensePlate
                    lblEstimasiSebelumnya.Text = FormatNumber(.STNKFee, 0)
                    lblBiayaSudahDibayar.Text = FormatNumber(.STNKFeePaid, 0)
                End With
        End Select
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        Dim oCustomClass As New Parameter.STNKRenewal
        Dim StnkController As New STNKRenewalController
        Try
            With oCustomClass
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .STNKFee = CDec(txtEstimasiBiayaSTNK.Text)
            End With
            oCustomClass = StnkController.SaveEstimasiBiayaSTNK(oCustomClass)

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            pnlAdd.Visible = False
            pnlDatagrid.Visible = True
            pnlList.Visible = True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("PenambahanEstimasiBiayaSTNK.aspx")
    End Sub
End Class