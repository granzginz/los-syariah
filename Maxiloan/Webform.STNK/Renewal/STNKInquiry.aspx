﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STNKInquiry.aspx.vb" Inherits="Maxiloan.Webform.STNK.STNKInquiry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../Webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>STNKInquiry</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
         <div class="title_strip">
                </div>
            <div class="form_single">
                <h4>
                    CARI PERPANJANGAN STNK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
               <label class ="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" Display="Dynamic"
                    ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Tanggal STNK
                </label>
                <asp:TextBox runat="server" ID="txtSTNKDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtSTNKDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Aging &gt;=
                </label>
                <asp:TextBox ID="txtAging" runat="server" ></asp:TextBox>
                day(s)
            </div>
            <div class="form_right">
                <label>
                    Status
                </label>
                <asp:DropDownList ID="cboStatus" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="REQ">Request</asp:ListItem>
                    <asp:ListItem Value="REG">Register</asp:ListItem>
                    <asp:ListItem Value="INV">Invoice</asp:ListItem>
                    <asp:ListItem Value="CAN">Cancel</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
         
            <div class="form_box_uc">
                <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
            </div>
         
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDatagrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DAFTAR PERPANJANGAN STNK
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAgree" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                <HeaderStyle HorizontalAlign="Center" Width="9%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="9%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="9%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="9%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblApplicationid" Visible="false" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:HyperLink>
                                    <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustID" runat="server" Visible="false" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblCustomerType" runat="server" Visible="false" Text='<%#Container.DataItem("CustomerType")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="am.Description" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="LICENSEPLATE" HeaderText="NO POLISI">
                                <HeaderStyle HorizontalAlign="Center" Width="6%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="6%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblLicensePlate" runat="server" Text='<%#Container.DataItem("LicensePlate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="TaxDate" SortExpression="TaxDate" HeaderText="TGL STNK"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="Aging" HeaderText="AGING">
                                <HeaderStyle HorizontalAlign="Center" Width="4%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="4%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAging" runat="server" Text='<%#Container.DataItem("Aging")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Status" HeaderText="STS">
                                <HeaderStyle HorizontalAlign="Center" Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="3%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Container.DataItem("Status")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  Display="Dynamic"
                        Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        ControlToValidate="txtpage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general" 
                        Display="Dynamic" ErrorMessage="No Halaman Salah" ControlToValidate="txtpage"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    
    </form>
</body>
</html>
