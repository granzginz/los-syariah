﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STNKRequest.aspx.vb" Inherits="Maxiloan.Webform.STNK.STNKRequest" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>STNKRequest</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
     <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/';

        function HideTextBox(ddlId) {

            var ControlName = document.getElementById(ddlId.id);
            ShowAdminfee(ControlName.value);
            if (ControlName.value == "-") {
                document.getElementById('hidebiaya1').style.display = 'none';
                document.getElementById('hidebiaya2').style.display = 'none';
                document.getElementById('hideTotal').style.display = 'none';
                
            }
            if (ControlName.value == "K") {
                document.getElementById('hidebiaya1').style.display = 'none';
                document.getElementById('hidebiaya2').style.display = '';
                document.getElementById('hideTotal').style.display = '';

            }
            else if (ControlName.value == "U") {
                document.getElementById('hidebiaya1').style.display = '';
                document.getElementById('hidebiaya2').style.display = '';
                document.getElementById('hideTotal').style.display = '';
            }
            

        }

        $(document).ready(function () {

            var ControlName = document.getElementById('cboCustReq');
            ShowAdminfee(ControlName.value);
            if (ControlName.value == "-") {
                document.getElementById('hidebiaya1').style.display = 'none';
                document.getElementById('hidebiaya2').style.display = 'none';
                document.getElementById('hideTotal').style.display = 'none';
            }
            if (ControlName.value == "K") {
                document.getElementById('hidebiaya1').style.display = 'none';
                document.getElementById('hidebiaya2').style.display = '';
                document.getElementById('hideTotal').style.display = '';

            }
            else if (ControlName.value == "U") {
                document.getElementById('hidebiaya1').style.display = '';
                document.getElementById('hidebiaya2').style.display = '';
                document.getElementById('hideTotal').style.display = '';
            }
            perkiraanTotal();
            $("#LblCost_txtNumber").attr('disabled', 'disabled');
        });

        function ShowAdminfee(custreq) {

            $.ajax({
                type: "POST",
                url: "STNKRequest.aspx/GetAdminFee",
                data: '{custreq: "' + custreq + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function OnSuccess(response) {
            $('#lblAdminFee_txtNumber').val(response.d);
            perkiraanTotal();
        }




        function perkiraanTotal() {
            var ControlName = document.getElementById('cboCustReq');
            var adminfeeValue = $('#lblAdminFee_txtNumber').val().replace(/,/g, '');
            var pajakValue = $('#lblEstimasiPajak_txtNumber').val().replace(/,/g, '');
            var dendaValue = $('#lblEstimasiDenda_txtNumber').val().replace(/,/g, '');
            var jasaValue = $('#lblEstimasiBiroJasa_txtNumber').val().replace(/,/g, '');
            var biayalainValue = $('#lblEstimasiBiayaLain_txtNumber').val().replace(/,/g, '');

            var adminvalue = parseFloat(adminfeeValue);
            var pjvalue = parseFloat(pajakValue);
            var dnvalue = parseFloat(dendaValue);
            var jsvalue = parseFloat(jasaValue);
            var blvalue = parseFloat(biayalainValue);
      

           
            if (ControlName.value == "-") {
                var totalCost = parseFloat(0);
            }
            if (ControlName.value == "K") {
                var res = adminvalue+jsvalue + blvalue;

            }
            else if (ControlName.value == "U") {
                var res = adminvalue+pjvalue + dnvalue + jsvalue + blvalue;
            }


            $("#LblCost_txtNumber").val(number_format(res));

        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
    </script>
   
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENGAJUAN PERPANJANGAN STNK
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <strong>DATA REGISTRASI</strong>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jenis Asset
            </label>
            <asp:Label ID="lblAssetType" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Rangka
            </label>
            <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Mesin
            </label>
            <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tahun Produksi
            </label>
            <asp:Label ID="lblManufacturing" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Warna
            </label>
            <asp:Label ID="lblColor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama
            </label>
            <asp:Label ID="lblName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Alamat
            </label>
            <asp:Label ID="lblAddress" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Polisi
            </label>
            <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal STNK
            </label>
            <asp:Label ID="lblSTNKDate" runat="server"></asp:Label>
            <asp:TextBox runat="server" ID="txtuscDate" CssClass="small_text"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtuscDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jenis Jasa STNK
            </label>
            <asp:DropDownList ID="cboRenewalType" runat="server" AutoPostBack="false">
                <asp:ListItem Value="">Select One</asp:ListItem>
                <asp:ListItem Value="STNK">Perpanjangan STNK</asp:ListItem>
                <asp:ListItem Value="BBN">BBN</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvRenewalType" runat="server" Display="Dynamic"
                ControlToValidate="cboRenewalType" ErrorMessage="Harap Pilih Jenis Jasa STNK"
                InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Permintaan Konsumen
            </label>
            <asp:DropDownList ID="cboCustReq" runat="server" AutoPostBack="false" onchange="HideTextBox(this);">
                <asp:ListItem Value="-">Select One</asp:ListItem>
                <asp:ListItem Value="U">Pengurusan</asp:ListItem>
                <asp:ListItem Value="K">Surat Keterangan</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvCustReq" runat="server" Display="Dynamic" ControlToValidate="cboCustReq"
                ErrorMessage="Harap Pilih Permintaan Konsumen" InitialValue="-" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div id="hidebiaya1">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Perkiraan Pajak
                </label>
                <uc1:ucnumberformat runat="server" id="lblEstimasiPajak" onchange="perkiraanTotal();"></uc1:ucnumberformat>
            </div>
            <div class="form_right">
                <label>
                    Ta'widh Keterlambatan
                </label>
                <uc1:ucnumberformat runat="server" id="lblEstimasiDenda" onchange="perkiraanTotal();"></uc1:ucnumberformat>
            </div>
        </div>
    </div>
    <div id="hidebiaya2">
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Biro Jasa
                </label>
                <uc1:ucnumberformat runat="server" id="lblEstimasiBiroJasa" onchange="perkiraanTotal();"></uc1:ucnumberformat>
            </div>
            <div class="form_right">
                <label>
                    Biaya Lainnya
                </label>
                <uc1:ucnumberformat runat="server" id="lblEstimasiBiayaLain" onchange="perkiraanTotal();"></uc1:ucnumberformat>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label class="label_req">
                Biaya Administrasi
            </label>
            <uc1:ucnumberformat runat="server" id="lblAdminFee" onchange="perkiraanTotal();"></uc1:ucnumberformat>
            <%--<asp:TextBox ID="lblAdminFee"  runat="server"></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ErrorMessage="Harap isi dengan angka"
                ControlToValidate="lblAdminFee" MinimumValue="0" Type="Double" MaximumValue="99999999999999"></asp:RangeValidator>
            <asp:RequiredFieldValidator ID="rvfDownPayment" runat="server" Display="Dynamic"
                ErrorMessage="Harap isi Biaya Administrasi" ControlToValidate="lblAdminFee"></asp:RequiredFieldValidator>--%>
        </div>
        <div class="form_right" id="hideTotal" style = 'display:none'>
            <label>
                Perkiraan Total
            </label>
            <uc1:ucnumberformat runat="server" id="LblCost"></uc1:ucnumberformat>
         
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_general">
                Catatan
            </label>
            <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnRequest" CausesValidation="True" runat="server" Text="Request"
            CssClass="small button blue"></asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
