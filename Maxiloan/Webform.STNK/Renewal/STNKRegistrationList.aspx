﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STNKRegistrationList.aspx.vb"
    Inherits="Maxiloan.Webform.STNK.STNKRegistrationList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../webform.UserController/UcSearchBy.ascx" %>
<%@ Register Src="../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>STNKRegistrationList</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
    <div class="form_title">
            <div class="form_single">
                <h4>
                    REGISTRASI STNK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tanggal STNK
                </label>
                <uc2:ucdatece id="txtPeriod1" runat="server" />
                &nbsp; S/D
                <uc2:ucdatece id="txtPeriod2" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="0">Select one</asp:ListItem>
                    <asp:ListItem Value="sr.RequestNo">No Request</asp:ListItem>
                    <asp:ListItem Value="am.Description">Nama Asset</asp:ListItem>
                    <asp:ListItem Value="aa.LicensePlate">No Polisi</asp:ListItem>
                    <asp:ListItem Value="agr.AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Cust.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" Width="123px" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
        <asp:Panel ID="pnlDatagrid" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR PERPANJANGAN STNK
                    </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgAgree" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="SortGrid" DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0"
                            CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="imbRegister" runat="server" Text="REGISTER"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CANCEL">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="4%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="imbCancel" runat="server" Text="CANCEL"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="RequestNo" HeaderText="NO REQUEST">
                                    <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hypRequestNo" runat="server" Text='<%#Container.DataItem("RequestNo")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <HeaderStyle HorizontalAlign="Center" Width="11%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="11%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationid" Visible="false" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                        </asp:HyperLink>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle HorizontalAlign="Center" Width="24%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="24%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblName" runat="server" Text='<%#Container.DataItem("Name")%>'>
                                        </asp:HyperLink>
                                        <asp:Label ID="lblCustID" runat="server" Visible="false" Text='<%#Container.DataItem("CustomerId")%>'>
                                        </asp:Label>
                                        <asp:Label ID="lblRequestDate" runat="server" Visible="false" Text='<%#Container.DataItem("RequestDate")%>'>
                                        </asp:Label>
                                        <asp:Label ID="lblCustomerType" runat="server" Visible="false" Text='<%#Container.DataItem("CustomerType")%>'>
                                        </asp:Label>
                                        <asp:Label ID="lblBranchID" runat="server" Visible="false" Text='<%#Container.DataItem("BranchID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="am.DESCRIPTION" HeaderText="NAMA ASSET">
                                    <HeaderStyle HorizontalAlign="Center" Width="30%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescription" runat="server" Text='<%#Container.DataItem("Description")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="LICENSEPLATE" HeaderText="NO POLISI">
                                    <HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="7%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblLicensePlate" runat="server" Text='<%#Container.DataItem("LicensePlate")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="TaxDate" SortExpression="AA.TaxDate" HeaderText="TGL STNK"
                                    DataFormatString="{0:dd/MM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                </asp:BoundColumn>
                            </Columns>
                            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                                Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CommandName="First" OnCommand="NavigationLink_Click"
                            ImageUrl="../../Images/grid_navbutton01.png" CausesValidation="False"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CommandName="Prev" OnCommand="NavigationLink_Click"
                            ImageUrl="../../Images/grid_navbutton02.png" CausesValidation="False"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CommandName="Next" OnCommand="NavigationLink_Click"
                            ImageUrl="../../Images/grid_navbutton03.png" CausesValidation="False"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CommandName="Last" OnCommand="NavigationLink_Click"
                            ImageUrl="../../Images/grid_navbutton04.png" CausesValidation="False"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvgo" runat="server"  CssClass="validator_general"  Display="Dynamic"
                            Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1"
                            ControlToValidate="txtpage"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" 
                         CssClass="validator_general"  ErrorMessage="No Halaman Salah" ControlToValidate="txtpage"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </asp:Panel>
        
    </form>
</body>
</html>
