﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region



Public Class STNKInvoicePrintViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"

    Private Property InvoiceNo() As String
        Get
            Return CType(viewstate("InvoiceNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InvoiceNo") = Value
        End Set
    End Property
#End Region

#Region "Deklarasi dan konstanta"
    Private m_controller As New GeneralPagingController
    Private oCustomClass As New Parameter.GeneralPaging
    Private dtSuspendDate As Date
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
    Sub GetCookies()

        Dim cookie As HttpCookie = Request.Cookies("STNKInvoice")
        Me.BranchID = cookie.Values("BranchID")
        Me.BranchName = cookie.Values("BranchName")
        Me.InvoiceNo = cookie.Values("InvoiceNo")


    End Sub

    Sub BindReport()

        GetCookies()
        Dim dsAssetDocRep As New DataSet
        Dim PrintAssetDoc As New STNKInvoice
        Dim strStatusDateFrom As String = ""
        Dim strStatusDateTo As String = ""
        Dim dtStatusDateFrom As Date
        Dim dtStatusDateTo As Date

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = "Ag.BranchID='" & Me.BranchID & "' And InvoiceNo='" & Me.InvoiceNo & "'"
            .SortBy = "AgreementNo"
            .SpName = "spSTNKInvoicePrint"
        End With

        oCustomClass = m_controller.GetReportWithParameterWhereAndSort(oCustomClass)
        dsAssetDocRep = oCustomClass.ListDataReport
        Try
            PrintAssetDoc.SetDataSource(dsAssetDocRep.Tables(0))
        Catch ex As Exception
            'Response.Write(ex.Message.ToString)
        End Try
        CrystalReportViewer1.ReportSource = PrintAssetDoc
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.EnableDrillDown = False

        CrystalReportViewer1.DataBind()


        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("BranchName")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.BranchName
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)


        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("CompanyName")
        discrete = New ParameterDiscreteValue
        discrete.Value = "PT MAXIMA INTI FINANCE"
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("ReportID")
        discrete = New ParameterDiscreteValue
        discrete.Value = "rptInvoice"
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = PrintAssetDoc.DataDefinition.ParameterFields("InvoiceNo")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.InvoiceNo
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)


    End Sub
    'Private Sub imbBack_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
    '    Response.Redirect("STNKInvoicePrint.aspx")
    'End Sub
End Class