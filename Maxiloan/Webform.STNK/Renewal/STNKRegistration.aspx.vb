﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKRegistration
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtAdminFee As ucNumberFormat

#Region "Property"
    Private Property Tax() As Date
        Get
            Return CType(ViewState("Tax"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("Tax") = Value
        End Set
    End Property
    Private Property AdmFee() As String
        Get
            Return CType(ViewState("AdmFee"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AdmFee") = Value
        End Set
    End Property
    Private Enum ProcessMode
        Cancel = 0
        Execute = 1
        View = 2
    End Enum
    Private Property Mode() As ProcessMode
        Get
            Return (CType(ViewState("Mode"), ProcessMode))
        End Get
        Set(ByVal Value As ProcessMode)
            ViewState("Mode") = Value
        End Set
    End Property
    Private Property RequestTo() As String
        Get
            Return (CType(ViewState("RequestTo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("RequestTo") = Value
        End Set
    End Property
    Private Property AgentID() As String
        Get
            Return (CType(ViewState("AgentID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgentID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property agreementNo() As String
        Get
            Return (CType(ViewState("agreementNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("agreementNo") = Value
        End Set
    End Property
    Private Property customerName() As String
        Get
            Return (CType(ViewState("customerName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("customerName") = Value
        End Set
    End Property
    Private Property customerID() As String
        Get
            Return (CType(ViewState("customerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("customerID") = Value
        End Set
    End Property
    Private Property RequestDate() As Date
        Get
            Return (CType(ViewState("RequestDate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("RequestDate") = Value
        End Set
    End Property
    Private Property Req() As String
        Get
            Return (CType(ViewState("Req"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Req") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oCustomClassAg As New Parameter.AgreementList
    Private oCustomClass As New Parameter.STNKRenewal
    Private oCustomClassGeneral As New Parameter.GeneralSetting
    Private oController As New STNKRenewalController
    Private oCommonController As New DataUserControlController
    Private oCustomClassAgency As New Parameter.Agency
    Private oControllerAgency As New AgencyController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '  If CheckFeature(Me.Loginid, Me.FormID, "REQ", Me.AppId) Then
        If Not IsPostBack Then
            Me.ApplicationID = Request.QueryString("Applicationid")
            Me.BranchID = Request.QueryString("branchid")
            Me.agreementNo = Request.QueryString("agreementno")
            Me.customerID = Request.QueryString("customerid")
            Me.customerName = Request.QueryString("name")
            Me.Req = Request.QueryString("requestdate")
            Me.RequestTo = Request.QueryString("requestno")

            Select Case Request.QueryString("mode")
                Case "0"
                    Me.Mode = ProcessMode.Cancel
                Case "1"
                    Me.Mode = ProcessMode.Execute
                Case "2"
                    Me.Mode = ProcessMode.View
            End Select

            Dim dtAgency As New DataTable

            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID

            End With
            dtAgency = oController.STNKView(oCustomClass.strConnection, Me.sesBranchId.Replace("'", ""))
            With cboAgent
                .DataSource = dtAgency.DefaultView
                .DataValueField = "AgentID"
                .DataTextField = "AgentName"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With
            InitializeForm()
            GetInformation()
            txtValueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        End If

    End Sub
    Private Sub GetInformation()
        lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
        HypRequestNo.Text = Me.RequestTo
        Me.RequestDate = CDate(Me.Req)
        HypRequestDate.Text = CType(Me.RequestDate, String)
        lblAgreementNo.Text = Me.agreementNo
        lblCustomerName.Text = Me.customerName
        lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.customerID.Trim) & "')"
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.WhereCond = "ApplicationID='" & Me.ApplicationID & "' and RequestNo='" & Me.RequestTo & "'"
        oCustomClass = oController.GetSTNKRequest(oCustomClass)
        With oCustomClass
            lblAssetDescription.Text = .AssetDescription
            lblAssetType.Text = .AssetType
            lblChassisNo.Text = .SerialNo1
            lblEngineNo.Text = .SerialNo2
            lblManufacturing.Text = .ManufacturingYear
            lblColor.Text = .Color
            If .OwnerName = "" Then lblName.Text = "-" Else lblName.Text = .OwnerName
            If .OwnerAddress = "" Then lblAddress.Text = "-" Else lblAddress.Text = .OwnerAddress
            lblLicensePlate.Text = .LicensePlate

            If .TaxDate = "-" Then
                lblSTNKDate.Text = ""
            Else
                Me.Tax = CDate(.TaxDate)
                lblSTNKDate.Text = Me.Tax.ToString
            End If

            Dim oCustomClassGeneral As New Parameter.GeneralSetting
            Dim oCommonController As New DataUserControlController
            Dim oCustomClass As New Parameter.STNKRenewal

            Dim AdmFee As String
            oCustomClass.strConnection = GetConnectionString()
            oCustomClass.BranchId = .BranchId

            If (.CustReq.ToUpper = "U") Then
                oCustomClassGeneral.GSID = "STNKADMFEE"
            ElseIf (.CustReq.ToUpper = "K") Then
                oCustomClassGeneral.GSID = "STNKKTRFEE"
            End If

            If (.CustReq.ToUpper = "-") Then
                AdmFee = "0"
            Else
                AdmFee = oCommonController.GetGeneralSetting(oCustomClass.strConnection, oCustomClassGeneral)
            End If
            txtAdminFee.Text = FormatNumber(AdmFee, 0)
        End With        
    End Sub

    Private Sub InitializeForm()
        If Me.Mode = ProcessMode.Cancel Then
            pnlRgister.Visible = False
        ElseIf Me.Mode = ProcessMode.Execute Then
            pnlRgister.Visible = True
        ElseIf Me.Mode = ProcessMode.View Then
            pnlRgister.Visible = False
        End If
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        'If oValueDate.dateValue = "" Or oValueDate.dateValue Is Nothing Or txtDP.Text = "" Then
        '    lblMessage.Text = "Fill Down Payment"
        'Else
        Try
            Me.AgentID = cboAgent.SelectedItem.Value.Trim
            With oCustomClass
                .strConnection = GetConnectionString()
                .BranchId = Me.sesBranchId
                .ApplicationID = Me.ApplicationID
                .RequestNo = Me.RequestTo
                .AgentID = Me.AgentID
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .AdmFee = CDbl(txtAdminFee.Text)
            End With
            If Me.Mode = ProcessMode.Execute Then
                With oCustomClass
                    .ValueDate = ConvertDate2(txtValueDate.Text)
                    .ReffNo = txtReferenceNo.Text
                    .DP = 0
                    .Notes = txtNotes.Text
                End With
                oCustomClass = oController.SaveSTNKRegister(oCustomClass)
            Else
                oCustomClass = oController.CancelSTNKRegister(oCustomClass)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        'End If
        If oCustomClass.ErrorSave = True Then
            Response.Write("<script language='javascript'>alert('Error While Saving The Data!!');</script>")
        Else
            Response.Redirect("STNKRegistrationList.aspx")
        End If
    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("STNKRegistrationList.aspx")
    End Sub

End Class