﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STNKRenewalIncome.aspx.vb"
    Inherits="Maxiloan.Webform.STNK.STNKRenewalIncome" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>STNKRenewalIncome</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    PENDAPATAN PERPANJANGAN STNK PER PERIODE
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    Cabang
                </label>
                <asp:DropDownList ID="cboParent" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboParent"
                    ErrorMessage="Harap pilih Cabang" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Periode
                </label>
                <asp:DropDownList ID="cboBulan" runat="server">
                    <asp:ListItem Value="0">SelectOne</asp:ListItem>
                    <asp:ListItem Value="01">Januari</asp:ListItem>
                    <asp:ListItem Value="02">Februari</asp:ListItem>
                    <asp:ListItem Value="03">Maret</asp:ListItem>
                    <asp:ListItem Value="04">April</asp:ListItem>
                    <asp:ListItem Value="05">Mei</asp:ListItem>
                    <asp:ListItem Value="06">Juni</asp:ListItem>
                    <asp:ListItem Value="07">Juli</asp:ListItem>
                    <asp:ListItem Value="08">Agustus</asp:ListItem>
                    <asp:ListItem Value="09">September</asp:ListItem>
                    <asp:ListItem Value="10">Oktober</asp:ListItem>
                    <asp:ListItem Value="11">November</asp:ListItem>
                    <asp:ListItem Value="12">Desember</asp:ListItem>
                </asp:DropDownList>
                /
                <asp:TextBox ID="txtYear" runat="server" Columns="7" MaxLength="4" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVYear" runat="server" ControlToValidate="txtYear"
                    ErrorMessage="Harap pilih Periode" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtYear"
                    ErrorMessage="Tahun Salah" Display="Dynamic" ValidationExpression="\d{4}"></asp:RegularExpressionValidator>&nbsp;
                <asp:RequiredFieldValidator ID="rfvBulan" runat="server" ControlToValidate="cboBulan"
                    ErrorMessage="Harap pilih Bulan" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" CausesValidation="True" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
