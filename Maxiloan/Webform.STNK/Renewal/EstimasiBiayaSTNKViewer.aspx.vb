﻿Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller


Public Class EstimasiBiayaSTNKViewer
    Inherits Maxiloan.Webform.WebBased

    Private oCustomClass As New Parameter.STNKRenewal

    Private oController As New STNKRenewalController

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property BranchID() As String
        Get
            Return CType(ViewState("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property


#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
        CreateData()
    End Sub
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet


      

        Dim objReport As EstimasiBiayaSTNK = New EstimasiBiayaSTNK
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With
        oData = oController.ReportSTNKRenewal(oCustomClass)
        objReport.SetDataSource(oData)
        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "EstimasiBiayaSTNK.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("SNTKList.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "EstimasiBiayaSTNK")

    End Sub
#End Region
#Region "CreateData"
    Private Sub CreateData()


        Dim oData As New DataSet
     

        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With
        oData = oController.ReportSTNKRenewal(oCustomClass)
        oData.WriteXmlSchema("D:\AFISCM\Maxiloan\Webform.STNK\Print\EstimasiBiayaSTNK.xml")
    End Sub
#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptConfLetter")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.BranchID = cookie.Values("BranchID")
    End Sub
#End Region
End Class