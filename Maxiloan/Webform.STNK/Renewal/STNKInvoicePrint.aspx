﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STNKInvoicePrint.aspx.vb"
    Inherits="Maxiloan.Webform.STNK.STNKInvoicePrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>STNKInvoicePrint</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INVOICE STNK
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
           <label class ="label_req">
                Cabang</label>
            <asp:DropDownList ID="cboParent" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" Display="Dynamic"
                ErrorMessage="Harap pilih Cabang" ControlToValidate="cboParent"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
             <label class ="label_req">
                No Invoice</label>
            <asp:TextBox ID="txtInvoiceNo" runat="server" Width="176px" ></asp:TextBox><asp:Label
                ID="txtSignYear" runat="server">
            </asp:Label><asp:RequiredFieldValidator ID="rfvPeriodYear" runat="server" Display="Dynamic"
                ErrorMessage="Harap Isi No Invoice" ControlToValidate="txtInvoiceNo"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="BtnViewReport" runat="server" CausesValidation="True" Enabled="True"
            Text="View" CssClass="small button blue"></asp:Button>
    </div>
    </form>
</body>
</html>
