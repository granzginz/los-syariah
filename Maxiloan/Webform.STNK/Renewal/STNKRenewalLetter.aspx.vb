﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKRenewalLetter
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As UcBranchAll

#Region "Declaration & Const"
    Private currentPage As Integer = 1
    Private pageSize As Byte = 20
    Private recordCount As Int64 = 1
    Private m_updateSucces As Boolean = False
#End Region

#Region "Property "
    Private Property SelectMode() As Boolean
        Get
            Return (CType(viewstate("SelectMode"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            viewstate("SelectMode") = Value
        End Set
    End Property
    Property WhereCondPrint() As String
        Get
            Return CType(viewstate("WhereCondPrint"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("WhereCondPrint") = Value
        End Set
    End Property

#End Region


#Region "Private Sub & Function"
    Private Sub SearchSTNKRenewalLetter()
        Dim oSTNKRenewalLetterpaging As New Parameter.GeneralPaging
        Dim m_controller As New GeneralPagingController
        With oSTNKRenewalLetterpaging
            .strConnection = GetConnectionString
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .SpName = "spSTNKRenewalLetterSearch"
        End With
        oSTNKRenewalLetterpaging = m_controller.GetGeneralPaging(oSTNKRenewalLetterpaging)
        recordCount = oSTNKRenewalLetterpaging.TotalRecords
        DtgAgree.DataSource = oSTNKRenewalLetterpaging.ListData
        Try
            DtgAgree.DataBind()
        Catch exp As Exception
            DtgAgree.CurrentPageIndex = 0
            DtgAgree.DataBind()
          ShowMessage(lblMessage, exp.Message, True)
            Exit Sub
        End Try
        lblPage.Text = CType(currentPage, String).Trim
        lblTotPage.Text = Math.Ceiling(recordCount / pageSize).ToString
        lblrecord.Text = CType(recordCount, String).Trim
        pnlList.Visible = True
        pnlDatagrid.Visible = True
    End Sub
    Private Function BindLastDate() As String
        Dim cLastDate As New STNKRenewalController
        Dim strLastDate As String
        strLastDate = cLastDate.GetLastDate(GetConnectionString)
        Return strLastDate.Trim
    End Function
    Private Sub BindAgreementCheked()
        Dim dttDataList As New DataTable
        Dim dtrDataList As DataRow

        With dttDataList.Columns
            .Add("BranchID")
            .Add("ApplicationID")
        End With

        Dim dtgRecordsCount As Integer
        Dim ocbTemp As CheckBox
        Dim oBranchID As Literal
        Dim oApplicationId As Literal
        Dim counter As Integer
        Dim strBranchID As String
        Dim strApplicationID As String
        WhereCondPrint = ""
        strBranchID = " "
        strApplicationID = " "
        Dim FistBoolean As Boolean = True

        dtgRecordsCount = DtgAgree.Items.Count - 1
        For counter = 0 To dtgRecordsCount
            With DtgAgree.Items(counter)
                ocbTemp = DirectCast(.FindControl("cbItem"), CheckBox)
                If ocbTemp.Checked Then
                    dtrDataList = dttDataList.NewRow
                    oBranchID = DirectCast(.FindControl("ltlBranchId"), Literal)
                    oApplicationId = DirectCast(.FindControl("ltlApplicationId"), Literal)
                    dtrDataList("BranchID") = oBranchID.Text.Trim
                    dtrDataList("ApplicationID") = oApplicationId.Text.Trim
                    If FistBoolean Then
                        strBranchID = "'" & oBranchID.Text.Trim
                        strApplicationID = " '" & oApplicationId.Text.Trim
                        FistBoolean = False
                    Else
                        strBranchID = strBranchID & "','" & oBranchID.Text.Trim
                        strApplicationID = strApplicationID & "','" & oApplicationId.Text.Trim
                    End If
                    dttDataList.Rows.Add(dtrDataList)
                End If
            End With
        Next
        If FistBoolean Then
            Exit Sub
        End If

        WhereCondPrint = " And Agreement.BranchID in (" & strBranchID.Trim & "') and Agreement.ApplicationID in (" & strApplicationID.Trim & "')"


        'Update Data
        '==============
        Dim oUpdatePrint As New Parameter.STNKRenewal
        Dim controller As New STNKRenewalController

        With oUpdatePrint
            .strConnection = GetConnectionString
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid
            .listData = dttDataList
        End With

        m_updateSucces = controller.STNKRenewalUpdatePrint(oUpdatePrint)

        '=========PrintData
        If m_updateSucces Then

            Dim cookie As HttpCookie = Request.Cookies("STNKRenewalLetter")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = Me.WhereCondPrint
                cookie.Values("LoginID") = Me.Loginid
                cookie.Values("FormID") = "STNKRenewal"
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("STNKRenewalLetter")
                cookieNew.Values.Add("cmdwhere", Me.WhereCondPrint)
                cookieNew.Values.Add("LoginID", Me.Loginid)
                cookieNew.Values.Add("FormID", "STNKRenewal")
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("STNKRenewalLetterviewer.aspx")
        Else
            ShowMessage(lblMessage, "Update data gagal", True)
        End If

    End Sub
    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkItem As CheckBox
        Dim x As Integer
        If Me.SelectMode = True Then
            For x = 0 To DtgAgree.Items.Count - 1
                chkItem = CType(DtgAgree.Items(x).FindControl("cbItem"), CheckBox)
                chkItem.Checked = True
            Next
            Me.SelectMode = False
        Else
            For x = 0 To DtgAgree.Items.Count - 1
                chkItem = CType(DtgAgree.Items(x).FindControl("cbItem"), CheckBox)
                chkItem.Checked = False
            Next
            Me.SelectMode = True
        End If
    End Sub
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then

            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                'strFileLocation = "../../../maxiloan.bvf.dev/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                'Call file PDF 

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If

            Dim oCheked As CheckBox
            Me.FormID = "STNKRenewalLetter"
            pnlDatagrid.Visible = False
            Me.SortBy = ""
            Me.SearchBy = ""
            txtSTNKDate.Text = CDate(Me.BusinessDate).ToString("dd/MM/yyyy")
            Me.SelectMode = True
        End If
    End Sub
#End Region

#Region "Panel Initialize"
    Private Sub Initialize()
        pnlDatagrid.Visible = False
        DtgAgree.Visible = False
        lblMessage.Visible = True

    End Sub
#End Region

#Region "Search  And Reset"
    Private Sub Btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btnsearch.Click        
        Me.SearchBy = ""
        Me.SearchBy = " STNKRenewalLetter.BranchId = '" & oBranch.BranchID & " ' and STNKRenewalLetter.IsPrint = " & CType(ddlIsPrint.SelectedItem.Value, Integer)

        If txtSearchBy.Text.Trim <> "" Then            
            Me.SearchBy = Me.SearchBy & " and " & ddlSearchBy.SelectedItem.Value & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If

        If txtSTNKDate.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and STNKRenewalLetter.CreatedDate = '" & ConvertDate2(txtSTNKDate.Text).ToString("yyyyMMdd") & "' "
        End If

        SearchSTNKRenewalLetter()
    End Sub

    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Response.Redirect("STNKRenewalLetter.aspx")
    End Sub
#End Region

#Region "DTG"

    Private Sub DtgAgree_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAgree.ItemDataBound

        Dim oLtlCustomerID As Literal
        Dim lbTemp As HyperLink


        If e.Item.ItemIndex >= 0 Then

            oLtlCustomerID = DirectCast(e.Item.FindControl("ltlCustomerID"), Literal)
            '*** Add agreement view
            lbTemp = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            lbTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Collection" & "', '" & lbTemp.Text.Trim & "')"

            '*** Add customer  view
            lbTemp = CType(e.Item.FindControl("hypName"), HyperLink)
            lbTemp.NavigateUrl = "javascript:OpenCustomer('" & "Collection" & "', '" & oLtlCustomerID.Text.Trim & "')"
        End If
    End Sub


    Private Sub DtgAgree_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgAgree.SortCommand
        If Me.SortBy.IndexOf("DESC") > 0 Then
            Me.SortBy = e.SortExpression & " ASC"
        Else
            Me.SortBy = e.SortExpression & " DESC"
        End If
        Call SearchSTNKRenewalLetter()
    End Sub
#End Region

#Region "Print Handles"
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Dim StrPrint As String
        BindAgreementCheked()

    End Sub
#End Region

#Region "Navigation Page"

    Private Sub Navigation_click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) _
    Handles imbNextPage.Click, imbFirstPage.Click, imbPrevPage.Click, imbLastPage.Click

        Select Case DirectCast(sender, ImageButton).CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : If currentPage < CType(lblTotPage.Text.Trim, Integer) Then currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : If currentPage > 1 Then currentPage = Int32.Parse(lblPage.Text) - 1
            Case "Go" : If CType(txtPage.Text, Integer) > 0 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text.Trim, Integer) _
                        Then currentPage = CType(txtPage.Text, Integer)
        End Select
        SearchSTNKRenewalLetter()

    End Sub
#End Region


End Class