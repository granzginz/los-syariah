﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKRenewalIncome
    Inherits Maxiloan.Webform.WebBased

#Region "Declaration & Constant"
    Private m_controller As New DataUserControlController
    Private m_error As Boolean = False
#End Region

#Region "Sub & Function"
    Private Sub bindComboBranch()
        Dim dtbranch As New DataTable
        dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboParent
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(1).Value = "ALL"

            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Me.IsPostBack Then
            Me.FormID = "STNKRENEWALINCOME"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
            End If
            bindComboBranch()
        End If
    End Sub

#End Region

#Region "Event Button Print"

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", "Maxiloan") Then
            Dim strfilterBy As String
            Dim strBusinessDate As String
            Dim strBranchID As String
            Dim strPeriode As String
            strfilterBy = ""
            strBusinessDate = ""
            strPeriode = ""
            strBranchID = ""

            strBusinessDate = "01/" & cboBulan.SelectedItem.Value.Trim & "/" & txtYear.Text.Trim
            strPeriode = cboBulan.SelectedItem.Text.Trim & "   " & txtYear.Text.Trim
            If cboParent.SelectedItem.Value.Trim = "ALL" Then
                strBranchID = ""
            Else
                strBranchID = cboParent.SelectedItem.Value.Trim
            End If
            strfilterBy = strfilterBy & "Branch : " & cboParent.SelectedItem.Text.Trim & " and Periode : " & cboBulan.SelectedItem.Text.Trim & "  " & txtYear.Text.Trim
            ShowMessage(lblMessage, strBusinessDate.Trim, True)

            Dim cookie As HttpCookie = Request.Cookies("report")
            If Not cookie Is Nothing Then
                cookie.Values("BranchID") = strBranchID.Trim
                cookie.Values("BusinessDate") = strBusinessDate.Trim
                cookie.Values("Periode") = strPeriode.Trim
                cookie.Values("LoginID") = Me.Loginid
                cookie.Values("FilterBy") = strfilterBy.Trim
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("report")
                cookieNew.Values.Add("BranchID", strBranchID.Trim)
                cookieNew.Values.Add("BusinessDate", strBusinessDate.Trim)
                cookieNew.Values.Add("Periode", strPeriode.Trim)
                cookieNew.Values.Add("LoginID", Me.Loginid)
                cookieNew.Values.Add("FilterBy", strfilterBy.Trim)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("STNKRenewalIncomeViewer.aspx")
        End If
    End Sub
#End Region

End Class