﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STNKInvoicing.aspx.vb"
    Inherits="Maxiloan.Webform.STNK.STNKInvoicing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>STNKInvoicing</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/';   

        function Tax() {
            var MyAgentFee = document.forms[0].txtAgentFee.value;
            var MyTax = document.forms[0].hdnTax.value;
            var Hasil = parseFloat(MyAgentFee * MyTax) / 100;
            document.forms[0].txtTax.value = Hasil;
            document.forms[0].hdnTax2.value = Hasil;
            TotalAP();
        }
        function TotalAP() {
            var MyBBN = document.forms[0].txtBBN.value;
            var MyOther = document.forms[0].txtOtherFee.value;
            var MyAgentFee = document.forms[0].txtAgentFee.value;
            //var MyDP = document.forms[0].hdnDP.value;
            var Tax = document.forms[0].txtTax.value;
            //alert(MyBBN);
            //alert(MyOther);
            //alert(MyAgentFee);
            //alert(Tax);
            var Proses = parseFloat(MyBBN) + parseFloat(MyOther) + parseFloat(MyAgentFee) - parseFloat(Tax);
            document.forms[0].txtTotalAP.value = Proses;
            document.forms[0].hdnTotalAP2.value = Proses;

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INVOICE PERPANJANGAN STNK
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Request
            </label>
            <asp:HyperLink ID="HypRequestNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Tanggal Request
            </label>
            <asp:HyperLink ID="HypRequestDate" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                <strong>DATA REGISTRASI</strong>
            </label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jenis Asset
            </label>
            <asp:Label ID="lblAssetType" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Rangka
            </label>
            <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Mesin
            </label>
            <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tahun Produksi
            </label>
            <asp:Label ID="lblManufacturing" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Warna
            </label>
            <asp:Label ID="lblColor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama
            </label>
            <asp:Label ID="lblName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Alamat
            </label>
            <asp:Label ID="lblAddress" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Polisi
            </label>
            <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal STNK
            </label>
            <asp:Label ID="lblSTNKDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Biaya Administrasi
            </label>
            <asp:Label ID="lblAdminFee" runat="server"></asp:Label>
        </div>
    </div>
    <asp:Panel ID="pnlRgister" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label>
                    <strong>INVOICE DARI BIRO JASA</strong>
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Biro Jasa
                </label>
                <asp:Label ID="lblSTNKAgent" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Reference
                </label>
                <asp:Label ID="lblReffNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class ="label_req">
                    No Invoice
                </label>
                <asp:TextBox ID="txtInvoice" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtInvoice"
                    ErrorMessage="Harap isi No Invoice" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Invoice
                </label>
                <asp:TextBox runat="server" ID="txtValueDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tgl Rencana Bayar
                </label>
                 <asp:TextBox runat="server" ID="TxtTglRencanaBayar"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="TxtTglRencanaBayar"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>

        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya BBN/STNK
                </label>
                <asp:TextBox ID="txtBBN" onblur="TotalAP();" runat="server"  Text="0"></asp:TextBox>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtBBN"
                    ErrorMessage="Harap isi dengan Angka" Display="Dynamic" MinimumValue="0" Type="Double"
                    MaximumValue="999999999999.99"></asp:RangeValidator>
            </div>
            <div class="form_right">
                <label>
                    Biaya Lainnya
                </label>
                <asp:TextBox ID="txtOtherFee" onblur="TotalAP();" runat="server" 
                    Text="0"></asp:TextBox>
                <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="txtOtherFee"
                    ErrorMessage="Harap isi dengan Angka" Display="Dynamic" MinimumValue="0" Type="Double"
                    MaximumValue="999999999999.99"></asp:RangeValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Biro Jasa
                </label>
                <asp:TextBox ID="txtAgentFee" onblur="Tax();" runat="server"  Text="0"></asp:TextBox>
                <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="txtAgentFee"
                    ErrorMessage="Harap isi dengan Angka" Display="Dynamic" MinimumValue="0" Type="Double"
                    MaximumValue="999999999999.99"></asp:RangeValidator>
            </div>
            <div class="form_right">
                <label>
                    Total Tagihan (A/P)
                </label>
                <input id="hdnTotalAP2" type="hidden" name="hdnTotalAP2" runat="server" />
                <asp:TextBox ID="txtTotalAP" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    PPh Ps 23
                    <asp:Label ID="lblPph" runat="Server"></asp:Label>&nbsp;% Dari Biaya Jasa
                </label>
                <asp:TextBox ID="txtTax" runat="server" ></asp:TextBox>
                <input id="hdnTax" type="hidden" runat="server" />
                <input id="hdnTax2" type="hidden" runat="server" />
            </div>
        </div>
       
        <div class="form_box">
            <div class="form_single">
                <label>
                    Catatan
                </label>
                <asp:TextBox ID="txtNotes" runat="server"  Width="384px" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="BtnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
