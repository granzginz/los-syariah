﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STNKView.aspx.vb" Inherits="Maxiloan.Webform.STNK.STNKView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>STNKView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

     function fClose() {
            window.close();
            return false;
        }
    
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                REGISTRASI STNK KE BIROJASA
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Request
            </label>
            <asp:HyperLink ID="HypRequestNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Tanggal Request
            </label>
            <asp:HyperLink ID="HypRequestDate" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <strong>DATA REGISTRASI</strong>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jenis Asset
            </label>
            <asp:Label ID="lblAssetType" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Rangka
            </label>
            <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Mesin
            </label>
            <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tahun Produksi
            </label>
            <asp:Label ID="lblManufacturing" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Warna
            </label>
            <asp:Label ID="lblColor" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama
            </label>
            <asp:Label ID="lblName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Alamat
            </label>
            <asp:Label ID="lblAddress" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Polisi
            </label>
            <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal STNK
            </label>
            <asp:Label ID="lblSTNKDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Biaya Administrasi
            </label>
            <asp:Label ID="lblAdminFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <strong>REGISTRASI PERPANJANGAN STNK</strong>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biro Jasa
            </label>
            <asp:Label ID="lblSTNKAgent" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Valuta
            </label>
            <asp:Label ID="lblValueDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Uang Muka (DP)
            </label>
            <asp:Label ID="lblDP" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Reference
            </label>
            <asp:Label ID="lblReffNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Catatan
            </label>
            <asp:Label ID="lblNotes" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <strong>INVOICE DARI BIRO JASA</strong>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Invoice
            </label>
            <asp:Label ID="lblInvoiceNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Invoice
            </label>
            <asp:Label ID="lblInvoiceDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya BBN/STNK
            </label>
            <asp:Label ID="lblSTNKFee" runat="server" align="right"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Lainnya
            </label>
            <asp:Label ID="lblOtherFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Biro Jasa
            </label>
            <asp:Label ID="lblAgentFee" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Total Biaya BBN/STNK
            </label>
            <asp:Label ID="lblTotalSTNKFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                PPh
                <asp:Label ID="lblPph" runat="Server"></asp:Label>&nbsp;% Dari Jasa
            </label>
            <asp:Label ID="lblTaxFromAgent" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Total Invoice (A/P)
            </label>
            <asp:Label ID="lblTotalAP" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <strong>STATUS</strong>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Status
            </label>
            <asp:Label ID="lblStatus" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Status
            </label>
            <asp:Label ID="lblStatusDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Catatan
            </label>
            <asp:Label ID="lblStatusNotes" runat="server"></asp:Label>
        </div>
    </div>
      <div class="form_button">
       <asp:Button ID="ButtonClose" OnClientClick="fClose();" runat="server" CausesValidation="false" Text="Close" CssClass ="small button gray">
       </asp:Button>
    </div>
    </form>
</body>
</html>
