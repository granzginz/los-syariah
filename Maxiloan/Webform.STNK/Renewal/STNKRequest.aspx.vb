﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class STNKRequest
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents lblAdminFee As ucNumberFormat
    Protected WithEvents lblEstimasiPajak As ucNumberFormat
    Protected WithEvents lblEstimasiDenda As ucNumberFormat
    Protected WithEvents lblEstimasiBiroJasa As ucNumberFormat
    Protected WithEvents lblEstimasiBiayaLain As ucNumberFormat
#Region "Property"
    Private Property Status() As String
        Get
            Return CType(ViewState("Status"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Status") = Value
        End Set
    End Property
    Private Property Tax() As Date
        Get
            Return CType(ViewState("Tax"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("Tax") = Value
        End Set
    End Property
    Private Property AdmFee() As String
        Get
            Return CType(ViewState("AdmFee"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AdmFee") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oCustomClassAg As New Parameter.AgreementList
    Private oCustomClass As New Parameter.STNKRenewal
    Private oCustomClassGeneral As New Parameter.GeneralSetting
    Private oController As New STNKRenewalController
    Private oCommonController As New DataUserControlController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Me.ApplicationID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).ApplicationID
            Me.BranchID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).BranchId
            Me.CustomerName = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).CustomerName
            Me.CustomerID = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).CustomerID
            Me.AgreementNo = CType(HttpContext.Current.Items("Receive"), Parameter.AgreementList).Agreementno

            'lblEstimasiPajak.OnClientChange = "perkiraanTotal();"
            'lblEstimasiDenda.OnClientChange = "perkiraanTotal();"
            'lblEstimasiBiroJasa.OnClientChange = "perkiraanTotal();"
            'lblEstimasiBiayaLain.OnClientChange = "perkiraanTotal();"

            GetInformation()
            ModVar.StrConn = GetConnectionString()
            ModVar.BranchId = Me.sesBranchId.Replace("'", "")
        End If
    End Sub

    'Private Function ConvertDateVB_tmp(ByVal pStrValue As String) As String

    '    Dim lStrValue As String = Trim(pStrValue)
    '    Dim lArrValue As Array
    '    lArrValue = CType(lStrValue.Split(CChar("/")), Array)
    '    If UBound(lArrValue) = 2 Then
    '        lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(0).ToString), 2), 0)
    '        lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(1).ToString), 2), 1)
    '        If Len(lArrValue.GetValue(2)) = 2 Then
    '            lArrValue.SetValue(Left(CStr(Year(Now())), 2) + lArrValue.GetValue(2).ToString, 2)
    '        End If
    '        lStrValue = lArrValue.GetValue(1).ToString & "/" & lArrValue.GetValue(0).ToString & "/" & lArrValue.GetValue(2).ToString
    '    End If
    '    Return lStrValue
    'End Function
    Private Sub BtnRequest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRequest.Click
        If txtuscDate.Text = "" Then
            ShowMessage(lblMessage, "Tanggal STNK harus diisi!", True)
            Exit Sub
        End If


        'If Me.Status <> "Request" And Me.Status <> "Register" Then
        If IsDate(ConvertDateVB(txtuscDate.Text.Trim)) = False Then
            ShowMessage(lblMessage, "Tanggal STNK salah", True)
            Exit Sub
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
            .BranchAgreement = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
            .Notes = txtNotes.Text.Trim
            .LoginId = Me.Loginid
            .AdmFee = CDbl(lblAdminFee.Text)
            .STNKDate = ConvertDate2(txtuscDate.Text.Trim)
            .RenewalType = cboRenewalType.SelectedValue
            .CustReq = cboCustReq.SelectedValue
            .EstimasiPajak = CDbl(lblEstimasiPajak.Text)
            .EstimasiDenda = CDbl(lblEstimasiDenda.Text)
            .EstimasiJasa = CDbl(lblEstimasiBiroJasa.Text)
            .EstimasiByLain = CDbl(lblEstimasiBiayaLain.Text)
        End With
        oCustomClass = oController.SaveSTNKRequest(oCustomClass)
        If oCustomClass.ErrorSave = True Then
            Response.Write("<script language='javascript'>alert('Error While Saving The Data!!');</script>")
        Else

            If cboCustReq.SelectedValue <> "K" Then
                Dim cookieNew As New HttpCookie("RptConfLetter")
                cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
                cookieNew.Values.Add("BranchID", Me.BranchID)
                Response.AppendCookie(cookieNew)
                Response.Redirect("EstimasiBiayaSTNKViewer.aspx")
            Else
                Response.Redirect("SNTKList.aspx")
                '    Dim cookieNew As New HttpCookie("STNKRenewalLetter")
                '    cookieNew.Values.Add("cmdwhere", " And Agreement.BranchID in ('" & Me.BranchID.Trim & "') and Agreement.ApplicationID in ('" & Me.ApplicationID.Trim & "')")
                '    cookieNew.Values.Add("LoginID", Me.Loginid)
                '    cookieNew.Values.Add("FormID", "STNKRequest")
                '    Response.AppendCookie(cookieNew)
                '    Response.Redirect("STNKRenewalLetterviewer.aspx")
            End If
            'Response.Redirect("SNTKList.aspx")
        End If
        'Else
        'ShowMessage(lblMessage, "Kontrak ini sedang mengajukan Perpanjang STNK", True)
        'End If

    End Sub
    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("SNTKList.aspx")
    End Sub

    <System.Web.Services.WebMethod()> _
    Public Shared Function GetAdminFee(ByVal custreq As String) As String
        Dim oCustomClassGeneral As New Parameter.GeneralSetting
        Dim oCommonController As New DataUserControlController
        Dim oCustomClass As New Parameter.STNKRenewal

        Dim AdmFee As String

        oCustomClass.strConnection = ModVar.StrConn
        oCustomClass.BranchId = ModVar.BranchId

        If (custreq = "U") Then
            oCustomClassGeneral.GSID = "STNKADMFEE"
        ElseIf (custreq = "K") Then
            oCustomClassGeneral.GSID = "STNKKTRFEE"
        End If

        If (custreq = "-") Then
            AdmFee = "0"
        Else
            AdmFee = oCommonController.GetGeneralSetting(oCustomClass.strConnection, oCustomClassGeneral)
        End If

        Return FormatNumber(AdmFee, 0)
    End Function

    '<System.Web.Services.WebMethod()> _
    'Public Shared Function GetPerkiraanTotal(ByVal custreq As String) As String


    '    Dim perkiraanTotal As String
    '    Dim total As Decimal

    '    total=lblEstimasiPajak.
    '    Return FormatNumber(perkiraanTotal, 0)
    'End Function

    Private Sub GetInformation()

        lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccMnt" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
        lblAgreementNo.Text = Me.AgreementNo
        lblCustomerName.Text = Me.CustomerName
        lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.WhereCond = "ApplicationID='" & Me.ApplicationID & "'"
        oCustomClass = oController.GetSTNKRequest(oCustomClass)
        With oCustomClass
            lblAssetDescription.Text = .AssetDescription
            lblAssetType.Text = .AssetType
            lblChassisNo.Text = .SerialNo1
            lblEngineNo.Text = .SerialNo2
            lblManufacturing.Text = .ManufacturingYear
            lblColor.Text = .Color
            If .OwnerName = "" Then lblName.Text = "-" Else lblName.Text = .OwnerName
            If .OwnerAddress = "" Then lblAddress.Text = "-" Else lblAddress.Text = .OwnerAddress
            lblLicensePlate.Text = .LicensePlate
            'If .TaxDate = "" Then Me.Tax = Nothing Else Me.Tax = CDate(.TaxDate)
            If .TaxDate = "-" Then
                txtuscDate.Text = ""
                Me.Tax = Nothing
            Else
                Me.Tax = CDate(.TaxDate)
                txtuscDate.Text = CType(Me.Tax, String)
            End If

            Me.Status = .txtSearch
            If Me.Tax = Nothing Then
                txtuscDate.Text = ""
            Else
                txtuscDate.Text = Me.Tax.ToString("dd/MM/yyyy")
            End If

        End With
        oCustomClassGeneral.GSID = "STNKADMFEE"
        Me.AdmFee = oCommonController.GetGeneralSetting(oCustomClass.strConnection, oCustomClassGeneral)
        lblAdminFee.Text = FormatNumber(Me.AdmFee, 0)
    End Sub

End Class