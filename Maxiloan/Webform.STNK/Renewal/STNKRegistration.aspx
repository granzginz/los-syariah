﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STNKRegistration.aspx.vb"
    Inherits="Maxiloan.Webform.STNK.STNKRegistration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>STNKRegistration</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlDatagrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    REGISTRASI STNK KE BIRO JASA
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Request
                </label>
                <asp:HyperLink ID="HypRequestNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Request
                </label>
                <asp:HyperLink ID="HypRequestDate" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h5>DATA REGISTRASI</h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nama Asset
                </label>
                <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jenis Asset
                </label>
                <asp:Label ID="lblAssetType" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Rangka
                </label>
                <asp:Label ID="lblChassisNo" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Mesin
                </label>
                <asp:Label ID="lblEngineNo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Tahun Produksi
                </label>
                <asp:Label ID="lblManufacturing" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Warna
                </label>
                <asp:Label ID="lblColor" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama
                </label>
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alamat
                </label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Polisi
                </label>
                <asp:Label ID="lblLicensePlate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal STNK
                </label>
                <asp:Label ID="lblSTNKDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Biaya Administrasi
                </label>
                <uc1:ucnumberformat runat="server" id="txtAdminFee"></uc1:ucnumberformat>
                <%--<asp:TextBox ID="txtAdminFee" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAdminFee"
                    ErrorMessage="*)"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtAdminFee"
                    ErrorMessage="Harap isi dengan Angka" MaximumValue="999999999" MinimumValue="0"></asp:RangeValidator>--%>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlRgister" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h5>REGISTRASI PERPANJANGAN STNK</h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                 <label class ="label_req">
                    Biro Jasa
                </label>
                <asp:DropDownList ID="cboAgent" Width = "200px" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ErrorMessage="Harap pilih Biro Jasa"
                    ControlToValidate="cboAgent" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Valuta
                </label>
                <asp:TextBox runat="server" ID="txtValueDate" CssClass ="small_text"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtValueDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Reference
                </label>
                <asp:TextBox ID="txtReferenceNo" runat="server" ></asp:TextBox>
            </div>            
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general" >
                    Catatan
                </label>
                <asp:TextBox ID="txtNotes" runat="server" CssClass="multiline_textbox"  TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="BtnSave" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="BtnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
