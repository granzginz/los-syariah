﻿Public Module RateConvertion

    'Start modify ario FACT & MDKJ 10 Feb 2020
    'Function cEffToFlat(ByVal oClass As Parameter.FactoringInvoice) As Decimal
    '    If oClass.FirstInstallment = "AD" Then
    '        Return cEffToFlatAdvanceFact(oClass)
    '    Else
    '        Return cEffToFlatArrearFact(oClass)
    '    End If
    'End Function
    'Function cEffToFlatAdvanceFact(ByVal oClass As Parameter.FactoringInvoice) As Decimal
    '    Dim FlatRateAdvancePerTenor As Decimal

    '    FlatRateAdvancePerTenor = CDec((((pmtAdvance(oClass) * (oClass.Tenor / 12) * 12) - 1000) * 100 / 1000) / (oClass.Tenor / 12) / 12 * oClass.Tenor)
    '    Return CDec(FlatRateAdvancePerTenor / (oClass.Tenor / 12))
    'End Function
    'Function cEffToFlatArrearFact(ByVal oClass As Parameter.FactoringInvoice) As Decimal
    '    Dim FlatRateArrearPerTenor As Decimal

    '    FlatRateArrearPerTenor = CDec((((pmtArrear(oClass) * oClass.Tenor) - 1000) * 100 / 1000) / (oClass.Tenor / 12) / 12 * oClass.Tenor)
    '    Return CDec(FlatRateArrearPerTenor / (oClass.Tenor / 12))
    'End Function
    Function pmtAdvance(ByVal oClass As Parameter.FactoringInvoice) As Decimal
        Return CDec(1000 / (((1 - 1 / (1 + oClass.EffectiveRate / 12) ^ ((oClass.Tenor) / 12 * 12 - 1)) / (oClass.EffectiveRate / 12)) + 1))
    End Function
    Function pmtArrear(ByVal oClass As Parameter.FactoringInvoice) As Decimal
        Return CDec(Pmt(oClass.EffectiveRate / 12, oClass.Tenor, -1000))
    End Function
    'End modify

    Function cEffToFlat(ByVal oClass As Parameter.FinancialData) As Decimal
        If oClass.FirstInstallment = "AD" Then
            Return cEffToFlatAdvance(oClass)
        Else
            Return cEffToFlatArrear(oClass)
        End If
    End Function

    Function cEffToFlatAdvance(ByVal oClass As Parameter.FinancialData) As Decimal
        Dim FlatRateAdvancePerTenor As Decimal

        FlatRateAdvancePerTenor = CDec((((pmtAdvance(oClass) * (oClass.Tenor / 12) * 12) - 1000) * 100 / 1000) / (oClass.Tenor / 12) / 12 * oClass.Tenor)
        Return CDec(FlatRateAdvancePerTenor / (oClass.Tenor / 12))
    End Function

    Function pmtAdvance(ByVal oClass As Parameter.FinancialData) As Decimal
        Return CDec(1000 / (((1 - 1 / (1 + oClass.EffectiveRate / 12) ^ ((oClass.Tenor) / 12 * 12 - 1)) / (oClass.EffectiveRate / 12)) + 1))
    End Function

    Function cEffToFlatArrear(ByVal oClass As Parameter.FinancialData) As Decimal
        Dim FlatRateArrearPerTenor As Decimal

        FlatRateArrearPerTenor = CDec((((pmtArrear(oClass) * oClass.Tenor) - 1000) * 100 / 1000) / (oClass.Tenor / 12) / 12 * oClass.Tenor)
        Return CDec(FlatRateArrearPerTenor / (oClass.Tenor / 12))
    End Function

    Function pmtArrear(ByVal oClass As Parameter.FinancialData) As Decimal
        Return CDec(Pmt(oClass.EffectiveRate / 12, oClass.Tenor, -1000))
    End Function

    Function cFlatToEff(ByVal oClass As Parameter.FinancialData) As Decimal
        Dim decRunRate As Decimal
        Dim decPmt As Decimal

        decRunRate = CDec(oClass.FlatRate * (oClass.Tenor / 12))
        decPmt = (1 + (1 * decRunRate)) / oClass.Tenor

        If oClass.FirstInstallment = "AD" Then
            Return CDec((Rate(oClass.Tenor, decPmt, -1, 0, DueDate.BegOfPeriod, 0) / 1 * 12) * 100)
        Else
            Return CDec((Rate(oClass.Tenor, decPmt, -1, 0, DueDate.EndOfPeriod, 0) / 1 * 12) * 100)
        End If
    End Function

    Public Function GetEffectiveRateAdv(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer) As Decimal
        Dim i As Integer
        Dim Values() As Double
        Dim Ctr_Values As Integer
        Dim Total_In_Advanced As Double
        ReDim Values(intNumInstallment)

        Ctr_Values = 0
        Total_In_Advanced = dblInstAmt
        Values(0) = -dblNTF + Total_In_Advanced

        For i = 2 To intNumInstallment
            Ctr_Values = Ctr_Values + 1
            Values(Ctr_Values) = dblInstAmt
        Next

        Return CDec((IRR(Values, 0.00000001) * GetTerm(intPayFreq)) * 100)
    End Function

    Public Function GetEffectiveRateArr(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer) As Decimal
        Dim i As Integer
        Dim Values() As Double
        ReDim Values(intNumInstallment + 1)

        Values(0) = -dblNTF

        For i = 1 To intNumInstallment
            Values(i) = dblInstAmt
        Next

        Return CDec((IRR(Values, 0.00000001) * GetTerm(intPayFreq)) * 100)
    End Function

    Public Function GetTerm(ByVal intInstTerm As Integer) As Integer
        Select Case intInstTerm
            Case 1
                Return 12
            Case 2
                Return 6
            Case 3
                Return 4
            Case 6
                Return 2
            Case Else
                Return 0
        End Select
    End Function
End Module
