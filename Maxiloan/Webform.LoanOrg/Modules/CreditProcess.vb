﻿
Public Module CreditProcess
    Public Sub FeeBehaviour(ByVal ColumnName As String, ByVal TextName As TextBox, ByVal RangeValid As RangeValidator, ByVal oRow As DataRow)
        TextName.Text = CInt(oRow(ColumnName)).ToString.Trim
        Select Case oRow(ColumnName + "Behaviour").ToString.Trim
            Case "D"
                RangeValid.MinimumValue = "0"
                RangeValid.MaximumValue = "999999999999999"
                RangeValid.ErrorMessage = "Input Salah"
                RangeValid.Enabled = True
                TextName.ReadOnly = False
            Case "L"
                RangeValid.MinimumValue = "0"
                RangeValid.MaximumValue = "999999999999999"
                RangeValid.ErrorMessage = "Input Salah"
                RangeValid.Enabled = True
                TextName.ReadOnly = True
            Case "N"
                TextName.ReadOnly = False
                RangeValid.MinimumValue = CInt(oRow(ColumnName)).ToString.Trim
                RangeValid.MaximumValue = "999999999999999"
                RangeValid.ErrorMessage = "Input harus >= " & oRow(ColumnName).ToString.Trim & "!"
                RangeValid.Enabled = True
            Case "X"
                TextName.ReadOnly = False
                RangeValid.MaximumValue = CInt(oRow(ColumnName)).ToString.Trim
                RangeValid.MinimumValue = "0"
                RangeValid.ErrorMessage = "Input harus <= " & oRow(ColumnName).ToString.Trim & "!"
                RangeValid.Enabled = True
        End Select
    End Sub


    Public Function getNPV(ntf As Double, refundRate As Decimal, flatRate As Decimal, TenorYear As Integer) As Double
        Dim npv As Double
        'npv = (ntf * (refundRate / 100) * TenorYear) / (1 + ((flatRate / 100) * TenorYear))
        'Return Math.Ceiling(Math.Round(npv, 0) / 1000) * 1000

        'npv = (ntf * TenorYear * (refundRate / 100)) / (1 + (flatRate / 100))
        npv = (ntf * TenorYear * refundRate) / (1 + (flatRate / 100))
        Return Math.Ceiling(Math.Round(npv, 0) / 1000) * 1000

    End Function
    Public Function getRefundRate(ntf As Double, npv As Double, flatrate As Decimal, TenorYear As Integer) As Double
        Dim refundRate As Double
        Dim x As Double = (1 + ((flatrate / 100) * TenorYear))
        refundRate = ((npv * x) / TenorYear / ntf) * 100

        Return Math.Round(refundRate, 1)
    End Function
End Module
