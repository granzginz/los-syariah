﻿Imports System.Data.SqlClient
Imports Maxiloan.Webform.WebBased
Imports Maxiloan.Parameter
 

Public Module CreditScoringMdl
    Public Function CalculateCreditScoring(ByVal scoringData As CreditScoring_calculate) As CreditScoring_calculate
        Dim LObjCommand As SqlCommand
        Dim LObjAdpt As New SqlDataAdapter
        Dim LObjDS As New DataSet
        Dim LIntIndex As Integer
        Dim LObjRowCreditScoreSchemeComponent As DataRow
        Dim LObjDataReader As SqlDataReader
        Dim LStrResultQuery As String
        Dim ScoreDesc As String
        Dim LObjRow As DataRow
        Dim ScoreValue As Double = 0
        Dim myFatalScore As Boolean = False
        Dim myWarningScore As Boolean = False
        Dim FatalDesc As String = ""
        Dim WarningDesc As String = ""
        Dim LDblResultCalc As Double = 0
        Dim LDblApproveScore As Double
        Dim LDblRejectScore As Double
        Dim GObjCon As New SqlConnection(scoringData.strConnection)
        Dim LStrSQL As String
        Dim ReturnResult As String
        Dim myDataColumn As DataColumn
        Dim myDataRow As DataRow
        Dim LDblGradeTotal As Double = 0

        Dim LStrStatusNote As String
        Dim LStrStatus As String
        Dim mydatatable As DataTable = scoringData.DT

        Dim ScoreAbsolute As Double = 0

        'cari CustomerID, ProductID, ProductOfferingID dari Agreement
        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
        LStrSQL = "spCount_CreditScoring1"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
        LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Agreement")

        'cari tipe Customer (Personal/Company)
        LStrSQL = "spCount_CreditScoring2"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CustomerID", LObjDS.Tables("Agreement").Rows(0)("CustomerID"))
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Customer")
        scoringData.CustomerType = LObjDS.Tables("Customer").Rows(0)("CustomerType").ToString

        'cari CreditScoreSchemeID dari ProductOffering 
        LStrSQL = "spCount_CreditScoring3"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
        LObjCommand.Parameters.AddWithValue("@ProductID", LObjDS.Tables("Agreement").Rows(0)("ProductID"))
        LObjCommand.Parameters.AddWithValue("@ProductOfferingID", LObjDS.Tables("Agreement").Rows(0)("ProductOfferingID"))
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "ProductOffering")
        scoringData.CreditScoreSchemeID = LObjDS.Tables("ProductOffering").Rows(0)("CreditScoreSchemeID").ToString

        'cari Component Scoring yang sesuai dengan CreditScoreSchemeID        
        LStrSQL = "spCount_CreditScoring4"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", scoringData.CreditScoreSchemeID)
        LObjCommand.Parameters.AddWithValue("@ScoringType", scoringData.CustomerType)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreSchemeComponent")

        'cari dr tbl CreditScoreComponentContent
        LStrSQL = "spCount_CreditScoring5"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", scoringData.CreditScoreSchemeID)
        LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreComponentContent")
        '----------------------------------

        mydatatable = New DataTable("Result")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "ID"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Description"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Value"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "ScoreValue"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "ScoreAbsolute"
        mydatatable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "Weight"
        mydatatable.Columns.Add(myDataColumn)

        For LIntIndex = 0 To LObjDS.Tables("CreditScoreSchemeComponent").Rows.Count - 1
            LObjRowCreditScoreSchemeComponent = LObjDS.Tables("CreditScoreSchemeComponent").Rows(LIntIndex)
            scoringData.CreditScoreComponentID = LObjRowCreditScoreSchemeComponent("CreditScoreComponentID").ToString

            LObjCommand = New SqlCommand(LObjRowCreditScoreSchemeComponent("SQLCmd").ToString, GObjCon)

            If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

            LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
            LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)

            LObjDataReader = LObjCommand.ExecuteReader()
            LObjDataReader.Read()

            If Not IsDBNull(LObjDataReader(0)) Then
                LStrResultQuery = Trim(LObjDataReader(0).ToString)
            Else
                LStrResultQuery = ""
            End If

            LObjDataReader.Close()
            myDataRow = mydatatable.NewRow()
            ScoreDesc = LObjRowCreditScoreSchemeComponent("Description").ToString

            If LObjRowCreditScoreSchemeComponent("CalculationType").ToString = "R" Then
                Try
                    If Not IsNumeric(LStrResultQuery) Then
                        LStrResultQuery = CDbl(LStrResultQuery).ToString
                    End If

                    LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & scoringData.CreditScoreComponentID & "' and  ValueFrom <= " & LStrResultQuery & " and ValueTo >= " & LStrResultQuery & "")(0)
                    ScoreValue = CDbl(LObjRow("ScoreValue"))

                    If LObjRow("ScoreStatus").ToString.Trim = "F" Then
                        myFatalScore = True
                        FatalDesc &= ScoreDesc & " - "
                    End If
                Catch ex As Exception
                    ScoreValue = 0
                    Exit Try
                End Try
            Else
                Try
                    LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & scoringData.CreditScoreComponentID & "' and valueContent = '" & LStrResultQuery & "'")(0)
                    ScoreValue = CDbl(LObjRow("ScoreValue"))
                    If LObjRow("ScoreStatus").ToString.Trim = "F" Then
                        myFatalScore = True
                        FatalDesc &= ScoreDesc & " - "
                    End If
                Catch
                    ScoreValue = 0
                    Exit Try
                End Try
            End If

            LDblResultCalc += ((CDbl(LObjRowCreditScoreSchemeComponent("Weight")) / 100) * ScoreValue)
            LDblGradeTotal += ScoreValue

            myDataRow("ID") = scoringData.CreditScoreComponentID
            myDataRow("Description") = ScoreDesc
            myDataRow("Value") = LStrResultQuery
            myDataRow("ScoreValue") = ScoreValue
            myDataRow("ScoreAbsolute") = ScoreAbsolute
            myDataRow("Weight") = LObjRowCreditScoreSchemeComponent("Weight")
            mydatatable.Rows.Add(myDataRow)
        Next

        If scoringData.CustomerType = "P" Then
            LDblApproveScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("PersonalApprovedScore"))
            LDblRejectScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("PersonalRejectScore"))
        Else
            LDblApproveScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("CompanyApprovedScore"))
            LDblRejectScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("CompanyRejectScore"))
        End If

        If myFatalScore = True Then
            LStrStatusNote = "Rejected (Fatal Score - " & FatalDesc & ")"
            LStrStatus = "R"
            LDblResultCalc = 0
            scoringData.CSResult_Temp = "F"
        ElseIf LDblRejectScore >= LDblResultCalc Then
            LStrStatusNote = "Rejected"
            LStrStatus = "R"
            scoringData.CSResult_Temp = "R"
        ElseIf LDblApproveScore > LDblResultCalc And LDblRejectScore < LDblResultCalc Then
            LStrStatusNote = "Marginal"
            LStrStatus = "M"
            scoringData.CSResult_Temp = "M"
        ElseIf LDblApproveScore <= LDblResultCalc Then
            LStrStatusNote = "Approved"
            LStrStatus = "A"
            scoringData.CSResult_Temp = "A"
        End If

        scoringData.DT = mydatatable

        ReturnResult = CStr(LDblResultCalc) + " - " + LStrStatusNote

        scoringData.CreditScore = CDec(LDblGradeTotal)
        scoringData.ReturnResult = ReturnResult

        'LStrSQL = "spCreditScoringGrade"
        'LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        'LObjCommand.CommandType = CommandType.StoredProcedure
        'LObjCommand.Parameters.AddWithValue("@GradeValue", scoringData.CreditScore)
        'LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", scoringData.CreditScoreSchemeID)
        'LObjCommand.Parameters.AddWithValue("@CustType", scoringData.CustomerType)
        'LObjCommand.Parameters.Add("@GradeDescription", SqlDbType.VarChar, 50)
        'LObjCommand.Parameters.Add("@GradeID", SqlDbType.VarChar, 50)
        'LObjCommand.Parameters("@GradeDescription").Direction = ParameterDirection.Output
        'LObjCommand.Parameters("@GradeID").Direction = ParameterDirection.Output
        'LObjCommand.ExecuteNonQuery()
        scoringData = SetGradeValue(scoringData)

        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()
        'Return ReturnResult

        Return scoringData
    End Function




    Public Function SetGradeValue(ByVal data As CreditScoring_calculate) As CreditScoring_calculate
        Dim LObjCommand As SqlCommand
        Dim LStrSQL As String
        Dim GObjCon As New SqlConnection(data.strConnection)

        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

        LStrSQL = "spCreditScoringGrade"
        LObjCommand = New SqlCommand(LStrSQL, GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@GradeValue", data.CreditScore)
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", "SC1")
        LObjCommand.Parameters.AddWithValue("@CustType", data.CustomerType)
        LObjCommand.Parameters.Add("@GradeDescription", SqlDbType.VarChar, 50)
        LObjCommand.Parameters.Add("@GradeID", SqlDbType.VarChar, 50)
        LObjCommand.Parameters("@GradeDescription").Direction = ParameterDirection.Output
        LObjCommand.Parameters("@GradeID").Direction = ParameterDirection.Output
        LObjCommand.ExecuteNonQuery()

        Dim strGrade As String = IIf(IsDBNull(LObjCommand.Parameters("@GradeDescription").Value), "Tidak ada grade!", LObjCommand.Parameters("@GradeDescription").Value).ToString
        'lblGrade.Text = " " & strGrade & " (" & LDblGradeTotal & ")"
        data.lblGrade = " " & strGrade & " (" & data.CreditScore & ")"
        data.CreditScoreResult = IIf(IsDBNull(LObjCommand.Parameters("@GradeID").Value), "0", LObjCommand.Parameters("@GradeID").Value).ToString

        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()

        Return data
    End Function



    Public Function FicoCalculateCreditScoring(ByVal scoringData As CreditScoring_calculate) As CreditScoring_calculate

        Dim _scoreResults As Parameter.ScoreResults = New Parameter.ScoreResults

        Dim GObjCon As New SqlConnection(scoringData.strConnection)
        Dim LObjDS As New DataSet

        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

        Dim LObjCommand = New SqlCommand("spCount_CreditScoring1", GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
        LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)
        Dim LObjAdpt = New SqlDataAdapter("spCount_CreditScoring1", GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Agreement")

        'cari tipe Customer (Personal/Company) 
        LObjCommand = New SqlCommand("spCount_CreditScoring2", GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CustomerID", LObjDS.Tables("Agreement").Rows(0)("CustomerID"))
        LObjAdpt = New SqlDataAdapter("spCount_CreditScoring2", GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Customer")
        Dim CustomerType = LObjDS.Tables("Customer").Rows(0)("CustomerType").ToString

        'cari CreditScoreSchemeID dari ProductOffering  
        LObjCommand = New SqlCommand("spCount_CreditScoring3", GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
        LObjCommand.Parameters.AddWithValue("@ProductID", LObjDS.Tables("Agreement").Rows(0)("ProductID"))
        LObjCommand.Parameters.AddWithValue("@ProductOfferingID", LObjDS.Tables("Agreement").Rows(0)("ProductOfferingID"))
        LObjAdpt = New SqlDataAdapter("spCount_CreditScoring3", GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "ProductOffering")
        Dim CreditScoreSchemeID = LObjDS.Tables("ProductOffering").Rows(0)("CreditScoreSchemeID").ToString

        Dim _ScoreSchemeComponentList As IList(Of Parameter.ScoreSchemeComponent) = GetScoreSchemeComponents(scoringData.strConnection, CreditScoreSchemeID, CustomerType)
        Dim _ScoreComponentContentList As IList(Of Parameter.ScoreComponentContent) = GetScoreComponentContents(scoringData.strConnection, CreditScoreSchemeID)
        Dim _scoreResult As Parameter.ScoreResult
        Dim comCont As Parameter.ScoreComponentContent
        _scoreResults.SetRejectApproveScore(IIf(CustomerType.Trim = "P", _ScoreSchemeComponentList(0).PersonalApprovedScore, _ScoreSchemeComponentList(0).CompanyApprovedScore),
                                            IIf(CustomerType.Trim.Trim = "P", _ScoreSchemeComponentList(0).PersonalRejectScore, _ScoreSchemeComponentList(0).CompanyRejectScore), _ScoreSchemeComponentList(0).CuttOffScore)
        'cari dr tbl CreditScoreComponentContent 
        _scoreResults.ScoreSchemeID = CreditScoreSchemeID
        For Each item As Parameter.ScoreSchemeComponent In _ScoreSchemeComponentList

            _scoreResult = New Parameter.ScoreResult(scoringData.BranchId, scoringData.ApplicationId, item.ScoreComponentID, item.Description, item.Weight, "CRDITSCORE", item.Description) 
            LObjCommand = New SqlCommand(item.SQLCmd, GObjCon)

            If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

            LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
            LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)


            Dim lObjDataReader = LObjCommand.ExecuteReader()

            Dim lStrResultQuery = "-1"
            If (lObjDataReader.HasRows) Then
                lObjDataReader.Read()
                lStrResultQuery = IIf(IsDBNull(lObjDataReader(0)), "", Trim(lObjDataReader(0).ToString))
            End If


            If IsNumeric(lStrResultQuery) Then
                lStrResultQuery = CDbl(lStrResultQuery)
            End If

            lObjDataReader.Close()

            If item.CalculationType.Trim = "R" Then
                If Not IsNumeric(lStrResultQuery) Then lStrResultQuery = 0
                comCont = _ScoreComponentContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueFrom <= lStrResultQuery And x.ValueTo >= lStrResultQuery).SingleOrDefault()
            Else
                comCont = _ScoreComponentContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And Trim(x.ValueContent) = lStrResultQuery.ToString).SingleOrDefault()
            End If

            _scoreResult.ScoreValue = 0
            _scoreResult.ScoreStatus = ""
            _scoreResult.ContentSeqNo = 97
            _scoreResult.QueryResult = lStrResultQuery
            If Not (comCont Is Nothing) Then
                _scoreResult.ScoreStatus = comCont.ScoreStatus.ToString.Trim
                _scoreResult.ScoreValue = comCont.ScoreValue
                _scoreResult.ScoreAbsolute = (comCont.ScoreValue * _scoreResult.Weight / 100)
                _scoreResult.ContentSeqNo = comCont.ContentSeqNo
            End If
            _scoreResults.Add(_scoreResult)
        Next
        getScoreRejectPolicyScoring(scoringData.strConnection, scoringData.BranchId, scoringData.ApplicationId, CreditScoreSchemeID, _scoreResults)

        scoringData.CustomerType = CustomerType
        scoringData.CreditScoreSchemeID = CreditScoreSchemeID 
        scoringData.ScoreResults = _scoreResults
        'scoringData.DT = _scoreResults.ToDataTable
        'scoringData.CreditScore = _scoreResults.TotalScore
        'scoringData.CreditScoreResult = _scoreResults.TotalScore
        scoringData.ReturnResult = String.Format("CutOff Score : {0} Total Score : {1}", _scoreResults.CuttOff, CStr(_scoreResults.TotalScore))

        Return scoringData
    End Function

    Function GetScoreSchemeComponents(ByVal strCnn As String, ByVal CreditScoreSchemeID As String, CustomerType As String) As IList(Of Parameter.ScoreSchemeComponent)
        Dim GObjCon As New SqlConnection(strCnn)
        Dim LObjDS As New DataSet

        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
        Dim LObjCommand = New SqlCommand("spCount_CreditScoring4", GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", CreditScoreSchemeID)
        LObjCommand.Parameters.AddWithValue("@ScoringType", CustomerType)
        Dim LObjAdpt = New SqlDataAdapter("spCount_CreditScoring4", GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreSchemeComponent") 
        Dim data As DataTable = LObjDS.Tables("CreditScoreSchemeComponent")
        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()
        Return (From item As DataRow In data.Rows Select New Parameter.ScoreSchemeComponent(
                item("Description"),
                 item("CustOffScore"),
                item("PersonalApprovedScore"),
                item("PersonalRejectScore"),
                item("CompanyApprovedScore"),
                item("CompanyRejectScore"),
                item("CreditScoreComponentID"),
                item("Weight"),
                item("SQLCmd"),
                item("CalculationType"))).ToList()


    End Function

    Function GetScoreComponentContents(ByVal strCnn As String, ByVal scoreSchemeId As String) As IList(Of Parameter.ScoreComponentContent)

        Dim GObjCon As New SqlConnection(strCnn)
        Dim LObjDS As New DataSet
        Dim LObjCommand = New SqlCommand("spCount_CreditScoring5", GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", scoreSchemeId)
        Dim LObjAdpt = New SqlDataAdapter("spCount_CreditScoring5", GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreComponentContent")

        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()
        Dim data As DataTable = LObjDS.Tables("CreditScoreComponentContent")
        Return (From item As DataRow In data.Rows Select New Parameter.ScoreComponentContent(
                item("CreditScoreSchemeID"),
                item("CreditScoreComponentID"),
                item("ValueContent"),
                item("ValueDescription"),
                item("ValueFrom"),
                item("ValueTo"),
                item("ScoreValue"),
                item("ScoreAbsolute"),
                item("ScoreStatus"),
                item("ContentSeqNo")
                )).ToList()

        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()
    End Function



    Sub getScoreRejectPolicyScoring(cnn As String, branchId As String, prospectAppId As String, CreditScoreSchemeID As String, _RejectPolicyResults As Parameter.ScoreResults)
        Dim lObjCommand As SqlCommand

        Dim lObjDataReader As SqlDataReader
        Dim lStrResultQuery As Object
        Dim comCont As Parameter.ScoreComponentContent
        Dim _RejectPolicyContentList As IList(Of Parameter.ScoreComponentContent) = GetScoreRejectPolicys(cnn, CreditScoreSchemeID)
        Dim _RejectPolicyComponentList As IList(Of Parameter.ScoreSchemeComponent) = GetScoreRejectPolicyComponents(cnn)
        Dim gObjCon As New SqlConnection(cnn)
        Dim _RejectPolicyResult As Parameter.ScoreResult

        If gObjCon.State = ConnectionState.Closed Then gObjCon.Open()

        For Each item As Parameter.ScoreSchemeComponent In _RejectPolicyComponentList

            _RejectPolicyResult = New Parameter.ScoreResult(branchId, prospectAppId, item.ScoreComponentID, item.Description, item.Weight, "REJECTPOLICY", item.Description)

            lObjCommand = New SqlCommand(item.SQLCmd, gObjCon)
            If gObjCon.State = ConnectionState.Closed Then gObjCon.Open()

            lObjCommand.Parameters.AddWithValue("@BranchID", branchId)
            lObjCommand.Parameters.AddWithValue("@ApplicationID", prospectAppId)

            lObjDataReader = lObjCommand.ExecuteReader()



            lStrResultQuery = "-1"
            If (lObjDataReader.HasRows) Then
                lObjDataReader.Read()
                lStrResultQuery = IIf(IsDBNull(lObjDataReader(0)), "", Trim(lObjDataReader(0).ToString))
            End If

            If IsNumeric(lStrResultQuery) Then
                lStrResultQuery = CDbl(lStrResultQuery)
            End If

            lObjDataReader.Close()

            If item.CalculationType.Trim = "R" Then
                If Not IsNumeric(lStrResultQuery) Then lStrResultQuery = 0
                comCont = _RejectPolicyContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueFrom <= lStrResultQuery And x.ValueTo >= lStrResultQuery).SingleOrDefault()
            Else
                comCont = _RejectPolicyContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueContent = lStrResultQuery.ToString).SingleOrDefault()
            End If

            _RejectPolicyResult.ScoreValue = 0
            _RejectPolicyResult.ScoreStatus = "N"
            _RejectPolicyResult.ContentSeqNo = 0
            _RejectPolicyResult.QueryResult = lStrResultQuery
            If Not (comCont Is Nothing) Then
                _RejectPolicyResult.ScoreStatus = comCont.ScoreStatus.ToString.Trim
                _RejectPolicyResult.ScoreValue = comCont.ScoreValue
            End If
            _RejectPolicyResults.Add(_RejectPolicyResult)
        Next

    End Sub



    Function GetScoreRejectPolicys(cnn As String, CreditScoreSchemeID As String) As IList(Of Parameter.ScoreComponentContent)
        Dim query = " select '" + CreditScoreSchemeID + "' ScoreSchemeID, item ScoreComponentID , ValueContent, ValueDescription,  ValueFrom, ValueTo, ScoreValue, ScoreStatus, 0 ContentSeqNo  from CreditScoreRejectPolicy "
          
        Dim GObjCon As New SqlConnection(cnn)
        Dim LObjDS As New DataSet

        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
        Dim LObjCommand = New SqlCommand(query, GObjCon)
        LObjCommand.CommandType = CommandType.Text
       
        Dim LObjAdpt = New SqlDataAdapter(query, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreRejectPolicy")
        Dim data As DataTable = LObjDS.Tables("CreditScoreRejectPolicy")
        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()
        Return (From item As DataRow In data.Rows Select New Parameter.ScoreComponentContent(
                item("ScoreSchemeID"), item("ScoreComponentID"),
                item("ValueContent"), item("ValueDescription"),
                item("ValueFrom"), item("ValueTo"),
                item("ScoreValue"), item("ScoreAbsolute"), item("ScoreStatus"), item("ContentSeqNo")
                )).ToList()
    End Function

    Function GetScoreRejectPolicyComponents(ByVal strCnn As String) As IList(Of Parameter.ScoreSchemeComponent)
        Dim result As IList(Of Parameter.ScoreSchemeComponent) = New List(Of Parameter.ScoreSchemeComponent)
        Dim query = " select item , itemdescription , calculationtype,sqlcmd from CreditScoreRejectPolicyMaster  order by id"


        Dim GObjCon As New SqlConnection(strCnn)
        Dim LObjDS As New DataSet

        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
        Dim LObjCommand = New SqlCommand(query, GObjCon)
        LObjCommand.CommandType = CommandType.Text

        Dim LObjAdpt = New SqlDataAdapter(query, GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "CreditScoreRejectPolicyMaster")
        Dim data As DataTable = LObjDS.Tables("CreditScoreRejectPolicyMaster")
        If GObjCon.State = ConnectionState.Open Then GObjCon.Close()


        Return (From item As DataRow In data.Rows Select New Parameter.ScoreSchemeComponent(
               item("itemdescription"),
               0, 0, 0, 0, 0, item("item"), 0,
               item("sqlcmd"),
               item("calculationtype"))).ToList()
    End Function

    'Public Function CalculateCreditScoringSM(ByVal scoringData As CreditScoring_calculate) As CreditScoring_calculate
    '    Dim LObjCommand As SqlCommand
    '    Dim LObjAdpt As New SqlDataAdapter
    '    Dim LObjDS As New DataSet
    '    Dim LIntIndex As Integer
    '    Dim LObjRowCreditScoreSchemeComponent As DataRow
    '    Dim LObjDataReader As SqlDataReader
    '    Dim LStrResultQuery As String
    '    Dim ScoreDesc As String
    '    Dim LObjRow As DataRow
    '    Dim ScoreValue As Double = 0
    '    Dim myFatalScore As Boolean = False
    '    Dim myWarningScore As Boolean = False
    '    Dim FatalDesc As String = ""
    '    Dim WarningDesc As String = ""
    '    Dim LDblResultCalc As Double = 0
    '    Dim LDblApproveScore As Double
    '    Dim LDblRejectScore As Double
    '    Dim GObjCon As New SqlConnection(scoringData.strConnection)
    '    Dim LStrSQL As String
    '    Dim ReturnResult As String
    '    Dim myDataColumn As DataColumn
    '    Dim myDataRow As DataRow
    '    Dim LDblGradeTotal As Double = 0

    '    Dim LStrStatusNote As String
    '    Dim LStrStatus As String
    '    Dim mydatatable As DataTable = scoringData.DT

    '    'cari CustomerID, ProductID, ProductOfferingID dari Agreement
    '    If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()
    '    LStrSQL = "spCount_CreditScoringSM"
    '    LObjCommand = New SqlCommand(LStrSQL, GObjCon)
    '    LObjCommand.CommandType = CommandType.StoredProcedure
    '    LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
    '    LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)
    '    LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
    '    LObjAdpt.SelectCommand = LObjCommand
    '    LObjAdpt.Fill(LObjDS, "Agreement")

    '    'cari tipe Customer (Personal/Company)
    '    LStrSQL = "spCount_CreditScoring2"
    '    LObjCommand = New SqlCommand(LStrSQL, GObjCon)
    '    LObjCommand.CommandType = CommandType.StoredProcedure
    '    LObjCommand.Parameters.AddWithValue("@CustomerID", LObjDS.Tables("Agreement").Rows(0)("CustomerID"))
    '    LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
    '    LObjAdpt.SelectCommand = LObjCommand
    '    LObjAdpt.Fill(LObjDS, "Customer")
    '    scoringData.CustomerType = LObjDS.Tables("Customer").Rows(0)("CustomerType").ToString

    '    'di tutup sementara karena belum ada isi product di tabel prospek
    '    'cari CreditScoreSchemeID dari ProductOffering 
    '    'LStrSQL = "spCount_CreditScoring3"
    '    'LObjCommand = New SqlCommand(LStrSQL, GObjCon)
    '    'LObjCommand.CommandType = CommandType.StoredProcedure
    '    'LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
    '    'LObjCommand.Parameters.AddWithValue("@ProductID", LObjDS.Tables("Agreement").Rows(0)("ProductID"))
    '    'LObjCommand.Parameters.AddWithValue("@ProductOfferingID", LObjDS.Tables("Agreement").Rows(0)("ProductOfferingID"))
    '    'LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
    '    'LObjAdpt.SelectCommand = LObjCommand
    '    'LObjAdpt.Fill(LObjDS, "ProductOffering")
    '    'scoringData.CreditScoreSchemeID = LObjDS.Tables("ProductOffering").Rows(0)("CreditScoreSchemeID").ToString

    '    'cari Component Scoring yang sesuai dengan CreditScoreSchemeID        
    '    LStrSQL = "spCount_CreditScoring4"
    '    LObjCommand = New SqlCommand(LStrSQL, GObjCon)
    '    LObjCommand.CommandType = CommandType.StoredProcedure
    '    LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", "SC1")
    '    LObjCommand.Parameters.AddWithValue("@ScoringType", scoringData.CustomerType)
    '    LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
    '    LObjAdpt.SelectCommand = LObjCommand
    '    LObjAdpt.Fill(LObjDS, "CreditScoreSchemeComponent")

    '    'cari dr tbl CreditScoreComponentContent
    '    LStrSQL = "spCount_CreditScoring5"
    '    LObjCommand = New SqlCommand(LStrSQL, GObjCon)
    '    LObjCommand.CommandType = CommandType.StoredProcedure
    '    LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", "SC1")
    '    LObjAdpt = New SqlDataAdapter(LStrSQL, GObjCon)
    '    LObjAdpt.SelectCommand = LObjCommand
    '    LObjAdpt.Fill(LObjDS, "CreditScoreComponentContent")
    '    '----------------------------------

    '    mydatatable = New DataTable("Result")

    '    myDataColumn = New DataColumn
    '    myDataColumn.DataType = System.Type.GetType("System.String")
    '    myDataColumn.ColumnName = "ID"
    '    mydatatable.Columns.Add(myDataColumn)

    '    myDataColumn = New DataColumn
    '    myDataColumn.DataType = System.Type.GetType("System.String")
    '    myDataColumn.ColumnName = "Description"
    '    mydatatable.Columns.Add(myDataColumn)

    '    myDataColumn = New DataColumn
    '    myDataColumn.DataType = System.Type.GetType("System.String")
    '    myDataColumn.ColumnName = "Value"
    '    mydatatable.Columns.Add(myDataColumn)

    '    myDataColumn = New DataColumn
    '    myDataColumn.DataType = System.Type.GetType("System.String")
    '    myDataColumn.ColumnName = "ScoreValue"
    '    mydatatable.Columns.Add(myDataColumn)

    '    myDataColumn = New DataColumn
    '    myDataColumn.DataType = System.Type.GetType("System.String")
    '    myDataColumn.ColumnName = "Weight"
    '    mydatatable.Columns.Add(myDataColumn)

    '    For LIntIndex = 0 To LObjDS.Tables("CreditScoreSchemeComponent").Rows.Count - 1
    '        LObjRowCreditScoreSchemeComponent = LObjDS.Tables("CreditScoreSchemeComponent").Rows(LIntIndex)
    '        scoringData.CreditScoreComponentID = LObjRowCreditScoreSchemeComponent("CreditScoreComponentID").ToString

    '        LObjCommand = New SqlCommand(LObjRowCreditScoreSchemeComponent("SQLCmd").ToString, GObjCon)

    '        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

    '        LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
    '        LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)

    '        LObjDataReader = LObjCommand.ExecuteReader()
    '        LObjDataReader.Read()

    '        If Not IsDBNull(LObjDataReader(0)) Then
    '            LStrResultQuery = Trim(LObjDataReader(0).ToString)
    '        Else
    '            LStrResultQuery = ""
    '        End If

    '        LObjDataReader.Close()
    '        myDataRow = mydatatable.NewRow()
    '        ScoreDesc = LObjRowCreditScoreSchemeComponent("Description").ToString

    '        If LObjRowCreditScoreSchemeComponent("CalculationType").ToString = "R" Then
    '            Try
    '                If Not IsNumeric(LStrResultQuery) Then
    '                    LStrResultQuery = CDbl(LStrResultQuery).ToString
    '                End If

    '                LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & scoringData.CreditScoreComponentID & "' and  ValueFrom <= " & LStrResultQuery & " and ValueTo >= " & LStrResultQuery & "")(0)
    '                ScoreValue = CDbl(LObjRow("ScoreValue"))

    '                If LObjRow("ScoreStatus").ToString.Trim = "F" Then
    '                    myFatalScore = True
    '                    FatalDesc &= ScoreDesc & " - "
    '                End If
    '            Catch ex As Exception
    '                ScoreValue = 0
    '                Exit Try
    '            End Try
    '        Else
    '            Try
    '                LObjRow = LObjDS.Tables("CreditScoreComponentContent").Select("CreditScoreComponentID = '" & scoringData.CreditScoreComponentID & "' and valueContent = '" & LStrResultQuery & "'")(0)
    '                ScoreValue = CDbl(LObjRow("ScoreValue"))
    '                If LObjRow("ScoreStatus").ToString.Trim = "F" Then
    '                    myFatalScore = True
    '                    FatalDesc &= ScoreDesc & " - "
    '                End If
    '            Catch
    '                ScoreValue = 0
    '                Exit Try
    '            End Try
    '        End If

    '        LDblResultCalc += ((CDbl(LObjRowCreditScoreSchemeComponent("Weight")) / 100) * ScoreValue)
    '        LDblGradeTotal += ScoreValue

    '        myDataRow("ID") = scoringData.CreditScoreComponentID
    '        myDataRow("Description") = ScoreDesc
    '        myDataRow("Value") = LStrResultQuery
    '        myDataRow("ScoreValue") = ScoreValue
    '        myDataRow("Weight") = LObjRowCreditScoreSchemeComponent("Weight")
    '        mydatatable.Rows.Add(myDataRow)
    '    Next

    '    If scoringData.CustomerType = "P" Then
    '        LDblApproveScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("PersonalApprovedScore"))
    '        LDblRejectScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("PersonalRejectScore"))
    '    Else
    '        LDblApproveScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("CompanyApprovedScore"))
    '        LDblRejectScore = CDbl(LObjDS.Tables("CreditScoreSchemeComponent").Rows(0)("CompanyRejectScore"))
    '    End If

    '    If myFatalScore = True Then
    '        LStrStatusNote = "Rejected (Fatal Score - " & FatalDesc & ")"
    '        LStrStatus = "R"
    '        LDblResultCalc = 0
    '        scoringData.CSResult_Temp = "F"
    '    ElseIf LDblRejectScore >= LDblResultCalc Then
    '        LStrStatusNote = "Rejected"
    '        LStrStatus = "R"
    '        scoringData.CSResult_Temp = "R"
    '    ElseIf LDblApproveScore > LDblResultCalc And LDblRejectScore < LDblResultCalc Then
    '        LStrStatusNote = "Marginal"
    '        LStrStatus = "M"
    '        scoringData.CSResult_Temp = "M"
    '    ElseIf LDblApproveScore <= LDblResultCalc Then
    '        LStrStatusNote = "Approved"
    '        LStrStatus = "A"
    '        scoringData.CSResult_Temp = "A"
    '    End If

    '    scoringData.DT = mydatatable

    '    ReturnResult = CStr(LDblResultCalc) + " - " + LStrStatusNote

    '    scoringData.CreditScore = CDec(LDblGradeTotal)
    '    scoringData.ReturnResult = ReturnResult

    '    'LStrSQL = "spCreditScoringGrade"
    '    'LObjCommand = New SqlCommand(LStrSQL, GObjCon)
    '    'LObjCommand.CommandType = CommandType.StoredProcedure
    '    'LObjCommand.Parameters.AddWithValue("@GradeValue", scoringData.CreditScore)
    '    'LObjCommand.Parameters.AddWithValue("@CreditScoreSchemeID", scoringData.CreditScoreSchemeID)
    '    'LObjCommand.Parameters.AddWithValue("@CustType", scoringData.CustomerType)
    '    'LObjCommand.Parameters.Add("@GradeDescription", SqlDbType.VarChar, 50)
    '    'LObjCommand.Parameters.Add("@GradeID", SqlDbType.VarChar, 50)
    '    'LObjCommand.Parameters("@GradeDescription").Direction = ParameterDirection.Output
    '    'LObjCommand.Parameters("@GradeID").Direction = ParameterDirection.Output
    '    'LObjCommand.ExecuteNonQuery()
    '    scoringData = SetGradeValue(scoringData)

    '    If GObjCon.State = ConnectionState.Open Then GObjCon.Close()
    '    'Return ReturnResult

    '    Return scoringData
    'End Function

    Public Function CalculateCreditScoringSM(ByVal scoringData As CreditScoring_calculate) As CreditScoring_calculate

        Dim _scoreResults As Parameter.ScoreResults = New Parameter.ScoreResults

        Dim GObjCon As New SqlConnection(scoringData.strConnection)
        Dim LObjDS As New DataSet

        If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

        'Dim LObjCommand = New SqlCommand("spCount_CreditScoringSM", GObjCon)
        'LObjCommand.CommandType = CommandType.StoredProcedure
        'LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
        'LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)
        'Dim LObjAdpt = New SqlDataAdapter("spCount_CreditScoring1", GObjCon)
        'LObjAdpt.SelectCommand = LObjCommand
        'LObjAdpt.Fill(LObjDS, "Agreement")

        'cari tipe Customer (Personal/Company) 
        Dim LObjCommand = New SqlCommand("spCount_CreditScoringSM", GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)
        Dim LObjAdpt = New SqlDataAdapter("spCount_CreditScoringSM", GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "Customer")
        Dim CustomerType = LObjDS.Tables("Customer").Rows(0)("CustomerType").ToString

        'cari CreditScoreSchemeID dari ProductOffering  
        LObjCommand = New SqlCommand("spCount_CreditScoring3", GObjCon)
        LObjCommand.CommandType = CommandType.StoredProcedure
        LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
        LObjCommand.Parameters.AddWithValue("@ProductID", scoringData.ProductID)
        LObjCommand.Parameters.AddWithValue("@ProductOfferingID", scoringData.ProductOfferingID)
        LObjAdpt = New SqlDataAdapter("spCount_CreditScoring3", GObjCon)
        LObjAdpt.SelectCommand = LObjCommand
        LObjAdpt.Fill(LObjDS, "ProductOffering")
        Dim CreditScoreSchemeID = LObjDS.Tables("ProductOffering").Rows(0)("CreditScoreSchemeID").ToString

        Dim _ScoreSchemeComponentList As IList(Of Parameter.ScoreSchemeComponent) = GetScoreSchemeComponents(scoringData.strConnection, CreditScoreSchemeID, CustomerType)
        Dim _ScoreComponentContentList As IList(Of Parameter.ScoreComponentContent) = GetScoreComponentContents(scoringData.strConnection, CreditScoreSchemeID)
        Dim _scoreResult As Parameter.ScoreResult
        Dim comCont As Parameter.ScoreComponentContent
        _scoreResults.SetRejectApproveScore(IIf(CustomerType.Trim = "P", _ScoreSchemeComponentList(0).PersonalApprovedScore, _ScoreSchemeComponentList(0).CompanyApprovedScore),
                                            IIf(CustomerType.Trim.Trim = "P", _ScoreSchemeComponentList(0).PersonalRejectScore, _ScoreSchemeComponentList(0).CompanyRejectScore), _ScoreSchemeComponentList(0).CuttOffScore)
        'cari dr tbl CreditScoreComponentContent 
        _scoreResults.ScoreSchemeID = CreditScoreSchemeID
        For Each item As Parameter.ScoreSchemeComponent In _ScoreSchemeComponentList

            _scoreResult = New Parameter.ScoreResult(scoringData.BranchId, scoringData.ApplicationId, item.ScoreComponentID, item.Description, item.Weight, "CRDITSCORE", item.Description)
            LObjCommand = New SqlCommand(item.SQLCmd, GObjCon)

            If GObjCon.State = ConnectionState.Closed Then GObjCon.Open()

            LObjCommand.Parameters.AddWithValue("@BranchID", scoringData.BranchId)
            LObjCommand.Parameters.AddWithValue("@ApplicationID", scoringData.ApplicationId)


            Dim lObjDataReader = LObjCommand.ExecuteReader()

            Dim lStrResultQuery = "-1"
            If (lObjDataReader.HasRows) Then
                lObjDataReader.Read()
                lStrResultQuery = IIf(IsDBNull(lObjDataReader(0)), "", Trim(lObjDataReader(0).ToString))
            End If


            If IsNumeric(lStrResultQuery) Then
                lStrResultQuery = CDbl(lStrResultQuery)
            End If

            lObjDataReader.Close()

            If item.CalculationType.Trim = "R" Then
                If Not IsNumeric(lStrResultQuery) Then lStrResultQuery = 0
                comCont = _ScoreComponentContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueFrom <= lStrResultQuery And x.ValueTo >= lStrResultQuery).SingleOrDefault()
            Else
                comCont = _ScoreComponentContentList.Where(Function(x) x.ScoreComponentID = item.ScoreComponentID And x.ValueContent = lStrResultQuery.ToString).SingleOrDefault()
            End If

            _scoreResult.ScoreValue = 0
            _scoreResult.ScoreStatus = ""
            _scoreResult.ContentSeqNo = 97
            _scoreResult.QueryResult = lStrResultQuery
            If Not (comCont Is Nothing) Then
                _scoreResult.ScoreStatus = comCont.ScoreStatus.ToString.Trim
                _scoreResult.ScoreValue = comCont.ScoreValue
                _scoreResult.ContentSeqNo = comCont.ContentSeqNo
            End If
            _scoreResults.Add(_scoreResult)
        Next
        getScoreRejectPolicyScoring(scoringData.strConnection, scoringData.BranchId, scoringData.ApplicationId, CreditScoreSchemeID, _scoreResults)

        scoringData.CustomerType = CustomerType
        scoringData.CreditScoreSchemeID = CreditScoreSchemeID
        scoringData.ScoreResults = _scoreResults
        'scoringData.DT = _scoreResults.ToDataTable
        'scoringData.CreditScore = _scoreResults.TotalScore
        'scoringData.CreditScoreResult = _scoreResults.TotalScore
        scoringData.ReturnResult = String.Format("CutOff Score : {0} Total Score : {1}", _scoreResults.CuttOff, CStr(_scoreResults.TotalScore))

        Return scoringData
    End Function
End Module
