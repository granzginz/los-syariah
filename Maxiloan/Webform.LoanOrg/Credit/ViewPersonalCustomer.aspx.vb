﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region

Public Class ViewPersonalCustomer
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oAgreementList As UcAgreementList

#Region "Property"
    Public Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Public Property CustomerType() As String
        Get
            Return CType(viewstate("CustomerType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerType") = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CType(Viewstate("Style"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property
#End Region

    Dim c_customer As New CustomerController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.Style = Request.QueryString("style")
            If Request.QueryString("CustomerID").Trim <> "" Then Me.CustomerID = Request.QueryString("CustomerID")


            Dim oCustomer As New Parameter.Customer

            With oCustomer
                .strConnection = getConnectionString
                .CustomerID = Me.CustomerID
            End With

            Me.CustomerType = c_customer.GetCustomerType(oCustomer)
            If Me.CustomerType.Trim = "C" Then Response.Redirect("ViewCompanyCustomer.aspx?CustomerID=" & CustomerID & "&Style=" & Me.Style)
            oCustomer = New Parameter.Customer
            Dim dt As New DataTable
            Dim CustomerType As String

            With oCustomer
                .strConnection = getConnectionString
                .CustomerID = Me.CustomerID
                .CustomerType = Me.CustomerType
            End With
            oCustomer = c_customer.GetViewCustomer(oCustomer)
            dt = oCustomer.listdata
            With oAgreementList
                .CustomerID = CustomerID
                .Style = Me.Style
                .BindAgreement()
            End With


            If dt.Rows.Count > 0 Then
                lblNamaIbuKandung.Text = CStr(dt.Rows(0).Item("MotherName"))                 
                lblKondisiLingkungan.Text = CStr(dt.Rows(0).Item("KondisiRumah"))
                If IsDate(dt.Rows(0).Item("IDExpiredDate").ToString) And Not dt.Rows(0).Item("IDExpiredDate").ToString.Contains("1900") Then
                    lblBerlakuSampai.Text = Format(dt.Rows(0).Item("IDExpiredDate"), "dd/MM/yyyy")
                Else
                    lblBerlakuSampai.Text = ""
                End If

				lblcustomerID.Text = CStr(dt.Rows(0).Item("CustomerID"))
				lblName.Text = CStr(dt.Rows(0).Item("CustomerName"))
                Me.CustomerName = CStr(dt.Rows(0).Item("CustomerName"))
                lblIDType.Text = CStr(dt.Rows(0).Item("IDType"))
                lblIDNumber.Text = CStr(dt.Rows(0).Item("IDNumber"))
                lblGender.Text = CStr(dt.Rows(0).Item("Gender"))
                lblBirthPlaceDate.Text = CStr(dt.Rows(0).Item("BirthPlace")) & " , " & CStr(dt.Rows(0).Item("BirthDate"))

                lblMobilePhone.Text = CStr(dt.Rows(0).Item("MobilePhone"))
                lblEmail.Text = CStr(dt.Rows(0).Item("Email"))
                lblReligion.Text = CStr(dt.Rows(0).Item("Religion"))
                lblMaritalStatus.Text = CStr(dt.Rows(0).Item("MaritalStatus"))
                lblDependentNumber.Text = CStr(dt.Rows(0).Item("NumOfDependence"))
                lblPersonalNPWP.Text = CStr(dt.Rows(0).Item("PersonalNPWP"))
                lblNoKartuKeluarga.Text = CStr(dt.Rows(0).Item("NoKK"))

                lblEducation.Text = CStr(dt.Rows(0).Item("Education"))
                lblProfession.Text = CStr(dt.Rows(0).Item("Profession"))
                lblNationality.Text = CStr(dt.Rows(0).Item("Nationality"))
                lblWNACountry.Text = CStr(dt.Rows(0).Item("WNACountry"))
                lblGroup.Text = CStr(dt.Rows(0).Item("Group"))
                lblHomeStatus.Text = CStr(dt.Rows(0).Item("HomeStatus"))
                If dt.Rows(0).Item("HomeStatus").ToString.Trim.ToUpper = "KONTRAK" Then                    
                    lblRentFinishDate.Text = CStr(dt.Rows(0).Item("RentFinishDate"))
                Else
                    lblRentFinishDate.Text = ""
                End If

                lblHomeLocation.Text = CStr(dt.Rows(0).Item("HomeLocation"))
                lblHomePrice.Text = FormatNumber(CStr(dt.Rows(0).Item("HomePrice")), 0)
                lblStaySinceYear.Text = CStr(dt.Rows(0).Item("StaySinceYear"))

                lblLegalAddress.Text = CStr(dt.Rows(0).Item("LegalAddress"))
                lblLegalRTRW.Text = CStr(dt.Rows(0).Item("LegalRT")) & "/" & CStr(dt.Rows(0).Item("LegalRW"))
                lblLegalKelurahan.Text = CStr(dt.Rows(0).Item("LegalKelurahan"))
                lblLegalKecamatan.Text = CStr(dt.Rows(0).Item("LegalKecamatan"))
                lblLegalCity.Text = CStr(dt.Rows(0).Item("LegalCity"))
                lblLegalZipCode.Text = CStr(dt.Rows(0).Item("LegalZipCode"))
                lblLegalPhone1.Text = CStr(dt.Rows(0).Item("LegalAreaPhone1")) & "-" & CStr(dt.Rows(0).Item("LegalPhone1"))
                lblLegalPhone2.Text = CStr(dt.Rows(0).Item("LegalAreaPhone2")) & "-" & CStr(dt.Rows(0).Item("LegalPhone2"))
                lblLegalFax.Text = CStr(dt.Rows(0).Item("LegalAreaFax")) & "-" & CStr(dt.Rows(0).Item("LegalFax"))


                lblResidenceAddress.Text = CStr(dt.Rows(0).Item("ResidenceAddress"))
                lblResidenceRTRW.Text = CStr(dt.Rows(0).Item("ResidenceRT")) & "/" & CStr(dt.Rows(0).Item("ResidenceRW"))
                lblResidenceKelurahan.Text = CStr(dt.Rows(0).Item("ResidenceKelurahan"))
                lblResidenceKecamatan.Text = CStr(dt.Rows(0).Item("ResidenceKecamatan"))
                lblResidenceCity.Text = CStr(dt.Rows(0).Item("ResidenceCity"))
                lblResidenceZipCode.Text = CStr(dt.Rows(0).Item("ResidenceZipCode"))
                lblResidencePhone1.Text = CStr(dt.Rows(0).Item("ResidenceAreaPhone1")) & "-" & CStr(dt.Rows(0).Item("ResidencePhone1"))
                lblResidencePhone2.Text = CStr(dt.Rows(0).Item("ResidenceAreaPhone2")) & "-" & CStr(dt.Rows(0).Item("ResidencePhone2"))
                lblResidenceFax.Text = CStr(dt.Rows(0).Item("ResidenceAreaFax")) & "-" & CStr(dt.Rows(0).Item("ResidenceFax"))


                '----------------------------------
                'Employeee Job
                '----------------------------------
                lblJobType.Text = CStr(dt.Rows(0).Item("Job"))
                lblJobTitle.Text = CStr(dt.Rows(0).Item("JobPosition"))
                lblJobCompanyName.Text = CStr(dt.Rows(0).Item("CompanyName"))
                lblBidangUsaha.Text = CStr(dt.Rows(0).Item("Industry"))
                lblJobAddress.Text = CStr(dt.Rows(0).Item("CompanyAddress"))
                lblKategoriPerusahaan.Text = CStr(dt.Rows(0).Item("EXKategoryPers"))
                lblKondisiKantor.Text = CStr(dt.Rows(0).Item("EXKondisiKantor"))
                'lblJobRTRW.Text = CStr(dt.Rows(0).Item("CompanyRT")) & "/" & CStr(dt.Rows(0).Item("CompanyRW"))
                'lblJobKelurahan.Text = CStr(dt.Rows(0).Item("CompanyKelurahan"))
                'lblJobKecamatan.Text = CStr(dt.Rows(0).Item("CompanyKecamatan"))
                lblJobCity.Text = CStr(dt.Rows(0).Item("CompanyCity"))
                'lblJobZipCode.Text = CStr(dt.Rows(0).Item("CompanyZipCode"))
                lblJobPhone1.Text = CStr(dt.Rows(0).Item("CompanyAreaPhone1")) & " " & CStr(dt.Rows(0).Item("CompanyPhone1"))
                'lblJobPhone2.Text = CStr(dt.Rows(0).Item("CompanyAreaPhone2")) & " " & CStr(dt.Rows(0).Item("CompanyPhone2"))
                lblJobFax.Text = CStr(dt.Rows(0).Item("CompanyAreaFax")) & " " & CStr(dt.Rows(0).Item("CompanyFax"))
                lblKetUsaha.Text = CStr(dt.Rows(0).Item("CompanyJobTitle"))
                lblJobEmploymentSince.Text = CStr(dt.Rows(0).Item("EmploymentSinceYear"))
                lblMonthlyFixedIncome.Text = FormatNumber(CStr(dt.Rows(0).Item("MonthlyFixedIncome")), 0)
                lblMonthlyVariableIncome.Text = FormatNumber(CStr(dt.Rows(0).Item("MonthlyVariableIncome")), 0)
                lblEmpOtherBusinessName.Text = CStr(dt.Rows(0).Item("OtherBusinessName"))
                'lblEmpOtherBusinessType.Text = CStr(dt.Rows(0).Item("OtherBusinessType"))
                'lblEmpOtherBusinessIndustryType.Text = CStr(dt.Rows(0).Item("OtherBusinessIndustryType"))
                'lblEmpOtherBusinessJobTitle.Text = CStr(dt.Rows(0).Item("OtherBusinessjobTitle"))
                'lblEmpOtherBusinessSinceYear.Text = CStr(dt.Rows(0).Item("OtherBusinessSinceYear"))

                '-----------------------
                'Data Keuangan
                '-----------------------
                lblBiayaHidup.Text = FormatNumber(CStr(dt.Rows(0).Item("LivingCostAmount")), 0)
                lblDepositoKeuangan.Text = FormatNumber(CStr(dt.Rows(0).Item("Deposito")), 0)
                lblJenisJaminanTambahan.Text = CStr(dt.Rows(0).Item("AdditionalCollateralType"))
                lblNilaiJaminanTambahan.Text = FormatNumber(CStr(dt.Rows(0).Item("AdditionalCollateralAmount")), 0)
                '-----------------------
                'Data Keuangan
                '-----------------------
                lblNamaPasangan.Text = CStr(dt.Rows(0).Item("NamaSuamiIstri"))
                lblPenghasilanTetapPasangan.Text = FormatNumber(CStr(dt.Rows(0).Item("SIPenghasilanTetap")), 0)
                lblPenghasilanTambahanPasangan.Text = FormatNumber(CStr(dt.Rows(0).Item("SIPenghasilanTambahan")), 0)
                lblNamaPerusahaanPasangan.Text = CStr(dt.Rows(0).Item("NamaPersSI"))
                lblBidangUsahaPasangan.Text = CStr(dt.Rows(0).Item("SIJenisIndustriName"))
                lblPekerjaanPasangan.Text = CStr(dt.Rows(0).Item("JobTypeSI"))
                lblKeteranganUsahaPasangan.Text = CStr(dt.Rows(0).Item("NamaSuamiIstri"))
                '-----------------------------
                'Data Professional Data
                '-----------------------------
                lblProfFixIncome.Text = FormatNumber(CStr(dt.Rows(0).Item("MonthlyFixedIncome")), 0)
                lblProfVarIncome.Text = FormatNumber(CStr(dt.Rows(0).Item("MonthlyVariableIncome")), 0)


                '-----------------------------
                'Data Enterpreneur Data
                '-----------------------------
                lblEntBusinessName.Text = CStr(dt.Rows(0).Item("OtherBusinessName"))
                lblEntBusinessType.Text = CStr(dt.Rows(0).Item("OtherBusinessType"))
                lblEntBusinessIndustryType.Text = CStr(dt.Rows(0).Item("OtherBusinessIndustryType"))
                lblEntJobTitle.Text = CStr(dt.Rows(0).Item("OtherBusinessjobTitle"))
                lblEntSinceYear.Text = CStr(dt.Rows(0).Item("OtherBusinessSinceYear"))



                lblFinFixedIncome.Text = FormatNumber(CStr(dt.Rows(0).Item("MonthlyFixedIncome")), 0)
                lblFinVariableIncome.Text = FormatNumber(CStr(dt.Rows(0).Item("MonthlyVariableIncome")), 0)
                lblSpouseIncome.Text = FormatNumber(CStr(dt.Rows(0).Item("SpouseIncome")), 0)
                lblAvgAvgBalance.Text = FormatNumber(CStr(dt.Rows(0).Item("AVGbalanceAccount")), 0)
                lblBankAccountType.Text = CStr(dt.Rows(0).Item("BankAccount"))
                lblAvgDebitTrans.Text = FormatNumber(CStr(dt.Rows(0).Item("AverageDebitTransaction")), 0)
                lblAvgCreditTrans.Text = FormatNumber(CStr(dt.Rows(0).Item("AverageCreditTransaction")), 0)
                lblAvgBalance.Text = FormatNumber(CStr(dt.Rows(0).Item("AverageBalance")), 0)
                lblDeposito.Text = FormatNumber(CStr(dt.Rows(0).Item("Deposito")), 0)
                lblLivingCostAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("LivingCostAmount")), 0)
                lblOtherLoanInstallment.Text = FormatNumber(CStr(dt.Rows(0).Item("OtherLoanInstallment")), 0)
                lblAdditionalCollateralType.Text = CStr(dt.Rows(0).Item("AdditionalCollateralType"))
                lblAdditionalCollateralAmount.Text = FormatNumber(CStr(dt.Rows(0).Item("AdditionalCollateralAmount")), 0)
                lblCreditCard.Text = CStr(dt.Rows(0).Item("CreditCardID"))
                lblCreditCardType.Text = CStr(dt.Rows(0).Item("CreditCardType"))
                lblNumberCreditCard.Text = CStr(dt.Rows(0).Item("NumOfCreditCard"))


                lblBankName.Text = CStr(dt.Rows(0).Item("BankName"))
                lblBankBranch.Text = CStr(dt.Rows(0).Item("BankBranch"))
                lblAccountNo.Text = CStr(dt.Rows(0).Item("AccountNo"))
                lblAccountName.Text = CStr(dt.Rows(0).Item("AccountName"))

                lblReference.Text = CStr(dt.Rows(0).Item("Reference"))
                lblApplyBefore.Text = CStr(dt.Rows(0).Item("isApplyCarLoanBefore"))
                lblNotes.Text = CStr(dt.Rows(0).Item("Notes"))


                CustomerType = CStr(dt.Rows(0).Item("PersonalCustomerType"))
                If CustomerType.Trim = "M" Then
                    PnlProfJobData.Visible = True
                    PnlEmpJobData.Visible = False
                    PnlEntJobData.Visible = False
                End If
                If CustomerType.Trim = "E" Then
                    PnlProfJobData.Visible = False
                    PnlEmpJobData.Visible = False
                    PnlEntJobData.Visible = True
                End If
                If CustomerType.Trim = "M" Then
                    PnlProfJobData.Visible = False
                    PnlEmpJobData.Visible = True
                    PnlEntJobData.Visible = False
                End If


                '------------------------------
                'Data-data di grid
                '------------------------------
                Dim oCustomerEmp As New Parameter.Customer
                Dim oCustomerEmpOmzet As New Parameter.Customer
                Dim oCustomerEmpFamily As New Parameter.Customer

                With oCustomerEmp
                    .strConnection = GetConnectionString()
                    .CustomerID = Me.CustomerID
                End With

                oCustomerEmpOmzet = c_customer.GetViewCustomerEmpOmset(oCustomerEmp)

                'dtgEmpOmset.DataSource = oCustomerEmpOmzet.listdata
                'dtgEmpOmset.DataBind()


                dt = New DataTable
                dt = c_customer.GetViewCustomerEntOmset(oCustomerEmp)
                'dtgEntOmset.DataSource = dt
                'dtgEntOmset.DataBind()

                oCustomerEmpFamily = c_customer.GetViewCustomerFamily(oCustomerEmp)
                dtgFamily.DataSource = oCustomerEmpFamily.listdata
                dtgFamily.DataBind()



            End If
            Get_Link()

        End If
    End Sub
    Private Sub Get_Link()
        HyCustomerExposure.NavigateUrl = "viewCustomerExposure.aspx?CustomerId=" & Me.CustomerID & "&CustomerName=" & Me.CustomerName & "&Style=" & Me.Style
        HyAnalisa.NavigateUrl = "../../Webform.SalesMarketing/Report/AnalisaROViewer.aspx?CustomerId=" & Me.CustomerID & "&CustomerName=" & Me.CustomerName & "&Style=" & Me.Style
    End Sub

    Private Sub ImbPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim cookieNew As New HttpCookie("PersonalCustomerRpt")
        cookieNew.Values.Add("CustomerID", Me.CustomerID)
        cookieNew.Values.Add("LoginID", Me.Loginid)
        Response.AppendCookie(cookieNew)
        Response.Redirect("RptPersonalCustomerViewer.aspx")
    End Sub
End Class