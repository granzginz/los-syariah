﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller

#End Region

Public Class ViewAccrInc
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property


    Property TotalAmount() As Double
        Get
            Return CDbl(viewstate("TotalAmount"))
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalAmount") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property

#End Region
    Dim tempAmount As Double
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ViewAccrInc"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                tempAmount = 0
                Me.ApplicationID = Request("ApplicationId").ToString
                Me.AgreementNo = Request("AgreementNo").ToString
                lblAgreementNo.Text = Me.AgreementNo
                Me.CustomerName = Request("CustomerName").ToString
                lblCustomerName.Text = Me.CustomerName
                Me.Style = Request("Style").ToString
                Me.CustomerID = Request("CustomerID").ToString.Trim
                Bindgrid()
                Dim lb As New LinkButton
                lb = CType(Me.FindControl("lblCustomerName"), LinkButton)
                lb.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")
            End If
        End If
    End Sub
    Sub Bindgrid()
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)

        Dim adapter As New SqlDataAdapter
        Dim objReaderGrid, objReaderLabel As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAccruedIncome"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim

            objReaderLabel = objCommand.ExecuteReader
            If objReaderLabel.Read Then
                lblOSAR.Text = FormatNumber(objReaderLabel("OSAR").ToString, 2)
                lblOSInterest.Text = FormatNumber(objReaderLabel("OSInterest").ToString, 2)
                lblOSPrincipal.Text = FormatNumber(objReaderLabel("OSPrincipal").ToString, 2)
                lblEffectiveRate.Text = objReaderLabel("EffectiveRate").ToString
            End If
            objReaderLabel.Close()

            objReaderGrid = objCommand.ExecuteReader
            dtg.DataSource = objReaderGrid
            dtg.DataBind()
            objReaderGrid.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        'Dim lb As New Label
        'Dim lbTot As New Label

        'If e.Item.ItemIndex >= 0 Then
        '    lb = CType(e.Item.FindControl("lblAccIncome"), Label)
        '    tempAmount = tempAmount + CDbl(lb.Text)
        '    Me.TotalAmount = tempAmount
        '    lbTot = CType(e.Item.FindControl("lblTotAccIncome"), Label)
        '    lbTot.Text = FormatNumber(Me.TotalAmount.ToString, 2)
        'End If

    End Sub
    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    'End Sub

End Class