﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerCompanyEC.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerCompanyEC" %>
<%@ Register Src="CustomerTabsCompany.ascx" TagName="tabs" TagPrefix="uct" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>

<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1"> <ProgressTemplate> <div class="progress_bg"> <label class="progress_img" /> </div> </ProgressTemplate> </asp:UpdateProgress>

<form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
 
    <div runat="server" id="jlookupContent" />
    <uct:tabs id='cTabs' runat='server'></uct:tabs>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
    <div class="form_title">
        <div class="title_strip" ></div>
        <div class="form_single">
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <h4> <asp:Label ID="lblTitle" runat="server"></asp:Label> EMERGENCY CONTACT </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req"> Nama</label>
            <asp:TextBox runat="server" ID="txtNamaPJ" CssClass="long_text"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Harap isi nama!" ControlToValidate="txtNamaPJ" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
    <div class="form_single"> 
            <label class="label_req">Hubungan</label>
            <asp:DropDownList ID="cboHubungan" runat="server"/> 
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator65" runat="server" ControlToValidate="cboHubungan" CssClass="validator_general" ErrorMessage="Harap pilih Hubungan" InitialValue="Select One"></asp:RequiredFieldValidator>    
        </div>
    </div>
    <div class="form_box_uc">
            <uc1:ucaddress id="UCAddressPJ" runat="server"></uc1:ucaddress>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>No. HP</label>
            <asp:TextBox runat="server" ID="txtHP"   MaxLength="15"  CssClass="medium_text" onkeypress="return numbersonly2(event)"></asp:TextBox> 
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label> EMail</label>
            <asp:TextBox runat="server" ID="txtEmail" CssClass="medium_text"></asp:TextBox> 
        </div>
    </div>
    
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</form>
</body>
</html>
