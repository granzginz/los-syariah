﻿<Serializable()>
Public Class CustomerTab
    Public Property Tab As System.Web.UI.HtmlControls.HtmlGenericControl
    Public Property Link As HyperLink
    Public Sub New(t As HtmlGenericControl, l As HyperLink)
        Tab = t
        Link = l
    End Sub
End Class

'Public Class TabHelperModule
'    Private _tabs As Dictionary(Of String, CustomerTab)

'    Public Sub New(tabs As Dictionary(Of String, CustomerTab))
'        _tabs = tabs
'    End Sub


'    Public Sub SetNavigateUrl(page As String, id As String)
'        _tabs("tabIdentitas").Link.NavigateUrl = "CustomerPersonal.aspx?page=" + page + "&id=" + id + "&pnl=tabIdentitas" 
'        _tabs("tabPekerjaan").Link.NavigateUrl = "CustomerPersonalPekerjaan.aspx?page=" + page + "&id=" + id + "&pnl=tabPekerjaan"
'        _tabs("tabKeuangan").Link.NavigateUrl = "CustomerPersonalKeuangan.aspx?page=" + page + "&id=" + id + "&pnl=tabKeuangan"
'        _tabs("tabPasangan").Link.NavigateUrl = "CustomerPersonalPasangan.aspx?page=" + page + "&id=" + id + "&pnl=tabPasangan"
'        _tabs("tabGuarantor").Link.NavigateUrl = "CustomerPersonalGuarantor.aspx?page=" + page + "&id=" + id + "&pnl=tabGuarantor"
'        _tabs("tabEmergency").Link.NavigateUrl = "CustomerPersonalEmergency.aspx?page=" + page + "&id=" + id + "&pnl=tabEmergency"
'        _tabs("tabKeluarga").Link.NavigateUrl = "CustomerPersonalKeluarga.aspx?page=" + page + "&id=" + id + "&pnl=tabKeluarga"
'    End Sub

'    Public Sub RefreshAttr(pnl As String)
'        For Each t In _tabs
'            Dim r = t.Value.Tab
'            r.Attributes.Remove("class")
'            r.Attributes.Add("class", "tab_notselected")
'        Next

'        If pnl = "" Then
'            _tabs("tabIdentitas").Tab.Attributes.Remove("class")
'            _tabs("tabIdentitas").Tab.Attributes.Add("class", "tab_selected")
'        Else
'            _tabs(pnl).Tab.Attributes.Remove("class")
'            _tabs(pnl).Tab.Attributes.Add("class", "tab_selected")
'        End If
'    End Sub
'End Class

