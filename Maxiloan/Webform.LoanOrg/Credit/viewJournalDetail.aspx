﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="viewJournalDetail.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.viewJournalDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>viewJournalDetail</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                JURNAL TRANSAKSI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang
            </label>
            <asp:Label ID="add_lblcompanybranch" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Keterangan
            </label>
            <asp:Label ID="add_lbldesc" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Transaksi
            </label>
            <asp:Label ID="add_lbltransaction" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Reference
            </label>
            <asp:Label ID="add_lblref" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Voucher
            </label>
            <asp:Label ID="add_lbltr_nomor" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Reference
            </label>
            <asp:Label ID="add_lblrefdate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Pre Voucher Date
            </label>
            <asp:Label ID="add_lbltrdate" runat="server"></asp:Label>
        </div>
    </div>
    <asp:Panel ID="PnlPaging" runat="server" Visible="True">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DETAIL JURNAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgIndType" runat="server" OnSortCommand="Sorting" AutoGenerateColumns="False"
                        AllowSorting="True" CssClass="grid_general" ShowFooter="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="account" HeaderText="PERS/CAB/COA"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="account" HeaderText="Account Name"></asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="AddFree" HeaderText="AddFree"></asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="tr_desc" HeaderText="KETERANGAN"></asp:BoundColumn>
                            <asp:BoundColumn DataField="post" HeaderText="D/C" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="D/C">
                                <ItemStyle HorizontalAlign="Right" Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Post") %>'
                                        ID="lblPost">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label runat="server" ID="lblSelilih"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Debit" HeaderStyle-Width="15%" HeaderStyle-CssClass="th_right"
                                ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right">
                                <ItemTemplate>
                                         <asp:Label runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.amountD"), 2) %>'
                                        ID="lblAmountD">
                                    </asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="add_lbltotalD" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Credit" HeaderStyle-Width="15%" HeaderStyle-CssClass="th_right"
                                ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right">
                                <ItemTemplate>
                                          <asp:Label runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.amountC"), 2) %>'
                                        ID="lblAmountC">
                                    </asp:Label>
                                    
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="add_lbltotalC" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH" HeaderStyle-Width="15%" HeaderStyle-CssClass="th_right"
                                ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right" Visible="false">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.amount"), 2) %>'
                                        ID="lblAmount">
                                    </asp:Label>
                                    <asp:TextBox runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.amount"), 2) %>'
                                        ID="amount" Visible="False">
                                    </asp:TextBox>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="add_lbltotal" runat="server"></asp:Label>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" Text='<%#FormatNumber(DataBinder.Eval(Container, "DataItem.amount"), 2) %>'
                                        ID="Textbox4">
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL PAGING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
    <div class="form_button">
        <asp:Button ID="btn_Trans_view_back" runat="server" Text="BACK" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
