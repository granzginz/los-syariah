﻿Public Class CustomerTabsCompany
    Inherits System.Web.UI.UserControl
    Private _tabs As Dictionary(Of String, CustomerTab)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Sub initTabs()
		_tabs = New Dictionary(Of String, CustomerTab)() From {
						   {"tabDataKonsumen", New CustomerTab(tabDataKonsumen, hyDataKonsumen)},
						   {"tabLegalitas", New CustomerTab(tabLegalitas, hyLegalitas)},
						   {"tabManagement", New CustomerTab(tabManagement, hyManagement)},
						   {"tabPenjamin", New CustomerTab(tabPenjamin, hyPenjamin)},
						   {"tabEmergencyCt", New CustomerTab(tabEmergencyCt, hyEmergencyCt)},
						   {"tabKeuangan", New CustomerTab(tabKeuangan, hyKeuangan)},
						   {"tabAnalisaProyeksiKeuangan", New CustomerTab(tabAnalisaProyeksiKeuangan, hyAnalisaProyeksiKeuangan)}
		}
	End Sub
    
    Public Sub SetNavigateUrl(page As String, id As String)
        If Object.ReferenceEquals(_tabs, Nothing) Then
            initTabs()
        End If

        _tabs("tabDataKonsumen").Link.NavigateUrl = "CustomerCompany.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabDataKonsumen"
        _tabs("tabLegalitas").Link.NavigateUrl = "CustomerCompanyLegalitas.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabLegalitas"
        _tabs("tabManagement").Link.NavigateUrl = "CustomerCompanyManagement.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabManagement"
        _tabs("tabPenjamin").Link.NavigateUrl = "CompanyCustomerGuarantor.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabPenjamin"
        _tabs("tabEmergencyCt").Link.NavigateUrl = "CustomerCompanyEC.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabEmergencyCt"
        _tabs("tabKeuangan").Link.NavigateUrl = "CustomerCompanyKeuangan.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabKeuangan"
		_tabs("tabAnalisaProyeksiKeuangan").Link.NavigateUrl = "CustomerCompanyProyeksiKeuangan.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabAnalisaProyeksiKeuangan"

	End Sub
    Public Sub EnabledLink(link As String, enable As Boolean)
        _tabs(link).Link.Enabled = enable
    End Sub

    Public Sub RefreshAttr(pnl As String)
        If Object.ReferenceEquals(_tabs, Nothing) Then
            initTabs()
        End If

        For Each t In _tabs
            Dim r = t.Value.Tab
            r.Attributes.Remove("class")
            r.Attributes.Add("class", "tab_notselected")
        Next

        If pnl = "" Then
            _tabs("tabDataKonsumen").Tab.Attributes.Remove("class")
            _tabs("tabDataKonsumen").Tab.Attributes.Add("class", "tab_selected")
        Else
            _tabs(pnl).Tab.Attributes.Remove("class")
            _tabs(pnl).Tab.Attributes.Add("class", "tab_selected")
        End If
    End Sub
End Class