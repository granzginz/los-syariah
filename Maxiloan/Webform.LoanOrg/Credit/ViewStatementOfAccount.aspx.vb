﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Controller

#End Region

Public Class ViewStatementOfAccount
    Inherits Maxiloan.Webform.WebBased
    Private oApplicationContr As New ApplicationController

    Dim pageSource As String = "ViewStatementOfAccount"

#Region "Property"
    Public Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Public Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CType(viewstate("Style"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property

    Property ApprovalNo() As String
        Get
            Return viewstate("ApprovalNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApprovalNo") = Value
        End Set
    End Property

   

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

		If Not IsPostBack Then
			Me.FormID = "ViewAgreement"

			If Request.QueryString("filekwitansi") <> "" Then
				Dim strHTTPServer As String
				Dim StrHTTPApp As String
				Dim strNameServer As String
				Dim strFileLocation As String

				strHTTPServer = Request.ServerVariables("PATH_INFO")
				strNameServer = Request.ServerVariables("SERVER_NAME")
				StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
				strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("filekwitansi") & ".pdf"
				Response.Write("<script language = javascript>" & vbCrLf _
				& "window.open('" & strFileLocation & "','StatementOfAccount', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes, resizable=yes') " & vbCrLf _
				& "</script>")
			End If

			Me.AgreementNo = Request.QueryString("AgreementNo")
			Me.Style = Request.QueryString("Style")

			Dim objCommand As New SqlCommand
			Dim objConnection As New SqlConnection(GetConnectionString)
			Dim objReader As SqlDataReader

			Try


				If objConnection.State = ConnectionState.Closed Then objConnection.Open()

				objCommand.CommandType = CommandType.StoredProcedure
				objCommand.CommandText = "spViewMKK"
				objCommand.Parameters.Add("@AgreementNo", SqlDbType.VarChar, 20).Value = Me.AgreementNo.Trim
				objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
				objCommand.Connection = objConnection
				objReader = objCommand.ExecuteReader()

				If objReader.Read Then
					Me.CustomerName = CStr(objReader.Item("CustomerName")).Trim
					Me.CustomerID = CStr(objReader.Item("CustomerID")).Trim
					Me.ApplicationID = CStr(objReader.Item("ApplicationID")).Trim

					Me.AgreementNo = CStr(objReader.Item("AgreementNo")).Trim
					Me.BranchID = CStr(objReader.Item("BranchID")).Trim
					Me.ApprovalNo = CStr(objReader.Item("ApprovalNo")).Trim

					If objReader.Item("ApplicationModule").ToString.Trim = "AGRI" Then
						Response.Redirect("../Credit/CreditProcess/ApplicationAgriculture/ViewApplicationAgriculture_003.aspx?ApplicationID=" & Me.ApplicationID & "&CustID=" & Me.CustomerID)
					ElseIf objReader.Item("ApplicationModule").ToString.Trim = "OPLS" Then
						Response.Redirect("../Credit/CreditProcess/OperatingLease/ViewApplicationOperatingLease_003.aspx?ApplicationID=" & Me.ApplicationID & "&CustID=" & Me.CustomerID)
					ElseIf objReader.Item("ApplicationModule").ToString.Trim = "FACT" Then
						Response.Redirect("../Credit/CreditProcess/NewFactoring/ViewApplicationFactoring_003.aspx?ApplicationID=" & Me.ApplicationID & "&CustID=" & Me.CustomerID)
					ElseIf objReader.Item("ApplicationModule").ToString.Trim = "MDKJ" Then
						Response.Redirect("../Credit/CreditProcess/NewModalKerja/ViewApplicationModalKerja_003.aspx?ApplicationID=" & Me.ApplicationID & "&CustID=" & Me.CustomerID)
					End If

					hdCustomerName.Value = CStr(objReader.Item("CustomerName")).Trim
					hdCustomerID.Value = CStr(objReader.Item("CustomerID")).Trim
					hdAplicationID.Value = CStr(objReader.Item("ApplicationID")).Trim
					hdAgreementNo.Value = CStr(objReader.Item("AgreementNo")).Trim
					hdBranchID.Value = CStr(objReader.Item("BranchID")).Trim
					hdApprovalNo.Value = CStr(objReader.Item("ApprovalNo")).Trim
					hdApplicationModule.Value = CStr(objReader.Item("ApplicationModule")).Trim

					lbBranch.Text = CStr(objReader.Item("BranchFullName")).Trim
					lbAgreementNo.Text = Me.AgreementNo
					If Not IsDBNull(objReader.Item("RCADate")) Then lbScoringDate.Text = Format(objReader.Item("RCADate"), "dd/MM/yyyy")
					chkBaru.Checked = Not CBool(objReader.Item("IsRO"))
					chkRO.Checked = CBool(objReader.Item("IsRO"))
					lnkCustomerName.Text = Me.CustomerName
					lbProfesi.Text = CStr(objReader.Item("ProfDesc")).Trim
					lbAlamat.Text = CStr(objReader.Item("LegalAddr")).Trim
					lbJenisUsaha.Text = CStr(objReader.Item("IndsDesc")).Trim
					lbPH.Text = FormatNumber(objReader.Item("NTF"), 2)
					lbDPGross.Text = FormatNumber(objReader.Item("DPGross"), 2)
					lbDPNet.Text = FormatNumber(objReader.Item("DPNett"), 2)
					lbAngs1.Text = FormatNumber(objReader.Item("InstallmentAmount"), 2)
					lbBungaFlat.Text = FormatNumber(objReader.Item("FlatRate"), 2)
					lbBungaEfektif.Text = FormatNumber(objReader.Item("EffectiveRate"), 2)
					lbTenor.Text = objReader.Item("Tenor").ToString
					lbAdm.Text = FormatNumber(objReader.Item("AdminFee"), 2)
					lbOtherFee.Text = FormatNumber(objReader.Item("OtherFee"), 2)
					'lbAsuransi.Text = FormatNumber(objReader.Item("InsAssetPremium"), 2)
					lbAsuransi.Text = FormatNumber(objReader.Item("PremiGross"), 2)

					lbKendaraan.Text = objReader.Item("DescriptionAsset").ToString.Trim
					lbType.Text = objReader.Item("assetType").ToString.Trim
					lbTahun.Text = objReader.Item("ManufacturingYear").ToString.Trim
					lbNilai.Text = FormatNumber(objReader.Item("TotalOTR"), 2)
					lbHargaLaku.Text = FormatNumber(objReader.Item("HargaCepatLaku"), 2)
					lbSR1.Text = objReader.Item("SR1").ToString.Trim
					lbSR2.Text = objReader.Item("SR2").ToString.Trim
					lbSRNilai1.Text = FormatNumber(objReader.Item("SRNilai1"), 2)
					lbSrNilai2.Text = FormatNumber(objReader.Item("SRNilai2"), 2)
					lbTujuan.Text = objReader.Item("AssetUsageDesc").ToString.Trim
					lbLokasi.Text = objReader.Item("lokasi").ToString.Trim
					'lbPakai.Text = objReader.Item("pemakai").ToString.Trim

					lbIncTetap.Text = FormatNumber(objReader.Item("MonthlyFixedIncome"), 2)
					lbIncSuamiIstri.Text = FormatNumber(objReader.Item("SpouseIncome"), 2)
					lbIncTambahan.Text = FormatNumber(objReader.Item("MonthlyVariableIncome"), 2)
					lbIncTotal.Text = FormatNumber(objReader.Item("TotalInc"), 2)
					lbExpTotal.Text = FormatNumber(objReader.Item("LivingCostAmount"), 2)
					lbAngs.Text = FormatNumber(objReader.Item("InstallmentAmount"), 2)
					lbAngsRO.Text = FormatNumber(objReader.Item("InstallmentAmountRO"), 2)
					lbTotalExp.Text = FormatNumber(objReader.Item("TotalCost"), 2)
					lbSisaInc.Text = FormatNumber(objReader.Item("SisaInc"), 2)
					lbRasioAngs.Text = FormatNumber(objReader.Item("rasioAngs"), 2)
					lblRasioSisaInc.Text = FormatNumber(objReader.Item("RasioAngsSisa"), 2)

					lbPend.Text = objReader.Item("Education").ToString.Trim
					lbStsKawin.Text = objReader.Item("MaritalStatusDesc").ToString.Trim
					lbTangungan.Text = objReader.Item("NumOfDependence").ToString.Trim
					lbStsRumah.Text = objReader.Item("HomeStatusDesc").ToString.Trim
					lbMasaSewa.Text = objReader.Item("LamaTinggal").ToString.Trim
					lbNmPerusahaan.Text = objReader.Item("CompanyName").ToString.Trim
					lbBidUsaha.Text = objReader.Item("IndsDesc").ToString.Trim
					lbJabatan.Text = objReader.Item("jobDesc").ToString.Trim
					lbMasaKerja.Text = objReader.Item("MasaKerja").ToString.Trim
					lbGolPerusahaan.Text = objReader.Item("ecoDesc").ToString.Trim
					lbStsMilikPerusahaan.Text = objReader.Item("StsKepemilikan").ToString.Trim

					lbScoring.Text = objReader.Item("ScoringRes").ToString.Trim & "(" & FormatNumber(objReader.Item("CreditScore"), 2) & ")"

					lbCMO.Text = objReader.Item("EmployeeName").ToString.Trim
					lbCreditReview.Text = objReader.Item("CreditReview").ToString.Trim
					lbAppIsFinal.Text = objReader.Item("ApprStatus").ToString.Trim

					lblStatusPinjaman.Text = objReader.Item("FundingPledgeStatus").ToString.Trim
					lblNamaBank.Text = objReader.Item("FundingCoyName").ToString.Trim
					lblTanggalBatch.Text = Format(objReader.Item("FundingBatchDate"), "dd/MM/yyyy")
					lblNoFasilitas.Text = objReader.Item("FundingContractID").ToString.Trim
					lbDefaultStatus.Text = objReader.Item("ContractStatus").ToString.Trim
					lbPrePaid.Text = FormatNumber(objReader.Item("ContractPrepaidAmount"), 2)
					lblCustomerID.Text = objReader.Item("CustomerID")

					Dim lb As New HyperLink
					lb = CType(Me.FindControl("lnkCustomerName"), HyperLink)
					lb.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")

					'lnkCustomerName.NavigateUrl = "javascript:OpenWinCustomer('" & "AccMnt" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
				End If

				objReader.Close()

				If Not IsNothing(Me.ApplicationID) Then
					BindFasilitasSaatIni()
					BindgridApprovalNotes()
					BindgridApprovalHistory()
				End If




			Catch ex As Exception
				'Response.Write(ex.Message)
				ShowMessage(lblMessage, ex.Message, True)
			Finally
				If objConnection.State = ConnectionState.Open Then objConnection.Close()
				objConnection.Dispose()
				objCommand.Dispose()
			End Try

			Dim imb As New Button
			imb = CType(Me.FindControl("btnClose"), Button)
			imb.Attributes.Add("onclick", "window.close()")


		End If
	End Sub



	Private Sub BindFasilitasSaatIni()
        Dim oCustomClass As New Parameter.Application
        Dim dt As New DataTable

        With oCustomClass
            .ApplicationID = Me.ApplicationID
            .CustomerId = Me.CustomerID
            .strConnection = GetConnectionString()
        End With

        Try
            oCustomClass = oApplicationContr.GetViewMKKFasilitas(oCustomClass)
            dt = oCustomClass.ListData
            DtgFasiltas.DataSource = dt.DefaultView
            DtgFasiltas.DataBind()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub BindgridApprovalNotes()
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objconnection
            objCommand.CommandText = "spRCANotes"
            objCommand.Parameters.Add("@ApprovalNo", SqlDbType.VarChar, 50).Value = Me.ApprovalNo
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objread = objCommand.ExecuteReader()

            dtgApprNotes.DataSource = objread
            dtgApprNotes.DataBind()

            objread.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)

        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try

    End Sub

	Private Sub BindgridApprovalHistory()
		Dim objCommand As New SqlCommand
		Dim objconnection As New SqlConnection(GetConnectionString)
		Dim objread As SqlDataReader

		Try
			If objconnection.State = ConnectionState.Closed Then objconnection.Open()

			objCommand.CommandType = CommandType.StoredProcedure
			objCommand.Connection = objconnection
			objCommand.CommandText = "spApprovalHistoryList"
			objCommand.Parameters.Add("@ApprovalNo", SqlDbType.VarChar, 50).Value = Me.ApprovalNo
			objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
			objread = objCommand.ExecuteReader()

			dtgHistory.DataSource = objread
			dtgHistory.DataBind()

			objread.Close()

			If dtgHistory.Items.Count > 0 Then
				dtgHistory.Visible = True
				pnlHistory.Visible = True
			Else
				dtgHistory.Visible = False
				pnlHistory.Visible = False
			End If

		Catch ex As Exception
			Response.Write(ex.StackTrace)

		Finally
			objCommand.Parameters.Clear()
			objCommand.Dispose()
			If objconnection.State = ConnectionState.Open Then objconnection.Close()
			objconnection.Dispose()
		End Try

	End Sub

	'Private Sub btnPrintCustomer_Click(sender As Object, e As System.EventArgs) Handles btnPrintCustomer.Click
	'    If SessionInvalid() Then
	'        Exit Sub
	'    End If

	'    If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
	'        Dim cookie As HttpCookie = Request.Cookies("StatementKartuPiutang")
	'        If Not cookie Is Nothing Then
	'            cookie.Values("ApplicationId") = Me.ApplicationID
	'            cookie.Values("BranchID") = Me.BranchID
	'            cookie.Values("Status") = "C"
	'            Response.AppendCookie(cookie)
	'        Else
	'            Dim cookieNew As New HttpCookie("StatementKartuPiutang")
	'            cookieNew.Values.Add("ApplicationId", Me.ApplicationID)
	'            cookieNew.Values.Add("BranchID", Me.BranchID)
	'            cookieNew.Values.Add("Status", "C")
	'            Response.AppendCookie(cookieNew)
	'        End If
	'    End If
	'    Response.Redirect("Print/StatementKartuPiutangViewer.aspx")
	'End Sub

	Private Sub btnPrintInternal_Click(sender As Object, e As System.EventArgs) Handles btnPrintInternal.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies("StatementKartuPiutang")
            If Not cookie Is Nothing Then
                cookie.Values("ApplicationId") = Me.ApplicationID
                cookie.Values("BranchID") = Me.BranchID
                cookie.Values("AgreementNo") = Me.AgreementNo
                cookie.Values("Status") = "I"
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("StatementKartuPiutang")
                cookieNew.Values.Add("ApplicationId", Me.ApplicationID)
                cookieNew.Values.Add("BranchID", Me.BranchID)
                cookieNew.Values.Add("AgreementNo", Me.AgreementNo)
                cookieNew.Values.Add("Status", "I")
                Response.AppendCookie(cookieNew)
            End If
        End If
        Response.Redirect("Print/StatementKartuPiutangViewer.aspx")
    End Sub
    Private Sub btnPrintEkternal_Click(sender As Object, e As System.EventArgs) Handles btnPrintEkternal.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies("StatementKartuPiutangEks")
            If Not cookie Is Nothing Then
                cookie.Values("ApplicationId") = Me.ApplicationID
                cookie.Values("BranchID") = Me.BranchID
                cookie.Values("AgreementNo") = Me.AgreementNo
                cookie.Values("Status") = "E"
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("StatementKartuPiutangEks")
                cookieNew.Values.Add("ApplicationId", Me.ApplicationID)
                cookieNew.Values.Add("BranchID", Me.BranchID)
                cookieNew.Values.Add("AgreementNo", Me.AgreementNo)
                cookieNew.Values.Add("Status", "E")
                Response.AppendCookie(cookieNew)
            End If
        End If
        Response.Redirect("Print/StatementKartuPiutangEksViewer.aspx")
    End Sub
    Protected Sub btnPrintProfileKonsumen_Click(sender As Object, e As EventArgs) Handles btnPrintProfileKonsumen.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            Dim cookie As HttpCookie = Request.Cookies("PrintProfileKonsumen")
            If Not cookie Is Nothing Then
                cookie.Values("ApplicationId") = Me.ApplicationID
                cookie.Values("BranchID") = Me.BranchID
                cookie.Values("AgreementNo") = Me.AgreementNo
                cookie.Values("Status") = "I"
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("PrintProfileKonsumen")
                cookieNew.Values.Add("ApplicationId", Me.ApplicationID)
                cookieNew.Values.Add("BranchID", Me.BranchID)
                cookieNew.Values.Add("AgreementNo", Me.AgreementNo)
                cookieNew.Values.Add("Status", "I")
                Response.AppendCookie(cookieNew)
            End If
        End If
        Response.Redirect("Print/PrintProfileKonsumenViewer.aspx")
    End Sub
End Class