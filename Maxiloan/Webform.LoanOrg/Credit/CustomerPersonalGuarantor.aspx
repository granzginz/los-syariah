﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerPersonalGuarantor.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerPersonalGuarantor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddressCity" Src="../../Webform.UserController/ucAddressCity.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../../webform.UserController/ucMonthCombo.ascx" TagName="ucMonthCombo" TagPrefix="uc2" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>
<%@ Register Src="CustomerTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  
     <title>Customer Penjamin</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
     <script type='text/javascript' language='javascript'>
         $(function () {
             $(".accordion").accordion();
         });
         $(document).ready(function () {
         });


         function NamaIndustriBuClick(e, h) {
             var cboselect = $('#cboIndrustriHeader').val();
             if (cboselect == 'Select One') return;
             var url = e + cboselect
             OpenJLookup(url, 'Daftar Industri', h); return false;
         }

         function NamaIndustri2Click(e, h) {
             var cboselect = $('#cboIndrustriHeader2').val();
             if (cboselect == 'Select One') return;
             var url = e + cboselect
             OpenJLookup(url, 'Daftar Industri', h); return false;

         }

    </script>
</head>
<body>
  <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress> 


    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    
    <div runat="server" id="jlookupContent" /> 
    <uct:tabs id='cTabs' runat='server'></uct:tabs> 
  

    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>

            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        DATA PENJAMIN
                    </h4>
                </div>
            </div>

             <div class="form_box_header">

             <div class="accordion">

                <h3>Data Penjamin Utama</h3> 
                <div>  
                <asp:Panel ID="pnlDataPasangan" runat="server">
                <div class="form_title"><H4>UMUM</H4></div>
                    <div class="form_box">
                         <div class="form_left"> 
                                <%--<asp:Button ID="btnCopyDataPasangan" CausesValidation="false" runat="server" Text="COPY DATA PASANGAN" CssClass="small buttongo blue label_calc"></asp:Button>--%>
                                <label> Nama Penjamin</label>
                                <asp:TextBox runat="server" ID="txtNamaPasangan"   Width="25%"></asp:TextBox>   
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtNamaPasangan"  runat="server" Display="Dynamic" ErrorMessage="Harap isi Nama" CssClass="validator_general" />                          
                        </div>
                         <div class="form_right"> 
                                <label> Nama Perusahaan/Jenis Usaha</label>
                                <asp:TextBox ID="txtCompanyNameSI" runat="server" Width="50%"></asp:TextBox>     
                        </div>
                    </div>

                    <div class="form_box">
                         <div class="form_left">  
                                <label>Tempat/Tanggal Lahir</label>
                                <asp:TextBox ID="txtBirthPlaceP" runat="server" MaxLength="20"  Width="25%"></asp:TextBox>&nbsp;/ &nbsp;
                                <uc2:ucdatece id="txtTglLahirNew" runat="server" />
                        </div>
                         <%--<div class="form_right">
                            <label> Bidang Usaha</label>
                                      <asp:HiddenField runat="server" ID="hdfKodeIndustriPS" />
                                      <asp:TextBox runat="server" ID="txtNamaIndustriPS" Enabled="false" CssClass="medium_text"></asp:TextBox>
                                      <button class="small buttongo blue" onclick="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriPS.ClientID & "&nama=" & txtNamaIndustriPS.ClientID) %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;"> ...</button>        
                                </div>--%>
                          <div class="form_right">                            
                            <label> Bidang Usaha Header</label>
                            <asp:DropDownList ID="cboIndrustriHeader" runat="server" />
                        </div>
                     </div>
                     <div class="form_box">
                         <div class="form_left">   
                                <label>Jenis Identitas</label>
                                 <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select"/> 
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Jenis Identitas" ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>                           
                            </div>
                            <div class="form_right">
                            <label> Bidang Usaha Detail</label> 
                            <asp:HiddenField runat="server" ID="hdfKodeIndustriBU" /> 
                            <asp:TextBox runat="server" ID="txtNamaIndustriBU" Enabled="false" CssClass="medium_text" text="-"/>
                            <button class="small buttongo blue"  onclick ="NamaIndustriBuClick('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) & "&header=" %>','<%= jlookupContent.ClientID %>');">...</button>
                        </div>
                        </div>
                      <%--modify by ario--%>
                      <div class="form_box">
                            <div class="form_left">  
                                <label class ="label_split_req">No Identitas</label>
                                <asp:TextBox runat="server" ID="txtNoDokument"  Width="25%"></asp:TextBox>     
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNoDokument"
                                ErrorMessage="Harap isi Nomor Identitas" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" Display="Dynamic"
                                ValidationExpression="\d+" ErrorMessage="Harap isi Nomor Identitas dengan Angka"
                                ControlToValidate="txtNoDokument" CssClass="validator_general"></asp:RegularExpressionValidator>                       
                            </div>
                            <div class="form_right">  
                                      <label> Pekerjaan</label>
                                      <asp:DropDownList ID="cboJobTypeSI" runat="server"/> 
                            </div> 
                       </div>

                       <div class="form_box">
                            <div class="form_left">    
                                <label>Masa Berlaku KTP</label>
                                <uc2:ucdatece id="txtMasaBerlakuKTP" runat="server" />  
                            </div>
                            <div class="form_right"> 
                                <label> Keterangan Usaha</label>
                                <asp:TextBox ID="txtKeteranganUsahaSI" runat="server" Width="50%"></asp:TextBox>   
                            </div>  
                        </div>
                       

                    <div class="form_box_uc">
                        <div class="form_left_uc">
                            <asp:UpdatePanel runat="server" ID="ucAddressSIKTP">
                                <ContentTemplate>
                                    <div class="form_box_usercontrol_header">
                                        <h4>ALAMAT KTP</h4>
                                    </div> 
                                    <uc1:ucaddresscity id="ucAddressSI_KTP" runat="server"></uc1:ucaddresscity>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="form_right_uc">
                            <asp:UpdatePanel runat="server" ID="upAddressSI">
                                <ContentTemplate> 
                                    <div class="form_box_usercontrol">                                        
                                        <div class="form_box_usercontrol_header">
                                            <h4>ALAMAT KANTOR</h4>
                                        </div>   
                                    </div>                     
                                    <uc1:ucaddresscity id="ucAddressSI" runat="server"></uc1:ucaddresscity>
                                  <div class="form_single form_box_in_uc "> 
                                            <label>  Beroperasi Sejak</label> 
                                            <asp:TextBox ID="txtBerOperasiSejak" class="small_text" runat="server" MaxLength="4" Columns="7" Text="0" onkeypress="return numbersonly2(event)"></asp:TextBox>    
                                    </div>  
                                    <div class="form_single form_box_in_uc "> 
                                        <label>Masa Kerja</label>
                                        <uc1:ucnumberformat id="txtMasaKerja" runat="server"></uc1:ucnumberformat> tahun
                                    </div>   
                                    
                                    <div class="form_single form_box_in_uc "> 
                                        <label>Usaha Lainnya</label>
                                        <asp:TextBox ID="txtUsahaLainnya" runat="server"  Width="50%"/> 
                                </div> 
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>                                        

                </asp:Panel>
                  
                </div>
             
                  <h3>Data Penjamin Kedua</h3> 
                  <div>
                  
                  <asp:Label ID="lblPenjaminMessage"  runat="server">Silahkan Input Penjamin Utama</asp:Label>

                        <asp:Panel ID="pnlDataPasangan2" runat="server">
                        <div class="form_title"><H4>UMUM</H4></div>

                         <div class="form_box">
                             <div class="form_left">  
                                    <label> Nama Penjamin</label>
                                    <asp:TextBox runat="server" ID="txtNamaPasangan2"   Width="25%"></asp:TextBox> 
                                     
                            </div>
                             <div class="form_right"> 
                                    <label> Nama Perusahaan/Jenis Usaha</label>
                                    <asp:TextBox ID="txtCompanyNameSI2" runat="server" Width="50%"></asp:TextBox>     
                            </div>
                         </div>

                          <div class="form_box">
                         <div class="form_left">  
                                <label>Tempat/Tanggal Lahir</label>
                                <asp:TextBox ID="txtBirthPlaceP2" runat="server" MaxLength="20"  Width="25%"></asp:TextBox>&nbsp;/ &nbsp;
                                <uc2:ucdatece id="txtTglLahirNew2" runat="server" />
                        </div>
                        <%-- <div class="form_right">
                                <label> Bidang Usaha</label>
                                <asp:HiddenField runat="server" ID="hdfKodeIndustriPS2" />
                                <asp:TextBox runat="server" ID="txtNamaIndustriPS2" Enabled="false" CssClass="medium_text"></asp:TextBox>
                                <button class="small buttongo blue" onclick="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriPS2.ClientID & "&nama=" & txtNamaIndustriPS2.ClientID) %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;"> ...</button>        
                          </div>--%>
                          <div class="form_right">                            
                            <label> Bidang Usaha Header</label>
                            <asp:DropDownList ID="cboIndrustriHeader2" runat="server" />
                        </div>
                     </div>

                      <div class="form_box">
                         <div class="form_left">   
                                <label>Jenis Dokumen</label>
                                 <asp:DropDownList ID="cboIDTypeP2" runat="server" CssClass="select"/> 
                            </div>
                            <div class="form_right">
                            <label> Bidang Usaha Detail</label> 
                            <asp:HiddenField runat="server" ID="hdfKodeIndustriPS2" /> 
                            <asp:TextBox runat="server" ID="txtNamaIndustriPS2" Enabled="false" CssClass="medium_text" text="-"/>
                            <button class="small buttongo blue"  onclick ="NamaIndustri2Click('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriPS2.ClientID & "&nama=" & txtNamaIndustriPS2.ClientID) & "&header=" %>','<%= jlookupContent.ClientID %>');">...</button>
                        </div>
                           
                        </div>
                             <%--modify by ario--%>
                          <div class="form_box">
                            <div class="form_left">  
                                <label class="label<%--_req--%>">No Dokument</label>
                                <asp:TextBox runat="server" ID="txtNoDokument2"  Width="25%"></asp:TextBox>  
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNoDokument2"
                                ErrorMessage="Harap isi Nomor Identitas" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" Display="Dynamic"
                                ValidationExpression="\d+" ErrorMessage="Harap isi Nomor Identitas dengan Angka"
                                ControlToValidate="txtNoDokument2" CssClass="validator_general"></asp:RegularExpressionValidator>--%>                           
                            </div>
                             <div class="form_right">  
                                      <label> Pekerjaan</label>
                                      <asp:DropDownList ID="cboJobTypeSI2" runat="server"/> 
                            </div> 
                       </div>
                       
                       <div class="form_box">
                            <div class="form_left">    
                                <label>Masa Berlaku KTP</label>
                                <uc2:ucdatece id="txtMasaBerlakuKTP2" runat="server" />  
                            </div>
                            <div class="form_right"> 
                                <label> Keterangan Usaha</label>
                                <asp:TextBox ID="txtKeteranganUsahaSI2" runat="server" Width="50%"></asp:TextBox>   
                            </div>  
                        </div>


                         <div class="form_box_uc">
                        <div class="form_left_uc">
                            <asp:UpdatePanel runat="server" ID="ucAddressSIKTP2">
                                <ContentTemplate>
                                    <div class="form_box_usercontrol_header">
                                        <h4>ALAMAT KTP</h4>
                                    </div> 
                                    <uc1:ucaddresscity id="ucAddressSI_KTP2" runat="server"></uc1:ucaddresscity>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>


                         <div class="form_right_uc">
                            <asp:UpdatePanel runat="server" ID="upAddressSI2">
                                <ContentTemplate> 
                                    <div class="form_box_usercontrol">                                        
                                        <div class="form_box_usercontrol_header">
                                            <h4>ALAMAT KANTOR</h4>
                                        </div>   
                                    </div>                     
                                    <uc1:ucaddresscity id="ucAddressSI2" runat="server"></uc1:ucaddresscity>
                                  <div class="form_single form_box_in_uc "> 
                                            <label>  Beroperasi Sejak</label> 
                                            <asp:TextBox ID="txtBerOperasiSejak2" class="small_text" runat="server" MaxLength="4" Columns="7" Text="0" onkeypress="return numbersonly2(event)"></asp:TextBox>    
                                    </div>  
                                    <div class="form_single form_box_in_uc "> 
                                        <label>Masa Kerja</label>
                                        <uc1:ucnumberformat id="txtMasaKerja2" runat="server"></uc1:ucnumberformat> tahun
                                    </div>   
                                    
                                    <div class="form_single form_box_in_uc "> 
                                        <label>Usaha Lainnya</label>
                                        <asp:TextBox ID="txtUsahaLainnya2" runat="server"  Width="50%"/> 
                                </div> 
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>  
                     </asp:Panel>

                     </div>
                  
              </div>
                   
            </div>

            <div class="form_button">
                <asp:Button ID="btnPSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnPCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <style>
    .form_box_in_uc 
    {
        margin-left:0px !important;
        width:98% !important;
         border-left:none !important;
         border-right:none !important; 
         border-bottom: 1px solid #e5e5e5;
    }
  .ui-widget {
    font-family: Open Sans;
    font-size: 10pt;
  }
  .small_text {width:100px;}
.ui-accordion-content{padding:0px !important; }  
    </style>
    </form>
</body>
</html>