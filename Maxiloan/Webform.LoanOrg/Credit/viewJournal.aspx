﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="viewJournal.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.viewJourna" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Jurnal</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="PnlPaging" runat="server" Visible="True">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR JURNAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgIndType" runat="server" Width="100%" CssClass="grid_general"
                        AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True"
                        DataKeyField="tr_nomor">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <FooterStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="transactionid" SortExpression="transactionid" HeaderText="TRANSAKSI">
                                <ItemStyle Width="20%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="tr_nomor" HeaderText="NO VOUCHER">
                                <ItemStyle Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkBatchNo" runat="server" CommandName="ShowView" Text='<%# Container.DataItem("tr_nomor")%>'
                                        CausesValidation="False">
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                              <%--<asp:BoundColumn DataField="CustomerName" HeaderText="Customer" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">--%>
                              <asp:BoundColumn DataField="CustomerName" HeaderText="CUSTOMER">
                               <ItemStyle Width="20%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="tr_date" HeaderText="TANGGAL">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_date" runat="server">
													<%# format(Container.DataItem("tr_date"),"dd MMM yyyy")%>
                                    </asp:Label>
                                    <input type="hidden" id="hid_date" runat="server" value='<%# container.dataitem("tr_date")%>'
                                        name="hid_date" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="tr_desc" HeaderText="KETERANGAN">
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="status" HeaderText="STATUS">
                                <ItemStyle Width="5%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="reff_no" HeaderText="NO REFF" ItemStyle-Width="20%">
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False"></PagerStyle>
                    </asp:DataGrid>
                    <!--<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<PANEL PAGING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>-->
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="imbGoPage" runat="server"  CssClass="buttongo small blue" Text="Go"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" ErrorMessage="No Halaman Salah"
                            CssClass="validator_general" Display="Dynamic" Type="Integer" MaximumValue="999999999"
                            MinimumValue="1"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                            ErrorMessage="No Halaman Salah" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotrec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
      
    </asp:Panel>
    </form>
</body>
</html>
