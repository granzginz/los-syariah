﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class KoreksiIdentitasCustomer
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents txtIDTglBerlakuNew As ucDateCE
    Protected WithEvents txtTglLahirNew As ucDateCE

    Private m_controller As New CustomerController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property DataTableCases() As DataTable
        Get
            Return CType(ViewState("DataTableCases"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataTableCases") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            FillCbo()
            'If Not Me.IsHoBranch Then
            '    Response.Redirect("Customer.aspx")
            'End If

            Me.FormID = "CUSTCORR"
            Me.CustomerID = Request("id")
            fillCboIDType()
            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            BindEdit()

            txtIDTglBerlakuNew.IsRequired = True
            txtTglLahirNew.IsRequired = True

            'End If

        End If

    End Sub
#Region "FillCboIDType"
    Sub fillCboIDType()
        Dim oCustomer As New Parameter.Customer
        Dim dtList As DataTable
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.CustomerPersonalGetIDType(oCustomer)
        dtList = oCustomer.listdata
        cboIDTypeP.DataSource = dtList
        cboIDTypeP.DataTextField = "Description"
        cboIDTypeP.DataValueField = "ID"
        cboIDTypeP.DataBind()
        cboIDTypeP.Items.Insert(0, "Select One")
        cboIDTypeP.Items(0).Value = "Select One"
    End Sub
#End Region
    Sub BindEdit()
        Dim dtEntity As New DataTable
        Dim dtOmset As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = Me.CustomerID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "1")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        txtNameP.Text = oRow("Name").ToString.Trim        
        cboIDTypeP.SelectedIndex = cboIDTypeP.Items.IndexOf(cboIDTypeP.Items.FindByValue(oRow("IDType").ToString.Trim))
        txtIDNumberP.Text = oRow("IDNumber").ToString.Trim
        If Not IsDBNull(oRow("BirthDate")) Then
            txtTglLahirNew.Text = IIf(Day(CDate(oRow("BirthDate"))).ToString.Trim.Length = 1, "0", "").ToString + Day(CDate(oRow("BirthDate"))).ToString.Trim + "/" + IIf(Month(CDate(oRow("BirthDate"))).ToString.Trim.Length = 1, "0", "").ToString + Month(CDate(oRow("BirthDate"))).ToString.Trim + "/" + Year(CDate(oRow("BirthDate"))).ToString.Trim
        Else
            txtTglLahirNew.Text = ""
        End If

        If Not IsDBNull(oRow("IDExpiredDate")) Then
            txtIDTglBerlakuNew.Text = IIf(Day(CDate(oRow("IDExpiredDate"))).ToString.Trim.Length = 1, "0", "").ToString + Day(CDate(oRow("IDExpiredDate"))).ToString.Trim + "/" + IIf(Month(CDate(oRow("IDExpiredDate"))).ToString.Trim.Length = 1, "0", "").ToString + Month(CDate(oRow("IDExpiredDate"))).ToString.Trim + "/" + Year(CDate(oRow("IDExpiredDate"))).ToString.Trim
        Else
            txtIDTglBerlakuNew.Text = ""
        End If

        txtBirthPlaceP.Text = oRow("BirthPlace").ToString.Trim
        cboPMarital.SelectedIndex = cboPMarital.Items.IndexOf(cboPMarital.Items.FindByValue(oRow("MaritalStatus").ToString.Trim))
        txtPMotherName.Text = oRow("MotherName").ToString.Trim
        txtNamaPasangan.Text = oRow("SINamaSuamiIstri").ToString.Trim
        txtPNPWP.Text = oRow("PersonalNPWP").ToString.Trim
        txtPNoKK.Text = oRow("NoKK").ToString.Trim        
        cboGenderP.SelectedIndex = cboGenderP.Items.IndexOf(cboGenderP.Items.FindByValue(oRow("Gender").ToString.Trim))

        ScriptManager.RegisterStartupScript(cboPMarital, GetType(Page), cboPMarital.ClientID, String.Format("cboPMarital_IndexChanged('{0}');", cboPMarital.SelectedValue.Trim), True)
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim customClass As New Parameter.Customer
        Dim c_PersonalCustomer As New PersonalCustomerIdentitasHistoryController
        Dim ErrMessage As String = ""

        Dim tglLahir As Date = ConvertDate2(txtTglLahirNew.Text)
        If tglLahir >= Me.BusinessDate Then
            ShowMessage(lblMessage, "Tanggal Lahir harus lebih Kecil dari hari ini", True)
            Exit Sub
        End If
        'condition 17 tahun
        Dim GScont As New DataUserControlController
        Dim GSdata As New Parameter.GeneralSetting
        GSdata.GSID = "AGEMIN"
        Dim jmlTahun As Double = Math.Round(Me.BusinessDate.Subtract(tglLahir).TotalDays / 365, 0)
        Dim minAge As String = GScont.GetGeneralSetting(GetConnectionString, GSdata)

        GSdata.GSID = "AGEMAX"
        Dim maxAge As String = GScont.GetGeneralSetting(GetConnectionString, GSdata)
        If jmlTahun > CDbl(maxAge) Or jmlTahun < CDbl(minAge) Then
            ShowMessage(lblMessage, "Umur harus antara " & minAge & " dan " & maxAge, True)
            Exit Sub
        End If

        If ConvertDate2(txtIDTglBerlakuNew.Text) <= Me.BusinessDate Then
            ShowMessage(lblMessage, "Identitas sudah tidak berlaku!", True)
            Exit Sub
        End If

        With customClass            
            .CustomerID = Me.CustomerID
            .Name = txtNameP.Text.Trim
            .IDType = cboIDTypeP.SelectedValue
            .IDNumber = txtIDNumberP.Text.Trim
            .IDExpiredDate = ConvertDate2(txtIDTglBerlakuNew.Text)
            '.IDExpiredDate = Date.ParseExact(txtIDTglBerlakuNew.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .BirthPlace = txtBirthPlaceP.Text.Trim
            .TglLahir = ConvertDate2(txtTglLahirNew.Text)
            '.TglLahir = Date.ParseExact(txtTglLahirNew.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .MotherName = txtPMotherName.Text.Trim
            .NamaSuamiIstri = txtNamaPasangan.Text.Trim
            .PersonalNPWP = txtPNPWP.Text.Trim
            .NoKK = txtPNoKK.Text
            .Notes = txtAlasanKoreksi.Text
            .MaritalStatus = cboPMarital.SelectedValue.Trim
            .strConnection = GetConnectionString()
            .Gender = cboGenderP.SelectedValue.Trim
        End With

        c_PersonalCustomer.PersonalCustomerIdentitasHistorySaveAdd(customClass)
        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Response.Redirect("Customer.aspx")
    End Sub    
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("Customer.aspx")
    End Sub
    Sub FillCbo()
        Dim dtEntity As DataTable
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblMaritalStatus"
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        cboPMarital.DataSource = dtEntity.DefaultView
        cboPMarital.DataTextField = "Description"
        cboPMarital.DataValueField = "ID"
        cboPMarital.DataBind()
        cboPMarital.Items.Insert(0, "Select One")
        cboPMarital.Items(0).Value = ""
    End Sub
End Class