﻿Public Class CustomerTabs
    Inherits CustomerTabsBase
    ' Private _tabs As Dictionary(Of String, CustomerTab)  
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
        End If
    End Sub
    Protected Overrides Sub initTabs()
        _tabs = New Dictionary(Of String, CustomerTab) From {
                           {"tabIdentitas", New CustomerTab(tabIdentitas, hyIdentitas)},
                           {"tabPekerjaan", New CustomerTab(tabPekerjaan, hyPekerjaan)},
                           {"tabKeuangan", New CustomerTab(tabKeuangan, hyKeuangan)},
                           {"tabPasangan", New CustomerTab(tabPasangan, hyPasangan)},
                           {"tabGuarantor", New CustomerTab(tabGuarantor, hyGuarantor)},
                           {"tabEmergency", New CustomerTab(tabEmergency, hyEmergency)},
                           {"tabKeluarga", New CustomerTab(tabKeluarga, hyKeluarga)},
                           {"tabPinjamanLainnya", New CustomerTab(tabPinjamanLainnya, hyPinjamanLainnya)}}
    End Sub
  
    Public Overrides Sub SetNavigateUrl(page As String, id As String) 
        MyBase.SetNavigateUrl(page, id) 
        _tabs("tabIdentitas").Link.NavigateUrl = "CustomerPersonal.aspx?page=" + page + "&id=" + id + "&pnl=tabIdentitas"
        _tabs("tabPekerjaan").Link.NavigateUrl = "CustomerPersonalPekerjaan.aspx?page=" + page + "&id=" + id + "&pnl=tabPekerjaan"
        _tabs("tabKeuangan").Link.NavigateUrl = "CustomerPersonalKeuangan.aspx?page=" + page + "&id=" + id + "&pnl=tabKeuangan"
        _tabs("tabPasangan").Link.NavigateUrl = "CustomerPersonalPasangan.aspx?page=" + page + "&id=" + id + "&pnl=tabPasangan"
        _tabs("tabGuarantor").Link.NavigateUrl = "CustomerPersonalGuarantor.aspx?page=" + page + "&id=" + id + "&pnl=tabGuarantor"
        _tabs("tabEmergency").Link.NavigateUrl = "CustomerPersonalEmergency.aspx?page=" + page + "&id=" + id + "&pnl=tabEmergency"
        _tabs("tabKeluarga").Link.NavigateUrl = "CustomerPersonalKeluarga.aspx?page=" + page + "&id=" + id + "&pnl=tabKeluarga"
        _tabs("tabPinjamanLainnya").Link.NavigateUrl = "CustomerPersonalPinjamanLainnya.aspx?page=" + page + "&id=" + id + "&pnl=tabPinjamanLainnya"

    End Sub
    'Public Sub EnabledLink(link As String, enable As Boolean)
    '    _tabs(link).Link.Enabled = enable
    'End Sub

    Public Overrides Sub RefreshAttr(pnl As String)
        MyBase.RefreshAttr(pnl) 
        If pnl = "" Then
            _tabs("tabIdentitas").Tab.Attributes.Remove("class")
            _tabs("tabIdentitas").Tab.Attributes.Add("class", "tab_selected") 
        End If
    End Sub
End Class