﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="STSAPK.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.STSAPK" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Status Aplikasi</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script type="text/javascript">
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="STSAPK" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <asp:Panel ID="pnlList" runat="server" Width="100%">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    Status Aplikasi
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="1500px" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderWidth="0" CellPadding="3" CellSpacing="1" DataKeyField="ApplicationID"
                        CssClass="grid_ws">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO APLIKASI" SortExpression="ApplicationID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkApplicationID" CausesValidation="False" CommandName="View"
                                        runat="server" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO KONTRAK" SortExpression="agreementno">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" CausesValidation="False" CommandName="View" runat="server"
                                        Text='<%# DataBinder.eval(Container,"DataItem.agreementno") %>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA KONSUMEN" SortExpression="CustomerName">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkCustomerName" CausesValidation="False" CommandName="View" runat="server"
                                        Text='<%# DataBinder.eval(Container,"DataItem.CustomerName") %>'>
                                   </asp:LinkButton>
                                    <asp:Label ID="lblCustomerID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="StatusKontrak" HeaderText="STEP APP"  />   
                            <asp:TemplateColumn HeaderText="APK">
                                <ItemTemplate>
                                    <asp:Label ID="lblAPKStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntryApplicationData") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgApkStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ASSET">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.dateEntryAssetData") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgAssetStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ASUR">
                                <ItemTemplate>
                                    <asp:Label ID="lblAsuransiStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.dateEntryInsuranceData") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgAsuransiStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FIN">
                                <ItemTemplate>
                                    <asp:Label ID="lblFinancialStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntryFinancialData") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgFinancialStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="FIN2">
                                <ItemTemplate>
                                    <asp:Label ID="lblFinancialStat2" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntryFinancialData2") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgFinancialStat2" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REF">
                                <ItemTemplate>
                                    <asp:Label ID="lblRefundStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntryIncentiveData") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgRefundStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SUR">
                                <ItemTemplate>
                                    <asp:Label ID="lblSurvayStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.DateEntryHasilSurvey") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgSurvayStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="RCA">
                                <ItemTemplate>
                                    <asp:Label ID="lblRCAStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.RCADate") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgRcaStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CurrentApprovalStep" HeaderText="APPROVAL STEP" />                    
                          
                            <asp:TemplateColumn HeaderText="PO">
                                <ItemTemplate>
                                    <asp:Label ID="lblPOStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.PurchaseOrderDate") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgPOstat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DO">
                                <ItemTemplate>
                                    <asp:Label ID="lblDOStat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.DeliveryOrderDate") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgDOStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="LIV">
                                <ItemTemplate>
                                    <asp:Label ID="lblLIVtat" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.GoliveDate") %>'>
                                    </asp:Label>
                                    <asp:image ID="imgLIVStat" runat="server"></asp:image>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:BoundColumn DataField="DisbStat" HeaderText="DISB STATUS"  />  
                            <asp:TemplateColumn HeaderText="PVStatus" SortExpression="PVStatus" visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblPVStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PVStatus") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="BranchID" visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ReturnStatus" HeaderText="STATUS RETURN">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblReturnStatus" Text='<%# DataBinder.eval(Container,"DataItem.ReturnStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CurrentApprovalStepName" HeaderText="DARI / KE">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblCurrentApprovalStepName" Text='<%# DataBinder.eval(Container,"DataItem.CurrentApprovalStepName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationStepDesc" HeaderText="STEP APLIKASI">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblApplicationStepDesc" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationStepDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
       
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI STATUS APPLIKASI
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cabang</label>
                <asp:DropDownList ID="cboBranch" runat="server">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="agreement.ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="agreement.agreementno">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name" Selected="True">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" CausesValidation="false" CssClass="small button blue"
                Text="Search"></asp:Button>
            <asp:Button ID="imbButtonReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
