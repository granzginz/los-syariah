﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class AgreementCancel
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property Filter() As String
        Get
            Return CType(ViewState("Filter"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Filter") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New GeneralPagingController

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "CrtInqAgrCancel"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                If Request.QueryString("strFileLocation") <> "" Then
                    Dim strFileLocation As String
                    strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; var y = screen.height;  window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                    & "</script>")
                End If

                txtActivityDate1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                txtActivityDate2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                FillCbo()
                InitialDefaultPanel()
            End If
        End If
    End Sub
#End Region

#Region "FillCbo"
    Sub FillCbo()
        Dim dtbranch As New DataTable
        dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboParent
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
        'Response.Write(GenerateScript(dtChild))
    End Sub

#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
    End Sub
#End Region

#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click

        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            Me.SearchBy = "BranchID='" & cboParent.SelectedItem.Value.Trim & "'"
            Me.SearchBy = Me.SearchBy & " and ContractStatus='" & cboStatus.SelectedItem.Value.Trim & "'"
            Me.Filter = Me.SearchBy

            If txtDateEntry.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and DateEntryApplicationData = '" & ConvertDate2(txtDateEntry.Text).ToString("yyyyMMdd") & "'"
            End If

            If txtActivityDate1.Text.Trim <> "" And txtActivityDate2.Text.Trim <> "" Then
                If cboStatus.SelectedItem.Value.Trim = "CAN" Then
                    Me.SearchBy = Me.SearchBy & " and (CancellationDate >= convert(datetime, left(convert(varchar(20), '" & ConvertDate2(txtActivityDate1.Text).ToString("MM/dd/yyyy") & "'),11),101))"
                    Me.SearchBy = Me.SearchBy & " and CancellationDate <= dateadd(d, 1, convert(datetime, left(convert(varchar(20), '" & ConvertDate2(txtActivityDate2.Text).ToString("MM/dd/yyyy") & "'),11),101))"
                    Me.Filter = Me.Filter & " And CancellationDate Between '" & ConvertDate2(txtActivityDate1.Text) & "' and '" & ConvertDate2(txtActivityDate2.Text) & "' "

                Else
                    Me.SearchBy = Me.SearchBy & " and (RejectionDate >= convert(datetime, left(convert(varchar(20), '" & ConvertDate2(txtActivityDate1.Text).ToString("MM/dd/yyyy") & "'),11),101))"
                    Me.SearchBy = Me.SearchBy & " and RejectionDate <= dateadd(d, 1, convert(datetime, left(convert(varchar(20), '" & ConvertDate2(txtActivityDate2.Text).ToString("MM/dd/yyyy") & "'),11),101))"
                    Me.Filter = Me.Filter & " And RejectionDate Between '" & ConvertDate2(txtActivityDate1.Text) & "' and '" & ConvertDate2(txtActivityDate2.Text) & "' "
                End If
            End If

            If txtSearchBy.Text.Trim <> "" Then
                If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
                    Me.Filter = Me.Filter & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
                Else
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
                    Me.Filter = Me.Filter & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
                End If
            End If
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .SpName = "spPagingAgrCancel"
            .strConnection = GetConnectionString()
            .WhereCond1 = cboStatus.SelectedValue.Trim
            .WhereCond2 = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.GetGeneralPagingWithTwoWhereCondition(oCustomClass)

        DtUserList = oCustomClass.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgActivity.DataSource = DvUserList

        Try
            dtgActivity.DataBind()
        Catch
            dtgActivity.CurrentPageIndex = 0
            dtgActivity.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub
#End Region

#Region "Reset"
    Private Sub ImbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click

        txtActivityDate1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtActivityDate2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        'cboSearchBy.ClearSelection()
        'txtSearchBy.Text = ""
        'cboActyivityType.ClearSelection()
        'cboChild.ClearSelection()
        'InitialDefaultPanel()
        'FillCbo()
        Response.Redirect("AgreementCancel.aspx")
    End Sub
#End Region
#Region "Databound"
    Private Sub dtgActivity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgActivity.ItemDataBound
        Dim lnkApp As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lnkApp = CType(e.Item.FindControl("lblApplicationID"), HyperLink)
            lnkApp.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & lnkApp.Text & "')"

            Dim lblCustId As Label
            Dim lnkCust As HyperLink
            lblCustId = CType(e.Item.FindControl("lblCustomerID"), Label)
            lnkCust = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & lblCustId.Text & "' )"

            'lnkCust.Attributes.Add("OnClick", "return OpenCust('" & e.Item.Cells(4).Text & "','AccAcq');")
            Dim lnkAgr As HyperLink
            lnkAgr = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lnkAgr.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & lnkApp.Text.Trim & "')"

            'lnkPO.Attributes.Add("OnClick", "return OpenWinProductOfferingBranchView('" & e.Item.Cells(6).Text & "','" & e.Item.Cells(7).Text & "','" & e.Item.Cells(8).Text & "','AccAcq');")
        End If
    End Sub
#End Region

#Region "View Report"
    Private Sub imbViewReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonViewReport.Click
        Dim cookie As HttpCookie = Request.Cookies("AgreementCancel")
        '===============
        If CheckFeature(Me.Loginid, Me.FormID, "print", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/error_notauthorized.aspx")
            Else
                If Not cookie Is Nothing Then
                    cookie.Values("BranchID") = cboParent.SelectedItem.Value
                    cookie.Values("BranchName") = cboParent.SelectedItem.Text
                    cookie.Values("LoginID") = Me.Loginid
                    cookie.Values("Status") = cboStatus.SelectedValue
                    cookie.Values("Filter") = Me.Filter
                    cookie.Values("SearchBy") = Me.SearchBy
                    cookie.Values("DateFrom") = ConvertDateVB(txtActivityDate1.Text)
                    cookie.Values("DateTo") = ConvertDateVB(txtActivityDate2.Text)
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("AgreementCancel")
                    cookieNew.Values.Add("BranchID", cboParent.SelectedItem.Value)
                    cookieNew.Values.Add("BranchName", cboParent.SelectedItem.Text)
                    cookieNew.Values.Add("LoginID", Me.Loginid)
                    cookieNew.Values.Add("Status", cboStatus.SelectedValue)
                    cookieNew.Values.Add("Filter", Me.Filter)
                    cookieNew.Values.Add("SearchBy", Me.SearchBy)
                    cookieNew.Values.Add("DateFrom", ConvertDateVB(txtActivityDate1.Text))
                    cookieNew.Values.Add("DateTo", ConvertDateVB(txtActivityDate2.Text))
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("AgreementCancelViewer.aspx")
            End If
        End If
    End Sub
#End Region

End Class