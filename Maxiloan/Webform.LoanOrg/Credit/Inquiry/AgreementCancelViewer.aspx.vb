﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Controller

Public Class AgreementCancelViewer
    Inherits Webform.WebBased

    Private m_coll As New ActivityLogController
    Private oCustomClass As New Parameter.ActivityLog
    Private FROM_FILE As String = ""
    Dim PDFFile As String = ""

#Region "Property"
    
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property Status() As String
        Get
            Return CType(ViewState("Status"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Status") = Value
        End Set
    End Property
    Private Property Filter() As String
        Get
            Return CType(ViewState("Filter"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Filter") = Value
        End Set
    End Property

    Private Property DateFrom() As Date
        Get
            Return CType(ViewState("DateFrom"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateFrom") = Value
        End Set
    End Property
    Private Property DateTo() As Date
        Get
            Return CType(ViewState("DateTo"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateTo") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub

        BindReport()
    End Sub
    Sub BindReport()
        GetCookies()
        Dim oData As New DataSet
        Dim oPrint As New Parameter.ActivityLog

        Dim objReport As AgreementCancelRPT = New AgreementCancelRPT
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        With oPrint
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .status = Me.Status
            .DateFrom = Me.DateFrom
            .DateTo = Me.DateTo
        End With
        oPrint = m_coll.AgreementCancelRpt(oPrint)
        oData = oPrint.ListReport

        objReport.SetDataSource(oData)
        crvAgreementCancelRPT.ReportSource = objReport
        crvAgreementCancelRPT.Visible = True
        crvAgreementCancelRPT.DataBind()

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "AgreementCancel.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("AgreementCancel.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "AgreementCancel")
    End Sub

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("AgreementCancel")
        Me.BranchID = cookie.Values("BranchID")
        Me.BranchName = cookie.Values("BranchName")
        Me.Loginid = cookie.Values("LoginID")
        Me.Status = cookie.Values("Status")
        Me.Filter = cookie.Values("Filter")

        Me.SearchBy = cookie.Values("SearchBy")
        Me.DateFrom = CDate(cookie.Values("DateFrom"))
        Me.DateTo = CDate(cookie.Values("DateTo"))


    End Sub
End Class