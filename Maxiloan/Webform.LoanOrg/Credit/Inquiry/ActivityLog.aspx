﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ActivityLog.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ActivityLog" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ActivityLog</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        <!--
        var hdnDetail;
        var hdndetailvalue;
        function ParentChange(pCmbOfPayment,pBankAccount,pHdnDetail,pHdnDetailValue, itemArray)
        {
		        hdnDetail = eval('document.forms[0].' + pHdnDetail);
		        HdnDetailValue = eval('document.forms[0].'+pHdnDetailValue);
		        var i, j;
		        for(i= eval('document.forms[0].' + pBankAccount).options.length;i>=0;i--)
		        {   
			        eval('document.forms[0].' + pBankAccount).options[i] = null
		
		        }  ;
		        if (itemArray==null) 
		        { 
			        j = 0 ;
		        }
		        else
		        {  
			        j=1;
		        };
		        eval('document.forms[0].' + pBankAccount).options[0] = new Option('All','');
		        if (itemArray!=null)
		        {
			        for(i=0;i<itemArray.length;i++)
			        {	
				        eval('document.forms[0].' + pBankAccount).options[j++]=new Option(itemArray[i][0], itemArray[i][1]);
				
			        };
			        eval('document.forms[0].' + pBankAccount).selected=true;
		        }
		        };
		
		        function cboChildonChange(l,j)
			        {
				        hdnDetail.value = l; 
				        HdnDetailValue.value = j;
			        }
        }-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server"/>
        <input id="hdnChildName" type="hidden" name="hdnSP" runat="server"/>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
	        <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    DAFTAR AKTIVITAS
                </h3>
            </div>
        </div> 
        <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_header">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgActivity" runat="server" Width="100%" OnSortCommand="SortGrid"
                        CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        DataKeyField="AgreementNo" AutoGenerateColumns="False" AllowSorting="True">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                DataField="ACTIVITYDATE" SortExpression="ACTIVITYDATE" HeaderText="TGL AKTIVITAS">
                            </asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                DataField="ACTIVITYTYPE" SortExpression="ACTIVITYTYPE" HeaderText="JENIS AKTIVITAS">
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="APPLICATIONID" HeaderText="NO APLIKASI">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblApplicationID" runat="server" Text='<%# Container.DataItem("ApplicationID")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%# Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblCustomerName" runat="server" Text='<%# Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%# Container.dataitem("CustomerID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                DataField="ACTIVITYUSER" SortExpression="ACTIVITYUSER" HeaderText="OLEH">
                            </asp:BoundColumn>
                        </Columns>                       
                </asp:DataGrid>
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
	        </div>
        </div>               
        </asp:Panel>   
        <asp:Panel ID="pnlsearch" runat="server">        
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    CARI AKTIVITAS
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cabang</label>
                <asp:DropDownList ID="cboParent" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ControlToValidate="cboParent"
                InitialValue="0" ErrorMessage="Harap Pilih Cabang" CssClass="validator_general"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Customer.Name">Name Customer</asp:ListItem>
                    <asp:ListItem Value="Agreement.ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Aktivitas</label>                
                <asp:TextBox ID="txtActivityDate1" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtActivityDate1_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtActivityDate1" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                ControlToValidate="txtActivityDate1"></asp:RequiredFieldValidator>
                &nbsp;to&nbsp;                                
                <asp:TextBox ID="txtActivityDate2" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtActivityDate2_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtActivityDate2" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                ControlToValidate="txtActivityDate2"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Jenis Aktivitas</label>
                <asp:DropDownList ID="cboActyivityType" runat="server">
                    <asp:ListItem Value="All">All</asp:ListItem>
                    <asp:ListItem Value="APK">Aplikasi Baru</asp:ListItem>
                    <asp:ListItem Value="SCO">Scoring Pembiayaan</asp:ListItem>
                    <asp:ListItem Value="REQ">Pengajuan Approval</asp:ListItem>
                    <asp:ListItem Value="APR">Approval</asp:ListItem>
                    <asp:ListItem Value="PO">Purchase Order</asp:ListItem>
                    <asp:ListItem Value="DO">Pra Pencairan</asp:ListItem>
                    <asp:ListItem Value="AKR">Aktivasi Pembiayaan</asp:ListItem>
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Oleh</label>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                </asp:DropDownList>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"/>
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray" CausesValidation="False"/>
	    </div>
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>        
    </form>
</body>
</html>
