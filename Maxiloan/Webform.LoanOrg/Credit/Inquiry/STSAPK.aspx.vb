﻿
#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region


Public Class STSAPK
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"


    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New GeneralPagingController
    Private Dcontroller As New DataUserControlController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow
#End Region

#Region "Property"
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, "STSAPK", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If


            With cboBranch
                .DataSource = Dcontroller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
            End With

            cboBranch.SelectedValue = Replace(Me.sesBranchId, "'", "")
            Me.Sort = " agreement.ApplicationID ASC"
            Me.BranchID = cboBranch.SelectedValue 'Replace(Me.sesBranchId, "'", "")
            Me.CmdWhere = " agreement.BranchID='" & Me.BranchID & "'"
            BindGridEntity(Me.CmdWhere)
            InitialPanel()
        End If
    End Sub

    Sub InitialPanel()
        pnlList.Visible = True
        rgvGo.Enabled = True
    End Sub

#Region "DisplayMessage"
    Private Sub DisplayMessage(ByVal strMsg As String)
        ShowMessage(lblMessage, strMsg, True)
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oPaging As New Parameter.GeneralPaging

        Try
            With oPaging
                .strConnection = GetConnectionString()
                .WhereCond = Me.CmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = Me.Sort
                .SpName = "spGetStatusAplikasi"
            End With
            oPaging = oController.GetGeneralPaging(oPaging)
            If Not oPaging Is Nothing Then
                dtEntity = oPaging.ListData
                recordCount = oPaging.TotalRecords
            Else
                recordCount = 0
            End If
            dtgPaging.DataSource = dtEntity.DefaultView
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()
            PagingFooter()
            pnlList.Visible = True
        Catch ex As Exception
            DisplayMessage(ex.ToString)
        End Try
    End Sub

#End Region
#Region "Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"

    

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkApplicationID As HyperLink
        Dim lnkCustomer As LinkButton
        Dim lblCustomer As Label

        Dim imgApkStat As Image
        Dim imgAssetStat As Image
        Dim imgAsuransiStat As Image
        Dim imgFinancialStat As Image
        Dim imgFinancialStat2 As Image
        Dim imgRefundStat As Image
        Dim imgSurvayStat As Image
        Dim imgRcaStat As Image
        Dim imgPOstat As Image
        Dim imgDOStat As Image
        Dim imgLIVStat As Image

        Dim lblPVStatus, lblBranchID As Label

        Dim pathDirectory As String = Request.ServerVariables("APPL_PHYSICAL_PATH")

        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lnkCustomer = CType(e.Item.FindControl("lnkCustomerName"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblPVStatus = CType(e.Item.FindControl("lblPVStatus"), Label)
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)

            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lnkAgreementNo.Text.Trim) & "')"
            End If
            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            If lnkApplicationID.Text.Trim.Length > 0 Then
                lnkApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationID.Text.Trim) & "','" & Server.UrlEncode(lblBranchID.Text.Trim) & "')"
            End If
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','accacq');")

            If lblPVStatus.Text = "P" Then
                e.Item.CssClass = "item_grid DoneProses"
            End If


            imgApkStat = CType(e.Item.FindControl("imgApkStat"), Image)
            If CType(e.Item.FindControl("lblAPKStat"), Label).Text <> "" Then
                imgApkStat.ImageUrl = "../../../images/success.png"
                imgApkStat.ToolTip = CType(e.Item.FindControl("lblAPKStat"), Label).Text
            Else
                imgApkStat.ImageUrl = "../../../images/warning.png"
            End If

            imgAssetStat = CType(e.Item.FindControl("imgAssetStat"), Image)
            If CType(e.Item.FindControl("lblAssetStat"), Label).Text <> "" Then
                imgAssetStat.ImageUrl = "../../../images/success.png"
                imgAssetStat.ToolTip = CType(e.Item.FindControl("lblAssetStat"), Label).Text
            Else
                imgAssetStat.ImageUrl = "../../../images/warning.png"
            End If

            imgAsuransiStat = CType(e.Item.FindControl("imgAsuransiStat"), Image)
            If CType(e.Item.FindControl("lblAsuransiStat"), Label).Text <> "" Then
                imgAsuransiStat.ImageUrl = "../../../images/success.png"
                imgAsuransiStat.ToolTip = CType(e.Item.FindControl("lblAsuransiStat"), Label).Text
            Else
                imgAsuransiStat.ImageUrl = "../../../images/warning.png"
            End If

            imgFinancialStat = CType(e.Item.FindControl("imgFinancialStat"), Image)
            If CType(e.Item.FindControl("lblFinancialStat"), Label).Text <> "" Then
                imgFinancialStat.ImageUrl = "../../../images/success.png"
                imgFinancialStat.ToolTip = CType(e.Item.FindControl("lblFinancialStat"), Label).Text
            Else
                imgFinancialStat.ImageUrl = "../../../images/warning.png"
            End If

            imgFinancialStat2 = CType(e.Item.FindControl("imgFinancialStat2"), Image)
            If CType(e.Item.FindControl("lblFinancialStat2"), Label).Text <> "" Then
                imgFinancialStat2.ImageUrl = "../../../images/success.png"
                imgFinancialStat2.ToolTip = CType(e.Item.FindControl("lblFinancialStat2"), Label).Text
            Else
                imgFinancialStat2.ImageUrl = "../../../images/warning.png"
            End If

            imgRefundStat = CType(e.Item.FindControl("imgRefundStat"), Image)
            If CType(e.Item.FindControl("lblRefundStat"), Label).Text <> "" Then
                imgRefundStat.ImageUrl = "../../../images/success.png"
                imgRefundStat.ToolTip = CType(e.Item.FindControl("lblRefundStat"), Label).Text
            Else
                imgRefundStat.ImageUrl = "../../../images/warning.png"
            End If

            imgSurvayStat = CType(e.Item.FindControl("imgSurvayStat"), Image)
            If CType(e.Item.FindControl("lblSurvayStat"), Label).Text <> "" Then
                imgSurvayStat.ImageUrl = "../../../images/success.png"
                imgSurvayStat.ToolTip = CType(e.Item.FindControl("lblSurvayStat"), Label).Text
            Else
                imgSurvayStat.ImageUrl = "../../../images/warning.png"
            End If



            imgRcaStat = CType(e.Item.FindControl("imgRcaStat"), Image)
            If CType(e.Item.FindControl("lblRcaStat"), Label).Text <> "" Then
                imgRcaStat.ImageUrl = "../../../images/success.png"
                imgRcaStat.ToolTip = CType(e.Item.FindControl("lblRcaStat"), Label).Text
            Else
                imgRcaStat.ImageUrl = "../../../images/warning.png"
            End If


            imgPOstat = CType(e.Item.FindControl("imgPOstat"), Image)
            If CType(e.Item.FindControl("lblPOstat"), Label).Text <> "" Then
                imgPOstat.ImageUrl = "../../../images/success.png"
                imgPOstat.ToolTip = CType(e.Item.FindControl("lblPOstat"), Label).Text
            Else
                imgPOstat.ImageUrl = "../../../images/warning.png"
            End If

            imgDOStat = CType(e.Item.FindControl("imgDOStat"), Image)
            If CType(e.Item.FindControl("lblDOStat"), Label).Text <> "" Then
                imgDOStat.ImageUrl = "../../../images/success.png"
                imgDOStat.ToolTip = CType(e.Item.FindControl("lblDOStat"), Label).Text
            Else
                imgDOStat.ImageUrl = "../../../images/warning.png"
            End If

            imgLIVStat = CType(e.Item.FindControl("imgLIVStat"), Image)
            If CType(e.Item.FindControl("lblLIVtat"), Label).Text <> "" Then
                imgLIVStat.ImageUrl = "../../../images/success.png"
                imgLIVStat.ToolTip = CType(e.Item.FindControl("lblLIVtat"), Label).Text
            Else
                imgLIVStat.ImageUrl = "../../../images/warning.png"
            End If
        End If
    End Sub


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset-Search"
    Private Sub imbButtonReset_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "BranchID=" & Me.BranchID & ""
        BindGridEntity(Me.CmdWhere)
    End Sub

#End Region

    Protected Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Dim Search As String
            Me.BranchID = cboBranch.SelectedValue.Trim 'Replace(Me.sesBranchId, "'", "")
            If txtSearch.Text.Trim <> "" Then
                If txtSearch.Text.Trim <> "" Then
                    ' Me.CmdWhere = "BranchID=" & Me.BranchID & ""
                    Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + txtSearch.Text.Trim + "%'" + " AND agreement.BranchID=" & cboBranch.SelectedValue.Trim & ""
                End If
            Else
                Me.CmdWhere = " agreement.BranchID='" & Me.BranchID & "'"
            End If
        Else
            Me.CmdWhere = " agreement.BranchID='" & cboBranch.SelectedValue.Trim & "'"
        End If


        BindGridEntity(Me.CmdWhere)
    End Sub

End Class