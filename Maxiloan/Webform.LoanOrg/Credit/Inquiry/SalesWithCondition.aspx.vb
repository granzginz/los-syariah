﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SalesWithCondition
    Inherits Maxiloan.Webform.WebBased    

#Region "Constanta"
    Private oCustomClass As New Parameter.Sales
    Private oController As New RptSalesController
    Private m_controller As New DataUserControlController
    Private salesController As New SalesWithConditionController
    Private salesCustomclass As New Parameter.SalesWithCondition

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SalesWithCondition"
            If checkform(Me.Loginid, Me.FormID, "MAXILOAN") Then
                FillCbo()
                getchildcombo()
                InitialDefaultPanel()

            End If
        End If
    End Sub
#End Region

#Region "InitialDefaultPanel"
    Sub InitialDefaultPanel()
        pnlsearch.Visible = True
        pnlDtGrid.Visible = False
    End Sub
#End Region

#Region "FillCbo"
    Sub FillCbo()
        Dim dtbranch As New DataTable
        dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboParent
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
#End Region

#Region "BranchIDChange"
    Protected Function BranchIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function
#End Region

#Region "GenerateScript"
    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Srch", "MAXILOAN") Then
            If (cboCondition.SelectedItem.Value.Trim = "SupplierName" Or cboCondition.SelectedItem.Value.Trim = "Description") And cboOperator.SelectedItem.Value.Trim <> "=" Then                
                ShowMessage(lblMessage, "Operator Salah", True)
                Exit Sub
            End If
            Me.SearchBy = "BranchID='" & cboParent.SelectedItem.Value.Trim & "'"
            If txtSalesDate1.Text <> "" And txtSalesDate2.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " and SalesDate between '" & ConvertDate(txtSalesDate1.Text) & "' and '" & ConvertDate(txtSalesDate2.Text) & "'"
            End If
            If hdnChildValue.Value.Trim = "" Or hdnChildValue.Value.Trim = "0" Or hdnChildValue.Value.Trim = "ALL" Then
            Else
                Me.SearchBy = Me.SearchBy & " and AOID='" & hdnChildValue.Value.Trim & "'"
            End If
            'str = hdnChildValue.Value.Trim
            'If str <> "" Then
            '    Me.SearchBy = Me.SearchBy & " and AOID='" & hdnChildValue.Value.Trim & "'"
            'End If
            If txtCondition.Text <> "" Then
                Me.SearchBy = Me.SearchBy & " and " & cboCondition.SelectedItem.Value.Trim & cboOperator.SelectedItem.Value.Trim & "'" & txtCondition.Text.Trim & "'"
            End If
            If txtSearchBy.Text <> "" Then
                If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like  '" & txtSearchBy.Text.Trim & "'"
                Else
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " = '" & txtSearchBy.Text.Trim & "'"
                End If
            End If
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With salesCustomclass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        salesCustomclass = salesController.ListSalesWithCondition(salesCustomclass)

        DtUserList = salesCustomclass.ListSales
        DvUserList = DtUserList.DefaultView
        recordCount = salesCustomclass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgSales.DataSource = DvUserList

        Try
            dtgSales.DataBind()
        Catch
            dtgSales.CurrentPageIndex = 0
            dtgSales.DataBind()
        End Try
        getchildcombo()
        Dim strScript As String
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex  == -1) ? null : ListData[eval('document.forms[0]." & cboParent.ClientID & "').selectedIndex]));"
        strScript &= "RestoreInsuranceCoIndex(" & Request("hdnBankAccount") & ");"
        strScript &= "</script>"
        divInnerHTML.InnerHtml = strScript
        PagingFooter()
        pnlDtGrid.Visible = True        
    End Sub
#End Region

#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblrecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click        
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Reset"
    Private Sub ImbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        InitialDefaultPanel()
        FillCbo()
        txtSearchBy.Text = ""
        txtCondition.Text = ""
        txtSalesDate1.Text = ""
        txtSalesDate2.Text = ""
        cboSearchBy.ClearSelection()
        cboCondition.ClearSelection()
        cboOperator.ClearSelection()
    End Sub
#End Region

#Region "Bound datagrid"
    Private Sub dtgSales_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgSales.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label

            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"

            lblTemp = CType(e.Item.FindControl("lblSupplierID"), Label)
            hyTemp = CType(e.Item.FindControl("lblSupplierName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenWinSupplier('" & "AccAcq" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** AO Link
            lblTemp = CType(e.Item.FindControl("lblAOId"), Label)
            hyTemp = CType(e.Item.FindControl("lblEmployeeName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAO('" & "AccAcq" & "', " & Me.sesBranchId & ",'" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
        End If
    End Sub
#End Region
    Protected Function getchildcombo() As String
        Dim spAO As New Parameter.Sales
        Dim dtAO As New DataTable
        With spAO
            .strConnection = getconnectionstring()
            .WhereCond = ""
        End With
        dtAO = oController.GetAO(spAO)
        Response.Write(GenerateScript(dtAO))
    End Function

End Class