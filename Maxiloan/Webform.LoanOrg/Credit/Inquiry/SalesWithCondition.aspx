﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SalesWithCondition.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.SalesWithCondition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SalesWithCondition</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script language="JavaScript" type="text/javascript">
<!--
var hdnDetail;
var hdndetailvalue;
var cboBankAccount = null;
function ParentChange(pCmbOfPayment,pBankAccount,pHdnDetail,pHdnDetailValue, itemArray)
{
		hdnDetail = eval('document.forms[0].' + pHdnDetail);
		HdnDetailValue = eval('document.forms[0].'+pHdnDetailValue);
		var i, j;
		for(i= eval('document.forms[0].' + pBankAccount).options.length;i>=0;i--)
		{   
			eval('document.forms[0].' + pBankAccount).options[i] = null
		
		}  ;
		if (itemArray==null) 
		{ 
			j = 0 ;
		}
		else
		{  
			j=2;
		};
		eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One','0');
		eval('document.forms[0].' + pBankAccount).options[1] = new Option('ALL','ALL');
		if (itemArray!=null)
		{
			for(i=0;i<itemArray.length;i++)
			{	
				eval('document.forms[0].' + pBankAccount).options[j++]=new Option(itemArray[i][0], itemArray[i][1]);
				
			};
			eval('document.forms[0].' + pBankAccount).selected=true;
		}
		};
		
		function cboChildonChange(selIndex,l,j)
			{
				hdnDetail.value = l; 
				HdnDetailValue.value = j;
				eval('document.forms[0].hdnBankAccount').value = selIndex;
			}
		function RestoreInsuranceCoIndex(BankAccountIndex)
		{								
			cboChild  = eval('document.forms[0].cboChild');
			if (cboChild != null)
				if(eval('document.forms[0].hdnBankAccount').value != null)
				{
					if(BankAccountIndex != null)
					cboChild.selectedIndex = BankAccountIndex;
				}
		}
}-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
        <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
        <input id="hdnBankAccount" type="hidden" name="hdSP" runat="server" />
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
	        <div class="title_strip">
            </div>
            <div class="form_single">           
                <h3>
                    DAFTAR KONTRAK
                </h3>
            </div>
        </div>  
        <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_box_header">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgSales" runat="server" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False" DataKeyField="AgreementNo" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                        OnSortCommand="SortGrid">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="SALESDATE" SortExpression="SALESDATE" HeaderText="TGL JUAL"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%# Container.dataitem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblCustomerName" runat="server" Text='<%# Container.dataitem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.dataItem("CustomerID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="SUPPLIERNAME" HeaderText="NAMA SUPPLIER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblSupplierName" runat="server" Text='<%# Container.dataitem("SupplierName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%# Container.dataItem("SupplierID")%>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="EMPLOYEENAME" HeaderText="NAMA CMO">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblEmployeeName" runat="server" Text='<%# Container.dataitem("EmployeeName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DESCRIPTION" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblASSET" runat="server" Text='<%# Container.dataitem("DESCRIPTION")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DP" SortExpression="DP" HeaderText="%DP">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="INSTALLMENTAMOUNT" HeaderText="ANGSURAN">
                                <HeaderStyle HorizontalAlign="Right" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblINSTALLMENTAMOUNT" runat="server" Text='<%#formatnumber(Container.DataItem("INSTALLMENTAMOUNT"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="INSASSETPREMIUM" HeaderText="PREMI">
                                <HeaderStyle HorizontalAlign="Right" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblPREMIUM" runat="server" Text='<%#formatnumber(Container.DataItem("INSASSETPREMIUM"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="EFFECTIVERATE" SortExpression="EFFECTIVERATE" HeaderText="BUNGA EFF">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="TENOR" SortExpression="TENOR" HeaderText="JK WAKTU">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="AOId" HeaderText="AOId" Visible="False">
                                <HeaderStyle HorizontalAlign="Right" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblAOId" runat="server" Text='<%#Container.DataItem("AOId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="ApplicationId" Visible="False">
                                <HeaderStyle Width="12%"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Visible="False" ID="lblApplicationId" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                </asp:DataGrid>
                <div class="button_gridnavigation">
	                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
		                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
		                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
		                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
		                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
	                </asp:ImageButton>
	                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
	                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
		                EnableViewState="False"></asp:Button>
	                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage"
				        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
				        CssClass="validator_general"></asp:RangeValidator>
			        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
				        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
			        </asp:RequiredFieldValidator>              
                </div>
                <div class="label_gridnavigation">
	                <asp:Label ID="lblPage" runat="server"></asp:Label>of
	                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
	                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
	        </div>
        </div>                         
        </asp:Panel>
        <asp:Panel ID="pnlsearch" runat="server">     
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    CARI PENJUALAN DENGAN KONDISI
                </h4>
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cabang</label>
                <asp:DropDownList ID="cboParent" runat="server" onchange="<%#BranchIDChange()%>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvBranch" runat="server" Display="Dynamic" ControlToValidate="cboParent"
                ErrorMessage="Harap Pilih Cabang" CssClass="validator_general"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="CustomerName">Name Customer</asp:ListItem>
                    <asp:ListItem Value="Application ID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Tanggal Jual</label>                
                <asp:TextBox ID="txtSalesDate1" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtSalesDate1_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtSalesDate1" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                ControlToValidate="txtSalesDate1"></asp:RequiredFieldValidator>
                &nbsp;to&nbsp;                
                <asp:TextBox ID="txtSalesDate2" runat="server"></asp:TextBox>                   
                <asp:CalendarExtender ID="txtSalesDate2_CalendarExtender" runat="server" 
                Enabled="True" TargetControlID="txtSalesDate2" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                ControlToValidate="txtSalesDate2"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Kondisi</label>
                <asp:DropDownList ID="cboCondition" runat="server">
                    <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                    <asp:ListItem Value="Description">Asset</asp:ListItem>
                    <asp:ListItem Value="DP">%DP</asp:ListItem>
                    <asp:ListItem Value="InstallmentAmount">Angsuran</asp:ListItem>
                    <asp:ListItem Value="InsAssetPremium">Asuransi</asp:ListItem>
                    <asp:ListItem Value="EffectiveRate">Bunga Effective</asp:ListItem>
                    <asp:ListItem Value="Tenor">Tenor</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="cboOperator" runat="server">
                    <asp:ListItem Value="=">=</asp:ListItem>
                    <asp:ListItem Value="&gt;">&gt;</asp:ListItem>
                    <asp:ListItem Value="&lt;">&lt;</asp:ListItem>
                    <asp:ListItem Value="&gt;=">&gt;=</asp:ListItem>
                    <asp:ListItem Value="&lt;=">&lt;=</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtCondition" runat="server" ></asp:TextBox>
	        </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>CMO</label>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcboChild" runat="server" Display="Dynamic" ControlToValidate="cboChild"
                ErrorMessage="Harap Pilih CMO" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Buttonsearch" runat="server"  Text="Search" CssClass ="small button blue"/>            
            <asp:Button ID="ButtonReset" runat="server"  Text="Reset" CssClass ="small button gray"   CausesValidation="False"/>
	    </div>
        </asp:Panel>
        <div id="divInnerHTML" runat="server">
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>        
    </form>
</body>
</html>
