﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PendingDO.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.PendingDO" %>

<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pending DO</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
<!--
var hdnDetail;
var hdndetailvalue;
function ParentChange(pCmbOfPayment,pBankAccount,pHdnDetail,pHdnDetailValue, itemArray)
{
		hdnDetail = eval('document.forms[0].' + pHdnDetail);
		HdnDetailValue = eval('document.forms[0].'+pHdnDetailValue);
		var i, j;
		for(i= eval('document.forms[0].' + pBankAccount).options.length;i>=0;i--)
		{   
			eval('document.forms[0].' + pBankAccount).options[i] = null
		
		}  ;
		if (itemArray==null) 
		{ 
			j = 0 ;
		}
		else
		{  
			j=1;
		};
		eval('document.forms[0].' + pBankAccount).options[0] = new Option('ALL','0');
		if (itemArray!=null)
		{
			for(i=0;i<itemArray.length;i++)
			{	
				eval('document.forms[0].' + pBankAccount).options[j++]=new Option(itemArray[i][0], itemArray[i][1]);
				
			};
			eval('document.forms[0].' + pBankAccount).selected=true;
		}
		};
		
		function cboChildonChange(l,j)
			{
				hdnDetail.value = l; 
				HdnDetailValue.value = j;
				
			}			
		
}-->
    </script>
    <script language="JavaScript" type="text/javascript">
		<!--
        var x = screen.width;
        var y = screen.height - 100;	

        function OpenSupp(SupplierID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Viewsupplier.aspx?style=' + pStyle + '&SupplierID=' + SupplierID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenAO(pBranchID, pEmployeeID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pEmployeeID, 'AO', 'left=50, top=10, width=900, height=650, menubar=0, scrollbars=1');
        }
        function OpenWinAgreementNo(pAgreementNo, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?AgreementNo=' + pAgreementNo + '&style=' + pStyle, 'AgreementNo', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function Reset() {
            document.forms[0].cboBranch.options[0].selected = true;
            document.forms[0].cboAO.options[0].selected = true;
            document.forms[0].cboSupplier.options[0].selected = true;
            document.forms[0].hdnIndex.value = 0;
            document.forms[0].hdnIndexAO.value = 0;
            hdnDetailAO.value = 'All';
            hdndetailvalueAO.value = '0';
            document.forms[0].hdnIndexSupplier.value = 0;
            hdnDetailSupplier.value = 'All';
            hdndetailvalueSupplier.value = '0';
        }
		-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                VERIFIKASI PRA PENCAIRAN YANG PENDING</h3>
        </div>
    </div>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cabang
                    </label>
                    <asp:DropDownList ID="cboParent" runat="server" onchange="<%#BranchIDChange()%>">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboParent"
                        ErrorMessage="Harap pilih Cabang" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label>
                        Nama CMO
                    </label>
                    <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlList" runat="server">        
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CssClass="grid_general"
                    CellSpacing="1" CellPadding="3" BorderWidth="0px" OnSortCommand="Sorting" AutoGenerateColumns="False"
                    AllowSorting="True">
                    <ItemStyle CssClass="item_grid" />
                    <HeaderStyle CssClass="th" />
                    <Columns>
                        <asp:BoundColumn DataField="PurchaseOrderDate" SortExpression="PurchaseOrderDate"
                            HeaderText="TGL PO" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle Width="7%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Aging" SortExpression="Aging" HeaderText="HARI AGING">
                            <HeaderStyle Width="6%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SupplierID" SortExpression="SupplierID" HeaderText="ID SUPPLIER">
                            <HeaderStyle Width="8%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="SupplierName" HeaderText="NAMA SUPPLIER">
                            <HeaderStyle Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkSupplier" Text='<%# DataBinder.eval(Container,"DataItem.SupplierName")%>'>
                                </asp:HyperLink>
                                <asp:Label ID="lblSupplier" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.SupplierID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="SupplierPhone" SortExpression="SupplierPhone" HeaderText="TELP SUPPLIER">
                            <HeaderStyle Width="7%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAgreementNo" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>'>
                                </asp:HyperLink>
                                <asp:Label ID="lblAgreementNo" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                            <HeaderStyle Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'>
                                </asp:HyperLink>
                                <asp:Label ID="lblName" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AO" HeaderText="NAMA CMO">
                            <HeaderStyle Width="18%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAO" Text='<%# DataBinder.eval(Container,"DataItem.AO")%>'>
                                </asp:HyperLink>
                                <asp:Label ID="lblAOID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.AOID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CustomerId" HeaderText="CustomerId" Visible="False">
                            <HeaderStyle Width="12%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Visible="False" ID="lblCustomerID" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BranchId" HeaderText="BranchId" Visible="False">
                            <HeaderStyle Width="12%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Visible="False" ID="lblBranchId" Text='<%# DataBinder.eval(Container,"DataItem.BranchId")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ApplicationId" HeaderText="ApplicationId" Visible="False">
                            <HeaderStyle Width="12%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" Visible="False" ID="lblApplicationId" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationId")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtGopage" CssClass="validator_general"></asp:RequiredFieldValidator></tr>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlSearchNext" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI VERIFIKASI PRA PENCAIRAN YANG PENDING</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cabang
                    </label>
                    <asp:Label ID="lblbranch" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Nama CMO
                    </label>
                    <asp:Label ID="lblAO" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Supplier
                    </label>
                    <asp:DropDownList ID="cboSupplier" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal PO
                    </label>
                    <asp:TextBox runat="server" ID="txtDateFrom" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDateFrom">
                    </asp:CalendarExtender>
                    <label class="label_auto">
                        to</label>
                    <asp:TextBox runat="server" ID="txtDateTo" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtDateTo">
                    </asp:CalendarExtender>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_single">
                    <label>
                        Cari Berdasarkan
                    </label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                        <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>                
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="buttonSearch" runat="server" Text="Search" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    <div id="mydiv" runat="server">
    </div>
    </form>
</body>
</html>
