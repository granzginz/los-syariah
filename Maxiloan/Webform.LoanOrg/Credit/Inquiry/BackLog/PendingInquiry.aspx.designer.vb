﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PendingInquiry

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''up1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents up1 As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cboStatusAplikasi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboStatusAplikasi As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''pnlSearchAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearchAPK As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboParentAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboParentAPK As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rfvcbobranchid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvcbobranchid As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''cboChildAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboChildAPK As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboSupplierAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplierAPK As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtDateFromAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateFromAPK As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender1 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtDateToAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateToAPK As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender2 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''cboSearchAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchAPK As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearchAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchAPK As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''buttonSearchAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents buttonSearchAPK As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnResetAPK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnResetAPK As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSearchRCA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearchRCA As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboBranchRCA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboBranchRCA As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''RequiredFieldValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtDateFromRCA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateFromRCA As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender3 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtDateToRCA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateToRCA As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender4 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''cboSearchRCA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchRCA As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearchRCA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchRCA As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSearchRCA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearchRCA As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnResetRCA control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnResetRCA As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSearchAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearchAPR As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboBranchAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboBranchAPR As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''RequiredFieldValidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator2 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtDateFromAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateFromAPR As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender5 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtDateToAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateToAPR As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender6 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''cboApprovedAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboApprovedAPR As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboSearchAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchAPR As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearchAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchAPR As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSearchAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearchAPR As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnResetAPR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnResetAPR As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSearchPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearchPOP As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboParentPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboParentPOP As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''RequiredFieldValidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator3 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''cboChildPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboChildPOP As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboSupplierPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplierPOP As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtDateFromPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateFromPOP As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender7 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtDateToPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateToPOP As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender8 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''cboSearchPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchPOP As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearchPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchPOP As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''buttonSearchPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents buttonSearchPOP As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnResetPOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnResetPOP As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSearchDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearchDLO As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboParentDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboParentDLO As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''RequiredFieldValidator4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator4 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''cboChildDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboChildDLO As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboSupplierDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplierDLO As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtDateFromDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateFromDLO As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender9 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtDateToDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateToDLO As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender10 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender10 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''cboSearchDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchDLO As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearchDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchDLO As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''buttonSearchDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents buttonSearchDLO As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnResetDLO control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnResetDLO As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSearchINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearchINV As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboBranchINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboBranchINV As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''RequiredFieldValidator5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator5 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtDateFromINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateFromINV As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender11 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender11 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtDateToINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateToINV As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender12 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender12 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''cboAOINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboAOINV As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboSearchINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchINV As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearchINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchINV As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSearchINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSearchINV As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnResetINV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnResetINV As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlSearchGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlSearchGLV As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''cboParentGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboParentGLV As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''RequiredFieldValidator6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RequiredFieldValidator6 As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''cboChildGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboChildGLV As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''cboSupplierGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSupplierGLV As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtDateFromGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateFromGLV As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender13 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender13 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtDateToGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDateToGLV As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''CalendarExtender14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender14 As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''cboSearchGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSearchGLV As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSearchGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearchGLV As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''buttonSearchGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents buttonSearchGLV As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnResetGLV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnResetGLV As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''pnlGrid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlGrid As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''dtgPaging control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgPaging As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''imbFirstPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbFirstPage As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''imbPrevPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbPrevPage As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''imbNextPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbNextPage As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''imbLastPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imbLastPage As Global.System.Web.UI.WebControls.ImageButton

    '''<summary>
    '''txtPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPage As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnPageNumb control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPageNumb As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rgvGo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rgvGo As Global.System.Web.UI.WebControls.RangeValidator

    '''<summary>
    '''rfvGo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvGo As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''lblPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblrecord control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblrecord As Global.System.Web.UI.WebControls.Label
End Class
