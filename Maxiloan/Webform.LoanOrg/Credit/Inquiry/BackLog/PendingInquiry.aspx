﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PendingInquiry.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.PendingInquiry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Application Pending</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        //        
        //        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        //        var ServerName = ServerName;
        //        var hdnDetail;
        //        var hdndetailvalue;
        //        var x = screen.width;
        //        var y = screen.height - 100;

        //        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
        //            hdnDetail = eval('document.forms[0].' + pHdnDetail);
        //            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
        //            var i, j;
        //            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
        //                eval('document.forms[0].' + pBankAccount).options[i] = null

        //            };
        //            if (itemArray == null) {
        //                j = 0;
        //            }
        //            else {
        //                j = 1;
        //            };
        //            eval('document.forms[0].' + pBankAccount).options[0] = new Option('ALL', '0');
        //            if (itemArray != null) {
        //                for (i = 0; i < itemArray.length; i++) {
        //                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

        //                };
        //                eval('document.forms[0].' + pBankAccount).selected = true;
        //            }
        //        };

        //        function cboChildonChange(l, j) {
        //            hdnDetail.value = l;
        //            HdnDetailValue.value = j;

        //        }

        function OpenAppID(pApplicationID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenSupp(SupplierID, pStyle) {
            window.open(ServerName + App + '/Webform.SalesMarketing/Viewsupplier.aspx?style=' + pStyle + '&SupplierID=' + SupplierID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenCust(CustomerID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenAO(pBranchID, pEmployeeID, pStyle) {
            window.open(ServerName + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pEmployeeID, 'AO', 'left=50, top=10, width=900, height=650, menubar=0, scrollbars=1');
        }

        function OpenWinAgreementNo(pAgreementNo, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?AgreementNo=' + pAgreementNo + '&style=' + pStyle, 'AgreementNo', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        //        function Reset() {
        //            document.forms[0].cboBranch.options[0].selected = true;
        //            document.forms[0].cboAO.options[0].selected = true;
        //            document.forms[0].cboSupplier.options[0].selected = true;
        //            document.forms[0].hdnIndex.value = 0;
        //            document.forms[0].hdnIndexAO.value = 0;
        //            hdnDetailAO.value = 'All';
        //            hdndetailvalueAO.value = '0';
        //            document.forms[0].hdnIndexSupplier.value = 0;
        //            hdnDetailSupplier.value = 'All';
        //            hdndetailvalueSupplier.value = '0';
        //        }

        //        function cboChildonChange(l, j) {
        //            hdnDetail.value = l;
        //            HdnDetailValue.value = j;

        //        }

        //        function Reset() {
        //            document.forms[0].cboBranch.options[0].selected = true;
        //            document.forms[0].cboAO.options[0].selected = true;
        //            document.forms[0].cboSupplier.options[0].selected = true;
        //            document.forms[0].hdnIndex.value = 0;
        //            document.forms[0].hdnIndexAO.value = 0;
        //            hdnDetailAO.value = 'All';
        //            hdndetailvalueAO.value = '0';
        //            document.forms[0].hdnIndexSupplier.value = 0;
        //            hdnDetailSupplier.value = 'All';
        //            hdndetailvalueSupplier.value = '0';
        //        }		
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildValueAO" type="hidden" name="hdnSPAO" runat="server" />
    <input id="hdnChildNameAO" type="hidden" name="hdnSPAO" runat="server" />
    <input id="hdnIndexAO" type="hidden" name="hdnIndexAO" runat="server" />
    <input id="hdnIndex" type="hidden" name="hdnIndex" runat="server" />    --%>
    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        APPLICATION PENDING INQUIRY
                    </h3>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        CARI APLIKASI PENDING
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Status Aplikasi</label>
                    <asp:DropDownList runat="server" ID="cboStatusAplikasi" AutoPostBack="true">
                        <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                        <asp:ListItem Value="APK">Pending Data Entri</asp:ListItem>
                        <asp:ListItem Value="REQ">Pending Review Aplikasi</asp:ListItem>
                        <asp:ListItem Value="APR">Pending Persetujuan Pembiayaan</asp:ListItem>
                        <asp:ListItem Value="PO">Pending PO</asp:ListItem>
                        <asp:ListItem Value="DO">Pending DO</asp:ListItem>
                        <asp:ListItem Value="INV">Pending Invoice</asp:ListItem>
                        <asp:ListItem Value="AKR">Pending Aktivasi Pembiayaan</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlSearchAPK">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PENDING DATA ENTRI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Cabang
                            </label>
                            <asp:DropDownList runat="server" ID="cboParentAPK" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ErrorMessage="Harap pilih Cabang"
                                ControlToValidate="cboParentAPK" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label>
                                Nama CMO
                            </label>
                            <asp:DropDownList ID="cboChildAPK" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Nama Supplier
                            </label>
                            <asp:DropDownList ID="cboSupplierAPK" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Tgl. Aplikasi baru
                            </label>
                            <asp:TextBox runat="server" ID="txtDateFromAPK" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDateFromAPK">
                            </asp:CalendarExtender>
                            to
                            <asp:TextBox runat="server" ID="txtDateToAPK" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtDateToAPK">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan
                            </label>
                            <asp:DropDownList ID="cboSearchAPK" runat="server">
                                <asp:ListItem Value="Agreement.ApplicationID">No Aplikasi</asp:ListItem>
                                <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchAPK" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="buttonSearchAPK" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="btnResetAPK" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSearchRCA">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PENDING REVIEW APLIKASI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Cabang
                        </label>
                        <asp:DropDownList ID="cboBranchRCA" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ControlToValidate="cboBranchRCA" ErrorMessage="Harap Pilih Cabang" InitialValue="0"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>
                            Tanggal Scoring Pembiayaan
                        </label>
                        <asp:TextBox runat="server" ID="txtDateFromRCA" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender3" TargetControlID="txtDateFromRCA">
                        </asp:CalendarExtender>
                        to
                        <asp:TextBox runat="server" ID="txtDateToRCA" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender4" TargetControlID="txtDateToRCA">
                        </asp:CalendarExtender>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <asp:DropDownList ID="cboSearchRCA" runat="server">
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                            <asp:ListItem Value="Agreement.ApplicationID">No. Aplikasi</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchRCA" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearchRCA" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="btnResetRCA" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSearchAPR">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PENDING PERSETUJUAN PEMBIAYAAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Cabang
                            </label>
                            <asp:DropDownList ID="cboBranchAPR" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Branch!"
                                ControlToValidate="cboBranchAPR" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Scoring Pembiayaan
                            </label>
                            <asp:TextBox runat="server" ID="txtDateFromAPR" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender5" TargetControlID="txtDateFromAPR">
                            </asp:CalendarExtender>
                            to
                            <asp:TextBox runat="server" ID="txtDateToAPR" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender6" TargetControlID="txtDateToAPR">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Akan diSetujui Oleh
                        </label>
                        <asp:DropDownList ID="cboApprovedAPR" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <asp:DropDownList ID="cboSearchAPR" runat="server">
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                            <asp:ListItem Value="Agreement.ApplicationID">No Aplikasi</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchAPR" runat="server" CssClass="medium_text"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearchAPR" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="btnResetAPR" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSearchPOP">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PENDING PO
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Cabang
                            </label>
                            <asp:DropDownList runat="server" ID="cboParentPOP" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please Select Branch"
                                ControlToValidate="cboParentPOP" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label>
                                Nama CMO
                            </label>
                            <asp:DropDownList ID="cboChildPOP" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Nama Supplier
                            </label>
                            <asp:DropDownList ID="cboSupplierPOP" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Approval
                            </label>
                            <asp:TextBox runat="server" ID="txtDateFromPOP" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender7" TargetControlID="txtDateFromPOP">
                            </asp:CalendarExtender>
                            to
                            <asp:TextBox runat="server" ID="txtDateToPOP" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender8" TargetControlID="txtDateToPOP">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <asp:DropDownList ID="cboSearchPOP" runat="server">
                            <asp:ListItem Value="Agreement.ApplicationID">No Aplikasi</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchPOP" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="buttonSearchPOP" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="btnResetPOP" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSearchDLO">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PENDING DO
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Cabang
                            </label>
                            <asp:DropDownList runat="server" ID="cboParentDLO" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Cabang"
                                ControlToValidate="cboParentDLO" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label>
                                Nama CMO
                            </label>
                            <asp:DropDownList ID="cboChildDLO" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Nama Supplier
                            </label>
                            <asp:DropDownList ID="cboSupplierDLO" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal PO
                            </label>
                            <asp:TextBox runat="server" ID="txtDateFromDLO" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender9" TargetControlID="txtDateFromDLO">
                            </asp:CalendarExtender>
                            to
                            <asp:TextBox runat="server" ID="txtDateToDLO" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender10" TargetControlID="txtDateToDLO">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_single">
                            <label>
                                Cari Berdasarkan
                            </label>
                            <asp:DropDownList ID="cboSearchDLO" runat="server">
                                <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                                <asp:ListItem Value="Agreement.AgreementNo">No Kontrak</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearchDLO" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="buttonSearchDLO" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="btnResetDLO" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSearchINV">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PENDING INVOICE
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Cabang
                            </label>
                            <asp:DropDownList ID="cboBranchINV" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="cboBranchINV"
                                ErrorMessage="Harap Pilih Cabang" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal DO
                            </label>
                            <asp:TextBox runat="server" ID="txtDateFromINV" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender11" TargetControlID="txtDateFromINV">
                            </asp:CalendarExtender>
                            to
                            <asp:TextBox runat="server" ID="txtDateToINV" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender12" TargetControlID="txtDateToINV">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            NAMA CMO
                        </label>
                        <asp:DropDownList ID="cboAOINV" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <asp:DropDownList ID="cboSearchINV" runat="server">
                            <asp:ListItem Value="Agreement.SupplierID">ID Supplier</asp:ListItem>
                            <asp:ListItem Value="Supplier.SupplierName">Nama Supplier</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchINV" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearchINV" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="btnResetINV" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSearchGLV">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PENDING AKTIVASI PEMBIAYAAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Cabang
                            </label>
                            <asp:DropDownList ID="cboParentGLV" runat="server" AutoPostBack="true" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="cboParentGLV"
                                ErrorMessage="Harap Pilih Cabang" InitialValue="0" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_right">
                            <label>
                                Nama CMO
                            </label>
                            <asp:DropDownList ID="cboChildGLV" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Nama Supplier
                            </label>
                            <asp:DropDownList ID="cboSupplierGLV" runat="server">
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Loan Activation
                            </label>
                            <asp:TextBox runat="server" ID="txtDateFromGLV" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender13" TargetControlID="txtDateFromGLV">
                            </asp:CalendarExtender>
                            to
                            <asp:TextBox runat="server" ID="txtDateToGLV" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="CalendarExtender14" TargetControlID="txtDateToGLV">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan
                        </label>
                        <asp:DropDownList ID="cboSearchGLV" runat="server">
                            <asp:ListItem Value="Customer.Name">Customer Name</asp:ListItem>
                            <asp:ListItem Value="Agreement.AgreementNo">Agreement No</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchGLV" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="buttonSearchGLV" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="btnResetGLV" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlGrid">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            LIST DATA PENDING
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" DataKeyField="ApplicationID" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn DataField="DateEntryApplicationData" SortExpression="DateEntryApplicationData"
                                        HeaderText="TGL APK" />
                                    <asp:BoundColumn DataField="RCADate" SortExpression="RCADate" HeaderText="TGL RCA" />
                                    <asp:BoundColumn DataField="NewApplicationDate" SortExpression="NewApplicationDate"
                                        HeaderText="TGL APR" />
                                    <asp:BoundColumn DataField="PurchaseOrderDate" SortExpression="PurchaseOrderDate"
                                        HeaderText="TGL PO" />
                                    <asp:BoundColumn DataField="DeliveryOrderDate" SortExpression="DeliveryOrderDate"
                                        HeaderText="TGL DO" />
                                    <asp:BoundColumn DataField="Aging" SortExpression="Aging" HeaderText="AGING" />
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnkAgreementNo" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="CUSTOMER">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnkName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'>
                                            </asp:HyperLink>
                                            <asp:Label runat="server" ID="lblCustomerID" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SupplierName" HeaderText="SUPPLIER">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnkSupplierName" Text='<%# DataBinder.eval(Container,"DataItem.SupplierName")%>'>
                                            </asp:HyperLink>
                                            <asp:Label runat="server" ID="lblSupplierID" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.SupplierID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ContactPersonHP" SortExpression="ContactPersonHP" HeaderText="TELP SUPPLIER" />
                                    <asp:TemplateColumn SortExpression="AO" HeaderText="CMO">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnkAO" Text='<%# DataBinder.eval(Container,"DataItem.AO")%>'>
                                            </asp:HyperLink>
                                            <asp:Label runat="server" ID="lblEmployeeID" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.EmployeeID")%>' />
                                            <asp:Label runat="server" ID="lblBranchID" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.BranchID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="CreditScoringResult" SortExpression="CreditScoringResult"
                                        HeaderText="HASIL SCORING" />
                                    <asp:BoundColumn DataField="CreditScoringDate" SortExpression="CreditScoringDate"
                                        HeaderText="AKAN DISETUJUI" />
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
