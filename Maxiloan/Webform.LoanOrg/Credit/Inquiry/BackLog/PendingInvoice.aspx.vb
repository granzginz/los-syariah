﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PendingInvoice
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New PendingController
    Private m_DataUsercontroller As New DataUserControlController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(viewstate("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property CustID() As String
        Get
            Return CType(viewstate("CustID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustID") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Private Property Type() As String
        Get
            Return CType(viewstate("Type"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Type") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        If Not Page.IsPostBack Then
            txtGoPage.Text = "1"
            If CheckForm(Me.Loginid, "PendingInvoice", "MAXILOAN") Then
                If sessioninvalid() Then
                    NotAuthorized()
                End If
            Else
                NotAuthorized()
            End If
            Me.Sort = "Aging desc"
            InitialPanel()
            FillBranch()
            Dim Func As String = "SelectFromArray(this.selectedIndex, '" & cboAO.ClientID & "', '" & hdnChildNameAO.ClientID & "','" & hdnChildValueAO.ClientID & "', ((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
            cboBranch.Attributes.Add("onChange", "return " + Func)
            'imbReset.Attributes.Add("OnClick", "return Reset('" & cboAO.ClientID & "');")
            btnReset.Attributes.Add("OnClick", "return Reset();")
            rgvGo.MaximumValue = "1"
            cboAO.Items.Insert(0, "All")
            hdnChildValueAO.Value = "0"
            cboAO.Enabled = False
        End If
        GetAO()
    End Sub
    Sub InitialPanel()
        pnlList.Visible = False
        pnlSearch.Visible = True
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
    Sub FillBranch()
        Dim dtbranch As New DataTable
        dtbranch = m_DataUsercontroller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboBranch
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
    Sub GetAO()
        Dim oPending As New Parameter.Pending
        Dim oData As New DataTable
        oPending.strConnection = getconnectionstring()
        oData = m_controller.GetAO(oPending)
        Response.Write(GenerateScriptAO(oData))
    End Sub
    Private Function GenerateScriptAO(ByVal DtTable As DataTable) As String
        Dim strScript As String = ""
        Dim strScript1 As String = ""
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboBranch.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboBranch.Items(j).Value.Trim & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("branchID")).Trim Then
                        strType = CStr(DataRow(i)("branchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & DataRow(i)("employeename").ToString.Trim & "','" & DataRow(i)("EmployeeId").ToString.Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & DataRow(i)("employeename").ToString.Trim & "','" & DataRow(i)("EmployeeId").ToString.Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next
        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If
        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        strScript &= "</script>"
        Return strScript
    End Function
#End Region
#Region "BindGrid"
    Sub BindGrid()
        pnlList.Visible = True
        pnlSearch.Visible = True
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Pending
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BusinessDate = Me.BusinessDate
        oCustomClass.Table = "spPendingInvoicePaging"
        oCustomClass = m_controller.GetPendingDataEntry(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "AgreementNo" Then
        ElseIf e.CommandName = "Name" Then
        ElseIf e.CommandName = "Supplier" Then
        ElseIf e.CommandName = "AO" Then
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtGoPage.Text = "1"
        txtSearch.Text = ""
        txtDateFrom.Text = ""
        txtDateTo.Text = ""
        cboBranch.ClearSelection()
        InitialPanel()
        mydiv.InnerHtml = ""
    End Sub
    Function Validator() As Boolean
        If txtDateFrom.Text.Trim <> "" Then
            If txtDateTo.Text.Trim = "" Then
                lblMessage.Text = "Harap isi Tanggal DO"
                Return False
            End If
        Else
            If txtDateTo.Text.Trim <> "" Then
                lblMessage.Text = "Harap isi Tanggal DO"
                Return False
            End If
        End If
        Return True
    End Function
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim Search As String = "DeliveryOrderDate < '" & Me.BusinessDate & "'"
        If Validator() = False Then
            Exit Sub
        End If
        If txtSearch.Text.Trim <> "" Then
            If cboSearch.SelectedIndex = 1 Then
                Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
            Else
                Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
            End If
        Else
            Me.CmdWhere = "All"
        End If
        If cboBranch.SelectedIndex <> 0 Then
            Search = Search + " and BranchId = '" & cboBranch.SelectedValue & "'"
            If hdnChildValueAO.Value <> "0" Then
                Search = Search + " and AOID = '" & hdnChildValueAO.Value & "'"
            End If
        End If
        If txtDateFrom.Text.Trim <> "" And txtDateTo.Text.Trim <> "" Then
            Search = Search + " and DeliveryOrderDate between '" & ConvertDate(txtDateFrom.Text.Trim) & "' and '" & ConvertDate(txtDateTo.Text.Trim) & "'"
        End If
        Me.CmdWhere2 = Search
        BindGrid()
        GetAO()
        Dim BranchIndex As Integer
        Dim AOIndex As Integer
        Dim strScript As String
        If Request("hdnIndex").ToString.Trim <> "" Then
            BranchIndex = CInt(Request("hdnIndex"))
        Else
            BranchIndex = 0
        End If
        If Request("hdnIndexAO").ToString.Trim <> "" Then
            AOIndex = CInt(Request("hdnIndexAO"))
        Else
            AOIndex = 0
        End If
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "SelectFromArray(" & BranchIndex & ",'" & cboAO.ClientID & "','" & hdnChildNameAO.ClientID & "','" & hdnChildValueAO.ClientID & "', ((" & BranchIndex & "== -1) ? null : ListData[" & BranchIndex & "]));" & vbCrLf
        strScript &= "Select(" & BranchIndex & ",'" & cboAO.ClientID & "'," & AOIndex & ");"
        strScript &= "</script>"
        mydiv.InnerHtml = strScript
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkCust As HyperLink
        Dim lnkSupp As HyperLink
        Dim lnkAO As HyperLink

        Dim lblCustomerID As Label
        Dim lblSupplierID As Label
        Dim lblBranchID As Label
        Dim lblEmployeeID As Label
        Dim lblApplicationId As Label
        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lnkCust = CType(e.Item.FindControl("lnkName"), HyperLink)
            lnkSupp = CType(e.Item.FindControl("lnkSupplier"), HyperLink)
            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)

            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblSupplierID = CType(e.Item.FindControl("lblSupplier"), Label)
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)
            lblEmployeeID = CType(e.Item.FindControl("lblAOID"), Label)

            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text) & "')"
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(lblCustomerID.Text) & "')"
            lnkAO.NavigateUrl = "javascript:OpenAO('" & "AccAcq" & "', '" & lblBranchID.Text & "','" & lblEmployeeID.Text & "')"
            lnkSupp.NavigateUrl = "javascript:OpenWinSupplier('" & "AccAcq" & "', '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"
        End If
    End Sub
#End Region

End Class