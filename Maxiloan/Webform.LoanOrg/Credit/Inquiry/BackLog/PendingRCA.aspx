﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PendingRCA.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.PendingRCA" %>

<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pending RCA</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />    
    <script language="JavaScript" type="text/javascript">
		<!--
        var x = screen.width;
        var y = screen.height - 100;	
        function OpenAppID(pApplicationID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenSupp(SupplierID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Viewsupplier.aspx?style=' + pStyle + '&SupplierID=' + SupplierID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenAO(pBranchID, pEmployeeID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pEmployeeID, 'AO', 'left=50, top=10, width=900, height=650, menubar=0, scrollbars=1');
        }
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
			-->
    </script>
    <script language="JavaScript" src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR PEMBIAYAAN APPROVAL REQUEST PENDING</h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" CellSpacing="1" CellPadding="3"
                    BorderWidth="0px" OnSortCommand="Sorting" AutoGenerateColumns="False" AllowSorting="True"
                    CssClass="grid_general">
                    <ItemStyle CssClass="item_grid" />
                    <HeaderStyle CssClass="th" />
                    <Columns>
                        <asp:BoundColumn DataField="CreditScoringDate" SortExpression="CreditScoringDate"
                            HeaderText="TGL SCORING" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle Width="12%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Aging" SortExpression="Aging" HeaderText="HARI AGING">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                            <HeaderStyle Width="13%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkApplication" CausesValidation="False" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID")%>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                            <HeaderStyle Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkName" CausesValidation="False" CommandName="Name"
                                    Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'>
                                </asp:LinkButton>
                                <asp:Label ID="lblName" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierName" HeaderText="NAMA SUPPLIER">
                            <HeaderStyle Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkSupplier" CausesValidation="False" CommandName="Supplier"
                                    Text='<%# DataBinder.eval(Container,"DataItem.SupplierName")%>'>
                                </asp:LinkButton>
                                <asp:Label ID="lblSupplier" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.SupplierID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AO" HeaderText="NAMA CMO">
                            <HeaderStyle Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkAO" CausesValidation="False" CommandName="AO"
                                    Text='<%# DataBinder.eval(Container,"DataItem.AO")%>'>
                                </asp:LinkButton>
                                <asp:Label ID="lblAOID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.AOID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CreditScoringResult" HeaderText="HASIL SCORING">
                            <HeaderStyle Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton runat="server" ID="lnkScoringRslt" CausesValidation="False" CommandName="ScoringResult"
                                    Text='<%# DataBinder.eval(Container,"DataItem.CreditScoringResult")%>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="BranchId" Visible="false"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtGopage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    CARI PEMBIAYAAN APPROVAL REQUEST PENDING</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cabang
                    </label>
                    <asp:DropDownList ID="cboBranch" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                        ControlToValidate="cboBranch" ErrorMessage="Harap Pilih Cabang" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Scoring Pembiayaan
                    </label>
                    <asp:TextBox runat="server" ID="txtDateFrom" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtDateFrom">
                    </asp:CalendarExtender>
                    <label class="label_auto">
                        to</label>
                    <asp:TextBox runat="server" ID="txtDateTo" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtDateTo">
                    </asp:CalendarExtender>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="ApplicationID">No. Aplikasi</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
