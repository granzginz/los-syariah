﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PendingRCA
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New PendingController
    Private m_DataUsercontroller As New DataUserControlController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(viewstate("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property CustID() As String
        Get
            Return CType(viewstate("CustID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustID") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Private Property Type() As String
        Get
            Return CType(viewstate("Type"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Type") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        'lblMessage.Text = ""
        If Not Page.IsPostBack Then
            If CheckForm(Me.Loginid, "PendingRCA", "MAXILOAN") Then
                txtGoPage.Text = "1"
                Me.Sort = "Aging desc"
                InitialPanel()
                FillBranch()
                rgvGo.MaximumValue = "1"
            End If

        End If
    End Sub
    Sub InitialPanel()
        pnlList.Visible = False
        pnlSearch.Visible = True
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
    Sub FillBranch()
        Dim dtbranch As New DataTable
        dtbranch = m_DataUsercontroller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboBranch
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
#End Region
#Region "BindGrid"
    Sub BindGrid()
        pnlList.Visible = True
        pnlSearch.Visible = True
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Pending
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BusinessDate = Me.BusinessDate
        oCustomClass.Table = "spPendingRCAPaging"
        oCustomClass = m_controller.GetPendingDataEntry(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "AgreementNo" Then
        ElseIf e.CommandName = "Name" Then
        ElseIf e.CommandName = "Supplier" Then
        ElseIf e.CommandName = "AO" Then
        ElseIf e.CommandName = "ScoringResult" Then
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtGoPage.Text = "1"
        txtSearch.Text = ""
        txtDateFrom.Text = ""
        txtDateTo.Text = ""
        cboBranch.ClearSelection()
        InitialPanel()
    End Sub
    Function Validator() As Boolean
        If txtDateFrom.Text.Trim <> "" Then
            If txtDateTo.Text.Trim = "" Then
                lblMessage.Text = "Harap isi Tanggal Scoring"
                Return False
            End If
        Else
            If txtDateTo.Text.Trim <> "" Then
                lblMessage.Text = "Harap isi Tanggal Scoring"
                Return False
            End If
        End If
        Return True
    End Function
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'Dim Search As String = "CreditScoringDate < '" & Me.BusinessDate & "'"
        Dim Search As String
        Search = ""
        lblMessage.Text = ""
        If Validator() = False Then
            Exit Sub
        End If
        If txtSearch.Text.Trim <> "" Then
            If cboSearch.SelectedIndex = 0 Then
                Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
            Else
                Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
            End If
        Else
            Me.CmdWhere = "All"
        End If

        If txtDateFrom.Text.Trim <> "" And txtDateTo.Text.Trim <> "" Then
            If DateDiff(DateInterval.Day, ConvertDate2(txtDateFrom.Text), ConvertDate2(txtDateTo.Text)) <= 0 Then
                lblMessage.Text = "Tanggal Awal harus lebih kecil dari Tanggal Akhir"

                pnlList.Visible = False
                Exit Sub
            End If
            If txtDateTo.Text = Me.BusinessDate.ToString("dd/MM/yyyy") Then
                Search = Search + "CreditScoringDate >= '" & ConvertDate(txtDateFrom.Text.Trim) & "' and "
            Else
                Search = Search + "CreditScoringDate >= '" & ConvertDate(txtDateFrom.Text.Trim) & "' and CreditScoringDate <= '" & ConvertDate(txtDateTo.Text.Trim) & "' and "
            End If
        Else
            Search = Search + "CreditScoringDate >= '" & Me.BusinessDate & "' and "
        End If
        If cboBranch.SelectedIndex <> 0 Then
            Search = Search + "BranchId = '" & cboBranch.SelectedValue & "'"
        End If
        Me.CmdWhere2 = Search
        BindGrid()
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnkApp As LinkButton
            lnkApp = CType(e.Item.FindControl("lnkApplication"), LinkButton)
            lnkApp.Attributes.Add("OnClick", "return OpenAppID('" & lnkApp.Text & "','AccAcq');")
            Dim lnkSupplier As LinkButton
            Dim lblSupplier As Label
            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), LinkButton)
            lblSupplier = CType(e.Item.FindControl("lblSupplier"), Label)
            lnkSupplier.Attributes.Add("OnClick", "return OpenSupp('" & lblSupplier.Text & "','AccAcq');")
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkName"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblName"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','AccAcq');")
            Dim lnkAO As LinkButton
            Dim lblAOID As Label
            lnkAO = CType(e.Item.FindControl("lnkAO"), LinkButton)
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)
            lnkAO.Attributes.Add("OnClick", "return OpenAO('" & e.Item.Cells(7).Text & "','" & lblAOID.Text & "','AccAcq');")
        End If
    End Sub
#End Region

End Class