﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PendingInquiry
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New PendingController
    Private d_controller As New DataUserControlController
    Private oCustom As New Parameter.Pending
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region


#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            If CheckForm(Me.Loginid, "", "MAXILOAN") Then
                If SessionInvalid() Then
                End If
                txtPage.Text = "1"
                Me.Sort = "ApplicationID desc"
                rgvGo.MaximumValue = "1"
            End If
            InitialPanelDefault(False)
            'Else
            '    If cboStatusAplikasi.SelectedValue = "APR" Then
            '        GetApproved()
            '    ElseIf cboStatusAplikasi.SelectedValue = "PO" Then
            '        GetAO()
            '    ElseIf cboStatusAplikasi.SelectedValue = "DO" Then
            '        GetDLO()
            '    ElseIf cboStatusAplikasi.SelectedValue = "INV" Then
            '        GetINV()
            '    ElseIf cboStatusAplikasi.SelectedValue = "AKR" Then
            '        GetGLV()
            '    End If
        End If
    End Sub



#Region "Bind All"
    'Protected Function BranchIDChangeAPK() As String
    '    Return "ParentChange('" & Trim(cboParentAPK.ClientID) & "','" & Trim(cboChildAPK.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    'End Function
    'Protected Function BranchIDChangePOP() As String
    '    Return "ParentChange('" & Trim(cboParentPOP.ClientID) & "','" & Trim(cboChildPOP.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    'End Function
    'Protected Function BranchIDChangeDLO() As String
    '    Return "ParentChange('" & Trim(cboParentDLO.ClientID) & "','" & Trim(cboChildDLO.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    'End Function
    'Protected Function BranchIDChangeGLV() As String
    '    Return "ParentChange('" & Trim(cboParentGLV.ClientID) & "','" & Trim(cboChildGLV.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    'End Function
    Sub FillBranchAPK()
        Dim dtbranch As New DataTable
        With cboParentAPK
            If Me.IsHoBranch Then
                .DataSource = d_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            Else
                .DataSource = d_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End If
        End With
    End Sub
    Sub FillBranchRCA()
        Dim dtbranch As New DataTable
        dtbranch = d_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboBranchRCA
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub
    Sub FillBranchAPR()
        Dim dtbranch As New DataTable
        dtbranch = d_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboBranchAPR
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    Function GetApproved() As DataTable
        Dim oPending As New Parameter.Pending
        oPending.strConnection = GetConnectionString()
        Return m_controller.GetApproved(oPending)
    End Function

    'Private Function GenerateScriptApproved(ByVal DtTable As DataTable) As String
    '    Dim strScript As String = ""
    '    Dim strScript1 As String = ""
    '    Dim strType As String
    '    Dim DataRow As DataRow()
    '    Dim i As Int32
    '    Dim j As Int32
    '    strScript = "<script language=""JavaScript"">" & vbCrLf
    '    strScript &= "ListData = new Array(" & vbCrLf
    '    strType = ""
    '    For j = 0 To cboBranchAPR.Items.Count - 1
    '        DataRow = DtTable.Select(" BranchID = '" & cboBranchAPR.Items(j).Value.Trim & "'")
    '        If DataRow.Length > 0 Then
    '            For i = 0 To DataRow.Length - 1
    '                If strType <> CStr(DataRow(i)("BranchID")).Trim Then
    '                    strType = CStr(DataRow(i)("BranchID")).Trim
    '                    strScript &= "new Array(" & vbCrLf
    '                    strScript1 = ""
    '                End If
    '                If strScript1 = "" Then
    '                    strScript1 = " new Array('" & DataRow(i)("UserApproval").ToString.Trim & "','" & DataRow(i)("UserApproval").ToString.Trim & "') "
    '                Else
    '                    strScript1 &= "," & vbCrLf & " new Array('" & DataRow(i)("UserApproval").ToString.Trim & "','" & DataRow(i)("UserApproval").ToString.Trim & "') "
    '                End If
    '            Next
    '            strScript &= strScript1 & ")," & vbCrLf
    '        Else
    '            strScript &= " null," & vbCrLf
    '        End If
    '    Next
    '    If Right(strScript.Trim, 5) = "null," Then
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
    '    Else
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
    '    End If

    '    If Right(strScript.Trim, 4) = "null" Then
    '        strScript &= vbCrLf & ");" & vbCrLf
    '    Else
    '        strScript &= vbCrLf & "));" & vbCrLf
    '    End If
    '    'strScript &= vbCrLf & "));" & vbCrLf

    '    strScript &= "</script>"
    '    Return strScript
    'End Function
    Sub FillBranchPOP()
        Dim Branch_controller As New DataUserControlController
        Dim dtbranch As New DataTable
        With cboParentPOP
            If Me.IsHoBranch Then
                .DataSource = d_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = d_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub

    Function GetAO(ByVal strBranchID As String) As DataTable
        Dim oPending As New Parameter.Pending

        oPending.BranchId = strBranchID
        oPending.strConnection = GetConnectionString()
        Return m_controller.GetAO(oPending)
    End Function

    'Private Function GenerateScriptAO(ByVal DtTable As DataTable) As String
    '    Dim strScript As String = ""
    '    Dim strScript1 As String = ""
    '    Dim DataRow As DataRow()
    '    Dim strType As String
    '    Dim i As Int32
    '    Dim j As Int32
    '    strScript = "<script language=""JavaScript"">" & vbCrLf
    '    strScript &= "ListData = new Array(" & vbCrLf
    '    strType = ""
    '    For j = 0 To cboParentPOP.Items.Count - 1
    '        DataRow = DtTable.Select(" BranchID = '" & cboParentPOP.Items(j).Value & "'")
    '        If DataRow.Length > 0 Then
    '            For i = 0 To DataRow.Length - 1
    '                If strType <> CStr(DataRow(i)("BranchID")).Trim Then
    '                    strType = CStr(DataRow(i)("BranchID")).Trim
    '                    strScript &= "new Array(" & vbCrLf
    '                    strScript1 = ""
    '                End If


    '                If strScript1 = "" Then
    '                    strScript1 = " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
    '                Else
    '                    strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
    '                End If
    '            Next
    '            strScript &= strScript1 & ")," & vbCrLf
    '        Else
    '            strScript &= " null," & vbCrLf
    '        End If
    '    Next

    '    If Right(strScript.Trim, 5) = "null," Then
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
    '    Else
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
    '    End If

    '    If Right(strScript.Trim, 4) = "null" Then
    '        strScript &= vbCrLf & ");" & vbCrLf
    '    Else
    '        strScript &= vbCrLf & "));" & vbCrLf
    '    End If
    '    'strScript &= vbCrLf & "));" & vbCrLf

    '    strScript &= "</script>"
    '    Return strScript
    'End Function

    Sub FillBranchDLO()
        Dim Branch_controller As New DataUserControlController
        Dim dtbranch As New DataTable
        With cboParentDLO
            If Me.IsHoBranch Then
                .DataSource = d_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = d_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub


    'Sub GetDLO()
    '    Dim oPending As New Parameter.Pending
    '    Dim oData As New DataTable
    '    oPending.strConnection = GetConnectionString()
    '    oData = m_controller.GetAO(oPending)
    '    'Response.Write(GenerateScriptDLO(oData))
    'End Sub

    'Private Function GenerateScriptDLO(ByVal DtTable As DataTable) As String
    '    Dim strScript As String = ""
    '    Dim strScript1 As String = ""
    '    Dim DataRow As DataRow()
    '    Dim strType As String
    '    Dim i As Int32
    '    Dim j As Int32
    '    strScript = "<script language=""JavaScript"">" & vbCrLf
    '    strScript &= "ListData = new Array(" & vbCrLf
    '    strType = ""
    '    For j = 0 To cboParentDLO.Items.Count - 1
    '        DataRow = DtTable.Select(" BranchID = '" & cboParentDLO.Items(j).Value & "'")
    '        If DataRow.Length > 0 Then
    '            For i = 0 To DataRow.Length - 1
    '                If strType <> CStr(DataRow(i)("BranchID")).Trim Then
    '                    strType = CStr(DataRow(i)("BranchID")).Trim
    '                    strScript &= "new Array(" & vbCrLf
    '                    strScript1 = ""
    '                End If


    '                If strScript1 = "" Then
    '                    strScript1 = " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
    '                Else
    '                    strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
    '                End If
    '            Next
    '            strScript &= strScript1 & ")," & vbCrLf
    '        Else
    '            strScript &= " null," & vbCrLf
    '        End If
    '    Next

    '    If Right(strScript.Trim, 5) = "null," Then
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
    '    Else
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
    '    End If

    '    If Right(strScript.Trim, 4) = "null" Then
    '        strScript &= vbCrLf & ");" & vbCrLf
    '    Else
    '        strScript &= vbCrLf & "));" & vbCrLf
    '    End If
    '    'strScript &= vbCrLf & "));" & vbCrLf

    '    strScript &= "</script>"
    '    Return strScript
    'End Function

    Sub FillBranchINV()
        Dim dtbranch As New DataTable
        dtbranch = d_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboBranchINV
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    'Sub GetINV()
    '    Dim oPending As New Parameter.Pending
    '    Dim oData As New DataTable
    '    oPending.strConnection = GetConnectionString()
    '    oData = m_controller.GetAO(oPending)
    '    'Response.Write(GenerateScriptAO(oData))
    'End Sub
    'Private Function GenerateScriptINV(ByVal DtTable As DataTable) As String
    '    Dim strScript As String = ""
    '    Dim strScript1 As String = ""
    '    Dim strType As String
    '    Dim DataRow As DataRow()
    '    Dim i As Int32
    '    Dim j As Int32
    '    strScript = "<script language=""JavaScript"">" & vbCrLf
    '    strScript &= "ListData = new Array(" & vbCrLf
    '    strType = ""
    '    For j = 0 To cboBranchINV.Items.Count - 1
    '        DataRow = DtTable.Select(" BranchID = '" & cboBranchINV.Items(j).Value.Trim & "'")
    '        If DataRow.Length > 0 Then
    '            For i = 0 To DataRow.Length - 1
    '                If strType <> CStr(DataRow(i)("BranchID")).Trim Then
    '                    strType = CStr(DataRow(i)("BranchID")).Trim
    '                    strScript &= "new Array(" & vbCrLf
    '                    strScript1 = ""
    '                End If
    '                If strScript1 = "" Then
    '                    strScript1 = " new Array('" & DataRow(i)("employeename").ToString.Trim & "','" & DataRow(i)("EmployeeId").ToString.Trim & "') "
    '                Else
    '                    strScript1 &= "," & vbCrLf & " new Array('" & DataRow(i)("employeename").ToString.Trim & "','" & DataRow(i)("EmployeeId").ToString.Trim & "') "
    '                End If
    '            Next
    '            strScript &= strScript1 & ")," & vbCrLf
    '        Else
    '            strScript &= " null," & vbCrLf
    '        End If
    '    Next
    '    If Right(strScript.Trim, 5) = "null," Then
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
    '    Else
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
    '    End If
    '    If Right(strScript.Trim, 4) = "null" Then
    '        strScript &= vbCrLf & ");" & vbCrLf
    '    Else
    '        strScript &= vbCrLf & "));" & vbCrLf
    '    End If
    '    strScript &= "</script>"
    '    Return strScript
    'End Function

    Sub FillBranchGLV()
        Dim Branch_controller As New DataUserControlController
        Dim dtbranch As New DataTable
        With cboParentGLV
            If Me.IsHoBranch Then
                .DataSource = d_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = d_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub

    Sub GetGLV()
        Dim oPending As New Parameter.Pending
        Dim oData As New DataTable
        oPending.strConnection = GetConnectionString()
        oData = m_controller.GetAO(oPending)
        'Response.Write(GenerateScriptGLV(oData))
    End Sub
    'Private Function GenerateScriptGLV(ByVal DtTable As DataTable) As String
    '    Dim strScript As String = ""
    '    Dim strScript1 As String = ""
    '    Dim DataRow As DataRow()
    '    Dim strType As String
    '    Dim i As Int32
    '    Dim j As Int32
    '    strScript = "<script language=""JavaScript"">" & vbCrLf
    '    strScript &= "ListData = new Array(" & vbCrLf
    '    strType = ""
    '    For j = 0 To cboParentGLV.Items.Count - 1
    '        DataRow = DtTable.Select(" BranchID = '" & cboParentGLV.Items(j).Value & "'")
    '        If DataRow.Length > 0 Then
    '            For i = 0 To DataRow.Length - 1
    '                If strType <> CStr(DataRow(i)("BranchID")).Trim Then
    '                    strType = CStr(DataRow(i)("BranchID")).Trim
    '                    strScript &= "new Array(" & vbCrLf
    '                    strScript1 = ""
    '                End If


    '                If strScript1 = "" Then
    '                    strScript1 = " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
    '                Else
    '                    strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("employeename")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("EmployeeId")), "null", DataRow(i)("EmployeeId"))).Trim & "') "
    '                End If
    '            Next
    '            strScript &= strScript1 & ")," & vbCrLf
    '        Else
    '            strScript &= " null," & vbCrLf
    '        End If
    '    Next

    '    If Right(strScript.Trim, 5) = "null," Then
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
    '    Else
    '        strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
    '    End If

    '    If Right(strScript.Trim, 4) = "null" Then
    '        strScript &= vbCrLf & ");" & vbCrLf
    '    Else
    '        strScript &= vbCrLf & "));" & vbCrLf
    '    End If
    '    'strScript &= vbCrLf & "));" & vbCrLf

    '    strScript &= "</script>"
    '    Return strScript
    'End Function
    'Sub StatusAplikasi_IndexChanged()

    '    If cboStatusAplikasi.SelectedValue = "APK" Then
    '        InitialPanelNAP(False)
    '    ElseIf cboStatusAplikasi.SelectedValue = "REQ" Then
    '        InitialPanelRCA(False)
    '    ElseIf cboStatusAplikasi.SelectedValue = "APR" Then
    '        InitialPanelAPR(False)
    '    ElseIf cboStatusAplikasi.SelectedValue = "PO" Then
    '        InitialPanelPOP(False)
    '    ElseIf cboStatusAplikasi.SelectedValue = "DO" Then
    '        InitialPanelDLO(False)
    '    ElseIf cboStatusAplikasi.SelectedValue = "INV" Then
    '        InitialPanelINV(False)
    '    ElseIf cboStatusAplikasi.SelectedValue = "AKR" Then
    '        InitialPanelGLV(False)
    '    End If

    'End Sub
#End Region


#Region "Set Panel"
    Sub InitialPanelDefault(ByVal StatusGrid As Boolean)

        pnlGrid.Visible = StatusGrid
        pnlSearchAPK.Visible = False
        pnlSearchAPR.Visible = False
        pnlSearchDLO.Visible = False
        pnlSearchGLV.Visible = False
        pnlSearchINV.Visible = False
        pnlSearchPOP.Visible = False
        pnlSearchRCA.Visible = False
    End Sub
    Sub InitialPanelNAP(ByVal StatusGrid As Boolean)
        pnlGrid.Visible = StatusGrid
        pnlSearchAPK.Visible = True
        pnlSearchAPR.Visible = False
        pnlSearchDLO.Visible = False
        pnlSearchGLV.Visible = False
        pnlSearchINV.Visible = False
        pnlSearchPOP.Visible = False
        pnlSearchRCA.Visible = False


    End Sub
    Sub InitialPanelRCA(ByVal StatusGrid As Boolean)
        pnlGrid.Visible = StatusGrid
        pnlSearchAPK.Visible = False
        pnlSearchAPR.Visible = False
        pnlSearchDLO.Visible = False
        pnlSearchGLV.Visible = False
        pnlSearchINV.Visible = False
        pnlSearchPOP.Visible = False
        pnlSearchRCA.Visible = True

    End Sub
    Sub InitialPanelAPR(ByVal StatusGrid As Boolean)
        pnlGrid.Visible = StatusGrid
        pnlSearchAPK.Visible = False
        pnlSearchAPR.Visible = True
        pnlSearchDLO.Visible = False
        pnlSearchGLV.Visible = False
        pnlSearchINV.Visible = False
        pnlSearchPOP.Visible = False
        pnlSearchRCA.Visible = False
    End Sub
    Sub InitialPanelPOP(ByVal StatusGrid As Boolean)
        Dim dtsupplier As New DataTable
        Dim cmdwheresupplier As String
        Dim oCustomClass As New Parameter.Sales
        Dim ocontroller As New RptSalesController

        pnlGrid.Visible = StatusGrid
        pnlSearchAPK.Visible = False
        pnlSearchAPR.Visible = False
        pnlSearchDLO.Visible = False
        pnlSearchGLV.Visible = False
        pnlSearchINV.Visible = False
        pnlSearchPOP.Visible = True
        pnlSearchRCA.Visible = False
    End Sub


    Sub InitialPanelDLO(ByVal StatusGrid As Boolean)

        pnlGrid.Visible = StatusGrid
        pnlSearchAPK.Visible = False
        pnlSearchAPR.Visible = False
        pnlSearchDLO.Visible = True
        pnlSearchGLV.Visible = False
        pnlSearchINV.Visible = False
        pnlSearchPOP.Visible = False
        pnlSearchRCA.Visible = False


    End Sub



    Sub InitialPanelINV(ByVal StatusGrid As Boolean)
        pnlGrid.Visible = StatusGrid
        pnlSearchAPK.Visible = False
        pnlSearchAPR.Visible = False
        pnlSearchDLO.Visible = False
        pnlSearchGLV.Visible = False
        pnlSearchINV.Visible = True
        pnlSearchPOP.Visible = False
        pnlSearchRCA.Visible = False
    End Sub

    Sub InitialPanelGLV(ByVal StatusGrid As Boolean)
        pnlGrid.Visible = StatusGrid
        pnlSearchAPK.Visible = False
        pnlSearchAPR.Visible = False
        pnlSearchDLO.Visible = False
        pnlSearchGLV.Visible = True
        pnlSearchINV.Visible = False
        pnlSearchPOP.Visible = False
        pnlSearchRCA.Visible = False

    End Sub

#End Region



#Region "Button"
    Protected Sub buttonSearchAPK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearchAPK.Click
        Dim Search As String = "Agreement.NewApplicationDate < '" & Me.BusinessDate & "'"

        If txtSearchAPK.Text.Trim <> "" Then
            If cboSearchAPK.SelectedIndex = 1 Then
                Search += " AND " + cboSearchAPK.SelectedItem.Value + " = '" + Replace(txtSearchAPK.Text.Trim, "'", "''") + "' "
            Else
                Search += " AND " + cboSearchAPK.SelectedItem.Value + " = '" + txtSearchAPK.Text.Trim + "' "
            End If
        End If

        If cboSupplierAPK.SelectedValue.Trim <> "0" Then
            Search += " AND Agreement.SupplierID = '" & cboSupplierAPK.SelectedValue & "' "
        End If

        If cboChildAPK.SelectedItem.Text <> "All" Then
            Search += " AND Agreement.AOID = '" & cboChildAPK.SelectedValue & "' "
        End If

        If txtDateFromAPK.Text.Trim <> "" And txtDateToAPK.Text.Trim <> "" Then
            Search += " AND Agreement.NewApplicationDate between '" & ConvertDate(txtDateFromAPK.Text.Trim) & "' and '" & ConvertDate(txtDateToAPK.Text.Trim) & "' "
        End If

        Search += " AND Agreement.BranchId = '" & cboParentAPK.SelectedValue & "'"

        Search += " AND Agreement.DateEntryApplicationData IS NOT NULL AND " _
        + "Agreement.DateEntryAssetData IS NOT NULL AND " _
        + "Agreement.DateEntryFinancialData IS NOT NULL AND " _
        + "Agreement.DateEntryInsuranceData IS NOT NULL AND " _
        + "Agreement.ApplicationStep = 'NAP' AND " _
        + "Agreement.IsRejected = 0 And " _
        + "Agreement.IsCancelled = 0 "

        Me.CmdWhere = Search
        BindGridEntity(Me.CmdWhere)
        InitialPanelNAP(True)
    End Sub

    Protected Sub btnResetAPK_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnResetAPK.Click
        InitialPanelDefault(False)
    End Sub

    Protected Sub btnSearchRCA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearchRCA.Click
        Dim Search As String = ""
        If txtSearchRCA.Text.Trim <> "" Then
            If cboSearchRCA.SelectedIndex = 0 Then
                Search += " " + cboSearchRCA.SelectedItem.Value + " = '" + Replace(txtSearchRCA.Text.Trim, "'", "''") + "' and"
            Else
                Search += " " + cboSearchRCA.SelectedItem.Value + " = '" + txtSearchRCA.Text.Trim + "' and"
            End If
        End If

        If txtDateFromRCA.Text.Trim <> "" And txtDateToRCA.Text.Trim <> "" Then
            If DateDiff(DateInterval.Day, ConvertDate2(txtDateFromRCA.Text), ConvertDate2(txtDateToRCA.Text)) <= 0 Then
                ShowMessage(lblMessage, "Tanggal Awal harus lebih kecil dari Tanggal Akhir", True)
                Exit Sub
            End If
            If txtDateToRCA.Text = Me.BusinessDate.ToString("dd/MM/yyyy") Then
                Search += " Agreement.CreditScoringDate >= '" & ConvertDate(txtDateFromRCA.Text.Trim) & "' and "
            Else
                Search += " Agreement.CreditScoringDate >= '" & ConvertDate(txtDateFromRCA.Text.Trim) & "' and Agreement.CreditScoringDate <= '" & ConvertDate(txtDateToRCA.Text.Trim) & "' and "
            End If
        Else
            Search += " Agreement.CreditScoringDate >= '" & Me.BusinessDate & "' and "
        End If
        If cboBranchRCA.SelectedIndex <> 0 Then
            Search += " Agreement.BranchId = '" & cboBranchRCA.SelectedValue & "'"
        End If

        Me.CmdWhere = " Agreement.ApplicationStep = 'CSR' AND " _
        + "Agreement.IsRejected = 0 AND " _
        + "Agreement.IsCancelled = 0 AND"

        Me.CmdWhere += " " + Search

        BindGridEntity(Me.CmdWhere)
        InitialPanelRCA(True)
    End Sub

    Protected Sub btnResetRCA_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnResetRCA.Click
        InitialPanelDefault(False)
    End Sub

    Protected Sub btnSearchAPR_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearchAPR.Click
        Dim Search As String = ""

        If txtSearchAPR.Text.Trim <> "" Then
            If cboSearchAPR.SelectedIndex = 0 Then
                Search += " " + cboSearchAPR.SelectedItem.Value + " = '" + Replace(txtSearchAPR.Text.Trim, "'", "''") + "' and"
            Else
                Search += " " + cboSearchAPR.SelectedItem.Value + " = '" + txtSearchAPR.Text.Trim + "' and"
            End If
        End If

        If cboApprovedAPR.SelectedItem.Text <> "All" Then
            Search += " Approval.UserApproval = '" & cboApprovedAPR.SelectedValue & "' and "
        End If

        If txtDateFromAPR.Text.Trim <> "" And txtDateToAPR.Text.Trim <> "" Then
            If DateDiff(DateInterval.Day, ConvertDate2(txtDateFromAPR.Text), ConvertDate2(txtDateToAPR.Text)) <= 0 Then
                ShowMessage(lblMessage, "Tanggal Akhir harus lebih < dari Tanggal Awal", True)
            End If

            If txtDateToAPR.Text = Me.BusinessDate.ToString("dd/MM/yyyy") Then
                Search += " Agreement.RCADate >= '" & ConvertDate(txtDateFromAPR.Text.Trim) & "' and "
            Else
                Search += " Agreement.RCADate >= '" & ConvertDate(txtDateFromAPR.Text.Trim) & "' and Agreement.RCADate <= '" & ConvertDate(txtDateToAPR.Text.Trim) & "' and "
            End If

        Else
            Search += " Agreement.RCADate >= '" & Me.BusinessDate & "' and "
        End If
        If cboBranchAPR.SelectedIndex <> 0 Then
            Search = Search + "Agreement.BranchId = '" & cboBranchAPR.SelectedValue & "'"
        End If

        Me.CmdWhere = " Agreement.ApplicationStep = 'RCA' AND " _
        + "Agreement.IsRejected = 0 AND " _
        + "Agreement.IsCancelled = 0 AND"

        Me.CmdWhere += " " + Search

        BindGridEntity(Me.CmdWhere)
        InitialPanelAPR(True)
    End Sub

    Protected Sub btnResetAPR_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnResetAPR.Click
        InitialPanelDefault(False)
    End Sub

    Protected Sub buttonSearchPOP_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearchPOP.Click
        Dim Search As String = ""

        If txtSearchPOP.Text.Trim <> "" Then
            If cboSearchPOP.SelectedIndex = 1 Then
                Search += " " + cboSearchPOP.SelectedItem.Value + " = '" + Replace(txtSearchPOP.Text.Trim, "'", "''") + "' and"
            Else
                Search += " " + cboSearchPOP.SelectedItem.Value + " = '" + txtSearchPOP.Text.Trim + "' and"
            End If
        End If

        If cboSupplierPOP.SelectedItem.Text.Trim <> "All" Then
            Search += " Agreement.SupplierID = '" & cboSupplierPOP.SelectedItem.Value.Trim & "' and"
        End If

        If cboChildPOP.SelectedItem.Text <> "All" Then
            Search += " BranchEmployee.EmployeeID = '" & cboChildPOP.SelectedValue & "' and"
        End If

        If txtDateFromPOP.Text.Trim <> "" And txtDateToPOP.Text.Trim <> "" Then
            If DateDiff(DateInterval.Day, ConvertDate2(txtDateFromPOP.Text), ConvertDate2(txtDateToPOP.Text)) <= 0 Then
                ShowMessage(lblMessage, "Tanggal Awal Harus Lebih Kecil dari Tanggal Akhir", True)
            End If

            If txtDateToPOP.Text = Me.BusinessDate.ToString("dd/MM/yyyy") Then
                Search += " Agreement.ApprovalDate >= '" & ConvertDate(txtDateFromPOP.Text.Trim) & "' and"
            Else
                Search += " Agreement.ApprovalDate >= '" & ConvertDate(txtDateFromPOP.Text.Trim) & "' and Agreement.ApprovalDate <= '" & ConvertDate(txtDateToPOP.Text.Trim) & "' and"
            End If
        Else
            Search += " Agreement.ApprovalDate >= '" & Me.BusinessDate & "' and"
        End If
        Search += " Agreement.BranchId = '" & cboParentPOP.SelectedValue & "'"

        Me.CmdWhere = " Agreement.ApplicationStep = 'APR' AND " _
            + "Agreement.IsRejected = 0 AND " _
            + "Agreement.IsCancelled = 0 And"

        Me.CmdWhere += " " + Search

        BindGridEntity(Me.CmdWhere)
        InitialPanelPOP(True)
    End Sub

    Protected Sub btnResetPOP_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnResetPOP.Click
        InitialPanelDefault(False)
    End Sub

    Protected Sub buttonSearchDLO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearchDLO.Click
        Dim Search As String = ""

        If txtSearchDLO.Text.Trim <> "" Then
            If cboSearchDLO.SelectedIndex = 0 Then
                Search += cboSearchDLO.SelectedItem.Value + " = '" + Replace(txtSearchDLO.Text.Trim, "'", "''") + "' and"
            Else
                Search += cboSearchDLO.SelectedItem.Value + " = '" + txtSearchDLO.Text.Trim + "' and"
            End If
        End If

        If cboSupplierDLO.SelectedItem.Text <> "All" Then
            Search += " Agreement.SupplierID = '" & cboSupplierDLO.SelectedItem.Value.Trim & "' and "
        End If

        If cboChildDLO.SelectedItem.Text <> "All" Then
            Search += " Agreement.AOID = '" & cboChildDLO.SelectedValue & "' and "
        End If

        If txtDateFromDLO.Text.Trim <> "" And txtDateToDLO.Text.Trim <> "" Then
            If DateDiff(DateInterval.Day, ConvertDate2(txtDateFromDLO.Text), ConvertDate2(txtDateToDLO.Text)) <= 0 Then
                ShowMessage(lblMessage, "Tanggal Awal harus lebih kecil dari tanggal Akhir", True)
            End If
            If txtDateToDLO.Text = Me.BusinessDate.ToString("dd/MM/yyyy") Then
                Search += " Agreement.PurchaseOrderDate >= '" & ConvertDate(txtDateFromDLO.Text.Trim) & "' and "
            Else
                Search += " Agreement.PurchaseOrderDate >= '" & ConvertDate(txtDateFromDLO.Text.Trim) & "' and Agreement.PurchaseOrderDate <= '" & ConvertDate(txtDateToDLO.Text.Trim) & "' and "
            End If

        Else
            Search += " Agreement.PurchaseOrderDate >= '" & Me.BusinessDate & "' and "
        End If
        Search += " Agreement.BranchId = '" & cboParentDLO.SelectedValue & "'"

        Me.CmdWhere = " Agreement.ApplicationStep = 'POP' AND " _
        + "Agreement.IsRejected = 0 AND " _
        + "Agreement.IsCancelled = 0 and"

        Me.CmdWhere += " " + Search

        BindGridEntity(Me.CmdWhere)
        InitialPanelDLO(True)
    End Sub

    Protected Sub btnResetDLO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnResetDLO.Click
        InitialPanelDefault(False)
    End Sub

    Protected Sub btnSearchINV_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearchINV.Click
        Dim Search As String = ""
        If txtSearchINV.Text.Trim <> "" Then
            If cboSearchINV.SelectedIndex = 1 Then
                Search += " " + cboSearchINV.SelectedItem.Value + " = '" + Replace(txtSearchINV.Text.Trim, "'", "''") + "' and"
            Else
                Search += " " + cboSearchINV.SelectedItem.Value + " = '" + txtSearchINV.Text.Trim + "' and"
            End If
        End If

        If cboBranchINV.SelectedIndex <> 0 Then
            Search += " Agreement.BranchId = '" & cboBranchINV.SelectedValue & "' and"
            If cboAOINV.SelectedItem.Text <> "ALL" Then
                Search += " Agreement.AOID = '" & cboAOINV.SelectedValue & "' and"
            End If
        End If

        If txtDateFromINV.Text.Trim <> "" And txtDateToINV.Text.Trim <> "" Then
            Search += " Agreement.DeliveryOrderDate between '" & ConvertDate(txtDateFromINV.Text.Trim) & "' and '" & ConvertDate(txtDateToINV.Text.Trim) & "' and"
        End If

        Search += " Agreement.DeliveryOrderDate < '" & Me.BusinessDate & "'"

        Me.CmdWhere = " Agreement.ApplicationStep = 'DLO' OR " _
        + "Agreement.ApplicationStep = 'GLV' AND " _
        + "Agreement.IsRejected = 0 AND " _
        + "Agreement.IsCancelled = 0 AND " _
        + "Agreement.ApplicationId not in (select applicationId from InvoiceAgreement) and"

        Me.CmdWhere += " " + Search

        BindGridEntity(Me.CmdWhere)
        InitialPanelINV(True)
    End Sub

    Protected Sub btnResetINV_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnResetINV.Click
        InitialPanelDefault(False)
    End Sub

    Protected Sub buttonSearchGLV_Click(ByVal sender As Object, ByVal e As EventArgs) Handles buttonSearchGLV.Click
        Dim Search As String = ""

        If txtSearchGLV.Text.Trim <> "" Then
            If cboSearchGLV.SelectedIndex = 0 Then
                Search += cboSearchGLV.SelectedItem.Value + " = '" + Replace(txtSearchGLV.Text.Trim, "'", "''") + "' and"
            Else
                Search += cboSearchGLV.SelectedItem.Value + " = '" + txtSearchGLV.Text.Trim + "' and"
            End If
        End If

        If cboSupplierGLV.SelectedItem.Text <> "All" Then
            Search += " Agreement.SupplierID = '" & cboSupplierGLV.SelectedItem.Value.Trim & "' and "
        End If

        If cboChildGLV.SelectedItem.Text <> "All" Then
            Search += " Agreement.AOID = '" & cboChildGLV.SelectedValue & "' and "
        End If

        If txtDateFromGLV.Text.Trim <> "" And txtDateToGLV.Text.Trim <> "" Then
            If DateDiff(DateInterval.Day, ConvertDate2(txtDateFromGLV.Text), ConvertDate2(txtDateToGLV.Text)) <= 0 Then
                ShowMessage(lblMessage, "Tanggal Awal harus lebih kecil dari tgl Akhir", True)
            End If
            If txtDateToGLV.Text = Me.BusinessDate.ToString("dd/MM/yyyy") Then
                Search += " Agreement.DeliveryOrderDate >= '" & ConvertDate(txtDateFromGLV.Text.Trim) & "' and "
            Else
                Search += " Agreement.DeliveryOrderDate >= '" & ConvertDate(txtDateFromGLV.Text.Trim) & "' and Agreement.DeliveryOrderDate <= '" & ConvertDate(txtDateToGLV.Text.Trim) & "' and "
            End If
        Else
            Search += " Agreement.DeliveryOrderDate >= '" & Me.BusinessDate & "' and "
        End If

        Search += " Agreement.BranchId = '" & cboParentGLV.SelectedValue & "'"

        Me.CmdWhere = " Agreement.ApplicationStep = 'DLO' AND " _
        + "Agreement.IsRejected = 0 AND " _
        + "Agreement.IsCancelled = 0 and"

        Me.CmdWhere += " " + Search

        BindGridEntity(Me.CmdWhere)
        InitialPanelGLV(True)
    End Sub

    Protected Sub btnResetGLV_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnResetGLV.Click
        InitialPanelDefault(False)
    End Sub

#End Region




#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region



    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing

        oCustom.strConnection = GetConnectionString()
        oCustom.WhereCond = cmdWhere
        oCustom.CurrentPage = currentPage
        oCustom.PageSize = pageSize
        oCustom.SortBy = Me.Sort
        oCustom.BusinessDate = Me.BusinessDate
        oCustom = m_controller.GetApplicationPendingInquiry(oCustom)

        If Not oCustom Is Nothing Then
            dtEntity = oCustom.listdata
            recordCount = oCustom.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkAgreementNo As HyperLink
        Dim lnkName As HyperLink
        Dim lnkSupplierName As HyperLink
        Dim lnkAO As HyperLink
        Dim lblCustomerID As Label
        Dim lblSupplierID As Label
        Dim lblEmployeeID As Label
        Dim lblBranchID As Label
        If e.Item.ItemIndex >= 0 Then
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            lnkSupplierName = CType(e.Item.FindControl("lnkSupplierName"), HyperLink)
            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblEmployeeID = CType(e.Item.FindControl("lblEmployeeID"), Label)
            lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)


            lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & dtgPaging.DataKeyField(e.Item.ItemIndex).ToString & "')"
            lnkName.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & lblCustomerID.Text & "')"
            lnkAO.NavigateUrl = "javascript:OpenAO('" & "AccAcq" & "', '" & lblBranchID.Text & "','" & lblEmployeeID.Text & "')"
            lnkSupplierName.NavigateUrl = "javascript:OpenWinSupplier('" & "AccAcq" & "', '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"

            If CDate(e.Item.Cells(0).Text).ToString("dd/MM/yyyy") = "01/01/1900" Then
                e.Item.Cells(0).Text = ""
            Else
                e.Item.Cells(0).Text = CDate(e.Item.Cells(0).Text).ToString("dd/MM/yyyy")
            End If

            If CDate(e.Item.Cells(1).Text).ToString("dd/MM/yyyy") = "01/01/1900" Then
                e.Item.Cells(1).Text = ""
            Else
                e.Item.Cells(1).Text = CDate(e.Item.Cells(1).Text).ToString("dd/MM/yyyy")
            End If

            If CDate(e.Item.Cells(2).Text).ToString("dd/MM/yyyy") = "01/01/1900" Then
                e.Item.Cells(2).Text = ""
            Else
                e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).ToString("dd/MM/yyyy")
            End If

            If CDate(e.Item.Cells(3).Text).ToString("dd/MM/yyyy") = "01/01/1900" Then
                e.Item.Cells(3).Text = ""
            Else
                e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToString("dd/MM/yyyy")
            End If

            If CDate(e.Item.Cells(4).Text).ToString("dd/MM/yyyy") = "01/01/1900" Then
                e.Item.Cells(4).Text = ""
            Else
                e.Item.Cells(4).Text = CDate(e.Item.Cells(4).Text).ToString("dd/MM/yyyy")
            End If

            If CDate(e.Item.Cells(5).Text).ToString("dd/MM/yyyy") = "01/01/1900" Then
                e.Item.Cells(5).Text = ""
            Else
                e.Item.Cells(5).Text = CDate(e.Item.Cells(5).Text).ToString("dd/MM/yyyy")
            End If

            If CDate(e.Item.Cells(12).Text).ToString("dd/MM/yyyy") = "01/01/1900" Then
                e.Item.Cells(12).Text = ""
            Else
                e.Item.Cells(12).Text = CDate(e.Item.Cells(12).Text).ToString("dd/MM/yyyy")
            End If

        End If
    End Sub

    Private Sub cboParentAPK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboParentAPK.SelectedIndexChanged
        With cboChildAPK
            .Items.Clear()
            .DataSource = GetAO(cboParentAPK.SelectedValue)
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataBind()
            .Items.Insert(0, "All")
        End With


        With cboSupplierAPK
            .Items.Clear()
            .DataTextField = "SupplierName"
            .Items.Insert(0, "ALL")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub cboStatusAplikasi_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatusAplikasi.SelectedIndexChanged
        If cboStatusAplikasi.SelectedValue = "APK" Then
            InitialPanelNAP(False)
            FillBranchAPK()
            cboParentAPK_SelectedIndexChanged(sender, e)
        ElseIf cboStatusAplikasi.SelectedValue = "REQ" Then
            InitialPanelRCA(False)
            FillBranchRCA()
        ElseIf cboStatusAplikasi.SelectedValue = "APR" Then
            InitialPanelAPR(False)
            FillBranchAPR()
            cboBranchAPR_SelectedIndexChanged(sender, e)
        ElseIf cboStatusAplikasi.SelectedValue = "PO" Then
            InitialPanelPOP(False)
            FillBranchPOP()
        ElseIf cboStatusAplikasi.SelectedValue = "DO" Then
            InitialPanelDLO(False)
            FillBranchDLO()
        ElseIf cboStatusAplikasi.SelectedValue = "INV" Then
            InitialPanelINV(False)
            FillBranchINV()
        ElseIf cboStatusAplikasi.SelectedValue = "AKR" Then
            InitialPanelGLV(False)
            FillBranchGLV()
        End If
    End Sub

    Private Sub cboBranchAPR_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranchAPR.SelectedIndexChanged
        With cboApprovedAPR
            .Items.Clear()
            .DataSource = GetApproved()
            .DataTextField = "UserApproval"
            .DataValueField = "UserApproval"
            .DataBind()
            .Items.Insert(0, "All")
        End With
    End Sub

    Private Sub cboParentPOP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboParentPOP.SelectedIndexChanged
        With cboChildPOP
            .Items.Clear()
            .DataSource = GetAO(cboParentPOP.SelectedValue)
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataBind()
            .Items.Insert(0, "All")
        End With

        With cboSupplierPOP
            .Items.Clear()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub cboChildAPK_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChildAPK.SelectedIndexChanged
        Dim cmdwheresupplier As String
        Dim oCustomClass As New Parameter.Sales
        Dim ocontroller As New RptSalesController


        If cboChildAPK.SelectedValue = "0" Then
            cmdwheresupplier = "supplierbranch.branchid = '" & cboParentAPK.SelectedItem.Value.Trim & "'"
        Else
            cmdwheresupplier = "supplierbranch.AOID = '" & cboChildAPK.SelectedValue & "' and "
            cmdwheresupplier = cmdwheresupplier + "supplierbranch.branchid = '" & cboParentAPK.SelectedItem.Value.Trim & "'"
        End If


        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdwheresupplier
        End With

        With cboSupplierAPK
            .Items.Clear()
            .DataTextField = "SupplierName"
            .DataValueField = "SupplierId"
            .DataSource = ocontroller.GetSupplier(oCustomClass)
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub cboChildPOP_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChildPOP.SelectedIndexChanged
        Dim cmdwheresupplier As String
        Dim oCustomClass As New Parameter.Sales
        Dim ocontroller As New RptSalesController

        If cboChildPOP.SelectedValue = "0" Then
            cmdwheresupplier = "supplierbranch.branchid = '" & cboParentAPK.SelectedItem.Value.Trim & "'"
        Else
            cmdwheresupplier = "supplierbranch.AOID = '" & cboChildPOP.SelectedValue & "' and "
            cmdwheresupplier = cmdwheresupplier + "supplierbranch.branchid = '" & cboParentPOP.SelectedItem.Value.Trim & "'"
        End If


        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdwheresupplier
        End With

        With cboSupplierPOP
            .Items.Clear()
            .DataTextField = "SupplierName"
            .DataValueField = "SupplierId"
            .DataSource = ocontroller.GetSupplier(oCustomClass)
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub cboParentDLO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboParentDLO.SelectedIndexChanged
        With cboChildDLO
            .Items.Clear()
            .DataSource = GetAO(cboParentDLO.SelectedValue)
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataBind()
            .Items.Insert(0, "All")
        End With

        With cboSupplierDLO
            .Items.Clear()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub

   
    Private Sub cboChildDLO_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChildDLO.SelectedIndexChanged
        Dim cmdwheresupplier As String
        Dim oCustomClass As New Parameter.Sales
        Dim ocontroller As New RptSalesController

        If cboChildDLO.SelectedValue = "0" Then
            cmdwheresupplier = "supplierbranch.branchid = '" & cboParentDLO.SelectedItem.Value.Trim & "'"
        Else
            cmdwheresupplier = "supplierbranch.AOID = '" & cboChildDLO.SelectedValue & "' and "
            cmdwheresupplier = cmdwheresupplier + "supplierbranch.branchid = '" & cboParentDLO.SelectedItem.Value.Trim & "'"
        End If


        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdwheresupplier
        End With

        With cboSupplierDLO
            .Items.Clear()
            .DataTextField = "SupplierName"
            .DataValueField = "SupplierId"
            .DataSource = ocontroller.GetSupplier(oCustomClass)
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub cboBranchINV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranchINV.SelectedIndexChanged
        With cboAOINV
            .Items.Clear()
            .DataSource = GetAO(cboBranchINV.SelectedValue)
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataBind()
            .Items.Insert(0, "All")
        End With
    End Sub

    Private Sub cboParentGLV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboParentGLV.SelectedIndexChanged
        With cboChildGLV
            .Items.Clear()
            .DataSource = GetAO(cboParentGLV.SelectedValue)
            .DataTextField = "EmployeeName"
            .DataValueField = "EmployeeID"
            .DataBind()
            .Items.Insert(0, "All")
        End With

        With cboSupplierGLV
            .Items.Clear()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub cboChildGLV_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChildGLV.SelectedIndexChanged
        Dim cmdwheresupplier As String
        Dim oCustomClass As New Parameter.Sales
        Dim ocontroller As New RptSalesController

        If cboChildGLV.SelectedValue = "0" Then
            cmdwheresupplier = "supplierbranch.branchid = '" & cboParentGLV.SelectedItem.Value.Trim & "'"
        Else
            cmdwheresupplier = "supplierbranch.AOID = '" & cboChildGLV.SelectedValue & "' and "
            cmdwheresupplier = cmdwheresupplier + "supplierbranch.branchid = '" & cboParentGLV.SelectedItem.Value.Trim & "'"
        End If


        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdwheresupplier
        End With

        With cboSupplierGLV
            .Items.Clear()
            .DataTextField = "SupplierName"
            .DataValueField = "SupplierId"
            .DataSource = ocontroller.GetSupplier(oCustomClass)
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "0"
            .SelectedIndex = 0
        End With
    End Sub
End Class