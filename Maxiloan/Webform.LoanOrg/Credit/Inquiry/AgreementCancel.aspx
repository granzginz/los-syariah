﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgreementCancel.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.AgreementCancel" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ActivityLog</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR KONTRAK
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlDtGrid" runat="server">
                <div class="form_box_header">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgActivity" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            DataKeyField="AgreementNo" CssClass="grid_general" BorderStyle="None" BorderWidth="0"
                            OnSortCommand="SortGrid" Width="100%">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn SortExpression="APPLICATIONID" HeaderText="NO APLIKASI">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblApplicationID" runat="server" Text='<%# Container.DataItem("ApplicationID")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AGREEMENTNO" HeaderText="NO KONTRAK">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblAgreementNo" runat="server" Text='<%# Container.DataItem("AgreementNo")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="CUSTOMERNAME" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lblCustomerName" runat="server" Text='<%# Container.DataItem("Name")%>'>
                                        </asp:HyperLink>
                                        <asp:Label ID="lblCustomerId" runat="server" Text='<%# Container.dataitem("CustomerID")%>'
                                            Visible="False">
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                    DataField="ApplicationStep" SortExpression="ApplicationStep" HeaderText="STEP">
                                </asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                    DataField="CancellationDate" SortExpression="CancellationDate" HeaderText="TANGGAL"
                                    DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    DataField="Description" SortExpression="Description" HeaderText="ALASAN"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left"
                                    DataField="NotesOfCancellation" SortExpression="NotesOfCancellation" HeaderText="CATATAN">
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonViewReport" runat="server" CausesValidation="True" Enabled="True"
                        Text="View Report" CssClass="small button blue" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlsearch" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            CARI KONTRAK YANG BATAL/REJECT
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:DropDownList ID="cboParent" runat="server" Width="100px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvBranch" runat="server" ErrorMessage="Harap Pilih Cabang"
                            InitialValue="0" ControlToValidate="cboParent" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server" Width="100px">
                            <asp:ListItem Value="Name">Nama</asp:ListItem>
                            <asp:ListItem Value="ApplicationID">No Aplikasi</asp:ListItem>
                            <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearchBy" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Periode</label>
                        <asp:TextBox ID="txtActivityDate1" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="txtActivityDate1_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtActivityDate1" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtActivityDate1" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <label class="label_auto">
                            to</label>
                        <asp:TextBox ID="txtActivityDate2" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="txtActivityDate2_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="txtActivityDate2" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtActivityDate2" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Aplikasi
                        </label>
                        <asp:TextBox runat="server" ID="txtDateEntry" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="txtDateEntryApplication_CalendarExtender" runat="server"
                            Enabled="true" TargetControlID="txtDateEntry" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <%--<asp:RequiredFieldValidator runat="server" ID="rf" ErrorMessage="*" ControlToValidate="txtDateEntry"
                            CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Status</label>
                        <asp:DropDownList ID="cboStatus" runat="server" Width="100px">
                            <asp:ListItem Value="CAN">CANCEL</asp:ListItem>
                            <asp:ListItem Value="RJC">REJECT</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue" />
                    <asp:Button ID="ButtonReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
