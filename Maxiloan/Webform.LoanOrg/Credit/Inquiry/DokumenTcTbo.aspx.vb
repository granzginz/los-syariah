﻿Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController



Public Class DokumenTcTbo
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As UcBranch
    Protected WithEvents GridNavigator As ucGridNav
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private salesController As New SalesWithConditionController
    Private oCustomClass As New Parameter.SalesWithCondition
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not IsPostBack Then
            If Request.QueryString("message") <> "" Then
                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
            Me.FormID = "AGRTBOINQ"
            oSearchBy.ListData = "Name, Nama Konsumen-ApplicationID, No Aplikasi-Agreementno, No Kontrak-description, Nama Asset-Licenseplate, No Polisi"
            oSearchBy.BindData()

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If
    End Sub
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub


    Sub DoBind(Optional isFrNav As Boolean = False)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
       
        Dim wherestr As String = ""
        If oSearchBy.Text.Trim <> "" Then
            wherestr = String.Format("{0},{1}", oSearchBy.ValueID, oSearchBy.Text.Trim)
        End If
        oCustomClass = salesController.ListTboInquiry(GetConnectionString(), currentPage, pageSize, oBranch.BranchID, wherestr)


        DtUserList = oCustomClass.ListSales
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        DtgAsset.DataSource = DvUserList

        Try
            DtgAsset.DataBind()
        Catch
            DtgAsset.CurrentPageIndex = 0
            DtgAsset.DataBind()
        End Try
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If

        pnlList.Visible = True
        pnlDatagrid.Visible = True


    End Sub
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        If SessionInvalid() Then
            Exit Sub
        End If
        pnlDatagrid.Visible = True
        DtgAsset.Visible = True
        pnlList.Visible = True
        DoBind()
    End Sub
    Private Sub DtgAsset_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgAsset.ItemDataBound
        If SessionInvalid() Then
            Exit Sub
        End If


        If e.Item.ItemIndex >= 0 Then


            '*** Customer Link  
            CType(e.Item.FindControl("hyCustomerName"), HyperLink).NavigateUrl = "javascript:OpenCustomer('" & "AssetDocument" & "', '" &
                Server.UrlEncode(CType(e.Item.FindControl("lblCustomerId"), Label).Text.Trim) & "')"

            '*** Agreement No link  
            CType(e.Item.FindControl("hyAgreementNo"), HyperLink).NavigateUrl = "javascript:OpenAgreementNo('" & "AssetDocument" & "', '" &
                Server.UrlEncode(CType(e.Item.FindControl("hyApplicationId"), HyperLink).Text.Trim) & "')"

            '*** ApplicationId link
            Dim hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AssetDocument" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"
        End If
    End Sub


    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", "DESC"))
        DoBind()
    End Sub
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("DokumenTcTbo.aspx")
    End Sub
End Class