﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class ActivityLog
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New DataUserControlController
    Private oCustomClass As New Parameter.ActivityLog
    Private oController As New ActivityLogController

    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1   
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ActivityLog"
            If checkform(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtActivitydate1.Text = Now.ToString("dd/MM/yyyy")
                txtActivitydate2.Text = Now.ToString("dd/MM/yyyy")
                FillCbo()
                InitialDefaultPanel()
            End If
        End If
    End Sub
#End Region

#Region "FillCbo"
    Sub FillCbo()
        Dim dtbranch As New DataTable
        dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
        With cboParent
            .DataTextField = "Name"
            .DataValueField = "ID"
            .DataSource = dtbranch
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
        'Response.Write(GenerateScript(dtChild))
    End Sub

    Sub FillCboChild()
        Dim dtChild As New DataTable
        With oCustomClass
            .strConnection = getconnectionstring()
            .WhereCond = "BranchID='" & cboParent.SelectedItem.Value & "'"
        End With
        oCustomClass = oController.ListActivityUser(oCustomClass)
        dtChild = oCustomClass.ListActivity
        With cboChild
            .DataTextField = "ActivityUser"
            .DataValueField = "ActivityUser"
            .DataSource = dtChild
            .DataBind()
            .Items.Insert(0, "ALL")
            .Items(0).Value = "ALL"
        End With
    End Sub
#End Region

#Region "BranchIDChange"
    Protected Function BranchIDChange() As String
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function
#End Region

#Region "GenerateScript"
    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("ActivityUser")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ActivityUser")), "null", DataRow(i)("ActivityUser"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("ActivityUser")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ActivityUser")), "null", DataRow(i)("ActivityUser"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"
        Return strScript
    End Function
#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
    End Sub
#End Region

#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Navigation"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Protected Sub btnPageNumb_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtpage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtpage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtpage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                'DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Buttonsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Srch", "MAXILOAN") Then
            Me.SearchBy = "ActivityLog.BranchID='" & cboParent.SelectedItem.Value.Trim & "'"
            If hdnChildValue.Value.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and ActivityLog.ActivityUser='" & hdnChildValue.Value.Trim & "'"
            End If
            If txtActivitydate1.Text.Trim <> "" And txtActivitydate2.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " and (ActivityLog.ActivityDate >= convert(datetime, left(convert(varchar(20), '" & ConvertDate2(txtActivitydate1.Text).ToString("MM/dd/yyyy") & "'),11),101))"
                Me.SearchBy = Me.SearchBy & " and ActivityLog.ActivityDate <= dateadd(d, 1, convert(datetime, left(convert(varchar(20), '" & ConvertDate2(txtActivitydate2.Text).ToString("MM/dd/yyyy") & "'),11),101))"
            End If
            If cboActyivityType.SelectedItem.Value.Trim <> "All" Then
                Me.SearchBy = Me.SearchBy & " and ActivityLog.ActivityType='" & cboActyivityType.SelectedItem.Value.Trim & "'"
            End If

            If cboChild.SelectedItem.Value.Trim <> "ALL" Then
                Me.SearchBy = Me.SearchBy & " and ActivityLog.ActivityUser='" & cboChild.SelectedItem.Value.Trim & "'"
            End If

            If txtSearchBy.Text.Trim <> "" Then
                If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
                Else
                    Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
                End If
            End If
            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListActivity(oCustomClass)

        DtUserList = oCustomClass.ListActivity
        DvUserList = DtUserList.DefaultView
        recordCount = oCustomClass.TotalRecord
        DvUserList.Sort = Me.SortBy
        dtgActivity.DataSource = DvUserList

        Try
            dtgActivity.DataBind()
        Catch
            dtgActivity.CurrentPageIndex = 0
            dtgActivity.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub
#End Region

#Region "Reset"
    Private Sub ImbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click

        txtActivitydate1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtActivitydate2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        'cboSearchBy.ClearSelection()
        'txtSearchBy.Text = ""
        'cboActyivityType.ClearSelection()
        'cboChild.ClearSelection()
        'InitialDefaultPanel()
        'FillCbo()
        Response.Redirect("ActivityLog.aspx")
    End Sub
#End Region

#Region "cboParent_SelectedIndexChanged"
    Private Sub cboParent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboParent.SelectedIndexChanged
        FillCboChild()
    End Sub
#End Region

    Private Sub dtgActivity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgActivity.ItemDataBound
        Dim lnkApp As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lnkApp = CType(e.Item.FindControl("lblApplicationID"), HyperLink)
            lnkApp.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & lnkApp.Text & "')"

            Dim lblCustId As Label
            Dim lnkCust As HyperLink
            lblCustId = CType(e.Item.FindControl("lblCustomerID"), Label)
            lnkCust = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & lblCustId.Text & "' )"

            'lnkCust.Attributes.Add("OnClick", "return OpenCust('" & e.Item.Cells(4).Text & "','AccAcq');")
            Dim lnkAgr As HyperLink
            lnkAgr = CType(e.Item.FindControl("lblAgreementNo"), HyperLink)
            lnkAgr.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & lnkApp.Text.Trim & "')"

            'lnkPO.Attributes.Add("OnClick", "return OpenWinProductOfferingBranchView('" & e.Item.Cells(6).Text & "','" & e.Item.Cells(7).Text & "','" & e.Item.Cells(8).Text & "','AccAcq');")
        End If
    End Sub
End Class