﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic
Imports System.Data.SqlClient

Public Class HasilSurvey_002Character
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucHasilSurveyTab1 As ucHasilSurveyTab
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyTabPhone
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat

    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController
    Private m_controller As New AssetDataController
    Private m_Employee As New EmployeeController
    Private time As String

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property

    Private Property CSResult_Temp() As String
        Get
            Return CType(ViewState("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CSResult_Temp") = Value
        End Set
    End Property
    Private Property TotalDebet() As Decimal
        Get
            Return (CType(ViewState("TotalDebet"), Decimal))
        End Get
        Set(ByVal TotalDebet As Decimal)
            ViewState("TotalDebet") = TotalDebet
        End Set
    End Property
    Private Property TotalKredit() As Decimal
        Get
            Return (CType(ViewState("TotalKredit"), Decimal))
        End Get
        Set(ByVal TotalKredit As Decimal)
            ViewState("TotalKredit") = TotalKredit
        End Set
    End Property
    Private Property myDataTable() As DataTable
        Get
            Return CType(ViewState("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("myDataTable") = Value
        End Set
    End Property
    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Private Property DateEntryCapacity() As Date
        Get
            Return CType(ViewState("DateEntryCapacity"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryCapacity") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            Me.CustomerID = Request("id")
            'Me.ProspectAppID = "-"
            'Modify by Wira 20171013
            'Ambil data hasil survey dari mobile jika add ke agreement survey
            Me.BranchID = Request("branchID")
            Me.PageAddEdit = Request("page")
            Me.ProspectAppID = Request("prospectappid")
            InitialPageLoad()
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------
            If Me.ProspectAppID <> Nothing And Me.ProspectAppID <> "-" And Request("prospectappid") <> "" And Me.PageAddEdit = "Add" Then
                getHasilMobileSurvey()
            Else
                getHasilSurvey()
            End If

            'GetCookies()
            'InitialPageLoad()
            'fillFormTesting()
            'getHasilSurvey()            
        End If
    End Sub

    'Sub GetCookies()
    '    Dim cookie As HttpCookie = Request.Cookies("HASIL_SURVEY")

    '    Me.ApplicationID = cookie.Values("ApplicationID")
    '    Me.CustomerID = cookie.Values("CustomerID")
    '    Me.BranchID = cookie.Values("BranchID")
    '    Me.SupplierID = cookie.Values("SupplierID")
    'End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        Me.ApplicationID = Request("appid")

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
            End If
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("Character")
        ucHasilSurveyTab1.setLink()


        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        'FillCbo(cboPihakYangTurutSurvey, "TblEmployeePosition")
        FillCombo()
        FillCbo(cboPEducation, "dbo.TblEducation")
        'FillCboEmp("BranchEmployee", cboHasilCekLkgAspekSosial, "")
        'FillCboEmp("BranchEmployee", cboPEducation, "")
        'FillCboEmp("BranchEmployee", cboHubSalesdgnPemohon, "")

    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            cboPihakYangTurutSurvey.SelectedIndex = cboPihakYangTurutSurvey.Items.IndexOf(cboPihakYangTurutSurvey.Items.FindByValue(oPar.PihakYangTurutSurvey))
            txtAlasanPihakYangTurutSurvey.Text = .AlasanPihakYangTurutSurvey
            txtAlasanLokasiYangDisurvey.Text = .AlasanLokasiSurvey
            rboLokasiyangDisurvey.SelectedIndex = rboLokasiyangDisurvey.Items.IndexOf(rboLokasiyangDisurvey.Items.FindByValue(oPar.LokasiSurvey))
            rboSurveySesuaiKTP.SelectedIndex = rboSurveySesuaiKTP.Items.IndexOf(rboSurveySesuaiKTP.Items.FindByValue(oPar.LokasiSurveySesuaiKTP))
            txtAlasanSurveySesuaiKTP.Text = .AlasanLokasiSurveySesuaiKTP
            txtWaktuSurvey1.Text = IIf(.WaktuSurvey1.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .WaktuSurvey1.ToString("dd/MM/yyyy"))
            txtWaktuSurvey2.Text = IIf(.WaktuSurvey2.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .WaktuSurvey2.ToString("dd/MM/yyyy"))
            cboHasilCekLkgAspekSosial.SelectedIndex = cboHasilCekLkgAspekSosial.Items.IndexOf(cboHasilCekLkgAspekSosial.Items.FindByValue(oPar.HasilCekLkgAspekSosial))
            cboIntegritas.SelectedIndex = cboIntegritas.Items.IndexOf(cboIntegritas.Items.FindByValue(oPar.Integritas))
            rboPengalamanKredit.SelectedIndex = rboPengalamanKredit.Items.IndexOf(rboPengalamanKredit.Items.FindByValue(oPar.PengalamanKredit))
            txtPembayaran.Text = .Pembayaran
            rboBuktiBayar.SelectedIndex = rboBuktiBayar.Items.IndexOf(rboBuktiBayar.Items.FindByValue(oPar.BuktiBayar))
            txtPekerjaanSekarang.Text = .PekerjaanSekarang
            txtLamaPekerjaanSekarang.Text = .LamanyaPekerjaanSekarang
            txtPekerjaanSebelumnya.Text = .PekerjaanSebelumnya
            txtLamaPekerjaanSebelumnya.Text = .LamanyaPekerjaanSebelumnya
            cboPEducation.SelectedIndex = cboPEducation.Items.IndexOf(cboPEducation.Items.FindByValue(oPar.Pendidikan))
            cboHubBankLeasing.SelectedIndex = cboHubBankLeasing.Items.IndexOf(cboHubBankLeasing.Items.FindByValue(oPar.HubBankLeasing))
            txtNamaYangMenemaniSurvey1.Text = .NamaYangMenemaniSurvey1
            txtHubungan1.Text = .Hubungan1
            txtNamaYangMenemaniSurvey2.Text = .NamaYangMenemaniSurvey2
            txtHubungan2.Text = .Hubungan2
            cboHubSalesdgnPemohon.SelectedIndex = cboHubSalesdgnPemohon.Items.IndexOf(cboHubSalesdgnPemohon.Items.FindByValue(oPar.HubunganSalesDgnPemohon))
            txtKesimpulan.Text = .AnalisaCharacter
        End With
        BindgridCekLingkungan()
    End Sub

    Sub BindgridCekLingkungan()
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        'Dim adapter As New SqlDataAdapter
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spHasilSurvey_002CharacterCLView"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim

            objReader = objCommand.ExecuteReader
            'adapter.SelectCommand = objCommand
            'adapter.Fill(dtGrid)

            dtg.DataSource = objReader
            dtg.DataBind()
            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click        
        Response.Redirect("../CreditProcess/HasilSurvey_002.aspx?id=" & Me.CustomerID & "&appid=" & Me.ApplicationID & "&page=Edit")
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Private Sub FillCombo()
        Dim strCombo As String
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = getConnectionString()

        '--------------------------------
        ' Combo pihak Yang Turut Survey
        '--------------------------------
        strCombo = "Position"

        dtCombo = m_Employee.getCombo(strConn, strCombo, Me.BranchID)

        cboPihakYangTurutSurvey.DataTextField = "Name"
        cboPihakYangTurutSurvey.DataValueField = "ID"
        cboPihakYangTurutSurvey.DataSource = dtCombo
        cboPihakYangTurutSurvey.DataBind()

        cboPihakYangTurutSurvey.Items.Insert(0, "Select One")
        cboPihakYangTurutSurvey.Items(0).Value = "0"

    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If ConvertDate2(txtWaktuSurvey1.Text) > ConvertDate2(txtWaktuSurvey2.Text) Then
            ShowMessage(lblMessage, "Tanggal Awal Waktu Survey Tidak Boleh Lebih Besar Tanggal Akhir", True)
            Exit Sub
        End If

        Dim oPar As New Parameter.HasilSurvey
        Dim intLoop As Integer
        Dim CLNama, CLDomisili, CLLamaTinggal, CLLamaUsaha, CLNumber As String

        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .CustomerIDSurvey = Me.CustomerID
                .PihakYangTurutSurvey = cboPihakYangTurutSurvey.SelectedValue
                .AlasanPihakYangTurutSurvey = txtAlasanPihakYangTurutSurvey.Text
                .LokasiSurvey = rboLokasiyangDisurvey.SelectedValue
                .AlasanLokasiSurvey = txtAlasanLokasiYangDisurvey.Text
                .LokasiSurveySesuaiKTP = rboSurveySesuaiKTP.SelectedValue
                .AlasanLokasiSurveySesuaiKTP = txtAlasanSurveySesuaiKTP.Text
                .WaktuSurvey1 = Date.ParseExact(txtWaktuSurvey1.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .WaktuSurvey2 = Date.ParseExact(txtWaktuSurvey2.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .HasilCekLkgAspekSosial = cboHasilCekLkgAspekSosial.SelectedValue
                .Integritas = cboIntegritas.SelectedValue
                .PengalamanKredit = rboPengalamanKredit.SelectedValue
                .Pembayaran = txtPembayaran.Text
                .BuktiBayar = rboBuktiBayar.SelectedValue
                .PekerjaanSekarang = txtPekerjaanSekarang.Text
                .LamanyaPekerjaanSekarang = txtLamaPekerjaanSekarang.Text
                .PekerjaanSebelumnya = txtPekerjaanSebelumnya.Text
                .LamanyaPekerjaanSebelumnya = txtLamaPekerjaanSebelumnya.Text
                .Pendidikan = cboPEducation.SelectedValue
                .NamaYangMenemaniSurvey1 = txtNamaYangMenemaniSurvey1.Text
                .Hubungan1 = txtHubungan1.Text
                .NamaYangMenemaniSurvey2 = txtNamaYangMenemaniSurvey2.Text
                .Hubungan2 = txtHubungan2.Text
                .HubunganSalesDgnPemohon = cboHubSalesdgnPemohon.SelectedValue
                .AnalisaCharacter = txtKesimpulan.Text
            End With

            Dim numSelectedRows = dtg.Items.Count - 1

            For intLoop = 0 To numSelectedRows
                CLNumber = CType(dtg.Items(intLoop).Cells(0).FindControl("lblCLNumber"), Label).Text
                If CLNumber = 1 Then
                    oPar.ApplicationID = Me.ApplicationID
                    CLNama = CType(dtg.Items(intLoop).Cells(1).FindControl("txtCLNama"), TextBox).Text
                    CLDomisili = CType(dtg.Items(intLoop).Cells(2).FindControl("cboCLDomisili"), DropDownList).SelectedValue
                    CLLamaTinggal = CType(dtg.Items(intLoop).Cells(3).FindControl("txtCLLamaTinggal"), TextBox).Text
                    CLLamaUsaha = CType(dtg.Items(intLoop).Cells(4).FindControl("txtCLLamaUsaha"), TextBox).Text
                    oPar.CLNama1 = CLNama
                    oPar.CLDomisili1 = CLDomisili
                    oPar.CLLamaTinggal1 = CLLamaTinggal
                    oPar.CLLamaUsaha1 = CLLamaUsaha
                    oPar.CLStatus = CLNumber

                ElseIf CLNumber = 2 Then
                    oPar.ApplicationID = Me.ApplicationID
                    CLNama = CType(dtg.Items(intLoop).Cells(1).FindControl("txtCLNama"), TextBox).Text
                    CLDomisili = CType(dtg.Items(intLoop).Cells(2).FindControl("cboCLDomisili"), DropDownList).SelectedValue
                    CLLamaTinggal = CType(dtg.Items(intLoop).Cells(3).FindControl("txtCLLamaTinggal"), TextBox).Text
                    CLLamaUsaha = CType(dtg.Items(intLoop).Cells(4).FindControl("txtCLLamaUsaha"), TextBox).Text
                    oPar.CLNama1 = CLNama
                    oPar.CLDomisili1 = CLDomisili
                    oPar.CLLamaTinggal1 = CLLamaTinggal
                    oPar.CLLamaUsaha1 = CLLamaUsaha
                    oPar.CLStatus = CLNumber

                ElseIf CLNumber = 3 Then
                    oPar.ApplicationID = Me.ApplicationID
                    CLNama = CType(dtg.Items(intLoop).Cells(1).FindControl("txtCLNama"), TextBox).Text
                    CLDomisili = CType(dtg.Items(intLoop).Cells(2).FindControl("cboCLDomisili"), DropDownList).SelectedValue
                    CLLamaTinggal = CType(dtg.Items(intLoop).Cells(3).FindControl("txtCLLamaTinggal"), TextBox).Text
                    CLLamaUsaha = CType(dtg.Items(intLoop).Cells(4).FindControl("txtCLLamaUsaha"), TextBox).Text
                    oPar.CLNama1 = CLNama
                    oPar.CLDomisili1 = CLDomisili
                    oPar.CLLamaTinggal1 = CLLamaTinggal
                    oPar.CLLamaUsaha1 = CLLamaUsaha
                    oPar.CLStatus = CLNumber
                End If
                oController.HasilSurvey002CharacterCLSave(oPar)
            Next

            oController.HasilSurvey002CharacterSave(oPar)

            Dim errMsg As String

            ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
            ucHasilSurveyTab1.selectedTab("Character")
            ucHasilSurveyTab1.setLink()
            InitialPageLoad()
            getHasilSurvey()

            If errMsg = String.Empty Then
                ShowMessage(lblMessage, "Data saved!", False)
            End If
            'Response.Redirect("HasilSurvey_002Capacity.aspx?appid=" + Me.ApplicationID + "&page=" + Request("page") + "&id=" + Request("id") + "&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
            CekTabAplikasiSurvey()
            If Me.ProspectAppID <> "-" And Me.DateEntryCapacity = "1/1/1900" Then
                Response.Redirect("HasilSurvey_002Capacity.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Add&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim & "&branchID=" & Me.BranchID.Trim & "&ActivityDateStart=" & Me.ActivityDateStart)
            Else
                Response.Redirect("HasilSurvey_002Capacity.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Sub getHasilMobileSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilMobileSurvey(oPar)

        With oPar
            Me.ProspectAppID = .ProspectAppID.ToString
            cboPihakYangTurutSurvey.SelectedIndex = cboPihakYangTurutSurvey.Items.IndexOf(cboPihakYangTurutSurvey.Items.FindByValue(oPar.PihakYangTurutSurvey))
            txtAlasanPihakYangTurutSurvey.Text = .AlasanPihakYangTurutSurvey
            txtAlasanLokasiYangDisurvey.Text = .AlasanLokasiSurvey
            rboLokasiyangDisurvey.SelectedIndex = rboLokasiyangDisurvey.Items.IndexOf(rboLokasiyangDisurvey.Items.FindByValue(oPar.LokasiSurvey))
            'rboSurveySesuaiKTP.SelectedIndex = rboSurveySesuaiKTP.Items.IndexOf(rboSurveySesuaiKTP.Items.FindByValue(oPar.LokasiSurveySesuaiKTP))
            rboSurveySesuaiKTP.SelectedIndex = rboSurveySesuaiKTP.Items.IndexOf(rboSurveySesuaiKTP.Items.FindByValue(oPar.LokasiSurvey))
            txtAlasanSurveySesuaiKTP.Text = .AlasanLokasiSurveySesuaiKTP
            txtWaktuSurvey1.Text = IIf(.WaktuSurvey1.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .WaktuSurvey1.ToString("dd/MM/yyyy"))
            txtWaktuSurvey2.Text = IIf(.WaktuSurvey2.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .WaktuSurvey2.ToString("dd/MM/yyyy"))
            cboHasilCekLkgAspekSosial.SelectedIndex = cboHasilCekLkgAspekSosial.Items.IndexOf(cboHasilCekLkgAspekSosial.Items.FindByValue(oPar.HasilCekLkgAspekSosial))
            cboIntegritas.SelectedIndex = cboIntegritas.Items.IndexOf(cboIntegritas.Items.FindByValue(oPar.Integritas))
            rboPengalamanKredit.SelectedIndex = rboPengalamanKredit.Items.IndexOf(rboPengalamanKredit.Items.FindByValue(oPar.PengalamanKredit))
            txtPembayaran.Text = .Pembayaran
            rboBuktiBayar.SelectedIndex = rboBuktiBayar.Items.IndexOf(rboBuktiBayar.Items.FindByValue(oPar.BuktiBayar))
            txtPekerjaanSekarang.Text = .PekerjaanSekarang
            txtLamaPekerjaanSekarang.Text = .LamanyaPekerjaanSekarang
            txtPekerjaanSebelumnya.Text = .PekerjaanSebelumnya
            txtLamaPekerjaanSebelumnya.Text = .LamanyaPekerjaanSebelumnya
            cboPEducation.SelectedIndex = cboPEducation.Items.IndexOf(cboPEducation.Items.FindByValue(oPar.Pendidikan))
            cboHubBankLeasing.SelectedIndex = cboHubBankLeasing.Items.IndexOf(cboHubBankLeasing.Items.FindByValue(oPar.HubBankLeasing))
            txtNamaYangMenemaniSurvey1.Text = .NamaYangMenemaniSurvey1
            txtHubungan1.Text = .Hubungan1
            txtNamaYangMenemaniSurvey2.Text = .NamaYangMenemaniSurvey2
            txtHubungan2.Text = .Hubungan2
            cboHubSalesdgnPemohon.SelectedIndex = cboHubSalesdgnPemohon.Items.IndexOf(cboHubSalesdgnPemohon.Items.FindByValue(oPar.HubunganSalesDgnPemohon))
            txtKesimpulan.Text = .AnalisaCharacter
        End With
        BindgridCekLingkunganbyMobApps()
    End Sub

    Sub BindgridCekLingkunganbyMobApps()
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        'Dim adapter As New SqlDataAdapter
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spHasilSurvey_002CharacterCLMobAppsView"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ProspectAppID", SqlDbType.VarChar, 20).Value = Me.ProspectAppID.Trim

            objReader = objCommand.ExecuteReader
            'adapter.SelectCommand = objCommand
            'adapter.Fill(dtGrid)

            dtg.DataSource = objReader
            dtg.DataBind()
            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryCapacity = .DateEntryCapacity
        End With
    End Sub
End Class