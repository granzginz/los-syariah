﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class Invoice
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtInvInvoiceAmount As ucNumberFormat
    Protected WithEvents txtAPDueDate As ucDateCE
    Protected WithEvents txtInvoiceDate As ucDateCE
    Private x_controller As New DataUserControlController
#Region "Constanta"
    Private m_controller As New InvoiceController
    Dim objrow As DataRow
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim intLoop As Integer
    Dim Status As Boolean = True
#End Region
#Region "Property"
    Private Property ContactPersonName() As String
        Get
            Return CType(ViewState("ContactPersonName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ContactPersonName") = Value
        End Set
    End Property

    Private Property SupplierAddress() As String
        Get
            Return CType(ViewState("SupplierAddress"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierAddress") = Value
        End Set
    End Property


    Private Property SupplierPhone() As String
        Get
            Return CType(ViewState("SupplierPhone"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierPhone") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Private Property SupplierName() As String
        Get
            Return CType(ViewState("SupplierName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierName") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhere") = Value
        End Set
    End Property
#End Region
#Region "LinkTo"
    Function LinkToApplication(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenApplicationId('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function

    Function LinkToAgreement(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToSupplier(ByVal SupplierID As String, ByVal style As String) As String
        Return "javascript:OpenWinSupplier('" & style & "' , '" & SupplierID.Trim & "')"
    End Function

#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "Invoice"
            txtAPDueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                cboBranch()
                Me.SearchBy = ""
                Me.SortBy = ""
                txtGoPage.Text = "1"
                Me.Sort = "SupplierID ASC"
                Me.CmdWhere = ""


                Me.BranchID = oBranch.SelectedValue.ToString.Trim

                ''If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) = 0 And 
                If Me.IsHoBranch = False Then

                    BindGridEntity(Me.CmdWhere)
                Else
                    Response.Redirect("../../../error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub
#End Region
    Sub cboBranch()
        With oBranch
            If Me.IsHoBranch Then
                .DataSource = x_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "ALL"
                .Enabled = True
            Else
                .DataSource = x_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Enabled = False
            End If

        End With
    End Sub
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlInvoice.Visible = False
        lblMessage.Text = ""
        lblMessage.Visible = False
        txtInvoiceDate.Attributes.Add("readonly", "true")

        With txtInvInvoiceAmount
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = False
        End With

        txtAPDueDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtAPDueDate.IsRequired = True
        txtInvoiceDate.IsRequired = True
    End Sub
#End Region
#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oInvoice As New Parameter.Invoice

        With oInvoice
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .BranchId = oBranch.SelectedValue.ToString.Trim
            .strConnection = GetConnectionString()
        End With

        oInvoice = m_controller.InvoicePaging(oInvoice)

        If Not oInvoice Is Nothing Then
            dtEntity = oInvoice.ListData
            recordCount = oInvoice.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        dtgPaging.DataBind()
        PagingFooter()

        'If dtgPaging.Items.Count = 0 Then
        '    btnSave.Enabled = False
        'End If

    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        Me.CmdWhere = ""
        txtSearch.Text = ""
        BindGridEntity(Me.CmdWhere)
        txtGoPage.Text = "1"
    End Sub
#End Region
#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = "a." & cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "dtgPaging_ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Invoice" Then
            If CheckFeature(Me.Loginid, Me.FormID, "INV", Me.AppId) Then


                pnlList.Visible = False
                pnlInvoice.Visible = True
                lblErrMsgInvoiceDate.Text = ""

                txtInvInvoiceNo.Text = ""
                txtInvInvoiceAmount.Text = ""
                txtInvoiceDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

                Dim dtEntity As DataTable

                Dim hypSupplierID As New Label
                hypSupplierID = CType(e.Item.FindControl("hypSupplierID"), Label)

                Dim lblSupplierName As New HyperLink
                lblSupplierName = CType(e.Item.FindControl("hySupplierName"), HyperLink)

                Dim lblBranchID As New Label
                lblBranchID = CType(e.Item.FindControl("lblBranchID"), Label)


                pnlInvoice.Visible = True
                lblInvSupplierName.Text = lblSupplierName.Text
                lblInvSupplierName.NavigateUrl = LinkToSupplier(hypSupplierID.Text.Trim, "AccAcq")
                Dim oInvoiceListAgreement As New Parameter.Invoice
                oInvoiceListAgreement.BranchId = lblBranchID.Text.ToString.Trim
                oInvoiceListAgreement.SupplierID = hypSupplierID.Text.Trim
                oInvoiceListAgreement.strConnection = GetConnectionString()
                Me.SupplierID = hypSupplierID.Text.Trim
                oInvoiceListAgreement = m_controller.GetInvoiceAgreementList(oInvoiceListAgreement)

                If Not oInvoiceListAgreement Is Nothing Then
                    dtEntity = oInvoiceListAgreement.ListData
                    recordCount = oInvoiceListAgreement.TotalRecords
                Else
                    recordCount = 0
                End If

                dtgListOfAgreement.DataSource = dtEntity.DefaultView
                dtgListOfAgreement.CurrentPageIndex = 0
                dtgListOfAgreement.DataBind()
                txtInvInvoiceAmount.Text = "0"
                txtInvoiceDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            End If
        End If
    End Sub
#End Region
#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hypSupplierID As New Label
            hypSupplierID = CType(e.Item.FindControl("hypSupplierID"), Label)
            Me.SupplierID = hypSupplierID.Text.Trim

            Dim hySupplierName As New HyperLink
            hySupplierName = CType(e.Item.FindControl("hySupplierName"), HyperLink)
            hySupplierName.NavigateUrl = LinkToSupplier(hypSupplierID.Text.Trim, "AccAcq")
        End If
    End Sub
#End Region
#Region "dtgListOfAgreement_ItemDataBound"
    Private Sub dtgListOfAgreement_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgListOfAgreement.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblAgreementNo As New Label
            Dim lblApplicationId As Label
            Dim lblCustomerID As New Label
            Dim hynAgreementNo As New HyperLink
            Dim hynCustomerName As New HyperLink
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationID"), Label)
            hynAgreementNo = CType(e.Item.FindControl("hynAgreementNo"), HyperLink)
            hynAgreementNo.NavigateUrl = LinkToAgreement(lblApplicationId.Text.Trim, "AccAcq")

            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            hynCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
        End If
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgListOfAgreement.ItemCommand
        If e.CommandName = "PILIH" Then
            Dim oInvoice_SuppTopDays As New Parameter.Invoice
            oInvoice_SuppTopDays.strConnection = GetConnectionString()

            lblMessage.Text = ""
            lblMessage.Visible = False

            If txtInvoiceDate.Text = "" Then
                pnlInvoice.Visible = True
                pnlList.Visible = False
                Exit Sub
            End If

            Dim SuppTopDays = m_controller.GetInvoiceSuppTopDays(oInvoice_SuppTopDays)
            Dim MaxBackDate = m_controller.GetInvoiceMaxBackDate(oInvoice_SuppTopDays)
            Dim Temp_MaxBackDate As Integer = MaxBackDate * (-1)
            If ((ConvertDate2(txtInvoiceDate.Text) < DateAdd(DateInterval.Day, Temp_MaxBackDate, Me.BusinessDate)) Or (ConvertDate2(txtInvoiceDate.Text) > Me.BusinessDate)) Then
                lblErrMsgInvoiceDate.Text = "Invoice Date is not allowed!"
                lblErrMsgInvoiceDate.Visible = True
                pnlList.Visible = False
                pnlInvoice.Visible = True
                Exit Sub
            End If

            Dim i = e.Item.ItemIndex
            Dim invSupp = New Parameter.InvoiceSupp
            invSupp.BranchID = CType(dtgListOfAgreement.Items(i).FindControl("lblCabangId"), Label).Text.Trim
            invSupp.SupplierID = Me.SupplierID
            invSupp.SupplierName = lblInvSupplierName.Text
            invSupp.InvoiceNo = txtInvInvoiceNo.Text.Trim
            invSupp.InvoiceDate = ConvertDate2(txtInvoiceDate.Text)
            invSupp.APDueDate = ConvertDate2(txtAPDueDate.Text)

            invSupp.InvAmountOriginal = CDec(txtInvInvoiceAmount.Text.Trim)

            invSupp.ApplicationID = CType(dtgListOfAgreement.Items(i).FindControl("lblApplicationID"), Label).Text.Trim
            invSupp.AgreementNo = CType(dtgListOfAgreement.Items(i).FindControl("hynAgreementNo"), HyperLink).Text.Trim()

            invSupp.CustomerID = CType(dtgListOfAgreement.Items(i).FindControl("lblCustomerID"), Label).Text.Trim()
            invSupp.CustomerName = CType(dtgListOfAgreement.Items(i).FindControl("hynCustomerName"), HyperLink).Text.Trim()

            invSupp.APAmount = CDec(CType(dtgListOfAgreement.Items(i).FindControl("lblAPAmount"), Label).Text.Trim)
            invSupp.APType = CType(dtgListOfAgreement.Items(i).FindControl("lblAPType"), Label).Text.Trim

            invSupp.IsGabungRefundSupplier = CBool(CType(dtgListOfAgreement.Items(i).FindControl("lblIsGabung"), Label).Text.Trim)
            invSupp.Refound = CDec(CType(dtgListOfAgreement.Items(i).FindControl("lblRefund"), Label).Text.Trim)

            invSupp.NilPelunasan = CDbl(CType(dtgListOfAgreement.Items(i).FindControl("lblpelunasan"), Label).Text)
            invSupp.FirstInstallment = CDbl(CType(dtgListOfAgreement.Items(i).FindControl("lblFirstInstallment"), Label).Text)


            If invSupp.APAmount <> CDec(txtInvInvoiceAmount.Text.Trim) Then
                ShowMessage(lblMessage, "Nilai Invoice Salah!", True)
                pnlList.Visible = False
                pnlInvoice.Visible = True
                Exit Sub
            End If

            Session("INVOCICESUPP") = invSupp
            Response.Redirect("InvoiceDeductions.aspx")
        End If
    End Sub
#End Region
#Region "imgSave"



    Private Sub imbSave_Click_()


        'Dim oInvoice_MaxBackDate As New Parameter.Invoice
        'Dim oInvoice_SuppTopDays As New Parameter.Invoice

        'Dim SuppTopDays As Integer
        'oInvoice_SuppTopDays.strConnection = GetConnectionString()
        'SuppTopDays = m_controller.GetInvoiceSuppTopDays(oInvoice_SuppTopDays)

        'Dim MaxBackDate As Integer
        'oInvoice_MaxBackDate.strConnection = GetConnectionString()
        'MaxBackDate = m_controller.GetInvoiceMaxBackDate(oInvoice_MaxBackDate)

        'Dim Temp_MaxBackDate As Integer
        'Temp_MaxBackDate = MaxBackDate * (-1)

        'If txtInvoiceDate.Text = "" Then
        '    pnlInvoice.Visible = True
        '    pnlList.Visible = False
        '    Exit Sub
        'End If

        'If ((ConvertDate2(txtInvoiceDate.Text) < DateAdd(DateInterval.Day, Temp_MaxBackDate, Me.BusinessDate)) Or (ConvertDate2(txtInvoiceDate.Text) > Me.BusinessDate)) Then
        '    'ShowMessage(lblErrMsgInvoiceDate, "Invoice Date is not allowed!", True)
        '    lblErrMsgInvoiceDate.Text = "Invoice Date is not allowed!"
        '    lblErrMsgInvoiceDate.Visible = True
        '    pnlList.Visible = False
        '    pnlInvoice.Visible = True
        '    Exit Sub
        'End If

        'Dim i As Integer
        'Dim temp_APAmount As Decimal
        'temp_APAmount = 0
        'Dim chkAgreement As New CheckBox
        'Dim lblAPAmount As New Label
        'Dim hynAgreementNo As New HyperLink
        'Dim lblDeliveryOrderDate As New Label
        'Dim lblApplicationID As New Label
        'Dim hynCustomerName As New HyperLink
        'Dim lblApplicationStep As New Label
        'Dim lblAPType As Label
        'Dim counter_check As Integer = 0

        'For i = 0 To dtgListOfAgreement.Items.Count - 1
        '    chkAgreement = CType(dtgListOfAgreement.Items(i).FindControl("chkAgreement"), CheckBox)
        '    lblAPAmount = CType(dtgListOfAgreement.Items(i).FindControl("lblAPAmount"), Label)

        '    If chkAgreement.Checked = True Then
        '        counter_check += 1
        '        temp_APAmount = temp_APAmount + CDec(lblAPAmount.Text.Trim)


        '    End If

        'Next

        'If counter_check = 0 Then
        '    ShowMessage(lblMessage, "Pilih kontrak!", True)

        '    pnlList.Visible = False
        '    pnlInvoice.Visible = True
        '    Exit Sub
        'End If

        'If temp_APAmount <> CDec(txtInvInvoiceAmount.Text.Trim) Then
        '    ShowMessage(lblMessage, "Nilai Invoice Salah!", True)

        '    pnlList.Visible = False
        '    pnlInvoice.Visible = True
        '    Exit Sub
        'End If






        'Dim ResultSave As String
        'Dim oInvoice As New Parameter.Invoice

        'oInvoice.BranchId = Me.BranchID
        'oInvoice.SupplierID = Me.SupplierID
        'oInvoice.InvoiceNo = txtInvInvoiceNo.Text.Trim
        'oInvoice.InvoiceDate = ConvertDate2(txtInvoiceDate.Text)
        'oInvoice.InvoiceAmount = CDec(txtInvInvoiceAmount.Text.Trim)
        'oInvoice.APDueDate = ConvertDate2(txtAPDueDate.Text)
        'oInvoice.BusinessDate = Me.BusinessDate
        'oInvoice.strConnection = GetConnectionString()


        'Dim DueDate As Date
        'Dim Temp_DueDate As Date
        'Dim oData1 As New DataTable

        'oData1.Columns.Add("AplicationID", GetType(String))
        'oData1.Columns.Add("CustomerName", GetType(String))
        'oData1.Columns.Add("AgreementNo", GetType(String))
        'oData1.Columns.Add("APAmount", GetType(Decimal))
        'oData1.Columns.Add("DeliveryOrderDate", GetType(Date))
        'oData1.Columns.Add("ApplicationStep", GetType(String))
        'oData1.Columns.Add("APType", GetType(String))

        'For i = 0 To dtgListOfAgreement.Items.Count - 1
        '    chkAgreement = CType(dtgListOfAgreement.Items(i).FindControl("chkAgreement"), CheckBox)
        '    lblAPAmount = CType(dtgListOfAgreement.Items(i).FindControl("lblAPAmount"), Label)
        '    hynAgreementNo = CType(dtgListOfAgreement.Items(i).FindControl("hynAgreementNo"), HyperLink)
        '    lblDeliveryOrderDate = CType(dtgListOfAgreement.Items(i).FindControl("lblDeliveryOrderDate"), Label)
        '    lblApplicationID = CType(dtgListOfAgreement.Items(i).FindControl("lblApplicationID"), Label)
        '    hynCustomerName = CType(dtgListOfAgreement.Items(i).FindControl("hynCustomerName"), HyperLink)
        '    lblApplicationStep = CType(dtgListOfAgreement.Items(i).FindControl("lblApplicationStep"), Label)
        '    lblAPType = CType(dtgListOfAgreement.Items(i).FindControl("lblAPType"), Label)

        '    If chkAgreement.Checked = True Then
        '        objrow = oData1.NewRow
        '        objrow("AplicationID") = lblApplicationID.Text.Trim
        '        objrow("CustomerName") = hynCustomerName.Text
        '        objrow("AgreementNo") = hynAgreementNo.Text.Trim
        '        objrow("APAmount") = CDec(lblAPAmount.Text.Trim)
        '        Temp_DueDate = ConvertDate2(lblDeliveryOrderDate.Text.Trim)
        '        DueDate = DateAdd(DateInterval.Day, SuppTopDays, Temp_DueDate)
        '        If DueDate < Me.BusinessDate Then
        '            DueDate = Me.BusinessDate
        '            objrow("DeliveryOrderDate") = DueDate
        '        Else
        '            objrow("DeliveryOrderDate") = Temp_DueDate
        '        End If
        '        objrow("ApplicationStep") = lblApplicationStep.Text.Trim
        '        objrow("APType") = lblAPType.Text.Trim
        '        oData1.Rows.Add(objrow)

        '    End If
        'Next

        'Try
        '    oInvoice.ListData = oData1
        '    ResultSave = m_controller.InvoiceSave(oInvoice)
        '    BindGridEntity(Me.CmdWhere)
        '    pnlList.Visible = True
        '    pnlInvoice.Visible = False
        'Catch ex As Exception
        '    ShowMessage(lblMessage, ex.Message, True)

        '    pnlList.Visible = False
        '    pnlInvoice.Visible = True
        '    Exit Sub
        'End Try
    End Sub
#End Region

#Region "imbCancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlInvoice.Visible = False
        pnlList.Visible = True
    End Sub
#End Region

End Class