﻿
Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Public Class CreditAssesmentList
    Inherits Maxiloan.Webform.WebBased


    Private m_controller As New RCAController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Protected WithEvents GridNavigator As ucGridNav
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(viewstate("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property CustName() As String
        Get
            Return CType(viewstate("CustName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        FormID = "CRDASSNT"
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Page.IsPostBack Then
            If CheckForm(Me.Loginid, FormID, Me.AppId) Then
                lblMessage.Text = ""
                lblMessage.Visible = False
                'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then 
                'Modify by Ario 20180814 by SK
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And
                    (Me.sesBranchId = "014" Or Me.sesBranchId = "007" Or Me.sesBranchId = "006") Then
                    Response.Redirect("../../../error_notauthorized.aspx")
                Else
                    If Request("cond") <> "" Then
                        Me.CmdWhere = Request("cond")
                    Else
                        Me.CmdWhere = "ALL"
                    End If
                    Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                    Me.Sort = "ApplicationID ASC"
                    BindGrid()
                    Me.BranchID = Me.sesBranchId.Replace("'", "")
                    InitialPanel()

                    If Request("result") <> "" Then
                        ShowMessage(lblMessage, Request("result"), False)
                    End If


                End If
            End If
        End If
    End Sub


    Sub InitialPanel()
        pnlList.Visible = True
    End Sub


    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGrid(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Sub BindGrid(Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.RCA

        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetCreditAssesmentList(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        ' PagingFooter()
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        Me.Sort = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.Sort, "DESC") > 0, "", "DESC"))
        BindGrid()
    End Sub


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        BindGrid()
    End Sub
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.CmdWhere = "ALL"
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = String.Format("{0} like  '%{1}%' ", cboSearch.SelectedItem.Value, txtSearch.Text.Replace("%", ""))
        End If
        Me.CmdWhere2 = String.Format("BranchID = '{0}'", Replace(Me.sesBranchId, "'", ""))
        BindGrid()
    End Sub

    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim SchemeId As String
        Dim NTF As String

        If CheckFeature(Me.Loginid, FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        pnlReturn.Visible = False

        Select Case e.CommandName
            Case "ApplicationID"
            Case "Name"
            Case "CA"
            Case "AO"
            Case "REQ"
                Me.ApplicationID = CType(e.Item.Cells(1).FindControl("lnkApplicationID"), HyperLink).Text.Trim
                Me.CustName = CType(e.Item.Cells(2).FindControl("lnkName"), HyperLink).Text.Trim
                Me.CustomerID = CType(e.Item.FindControl("lblCustomerID"), Label).Text.Trim
                SchemeId = CType(e.Item.FindControl("lblSchemeID"), Label).Text.Trim
                NTF = CType(e.Item.FindControl("lblNTF"), Label).Text.Trim
                Response.Redirect(String.Format("CreditAssesment.aspx?appid={0}&Name={1}&CustomerID={2}&Scheme={3}&NTF={4}",
                                                 Me.ApplicationID, Me.CustName, Me.CustomerID, SchemeId, NTF))
        End Select
        If e.CommandName = "Return" Then
            pnlList.Visible = False
            pnlReturn.Visible = True

            Dim lblCustomerID As New Label
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Dim hypAgreementNo As New HyperLink
            hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            Dim lblApplicationID As New Label
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)

            hdnApplicationID.Value = lblApplicationID.Text.Trim
            ucViewApplication1.ApplicationID = lblApplicationID.Text.Trim
            ucViewApplication1.CustomerID = lblCustomerID.Text.Trim
            ucViewApplication1.bindData()
            ucViewApplication1.initControls("OnNewApplication")
            ucViewCustomerDetail1.CustomerID = lblCustomerID.Text.Trim
            ucViewCustomerDetail1.bindCustomerDetail()
        End If

    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound

        If e.Item.ItemIndex >= 0 Then
            Dim lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            Dim lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            Dim lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)
            Dim lnkCA = CType(e.Item.FindControl("lnkCA"), HyperLink)


            lnkApplicationID.NavigateUrl = String.Format("javascript:OpenWinApplication('{0}','AccAcq')", CType(e.Item.FindControl("lnkApplicationID"), HyperLink).Text.Trim)
            lnkName.NavigateUrl = String.Format("javascript:OpenWinCustomer('{0}','AccAcq')", CType(e.Item.FindControl("lblCustomerID"), Label).Text.Trim)
            lnkAO.NavigateUrl = String.Format("javascript:OpenWinEmployee('{0}','{1}','AccAcq')", Replace(Me.sesBranchId, "'", ""), CType(e.Item.FindControl("lblAOID"), Label).Text.Trim)
            lnkCA.NavigateUrl = String.Format("javascript:OpenWinEmployee('{0}','{1}','AccAcq')sss", Replace(Me.sesBranchId, "'", ""), CType(e.Item.FindControl("lblCAID"), Label).Text.Trim)
        End If
    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Dim ParDO As New Parameter._DO
        Dim m_DO As New DOController

        ParDO.strConnection = GetConnectionString()
        ParDO.BusinessDate = Me.BusinessDate
        ParDO.BranchId = Me.sesBranchId.Replace("'", "").Trim
        ParDO.ApplicationID = hdnApplicationID.Value
        ParDO.AlasanReturn = txtAlasanReturn.Text.Trim

        Try
            m_DO.ApplicationReturnUpdate(ParDO)

            BindGrid()
            pnlList.Visible = True
            pnlReturn.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Me.CmdWhere = ""
        BindGrid()
        pnlList.Visible = True
        pnlReturn.Visible = False
    End Sub
End Class