﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewApplicationAgriculture_003.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewApplicationAgriculture_003" %>

<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabAgriculture.ascx" TagName="ucApplicationViewTab"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../Webform.UserController/ucCompanyAddress.ascx" TagName="UcCompanyAdress"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Application Agriculture</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <link rel="stylesheet" type="text/css" href="../../../../include/ui-lightness/jquery-ui-1.10.3.custom.css"  />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
     
    </script>
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">
    <form id="form1" runat="server" class="tab_container_form">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
     <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucapplicationviewtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        VIEW APLIKASI</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA APLIKASI
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="">Kegiatan Usaha</label>  
                        <asp:Label ID="lblKegiatanUsaha" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboKegiatanUsaha" runat="server" AutoPostBack="false"  Width="300px" Visible="false" />
                    </div>
                    <div class="form_right">
                        <label>Lini Bisnis</label>
                        <asp:Label ID="lblLiniBisnis" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboLiniBisnis" runat="server"  Width="200px" Visible="false">
                            <asp:ListItem Value="RETAIL">RETAIL</asp:ListItem>
                            <asp:ListItem Value="FLEET">FLEET</asp:ListItem>
                            <asp:ListItem Value="AGRO">AGRO</asp:ListItem>
                            <asp:ListItem Value="NPCD">NPCD</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="">
                            Jenis Pembiayaan
                        </label>
                        <asp:Label ID="lblJenisPembiyaan" runat="server" ></asp:Label>
                        <asp:DropDownList runat="server" ID="cboJenisPembiyaan"  Width="300px" AutoPostBack="false" Visible="false"></asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>Tipe Loan</label>
                        <asp:Label ID="lblTipeLoan" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboTipeLoan" runat="server"  Width="200px" Visible="false">
                            <asp:ListItem Value="R">Regular</asp:ListItem>
                            <asp:ListItem Value="O">Over Pembiayaan</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label>Produk Jual</label>
                        <asp:Label ID="lblProductOffering" runat="server" ></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                         <label>
                            Skema Angsuran</label>
                         <asp:Label ID="lblInstScheme" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboInstScheme" runat="server" Visible="false">
                            <asp:ListItem Value="RF">Regular Fixed Installment Scheme</asp:ListItem>
                            <asp:ListItem Value="IR">Irregular Installment</asp:ListItem>
                            <asp:ListItem Value="ST">Step Up/Step Down</asp:ListItem>
                            <asp:ListItem Value="BP">Ballon Payment</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>
                                Jenis Margin</label>
                                <asp:Label ID="lblInterestType" runat="server" ></asp:Label>
                            <asp:DropDownList ID="cboInterestType" runat="server" Visible="false">
                                <asp:ListItem Value="FX">Fixed Rate</asp:ListItem>
                                <asp:ListItem Value="FL">Floating Rate</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                       <label class="label_general">
                                Tipe Step Up Step Down</label>
                            <asp:RadioButtonList ID="rdoSTTYpe" runat="server" RepeatDirection="Horizontal" Enabled="False"
                                CssClass="opt_single">
                                <asp:ListItem Value="NM" Selected="True">Basic</asp:ListItem>
                                <asp:ListItem Value="RL">Normal</asp:ListItem>
                                <asp:ListItem Value="LS">Leasing</asp:ListItem>
                            </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                            <label>
                                COP</label>
                            <asp:CheckBox runat="server" ID="chkCOP" Enabled="false" />
                        </div>
                </div>
            </div>
            <div class="form_box">
                    <div>
                        <div class="form_left">
                            
                        </div>
                        <div class="form_right">
                            <label>
                                Hak Opsi</label>
                            <asp:CheckBox runat="server" ID="chkHakOpsi" Enabled="false" />
                        </div>
                    </div>
             </div>

            <div class="form_box_title">
                <div class="form_left">
                    <h4>
                        DATA UMUM</h4>
                </div>
                <div class="form_right">
                    <h4>
                        BIAYA - BIAYA</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                        
                    <div class="form_left">
                        <label>
                            Cara Pembayaran
                        </label>
                        <asp:Label ID="lblWayPymt" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboWayPymt" runat="server" Visible="false">
                            <asp:ListItem Value="TF" Selected="True">Transfer</asp:ListItem>
                            <asp:ListItem Value="PD">PDC</asp:ListItem>
                            <asp:ListItem Value="CA">Cash</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Administrasi Nett</label>
                        <asp:Label ID="ucAdminFee" runat="server"  CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Fiducia Register</label>
                        <asp:CheckBox runat="server" ID="chkFiducia" Enabled="false" CssClass="checkbox_general" />
                    </div>
                        
                    <div class="form_right">
                        <label>
                            Biaya Fiducia
                        </label>
                        <asp:Label ID="ucFiduciaFee" runat="server" CssClass="numberAlign regular_text" ></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Sumber Aplikasi
                        </label>
                        <asp:Label ID="lblSourceApp" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboSourceApp" runat="server" Visible="false">
                            <asp:ListItem Value="D">Direct</asp:ListItem>
                            <asp:ListItem Value="I" Selected="True">Indirect</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Lainnya
                        </label>
                        <asp:Label ID="ucOtherFee" runat="server" CssClass="numberAlign regular_text" ></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <div style="display:none">
                        
                        </div>
                    </div>
                        
                    <div class="form_right">
                        <label>
                            Biaya Administrasi Gross</label>
                        <asp:Label runat="server" ID="lblAdminFeeGross" CssClass="numberAlign regular_text">0</asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        CATATAN APLIKASI</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:TextBox ID="txtAppNotes" runat="server" ReadOnly="true" TextMode="MultiLine" CssClass="multiline_textbox" Width="80%"></asp:TextBox>
                </div>
            </div>
               
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ALAMAT SURAT MENYURAT
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <uc1:UcCompanyAdress id="UCMailingAddress" runat="server"></uc1:UcCompanyAdress>
        </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Kota/Kabupaten DATI II</label>                            
                    <asp:Label runat="server" ID="txtKabupaten" ></asp:Label>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h5>
                        SYARAT & KONDISI</h5>
                </div>
            </div>  
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgTC" runat="server" EnableViewState="False" Width="100%" CssClass="grid_general"
                        CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False"
                        PageSize="3">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTCNo" runat="server" EnableViewState="False"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                <HeaderStyle HorizontalAlign="Center" Width="35%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="">
                                <HeaderStyle Width="80px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyupload" style="cursor:pointer" runat="server">Attached</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                <HeaderStyle HorizontalAlign="Center" Width="7%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Checked" HeaderText="PERIKSA">
                                <HeaderStyle HorizontalAlign="Center" Width="8%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Mandatory" HeaderText="MANDATORY">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PromiseDate" HeaderText="TGL JANJI" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Notes" HeaderText="CATATAN">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="" HeaderStyle-Width="0px" >
                                <ItemTemplate>
                                    <asp:TextBox style="display:none" runat="server" ID="MasterTCID" Text='<%# DataBinder.eval(Container, "DataItem.MasterTCID") %>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
            <div class="tab_container_form_space"><br />
            </div>
            <div class="tab_container_button">
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
