﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class AssetDataAgriculture_002
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabAgriculture
    Protected WithEvents ucOTR As ucNumberFormat
#End Region

#Region "Constanta"
    Dim style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim status As Boolean = True

    Private m_controller As New AssetDataController
    Private oApplication As New Parameter.Application
    Private m_ControllerApp As New ApplicationController
    Private oAssetMasterPriceController As New AssetMasterPriceController
    Private oApplicationController As New ApplicationController
    Private m_Doc As New DocReceiveController
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property AssetDesc() As String
        Get
            Return ViewState("AssetDesc").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AssetDesc") = Value
        End Set
    End Property
    Property AssetCode() As String
        Get
            Return ViewState("AssetCode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AssetCode") = Value
        End Set
    End Property
    Property DP() As Double
        Get
            Return CType(ViewState("DP"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("DP") = Value
        End Set
    End Property
    Property OTR() As Double
        Get
            Return CType(ViewState("OTR"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("OTR") = Value
        End Set
    End Property
    Property AOID() As String
        Get
            Return ViewState("AOID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AOID") = Value
        End Set
    End Property
    Property NU() As String
        Get
            Return ViewState("NU").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NU") = Value
        End Set
    End Property
    Property App() As String
        Get
            Return ViewState("App").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("App") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property Asset() As String
        Get
            Return ViewState("Asset").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Asset") = Value
        End Set
    End Property
    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property ProductID() As String
        Get
            Return ViewState("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Property SalesID() As String
        Get
            Return ViewState("SalesID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SalesID") = Value
        End Set
    End Property
    Property SupervisorID() As String
        Get
            Return ViewState("SupervisorID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupervisorID") = Value
        End Set
    End Property
    Property AdminID() As String
        Get
            Return ViewState("AdminID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AdminID") = Value
        End Set
    End Property
    Property CustomerType() As String
        Get
            Return ViewState("CustomerType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerType") = Value
        End Set
    End Property
    Property Origination() As String
        Get
            Return ViewState("Origination").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Origination") = Value
        End Set
    End Property
    Property DownPayment() As Integer
        Get
            Return CType(ViewState("DownPayment"), Integer)
        End Get
        Set(value As Integer)
            ViewState("DownPayment") = value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("appid")
            hdfApplicationID.Value = Me.ApplicationID
            InitObjects()

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()


                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If

                .initControls("OnNewApplication")

                Me.CustomerID = .CustomerID
                Me.CustName = .CustomerName
                Me.Asset = .AssetTypeID
                Me.ProductID = .ProductID
                Me.ProductOfferingID = .ProductOfferingID
                Me.SupplierID = .SupplierID
                Me.CustomerType = .CustomerType
                Me.Origination = .Origination

                If .DownPayment > 0 Then
                    Me.DownPayment = .DownPayment
                Else
                    txtDPPersen.Text = CStr(.DPPercentage)
                End If

            End With

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            hdfAssetTypeID.Value = Me.Asset

            FillCbo(cboInsuredBy, "tblInsuredBy")
            FillCbo(cboPaidBy, "tblPaidBy")
            Mode = Request("Page")

            If Request("Page") = "Edit" Then
                GetDefaultAssetData()
                BindAttributeEdit()
                BindAssetRegistration()
            Else

                BindAttribute()
                cboInsuredBy.SelectedValue = "CO"
                If ucViewApplication1.PaketProgram = "RGL" Then
                    cboPaidBy.SelectedValue = "CU"
                Else
                    cboPaidBy.SelectedValue = "OC"
                End If

                cboPaidBy.Visible = True
                cboInsuredBy.Attributes.Add("OnChange", "return fInsured();")

            End If

            GetSerial()
            GetFee()

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Asset")
            ucApplicationTab1.setLink()

            ucOTR.RangeValidatorEnable = True
            ucOTR.RangeValidatorMinimumValue = "1"



            If ucViewApplication1.isProceed = True Then
                btnNext.Visible = False
            End If

        End If

    End Sub
#End Region

#Region "Initial Objects"
    Private Sub InitObjects()
        lblMessage.Text = ""
        lblMessage.Visible = False

        ucOTR.AutoPostBack = True
        pnlNext.Visible = True
        pnlLast.Visible = False
        cboKondisiAsset.SelectedValue = "N"
        Me.NU = "N"
    End Sub

    Sub InitialLabelValid()
        Dim count As Integer
        lblVPaidBy.Visible = False
        count = CInt(IIf(dtgAssetDoc.Items.Count > dtgAttribute.Items.Count, dtgAssetDoc.Items.Count, dtgAttribute.Items.Count))
        For Me.intLoop = 0 To count - 1
            If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label).Visible = False
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label).Visible = False
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber2"), Label).Visible = False
            End If
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label).Visible = False
            End If
        Next
    End Sub

#End Region

#Region "FillCbo"
    Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = m_controller.GetCboEmp(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

#End Region

#Region "BindAsset, BindAttribute, GetAO, & GetSerial"
    Sub BindAsset(ByVal WhereCond As String)
        Dim oData As New DataTable
        Dim oAssetData As New Parameter.AssetData

        oAssetData.strConnection = GetConnectionString()
        oAssetData.AssetID = Me.Asset
        oAssetData.CustomerType = Me.CustomerType
        oAssetData.Origination = Me.Origination
        oAssetData.WhereCond = WhereCond
        oAssetData = m_controller.GetAssetDoc(oAssetData)
        oData = oAssetData.ListData

        dtgAssetDoc.DataSource = oData
        dtgAssetDoc.DataBind()
    End Sub

    Sub BindAttribute()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.AssetID = Me.Asset
        oAssetData = m_controller.GetAttribute(oAssetData)
        oData = oAssetData.ListData
        dtgAttribute.DataSource = oData
        dtgAttribute.DataBind()
    End Sub

    Sub GetSerial()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oData2 As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.AssetID = Me.Asset
        oAssetData = m_controller.GetSerial(oAssetData)
        oData = oAssetData.ListData

        lblSerial1.Text = oData.Rows(0).Item(0).ToString

        'oAssetData.strConnection = GetConnectionString()
        'oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
        'oAssetData.AppID = Me.ApplicationID
        'oAssetData = m_controller.GetUsedNew(oAssetData)
        'oData2 = oAssetData.ListData

        'Me.NU = oData2.Rows(0).Item(0).ToString

        RFVSerial1.Enabled = False
        'If Me.NU.ToUpper.Trim = "U" Then
        '    RFVSerial1.Enabled = True
        '    RFVSerial1.ErrorMessage = "Harap isi " & oData.Rows(0).Item(0).ToString & "!"
        'Else

        'End If
    End Sub

#End Region

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub

#Region "ItemDataBound"
    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        Dim chkFU As New CheckBox
        Dim ddlNotes As New DropDownList
        Dim txtNotes As New TextBox
        Dim lblNotes As New Label
        Dim lblIsNoRequired As New Label
        Dim txtNumber As New TextBox
        Dim lblVNumber2 As New Label
        Dim RFVNumber As New RequiredFieldValidator
        Dim ucTglDokumen As New ucDateCE


        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            chkFU = CType(e.Item.FindControl("chkFU"), CheckBox)
            ddlNotes = CType(e.Item.FindControl("ddlNotes"), DropDownList)
            txtNotes = CType(e.Item.FindControl("txtNotes"), TextBox)
            lblNotes = CType(e.Item.FindControl("lblNotes"), Label)
            lblIsNoRequired = CType(e.Item.FindControl("lblIsNoRequired"), Label)
            txtNumber = CType(e.Item.FindControl("txtNumber"), TextBox)
            lblVNumber2 = CType(e.Item.FindControl("lblVNumber2"), Label)
            RFVNumber = CType(e.Item.FindControl("RFVNumber"), RequiredFieldValidator)
            ucTglDokumen = CType(e.Item.FindControl("ucTglDokumen"), ucDateCE)

            ddlNotes.Attributes.Remove("style")
            txtNotes.Attributes.Remove("style")

            If chkFU.Checked Then
                ddlNotes.SelectedValue = lblNotes.Text.Trim
                ddlNotes.Attributes.Add("style", "display : inherit")
                txtNotes.Attributes.Add("style", "display : none")
            Else
                txtNotes.Text = lblNotes.Text.Trim
                ddlNotes.Attributes.Add("style", "display : none")
                txtNotes.Attributes.Add("style", "display : inherit")
            End If

            If CBool(lblIsNoRequired.Text) Then
                txtNumber.Visible = True
                RFVNumber.Visible = True
                ucTglDokumen.Visible = True
            Else
                txtNumber.Visible = False
                RFVNumber.Visible = False
                lblVNumber2.Visible = False
                ucTglDokumen.Visible = False
            End If


        End If
    End Sub
#End Region

#Region "Validator & save"
    Sub Validator(ByVal oData2 As DataTable, ByVal oData3 As DataTable)
        Dim ErrS1 As Boolean = False
        Dim ErrS2 As Boolean = False

        If cboPaidBy.SelectedValue = "Select One" And cboInsuredBy.SelectedValue <> "CU" Then
            lblVPaidBy.Visible = True
            status = False
        End If

        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        If (txtSerial1.Text.Trim <> "") Then
            oAssetData.strConnection = GetConnectionString()
            oAssetData.Serial1 = txtSerial1.Text
            oAssetData.Serial2 = ""
            oAssetData.AssetID = Me.Asset
            oAssetData.AppID = Me.ApplicationID
            oAssetData.ApplicationId = Me.ApplicationID
            oAssetData = m_controller.CheckSerial(oAssetData)
            oData = oAssetData.ListData

            If oData.Rows.Count <> 0 Then
                oData = oAssetData.ListData
                Dim intLoop As Integer
                For intLoop = 0 To oData.Rows.Count - 1
                    If txtSerial1.Text.Trim <> "" Then
                        If oData.Rows(intLoop).Item(0).ToString.Trim = txtSerial1.Text.Trim Then
                            ErrS1 = True
                        End If
                    End If
                Next
                If ErrS1 = True And ErrS2 = True Then
                    ShowMessage(lblMessage, "Nomor Chasis dan Nomor Mesin sudah ada!", True)
                    status = False
                Else
                    If ErrS1 = True Then
                        ShowMessage(lblMessage, "Nomor Chasis sudah ada!", True)
                        status = False
                    ElseIf ErrS2 = True Then
                        ShowMessage(lblMessage, "Nomor Mesin sudah ada!", True)
                        status = False
                    End If
                End If
            End If
        End If

        Dim AttributeId As String
        Dim txtAttribute As TextBox
        Dim lblVAttribute, lblVNumber, lblVNumber2 As Label
        Dim count As Integer
        Dim lblvChk As Label
        Dim chk As CheckBox
        Dim MandatoryNew, MandatoryUsed, isValueNeeded As TextBox
        Dim txtnumber As TextBox
        Dim value As String
        Dim ucTglDokumen As ucDateCE

        count = CInt(IIf(dtgAttribute.Items.Count > dtgAssetDoc.Items.Count, dtgAttribute.Items.Count, dtgAssetDoc.Items.Count))
        For intLoop = 0 To count - 1
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                AttributeId = dtgAttribute.Items(intLoop).Cells(2).Text.Trim
                txtAttribute = CType(dtgAttribute.Items(intLoop).FindControl("txtAttribute"), TextBox)
                lblVAttribute = CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label)

                If AttributeId = "LICPLATE" And txtAttribute.Text.Trim <> "" Then
                    oAssetData = New Parameter.AssetData
                    oData = New DataTable

                    If Request("Page") = "Edit" Then
                        oAssetData.strConnection = GetConnectionString()
                        oAssetData.Input = txtAttribute.Text
                        oAssetData.AssetID = Me.Asset
                        oAssetData.ApplicationId = Me.ApplicationID
                        oAssetData = m_controller.CheckAttribute(oAssetData)
                        oData = oAssetData.ListData
                    Else
                        oAssetData.strConnection = GetConnectionString()
                        oAssetData.Input = txtAttribute.Text
                        oAssetData.AssetID = Me.Asset
                        oAssetData.ApplicationId = ""
                        oAssetData = m_controller.CheckAttribute(oAssetData)
                        oData = oAssetData.ListData
                    End If

                    If oData.Rows.Count > 0 Then
                        lblVAttribute.Visible = True
                        If status <> False Then
                            status = False
                        End If
                    Else
                        lblVAttribute.Visible = False
                    End If
                Else
                    lblVAttribute.Visible = False
                End If

                If AttributeId = "COLOR" Then
                    lblVAttribute.Visible = False
                End If

                objrow = oData2.NewRow
                objrow("AttributeID") = AttributeId
                objrow("AttributeContent") = txtAttribute.Text.Trim
                oData2.Rows.Add(objrow)
            End If
            If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                chk = CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox)
                lblvChk = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label)
                lblVNumber = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label)
                lblVNumber2 = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber2"), Label)
                value = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                txtnumber = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox)
                ucTglDokumen = CType(dtgAssetDoc.Items(intLoop).FindControl("ucTglDokumen"), ucDateCE)
                isValueNeeded = CType(dtgAssetDoc.Items(intLoop).FindControl("isValueNeeded"), TextBox)
                MandatoryNew = CType(dtgAssetDoc.Items(intLoop).FindControl("MandatoryForNewAsset"), TextBox)
                MandatoryUsed = CType(dtgAssetDoc.Items(intLoop).FindControl("MandatoryForUsedAsset"), TextBox)
                If cboKondisiAsset.SelectedValue = "N" And CBool(MandatoryNew.Text) = True Then
                    If chk.Checked = False Then
                        If CBool(MandatoryNew.Text) = True Then
                            lblvChk.Visible = True
                        Else
                            lblvChk.Visible = False
                        End If
                        If status <> False Then
                            status = False
                        End If
                    Else
                        lblvChk.Visible = False
                        If value = "" And txtnumber.Visible Then
                            lblVNumber2.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        Else
                            lblVNumber2.Visible = False
                        End If
                    End If
                ElseIf cboKondisiAsset.SelectedValue = "U" And CBool(MandatoryUsed.Text) = True Then
                    If chk.Checked = False Then
                        If CBool(MandatoryUsed.Text) = True Then
                            lblvChk.Visible = True
                        Else
                            lblvChk.Visible = False
                        End If
                        If status <> False Then
                            status = False
                        End If
                    Else
                        lblvChk.Visible = False
                        If value = "" And txtnumber.Visible Then
                            lblVNumber2.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        Else
                            lblVNumber2.Visible = False
                        End If
                    End If
                End If

                If txtnumber.Text.Trim <> "" Then
                    oAssetData = New Parameter.AssetData
                    oData = New DataTable
                    oAssetData.strConnection = GetConnectionString()
                    oAssetData.Input = txtnumber.Text.Trim
                    oAssetData.AssetDocID = CType(dtgAssetDoc.Items(intLoop).FindControl("AssetDocID"), TextBox).Text.Trim
                    oAssetData.ApplicationId = Me.ApplicationID
                    oAssetData.AssetID = Me.Asset
                    oAssetData = m_controller.CheckAssetDoc(oAssetData)
                    oData = oAssetData.ListData
                    If oData.Rows.Count > 0 Then
                        lblVNumber.Visible = True
                        If status <> False Then
                            status = False
                        End If
                    End If
                End If
                objrow = oData3.NewRow
                objrow("AssetDocID") = CType(dtgAssetDoc.Items(intLoop).FindControl("AssetDocID"), TextBox).Text.Trim
                objrow("DocumentNo") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                objrow("IsMainDoc") = CType(dtgAssetDoc.Items(intLoop).FindControl("IsMainDoc"), TextBox).Text.Trim
                objrow("IsDocExist") = IIf(CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox).Checked = True, "1", "0").ToString
                objrow("IsFollowUp") = IIf(CType(dtgAssetDoc.Items(intLoop).FindControl("chkFU"), CheckBox).Checked = True, "1", "0").ToString

                If CType(dtgAssetDoc.Items(intLoop).FindControl("chkFU"), CheckBox).Checked Then
                    objrow("Notes") = CType(dtgAssetDoc.Items(intLoop).FindControl("ddlNotes"), DropDownList).SelectedValue.Trim
                Else
                    objrow("Notes") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNotes"), TextBox).Text.Trim
                End If
                If CType(dtgAssetDoc.Items(intLoop).FindControl("ucTglDokumen"), ucDateCE).Text.Trim <> "" Then
                    objrow("TglDokument") = ConvertDate2(CType(dtgAssetDoc.Items(intLoop).FindControl("ucTglDokumen"), ucDateCE).Text.Trim)
                Else
                    objrow("TglDokument") = "1900-01-01"
                End If


                oData3.Rows.Add(objrow)
            End If
        Next
        If status = False Then
            Exit Sub
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblMessage.Visible = False
        Try
            status = True
            Dim notValidValidators = Page.Validators.Cast(Of IValidator)().Where(Function(v) Not v.IsValid)

            If Page.IsValid Then

                Dim i As Integer

                Dim oAssetData As New Parameter.AssetData
                Dim oAddress As New Parameter.Address
                Dim oData1 As New DataTable
                Dim oData2 As New DataTable
                Dim oData3 As New DataTable

                oData1.Columns.Add("AssetLevel", GetType(Integer))
                oData1.Columns.Add("AssetCode", GetType(String))

                oData2.Columns.Add("AttributeID", GetType(String))
                oData2.Columns.Add("AttributeContent", GetType(String))

                oData3.Columns.Add("AssetDocID", GetType(String))
                oData3.Columns.Add("DocumentNo", GetType(String))
                oData3.Columns.Add("IsMainDoc", GetType(String))
                oData3.Columns.Add("IsDocExist", GetType(String))
                oData3.Columns.Add("IsFollowUp", GetType(String))
                oData3.Columns.Add("Notes", GetType(String))
                oData3.Columns.Add("TglDokument", GetType(Date))

                Validator(oData2, oData3)

                If status = False Then Exit Sub

                If status = False Then
                    If cboInsuredBy.SelectedValue = "CU" Then
                        cboPaidBy.ClearSelection()
                        cboPaidBy.Items.FindByValue("CU").Selected = True
                        cboPaidBy.Enabled = False
                    End If
                    Exit Sub
                End If

                Dim AssetCode As String = hdfAssetCode.Value

                Dim lArrValue As Array = CType(AssetCode.Split(CChar(".")), Array)
                Dim DataCount As Integer = UBound(lArrValue)
                Dim Asset As String = ""

                For Me.intLoop = 0 To DataCount
                    objrow = oData1.NewRow
                    objrow("AssetLevel") = intLoop + 1
                    If intLoop = 0 Then
                        Asset = lArrValue.GetValue(intLoop).ToString
                        objrow("AssetCode") = Asset
                    Else
                        Asset = Asset + "." + lArrValue.GetValue(intLoop).ToString
                        objrow("AssetCode") = Asset
                    End If
                    oData1.Rows.Add(objrow)
                Next

                With oAssetData
                    .BranchId = Replace(Me.sesBranchId, "'", "")
                    .AppID = Me.ApplicationID
                    .SupplierID = Me.SupplierID
                    .OTR = CDec(ucOTR.Text)
                    '.DP = CDec(IIf(txtDP.Text.Trim = "", "0", txtDP.Text))
                    .DP = CInt(txtDP.Text)
                    .AssetID = Me.Asset

                    .AssetCode = hdfAssetCode.Value
                    '.AssetCode = Me.AssetCode

                    .Serial1 = txtSerial1.Text
                    .Serial2 = ""
                    .UsedNew = cboKondisiAsset.SelectedValue
                    .AssetUsage = "X"
                    .ManufacturingYear = 0
                    .OldOwnerAsset = ""
                    .OwnerAssetCompany = False
                    .TaxDate = ""
                    .Notes = ""
                    .InsuredBy = cboInsuredBy.SelectedValue
                    .PaidBy = IIf(cboInsuredBy.SelectedValue = "CU", "CU", cboPaidBy.SelectedValue).ToString
                    .SalesmanID = cboSalesman.SelectedValue
                    .SalesSupervisorID = cboSalesSpv.SelectedValue
                    .SupplierAdminID = cboSupplierAdm.SelectedValue

                    .AOID = cbocmo.SelectedValue.ToString

                    .DateEntryAssetData = Me.BusinessDate
                    .Pemakai = ""
                    .Lokasi = ""
                    .HargaLaku = 0
                    .SR1 = ""
                    .SR2 = ""
                    .HargaSR1 = 0
                    .HargaSR2 = 0
                    .SplitPembayaran = False
                    .UangMukaBayarDi = cboUangMukaBayar.SelectedValue
                    .PencairanKe = cboPencairanKe.SelectedValue

                    .SupplierIDKaroseri = ""

                    .HargaKaroseri = 0
                    .PHJMB = 0

                    .StatusKendaraan = "T" 'tersedia
                    .NamaBPKBSama = False
                    .BPKBPengganti = False
                    .IsIzinTrayek = False
                    .AlasanSTNKExpired = ""

                    .Flag = "Add"
                    .strConnection = GetConnectionString()
                    .KondisiAsset = cboKondisiAsset.SelectedValue.ToString
                    .GradeCode = ""
                    .GradeValue = 0

                    .TrayekAtasNama = ""
                    .Jurusan = ""
                    .BukuKeur = ""

                End With


                Dim oReturn As New Parameter.AssetData
                oAddress.Address = ""
                oAddress.City = ""
                oAddress.ZipCode = ""

                oReturn = m_controller.AssetDataSaveAdd(oAssetData, oAddress, oData1, oData2, oData3)

                If oReturn.Output <> "" Then
                    ShowMessage(lblMessage, oReturn.Output, True)
                Else
                    With ucViewApplication1
                        .CustomerID = Me.CustomerID
                        .ApplicationID = Me.ApplicationID
                        .bindData()
                        .initControls("OnNewApplication")
                    End With

                    ucApplicationTab1.ApplicationID = Me.ApplicationID
                    ucApplicationTab1.setLink()
                    ShowMessage(lblMessage, "Data saved!", False)
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region


    Private Sub GetFee()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        With oApplication
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
            .ProductOffID = Me.ProductOfferingID
            .ProductID = Me.ProductID
        End With

        oApplication = oApplicationController.GetFee(oApplication)
        oData = oApplication.ListData
        'lblPersenUangMuka.Text = FormatNumber(oData.Rows(0).Item("DPPercentage").ToString, 0) & "%"
    End Sub

#Region "Calculate Pembiayaan"
    Protected Sub txtOTR_TextChanged() Handles ucOTR.TextChanged
        calcHargaOTR()
    End Sub

    Sub calcHargaOTR()
        Dim otr As Double

        otr = CDbl(IIf(IsNumeric(IIf(ucOTR.Text.Trim = "", "0", ucOTR.Text.Trim).ToString), IIf(ucOTR.Text.Trim = "", "0", ucOTR.Text.Trim).ToString, 0))
        lblTotalHargaOTR.Text = FormatNumber(CDbl(IIf(ucOTR.Text.Trim = "", "0", ucOTR.Text.Trim).ToString), 0)
    End Sub

#End Region


    Sub GetDefaultAssetData()
        Try
            Dim controllerassetData As New EditApplicationController
            Dim EntitiesAssetData As New Parameter.Application
            Dim oDataAsset As New DataTable

            With EntitiesAssetData
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
                .SpName = "EditAssetDataGetInfoAssetData"
            End With
            Me.SalesID = ""
            Me.SupervisorID = ""
            Me.AdminID = ""
            Me.AOID = ""

            EntitiesAssetData = controllerassetData.EditAssetDataGetInfoAssetData(EntitiesAssetData)

            If Not EntitiesAssetData Is Nothing Then
                oDataAsset = EntitiesAssetData.ListData

                If oDataAsset.Rows.Count > 0 Then
                    Me.SupplierID = oDataAsset.Rows(0).Item("SupplierID").ToString

                    txtSupplierCode.Text = oDataAsset.Rows(0).Item("SupplierID").ToString
                    txtSupplierName.Text = oDataAsset.Rows(0).Item("SupplierName").ToString


                    hdfAssetCode.Value = oDataAsset.Rows(0).Item(1).ToString
                    txtAssetName.Text = oDataAsset.Rows(0).Item(2).ToString
                    Me.AssetCode = hdfAssetCode.Value
                    ucOTR.Text = FormatNumber(oDataAsset.Rows(0).Item(3), 0)
                    lblTotalHargaOTR.Text = FormatNumber(CDbl(ucOTR.Text), 0)

                    cboPencairanKe.SelectedIndex = cboPencairanKe.Items.IndexOf(cboPencairanKe.Items.FindByValue(oDataAsset.Rows(0).Item("PencairanKe").ToString))

                    txtSerial1.Text = oDataAsset.Rows(0).Item(5).ToString
                    Me.NU = oDataAsset.Rows(0).Item("KondisiAsset").ToString
                    cboKondisiAsset.SelectedIndex = cboKondisiAsset.Items.IndexOf(cboKondisiAsset.Items.FindByValue(oDataAsset.Rows(0).Item("KondisiAsset").ToString))

                    cboInsuredBy.SelectedIndex = cboInsuredBy.Items.IndexOf(cboInsuredBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetInsuredBy").ToString))
                    cboPaidBy.SelectedIndex = cboPaidBy.Items.IndexOf(cboPaidBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetPaidBy").ToString))

                    Me.SalesID = oDataAsset.Rows(0).Item("SalesmanID").ToString
                    Me.SupervisorID = oDataAsset.Rows(0).Item("SalesSupervisorID").ToString
                    Me.AdminID = oDataAsset.Rows(0).Item("SupplierAdminID").ToString
                    Me.AOID = oDataAsset.Rows(0).Item("AOID").ToString
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Sub BindAttributeEdit()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.AppID = Me.ApplicationID
        oAssetData.AssetID = Me.Asset
        oAssetData.SpName = "spEditAssetDataAttribute"
        oAssetData = m_controller.EditGetAttribute(oAssetData)
        oData = oAssetData.ListData
        dtgAttribute.DataSource = oData
        dtgAttribute.DataBind()
    End Sub

    Sub BindAssetEdit(ByVal WhereCond As String)
        Dim oData As New DataTable
        Dim oDoc As New DocRec
        oDoc.strConnection = GetConnectionString()
        oDoc.WhereCond = WhereCond
        oDoc.SpName = "spEditAssetDataAssetDoc"
        oDoc = m_Doc.GetSPReport(oDoc)
        oData = oDoc.ListDataReport.Tables(0)
        dtgAssetDoc.DataSource = oData
        dtgAssetDoc.DataBind()
    End Sub

    Sub BindAssetRegistration()
        Dim entitiesAssetData As New Parameter.AssetData
        Dim oAssetDataController As New AssetDataController
        Dim oDataRegistration As New DataTable

        With entitiesAssetData
            .AppID = Me.ApplicationID
            .strConnection = GetConnectionString()
            .SpName = "spEditAssetDataAssetRegistration"
        End With

        entitiesAssetData = oAssetDataController.EditGetAssetRegistration(entitiesAssetData)

        If Not entitiesAssetData Is Nothing Then
            oDataRegistration = entitiesAssetData.ListData
        End If
    End Sub


    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        Me.SupplierID = txtSupplierCode.Text.ToString.Trim

        Me.AssetCode = hdfAssetCode.Value
        GetAssetMaster()
        AssetDocumentFilter()
        InitialLabelValid()
        getSupplierStatus(Me.SupplierID)

        For index = 0 To dtgAttribute.Items.Count - 1
            CType(dtgAttribute.Items(index).FindControl("lblVAttribute"), Label).Visible = False
        Next

        Dim WhereSup As String = "SupplierID = '" & txtSupplierCode.Text.ToString.Trim & "' and SupplierEmployeePosition = 'SL' "
        FillCboEmp("SupplierEmployee", cboSalesman, WhereSup)

        WhereSup = "SupplierID = '" & txtSupplierCode.Text.ToString.Trim & "' and SupplierEmployeePosition = 'SV'"
        FillCboEmp("SupplierEmployee", cboSalesSpv, WhereSup)

        WhereSup = "SupplierID = '" & txtSupplierCode.Text.ToString.Trim & "' and SupplierEmployeePosition = 'AM'"
        FillCboEmp("SupplierEmployee", cboSupplierAdm, WhereSup)

        'WhereSup = "BranchID = '" & Me.sesBranchId.Replace("'", "") & "' and EmployeePosition = 'AO'"
        WhereSup = "BranchID = '" & Me.sesBranchId.Replace("'", "") & "' and IsCMO = 1"
        FillCboEmp("BranchEmployee", cbocmo, WhereSup)

        If Request("Page") = "Edit" Then
            cboSalesman.SelectedIndex = cboSalesman.Items.IndexOf(cboSalesman.Items.FindByValue(Me.SalesID.Trim))
            cboSalesSpv.SelectedIndex = cboSalesSpv.Items.IndexOf(cboSalesSpv.Items.FindByValue(Me.SupervisorID.Trim))
            cboSupplierAdm.SelectedIndex = cboSupplierAdm.Items.IndexOf(cboSupplierAdm.Items.FindByValue(Me.AdminID.Trim))
            cbocmo.SelectedIndex = cbocmo.Items.IndexOf(cbocmo.Items.FindByValue(Me.AOID.Trim))
        Else
            If cboSalesman.Items.Count <= 1 Then
                ShowMessage(lblMessage, "Supplier tidak mempunyai salesman, Isi Salesman pada master supplier terlebih dahulu!", True)
                Exit Sub
            End If
        End If

        pnlLookupSupplier.Visible = False
        pnlLookupAsset.Visible = False
        pnlLast.Visible = True
        pnlNext.Visible = False
        ucOTR.Enabled = False

        If Me.DownPayment > 0 Then
            txtDP.Text = FormatNumber(Me.DownPayment, 0)
            txtDPPersen.Text = FormatNumber(Me.DownPayment / CDbl(ucOTR.Text) * 100)
        Else
            Dim totaldp As Double = CDbl(ucOTR.Text) * CDbl(txtDPPersen.Text) / 100
            txtDP.Text = FormatNumber(totaldp, 0)
        End If
        lblTotalPembiayaan.Text = FormatNumber(CStr(CDbl(lblTotalHargaOTR.Text) - CDbl(txtDP.Text)), 0)
    End Sub

    Sub GetAssetMaster()
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objcommand.Connection = objconnection
        objcommand.CommandType = CommandType.StoredProcedure
        objcommand.CommandText = "spAssetMasterView"
        objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = Me.AssetCode
        objread = objcommand.ExecuteReader
        If objread.Read Then
            Me.Origination = CStr(objread("Origination")).Trim
        End If

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objconnection.Dispose()
    End Sub

    Private Sub cboSalesman_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboSalesman.SelectedIndexChanged
        Dim SPVID As String
        Dim suppController As New Controller.SupplierController
        Dim oCustom As New Parameter.Supplier
        With oCustom
            .strConnection = GetConnectionString()
            .SupplierID = txtSupplierCode.Text
            .SupplierEmployeeID = cboSalesman.SelectedValue
        End With
        SPVID = suppController.GetSupplierEmployeeSPVBySales(oCustom).SupervisorID
        cboSalesSpv.SelectedIndex = cboSalesSpv.Items.IndexOf(cboSalesSpv.Items.FindByValue(SPVID))
    End Sub


    Sub AssetDocumentFilter()
        If Request("Page") = "Edit" Then
            BindAssetEdit(" AssetDocumentList.AssetTypeID = '" + Me.Asset + "'  and  AgreementAsset.ApplicationID = '" _
                    + Me.ApplicationID + "' ")
        Else
            BindAsset(" AssetDocumentList.AssetTypeID = '" + Me.Asset + "'")
        End If

    End Sub

    Private Sub getSupplierStatus(supplierid As String)
        Dim supController As New SupplierController
        Dim supData As New Parameter.Supplier
        Dim dt As New DataTable

        With supData
            .SupplierID = supplierid
            .strConnection = GetConnectionString()
        End With

        supData = supController.SupplierView(supData)
        lblSupplierStatus.Text = ""

        If Not supData Is Nothing Then
            dt = supData.ListData
            If dt.Rows.Count > 0 Then
                lblSupplierStatus.Text = "Status: " & dt.Rows(0).Item("SupplierBadStatus").ToString
            End If
        End If
    End Sub

End Class