﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewFinancialDataAgriculture_004.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewFinancialDataAgriculture_004" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabAgriculture.ascx" TagName="ucApplicationViewTab"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>View Financial Data 2</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <link rel="stylesheet" type="text/css" href="../../../../include/ui-lightness/jquery-ui-1.10.3.custom.css"  />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" class="tab_container_form">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucapplicationviewtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        VIEW APLIKASI</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA FINANSIAL 2</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h5>
                            REFUND MARGIN</h5>
                        <div style="display:none">
                        <asp:textbox ID="lblNilaiNTFOTR" runat="server" />
                        <asp:textbox ID="lblNilaiTenor" runat="server" />
                        <asp:textbox ID="lblNilaiRefundBunga" runat="server" />
                        <asp:TextBox ID="txtFlatRate" runat="server" />
                        <asp:TextBox ID="lblNilaiTotalBunga" runat="server" />
                        </div>
                    </div>
                    <div class="form_right">
                        <h5>
                            PREMI ASURANSI</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Total Margin
                        </label>
                        <asp:Label ID="lblTotalBunga" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Premi Asuransi
                        </label>
                        <asp:Label runat="server" ID="lblPremiAsuransiGross" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Refund Margin
                        </label>
                        <asp:Label runat="server" ID="txtRefundBunga" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Premi Asuransi Maskapai Nett
                        </label>
                        <asp:Label runat="server" ID="lblPremiAsuransiNet" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#f9f9f9">
                        <h5>SUBSIDI DEALER</h5>
                    </div>
                    <div class="form_right">
                        <label>Diskon Premi</label>
                        <asp:Label runat="server" ID="lblSelisihPremi" CssClass="numberAlign2 regular_text" style="font-weight:bold"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Margin Dealer</label>
                        <asp:Label runat="server" ID="txtSubsidiBungaDealer" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right"  style="background-color:#FCFCCC">
                        <label>Refund Premi</label>
                        <asp:Label runat="server" ID="txtRefundPremi" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Angsuran Dealer</label>
                        <asp:Label runat="server" ID="txtSubsidiAngsuran" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                     <label><b>Pendapatan / Pengeluaran Asuransi</b></label>
                     <asp:Label runat="server" ID="lblPendapatanPremi" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Uang Muka Dealer</label>
                        <asp:Label runat="server" ID="txtSubsidiUangMuka" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <h5>BIAYA LAINNYA</h5>
                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Dealer Discount</label>
                        <asp:Label runat="server" ID="txtDealerDiscount" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>

                    <div class="form_right">
                        <label>
                        Biaya Lainnya
                        </label>
                        <asp:Label ID="lblOtherFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Biaya Maintenance</label>
                        <asp:Label runat="server" ID="txtBiayaMaintenance" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Refund Biaya Lainnya</label>
                        <asp:Label runat="server" ID="txtRefundLain" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#f9f9f9">
                        <h5>SUBSIDI ATPM</h5>
                    </div>

                    <div class="form_right">
                        <h5>BIAYA PROVISI</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <div style="">
                            <label class="">
                                Supplier/Dealer ATPM</label>                            
                            <asp:Label ID="txtSupplierName" runat="server" ></asp:Label>
                        </div>
                    </div>
                    <div class="form_right">
                        <label>Biaya Provisi</label>
                        <asp:Label ID="lblBiayaProvisi" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Margin ATPM</label>
                        <asp:Label ID="txtSubsidiBungaATPM" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Refund Biaya Provisi</label>
                        <asp:Label ID="txtRefundBiayaProvisi" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Angsuran ATPM</label>
                        <asp:Label ID="txtSubsidiAngsuranATPM" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Uang Muka ATPM</label>
                        <asp:Label ID="txtSubsidiUangMukaATPM" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
            </div>

            <asp:Panel runat="server" ID="pnlPencairan">
                <div class="form_box_title">
                    <div class="form_left">
                        <h5>
                            PERHITUNGAN MARGIN NET</h5>
                    </div>
                    <div class="form_right">
                        <h5>
                            TOTAL PENCAIRAN</h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_left">
                        <asp:GridView runat="server" ID="gvBungaNet" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc1" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Margin Nett</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Nilai" DataFormatString="{0:N0}" HeaderStyle-CssClass="th_right"
                                    ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft" />
                                <asp:TemplateField  HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblRate" Text='<%# math.round(Container.DataItem("Rate"),2) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalRate"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="BungaNett" />
                            </Columns>
                        </asp:GridView>
                        <asp:GridView runat="server" ID="gvNetRateStatus" AllowPaging="false" AutoGenerateColumns="false"
                            CssClass="grid_general" ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <label>
                                            Status Rate</label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RateTypeResult" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="form_right">
                        <asp:GridView runat="server" ID="gvPencairan" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc2" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Total Pencairan</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                                    FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblNilai" Text='<%# formatnumber(Container.DataItem("Nilai"),0) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalPencairan"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FlagCustomer" Visible="false" />
                                <asp:BoundField DataField="FlagSupplier" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
            <div class="tab_container_form_space"><br />
            </div>
            <div class="tab_container_button">
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    </form>
</body>
</html>
