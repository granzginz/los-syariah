﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services


Public Class ViewFinancialDataAgriculture_004
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabAgriculture
#End Region


#Region "Constanta"
    Private m_controller As New FinancialDataController
    Private oController As New ApplicationDetailTransactionController
    Private TotalInsGrid As Double
    Private TotalProsentase As Double
    Private TotalBungaNett As Decimal
    Private TotalPencairan As Double
    Private TotalBungaNettAmount As Double
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property InstallmentAmount() As Double
        Get
            Return CDbl(ViewState("InstallmentAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("InstallmentAmount") = Value
        End Set
    End Property
    Property RefundInterest As Double
        Get
            Return CType(ViewState("RefundInterest"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundInterest") = value
        End Set
    End Property
    Property AdminFee As Double
        Get
            Return CType(ViewState("AdminFee"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("AdminFee") = value
        End Set
    End Property
    Property Provisi As Double
        Get
            Return CType(ViewState("Provisi"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("Provisi") = value
        End Set
    End Property
    Public Property FirstInstallment As String
        Get
            Return CStr(ViewState("FirstInstallment"))
        End Get
        Set(value As String)
            ViewState("FirstInstallment") = value
        End Set
    End Property

    Public Property Tenor As Integer
        Get
            Return CInt(ViewState("Tenor"))
        End Get
        Set(value As Integer)
            ViewState("Tenor") = value
        End Set
    End Property

    Public Property OtrNtf() As String
        Get
            Return CStr(ViewState("OtrNtf"))
        End Get
        Set(value As String)
            ViewState("OtrNtf") = value
        End Set
    End Property

    Public Property ntf() As Double
        Get
            Return CDbl(ViewState("NTF"))
        End Get
        Set(value As Double)
            ViewState("NTF") = value
        End Set
    End Property

    Private Property EffectiveRate() As Decimal
        Get
            Return CType(ViewState("EffectiveRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffectiveRate") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("CustID")
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .CustomerID = Me.CustomerID
                .bindData()
                .initControls("OnNewApplication")
            End With
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            Bindgrid()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial2")
            ucApplicationTab1.setLink()
        End If
    End Sub

#Region "Load Financial Data"
    Sub Bindgrid()
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData
        Dim objrow As DataRow

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_003"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)

        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.ListData
        End If

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            lblNilaiNTFOTR.Text = objrow(objrow("OtrNtf").ToString.Trim).ToString
            lblNilaiRefundBunga.Text = objrow("NilaiRefundBunga").ToString
            lblNilaiTenor.Text = CStr(Math.Ceiling(CDbl(objrow("tenor").ToString) / 12))
            txtFlatRate.Text = FormatNumber(objrow.Item("FlatRate"), 2)
            lblTotalBunga.Text = FormatNumber(CDbl(objrow("TotalBunga")), 0)
            lblNilaiTotalBunga.Text = objrow("TotalBunga").ToString.Trim
            lblPremiAsuransiGross.Text = FormatNumber(CDbl(objrow.Item("PremiGross")), 0)

            If CInt(objrow("refundBungaPercent")) > 0 Then
                txtRefundBunga.Text = FormatNumber(objrow("refundBungaPercent"), 2) & "% &nbsp;&nbsp;&nbsp;" & FormatNumber(objrow("refundBunga"), 0)
            Else
                txtRefundBunga.Text = "0"
            End If

            lblPremiAsuransiNet.Text = FormatNumber(objrow.Item("PremiNett"), 0)
            Dim pgross, pinsnet, pselisih, ppolis, potherfee As Double
            pgross = CDbl(IIf(IsNumeric(lblPremiAsuransiGross.Text), lblPremiAsuransiGross.Text, 0))
            pinsnet = CDbl(IIf(IsNumeric(lblPremiAsuransiNet.Text), lblPremiAsuransiNet.Text, 0))
            ppolis = CDbl(IIf(IsNumeric(objrow.Item("BiayaPolis")), objrow.Item("BiayaPolis"), 0))
            potherfee = CDbl(IIf(IsNumeric(objrow.Item("OtherFee")), objrow.Item("OtherFee"), 0))
            pselisih = pgross - pinsnet
            lblSelisihPremi.Text = FormatNumber(pselisih, 0)

            txtSubsidiBungaDealer.Text = FormatNumber(objrow("SubsidiBungaDealer"), 0)

            'Refund Premi
            Dim refundPremi_ As Double = 0
            If CInt(objrow.Item("RefundPremiPercent")) > 0 Then
                txtRefundPremi.Text = FormatNumber(CDbl(objrow.Item("RefundPremiPercent")), 2) & "% &nbsp;&nbsp;&nbsp;" & FormatNumber(objrow("RefundPremi"), 0)
                refundPremi_ = CDbl(objrow("RefundPremi"))
            Else
                txtRefundPremi.Text = "0"
            End If
            lblPendapatanPremi.Text = FormatNumber(pselisih - refundPremi_, 0)
            txtSubsidiBungaATPM.Text = FormatNumber(objrow("SubsidiBungaATPM"), 0)
            txtSubsidiAngsuran.Text = FormatNumber(objrow("SubsidiAngsuran"), 0)
            txtSubsidiAngsuranATPM.Text = FormatNumber(objrow("SubsidiAngsuranATPM"), 0)
            lblOtherFee.Text = FormatNumber(objrow.Item("OtherFee"), 0)
            txtSubsidiUangMuka.Text = FormatNumber(objrow.Item("SubsidiUangMuka"), 0)
            txtSubsidiUangMukaATPM.Text = FormatNumber(objrow.Item("SubsidiUangMukaATPM"), 0)
            txtRefundLain.Text = FormatNumber(objrow.Item("RefundBiayaLainPercent"), 2) & "% &nbsp;&nbsp;&nbsp;" & FormatNumber(objrow.Item("RefundBiayaLain"), 0)
            txtDealerDiscount.Text = FormatNumber(objrow.Item("DealerDiscount"), 0)
            lblBiayaProvisi.Text = FormatNumber(objrow("ProvisionFee"), 0)
            txtBiayaMaintenance.Text = FormatNumber(objrow("BiayaMaintenance"), 0)
            txtRefundBiayaProvisi.Text = FormatNumber(objrow("RefundBiayaProvisiPercent"), 2) & "% &nbsp;&nbsp;&nbsp;" & FormatNumber(objrow.Item("RefundBiayaProvisi"), 0)
            txtSupplierName.Text = objrow.Item("SupplierNameATPM").ToString.Trim

            Me.RefundInterest = CDbl(objrow.Item("RefundInterest"))
            Me.AdminFee = CDbl(objrow.Item("AdminFee"))
            Me.Provisi = CDbl(objrow.Item("ProvisionFee"))
            Me.FirstInstallment = objrow.Item("FirstInstallment").ToString
            Me.Tenor = CInt(objrow.Item("Tenor").ToString.Trim)
            Me.InstallmentAmount = CDbl(objrow("InstallmentAmount").ToString.Trim)
            Me.OtrNtf = objrow("OtrNtf").ToString.Trim
            Me.ntf = CDbl(objrow("NTF"))
            Me.EffectiveRate = CDec(objrow.Item("EffectiveRate"))

            showPencairan()
        End If
    End Sub

    Public Sub showPencairan()
        Dim oClass As New Parameter.ApplicationDetailTransaction

        With oClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
        End With

        Try
            oClass = oController.getBungaNett(oClass)

            gvBungaNet.DataSource = oClass.Tabels.Tables(0)
            gvBungaNet.DataBind()

            gvPencairan.DataSource = oClass.Tabels.Tables(1)
            gvPencairan.DataBind()

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub gvBungaNet_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBungaNet.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblRate As Label = CType(e.Row.Cells(2).FindControl("lblRate"), Label)

            If gvBungaNet.DataKeys(e.Row.RowIndex).Item("TransID").ToString <> "EFF" Then
                Dim nilai As Double = CType(e.Row.Cells(1).Text, Double)
                lblRate.Text = getRateDetailTransaksi(ntf + nilai).ToString
            End If

            If e.Row.Cells(3).Text = "+" Then
                TotalBungaNett += CDec(lblRate.Text)
            Else
                TotalBungaNett -= CDec(lblRate.Text)
            End If

        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTotalRate As Label = CType(e.Row.Cells(2).FindControl("lblTotalRate"), Label)
            lblTotalRate.Text = Math.Round(TotalBungaNett, 2).ToString
        End If

    End Sub

    Private Sub gvPencairan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPencairan.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblNilai As Label = CType(e.Row.Cells(1).FindControl("lblNilai"), Label)

            If e.Row.Cells(3).Text = "+" Then
                TotalPencairan += CDbl(lblNilai.Text)
            Else
                TotalPencairan -= CDbl(lblNilai.Text)
            End If


        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalPencairan As Label = CType(e.Row.Cells(1).FindControl("lblTotalPencairan"), Label)
            'lblTotalPencairan.Text = FormatNumber(Math.Ceiling(Math.Round(TotalPencairan, 0) / 1000) * 1000, 0)
            'lblTotalPencairan.Text = FormatNumber(Math.Round(TotalPencairan / 10, 0) * 10, 0)
            lblTotalPencairan.Text = FormatNumber(TotalPencairan, 0)


        End If
    End Sub

    Function getRateDetailTransaksi(TransAmount As Double) As Decimal
        Dim xrate As Decimal
        If Me.FirstInstallment = "AD" Then
            xrate = RateConvertion.GetEffectiveRateAdv(CInt(Tenor), CDbl(Me.InstallmentAmount), TransAmount, 1)
        Else
            xrate = RateConvertion.GetEffectiveRateArr(CInt(Tenor), CDbl(Me.InstallmentAmount), TransAmount, 1)
        End If

        Return Math.Round(Math.Abs(Me.EffectiveRate - xrate), 2)
    End Function

#End Region


End Class