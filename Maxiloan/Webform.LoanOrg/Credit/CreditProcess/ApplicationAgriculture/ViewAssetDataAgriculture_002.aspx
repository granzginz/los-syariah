﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAssetDataAgriculture_002.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewAssetDataAgriculture_002" %>

<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabAgriculture.ascx" TagName="ucApplicationViewTab"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../Webform.UserController/ucAddressCity.ascx" TagName="ucAddress"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Asset Data Agriculture</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <link rel="stylesheet" type="text/css" href="../../../../include/ui-lightness/jquery-ui-1.10.3.custom.css"  />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" class="tab_container_form">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="upnlSupplier" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucapplicationviewtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        VIEW APLIKASI</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA ASSET
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">    
                    <label class="">
                        Supplier/Dealer</label>
                    <asp:Label ID="txtSupplierName" runat="server" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                            Jenis Asset</label>                            
                        <asp:Label ID="txtAssetName" runat="server" ></asp:Label>
                    </div>
                </div>
            </div>
           
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Harga per unit
                        </label>
                        <asp:Label ID="ucOTR" runat="server" CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Kondisi Asset
                        </label>
                        <asp:Label ID="lblKondisiAsset" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboKondisiAsset" runat="server" Width="150PX" visible="false">
                            <asp:ListItem Text="BEKAS" Value="U" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="BARU" Value="N"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Uang Muka</label>
                            <asp:Label ID="txtDPPersen" runat="server" CssClass="regular_text"></asp:Label> %
                            <asp:Label ID="txtDP" runat="server" CssClass="regular_text numberAlign "></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Uang Muka bayar di
                        </label>
                        <asp:Label ID="lblUangMukaBayar" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboUangMukaBayar" runat="server" Width="150PX" Visible="false">
                            <asp:ListItem Selected="True" Text="SUPPLIER" Value="S"></asp:ListItem>
                            <asp:ListItem Text="MULTI FINANCE" Value="A"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Total Pokok Hutang
                        </label>
                        <asp:Label runat="server" ID="lblTotalPembiayaan"  CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Pencairan Ke
                        </label>
                        <asp:Label ID="lblPencairanKe" runat="server" ></asp:Label>
                        <asp:DropDownList ID="cboPencairanKe" runat="server" Width="150PX" Visible="false">
                            <asp:ListItem Text="SUPPLIER" Value="S" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="CUSTOMER" Value="A"></asp:ListItem>
                        </asp:DropDownList>
                    </div>


                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        ASSET INFO</h4>
                </div>
            </div>
            <div class="form_box_uc">
                <div class="form_left_uc">
                    <asp:DataGrid ID="dtgAttribute" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                        BorderWidth="0" BorderStyle="none" CssClass="grid_general">
                        <ItemStyle CssClass="item_grid_attr" />
                        <Columns>
                            <asp:BoundColumn DataField="Name">
                                <ItemStyle CssClass="label_col item_grid_attr_collabel" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="AttributeContent">
                                <ItemStyle CssClass="left_col" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="form_right_uc">
                    <div class="form_single_wb">
                        <asp:Label ID="lblSerial1" runat="server" CssClass="label"></asp:Label>
                        <asp:Label ID="txtSerial1" runat="server" ></asp:Label>
                    </div>
                    <%--<div class="form_single_wb">
                        <asp:Label ID="lblSerial2" runat="server" CssClass="label"></asp:Label>
                        <asp:Label ID="txtSerial2" runat="server" ></asp:Label>
                    </div>--%>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        ASURANSI ASSET</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="">
                        Di Asuransi Oleh
                    </label>
                    <asp:Label ID="lblInsuredBy" runat="server" ></asp:Label>
                    <asp:DropDownList ID="cboInsuredBy" runat="server" Visible="false">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="">
                        Di bayar oleh
                    </label>
                    <asp:Label ID="lblPaidBy" runat="server" ></asp:Label>
                    <asp:DropDownList ID="cboPaidBy" runat="server" Visible="false">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box_title">
                <div>
                    <div class="form_left">
                        <h4>
                            KARYAWAN</h4>
                    </div>
                    <div class="form_right">
                        <h4>
                            KARYAWAN SUPPLIER
                        </h4>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="">
                        CMO &nbsp;&nbsp;&nbsp;</label>
                        <asp:Label ID="lblcmo" runat="server" ></asp:Label>
                    </div>
                    <div class="form_right">
                        <label class="">
                            Salesman
                        </label>
                        <asp:Label ID="lblSalesman" runat="server" ></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DOKUMEN ASSET</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgAssetDoc" runat="server" EnableViewState="False" AutoGenerateColumns="False"
                        BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" EnableViewState="False"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Document" HeaderText="DOKUMEN">
                                <HeaderStyle Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Number" HeaderText="NO DOKUMEN">
                                <HeaderStyle Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Checked" HeaderText="PERIKSA">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Notes" HeaderText="CATATAN">
                                <HeaderStyle Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
            
            <div class="tab_container_form_space"><br />
            </div>
            <div class="tab_container_button">
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
