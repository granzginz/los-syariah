﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAppInsuranceAgriculture.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewAppInsuranceAgriculture" %>

<%@ Register Src="../../../../Webform.UserController/ViewApplication/UcInsuranceDataAgriculture.ascx"  TagName="UcInsuranceData"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabAgriculture.ascx" TagName="ucApplicationViewTab"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../Webform.UserController/ucCompanyAddress.ascx" TagName="UcCompanyAdress"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Data Asuransi</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <link rel="stylesheet" type="text/css" href="../../../../include/ui-lightness/jquery-ui-1.10.3.custom.css"  />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" class="tab_container_form">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucapplicationviewtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        VIEW APLIKASI</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <uc1:ucinsurancedata id="UcInsuranceData" runat="server"></uc1:ucinsurancedata>
            <div class="tab_container_form_space"><br />
            </div>
            <div class="tab_container_button">
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
