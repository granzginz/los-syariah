﻿#Region "Imports"
Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Webform
#End Region

Public Class AppInsuranceAgriculture
    Inherits Maxiloan.Webform.WebBased

#Region "Property"

    Private Property Tenor() As String
        Get
            Return (CType(ViewState("Tenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Tenor") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return (CType(ViewState("CustomerName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property PageSource() As String
        Get
            Return (CType(ViewState("PageSource"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PageSource") = Value
        End Set
    End Property
    Private Property MaximumTenor() As String
        Get
            Return (CType(ViewState("MaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("MaximumTenor") = Value
        End Set
    End Property
    Private Property MinimumTenor() As String
        Get
            Return (CType(ViewState("MinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("MinimumTenor") = Value
        End Set
    End Property
    Private Property ModuleID() As String
        Get
            Return (CType(ViewState("ModuleID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ModuleID") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property ApplicationTypeID() As String
        Get
            Return CType(ViewState("ApplicationTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationTypeID") = Value
        End Set
    End Property
    Private Property CoverageTypeMs() As DataTable
        Get
            Return CType(ViewState("CoverageTypeMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("CoverageTypeMs") = Value
        End Set
    End Property
    Private Property PaidByCustMs() As DataTable
        Get
            Return CType(ViewState("PaidByCustMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("PaidByCustMs") = Value
        End Set
    End Property
    Private Property MaskAssBranchID() As String
        Get
            Return CType(ViewState("MaskAssBranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MaskAssBranchID") = Value
        End Set
    End Property
    Private Property InsuranceType() As String
        Get
            Return CType(ViewState("InsuranceType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsuranceType") = Value
        End Set
    End Property
    Private Property AssetUsageID() As String
        Get
            Return CType(ViewState("AssetUsageID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetUsageID") = Value
        End Set
    End Property
    Private Property AssetNewUsed() As String
        Get
            Return CType(ViewState("AssetNewUsed"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetNewUsed") = Value
        End Set
    End Property
    Private Property IsPreview() As String
        Get
            Return (CType(ViewState("IsPreview"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("IsPreview") = Value
        End Set
    End Property
    Private Property idxGridInscoSelected As Integer
        Get
            Return CType(ViewState("idxGridInscoSelected"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("idxGridInscoSelected") = value
        End Set
    End Property

    Private Property JenisCover() As DataTable()
        Get
            Return CType(ViewState("JenisCover"), DataTable())
        End Get
        Set(ByVal Value As DataTable())
            ViewState("JenisCover") = Value
        End Set
    End Property

    Public Property AmountCoverage() As Double
        Get
            Return (CType(ViewState("AmountCoverage"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountCoverage") = Value
        End Set
    End Property

    Private Property AssetSeqNo As Int16
        Get
            Return CType(ViewState("AssetSeqNo"), Int16)
        End Get
        Set(ByVal value As Int16)
            ViewState("AssetSeqNo") = value
        End Set
    End Property

    Private Property totalPremitext As Double
        Get
            Return CType(lblPremi.Text, Double)
        End Get
        Set(value As Double)
            lblPremi.Text = FormatNumber(value, 0)
        End Set
    End Property

    Public Property PremiDiBayar As Double
        Get
            Return CType(lblBayarTunai.Text, Double)
        End Get
        Set(value As Double)
            lblBayarTunai.Text = FormatNumber(value, 0)
        End Set
    End Property
    Public Property PremiDiKredit As Double
        Get
            Return CType(lblPremiDiKredit.Text, Double)
        End Get
        Set(value As Double)
            lblPremiDiKredit.Text = FormatNumber(value, 0)
        End Set
    End Property

    Private Property NilaiAdminFeeAwal() As Double
        Get
            Return (CType(ViewState("NilaiAdminFeeAwal"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("NilaiAdminFeeAwal") = Value
        End Set
    End Property

    Private Property NilaiStampDutyFeeAwal() As Double
        Get
            Return (CType(ViewState("NilaiStampDutyFeeAwal"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("NilaiStampDutyFeeAwal") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE As String = CommonCacheHelper.CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE
    Private oCustomclass As New Parameter.NewAppInsuranceByCompany

    Private oController As New NewAppInsuranceByCompanyController

    Private oControllerPremium As New InsuranceApplicationController
    Private oInsAppController As New InsuranceApplicationController
    Private oCustomClassResult As New Parameter.InsuranceCalculationResult
    Private oInsCalResultController As New InsuranceCalculationResultController

    Private Const COMPANY_CUSTOMER As String = "CompanyCustomer"
    Private Const CACHE_APPLICATION_TYPE As String = "CacheApplicationType"
#End Region

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabAgriculture
    Protected WithEvents cmbMaskAssBranch As UcInsuranceBranchName
    Protected WithEvents txtRate As ucNumberFormat
    Protected WithEvents txtAmountRate As ucNumberFormat
    Protected WithEvents txtBiayaPolis As ucNumberFormat
    Protected WithEvents txtBiayaMaterai As ucNumberFormat
    Protected WithEvents txtBayarTunai As ucNumberFormat

    Dim jenisCoverArray As New List(Of TotalPremiAsuransi)
    Dim listjeniscover As New List(Of String())
    Dim listGridAsuransi As New List(Of DataGrid)

#End Region

#Region "Default Panel"
    Sub InitialDefaultPanel()
        lblMessage.Visible = False
        PnlGrid.Visible = False
        pnlJenisCover.Visible = False
        PnlDGridPaid.Visible = False
        btnOK.Visible = True
        pnlPaidHPP.Visible = False
        txtBiayaPolis.isReadOnly = True
        txtBiayaMaterai.isReadOnly = True
        With cmbMaskAssBranch
            .FillRequired = True
        End With
    End Sub
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Visible = False

        ' Get Coverage Type From Database
        Dim strsqlQuery As String
        strsqlQuery = CommonVariableHelper.SQL_QUERY_COVERAGE_TYPEMS
        Dim objAdapter As New SqlDataAdapter(strsqlQuery, GetConnectionString)
        Dim CboCoverageTypeDT As New DataTable("CoverageTypeMs")
        objAdapter.Fill(CboCoverageTypeDT)

        For Each row As DataRow In CboCoverageTypeDT.Rows
            listjeniscover.Add({row("ID").ToString.Trim, row("DESCRIPTION").ToString.Trim})
        Next

        If Not IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                Me.ApplicationID = Request("ApplicationID")
                Me.PageSource = Request("PageSource")
                Me.Mode = Request("Edit")
                InitialDefaultPanel()
                If Not getInsEntryStep1() Then Exit Sub
            End If

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnOK.Visible = False
                End If

                .initControls("OnNewApplication")
                .CustomerID = Me.CustomerID
            End With

            With ucViewCustomerDetail1
                .CustomerID = Me.CustomerID
                .bindCustomerDetail()
            End With

            With ucApplicationTab1
                .ApplicationID = Me.ApplicationID
                .selectedTab("Asuransi")
                .setLink()
            End With
        End If
    End Sub
#End Region


    Protected Function getInsEntryStep1() As Boolean
        Dim oNewAppInsuranceMultiAsset As New Parameter.NewAppInsuranceByCompany
        Dim oInsuranceCom As New Parameter.NewAppInsuranceByCompany
        Dim assetData As DataTable
        Dim IsBMSave As Boolean

        With oNewAppInsuranceMultiAsset
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With
        With oInsuranceCom
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With

        Try
            Dim dtasset As DataTable
            assetData = oController.GetAssetCategory(oNewAppInsuranceMultiAsset)
            Dim nilaiCover As Double = 0

            For Each dtr As DataRow In assetData.Rows
                Dim dbgrid As New System.Web.UI.WebControls.DataGrid
                Dim dt As New DataTable

                Dim dr As DataRow
                Dim tn As Decimal = CType(IIf(dtr.Item("Tenor").ToString.Trim <> "", dtr.Item("Tenor"), "0"), Decimal)
                Dim years As Decimal = Math.Ceiling(tn / 12)
                dt.Columns.Add("Tahun", GetType(Integer))
                dt.Columns.Add("JenisCover", GetType(String))
                dt.Columns.Add("Penyusutan", GetType(Decimal))
                dt.Columns.Add("NilaiCover", GetType(String))
                dt.Columns.Add("RatePremi", GetType(Decimal))
                dt.Columns.Add("Premi", GetType(Decimal))
                For i As Integer = 1 To CInt(years)
                    dr = dt.NewRow
                    dr("Tahun") = i
                    dr("JenisCover") = ""
                    dr("Penyusutan") = "100"
                    dr("NilaiCover") = dtr.Item("NilaiCover").ToString.Trim
                    dr("RatePremi") = "0"
                    dr("Premi") = "0"
                    dt.Rows.Add(dr)
                Next
                nilaiCover = nilaiCover + CType(dtr.Item("NilaiCover").ToString.Trim, Double)
            Next

            oNewAppInsuranceMultiAsset = oController.GetInsuranceEntryStep1List(oNewAppInsuranceMultiAsset)
            'dtasset = oNewAppInsuranceMultiAsset.ListData
            Me.CustomerName = oNewAppInsuranceMultiAsset.CustomerName
            Me.CustomerID = oNewAppInsuranceMultiAsset.CustomerID
            LblInsuredBy.Text = oNewAppInsuranceMultiAsset.InsAssetInsuredByName
            LblPaidBy.Text = oNewAppInsuranceMultiAsset.InsAssetPaidByName
            Me.AmountCoverage = nilaiCover
            Me.InsuranceType = oNewAppInsuranceMultiAsset.InsuranceType
            Me.BranchID = oNewAppInsuranceMultiAsset.BranchId
            lblAmountCoverage.Text = FormatNumber(Me.AmountCoverage, 0)
            TxtTenorCredit.Text = oNewAppInsuranceMultiAsset.Tenor
            txtRate.Text = FormatNumber(oNewAppInsuranceMultiAsset.sellingRate, 2)

            If Me.EmpPos = "BM" Then
                cmbMaskAssBranch.loadInsBranch(False)
                cmbMaskAssBranch.Enabled = True
            Else
                IsBMSave = CType(oNewAppInsuranceMultiAsset.IsBMSave, Boolean)
                If IsBMSave Then
                    cmbMaskAssBranch.loadInsBranch(False)
                    cmbMaskAssBranch.Enabled = False
                Else
                    cmbMaskAssBranch.loadInsBranch(True)
                    cmbMaskAssBranch.Enabled = True
                End If
            End If

            cmbMaskAssBranch.SelectedValue = oNewAppInsuranceMultiAsset.InsuranceComBranchID

            Me.NilaiAdminFeeAwal = 0
            Me.NilaiStampDutyFeeAwal = 0
            If oNewAppInsuranceMultiAsset.InsuranceComBranchID <> "" Then
                txtBiayaPolis.Text = FormatNumber(oNewAppInsuranceMultiAsset.BiayaPolis, 2)
                txtBiayaMaterai.Text = FormatNumber(oNewAppInsuranceMultiAsset.BiayaMaterai, 2)
                Me.NilaiAdminFeeAwal = oNewAppInsuranceMultiAsset.BiayaPolis
                Me.NilaiStampDutyFeeAwal = oNewAppInsuranceMultiAsset.BiayaMaterai
            End If

            If Me.NilaiAdminFeeAwal = 0 And Me.NilaiStampDutyFeeAwal = 0 Then
                'Get Biaya polis dan biaya materai dari table InsuranceComBranch
                oInsuranceCom.InsuranceComBranchID = oNewAppInsuranceMultiAsset.InsuranceComBranchID
                Dim insCom As DataTable = oController.GetInsurancehComByBranch(oInsuranceCom)
                If insCom.Rows.Count > 0 Then
                    Me.NilaiAdminFeeAwal = CDbl(IIf(IsNumeric(insCom.Rows(0).Item("AdminFee")), insCom.Rows(0).Item("AdminFee"), 0))
                    Me.NilaiStampDutyFeeAwal = CDbl(IIf(IsNumeric(insCom.Rows(0).Item("StampDutyFee")), insCom.Rows(0).Item("StampDutyFee"), 0))
                    txtBiayaPolis.Text = FormatNumber(Me.NilaiAdminFeeAwal, 2)
                    txtBiayaMaterai.Text = FormatNumber(Me.NilaiStampDutyFeeAwal, 2)
                End If
            End If

            Return True

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try

    End Function

    Protected Function getInsEntryStep2() As Boolean
        Dim oNewAppInsuranceMultiAsset As New Parameter.NewAppInsuranceByCompany
        Dim assetData As DataTable
        Dim tnr As Decimal = CType(IIf(TxtTenorCredit.Text = "", "0", TxtTenorCredit.Text.Trim), Decimal)

        With oNewAppInsuranceMultiAsset
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With

        Try
            assetData = oController.GetAssetCategory(oNewAppInsuranceMultiAsset)
            Dim nilaiCover As Double = 0

            For Each dtr As DataRow In assetData.Rows
                Dim dbgrid As New System.Web.UI.WebControls.DataGrid
                Dim lblasset As New Label
                Dim dt As New DataTable
                Dim linespace As New Label
                linespace.Text = "<br />"

                Dim dr As DataRow
                Dim years As Decimal = Math.Ceiling(tnr / 12)
                dt.Columns.Add("Tahun", GetType(Integer))
                dt.Columns.Add("JenisCover", GetType(String))
                dt.Columns.Add("Penyusutan", GetType(Decimal))
                dt.Columns.Add("NilaiCover", GetType(Decimal))
                dt.Columns.Add("RatePremi", GetType(Decimal))
                dt.Columns.Add("Premi", GetType(Decimal))
                For i As Integer = 1 To CInt(years)
                    dr = dt.NewRow
                    dr("Tahun") = i
                    dr("JenisCover") = ""
                    dr("Penyusutan") = 100
                    dr("NilaiCover") = dtr.Item("NilaiCover").ToString.Trim
                    dr("RatePremi") = 0
                    dr("Premi") = 0
                    dt.Rows.Add(dr)
                Next
                nilaiCover = nilaiCover + CType(IIf(dtr.Item("NilaiCover").ToString.Trim = "", "0", dtr.Item("NilaiCover").ToString.Trim), Decimal)

                lblasset.Text = "Kategori Asset : " & dtr.Item("AssetTypeID").ToString.Trim

                ' Create Dynamic Grid
                dbgrid.ID = "grid_" & dtr.Item("AssetTypeID").ToString.Trim
                AddHandler dbgrid.ItemDataBound, AddressOf Item_Bound
                dbgrid.DataSource = dt
                dbgrid.CssClass = "grid_general"
                dbgrid.AutoGenerateColumns = False
                dbgrid.ItemStyle.CssClass = "item_grid"
                dbgrid.HeaderStyle.CssClass = "th"
                For Each c As DataColumn In dt.Columns
                    dbgrid.Columns.Add(CreateBoundColumn(c))
                Next
                dbgrid.DataBind()

                pnlJenisCover.Controls.Add(lblasset)
                pnlJenisCover.Controls.Add(dbgrid)
                pnlJenisCover.Controls.Add(linespace)

                listGridAsuransi.Add(dbgrid)
            Next
            Return True

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try

    End Function

    Protected Function ValidatorPremi() As Boolean
        Dim rtn As Boolean = True
        Try
            For Each grid As DataGrid In listGridAsuransi
                For Each it As DataGridItem In grid.Items
                    If (it.FindControl("JenisCover") IsNot Nothing) Then
                        Dim jnc As DropDownList = DirectCast(it.FindControl("JenisCover"), DropDownList)
                        If jnc.SelectedValue = "" Then
                            ShowMessage(lblMessage, "Harap pilih jenis cover asuransi", True)
                            rtn = False
                        Else
                            jnc.Enabled = False
                        End If
                    End If
                    If (it.FindControl("Premi") IsNot Nothing) Then
                        Dim ucNumber As ucNumberFormat = DirectCast(it.FindControl("Premi"), ucNumberFormat)
                        If ucNumber.txtNumber.Text.Trim = "0" Or ucNumber.txtNumber.Text.Trim = "" Then
                            ShowMessage(lblMessage, "Premi harus lebih besar dari 0", True)
                            ucNumber.txtNumber.ReadOnly = False
                            rtn = False
                        Else
                            ucNumber.txtNumber.ReadOnly = True
                        End If
                    End If
                    If (it.FindControl("RatePremi") IsNot Nothing) Then
                        Dim ucNumber As ucNumberFormat = DirectCast(it.FindControl("RatePremi"), ucNumberFormat)
                        If ucNumber.txtNumber.Text.Trim = "0" Or ucNumber.txtNumber.Text.Trim = "" Then
                            ShowMessage(lblMessage, "Rate premi harus lebih besar dari 0", True)
                            ucNumber.txtNumber.ReadOnly = False
                            rtn = False
                        Else
                            ucNumber.txtNumber.ReadOnly = True
                        End If
                    End If
                    If (it.FindControl("Penyusutan") IsNot Nothing) Then
                        Dim ucNumber As ucNumberFormat = DirectCast(it.FindControl("Penyusutan"), ucNumberFormat)
                        If ucNumber.txtNumber.Text.Trim = "0" Or ucNumber.txtNumber.Text.Trim = "" Then
                            ShowMessage(lblMessage, "Penyusutan harus lebih besar dari 0", True)
                            ucNumber.txtNumber.ReadOnly = False
                            rtn = False
                        Else
                            ucNumber.txtNumber.ReadOnly = True
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            rtn = False
        End Try
        Return rtn
    End Function

    Sub Item_Bound(sender As Object, e As DataGridItemEventArgs)
        Dim row As DataRowView = CType(e.Item.DataItem, DataRowView)
        Dim frm As NameValueCollection = Page.Request.Form
        Dim thn As Integer = e.Item.ItemIndex

        If (e.Item.FindControl("JenisCover") IsNot Nothing) Then
            Dim jnc As DropDownList = DirectCast(e.Item.FindControl("JenisCover"), DropDownList)
            Dim lbl As Label = DirectCast(e.Item.FindControl("JenisCoverlabel"), Label)
            jnc.SelectedValue = frm(jnc.UniqueID)
            If Me.IsPreview = "1" Then
                Dim found As Boolean = False
                For Each j As TotalPremiAsuransi In jenisCoverArray
                    If j.tahun = thn Then
                        j.jeniscover = j.jeniscover & ", " & getJenisCoverDescription(frm(jnc.UniqueID).ToString.Trim)
                        found = True
                    End If
                Next
                If found = False Then
                    Dim jc As New TotalPremiAsuransi
                    jc.tahun = thn
                    jc.jeniscover = getJenisCoverDescription(frm(jnc.UniqueID).ToString.Trim)
                    jc.premitotal = 0
                    jenisCoverArray.Add(jc)
                End If
                If jnc.SelectedValue = "" Then
                    lbl.Visible = True
                End If
            End If
        End If
        If (e.Item.FindControl("RatePremi") IsNot Nothing) Then
            Dim ucNumber As ucNumberFormat = DirectCast(e.Item.FindControl("RatePremi"), ucNumberFormat)
            Dim lbl As Label = DirectCast(e.Item.FindControl("RatePremilabel"), Label)
            Dim r As DataRowView = CType(e.Item.DataItem, DataRowView)
            ucNumber.Text = frm(ucNumber.txtNumber.UniqueID)
            If Me.IsPreview = "1" Then
                ucNumber.txtNumber.ReadOnly = True
                If ucNumber.Text = "" Then
                    lbl.Visible = True
                    ucNumber.txtNumber.ReadOnly = False
                End If
            End If
            ucNumber.OnClientChange = "hitungpremip(" & r.Item("NilaiCover").ToString & ", this.id)"
        End If
        If (e.Item.FindControl("Premi") IsNot Nothing) Then
            Dim ucNumber As ucNumberFormat = DirectCast(e.Item.FindControl("Premi"), ucNumberFormat)
            Dim lbl As Label = DirectCast(e.Item.FindControl("Premilabel"), Label)
            Dim r As DataRowView = CType(e.Item.DataItem, DataRowView)
            ucNumber.Text = frm(ucNumber.txtNumber.UniqueID)
            If Me.IsPreview = "1" Then
                Dim found As Boolean = False
                For Each j As TotalPremiAsuransi In jenisCoverArray
                    If j.tahun = thn Then
                        j.premitotal = j.premitotal + CType(IIf(frm(ucNumber.txtNumber.UniqueID).ToString.Trim = "", "0", frm(ucNumber.txtNumber.UniqueID).ToString.Trim), Decimal)
                        found = True
                    End If
                Next
                If found = False Then
                    Dim jc As New TotalPremiAsuransi
                    jc.tahun = thn
                    jc.premitotal = CType(IIf(frm(ucNumber.txtNumber.UniqueID).ToString.Trim = "", "0", frm(ucNumber.txtNumber.UniqueID).ToString.Trim), Decimal)
                    jenisCoverArray.Add(jc)
                End If
                ucNumber.txtNumber.ReadOnly = True
                If ucNumber.Text = "" Then
                    ucNumber.txtNumber.ReadOnly = False
                    lbl.Visible = True
                End If
            End If
            ucNumber.OnClientChange = "hitungpremim(" & r.Item("NilaiCover").ToString & ", this.id)"
        End If
        If (e.Item.FindControl("Penyusutan") IsNot Nothing) Then
            Dim ucNumber As ucNumberFormat = DirectCast(e.Item.FindControl("Penyusutan"), ucNumberFormat)
            Dim lbl As Label = DirectCast(e.Item.FindControl("Premilabel"), Label)
            Dim r As DataRowView = CType(e.Item.DataItem, DataRowView)
            ucNumber.Text = frm(ucNumber.txtNumber.UniqueID)
            If Me.IsPreview = "1" Then
                ucNumber.txtNumber.ReadOnly = True
                If ucNumber.Text = "" Then
                    lbl.Visible = True
                    ucNumber.txtNumber.ReadOnly = False
                End If
            Else
                If frm(ucNumber.txtNumber.UniqueID) = "" Then
                    ucNumber.Text = "100"
                End If
            End If

            ucNumber.OnClientChange = "hitungpremis(" & r.Item("NilaiCover").ToString & ", this.id, " & thn.ToString & ")"


            Dim ncl As Label = DirectCast(e.Item.FindControl("NilaiCoverlabel"), Label)
            ncl.Text = FormatNumber(CDbl(r.Item("NilaiCover").ToString) * CDbl(ucNumber.Text) / 100, 0)
        End If

        Dim dv As DataRowView = CType(e.Item.DataItem, DataRowView)
        If (e.Item.FindControl("NilaiCover") IsNot Nothing) Then
            Dim nc As HiddenField = DirectCast(e.Item.FindControl("NilaiCover"), HiddenField)
            nc.Value = dv.Item("NilaiCover").ToString
        End If
        If (e.Item.FindControl("Tahun") IsNot Nothing) Then
            Dim lbl As Label = DirectCast(e.Item.FindControl("Tahun"), Label)
            lbl.Text = dv.Item("Tahun").ToString
        End If

    End Sub


    Private Function CreateBoundColumn(c As DataColumn) As DataGridColumn
        Select Case c.ColumnName.Trim
            Case "Tahun"
                Dim column As New TemplateColumn
                Dim tc1 As New TemplateColumn()
                tc1.HeaderTemplate = New  _
                   DataGridTemplate(ListItemType.Header, c.ColumnName, "")
                tc1.ItemTemplate = New DataGridTemplate(ListItemType.Item, _
                   c.ColumnName.Replace("_", " "), "label", Nothing, Nothing)
                Return tc1
            Case "JenisCover"
                Dim column As New TemplateColumn
                Dim tc1 As New TemplateColumn()
                tc1.HeaderTemplate = New  _
                   DataGridTemplate(ListItemType.Header, c.ColumnName, "")
                tc1.ItemTemplate = New DataGridTemplate(ListItemType.Item, _
                   c.ColumnName.Replace("_", " "), "dropdownlist", Nothing, listjeniscover)
                Return tc1
            Case "Penyusutan"
                Dim column As New TemplateColumn
                Dim tc1 As New TemplateColumn()
                tc1.HeaderTemplate = New  _
                   DataGridTemplate(ListItemType.Header, c.ColumnName, "")
                tc1.ItemTemplate = New DataGridTemplate(ListItemType.Item, _
                   c.ColumnName.Replace("_", " "), "numeric", 40)
                Return tc1
            Case "RatePremi"
                Dim column As New TemplateColumn
                Dim tc1 As New TemplateColumn()
                tc1.HeaderTemplate = New  _
                   DataGridTemplate(ListItemType.Header, c.ColumnName, "")
                tc1.ItemTemplate = New DataGridTemplate(ListItemType.Item, _
                   c.ColumnName.Replace("_", " "), "numeric", 40)
                Return tc1
            Case "Premi"
                Dim column As New TemplateColumn
                Dim tc1 As New TemplateColumn()
                tc1.HeaderTemplate = New  _
                   DataGridTemplate(ListItemType.Header, c.ColumnName, "")
                tc1.ItemTemplate = New DataGridTemplate(ListItemType.Item, _
                   c.ColumnName.Replace("_", " "), "numeric", 120)
                Return tc1
            Case "NilaiCover"
                Dim column As New TemplateColumn
                Dim tc1 As New TemplateColumn()
                tc1.HeaderTemplate = New  _
                   DataGridTemplate(ListItemType.Header, c.ColumnName, "")
                tc1.ItemTemplate = New DataGridTemplate(ListItemType.Item, _
                   c.ColumnName.Replace("_", " "), "labelhidden", Nothing, Nothing)
                Return tc1
            Case Else
                Dim column As New BoundColumn
                column.DataField = c.ColumnName
                column.HeaderText = c.ColumnName.Replace("_", " ")
                column.DataFormatString = setFormating(c)
                Return column
        End Select

    End Function

    Private Function setFormating(bc As DataColumn) As String
        Dim dataType As String = Nothing
        Select Case bc.DataType.ToString()
            Case "System.Int32"
                dataType = "{0:#,###}"
                Exit Select
            Case "System.Decimal"
                dataType = "{0:#,###}"
                Exit Select
            Case "System.DateTime"
                dataType = "{0:dd-mm-yyyy}"
                Exit Select
            Case "System.String"
                dataType = ""
                Exit Select
            Case Else
                dataType = ""
                Exit Select
        End Select
        Return dataType
    End Function

#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        'Response.Redirect("NewAppInsurance.aspx")
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
#End Region

#Region "Ketika Tombol OK di Klik "
    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim tnr As Integer = CInt(IIf(TxtTenorCredit.Text = "", "0", TxtTenorCredit.Text.Trim))
        If tnr < 1 Then
            ShowMessage(lblMessage, "Jangka waktu asuransi harus diisi", True)
            Exit Sub
        End If

        getInsEntryStep2()
        Me.MaskAssBranchID = cmbMaskAssBranch.SelectedValue
        pnlJenisCover.Visible = True
        PnlDGridPaid.Visible = False
        PnlGrid.Visible = True
        btnSave.Visible = False
        Me.IsPreview = "0"
        lblMessage.Visible = False
        btnOK.Visible = False
        btnReset.Visible = False
        TxtTenorCredit.Text = CStr(tnr)
        TxtTenorCredit.Enabled = False
        Context.Trace.Write("Tenor = " + TxtTenorCredit.Text)
    End Sub
#End Region

#Region "Tombol Cancel Paling Bawah !"
    Private Sub btnCancelPalingBawah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelPalingBawah.Click
        'Response.Redirect("NewAppInsurance.aspx")
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
#End Region


#Region "Ketika Tombol Save Paling Bawah Dipijit "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim errflag As Boolean = False

        Try
            lblMessage.Visible = False

            Dim oCheck As New Parameter.InsuranceCalculation

            With oCheck
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
            End With

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            errflag = True
            err.WriteLog("AppInsuranceAgriculture.aspx", "Save-check dateinsurancedata", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try

        getInsEntryStep2()

        Dim oCustomClass As New Parameter.InsuranceCalculationResult
        Dim oNewAppInsuranceMultiAsset As New Parameter.NewAppInsuranceByCompany
        With oNewAppInsuranceMultiAsset
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With

        Dim assetData As DataTable

        assetData = oController.GetInsuranceEntryStep1ListAgriculture(oNewAppInsuranceMultiAsset).ListData

        'Save Per Asset
        For Each dr As DataRow In assetData.Rows
            Me.AssetNewUsed = dr.Item("UsedNew").ToString.Trim
            Me.ApplicationTypeID = dr.Item("ApplicationType").ToString.Trim
            Me.AssetUsageID = dr.Item("AssetUsage").ToString.Trim
            Me.AssetSeqNo = CType(dr.Item("AssetSeqNo").ToString.Trim, Int16)
            Dim ManufacturingYear As Int16 = CType(dr.Item("ManufacturingYear").ToString.Trim, Int16)

            If saveInsuranceAsset() = True Then
                For Each gr As DataGrid In listGridAsuransi
                    If gr.ID = "grid_" & dr.Item("AssetTypeID").ToString.Trim Then

                        Dim bayartunai As Decimal = 0
                        Dim totalpremi As Decimal = 0
                        Dim paidInsurance As New List(Of String)
                        For Each it As DataGridItem In DgridInsurancePaid.Items
                            Dim r As DataRowView = CType(it.DataItem, DataRowView)
                            If (it.FindControl("DDLPaidByCustDGrid") IsNot Nothing) Then
                                Dim paidcombo As DropDownList = DirectCast(it.FindControl("DDLPaidByCustDGrid"), DropDownList)
                                'Dim hdnpremi As HiddenField = DirectCast(it.FindControl("hdnpremi"), HiddenField)
                                'Dim premi As Decimal = CType(hdnpremi.Value.ToString.Trim, Decimal)
                                paidInsurance.Add(paidcombo.SelectedValue)
                            End If
                        Next

                        ' Save Insurance Detail per year
                        For Each it As DataGridItem In gr.Items
                            Dim customClass As New Parameter.InsuranceCalculation
                            Dim strdatemanufacturing As DateTime = CType("01/01/" & ManufacturingYear.ToString, DateTime)
                            If strdatemanufacturing < CType("01/01/1900", DateTime) Then strdatemanufacturing = CType("01/01/1900", DateTime)
                            Dim rx As DataRowView = CType(it.DataItem, DataRowView)

                            Dim tahun As Int16 = 0
                            Dim jeniscover As String = ""
                            Dim premi As Decimal = 0
                            Dim ratepremi As Decimal = 0
                            Dim nilaicover As Decimal = 0
                            Dim penyusutan As Decimal = 0

                            If (it.FindControl("JenisCover") IsNot Nothing) Then
                                Dim jnc As DropDownList = DirectCast(it.FindControl("JenisCover"), DropDownList)
                                jeniscover = jnc.SelectedValue
                            End If
                            If (it.FindControl("RatePremi") IsNot Nothing) Then
                                Dim ucNumber As ucNumberFormat = DirectCast(it.FindControl("RatePremi"), ucNumberFormat)
                                ratepremi = CType(ucNumber.txtNumber.Text.ToString.Trim, Decimal)
                            End If
                            If (it.FindControl("Penyusutan") IsNot Nothing) Then
                                Dim ucNumber As ucNumberFormat = DirectCast(it.FindControl("Penyusutan"), ucNumberFormat)
                                penyusutan = CType(ucNumber.txtNumber.Text.ToString.Trim, Decimal)
                            End If
                            'If (it.FindControl("NilaiCover") IsNot Nothing) Then
                            '    Dim lbl As HiddenField = DirectCast(it.FindControl("NilaiCover"), HiddenField)
                            '    nilaicover = CType(lbl.Value.ToString.Trim, Decimal)
                            'End If
                            'If (it.FindControl("Premi") IsNot Nothing) Then
                            '    Dim ucNumber As ucNumberFormat = DirectCast(it.FindControl("Premi"), ucNumberFormat)
                            '    premi = CType(ucNumber.txtNumber.Text.ToString.Trim, Decimal)
                            'End If
                            nilaicover = CType(dr.Item("TotalOTR").ToString.Trim, Decimal) * (penyusutan / 100)
                            premi = (ratepremi / 100) * nilaicover

                            If (it.FindControl("Tahun") IsNot Nothing) Then
                                Dim lbl As Label = DirectCast(it.FindControl("Tahun"), Label)
                                tahun = CType(lbl.Text.ToString.Trim, Int16)
                            End If

                            If paidInsurance.Item(tahun - 1) = "PAID" Or paidInsurance.Item(tahun - 1) = "PDC" Then
                                bayartunai = bayartunai + premi
                            End If
                            totalpremi = totalpremi + premi

                            With customClass
                                .MaskAssBranchID = Me.MaskAssBranchID
                                .BranchId = Me.BranchID
                                .ApplicationID = Me.ApplicationID
                                .ApplicationTypeID = Me.ApplicationTypeID
                                .UsageID = Me.AssetUsageID
                                .NewUsed = Me.AssetNewUsed
                                .strConnection = GetConnectionString()
                                .YearNum = tahun
                                .YearNumRate = 0
                                .CoverageType = jeniscover
                                .BolSRCC = Nothing ' CType(SRCC.SelectedItem.Value.Trim, Boolean)
                                .BolFlood = Nothing 'CType(Flood.SelectedItem.Value.Trim, Boolean)
                                .BolRiot = Nothing 'False
                                .BolPA = Nothing 'CType(PA.SelectedItem.Value.Trim, Boolean)
                                .bolPADriver = Nothing 'CType(PADriver.SelectedItem.Value.Trim, Boolean)
                                .AmountCoverage = nilaicover
                                .InsuranceType = Me.InsuranceType
                                .UsageID = Me.AssetUsageID
                                .NewUsed = Me.AssetNewUsed
                                .BranchId = Me.BranchID
                                .InsLength = CType(TxtTenorCredit.Text, Short) 'Me.InsLength
                                .BusinessDate = Me.BusinessDate
                                .DateMonthYearManufacturingYear = strdatemanufacturing
                                .IsPageSourceCompanyCustomer = CBool(IIf(Me.PageSource.Trim = COMPANY_CUSTOMER, True, False))
                                .DiscountToCust = 0
                                .bolTPL = Nothing 'CType(ucTPLOption.SelectedValue, Boolean)
                                If paidInsurance.Item(tahun - 1) = "PAID" Or paidInsurance.Item(tahun - 1) = "PDC" Then
                                    .PaidAmountByCust = premi
                                End If
                                .PaidByCustStatus = paidInsurance.Item(tahun - 1)
                                .InsuranceRate = ratepremi
                                .TPL = 0 'CType(txtNilaiTPL.Text, Double)
                                .AssetSeqNo = Me.AssetSeqNo
                                .MainPremiumToCust = premi
                            End With

                            Try
                                oInsAppController.ProcessAppInsuranceAgricultureDetailSave(customClass)
                            Catch ex As Exception
                                errflag = True
                                ShowMessage(lblMessage, ex.Message, True)
                            End Try
                        Next

                        ' Save Insurance per Asset
                        With oCustomClass
                            .BranchId = Me.BranchID
                            .ApplicationID = Me.ApplicationID
                            .AmountCoverage = Me.AmountCoverage
                            .AdminFeeToCust = CDbl(txtBiayaPolis.Text)
                            .MeteraiFeeToCust = CDbl(txtBiayaMaterai.Text)
                            .DiscToCustAmount = 0 ' CType(Replace(TxtDiscountPremium.Value, ",", ""), Double)
                            .PaidAmountByCust = bayartunai
                            .PremiumBaseForRefundSupp = 0 'CType(TxtPremiumBase.Text, Double)
                            .AccNotes = ""
                            .InsNotes = TxtInsNotes.Text.Trim
                            .InsLength = CType(TxtTenorCredit.Text, Short) 'Me.InsLength
                            .BusinessDate = Me.BusinessDate
                            .strConnection = GetConnectionString()
                            .PremiumToCustAmount = totalpremi
                            .MaskAssBranchID = Me.MaskAssBranchID
                            .InsuranceComBranchID = Me.MaskAssBranchID
                            .AdditionalCapitalized = 0 'CDbl(TxtAdditionalCap.Text)
                            .AdditionalInsurancePremium = 0 'CDbl(TxtAdditionalInsurancePremium.Value)
                            .RefundToSupplier = 0 'CDbl(txtRfnSupplier.Text.Trim)
                            .JenisAksesoris = "" 'txtJenisAksesoris.Text
                            .NilaiAksesoris = 0 'CDbl(txtNilaiAksesoris.Text)
                            .BiayaPolis = CDbl(txtBiayaPolis.Text)
                            .BiayaMaterai = CDbl(txtBiayaMaterai.Text)
                            .AssetSeqNo = Me.AssetSeqNo
                        End With

                        Try
                            oInsAppController.ProcessSaveInsuranceApplicationLastProcess(oCustomClass)
                        Catch ex As Exception
                            errflag = True
                            ShowMessage(lblMessage, ex.Message, True)
                        End Try

                        Exit For
                    End If
                Next
            Else
                errflag = True
            End If
        Next

        If errflag = False Then
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.setLink()
            ShowMessage(lblMessage, "Data saved!", False)
        End If

    End Sub
#End Region



#Region "Validasi Save"
    Public Function ValidasiAngkaPertama(ByVal angkapertama As Decimal, ByVal angkakedua As Decimal) As Boolean
        If angkapertama >= angkakedua Then
            'bila angka pertama lebih besar dari angka kedua maka BENAR
            Return True
        Else
            'bila angka pertama lebih kecil dari angka kedua maka Salah
            Return False
        End If
    End Function
#End Region

    Private Sub DgridInsurancePaid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DgridInsurancePaid.ItemDataBound
        Dim CboPaidByCustDT As New DataTable("PaidByCustMs")
        Dim DDLPaidByCustPilihan As DropDownList
        Dim LblHiddenPaidByCust As Label
        Dim lblPremix As Label
        Dim hdnPremi As HiddenField

        If e.Item.ItemIndex > -1 Then
            lblPremix = CType(e.Item.FindControl("lblPremi"), Label)
            hdnPremi = CType(e.Item.FindControl("hdnPremi"), HiddenField)
            DDLPaidByCustPilihan = CType(e.Item.FindControl("DDLPaidByCustDGrid"), DropDownList)
            LblHiddenPaidByCust = CType(e.Item.FindControl("LblHiddenPaidByCust"), Label)
            CboPaidByCustDT = Me.PaidByCustMs
            LblHiddenPaidByCust.Visible = False


            Dim CbodvPaidByCust As DataView
            CbodvPaidByCust = CboPaidByCustDT.DefaultView
            DDLPaidByCustPilihan.DataSource = CbodvPaidByCust
            DDLPaidByCustPilihan.DataTextField = "Description"
            DDLPaidByCustPilihan.DataValueField = "ID"
            DDLPaidByCustPilihan.DataBind()
            DDLPaidByCustPilihan.ClearSelection()

            Context.Trace.Write("Nilai PaidByCust dari data= '" & LblHiddenPaidByCust.Text.Trim & "'")
            If LblHiddenPaidByCust.Text = "NOTPAID" Then
                DDLPaidByCustPilihan.Items.FindByValue("PDC").Selected = True
            Else
                DDLPaidByCustPilihan.Items.FindByValue(LblHiddenPaidByCust.Text).Selected = True
            End If

            DDLPaidByCustPilihan.Attributes.Add("OnChange", "PilihDibayar(this.value,'" & hdnPremi.ClientID & "')")
        Else
            Dim strsqlQuery2 As String
            If Me.PageSource = COMPANY_CUSTOMER Then
                strsqlQuery2 = "Select rtrim(ltrim(upper(ID))) as ID, description from tblpaidbycuststatus Where ID <> 'NOTPAID' Order by ID"
            Else
                strsqlQuery2 = CommonVariableHelper.SQL_QUERY_PAIDBYCUST_MS_PAGESOURCE_COMPANY_AT_CUST
            End If

            Dim objAdapter2 As New SqlDataAdapter(strsqlQuery2, GetConnectionString)
            objAdapter2.Fill(CboPaidByCustDT)
            Me.PaidByCustMs = CboPaidByCustDT
        End If

    End Sub

    Private Sub imbPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Me.IsPreview = "1"
        getInsEntryStep2()
        If ValidatorPremi() = True Then
            cmbMaskAssBranch.Enabled = False
            btnPreview.Visible = False
            btnSave.Visible = True
            btnCancelPalingBawah.Visible = True
            PnlDGridPaid.Visible = True
            pnlPaidHPP.Visible = True

            Dim tnr As Decimal = CType(IIf(TxtTenorCredit.Text = "", "0", TxtTenorCredit.Text.Trim), Decimal)
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim years As Decimal = Math.Ceiling(tnr / 12)
            dt.Columns.Add("YearInsurance", GetType(Integer))
            dt.Columns.Add("PaidByCust", GetType(String))
            dt.Columns.Add("JenisCover", GetType(String))
            dt.Columns.Add("MainPremiumToCust", GetType(Decimal))
            For i As Integer = 1 To CInt(years)
                dr = dt.NewRow
                dr("YearInsurance") = i
                dr("PaidByCust") = "NOTPAID"
                dr("JenisCover") = jenisCoverArray(i - 1).jeniscover
                dr("MainPremiumToCust") = jenisCoverArray(i - 1).premitotal
                dt.Rows.Add(dr)
            Next

            DgridInsurancePaid.DataSource = dt
            DgridInsurancePaid.DataBind()

            Dim oInsuranceCom As New Parameter.NewAppInsuranceByCompany
            With oInsuranceCom
                .ApplicationID = Me.ApplicationID.Trim
                .strConnection = GetConnectionString()
            End With
            oInsuranceCom.InsuranceComBranchID = cmbMaskAssBranch.SelectedValue
            Dim insCom As DataTable = oController.GetInsurancehComByBranch(oInsuranceCom)
            If insCom.Rows.Count > 0 Then
                Me.NilaiAdminFeeAwal = CDbl(IIf(IsNumeric(insCom.Rows(0).Item("AdminFee")), insCom.Rows(0).Item("AdminFee"), 0))
                Me.NilaiStampDutyFeeAwal = CDbl(IIf(IsNumeric(insCom.Rows(0).Item("StampDutyFee")), insCom.Rows(0).Item("StampDutyFee"), 0))
                txtBiayaPolis.Text = FormatNumber(Me.NilaiAdminFeeAwal, 2)
                txtBiayaMaterai.Text = FormatNumber(Me.NilaiStampDutyFeeAwal, 2)
            End If

            totalPremitext = 0
            For Each j As TotalPremiAsuransi In jenisCoverArray
                totalPremitext = totalPremitext + j.premitotal
            Next
            txtBayarTunai.Text = FormatNumber(CDbl(totalPremitext) + Me.NilaiAdminFeeAwal + Me.NilaiStampDutyFeeAwal, 0)
            lblBayarTunai.Text = FormatNumber(CDbl(totalPremitext) + Me.NilaiAdminFeeAwal + Me.NilaiStampDutyFeeAwal, 0)
        Else
            btnPreview.Visible = True
            Me.IsPreview = "0"
        End If

    End Sub

    Public Function getJenisCoverDescription(ByVal key As String) As String
        For Each a As String() In listjeniscover
            If a(0) = key Then
                Return a(1)
            End If
        Next
        Return ""
    End Function

    Protected Function saveInsuranceAsset() As Boolean
        Dim customClass As New Parameter.InsuranceCalculation

        With customClass
            .MaskAssBranchID = Me.MaskAssBranchID
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .ApplicationTypeID = Me.ApplicationTypeID
            .InsuranceComBranchID = Me.MaskAssBranchID
            .UsageID = Me.AssetUsageID
            .NewUsed = Me.AssetNewUsed
            .AssetSeqNo = Me.AssetSeqNo
            .strConnection = GetConnectionString()
        End With

        Try
            oInsAppController.ProcessNewAppInsuranceByCompanySaveAddTemporary(customClass)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function

End Class


Public Class DataGridTemplate
    Inherits Maxiloan.Webform.WebBased
    Implements ITemplate

    Dim templateType As ListItemType
    Dim columnName As String
    Dim controlType As String
    Dim width As Integer
    Dim dropdownvalue As List(Of String())

    Sub New(ByVal type As ListItemType, ByVal ColName As String, ByVal conType As String, Optional ByVal wid As Integer = 100, Optional ByVal val As List(Of String()) = Nothing)
        templateType = type
        columnName = ColName
        controlType = conType
        width = wid
        dropdownvalue = val
    End Sub

    Sub InstantiateIn(ByVal container As Control) _
       Implements ITemplate.InstantiateIn
        Dim lc As New Literal()
        Select Case templateType
            Case ListItemType.Header
                lc.Text = columnName
                container.Controls.Add(lc)
            Case ListItemType.Item
                Select Case controlType
                    Case "textbox"
                        Dim tb As New TextBox()
                        tb.Text = ""
                        tb.ID = columnName
                        container.Controls.Add(tb)
                    Case "numeric"
                        Dim nb As ucNumberFormat = TryCast(LoadControl("~/webform.UserController/ucNumberFormat.ascx"), ucNumberFormat)
                        nb.ID = columnName
                        nb.Width = width
                        container.Controls.Add(nb)

                        Dim lb As New Label()
                        lb.ID = columnName & "label"
                        lb.CssClass = "validator_general"
                        lb.Text = "Harap isi field ini!!"
                        lb.Visible = False
                        container.Controls.Add(lb)
                    Case "label"
                        Dim lb As New Label()
                        lb.ID = columnName
                        lb.Text = ""
                        container.Controls.Add(lb)
                    Case "labelhidden"
                        Dim lb As New Label()
                        Dim hdn As New HiddenField
                        hdn.ID = columnName
                        lb.ID = columnName & "label"
                        lb.Text = ""
                        container.Controls.Add(lb)
                        container.Controls.Add(hdn)
                    Case "dropdownlist"
                        Dim dd As New DropDownList
                        dd.ID = columnName
                        For Each a As String() In dropdownvalue
                            dd.Items.Add(a(0))
                            dd.Items(dd.Items.Count - 1).Value = a(0)
                            dd.Items(dd.Items.Count - 1).Text = a(1)
                        Next
                        container.Controls.Add(dd)

                        Dim lb As New Label()
                        lb.ID = columnName & "label"
                        lb.CssClass = "validator_general"
                        lb.Text = "Harap isi field ini!!"
                        lb.Visible = False
                        container.Controls.Add(lb)
                End Select

        End Select
    End Sub

End Class




Public Class TotalPremiAsuransi
    Private m_tahun As Integer
    Private m_jeniscover As String
    Private m_premitotal As Double
    Private m_totalcover As Double

    Public Property tahun() As Integer
        Get
            Return m_tahun
        End Get
        Set(value As Integer)
            m_tahun = value
        End Set
    End Property

    Public Property jeniscover() As String
        Get
            Return m_jeniscover
        End Get
        Set(value As String)
            m_jeniscover = value
        End Set
    End Property

    Public Property premitotal As Double
        Get
            Return m_premitotal
        End Get
        Set(value As Double)
            m_premitotal = value
        End Set
    End Property

    Public Property totalcover As Double
        Get
            Return m_totalcover
        End Get
        Set(value As Double)
            m_totalcover = value
        End Set
    End Property

End Class