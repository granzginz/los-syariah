﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class ViewAssetDataAgriculture_002
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents UCAddress As ucAddressCity
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabAgriculture
#End Region

#Region "Constanta"
    Private m_controller As New AssetDataController
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("CustID")
            InitObjects()
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .CustomerID = Me.CustomerID
                .bindData()
                .initControls("OnNewApplication")
            End With
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            Bindgrid()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Asset")
            ucApplicationTab1.setLink()
        End If

    End Sub
#End Region

#Region "Initial Objects"
    Private Sub InitObjects()
        FillCbo(cboInsuredBy, "tblInsuredBy")
        FillCbo(cboPaidBy, "tblPaidBy")
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub Bindgrid()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oDataAttribute As New DataTable
        Dim oDataAssetDoc As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.AppID = Me.ApplicationID
        oAssetData = m_controller.GetViewAssetData(oAssetData)

        If Not oAssetData Is Nothing Then
            oData = oAssetData.ListData
            oDataAttribute = oAssetData.DataAttribute
            oDataAssetDoc = oAssetData.DataAssetdoc
        End If
        If oAssetData.Output <> "" Then
            lblMessage.Text = oAssetData.Output
            Exit Sub
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
        dtgAttribute.DataSource = oDataAttribute.DefaultView
        dtgAttribute.DataBind()

        dtgAssetDoc.DataSource = oDataAssetDoc.DefaultView
        dtgAssetDoc.DataBind()
    End Sub
#End Region

    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)

        txtSupplierName.Text = oRow.Item("SupplierName").ToString
        txtAssetName.Text = oRow.Item("Description").ToString
        ucOTR.Text = FormatNumber(oRow.Item("TotalOTR"), 0)
        cboUangMukaBayar.SelectedIndex = cboUangMukaBayar.Items.IndexOf(cboUangMukaBayar.Items.FindByValue(oRow.Item("UangMukaBayarDi").ToString))
        lblUangMukaBayar.Text = cboUangMukaBayar.SelectedItem.Text
        cboPencairanKe.SelectedIndex = cboPencairanKe.Items.IndexOf(cboPencairanKe.Items.FindByValue(oRow.Item("PencairanKe").ToString))
        lblPencairanKe.Text = cboPencairanKe.SelectedItem.Text
        lblSerial1.Text = oRow.Item("SerialNo1Label").ToString
        'lblSerial2.Text = oRow.Item("SerialNo2Label").ToString
        txtSerial1.Text = oRow.Item("SerialNo1").ToString
        'txtSerial2.Text = oRow.Item("SerialNo2").ToString
      
        cboKondisiAsset.SelectedIndex = cboKondisiAsset.Items.IndexOf(cboKondisiAsset.Items.FindByValue(oRow.Item("KondisiAsset").ToString))
        lblKondisiAsset.Text = cboKondisiAsset.SelectedItem.Text
      
        cboInsuredBy.SelectedIndex = cboInsuredBy.Items.IndexOf(cboInsuredBy.Items.FindByValue(oRow.Item("InsAssetInsuredBy").ToString))
        lblInsuredBy.Text = cboInsuredBy.SelectedItem.Text
        cboPaidBy.SelectedIndex = cboPaidBy.Items.IndexOf(cboPaidBy.Items.FindByValue(oRow.Item("InsAssetPaidBy").ToString))
        lblPaidBy.Text = cboPaidBy.SelectedItem.Text
   
        lblSalesman.Text = oRow.Item("Salesman").ToString.Trim
        lblcmo.Text = oRow.Item("AO").ToString.Trim

        Dim DownPayment As Double = CDbl(oRow.Item("DownPayment"))

        If DownPayment > 0 Then
            txtDP.Text = FormatNumber(DownPayment, 0)
            txtDPPersen.Text = FormatNumber(DownPayment / CDbl(ucOTR.Text) * 100)
        Else
            Dim totaldp As Double = CDbl(oRow.Item("TotalOTR")) * CDbl(txtDPPersen.Text) / 100
            txtDP.Text = FormatNumber(totaldp, 0)
        End If
        lblTotalPembiayaan.Text = FormatNumber(CStr(CDbl(oRow.Item("TotalOTR")) - CDbl(txtDP.Text)), 0)



    End Sub

#Region "FillCbo"

    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
   
#End Region

    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

  

    Private Function pathFile(ByVal Group As String, ByVal ApplicationID As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ApplicationID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

End Class