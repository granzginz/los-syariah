﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppInsuranceAgriculture.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.AppInsuranceAgriculture" %>

<%@ Register Src="../../../../webform.UserController/UcApplicationType.ascx" TagName="UcApplicationType"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTabAgriculture.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucTPLOption.ascx" TagName="ucTPLOption"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucCoverageTypeApk.ascx" TagName="ucCoverageTypeApk"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../Webform.UserController/ucInsuranceBranchName.ascx"
    TagName="ucInsuranceBranchName" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data Asuransi Agriculture</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.min.js"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinApplication(pApplicationID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'Application', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function hitungpremim(tcover, id) {
            var idpremim = id;
            var idpremip = idpremim.replace("Premi", "RatePremi");
            var premip = $('#' + idpremip).val();
            var premim = $('#' + idpremim).val();
            var hasil = (premim / (tcover)) * 100;
            $('#' + idpremip).val(hasil);
        }
        function hitungpremip(tcover, id) {
            var idpremip = id;
            var idpremim = idpremip.replace("RatePremi", "Premi");
            var penyusutan = idpremip.replace("RatePremi", "Penyusutan");
            var premip = $('#' + idpremip).val();
            var premim = $('#' + idpremim).val();
            var peny = $('#' + penyusutan).val();
            var coversusut = 0;
            if (peny > 0) coversusut = (peny / 100) * tcover;
            var hasil = (premip / 100) * coversusut;
            $('#' + idpremim).val(number_format(hasil));
        }
        function hitungpremis(tcover, id, thn) {
            var penyusutan = id;
            var idpremim = penyusutan.replace("Penyusutan", "Premi");
            var idpremip = penyusutan.replace("Penyusutan", "RatePremi");
            var lblp = penyusutan.replace("Penyusutan", "NilaiCoverlabel");
            var hdnp = penyusutan.replace("Penyusutan", "NilaiCover");
            lblp = lblp.replace("_txtNumber_" + thn, "");
            hdnp = hdnp.replace("_txtNumber_" + thn, "");
            var premip = $('#' + idpremip).val();
            var premim = $('#' + idpremim).val();
            var peny = $('#' + penyusutan).val();
            var coversusut = (peny / 100) * tcover;
            var hasil = 0;
            if (premip > 0) hasil = (premip / 100) * coversusut;
            $('#' + hdnp).val(number_format(coversusut));
            $('#' + lblp).html(number_format(coversusut));
            $('#' + idpremim).val(number_format(hasil));
        }
        function hitung(val) {
            var rateAmount = $("#txtAmountRate_txtNumber").val();
            var bpolis = $("#txtBiayaPolis_txtNumber").val();
            var bmaterai = $("#txtBiayaMaterai_txtNumber").val();
            var hasil = (parseInt(rateAmount.replace(/\s*,\s*/g, '')) + parseInt(bmaterai.replace(/\s*,\s*/g, '')) + parseInt(bpolis.replace(/\s*,\s*/g, ''))) - parseInt(val.replace(/\s*,\s*/g, ''));
            $("#<%=lblPremiDiKredit.ClientID %>").html(number_format(hasil));
        }
        function hitungdaripolis(val) {
            var rateAmount = $("#txtAmountRate_txtNumber").val();
            var bayartunai = $("#txtBayarTunai_txtNumber").val();
            var bmaterai = $("#txtBiayaMaterai_txtNumber").val();
            var hasil = (parseInt(rateAmount.replace(/\s*,\s*/g, '')) + parseInt(bmaterai.replace(/\s*,\s*/g, '')) + parseInt(val.replace(/\s*,\s*/g, ''))) - parseInt(bayartunai.replace(/\s*,\s*/g, ''));
            $("#<%=lblPremiDiKredit.ClientID %>").html(number_format(hasil));
        }
        function hitungdarimaterai(val) {
            var rateAmount = $("#txtAmountRate_txtNumber").val();
            var bayartunai = $("#txtBayarTunai_txtNumber").val();
            var bpolis = $("#txtBiayaPolis_txtNumber").val();
            var hasil = (parseInt(rateAmount.replace(/\s*,\s*/g, '')) + parseInt(bpolis.replace(/\s*,\s*/g, '')) + parseInt(val.replace(/\s*,\s*/g, ''))) - parseInt(bayartunai.replace(/\s*,\s*/g, ''));
            $("#<%=lblPremiDiKredit.ClientID %>").html(number_format(hasil));
        }
        function PilihPlusMinus(isi, txtDiscToCust) {
            if(isi != ""){
                $('#' + txtDiscToCust).removeAttr("disabled");
            }else{
                $('#' + txtDiscToCust).attr("disabled","disabled");
                $('#' + txtDiscToCust).val(0);
            }
            hitunganPaidCust()
        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function PilihDibayar(isi, lblClient) {
            var vartype = "";
            $('#DgridInsurancePaid').find('tr').each(function (i, el) {
                if (i > 0) {
                    var $tds = $(this).find('td');
                    var type = $tds[1].getElementsByTagName('select')[0];
                    var bayarcust = $tds[3].getElementsByTagName('input')[0];

                    if (vartype == "ONLOAN") {
                        type.value = "ONLOAN";
                    } else if (bayarcust.id == lblClient) {
                        vartype = type.value;
                    }
                }
            });

            hitunganPaidCust();
        }
        
       
        function hitunganPaidCust() {
            var totalbayarcust = 0;
            var totalKredit = 0;
            var grd = document.getElementById("DgridInsurancePaid");
            var row = grd.rows.length - 1;

            $('#DgridInsurancePaid').find('tr').each(function (i, el) {
                if (i > 0) {
                    var $tds = $(this).find('td');
                    var bayarcust = $tds[3].getElementsByTagName('input')[0];
                    var type = $tds[1].getElementsByTagName('select')[0];
                    var valbayarcust = bayarcust.value;

                    if (type.value == 'ONLOAN') {
                        totalKredit = parseInt(totalKredit) + parseInt(valbayarcust.replace(/\s*,\s*/g, ''));
                    } else {
                        totalbayarcust = parseInt(totalbayarcust) + parseInt(valbayarcust.replace(/\s*,\s*/g, ''));
                    }
                }
            });

            var bpolis = parseInt($("#txtBiayaPolis_txtNumber").val().replace(/\s*,\s*/g, ''));
            var bmaterai = parseInt($("#txtBiayaMaterai_txtNumber").val().replace(/\s*,\s*/g, ''));
            var premi = parseInt($('#<%=lblPremi.ClientID %>').text().replace(/\s*,\s*/g, ''));

            $('#<%=lblPremi.ClientID %>').val(number_format(premi ));

            if (totalbayarcust > 0) {
                $('#<%=lblBayarTunai.ClientID %>').val(number_format(totalbayarcust + bpolis + bmaterai, 0));
                $('#<%=lblPremiDiKredit.ClientID %>').val(number_format(totalKredit, 0));
            } else {
                $('#<%=lblBayarTunai.ClientID %>').val('0');
                $('#<%=lblPremiDiKredit.ClientID %>').val(number_format(totalKredit + bpolis + bmaterai, 0));
            }

        }
    </script>
</head>
<body>
   <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="up1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        ENTRI DATA ASURANSI</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Perusahaan Asuransi
                    </label>
                    <uc4:ucinsurancebranchname runat="server" id="cmbMaskAssBranch"></uc4:ucinsurancebranchname>
                    <asp:Label ID="LblFocusMaskAssID" runat="server"></asp:Label>
                    <asp:Label ID="LblFocusMaskAssBranchName" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Cover Asuransi Dilakukan Oleh</label>
                    <asp:Label ID="LblInsuredBy" runat="server">LblInsuredBy</asp:Label>
                </div>
                <div class="form_right">
                    <label>Premi Dibayar Oleh</label>
                    <asp:Label ID="LblPaidBy" runat="server">LblPaidBy</asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Jangka Waktu Asuransi
                        </label>
                        <asp:TextBox ID="TxtTenorCredit" runat="server" CssClass="smaller_text"></asp:TextBox><label
                            class="label_unit ">Bulan</label>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic"
                            ControlToValidate="TxtTenorCredit" ErrorMessage="Harap isi Jangka Waktu Pembiayaan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgvTenorCredit" runat="server" ControlToValidate="TxtTenorCredit"
                            ErrorMessage="RangeValidator" MaximumValue="120" MinimumValue="0" Type="Integer"
                            Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                    </div>
                    <div class="form_right">
                        <label>
                            Nilai Pertanggungan
                        </label>
                        <asp:Label ID="lblAmountCoverage" runat="server" CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="small button green"></asp:Button>
                <asp:Button ID="btnReset" Text="Back" CssClass="small button gray" runat="server"
                    CausesValidation="False"></asp:Button>
            </div>
            <asp:Panel ID="PnlGrid" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>JENIS COVER ASURANSI</h4>
                    </div>
                </div>
                <div class="form_box">
                        <div class="form_single">
                         <asp:Panel ID="pnlJenisCover" runat="server">
                         </asp:Panel>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnPreview" Text="Proses" CssClass="small button blue" runat="server"
                        CausesValidation="true"></asp:Button>
                </div>
                <asp:Panel ID="PnlDGridPaid" runat="server">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                BAYAR ASURANSI</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <asp:DataGrid ID="DgridInsurancePaid" runat="server" AutoGenerateColumns="False"
                                BorderStyle="None" BorderWidth="0px" CssClass="grid_general">
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="TAHUN">
                                        <HeaderStyle Width="5%"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Tahun" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearInsurance") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DIBAYAR CUST">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="LblHiddenPaidByCust" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.PaidByCust") %>'>
                                            </asp:Label>
                                            <asp:DropDownList ID="DDLPaidByCustDGrid" runat="server" Visible="True">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JENIS COVER">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Lbljeniscover" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JenisCover") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="Lblpremi" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust","{0:###,###,##0.00}") %>'>
                                            </asp:Label>
                                            <asp:HiddenField ID="hdnpremi" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust") %>'> 
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
                <asp:Panel runat="server" ID="pnlPaidHPP">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                DETAIL PREMI CUSTOMER</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <div class="div_label">
                                    <label class="label_auto">
                                        Premi
                                    </label>
                                    <uc4:ucnumberformat id="txtRate" runat="server" visible="false" />
                                </div>
                                <uc4:ucnumberformat id="txtAmountRate" runat="server" visible="false"/>    
                                <asp:label ID="lblPremi" runat="server" CssClass="numberAlign regular_text">0</asp:label>                            
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                     <div class="form_box">
                        <div>
                            <div class="form_left ">
                                <div class="div_label">
                                    <label class="label_auto">
                                        Biaya Polis
                                    </label>
                                </div>
                                    <uc4:ucnumberformat id="txtBiayaPolis" runat="server" visible="true" TextCssClass="numberAlign regular_text" />
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left ">
                                <div class="div_label">
                                    <label class="label_auto">
                                        Biaya Materai
                                    </label>
                                </div>
                                    <uc4:ucnumberformat id="txtBiayaMaterai" runat="server" visible="true" TextCssClass="numberAlign regular_text" />
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left border_sum">
                                <label>
                                    Premi di Bayar
                                </label>
                                
                                <label class="label_calc">
                                    -
                                </label>
                                <uc4:ucnumberformat id="txtBayarTunai" runat="server" visible="false" />
                                <asp:textbox runat="server" Enabled="false" ID="lblBayarTunai" CssClass="numberAlign regular_text">0</asp:textbox>
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    Premi di Pembiayaan
                                </label>
                                <asp:textbox runat="server" Enabled="false" ID="lblPremiDiKredit" CssClass="numberAlign regular_text"></asp:textbox>
                            </div>
                            <div class="form_right">
                                <label>Ditambahkan Ke</label>
                                <asp:DropDownList ID="cboAppandTo" runat="server" Enabled="false">
                                    <asp:ListItem Value="PH">POKOK HUTANG</asp:ListItem>
                                    <%--<asp:ListItem Value="BG">BUNGA</asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_single">
                                <label class="label_general">
                                    Catatan Asuransi
                                </label>
                                <asp:TextBox ID="TxtInsNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
                        </asp:Button>
                        <asp:Button ID="btnCancelPalingBawah" Text="Cancel" CssClass="small button gray"
                            runat="server" CausesValidation="False"></asp:Button>
                    </div>
            </asp:Panel>
               
        </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnOK" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnCancelPalingBawah" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnPreview" EventName="Click" />
            </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
