﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services


Public Class FinancialDataAgriculture_003
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabAgriculture
    Protected WithEvents txtDP As ucNumberFormat
    Protected WithEvents txtNumInst As ucNumberFormat
    Protected WithEvents txtInstAmt As ucNumberFormat
    Protected WithEvents ucSupplierEmployeeGrid1 As ucSupplierEmployeeGrid
    Protected WithEvents txtTidakAngsur As ucNumberFormat
    Protected WithEvents txtDPPersen As ucNumberFormat
    Protected WithEvents txtTglAngsuranI As ucDateCE
    Protected WithEvents txtRefundBunga As ucNumberFormat
    Protected WithEvents lblPendapatanPremiN As ucNumberFormat
    Protected WithEvents lblPendapatanPremi As ucNumberFormat
    Protected WithEvents txtSubsidiBungaDealer As ucNumberFormat
    Protected WithEvents txtSubsidiAngsuran As ucNumberFormat


#Region "Refund"
    Protected WithEvents txtBiayaProvisiN As ucNumberFormat
    Protected WithEvents txtBiayaProvisi As ucNumberFormat
    Protected WithEvents txtRefundBiayaProvisiN As ucNumberFormat
    Protected WithEvents txtRefundBiayaProvisi As ucNumberFormat

    Protected WithEvents txtRefundPremi As ucNumberFormat
    Protected WithEvents txtRefundPremiN As ucNumberFormat

    Protected WithEvents txtRefundAdminN As ucNumberFormat
    Protected WithEvents ucRefundAdmin As ucNumberFormat

#End Region


#End Region


#Region "Constanta"
    Private Const style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim RunRate As Double
    'Dim FlatRate As Decimal
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim m_controller As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController

    Private oRefundInsentifController As New RefundInsentifController
    Private oController As New ApplicationDetailTransactionController

    Private TotalInsGrid As Double
    Private TotalProsentase As Double
    Private TotalBungaNett As Decimal
    Private TotalPencairan As Double
    Private TotalBungaNettAmount As Double

#End Region

#Region "Property"
    Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
    Property Installment() As Double
        Get
            Return CDbl(ViewState("Installment"))
        End Get
        Set(ByVal Value As Double)
            ViewState("Installment") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(ViewState("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(ViewState("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(ViewState("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            ViewState("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(ViewState("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("RejectMinimumIncome") = Value
        End Set
    End Property
    Property First() As String
        Get
            Return ViewState("First").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("First") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return ViewState("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Supplier") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return ViewState("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Property InstallmentScheme() As String
        Get
            Return ViewState("InstallmentScheme").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Property IsIncentiveSupplier() As Boolean
        Get
            Return CType(ViewState("IsIncentiveSupplier"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsIncentiveSupplier") = Value
        End Set
    End Property
    Property RefundInterest As Double
        Get
            Return CType(ViewState("RefundInterest"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundInterest") = value
        End Set
    End Property
    Property AdminFee As Double
        Get
            Return CType(ViewState("AdminFee"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("AdminFee") = value
        End Set
    End Property
    Property Provisi As Double
        Get
            Return CType(ViewState("Provisi"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("Provisi") = value
        End Set
    End Property
    Private Property dtDistribusiNilaiInsentif As DataTable
        Get
            Return CType(ViewState("dtDistribusiNilaiInsentif"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtDistribusiNilaiInsentif") = value
        End Set
    End Property
    Private Property ApplicationDetailTransactionDT As DataTable
        Get
            Return CType(ViewState("ApplicationDetailTransactionDT"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("ApplicationDetailTransactionDT") = value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property NilaiTransaksi() As Decimal
        Get
            Return CType(ViewState("NilaiTransaksi"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("NilaiTransaksi") = Value
        End Set
    End Property
    Private Property EffectiveRate() As Decimal
        Get
            Return CType(ViewState("EffectiveRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffectiveRate") = Value
        End Set
    End Property
    Private Property FlatRate() As Decimal
        Get
            Return CType(ViewState("FlatRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("FlatRate") = Value
        End Set
    End Property
    Private Property rvEffValue As String
        Get
            Return CType(ViewState("rvEffValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffValue") = value
        End Set
    End Property
    Private Property rvEffKey As String
        Get
            Return CType(ViewState("rvEffKey"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffKey") = value
        End Set
    End Property
    Private Property rvEffMinValue As String
        Get
            Return CType(ViewState("rvEffMinValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffMinValue") = value
        End Set
    End Property
    Private Property rvEffMaxValue As String
        Get
            Return CType(ViewState("rvEffMaxValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffMaxValue") = value
        End Set
    End Property
    Private Property rvEffErrorMsg As String
        Get
            Return CType(ViewState("rvEffErrorMsg"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffErrorMsg") = value
        End Set
    End Property
    Property RefundBungaAmount As Double
        Get
            Return CType(ViewState("RefundBungaAmount"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundBungaAmount") = value
        End Set
    End Property
    Property IncomeRatioPercentage() As Decimal
        Get
            Return CDec(ViewState("IncomeRatioPercentage"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("IncomeRatioPercentage") = Value
        End Set
    End Property
    Property CustomerTotalIncome() As Double
        Get
            Return CDbl(ViewState("CustomerTotalIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("CustomerTotalIncome") = Value
        End Set
    End Property
    Public Property OtrNtf() As String
        Get
            Return CStr(ViewState("OtrNtf"))
        End Get
        Set(value As String)
            ViewState("OtrNtf") = value
        End Set
    End Property
#End Region

    Public Property Balon() As DataTable
        Get
            Return CType(ViewState("Balon"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("Balon") = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Session.Remove("balon")
            Me.ApplicationID = Request("ApplicationID")

            Me.EffectiveRate = 0
            Me.rvEffValue = CStr(0)

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()
                .initControls("OnNewApplication")

                Me.CustomerID = .CustomerID
                Me.CustName = .CustomerName
                Me.InterestType = .InterestType.ToString
                Me.InstallmentScheme = .InstallmentScheme.ToString

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If
            End With

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial")
            ucApplicationTab1.setLink()
            txtAmount.Text = "0"
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            initObjects()
            Bindgrid_002()
            GetRefundPremiumToSupplier()

            If ucViewApplication1.isProceed = True Then
                btnSave.Visible = False
            End If

        End If
    End Sub

#Region "Load Financial Data"
    Sub Bindgrid_002()
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_002"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)

        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.ListData
        End If

        Dim strPOEffectiveRate As String
        Dim strPOEffectiveRateBehaviour As String
        Dim strPBEffectiveRate As String
        Dim strPBEffectiveRateBehaviour As String
        Dim strPEffectiveRate As String
        Dim strPEffectiveRateBehaviour As String

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            lblAssetInsurance3.Text = FormatNumber(objrow.Item("InsAssetCapitalized"), 0)
            lblAssetInsurance32.Text = FormatNumber(objrow.Item("PaidAmountByCust"), 0)
            'lblTotalOTR.InnerText = FormatNumber(objrow.Item(3), 2)
            lblOTR.Text = FormatNumber(objrow.Item("OTRKendaraan"), 0)
            lblKaroseri.Text = FormatNumber(objrow.Item("HargaKaroseri"), 0)
            lblTotalOTR.Text = FormatNumber(objrow.Item("TotalOTR"), 0)
            lblOTRSupplier.Text = FormatNumber(objrow.Item(3), 2)
            txtDP.Text = FormatNumber(Math.Round(CDbl(objrow.Item(4)), 0), 0)
            txtDPSupplier.Text = txtDP.Text
            If Not IsDBNull(objrow("EffectiveDate")) Then txtTglAngsuranI.Text = Format(objrow("EffectiveDate"), "dd/MM/yyyy")
            lblNTF.Text = FormatNumber(objrow.Item(5), 0)
            lblNTFSupplier.Text = FormatNumber(objrow.Item(5), 2)
            lblTenor.Text = objrow.Item(8).ToString.Trim
            lblFlatRateTenor.Text = objrow.Item(8).ToString.Trim
            lblFlatRateTenorSupplier.Text = objrow.Item(8).ToString.Trim
            txtNumInst.Text = objrow.Item(8).ToString.Trim
            lblTenorSupplier.Text = objrow.Item(8).ToString.Trim
            txtNumInstSupplier.Text = objrow.Item(8).ToString.Trim

            EffectiveRate = CDec(objrow.Item("EffectiveRate"))
            txtEffectiveRate.Text = Str(EffectiveRate) ' objrow.Item("EffectiveRate").ToString.Trim

            txtEffectiveRateSupplier.Text = objrow.Item(6).ToString.Trim
            lblSupplierRate.Text = objrow.Item(6).ToString.Trim

            lblAdminFee.Text = FormatNumber(objrow.Item("AdminFee"), 0)
            lblOtherFee.Text = FormatNumber(objrow.Item("OtherFee"), 0)

            lblAdminFee2.Text = lblAdminFee.Text
            lblIsAdminFeeCredit.Text = "(" & objrow.Item("IsAdminFeeCredit").ToString & ")"
            lblIsAdminFeeCredit2.Text = lblIsAdminFeeCredit.Text


            ' Bagin Effective Rate dan pengecekannya
            strPOEffectiveRate = objrow.Item("POEffectiveRate").ToString.Trim
            strPOEffectiveRateBehaviour = objrow.Item("POEffectiveRatePatern").ToString.Trim

            strPBEffectiveRate = objrow.Item(13).ToString.Trim
            strPBEffectiveRateBehaviour = objrow.Item(14).ToString.Trim

            strPEffectiveRate = objrow.Item(15).ToString.Trim
            strPEffectiveRateBehaviour = objrow.Item(16).ToString.Trim

            rvEffKey = strPOEffectiveRateBehaviour
            rvEffValue = strPOEffectiveRate
            RVPBEffectiveRate.Enabled = False
            RVPEffectiveRate.Enabled = False
            RVPOEffectiveRate.Enabled = False

            cboPolaAngsuran.SelectedValue = objrow.Item("PolaAngsuran").ToString.Trim

            If Me.InstallmentScheme <> "IR" Then
                txtInstAmt.setBehaviour("InstallmentAmount", objrow)
            End If

            Me.RejectMinimumIncome = CDbl(objrow.Item(11))
            Me.NTFGrossYield = CDbl(objrow.Item(12))
            Me.RefundInterest = CDbl(objrow.Item("RefundInterest"))
            Me.AdminFee = CDbl(objrow.Item("AdminFee"))
            Me.Provisi = CDbl(objrow.Item("ProvisionFee"))
            Me.CustomerTotalIncome = CDbl(objrow.Item("CustomerTotalIncome"))
            Me.IncomeRatioPercentage = CDec(objrow.Item("IncomeInsRatioPercentage"))

            txtGracePeriod2.Text = objrow.Item("GracePeriodLateCharges").ToString
            txtGracePeriod.Text = "0"
            'cboPaymentFreq.Attributes.Add("OnChange", "return NumInst('" & lblTenor.Text & "',this.value, 'txtNumInst_txtNumber');")
            oApplication.BranchId = Replace(Me.sesBranchId, "'", "")
            oApplication.ApplicationID = Me.ApplicationID
            oApplication.strConnection = GetConnectionString()
            oApplication = m_ControllerApp.GetDataAppIDProspect(oApplication)

            If oApplication.Err = "" Then
                If Me.ApplicationID.Trim = oApplication.ApplicationID.Trim Then
                    txtInstAmt.Text = CStr(oApplication.InstallmentAmount)
                    cboFirstInstallment.SelectedIndex = cboFirstInstallment.Items.IndexOf(cboFirstInstallment.Items.FindByValue(oApplication.FirstInstallment))
                Else
                    txtInstAmt.Text = CInt(objrow.Item(9)).ToString.Trim
                End If
            End If

            cboFirstInstallment.SelectedValue = objrow.Item("FirstInstallment").ToString
            txtDP.Text = FormatNumber(objrow.Item("DownPayment"), 0)
            txtTidakAngsur.Text = FormatNumber(objrow.Item("InstallmentUnpaid"), 0)
            txtFlatRate.Text = FormatNumber(objrow.Item("FlatRate"), 2)

            If CDbl(txtFlatRate.Text.Trim) = 0 Then
                cboFirstInstallment.SelectedValue = "AD"
                Dim oClass As New Parameter.FinancialData
                With oClass
                    .Tenor = CInt(txtNumInst.Text)
                    .EffectiveRate = CDec(objrow.Item("POEffectiveRate")) / 100
                    .FirstInstallment = cboFirstInstallment.SelectedValue
                End With
                Me.FlatRate = RateConvertion.cEffToFlat(oClass)
                txtFlatRate.Text = FormatNumber(Me.FlatRate, 2)
            End If

            Dim pgross, pinsnet, pselisih, ppolis, potherfee As Double

            lblPremiAsuransiGross.Text = FormatNumber(CDbl(objrow.Item("PremiGross")) + CDbl(objrow.Item("BiayaPolis")), 0)
            pgross = CDbl(IIf(IsNumeric(lblPremiAsuransiGross.Text), lblPremiAsuransiGross.Text, 0))
            ppolis = CDbl(IIf(IsNumeric(objrow.Item("BiayaPolis")), objrow.Item("BiayaPolis"), 0))
            potherfee = CDbl(IIf(IsNumeric(objrow.Item("OtherFee")), objrow.Item("OtherFee"), 0))
            pselisih = pgross - pinsnet
            lblBiayaFidusia.Text = FormatNumber(CDec(objrow("FiduciaFee")), 0)
            lblInsAmountToNPV.Text = FormatNumber(objrow("InsAmountToNPV"), 0)
            lblTotalBunga.Text = FormatNumber(CDbl(objrow("TotalBunga")), 0)
            lblNilaiKontrak.Text = FormatNumber(CDbl(lblNTF.Text) + CDbl(lblTotalBunga.Text), 0)

            CalculateDPPersen()
            txtInstAmt.Text = FormatNumber(CDec(objrow("InstallmentAmount")), 0)

            lblAngsuranPertama.Text = IIf(cboFirstInstallment.SelectedValue = "AD", txtInstAmt.Text, "0").ToString
            lblBiayaProvisi.Text = FormatNumber(objrow("ProvisionFee"), 0)

            txtBiayaProvisiN.Text = FormatNumber(objrow("ProvisionFee"), 0)
            txtBiayaProvisi.Text = FormatNumber(objrow("ProvisiPercent"), 2)
            lblBiayaProvisi.Text = FormatNumber(objrow("ProvisionFee"), 0)
            Me.OtrNtf = objrow("OtrNtf").ToString.Trim

            If CInt(objrow("refundAdminPercent")) > 0 Then
                '   ucRefundAdmin.Text = FormatNumber(objrow("refundAdminPercent").ToString, 0)
            End If
            If CInt(objrow("refundAdmin")) > 0 Then
                'txtRefundAdminN.Text = FormatNumber(objrow("refundAdmin").ToString, 2)
            End If

            'If CInt(objrow("refundBunga")) > 0 Then
            '    txtRefundBungaN.Text = FormatNumber(objrow("refundBunga"), 0)
            'End If

            If objrow("GabungRefundSupplier").ToString = "True" Then
                rdoGabungRefundSupplier.SelectedValue = "1"
            Else
                rdoGabungRefundSupplier.SelectedValue = "0"
            End If

            If FormatNumber(CInt(lblNTF.Text)) <> FormatNumber(objrow.Item("OldNTF")) Then
                CalculateInstallment()
            End If

            CalculateTotalPembiayaan()
            CalculateTotalBayarPertama()

        End If
    End Sub

    Sub Behaviour(ByVal textbox As TextBox, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As Button)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MinimumValue = textbox.Text.Trim
                rv.MaximumValue = "999999999999999"
                rv.ErrorMessage = "Input Harus  >= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MaximumValue = textbox.Text.Trim
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input Harus <= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "999999999999999"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Harap isi dengan Angka"
        End Select
    End Sub


    Function checkBehaviourPersenOnSave() As Boolean
        Select Case Me.rvEffKey
            Case "L"
                rvEffMinValue = Me.rvEffValue
                rvEffMaxValue = Me.rvEffValue
                rvEffErrorMsg = "Bunga efektif harus = " & FormatNumber(Me.rvEffValue, 2)

            Case "N"
                rvEffMinValue = Me.rvEffValue
                rvEffMaxValue = "100"
                rvEffErrorMsg = "Bunga efektif harus >= " & FormatNumber(Me.rvEffValue, 2) & " dan <= 100 (dari Produk Jual) "

            Case "X"
                rvEffMinValue = "0"
                rvEffMaxValue = Me.rvEffValue
                rvEffErrorMsg = "Bunga efektif harus >=0 dan <= " & FormatNumber(Me.rvEffValue, 2) & " (dari Produk Jual) "

            Case "D"
                rvEffMinValue = "0"
                rvEffMaxValue = "100"
                rvEffErrorMsg = "Bunga efektif harus >= 0 dan <= 100"
        End Select


        If Not (CDec(txtEffectiveRate.Text) >= CDec(rvEffMinValue) And CDec(txtEffectiveRate.Text) <= CDec(rvEffMaxValue)) Then
            ShowMessage(lblMessage, rvEffErrorMsg, True)
            Return False
        Else
            Return True
        End If
    End Function

    Sub BehaviourPersen(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As Button)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                rv.MinimumValue = value
                rv.MaximumValue = "100"
                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                rv.MaximumValue = value
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >=0 dan <= " & value

                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "100"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >= 0 dan <= 100"
        End Select
    End Sub

    Sub BehaviourPersenGY(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As Button)
        Select Case Key
            Case "L"
                imb.Visible = False
                lblMessage.Text = ""
                lblMessage.Visible = False
            Case "N"
                If CDbl(textbox.Text.Trim) < CDbl(value) Then
                    If Flag = "Product" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk) ", True)
                        Me.Status = True
                    ElseIf Flag = "ProductBranch" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) ", True)
                        Me.Status = True
                    ElseIf Flag = "ProductOffering" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) ", True)
                        Me.Status = True
                    End If
                End If
                imb.Visible = True
            Case "X"
                If CDbl(textbox.Text.Trim) > CDbl(value) Then
                    If Flag = "Product" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk) ", True)
                        Me.Status = True
                    ElseIf Flag = "ProductBranch" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) ", True)
                        Me.Status = True
                    ElseIf Flag = "ProductOffering" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) ", True)
                        Me.Status = True
                    End If
                End If

                imb.Visible = True
            Case "D"
                rv.Enabled = True
                imb.Visible = True
                If (CDbl(textbox.Text.Trim) < 0 And CDbl(textbox.Text.Trim) > 100) Then
                    ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= 100!", True)
                    Me.Status = True
                End If
        End Select
    End Sub
#End Region

    Protected Sub CalculateDP() Handles txtDPPersen.TextChanged
        txtDP.Text = FormatNumber(CDbl(lblTotalOTR.Text) * CDec(IIf(IsNumeric(txtDPPersen.Text), txtDPPersen.Text, 0)) / 100, 0)
        lblUangMuka.Text = FormatNumber(txtDP.Text, 0)
        'CalculateInstallment()

        CalculateTotalPembiayaan()

    End Sub

    Protected Sub CalculateDPPersen() Handles txtDP.TextChanged
        txtDPPersen.Text = FormatNumber(CDec(IIf(IsNumeric(txtDP.Text), txtDP.Text, 0)) / CDbl(lblTotalOTR.Text) * 100, 2)
        'CalculateInstallment()
        lblUangMuka.Text = FormatNumber(txtDP.Text, 0)
        CalculateTotalPembiayaan()
    End Sub

#Region "Calculate Installment"

    Protected Sub FrekuensiChanged() Handles cboPaymentFreq.SelectedIndexChanged
        txtNumInst.Text = CStr(GetNumInst(CInt(cboPaymentFreq.SelectedValue), CInt(lblTenor.Text)))
        CalculateInstallment()
    End Sub

    Protected Sub CalculateInstallment() Handles txtFlatRate.TextChanged, txtDP.TextChanged, txtNumInst.TextChanged, txtDPPersen.TextChanged, cboFirstInstallment.SelectedIndexChanged
        Dim oEntities As New Parameter.FinancialData
        Dim Inst As Double
        'Dim EffRateToUse As Double = EffectiveRate        
        lblMessage.Visible = False

        If cboPolaAngsuran.SelectedValue = "NOTARIIL" Then
            Exit Sub
        End If

        CalculateTotalPembiayaan()

        If Me.StepUpDownType = "RL" Or Me.StepUpDownType = "LS" Then
            If CInt(txtCummulative.Text) > CInt(txtNumInst.Text) Then
                ShowMessage(lblMessage, "Kumulatif  < Jangka waktu Angsuran", True)
                Exit Sub
            End If
        End If

        'If EffRateToUse <= 0 Then
        '    ShowMessage(lblMessage, "Bunga Efective Harus > 0!", True)
        '    Exit Sub
        'End If

        Me.FlatRate = CDec(txtFlatRate.Text)

        If FlatRate < 0 Then
            ShowMessage(lblMessage, "Bunga harus >= 0!", True)
            Exit Sub
        Else
            Dim oClass As New Parameter.FinancialData

            With oClass
                .Tenor = CInt(lblTenor.Text)
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .FlatRate = Me.FlatRate / 100
            End With

            EffectiveRate = Math.Round(RateConvertion.cFlatToEff(oClass), 4)
            txtEffectiveRate.Text = Str(EffectiveRate) 'Str(CDec(Int(CDbl(EffectiveRate) * 100) / 100)) 'FormatNumber(EffectiveRate, 2)

        End If

        'btnRecalcEffRate.Enabled = True
        'btnRecalcEffRate.Visible = True


        If Me.InstallmentScheme = "RF" Then


            Try
                'Inst = InstAmt(EffRateToUse)
                Inst = InstAmt(EffectiveRate)
                txtInstAmt.Text = FormatNumber(Math.Round(Inst, 0), 0)
                lblAngsuranPertama.Text = IIf(cboFirstInstallment.SelectedValue = "AD", txtInstAmt.Text, "0").ToString
                'CountFlatRate()
                CalculateTotalBunga()

                'lblAngsuranTidakSama.Text = Math.Abs(CDbl(txtInstAmt.Text) - CDbl(txtInstAmtSupplier.Text)).ToString
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
                Exit Sub
            End Try


        ElseIf Me.InstallmentScheme = "IR" Then
            BuatTabelInstallment()
            btnSave.Enabled = True
            btnSave.Visible = True
            txtInstAmt.Enabled = True
        ElseIf Me.InstallmentScheme = "ST" Then
            If Me.StepUpDownType = "NM" Then
                BuatTabelInstallment()
            ElseIf Me.StepUpDownType = "RL" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.CummulativeStart = CInt(txtCummulative.Text)
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                'Entities.EffectiveRate = EffRateToUse
                Entities.EffectiveRate = EffectiveRate
                Entities = m_controller.GetInstAmtStepUpDownRegLeasing(Entities)
                txtInstAmt.Text = FormatNumber(Math.Round(Entities.InstallmentAmount, 0), 0)
                btnSave.Enabled = True
                btnSave.Visible = True
                txtInstAmt.Enabled = True
            Else
                BuatTabelInstallment()
            End If
        Else
            Try
                With oEntities
                    .NTF = CDbl(lblNTF.Text)
                    .NumOfInstallment = CInt(txtNumInst.Text)
                    .GracePeriod = CInt(txtGracePeriod.Text.Trim)
                End With
                oEntities = m_controller.GetPrincipleAmount(oEntities)
                Me.PrincipleAmount = oEntities.Principal
            Catch ex As Exception
                ShowMessage(lblMessage, "Angsuran Pokok Salah", True)
            End Try
        End If


        CalculateTotalBayarPertama()
    End Sub



    Private Function IsValidValidation(ByVal textbox As TextBox, ByVal CompareText As ucNumberFormat, ByVal Message As String) As Boolean
        If textbox.Text.Trim = "" Then
            ShowMessage(lblMessage, Message & " Harap diisi", True)
            Return False
        ElseIf CDbl(textbox.Text) = 0 Then
            ShowMessage(lblMessage, Message & " Harap isi > 0 ", True)
            Return False
        ElseIf CDbl(textbox.Text) > CDbl(CompareText.Text) Then
            ShowMessage(lblMessage, Message & " Harap isi < Jangka Waktu Angsuran", True)
            Return False
        End If
        Return True
    End Function




    Private Sub RecalculateEffRate() Handles txtInstAmt.TextChanged

        If cboPolaAngsuran.SelectedValue = "NOTARIIL" Then
            Exit Sub
        End If

        If CInt(txtInstAmt.Text) <= 0 Then
            ShowMessage(lblMessage, "Jumlah Angsuran harus > 0", True)
            Exit Sub
        End If



        Dim Eff As Double
        Dim Supp As Double

        Try
            If Me.InstallmentScheme = "IR" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue

                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Eff = m_controller.GetNewEffRateIRR(Entities)
                txtEffectiveRate.Text = Eff.ToString.Trim
                'count SupplierRate
                Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue

                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Supp = m_controller.GetNewEffRateIRR(Entities)
                lblSupplierRate.Text = Supp.ToString.Trim
            ElseIf Me.InstallmentScheme = "ST" Then
                If Me.StepUpDownType = "NM" Then
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme

                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Eff = m_controller.GetNewEffRateIRR(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme

                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Supp = m_controller.GetNewEffRateIRR(Entities)
                    lblSupplierRate.Text = Supp.ToString.Trim

                ElseIf Me.StepUpDownType = "RL" Then
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateRegLeasing(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Supp = m_controller.GetNewEffRateRegLeasing(Entities)
                    lblSupplierRate.Text = Supp.ToString.Trim

                Else
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme

                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateLeasing(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme

                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Supp = m_controller.GetNewEffRateLeasing(Entities)
                    lblSupplierRate.Text = Supp.ToString.Trim

                End If
            Else
                'count EffectiveRate

                Eff = CountRate(CDbl(lblNTF.Text))
                txtEffectiveRate.Text = Eff.ToString.Trim
                'CaclFlatRate()
                '' cara ambil eff nya samain kaya yang di lostfocus txteffrate
                'Dim oClass2 As New Parameter.FinancialData

                'With oClass2
                '    .Tenor = CInt(txtNumInst.Text)
                '    .FirstInstallment = cboFirstInstallment.SelectedValue
                '    .FlatRate = Me.FlatRate / 100
                'End With

                'EffectiveRate = RateConvertion.cFlatToEff(oClass2)

                'txtEffectiveRate.Text = EffectiveRate.ToString.Trim
                '' end off



                'count SupplierRate
                'Supp = CountRate(CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim))
                'lblSupplierRate.Text = CStr(CDbl(Supp))
                lblSupplierRate.Text = txtEffectiveRate.Text


                'CountFlatRate()

            End If

            'format txtboxeffectiveRate
            EffectiveRate = Math.Round(CDec(txtEffectiveRate.Text), 4)
            txtEffectiveRate.Text = Str(EffectiveRate)

            'hitung flat rate
            Dim oClass As New Parameter.FinancialData

            With oClass
                .Tenor = CInt(lblTenor.Text)
                .EffectiveRate = Me.EffectiveRate / 100
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .NumOfInstallment = CInt(txtNumInst.Text)
                .InstallmentAmount = CDbl(txtInstAmt.Text)
            End With

            Me.FlatRate = RateConvertion.cEffToFlat(oClass)
            txtFlatRate.Text = FormatNumber(Me.FlatRate, 2)

            CalculateTotalBunga()
            lblAngsuranPertama.Text = IIf(cboFirstInstallment.SelectedValue = "AD", txtInstAmt.Text, "0").ToString
            CalculateTotalBayarPertama()

        Catch ex As Exception
            ShowMessage(lblMessage, "Jumlah Angsuran Salah", True)
        End Try
    End Sub

    Function cEffToFlat() As Double
        
    End Function

    Function InstAmtFlat(ByVal FlatRate As Decimal) As Double
        RunRate = FlatRate / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100)
        If cboFirstInstallment.SelectedValue = "AD" Then
            Return m_controller.GetInstAmtAdvFlat(FlatRate, m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)), CInt(txtNumInst.Text), CDbl(lblNTF.Text))
        Else
            Return 0
        End If
    End Function

    Function InstAmt(ByVal Rate As Double) As Double
        Dim ReturnVal As Double

        RunRate = (Rate / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = Math.Round(m_controller.GetInstAmtAdv(RunRate, CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInst.Text) Then
                    ShowMessage(lblMessage, "Grace Period harus lebih kecil dari Jangka Waktu Angsuran", True)

                    Return 0
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInst.Text) - CInt(txtGracePeriod.Text.Trim), _
                    CType(lblNTF.Text, Double)), 0)
                ElseIf cboGracePeriod.SelectedValue = "R" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArrRO(CDbl(txtEffectiveRate.Text), CInt(txtNumInst.Text), _
                    RunRate, CType(lblNTF.Text, Double), _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim)), 0)
                ElseIf CInt(txtGracePeriod.Text) = 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                    CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
                End If
            Else
                ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
            End If
        End If
        Return ReturnVal
    End Function

    Function InstAmtSupplier(ByVal Rate As Double) As Double
        Dim ReturnVal As Double

        RunRate = (Rate / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = Math.Round(m_controller.GetInstAmtAdv(RunRate, CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInstSupplier.Text), CType(lblNTFSupplier.Text, Double)), 0)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInstSupplier.Text) Then
                    ShowMessage(lblMessage, "Grace Period harus lebih kecil dari Jangka Waktu Angsuran", True)
                    Return 0
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInstSupplier.Text) - CInt(txtGracePeriod.Text.Trim), _
                    CType(lblNTFSupplier.Text, Double)), 0)
                ElseIf cboGracePeriod.SelectedValue = "R" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArrRO(CDbl(txtEffectiveRateSupplier.Text), CInt(txtNumInstSupplier.Text), _
                    RunRate, CType(lblNTFSupplier.Text, Double), _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim)), 0)
                ElseIf CInt(txtGracePeriod.Text) = 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                    CInt(txtNumInstSupplier.Text), CType(lblNTFSupplier.Text, Double)), 0)
                End If
            Else
                ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                CInt(txtNumInstSupplier.Text), CType(lblNTFSupplier.Text, Double)), 0)
            End If
        End If
        Return ReturnVal
    End Function

    Function CountRate(ByVal NTF As Double) As Double
        Dim ReturnVal As Double
        Dim intJmlInst As Integer

        If Me.InstallmentScheme = "ST" And Me.StepUpDownType = "RL" Then
            intJmlInst = CInt(txtCummulative.Text)
        Else
            intJmlInst = CInt(txtNumInst.Text)
        End If

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = m_controller.GetEffectiveRateAdv(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
            Context.Trace.Write("Bila Angsuran Pertama sama dengan AD =" & ReturnVal)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInst.Text) Then
                    ShowMessage(lblMessage, "Grace Period harus lebih kecil dari Jangka Waktu Angsuran", True)
                    Return 0
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst - CInt(txtGracePeriod.Text.Trim), CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
                ElseIf cboGracePeriod.SelectedValue = "R" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = m_controller.GetEffectiveRateRO(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim))
                Else
                    ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
                End If
            Else
                ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
            End If
        End If
        Return ReturnVal
    End Function

    Function CountRateSupplier(ByVal NTF As Double) As Double
        Dim ReturnVal As Double
        Dim intJmlInst As Integer = CInt(txtNumInst.Text)

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = m_controller.GetEffectiveRateAdv(intJmlInst, CDbl(txtInstAmtSupplier.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInstSupplier.Text) Then
                    ShowMessage(lblMessage, "Grace Period harus lebih kecil dari Jangka Waktu Angsuran", True)
                    Return 0
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" Then
                    ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst - CInt(txtGracePeriod.Text.Trim), CDbl(txtInstAmtSupplier.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
                Else
                    ReturnVal = m_controller.GetEffectiveRateRO(intJmlInst, CDbl(txtInstAmtSupplier.Text), NTF, CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim))
                End If
            Else
                ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst, CDbl(txtInstAmtSupplier.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
            End If
        End If

        Return ReturnVal
    End Function

    Private Sub CountFlatRate()
        lblFlatRate.Text = CDec(100 * ((CDbl(cNum(txtInstAmt.Text)) * CDbl(lblTenor.Text)) - CDbl(lblNTF.Text.Trim)) / CDbl(lblNTF.Text.Trim)).ToString
        lblFlatRateFormatted.Text = FormatNumber(lblFlatRate.Text, 2)
        'lblFlatRate.Visible = True
    End Sub

    Private Sub FlatRateForSupplier()
        Dim FlateRate As Double

        FlateRate = 100 * ((CDbl(txtInstAmtSupplier.Text) * CDbl(lblTenorSupplier.Text)) - CDbl(lblNTFSupplier.Text.Trim)) / CDbl(lblNTFSupplier.Text.Trim)
        lblFlatRateSupplier.Text = FlateRate.ToString
        lblFlatRateSupplier.Visible = True
    End Sub

    Private Sub CalculateTotalBunga()
        'lblTotalBunga.Text = FormatNumber((CDbl(lblNTF.Text) * CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * Me.FlatRate / 100) / (CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue))), 0)
        'Math.Ceiling(Math.Round((CDbl(lblNTF.Text) * CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * Me.FlatRate / 100), 0) / 1000) * 1000
        If cboPolaAngsuran.SelectedValue.ToString <> "NOTARIIL" Then
            'lblTotalBunga.Text = FormatNumber(Math.Ceiling(Math.Round((CDbl(lblNTF.Text) * CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * Me.FlatRate / 100), 0) / 1000) * 1000, 0)
            'lblTotalBunga.Text = FormatNumber((CDbl(lblNTF.Text) * CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * Me.FlatRate / 100), 0)
            lblTotalBunga.Text = FormatNumber((CDbl(txtInstAmt.Text) * CInt(txtNumInst.Text)) - CDbl(lblNTF.Text), 0)
            CalculateNilaiKontrak()
        End If

        txtBiayaProvisiN.Text = FormatNumber(CDbl(lblNilaiKontrak.Text) * (CDbl(txtBiayaProvisi.Text) / 100), 0)
        lblBiayaProvisi.Text = txtBiayaProvisiN.Text

        'Dim rasioAngs As Decimal = CDec(CDbl(txtInstAmt.Text) / Me.CustomerTotalIncome * 100)

        'If rasioAngs > Me.IncomeRatioPercentage Then
        '    ShowMessage(lblMessage, "Ratio angsuran terhadap penghasilan " & FormatNumber(rasioAngs, 2) & "%, harus lebih kecil dari " & FormatNumber(Me.IncomeRatioPercentage, 2) & "%", True)
        'End If

    End Sub

    Private Sub CalculateNilaiKontrak()
        lblNilaiKontrak.Text = FormatNumber(CDbl(lblNTF.Text) + CDbl(lblTotalBunga.Text) + CDbl(lblInsAmountToNPV.Text), 0)
    End Sub

    Private Sub CalculateTotalBungaSupplier()
        lblNilaiKontrakSupplier.Text = FormatNumber(CDbl(lblNTFSupplier.Text))
    End Sub

#End Region

#Region "Check_GrossYeildRate"
    Private Sub Check_GrossYeildRate()
        Dim oFinancialData As New Parameter.FinancialData
        Dim dtEntity As New DataTable

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_003"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)
        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.ListData
        End If

        Dim strPOGrossYieldRate As String
        Dim strPOGrossYieldRateBehaviour As String
        Dim strPBGrossYieldRate As String
        Dim strPBGrossYieldRateBehaviour As String
        Dim strPGrossYieldRate As String
        Dim strPGrossYieldRateBehaviour As String

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            ' Bagin GrossYieldRate dan pengecekannya
            txtGrossYieldRate.Text = CType(GetGrossYield(), String)
            strPGrossYieldRate = objrow.Item(17).ToString.Trim
            strPGrossYieldRateBehaviour = objrow.Item(18).ToString.Trim

            strPBGrossYieldRate = objrow.Item(19).ToString.Trim
            strPBGrossYieldRateBehaviour = objrow.Item(20).ToString.Trim

            strPOGrossYieldRate = objrow.Item(21).ToString.Trim
            strPOGrossYieldRateBehaviour = objrow.Item(22).ToString.Trim


            'TODO Enable gross yield dan pelajari rumusnya
            'BehaviourPersenGY("ProductOffering", txtGrossYieldRate, strPOGrossYieldRate, RVPOEffectiveRate, strPOGrossYieldRateBehaviour, btnCalcInst)

            'If Me.Status = True Then
            '    Exit Sub
            'End If

            'BehaviourPersenGY("ProductBranch", txtGrossYieldRate, strPBGrossYieldRate, RVPBEffectiveRate, strPBGrossYieldRateBehaviour, btnCalcInst)
            'If Me.Status = True Then
            '    Exit Sub
            'End If

            'BehaviourPersenGY("Product", txtGrossYieldRate, strPGrossYieldRate, RVPEffectiveRate, strPGrossYieldRateBehaviour, btnCalcInst)
            'If Me.Status = True Then
            '    Exit Sub
            'End If
        End If
    End Sub
#End Region


#Region "Save"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            'Dim FlatRate As Double
            Dim dtEntity As New DataTable
            Dim oFinancialData As New Parameter.FinancialData
            Dim effRateToUse As Double = EffectiveRate
            Dim ds As New DataSet

            If txtEffectiveRate.Text.Trim = "" Then
                ShowMessage(lblMessage, "Harap Klik Tombol Recalculate Effective Rate", True)
                Exit Sub
            End If

            Me.Status = False

            If Not checkBehaviourPersenOnSave() Then Exit Sub

            If Me.NTFGrossYield <= 0 Then
                ShowMessage(lblMessage, "Simpan data Gagal. NTF untuk Gross Yield <= 0, NTF Gross yield sekarang adalah " & Me.NTFGrossYield, True)
                Exit Sub
            End If

            If cboPolaAngsuran.SelectedValue = "NOTARIIL" Then

                BuatTable()
                If Not IsNothing(Session("balon")) Then
                    Me.Balon = CType(Session("balon"), DataTable)
                End If

                oFinancialData.MydataSet = InstallmentBalon()

            Else


                If Me.InstallmentScheme = "RF" Then
                    'If CDbl(txtInstAmt.Text) <> InstAmt(effRateToUse) Then
                    'If CDbl(txtInstAmt.Text) <> Math.Round(InstAmt(effRateToUse), 0) Then
                    '    ShowMessage(lblMessage, "Jumlah Angsuran dan Bunga Effective tidak syncronized. Harap Klik tombol Calculate Installment Amount untuk menghitung Bunga Effective", True)
                    '    Exit Sub
                    'End If
                End If

                If Me.InstallmentScheme = "RF" Then
                    Check_GrossYeildRate()
                    If Me.Status = True Then
                        Exit Sub
                    End If

                    oFinancialData.DiffRate = CDbl(txtAmount.Text.Trim)
                    oFinancialData.GrossYield = GetGrossYield()

                    If HitungUlangSisa Then
                        dblInterestTotal = Math.Round(dblInterestTotal + SisaPrincipal, 2)
                        oFinancialData.InterestTotal = dblInterestTotal
                    End If
                    If HitungUlangKurang Then
                        dblInterestTotal = Math.Round(dblInterestTotal - KurangPrincipal, 2)
                        oFinancialData.InterestTotal = dblInterestTotal
                    End If
                End If

            End If

            If dblInterestTotal < Me.RejectMinimumIncome Then
                ShowMessage(lblMessage, "Total Bunga < Reject Minimum Income, tidak dapat diproses!", True)
            Else
                If Me.InstallmentScheme = "RF" Then
                    'FlatRate = GetFlatRate(dblInterestTotal, CDbl(lblNTF.Text), CInt(cboPaymentFreq.SelectedValue), CInt(lblTenor.Text))
                    oFinancialData.FlatRate = FlatRate
                    oFinancialData.InstallmentAmount = CDbl(txtInstAmt.Text)
                    oFinancialData.NumOfInstallment = CInt(txtNumInst.Text)
                    oFinancialData.FloatingPeriod = CType(IIf(Me.InterestType = "FL", cboPaymentFreq.SelectedValue.ToString, ""), String)

                    If cboFirstInstallment.SelectedValue = "AD" Then
                        ds = MakeAmortTable1(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue))
                    ElseIf cboFirstInstallment.SelectedValue = "AR" Then
                        If (txtGracePeriod.Text.Trim = "0" Or txtGracePeriod.Text.Trim = "") Then
                            ds = MakeAmortTable2(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue))
                        ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "I" Then
                            ds = MakeAmortTable6(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                        ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "R" Then
                            ds = MakeAmortTable3(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                        End If
                    End If

                ElseIf Me.InstallmentScheme = "IR" Then
                    oFinancialData.IsSave = 1
                ElseIf Me.InstallmentScheme = "ST" Then
                    If Me.StepUpDownType = "NM" Then
                        ds = GetEntryInstallmentStepUpDown()
                        oFinancialData.MydataSet = ds
                    ElseIf Me.StepUpDownType = "RL" Then
                        Entities.NTF = CDbl(lblNTF.Text)
                        Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                        Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                        Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                        Entities.CummulativeStart = CInt(txtCummulative.Text)
                        Entities.NumOfInstallment = CInt(txtNumInst.Text)
                        Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                        Entities = m_controller.GetStepUpDownRegLeasingTBL(Entities)
                        ds = Entities.MydataSet
                        oFinancialData.MydataSet = Entities.MydataSet
                    Else
                        ds = GetInstallmentAmountStepUpDownLeasingTBL()
                        oFinancialData.MydataSet = ds
                    End If

                End If

                Me.DiffRate = CDbl(txtAmount.Text)

                With oFinancialData
                    .BusinessDate = Me.BusinessDate
                    .BranchId = Replace(Me.sesBranchId, "'", "")
                    .AppID = Me.ApplicationID
                    .ApplicationID = Me.ApplicationID
                    .NTF = CDbl(lblNTF.Text)
                    .EffectiveRate = effRateToUse
                    .RateIRR = 0
                    .SupplierRate = .EffectiveRate
                    .PaymentFrequency = cboPaymentFreq.SelectedValue
                    .FirstInstallment = cboFirstInstallment.SelectedValue
                    .Tenor = CInt(lblTenor.Text)
                    .GracePeriod = CInt(IIf(txtGracePeriod2.Text.Trim = "", "0", txtGracePeriod2.Text.Trim))
                    .GracePeriodType = cboGracePeriod.SelectedValue
                    .BusDate = Me.BusinessDate
                    .DiffRate = Me.DiffRate
                    .Flag = "Add"
                    .strConnection = GetConnectionString()
                    .AdministrationFee = CDbl(lblAdminFee.Text)
                    .PolaAngsuran = cboPolaAngsuran.SelectedValue.ToString
                    .TidakAngsur = CDbl(IIf(IsNumeric(txtTidakAngsur.Text), txtTidakAngsur.Text, "0"))
                    .PotongDanaCair = CBool(optPotongPencairanDana.SelectedValue)
                    .AngsuranBayarDealer = CBool(optBayarDealer.SelectedValue)

                    .TotalBunga = CDbl(lblTotalBunga.Text)
                    .NilaiKontrak = CDbl(lblNilaiKontrak.Text)
                    .InstallmentAmount = CDbl(txtInstAmt.Text)
                    .Term = m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue))

                    .InstallmentUnpaid = CDbl(txtTidakAngsur.Text)
                    .DownPayment = CDbl(txtDP.Text)
                    .FlatRate = CDbl(txtFlatRate.Text)

                    If txtTglAngsuranI.Text <> String.Empty Then
                        .EffectiveDate = ConvertDate2(txtTglAngsuranI.Text)
                    Else
                        .EffectiveDate = CDate("01/01/1900")
                    End If

                    .ProvisionFee = CDbl(txtBiayaProvisiN.Text)
                    .AlokasiInsentifRefundBunga = 0
                    .AlokasiInsentifRefundBungaPercent = 0
                    .TitipanRefundBunga = 0
                    .TitipanRefundBungaPercent = 0
                    .AlokasiInsentifPremi = 0
                    .AlokasiInsentifPremiPercent = 0
                    .AlokasiProgresifPremi = 0
                    .AlokasiProgresifPremiPercent = 0
                    .SubsidiBungaPremi = 0
                    .SubsidiBungaPremiPercent = 0
                    .PendapatanPremi = 0
                    .PendapatanPremiPercent = 0
                    .ProvisiPercent = CDec(txtBiayaProvisi.Text)
                    .AlokasiInsentifProvisi = 0
                    .AlokasiInsentifProvisiPercent = 0
                    .SubsidiBungaProvisi = 0
                    .SubsidiBungaProvisiPercent = 0
                    .TitipanProvisi = 0
                    .TitipanProvisiPercent = 0
                    .AlokasiInsentifBiayaLain = 0
                    .AlokasiInsentifBiayaLainPercent = 0
                    .SubsidiBungaBiayaLain = 0
                    .SubsidiBungaBiayaLainPercent = 0
                    .TitipanBiayaLain = 0
                    .TitipanBiayaLainPercent = 0
                    .SubsidiBungaDealer = 0
                    .SubsidiAngsuran = 0

                    .BungaNettEff = 0
                    .BungaNettFlat = 0

                    .RefundBungaPercent = 0 ' CDec(ucRefundBunga.Text)
                    .RefundBungaAmount = 0 'CDbl(txtRefundBungaN.Text)
                    .RefundPremiPercent = 0 'CDec(txtRefundPremi.Text)
                    .RefundPremiAmount = 0 'CDbl(txtRefundPremiN.Text)
                    .RefundAdminPercent = 0 'CDec(ucRefundAdmin.Text)
                    .RefundAdminAmount = 0 'CDbl(txtRefundAdminN.Text)
                    .RefundProvisiPercent = 0 'CDec(txtRefundBiayaProvisi.Text)
                    .RefundProvisiAmount = 0 'CDbl(txtRefundBiayaProvisiN.Text)

                    .GabungRefundSupplier = CBool(rdoGabungRefundSupplier.SelectedValue.ToString)

                    'tidak ada subsidi atau provisi maka ke supplier ratenya sama!
                    '.SupplierRate = CDbl(IIf(lblSupplierRate.Text.Trim = "", "0", lblSupplierRate.Text.Trim))
                    '.TotalOTRSupplier = CDbl(lblOTRSupplier.Text)
                    '.DPSupplier = CDbl(txtDPSupplier.Text)
                    '.NTFSupplier = CDbl(lblNTFSupplier.Text)
                    '.TenorSupplier = CInt(lblTenorSupplier.Text)
                    '.FlatRateSupplier = CDec(lblFlatRateSupplier.Text)
                    '.EffectiveRateSupplier = CDec(txtEffectiveRateSupplier.Text)
                    '.TotalBungaSupplier = CDbl(lblTotalBungaSupplier.Text)
                    '.NilaiKontrakSupplier = CDbl(lblNilaiKontrakSupplier.Text)
                    '.InstallmentAmountSupplier = CDbl(txtInstAmtSupplier.Text)
                    '.AngsuranTidakSamaSupplier = CDbl(lblAngsuranTidakSama.Text)
                End With

                ' mengembalikan nilai yang sudah di ubah ke dalam textboxt view

                Try
                    If Me.InstallmentScheme = "RF" Then
                        oFinancialData = m_controller.SaveFinancialData(oFinancialData)
                    ElseIf Me.InstallmentScheme = "IR" Then
                        oFinancialData = m_controller.SaveAmortisasiIRR(oFinancialData)
                    ElseIf Me.InstallmentScheme = "ST" Then
                        oFinancialData = m_controller.SaveAmortisasiStepUpDown(oFinancialData)
                    Else
                        oFinancialData = m_controller.SaveAmortisasi(oFinancialData)
                    End If

                    'Dim rasioAngs As Decimal = CDec(CDbl(txtInstAmt.Text) / Me.CustomerTotalIncome * 100)

                    'If rasioAngs > Me.IncomeRatioPercentage Then
                    '    ShowMessage(lblMessage, "Data Financial sudah tersimpan tapi rasio angsuran " & FormatNumber(rasioAngs, 2) & ", harus lebih kecil dari " & FormatNumber(Me.IncomeRatioPercentage, 2), True)
                    'End If
                Catch ex As Exception
                    Dim err As New MaxiloanExceptions
                    err.WriteLog("FinancialData_003.aspx", "imbSave_Click", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                    ShowMessage(lblMessage, ex.Message, True)
                End Try

                If oFinancialData.Output = "" Then
                    ShowMessage(lblMessage, "Data saved!", False)
                    ucApplicationTab1.ApplicationID = Me.ApplicationID
                    ucApplicationTab1.selectedTab("Financial")
                    ucApplicationTab1.setLink()
                Else
                    ShowMessage(lblMessage, "Proses Simpan data Gagal " & oFinancialData.Output & "", True)
                End If

            End If


        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("FinancialData_003.aspx", "imbSave_Click", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

#End Region




#Region "DiffRate, GrossYield, FlatRate"
    Function DiffRateNormal(ByVal NumInst As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(lblSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate
        For i = 0 To NumInst - 1
            InstallmentAmt(i) = (InstAmtEffective - InstAmtSupp)
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
    End Function
    Function DiffRateRO(ByVal NumInst As Integer, ByVal GracePeriod As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(lblSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate
        For i = 0 To GracePeriod - 1
            InstallmentAmt(i) = 0
        Next i
        For i = GracePeriod To NumInst - 1
            InstallmentAmt(i) = InstAmtEffective - InstAmtSupp
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
    End Function
    Function DiffRateI(ByVal NumInst As Integer, ByVal GracePeriod As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(lblSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate

        For i = 0 To GracePeriod - 1
            InstallmentAmt(i) = InstAmtEffective - (Math.Round(CDbl(lblNTF.Text) * RunRateSupp / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) / 100, 2))
        Next i
        For i = 0 To NumInst - 1
            InstallmentAmt(i) = InstAmtEffective - InstAmtSupp
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
    End Function

    Function GetGrossYield() As Double
        'Me.NTFGrossYield = Me.NTFGrossYield + Me.DiffRate
        'Return CountRate(Me.NTFGrossYield)
        Return CountRate(CDbl(lblNTF.Text))
        CaclFlatRate()
    End Function

    'Public Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Integer) As Double
    '    Return (dblInterestTotal * 1200) / (dblNTF * CDbl(lblTenor.Text))
    'End Function
#End Region

#Region "Make Amortization Table"
    Public Sub BuatTable()
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "InsSeqNo"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentAmount"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincipalAmount"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InterestAmount"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "OutStandingPrincipal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "OutStandingInterest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "RefundInterest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "AdminFee"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Provisi"
        myDataTable.Columns.Add(myDataColumn)
    End Sub

    Private Sub BuatTabelInstallment()
        Dim inc As Integer
        Dim intRowMax As Integer
        intRowMax = CInt(txtNumInst.Text) - 1

        If Me.InstallmentScheme = "ST" Then
            If CInt(txtStep.Text) > CInt(txtNumInst.Text) Then
                ShowMessage(lblMessage, "Number of Step harus < Jangka waktu Angsuran", True)
                Exit Sub
            End If
            intRowMax = CInt(txtStep.Text) - 1
        End If

        myDataTable = New DataTable("Installment")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)


        For inc = 1 To intRowMax
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("NoOfStep") = 0
            myDataRow("Amount") = 0
            myDataTable.Rows.Add(myDataRow)
        Next inc

        If Me.InstallmentScheme = "ST" Then
            dtgEntryInstallment.Columns(1).Visible = True
        Else
            dtgEntryInstallment.Columns(1).Visible = False
        End If

        dtgEntryInstallment.DataSource = myDataTable.DefaultView
        dtgEntryInstallment.DataBind()

    End Sub


    Public Function InstallmentBalon() As DataSet
        Dim dtbalon As New DataTable
        Dim oFinancialData As Parameter.FinancialData
        Dim dtrow As DataRow
        Dim TotalInterest As Double = 0

        If Not IsNothing(Me.Balon) Then
            dtbalon = Me.Balon

            For Each dtrow In dtbalon.Rows
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = dtrow.Item("InsSeqNo").ToString
                myDataRow("Installment") = Math.Round(CDbl(dtrow.Item("InstallmentAmount").ToString), 2)
                myDataRow("Principal") = Math.Round(CDbl(dtrow.Item("PrincipalAmount").ToString), 2)
                myDataRow("Interest") = Math.Round(CDbl(dtrow.Item("InterestAmount").ToString), 2)
                myDataRow("PrincBalance") = Math.Round(CDbl(dtrow.Item("OutstandingPrincipal").ToString), 2)
                myDataRow("OutStandingInterest") = Math.Round(CDbl(dtrow.Item("OutstandingInterest").ToString), 2)
                myDataTable.Rows.Add(myDataRow)

                TotalInterest = TotalInterest + Math.Round(CDbl(dtrow.Item("InterestAmount").ToString), 2)
            Next
        Else

            Dim oFinancialDataBalon As New Parameter.FinancialData

            oFinancialDataBalon.BranchId = Replace(Me.sesBranchId, "'", "")
            oFinancialDataBalon.AppID = Me.ApplicationID
            oFinancialDataBalon.strConnection = GetConnectionString()

            dtbalon = m_controller.GetInstallmentSchedule(oFinancialDataBalon)

            For Each dtrow In dtbalon.Rows
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = dtrow.Item("InsSeqNo").ToString
                myDataRow("Installment") = Math.Round(CDbl(dtrow.Item("InstallmentAmount").ToString), 2)
                myDataRow("Principal") = Math.Round(CDbl(dtrow.Item("PrincipalAmount").ToString), 2)
                myDataRow("Interest") = Math.Round(CDbl(dtrow.Item("InterestAmount").ToString), 2)
                myDataRow("PrincBalance") = Math.Round(CDbl(dtrow.Item("OutstandingPrincipal").ToString), 2)
                myDataRow("PrincInterest") = Math.Round(CDbl(dtrow.Item("OutstandingInterest").ToString), 2)
                myDataRow("RefundInterest") = Math.Round(CDbl(dtrow.Item("RefundInterest").ToString), 2)
                myDataRow("AdminFee") = Math.Round(CDbl(dtrow.Item("AdminFee").ToString), 2)
                myDataRow("Provisi") = Math.Round(CDbl(dtrow.Item("Provisi").ToString), 2)
                myDataTable.Rows.Add(myDataRow)
            Next

        End If

        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)

        If Not IsNothing(Me.Balon) Then
            Dim custRow As DataRow

            For Each custRow In myDataSet.Tables("AmortTable").Rows
                custRow("RefundInterest") = Math.Round(CDbl(custRow("Interest")) / CDbl(TotalInterest) * Me.RefundInterest, 2)
                custRow("AdminFee") = Math.Round(CDbl(custRow("Interest")) / CDbl(TotalInterest) * Me.AdminFee, 2)
                custRow("Provisi") = Math.Round(CDbl(custRow("Interest")) / CDbl(TotalInterest) * Me.Provisi, 2)
            Next
        End If

        Return myDataSet
    End Function


    Public Function MakeAmortTable1(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Advance - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)

        Dim TotalPrincipal As Double

        PokokHutang(0) = dblNTF
        Bunga(0) = 0
        BuatTable()

        For Me.i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            'Advance dan installment I
            If i = 1 Then
                myDataRow("InsSeqNo") = 0
                myDataRow("InstallmentAmount") = 0
                myDataRow("InterestAmount") = 0
                myDataRow("PrincipalAmount") = 0
            ElseIf i = 2 Then
                myDataRow("InsSeqNo") = i - 1
                myDataRow("InstallmentAmount") = dblInstAmt
                myDataRow("InterestAmount") = 0
                myDataRow("PrincipalAmount") = dblInstAmt
            Else
                myDataRow("InsSeqNo") = i - 1
                myDataRow("InstallmentAmount") = dblInstAmt
                myDataRow("InterestAmount") = Math.Round(((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100)) / 10, 2) * 10 'Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 0)
                myDataRow("PrincipalAmount") = dblInstAmt - CDbl(myDataRow("InterestAmount"))
            End If

            If i = 1 Then
                myDataRow("OutStandingPrincipal") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("OutStandingPrincipal") = 0
                Else
                    myDataRow("OutStandingPrincipal") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("PrincipalAmount")), 2)
                End If
            End If

            myDataRow("OutStandingInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("OutStandingPrincipal"))
            Bunga(i) = CDbl(myDataRow("InterestAmount"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("PrincipalAmount")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For Me.i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet

        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("OutStandingInterest") = PokokBunga(i)
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("InsSeqNo")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("PrincipalAmount") = Math.Round(CDbl(custRow("PrincipalAmount")) - SisaPrincipal, 2)
                    custRow("InterestAmount") = Math.Round(CDbl(custRow("InterestAmount")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("PrincipalAmount") = Math.Round(CDbl(custRow("PrincipalAmount")) + KurangPrincipal, 2)
                    custRow("InterestAmount") = Math.Round(CDbl(custRow("InterestAmount")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            custRow("RefundInterest") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.RefundInterest, 2)
            custRow("AdminFee") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.AdminFee, 2)
            custRow("Provisi") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.Provisi, 2)
        Next

        Return myDataSet
    End Function

    Public Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()

            If i = 1 Then 'Installment I
                myDataRow("InsSeqNo") = 0
                myDataRow("InstallmentAmount") = 0
                myDataRow("PrincipalAmount") = 0
                myDataRow("InterestAmount") = 0
            Else
                myDataRow("InsSeqNo") = i - 1
                myDataRow("InstallmentAmount") = dblInstAmt
                myDataRow("InterestAmount") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("PrincipalAmount") = Math.Round(dblInstAmt - CDbl(myDataRow("InterestAmount")), 2)
            End If

            If i = 1 Then
                myDataRow("OutStandingPrincipal") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("OutStandingPrincipal") = 0
            Else
                myDataRow("OutStandingPrincipal") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("PrincipalAmount")), 2)
            End If
            myDataRow("OutStandingInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("OutStandingPrincipal"))
            Bunga(i) = CDbl(myDataRow("InterestAmount"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("PrincipalAmount")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("OutStandingInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("InsSeqNo")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("PrincipalAmount") = Math.Round(CDbl(custRow("PrincipalAmount")) - SisaPrincipal, 2)
                    custRow("InterestAmount") = Math.Round(CDbl(custRow("InterestAmount")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("PrincipalAmount") = Math.Round(CDbl(custRow("PrincipalAmount")) + KurangPrincipal, 2)
                    custRow("InterestAmount") = Math.Round(CDbl(custRow("InterestAmount")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            custRow("RefundInterest") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.RefundInterest, 2)
            custRow("AdminFee") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.AdminFee, 2)
            custRow("Provisi") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.Provisi, 2)
        Next

        Return myDataSet
    End Function

    Public Function MakeAmortTable3(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Roll Over)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("InsSeqNo") = 0
                myDataRow("InstallmentAmount") = 0
                myDataRow("PrincipalAmount") = 0
                myDataRow("InterestAmount") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("InsSeqNo") = i - 1
                myDataRow("InstallmentAmount") = 0
                myDataRow("InterestAmount") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("PrincipalAmount") = Math.Round(CDbl(myDataRow("InstallmentAmount")) - CDbl(myDataRow("InterestAmount")), 2)
            Else
                myDataRow("InsSeqNo") = i - 1
                myDataRow("InstallmentAmount") = dblInstAmt
                myDataRow("InterestAmount") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("PrincipalAmount") = Math.Round(dblInstAmt - CDbl(myDataRow("InterestAmount")), 2)
            End If
            If i = 1 Then
                myDataRow("OutStandingPrincipal") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("OutStandingPrincipal") = 0
                Else
                    myDataRow("OutStandingPrincipal") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("PrincipalAmount")), 2)
                End If
            End If
            myDataRow("OutStandingInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("OutStandingPrincipal"))
            Bunga(i) = CDbl(myDataRow("InterestAmount"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("PrincipalAmount")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("OutStandingInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("InsSeqNo")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("PrincipalAmount") = Math.Round(CDbl(custRow("PrincipalAmount")) - SisaPrincipal, 2)
                    custRow("InterestAmount") = Math.Round(CDbl(custRow("InterestAmount")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("PrincipalAmount") = Math.Round(CDbl(custRow("PrincipalAmount")) + KurangPrincipal, 2)
                    custRow("InterestAmount") = Math.Round(CDbl(custRow("InterestAmount")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            custRow("RefundInterest") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.RefundInterest, 2)
            custRow("AdminFee") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.AdminFee, 2)
            custRow("Provisi") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.Provisi, 2)
        Next
        Return myDataSet
    End Function

    Public Function MakeAmortTable6(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Interest Only)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("InsSeqNo") = 0
                myDataRow("InstallmentAmount") = 0
                myDataRow("PrincipalAmount") = 0
                myDataRow("InterestAmount") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("InsSeqNo") = i - 1
                myDataRow("InstallmentAmount") = Math.Round(dblNTF * dblRunRate / m_controller.GetTerm(intPayFreq) / 100, 2)
                myDataRow("InterestAmount") = myDataRow("InstallmentAmount")
                myDataRow("PrincipalAmount") = 0
            ElseIf i = intGracePeriod + 1 Then
                myDataRow("InsSeqNo") = i - 1
                myDataRow("InterestAmount") = Math.Round(dblNTF * dblRunRate / m_controller.GetTerm(intPayFreq) / 100, 2)
                myDataRow("InstallmentAmount") = dblInstAmt
                myDataRow("PrincipalAmount") = Math.Round(dblInstAmt - CDbl(myDataRow("InterestAmount")), 2)
            Else
                myDataRow("InsSeqNo") = i - 1
                myDataRow("InstallmentAmount") = dblInstAmt
                myDataRow("InterestAmount") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("PrincipalAmount") = Math.Round(dblInstAmt - CDbl(myDataRow("InterestAmount")), 2)
            End If
            If i = 1 Then
                myDataRow("OutStandingPrincipal") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("OutStandingPrincipal") = 0
                Else
                    myDataRow("OutStandingPrincipal") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("PrincipalAmount")), 2)
                End If
            End If
            myDataRow("OutStandingInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("OutStandingPrincipal"))
            Bunga(i) = CDbl(myDataRow("InterestAmount"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("PrincipalAmount")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("OutStandingInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("InsSeqNo")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("PrincipalAmount") = Math.Round(CDbl(custRow("PrincipalAmount")) - SisaPrincipal, 2)
                    custRow("InterestAmount") = Math.Round(CDbl(custRow("InterestAmount")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("PrincipalAmount") = Math.Round(CDbl(custRow("PrincipalAmount")) + KurangPrincipal, 2)
                    custRow("InterestAmount") = Math.Round(CDbl(custRow("InterestAmount")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            custRow("RefundInterest") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.RefundInterest, 2)
            custRow("AdminFee") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.AdminFee, 2)
            custRow("Provisi") = Math.Round(CDbl(custRow("InterestAmount")) / CDbl(lblTotalBunga.Text) * Me.Provisi, 2)
        Next

        Return myDataSet
    End Function
#End Region

#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect("FinancialData.aspx")
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbCancel.Click
        'Response.Redirect("FinancialData.aspx")
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As New HttpCookie("Incentive")
        cookie.Values.Add("ApplicationID", Me.ApplicationID)
        cookie.Values.Add("CustomerID", Me.CustomerID)
        cookie.Values.Add("CustomerName", Me.CustName)

        Response.AppendCookie(cookie)
    End Sub
#End Region

#Region "Calculate Total Pembiayaan"
    'Protected Sub txtDP_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtDP.OnTextChanged
    '    CalculateTotalPembiayaan()
    'End Sub

    Protected Sub CalculateTotalPembiayaan()
        Dim ntf_ As Double = CDbl(lblOTR.Text) + CDbl(lblKaroseri.Text) - CDbl(txtDP.Text) + CDbl(lblAssetInsurance3.Text) - CDbl(IIf(lblIsAdminFeeCredit.Text = "(Paid)", 0, lblIsAdminFeeCredit.Text))
        lblNTF.Text = FormatNumber(ntf_, 0)
        'If Me.OtrNtf = "ntf" Then
        '    lblNilaiNTFOTR.Text = ntf_.ToString
        'End If
    End Sub

    Protected Sub CalculateTotalPembiayaanSupplier()
        lblNTFSupplier.Text = FormatNumber(CDbl(lblOTRSupplier.Text) - CDbl(txtDPSupplier.Text) + CDbl(lblAssetInsurance3.Text) - CDbl(IIf(lblIsAdminFeeCredit.Text = "(Paid)", 0, lblIsAdminFeeCredit.Text)), 0)
    End Sub

    Private Sub txtDPSupplier_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDPSupplier.TextChanged
        CalculateTotalPembiayaanSupplier()
    End Sub

#End Region

    Protected Sub initObjects()
        lblSupplierRate.Text = ""
        lblInterestType.Text = IIf(Me.InterestType = "FX", "Fixed Rate", "FloatingRate").ToString
        txtStep.Enabled = False
        txtCummulative.Enabled = False
        pnlEntryInstallment.Visible = False
        pnlViewST.Visible = False
        updPanelHitung.Visible = True
        uPanelSave.Visible = False
        txtStep.Enabled = False
        txtCummulative.Enabled = False
        rvStep.Enabled = False
        rvCummulative.Enabled = False

        Entities.strConnection = GetConnectionString()
        Entities.BranchId = Replace(Me.sesBranchId, "'", "")
        Entities.AppID = Me.ApplicationID
        Me.StepUpDownType = m_controller.GetStepUpStepDownType(Entities)

        'If Me.InterestType <> "FL" Then
        '    cboPaymentFreq.Enabled = False
        'Else
        '    cboPaymentFreq.Enabled = True
        'End If

        If Me.InstallmentScheme = "RF" Then
            lblInstScheme.Text = "Regular Fixed Installment Scheme"
            If cboFirstInstallment.SelectedValue = "AD" Then
                txtGracePeriod.Enabled = False
                txtGracePeriod.Text = "0"
                cboGracePeriod.Enabled = False
                cboGracePeriod.ClearSelection()
            Else
                txtGracePeriod.Enabled = True
                cboGracePeriod.Enabled = True
            End If
        ElseIf Me.InstallmentScheme = "IR" Then
            lblInstScheme.Text = "Irregular Installment Scheme"
            lblInstallment.Text = "Last Installment"
            'RVInstAmt.Enabled = False
            txtInstAmt.rv.Enabled = False
            RVInstAmtSupplier.Enabled = False
            txtInstAmt.Enabled = False
            txtGracePeriod.Enabled = False
            cboGracePeriod.Enabled = False
            btnSave.Enabled = False
            btnSave.Visible = False
        ElseIf Me.InstallmentScheme = "ST" Then
            lblInstScheme.Text = "Step Up / Step Down Installment Scheme"
            'RVInstAmt.Enabled = False
            txtInstAmt.rv.Enabled = False
            RVInstAmtSupplier.Enabled = False
            txtInstAmt.Enabled = False
            txtGracePeriod.Enabled = False
            cboGracePeriod.Enabled = False
            btnSave.Enabled = False
            btnSave.Visible = False
            If Me.StepUpDownType = "NM" Then
                txtStep.Enabled = True
                txtCummulative.Enabled = False
                rvStep.Enabled = True
            ElseIf Me.StepUpDownType = "RL" Then
                txtStep.Enabled = False
                txtCummulative.Enabled = True
                rvCummulative.Enabled = True
            Else
                txtStep.Enabled = True
                txtCummulative.Enabled = True
                rvStep.Enabled = True
                rvCummulative.Enabled = True
            End If
            If Me.StepUpDownType = "NM" Or Me.StepUpDownType = "LS" Then
                imbRecalcEffRate.Visible = False
            Else
                imbRecalcEffRate.Enabled = True
                imbRecalcEffRate.Visible = True

            End If
        Else
            lblInstScheme.Text = "Even Principle Installment Scheme"
            txtInstAmt.Enabled = False
            If cboFirstInstallment.SelectedValue = "AD" Then
                txtGracePeriod.Enabled = False
                txtGracePeriod.Text = "0"
                cboGracePeriod.Enabled = False
                cboGracePeriod.ClearSelection()
            Else
                txtGracePeriod.Enabled = True
                cboGracePeriod.Enabled = True
            End If
        End If

        lblMessage.Text = ""
        lblMessage.Visible = False
        lblFlatRate.Visible = False

        txtTidakAngsur.RequiredFieldValidatorEnable = False
        If Me.InstallmentScheme = "BP" Then
            txtTidakAngsur.Enabled = True
        Else
            txtTidakAngsur.Enabled = False
        End If

        txtDP.RequiredFieldValidatorEnable = True
        txtDP.AutoPostBack = True
        txtDP.TextCssClass = "numberAlign2 regular_text"
        txtNumInst.RequiredFieldValidatorEnable = True
        txtNumInst.TextCssClass = "numberAlign2 regular_text"

        With txtNumInst
            .AutoPostBack = True
            .RequiredFieldValidatorEnable = True
            .TextCssClass = "numberAlign2 regular_text"
            .Text = "0"
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "1"
            .RangeValidatorMaximumValue = "9999999999999"
        End With

        txtInstAmt.RequiredFieldValidatorEnable = True
        txtInstAmt.AutoPostBack = True
        txtDPPersen.RequiredFieldValidatorEnable = True
        txtDPPersen.AutoPostBack = True
        txtDPPersen.TextCssClass = "numberAlign2 smaller_text"


    End Sub

    Protected Sub FillTestingData()
        cboPolaAngsuran.SelectedIndex = 1
        cboFirstInstallment.SelectedIndex = 1
    End Sub

#Region "incentive"

    Private Sub GetRefundPremiumToSupplier()
        Dim data As New Supplier
        data.strConnection = GetConnectionString()
        data.AppID = Me.ApplicationID

        data = m_controller.GetRefundPremiumToSupplier(data)
        Me.SupplierID = data.SupplierID
    End Sub

    Private Function validateinsentif() As String
        Dim errmsg As String = String.Empty
        'loop rincian transaksi get each transaction total

        For i As Integer = 0 To ApplicationDetailTransactionDT.Rows.Count - 1
            If CBool(ApplicationDetailTransactionDT.Rows(i)("isDistributable")) = False Then Continue For
            Dim transAmount As Decimal = CDec(ApplicationDetailTransactionDT.Rows(i)("txtTransAmount"))
            Dim transID As String = ApplicationDetailTransactionDT.Rows(i)("TransID").ToString

            'errmsg = validateInsentifPerTransaction(transID, transAmount)
            If errmsg <> String.Empty Then Return errmsg
        Next


        Return String.Empty
    End Function

    Private Function validateInsentifPerTransaction(ByVal transID As String, ByVal transAmount As Decimal) As String
        Dim dblTotalInsentifSupplierEmployee As Double

        For i As Integer = 0 To Me.dtDistribusiNilaiInsentif.Rows.Count - 1
            If dtDistribusiNilaiInsentif.Rows(i).Item("transId").ToString.Trim.ToLower = transID.Trim.ToLower Then
                dblTotalInsentifSupplierEmployee += CDbl(Me.dtDistribusiNilaiInsentif.Rows(i).Item("IncentiveForRecepient"))
                TotalInsGrid = TotalInsGrid + dblTotalInsentifSupplierEmployee
            End If

        Next

        If transAmount <> dblTotalInsentifSupplierEmployee Then
            Return "Total distribusi insentif " & transID & "tidak sama dengan nilai insentif!"
        End If

        Return String.Empty
    End Function
#End Region


#Region "rincian transaksi"




#End Region


    Private Sub CalculateTotalBayarPertama()
        Dim um, ba, bf, at, ap, ot, bp, bpr As Double

        um = CDbl(IIf(IsNumeric(lblUangMuka.Text), lblUangMuka.Text, 0))
        ba = CDbl(IIf(IsNumeric(lblAdminFee.Text), lblAdminFee.Text, 0))
        bf = CDbl(IIf(IsNumeric(lblBiayaFidusia.Text), lblBiayaFidusia.Text, 0))
        at = CDbl(IIf(IsNumeric(lblAssetInsurance32.Text), lblAssetInsurance32.Text, 0))
        ap = CDbl(IIf(IsNumeric(lblAngsuranPertama.Text), lblAngsuranPertama.Text, 0))
        ot = CDbl(IIf(IsNumeric(lblOtherFee.Text), lblOtherFee.Text, 0))
        '  bp = CDbl(IIf(IsNumeric(lblBiayaPolis.Text), lblBiayaPolis.Text, 0))
        bpr = CDbl(IIf(IsNumeric(lblBiayaProvisi.Text), lblBiayaProvisi.Text, 0))


        '        lblTotalBayarPertama.Text = FormatNumber(um + ba + bf + at + ap + ot + bp + bpr, 0)
        lblTotalBayarPertama.Text = FormatNumber(um + ba + bf + at + ap + ot + bpr, 0)

    End Sub



    Private Function getMaxRefundBunga() As String
        Dim oClass As New Parameter.GeneralSetting
        Dim oCtr As New GeneralSettingController

        With oClass
            .strConnection = GetConnectionString()
            .GSID = "RFBNG"
            .ModuleID = "CRD"
        End With

        Try
            Return oCtr.GetGeneralSettingValue(oClass)
        Catch ex As Exception
            Return "0"
        End Try
    End Function
    Private Function getTotalAlokasiPremiGross() As String
        Dim oClass As New Parameter.GeneralSetting
        Dim oCtr As New GeneralSettingController

        With oClass
            .strConnection = GetConnectionString()
            .GSID = "PREMIGROSSTP"
            .ModuleID = "CRD"
        End With

        Try
            Return oCtr.GetGeneralSettingValue(oClass)
        Catch ex As Exception
            Return "0"
        End Try
    End Function


    Function getRateDetailTransaksi(TransAmount As Double) As Decimal
        Dim xrate As Decimal
        If cboFirstInstallment.Text = "AD" Then
            xrate = RateConvertion.GetEffectiveRateAdv(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), TransAmount, 1)
        Else
            xrate = RateConvertion.GetEffectiveRateArr(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), TransAmount, 1)
        End If

        Return Math.Round(Math.Abs(Me.EffectiveRate - xrate), 2)
    End Function


    Private Sub saveBungaNett()
        Try
            Dim oBungaNet As New Parameter.FinancialData

            With oBungaNet
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .EffectiveRate = TotalBungaNett / 100
                .Tenor = CInt(txtNumInst.Text)
                .BungaNettEff = TotalBungaNett
                .BungaNettFlat = RateConvertion.cEffToFlat(oBungaNet)
            End With

            oBungaNet = m_controller.saveBungaNett(oBungaNet)

        Catch ex As Exception
            'skip

        End Try
    End Sub


    Private Sub imbCalcInst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbCalcInst.Click
        Dim oEntities As New Parameter.FinancialData
        Dim Inst As Double

        If Me.StepUpDownType = "RL" Or Me.StepUpDownType = "LS" Then
            If CInt(txtCummulative.Text) > CInt(txtNumInst.Text) Then
                ShowMessage(lblMessage, "Cummulative  < Number Of Installment!", True)
                Exit Sub
            End If
        End If

        If CInt(txtEffectiveRate.Text) <= 0 Then
            ShowMessage(lblMessage, "Efective Rate must > 0!", True)
            Exit Sub
        End If

        imbRecalcEffRate.Enabled = True
        imbRecalcEffRate.Visible = True
        pnlViewST.Visible = False
        pnlEntryInstallment.Visible = False

        If Me.InstallmentScheme = "RF" Then
            Dim ds As New DataSet
            If cboFirstInstallment.SelectedValue = "AD" Then
                ds = MakeAmortTable1(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue))
            ElseIf cboFirstInstallment.SelectedValue = "AR" Then
                If (txtGracePeriod.Text.Trim = "0" Or txtGracePeriod.Text.Trim = "") Then
                    ds = MakeAmortTable2(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue))
                ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "I" Then
                    ds = MakeAmortTable6(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "R" Then
                    ds = MakeAmortTable3(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                End If
            End If
            ds.Tables(0).Rows(0).Delete()
            dtgViewInstallment.DataSource = ds
            dtgViewInstallment.DataBind()
            pnlViewST.Visible = True
            uPanelSave.Visible = True
        ElseIf Me.InstallmentScheme = "IR" Then
            'lblInstScheme.Text = "Irregular Installment Scheme"
            BuatTabelInstallment()
            uPanelSave.Visible = False
            ImgEntryInstallment.Visible = True
            pnlEntryInstallment.Visible = True
            txtInstAmt.Enabled = True
        ElseIf Me.InstallmentScheme = "ST" Then
            'lblInstScheme.Text = "Step Up / Step Down Installment Scheme"
            If Me.StepUpDownType = "NM" Then
                BuatTabelInstallment()
                uPanelSave.Visible = False
                ImgEntryInstallment.Visible = True
                pnlEntryInstallment.Visible = True
                txtInstAmt.Enabled = True
            ElseIf Me.StepUpDownType = "RL" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.CummulativeStart = CInt(txtCummulative.Text)
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                Entities = m_controller.GetInstAmtStepUpDownRegLeasing(Entities)
                txtInstAmt.Text = FormatNumber(Math.Round(Entities.InstallmentAmount, 0), 2)
                uPanelSave.Visible = True
                txtInstAmt.Enabled = True
            Else
                BuatTabelInstallment()
                uPanelSave.Visible = False
                ImgEntryInstallment.Visible = True
                pnlEntryInstallment.Visible = True
                txtInstAmt.Enabled = True
            End If
            '--- view installment ----'
            If Me.StepUpDownType <> "NM" And Me.StepUpDownType <> "RS" And Me.StepUpDownType <> "LS" Then
                ViewInstallment()
                If cboFirstInstallment.SelectedValue = "AD" Then
                    ' lblAngsuranPertama.Text = FormatNumber(firstInstallment, 0)

                Else
                    lblAngsuranPertama.Text = "0"
                End If

            End If
        Else
            'lblInstScheme.Text = "Even Principle Installment Scheme"
            Try
                With oEntities
                    .NTF = CDbl(lblNTF.Text)
                    .NumOfInstallment = CInt(txtNumInst.Text)
                    .GracePeriod = CInt(txtGracePeriod.Text.Trim)
                End With
                oEntities = m_controller.GetPrincipleAmount(oEntities)
                Me.PrincipleAmount = oEntities.Principal
            Catch ex As Exception
                ShowMessage(lblMessage, "Invalid Principal Amount!", True)
            End Try
        End If
    End Sub


    Private Sub ImgEntryInstallment_Click(sender As Object, e As System.EventArgs) Handles ImgEntryInstallment.Click
        Dim oFinancialData As New Parameter.FinancialData
        Dim oDs As New DataSet
        Dim drow As DataRow
        If Me.InstallmentScheme = "ST" Then
            If IsValidEntry(dtgEntryInstallment, CDbl(lblNTF.Text), CInt(txtNumInst.Text), CInt(txtCummulative.Text), Me.StepUpDownType) = False Then
                ShowMessage(lblMessage, "Invalid Entry Installment", True)
                Exit Sub
            End If

            If Me.StepUpDownType = "NM" Then
                txtInstAmt.Text = FormatNumber(GetInstallmentAmountStepUpDown(), 0)
            Else
                txtInstAmt.Text = FormatNumber(GetInstallmentAmountStepUpDownLeasing(), 0)
            End If

            txtInstAmt.Enabled = True
        ElseIf Me.InstallmentScheme = "IR" Then
            With oFinancialData
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .AppID = Me.ApplicationID
                .NTF = CDbl(lblNTF.Text)
                .EffectiveRate = CDbl(txtEffectiveRate.Text)
                .SupplierRate = CDbl(IIf(txtEffectiveRateSupplier.Text.Trim = "", "0", txtEffectiveRateSupplier.Text.Trim))
                .PaymentFrequency = cboPaymentFreq.SelectedValue
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .Tenor = CInt(lblTenor.Text)
                .GracePeriod = CInt(IIf(txtGracePeriod.Text.Trim = "", "0", txtGracePeriod.Text.Trim))
                .GracePeriodType = cboGracePeriod.SelectedValue
                .BusDate = Me.BusinessDate
                .DiffRate = Me.DiffRate
                .strConnection = GetConnectionString()
                .MydataSet = GetEntryInstallment()
                .IsSave = 0
                .Flag = "Add"
            End With
            oFinancialData = m_controller.SaveAmortisasiIRR(oFinancialData)
            txtInstAmt.Text = FormatNumber(oFinancialData.InstallmentAmount, 0)
        End If
        Call ViewInstallment()
    End Sub

    Private Function IsValidEntry(ByVal Dg As DataGrid, ByVal NTF As Double, ByVal NumOfIns As Integer, ByVal Cummulative As Integer, ByVal StepUpDownType As String) As Boolean
        Dim inc As Integer
        Dim intStep As Integer
        Dim intTotStep As Integer
        Dim dblAmount As Double
        Dim dblTotAmount As Double
        Dim firstInstallment As Double
        intTotStep = 0
        dblTotAmount = 0

        For inc = 0 To Dg.Items.Count - 1
            Dim txtStep As New TextBox
            Dim txtAmount As New TextBox
            txtStep = CType(Dg.Items(inc).FindControl("txtNoStep"), TextBox)
            txtAmount = CType(Dg.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            If inc = 0 Then
                firstInstallment = CDbl(txtAmount.Text)
            End If
            If StepUpDownType = "NM" Then
                dblAmount = CDbl(txtAmount.Text)
                dblTotAmount = dblTotAmount + dblAmount
                intStep = CInt(txtStep.Text)
                intTotStep = intTotStep + intStep
            ElseIf StepUpDownType = "RL" Then
                intStep = CInt(txtStep.Text)
                intTotStep = intTotStep + intStep
            Else
                intStep = CInt(txtStep.Text)
                dblAmount = CDbl(txtAmount.Text) * intStep
                intTotStep = intTotStep + intStep
                dblTotAmount = dblTotAmount + dblAmount
            End If
        Next
        If StepUpDownType = "NM" Then
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If dblTotAmount >= NTF Then
                Return False
            End If
        ElseIf StepUpDownType = "RL" Then
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If Cummulative < intTotStep Then
                Return False
            End If
        Else
            If Cummulative < intTotStep Then
                Return False
            End If
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If dblTotAmount >= NTF Then
                Return False
            End If
        End If
        If intTotStep < Cummulative Then
            Return False
        End If
        Return True
    End Function

    Private Function GetEntryInstallment() As DataSet
        Dim inc As Integer
        Dim jmlRow As Integer
        Dim InstallmentAmount As Double
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.FinancialData

        jmlRow = dtgEntryInstallment.Items.Count
        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            myDataRow = myDataTable.NewRow()
            myDataRow("InsSeqNo") = inc
            myDataRow("NoOfStep") = CDbl(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        objDs.Tables.Add(myDataTable)
        Return objDs
    End Function

    Private Function GetInstallmentAmountStepUpDown() As Double
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(lblNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
            Next
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1 '((PokokHutang(0) - TotalInstallment) + TotalInterest) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        Return InstallmentAmount
    End Function
    Private Function GetInstallmentAmountStepUpDownLeasing() As Double
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(lblNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
            Next
        Next
        intSisa = (CInt(txtCummulative.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        Return InstallmentAmount
    End Function
    Private Function GetInstallmentAmountStepUpDownLeasingTBL() As DataSet
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(lblNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
            Next
        Next
        intSisa = (CInt(txtCummulative.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        For inc = 1 To intSisa - 1
            inc3 = inc3 + 1
            Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
            Principal(inc3) = InstallmentAmount - Interest(inc3)
            PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)

            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc3
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        For inc = 1 To intSisa
            inc3 = inc3 + 1
            Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
            Principal(inc3) = InstallmentAmount - Interest(inc3)
            PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)

            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc3
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        myDataSet.Tables.Add(myDataTable)
        Return myDataSet
    End Function

    Private Sub ViewInstallment()
        Dim oFinancialData As New Parameter.FinancialData
        pnlViewST.Visible = True
        If Me.InstallmentScheme = "ST" Then
            If Me.StepUpDownType = "NM" Then
                oFinancialData.MydataSet = GetEntryInstallmentStepUpDown()
            ElseIf Me.StepUpDownType = "RL" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.CummulativeStart = CInt(txtCummulative.Text)
                Entities.NumOfInstallment = CInt(txtNumInst.Text)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities = m_controller.GetStepUpDownRegLeasingTBL(Entities)
                oFinancialData.MydataSet = Entities.MydataSet
            Else
                oFinancialData.MydataSet = GetInstallmentAmountStepUpDownLeasingTBL()
            End If
        End If

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.NTF = CDbl(lblNTF.Text)
        oFinancialData.EffectiveRate = CDbl(txtEffectiveRate.Text)
        oFinancialData.SupplierRate = CDbl(IIf(txtEffectiveRateSupplier.Text.Trim = "", "0", txtEffectiveRateSupplier.Text.Trim))
        oFinancialData.PaymentFrequency = cboPaymentFreq.SelectedValue
        oFinancialData.FirstInstallment = cboFirstInstallment.SelectedValue
        oFinancialData.Tenor = CInt(lblTenor.Text)
        oFinancialData.GracePeriod = CInt(IIf(txtGracePeriod.Text.Trim = "", "0", txtGracePeriod.Text.Trim))
        oFinancialData.GracePeriodType = cboGracePeriod.SelectedValue
        oFinancialData.BusDate = Me.BusinessDate
        oFinancialData.DiffRate = Me.DiffRate
        oFinancialData.Flag = "Add"
        oFinancialData.strConnection = GetConnectionString()

        If Me.InstallmentScheme = "ST" Then
            oFinancialData = m_controller.ListAmortisasiStepUpDown(oFinancialData)
            If oFinancialData.ListData.Rows.Count > 0 Then
                lblTotalBunga.Text = FormatNumber(CDec(oFinancialData.ListData.Rows(0).Item("OutStandingInterest").ToString.Trim), 0)
                lblAngsuranPertama.Text = FormatNumber(CDec(oFinancialData.ListData.Rows(0).Item("InstallmentAmount").ToString.Trim), 0)
            End If
        End If
        If Me.InstallmentScheme = "IR" Then
            oFinancialData = m_controller.ListAmortisasiIRR(oFinancialData)
            If oFinancialData.ListData.Rows.Count > 0 Then
                lblTotalBunga.Text = FormatNumber(CDec(oFinancialData.ListData.Rows(0).Item("OutStandingInterest").ToString.Trim), 0)
                lblAngsuranPertama.Text = FormatNumber(CDec(oFinancialData.ListData.Rows(0).Item("InstallmentAmount").ToString.Trim), 0)
            End If
        End If

        CalculateTotalBayarPertama()
        CalculateNilaiKontrak()

        dtgViewInstallment.DataSource = oFinancialData.ListData
        dtgViewInstallment.DataBind()


        'If oFinancialData.ListData.Rows.Count > 0 Then
        '    firstInstallment = CType(oFinancialData.ListData.Rows(0).Item("InstallmentAmount"), Double)
        'End If
        uPanelSave.Visible = True
        btnSave.Enabled = True
        btnSave.Visible = True

    End Sub

    Private Function GetEntryInstallmentStepUpDown() As DataSet
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim intNumOfInst As Integer
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim IRRValues(intNumOfInst) As Double
        Dim irrCount As Integer = 0
        IRRValues(irrCount) = CDbl(lblNTF.Text) * -1

        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                irrCount += 1
                IRRValues(irrCount) = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
                inc3 = inc3 + 1
            Next
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        InstallmentAmount = CDbl(txtInstAmt.Text)
        If inc3 <= CInt(txtNumInst.Text) Then
            For inc = 1 To intSisa
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                irrCount += 1
                IRRValues(irrCount) = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
                inc3 = inc3 + 1
            Next
        End If
        objDs.Tables.Add(myDataTable)
        Return objDs
    End Function

    Private Sub imbRecalcEffRate_Click(sender As Object, e As System.EventArgs) Handles imbRecalcEffRate.Click
        If CInt(txtInstAmt.Text) <= 0 Then
            ShowMessage(lblMessage, "Installment Amount must > 0!", True)
            Exit Sub
        End If
        Dim Eff As Double
        Dim Supp As Double
        Try
            If Me.InstallmentScheme = "IR" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.MydataSet = GetEntryInstallment()
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Eff = m_controller.GetNewEffRateIRR(Entities)
                txtEffectiveRate.Text = Eff.ToString.Trim
                'count SupplierRate
                Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.MydataSet = GetEntryInstallment()
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Supp = m_controller.GetNewEffRateIRR(Entities)
            ElseIf Me.InstallmentScheme = "ST" Then
                If Me.StepUpDownType = "NM" Then
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme
                    Entities.MydataSet = GetEntryInstallmentStepUpDown()
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Eff = m_controller.GetNewEffRateIRR(Entities)
                    txtEffectiveRate.Text = FormatNumber(Eff, 2)
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme
                    Entities.MydataSet = GetEntryInstallmentStepUpDown()
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Supp = m_controller.GetNewEffRateIRR(Entities)
                    ViewInstallment()
                ElseIf Me.StepUpDownType = "RL" Then
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateRegLeasing(Entities)
                    txtEffectiveRate.Text = FormatNumber(Eff, 2)
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Supp = m_controller.GetNewEffRateRegLeasing(Entities)
                    ViewInstallment()
                Else
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme
                    Entities.MydataSet = GetEntryInstallmentStepUpDown()
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateLeasing(Entities)
                    txtEffectiveRate.Text = FormatNumber(Eff, 2)
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme
                    Entities.MydataSet = GetEntryInstallmentStepUpDown()
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Supp = m_controller.GetNewEffRateLeasing(Entities)

                    ViewInstallment()
                End If
                'Else
                '    'count EffectiveRate
                '    Eff = CountRate(CDbl(lblNTF.Text))
                '    Context.Trace.Write("Eff = " & Eff)
                '    txtEffectiveRate.Text = Eff.ToString.Trim

                '    'count SupplierRate
                '    Supp = CountRate(CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim))
                '    Context.Trace.Write("Supp = " & Supp)
                '    CaclFlatRate()
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, "Invalid Installment Amount!", True)
        End Try
    End Sub

    Private Sub CaclFlatRate()
        Dim FR As Double
        FR = (100 * ((CDbl(txtInstAmt.Text) * CDbl(txtNumInst.Text)) - CDbl(lblNTF.Text.Trim)) / CDbl(lblNTF.Text.Trim)) / (CDbl(lblTenor.Text) / 12)
        Me.FlatRate = CDec(FR)
        txtFlatRate.Text = FormatNumber(FR, 2)
        ' lblTotalBunga.Text = FormatNumber((FR * CDbl(txtInstAmt.Text) * CDbl(lblTenor.Text)) / 100, 0)
    End Sub

    Public Function GetNumInst(ByVal intInstTerm As Integer, ByVal tenor As Integer) As Integer
        Select Case intInstTerm
            Case 1
                Return CInt(tenor / 1)
            Case 2
                Return CInt(tenor / 3)
            Case 3
                Return CInt(tenor / 6)
            Case 6
                Return CInt(tenor / 12)
            Case Else
                Return 0
        End Select
    End Function

End Class