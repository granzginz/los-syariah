﻿#Region "Import"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Linq
Imports System.Web.Services

#End Region

Public Class IncentiveDataAgriculture
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabAgriculture
    Protected WithEvents UcRefundPremi As UcIncentiveGridAgri
    Protected WithEvents UcRefundBunga As UcIncentiveGridAgri
    Protected WithEvents UcRefundBiayaAdmin As UcIncentiveGridAgri
    Protected WithEvents UcRefundBiayaProvisi As UcIncentiveGridAgri


#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Set(ByVal SupplierID As String)
            ViewState("SupplierID") = SupplierID
        End Set
        Get
            Return ViewState("SupplierID").ToString
        End Get
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property SupplierEmployeeID() As String
        Get
            Return ViewState("SupplierEmployeeID").ToString
        End Get
        Set(ByVal SupplierEmployeeID As String)
            ViewState("SupplierEmployeeID") = SupplierEmployeeID
        End Set
    End Property
    Property SupplierEmployeePosition() As String
        Get
            Return ViewState("SupplierEmployeePosition").ToString
        End Get
        Set(ByVal SupplierEmployeePosition As String)
            ViewState("SupplierEmployeePosition") = SupplierEmployeePosition
        End Set
    End Property
    Property TransID() As String
        Get
            Return ViewState("TransID").ToString
        End Get
        Set(ByVal TransID As String)
            ViewState("TransID") = TransID
        End Set
    End Property
    Property PolaTransaksi() As String
        Get
            Return ViewState("PolaTransaksi").ToString
        End Get
        Set(ByVal PolaTransaksi As String)
            ViewState("PolaTransaksi") = PolaTransaksi
        End Set
    End Property
    Property isButtonSave() As Boolean
        Get
            Return CBool(ViewState("isButtonSave").ToString)
        End Get
        Set(value As Boolean)
            ViewState("isButtonSave") = value
        End Set
    End Property
    Public Property OTR As String
        Set(value As String)
            ViewState("OTR") = value
        End Set
        Get
            Return ViewState("OTR").ToString
        End Get
    End Property
    Public Property Tenor As String
        Set(value As String)
            ViewState("Tenor") = value
        End Set
        Get
            Return ViewState("Tenor").ToString
        End Get
    End Property
    Public Property FlatRate As String
        Set(value As String)
            ViewState("FlatRate") = value
        End Set
        Get
            Return ViewState("FlatRate").ToString
        End Get
    End Property
    Public Property Admin As String
        Set(value As String)
            ViewState("Admin") = value
        End Set
        Get
            Return ViewState("Admin").ToString
        End Get
    End Property
    Public Property Other As String
        Set(value As String)
            ViewState("Other") = value
        End Set
        Get
            Return ViewState("Other").ToString
        End Get
    End Property
    Public Property AsuransiCoy As String
        Set(value As String)
            ViewState("AsuransiCoy") = value
        End Set
        Get
            Return ViewState("AsuransiCoy").ToString
        End Get
    End Property
    Public Property Provisi As String
        Set(value As String)
            ViewState("Provisi") = value
        End Set
        Get
            Return ViewState("Provisi").ToString
        End Get
    End Property
    Public Property NTF As String
        Set(value As String)
            ViewState("ntf") = value
        End Set
        Get
            Return ViewState("ntf").ToString
        End Get
    End Property
    Public Property TotalBunga As String
        Set(value As String)
            ViewState("TotalBunga") = value
        End Set
        Get
            Return ViewState("TotalBunga").ToString
        End Get
    End Property
    Private Property TotalDebet() As Decimal
        Get
            Return (CType(ViewState("TotalDebet"), Decimal))
        End Get
        Set(ByVal TotalDebet As Decimal)
            ViewState("TotalDebet") = TotalDebet
        End Set
    End Property
    Private Property TotalKredit() As Decimal
        Get
            Return (CType(ViewState("TotalKredit"), Decimal))
        End Get
        Set(ByVal TotalKredit As Decimal)
            ViewState("TotalKredit") = TotalKredit
        End Set
    End Property
    Private Property DownPayment() As String
        Get
            Return (CType(ViewState("DownPayment"), String))
        End Get
        Set(ByVal TotalDebet As String)
            ViewState("DownPayment") = TotalDebet
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oController As New IncentiveCardController
    Private oRefundInsentifController As New RefundInsentifController
    Private m_Controller As New SupplierController
    Private m_Insentif As New RefundInsentifController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub
        If Not Page.IsPostBack Then

            Me.ApplicationID = Request("ApplicationID")
            isButtonSave = True
            InitialPageLoad()

            pnlBalance.Visible = False
            ' btnSave.Visible = True

            ModVar.StrConn = GetConnectionString()
            ModVar.SuppilerID = Me.SupplierID
            ModVar.ApplicationID = Me.ApplicationID
            ModVar.BranchId = Me.sesBranchId.Replace("'", "")
            ModVar.BussinessDate = Me.BusinessDate
            Me.BranchID = Me.sesBranchId.Replace("'", "")
        End If

        If Not IsNothing(Request("btnSave")) Then
            Dim m_Insentif As New RefundInsentifController
            Dim oRefund = New Parameter.RefundInsentif
            Dim status As String
            With oRefund
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BusinessDate = Me.BusinessDate
            End With
            status = m_Insentif.UpdateDateEntryInsentif(oRefund)
            If status = "true" Then
                ShowMessage(lblMessage, "Distribusi alokasi selesai", False)
            Else
                ShowMessage(lblMessage, "Nilai Alokasi masih belum komplit, periksa kembali", True)
                'Session("msgErr") = True
                'Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
            End If

            Dim oParBalance As New Parameter.RefundInsentif
            Dim dataBalance As DataTable
            With oParBalance
                .BusinessDate = Me.BusinessDate
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
            End With
            m_Insentif.GoLiveJournalDummy(oParBalance)
            dataBalance = oParBalance.ListData.Tables(0)

            dtgJournal.DataSource = dataBalance
            dtgJournal.DataBind()

            If TotalDebet = TotalKredit Then
                ucApplicationTab1.ApplicationID = Me.ApplicationID
                ucApplicationTab1.selectedTab("Refund")
                ucApplicationTab1.setLink()
                ShowMessage(lblMessage, "Data saved!", False)
                pnlBalance.Visible = True
            Else
                pnlBalance.Visible = True
                ShowMessage(lblMessage, "Data Not Balance, Debit : " & FormatNumber(TotalDebet, 0) & " Credit : " & FormatNumber(TotalKredit, 0) & "", True)
            End If

        End If

        'If CBool(Session("msgErr")) Then
        '    ShowMessage(lblMessage, "Nilai Alokasi masih belum komplit, periksa kembali", True)
        '    Session.Remove("msgErr")
        'End If
        'If CBool(Session("msgSucc")) Then
        '    ShowMessage(lblMessage, "Distribusi alokasi selesai", False)
        '    Session.Remove("msgSucc")
        'End If

    End Sub

    Private Sub dtgJournal_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgJournal.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblNo As New Label
            Dim lblPost As New Label
            Dim lblAmount As New Label
            Dim lblDebet As New Label
            Dim lblKredit As New Label

            lblNo = CType(e.Item.FindControl("lblNo"), Label)
            lblPost = CType(e.Item.FindControl("lblPost"), Label)
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            lblDebet = CType(e.Item.FindControl("lblDebet"), Label)
            lblKredit = CType(e.Item.FindControl("lblKredit"), Label)

            lblNo.Text = (e.Item.ItemIndex + 1).ToString

            If lblPost.Text.ToUpper.Trim = "C" Then
                lblKredit.Text = FormatNumber(lblAmount.Text, 0)
                TotalKredit = TotalKredit + CDec(lblKredit.Text)
            Else
                lblDebet.Text = FormatNumber(lblAmount.Text, 0)
                TotalDebet = TotalDebet + CDec(lblDebet.Text)
            End If

        ElseIf e.Item.ItemType = ListItemType.Footer Then
            Dim lblDebetTotal As New Label
            Dim lblKreditTotal As New Label

            lblDebetTotal = CType(e.Item.FindControl("lblDebetTotal"), Label)
            lblKreditTotal = CType(e.Item.FindControl("lblKreditTotal"), Label)
            lblDebetTotal.Text = FormatNumber(TotalDebet, 0)
            lblKreditTotal.Text = FormatNumber(TotalKredit, 0)
        End If
    End Sub


    'Private Sub ImbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
    '    Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    'End Sub
    Private Sub btnSave_Check()
        Dim m_Insentif As New RefundInsentifController
        Dim oRefund = New Parameter.RefundInsentif
        Dim status As String
        With oRefund
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
        End With
        status = m_Insentif.UpdateDateEntryInsentif(oRefund)
        If status = "true" Then
            Session("msgSucc") = True
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        Else
            Session("msgErr") = True
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)
        End If

    End Sub
    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.SupplierID = .SupplierID
            Me.PolaTransaksi = .PolaTransaksi
        End With

        With ucApplicationTab1
            .ApplicationID = Me.ApplicationID
            .selectedTab("Refund")
            .setLink()
        End With

        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        BindDataAlokasi()


        BindGridAlokasi(UcRefundPremi, "TDI", lblRefundPremi)
        BindGridAlokasi(UcRefundBunga, "NPV", lblRefundBunga)
        BindGridAlokasi(UcRefundBiayaProvisi, "PRS", lblRefundBiayaProvisi)
        BindGridAlokasi(UcRefundBiayaLain, "TDS", lblRefundBiayaLain)

        BindGridSumPerOrang()



    End Sub
    Sub BindDataAlokasi()
        Dim oCustom As New Parameter.RefundInsentif

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " agreement.ApplicationID = '" & Me.ApplicationID & "'"
            .SPName = "spGetAlokasiInsentif"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            lblRefundPremi.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundPremi").ToString, 0)
            lblRefundBunga.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBunga").ToString, 0)
            'lblRefundBiayaAdmin.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundAdmin").ToString, 0)
            lblRefundBiayaLain.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBiayaLain").ToString, 0)
            lblRefundBiayaProvisi.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBiayaProvisi").ToString, 0)

            lblRefundPremiPersen.Text = "(" & FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundPremiPercent").ToString, 2) & "%)"
            lblRefundBungaPersen.Text = "(" & FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBungaPercent").ToString, 2) & "%)"
            lblRefundBiayaLainPersen.Text = "(" & FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBiayaLainPercent").ToString, 2) & "%)"
            'lblRefundBiayaAdminPersen.Text = "(" & FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundAdminPercent").ToString, 2) & "%)"
            lblRefundBiayaProvisiPersen.Text = "(" & FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBiayaProvisiPercent").ToString, 2) & "%)"

            Me.OTR = oCustom.ListDataTable.Rows(0).Item("TotalOTR").ToString
            Me.Admin = oCustom.ListDataTable.Rows(0).Item("AdminFee").ToString
            Me.Other = oCustom.ListDataTable.Rows(0).Item("OtherFee").ToString
            Me.Tenor = oCustom.ListDataTable.Rows(0).Item("tenor").ToString
            Me.FlatRate = oCustom.ListDataTable.Rows(0).Item("FlatRate").ToString
            Me.Provisi = oCustom.ListDataTable.Rows(0).Item("ProvisionFee").ToString
            Me.AsuransiCoy = oCustom.ListDataTable.Rows(0).Item("SelisihPremi").ToString
            Me.NTF = oCustom.ListDataTable.Rows(0).Item("ntf").ToString
            Me.TotalBunga = oCustom.ListDataTable.Rows(0).Item("TotalBunga").ToString
            DownPayment = oCustom.ListDataTable.Rows(0).Item("DownPayment").ToString
        End If

    End Sub
    Sub BindGridAlokasi(ByVal uc As UcIncentiveGridAgri, ByVal tipe As String, ByVal x As Label)
        uc.SupplierID = Me.SupplierID
        uc.ApplicationID = Me.ApplicationID
        uc.BranchID = Me.sesBranchId.Replace("'", "")
        uc.BussinessDate = CStr(Me.BusinessDate)
        uc.AlokasiPremiAsuransi = x.Text

        uc.Admin = Me.Admin
        uc.OTR = Me.OTR
        uc.Tenor = Me.Tenor
        uc.FlatRate = Me.FlatRate
        uc.Provisi = Me.Provisi
        uc.AsuransiCoy = Me.AsuransiCoy
        uc.NTF = Me.NTF
        'uc.TotalBunga = Me.TotalBunga
        uc.TotalBunga = (CDbl(Me.OTR) - CDbl(DownPayment)).ToString
        uc.Other = Me.Other

        'If isButtonSave = True Then
        '    If CDbl(uc.AlokasiPremiAsuransi.ToString) > 0 Then
        '        isButtonSave = False
        '    End If
        'End If

        uc.OnClientClick = "return AddNewRecordJQ('" & uc.dtg.ClientID & "');"
        uc.OnSaveClick = "return Apply('" & uc.dtg.ClientID & "');"

        Dim ddlJabatan As New DropDownList
        Dim ddlPenerima As New DropDownList
        Dim ddlTransID As New DropDownList
        Dim listRefund As New List(Of Parameter.RefundInsentif)
        Dim RowData As New List(Of Parameter.RefundInsentif)
        Dim lblPPH As New Label
        Dim lblTarifPajak, lblInsentifGross, lblNilaiPajak, lblInsentifNet As New Label
        Dim txtInsentif, txtpersentase As New TextBox
        Dim strEmpPos As String = ""
        Dim defPenerima As String = ""
        Dim defTarifPajak As String = ""
        Dim lastIndex As Integer

        BindGridForDB(listRefund, tipe)

        If listRefund.Count = 0 Then
            uc.AddRecord()
            uc.BindddlTransID(0, tipe)
            Exit Sub
        End If

        'Block untuk ambil defualt sales dan spv
        Dim oCustom As New Parameter.RefundInsentif
        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " ApplicationID = '" & Me.ApplicationID & "'"
            .SPName = "spGetSalesSupervisor"
        End With

        For index = 0 To listRefund.Count - 1
            uc.AddRecord()
        Next

        For index = 0 To listRefund.Count - 1
            Dim Row As New Parameter.RefundInsentif
            Row = listRefund(index)

            ddlJabatan = CType(uc.dtg.Items(index).FindControl("ddlJabatan"), DropDownList)
            ddlPenerima = CType(uc.dtg.Items(index).FindControl("ddlPenerima"), DropDownList)
            txtpersentase = CType(uc.dtg.Items(index).FindControl("txtProsentase"), TextBox)
            txtInsentif = CType(uc.dtg.Items(index).FindControl("txtInsentif"), TextBox)
            lblInsentifGross = CType(uc.dtg.Items(index).FindControl("lblInsentifGross"), Label)
            lblPPH = CType(uc.dtg.Items(index).FindControl("lblPPH"), Label)
            lblTarifPajak = CType(uc.dtg.Items(index).FindControl("lblTarifPajak"), Label)
            lblNilaiPajak = CType(uc.dtg.Items(index).FindControl("lblNilaiPajak"), Label)
            lblInsentifNet = CType(uc.dtg.Items(index).FindControl("lblInsentifNet"), Label)
            ddlTransID = CType(uc.dtg.Items(index).FindControl("ddlTransID"), DropDownList)

            ddlJabatan.SelectedIndex = ddlJabatan.Items.IndexOf(ddlJabatan.Items.FindByValue(Row.SupplierEmployeePosition.Trim))

            uc.BindddlPenerima(ddlJabatan, index)
            uc.BindddlTransID(index, tipe)

            ddlPenerima.SelectedIndex = ddlPenerima.Items.IndexOf(ddlPenerima.Items.FindByValue(Row.SupplierEmployeeID))
            ddlTransID.SelectedIndex = ddlTransID.Items.IndexOf(ddlTransID.Items.FindByValue(Row.TransID))

            txtpersentase.Text = FormatNumber(Row.persentase, 2)
            txtInsentif.Text = FormatNumber(Row.NilaiInsentif, 0)
            lblInsentifGross.Text = FormatNumber(Row.NilaiInsentif, 0)
            lblPPH.Text = Row.pph.Trim
            lblTarifPajak.Text = FormatNumber(Row.tarifPajak, 2)
            lblNilaiPajak.Text = FormatNumber(Row.nilaiPajak, 0)
            lblInsentifNet.Text = FormatNumber(Row.insentifNet, 0)
            lastIndex = index
        Next
        uc.BindTotal()

    End Sub

    Sub BindGridSumPerOrang()
        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " ApplicationID = '" & Me.ApplicationID & "' and EmployeeName is not null and SupplierEmployeePosition <> '' and SupplierEmployeePosition <> '-' "
            .SPName = "spGetSupplierIncentiveDailyDSum"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            entity = oCustom.ListDataTable
        Else
            With entity
                .Columns.Add(New DataColumn("EmployeePosition", GetType(String)))
                .Columns.Add(New DataColumn("EmployeeName", GetType(String)))
                .Columns.Add(New DataColumn("nilaiAlokasi", GetType(String)))
            End With
            Dim row As DataRow
            row = entity.NewRow()
            row("EmployeePosition") = "-"
            row("EmployeeName") = "-"
            row("nilaiAlokasi") = "-"
            entity.Rows.Add(row)
        End If
        dtgSum.DataSource = entity.DefaultView
        dtgSum.DataBind()
    End Sub


    Sub BindGridForDBInternal(ByVal listRefund As List(Of Parameter.RefundInsentif), ByVal TransID As String)
        Dim oCustom As New Parameter.RefundInsentif
        Dim oRefund As New Parameter.RefundInsentif

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " ApplicationID = '" & Me.ApplicationID & "' and BranchID = '" & Me.sesBranchId.Replace("'", "") & "' " & _
                        " and SupplierID = '" & Me.SupplierID & "' and TransID in (select TransID from tblAgreementMasterTransaction WHERE GroupTransID = '" & TransID & "') " & _
                        " and SupplierEmployeePosition = '-'"
            .SPName = "spGetSupplierIncentiveDailyD"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            For index = 0 To oCustom.ListDataTable.Rows.Count - 1
                oRefund = New Parameter.RefundInsentif
                oRefund.BranchId = oCustom.ListDataTable.Rows(index).Item("BranchId").ToString.Trim
                oRefund.ApplicationID = oCustom.ListDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                oRefund.TransID = oCustom.ListDataTable.Rows(index).Item("TransID").ToString.Trim

                oRefund.nilaiAlokasi = CDec(oCustom.ListDataTable.Rows(index).Item("nilaiAlokasi").ToString)
                oRefund.nilaiAlokasiPersen = CDec(oCustom.ListDataTable.Rows(index).Item("PersenAlokasi").ToString)
                listRefund.Add(oRefund)
            Next

        End If
    End Sub
    Sub BindGridForDB(ByVal listRefund As List(Of Parameter.RefundInsentif), ByVal TransID As String)
        Dim oCustom As New Parameter.RefundInsentif
        Dim oRefund As New Parameter.RefundInsentif
        Dim nilaiPajak, insentifNet As Decimal

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " ApplicationID = '" & Me.ApplicationID & "' and BranchID = '" & Me.sesBranchId.Replace("'", "") & "' " & _
                        " and SupplierID = '" & Me.SupplierID & "' and TransID in (select TransID from tblAgreementMasterTransaction WHERE GroupTransID = '" & TransID & "') " & _
                        " and SupplierEmployeePosition <> '-'"
            .SPName = "spGetSupplierIncentiveDailyD"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            For index = 0 To oCustom.ListDataTable.Rows.Count - 1
                oRefund = New Parameter.RefundInsentif
                oRefund.BranchId = oCustom.ListDataTable.Rows(index).Item("BranchId").ToString.Trim
                oRefund.ApplicationID = oCustom.ListDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                oRefund.SupplierID = oCustom.ListDataTable.Rows(index).Item("SupplierID").ToString.Trim
                oRefund.SupplierEmployeeID = oCustom.ListDataTable.Rows(index).Item("SupplierEmployeeID").ToString.Trim
                oRefund.SupplierEmployeePosition = oCustom.ListDataTable.Rows(index).Item("SupplierEmployeePosition").ToString.Trim
                oRefund.EmployeeName = oCustom.ListDataTable.Rows(index).Item("EmployeeName").ToString.Trim
                oRefund.TransID = oCustom.ListDataTable.Rows(index).Item("TransID").ToString.Trim
                oRefund.pph = oCustom.ListDataTable.Rows(index).Item("pph").ToString.Trim
                oRefund.tarifPajak = CDec(oCustom.ListDataTable.Rows(index).Item("tarifPajak").ToString)
                oRefund.NilaiInsentif = CDec(oCustom.ListDataTable.Rows(index).Item("IncentiveForRecepient"))
                If CDec(oCustom.ListDataTable.Rows(index).Item("nilaiPajak").ToString) = 0 Then
                    nilaiPajak = CDec(oCustom.ListDataTable.Rows(index).Item("IncentiveForRecepient")) * CDec(oCustom.ListDataTable.Rows(index).Item("tarifPajak")) / 100
                Else
                    nilaiPajak = CDec(oCustom.ListDataTable.Rows(index).Item("nilaiPajak").ToString)
                End If
                If CDec(oCustom.ListDataTable.Rows(index).Item("insentifNet").ToString) = 0 Then
                    insentifNet = CDec(oCustom.ListDataTable.Rows(index).Item("IncentiveForRecepient")) - nilaiPajak
                Else
                    insentifNet = CDec(oCustom.ListDataTable.Rows(index).Item("insentifNet").ToString)
                End If
                oRefund.nilaiPajak = nilaiPajak
                oRefund.insentifNet = insentifNet
                oRefund.nilaiAlokasi = CDec(oCustom.ListDataTable.Rows(index).Item("nilaiAlokasi").ToString)
                oRefund.persentase = CDec(oCustom.ListDataTable.Rows(index).Item("PersenAlokasi").ToString)
                listRefund.Add(oRefund)
            Next

        End If
    End Sub

    Public Function PredicateFunction(ByVal custom As Parameter.RefundInsentif) As Boolean
        Return (custom.SupplierEmployeePosition = Me.SupplierEmployeePosition)
    End Function
    <Serializable()> _
    Public Class ListEmp
        Public Property SupplierEmployeeID As String
        Public Property SupplierEmployeeName As String
    End Class
    <WebMethod()> _
    Public Shared Function GetEmployee(id As String, supplierID As String, ApplicationID As String) As List(Of ListEmp)
        Dim m_Insentif As New RefundInsentifController
        Dim rtn As New List(Of ListEmp)


        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable

        With oCustom
            .strConnection = ModVar.StrConn
            .WhereCond = "  SupplierEmployeePosition= '" & id & "' and SupplierID='" & supplierID & "' "
            If id = "SL" Then
                .WhereCond = .WhereCond & " and SupplierEmployeeID = (SELECT SalesmanID FROM dbo.Agreement WHERE ApplicationID = '" & ApplicationID & "')"
            End If
            .SPName = "spGetSupplierEmployee"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        entity = oCustom.ListDataTable

        For Each dt As DataRowView In entity.DefaultView
            rtn.Add(New ListEmp() With {
                .SupplierEmployeeID = dt("SupplierEmployeeID").ToString,
                .SupplierEmployeeName = dt("Name").ToString
            })
        Next

        Return rtn
    End Function
    '<WebMethod()> _
    'Public Shared Function GetTarifPajak(supplierEmployeeID As String, supplierEmployeePosition As String, supplierID As String) As String
    '    Dim m_Insentif As New RefundInsentifController
    '    Dim oCustom As New Parameter.RefundInsentif
    '    Dim tarif As String = ""

    '    With oCustom
    '        .strConnection = ModVar.StrConn
    '        .WhereCond = "  se.SupplierEmployeeID= '" & supplierEmployeeID & "' and se.supplierEmployeePosition = '" & supplierEmployeePosition & "' and se.SupplierID='" & supplierID & "' "
    '        .SPName = "spGetTarifBySupplierEmployee"
    '    End With

    '    oCustom = m_Insentif.GetSPBy(oCustom)

    '    tarif = LTrim(Str(oCustom.ListDataTable.Rows(0).Item("tarif")))
    '    Return tarif.ToString
    'End Function

    <WebMethod()> _
    Public Shared Sub Apply(jabatan As String,
                            employee As String,
                            persen As String,
                            insentif As String,
                            pph As String,
                            tarifpajak As String,
                            nilaipajak As String,
                            insentifnet As String,
                            TransID As String,
                            seq As Integer,
                            ApplicationID As String,
                            BranchID As String,
                            BussinessDate As String,
                            SupplierID As String)



        Dim dtrow As DataRow

        If seq = 1 Then
            Dim dt As New DataTable

            dt.Columns.Add("jabatan")
            dt.Columns.Add("employee")
            dt.Columns.Add("persen")
            dt.Columns.Add("insentif")
            dt.Columns.Add("pph")
            dt.Columns.Add("tarifpajak")
            dt.Columns.Add("nilaipajak")
            dt.Columns.Add("insentifnet")
            dt.Columns.Add("TransID")
            dt.Columns.Add("ApplicationID")
            dt.Columns.Add("BranchID")
            dt.Columns.Add("BussinessDate")
            dt.Columns.Add("SupplierID")
            ModVar.dtTable = dt
        End If

        dtrow = ModVar.dtTable.NewRow
        dtrow("jabatan") = jabatan
        dtrow("employee") = employee
        dtrow("persen") = persen
        dtrow("insentif") = insentif
        dtrow("pph") = pph
        dtrow("tarifpajak") = tarifpajak
        dtrow("nilaipajak") = nilaipajak
        dtrow("insentifnet") = insentifnet
        dtrow("TransID") = TransID
        dtrow("ApplicationID") = ApplicationID
        dtrow("BranchID") = BranchID
        dtrow("BussinessDate") = BussinessDate
        dtrow("SupplierID") = SupplierID
        ModVar.dtTable.Rows.Add(dtrow)
    End Sub
    <WebMethod()> _
    Public Shared Function SaveApply(ByVal GrouptransID As String,
                            ApplicationID As String,
                            BranchID As String,
                            BussinessDate As String, SupplierID As String) As String
        Dim oRefund = New Parameter.RefundInsentif
        Dim m_Insentif As New RefundInsentifController
        Dim status As String
        With oRefund
            .strConnection = ModVar.StrConn
            .BranchId = BranchID
            .ApplicationID = ApplicationID
            .SupplierID = SupplierID
            .TransID = GrouptransID
            .isInternal = "0"
        End With

        m_Insentif.SupplierIncentiveDailyDDelete(oRefund)

        For index = 0 To ModVar.dtTable.Rows.Count - 1
            If ApplicationID.ToString.Trim = ModVar.dtTable.Rows(index).Item("ApplicationID").ToString.Trim Then
                With oRefund
                    .strConnection = ModVar.StrConn
                    .BranchId = ModVar.dtTable.Rows(index).Item("BranchID").ToString.Trim
                    .ApplicationID = ModVar.dtTable.Rows(index).Item("ApplicationID").ToString.Trim
                    .SupplierID = ModVar.dtTable.Rows(index).Item("SupplierID").ToString.Trim
                    .SupplierEmployeeID = ModVar.dtTable.Rows(index).Item("employee").ToString.Trim
                    .SupplierEmployeePosition = ModVar.dtTable.Rows(index).Item("jabatan").ToString.Trim
                    .EmployeeName = ""
                    .pph = ModVar.dtTable.Rows(index).Item("pph").ToString.Trim
                    .tarifPajak = CDec(ModVar.dtTable.Rows(index).Item("tarifpajak").ToString.Trim)
                    .nilaiPajak = CDec(ModVar.dtTable.Rows(index).Item("nilaipajak").ToString.Trim)
                    .insentifNet = CDec(ModVar.dtTable.Rows(index).Item("insentifnet").ToString.Trim)
                    .nilaiAlokasi = CDec(ModVar.dtTable.Rows(index).Item("insentif").ToString.Trim)
                    .nilaiAlokasiPersen = CDec(ModVar.dtTable.Rows(index).Item("persen").ToString.Trim)
                    .TransID = ModVar.dtTable.Rows(index).Item("TransID").ToString.Trim
                End With

                status = m_Insentif.SupplierIncentiveDailyDSave(oRefund)
            End If
        Next

        With oRefund
            .strConnection = ModVar.StrConn
            .BranchId = ModVar.BranchId
            .ApplicationID = ModVar.ApplicationID
            .BusinessDate = ModVar.BussinessDate
        End With
        m_Insentif.SupplierINCTVDailyDAdd(oRefund)  '--> insert ke AccountPayable

        Return status

    End Function
    <WebMethod()> _
    Public Shared Sub ApplyInternal(penggunaan As String,
                            persen As String,
                            insentif As String,
                            TransID As String,
                            seq As Integer,
                            ApplicationID As String,
                            BranchID As String,
                            BussinessDate As String,
                            SupplierID As String)

        Dim dtrow As DataRow

        If seq = 1 Then
            Dim dt As New DataTable

            dt.Columns.Add("penggunaan")
            dt.Columns.Add("persen")
            dt.Columns.Add("insentif")
            dt.Columns.Add("TransID")
            dt.Columns.Add("ApplicationID")
            dt.Columns.Add("BranchID")
            dt.Columns.Add("BussinessDate")
            dt.Columns.Add("SupplierID")
            ModVar.dtTableI = dt
        End If

        dtrow = ModVar.dtTableI.NewRow
        dtrow("penggunaan") = penggunaan
        dtrow("persen") = persen
        dtrow("insentif") = insentif
        dtrow("TransID") = TransID
        dtrow("ApplicationID") = ApplicationID
        dtrow("BranchID") = BranchID
        dtrow("BussinessDate") = BussinessDate
        dtrow("SupplierID") = SupplierID
        ModVar.dtTableI.Rows.Add(dtrow)

    End Sub
    <WebMethod()> _
    Public Shared Function SaveApplyInternal(ByVal GrouptransID As String,
                            ApplicationID As String,
                            BranchID As String,
                            BussinessDate As String, SupplierID As String) As String
        Dim m_Insentif As New RefundInsentifController
        Dim oRefund = New Parameter.RefundInsentif
        Dim status As String
        With oRefund
            .strConnection = ModVar.StrConn
            .BranchId = BranchID
            .ApplicationID = ApplicationID
            .SupplierID = SupplierID
            .TransID = GrouptransID
            .isInternal = "1"
        End With

        m_Insentif.SupplierIncentiveDailyDDelete(oRefund)

        For index = 0 To ModVar.dtTableI.Rows.Count - 1
            If ApplicationID.ToString.Trim = ModVar.dtTableI.Rows(index).Item("ApplicationID").ToString.Trim Then
                With oRefund
                    .strConnection = ModVar.StrConn
                    .BranchId = ModVar.dtTableI.Rows(index).Item("BranchID").ToString.Trim
                    .ApplicationID = ModVar.dtTableI.Rows(index).Item("ApplicationID").ToString.Trim
                    .SupplierID = ModVar.dtTableI.Rows(index).Item("SupplierID").ToString.Trim
                    .SupplierEmployeeID = "-"
                    .SupplierEmployeePosition = "-"
                    .EmployeeName = ""
                    .pph = "-"
                    .tarifPajak = 0
                    .nilaiPajak = 0
                    .insentifNet = 0
                    .nilaiAlokasi = CDec(ModVar.dtTableI.Rows(index).Item("insentif").ToString.Trim)
                    .nilaiAlokasiPersen = CDec(ModVar.dtTableI.Rows(index).Item("persen").ToString.Trim)
                    .TransID = ModVar.dtTableI.Rows(index).Item("TransID").ToString.Trim
                End With

                status = m_Insentif.SupplierIncentiveDailyDSave(oRefund)
            End If
        Next
        Return status
    End Function
End Class