﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApplicationAgriculture_002.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ApplicationAgriculture_002" %>

<%@ Register TagPrefix="uc1" TagName="UcAgreementList" Src="../../../../webform.UserController/UcAgreementList.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Entri Aplikasi</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                CUSTOMER AGRICULTURE</h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama Customer
            </label>
            <asp:Label ID="lblCustName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DAFTAR KONTRAK
            </h4>
        </div>
    </div>
    <div class="form_box">
        <uc1:ucagreementlist id="UcAgreementList" runat="server">
                    </uc1:ucagreementlist>
    </div>

    <div class="form_button">
        <asp:Button ID="btnNewApp" runat="server" Text="New Application" CssClass="small button blue " />
        <asp:Button ID="btnExit" runat="server" Text="Exit" CssClass="small button gray" />
    </div>
    </form>
</body>
</html>
