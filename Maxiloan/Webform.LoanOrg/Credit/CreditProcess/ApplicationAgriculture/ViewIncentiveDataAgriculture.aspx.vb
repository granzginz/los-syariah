﻿#Region "Import"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Linq
Imports System.Web.Services

#End Region

Public Class ViewIncentiveDataAgriculture
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabAgriculture
    Protected WithEvents UcRefundPremi As UcIncentiveGridView
    Protected WithEvents UcRefundBunga As UcIncentiveGridView
    Protected WithEvents UcPremiProvisi As UcIncentiveGridView
    Protected WithEvents UcBiayaLainnya As UcIncentiveGridView


#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Set(ByVal SupplierID As String)
            ViewState("SupplierID") = SupplierID
        End Set
        Get
            Return ViewState("SupplierID").ToString
        End Get
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property SupplierEmployeeID() As String
        Get
            Return ViewState("SupplierEmployeeID").ToString
        End Get
        Set(ByVal SupplierEmployeeID As String)
            ViewState("SupplierEmployeeID") = SupplierEmployeeID
        End Set
    End Property
    Property SupplierEmployeePosition() As String
        Get
            Return ViewState("SupplierEmployeePosition").ToString
        End Get
        Set(ByVal SupplierEmployeePosition As String)
            ViewState("SupplierEmployeePosition") = SupplierEmployeePosition
        End Set
    End Property
    Property TransID() As String
        Get
            Return ViewState("TransID").ToString
        End Get
        Set(ByVal TransID As String)
            ViewState("TransID") = TransID
        End Set
    End Property
    Property PolaTransaksi() As String
        Get
            Return ViewState("PolaTransaksi").ToString
        End Get
        Set(ByVal PolaTransaksi As String)
            ViewState("PolaTransaksi") = PolaTransaksi
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Property Style() As String
        Get
            Return ViewState("Style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property

    Public Property pageSource() As String
        Get
            Return CType(ViewState("pageSource"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("pageSource") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oController As New IncentiveCardController
    Private oRefundInsentifController As New RefundInsentifController
    Private m_Controller As New SupplierController
    Private m_Insentif As New RefundInsentifController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID").ToString.Trim

            InitialPageLoad()
            ModVar.StrConn = GetConnectionString()
            ModVar.SuppilerID = Me.SupplierID
            ModVar.ApplicationID = Me.ApplicationID
            ModVar.BranchId = Me.sesBranchId.Replace("'", "")
        End If
    End Sub
    'Private Sub ImbBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click

    '    Dim x As String = Request("page").Trim
    '    ' If x.ToString = Nothing Then Me.pageSource = ""
    '    If x.ToString = "ViewStatementOfAccount" Then
    '        Me.ApplicationID = Request("ApplicationID").ToString.Trim
    '        Me.CustomerName = Request("CustomerName").ToString.Trim
    '        Me.Style = Request("Style").ToString.Trim
    '        Me.CustomerID = Request("CustomerID").ToString.Trim
    '        Me.AgreementNo = Request("AgreementNo").ToString.Trim
    '        Response.Redirect("../../viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    '    Else
    '        Me.CustomerName = ""
    '        Me.AgreementNo = "-"
    '        Response.Redirect("ViewApplication.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&AgreementNo=" & Me.AgreementNo & "&CustomerName=" & Me.CustomerName & "&Style=ViewApplication&CustomerID=" & Me.CustomerID)
    '    End If
    'End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.SupplierID = .SupplierID
            Me.PolaTransaksi = .PolaTransaksi
        End With

        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        ucApplicationTab1.ApplicationID = Me.ApplicationID
        ucApplicationTab1.selectedTab("Refund")
        ucApplicationTab1.setLink()

        BindDataAlokasi()

        BindGridAlokasi(UcRefundPremi, "TDI", lblRefundPremi)
        BindGridAlokasi(UcRefundBunga, "NPV", lblRefundBunga)
        BindGridAlokasi(UcPremiProvisi, "PRS", lblPremiProvisi)
        BindGridAlokasi(UcBiayaLainnya, "TDS", lblBiayaLainnya)

        BindGridSumPerOrang()

        'BindGridInternal(ucTitipanPremiAsuransi, "TDI", lblAlokasiPremiAsuransiInternal)
        'BindGridInternal(UcTitipanRefundBunga, "NPV", lblTitipanRefundBunga)
        'BindGridInternal(UcTitipanProvisi, "PRS", lblTitipanProvisi)
        'BindGridInternal(UcTitipanBiayalainnya, "TDS", lblTitipanBiayaLainnya)

    End Sub
    Sub BindDataAlokasi()
        Dim oCustom As New Parameter.RefundInsentif

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " agreement.ApplicationID = '" & Me.ApplicationID & "'"
            .SPName = "spGetAlokasiInsentif"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then

            lblRefundPremi.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundPremi").ToString, 0)
            If lblRefundPremi.Text = "0" Then
                UcRefundPremi.Visible = False
            End If

            lblRefundBunga.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBunga").ToString, 0)
            If lblRefundBunga.Text = "0" Then
                UcRefundBunga.Visible = False
            End If

            lblPremiProvisi.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBiayaProvisi").ToString, 0)
            If lblPremiProvisi.Text = "0" Then
                UcPremiProvisi.Visible = False
            End If

            lblBiayaLainnya.Text = FormatNumber(oCustom.ListDataTable.Rows(0).Item("RefundBiayaLain").ToString, 0)
            If lblBiayaLainnya.Text = "0" Then
                UcBiayaLainnya.Visible = False
            End If

        End If

    End Sub
    Function GetJabatan(ByVal kode As String) As String
        Dim retValue As String
        Select Case kode.ToLower
            Case "gm" : retValue = "General Manager"
            Case "om" : retValue = "Operation Manager"
            Case "bm" : retValue = "Branch Manager"
            Case "sv" : retValue = "Sales Supervisor"
            Case "sl" : retValue = "Sales Person"
            Case "am" : retValue = "Supplier Admin"
            Case "fi" : retValue = "F & I"
            Case "so" : retValue = "Supplier Office"
            Case "fm" : retValue = "F & I Manager"
            Case "ot" : retValue = "Other"
            Case Else : retValue = "All"

        End Select
        Return retValue
    End Function

    Sub BindGridAlokasi(ByVal uc As UcIncentiveGridView, ByVal tipe As String, ByVal x As Label)
        uc.SupplierID = Me.SupplierID
        uc.ApplicationID = Me.ApplicationID
        uc.AlokasiPremiAsuransi = x.Text

        uc.OnClientClick = "return AddNewRecordJQ('" & uc.dtg.ClientID & "');"
        uc.OnSaveClick = "return Apply('" & uc.dtg.ClientID & "');"

        Dim lblddlJabatan As New Label
        Dim lblddlPenerima As New Label
        Dim lblddlTransID As New Label
        Dim listRefund As New List(Of Parameter.RefundInsentif)
        Dim RowData As New List(Of Parameter.RefundInsentif)
        Dim lblPPH As New Label
        Dim lblTarifPajak, lblInsentifGross, lblNilaiPajak, lblInsentifNet As New Label
        Dim lblInsentif, lblpersentase As New Label
        Dim strEmpPos As String = ""
        Dim defPenerima As String = ""
        Dim defTarifPajak As String = ""
        Dim lastIndex As Integer

        BindGridForDB(listRefund, tipe)

        If listRefund.Count = 0 Then
            uc.AddRecord()
            uc.BindlblddlTransID(0, tipe)
            Exit Sub
        End If

        'Block untuk ambil defualt sales dan spv
        Dim oCustom As New Parameter.RefundInsentif
        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " ApplicationID = '" & Me.ApplicationID & "'"
            .SPName = "spGetSalesSupervisor"
        End With

        For index = 0 To listRefund.Count - 1
            uc.AddRecord()
        Next

        For index = 0 To listRefund.Count - 1
            Dim Row As New Parameter.RefundInsentif
            Row = listRefund(index)

            lblddlJabatan = CType(uc.dtg.Items(index).FindControl("lblddlJabatan"), Label)
            lblddlPenerima = CType(uc.dtg.Items(index).FindControl("lblddlPenerima"), Label)
            lblpersentase = CType(uc.dtg.Items(index).FindControl("lblProsentase"), Label)
            lblInsentif = CType(uc.dtg.Items(index).FindControl("lblInsentif"), Label)
            lblInsentifGross = CType(uc.dtg.Items(index).FindControl("lblInsentifGross"), Label)
            lblPPH = CType(uc.dtg.Items(index).FindControl("lblPPH"), Label)
            lblTarifPajak = CType(uc.dtg.Items(index).FindControl("lblTarifPajak"), Label)
            lblNilaiPajak = CType(uc.dtg.Items(index).FindControl("lblNilaiPajak"), Label)
            lblInsentifNet = CType(uc.dtg.Items(index).FindControl("lblInsentifNet"), Label)
            lblddlTransID = CType(uc.dtg.Items(index).FindControl("lblddlTransID"), Label)

            lblddlJabatan.Text = Row.SupplierEmployeePosition.Trim
            lblddlJabatan.Text = GetJabatan(Row.SupplierEmployeePosition.Trim)
            uc.BindlblddlPenerima(lblddlJabatan, index)
            uc.BindlblddlTransID(index, tipe)

            lblddlPenerima.Text = Row.EmployeeName
            lblddlTransID.Text = Row.TransID

            lblpersentase.Text = FormatNumber(Row.persentase, 2)
            lblInsentif.Text = FormatNumber(Row.nilaiAlokasi, 0)
            lblInsentifGross.Text = FormatNumber(Row.nilaiAlokasi, 0)
            lblPPH.Text = Row.pph.Trim
            lblTarifPajak.Text = FormatNumber(Row.tarifPajak, 2)
            lblNilaiPajak.Text = FormatNumber(Row.nilaiPajak, 0)
            lblInsentifNet.Text = FormatNumber(Row.insentifNet, 0)
            lastIndex = index
        Next
        uc.BindTotal()

    End Sub
    Sub BindGridInternal(ByVal uc As UcIncentiveInternalGridView, ByVal tipe As String, x As Label)
        uc.SupplierID = Me.SupplierID
        uc.ApplicationID = Me.ApplicationID
        uc.AlokasiPremiAsuransiInternal = x.Text
        uc.OnSaveClick = "return ApplyInternal('" & uc.dtg.ClientID & "');"
        uc.OnClientClick = "return AddNewRecordInternal('" & uc.dtg.ClientID & "');"


        Dim lblddlPenggunaan As New Label
        Dim lblInsentif As New Label
        Dim lblProsentase As New Label
        Dim listRefund As New List(Of Parameter.RefundInsentif)
        Dim RowData As New List(Of Parameter.RefundInsentif)

        BindGridForDBInternal(listRefund, tipe)



        If listRefund.Count > 0 Then
            For a = 0 To listRefund.Count - 1
                uc.AddRecord()
            Next
            For index = 0 To listRefund.Count - 1
                Dim Row As New Parameter.RefundInsentif
                Row = listRefund(index)
                uc.BindDDl(tipe, index)

                lblddlPenggunaan = CType(uc.dtg.Items(index).FindControl("lblddlPenggunaan"), Label)
                lblInsentif = CType(uc.dtg.Items(index).FindControl("lblInsentif"), Label)
                lblProsentase = CType(uc.dtg.Items(index).FindControl("lblProsentase"), Label)

                lblddlPenggunaan.Text = Row.TransIDDescription.Trim

                lblInsentif.Text = FormatNumber(Row.nilaiAlokasi, 0)
                lblProsentase.Text = FormatNumber(Row.nilaiAlokasiPersen, 2)
            Next
            uc.BindTotal()
        Else
            uc.AddRecord()
            uc.BindDDl(tipe, 0)
        End If
    End Sub
    Sub BindGridAlokasiBAK(ByVal uc As UcIncentiveGridView, ByVal tipe As String, ByVal x As Label)
        uc.SupplierID = Me.SupplierID
        uc.AlokasiPremiAsuransi = x.Text

        uc.OnClientClick = "return AddNewRecordJQ('" & uc.dtg.ClientID & "');"
        uc.OnSaveClick = "return Apply('" & uc.dtg.ClientID & "');"

        If Me.PolaTransaksi.Trim = "94" Then
            uc.AddRecord()
            uc.AddRecord()
            uc.AddRecord()
            uc.AddRecord()
            uc.AddRecord()
            uc.AddRecord()
            uc.AddRecord()
        Else
            uc.AddRecord()
        End If

        Dim lblddlJabatan As New Label
        Dim lblddlPenerima As New Label
        Dim listRefund As New List(Of Parameter.RefundInsentif)
        Dim RowData As New List(Of Parameter.RefundInsentif)
        Dim lblPPH As New Label
        Dim lblTarifPajak, lblInsentifGross, lblNilaiPajak, lblInsentifNet As New Label
        Dim lblInsentif, lblpersentase As New Label
        Dim strEmpPos As String = ""
        Dim defPenerima As String = ""
        Dim defTarifPajak As String = ""

        BindGridForDB(listRefund, tipe)

        'Block untuk ambil defualt sales dan spv
        Dim oCustom As New Parameter.RefundInsentif
        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " ApplicationID = '" & Me.ApplicationID & "'"
            .SPName = "spGetSalesSupervisor"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        ModVar.StrConn = GetConnectionString()
        'end  of block

        If Me.PolaTransaksi.Trim = "94" Then

            For index = 0 To uc.dtg.Items.Count - 1
                defPenerima = ""
                defTarifPajak = ""
                If index = 0 Then
                    strEmpPos = "SL"
                    If oCustom.ListDataTable.Rows.Count - 1 > 0 Then
                        defPenerima = oCustom.ListDataTable.Rows(0).Item("SalesmanID").ToString
                        defTarifPajak = GetTarifPajak(defPenerima, strEmpPos)
                    End If

                ElseIf index = 1 Then
                    strEmpPos = "SV"
                    If oCustom.ListDataTable.Rows.Count - 1 > 0 Then
                        defPenerima = oCustom.ListDataTable.Rows(0).Item("SupervisorID").ToString
                        defTarifPajak = GetTarifPajak(defPenerima, strEmpPos)
                    End If

                ElseIf index = 2 Then
                    strEmpPos = "FI"
                ElseIf index = 3 Then
                    strEmpPos = "BM"
                ElseIf index = 4 Then
                    strEmpPos = "OM"
                ElseIf index = 5 Then
                    strEmpPos = "GM"
                ElseIf index = 6 Then
                    strEmpPos = "AM"
                    If oCustom.ListDataTable.Rows.Count - 1 > 0 Then
                        defPenerima = oCustom.ListDataTable.Rows(0).Item("SupplierAdminID").ToString
                        defTarifPajak = GetTarifPajak(defPenerima, strEmpPos)
                    End If
                End If

                lblddlJabatan = CType(uc.dtg.Items(index).FindControl("lblddlJabatan"), Label)

                ' lblddlJabatan.Text = lblddlJabatan.FindByValue(strEmpPos)
                uc.BindlblddlPenerima(lblddlJabatan, index)

                lblddlPenerima = CType(uc.dtg.Items(index).FindControl("ddlPenerima"), Label)
                lblpersentase = CType(uc.dtg.Items(index).FindControl("lblProsentase"), Label)
                lblInsentif = CType(uc.dtg.Items(index).FindControl("lblInsentif"), Label)
                lblInsentifGross = CType(uc.dtg.Items(index).FindControl("lblInsentifGross"), Label)
                lblPPH = CType(uc.dtg.Items(index).FindControl("lblPPH"), Label)
                lblTarifPajak = CType(uc.dtg.Items(index).FindControl("lblTarifPajak"), Label)
                lblNilaiPajak = CType(uc.dtg.Items(index).FindControl("lblNilaiPajak"), Label)
                lblInsentifNet = CType(uc.dtg.Items(index).FindControl("lblInsentifNet"), Label)


                lblPPH.Text = "21"
                If defPenerima <> "" Then
                    lblddlPenerima.Text = defPenerima.ToString.Trim
                Else
                    If CDbl(lblddlPenerima.Text) = 2 Then
                        lblddlPenerima.Text = ""
                        defTarifPajak = GetTarifPajak(defPenerima, strEmpPos)
                    End If
                End If

                If defTarifPajak <> "" Then
                    lblTarifPajak.Text = FormatNumber(defTarifPajak, 2)
                End If


                RowData = New List(Of Parameter.RefundInsentif)
                Me.SupplierEmployeePosition = strEmpPos

                RowData = listRefund.FindAll(AddressOf PredicateFunction)

                If RowData IsNot Nothing And RowData.Count > 0 Then
                    lblddlPenerima.Text = RowData(0).SupplierEmployeeID.Trim
                    lblpersentase.Text = FormatNumber(RowData(0).persentase, 2)
                    lblInsentif.Text = FormatNumber(RowData(0).nilaiAlokasi, 0)
                    lblInsentifGross.Text = ""
                    lblPPH.Text = RowData(0).pph.Trim
                    lblTarifPajak.Text = FormatNumber(RowData(0).tarifPajak, 0)
                    lblNilaiPajak.Text = FormatNumber(RowData(0).nilaiPajak, 0)
                    lblInsentifNet.Text = FormatNumber(RowData(0).insentifNet, 0)
                    ' uc.BindInsentif(txtInsentif, index)
                End If

            Next

            For index = 0 To listRefund.Count - 1
                Dim Row As New Parameter.RefundInsentif
                Row = listRefund(index)
                Dim indexGrid As Integer = 6

                If Not Row.SupplierEmployeePosition.Trim = "SL" And Not Row.SupplierEmployeePosition.Trim = "SV" _
                    And Not Row.SupplierEmployeePosition.Trim = "FI" And Not Row.SupplierEmployeePosition.Trim = "BM" _
                    And Not Row.SupplierEmployeePosition.Trim = "OM" And Not Row.SupplierEmployeePosition.Trim = "GM" And Not Row.SupplierEmployeePosition.Trim = "AM" Then

                    uc.AddRecord()

                    lblddlJabatan = CType(uc.dtg.Items(indexGrid).FindControl("lblddlJabatan"), Label)
                    lblddlPenerima = CType(uc.dtg.Items(indexGrid).FindControl("lblddlPenerima"), Label)
                    lblPPH = CType(uc.dtg.Items(indexGrid).FindControl("lblPPH"), Label)
                    lblTarifPajak = CType(uc.dtg.Items(indexGrid).FindControl("lblTarifPajak"), Label)
                    lblInsentif = CType(uc.dtg.Items(indexGrid).FindControl("lblInsentif"), Label)

                    lblddlJabatan.Text = Row.SupplierEmployeePosition.Trim

                    uc.BindlblddlPenerima(lblddlJabatan, indexGrid)

                    lblddlPenerima.Text = Row.SupplierEmployeeID.Trim
                    lblpersentase.Text = FormatNumber(Row.persentase, 2)
                    lblInsentif.Text = FormatNumber(Row.nilaiAlokasi, 0)
                    lblInsentifGross.Text = ""
                    lblPPH.Text = Row.pph.Trim
                    lblTarifPajak.Text = FormatNumber(Row.tarifPajak, 0)
                    lblNilaiPajak.Text = FormatNumber(Row.nilaiPajak, 0)
                    lblInsentifNet.Text = FormatNumber(Row.insentifNet, 0)

                End If

                indexGrid = indexGrid + 1
            Next
        Else

            If listRefund.Count > 0 Then
                For index = 0 To listRefund.Count - 1
                    Dim Row As New Parameter.RefundInsentif
                    Row = listRefund(index)

                    If index > 0 And listRefund.Count > uc.dtg.Items.Count Then
                        uc.AddRecord()
                    End If
                    lblddlJabatan = CType(uc.dtg.Items(index).FindControl("lbllbllbllblddlJabatan"), Label)
                    lblddlPenerima = CType(uc.dtg.Items(index).FindControl("ddlPenerima"), Label)
                    lblpersentase = CType(uc.dtg.Items(index).FindControl("lblProsentase"), Label)
                    lblInsentif = CType(uc.dtg.Items(index).FindControl("lblInsentif"), Label)
                    lblInsentifGross = CType(uc.dtg.Items(index).FindControl("lblInsentifGross"), Label)
                    lblPPH = CType(uc.dtg.Items(index).FindControl("lblPPH"), Label)
                    lblTarifPajak = CType(uc.dtg.Items(index).FindControl("lblTarifPajak"), Label)
                    lblNilaiPajak = CType(uc.dtg.Items(index).FindControl("lblNilaiPajak"), Label)
                    lblInsentifNet = CType(uc.dtg.Items(index).FindControl("lblInsentifNet"), Label)

                    lblddlJabatan.Text = Row.SupplierEmployeePosition.Trim

                    uc.BindlblddlPenerima(lblddlJabatan, index)

                    lblddlPenerima.Text = Row.SupplierEmployeeID.Trim
                    lblpersentase.Text = FormatNumber(Row.persentase, 2)
                    lblInsentif.Text = FormatNumber(Row.nilaiAlokasi, 0)
                    lblInsentifGross.Text = ""
                    lblPPH.Text = Row.pph.Trim
                    lblTarifPajak.Text = FormatNumber(Row.tarifPajak, 0)
                    lblNilaiPajak.Text = FormatNumber(Row.nilaiPajak, 0)
                    lblInsentifNet.Text = FormatNumber(Row.insentifNet, 0)

                    ' uc.BindInsentif(txtInsentif, index)
                Next
            End If

        End If

        uc.BindTotal()
    End Sub
    Sub BindGridSumPerOrang()
        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " ApplicationID = '" & Me.ApplicationID & "' and EmployeeName is not null and SupplierEmployeePosition <> '' and SupplierEmployeePosition <> '-' "
            .SPName = "spGetSupplierIncentiveDailyDSum"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            entity = oCustom.ListDataTable
        Else
            With entity
                .Columns.Add(New DataColumn("EmployeePosition", GetType(String)))
                .Columns.Add(New DataColumn("EmployeeName", GetType(String)))
                .Columns.Add(New DataColumn("nilaiAlokasi", GetType(String)))
            End With
            Dim row As DataRow
            row = entity.NewRow()
            row("EmployeePosition") = "-"
            row("EmployeeName") = "-"
            row("nilaiAlokasi") = "-"
            entity.Rows.Add(row)
        End If
        dtgSum.DataSource = entity.DefaultView
        dtgSum.DataBind()
    End Sub


    Sub BindGridForDBInternal(ByVal listRefund As List(Of Parameter.RefundInsentif), ByVal TransID As String)
        Dim oCustom As New Parameter.RefundInsentif
        Dim oRefund As New Parameter.RefundInsentif

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " a.ApplicationID = '" & Me.ApplicationID & "' and a.BranchID = '" & Me.sesBranchId.Replace("'", "") & "' " & _
                        " and a.SupplierID = '" & Me.SupplierID & "' and a.TransID in (select TransID from tblAgreementMasterTransaction WHERE GroupTransID = '" & TransID & "') " & " and a.SupplierEmployeePosition = '-'"
            .SPName = "spGetSupplierIncentiveDailyDView"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            For index = 0 To oCustom.ListDataTable.Rows.Count - 1
                oRefund = New Parameter.RefundInsentif
                oRefund.BranchId = oCustom.ListDataTable.Rows(index).Item("BranchId").ToString.Trim
                oRefund.ApplicationID = oCustom.ListDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                oRefund.TransID = oCustom.ListDataTable.Rows(index).Item("TransID").ToString.Trim
                oRefund.TransIDDescription = oCustom.ListDataTable.Rows(index).Item("TransIDDescription").ToString.Trim
                oRefund.nilaiAlokasi = CDec(oCustom.ListDataTable.Rows(index).Item("nilaiAlokasi").ToString)
                oRefund.nilaiAlokasiPersen = CDec(oCustom.ListDataTable.Rows(index).Item("PersenAlokasi").ToString)
                listRefund.Add(oRefund)
            Next

        End If
    End Sub
    Sub BindGridForDB(ByVal listRefund As List(Of Parameter.RefundInsentif), ByVal TransID As String)
        Dim oCustom As New Parameter.RefundInsentif
        Dim oRefund As New Parameter.RefundInsentif

        With oCustom
            .strConnection = GetConnectionString()
            .WhereCond = " ApplicationID = '" & Me.ApplicationID & "' and BranchID = '" & Me.sesBranchId.Replace("'", "") & "' " & _
                        " and SupplierID = '" & Me.SupplierID & "' and TransID in (select TransID from tblAgreementMasterTransaction WHERE GroupTransID = '" & TransID & "') " & _
                        " and SupplierEmployeePosition <> '-'"
            .SPName = "spGetSupplierIncentiveDailyD"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        If oCustom.ListDataTable.Rows.Count > 0 Then
            For index = 0 To oCustom.ListDataTable.Rows.Count - 1
                oRefund = New Parameter.RefundInsentif
                oRefund.BranchId = oCustom.ListDataTable.Rows(index).Item("BranchId").ToString.Trim
                oRefund.ApplicationID = oCustom.ListDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                oRefund.SupplierID = oCustom.ListDataTable.Rows(index).Item("SupplierID").ToString.Trim
                oRefund.SupplierEmployeeID = oCustom.ListDataTable.Rows(index).Item("SupplierEmployeeID").ToString.Trim
                oRefund.SupplierEmployeePosition = oCustom.ListDataTable.Rows(index).Item("SupplierEmployeePosition").ToString.Trim
                oRefund.EmployeeName = oCustom.ListDataTable.Rows(index).Item("EmployeeName").ToString.Trim
                oRefund.TransID = oCustom.ListDataTable.Rows(index).Item("TransID").ToString.Trim
                oRefund.pph = oCustom.ListDataTable.Rows(index).Item("pph").ToString.Trim
                oRefund.tarifPajak = CDec(oCustom.ListDataTable.Rows(index).Item("tarifPajak").ToString)
                oRefund.nilaiPajak = CDec(oCustom.ListDataTable.Rows(index).Item("nilaiPajak").ToString)
                oRefund.insentifNet = CDec(oCustom.ListDataTable.Rows(index).Item("insentifNet").ToString)
                oRefund.nilaiAlokasi = CDec(oCustom.ListDataTable.Rows(index).Item("IncentiveForRecepient").ToString)
                oRefund.persentase = CDec(oCustom.ListDataTable.Rows(index).Item("PersenAlokasi").ToString)
                listRefund.Add(oRefund)
            Next

        End If
    End Sub

    Public Function PredicateFunction(ByVal custom As Parameter.RefundInsentif) As Boolean
        Return (custom.SupplierEmployeePosition = Me.SupplierEmployeePosition)
    End Function
    <Serializable()> _
    Public Class ListEmp
        Public Property SupplierEmployeeID As String
        Public Property SupplierEmployeeName As String
    End Class
    <WebMethod()> _
    Public Shared Function GetEmployee(id As String) As List(Of ListEmp)
        Dim m_Insentif As New RefundInsentifController
        Dim rtn As New List(Of ListEmp)


        Dim oCustom As New Parameter.RefundInsentif
        Dim entity As New DataTable

        With oCustom
            .strConnection = ModVar.StrConn
            .WhereCond = "  SupplierEmployeePosition= '" & id & "' and SupplierID='" & ModVar.SuppilerID & "' "
            .SPName = "spGetSupplierEmployee"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)
        entity = oCustom.ListDataTable

        For Each dt As DataRowView In entity.DefaultView
            rtn.Add(New ListEmp() With {
                .SupplierEmployeeID = dt("SupplierEmployeeID").ToString,
                .SupplierEmployeeName = dt("Name").ToString
            })
        Next

        Return rtn
    End Function
    <WebMethod()> _
    Public Shared Function GetTarifPajak(supplierEmployeeID As String, supplierEmployeePosition As String) As String
        Dim m_Insentif As New RefundInsentifController
        Dim oCustom As New Parameter.RefundInsentif
        Dim tarif As String = ""

        With oCustom
            .strConnection = ModVar.StrConn
            .WhereCond = "  se.SupplierEmployeeID= '" & supplierEmployeeID & "' and se.supplierEmployeePosition = '" & supplierEmployeePosition & "' and se.SupplierID='" & ModVar.SuppilerID & "' "
            .SPName = "spGetTarifBySupplierEmployee"
        End With

        oCustom = m_Insentif.GetSPBy(oCustom)

        tarif = LTrim(Str(oCustom.ListDataTable.Rows(0).Item("tarif")))
        Return tarif.ToString
    End Function

    <WebMethod()> _
    Public Shared Sub Apply(jabatan As String,
                            employee As String,
                            persen As String,
                            insentif As String,
                            pph As String,
                            tarifpajak As String,
                            nilaipajak As String,
                            insentifnet As String,
                            TransID As String,
                            seq As Integer)



        Dim dtrow As DataRow

        If seq = 1 Then
            Dim dt As New DataTable

            dt.Columns.Add("jabatan")
            dt.Columns.Add("employee")
            dt.Columns.Add("persen")
            dt.Columns.Add("insentif")
            dt.Columns.Add("pph")
            dt.Columns.Add("tarifpajak")
            dt.Columns.Add("nilaipajak")
            dt.Columns.Add("insentifnet")
            dt.Columns.Add("TransID")

            ModVar.dtTable = dt
        End If

        dtrow = ModVar.dtTable.NewRow
        dtrow("jabatan") = jabatan
        dtrow("employee") = employee
        dtrow("persen") = persen
        dtrow("insentif") = insentif
        dtrow("pph") = pph
        dtrow("tarifpajak") = tarifpajak
        dtrow("nilaipajak") = nilaipajak
        dtrow("insentifnet") = insentifnet
        dtrow("TransID") = TransID
        ModVar.dtTable.Rows.Add(dtrow)

        Dim t As Integer = ModVar.dtTable.Rows.Count

    End Sub
    <WebMethod()> _
    Public Shared Function SaveApply(ByVal GrouptransID As String) As String
        Dim oRefund = New Parameter.RefundInsentif
        Dim m_Insentif As New RefundInsentifController
        Dim status As String
        With oRefund
            .strConnection = ModVar.StrConn
            .BranchId = ModVar.BranchId
            .ApplicationID = ModVar.ApplicationID
            .SupplierID = ModVar.SuppilerID
            .TransID = GrouptransID
            .isInternal = "0"
        End With

        m_Insentif.SupplierIncentiveDailyDDelete(oRefund)

        For index = 0 To ModVar.dtTable.Rows.Count - 1
            With oRefund
                .strConnection = ModVar.StrConn
                .BranchId = ModVar.BranchId
                .ApplicationID = ModVar.ApplicationID
                .SupplierID = ModVar.SuppilerID
                .SupplierEmployeeID = ModVar.dtTable.Rows(index).Item("employee").ToString.Trim
                .SupplierEmployeePosition = ModVar.dtTable.Rows(index).Item("jabatan").ToString.Trim
                .EmployeeName = ""
                .pph = ModVar.dtTable.Rows(index).Item("pph").ToString.Trim
                .tarifPajak = CDec(ModVar.dtTable.Rows(index).Item("tarifpajak").ToString.Trim)
                .nilaiPajak = CDec(ModVar.dtTable.Rows(index).Item("nilaipajak").ToString.Trim)
                .insentifNet = CDec(ModVar.dtTable.Rows(index).Item("insentifnet").ToString.Trim)
                .nilaiAlokasi = CDec(ModVar.dtTable.Rows(index).Item("insentif").ToString.Trim)
                .nilaiAlokasiPersen = CDec(ModVar.dtTable.Rows(index).Item("persen").ToString.Trim)
                .TransID = ModVar.dtTable.Rows(index).Item("TransID").ToString.Trim
            End With

            status = m_Insentif.SupplierIncentiveDailyDSave(oRefund)
        Next

        Return status

    End Function
    <WebMethod()> _
    Public Shared Sub ApplyInternal(penggunaan As String,
                            persen As String,
                            insentif As String,
                            TransID As String,
                            seq As Integer)

        Dim dtrow As DataRow

        If seq = 1 Then
            Dim dt As New DataTable

            dt.Columns.Add("penggunaan")
            dt.Columns.Add("persen")
            dt.Columns.Add("insentif")
            dt.Columns.Add("TransID")

            ModVar.dtTable = dt
        End If

        dtrow = ModVar.dtTable.NewRow
        dtrow("penggunaan") = penggunaan
        dtrow("persen") = persen
        dtrow("insentif") = insentif
        dtrow("TransID") = TransID
        ModVar.dtTable.Rows.Add(dtrow)

        Dim t As Integer = ModVar.dtTable.Rows.Count

    End Sub
    <WebMethod()> _
    Public Shared Function SaveApplyInternal(ByVal GrouptransID As String) As String
        Dim m_Insentif As New RefundInsentifController
        Dim oRefund = New Parameter.RefundInsentif
        Dim status As String
        With oRefund
            .strConnection = ModVar.StrConn
            .BranchId = ModVar.BranchId
            .ApplicationID = ModVar.ApplicationID
            .SupplierID = ModVar.SuppilerID
            .TransID = GrouptransID
            .isInternal = "1"
        End With

        m_Insentif.SupplierIncentiveDailyDDelete(oRefund)

        For index = 0 To ModVar.dtTable.Rows.Count - 1
            With oRefund
                .strConnection = ModVar.StrConn
                .BranchId = ModVar.BranchId
                .ApplicationID = ModVar.ApplicationID
                .SupplierID = ModVar.SuppilerID
                .SupplierEmployeeID = "-"
                .SupplierEmployeePosition = "-"
                .EmployeeName = ""
                .pph = "-"
                .tarifPajak = 0
                .nilaiPajak = 0
                .insentifNet = 0
                .nilaiAlokasi = CDec(ModVar.dtTable.Rows(index).Item("insentif").ToString.Trim)
                .nilaiAlokasiPersen = CDec(ModVar.dtTable.Rows(index).Item("persen").ToString.Trim)
                .TransID = ModVar.dtTable.Rows(index).Item("TransID").ToString.Trim
            End With

            status = m_Insentif.SupplierIncentiveDailyDSave(oRefund)
        Next

        Return status
    End Function
End Class