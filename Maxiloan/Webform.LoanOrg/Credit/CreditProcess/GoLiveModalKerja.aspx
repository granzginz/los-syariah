﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GoLiveModalKerja.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.GoLiveModalKerja" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pencairan</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <link href="../../../Include/Lookup.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO") %>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%# Request.servervariables("SERVER_NAME") %>/';			
    </script>
    <script language="JavaScript" src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }	
			
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                    <div class="form_single">
                        <h3>
                            LOAN ACTIVATION - MODAL KERJA</h3>
                    </div>
                </div>
            </div>
         <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                    Tanggal Aktivasi
                </label>
                <asp:TextBox runat="server" ID="txtTanggalAktivasi"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="ceTanggalAktivasi" TargetControlID="txtTanggalAktivasi" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderStyle Width="3%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <HeaderTemplate>
                                        <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                        </asp:CheckBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PILIH">    
                                    <ItemStyle CssClass="command_col"></ItemStyle>                            
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkReturn" runat="server" CommandName="Return" Text='RETURN'
                                            CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" ID="lnkAgreementNo" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>'
                                            NavigateUrl=''>
                                        </asp:HyperLink>
                                        <asp:Label runat="server" ID="lblAgreementNo" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" ID="lnkName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'
                                            NavigateUrl=''>
                                        </asp:HyperLink>
                                        <asp:Label runat="server" ID="lblCustomerID" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Plafond" HeaderText="Total Plafond" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="ApplicationID" HeaderText="ApplicationID">
                                </asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="AgreementDate" HeaderText="AgreementDate"
                                    DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="SurveyDate"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="FirstInstallment"></asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="Application Id" Visible="False">
                                    <HeaderStyle Width="17%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblApplicationId" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="BranchID" HeaderText="Branch Id" Visible="False">
                                    <HeaderStyle ></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblBranchID" Text='<%# DataBinder.eval(Container,"DataItem.BranchID")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                            </asp:ImageButton>
                            Page
                            <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                            <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False" />
                            <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer">
                            </asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                                Type="integer" MaximumValue="999999999" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RangeValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="true" />
                </div>
            </asp:panel>
            <asp:Panel ID="pnlSearch" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI KONTRAK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Cabang
                        </label>
                        <asp:DropDownList ID="oBranch" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
                            Display="Dynamic" ErrorMessage="Harap Pilih Cabang" ControlToValidate="oBranch"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label class="medium_text">
                                Cari Berdasarkan
                            </label>
                            <asp:DropDownList ID="cboSearch" runat="server">
                                <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                                <asp:ListItem Value="AgreementNo">No. Kontrak</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="true" />
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlReturn" runat="server" Visible="False">
                <div class="form_box">
                    <div class="form_left">
                        <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                    </div>
                    <div class="form_right">
                        <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Alasan Return</label>
                        <asp:TextBox ID="txtAlasanReturn" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtAlasanReturn"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
