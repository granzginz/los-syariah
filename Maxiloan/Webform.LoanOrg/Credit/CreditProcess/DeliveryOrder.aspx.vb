﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DeliveryOrder
    Inherits Maxiloan.Webform.WebBased        
    'Protected WithEvents oZipCode As ucLookupZipCode
    Protected WithEvents ucAddress As ucAddressCity
    Private x_controller As New DataUserControlController
#Region "Constanta"
    Private m_controller As New DOController
    Private m_TCcontroller As New ApplicationController
    Private m_AssetDataController As New AssetDataController
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail

    Protected WithEvents ucSyaratCair1 As ucSyaratCair
    Protected WithEvents ucPPN As ucNumberFormat
    Protected WithEvents ucBBN As ucNumberFormat


    Dim objrow As DataRow
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim intLoop As Integer
    Dim Status As Boolean = True
    Private time As String
#End Region

#Region "Property"


    Private Property myDataTable() As DataTable
        Get
            Return CType(ViewState("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("myDataTable") = Value
        End Set
    End Property

    Private Property UsedNew() As String
        Get
            Return CType(viewstate("UsedNew"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("UsedNew") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property SupplierPhone() As String
        Get
            Return CType(viewstate("SupplierPhone"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierPhone") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property

    Private Property SupplierName() As String
        Get
            Return CType(viewstate("SupplierName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierName") = Value
        End Set
    End Property

    Private Property EmployeeName() As String
        Get
            Return CType(viewstate("EmployeeName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeName") = Value
        End Set
    End Property

    Private Property NewApplicationDate() As Date
        Get
            Return CType(viewstate("NewApplicationDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("NewApplicationDate") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Private Property CustomerType() As String
        Get
            Return CType(viewstate("CustomerType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerType") = Value
        End Set
    End Property

    Private Property IsActive() As Boolean
        Get
            Return CType(viewstate("IsActive"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsActive") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Private Property CreditScoreSchemeID() As String
        Get
            Return CType(viewstate("CreditScoreSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreSchemeID") = Value
        End Set
    End Property

    Private Property CreditScoreComponentID() As String
        Get
            Return CType(viewstate("CreditScoreComponentID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreComponentID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(viewstate("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(viewstate("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreResult") = Value
        End Set
    End Property

    Private Property AssetTypeID() As String
        Get
            Return CType(viewstate("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetTypeID") = Value
        End Set
    End Property

    Private Property IsNewAssetDocumentContent() As Boolean
        Get
            Return CType(viewstate("IsNewAssetDocumentContent"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsNewAssetDocumentContent") = Value
        End Set
    End Property

    Private Property AssetOrigination() As String
        Get
            Return CType(ViewState("AssetOrigination"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetOrigination") = Value
        End Set
    End Property

    Private Property TipeAplikasi() As String
        Get
            Return CType(ViewState("TipeAplikasi"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("TipeAplikasi") = Value
        End Set
    End Property


    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateStart = Me.BusinessDate + " " + time

        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            'RangeValidator3.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
            Me.FormID = "DO"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If sessioninvalid() Then
                    Exit Sub
                End If
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
            txtGoPage.Text = "1"
            Me.Sort = "a.AgreementNo ASC"
            Me.CmdWhere = ""

            cboBranch()

            Me.BranchID = oBranch.SelectedValue.ToString.Trim
            'Modify by Wira 20171125
            With ucAddress
                .TeleponFalse()
            End With
            ucAddress.ValidatorFalse()

            'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) = 0 And Me.IsHoBranch = True Then
            If IsSingleBranch() And Me.IsHoBranch = False Then
                BindGridEntity(Me.CmdWhere)
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/error_notauthorized.aspx")
            End If
        End If
        'LabelValidationFalse()
        'InitialLabelValidDTGAssetDocNew()

        'ClientScript.RegisterStartupScript(Me.GetType, "resizeGrid", "gridGeneralSize('ucSyaratCair1_dtgSyaratCair')", True)
    End Sub
#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        txtTanggalDO.Attributes.Add("readonly", "true")
        txtTanggalPO.Attributes.Add("readonly", "true")
        'txtTanggalSTNK.Attributes.Add("readonly", "true")
        txtTanggalDO.Text = Format(Me.BusinessDate, "dd/MM/yyyy")
        pnlList.Visible = True
        pnlPO.Visible = False
        pnlReturn.Visible = False
        lblMessage.Text = ""
        lblMessage.Visible = False
        'lblErrSerial1.Text = ""
        'lblErrSerial2.Text = ""
        'lblErrSerial1.Visible = False
        'lblErrSerial2.Visible = False
    End Sub
#End Region
    Sub cboBranch()
        With oBranch
            If Me.IsHoBranch Then
                .DataSource = x_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "ALL"
                .Enabled = True
            Else
                .DataSource = x_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Enabled = False
            End If

        End With
    End Sub
#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oDO As New Parameter._DO
        InitialDefaultPanel()
        With oDO
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .BranchId = oBranch.SelectedValue.ToString.Trim
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString
        End With

        oDO = m_controller.DOPaging(oDO)

        If Not oDO Is Nothing Then
            dtEntity = oDO.ListData
            recordCount = oDO.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenApplicationID('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function

    Function LinkToEmployee(ByVal strBranchID As String, ByVal strAOID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinEmployee('" & strStyle & "','" & strBranchID & "','" & strAOID & "')"
    End Function

    Function LinkToAgreementNo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "','" & strApplicationID & "')"
    End Function

#End Region

#Region "dtgEntity_ItemDataBound"
    Private Sub dtgEntity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblSupplierID As New Label
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            Me.SupplierID = lblSupplierID.Text.Trim

            Dim lblCustomerID As New Label
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

            Dim lblAgreementNo As New Label
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)

            Dim lblAOID As New Label
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)

            Dim hypAgreementNo As New HyperLink
            Dim lblApplicationId As Label
            hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            Dim hynCustomerName As New HyperLink
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            Dim hynSupplierName As New HyperLink
            hynSupplierName = CType(e.Item.FindControl("hynSupplierName"), HyperLink)
            Dim hynEmployeeName As New HyperLink
            hynEmployeeName = CType(e.Item.FindControl("hynEmployeeName"), HyperLink)

            hypAgreementNo.NavigateUrl = LinkToAgreementNo(lblApplicationId.Text.Trim, "AccAcq")
            hynCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            hynSupplierName.NavigateUrl = LinkToSupplier(lblSupplierID.Text.Trim, "AccAcq")
            hynEmployeeName.NavigateUrl = LinkToEmployee(oBranch.SelectedValue.ToString.Trim, lblAOID.Text.Trim, "AccAcq")
        End If
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "BindTC"
    Sub TC()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString
        oApplication.AppID = Me.ApplicationID
        oApplication.BranchId = Me.BranchID
        oApplication.AddEdit = "GoLive"
        oApplication = m_TCcontroller.GetTC(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC.DataSource = oData.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()

        Dim intLoop As Integer

        For intLoop = 0 To dtgTC.Items.Count - 1
            If oData.Rows(intLoop).Item(4).ToString.Trim <> "" Then
                CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), TextBox).Text = Format(oData.Rows(intLoop).Item(4), "dd/MM/yyyy")
            End If
        Next
    End Sub
    Sub TC2()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication.AddEdit = "GoLive"
        oApplication = m_TCcontroller.GetTC2(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC2.DataSource = oData.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()

        Dim intLoop As Integer

        For intLoop = 0 To dtgTC2.Items.Count - 1
            If oData.Rows(intLoop).Item(5).ToString.Trim <> "" Then
                CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), TextBox).Text = Format(oData.Rows(intLoop).Item(5), "dd/MM/yyyy")
            End If
        Next
    End Sub
#End Region
#Region "BindSyaratCair"
    Sub BindSyaratCair()
        ucSyaratCair1.UseNew = Me.UsedNew
        ucSyaratCair1.BindData()
    End Sub
#End Region
#Region "LabelValidationFalse"
    Sub LabelValidationFalse()
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim countDtg As Integer = CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
        Dim validCheck As New Label
        Dim Checked As Boolean
        Dim PriorTo As String
        Dim PromiseDate As ValidDate
        Dim Mandatory As String
        For intLoop = 0 To countDtg
            If intLoop <= DtgTC1count Then
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("uscTCPromiseDate"), ValidDate)
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = False
                validCheck.Visible = False
                If PriorTo = "DO" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
            If intLoop <= DtgTC2count Then
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("uscTCPromiseDate2"), ValidDate)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = False
                validCheck.Visible = False
                If PriorTo = "DO" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
        Next
    End Sub
#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "DO", Me.AppId) Then
                pnlList.Visible = False
                pnlReturn.Visible = False
                pnlPO.Visible = True

                Me.BranchID = CType(e.Item.FindControl("lblBranchID"), Label).Text

                lblErrMsgDeliveryDate.Text = ""
                Dim lblApplicationID As New Label
                lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
                Me.ApplicationID = lblApplicationID.Text.Trim

                Dim lblSupplierID As New Label
                lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
                Me.SupplierID = lblSupplierID.Text.Trim

                Dim lblCustomerID As New Label
                lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

                Dim lbl_AssetDescription As New Label
                lbl_AssetDescription = CType(e.Item.FindControl("lblDescription"), Label)

                Dim lblAgreementNo As New Label
                lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)

                Dim lblAOID As New Label
                lblAOID = CType(e.Item.FindControl("lblAOID"), Label)

                Dim hypAgreementNo As New HyperLink
                hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
                Dim hynCustomerName As New HyperLink
                hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
                Dim hynSupplierName As New HyperLink
                hynSupplierName = CType(e.Item.FindControl("hynSupplierName"), HyperLink)
                Dim hynEmployeeName As New HyperLink
                hynEmployeeName = CType(e.Item.FindControl("hynEmployeeName"), HyperLink)

                hplCustomerName.Text = hynCustomerName.Text
                hplAgreement.Text = hypAgreementNo.Text

                hplCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
                hplAgreement.NavigateUrl = LinkToAgreementNo(lblApplicationID.Text.Trim, "AccAcq")
                lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & "AccAcq" & "', '" & Server.UrlEncode(lblSupplierID.Text.Trim) & "')"

                LabelValidationFalse()
                InitialLabelValidDTGAssetDocNew()

                Dim oDO_GetPONo As New Parameter._DO
                oDO_GetPONo.BranchId = Me.BranchID
                oDO_GetPONo.ApplicationID = Me.ApplicationID
                oDO_GetPONo.strConnection = GetConnectionString()
                lblPONumber.Text = m_controller.Get_PONumber(oDO_GetPONo)

                lblSupplier.Text = hynSupplierName.Text
                lblAssetDescription.Text = lbl_AssetDescription.Text

                Dim oDO_Process As New Parameter._DO
                oDO_Process.BranchId = Me.BranchID
                oDO_Process.ApplicationID = Me.ApplicationID
                oDO_Process.strConnection = GetConnectionString()
                oDO_Process = m_controller.Get_DOProcess(oDO_Process)

                lblSerial1.Text = oDO_Process.SerialNo1Label
                lblSerial2.Text = oDO_Process.SerialNo2Label
                lblSerial1Value.Text = oDO_Process.SerialNo1
                lblSerial2Value.Text = oDO_Process.SerialNo2
                lblWarna.Text = oDO_Process.Warna
                lblNopol.Text = oDO_Process.Nopol


                'txtYear.Text = CStr(oDO_Process.ManufacturingYear)
                lblYear.Text = CStr(oDO_Process.ManufacturingYear)
                Me.UsedNew = oDO_Process.UsedNew

                'Input serial pindah ke form konfirmasi dealer
                'If (Me.UsedNew <> "") Or (Not IsNothing(Me.UsedNew)) Then
                '    If Me.UsedNew.ToUpper.Trim = "U" Then
                '        RFVSerial1.Enabled = True
                '        RFVSerial1.Visible = True
                '        RFVSerial1.ErrorMessage = "Harap isi " & lblSerial1.Text.Trim & "!"
                '        RFVSerial2.Enabled = True
                '        RFVSerial2.Visible = True
                '        RFVSerial2.ErrorMessage = "Harap isi " & lblSerial2.Text.Trim & "!"
                '    Else
                '        RFVSerial1.Enabled = False
                '        RFVSerial2.Enabled = False
                '    End If
                'End If

                If (Me.UsedNew <> "") Or (Not IsNothing(Me.UsedNew)) Then
                    If Me.UsedNew.ToUpper.Trim = "U" Then
                        rfvTanggalSTNK.Enabled = True
                        lblTanggalSTNK.Attributes("class") = "label_req"
                    Else
                        rfvTanggalSTNK.Enabled = False
                        lblTanggalSTNK.Attributes.Remove("class")
                    End If
                End If

                Me.AssetTypeID = oDO_Process.AssetTypeID
                Me.AssetOrigination = oDO_Process.AssetOrigination
                Me.CustomerType = oDO_Process.customerType

                txtName.Text = oDO_Process.OldOwnerAsset
                'txtAddress.Text = oDO_Process.OldOwnerAddress

                'txtRT.Text = oDO_Process.OldOwnerRT
                'txtRW.Text = oDO_Process.OldOwnerRW
                'oZipCode.Kelurahan = oDO_Process.OldOwnerKelurahan
                'oZipCode.Kecamatan = oDO_Process.OldOwnerKecamatan
                'oZipCode.City = oDO_Process.OldOwnerCity
                'oZipCode.ZipCode = oDO_Process.OldOwnerZipCode
                'oZipCode.Style = "AccAcq"
                'oZipCode.BindData()


                ucAddress.Address = oDO_Process.OldOwnerAddress & IIf(oDO_Process.OldOwnerRT.Trim = "", "", " RT." & oDO_Process.OldOwnerRT.Trim & " ").ToString _
                            + " " & IIf(oDO_Process.OldOwnerRW.Trim = "", "", " RW." & oDO_Process.OldOwnerRW.Trim & " ").ToString _
                            + " " & IIf(oDO_Process.OldOwnerKelurahan.Trim = "", "", oDO_Process.OldOwnerKelurahan.Trim & " ").ToString _
                            + " " & IIf(oDO_Process.OldOwnerCity.Trim = "", "", oDO_Process.OldOwnerCity.Trim & "").ToString
                ucAddress.City = oDO_Process.OldOwnerCity
                ucAddress.ZipCode = oDO_Process.OldOwnerZipCode
                ucAddress.AreaPhone1 = ""
                ucAddress.Phone1 = ""
                ucAddress.AreaPhone2 = ""
                ucAddress.Phone2 = ""
                ucAddress.AreaFax = ""
                ucAddress.Fax = ""
                ucAddress.BindAddress()
                'Modify by Wira 20171125
                ucAddress.TeleponFalse()

                If oDO_Process.TaxDate.ToString("dd/MM/yyyy") = "01/01/1900" Then
                    txtTanggalSTNK.Text = ""
                Else
                    txtTanggalSTNK.Text = oDO_Process.TaxDate.ToString("dd/MM/yyyy")
                End If

                txtNotes.Text = oDO_Process.Notes

                BindAsset()
                BindAttribute()

                InitialLabelValid()

                TC()
                TC2()
                BindSyaratCair()


                LabelValidationFalse()

                TipeAplikasi = oDO_Process.TipeAplikasi.Trim
                If TipeAplikasi = "AGRI" Then
                    infoLain.Visible = False
                    infoNopol.Visible = False
                    infoSerial2.Visible = False
                    infoTahunKendaraan.Visible = False
                    infowarna.Visible = False
                    pnlPPNBBN.Visible = False
                ElseIf TipeAplikasi = "OPLS" Then
                    infoLain.Visible = True
                    infoNopol.Visible = True
                    infoSerial2.Visible = True
                    infoTahunKendaraan.Visible = True
                    infowarna.Visible = True
                    pnlPPNBBN.Visible = True
                    ucPPN.RequiredFieldValidatorEnable = True
                Else
                    infoLain.Visible = True
                    infoNopol.Visible = True
                    infoSerial2.Visible = True
                    infoTahunKendaraan.Visible = True
                    infowarna.Visible = True
                    pnlPPNBBN.Visible = False
                End If
            End If
        ElseIf e.CommandName = "Return" Then
            pnlList.Visible = False
            pnlReturn.Visible = True
            pnlPO.Visible = False

            Dim lblCustomerID As New Label
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Dim hypAgreementNo As New HyperLink
            hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            Dim lblApplicationID As New Label
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)

            hdnApplicationID.Value = lblApplicationID.Text.Trim
            ucViewApplication1.ApplicationID = lblApplicationID.Text.Trim
            ucViewApplication1.CustomerID = lblCustomerID.Text.Trim
            ucViewApplication1.bindData()
            ucViewApplication1.initControls("OnNewApplication")
            ucViewCustomerDetail1.CustomerID = lblCustomerID.Text.Trim
            ucViewCustomerDetail1.bindCustomerDetail()
        End If
    End Sub
#End Region

#Region "Initial Label Validator"
    Sub InitialLabelValid()
        Dim count As Integer
        count = CInt(IIf(dtgAssetDoc.Items.Count > dtgAttribute.Items.Count, dtgAssetDoc.Items.Count, dtgAttribute.Items.Count))
        For intLoop = 0 To count - 1
            If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label).Visible = False
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label).Visible = False
            End If
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label).Visible = False
            End If
        Next
    End Sub
#End Region

#Region "GetValue"
    Function GetValue(ByVal value As String) As String
        If value = "1" Then
            Return "True"
        Else
            Return "False"
        End If
    End Function
#End Region

#Region "BindAttribute, BindAsset"
    Sub BindAttribute()
        Dim oDO As New Parameter._DO
        Dim oData As New DataTable
        oDO.strConnection = GetConnectionString()
        oDO.BranchId = Me.BranchID
        oDO.ApplicationID = Me.ApplicationID
        oDO = m_controller.GetAttribute(oDO)
        oData = oDO.ListData
        dtgAttribute.DataSource = oData
        dtgAttribute.DataBind()
    End Sub

    Sub BindAsset()
        Dim oDO_Asset As New Parameter._DO
        Dim oData As New DataTable
        Dim oAssetData As New Parameter.AssetData

        oDO_Asset.strConnection = GetConnectionString()
        oDO_Asset.BranchId = Me.BranchID
        oDO_Asset.ApplicationID = Me.ApplicationID

        oDO_Asset = m_controller.GetDO_AssetDoc(oDO_Asset)
        oData = oDO_Asset.ListData

        If oData.Rows.Count > 0 Then
            Me.IsNewAssetDocumentContent = False
            dtgAssetDoc.DataSource = oData
            dtgAssetDoc.DataBind()
            dtgAssetDoc.Visible = True
            DTGAssetDocNew.Visible = False
        Else
            Me.IsNewAssetDocumentContent = True

            oAssetData.strConnection = GetConnectionString()
            oAssetData.AssetID = Me.AssetTypeID
            oAssetData.CustomerType = Me.CustomerType
            oAssetData.Origination = Me.AssetOrigination

            oAssetData = m_AssetDataController.GetAssetDoc(oAssetData)
            oData = oAssetData.ListData

            DTGAssetDocNew.DataSource = oData
            DTGAssetDocNew.DataBind()
            DTGAssetDocNew.Visible = True
            dtgAssetDoc.Visible = False
        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        Me.CmdWhere = ""
        txtSearch.Text = ""
        txtTanggalPO.Text = ""
        BindGridEntity(Me.CmdWhere)
        txtGoPage.Text = "1"
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim = "" And txtTanggalPO.Text.Trim = "" Then
            Me.CmdWhere = ""
        ElseIf txtSearch.Text.Trim = "" And txtTanggalPO.Text.Trim <> "" Then
            Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalPO.Text.Trim) & "'"
        ElseIf txtSearch.Text.Trim <> "" And txtTanggalPO.Text.Trim = "" Then
            If cboSearch.SelectedItem.Value = "Name" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "b." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "AgreementNo" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "EmployeeName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "d." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "SupplierName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "c." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            End If
        ElseIf txtSearch.Text.Trim <> "" And txtTanggalPO.Text.Trim <> "" Then

            If cboSearch.SelectedItem.Value = "Name" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalPO.Text.Trim) & "' and b." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "AgreementNo" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalPO.Text.Trim) & "' and a." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "EmployeeName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalPO.Text.Trim) & "' and d." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "SupplierName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalPO.Text.Trim) & "' and c." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            End If
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imgSave"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblMessage.Text = ""
        lblMessage.Visible = False
        lblErrMsgDeliveryDate.Text = ""

        Dim oPO_BackDate As New Parameter._DO
        Dim BackDate As Integer
        Dim Temp_BackDate As Integer

        oPO_BackDate.strConnection = GetConnectionString()
        BackDate = m_controller.GetDO_CheckBackDate(oPO_BackDate)
        Temp_BackDate = BackDate * (-1)

        If txtTanggalDO.Text = "" Then
            lblErrMsgDeliveryDate.Visible = True
            lblErrMsgDeliveryDate.Text = "Delivery Date is required"
            pnlPO.Visible = True
            pnlList.Visible = False
            pnlReturn.Visible = False
            Exit Sub
        End If

        If Me.TipeAplikasi = "OPLS" And CDbl(ucPPN.Text) < 1 Then
            ShowMessage(lblMessage, "Harap isi nilai PPN", True)
            Exit Sub
        End If

        Dim oData1 As New DataTable
        Dim oData2 As New DataTable
        Dim oData3 As New DataTable
        Dim oData4 As New DataTable
        Dim oData5 As New DataTable
        Status = True

        Dim oDO As New Parameter._DO
        Dim BusinessDate As Date

        BusinessDate = CType(CStr(Me.BusinessDate) & " " & Now.ToLongTimeString, DateTime)
        oDO.LoginId = Me.Loginid
        oDO.BusinessDate = BusinessDate
        oDO.BranchId = Me.BranchID
        oDO.ApplicationID = Me.ApplicationID
        oDO.DeliveryDate = ConvertDate2(txtTanggalDO.Text)
        oDO.SerialNo1 = lblSerial1Value.Text.Trim
        oDO.SerialNo2 = lblSerial2Value.Text.Trim
        'oDO.ManufacturingYear = CInt(txtYear.Text.Trim)
        oDO.ManufacturingYear = CInt(lblYear.Text.Trim)
        oDO.OldOwnerAsset = txtName.Text
        oDO.OldOwnerAddress = ucAddress.Address
        oDO.OldOwnerRT = ""
        oDO.OldOwnerRW = ""
        oDO.OldOwnerKelurahan = ""
        oDO.OldOwnerKecamatan = ""
        oDO.OldOwnerCity = ucAddress.City
        oDO.OldOwnerZipCode = ucAddress.ZipCode
        If txtTanggalSTNK.Text.Trim <> "" Then
            oDO.TaxDate = ConvertDate2(txtTanggalSTNK.Text)
        Else
            oDO.TaxDate = ConvertDate2("1/1/1900")
        End If

        If Me.TipeAplikasi = "OPLS" Then
            oDO.PPN = CDbl(IIf(IsNumeric(ucPPN.Text), ucPPN.Text, 0))
            oDO.BBN = CDbl(IIf(IsNumeric(ucBBN.Text), ucBBN.Text, 0))
        Else
            oDO.PPN = 0
            oDO.BBN = 0
        End If

        oDO.Notes = txtNotes.Text
        oDO.AssetTypeID = Me.AssetTypeID
        oDO.strConnection = GetConnectionString()

        ' Data Table Attribute Content
        oData3.Columns.Add("AttributeID", GetType(String))
        oData3.Columns.Add("AttributeContent", GetType(String))

        ' Data Table Asset Document Content
        oData4.Columns.Add("AssetDocID", GetType(String))
        oData4.Columns.Add("DocumentNo", GetType(String))
        oData4.Columns.Add("IsMainDoc", GetType(String))
        oData4.Columns.Add("IsDocExist", GetType(String))
        oData4.Columns.Add("Notes", GetType(String))
        oData4.Columns.Add("PromiseDate", GetType(String))

        Validator(oData3, oData4)
        If Status = False Then
            'cek_Check()
            pnlList.Visible = False
            pnlReturn.Visible = False
            pnlPO.Visible = True
            Exit Sub
        End If

        Status = True

        ' Data Table TC'
        oData1.Columns.Add("MasterTCID", GetType(String))
        oData1.Columns.Add("PriorTo", GetType(String))
        oData1.Columns.Add("IsChecked", GetType(String))
        oData1.Columns.Add("IsMandatory", GetType(String))
        oData1.Columns.Add("PromiseDate", GetType(String))
        oData1.Columns.Add("Notes", GetType(String))

        ' Data Table TC Check List
        oData2.Columns.Add("MasterTCID", GetType(String))
        oData2.Columns.Add("AGTCCLSequenceNo", GetType(String))
        oData2.Columns.Add("IsChecked", GetType(String))
        oData2.Columns.Add("PromiseDate", GetType(String))
        oData2.Columns.Add("Notes", GetType(String))

        ValidatorTC(oData1, oData2)
        If Status = False Then
            pnlList.Visible = False
            pnlReturn.Visible = False
            pnlPO.Visible = True
            lblMessage.Text = "Harap isi data yang diperlukan!"

            Exit Sub
        End If

        Try
            m_controller.DOSave(oDO, oData1, oData2, oData3, oData4, ucSyaratCair1.oData)
            'Modify by Wira 20171019
            ActivityLog()

            Me.CmdWhere = ""
            BindGridEntity(Me.CmdWhere)
            pnlList.Visible = True
            pnlReturn.Visible = False
        Catch ex As Exception

            lblMessage.Text = MessageHelper.MESSAGE_INSERT_FAILED

            pnlList.Visible = False
            pnlPO.Visible = True
            pnlReturn.Visible = False
            Exit Sub
        End Try
    End Sub

    Sub ValidatorTC(ByRef oData1 As DataTable, ByRef oData2 As DataTable)
        Dim PriorTo As String
        Dim Mandatory As String
        Dim Checked As Boolean
        Dim PromiseDate As TextBox
        Dim validCheck As New Label
        Dim MasterTCID, MasterTCID2, AGTCCLSequenceNo, Notes As String
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim objrow As DataRow
        Dim intLoop2, intLoop3 As Integer


        For intLoop = 0 To CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
            If intLoop <= DtgTC1count Then
                MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), TextBox)
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                Notes = CType(dtgTC.Items(intLoop).Cells(6).FindControl("txtTCNotes"), TextBox).Text
                If PromiseDate.Text <> "" Then
                    If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = True
                    End If
                End If
                If PriorTo = "DO" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.Text = "" Then
                        If Status <> False Then
                            Status = False
                            Exit Sub
                        End If
                    End If
                    validCheck.Visible = True
                End If
                objrow = oData1.NewRow
                objrow("MasterTCID") = MasterTCID
                objrow("PriorTo") = PriorTo
                objrow("IsChecked") = IIf(Checked = True, "1", "0")
                objrow("IsMandatory") = IIf(Mandatory = "v", "1", "0")
                objrow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
                objrow("Notes") = Notes
                oData1.Rows.Add(objrow)
            End If
            If intLoop <= DtgTC2count Then
                MasterTCID = dtgTC2.Items(intLoop).Cells(8).Text
                AGTCCLSequenceNo = dtgTC2.Items(intLoop).Cells(9).Text
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Notes = CType(dtgTC2.Items(intLoop).Cells(7).FindControl("txtTCNotes2"), TextBox).Text
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), TextBox)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                If PromiseDate.Text <> "" Then
                    If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = True
                    End If
                End If
                If PriorTo = "DO" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.Text = "" Then
                        If Status <> False Then
                            Status = False
                            Exit Sub
                        End If
                        validCheck.Visible = True
                    End If
                End If
                objrow = oData2.NewRow
                objrow("MasterTCID") = MasterTCID
                objrow("AGTCCLSequenceNo") = AGTCCLSequenceNo
                objrow("IsChecked") = IIf(Checked = True, "1", "0")
                objrow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
                objrow("Notes") = Notes
                oData2.Rows.Add(objrow)
            End If
        Next

        Dim RainCheck As Boolean = False
        Dim intLoop4, intLoop5 As Integer
        For intLoop = 0 To DtgTC1count
            MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
            Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
            For intLoop2 = 0 To DtgTC2count
                MasterTCID2 = dtgTC2.Items(intLoop2).Cells(8).Text
                If MasterTCID = MasterTCID2 Then
                    If Checked = True Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True

                        If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = False Then
                            CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                        Else
                            CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                        End If
                    Else
                        If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = True Then
                            For intLoop4 = 0 To intLoop2
                                If dtgTC2.Items(intLoop4).Cells(8).Text = MasterTCID Then
                                    CType(dtgTC2.Items(intLoop4).FindControl("lblVTC2Checked"), Label).Visible = True
                                End If
                            Next

                            For intLoop5 = intLoop2 To DtgTC2count
                                If dtgTC2.Items(intLoop5).Cells(8).Text = MasterTCID Then
                                    CType(dtgTC2.Items(intLoop5).FindControl("lblVTC2Checked"), Label).Visible = True
                                End If
                            Next


                            CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                            For intLoop3 = 0 To intLoop
                                If dtgTC.Items(intLoop3).Cells(7).Text = MasterTCID2 Then
                                    If CType(dtgTC.Items(intLoop3).FindControl("chkTCChecked"), CheckBox).Checked = False Then
                                        CType(dtgTC.Items(intLoop3).FindControl("lblVTCChecked"), Label).Visible = True
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            Next
            RainCheck = False
        Next
    End Sub


    Sub Validator(ByVal oData3 As DataTable, ByVal oData4 As DataTable)
        Dim oAssetData As New Parameter.AssetData
        Dim oDO As New Parameter._DO
        Dim oData As New DataTable

        ''CEK SERIAL
        'If lblSerial1Value.Text.Trim <> "" Or lblSerial2Value.Text.Trim <> "" Then
        '    Dim ErrS1 As Boolean = False
        '    Dim ErrS2 As Boolean = False


        '    oDO.strConnection = GetConnectionString()
        '    oDO.SerialNo1 = lblSerial1Value.Text
        '    oDO.SerialNo2 = lblSerial2Value.Text
        '    oDO.AssetTypeID = Me.AssetTypeID
        '    oDO.BranchId = Me.BranchID
        '    oDO.ApplicationID = Me.ApplicationID

        '    oDO = m_controller.CheckSerial(oDO)
        '    oData = oDO.ListData

        '    If oData.Rows.Count <> 0 Then
        '        oData = oDO.ListData
        '        Dim intLoop As Integer
        '        For intLoop = 0 To oData.Rows.Count - 1
        '            If lblSerial1Value.Text.Trim <> "" Then
        '                If oData.Rows(intLoop).Item(0).ToString.Trim = lblSerial1Value.Text.Trim Then
        '                    ErrS1 = True
        '                End If
        '            End If
        '            If lblSerial2Value.Text.Trim <> "" Then
        '                If oData.Rows(intLoop).Item(1).ToString.Trim = lblSerial2Value.Text.Trim Then
        '                    ErrS2 = True
        '                End If
        '            End If
        '        Next
        '        If ErrS1 = True And ErrS2 = True Then
        '            lblErrSerial1.Visible = True
        '            lblErrSerial1.Text = "Serial No 1 sudah ada"
        '            lblErrSerial2.Visible = True
        '            lblErrSerial2.Text = "Serial No 2 sudah ada"
        '            Status = False
        '            Exit Sub
        '        Else
        '            If ErrS1 = True Then
        '                lblErrSerial1.Visible = True
        '                lblErrSerial1.Text = "Serial No 1 sudah ada"
        '                Status = False
        '                Exit Sub
        '            ElseIf ErrS2 = True Then
        '                lblErrSerial2.Visible = True
        '                lblErrSerial2.Text = "Serial No 2 sudah ada"
        '                Status = False
        '                Exit Sub
        '            End If
        '        End If
        '    End If
        'End If

        ''CEK ATTRIBUTE       
        Dim lblVNumber, lblVNumber2, lblIsNoRequired As Label
        Dim count As Integer
        Dim lblvChk As Label
        Dim chk, BPKBchk As CheckBox
        Dim MandatoryNew, MandatoryUsed As String
        Dim txtnumber As TextBox
        Dim isValueNeeded, value As String

        If Me.IsNewAssetDocumentContent = True Then
            count = CInt(IIf(dtgAttribute.Items.Count > DTGAssetDocNew.Items.Count, dtgAttribute.Items.Count, DTGAssetDocNew.Items.Count))
        ElseIf Me.IsNewAssetDocumentContent = False Then
            count = CInt(IIf(dtgAttribute.Items.Count > dtgAssetDoc.Items.Count, dtgAttribute.Items.Count, dtgAssetDoc.Items.Count))
        End If

        For intLoop = 0 To count - 1
            '    If intLoop <= dtgAttribute.Items.Count - 1 Then
            '        AttributeId = dtgAttribute.Items(intLoop).Cells(2).Text.Trim
            '        txtAttribute = CType(dtgAttribute.Items(intLoop).FindControl("txtAttribute"), TextBox)
            '        lblVAttribute = CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label)
            '        If AttributeId = "LICPLATE" And txtAttribute.Text.Trim <> "" Then
            '            oDO = New Parameter._DO
            '            oData = New DataTable
            '            oDO.strConnection = GetConnectionString()
            '            oDO.Input = txtAttribute.Text
            '            oDO.AssetTypeID = Me.AssetTypeID
            '            oDO.BranchId = Me.BranchID
            '            oDO.ApplicationID = Me.ApplicationID
            '            oDO = m_controller.CheckAttribute(oDO)
            '            oData = oDO.ListData
            '            If oData.Rows.Count > 0 Then
            '                lblVAttribute.Visible = True
            '                If Status <> False Then
            '                    Status = False
            '                    Exit Sub
            '                End If
            '            End If
            '        End If
            '        objrow = oData3.NewRow
            '        objrow("AttributeID") = AttributeId
            '        objrow("AttributeContent") = txtAttribute.Text.Trim
            '        oData3.Rows.Add(objrow)
            '    End If
            If Me.IsNewAssetDocumentContent = False Then
                If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                    chk = CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox)
                    lblvChk = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label)
                    lblVNumber = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label)
                    lblVNumber2 = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber2"), Label)
                    lblIsNoRequired = CType(dtgAssetDoc.Items(intLoop).FindControl("lblIsNoRequired"), Label)
                    BPKBchk = CType(dtgAssetDoc.Items(intLoop).FindControl("BPKBchk"), CheckBox)

                    value = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                    isValueNeeded = dtgAssetDoc.Items(intLoop).Cells(11).Text.Trim
                    MandatoryNew = dtgAssetDoc.Items(intLoop).Cells(7).Text.Trim
                    MandatoryUsed = dtgAssetDoc.Items(intLoop).Cells(8).Text.Trim

                    'matikan sementara
                    'If CBool(lblIsNoRequired.Text) Then
                    '    If Not BPKBchk.Checked And TipeAplikasi <> "AGRI" Then
                    '        Status = False
                    '        ShowMessage(lblMessage, "BPKB harus dicentang dokumen yang diperlukan", True)
                    '        Exit Sub
                    '    End If
                    'End If

                    'If Me.UsedNew = "N" And MandatoryNew = "1" Then
                    '    If chk.Checked = False Then
                    '        lblvChk.Visible = True
                    '        If Status <> False Then
                    '            Status = False
                    '            Exit Sub
                    '        End If
                    '    Else
                    '        If isValueNeeded = "True" And value = "" And CBool(lblIsNoRequired.Text) And BPKBchk.Checked Then
                    '            lblVNumber2.Visible = True
                    '            If Status <> False Then
                    '                Status = False
                    '                Exit Sub
                    '            End If
                    '        End If
                    '    End If
                    'ElseIf Me.UsedNew = "U" And MandatoryUsed = "1" Then
                    '    If chk.Checked = False Then
                    '        lblvChk.Visible = True
                    '        If Status <> False Then
                    '            Status = False
                    '            Exit Sub
                    '        End If
                    '    Else
                    '        If isValueNeeded = "True" And value = "" And CBool(lblIsNoRequired.Text) And BPKBchk.Checked Then
                    '            lblVNumber2.Visible = True
                    '            If Status <> False Then
                    '                Status = False
                    '                Exit Sub
                    '            End If
                    '        End If
                    '    End If
                    'End If
                    txtnumber = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox)
                    If txtnumber.Text.Trim <> "" Then
                        oDO = New Parameter._DO
                        oData = New DataTable
                        oDO.strConnection = GetConnectionString()
                        oDO.Input = txtnumber.Text.Trim
                        oDO.AssetDocID = dtgAssetDoc.Items(intLoop).Cells(9).Text.Trim
                        oDO.AssetTypeID = Me.AssetTypeID
                        oDO.BranchId = Me.BranchID
                        oDO.ApplicationID = Me.ApplicationID
                        oDO = m_controller.CheckAssetDoc(oDO)
                        oData = oDO.ListData
                        If isValueNeeded = True Then
                            If oData.Rows.Count > 0 Then
                                'lblVNumber.Visible = True -------
                                If Status <> False Then
                                    Status = False
                                    Exit Sub
                                End If
                            End If
                        End If
                    End If
                    objrow = oData4.NewRow
                    objrow("AssetDocID") = dtgAssetDoc.Items(intLoop).Cells(9).Text.Trim
                    objrow("DocumentNo") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                    objrow("IsMainDoc") = dtgAssetDoc.Items(intLoop).Cells(10).Text.Trim
                    objrow("IsDocExist") = IIf(CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox).Checked = True, "1", "0").ToString
                    objrow("Notes") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNotes"), TextBox).Text.Trim
                    objrow("PromiseDate") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtPromiseDate2"), TextBox).Text.Trim
                    oData4.Rows.Add(objrow)
                End If
            ElseIf Me.IsNewAssetDocumentContent = True Then
                If intLoop <= DTGAssetDocNew.Items.Count - 1 Then
                    chk = CType(DTGAssetDocNew.Items(intLoop).FindControl("chkNew"), CheckBox)
                    lblvChk = CType(DTGAssetDocNew.Items(intLoop).FindControl("lblVChkNew"), Label)
                    lblVNumber = CType(DTGAssetDocNew.Items(intLoop).FindControl("lblVNumberNew"), Label)
                    lblVNumber2 = CType(DTGAssetDocNew.Items(intLoop).FindControl("lblVNumber2New"), Label)
                    value = CType(DTGAssetDocNew.Items(intLoop).FindControl("txtNumberNew"), TextBox).Text.Trim
                    lblIsNoRequired = CType(DTGAssetDocNew.Items(intLoop).FindControl("lblIsNoRequired"), Label)
                    BPKBchk = CType(DTGAssetDocNew.Items(intLoop).FindControl("BPKBchk"), CheckBox)

                    isValueNeeded = DTGAssetDocNew.Items(intLoop).Cells(11).Text.Trim
                    MandatoryNew = DTGAssetDocNew.Items(intLoop).Cells(7).Text.Trim
                    MandatoryUsed = DTGAssetDocNew.Items(intLoop).Cells(8).Text.Trim


                    'Matikan sementara
                    'If CBool(lblIsNoRequired.Text) Then
                    '    If Not BPKBchk.Checked And TipeAplikasi <> "AGRI" Then
                    '        Status = False
                    '        ShowMessage(lblMessage, "BPKB harus dicentang dokumen yang diperlukan", True)
                    '        Exit Sub
                    '    End If
                    'End If


                    'And CBool(lblIsNoRequired.Text) And BPKBchk.Checked
                    'If Me.UsedNew = "N" And MandatoryNew = "1" Then
                    '    If chk.Checked = False Then
                    '        If MandatoryNew = "1" Then
                    '            lblvChk.Visible = True
                    '        Else
                    '            lblvChk.Visible = False
                    '        End If
                    '        If Status <> False Then
                    '            Status = False
                    '        End If
                    '    Else
                    '        If isValueNeeded = "True" And value = "" And CBool(lblIsNoRequired.Text) And BPKBchk.Checked Then
                    '            'If isValueNeeded = "True" And value = "" And BPKBchk.Checked Then
                    '            lblVNumber2.Visible = True
                    '            If Status <> False Then
                    '                Status = False
                    '            End If
                    '        End If
                    '    End If
                    'ElseIf Me.UsedNew = "U" And MandatoryUsed = "1" Then
                    '    If chk.Checked = False Then
                    '        If MandatoryUsed = "1" Then
                    '            lblvChk.Visible = True
                    '        Else
                    '            lblvChk.Visible = False
                    '        End If
                    '        If Status <> False Then
                    '            Status = False
                    '        End If
                    '    Else
                    '        If isValueNeeded = "True" And value = "" And CBool(lblIsNoRequired.Text) And BPKBchk.Checked Then
                    '            'If isValueNeeded = "True" And value = "" And BPKBchk.Checked Then
                    '            lblVNumber2.Visible = True
                    '            If Status <> False Then
                    '                Status = False
                    '            End If
                    '        End If
                    '    End If
                    'End If
                    txtnumber = CType(DTGAssetDocNew.Items(intLoop).FindControl("txtNumberNew"), TextBox)
                    If txtnumber.Text.Trim <> "" Then
                        oDO = New Parameter._DO
                        oData = New DataTable
                        oDO.strConnection = GetConnectionString()
                        oDO.Input = txtnumber.Text.Trim
                        oDO.AssetDocID = DTGAssetDocNew.Items(intLoop).Cells(9).Text.Trim
                        oDO.AssetTypeID = Me.AssetTypeID
                        oDO.BranchId = Me.BranchID
                        oDO.ApplicationID = Me.ApplicationID
                        oDO = m_controller.CheckAssetDoc(oDO)
                        oData = oDO.ListData
                        If oData.Rows.Count > 0 Then
                            'lblVNumber.Visible = True -------------
                            If Status <> False Then
                                Status = False
                            End If
                        End If
                    End If
                    objrow = oData4.NewRow
                    objrow("AssetDocID") = DTGAssetDocNew.Items(intLoop).Cells(9).Text.Trim
                    objrow("DocumentNo") = CType(DTGAssetDocNew.Items(intLoop).FindControl("txtNumberNew"), TextBox).Text.Trim
                    objrow("IsMainDoc") = DTGAssetDocNew.Items(intLoop).Cells(10).Text.Trim
                    objrow("IsDocExist") = IIf(CType(DTGAssetDocNew.Items(intLoop).FindControl("chkNew"), CheckBox).Checked = True, "1", "0").ToString
                    objrow("Notes") = CType(DTGAssetDocNew.Items(intLoop).FindControl("txtNotesNew"), TextBox).Text.Trim
                    objrow("PromiseDate") = CType(DTGAssetDocNew.Items(intLoop).FindControl("txtPromiseDate2"), TextBox).Text.Trim
                    oData4.Rows.Add(objrow)
                End If
            End If

        Next

        If ucSyaratCair1.ChkValidator() = False Then
            Status = False
        End If
        If Status = False Then
            ShowMessage(lblMessage, "Peringatan: Periksa kembali data, isi yang berlambang *", True)
            Exit Sub
        End If
    End Sub
#End Region

#Region "cek_Check"
    Sub cek_Check()
        Dim CheckTC As Boolean
        Dim CheckTC2 As Boolean
        Dim counter As Integer
        Dim PromiseDate As ValidDate
        Dim PromiseDate2 As ValidDate

        For counter = 0 To dtgTC.Items.Count - 1
            CheckTC = CType(dtgTC.Items(counter).FindControl("chkTCChecked"), CheckBox).Checked
            If CheckTC Then
                PromiseDate = CType(dtgTC.Items(counter).FindControl("uscTCPromiseDate"), ValidDate)
                PromiseDate.dateValue = ""
            End If
        Next

        For counter = 0 To dtgTC2.Items.Count - 1
            CheckTC2 = CType(dtgTC2.Items(counter).FindControl("chkTCCheck2"), CheckBox).Checked
            If CheckTC2 Then
                PromiseDate2 = CType(dtgTC2.Items(counter).FindControl("uscTCPromiseDate2"), ValidDate)
                PromiseDate2.dateValue = ""
            End If
        Next
    End Sub
#End Region

#Region "ItemDataBound"
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim txtPromiseDate As TextBox
            txtPromiseDate = CType(e.Item.FindControl("txtPromiseDate"), TextBox)
            txtPromiseDate.Attributes.Add("readonly", "true")
            CType(e.Item.FindControl("chkTCChecked"), CheckBox).Attributes.Add("OnClick", _
                "return CheckTC('" & Trim(CType(e.Item.FindControl("txtPromiseDate"),  _
                TextBox).ClientID) & "',this.checked);")
        End If
    End Sub
    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)

            Dim txtPromiseDate2 As TextBox

            txtPromiseDate2 = CType(e.Item.FindControl("txtPromiseDate2"), TextBox)
            txtPromiseDate2.Attributes.Add("readonly", "true")

            CType(e.Item.FindControl("chkTCCheck2"), CheckBox).Attributes.Add("OnClick", _
         "return CheckTC('" & Trim(CType(e.Item.FindControl("txtPromiseDate2"),  _
         TextBox).ClientID) & "',this.checked);")
        End If
    End Sub
    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        Dim lblIsDocExist As New Label
        lblIsDocExist = CType(e.Item.FindControl("lblIsDocExist"), Label)
        Dim lblIsNoRequired As New Label
        Dim lblNumberNo As New Label
        Dim lblVNumber2 As New Label
        Dim RFVNumber As New RequiredFieldValidator
        Dim chk As New CheckBox
        Dim BPKBchk As New CheckBox
        Dim lblNo As New Label 


        'Dim counter As Integer
        'counter = 0
        If e.Item.ItemIndex >= 0 Then
            'counter = counter + 1
            'lblNo.Text = CStr(counter)
            BPKBchk = CType(e.Item.FindControl("BPKBchk"), CheckBox)
            chk = CType(e.Item.FindControl("chk"), CheckBox)
            lblNo = CType(e.Item.FindControl("lblNo"), Label)
            lblIsNoRequired = CType(e.Item.FindControl("lblIsNoRequired"), Label)
            lblNumberNo = CType(e.Item.FindControl("lblNumberNo"), Label)
            lblVNumber2 = CType(e.Item.FindControl("lblVNumber2"), Label)
            RFVNumber = CType(e.Item.FindControl("RFVNumber"), RequiredFieldValidator)

            lblNo.Text = (e.Item.ItemIndex + 1).ToString
            If ((lblIsDocExist.Text <> "") Or (Not IsNothing(lblIsDocExist.Text))) Then
                If lblIsDocExist.Text = "True" Then
                    chk.Checked = True
                    chk.Enabled = False
                Else
                    chk.Checked = False
                    chk.Enabled = True
                End If
            End If

            'If CBool(lblIsNoRequired.Text) Then
            '    lblNumberNo.Visible = True
            '    RFVNumber.Visible = True
            'Else
            '    lblNumberNo.Visible = False
            '    RFVNumber.Visible = False
            '    lblVNumber2.Visible = False
            'End If
            lblNumberNo.Visible = False
            RFVNumber.Visible = False
            lblVNumber2.Visible = False

            BPKBchk.Checked = CBool(e.Item.Cells(10).Text.Trim)

        End If
    End Sub
    Private Sub DTGAssetDocNew_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DTGAssetDocNew.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNoNew"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub dtgAttribute_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAttribute.ItemDataBound
        Dim lblAttributeType As New Label
        lblAttributeType = CType(e.Item.FindControl("lblAttributeType"), Label)

        Dim RVAttribute As New RegularExpressionValidator
        RVAttribute = CType(e.Item.FindControl("RVAttribute"), RegularExpressionValidator)

        If e.Item.ItemIndex >= 0 Then
            If lblAttributeType.Text.Trim <> "C" Then
                RVAttribute.Enabled = True
                RVAttribute.ControlToValidate = "txtAttribute"
                RVAttribute.ValidationExpression = "\d*"
                RVAttribute.ErrorMessage = "Harap isi dengan angka"
            End If
        End If
    End Sub

    
#End Region

#Region "imbCancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.CmdWhere = ""
        BindGridEntity(Me.CmdWhere)
        pnlList.Visible = True
        pnlReturn.Visible = False
        pnlPO.Visible = False
        'Response.Redirect("DeliveryOrder.aspx")
    End Sub
#End Region

#Region "Initial Label Validator"
    Sub InitialLabelValidDTGAssetDocNew()
        Dim count As Integer
        count = CInt(IIf(DTGAssetDocNew.Items.Count > dtgAttribute.Items.Count, DTGAssetDocNew.Items.Count, dtgAttribute.Items.Count))
        For intLoop = 0 To count - 1
            If intLoop <= DTGAssetDocNew.Items.Count - 1 Then
                CType(DTGAssetDocNew.Items(intLoop).FindControl("lblVChkNew"), Label).Visible = False
                CType(DTGAssetDocNew.Items(intLoop).FindControl("lblVNumberNew"), Label).Visible = False
            End If
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label).Visible = False
            End If
        Next
    End Sub
#End Region

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Dim ParDO As New Parameter._DO
        Dim m_DO As New DOController

        ParDO.strConnection = GetConnectionString()
        ParDO.BusinessDate = Me.BusinessDate
        ParDO.BranchId = Me.BranchID
        ParDO.ApplicationID = hdnApplicationID.Value
        ParDO.AlasanReturn = txtAlasanReturn.Text.Trim

        Try
            m_DO.ApplicationReturnUpdate(ParDO)

            BindGridEntity(Me.CmdWhere)
            pnlList.Visible = True
            pnlPO.Visible = False
            pnlReturn.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Me.CmdWhere = ""
        BindGridEntity(Me.CmdWhere)
        pnlList.Visible = True
        pnlPO.Visible = False
        pnlReturn.Visible = False
    End Sub

#Region "Activity Log"
    Sub ActivityLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Application
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.BranchID, "'", "").ToString
        oApplication.ApplicationID = Me.ApplicationID.Trim
        oApplication.ActivityType = "DO"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 17

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Application

        oReturn = m_TCcontroller.ActivityLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub
#End Region
End Class