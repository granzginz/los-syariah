﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class ApplicationReject
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail

#Region "Constanta"
    Private Dcontroller As New DataUserControlController
    Private m_controller As New HasilSurveyController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
#Region "LinkTo"
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function
    Function LinkToSupplier(ByVal strStyle As String, ByVal strSupplierID As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function

    Function LinkToEmployee(ByVal strStyle As String, ByVal strBranchID As String, ByVal strAOID As String) As String
        Return "javascript:OpenWinEmployee('" & strStyle & "','" & strBranchID.Replace("'", "") & "','" & strAOID & "')"
    End Function
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "APPREJECT"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.Sort = "ApplicationId ASC"
            Me.CmdWhere = " a.branchid = " + sesBranchId.Trim + ""
            InitialPanel()
            BindDropDownList()
            BindGrid()
        End If
    End Sub
#End Region
    Sub BindGrid()
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.HasilSurvey
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetApplicationReject(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#Region "Bind Combo"
    Private Sub BindDropDownList()
        Dim m_DataUser As New DataUserControlController
        Dim data As New DataTable
        data = m_DataUser.GetReason(GetConnectionString(), "REJCT")
        cboReject.DataTextField = "Name"
        cboReject.DataValueField = "ID"        
        cboReject.DataSource = data
        cboReject.DataBind()

        cboReject.Items.Insert(0, "Select One")
        cboReject.Items(0).Value = ""
        cboReject.SelectedIndex = 0
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkApplicationID As New HyperLink
        Dim hynCustomerName As New HyperLink
        Dim hynSupplierName As New HyperLink
        Dim hynEmployeeName As New HyperLink
        If e.Item.ItemIndex >= 0 Then

            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            hynSupplierName = CType(e.Item.FindControl("hynSupplierName"), HyperLink)
            hynEmployeeName = CType(e.Item.FindControl("hynEmployeeName"), HyperLink)

            lnkApplicationID.NavigateUrl = LinkTo(lnkApplicationID.Text.Trim, "AccAcq")
            hynCustomerName.NavigateUrl = LinkToCustomer(e.Item.Cells(5).Text.Trim, "AccAcq")
            hynSupplierName.NavigateUrl = LinkToSupplier("AccAcq", e.Item.Cells(6).Text.Trim)
            hynEmployeeName.NavigateUrl = LinkToEmployee("AccAcq", Me.sesBranchId.Trim, e.Item.Cells(7).Text.Trim)

        End If
    End Sub
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String = ""
        Dim dt As DataTable = Nothing
        Dim oCustomClass As New Parameter.HasilSurvey
        Dim lnkApplicationID As New HyperLink
        Dim strCustomerID As String

        Dim oGroupSupplierAccount As New Parameter.HasilSurvey
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            pnlList.Visible = False
            pnlView.Visible = True

            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            hdnApplicationId.Value = lnkApplicationID.Text.Trim
            strCustomerID = e.Item.Cells(5).Text.Trim

            ucViewApplication1.ApplicationID = hdnApplicationId.Value
            ucViewApplication1.CustomerID = strCustomerID
            ucViewApplication1.bindData()
            ucViewApplication1.initControls("OnNewApplication")
            ucViewCustomerDetail1.CustomerID = strCustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlView.Visible = False
        txtPage.Text = "1"
    End Sub
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReject.Click
        Dim customClass As New Parameter.HasilSurvey
        Dim ErrMessage As String = ""
        Dim status As Boolean = False

        With customClass
            .ApplicationID = hdnApplicationId.Value.Trim
            .BusinessDate = Me.BusinessDate
            .BranchId = sesBranchId.Replace("'", "")
            .RejectionReasonID = cboReject.SelectedValue.Trim
            .strConnection = GetConnectionString()
        End With

        Try
            m_controller.ApplicationRejectUpdate(customClass)
            BindGrid()
            pnlList.Visible = True
            pnlView.Visible = False

            ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try                
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        pnlList.Visible = True
        pnlView.Visible = False
    End Sub
    Protected Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        'Dim search As String = ""

        'If txtSearch.Text <> "" Then
        '    If txtSearch.Text.Contains("%") Then
        '        search = "'%" & txtSearch.Text.Replace("%", " ").Trim & "%'"
        '    Else
        '        search = "'%" & txtSearch.Text.Trim & "%'"
        '    End If

        '    CmdWhere += " and " & cboSearch.SelectedValue & " like " & search & ""
        'End If


        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = " a.branchid = " + sesBranchId.Trim + " and " & cboSearch.SelectedValue & " = '" & txtSearch.Text.Trim & "'"
        Else
            Me.CmdWhere = " a.branchid = " + sesBranchId.Trim + ""
        End If

        BindGrid()
    End Sub
    Protected Sub ButtonReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonReset.Click
        txtSearch.Text = ""
        Me.CmdWhere = " a.branchid = " + sesBranchId.Trim + ""
        BindGrid()
    End Sub

End Class