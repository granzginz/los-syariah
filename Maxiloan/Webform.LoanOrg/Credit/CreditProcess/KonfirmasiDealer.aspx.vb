﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class KonfirmasiDealer
    Inherits Maxiloan.Webform.WebBased

#Region "Constants & Properties"
    Private oAssetDataController As New AssetDataController
    Private oKonfirmasiDealerController As New KonfirmasiDealerController
    Private oDOController As New DOController
    Protected WithEvents ucViewCustomerDetail As ucViewCustomerDetail
    Protected WithEvents ucViewApplication As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucViewApplication1 As ucViewApplication

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Dim Status As Boolean = True

    Private time As String
    Private m_Appcontroller As New ApplicationController

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Property AssetTypeID() As String
        Get
            Return CType(ViewState("AssetTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetTypeID") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property isRefinancing As Boolean
        Set(value As Boolean)
            ViewState("Refinancing") = value
        End Set
        Get
            Return CBool(ViewState("Refinancing"))
        End Get
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.FormID = "KDE"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If

                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time

                txtGoPage.Text = "1"
                Me.BranchID = Replace(Me.sesBranchId, "'", "")
                Me.CmdWhere = ""
                Me.SortBy = "a.AgreementNo"


                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    InitObjects()
                    BindGridEntity(Me.CmdWhere)
                    'txtTanggalApproval.Attributes.Add("readonly", "true")
                    'txtTanggalKonfirmasi.Attributes.Add("readonly", "true")
                    'txtTanggalEfektif.Attributes.Add("readonly", "true")
                    'txtTanggalKontrak.Attributes.Add("readonly", "true")                    
                Else
                    Response.Redirect("../../../error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub

    Protected Sub GetSerial()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oData2 As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.AssetID = Me.AssetTypeID
        oAssetData = oAssetDataController.GetSerial(oAssetData)
        oData = oAssetData.ListData

        lblSerial1.Text = oData.Rows(0).Item(0).ToString
        lblSerial2.Text = oData.Rows(0).Item(1).ToString
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

    Protected Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oPar As New Parameter.KonfirmasiDealer

        With oPar
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .BranchId = Me.BranchID
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
        End With

        oPar = oKonfirmasiDealerController.KonfirmasiDealerPaging(oPar)

        If Not oPar Is Nothing Then
            dtEntity = oPar.dsKonfirmasiDealer
            recordCount = oPar.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0

        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Select Case e.CommandName
            Case "KONFIRMASI_DEALER"
                pnlList.Visible = False
                pnlForm.Visible = True

                'If CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblTglAngsuranI"), Label).Text.Trim = "" Then
                '    txtTanggalEfektif.Text = ""
                'Else
                '    txtTanggalEfektif.Text = Format(CDate(CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblTglAngsuranI"), Label).Text), "dd/MM/yyyy")
                'End If

                txtTanggalApproval.Text = ""
                txtTanggalKonfirmasi.Text = Format(Me.BusinessDate, "dd/MM/yyyy")                

                Me.AssetTypeID = CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblAssetTypeID"), Label).Text
                Me.ApplicationID = CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label).Text
                Me.isRefinancing = CBool(CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblRefinancing"), Label).Text)

                If Not Me.isRefinancing Then GetSerial()                

                GetDefaultAssetData()
                'rfSerial1.ErrorMessage = "Isi " & lblSerial1.Text & "!"
                'rfSerial2.ErrorMessage = "Isi " & lblSerial2.Text & "!"

                ucViewApplication.ApplicationID = CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblApplicationID"), Label).Text
                ucViewApplication.CustomerID = CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblCustomerID"), Label).Text
                ucViewApplication.bindData()
                ucViewApplication.initControls("OnNewApplication")                
                ucViewCustomerDetail.CustomerID = CType(dtgPaging.Items(e.Item.ItemIndex).FindControl("lblCustomerID"), Label).Text
                ucViewCustomerDetail.bindCustomerDetail()
            Case "Return"
                pnlList.Visible = False
                pnlReturn.Visible = True
                pnlForm.Visible = False

                Dim lblCustomerID As New Label
                lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
                Dim hypAgreementNo As New HyperLink
                hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
                Dim lblApplicationID As New Label
                lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)

                hdnApplicationID.Value = lblApplicationID.Text.Trim
                ucViewApplication1.ApplicationID = lblApplicationID.Text.Trim
                ucViewApplication1.CustomerID = lblCustomerID.Text.Trim
                ucViewApplication1.bindData()
                ucViewApplication1.initControls("OnNewApplication")
                ucViewCustomerDetail1.CustomerID = lblCustomerID.Text.Trim
                ucViewCustomerDetail1.bindCustomerDetail()
        End Select
    End Sub

    Protected Sub GetDefaultAssetData()
        Try
            Dim controllerassetData As New EditApplicationController
            Dim EntitiesAssetData As New Parameter.Application
            Dim oDataAsset As New DataTable

            With EntitiesAssetData
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString
                .SpName = "EditAssetDataGetInfoAssetData"
            End With

            EntitiesAssetData = controllerassetData.EditAssetDataGetInfoAssetData(EntitiesAssetData)

            If Not EntitiesAssetData Is Nothing Then
                oDataAsset = EntitiesAssetData.ListData

                With oDataAsset
                    txtSerial1.Text = .Rows(0).Item("SerialNo1").ToString
                    txtSerial2.Text = .Rows(0).Item("SerialNo2").ToString

                    txtWarna.Text = .Rows(0).Item("Color").ToString
                    txtNamaSTNK.Text = .Rows(0).Item("OldOwnerAsset").ToString

                    txtTahunKendaraan.Text = .Rows(0).Item("ManufacturingYear").ToString
                    cboFirstInstallment.SelectedValue = .Rows(0).Item("FirstInstallment").ToString
                    'If Not IsDBNull(.Rows(0)("FirstInstallmentDate")) Then txtTanggalEfektif.Text = Format(.Rows(0)("FirstInstallmentDate"), "dd/MM/yyyy")
                End With

            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        pnlList.Visible = True
        pnlForm.Visible = False
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not CheckFeature(Me.Loginid, Me.FormID, "SAVE", Me.AppId) Then Exit Sub
        lblMessage.Text = ""
        lblMessage.Visible = False
        'CheckSerial()
        'If Not Status Then Exit Sub

        Dim oPar As New Parameter.KonfirmasiDealer
        Dim dt As New DataTable

        With oPar
            .strConnection = GetConnectionString()
            .BusinessDate = Me.BusinessDate
            .TanggalKontrak = Me.BusinessDate
            .ApplicationID = Me.ApplicationID
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .TanggalKonfirmasi = ConvertDate2(txtTanggalKonfirmasi.Text)
            .SerialNo1 = txtSerial1.Text.Trim
            .SerialNo2 = txtSerial2.Text.Trim            
            '.TanggalEfektif = ConvertDate2(txtTanggalEfektif.Text)
            .Warna = txtWarna.Text
            .NamaSTNK = txtNamaSTNK.Text
            .TahunKendaraan = txtTahunKendaraan.Text
            .FirstInstallment = cboFirstInstallment.SelectedValue
        End With

        

        Try

            dt = oKonfirmasiDealerController.ValidationSerialNo(oPar)

            If (dt.Rows(0).Item("Sts").ToString() = "Y") Then
                ShowMessage(lblMessage, dt.Rows(0).Item("Name").ToString(), True)
                Exit Sub
            Else
                oKonfirmasiDealerController.KonfirmasiDealerSave(oPar)
                'Modify by Wira 20171019
                ActivityLog()

                BindGridEntity(Me.CmdWhere)
                pnlForm.Visible = False
                pnlList.Visible = True
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim = "" And txtTanggalApproval.Text.Trim = "" Then
            Me.CmdWhere = ""
        ElseIf txtSearch.Text.Trim = "" And txtTanggalApproval.Text.Trim <> "" Then
            Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalApproval.Text.Trim) & "'"
        ElseIf txtSearch.Text.Trim <> "" And txtTanggalApproval.Text.Trim = "" Then
            If cboSearch.SelectedItem.Value = "Name" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "b." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "AgreementNo" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "EmployeeName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "d." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "SupplierName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "c." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            End If
        ElseIf txtSearch.Text.Trim <> "" And txtTanggalApproval.Text.Trim <> "" Then

            If cboSearch.SelectedItem.Value = "Name" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalApproval.Text.Trim) & "' and b." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "AgreementNo" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalApproval.Text.Trim) & "' and a." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "EmployeeName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalApproval.Text.Trim) & "' and d." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "SupplierName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalApproval.Text.Trim) & "' and c." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            End If
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub

    Protected Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        Me.CmdWhere = ""
        txtSearch.Text = ""
        txtTanggalApproval.Text = ""
        BindGridEntity(Me.CmdWhere)
        txtGoPage.Text = "1"
    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Dim ParDO As New Parameter._DO
        Dim m_DO As New DOController

        ParDO.strConnection = GetConnectionString()
        ParDO.BusinessDate = Me.BusinessDate
        ParDO.BranchId = Me.sesBranchId.Replace("'", "").Trim
        ParDO.ApplicationID = hdnApplicationID.Value
        ParDO.AlasanReturn = txtAlasanReturn.Text.Trim

        Try
            m_DO.ApplicationReturnUpdate(ParDO)

            BindGridEntity(Me.CmdWhere)
            pnlList.Visible = True
            pnlForm.Visible = False
            pnlReturn.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Me.CmdWhere = ""
        BindGridEntity(Me.CmdWhere)
        pnlList.Visible = True
        pnlForm.Visible = False
        pnlReturn.Visible = False
    End Sub

    Protected Sub CheckSerial()
        If txtSerial1.Text.Trim <> "" Or txtSerial2.Text.Trim <> "" Then
            Dim ErrS1 As Boolean = False
            Dim ErrS2 As Boolean = False

            Dim oDO As New Parameter._DO
            Dim oData As New DataTable

            oDO.strConnection = GetConnectionString()
            oDO.SerialNo1 = txtSerial1.Text
            oDO.SerialNo2 = txtSerial2.Text
            oDO.AssetTypeID = Me.AssetTypeID
            oDO.BranchId = Me.BranchID
            oDO.ApplicationID = Me.ApplicationID

            oDO = oDOController.CheckSerial(oDO)
            oData = oDO.ListData

            If oData.Rows.Count <> 0 Then
                oData = oDO.ListData
                Dim intLoop As Integer
                For intLoop = 0 To oData.Rows.Count - 1
                    If txtSerial1.Text.Trim <> "" Then
                        If oData.Rows(intLoop).Item(0).ToString.Trim = txtSerial1.Text.Trim Then
                            ErrS1 = True
                        End If
                    End If
                    If txtSerial2.Text.Trim <> "" Then
                        If oData.Rows(intLoop).Item(1).ToString.Trim = txtSerial2.Text.Trim Then
                            ErrS2 = True
                        End If
                    End If
                Next
                If ErrS1 = True And ErrS2 = True Then
                    lblErrSerial1.Visible = True
                    lblErrSerial1.Text = "Serial No 1 sudah ada"
                    lblErrSerial2.Visible = True
                    lblErrSerial2.Text = "Serial No 2 sudah ada"
                    Status = False
                    Exit Sub
                Else
                    If ErrS1 = True Then
                        lblErrSerial1.Visible = True
                        lblErrSerial1.Text = "Serial No 1 sudah ada"
                        Status = False
                        Exit Sub
                    ElseIf ErrS2 = True Then
                        lblErrSerial2.Visible = True
                        lblErrSerial2.Text = "Serial No 2 sudah ada"
                        Status = False
                        Exit Sub
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub InitObjects()
        lblMessage.Visible = False
        pnlList.Visible = True
        pnlForm.Visible = False
        'txtTanggalEfektif.Text = ""
        txtTanggalApproval.Text = ""
        txtTanggalKonfirmasi.Text = ""
    End Sub

#Region "Activity Log"
    Sub ActivityLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Application
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.BranchID, "'", "").ToString
        oApplication.ApplicationID = Me.ApplicationID.Trim
        oApplication.ActivityType = "KDE"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 15

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Application

        oReturn = m_Appcontroller.ActivityLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub
#End Region
End Class