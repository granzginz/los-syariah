﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RCA_002
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New RCAController
    Private m_Appcontroller As New ApplicationController
    Dim Status As Boolean
    Dim oRow As DataRow
    Dim intLoop As Integer
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private time As String
#End Region

#Region "Property"
    Property NTF() As Double
        Get
            Return CDbl(viewstate("NTF"))
        End Get
        Set(ByVal Value As Double)
            viewstate("NTF") = Value
        End Set
    End Property
    Property SchemeID() As String
        Get
            Return viewstate("SchemeID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SchemeID") = Value
        End Set
    End Property
    Property AgreementDate() As String
        Get
            Return viewstate("AgreementDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementDate") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateStart = Me.BusinessDate + " " + time

        If Not Page.IsPostBack Then
            lblMessage.Text = ""
            lblMessage.Visible = False
            drdContract.Attributes.Add("onChange", "javascript:ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);")
            btnSave.Attributes.Add("OnClick", "return checksubmit();")
            Me.CustomerName = Request("Name")
            Me.CustomerID = Request("CustomerID")
            Me.ApplicationID = Request("App")
            Me.SchemeID = Request("Scheme")
            Me.NTF = CDbl(Request("NTF"))

            lblApplicationID.NavigateUrl = LinkTo(Me.ApplicationID, "AccAcq")
            lblCustName.NavigateUrl = LinkToCustomer(Me.CustomerID, "AccAcq")
            Bind()
            bindComboCompany()
            Response.Write(GenerateScript(getComboContract, getComboBatchDate))
            'lblMsgAgreementNo.Text = ""
            'lblMsgAgreementNo.Visible = False
        End If
        LabelValidationFalse()

    End Sub
#End Region

#Region "Bind"
    Sub Bind()
        'Dim oData As New DataTable
        'oData = Get_UserApproval(Me.SchemeID, Replace(Me.sesBranchId, "'", ""), Me.NTF)
        'cboApprovedBy.DataSource = oData.DefaultView
        'cboApprovedBy.DataTextField = "Name"
        'cboApprovedBy.DataValueField = "ID"
        'cboApprovedBy.DataBind()
        'cboApprovedBy.Items.Insert(0, "Select One")
        'cboApprovedBy.Items(0).Value = "0"

        lblCustName.Text = Me.CustomerName
        lblApplicationID.Text = Me.ApplicationID
        TC()
        TC2()
    End Sub
#End Region

#Region "BindTC"
    Sub TC()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        oApplication.strConnection = GetConnectionString
        oApplication.AppID = Me.ApplicationID
        oApplication.BranchId = Me.sesBranchId
        oApplication.AddEdit = "GoLive"
        oApplication = m_Appcontroller.GetTC(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        dtgTC.DataSource = oData.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()

        Dim intLoop As Integer
        For intLoop = 0 To dtgTC.Items.Count - 1
            If oData.Rows(intLoop).Item(4).ToString.Trim <> "" Then                
                CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), TextBox).Text = Format(oData.Rows(intLoop).Item(4), "dd/MM/yyyy")
            End If
        Next
    End Sub
    Sub TC2()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        oApplication.strConnection = GetConnectionString
        oApplication.BranchId = Me.sesBranchId
        oApplication.AppID = Me.ApplicationID
        oApplication.AddEdit = "GoLive"
        oApplication = m_Appcontroller.GetTC2(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        dtgTC2.DataSource = oData.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()
        Dim intLoop As Integer
        For intLoop = 0 To dtgTC2.Items.Count - 1
            If oData.Rows(intLoop).Item(5).ToString.Trim <> "" Then
                CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), TextBox).Text = Format(oData.Rows(intLoop).Item(5), "dd/MM/yyyy")
            End If
        Next
    End Sub
#End Region

#Region "LabelValidationFalse"
    Sub LabelValidationFalse()
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim countDtg As Integer = CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
        Dim validCheck As New Label
        Dim Checked As Boolean
        Dim PriorTo As String
        Dim PromiseDate As TextBox
        Dim Mandatory As String

        For intLoop = 0 To countDtg
            If intLoop <= DtgTC1count Then
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), TextBox)
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = False
                If PriorTo = "REQ" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
            If intLoop <= DtgTC2count Then
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), TextBox)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = False
                If PriorTo = "REQ" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
        Next
    End Sub
#End Region

#Region "ItemDataBound"
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            CType(e.Item.FindControl("chkTCChecked"), CheckBox).Attributes.Add("OnClick", _
                "return CheckTC('" & Trim(CType(e.Item.FindControl("txtPromiseDate"),  _
                TextBox).ClientID) & "',this.checked);")
        End If
    End Sub
    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            CType(e.Item.FindControl("chkTCCheck2"), CheckBox).Attributes.Add("OnClick", _
         "return CheckTC('" & Trim(CType(e.Item.FindControl("txtPromiseDate2"),  _
         TextBox).ClientID) & "',this.checked);")
        End If
    End Sub
#End Region

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If cboApprovedBy.SelectedIndex > 0 Then
        'If txtAgreementNo.Text.Trim = "" Then
        '    lblMsgAgreementNo.Text = "AgreementNo is required!"
        '    lblMsgAgreementNo.Visible = True
        '    Exit Sub
        'End If

        If drdCompany.SelectedItem.Value <> "All" Then
            If tempChild.Value.Trim = "-" Or tempChild.Value.Trim = "" Then
                ShowMessage(lblMessage, "Harap pilih No Kontrak bank atau tidak pilih Funding Bank", True)
                bindComboCompany()
                Response.Write(GenerateScript(getComboContract, getComboBatchDate))
                Exit Sub
            Else
                If tempChild2.Value.Trim = "-" Or tempChild2.Value.Trim = "" Then
                    ShowMessage(lblMessage, "Harap pilih No batch atau tidak pilih Funding Bank dan No Kontrak Bank", True)
                    bindComboCompany()
                    Response.Write(GenerateScript(getComboContract, getComboBatchDate))
                    Exit Sub
                End If
            End If
        End If

        Dim strBackDated As String, strAwalBln As String
        Dim tempdate As Date
        Dim oData1 As New DataTable
        Dim oData2 As New DataTable

        Status = True

        oData1.Columns.Add("MasterTCID", GetType(String))
        oData1.Columns.Add("PriorTo", GetType(String))
        oData1.Columns.Add("IsChecked", GetType(String))
        oData1.Columns.Add("IsMandatory", GetType(String))
        oData1.Columns.Add("PromiseDate", GetType(String))
        oData1.Columns.Add("Notes", GetType(String))

        oData2.Columns.Add("MasterTCID", GetType(String))
        oData2.Columns.Add("AGTCCLSequenceNo", GetType(String))
        oData2.Columns.Add("IsChecked", GetType(String))
        oData2.Columns.Add("PromiseDate", GetType(String))
        oData2.Columns.Add("Notes", GetType(String))

        Validator(oData1, oData2)

        If Status = False Then
            LabelValidationFalse()
            cek_Check()
            Exit Sub
        End If

        Dim oRCA As New Parameter.RCA

        With oRCA
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .BusinessDate = Me.BusinessDate
            .AppID = Me.ApplicationID
            .SchemeID = Me.SchemeID
            .Notes = txtRecommendation.Text.Trim
            .Argumentasi = txtArgumentasi.Text.Trim
            .RefundAmount = Me.NTF
            .LoginId = Me.Loginid
            .RequestBy = "" 'cboApprovedBy.SelectedValue
            .AgreementNo = "-" ' txtAgreementNo.Text.Trim
            .strConnection = GetConnectionString()
            .FundingCoyId = drdCompany.SelectedItem.Value
            .FundingContractNo = tempChild.Value.Trim
            .FundingBatchNo = tempChild2.Value.Trim
        End With

        Try
            m_controller.RCASave(oRCA, oData1, oData2)
            'Modifyby Wira 20171017
            ActivityLog()
            Response.Redirect("RCA.aspx")
        Catch exp As Exception
            ShowMessage(lblMessage, "Data Sudah Ada", True)
        End Try

        'Else
        '    lblMessage.Text = "Harap Pilih Akan disetujui Oleh"

        'End If

    End Sub

    Sub Validator(ByRef oData1 As DataTable, ByRef oData2 As DataTable)
        Dim PriorTo As String
        Dim Mandatory As String
        Dim Checked As Boolean
        Dim PromiseDate As TextBox
        Dim validCheck As New Label
        Dim MasterTCID, MasterTCID2, AGTCCLSequenceNo, Notes, AgreementNo As String
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim objrow As DataRow

        For intLoop = 0 To CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
            If intLoop <= DtgTC1count Then
                MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), TextBox)
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                Notes = CType(dtgTC.Items(intLoop).Cells(6).FindControl("txtTCNotes"), TextBox).Text
                If PromiseDate.Text <> "" Then
                    If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = True
                    End If
                End If
                If PriorTo = "REQ" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.Text = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                    validCheck.Visible = True
                End If
                objrow = oData1.NewRow
                objrow("MasterTCID") = MasterTCID
                objrow("PriorTo") = PriorTo
                objrow("IsChecked") = IIf(Checked = True, "1", "0")
                objrow("IsMandatory") = IIf(Mandatory = "v", "1", "0")
                objrow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
                objrow("Notes") = Notes
                oData1.Rows.Add(objrow)
            End If
            If intLoop <= DtgTC2count Then
                MasterTCID = dtgTC2.Items(intLoop).Cells(8).Text
                AGTCCLSequenceNo = dtgTC2.Items(intLoop).Cells(9).Text
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Notes = CType(dtgTC2.Items(intLoop).Cells(7).FindControl("txtTCNotes2"), TextBox).Text
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), TextBox)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                If PromiseDate.Text <> "" Then
                    If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = True
                    End If
                End If
                If PriorTo = "REQ" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.Text = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                        validCheck.Visible = True
                    End If
                End If
                objrow = oData2.NewRow
                objrow("MasterTCID") = MasterTCID
                objrow("AGTCCLSequenceNo") = AGTCCLSequenceNo
                objrow("IsChecked") = IIf(Checked = True, "1", "0")
                objrow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
                objrow("Notes") = Notes
                oData2.Rows.Add(objrow)
            End If
        Next
        Dim RainCheck As Boolean = False
        Dim intLoop2, intLoop3, intLoop4, intLoop5 As Integer
        For intLoop = 0 To DtgTC1count
            MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
            Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
            For intLoop2 = 0 To DtgTC2count
                MasterTCID2 = dtgTC2.Items(intLoop2).Cells(8).Text
                If MasterTCID = MasterTCID2 Then
                    If Checked = True Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True

                        If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = False Then
                            CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                        Else
                            CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                        End If
                    Else
                        If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = True Then
                            For intLoop4 = 0 To intLoop2
                                If dtgTC2.Items(intLoop4).Cells(8).Text = MasterTCID Then
                                    CType(dtgTC2.Items(intLoop4).FindControl("lblVTC2Checked"), Label).Visible = True
                                End If
                            Next

                            For intLoop5 = intLoop2 To DtgTC2count
                                If dtgTC2.Items(intLoop5).Cells(8).Text = MasterTCID Then
                                    CType(dtgTC2.Items(intLoop5).FindControl("lblVTC2Checked"), Label).Visible = True
                                End If
                            Next


                            CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                            For intLoop3 = 0 To intLoop
                                If dtgTC.Items(intLoop3).Cells(7).Text = MasterTCID2 Then
                                    If CType(dtgTC.Items(intLoop3).FindControl("chkTCChecked"), CheckBox).Checked = False Then
                                        CType(dtgTC.Items(intLoop3).FindControl("lblVTCChecked"), Label).Visible = True
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            Next
            RainCheck = False
        Next
    End Sub
#End Region

#Region "cek_Check"
    Sub cek_Check()
        Dim CheckTC As Boolean
        Dim CheckTC2 As Boolean
        Dim counter, counter2 As Integer
        Dim PromiseDate As ValidDate
        Dim PromiseDate2 As ValidDate

        For counter = 0 To dtgTC.Items.Count - 1
            CheckTC = CType(dtgTC.Items(counter).FindControl("chkTCChecked"), CheckBox).Checked
            If CheckTC Then
                PromiseDate = CType(dtgTC.Items(counter).FindControl("txtPromiseDate"), ValidDate)
                PromiseDate.dateValue = ""
            End If
        Next

        For counter = 0 To dtgTC2.Items.Count - 1
            CheckTC2 = CType(dtgTC2.Items(counter).FindControl("chkTCCheck2"), CheckBox).Checked
            If CheckTC2 Then
                PromiseDate2 = CType(dtgTC2.Items(counter).FindControl("txtPromiseDate2"), ValidDate)
                PromiseDate2.dateValue = ""
            End If
        Next
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
#End Region

#Region "imbCancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("RCA.aspx")
    End Sub
#End Region

#Region "imgView"
    Private Sub imgView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        Dim cookieNew As New HttpCookie("PersonalCustomerRpt")
        cookieNew.Values.Add("CustomerID", Me.CustomerID)
        cookieNew.Values.Add("LoginID", Me.Loginid)
        Response.AppendCookie(cookieNew)
        Response.Redirect("../RptPersonalCustomerViewer.aspx")
    End Sub
#End Region

#Region "Bind Combo Funding"
#Region "Generate Script"
    Private Function GenerateScript(ByVal DtChild As DataTable, ByVal DtChild2 As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "function ChildVTPair (value,text,parent)" & vbCrLf
        strScript &= "{" & vbCrLf
        strScript &= "			this.value = value;" & vbCrLf
        strScript &= "					this.text = text;" & vbCrLf
        strScript &= "		this.parent = parent;" & vbCrLf
        strScript &= "				}" & vbCrLf

        If DtChild.Rows.Count > 0 Then
            strScript &= "var AOSupervisor = new Array(" & vbCrLf

            For j = 0 To DtChild.Rows.Count - 1
                strScript &= " new ChildVTPair('" & CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                                CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                                CStr(DtChild.Rows(j).Item("FundingCoyID")).Trim & "')," & vbCrLf
            Next
            strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
            strScript &= ");" & vbCrLf
        End If

        strScript &= "var BatchDate = new Array(" & vbCrLf

        For j = 0 To DtChild2.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingContractNo")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var tampungGrandChild = '" & tempChild.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChild2 = '" & tempChild2.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChildName = '" & tempChildName.ClientID & "';" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function
#End Region

#Region "GetComboContract"
    Private Function getComboContract() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboContractRCA"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Function
#End Region
#Region "GetComboBatchDate"
    Private Function getComboBatchDate() As DataTable
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBatchDate"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Function
#End Region
#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "All"
        Catch e As Exception
            ShowMessage(lblMessage, e.Message, True)
        End Try

    End Sub
#End Region
    Protected Function drdCompanyChange() As String
        Return "ChangeMultiCombo(this,'" & drdContract.ClientID & "', AOSupervisor, '" & tempParent.ClientID & "');"
    End Function

    Protected Function drdContractChange() As String
        Return "ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);"
    End Function
#End Region
#Region "Activity Log"
    Sub ActivityLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Application
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ApplicationID = Me.ApplicationID.Trim
        oApplication.ActivityType = "REQ"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 11

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Application

        oReturn = m_Appcontroller.ActivityLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub
#End Region
End Class