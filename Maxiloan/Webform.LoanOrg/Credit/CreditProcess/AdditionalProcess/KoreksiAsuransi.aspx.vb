﻿Imports Maxiloan.Controller
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController

Public Class KoreksiAsuransi
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    
#End Region

    Private oController As New NewAppInsuranceByCompanyController
    Private oInsAppController As New InsuranceApplicationController
    Private oCustomClassResult As New Parameter.InsuranceCalculationResult
    Private oInsCalResultController As New InsuranceCalculationResultController
    Protected WithEvents ucViewApplication As ucViewApplication
    Protected WithEvents ucViewCustomerDetail As ucViewCustomerDetail

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oApplication As New Parameter.Application
        Dim m_ControllerApp As New ApplicationController

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                InitialDefatultPanel()

                Me.ApplicationID = Request.QueryString("AppID")
                Me.CustomerID = Request("id")

                'Me.PageSource = Request.QueryString("PageSource")
                'Me.SupplierGroupID = Request.QueryString("SupplierGroupID")

                'oApplicationType.SupplierGroupID = Me.SupplierGroupID

                ucViewApplication.ApplicationID = Me.ApplicationID
                ucViewApplication.CustomerID = Me.CustomerID
                ucViewApplication.bindData()
                ucViewApplication.initControls("OnNewApplication")
                ucViewCustomerDetail.CustomerID = Me.CustomerID
                ucViewCustomerDetail.bindCustomerDetail()

                Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany

                With oNewAppInsuranceByCompany
                    .ApplicationID = Me.ApplicationID.Trim
                    .strConnection = GetConnectionString()
                End With

                Try
                    oNewAppInsuranceByCompany = oController.GetInsuranceEntryStep1List(oNewAppInsuranceByCompany)
                Catch ex As Exception
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND, True)
                End Try

                With oNewAppInsuranceByCompany
                    'Me.CustomerName = .CustomerName
                    'Me.CustomerID = .CustomerID
                    'lblAssetNewused.Text = .AssetUsageNewUsed
                    'Me.AssetNewUsed = lblAssetNewused.Text
                    'lblInsuranceAssetType.Text = .InsuranceAssetDescr
                    'LblInsuredBy.Text = .InsAssetInsuredByName
                    'LblPaidBy.Text = .InsAssetPaidByName

                    'Try
                    '    LblAssetUsageID.Text = .AssetUsageID.Trim
                    'Catch ex As Exception
                    '    ShowMessage(lblMessage, MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND & MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND_ASSETUSAGE & " for this applicationID = " & Me.ApplicationID, True)
                    '    btnOK.Visible = False
                    '    Exit Sub
                    'End Try

                    'Me.AssetUsageID = LblAssetUsageID.Text.Trim
                    'LblAssetUsageDescr.Text = .AssetUsageDescr
                    'TxtTenorCredit.Text = CType(.Tenor, String)
                    'lblOTR.Text = FormatNumber(.TotalOTR, 0)
                    'txtNilaiAksesoris.Text = "0"
                    'Me.AmountCoverage = CType(TxtAmountCoverage.Text, Double)
                    'Me.AmountCoverage = CType(CDbl(lblOTR.Text) + CDbl(txtNilaiAksesoris.Text), Double)
                    'TxtAmountCoverage.Text = CType(.TotalOTR, String)
                    'lblAmountCoverage.Text = FormatNumber(Me.AmountCoverage, 0)
                    'LblMinimumTenor.Text = CType(.MinimumTenor, String)
                    'LblMaximumTenor.Text = CType(.MaximumTenor, String)

                    'rgvTenorCredit.MinimumValue = CType(.MinimumTenor, String)
                    'rgvTenorCredit.MaximumValue = CType(.MaximumTenor, String)
                    'rgvTenorCredit.ErrorMessage = "Tenor must <= " & CType(.MaximumTenor, String) & " and >= " & CType(.MinimumTenor, String)

                    'LblBranchID.Text = .BranchId
                    'Me.MinimumTenor = LblMinimumTenor.Text
                    'Me.MaximumTenor = LblMaximumTenor.Text
                    'Me.PMaximumTenor = CStr(.PMaximumTenor)
                    'Me.PMinimumTenor = CStr(.PMinimumTenor)
                    'Me.PBMaximumTenor = CStr(.PBMaximumTenor)
                    'Me.PBMinimumTenor = CStr(.PBMinimumTenor)

                    Me.BranchID = .BranchId
                    'txtInsAdminFee.Text = CType(.InsAdminFee, String)
                    'TxtInsStampDutyFee.Text = CType(.InsStampDutyFee, String)
                    'LblInsAdminFeeBehaviour.Text = .InsAdminFeeBehaviour
                    'LblInsStampDutyBehavior.Text = .InsStampDutyFeeBehaviour

                    'If LblInsAdminFeeBehaviour.Text.Trim.ToUpper = "L" Then
                    '    txtInsAdminFee.Enabled = False
                    'Else
                    '    txtInsAdminFee.Enabled = False
                    'End If


                    'If LblInsStampDutyBehavior.Text.Trim.ToUpper = "L" Then
                    '    TxtInsStampDutyFee.Enabled = False
                    'Else
                    '    TxtInsStampDutyFee.Enabled = False
                    'End If

                    'Me.AdminFeeBehavior = LblInsAdminFeeBehaviour.Text.Trim.ToUpper
                    'Me.StampDutyFeeBehavior = LblInsStampDutyBehavior.Text.Trim.ToUpper
                    'Me.NilaiAdminFeeAwal = .InsAdminFee
                    'Me.NilaiStampDutyFeeAwal = .InsStampDutyFee
                    'Me.InsuranceType = .InsuranceType
                    'Me.ManufacturingYear = .ManufacturingYear
                    'Me.RateCardID = .SesionID

                    txtJenisAksesoris.Text = .JenisAksesoris
                    TxtInsNotes.Text = .InsNotes


                End With
                'CheckProspect()
            End If

            'oApplication.BranchId = Replace(Me.sesBranchId, "'", "")
            'oApplication.ApplicationID = Me.ApplicationID
            'oApplication.strConnection = GetConnectionString()
            'oApplication = m_ControllerApp.GetDataAppIDProspect(oApplication)

            'If oApplication.Err = "" Then
            '    If Me.ApplicationID.Trim = oApplication.ApplicationID.Trim Then
            '        TxtTenorCredit.Text = CStr(oApplication.Tenor)
            '    End If
            'End If

            'With ucViewApplication1
            '    .CustomerID = Me.CustomerID
            '    .ApplicationID = Me.ApplicationID
            '    .bindData()

            '    If .IsAppTimeLimitReached Then
            '        ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
            '        btnOK.Visible = False
            '    End If

            '    .initControls("OnNewApplication")
            '    .CustomerID = Me.CustomerID
            'End With

            'With ucViewCustomerDetail1
            '    .CustomerID = Me.CustomerID
            '    .bindCustomerDetail()
            'End With

            'With ucApplicationTab1
            '    .ApplicationID = Me.ApplicationID
            '    .selectedTab("Asuransi")
            '    .setLink()
            'End With


        End If
    End Sub

    

    Sub InitialDefatultPanel()
        lblMessage.Visible = False
        'PnlGrid.Visible = False
        'PnlEntry.Visible = False
        'LblApplicationType.Visible = False
        'PnlDGrid2Insurance.Visible = False
        'pnlInsSelection.Visible = False
        'btnOK.Visible = True
        'txtRfnSupplier.Text = "0"
        'txtNilaiAksesoris.RequiredFieldValidatorEnable = False
        'txtNilaiAksesoris.AutoPostBack = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If Not SequenceValidasi() Then
        '    Exit Sub
        'End If
        hdnApplicationID.Value = Me.ApplicationID
        hdnCustomerID.Value = Me.CustomerID
        Dim checktotal As Integer

        'Try
        '    lblMessage.Visible = False

        '    Dim oCheck As New Parameter.InsuranceCalculation

        '    With oCheck
        '        .ApplicationID = Me.ApplicationID
        '        .strConnection = GetConnectionString()
        '    End With

        '    Dim OcontrollerCheck As New InsuranceApplicationController
        '    oCheck = OcontrollerCheck.GetDateEntryInsuranceData(oCheck)

        '    With oCheck
        '        checktotal = .TotalRecord
        '        Context.Trace.Write("TotalRecord  = " & .TotalRecord & "")
        '    End With

        '    If checktotal > 0 Then
        '        ShowMessage(lblMessage, " Data Suda Ada ", True)
        '        btnSave.Visible = False
        '        Exit Sub
        '    End If

        'Catch ex As Exception
        '    Dim err As New MaxiloanExceptions
        '    err.WriteLog("NewAppInsuranceByCompany.aspx", "Save-check dateinsurancedata", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        'End Try

        Dim oCustomClass As New Parameter.InsuranceCalculationResult

        'If Me.InsuranceComBranchID_lbl = "" Then
        '    Me.InsuranceComBranchID_lbl = "0"
        'End If

        With oCustomClass
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            '.AmountCoverage = Me.AmountCoverage
            '.AdminFeeToCust = CType(txtInsAdminFee.Text, Double)
            '.MeteraiFeeToCust = CType(TxtInsStampDutyFee.Text, Double)
            '.DiscToCustAmount = CType(Replace(TxtDiscountPremium.Value, ",", ""), Double)
            '.PaidAmountByCust = CType(Replace(TxtPaidAmountByCustomer.Value, ",", ""), Double)
            '.PremiumBaseForRefundSupp = CType(TxtPremiumBase.Text, Double)
            '.AccNotes = TxtAccNotes.Text.Trim
            .AccNotes = ""
            .InsNotes = TxtInsNotes.Text.Trim
            '.InsLength = Me.InsLength
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
            '.PremiumToCustAmount = CDbl(LblTotalPremiumByCust.Text)
            '.InsuranceComBranchID = Me.InsuranceComBranchID_lbl
            '.AdditionalCapitalized = CDbl(TxtAdditionalCap.Text)
            '.AdditionalInsurancePremium = CDbl(TxtAdditionalInsurancePremium.Value)
            '.RefundToSupplier = CDbl(txtRfnSupplier.Text.Trim)
            .JenisAksesoris = txtJenisAksesoris.Text
            '.NilaiAksesoris = CDbl(txtNilaiAksesoris.Text)
            .isKoreksi = True
            .alasanKoreksi = txtAlasanKoreksi.Text
        End With

        Try
            oInsAppController.ProcessKoreksiAsuransi(oCustomClass)
            'PnlEntry.Visible = False
            'PnlGrid.Visible = False
            'PnlDGrid2Insurance.Visible = False
            'DisplayGridInsco()
            'pnlInsSelection.Visible = True
            Me.ApplicationID = hdnApplicationID.Value
            Me.CustomerID = hdnCustomerID.Value
            ShowMessage(lblMessage, "Data Berhasil Disimpan", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Private Sub btnCancelPalingBawah_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelPalingBawah.Click
        Response.Redirect("KoreksiNonFinansial.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&id=" & Me.CustomerID.ToString.Trim & "&Status=Koreksi")
    End Sub
End Class