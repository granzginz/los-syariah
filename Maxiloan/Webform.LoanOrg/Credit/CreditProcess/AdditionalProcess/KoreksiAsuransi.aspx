﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KoreksiAsuransi.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.KoreksiAsuransi" %>

<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Koreksi Data Asuransi</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                KOREKSI DATA ASURANSI</h3>
        </div>
    </div>    
    <div class="form_box">           
        <div class="form_left">
            <uc3:ucviewapplication id="ucViewApplication" runat="server" />
        </div>
        <div class="form_right">
            <uc2:ucviewcustomerdetail id="ucViewCustomerDetail" runat="server" />
        </div>
    </div>
     <div class="form_box_title_nobg">
        <div class="form_single">
            <label class="label_general">
                Keterangan Koreksi
            </label>
            <asp:TextBox ID="txtAlasanKoreksi" runat="server" TextMode="MultiLine" Rows="3" Columns="40" />
            <asp:RequiredFieldValidator ID="rfvAlasanKoreksi" runat="server" ControlToValidate="txtAlasanKoreksi"
                Display="Dynamic" ErrorMessage="Alasan harus diisi" CssClass="validator_general" />
        </div>
    </div>    
    <div class="form_box">
        <div>
            <div class="form_single">
                <label>
                    Jenis Aksesoris
                </label>
                <asp:TextBox runat="server" ID="txtJenisAksesoris" CssClass="long_text"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <%--<div class="form_left">
                    <label>
                        Catatan Accessories
                    </label>
                </div>--%>
            <div class="form_left">
                <label>
                    Catatan Asuransi
                </label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <%--<div class="form_left">
                    <asp:TextBox ID="TxtAccNotes" runat="server" MaxLength="500" TextMode="MultiLine"
                        CssClass="multiline_textbox"></asp:TextBox>
                </div>--%>
            <div class="form_left">
                <asp:TextBox ID="TxtInsNotes" runat="server" MaxLength="500" TextMode="MultiLine"
                    CssClass="multiline_textbox"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancelPalingBawah" Text="Cancel" CssClass="small button gray"
            runat="server" CausesValidation="False"></asp:Button>
    </div>
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    <input id="hdnCustomerID" type="hidden" name="hdnCustomerID" runat="server" />
    </form>
</body>
</html>
