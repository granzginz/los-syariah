﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TolakanNonCust.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.TolakanNonCust" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ucDateCE" Src="../../../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc3" TagName="ucNumberFormat" Src="../../../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc4" TagName="ucSupplier" Src="../../../../Webform.UserController/ucSupplier.ascx" %>
<%@ Register TagPrefix="uc5" TagName="ucAsset" Src="../../../../webform.UserController/ucAsset.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Koreksi Aset</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <script type="text/javascript" src="../../../../Maxiloan.js"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                TOLAKAN NON CUSTOMER
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="NoKTP" BorderStyle="None" BorderWidth="0">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../../Images/iconedit.png"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" ImageUrl="../../../../Images/icondelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PRINT">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPrint" runat="server" CommandName="Print" CausesValidation="False">PRINT</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA" SortExpression="Nama">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNama" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Nama") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblBranchID" runat="server" Visible="false" CausesValidation="false"
                                        Text='<%# DataBinder.eval(Container,"DataItem.BranchID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblID" runat="server" Visible="false" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.IDTolakanNonCust") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ALAMAT" SortExpression="Alamat">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAlamat" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Alamat") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JENIS ASSET" SortExpression="JENISKend">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblJENISKend" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.JENISKend") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SUPPLIER" SortExpression="NamaSupplier">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNamaSupplier" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.NamaSupplier") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ALASAN PENOLAKAN" SortExpression="AlasanPenolakan">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAlasanPenolakan" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.AlasanPenolakan") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DETAIL PENOLAKAN" SortExpression="DetailPenolakan">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDetailPenolakan" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.DetailPenolakan") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KETERANGAN" SortExpression="Keterangan">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblKeterangan" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Keterangan") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                        </asp:ImageButton>
                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
            <asp:Button Visible="false" ID="ButtonPrint" runat="server" Enabled="true" Text="Print" CssClass="small button blue">
            </asp:Button>
            <asp:Button Visible="false" ID="ButtonBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h5>
                    MENCARI TOLAKAN NON CUSTOMER
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Nama">Nama</asp:ListItem>
                    <asp:ListItem Value="Alamat">Alamat</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                CausesValidation="False"></asp:Button>
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlAddEdit">
        <div class="form_box_title">
            <div class="form_single">
                <h5>
                    <asp:Label runat="server" ID="lblTitle"></asp:Label>
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Nama
                </label>
                <asp:TextBox ID="txtNama" runat="server" />
                <asp:RequiredFieldValidator ID="rfvNama" runat="server" ControlToValidate="txtNama"
                    Display="Dynamic" ErrorMessage="Nama harus diisi" CssClass="validator_general" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    No. KTP
                </label>
                <asp:TextBox ID="txtNoKTP" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNoKTP"
                    Display="Dynamic" ErrorMessage="No. KTP harus diisi" CssClass="validator_general" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Pekerjaan
                </label>
                <asp:TextBox ID="txtPekerjaan" runat="server" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPekerjaan"
                    Display="Dynamic" ErrorMessage="Pekerjaan harus diisi" CssClass="validator_general" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Status Perkawinan</label>
                <asp:DropDownList ID="cboMarital" AutoPostBack="true" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                    InitialValue="" ControlToValidate="cboMarital" ErrorMessage="*" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Alasan Penolakan</label>
                <asp:DropDownList ID="cboAlasanPenolakan" runat="server" CssClass="select" AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Alasan Penolakan"
                    ControlToValidate="cboAlasanPenolakan" Display="Dynamic" InitialValue="Select One"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Detail Penolakan</label>
                <asp:DropDownList ID="cboDetailPenolakan" runat="server" CssClass="select">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap pilih Detail Penokan"
                    ControlToValidate="cboDetailPenolakan" Display="Dynamic" InitialValue="Select One"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Keterangan</label>
                <asp:TextBox ID="txtKeterangan" runat="server" MaxLength="20" TextMode="MultiLine"
                    CssClass="multiline_textbox_uc"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Tempat / Tanggal Lahir</label>
                <asp:TextBox ID="txtBirthPlace" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
                <label class="label_auto">
                    /</label>
                <uc2:ucdatece id="txtBirthDate" runat="server" />
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccompanyadress id="UCAddress" runat="server"></uc1:uccompanyadress>
        </div>
        <div class="form_box">
            <div class="form_single">
                <uc4:ucsupplier id="UCSupplier" runat="server"></uc4:ucsupplier>
            </div>
        </div>
        <uc5:ucasset id="ucAsset" runat="server"></uc5:ucasset>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Jumlah</label>
                <uc3:ucnumberformat runat="server" id="txtJumlah"></uc3:ucnumberformat>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelPalingBawah" Text="Cancel" CssClass="small button gray"
                runat="server" CausesValidation="False"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
