﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CancelGoLive.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CancelGoLive" %>

<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../../../../webform.UserController/UCReason.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cancel Agreement</title>
    <link href="../../../../Include/General.css" type="text/css" rel="Stylesheet" />
    <link href="../../../../Include/Buttons.css" type="text/css" rel="Stylesheet" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var x = screen.width;
        var y = screen.height - 100;        		
    </script>
    <script language="JavaScript" type="text/javascript">
        function OpenWinSupplier(pSupplierID, pStyle) {
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/ViewSupplier.aspx?style=' + pStyle + '&SupplierID=' + pSupplierID, 'Supplier', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinEmployee(pBranchID, pAOID, pStyle) {
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pAOID, 'Employee', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
               		
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR KONTRAK</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" DataKeyField="ApplicationID"
                        CellSpacing="1" CellPadding="3" BorderWidth="0px" OnSortCommand="Sorting" AutoGenerateColumns="False"
                        AllowSorting="True" CssClass="grid_general">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="hyAction" runat="server" CommandName="DoCancel" Text='BATAL'
                                        CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                                <ItemStyle HorizontalAlign="Left" Width="13%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNO")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Step" HeaderText="STEP">
                                <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblStep" runat="server" Text='<%#container.dataitem("Step")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STATUS KONTRAK">
                                <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="DEFAULTSTATUS" HeaderText="STATUS DEFAULT">
                                <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDEFAULTSTATUS" runat="server" Text='<%#Container.DataItem("DEFAULTSTATUS")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="CUSTOMER ID">
                                <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%#container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGopage" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_box">
            <div class="form_single">
                <h4>
                    CARI KONTRAK UNTUK DIBATALKAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="AGR.ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="AGR.AgreementNO">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="CUST.[Name]">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlProcess" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    PROSES PEMBATALAN KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Customer
                    </label>
                    <asp:HyperLink ID="hyCustomerName_CAP" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        No Kontrak
                    </label>
                    <asp:HyperLink ID="hyAgreementNO_CAP" runat="server"></asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Alasan Pembatalan
                    </label>
                    <uc1:ucreason id="oUCReason" runat="server" autopostback="true" onReasonSelected="ReasonSelected"></uc1:ucreason>
                </div>
                <div class="form_right">
                    <label class="label_general">
                        Catatan
                    </label>
                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox_uc"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    &nbsp;</label>
                <asp:CheckBox ID="chkCopyApplication" runat="server" Text="Copy menjadi aplikasi baru" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
