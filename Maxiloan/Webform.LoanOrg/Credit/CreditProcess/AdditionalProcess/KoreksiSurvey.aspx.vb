﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

Public Class KoreksiSurvey
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat
    Protected WithEvents ucViewApplication As ucViewApplication
    Protected WithEvents ucViewCustomerDetail As ucViewCustomerDetail
    Private oCustomerController As New CustomerController
    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property

    Private Property CSResult_Temp() As String
        Get
            Return CType(ViewState("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CSResult_Temp") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "KOREKSISURVEY"
            Me.CustomerID = Request("id")
            Me.ApplicationID = Request("AppID")
            Me.BranchID = sesBranchId.Replace("'", "")
            'load ucViewApplication on the fly
            ucViewApplication1 = CType(Page.LoadControl("../../../../webform.UserController/ucViewApplication.ascx"), ucViewApplication)

            'GetCookies()
            InitialPageLoad()
            'fillFormTesting()
            getHasilSurvey()

            ucViewApplication.ApplicationID = Me.ApplicationID
            ucViewApplication.CustomerID = Me.CustomerID
            ucViewApplication.bindData()
            ucViewApplication.initControls("OnNewApplication")
            ucViewCustomerDetail.CustomerID = Me.CustomerID
            ucViewCustomerDetail.bindCustomerDetail()

        End If
    End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
            End If
        End With

        FillCbo("tblBankAccount")

        'With ucViewCustomerDetail1
        '    .CustomerID = Me.CustomerID
        '    .bindCustomerDetail()
        'End With

        Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        Dim WhereSup As String = "SupplierID = '" & Me.SupplierID & "'"

        'FillCboEmp("BranchEmployee", cboCreditAnalyst, WhereBranch + " and (EmployeePosition='CA' or EmployeePosition='AO') ")
        'FillCboEmp("BranchEmployee", cboSurveyor, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")
        FillCboEmp("BranchEmployee", cboCreditAnalyst, WhereBranch + " and IsCA = 1 ")
        FillCboEmp("BranchEmployee", cboSurveyor, WhereBranch + " and IsSurveyor = 1 ")

        With txtLamaSurvey
            .Text = "1"
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "1"
            .RangeValidatorMaximumValue = "99"
            .TextCssClass = "numberAlign small_text"
            .setErrMsg()
        End With
        With txtJumlahKendaraan
            .Text = "1"
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "1"
            .RangeValidatorMaximumValue = "99"
            .TextCssClass = "numberAlign small_text"
            .setErrMsg()
        End With
        With txtOrderKe
            .Text = "1"
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "1"
            .RangeValidatorMaximumValue = "99"
            .TextCssClass = "numberAlign small_text"
            .setErrMsg()
        End With
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("KoreksiNonFinansial.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&id=" & Me.CustomerID.ToString.Trim & "&Status=Koreksi")
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        hdnApplicationID.Value = Me.ApplicationID
        hdnCustomerID.Value = Me.CustomerID
        Dim oPar As New Parameter.HasilSurvey
        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .SurveyorID = cboSurveyor.SelectedValue
                .CAID = cboCreditAnalyst.SelectedValue
                .AgreementSurveyDate = Date.ParseExact(txtTanggalSurvey.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .SaldoRataRata = CDbl(txtSaldoRataRata.Text)
                .SaldoAwal = CDbl(txtSaldoAwal.Text)
                .SaldoAkhir = CDbl(txtSaldoAkhir.Text)
                .JumlahPemasukan = CDbl(txtJumlahPemasukan.Text)
                .JumlahPengeluaran = CDbl(txtJumlahPengeluaran.Text)
                .JumlahHariTransaksi = CInt(txtJumlahHariTransaksi.Text)
                .BusinessDate = Me.BusinessDate
                .SurveyorNotes = txtSurveyorNotes.Text
                .JenisRekening = cboBankAccType.SelectedValue
                .LamaSurvey = CShort(txtLamaSurvey.Text)
                .JumlahKendaraan = CShort(txtJumlahKendaraan.Text)
                .Garasi = CBool(rboGarasi.SelectedValue)
                .PertamaKredit = cboPertamaKredit.SelectedValue
                .OrderKe = CShort(txtOrderKe.Text)

                .isKoreksi = True
                .alasanKoreksi = txtAlasanKoreksi.Text
            End With

            'save scoring
            Dim scoringData As New Parameter.CreditScoring
            With scoringData
                '.ApplicationStep = "SCO"
                .ApplicationStep = "APK"
                .CreditScoringDate = Me.BusinessDate
                .CreditScore = Me.CreditScore
                .CreditScoringResult = Me.CreditScoreResult
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
                .CSResult_Temp = Me.CSResult_Temp
            End With

            oController.HasilSurveySave(oPar)

            SetScoring()

            Dim errMsg As String = saveScoring()


            'ucApplicationTab1.ApplicationID = Me.ApplicationID
            'ucApplicationTab1.selectedTab("Survey")
            'ucApplicationTab1.setLink()
            ucViewApplication1 = CType(Page.LoadControl("../../../../webform.UserController/ucViewApplication.ascx"), ucViewApplication)
            InitialPageLoad()
            getHasilSurvey()

            Me.ApplicationID = hdnApplicationID.Value
            Me.CustomerID = hdnCustomerID.Value
            If errMsg = String.Empty Then
                ShowMessage(lblMessage, "Data saved!", False)
            Else
                ShowMessage(lblMessage, "Data saved! but scoring data error : " & errMsg, True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            txtTanggalSurvey.Text = .AgreementSurveyDate.ToString("dd/MM/yyyy")
            cboSurveyor.SelectedIndex = cboSurveyor.Items.IndexOf(cboSurveyor.Items.FindByValue(oPar.SurveyorID))
            cboCreditAnalyst.SelectedIndex = cboCreditAnalyst.Items.IndexOf(cboCreditAnalyst.Items.FindByValue(oPar.CAID))
            txtSaldoRataRata.Text = .SaldoRataRata.ToString
            txtSaldoAwal.Text = .SaldoAwal.ToString
            txtSaldoAkhir.Text = .SaldoAkhir.ToString
            txtJumlahPemasukan.Text = .JumlahPemasukan.ToString
            txtJumlahPengeluaran.Text = .JumlahPengeluaran.ToString
            txtJumlahHariTransaksi.Text = .JumlahHariTransaksi.ToString
            txtSurveyorNotes.Text = .SurveyorNotes.ToString
            txtLamaSurvey.Text = .LamaSurvey.ToString
            txtJumlahKendaraan.Text = FormatNumber(.JumlahKendaraan, 0)
            rboGarasi.SelectedIndex = rboGarasi.Items.IndexOf(rboGarasi.Items.FindByValue(oPar.Garasi.ToString.Trim))
            If oPar.PertamaKredit.ToString.Trim <> "" Then
                cboPertamaKredit.SelectedIndex = cboPertamaKredit.Items.IndexOf(cboPertamaKredit.Items.FindByValue(oPar.PertamaKredit.ToString))
            Else
                cboPertamaKredit.SelectedIndex = cboPertamaKredit.Items.IndexOf(cboPertamaKredit.Items.FindByValue("YA"))
            End If
            cboBankAccType.SelectedIndex = cboBankAccType.Items.IndexOf(cboBankAccType.Items.FindByValue(oPar.JenisRekening.ToString))
            txtOrderKe.Text = FormatNumber(oPar.OrderKe.ToString, 0)

            'load Scoring data if exist
            If oPar.creditScore <> 0 Then
                SetScoring()
            End If
        End With
    End Sub

#Region "Scoring"
    Private Sub SetScoring()
        Dim scoringData As New Parameter.CreditScoring_calculate

        With scoringData
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationId = Me.ApplicationID
        End With
        scoringData = CreditScoringMdl.CalculateCreditScoring(scoringData)

        'set result        
        'lblGrade.Text = scoringData.lblGrade
        'lblResult.Text = scoringData.ReturnResult
        Me.CreditScore = scoringData.CreditScore
        Me.CreditScoreResult = scoringData.CreditScoreResult
        Me.CSResult_Temp = scoringData.CSResult_Temp

        'If lblResult.Text <> "" Then
        '    pnlScoring.Visible = True
        'End If
    End Sub

    Private Function saveScoring() As String
        Try
            Dim customClass As New Parameter.CreditScoring
            Dim scoringController As New CreditScoringController
            With customClass
                '.ApplicationStep = "SCO"
                .ApplicationStep = "APK"
                .CreditScoringDate = Me.BusinessDate
                .CreditScore = Me.CreditScore
                .CreditScoringResult = Me.CreditScoreResult
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
                .CSResult_Temp = Me.CSResult_Temp
            End With

            scoringController.CreditScoringSaveAdd(customClass, False)
            Return String.Empty
        Catch ex As Exception
            Return ex.Message
        End Try

    End Function
#End Region
#Region "combo button"
    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Sub FillCbo(ByVal table As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer

        oCustomer.strConnection = GetConnectionString
        oCustomer.Table = table
        oCustomer = oCustomerController.CustomerPersonalAdd(oCustomer)

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        Select Case table
            Case "tblBankAccount"
                With cboBankAccType
                    .DataSource = dtEntity
                    .DataTextField = "Description"
                    .DataValueField = "ID"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = ""
                End With
        End Select
    End Sub
#End Region

    
End Class