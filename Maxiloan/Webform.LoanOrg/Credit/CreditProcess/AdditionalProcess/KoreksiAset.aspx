﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KoreksiAset.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.KoreksiAset" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc" TagName="UcCompanyAdress" Src="../../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc" TagName="UCAO" Src="../../../../webform.UserController/UCAO.ascx" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Koreksi Aset</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <div class="title_strip">
            </div>
            <h3>
                KOREKSI ASET
            </h3>
        </div>
    </div>    
    <div class="form_box">           
        <div class="form_left">
            <uc3:ucviewapplication id="ucViewApplication" runat="server" />
        </div>
        <div class="form_right">
            <uc2:ucviewcustomerdetail id="ucViewCustomerDetail" runat="server" />
        </div>
    </div>
    <div class="form_box_title_nobg">
        <div class="form_single">
            <label class="label_general">
                Keterangan Koreksi
            </label>
            <asp:TextBox ID="txtAlasanKoreksi" runat="server" TextMode="MultiLine" Rows="3" Columns="40" />
            <asp:RequiredFieldValidator ID="rfvAlasanKoreksi" runat="server" ControlToValidate="txtAlasanKoreksi"
                Display="Dynamic" ErrorMessage="Alasan harus diisi" CssClass="validator_general" />
        </div>
    </div>    
    <div class="form_box">
        <div class="form_left">
            <label>
                Uang Muka bayar di
            </label>
            <asp:DropDownList ID="cboUangMukaBayar" runat="server">
                <asp:ListItem Selected="True" Text="SUPPLIER" Value="S"></asp:ListItem>
                <asp:ListItem Text="ANDALAN FINANCE" Value="A"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label class="label_req">
                    Penggunaan
                </label>
                <asp:DropDownList ID="cboUsage" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="cboUsage"
                    CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap pilih Penggunaan!"
                    InitialValue="Select One"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Pencairan Ke
                </label>
                <asp:DropDownList ID="cboPencairanKe" runat="server">
                    <asp:ListItem Text="SUPPLIER" Value="S" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="CUSTOMER" Value="A"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                REGISTRASI BPKB</h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_general">
                Nama BPKB sama dengan Kontrak?
            </label>
            <asp:RadioButtonList ID="rboNamaBPKBSamaKontrak" runat="server" AutoPostBack="True"
                CssClass="opt_single" RepeatDirection="Horizontal">
                <asp:ListItem Value="True" Text="Ya" Selected="True"></asp:ListItem>
                <asp:ListItem Value="False" Text="Tidak"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama BPKB
            </label>
            <asp:TextBox ID="txtName" runat="server" MaxLength="50" CssClass="long_text"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:Button ID="btnCopyAddress" runat="server" CausesValidation="False" Text="Copy Address"
                CssClass="small buttongo blue"></asp:Button>
        </div>
    </div>
    <div class="form_box_uc">
        <uc:uccompanyadress id="UCAddress" runat="server">
                    </uc:uccompanyadress>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req" runat="server" id="lblTanggalSTNK">
                Tanggal STNK
            </label>
            <asp:TextBox ID="txtTanggalSTNK" runat="server" CssClass="small_text"></asp:TextBox>
            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTanggalSTNK"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <asp:RequiredFieldValidator runat="server" ID="rfvTanggalSTNK" ControlToValidate="txtTanggalSTNK"
                Enabled="false" ErrorMessage="Harap isi tanggal STNK!" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_general">
                Keterangan BPKB
            </label>
            <asp:TextBox ID="txtAssetNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox_uc"></asp:TextBox>
        </div>
    </div>
     
    <div class="form_box_header">
        <div>
            <div class="form_left">
                <h4>
                    KARYAWAN</h4>
            </div>
            <div class="form_right">
                <h4>
                    KARYAWAN SUPPLIER
                </h4>
                <label>
                </label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Pembiayaan Marketing Officer
                </label>
                <asp:TextBox runat="server" ID="txtAO"></asp:TextBox>
                <asp:Button runat="server" ID="btnLookupAO" Text="..." CausesValidation="false" CssClass="small buttongo blue" />
                <uc:ucao id="ucAO1" runat="server" onaoselected="AOSelected">
                        </uc:ucao>
                <asp:Label ID="lblErrAccOff" runat="server" Visible="False"></asp:Label>
            </div>
            <div class="form_right">
                <label class="label_req">
                    Salesman
                </label>
                <asp:DropDownList ID="cboSalesman" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Harap pilih Salesman"
                    ControlToValidate="cboSalesman" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Sales Supervisor
                </label>
                <asp:DropDownList ID="cboSalesSpv" runat="server">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                </label>
            </div>
            <div class="form_right">
                <label>
                    Supplier Admin
                </label>
                <asp:DropDownList ID="cboSupplierAdm" runat="server">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
    </div>
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    <input id="hdnCustomerID" type="hidden" name="hdnCustomerID" runat="server" />
    </form>
</body>
</html>
