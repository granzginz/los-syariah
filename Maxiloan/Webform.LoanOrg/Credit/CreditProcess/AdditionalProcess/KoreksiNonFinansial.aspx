﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KoreksiNonFinansial.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.KoreksiNonFinansial" %>

<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Application Maintenance</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinSupplier(pStyle, pSupplierID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/ViewSupplier.aspx?style=' + pStyle + '&SupplierID=' + pSupplierID, 'Supplier', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinEmployee(pStyle, pBranchID, pAOID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pAOID, 'Employee', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR APLIKASI</h3>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlList">
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="imgPilih" runat="server" CausesValidation="False" Text="MODIFY"
                                        CommandName="Pilih"></asp:LinkButton>                                                                   
                                </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">                            
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkApplication" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">                            
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierName" HeaderText="NAMA SUPPLIER">                            
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkSupplier" Text='<%# DataBinder.eval(Container,"DataItem.SupplierName")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AO" HeaderText="NAMA ACCOUNT OFFICER">                            
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAO" Text='<%# DataBinder.eval(Container,"DataItem.AO")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="STEP APLIKASI">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblApplicationStep" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationStep")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="CustomerID">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCustomerID" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Type">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblType" Text='<%# DataBinder.eval(Container,"DataItem.Type")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="SupplierID">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSupplierID" Text='<%# DataBinder.eval(Container,"DataItem.SupplierID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="AOID">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAOID" Text='<%# DataBinder.eval(Container,"DataItem.AOID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ProductID">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProductID" Text='<%# DataBinder.eval(Container,"DataItem.ProductID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ProductOfferingID">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProductOfferingID" Text='<%# DataBinder.eval(Container,"DataItem.ProductOfferingID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                         CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" 
                        Display="Dynamic"  CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI APLIKASI</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="Name">Name Customer</asp:ListItem>
                    <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                    <asp:ListItem Value="AO">Nama Account Officer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CssClass="small button blue" Text="Find"
                CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDaftarAplikasi">
        <div class="form_box_header">
            <div class="form_single">              
                <h4>
                    KOREKSI APLIKASI
                </h4>
            </div>
        </div>
        <div class="form_box">           
            <div class="form_left">
                <uc3:ucviewapplication id="ucViewApplication" runat="server" />
            </div>
            <div class="form_right">
                <uc2:ucviewcustomerdetail id="ucViewCustomerDetail" runat="server" />
            </div>
        </div>
        <div class="form_box">   
            <div class="form_left">
                <asp:HyperLink ID="hyAplikasi" runat="server" Text='APLIKASI'></asp:HyperLink>
            </div>
            <div class="form_right">
                <asp:HyperLink ID="hyAset" runat="server" Text='ASET'></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">   
            <div class="form_left">
                <asp:HyperLink ID="hyAsuransi" runat="server" Text='ASURANSI'></asp:HyperLink>
            </div>
            <div class="form_right">
                <asp:HyperLink ID="hyHasilSurvey" runat="server" Text='HASIL SURVEY'></asp:HyperLink>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
