﻿#Region "Imports"
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Globalization
#End Region

Public Class TolakanNonCust
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property ReasonID() As String
        Get
            Return ViewState("ReasonID").ToString
        End Get
        Set(ByVal value As String)
            ViewState("ReasonID") = value
        End Set
    End Property
    Property ID() As Integer
        Get
            Return CInt(ViewState("ID"))
        End Get
        Set(ByVal value As Integer)
            ViewState("ID") = value
        End Set
    End Property
    Property Asset() As String
        Get
            Return ViewState("Asset").ToString
        End Get
        Set(ByVal value As String)
            ViewState("Asset") = value
        End Set
    End Property
    Property WhereCboDetail() As String
        Get
            Return ViewState("WhereCboDetail").ToString
        End Get
        Set(ByVal value As String)
            ViewState("WhereCboDetail") = value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property WhereCond() As String
        Get
            Return CType(ViewState("WhereCond"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("WhereCond") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("AddEdit"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AddEdit") = Value
        End Set
    End Property
    Private Property SelectItem() As String
        Get
            Return (CType(ViewState("SelectItem"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectItem") = Value
        End Set
    End Property

    Private Property SelectValue() As String
        Get
            Return (CType(ViewState("SelectValue"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SelectValue") = Value
        End Set
    End Property
#End Region

#Region "constanta"
    Protected WithEvents UCAddress As UcCompanyAddress
    Protected WithEvents txtBirthDate As ucDateCE
    Protected WithEvents UCSupplier As ucSupplier
    Protected WithEvents ucAsset As ucAsset
    Protected WithEvents txtJumlah As ucNumberFormat

    Dim oRow As DataRow
    Private m_controller As New AssetDataController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
    Private Sub InitObjects()
        lblMessage.Text = ""
        lblMessage.Visible = False
        Show()
    End Sub

#Region "FillCbo"
    Sub FillCbo(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.TolakanNonCust
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.table = Table
        oAssetData = m_controller.GetcboAlasanPenolakan(oAssetData)
        oData = oAssetData.listdata
        cboAlasanPenolakan.DataSource = oData
        cboAlasanPenolakan.DataTextField = "ReasonID"
        cboAlasanPenolakan.DataValueField = "ReasonID"
        cboAlasanPenolakan.DataBind()
        cboAlasanPenolakan.Items.Insert(0, "Select One")
        cboAlasanPenolakan.Items(0).Value = "Select One"

    End Sub
    Sub FillCboDetail(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.TolakanNonCust
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.table = Table
        oAssetData = m_controller.GetcboAlasanPenolakan(oAssetData)
        oData = oAssetData.listdata
        cboDetailPenolakan.DataSource = oData
        cboDetailPenolakan.DataTextField = "Description"
        cboDetailPenolakan.DataValueField = "Description"
        cboDetailPenolakan.DataBind()
        cboDetailPenolakan.Items.Insert(0, "Select One")
        cboDetailPenolakan.Items(0).Value = "Select One"
    End Sub
    Sub FillCboStatus(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim dtEntity As DataTable
        Dim oAssetData As New Parameter.TolakanNonCust
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Table
        oAssetData.table = Table
        oAssetData = m_controller.GetcboAlasanPenolakan(oAssetData)
        If Not oAssetData Is Nothing Then
            dtEntity = oAssetData.listdata
        End If
        cboMarital.DataSource = dtEntity.DefaultView
        cboMarital.DataTextField = "Description"
        cboMarital.DataValueField = "ID"
        cboMarital.DataBind()
        cboMarital.Items.Insert(0, "Select One")
        cboMarital.Items(0).Value = ""
    End Sub
#End Region
#Region "paging"
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.TolakanNonCust
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetTolakanNonCustPaging(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.totalrecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#Region "Sorting"

    Private Sub dtgPaging_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        Dim oCustom As New Parameter.TolakanNonCust
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imgDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")

        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region


    Sub Show()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub

#Region "BindEdit"
    Sub BindEdit(ByVal ID As Integer, ByVal BranchID As String)
        Dim oCustomClass As New Parameter.TolakanNonCust
        Dim oData As New DataTable
        Dim x As String

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.ID = ID
        oCustomClass = m_controller.GetTolakanNonCustEdit(oCustomClass)
        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.listdata
        End If
        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            ' Me.BranchID
            txtNama.Text = oRow("Nama").ToString.Trim
            txtNoKTP.Text = oRow("NoKTP").ToString.Trim
            cboMarital.SelectedIndex = cboMarital.Items.IndexOf(cboMarital.Items.FindByValue(oRow("Status").ToString.Trim))



            If oRow.Item(5).ToString.Trim <> "" Then
                txtBirthDate.Text = Format(oRow.Item(5), "dd/MM/yyyy")
                'txtBirthDate.Text = Format(oRow("TglLahir"), "dd/MM/yyyy")
            End If

            txtPekerjaan.Text = oRow("Pekerjaan").ToString.Trim
            txtKeterangan.Text = oRow("Keterangan").ToString.Trim
            txtBirthPlace.Text = oRow("TempatLahir").ToString.Trim
            txtJumlah.Text = FormatNumber(oRow.Item("jumlah"), 0)

            cboAlasanPenolakan.SelectedIndex = cboAlasanPenolakan.Items.IndexOf(cboAlasanPenolakan.Items.FindByValue(oRow("AlasanPenolakan").ToString.Trim))
            FillCboDetail("tblDetailPenolakan", cboDetailPenolakan, "")
            cboDetailPenolakan.SelectedIndex = cboDetailPenolakan.Items.IndexOf(cboDetailPenolakan.Items.FindByValue(oRow("DetailPenolakan").ToString.Trim))

            UCSupplier.SupplierID = oRow("SupplierID").ToString.Trim
            UCSupplier.SupplierName = oRow("NamaSupplier").ToString.Trim
            ucAsset.AssetCode = oRow("AssetCode").ToString.Trim
            ucAsset.AssetName = oRow("JENISKend").ToString.Trim

            With UCAddress
                .Address = oRow("Alamat").ToString.Trim
                .RT = oRow("RT").ToString.Trim
                .RW = oRow("RW").ToString.Trim
                .Kelurahan = oRow("Kelurahan").ToString.Trim
                .Kecamatan = oRow("Kecamatan").ToString.Trim
                .City = oRow("Kota").ToString.Trim
                .ZipCode = oRow("KodePos").ToString.Trim
                .AreaPhone1 = oRow("AreaPhone1").ToString.Trim
                .Phone1 = oRow("Phone1").ToString.Trim
                .AreaPhone2 = oRow("AreaPhone2").ToString.Trim
                .Phone2 = oRow("Phone2").ToString.Trim
                .AreaFax = oRow("AreaFax").ToString.Trim
                .Fax = oRow("Fax").ToString.Trim
                .BindAddress()
            End With
        End If

    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If SessionInvalid() Then
                Exit Sub
            End If
            Me.FormID = "TolakanNonCust"
           
            If Not Page.IsPostBack Then
                Me.BranchID = Replace(Me.sesBranchId, "'", "")
                InitObjects()

                If Request.QueryString("strFileLocation") <> "" Then
                    Dim strFileLocation As String
                    strFileLocation = "../../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; var y = screen.height;  window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                    & "</script>")
                End If

                Dim WhereCbo As String
                WhereCbo = "ReasonTypeID='CANCL' AND ReasonID='LOSTDEAL' OR ReasonID='KAPASITAS'"
                FillCbo("Reason", cboAlasanPenolakan, WhereCbo)
                
                FillCboStatus("tblMaritalStatus", cboMarital, "")
                UCSupplier.ApplicationID = ""

                UCSupplier.BindData()
                ucAsset.AssetTypeID = "MOBIL"
                ucAsset.BindData()

                Me.Sort = "BranchID ASC"
                Me.CmdWhere = "all"
                BindGrid(Me.CmdWhere)

            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        lblMessage.Visible = False
        Try
            Dim oAssetData As New Parameter.TolakanNonCust
            Dim oAddress As New Parameter.Address
            Dim oData1 As New DataTable

            With oAssetData
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .Nama = txtNama.Text.Trim
                .NoKTP = txtNoKTP.Text.Trim
                .StatusPerkawinan = cboMarital.SelectedValue
                .BirthDate = ConvertDate(txtBirthDate.Text).ToString
                .Pekerjaan = txtPekerjaan.Text.Trim

                .AlasanPenolakan = cboAlasanPenolakan.SelectedValue
                .DetailPenolakan = cboDetailPenolakan.SelectedValue
                .SupplierID = UCSupplier.SupplierID.Trim
                .AssetCode = ucAsset.AssetCode.Trim
                .jumlah = CInt(txtJumlah.Text)
                .Keterangan = txtKeterangan.Text.Trim
                .BirthPlace = txtBirthPlace.Text.Trim
            End With

            oAssetData.strConnection = GetConnectionString()
            oAssetData.AddEdit = Me.AddEdit

            If Me.AddEdit = "Add" Then
                oAssetData.ID = 0
            ElseIf Me.AddEdit = "Edit" Then
                oAssetData.ID = Me.ID
            End If

            oAddress.Address = UCAddress.Address
            oAddress.RT = UCAddress.RT
            oAddress.RW = UCAddress.RW
            oAddress.Kelurahan = UCAddress.Kelurahan
            oAddress.Kecamatan = UCAddress.Kecamatan
            oAddress.City = UCAddress.City
            oAddress.ZipCode = UCAddress.ZipCode
            oAddress.AreaPhone1 = UCAddress.AreaPhone1
            oAddress.Phone1 = UCAddress.Phone1
            oAddress.AreaPhone2 = UCAddress.AreaPhone2
            oAddress.Phone2 = UCAddress.Phone2
            oAddress.AreaFax = UCAddress.AreaFax
            oAddress.Fax = UCAddress.Fax

            Dim oReturn As New Parameter.TolakanNonCust
            oReturn = m_controller.TolakanNonCustSaveAdd(oAssetData, oAddress)
            If oReturn.Output <> "" Then
                ShowMessage(lblMessage, oReturn.Output, True)
                Show()
                BindGrid(Me.CmdWhere)
            Else
                ShowMessage(lblMessage, "Data saved!", False)
                Show()
                BindGrid(Me.CmdWhere)
            End If
            BindGrid(Me.CmdWhere)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub cboAlasanPenolakan_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboAlasanPenolakan.SelectedIndexChanged
        If cboAlasanPenolakan.SelectedValue = "LOSTDEAL" Then
            Me.WhereCboDetail = "IDTolakan = 'LOSTDEAL'"
            FillCboDetail("tblDetailPenolakan", cboDetailPenolakan, Me.WhereCboDetail)
        ElseIf cboAlasanPenolakan.SelectedValue = "KAPASITAS" Then
            Me.WhereCboDetail = "IDTolakan = 'KAPASITAS'"
            FillCboDetail("tblDetailPenolakan", cboDetailPenolakan, Me.WhereCboDetail)
        End If
    End Sub

    Protected Sub ButtonAdd_Click(sender As Object, e As EventArgs) Handles ButtonAdd.Click
        pnlList.Visible = False
        pnlAddEdit.Visible = True
        Clear()
        Me.AddEdit = "Add"
        lblTitle.Text = "ADD"
    End Sub

    Sub Clear()
        txtNama.Text = ""
        txtNoKTP.Text = ""
        txtPekerjaan.Text = ""
        cboMarital.ClearSelection()
        cboAlasanPenolakan.ClearSelection()
        cboDetailPenolakan.ClearSelection()
        txtKeterangan.Text = ""
        txtBirthPlace.Text = ""
        txtBirthDate.Text = ""
        txtJumlah.Text = "0"

        With UCAddress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kelurahan = ""
            .Kecamatan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .BindAddress()
        End With
        With UCSupplier
            .SupplierID = ""
            .SupplierName = ""
            .BindData()
        End With
        With ucAsset
            .AssetName = ""
            .BindData()
        End With
    End Sub

    Private Sub dtgPaging_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim ID As String
        Dim err As String
        lblTitle.Text = "EDIT"
        pnlList.Visible = False
        pnlAddEdit.Visible = True

        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "Edit"
            Dim lblBranchID, lblNama, lblAlamat, lblJENISKend, lblNamaSupplier, lblAlasanPenolakan, lblDetailPenolakan, lblKeterangan As New Label
            Dim lblID As Integer

            Me.ID = CInt(CType(e.Item.FindControl("lblID"), Label).Text)
            lblBranchID.Text = CType(e.Item.FindControl("lblBranchID"), Label).Text
            BindEdit(Me.ID, lblBranchID.Text)

        ElseIf e.CommandName = "Delete" Then
            Dim oCustomClass As New Parameter.TolakanNonCust
            ' oCustomClass.ID = Me.ID
            If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
                Try
                    oCustomClass.ID = CInt(CType(e.Item.FindControl("lblID"), Label).Text.Trim)
                    oCustomClass.BranchId = Replace(Me.sesBranchId, "'", "")
                    oCustomClass.strConnection = GetConnectionString()
                    err = m_controller.GetTolakanNonCustDelete(oCustomClass)
                    If err <> "" Then
                        ShowMessage(lblMessage, err, True)
                        BindGrid(Me.CmdWhere)
                        Show()
                    Else
                        ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                        BindGrid(Me.CmdWhere)
                        Show()
                    End If
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try
            End If
        ElseIf e.CommandName = "Print" Then
            If SessionInvalid() Then
                Exit Sub
            End If
            Try
                Me.ID = CInt(CType(e.Item.FindControl("lblID"), Label).Text)
                Me.BranchID = CType(e.Item.FindControl("lblBranchID"), Label).Text
                Me.WhereCond = "IDTolakanNonCust = '" & Me.ID & "' and TLKN.BranchID='" + Me.BranchID + "'"

                Dim cookie As HttpCookie = Request.Cookies("TolakanNonCust")
                If Not cookie Is Nothing Then
                    cookie.Values("CmdWhere") = Me.WhereCond
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("TolakanNonCust")
                    cookieNew.Values.Add("CmdWhere", Me.WhereCond)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("TolakanNonCustViewer.aspx")

            Catch ex As Exception

            End Try


        End If
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
     
        'If txtSearch.Text.Trim <> "" Then
        '    Me.SelectItem = cboSearch.SelectedValue
        '    Me.SelectValue = txtSearch.Text
        '    Me.CmdWhere = Me.SelectItem.Trim & "='" & Me.SelectValue & "'"
        'End If
        'BindGrid(Me.CmdWhere)

        If txtSearch.Text.Trim <> "" Then
            Dim tmpSearch As String = txtSearch.Text.Replace("%", "").Trim
            Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + tmpSearch + "%'"
            BindGrid(Me.CmdWhere)
        ElseIf txtSearch.Text.Trim <> "" And Me.CmdWhere = "" Then
            Dim tmpSearch As String = txtSearch.Text.Replace("%", "").Trim
            Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + tmpSearch + "%'"
            BindGrid(Me.CmdWhere)
        End If
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.WhereCond = "IDTolakanNonCust = '" & Me.ID & "' and BranchID='" + Me.BranchID + "'"
        Me.CmdWhere = "ALL"
        BindGrid(Me.CmdWhere)
    End Sub

    Protected Sub btnCancelPalingBawah_Click(sender As Object, e As EventArgs) Handles btnCancelPalingBawah.Click
        Show()
    End Sub

   
End Class