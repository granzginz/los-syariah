﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

Public Class KoreksiAset
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucAO1 As ucAO
    Protected WithEvents UCAddress As UcCompanyAddress
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucViewApplication1 As ucViewApplication
#End Region

#Region "Constanta"
    Private m_controller As New AssetDataController
    Private oAssetMasterPriceController As New AssetMasterPriceController
    Protected WithEvents ucViewApplication As ucViewApplication
    Protected WithEvents ucViewCustomerDetail As ucViewCustomerDetail
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property AssetCode() As String
        Get
            Return ViewState("AssetCode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AssetCode") = Value
        End Set
    End Property
    Property AOID() As String
        Get
            Return ViewState("AOID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AOID") = Value
        End Set
    End Property
    Property NU() As String
        Get
            Return ViewState("NU").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NU") = Value
        End Set
    End Property
    Property SupplierKaroseriID() As String
        Get
            Return ViewState("SupplierKaroseriID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierKaroseriID") = Value
        End Set
    End Property

    'bound property, for save
    Property OTRvalue() As String
        Get
            Return ViewState("AOID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("OTRvalue") = Value
        End Set
    End Property
    Property Asset() As String
        Get
            Return ViewState("Asset").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Asset") = Value
        End Set
    End Property
    Property Serial1() As String
        Get
            Return ViewState("Serial1").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Serial1") = Value
        End Set
    End Property
    Property Serial2() As String
        Get
            Return ViewState("Serial2").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Serial2") = Value
        End Set
    End Property
    Property YearValue() As String
        Get
            Return ViewState("YearValue").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("YearValue") = Value
        End Set
    End Property
    Property SplitBayar() As String
        Get
            Return ViewState("SplitBayar").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SplitBayar") = Value
        End Set
    End Property
    Property HargaKaroseri() As String
        Get
            Return ViewState("HargaKaroseri").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("HargaKaroseri") = Value
        End Set
    End Property
    Property PHJMB() As String
        Get
            Return ViewState("PHJMB").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PHJMB") = Value
        End Set
    End Property
    Property insuredBy() As String
        Get
            Return ViewState("insuredBy").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("insuredBy") = Value
        End Set
    End Property
    Property paidBy() As String
        Get
            Return ViewState("paidBy").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("paidBy") = Value
        End Set
    End Property
#End Region

    Dim status As Boolean = True
    Dim intLoop As Integer
    Dim objrow As DataRow

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If SessionInvalid() Then
                Exit Sub
            End If

            If Not Page.IsPostBack Then
                Me.ApplicationID = Request("appid")
                Me.CustomerID = Request("id")
                InitObjects()

                'load ucviewapp
                ucViewCustomerDetail1 = CType(Page.LoadControl("../../../../webform.UserController/ucViewCustomerDetail.ascx"), ucViewCustomerDetail)
                ucViewApplication1 = CType(Page.LoadControl("../../../../webform.UserController/ucViewApplication.ascx"), ucViewApplication)

                ucViewApplication.ApplicationID = Me.ApplicationID
                ucViewApplication.CustomerID = Me.CustomerID
                ucViewApplication.bindData()
                ucViewApplication.initControls("OnNewApplication")
                ucViewCustomerDetail.CustomerID = Me.CustomerID
                ucViewCustomerDetail.bindCustomerDetail()

                With ucViewApplication1
                    .ApplicationID = Me.ApplicationID
                    .bindData()


                    'If .IsAppTimeLimitReached Then
                    '    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    '    btnSave.Visible = False
                    'End If


                    '.initControls("OnNewApplication")

                    Me.CustomerID = .CustomerID
                    'Me.CustName = .CustomerName
                    'Me.Asset = .AssetTypeID
                    'Me.ProductID = .ProductID
                    'Me.ProductOfferingID = .ProductOfferingID
                    Me.SupplierID = .SupplierID
                End With

                ucViewCustomerDetail1.CustomerID = Me.CustomerID
                ucViewCustomerDetail1.bindCustomerDetail()
                'ucSupplier1.ApplicationId = Me.ApplicationID
                'ucLookupAsset1.ApplicationId = Me.ApplicationID

                'FillCbo(cboInsuredBy, "tblInsuredBy")
                'FillCbo(cboPaidBy, "tblPaidBy")
                'Option "at Cost" (AC)  sudah tidak dipakai
                FillCbo(cboUsage, "tblAssetUsage")

                Dim WhereSup As String = "SupplierID = '" & Me.SupplierID & "'"

                FillCboEmp("SupplierEmployee", cboSalesman, WhereSup)
                FillCboEmp("SupplierEmployee", cboSalesSpv, WhereSup)
                FillCboEmp("SupplierEmployee", cboSupplierAdm, WhereSup)


                GetDefaultAssetData()
                'BindAssetEdit()
                'BindAttributeEdit()
                BindAssetRegistration()



                'InitialLabelValid()
                'GetSerial()
                'GetFee()


            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Private Sub InitObjects()
        lblMessage.Text = ""
        lblMessage.Visible = False
        'RangeValidator3.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
        'ucSupplier1.Visible = True
        lblErrAccOff.Text = ""
        'txtSupplier.Enabled = False
        'txtSupplierKaroseri.Enabled = False
        'txtAsset.Enabled = False
        txtAO.Enabled = False
        'lblPF.Visible = False
        'lblSupplierRegular.Visible = False
        'rboKaroseri.SelectedValue = "False"
    End Sub

#Region "FillCbo"
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = m_controller.GetCboEmp(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
#End Region

    Sub GetDefaultAssetData()

        Dim controllerassetData As New EditApplicationController
        Dim EntitiesAssetData As New Parameter.Application
        Dim oDataAsset As New DataTable

        With EntitiesAssetData
            .ApplicationID = Me.ApplicationID
            .strConnection = GetConnectionString()
            .SpName = "EditAssetDataGetInfoAssetData"
        End With

        EntitiesAssetData = controllerassetData.EditAssetDataGetInfoAssetData(EntitiesAssetData)

        If Not EntitiesAssetData Is Nothing And EntitiesAssetData.ListData.Rows.Count > 0 Then
            oDataAsset = EntitiesAssetData.ListData

            Me.SupplierID = oDataAsset.Rows(0).Item("SupplierID").ToString
            'txtSupplier.Text = oDataAsset.Rows(0).Item("SupplierName").ToString
            Me.AssetCode = oDataAsset.Rows(0).Item(1).ToString
            'txtAsset.Text = oDataAsset.Rows(0).Item(2).ToString
            'pnlLookupKaroseri.Visible = CBool(oDataAsset.Rows(0).Item("Karoseri").ToString)
            Me.SupplierKaroseriID = oDataAsset.Rows(0).Item("SupplierIDKaroseri").ToString
            'txtSupplierKaroseri.Text = oDataAsset.Rows(0).Item("SupplierNameKaroseri").ToString
            'txtYear.Text = oDataAsset.Rows(0).Item(9).ToString
            'txtOTR.Text = oDataAsset.Rows(0).Item(3).ToString
            'chkSplitBayar.Checked = CBool(oDataAsset.Rows(0).Item("SplitPembayaran").ToString)
            'txtHargaKaroseri.Text = oDataAsset.Rows(0).Item("HargaKaroseri").ToString
            'lblTotalHargaOTR.Text = FormatNumber(CDbl(txtOTR.Text) + CDbl(txtHargaKaroseri.Text), 0)
            'txtDP.Text = oDataAsset.Rows(0).Item(4).ToString
            'rboKaroseri.Items.FindByValue(oDataAsset.Rows(0).Item("isKaroseri").ToString).Selected = True
            'resetPanelKaroseri()

            'the unedited property for save, as replace for the controls
            Me.OTRvalue = oDataAsset.Rows(0)(3).ToString
            Me.Asset = oDataAsset.Rows(0)(2).ToString
            Me.Serial1 = oDataAsset.Rows(0)(5).ToString
            Me.Serial2 = oDataAsset.Rows(0)(6).ToString
            Me.YearValue = oDataAsset.Rows(0)(9).ToString
            Me.SplitBayar = oDataAsset.Rows(0).Item("SplitPembayaran").ToString
            Me.HargaKaroseri = oDataAsset.Rows(0).Item("HargaKaroseri").ToString
            Me.insuredBy = oDataAsset.Rows(0).Item("InsAssetInsuredBy").ToString
            Me.paidBy = oDataAsset.Rows(0).Item("InsAssetPaidBy").ToString

            cboPencairanKe.SelectedIndex = cboPencairanKe.Items.IndexOf(cboPencairanKe.Items.FindByValue(oDataAsset.Rows(0).Item("PencairanKe").ToString))
            'cboStatusAsset.SelectedIndex = cboStatusAsset.Items.IndexOf(cboStatusAsset.Items.FindByValue(oDataAsset.Rows(0).Item("StatusKendaraan").ToString))                

            'txtSerial1.Text = oDataAsset.Rows(0).Item(5).ToString
            'txtSerial2.Text = oDataAsset.Rows(0).Item(6).ToString
            rboNamaBPKBSamaKontrak.SelectedValue = oDataAsset.Rows(0).Item("NamaBPKBSama").ToString
            'CalculateTotalPembiayaan()
            Me.NU = oDataAsset.Rows(0).Item(7).ToString

            cboUsage.SelectedIndex = cboUsage.Items.IndexOf(cboUsage.Items.FindByValue(oDataAsset.Rows(0).Item(8).ToString))

            If oDataAsset.Rows(0).Item("TaxDate").ToString <> "" Then
                txtTanggalSTNK.Text = Format(oDataAsset.Rows(0).Item("TaxDate"), "dd/MM/yyyy")
            Else
                txtTanggalSTNK.Text = ""
            End If

            txtAssetNotes.Text = oDataAsset.Rows(0).Item("Notes").ToString

            'cboInsuredBy.SelectedIndex = cboInsuredBy.Items.IndexOf(cboInsuredBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetInsuredBy").ToString))
            'cboPaidBy.SelectedIndex = cboPaidBy.Items.IndexOf(cboPaidBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetPaidBy").ToString))

            Me.AOID = oDataAsset.Rows(0).Item("AOID").ToString
            txtAO.Text = oDataAsset.Rows(0).Item("AOName").ToString

            cboSalesman.SelectedIndex = cboSalesman.Items.IndexOf(cboSalesman.Items.FindByValue(oDataAsset.Rows(0).Item("SalesmanID").ToString))
            cboSalesSpv.SelectedIndex = cboSalesSpv.Items.IndexOf(cboSalesSpv.Items.FindByValue(oDataAsset.Rows(0).Item("SalesSupervisorID").ToString))
            cboSupplierAdm.SelectedIndex = cboSupplierAdm.Items.IndexOf(cboSupplierAdm.Items.FindByValue(oDataAsset.Rows(0).Item("SupplierAdminID").ToString))

            'PHJMB set not on asset onchanged, but after bind
            getPHJMB()
        End If


    End Sub

    Sub BindAssetRegistration()
        Dim entitiesAssetData As New Parameter.AssetData
        Dim oAssetDataController As New AssetDataController
        Dim oDataRegistration As New DataTable

        With entitiesAssetData
            .AppID = Me.ApplicationID
            .strConnection = GetConnectionString()
            .SpName = "spEditAssetDataAssetRegistration"
        End With

        entitiesAssetData = oAssetDataController.EditGetAssetRegistration(entitiesAssetData)

        If Not entitiesAssetData Is Nothing And entitiesAssetData.ListData.Rows.Count > 0 Then
            oDataRegistration = entitiesAssetData.ListData
            txtName.Text = oDataRegistration.Rows(0).Item(1).ToString.Trim
            txtAssetNotes.Text = oDataRegistration.Rows(0).Item(10).ToString.Trim
            UCAddress.Address = oDataRegistration.Rows(0).Item(2).ToString.Trim
            UCAddress.RT = oDataRegistration.Rows(0).Item(6).ToString.Trim
            UCAddress.RW = oDataRegistration.Rows(0).Item(7).ToString.Trim
            UCAddress.Kelurahan = oDataRegistration.Rows(0).Item(3).ToString.Trim
            UCAddress.Kecamatan = oDataRegistration.Rows(0).Item(4).ToString.Trim
            UCAddress.City = oDataRegistration.Rows(0).Item(5).ToString.Trim
            UCAddress.ZipCode = oDataRegistration.Rows(0).Item(8).ToString.Trim
            UCAddress.BindAddress()
            UCAddress.BPKBView()
            UCAddress.ValidatorFalse()
        End If
    End Sub

    Public Sub AOSelected(ByVal strSelectedID1 As String, ByVal strSelectedName1 As String)
        Me.AOID = strSelectedID1
        txtAO.Text = strSelectedName1
    End Sub

  
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("KoreksiNonFinansial.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&id=" & Me.CustomerID.ToString.Trim & "&Status=Koreksi")
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblMessage.Visible = False
        Try
            Me.status = True
            hdnApplicationID.Value = Me.ApplicationID
            hdnCustomerID.Value = Me.CustomerID

            Dim i As Integer
            Dim strName As String
            Dim strAttribute As String

            'If Me.NU = "U" Then
            '    For i = 0 To (dtgAttribute.Items.Count - 1)
            '        strName = CType(dtgAttribute.Items(i).FindControl("lblName"), Label).Text
            '        strAttribute = CType(dtgAttribute.Items(i).FindControl("txtAttribute"), TextBox).Text.Trim
            '        If strName = "License Plate" Then
            '            If strAttribute = "" Then
            '                ShowMessage(lblMessage, "Harap isi No. Polisi", True)
            '                Exit Sub
            '            End If
            '        End If
            '    Next
            'End If

            If (txtAO.Text = "") Then
                ShowMessage(lblMessage, "Harap isi Setting Supplier untuk CMO ini terlebih dahulu!", True)
                Exit Sub
            End If

            Dim oAssetData As New Parameter.AssetData
            Dim oAddress As New Parameter.Address
            Dim oData1 As New DataTable
            Dim oData2 As New DataTable
            Dim oData3 As New DataTable

            oData1.Columns.Add("AssetLevel", GetType(Integer))
            oData1.Columns.Add("AssetCode", GetType(String))

            oData2.Columns.Add("AttributeID", GetType(String))
            oData2.Columns.Add("AttributeContent", GetType(String))

            oData3.Columns.Add("AssetDocID", GetType(String))
            oData3.Columns.Add("DocumentNo", GetType(String))
            oData3.Columns.Add("IsMainDoc", GetType(String))
            oData3.Columns.Add("IsDocExist", GetType(String))
            oData3.Columns.Add("Notes", GetType(String))

            'Validator(oData2, oData3)

            'If status = False Then
            '    If cboInsuredBy.SelectedValue = "CU" Then
            '        cboPaidBy.ClearSelection()
            '        cboPaidBy.Items.FindByValue("CU").Selected = True
            '        cboPaidBy.Enabled = False
            '    End If
            '    Exit Sub
            'End If

            Dim AssetCode As String = Me.AssetCode
            Dim lArrValue As Array = CType(AssetCode.Split(CChar(".")), Array)
            Dim DataCount As Integer = UBound(lArrValue)
            Dim Asset As String = ""

            For Me.intLoop = 0 To DataCount
                objrow = oData1.NewRow
                objrow("AssetLevel") = intLoop + 1
                If intLoop = 0 Then
                    Asset = lArrValue.GetValue(intLoop).ToString
                    objrow("AssetCode") = Asset
                Else
                    Asset = Asset + "." + lArrValue.GetValue(intLoop).ToString
                    objrow("AssetCode") = Asset
                End If
                oData1.Rows.Add(objrow)
            Next

            With oAssetData
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .AppID = Me.ApplicationID
                .SupplierID = Me.SupplierID
                .OTR = CDec(Me.OTRvalue)
                '.DP = CDec(IIf(txtDP.Text.Trim = "", "0", txtDP.Text))
                .DP = 0
                .AssetID = Me.Asset
                .AssetCode = Me.AssetCode
                .Serial1 = Me.Serial1
                .Serial2 = Me.Serial2
                .UsedNew = Me.NU
                .AssetUsage = cboUsage.SelectedValue
                .ManufacturingYear = CInt(Me.YearValue)
                .OldOwnerAsset = txtName.Text
                .TaxDate = IIf(txtTanggalSTNK.Text <> "", ConvertDate(txtTanggalSTNK.Text), "").ToString
                .Notes = txtAssetNotes.Text
                .InsuredBy = Me.insuredBy  ' cboInsuredBy.SelectedValue
                .PaidBy = Me.paidBy ' IIf(cboInsuredBy.SelectedValue = "CU", "CU", cboPaidBy.SelectedValue).ToString
                .SalesmanID = cboSalesman.SelectedValue
                .SalesSupervisorID = cboSalesSpv.SelectedValue
                .SupplierAdminID = cboSupplierAdm.SelectedValue
                .AOID = Me.AOID
                .DateEntryAssetData = Me.BusinessDate
                .Pemakai = ""
                .Lokasi = ""
                .HargaLaku = 0
                .SR1 = ""
                .SR2 = ""
                .HargaSR1 = 0
                .HargaSR2 = 0
                .SplitPembayaran = CBool(Me.SplitBayar) ' chkSplitBayar.Checked
                .UangMukaBayarDi = cboUangMukaBayar.SelectedValue
                .PencairanKe = cboPencairanKe.SelectedValue
                .SupplierIDKaroseri = Me.SupplierKaroseriID
                .HargaKaroseri = CDbl(IIf(IsNumeric(Me.HargaKaroseri), Me.HargaKaroseri, 0)) '  CDbl(IIf(IsNumeric(txtHargaKaroseri.Text), txtHargaKaroseri.Text, 0))
                .PHJMB = CDbl(IIf(IsNumeric(Me.PHJMB), Me.PHJMB, 0)) '  CDbl(IIf(IsNumeric(lblPHJMB.Text), lblPHJMB.Text, 0))
                '.StatusKendaraan = cboStatusAsset.SelectedValue
                .StatusKendaraan = "T" 'tersedia
                .NamaBPKBSama = CBool(rboNamaBPKBSamaKontrak.SelectedValue)

                .Flag = "Add"
                .strConnection = GetConnectionString()
            End With

            oAddress.Address = UCAddress.Address
            oAddress.RT = UCAddress.RT
            oAddress.RW = UCAddress.RW
            oAddress.Kelurahan = UCAddress.Kelurahan
            oAddress.Kecamatan = UCAddress.Kecamatan
            oAddress.City = UCAddress.City
            oAddress.ZipCode = UCAddress.ZipCode

            Dim oReturn As New Parameter.AssetData

            'koreksi data
            oAssetData.isKoreksi = True
            oAssetData.alasanKoreksi = txtAlasanKoreksi.Text
            oAssetData.ApplicationId = Me.ApplicationID
            oReturn = m_controller.AssetDataSaveAdd(oAssetData, oAddress, oData1, oData2, oData3)

            Me.ApplicationID = hdnApplicationID.Value
            Me.CustomerID = hdnCustomerID.Value
            If oReturn.Output <> "" Then
                ShowMessage(lblMessage, oReturn.Output, True)
            Else
                'With ucViewApplication1
                '    .CustomerID = Me.CustomerID
                '    .ApplicationID = Me.ApplicationID
                '    .bindData()
                '    .initControls("OnNewApplication")
                'End With

                'ucApplicationTab1.ApplicationID = Me.ApplicationID
                'ucApplicationTab1.setLink()
                ShowMessage(lblMessage, "Data saved!", False)
            End If            
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub getPHJMB()
        If Me.Asset <> "" Then ' If Me.Asset <> "" And Me.NU = "U" Then

            Dim oCustomClass As New Parameter.AssetMasterPrice

            With oCustomClass
                .strConnection = GetConnectionString()
                .ManufacturingYear = Me.YearValue
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .AssetCode = Me.AssetCode
            End With

            Try
                oCustomClass = oAssetMasterPriceController.GetAssetMasterPriceList(oCustomClass)
                Me.PHJMB = FormatNumber(oCustomClass.Price.ToString, 2)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)

            End Try
        End If
    End Sub
End Class