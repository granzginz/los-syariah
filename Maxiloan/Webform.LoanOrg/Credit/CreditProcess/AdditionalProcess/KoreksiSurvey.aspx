﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KoreksiSurvey.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.KoreksiSurvey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Koreksi Hasil Survey</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="sm" runat="server" />
    <asp:Label ID="lblMessage" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                KOREKSI HASIL SURVEY
            </h3>
        </div>
    </div>    
    <div class="form_box">           
        <div class="form_left">
            <uc3:ucviewapplication id="ucViewApplication" runat="server" />
        </div>
        <div class="form_right">
            <uc2:ucviewcustomerdetail id="ucViewCustomerDetail" runat="server" />
        </div>
    </div>
    <div class="form_box_title_nobg">
        <div class="form_single">
            <label class="label_general">
                Keterangan Koreksi
            </label>
            <asp:TextBox ID="txtAlasanKoreksi" runat="server" TextMode="MultiLine" Rows="3" Columns="40" />
            <asp:RequiredFieldValidator ID="rfvAlasanKoreksi" runat="server" ControlToValidate="txtAlasanKoreksi"
                Display="Dynamic" ErrorMessage="Alasan harus diisi" CssClass="validator_general" />
        </div>
    </div>    
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tanggal Survey
                </label>
                <asp:TextBox runat="server" ID="txtTanggalSurvey" AutoPostBack="True"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="ceTanggalSurver" TargetControlID="txtTanggalSurvey"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
            <div class="form_right">
                <label>
                    Saldo Rata-rata
                </label>
                <asp:TextBox runat="server" ID="txtSaldoRataRata"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Surveyor
                </label>
                <asp:DropDownList runat="server" ID="cboSurveyor">
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Saldo Awal
                </label>
                <asp:TextBox runat="server" ID="txtSaldoAwal"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Lama Survey
                </label>
                <uc:ucnumberformat id="txtLamaSurvey" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Saldo Akhir
                </label>
                <asp:TextBox runat="server" ID="txtSaldoAkhir"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Pembiayaan Analyst
                </label>
                <asp:DropDownList runat="server" ID="cboCreditAnalyst">
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Pemasukan
                </label>
                <asp:TextBox runat="server" ID="txtJumlahPemasukan"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                </label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Pengeluaran
                </label>
                <asp:TextBox runat="server" ID="txtJumlahPengeluaran"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    Jumlah Hari Transaksi
                </label>
                <asp:TextBox runat="server" ID="txtJumlahHariTransaksi"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
        </div>
        <div class="form_right">
            <label>
                Jenis Rekening
            </label>
            <asp:DropDownList ID="cboBankAccType" runat="server">
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DATA LAINNYA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jumlah Asset Yang Dimiliki?</label>
            <uc:ucnumberformat id="txtJumlahKendaraan" runat="server" />
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_general">
                Ada Garasi?</label>
            <asp:RadioButtonList ID="rboGarasi" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
                <asp:ListItem Value="True">Ya</asp:ListItem>
                <asp:ListItem Value="False" Selected="True">Tidak</asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Pertama Kali Pembiayaan</label>
            <asp:DropDownList ID="cboPertamaKredit" runat="server">
                <asp:ListItem Value="RO">Repeat Order</asp:ListItem>
                <asp:ListItem Selected="True" Value="YA">Ya</asp:ListItem>
                <asp:ListItem Value="TA">Tidak, Ada Bukti</asp:ListItem>
                <asp:ListItem Value="TT">Tidak, Tidak Ada Bukti</asp:ListItem>
                <asp:ListItem Value="AO">Additional Order</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Order Ke</label>
            <uc:ucnumberformat id="txtOrderKe" runat="server" />
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                HASIL ANALISA PEMBIAYAAN ANALYST</h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:TextBox runat="server" ID="txtSurveyorNotes" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="small button blue" />
        <asp:Button runat="server" ID="btnCancel" Text="Cancel" CausesValidation="false"  CssClass="small button gray" />
    </div>
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    <input id="hdnCustomerID" type="hidden" name="hdnCustomerID" runat="server" />
    </form>
</body>
</html>
