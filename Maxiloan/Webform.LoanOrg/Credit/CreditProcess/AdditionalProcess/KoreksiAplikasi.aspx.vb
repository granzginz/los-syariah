﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.IO

Public Class KoreksiAplikasi
    Inherits Maxiloan.Webform.WebBased
    Private oController As New ApplicationController
    Private m_controller As New ProductController

#Region "Controls"
    Protected WithEvents ucAdminFee As ucNumberFormat
    Protected WithEvents ucFiduciaFee As ucNumberFormat
    Protected WithEvents ucOtherFee As ucNumberFormat

    Protected WithEvents ucMailingAddress As UcCompanyAddress
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail

#End Region

#Region "Constanta"
    Dim oRow As DataRow
    Dim Status As Boolean
#End Region

#Region "Properties"
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property ProductID() As String
        Get
            Return ViewState("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property

    Property ProductOffID() As String
        Get
            Return ViewState("ProductOffID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOffID") = Value
        End Set
    End Property

    Property KodeDATI() As String
        Get
            Return ViewState("KodeDATI").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("KodeDATI") = Value
        End Set
    End Property

    Property Type() As String
        Get
            Return ViewState("Type").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Property Desc() As String
        Get
            Return ViewState("Desc").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Desc") = Value
        End Set
    End Property

    Property TotalCountApplicationID() As Integer
        Get
            Return CType(ViewState("TotalCountApplicationID"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("TotalCountApplicationID") = Value
        End Set
    End Property

    Property PageMode As String
        Get
            Return CType(ViewState("PageMode"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PageMode") = value
        End Set
    End Property
    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            ucMailingAddress.IsRequiredZipcode = False
            Me.CustomerID = Request("id")
            Me.ApplicationID = Request("appid")
            Me.ProspectAppID = "-"
            Dim oCustomclass As New Parameter.Application
            With oCustomclass
                .strConnection = GetConnectionString()
                .CustomerId = Me.CustomerID
            End With
            Me.TotalCountApplicationID = oController.GetCountAppliationId(oCustomclass)
            initObjects()
            Me.PageMode = Request("page")

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .CustomerID = Me.CustomerID
                .bindData()
                .initControls("OnNewApplication")
                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If
            End With
            hdnApplicationID.Value = Me.ApplicationID
            hdnCustomerID.Value = Me.CustomerID
            BindEdit()
            If ucViewApplication1.isProceed = True Then
                btnSave.Visible = False
            End If
        End If
    End Sub

#Region "Init Objects"
    Protected Sub initObjects()
        ucViewApplication1.ApplicationID = Me.ApplicationID
        ucViewApplication1.CustomerID = Me.CustomerID
        ucViewApplication1.bindData()
        ucViewApplication1.initControls("OnNewApplication")
        ucViewCustomerDetail1.CustomerID = Me.CustomerID
        ucViewCustomerDetail1.bindCustomerDetail()
        Me.Type = ucViewCustomerDetail1.CustomerType
        fillAddress(Me.CustomerID)
        ucMailingAddress.ValidatorTrue()
        ucMailingAddress.showMandatoryAll()
        LoadingKegiatanUsaha()
    End Sub
#End Region

#Region "FillAddress"
    Private Sub fillAddress(ByVal id As String)
        Dim oData As New DataTable
        Dim oApplication As New Parameter.Application
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.CustomerId = id
        oApplication.Type = Me.Type
        oApplication = oController.GetAddress(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        cboCopyAddress.DataSource = oData.DefaultView
        cboCopyAddress.DataTextField = "Combo"
        cboCopyAddress.DataValueField = "Type"
        cboCopyAddress.DataBind()

        If Me.Type = "C" Then
            If oData.Rows.Count > 0 Then
                If Me.PageMode <> "Edit" Then
                    oRow = oData.Rows(0)
                    ucMailingAddress.Address = oRow.Item(0).ToString.Trim
                    ucMailingAddress.RT = oRow.Item(1).ToString.Trim
                    ucMailingAddress.RW = oRow.Item(2).ToString.Trim
                    ucMailingAddress.Kelurahan = oRow.Item(3).ToString.Trim
                    ucMailingAddress.Kecamatan = oRow.Item(4).ToString.Trim
                    ucMailingAddress.City = oRow.Item(5).ToString.Trim
                    ucMailingAddress.ZipCode = oRow.Item(6).ToString.Trim
                    ucMailingAddress.AreaPhone1 = oRow.Item(7).ToString.Trim
                    ucMailingAddress.Phone1 = oRow.Item(8).ToString.Trim
                    ucMailingAddress.AreaPhone2 = oRow.Item(9).ToString.Trim
                    ucMailingAddress.Phone2 = oRow.Item(10).ToString.Trim
                    ucMailingAddress.AreaFax = oRow.Item(11).ToString.Trim
                    ucMailingAddress.Fax = oRow.Item(12).ToString.Trim
                    ucMailingAddress.BindAddress()
                End If
            End If
        Else
            If Me.PageMode <> "Edit" Then
                Dim index As Integer
                For intLoop = 0 To oData.Rows.Count - 1
                    If oData.Rows(intLoop).Item("Type").ToString.Trim = "Residence" Then
                        index = intLoop
                    End If
                Next

                cboCopyAddress.SelectedIndex = cboCopyAddress.Items.IndexOf(cboCopyAddress.Items.FindByValue("Residence"))

                oRow = oData.Rows(index)
                ucMailingAddress.Address = oRow.Item(0).ToString.Trim
                ucMailingAddress.RT = oRow.Item(1).ToString.Trim
                ucMailingAddress.RW = oRow.Item(2).ToString.Trim
                ucMailingAddress.Kelurahan = oRow.Item(3).ToString.Trim
                ucMailingAddress.Kecamatan = oRow.Item(4).ToString.Trim
                ucMailingAddress.City = oRow.Item(5).ToString.Trim
                ucMailingAddress.ZipCode = oRow.Item(6).ToString.Trim
                ucMailingAddress.AreaPhone1 = oRow.Item(7).ToString.Trim
                ucMailingAddress.Phone1 = oRow.Item(8).ToString.Trim
                ucMailingAddress.AreaPhone2 = oRow.Item(9).ToString.Trim
                ucMailingAddress.Phone2 = oRow.Item(10).ToString.Trim
                ucMailingAddress.AreaFax = oRow.Item(11).ToString.Trim
                ucMailingAddress.Fax = oRow.Item(12).ToString.Trim
                ucMailingAddress.BindAddress()
            End If
        End If

        If Not ClientScript.IsStartupScriptRegistered("copyAddress") Then ClientScript.RegisterStartupScript(Me.GetType, "copyAddress", GenerateScript(oData))
    End Sub
#End Region

#Region "BindEdit"
    Sub BindEdit()
         Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.AppID = Me.ApplicationID
        oApplication.ApplicationID = Me.ApplicationID
        oApplication = oController.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)

            cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(oRow("KegiatanUsaha").ToString))
            cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(oRow("JenisPembiayaan").ToString))
            cboInterestType.SelectedIndex = cboInterestType.Items.IndexOf(cboInterestType.Items.FindByValue(oRow("InterestType").ToString))
            cboInstScheme.SelectedIndex = cboInstScheme.Items.IndexOf(cboInstScheme.Items.FindByValue(oRow("InstallmentScheme").ToString))

            cboWayPymt.SelectedIndex = cboWayPymt.Items.IndexOf(cboWayPymt.Items.FindByValue(oRow("WayOfPayment").ToString))
            cboSourceApp.SelectedIndex = cboSourceApp.Items.IndexOf(cboSourceApp.Items.FindByValue(oRow("ApplicationSource").ToString))

            ucAdminFee.Text = FormatNumber(oRow("AdminFee"), 0)
            ucFiduciaFee.Text = FormatNumber(oRow("FiduciaFee"), 0)
            chkFiducia.Checked = CBool(oRow("IsFiduciaCovered").ToString.Trim)
            ucOtherFee.Text = FormatNumber(oRow("OtherFee"), 0)

            hdfkabupaten.Value = oRow("kodedati").ToString
            txtKabupaten.Text = oRow("NamaKabKot").ToString
            'chkCOP.Checked = CBool(IIf(oRow("IsCOP").ToString.Trim = "", "0", oRow("IsCOP").ToString.Trim))
            If oRow("IsCOP").ToString = "1" Then
                chkCOP.SelectedValue = "1"
            ElseIf oRow("IsCOP").ToString = "0" Then
                chkCOP.SelectedValue = "0"
            Else
                chkCOP.SelectedValue = "2"
            End If

            chkHakOpsi.Checked = CBool(IIf(oRow("HakOpsi").ToString.Trim = "", "0", oRow("HakOpsi").ToString.Trim))
            cboLiniBisnis.SelectedIndex = cboLiniBisnis.Items.IndexOf(cboLiniBisnis.Items.FindByValue(oRow("LiniBisnis").ToString.Trim))
            cboTipeLoan.SelectedIndex = cboTipeLoan.Items.IndexOf(cboTipeLoan.Items.FindByValue(oRow("TipeLoan").ToString.Trim))
            txtAppNotes.Text = oRow("Notes").ToString.Trim
            lblAdminFeeGross.Text = FormatNumber(CDbl(oRow("adminFee")) + CDbl(oRow("FiduciaFee")) + CDbl(oRow("BiayaPolis")) + CDbl(oRow("OtherFee")), 0)

            With ucMailingAddress
                .Address = oRow("MailingAddress").ToString.Trim
                .RT = oRow("MailingRT").ToString.Trim
                .RW = oRow("MailingRW").ToString.Trim
                .Kelurahan = oRow("MailingKelurahan").ToString.Trim
                .Kecamatan = oRow("MailingKecamatan").ToString.Trim
                .City = oRow("MailingCity").ToString.Trim
                .ZipCode = oRow("MailingZipCode").ToString.Trim
                .AreaPhone1 = oRow("MailingAreaPhone1").ToString.Trim
                .Phone1 = oRow("MailingPhone1").ToString.Trim
                .AreaPhone2 = oRow("MailingAreaPhone2").ToString.Trim
                .Phone2 = oRow("MailingPhone2").ToString.Trim
                .AreaFax = oRow("MailingAreaFax").ToString.Trim
                .Fax = oRow("MailingFax").ToString.Trim
                .BindAddress()
            End With

            txtProductOffering.Text = oRow("Description").ToString.Trim

            Me.ProductID = oRow("ProductID").ToString.Trim
            Me.ProductOffID = oRow("ProductOfferingID").ToString.Trim

        End If
    End Sub

    Private Sub LoadingKegiatanUsaha()
        Me.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataSource = KegiatanUsahaS
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"
        cboKegiatanUsaha.SelectedIndex = 0


        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"
        cboJenisPembiyaan.DataSource = def
        cboJenisPembiyaan.Items.Insert(0, "Select One")
        cboJenisPembiyaan.Items(0).Value = "SelectOne"
        cboJenisPembiyaan.DataBind()

    End Sub
#End Region

#Region "BindTC"
    Protected Sub TC(ByVal AddEdit As String)
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        If AddEdit = "Add" Then
            oApplication.Type = Me.Type
            oApplication.ProductID = Me.ProductID
        Else
            oApplication.AppID = Me.ApplicationID
        End If

        With oApplication
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
            .AddEdit = AddEdit
            .PersonalCustomerType = ucViewCustomerDetail1.PersonalCustomerType
        End With

        oApplication = oController.GetTC(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC.DataSource = oData.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()
        'dtgTC.Width = CType(IIf(hdnGridGeneralSize.Value = "", "100%", hdnGridGeneralSize.Value), Unit)
        If AddEdit = "Edit" Then
            Dim intLoop As Integer
            For intLoop = 0 To dtgTC.Items.Count - 1
                If oData.Rows(intLoop).Item(4).ToString.Trim <> "" Then
                    CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), ucDateCE).Text = Format(oData.Rows(intLoop).Item(4), "dd/MM/yyyy")
                End If
            Next
        End If
    End Sub

    Sub TC2(ByVal AddEdit As String)
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        If AddEdit = "Add" Then
            oApplication.Type = Me.Type
            oApplication.ProductID = Me.ProductID
        Else
            oApplication.AppID = Me.ApplicationID
        End If

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.AddEdit = AddEdit
        oApplication = oController.GetTC2(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC2.DataSource = oData.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()

        If AddEdit = "Edit" Then
            Dim intLoop As Integer
            For intLoop = 0 To dtgTC2.Items.Count - 1
                If oData.Rows(intLoop).Item("PromiseDate").ToString.Trim <> "" Then
                    CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), ucDateCE).Text = Format(oData.Rows(intLoop).Item("PromiseDate"), "dd/MM/yyyy")
                End If
            Next
        End If
    End Sub
    Private Function pathFile(ByVal Group As String, ByVal CustomerID As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & CustomerID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chkTCChecked As CheckBox
            Dim hyupload As HyperLink = CType(e.Item.FindControl("hyupload"), HyperLink)
            Dim txtDateCE As TextBox = CType(e.Item.FindControl("txtPromiseDate").FindControl("txtDateCE"), TextBox)
            Dim MasterTCID As TextBox = CType(e.Item.FindControl("MasterTCID"), TextBox)
            Dim imgAtteced As Image = CType(e.Item.FindControl("imgAtteced"), Image)


            If File.Exists(pathFile("Dokumen", Me.CustomerID) + MasterTCID.Text.ToString.Trim + ".jpg") Then
                imgAtteced.Visible = True
            End If

            chkTCChecked = CType(e.Item.FindControl("chkTCChecked"), CheckBox)

            If chkTCChecked.Checked Then
                txtDateCE.Enabled = False
            Else
                txtDateCE.Enabled = True
            End If

            hyupload.Attributes.Add("OnClick", "UploadDokument('" & ResolveClientUrl("~/webform.Utility/jLookup/UploadDokument.aspx?doc=" & MasterTCID.Text.Trim & "&CustomerID=" & Me.CustomerID) & "','Upload Dokument','" & jlookupContent.ClientID & "') ")

            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            chkTCChecked.Attributes.Add("OnClick", _
                "return CheckTC('" & txtDateCE.ClientID & "',this.checked);")
        End If
    End Sub

    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chkTCCheck2 As CheckBox
            Dim txtDateCE As TextBox = CType(e.Item.FindControl("txtPromiseDate2").FindControl("txtDateCE"), TextBox)


            chkTCCheck2 = CType(e.Item.FindControl("chkTCCheck2"), CheckBox)

            If chkTCCheck2.Checked Then
                txtDateCE.Enabled = False
            Else
                txtDateCE.Enabled = True
            End If


            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            chkTCCheck2.Attributes.Add("OnClick", _
            "return CheckTC('" & txtDateCE.ClientID & "',this.checked);")
        End If
    End Sub
#End Region

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String = ""
        Dim strScript1 As String = ""
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboCopyAddress.Items.Count - 1
            DataRow = DtTable.Select(" Type = '" & cboCopyAddress.Items(j).Value.Trim & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("Type")).Trim Then
                        strType = CStr(DataRow(i)("Type")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & DataRow(i)("Address").ToString.Trim & "','" & DataRow(i)("RT").ToString.Trim & "' " & _
                                        ",'" & DataRow(i)("RW").ToString.Trim & "','" & DataRow(i)("Kelurahan").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Kecamatan").ToString.Trim & "','" & DataRow(i)("City").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("ZipCode").ToString.Trim & "','" & DataRow(i)("AreaPhone1").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Phone1").ToString.Trim & "','" & DataRow(i)("AreaPhone2").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Phone2").ToString.Trim & "','" & DataRow(i)("AreaFax").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Fax").ToString.Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next
        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If
        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        strScript &= "</script>"
        Return strScript
    End Function

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click        
        Response.Redirect("KoreksiNonFinansial.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&id=" & Me.CustomerID.ToString.Trim & "&Status=Koreksi")
    End Sub

    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Page.IsValid Then
                lblMessage.Visible = False
                hdnApplicationID.Value = Me.ApplicationID
                hdnCustomerID.Value = Me.CustomerID

                Dim oApplication As New Parameter.Application
                Dim oRefAddress As New Parameter.Address
                Dim oRefPersonal As New Parameter.Personal
                Dim oMailingAddress As New Parameter.Address
                Dim oGuarantorPersonal As New Parameter.Personal
                Dim oGuarantorAddress As New Parameter.Address
                Dim oData1 As New DataTable
                Dim oData2 As New DataTable
                Dim oData3 As New DataTable
                Dim oCustomclass As New Parameter.Application

                With oCustomclass
                    .strConnection = GetConnectionString()
                    .CustomerId = Me.CustomerID
                End With

                If Me.TotalCountApplicationID = oController.GetCountAppliationId(oCustomclass) Then
                    Status = True
                    oData3.Columns.Add("AgreementNo", GetType(String))
                    oData1.Columns.Add("MasterTCID", GetType(String))
                    oData1.Columns.Add("PriorTo", GetType(String))
                    oData1.Columns.Add("IsChecked", GetType(String))
                    oData1.Columns.Add("IsMandatory", GetType(String))
                    oData1.Columns.Add("PromiseDate", GetType(String))
                    oData1.Columns.Add("Notes", GetType(String))

                    oData2.Columns.Add("MasterTCID", GetType(String))
                    oData2.Columns.Add("AGTCCLSequenceNo", GetType(String))
                    oData2.Columns.Add("IsChecked", GetType(String))
                    oData2.Columns.Add("PromiseDate", GetType(String))
                    oData2.Columns.Add("Notes", GetType(String))

                    Validator(oData1, oData2)

                    If Status = False Then
                        cek_Check()
                        Exit Sub
                    End If

                    oApplication.strConnection = GetConnectionString()
                    oApplication.BusinessDate = Me.BusinessDate
                    oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                    oApplication.CustomerId = Me.CustomerID

                    oApplication.ApplicationSource = cboSourceApp.SelectedValue
                    oApplication.Notes = txtAppNotes.Text
                    'oApplication.IsCOP = chkCOP.Checked
                    oApplication.IsCOP = chkCOP.SelectedValue
                    oApplication.LiniBisnis = cboLiniBisnis.SelectedValue
                    oApplication.TipeLoan = cboTipeLoan.SelectedValue
                    oApplication.HakOpsi = chkHakOpsi.Checked
                    oApplication.KodeDATI = hdfkabupaten.Value

                    oMailingAddress.Address = ucMailingAddress.Address
                    oMailingAddress.RT = ucMailingAddress.RT
                    oMailingAddress.RW = ucMailingAddress.RW
                    oMailingAddress.Kecamatan = ucMailingAddress.Kecamatan
                    oMailingAddress.Kelurahan = ucMailingAddress.Kelurahan
                    oMailingAddress.ZipCode = ucMailingAddress.ZipCode
                    oMailingAddress.City = ucMailingAddress.City
                    oMailingAddress.AreaPhone1 = ucMailingAddress.AreaPhone1
                    oMailingAddress.AreaPhone2 = ucMailingAddress.AreaPhone2
                    oMailingAddress.AreaFax = ucMailingAddress.AreaFax
                    oMailingAddress.Phone1 = ucMailingAddress.Phone1
                    oMailingAddress.Phone2 = ucMailingAddress.Phone2
                    oMailingAddress.Fax = ucMailingAddress.Fax

                    Dim ErrorMessage As String = ""
                    Dim oReturn As New Parameter.Application

                    oApplication.ApplicationID = Me.ApplicationID
                    oApplication.isKoreksi = True
                    oApplication.alasanKoreksi = txtAlasanKoreksi.Text
                    oReturn = oController.ApplicationSaveEditNonFinancial(oApplication, _
                                                              oRefPersonal, _
                                                              oRefAddress, _
                                                              oMailingAddress, _
                                                              oData1, _
                                                              oData2, _
                                                              oData3)
                    Me.PageMode = "Edit"
                    Me.ApplicationID = hdnApplicationID.Value
                    Me.CustomerID = hdnCustomerID.Value
                    If oReturn.Err <> "" Then
                        ShowMessage(lblMessage, ErrorMessage, True)
                    Else
                        ShowMessage(lblMessage, "Data saved!", False)
                    End If

                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub Validator(ByRef oData1 As DataTable, ByRef oData2 As DataTable)
        Dim PriorTo As String
        Dim Mandatory As String
        'Dim Checked As Boolean
        Dim PromiseDate As TextBox
        Dim validCheck As New Label
        Dim MasterTCID, MasterTCID2, AGTCCLSequenceNo, Notes As String
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1


        For intLoop = 0 To CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
            If intLoop <= DtgTC1count Then
                MasterTCID = dtgTC.Items(intLoop).Cells(6).Text
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(3).Text.Trim
                'Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), TextBox)
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                Notes = CType(dtgTC.Items(intLoop).Cells(5).FindControl("txtTCNotes"), TextBox).Text

                If PromiseDate.Text <> "" Then
                    If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = True
                    End If
                End If

                If PriorTo = "APK" And Mandatory = "v" Then
                    If PromiseDate.Text = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                    validCheck.Visible = True
                End If

                oRow = oData1.NewRow
                oRow("MasterTCID") = MasterTCID
                oRow("PriorTo") = PriorTo
                oRow("IsChecked") = "0" ' IIf(Checked = True, "1", "0")
                oRow("IsMandatory") = IIf(Mandatory = "v", "1", "0")
                oRow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
                oRow("Notes") = Notes
                oData1.Rows.Add(oRow)
            End If
            If intLoop <= DtgTC2count Then
                Context.Trace.Write("Validator Kondisi 4")
                MasterTCID = dtgTC2.Items(intLoop).Cells(7).Text
                AGTCCLSequenceNo = dtgTC2.Items(intLoop).Cells(8).Text
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(4).Text.Trim
                Notes = CType(dtgTC2.Items(intLoop).Cells(6).FindControl("txtTCNotes2"), TextBox).Text
                'Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), TextBox)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                If PromiseDate.Text <> "" Then
                    Context.Trace.Write("Validator Kondisi 5")
                    If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        Context.Trace.Write("Validator Kondisi 6")
                        CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = True
                    End If
                End If
                If PriorTo = "APK" And Mandatory = "v" Then
                    Context.Trace.Write("Validator Kondisi 7")
                    If PromiseDate.Text = "" Then
                        Context.Trace.Write("Validator Kondisi 8")
                        If Status <> False Then
                            Status = False
                            Context.Trace.Write("Validator Kondisi 8")
                        End If
                        validCheck.Visible = True
                        Context.Trace.Write("Validator Kondisi 9")
                    End If
                End If
                oRow = oData2.NewRow
                oRow("MasterTCID") = MasterTCID
                oRow("AGTCCLSequenceNo") = AGTCCLSequenceNo
                oRow("IsChecked") = "0" ' IIf(Checked = True, "1", "0")
                oRow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
                oRow("Notes") = Notes
                oData2.Rows.Add(oRow)
            End If
        Next

        Dim RainCheck As Boolean = False
        Dim intLoop2, intLoop3, intLoop4, intLoop5 As Integer

        For intLoop = 0 To DtgTC1count
            MasterTCID = dtgTC.Items(intLoop).Cells(6).Text
            'Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
            For intLoop2 = 0 To DtgTC2count
                MasterTCID2 = dtgTC2.Items(intLoop2).Cells(7).Text
                If MasterTCID = MasterTCID2 Then
                    'If Checked = True Then
                    '    CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True

                    '    If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = False Then
                    '        CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                    '        If Status <> False Then
                    '            Status = False
                    '        End If
                    '    Else
                    '        CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                    '    End If
                    'Else
                    '    If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = True Then
                    '        For intLoop4 = 0 To intLoop2
                    '            If dtgTC2.Items(intLoop4).Cells(8).Text = MasterTCID Then
                    '                CType(dtgTC2.Items(intLoop4).FindControl("lblVTC2Checked"), Label).Visible = True
                    '            End If
                    '        Next

                    '        For intLoop5 = intLoop2 To DtgTC2count
                    '            If dtgTC2.Items(intLoop5).Cells(8).Text = MasterTCID Then
                    '                CType(dtgTC2.Items(intLoop5).FindControl("lblVTC2Checked"), Label).Visible = True
                    '            End If
                    '        Next

                    '        CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True

                    '        If Status <> False Then
                    '            Status = False
                    '        End If

                    '        For intLoop3 = 0 To intLoop
                    '            If dtgTC.Items(intLoop3).Cells(7).Text = MasterTCID2 Then
                    '                If CType(dtgTC.Items(intLoop3).FindControl("chkTCChecked"), CheckBox).Checked = False Then
                    '                    CType(dtgTC.Items(intLoop3).FindControl("lblVTCChecked"), Label).Visible = True
                    '                End If
                    '            End If
                    '        Next
                    '    End If
                    'End If
                End If
            Next
            RainCheck = False
        Next
    End Sub

    Protected Sub cek_Check()
        Dim CheckTC As Boolean
        Dim CheckTC2 As Boolean
        Dim counter, counter2 As Integer
        Dim PromiseDate As ValidDate
        Dim PromiseDate2 As ValidDate

        For counter = 0 To dtgTC.Items.Count - 1
            CheckTC = CType(dtgTC.Items(counter).FindControl("chkTCChecked"), CheckBox).Checked
            If CheckTC Then
                PromiseDate = CType(dtgTC.Items(counter).FindControl("uscTCPromiseDate"), ValidDate)
                PromiseDate.dateValue = ""
            End If
        Next

        For counter = 0 To dtgTC2.Items.Count - 1
            CheckTC2 = CType(dtgTC2.Items(counter).FindControl("chkTCCheck2"), CheckBox).Checked
            If CheckTC2 Then
                PromiseDate2 = CType(dtgTC2.Items(counter).FindControl("uscTCPromiseDate2"), ValidDate)
                PromiseDate2.dateValue = ""
            End If
        Next
    End Sub

End Class