﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class KoreksiSurvey

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''sm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sm As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtAlasanKoreksi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAlasanKoreksi As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''rfvAlasanKoreksi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvAlasanKoreksi As Global.System.Web.UI.WebControls.RequiredFieldValidator

    '''<summary>
    '''txtTanggalSurvey control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTanggalSurvey As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ceTanggalSurver control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ceTanggalSurver As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''txtSaldoRataRata control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSaldoRataRata As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cboSurveyor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSurveyor As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSaldoAwal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSaldoAwal As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtSaldoAkhir control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSaldoAkhir As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cboCreditAnalyst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboCreditAnalyst As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtJumlahPemasukan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtJumlahPemasukan As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtJumlahPengeluaran control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtJumlahPengeluaran As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtJumlahHariTransaksi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtJumlahHariTransaksi As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''cboBankAccType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboBankAccType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''rboGarasi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rboGarasi As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''cboPertamaKredit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboPertamaKredit As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtSurveyorNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSurveyorNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hdnApplicationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnApplicationID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdnCustomerID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnCustomerID As Global.System.Web.UI.HtmlControls.HtmlInputHidden
End Class
