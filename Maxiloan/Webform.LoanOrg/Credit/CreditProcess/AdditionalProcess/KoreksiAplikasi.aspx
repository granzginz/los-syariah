﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KoreksiAplikasi.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.KoreksiAplikasi" %>

<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc" %>
<%@ Register Src="../../../../webform.UserController/ucGuarantor.ascx" TagName="ucGuarantor"
    TagPrefix="uc" %>
<%@ Register Src="../../../../webform.UserController/ucReferensi.ascx" TagName="ucReferensi"
    TagPrefix="uc" %>
<%@ Register Src="../../../../Webform.UserController/ucCompanyAddress.ascx" TagName="UcCompanyAdress"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucLookupKabupaten.ascx" TagName="ucLookupKabupaten"
    TagPrefix="uc" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script type = "text/javascript" >
    function SelectFromArray(itemArray) {
        var objAdd = 'document.forms[0].UCMailingAddress_';
        var pAddress = eval(objAdd + 'txtAddress');
        var pRT = eval(objAdd + 'txtRT');
        var pRW = eval(objAdd + 'txtRW');
        var pKelurahan = eval(objAdd + 'oLookUpKodePos_txtKelurahan');
        var pKecamatan = eval(objAdd + 'oLookUpKodePos_txtKecamatan');
        var pCity = eval(objAdd + 'oLookUpKodePos_txtCity');
        var pZipCode = eval(objAdd + 'oLookUpKodePos_txtZipCode');
        var pAreaPhone1 = eval(objAdd + 'txtAreaPhone1');
        var pPhone1 = eval(objAdd + 'txtPhone1');
        var pAreaPhone2 = eval(objAdd + 'txtAreaPhone2');
        var pPhone2 = eval(objAdd + 'txtPhone2');
        var pAreaFax = eval(objAdd + 'txtAreaFax');
        var pFax = eval(objAdd + 'txtFax');

        pAddress.value = itemArray[0][0];
        pRT.value = itemArray[0][1];
        pRW.value = itemArray[0][2];
        pKelurahan.value = itemArray[0][3];
        pKecamatan.value = itemArray[0][4];
        pCity.value = itemArray[0][5];
        pZipCode.value = itemArray[0][6];
        pAreaPhone1.value = itemArray[0][7];
        pPhone1.value = itemArray[0][8];
        pAreaPhone2.value = itemArray[0][9];
        pPhone2.value = itemArray[0][10];
        pAreaFax.value = itemArray[0][11];
        pFax.value = itemArray[0][12];
        return false;
    }
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Koreksi Aplikasi</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>   
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">         
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                KOREKSI APLIKASI
            </h3>
        </div>
    </div>    
    <div class="form_box">           
        <div class="form_left">
            <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
        </div>
        <div class="form_right">
            <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
        </div>
    </div>   
            <div class="form_box_title_nobg">
                <div class="form_single">
                    <label class="label_general">
                        Keterangan Koreksi
                    </label>                    
                    <asp:TextBox ID="txtAlasanKoreksi" runat="server" TextMode="MultiLine" CssClass="multiline_textbox" Width="80%"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAlasanKoreksi" runat="server" ControlToValidate="txtAlasanKoreksi"
                        Display="Dynamic" ErrorMessage="Alasan harus diisi" CssClass="validator_general" />
                </div>
            </div>            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="">Kegiatan Usaha</label>  
                        <asp:DropDownList ID="cboKegiatanUsaha" runat="server" AutoPostBack="false" Enabled="false"  Width="300px"/>
                    </div>
                    <div class="form_right">
                        <label>Lini Bisnis</label>
                        <asp:DropDownList ID="cboLiniBisnis" runat="server"  Width="200px">
                            <asp:ListItem Value="RETAIL">RETAIL</asp:ListItem>
                            <asp:ListItem Value="FLEET">FLEET</asp:ListItem>
                            <asp:ListItem Value="AGRO">AGRO</asp:ListItem>
                            <asp:ListItem Value="NPCD">NPCD</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label >
                            Jenis Pembiayaan
                        </label>
                        <asp:DropDownList runat="server" ID="cboJenisPembiyaan"  Width="300px" AutoPostBack="false" Enabled="false"></asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>Tipe Loan</label>
                        <asp:DropDownList ID="cboTipeLoan" runat="server"  Width="200px">
                            <asp:ListItem Value="R">Regular</asp:ListItem>
                            <asp:ListItem Value="O">Over Pembiayaan</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Produk Jual</label>
                    <asp:TextBox ID="txtProductOffering" runat="server" Enabled="False" CssClass="regular_text" Width="250px"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                         <label>
                            Skema Angsuran</label>
                        <asp:DropDownList ID="cboInstScheme" runat="server" enabled="false">
                            <asp:ListItem Value="RF">Regular Fixed Installment Scheme</asp:ListItem>
                            <asp:ListItem Value="IR">Irregular Installment</asp:ListItem>
                            <asp:ListItem Value="ST">Step Up/Step Down</asp:ListItem>
                            <asp:ListItem Value="BP">Ballon Payment</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>
                                Jenis Margin</label>
                            <asp:DropDownList ID="cboInterestType" runat="server" enabled="false">
                                <asp:ListItem Value="FX">Fixed Rate</asp:ListItem>
                                <asp:ListItem Value="FL">Floating Rate</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                       <label class="label_general">
                                Tipe Step Up Step Down</label>
                            <asp:RadioButtonList ID="rdoSTTYpe" runat="server" RepeatDirection="Horizontal" Enabled="False"
                                CssClass="opt_single">
                                <asp:ListItem Value="NM" Selected="True">Basic</asp:ListItem>
                                <asp:ListItem Value="RL">Normal</asp:ListItem>
                                <asp:ListItem Value="LS">Leasing</asp:ListItem>
                            </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                            <%--<label>
                                COP</label>
                            <asp:CheckBox runat="server" ID="chkCOP" />--%>
                        <label class="label_general"> Fasilitas Karyawan</label>
                            <%--<asp:CheckBox runat="server" ID="chkCOP" />--%>
                        <asp:RadioButtonList ID="chkCOP" runat="server" RepeatDirection="Horizontal" Enabled="true"
                                CssClass="opt_single">
                                <asp:ListItem Value="1" >COP</asp:ListItem>
                                <asp:ListItem Value="0">MOP</asp:ListItem>
                                <asp:ListItem Value="2" Selected="True">None</asp:ListItem> 
                            </asp:RadioButtonList>
                        </div>
                </div>
            </div>
            <div class="form_box">
                    <div>
                        <div class="form_left">
                            
                        </div>
                        <div class="form_right">
                            <label>
                                Hak Opsi</label>
                            <asp:CheckBox runat="server" ID="chkHakOpsi" />
                        </div>
                    </div>
             </div>
             <div class="form_box_title">
                    <div class="form_left">
                        <h4>
                            DATA UMUM</h4>
                    </div>
                    <div class="form_right">
                        <h4>
                            BIAYA - BIAYA</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        
                        <div class="form_left">
                            <label>
                                Cara Pembayaran
                            </label>
                            <asp:DropDownList ID="cboWayPymt" runat="server" Enabled="false">
                                <asp:ListItem Value="TF">Transfer</asp:ListItem>
                                <asp:ListItem Value="PD">PDC</asp:ListItem>
                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Administrasi Nett</label>
                            <uc:ucnumberformat id="ucAdminFee" runat="server" isReadOnly="true" />
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Fiducia Register</label>
                            <asp:CheckBox runat="server" ID="chkFiducia" Enabled="false" CssClass="checkbox_general" />
                        </div>
                        
                        <div class="form_right">
                            <label>
                                Biaya Fiducia
                            </label>
                            <uc:ucnumberformat id="ucFiduciaFee" runat="server" isReadOnly="true" />
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Sumber Aplikasi
                            </label>
                            <asp:DropDownList ID="cboSourceApp" runat="server">
                                <asp:ListItem Value="D">Direct</asp:ListItem>
                                <asp:ListItem Value="I" Selected="True">Indirect</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Lainnya
                            </label>
                            <uc:ucnumberformat id="ucOtherFee" runat="server" isReadOnly="true" />
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <div style="display:none">
                            
                            </div>
                        </div>
                        
                        <div class="form_right">
                            <label>
                                Biaya Administrasi Gross</label>
                            <asp:Label runat="server" ID="lblAdminFeeGross" CssClass="numberAlign regular_text">0</asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            CATATAN APLIKASI</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <asp:TextBox ID="txtAppNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox" Width="80%"></asp:TextBox>
                    </div>
                </div>
               
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            ALAMAT SURAT MENYURAT
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Copy Alamat dari</label>
                        <asp:DropDownList ID="cboCopyAddress" runat="server" onChange="SelectFromArray((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]);">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <uc1:UcCompanyAdress id="UCMailingAddress" runat="server"></uc1:UcCompanyAdress>
            </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kota/Kabupaten DATI II</label>                            
                        <asp:HiddenField runat="server" ID="hdfkabupaten" />                            
                        <asp:TextBox runat="server" ID="txtKabupaten" Enabled="false" CssClass="medium_text"></asp:TextBox>
                        <button class="small buttongo blue" 
                        onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Kabupaten.aspx?kode=" & hdfkabupaten.ClientID & "&nama=" & txtKabupaten.ClientID) %>','Daftar Kabupaten','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                        <asp:RequiredFieldValidator ID="rfvtxtKabupaten" runat="server" ErrorMessage="*" Display="Dynamic" 
                            CssClass="validator_general" ControlToValidate="txtKabupaten"></asp:RequiredFieldValidator>                            
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h5>
                            SYARAT & KONDISI</h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgTC" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                CellPadding="3" CellSpacing="1" DataKeyField="TCName" PageSize="3" CssClass="grid_general"
                                Width="100%">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DOKUMEN">
                                        <HeaderStyle Width="300px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:label ID="TCName" runat="server"><%# DataBinder.eval(Container, "DataItem.TCName") %></asp:label>
                                            <asp:Image runat="server" ID="imgAtteced" ImageUrl="../../../../Images/tick_icon.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyupload" style="cursor:pointer" runat="server">Attached</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                        <HeaderStyle Width="50px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PERIKSA">
                                        <HeaderStyle Width="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTCChecked" runat="server" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'>
                                            </asp:CheckBox>
                                            <asp:Label ID="lblVTCChecked" runat="server"  CssClass="label_req">&nbsp;&nbsp;&nbsp;</asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                                        <HeaderStyle Width="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="TGL. JANJI">
                                        <HeaderStyle Width="100px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <uc8:ucdatece runat="server" id="txtPromiseDate"></uc8:ucdatece>
                                            <asp:Label ID="lblVPromiseDate" runat="server" CssClass="validator_general">Tanggal janji harus > dari business date!</asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                                CssClass="desc_textbox">
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="" HeaderStyle-Width="0px" >
                                        <ItemTemplate>
                                            <asp:TextBox style="display:none" runat="server" ID="MasterTCID" Text='<%# DataBinder.eval(Container, "DataItem.MasterTCID") %>'>
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <%--tidak ditampilkan--%>
                <div class="form_box_hide">
                    <div class="form_single">
                        <h4>
                            SYARAT & KONDISI CHECK LIST</h4>
                    </div>
                </div>
                <div class="form_box_hide">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgTC2" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" DataKeyField="TCName" PageSize="3" CssClass="grid_ws"
                            Width="1500px">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                    <HeaderStyle Width="300px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="NO">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST">
                                    <HeaderStyle Width="100px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="PERIKSA">
                                    <HeaderStyle Width="150px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkTCCheck2" runat="server" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'>
                                        </asp:CheckBox>
                                        <asp:Label ID="lblVTC2Checked" runat="server" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="TANGGAL JANJI">
                                    <HeaderStyle Width="350px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <uc8:ucdatece runat="server" id="txtPromiseDate2"></uc8:ucdatece>
                                        <asp:Label ID="lblVPromiseDate2" runat="server" CssClass="validator_general">Tanggal janji harus > dari business date!</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CATATAN">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                            Width="95%">
                                        </asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                                </asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
            <input id="hdnCustomerID" type="hidden" name="hdnCustomerID" runat="server" />
    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
