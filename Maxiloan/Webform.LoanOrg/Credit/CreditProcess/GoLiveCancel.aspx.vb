﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Exceptions

Public Class GoLiveCancel
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ApplicationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property CmdWhere2() As String
        Get
            Return CType(viewstate("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere2") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property AgreementDate() As String
        Get
            Return CType(viewstate("AgreementDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementDate") = Value
        End Set
    End Property
    Private Property AADate() As String
        Get
            Return CType(viewstate("AADate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AADate") = Value
        End Set
    End Property
    Private Property CustName() As String
        Get
            Return CType(viewstate("CustName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property SurveyDate() As String
        Get
            Return viewstate("SurveyDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SurveyDate") = Value
        End Set
    End Property
    Private Property DeliveryOrderDate() As String
        Get
            Return viewstate("DeliveryOrderDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("DeliveryOrderDate") = Value
        End Set
    End Property
    Private Property FirstInstallment() As String
        Get
            Return viewstate("FirstInstallment").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("FirstInstallment") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        lblMessage.Text = ""
        lblMessage.Visible = False
        Me.FormID = "GOLIVECANCEL"

        If Not Page.IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                txtGoPage.Text = "1"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                Me.Sort = "AgreementNo ASC"
                BindGrid()
                InitialPanel()
            Else
                NotAuthorized()
            End If
        End If
    End Sub

    Sub InitialPanel()
        pnlList.Visible = True
    End Sub

    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/NotAuthorized.html")
    End Sub

#Region "BindGrid"
    Sub BindGrid()
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.Application

        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()

        oCustomClass = m_controller.GetGoLiveCancelPaging(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region

#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenApplicationId('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function

    Function LinkToEmployee(ByVal strBranchID As String, ByVal strAOID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinEmployee('" & strStyle & "','" & strBranchID & "','" & strAOID & "')"
    End Function

    Function LinkToAgreementNo(ByVal strAgreementNo As String, ByVal strStyle As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "','" & strAgreementNo & "')"
    End Function

#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region

#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Try
            If e.CommandName = "GoLive" Then
                If CheckFeature(Me.Loginid, "GoLive", "Add", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                End If

                Me.AgreementDate = e.Item.Cells(9).Text.Trim
                Me.ApplicationID = e.Item.Cells(8).Text.Trim
                Me.SurveyDate = e.Item.Cells(10).Text.Trim
                Me.AADate = e.Item.Cells(11).Text.Trim
                Me.FirstInstallment = e.Item.Cells(12).Text.Trim
                Me.DeliveryOrderDate = e.Item.Cells(7).Text.Trim
                Me.AgreementNo = CType(e.Item.Cells(1).FindControl("lnkAgreementNo"), HyperLink).Text.Trim
                Me.CustName = CType(e.Item.Cells(2).FindControl("lnkName"), HyperLink).Text.Trim
                Me.CustomerID = CType(e.Item.FindControl("lblCustomerID"), Label).Text.Trim
                Response.Redirect("GoLive_002.aspx?ANo=" & Me.AgreementNo & "&Name=" & Me.CustName & "&App=" & Me.ApplicationID & "&AgDate=" & Me.AgreementDate & "&CustomerID=" & Me.CustomerID & "&DeliveryOrderDate=" & Me.DeliveryOrderDate & "&SurveyDate=" & Me.DeliveryOrderDate & "&AADate=" & Me.AADate & "&FirstInstallment=" & Me.FirstInstallment & "")
            End If

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("GoLive.Aspx", "ItemCommand", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
#End Region

#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGrid()
    End Sub
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbSearch.Click
        Dim Search As String = ""
        If txtSearch.Text.Trim <> "" Then            
            Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
            Me.CmdWhere = cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGrid()
    End Sub
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblAgreementNo As New Label
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            Dim lblAplicationId As New Label
            lblAplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            Dim lblSupplierID As New Label
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            Dim lblCustomerID As New Label
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Dim lblAOID As New Label
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)

            Dim lnkAgreementNo As New HyperLink
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            Dim lnkName As New HyperLink
            lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            Dim lnkSupplier As New HyperLink
            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), HyperLink)
            Dim lnkAO As New HyperLink
            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)

            lnkAgreementNo.NavigateUrl = LinkToAgreementNo(lblAplicationId.Text.Trim, "AccAcq")
            lnkName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            lnkSupplier.NavigateUrl = LinkToSupplier(lblSupplierID.Text.Trim, "AccAcq")
            lnkAO.NavigateUrl = LinkToEmployee(Me.sesBranchId.Replace("'", ""), lblAOID.Text.Trim, "AccAcq")
        End If
    End Sub
#End Region

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To dtgPaging.Items.Count - 1
            chkItem = CType(dtgPaging.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imbSave.Click
        Dim lblApplicationID As Label
        Dim chkDtList As CheckBox
        Dim dt As DataTable = createNewDT()

        For i = 0 To dtgPaging.Items.Count - 1
            chkDtList = CType(dtgPaging.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lblApplicationID = CType(dtgPaging.Items(i).FindControl("lblApplicationID"), Label)
                    dt.Rows.Add((i + 1).ToString, lblApplicationID.Text.Trim, String.Empty)
                End If
            End If
        Next

        Dim oGoLive As New Parameter.Application

        oGoLive.BusinessDate = Me.BusinessDate
        oGoLive.ListData = dt
        oGoLive.strConnection = GetConnectionString()

        Try
            oGoLive = m_controller.GoLiveCancel(oGoLive)
            BindGrid()
        Catch ex As Exception
            lblMessage.Text = ex.Message
            lblMessage.Visible = True
        End Try
    End Sub

    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")

        Return dt
    End Function

End Class