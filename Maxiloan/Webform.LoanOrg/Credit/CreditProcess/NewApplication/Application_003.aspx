﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Application_003.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.Application_003" %>

<%@ Register Src="../../../../webform.UserController/UcLookUpProductOffering.ascx"
    TagName="UcLookUpProductOffering" TagPrefix="uc1" %>

   <%-- <%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>--%>
<%@ Register Src="../../../../webform.UserController/ucLookUpCustomer.ascx" TagName="ucLookUpCustomer"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucGuarantor.ascx" TagName="ucGuarantor"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucReferensi.ascx" TagName="ucReferensi"
    TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucLookupKabupaten.ascx" TagName="ucLookupKabupaten"
    TagPrefix="uc6" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTab.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc8" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<%@ Register Src="../../../../webform.UserController/ucRefinancing.ascx" TagName="ucRefinancing"
    TagPrefix="uc9" %>
<%@ Register Src="../../../../webform.UserController/uclookupNPP.ascx" TagName="ucLookupNPP"
    TagPrefix="uc10" %> 
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../../../Webform.UserController/ucAddress.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucProductOffering" Src="../../../../Webform.UserController/ucProductOffering.ascx" %>--%>

<%@ Register TagPrefix="uc1" TagName="ucProdOffering" Src="../../../../Webform.UserController/UcLookUpPdctOffering.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucKabupaten" Src="../../../../Webform.UserController/ucKabupaten.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        function calculateAdminFeeGross() {
            var admin = $('#ucAdminFee_txtNumber').val();
            var fiducia = $('#ucFiduciaFee_txtNumber').val();
            var notary = $('#ucNotaryFee_txtNumber').val();
            var other = $('#ucOtherFee_txtNumber').val();
            var handling = $('#ucHandlingFee_txtNumber').val();

            if (!admin) { admin = "0" };
            if (!fiducia) { admin = "0" };
            if (!other) { admin = "0" };
            if (!handling) { admin = "0" };

            var total = parseInt(admin.replace(/\s*,\s*/g, '')) +
                        parseInt(fiducia.replace(/\s*,\s*/g, '')) +
                        parseInt(notary.replace(/\s*,\s*/g, '')) +
                        parseInt(other.replace(/\s*,\s*/g, '')) +
                        parseInt(handling.replace(/\s*,\s*/g, ''));
            $('#lblAdminFeeGross').html(number_format(total));

        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function SelectFromArray(itemArray) {
            var objAdd = 'document.forms[0].UCMailingAddress_';
            var pAddress = eval(objAdd + 'txtAddress');
            var pRT = eval(objAdd + 'txtRT');
            var pRW = eval(objAdd + 'txtRW');
            var pKelurahan = eval(objAdd + 'oLookUpKodePos_txtKelurahan'); 
            var pKecamatan = eval(objAdd + 'oLookUpKodePos_txtKecamatan');
            var pCity = eval(objAdd + 'oLookUpKodePos_txtCity');
            var pZipCode = eval(objAdd + 'oLookUpKodePos_txtZipCode'); 
            var pAreaPhone1 = eval(objAdd + 'txtAreaPhone1');
            var pPhone1 = eval(objAdd + 'txtPhone1');
            var pAreaPhone2 = eval(objAdd + 'txtAreaPhone2');
            var pPhone2 = eval(objAdd + 'txtPhone2');
            var pAreaFax = eval(objAdd + 'txtAreaFax');
            var pFax = eval(objAdd + 'txtFax');

            pAddress.value = itemArray[0][0];
            pRT.value = itemArray[0][1];
            pRW.value = itemArray[0][2];
            pKelurahan.value = itemArray[0][3];
            pKecamatan.value = itemArray[0][4];
            pCity.value = itemArray[0][5];
            pZipCode.value = itemArray[0][6];
            pAreaPhone1.value = itemArray[0][7];
            pPhone1.value = itemArray[0][8];
            pAreaPhone2.value = itemArray[0][9];
            pPhone2.value = itemArray[0][10];
            pAreaFax.value = itemArray[0][11];
            pFax.value = itemArray[0][12];
            return false;
        }

        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date);
            if (chk == true) {
                text.disabled = true;
                text.value = '';
            }
            else {
                text.disabled = false;
            }

            return true;

        }

        function InterestType() {
            var myID = 'cboInstScheme';
            var obj = eval('document.forms[0].' + myID);
            if (document.forms[0].cboInterestType.value == 'FX') {
                obj.disabled = false;
            }
            else {
                obj.disabled = true;
            }
            document.forms[0].rdoSTTYpe_0.disabled = true;
            document.forms[0].rdoSTTYpe_1.disabled = true;
            document.forms[0].rdoSTTYpe_2.disabled = true;
        }

        function InstScheme() {
            document.forms[0].rdoSTTYpe_0.disabled = true;
            document.forms[0].rdoSTTYpe_1.disabled = true;
            document.forms[0].rdoSTTYpe_2.disabled = true;

            if (document.forms[0].cboInstScheme.value == 'ST') {
                document.forms[0].rdoSTTYpe_0.disabled = false;
                document.forms[0].rdoSTTYpe_1.disabled = false;
                document.forms[0].rdoSTTYpe_2.disabled = false;
            }
        }

        function rdoDisabled() {
            document.forms[0].rdoSTTYpe(0).checked = true;
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }
        function rboRefinancing_onchange() {
            var value = $('#rboRefinancing').find(':checked')[0].value;

            if (value === 'False') 
            {
                $('#divRefinancing').css("display", "inherit");
                return;
            }
            else 
            {
                $('#divRefinancing').css("display", "none");
                $('#ucRefinancing1_txtAgreementNo').val('');
                $('#ucRefinancing1_txtNilaiPelunasan').val('');
            }
        }
        function UploadDokument(ucASPXPath, ucTitle, divLookupContent, paramData) {
            paramData = paramData || '';
            $('#dialog').remove();
            var match = new RegExp(App, 'gi');
            var tmpStrPath = AppInfo.replace(match, '');
            tmpStrPath = tmpStrPath.substring(2, tmpStrPath.length - 1);
            var arrStrPath = tmpStrPath.split('/');
            tmpStrPath = '';
            $.each(arrStrPath, function (index, value) {
                if (arrStrPath.length - 1 != index) {
                    tmpStrPath += '../'
                }
            });



            $('#' + divLookupContent).prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><img src="' + tmpStrPath + 'Images/exit_lookup00.png" class="ui-custom-close" /><iframe src="' + ucASPXPath + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
            $('#dialog')
	        .data('fnName', paramData)
	        .dialog({
	            title: ucTitle,
	            bgiframe: false,
	            width: 900,
	            height: 500,
	            resizable: false,
	            modal: true,
	            closeOnEscape: true,
	            draggable: true,
	            create: function (event, ui) {
	                $(".ui-widget-header").hide();
	            },
	            open: function () {
	                $('.ui-custom-close').bind('click', function () {
	                    $('#dialog').dialog('close');
	                })
	            }
	        });
            return false;
        }

        function rboSourceApp_onchange(e) { 
            var _SourceApp = $('#cboSourceApp').val();
            if (e === 'R') {
                $('#divReferal').css("display", "inline"); 
                return;
            }
            else {
                $('#divReferal').css("display", "none"); 
                return;
            }
        }
        //$(document).ready(function (e) { 
        //    var _SourceApp = $('#cboSourceApp').val();
        //    alert(_SourceApp);
        //    //rboSourceApp_onchange(); 
        //});
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinZipCode(pZipCode, pKelurahan, pKecamatan, pCity, pStyle) {
            window.open(ServerName + App + '/General/LookUpZipCode.aspx?Zipcode=' + pZipCode + '&Kelurahan=' + pKelurahan + '&Kecamatan=' + pKecamatan + '&City=' + pCity + '&Style=' + pStyle, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=1, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinProductOfferingLookup(pProductOfferingID, pProductOfferingDescription, pProductID, pAssetTypeID, pStyle, pBranchID) {
            window.open(ServerName + App + '/General/LookUpProductOffering.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID + '&AssetTypeID=' + pAssetTypeID + '&branchid=' + pBranchID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinKabupaten(pProductOfferingID, pProductOfferingDescription, pProductID, pStyle) {
            window.open(ServerName + App + '/General/LookupKabupaten.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        ENTRI APLIKASI BARU</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">Tujuan Pembiayaan</label>  
                        <asp:DropDownList ID="cboKegiatanUsaha" runat="server" AutoPostBack="true"  Width="300px"/>
                         <asp:RequiredFieldValidator ID="Requiredfieldvalidator65" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboKegiatanUsaha" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>Lini Bisnis</label>
                        <asp:DropDownList ID="cboLiniBisnis" runat="server"  Width="200px">
                            <asp:ListItem Value="RETAIL">RETAIL</asp:ListItem>
                            <asp:ListItem Value="FLEET">FLEET</asp:ListItem>
                            <asp:ListItem Value="AGRO">AGRO</asp:ListItem>
                            <asp:ListItem Value="NPCD">NPCD</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Akad/Skema Pembiayaan
                        </label>
                        <asp:DropDownList runat="server" ID="cboAkad"  Width="300px" AutoPostBack="true">
                            <%--<asp:listitem value="Murabahah">Murabahah</asp:listitem>--%>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboAkad" CssClass="validator_general" ErrorMessage="*" InitialValue="Select One"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                       <%-- <label class="label_req">
                            Jenis Pembiayaan
                        </label>
                        <asp:DropDownList runat="server" ID="cboJenisPembiyaan"  Width="300px" AutoPostBack="true"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboJenisPembiyaan" CssClass="validator_general" ErrorMessage="*" InitialValue="Select One"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label>Tipe Loan</label>
                        <asp:DropDownList ID="cboTipeLoan" runat="server"  Width="200px">
                            <asp:ListItem Value="R">Regular</asp:ListItem>
                            <asp:ListItem Value="O">Over Pembiayaan</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
            <uc1:ucProdOffering id="ucLookupProductOffering1" runat="server"></uc1:ucProdOffering>
            </div>
            <div class="form_box" visible="false" style="display:none">
                 <div> 
                <div class="form_left">
                        <div style="float:left">
                        <label class="label_req" ID="lblFacilityNo" runat="server"> Fasilitas </label>
                        <label ID="lblFacilityNo1" runat="server"> Fasilitas</label>                     
                        <asp:HiddenField runat="server" ID="hdnKode" />                                        
                        <asp:HiddenField runat="server" ID="hdnCustID" />     
                        
                            <asp:TextBox runat="server" ID="txtFacilityNo" Enabled="false" CssClass="medium_text"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server" ErrorMessage="*"
                                ControlToValidate="txtFacilityNo" CssClass="validator_general" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                        <div style="margin-top:2px">
                            <asp:Panel ID="Panel1" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/CustFacility.aspx?kode=" & hdnKode.ClientID & "&nama=" & txtFacilityNo.ClientID & "&CustomerID=" & hdnCustID.Value) %>','Daftar Fasilitas','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" Display="Dynamic"
                                CssClass="validator_general" ControlToValidate="txtAssetType"></asp:RequiredFieldValidator>--%>
                            </asp:Panel>
                        </div> 
                 </div>
            </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                         <label>
                            Skema Angsuran</label>
                        <asp:DropDownList ID="cboInstScheme" runat="server" onchange="InstScheme();">
                            <asp:ListItem Value="RF">Regular Fixed Installment Scheme</asp:ListItem>
                            <asp:ListItem Value="IR">Irregular Installment</asp:ListItem>
                            <asp:ListItem Value="ST">Step Up/Step Down</asp:ListItem>
                            <asp:ListItem Value="BP">Ballon Payment</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>
                                Jenis Margin</label>
                            <asp:DropDownList ID="cboInterestType" runat="server" onchange="InterestType();">
                                <asp:ListItem Value="FX">Fix Margin</asp:ListItem>
                                <asp:ListItem Value="FL">Floating Margin</asp:ListItem>
                            </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                       <label class="label_general">
                                Tipe Step Up Step Down</label>
                            <asp:RadioButtonList ID="rdoSTTYpe" runat="server" RepeatDirection="Horizontal" Enabled="False"
                                CssClass="opt_single">
                                <asp:ListItem Value="NM" Selected="True">Basic</asp:ListItem>
                                <asp:ListItem Value="RL">Normal</asp:ListItem>
                                <asp:ListItem Value="LS">Leasing</asp:ListItem>
                            </asp:RadioButtonList>
                    </div>

                    <div class="form_right">
                            <label>
                                Hak Opsi</label>
                            <asp:CheckBox runat="server" ID="chkHakOpsi" />
                        </div>
                    
                    <div class="form_right" style="display:none">
                       <label class="label_general"> Fasilitas</label>
                            <%--<asp:CheckBox runat="server" ID="chkCOP" />--%>
                        <asp:RadioButtonList ID="chkCOP" runat="server" RepeatDirection="Horizontal" Enabled="true"
                                CssClass="opt_single">
                                <asp:ListItem Value="1" >Yes</asp:ListItem>
                                <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                <%--<asp:ListItem Value="2" Selected="True">None</asp:ListItem> --%>
                            </asp:RadioButtonList>
                        </div>
                </div>
            </div>
           <%-- <div class="form_box">
                    <div>
                        <div class="form_left">
                            
                        </div>
                        <div class="form_right">
                            <label>
                                Hak Opsi</label>
                            <asp:CheckBox runat="server" ID="chkHakOpsi" />
                        </div>
                    </div>
             </div>--%>
            <div class="form_button">
                <asp:Button runat="server" ID="btnNext" Text="Next" CssClass="button small green"  />
            </div>
            <asp:Panel runat="server" ID="pnlDataAplikasi">
                <div class="form_box_title">
                    <div class="form_left">
                        <h4>
                            DATA UMUM</h4>
                    </div>
                    <div class="form_right">
                        <h4>
                            BIAYA - BIAYA</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        
                        <div class="form_left">
                            <label>
                                Cara Pembayaran
                            </label>
                            <asp:DropDownList ID="cboWayPymt" runat="server">
                                <asp:ListItem Value="TF" Selected="True">Transfer</asp:ListItem>
                                <asp:ListItem Value="PD">PDC</asp:ListItem>
                                <asp:ListItem Value="CA">Cash</asp:ListItem>
                                <asp:ListItem Value="PG">Potong Gaji</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Administrasi Nett</label>
                            <uc8:ucnumberformat id="ucAdminFee" runat="server" />
                            <%--<asp:TextBox ID="txtAdminFee" runat="server" Enabled="True" MaxLength="12"></asp:TextBox>
                        <asp:RangeValidator ID="RVAdmin" runat="server" Display="Dynamic" ControlToValidate="txtAdminFee"
                            Type="Double" ErrorMessage="*" CssClass="validator_general"></asp:RangeValidator>--%>
                            <asp:CheckBox ID="chkIsAdminFeeCredit" runat="server" Text="Pembiayaan" CssClass="checkbox_general"
                                Visible="false"></asp:CheckBox>
                             <asp:DropDownList runat="server" ID="cboAdmCapitalized" style="width:100px">  
                                    <asp:ListItem Value="0">Dibebankan</asp:ListItem>
                                    <asp:ListItem Value="1">Capitalized</asp:ListItem>
                                </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Fiducia Register</label>
                            <asp:CheckBox runat="server" ID="chkFiducia" Checked="true" CssClass="checkbox_general" />
                        </div>
                        
                        <div class="form_right">
                            <label>
                                Biaya Fiducia
                            </label>
                            <uc8:ucnumberformat id="ucFiduciaFee" runat="server" />
                            <%--<asp:TextBox ID="txtFiduciaFee" runat="server" MaxLength="12"></asp:TextBox>
                        <asp:RangeValidator ID="RVFiducia" runat="server" Display="Dynamic" ControlToValidate="txtFiduciaFee"
                            Type="Double" ErrorMessage="*" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RangeValidator ID="RangeValidator9" runat="server" Display="Dynamic" ControlToValidate="txtFiduciaFee"
                            ErrorMessage="Harap isi dengan Angka" MinimumValue="0" MaximumValue="99999999"
                            CssClass="validator_general"></asp:RangeValidator><br />--%>
                            <%--<asp:DropDownList ID="CboStatusFiducia" runat="server">
                            <asp:ListItem>Select One</asp:ListItem>
                            <asp:ListItem Value="0" Selected="True">Optional - Not Registered</asp:ListItem>
                            <asp:ListItem Value="1">Mandatory - Registered !</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ControlToValidate="CboStatusFiducia" ErrorMessage="Harap isi Status Fiducia" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Sumber Aplikasi
                            </label>
                                <%--Modify by Wira 20180111--%>
<%--                            <asp:DropDownList ID="cboSourceApp" runat="server">                            
                                <asp:ListItem Value="D">Direct</asp:ListItem>
                                <asp:ListItem Value="I" Selected="True">Indirect</asp:ListItem>--%>                                
                            <asp:DropDownList ID="cboSourceApp" runat="server" onChange="rboSourceApp_onchange(this.value);">
                                <asp:ListItem Value="C" Selected="True">CMO</asp:ListItem>
                                <asp:ListItem Value="R" >Referal</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Notaris
                            </label>
                            <uc8:ucnumberformat id="ucNotaryFee" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                        </div>
                        <div class="form_right">
                            <label>
                                <%--Biaya Lainnya--%>
                                Upping Biaya Admin
                            </label>
                            <%--<asp:TextBox ID="txtOtherFee" runat="server" MaxLength="12"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator7" runat="server" Display="Dynamic" ControlToValidate="txtOtherFee"
                            Type="Double" ErrorMessage="Harap isi dengan Angka" MinimumValue="0" MaximumValue="999999999999.99"
                            CssClass="validator_general"></asp:RangeValidator>--%>
                            <uc8:ucnumberformat id="ucOtherFee" runat="server" />
                            <asp:DropDownList runat="server" ID="cboUpAdmCapitalized" style="width:100px">  
                                    <asp:ListItem Value="0">Dibebankan</asp:ListItem>
                                    <asp:ListItem Value="1">Capitalized</asp:ListItem>
                                </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--Modify by Wira 2018011--%>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                        </div>
                        <div class="form_right">
                            <label>
                                Handling Fee
                            </label>
                            <uc8:ucnumberformat id="ucHandlingFee" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <div style="display:none">
                            <label>
                                Ta'widh Keterlambatan
                            </label>
                            <asp:TextBox ID="txtLatePenalty" runat="server" Enabled="False" CssClass="small_text"></asp:TextBox>‰
                            / day
                            <asp:RangeValidator ID="RVPenalty" runat="server" Display="Dynamic" ControlToValidate="txtLatePenalty"
                                Type="Double" ErrorMessage="Ta'widh keterlambatan" CssClass="validator_general"></asp:RangeValidator>
                            </div>
                        </div>
                        
                        <div class="form_right">
                            <asp:UpdatePanel runat="server" ID="upAdminGross" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <label>
                                        Biaya Administrasi Gross</label>
                                    <%--<uc8:ucnumberformat id="txtAdminFeeGross" runat="server" />--%>
                                    <asp:Label runat="server" ID="lblAdminFeeGross" CssClass="numberAlign regular_text">0</asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ucAdminFee" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="ucFiduciaFee" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="ucOtherFee" EventName="TextChanged" />
                                    <asp:AsyncPostBackTrigger ControlID="ucHandlingFee" EventName="TextChanged" />                                    
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress runat="server" ID="upgAdminGross" AssociatedUpdatePanelID="upAdminGross">
                                <ProgressTemplate>
                                    <img alt="Calculating..." src="../../../../Images/pic-loader.gif" />
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>
                </div>
              
                <div class="form_box_hide">
                    <div>
                        <div class="form_left">
                            <label>
                                Recourse
                            </label>
                            <asp:Label ID="lblRecourse" runat="server"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Provisi
                            </label>
                            <%--<asp:TextBox ID="txtProvisionFee" runat="server" MaxLength="12"></asp:TextBox>
                        <asp:RangeValidator ID="RVProvision" runat="server" Display="Dynamic" ControlToValidate="txtProvisionFee"
                            Type="Double" ErrorMessage="*" CssClass="validator_general"></asp:RangeValidator>--%>
                            <asp:CheckBox ID="chkProvisiCredit" runat="server" Text="Pembiayaan" Visible="false"
                                CssClass="checkbox_general"></asp:CheckBox>
                            <uc8:ucnumberformat id="ucProvisionFee_" isReadOnly="true" runat="server" />
                            <div style="display:none">
                            <uc8:ucnumberformat id="ucProvisionFee" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_box_hide">
                    <div>
                        <div class="form_left">
                            <label>
                                Tanpa Data Keuangan</label>
                            <asp:CheckBox runat="server" ID="chkTanpaDataKeuangan" Checked="false" CssClass="checkbox_general" />
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Survey
                            </label>
                            <%--<asp:TextBox ID="txtSurveyFee" runat="server" MaxLength="12"></asp:TextBox>
                        <asp:RangeValidator ID="RVSurvey" runat="server" Display="Dynamic" ControlToValidate="txtSurveyFee"
                            Type="Double" ErrorMessage="*" CssClass="validator_general"></asp:RangeValidator>--%>
                            <uc8:ucnumberformat id="ucSurveyFee" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="form_box_hide">
                    <div>
                        <div class="form_left">
                            &nbsp;
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Balik Nama (BBN)
                            </label>
                            <%--<asp:TextBox ID="txtBBNFee" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rfvBBNFee" ControlToValidate="txtBBNFee"
                            CssClass="validator_general" Display="Dynamic" ErrorMessage="Isi 0 jika tidak ada biaya BBN!"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator6" runat="server" Display="Dynamic" ControlToValidate="txtBBNFee"
                            Type="Double" ErrorMessage="Harap isi dengan Angka" MinimumValue="0" MaximumValue="999999999999"
                            CssClass="validator_general"></asp:RangeValidator>--%>
                            <uc8:ucnumberformat id="ucBBNFee" runat="server" />
                        </div>
                    </div>
                </div>
                <div class="form_box_hide">
                    <div>
                        <div class="form_left">
                            &nbsp;
                        </div>
                        <div class="form_right">
                            <label>
                                Biaya Pemeriksaan BPKB
                            </label>
                             <uc8:ucnumberformat id="ucBiayaPeriksaBPKB" runat="server" />
                        </div>
                    </div>
                </div>

            
        <%--Modify by Wira 20180111 -- Referal--%>
        <div runat="server" id="divReferal">
            <div class="form_box_title">
                <div class="form_left"><h4>REFERAL RTMIS</h4></div> 
           
                <div class="form_right"><h4>ALOKASI REFERAL</h4></div> 
            </div>

             <div class="form_box">
                    <div>                        
                        <div class="form_right">
                            <label>
                                Unit Bisnis
                            </label>
                            <asp:DropDownList ID="cboUnitBisnis" runat="server">
                                <asp:ListItem Value="A" Selected="True">BANK BNI 46</asp:ListItem>
                                <asp:ListItem Value="B">BNI LIFE</asp:ListItem>
                                <asp:ListItem Value="C">BNI MULTIFINANCE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form_left">
                            <label>
                                Nilai Referal
                            </label>
                            <uc8:ucnumberformat id="ucReferalFee" runat="server" />
                        </div>
                    </div>
                </div>

            <div class="form_box">   
                <div>
                    <div class="form_right">
                            <label>
                                Lokasi
                            </label>
                            <asp:TextBox ID="txtLokasi" runat="server" Width="300px"></asp:TextBox>
                        </div>
                    <div class="form_left">
                            <label>
                                Porsi Institusi
                            </label>                       
                            <uc8:UcNumberFormat id="UcPorsiInstitusi" runat="server" width="50" />
                            <label class="label_auto "> % </label>
                        </div> 
                  </div>     
            </div> 
            <asp:UpdatePanel runat="server">
            <ContentTemplate>
            <div class="form_box">
                <div>
                    <div class="form_right">
                         <label>
                                NPP
                            </label>
                            <asp:TextBox ID="txtNPP" Enabled="false" runat="server" Width="300px"></asp:TextBox>
                             <asp:Button ID="btnLookupNPP" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                            <uc10:uclookupNPP id="ucLookupNPP1" runat="server" oncatselected="CatSelectedNPP"></uc10:uclookupNPP>  
                    </div>
                    <div class="form_left">
                         <label>
                                Porsi Pemberi Referal
                            </label> 
                            <uc8:UcNumberFormat id="UcPorsiPemberiReferal" runat="server" width="50" />
                            <label class="label_auto "> % </label>
                    </div>
                </div>
            </div>  
            <div class="form_box">
                <div>
                    <div class="form_right">
                        <label>
                            Nama
                        </label>
                        <asp:TextBox ID="txtNama" runat="server" Width="300px"></asp:TextBox>
                    </div>
                    <div class="form_left"></div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_right">
                        <label >
                            NPWP
                        </label>
                        <asp:TextBox ID="txtNPWP" runat="server" onkeypress="return numbersonly2(event)"></asp:TextBox> 
                    </div>
                    <div class="form_left"></div>
                </div>
            </div>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
            
 

                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            CATATAN APLIKASI</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <asp:TextBox ID="txtAppNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox" Width="80%"></asp:TextBox>
                    </div>
                </div>
               
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            ALAMAT SURAT MENYURAT
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Copy Alamat dari</label>
                        <asp:DropDownList ID="cboCopyAddress" runat="server" onChange="SelectFromArray((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]);">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
              
                    <uc1:UcCompanyAdress id="UCMailingAddress" runat="server"></uc1:UcCompanyAdress>
            </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kota/Kabupaten DATI II</label>                            
                        <asp:HiddenField runat="server" ID="hdfkabupaten" />                            
                        <asp:TextBox runat="server" ID="txtKabupaten" Enabled="false" CssClass="medium_text"></asp:TextBox>
                        <button class="small buttongo blue" 
                        onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Kabupaten.aspx?kode=" & hdfkabupaten.ClientID & "&nama=" & txtKabupaten.ClientID) %>','Daftar Kabupaten','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                        <asp:RequiredFieldValidator ID="rfvtxtKabupaten" runat="server" ErrorMessage="*" Display="Dynamic" 
                            CssClass="validator_general" ControlToValidate="txtKabupaten"></asp:RequiredFieldValidator>                            
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h5>
                            SYARAT & KONDISI</h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgTC" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                CellPadding="3" CellSpacing="1" DataKeyField="TCName" PageSize="3" CssClass="grid_general"
                                Width="100%">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="NO">
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DOKUMEN">
                                        <HeaderStyle Width="300px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:label ID="TCName" runat="server"><%# DataBinder.eval(Container, "DataItem.TCName") %></asp:label>
                                            <asp:Image runat="server" ID="imgAtteced" ImageUrl="../../../../Images/tick_icon.gif" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="">
                                        <HeaderStyle Width="80px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyupload" style="cursor:pointer" runat="server">Attached</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                        <HeaderStyle Width="50px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PERIKSA">
                                        <HeaderStyle Width="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTCChecked" runat="server" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'>
                                            </asp:CheckBox>
                                            <asp:Label ID="lblVTCChecked" runat="server"  CssClass="label_req">&nbsp;&nbsp;&nbsp;</asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                                        <HeaderStyle Width="30px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="TGL. JANJI">
                                        <HeaderStyle Width="100px"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <uc8:ucdatece runat="server" id="txtPromiseDate"></uc8:ucdatece>
                                            <asp:Label ID="lblVPromiseDate" runat="server" CssClass="validator_general">Tanggal janji harus > dari business date!</asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CATATAN">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                                CssClass="desc_textbox">
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="" HeaderStyle-Width="0px" >
                                        <ItemTemplate>
                                            <asp:TextBox style="display:none" runat="server" ID="MasterTCID" Text='<%# DataBinder.eval(Container, "DataItem.MasterTCID") %>'>
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <%--tidak ditampilkan--%>
                <div class="form_box_hide">
                    <div class="form_single">
                        <h4>
                            SYARAT & KONDISI CHECK LIST</h4>
                    </div>
                </div>
                <div class="form_box_hide">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgTC2" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" DataKeyField="TCName" PageSize="3" CssClass="grid_ws"
                            Width="1500px">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                    <HeaderStyle Width="300px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="NO">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST">
                                    <HeaderStyle Width="100px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="PERIKSA">
                                    <HeaderStyle Width="150px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkTCCheck2" runat="server" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'>
                                        </asp:CheckBox>
                                        <asp:Label ID="lblVTC2Checked" runat="server" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY">
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="TANGGAL JANJI">
                                    <HeaderStyle Width="350px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <uc8:ucdatece runat="server" id="txtPromiseDate2"></uc8:ucdatece>
                                        <asp:Label ID="lblVPromiseDate2" runat="server" CssClass="validator_general">Tanggal janji harus > dari business date!</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CATATAN">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                            Width="95%">
                                        </asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                                </asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cboKegiatanUsaha" EventName="SelectedIndexChanged" />
            <%--<asp:AsyncPostBackTrigger ControlID="cboJenisPembiyaan" EventName="SelectedIndexChanged" />--%>
            <asp:AsyncPostBackTrigger ControlID="cboAkad" EventName="SelectedIndexChanged" />
           <%-- <asp:AsyncPostBackTrigger ControlID="btnLookupNPP" EventName="Click" />--%>
            
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
