﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class Application_003
    Inherits Maxiloan.Webform.WebBased
    Private oController As New ApplicationController
    Private m_controller As New ProductController
    Private oAssetDataController As New AssetDataController
#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucLookupProductOffering1 As UcLookUpPdctOffering

    Protected WithEvents ucMailingAddress As UcCompanyAddress

    Protected WithEvents ucApplicationTab1 As ucApplicationTab

    Protected WithEvents ucAdminFee As ucNumberFormat
    Protected WithEvents ucFiduciaFee As ucNumberFormat
    Protected WithEvents ucProvisionFee As ucNumberFormat
    Protected WithEvents ucProvisionFee_ As ucNumberFormat
    Protected WithEvents ucNotaryFee As ucNumberFormat
    Protected WithEvents ucSurveyFee As ucNumberFormat
    Protected WithEvents ucBBNFee As ucNumberFormat
    Protected WithEvents ucBiayaPeriksaBPKB As ucNumberFormat
    Protected WithEvents ucOtherFee As ucNumberFormat
    'Protected WithEvents ucRefinancing1 As ucRefinancing
    'Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucHandlingFee As ucNumberFormat
    Protected WithEvents ucReferalFee As ucNumberFormat
    Protected WithEvents UcPorsiInstitusi As ucNumberFormat
    Protected WithEvents UcPorsiPemberiReferal As ucNumberFormat
    Protected WithEvents ucLookupNPP1 As ucLookupNPP

#End Region

#Region "Constanta"
    Dim oRow As DataRow
    Dim Status As Boolean
#End Region

#Region "Properties"
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property ProductID() As String
        Get
            Return ViewState("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property

    Property ProductOffID() As String
        Get
            Return ViewState("ProductOffID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOffID") = Value
        End Set
    End Property

    Property KodeDATI() As String
        Get
            Return ViewState("KodeDATI").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("KodeDATI") = Value
        End Set
    End Property

    Property Type() As String
        Get
            Return ViewState("Type").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Property Desc() As String
        Get
            Return ViewState("Desc").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Desc") = Value
        End Set
    End Property

    Property TotalCountApplicationID() As Integer
        Get
            Return CType(ViewState("TotalCountApplicationID"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("TotalCountApplicationID") = Value
        End Set
    End Property

    Property PageMode As String
        Get
            Return CType(ViewState("PageMode"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PageMode") = value
        End Set
    End Property
    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property
    Public Property RefinancingAgreementNo As String
        Get
            Return ViewState("RefinancingAgreementNo").ToString
        End Get
        Set(value As String)
            ViewState("RefinancingAgreementNo") = value
        End Set
    End Property
    Public Property RefinancingNilaiPelunasan As Integer
        Get
            Return CType(ViewState("RefinancingNilaiPelunasan"), Integer)
        End Get
        Set(value As Integer)
            ViewState("RefinancingNilaiPelunasan") = value
        End Set
    End Property
    Public Property NPPID() As String
        Get
            Return ViewState("NPPID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NPPID") = Value
        End Set
    End Property
    Public Property Name() As String
        Get
            Return ViewState("Name").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property
    Public Property NPWP() As String
        Get
            Return ViewState("NPWP").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NPWP") = Value
        End Set
    End Property

    Property IsCustomerFasility() As Integer
        Get
            Return CType(ViewState("IsCustomerFasility"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("IsCustomerFasility") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            ucMailingAddress.IsRequiredZipcode = False
            Me.CustomerID = Request("id")
            Me.ProspectAppID = "-"

            Dim oCustomclass As New Parameter.Application

            With oCustomclass
                .strConnection = GetConnectionString()
                .CustomerId = Me.CustomerID
            End With
            hdnCustID.Value = Me.CustomerID
            Me.TotalCountApplicationID = oController.GetCountAppliationId(oCustomclass)
            initObjects()

            Me.PageMode = Request("page")
            LoadingKegiatanUsaha()
            If Me.PageMode = "Edit" Then
                Me.ApplicationID = Request("appid")

                With ucViewApplication1
                    .ApplicationID = Me.ApplicationID
                    .CustomerID = Me.CustomerID
                    .bindData()
                    .initControls("OnNewApplication")
                    If .IsAppTimeLimitReached Then
                        ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                        btnSave.Visible = False
                    End If
                End With


                BindEdit()

                ucApplicationTab1.ApplicationID = Me.ApplicationID
                ucApplicationTab1.setLink()
            Else
                'fillFormTesting()
                'chkFleet.Checked = ucViewCustomerDetail1.IsFleet
                'Modify by Wira 20171114
                'ambil nilai initial data
                If Me.ProspectAppID = "-" And Me.PageMode = "Add" Then
                    initObjects()
                End If
                divReferal.Style.Add("display", "none")

            End If


            If ucViewApplication1.isProceed = True Then
                btnSave.Visible = False
            End If

            'modify Nofi 26042019
            Me.IsCustomerFasility = oController.GetCustomerFacility(oCustomclass)
            If Me.IsCustomerFasility = 1 Then
                Requiredfieldvalidator11.Enabled = True
                lblFacilityNo.Visible = True
                lblFacilityNo1.Visible = False
            Else
                Requiredfieldvalidator11.Enabled = False
                lblFacilityNo.Visible = False
                lblFacilityNo1.Visible = True
            End If
            'FillCboEmp("VwCustomerFacility Where CustomerID = '" & Me.CustomerID.Trim & "'", cboFasilitas)
            GetCboSkePemb_("TblSkemaPembiayaan", cboAkad)
            ' GetCboJePemb_("JenisPembiayaan", cboJenisPembiyaan)
            'chkCOP.SelectedValue = "0"
        End If
    End Sub
    Private Sub LoadingKegiatanUsaha()
        Me.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataSource = KegiatanUsahaS
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"
        cboKegiatanUsaha.SelectedIndex = 0


        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        'cboJenisPembiyaan.DataValueField = "Value"
        'cboJenisPembiyaan.DataTextField = "Text"
        'cboJenisPembiyaan.DataSource = def
        'cboJenisPembiyaan.Items.Insert(0, "Select One")
        'cboJenisPembiyaan.Items(0).Value = "SelectOne"
        'cboJenisPembiyaan.DataBind()

        'If cboJenisPembiyaan.SelectedValue.ToString.Trim = "FL" Then
        '    chkHakOpsi.Enabled = True
        'End If
    End Sub

#Region "Lookup Product Offering"
    'Public Sub ProductOffSelected(ByVal strSelectedID1 As String, ByVal strSelectedID2 As String, ByVal strSelectedName1 As String)
    '    Me.ProductOffID = strSelectedID1
    '    Me.ProductID = strSelectedID2
    '    'txtProductOffering.Text = strSelectedName1
    '    GetFee()
    '    TC(Me.PageMode)
    '    TC2(Me.PageMode)
    '    LabelValidationFalse()

    'End Sub

    'Protected Sub btnLookupProducOffering_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupProducOffering.Click
    '    ucLookupProductOffering1.BranchID = Replace(Me.sesBranchId, "'", "")
    '    ucLookupProductOffering1.doBind()
    '    ucLookupProductOffering1.Popup()
    'End Sub
#End Region

#Region "Init Objects"
    Protected Sub initObjects()
        'txtProductOffering.Attributes.Add("readOnly", "true")
        'txtKotaKabupaten.Attributes.Add("readonly", "true")

        ucViewApplication1.CustomerID = Me.CustomerID
        ucViewApplication1.bindData()
        ucViewApplication1.initControls("OnNewApplication")
        ucViewCustomerDetail1.CustomerID = Me.CustomerID
        ucViewCustomerDetail1.bindCustomerDetail()


        Me.Type = ucViewCustomerDetail1.CustomerType

        ucLookupProductOffering1.BranchID = Me.sesBranchId.Replace("'", "")
        ucAdminFee.OnClientChange = "calculateAdminFeeGross()"
        ucFiduciaFee.OnClientChange = "calculateAdminFeeGross()"
        ucNotaryFee.OnClientChange = "calculateAdminFeeGross()"
        ucOtherFee.OnClientChange = "calculateAdminFeeGross()"
        ucHandlingFee.OnClientChange = "calculateAdminFeeGross()"

        ucApplicationTab1.selectedTab("Application")
        fillAddress(Me.CustomerID)

        ucMailingAddress.ValidatorTrue()
        ucMailingAddress.showMandatoryAll()
        pnlDataAplikasi.Visible = False

        chkHakOpsi.Enabled = False
    End Sub

    Protected Sub fillFormTesting()
        'cboJenisPembiyaan.SelectedIndex = 1
        'cboPolaTransaksi.SelectedIndex = 1

        'txtProductOffering.Text = "MOBIL_1    Insurance"
        Me.ProductID = "MBL-BARU"
        Me.ProductOffID = "MOBIL_1"

        GetFee()
        TC(Me.PageMode)
        TC2(Me.PageMode)
        LabelValidationFalse()

        'txtSurveyFee.Text = "100000"
        'txtBBNFee.Text = "200000"
        'txtBiayaPeriksaBPKB.Text = "300000"
        'txtOtherFee.Text = "400000"

        txtAppNotes.Text = "Catatan aplikasi"



        Me.KodeDATI = "0111"
        'txtKotaKabupaten.Text = "BANDUNG"
    End Sub
#End Region



#Region "FillAddress"
    Private Sub fillAddress(ByVal id As String)
        Dim oData As New DataTable
        Dim oApplication As New Parameter.Application
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.CustomerId = id
        oApplication.Type = Me.Type
        oApplication = oController.GetAddress(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        cboCopyAddress.DataSource = oData.DefaultView
        cboCopyAddress.DataTextField = "Combo"
        cboCopyAddress.DataValueField = "Type"
        cboCopyAddress.DataBind()

        If Me.Type = "C" Then
            If oData.Rows.Count > 0 Then
                If Me.PageMode <> "Edit" Then
                    oRow = oData.Rows(0)
                    ucMailingAddress.Address = oRow.Item(0).ToString.Trim
                    ucMailingAddress.RT = oRow.Item(1).ToString.Trim
                    ucMailingAddress.RW = oRow.Item(2).ToString.Trim
                    ucMailingAddress.Kelurahan = oRow.Item(3).ToString.Trim
                    ucMailingAddress.Kecamatan = oRow.Item(4).ToString.Trim
                    ucMailingAddress.City = oRow.Item(5).ToString.Trim
                    ucMailingAddress.ZipCode = oRow.Item(6).ToString.Trim
                    ucMailingAddress.AreaPhone1 = oRow.Item(7).ToString.Trim
                    ucMailingAddress.Phone1 = oRow.Item(8).ToString.Trim
                    ucMailingAddress.AreaPhone2 = oRow.Item(9).ToString.Trim
                    ucMailingAddress.Phone2 = oRow.Item(10).ToString.Trim
                    ucMailingAddress.AreaFax = oRow.Item(11).ToString.Trim
                    ucMailingAddress.Fax = oRow.Item(12).ToString.Trim
                    ucMailingAddress.BindAddress()
                End If
            End If
        Else
            If Me.PageMode <> "Edit" Then
                Dim index As Integer
                For intLoop = 0 To oData.Rows.Count - 1
                    If oData.Rows(intLoop).Item("Type").ToString.Trim = "Residence" Then
                        index = intLoop
                    End If
                Next

                cboCopyAddress.SelectedIndex = cboCopyAddress.Items.IndexOf(cboCopyAddress.Items.FindByValue("Residence"))

                oRow = oData.Rows(index)
                ucMailingAddress.Address = oRow.Item(0).ToString.Trim
                ucMailingAddress.RT = oRow.Item(1).ToString.Trim
                ucMailingAddress.RW = oRow.Item(2).ToString.Trim
                ucMailingAddress.Kelurahan = oRow.Item(3).ToString.Trim
                ucMailingAddress.Kecamatan = oRow.Item(4).ToString.Trim
                ucMailingAddress.City = oRow.Item(5).ToString.Trim
                ucMailingAddress.ZipCode = oRow.Item(6).ToString.Trim
                ucMailingAddress.AreaPhone1 = oRow.Item(7).ToString.Trim
                ucMailingAddress.Phone1 = oRow.Item(8).ToString.Trim
                ucMailingAddress.AreaPhone2 = oRow.Item(9).ToString.Trim
                ucMailingAddress.Phone2 = oRow.Item(10).ToString.Trim
                ucMailingAddress.AreaFax = oRow.Item(11).ToString.Trim
                ucMailingAddress.Fax = oRow.Item(12).ToString.Trim
                ucMailingAddress.BindAddress()
            End If
        End If

        If Not ClientScript.IsStartupScriptRegistered("copyAddress") Then ClientScript.RegisterStartupScript(Me.GetType, "copyAddress", GenerateScript(oData))
    End Sub

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboCopyAddress.Items.Count - 1
            DataRow = DtTable.Select(" Type = '" & cboCopyAddress.Items(j).Value.Trim & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("Type")).Trim Then
                        strType = CStr(DataRow(i)("Type")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & DataRow(i)("Address").ToString.Trim.Replace(vbLf, "") & "','" & DataRow(i)("RT").ToString.Trim & "' " &
                                        ",'" & DataRow(i)("RW").ToString.Trim & "','" & DataRow(i)("Kelurahan").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("Kecamatan").ToString.Trim & "','" & DataRow(i)("City").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("ZipCode").ToString.Trim & "','" & DataRow(i)("AreaPhone1").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("Phone1").ToString.Trim & "','" & DataRow(i)("AreaPhone2").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("Phone2").ToString.Trim & "','" & DataRow(i)("AreaFax").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("Fax").ToString.Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next
        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If
        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        strScript &= "</script>"
        Return strScript
    End Function

#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.AppID = Me.ApplicationID
        oApplication = oController.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)

            cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(oRow("KegiatanUsaha").ToString))
            refresh_cboJenisPembiayaan(oRow("KegiatanUsaha").ToString)
            ' cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(oRow("JenisPembiayaan").ToString))
            cboAkad.SelectedValue = oRow("Akad").ToString.Trim()


            'If cboJenisPembiyaan.SelectedValue.ToString.Trim = "FL" Then
            '    chkHakOpsi.Enabled = True
            'End If

            cboInterestType.SelectedIndex = cboInterestType.Items.IndexOf(cboInterestType.Items.FindByValue(oRow("InterestType").ToString))
            cboInstScheme.SelectedIndex = cboInstScheme.Items.IndexOf(cboInstScheme.Items.FindByValue(oRow("InstallmentScheme").ToString))

            cboWayPymt.SelectedIndex = cboWayPymt.Items.IndexOf(cboWayPymt.Items.FindByValue(oRow("WayOfPayment").ToString))
            cboSourceApp.SelectedIndex = cboSourceApp.Items.IndexOf(cboSourceApp.Items.FindByValue(oRow("ApplicationSource").ToString))
            If cboSourceApp.SelectedValue.ToString.Trim = "C" Then
                divReferal.Style.Add("display", "none")

            ElseIf cboSourceApp.SelectedValue.ToString.Trim = "R" Then

            End If

            ucAdminFee.Text = FormatNumber(oRow("AdminFee"), 0)
            ucFiduciaFee.Text = FormatNumber(oRow("FiduciaFee") - oRow("NotaryFee"), 0)
            chkFiducia.Checked = CBool(oRow("IsFiduciaCovered").ToString.Trim)
            chkTanpaDataKeuangan.Checked = CBool(IIf(oRow("NoApplicationData").ToString.Trim = "", "0", oRow("NoApplicationData").ToString.Trim))
            ucProvisionFee.Text = FormatNumber(oRow("ProvisionFee").ToString, 0)
            ucProvisionFee_.Text = FormatNumber(oRow("ProvisionFee").ToString, 0)
            ucNotaryFee.Text = FormatNumber(oRow("NotaryFee").ToString, 0)
            ucSurveyFee.Text = FormatNumber(oRow("SurveyFee"), 0)
            ucBBNFee.Text = FormatNumber(oRow("BBNFee"), 0)
            ucBiayaPeriksaBPKB.Text = FormatNumber(oRow("BiayaPeriksaBPKB"), 0)
            ucOtherFee.Text = FormatNumber(oRow("OtherFee"), 0)
            ucHandlingFee.Text = FormatNumber(oRow("HandlingFee"), 0)
            cboUnitBisnis.SelectedIndex = cboUnitBisnis.Items.IndexOf(cboUnitBisnis.Items.FindByValue(oRow("UnitBisnis").ToString))
            ucReferalFee.Text = FormatNumber(oRow("ReferalFee"), 0)
            txtLokasi.Text = oRow("Lokasi").ToString
            UcPorsiInstitusi.Text = FormatNumber(oRow("PorsiInstitusi"), 0)
            txtNPP.Text = oRow("NPP").ToString
            UcPorsiPemberiReferal.Text = FormatNumber(oRow("PorsiPemberiReferal"), 0)
            txtNPWP.Text = oRow("NPWP").ToString
            txtNama.Text = oRow("Name").ToString
            ucLookupNPP1.NPP = oRow("NPP").ToString

            txtFacilityNo.Text = oRow("NoFasilitas").ToString

            hdfkabupaten.Value = oRow("kodedati").ToString
            txtKabupaten.Text = oRow("NamaKabKot").ToString
            'modify nofi 10032018
            'chkCOP.Checked = CBool(IIf(oRow("IsCOP").ToString.Trim = "", "0", oRow("IsCOP").ToString.Trim)) 
            If oRow("IsCOP").ToString = "1" Then
                chkCOP.SelectedValue = "1"
            ElseIf oRow("IsCOP").ToString = "0" Then
                chkCOP.SelectedValue = "0"
            Else
                chkCOP.SelectedValue = "2"
            End If

            chkHakOpsi.Checked = CBool(IIf(oRow("HakOpsi").ToString.Trim = "", "0", oRow("HakOpsi").ToString.Trim))
            cboLiniBisnis.SelectedIndex = cboLiniBisnis.Items.IndexOf(cboLiniBisnis.Items.FindByValue(oRow("LiniBisnis").ToString.Trim))
            cboTipeLoan.SelectedIndex = cboTipeLoan.Items.IndexOf(cboTipeLoan.Items.FindByValue(oRow("TipeLoan").ToString.Trim))

            txtLatePenalty.Text = FormatNumber(oRow("PercentagePenalty"), 0)
            lblRecourse.Text = IIf(CType(oRow("IsRecourse"), Boolean) = True, "Yes", "No").ToString
            txtAppNotes.Text = oRow("Notes").ToString.Trim
            lblAdminFeeGross.Text = FormatNumber(CDbl(oRow("adminFee")) + CDbl(oRow("FiduciaFee")) + CDbl(oRow("BiayaPolis")) + CDbl(oRow("OtherFee")) + CDbl(oRow("HandlingFee")), 0)

            With ucMailingAddress
                .Address = oRow("MailingAddress").ToString.Trim
                .RT = oRow("MailingRT").ToString.Trim
                .RW = oRow("MailingRW").ToString.Trim
                .Kelurahan = oRow("MailingKelurahan").ToString.Trim
                .Kecamatan = oRow("MailingKecamatan").ToString.Trim
                .City = oRow("MailingCity").ToString.Trim
                .ZipCode = oRow("MailingZipCode").ToString.Trim
                .AreaPhone1 = oRow("MailingAreaPhone1").ToString.Trim
                .Phone1 = oRow("MailingPhone1").ToString.Trim
                .AreaPhone2 = oRow("MailingAreaPhone2").ToString.Trim
                .Phone2 = oRow("MailingPhone2").ToString.Trim
                .AreaFax = oRow("MailingAreaFax").ToString.Trim
                .Fax = oRow("MailingFax").ToString.Trim
                .BindAddress()
            End With


            ucLookupProductOffering1.ProductID = oRow("ProductID").ToString.Trim
            ucLookupProductOffering1.ProductOfferingID = oRow("ProductOfferingID").ToString.Trim
            ucLookupProductOffering1.ProductOfferingDescription = oRow("Description").ToString.Trim

            ucLookupProductOffering1.KegiatanUsaha = oRow("KegiatanUsaha").ToString.Trim

            Me.ProductID = oRow("ProductID").ToString.Trim
            Me.ProductOffID = oRow("ProductOfferingID").ToString.Trim
            'ucRefinancing1.ApplicationID = oRow("RefinancingApplicationID").ToString.Trim
            'ucRefinancing1.AgreementNo = oRow("RefinancingAgreementNo").ToString.Trim
            'ucRefinancing1.NilaiPelunasan = FormatNumber(oRow("RefinancingContractPrepaidAmount").ToString, 0)

            cboAdmCapitalized.SelectedValue = oRow("isAdmCapitalized").ToString.Trim
            cboUpAdmCapitalized.SelectedValue = oRow("isUpAdmCapitalized").ToString.Trim


        End If
    End Sub
#End Region

#Region "Fee & Behaviour"
    Private Sub GetFee()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.ProductOffID = Me.ProductOffID
        oApplication.ProductID = Me.ProductID
        oApplication = oController.GetFee(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
            If oData.Rows.Count > 0 Then
                oRow = oData.Rows(0)

                ucAdminFee.setBehaviour("AdminFee", oRow)
                ucFiduciaFee.setBehaviour("FiduciaFee", oRow)
                ucProvisionFee.setBehaviour("ProvisionFee", oRow)
                ucNotaryFee.setBehaviour("NotaryFee", oRow)
                ucSurveyFee.setBehaviour("SurveyFee", oRow)
                ucHandlingFee.setBehaviour("HandlingFee", oRow)

                Behaviour("PenaltyPercentage", txtLatePenalty, RVPenalty)

                If Not Me.PageMode = "Edit" Then
                    ucAdminFee.Text = FormatNumber(oRow("AdminFee"), 0)
                    ucFiduciaFee.Text = FormatNumber(oRow("FiduciaFee"), 0)
                    ucProvisionFee.Text = FormatNumber(oRow("ProvisionFee"), 0)
                    ucProvisionFee_.Text = FormatNumber(oRow("ProvisionFee"), 0)
                    ucNotaryFee.Text = FormatNumber(oRow("NotaryFee"), 0)
                    ucSurveyFee.Text = FormatNumber(oRow("SurveyFee"), 0)
                    'txtLatePenalty.Text = FormatNumber(oRow("PenaltyPercentage"), 0)
                    'Modify by Wira 20180924
                    'jika load 0.5 maka jadi 1, user tidak mau
                    txtLatePenalty.Text = oRow("PenaltyPercentage")
                    ucHandlingFee.Text = FormatNumber(oRow("HandlingFee"), 0)
                End If

                'calculateAdminFeeGross()
            End If
        End If
    End Sub

    Sub Behaviour(ByVal ColumnName As String, ByVal TextName As TextBox, ByVal RangeValid As RangeValidator)
        TextName.Text = CInt(oRow(ColumnName)).ToString.Trim
        Select Case oRow(ColumnName + "Patern").ToString.Trim
            Case "D"
                RangeValid.MinimumValue = "0"
                RangeValid.MaximumValue = "999999999999999"
                RangeValid.ErrorMessage = "Input Salah"
                RangeValid.Enabled = True
                TextName.ReadOnly = False
            Case "L"
                RangeValid.MinimumValue = "0"
                RangeValid.MaximumValue = "999999999999999"
                RangeValid.ErrorMessage = "Input Salah"
                RangeValid.Enabled = True
                TextName.ReadOnly = True
            Case "N"
                TextName.ReadOnly = False
                RangeValid.MinimumValue = CInt(oRow(ColumnName)).ToString.Trim
                RangeValid.MaximumValue = "999999999999999"
                RangeValid.ErrorMessage = "Input harus >= " & oRow(ColumnName).ToString.Trim & "!"
                RangeValid.Enabled = True
            Case "X"
                TextName.ReadOnly = False
                RangeValid.MaximumValue = CInt(oRow(ColumnName)).ToString.Trim
                RangeValid.MinimumValue = "0"
                RangeValid.ErrorMessage = "Input harus <= " & oRow(ColumnName).ToString.Trim & "!"
                RangeValid.Enabled = True
        End Select
    End Sub
#End Region

#Region "BindTC"
    Protected Sub TC(ByVal AddEdit As String)
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        If AddEdit = "Add" Then
            oApplication.Type = Me.Type
            oApplication.ProductID = ucLookupProductOffering1.ProductID
        Else
            oApplication.AppID = Me.ApplicationID
        End If

        With oApplication
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
            .AddEdit = AddEdit
            .PersonalCustomerType = ucViewCustomerDetail1.PersonalCustomerType
        End With

        oApplication = oController.GetTC(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC.DataSource = oData.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()
        'dtgTC.Width = CType(IIf(hdnGridGeneralSize.Value = "", "100%", hdnGridGeneralSize.Value), Unit)
        If AddEdit = "Edit" Then
            Dim intLoop As Integer
            For intLoop = 0 To dtgTC.Items.Count - 1
                If oData.Rows(intLoop).Item(4).ToString.Trim <> "" Then
                    CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), ucDateCE).Text = Format(oData.Rows(intLoop).Item(4), "dd/MM/yyyy")
                End If
            Next
        End If
    End Sub

    Sub TC2(ByVal AddEdit As String)
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        If AddEdit = "Add" Then
            oApplication.Type = Me.Type
            oApplication.ProductID = Me.ProductID
        Else
            oApplication.AppID = Me.ApplicationID
        End If

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.AddEdit = AddEdit
        oApplication = oController.GetTC2(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC2.DataSource = oData.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()

        If AddEdit = "Edit" Then
            Dim intLoop As Integer
            For intLoop = 0 To dtgTC2.Items.Count - 1
                If oData.Rows(intLoop).Item("PromiseDate").ToString.Trim <> "" Then
                    CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), ucDateCE).Text = Format(oData.Rows(intLoop).Item("PromiseDate"), "dd/MM/yyyy")
                End If
            Next
        End If
    End Sub
    Private Function pathFile(ByVal Group As String, ByVal CustomerID As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & CustomerID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chkTCChecked As CheckBox
            Dim hyupload As HyperLink = CType(e.Item.FindControl("hyupload"), HyperLink)
            Dim txtDateCE As TextBox = CType(e.Item.FindControl("txtPromiseDate").FindControl("txtDateCE"), TextBox)
            Dim MasterTCID As TextBox = CType(e.Item.FindControl("MasterTCID"), TextBox)
            Dim imgAtteced As Image = CType(e.Item.FindControl("imgAtteced"), Image)


            If File.Exists(pathFile("Dokumen", Me.CustomerID) + MasterTCID.Text.ToString.Trim + ".jpg") Then
                imgAtteced.Visible = True
            End If

            chkTCChecked = CType(e.Item.FindControl("chkTCChecked"), CheckBox)

            If chkTCChecked.Checked Then
                txtDateCE.Enabled = False
            Else
                txtDateCE.Enabled = True
            End If

            hyupload.Attributes.Add("OnClick", "UploadDokument('" & ResolveClientUrl("~/webform.Utility/jLookup/UploadDokument.aspx?doc=" & MasterTCID.Text.Trim & "&CustomerID=" & Me.CustomerID) & "','Upload Dokument','" & jlookupContent.ClientID & "') ")

            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            chkTCChecked.Attributes.Add("OnClick",
                "return CheckTC('" & txtDateCE.ClientID & "',this.checked);")
        End If
    End Sub

    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chkTCCheck2 As CheckBox
            Dim txtDateCE As TextBox = CType(e.Item.FindControl("txtPromiseDate2").FindControl("txtDateCE"), TextBox)


            chkTCCheck2 = CType(e.Item.FindControl("chkTCCheck2"), CheckBox)

            If chkTCCheck2.Checked Then
                txtDateCE.Enabled = False
            Else
                txtDateCE.Enabled = True
            End If


            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            chkTCCheck2.Attributes.Add("OnClick",
            "return CheckTC('" & txtDateCE.ClientID & "',this.checked);")
        End If
    End Sub
#End Region

#Region "LabelValidationFalse"
    Protected Sub LabelValidationFalse()
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim countDtg As Integer = CInt(IIf(DtgTC1count > DtgTC2count, DtgTC1count, DtgTC2count))
        Dim validCheck As New Label
        Dim Checked As Boolean
        Dim PriorTo As String
        Dim PromiseDate As ucDateCE
        Dim Mandatory As String

        For intLoop = 0 To countDtg
            If intLoop <= DtgTC1count Then
                PriorTo = dtgTC.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(5).Text.Trim
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), ucDateCE)
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = False
                If PriorTo = "APK" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If

            If intLoop <= DtgTC2count Then
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), ucDateCE)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = False
                If PriorTo = "APK" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
        Next
    End Sub
#End Region

    Protected Sub cek_Check()
        Dim CheckTC As Boolean
        Dim CheckTC2 As Boolean
        Dim counter, counter2 As Integer
        Dim PromiseDate As ucDateCE
        Dim PromiseDate2 As ucDateCE

        For counter = 0 To dtgTC.Items.Count - 1
            CheckTC = CType(dtgTC.Items(counter).FindControl("chkTCChecked"), CheckBox).Checked
            If CheckTC Then
                PromiseDate = CType(dtgTC.Items(counter).FindControl("txtPromiseDate"), ucDateCE)
                PromiseDate.Text = ""
            End If
        Next

        For counter = 0 To dtgTC2.Items.Count - 1
            CheckTC2 = CType(dtgTC2.Items(counter).FindControl("chkTCCheck2"), CheckBox).Checked
            If CheckTC2 Then
                PromiseDate2 = CType(dtgTC2.Items(counter).FindControl("txtPromiseDate2"), ucDateCE)
                PromiseDate2.Text = ""
            End If
        Next
    End Sub

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Page.IsValid Then
                lblMessage.Visible = False

                Dim oApplication As New Parameter.Application
                Dim oValidasiCollector As New Parameter.Application
                Dim oRefAddress As New Parameter.Address
                Dim oRefPersonal As New Parameter.Personal
                Dim oMailingAddress As New Parameter.Address
                Dim oGuarantorPersonal As New Parameter.Personal
                Dim oGuarantorAddress As New Parameter.Address
                Dim oData1 As New DataTable
                Dim oData2 As New DataTable
                Dim oData3 As New DataTable
                Dim oCustomclass As New Parameter.Application

                With oCustomclass
                    .strConnection = GetConnectionString()
                    .CustomerId = Me.CustomerID
                End With

                'Validasi Collector
                oCustomclass.Kelurahan = ucMailingAddress.Kelurahan
                oCustomclass.ZipCode = ucMailingAddress.ZipCode
                oValidasiCollector = oController.ValidasiCollector(oCustomclass)
                If oValidasiCollector.ListData IsNot Nothing Then
                    If oValidasiCollector.ListData.Rows.Count < 1 Then
                        ShowMessage(lblMessage, "Collector belum disetting!", True)
                        Exit Sub
                    End If
                End If


                If Me.TotalCountApplicationID = oController.GetCountAppliationId(oCustomclass) Then
                    Status = True
                    oData3.Columns.Add("AgreementNo", GetType(String))

                    If Status = False Then
                        LabelValidationFalse()
                        ShowMessage(lblMessage, "No. Kontrak Salah!", True)
                        Exit Sub
                    End If

                    oData1.Columns.Add("MasterTCID", GetType(String))
                    oData1.Columns.Add("PriorTo", GetType(String))
                    oData1.Columns.Add("IsChecked", GetType(String))
                    oData1.Columns.Add("IsMandatory", GetType(String))
                    oData1.Columns.Add("PromiseDate", GetType(String))
                    oData1.Columns.Add("Notes", GetType(String))

                    oData2.Columns.Add("MasterTCID", GetType(String))
                    oData2.Columns.Add("AGTCCLSequenceNo", GetType(String))
                    oData2.Columns.Add("IsChecked", GetType(String))
                    oData2.Columns.Add("PromiseDate", GetType(String))
                    oData2.Columns.Add("Notes", GetType(String))

                    Validator(oData1, oData2)

                    If Status = False Then
                        cek_Check()
                        Exit Sub
                    End If

                    oApplication.BusinessDate = Me.BusinessDate
                    oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                    oApplication.CustomerId = Me.CustomerID
                    ' oApplication.JenisPembiayaan = cboJenisPembiyaan.SelectedValue
                    oApplication.Akad = cboAkad.SelectedValue
                    oApplication.KegiatanUsaha = cboKegiatanUsaha.SelectedValue.ToString.Trim
                    oApplication.PolaTransaksi = Me.ProductID
                    oApplication.ProductID = Me.ProductID
                    oApplication.ProductOffID = Me.ProductOffID
                    oApplication.ContractStatus = "PRP"
                    oApplication.DefaultStatus = "NM"
                    oApplication.ApplicationStep = "APK"
                    oApplication.NumOfAssetUnit = 1
                    oApplication.InterestType = cboInterestType.SelectedValue
                    If cboInstScheme.Enabled = True Then
                        oApplication.InstallmentScheme = cboInstScheme.SelectedValue
                    Else
                        oApplication.InstallmentScheme = "RF"
                    End If

                    oApplication.ApplicationModule = "AUTO"
                    oApplication.WayOfPayment = cboWayPymt.SelectedValue
                    oApplication.AgreementDate = ""
                    oApplication.SurveyDate = ConvertDate(Me.BusinessDate.ToString("dd/MM/yyyy"))
                    oApplication.ApplicationSource = cboSourceApp.SelectedValue
                    oApplication.PercentagePenalty = txtLatePenalty.Text
                    oApplication.AdminFee = ucAdminFee.Text

                    oApplication.AdditionalAdminFee = "0"
                    oApplication.StatusFiduciaFee = chkFiducia.Checked.ToString
                    oApplication.NoApplicationData = chkTanpaDataKeuangan.Checked.ToString
                    oApplication.ProspectAppID = Me.ProspectAppID
                    oApplication.FiduciaFee = CDbl(ucFiduciaFee.Text) + CDbl(ucNotaryFee.Text)
                    oApplication.ProvisionFee = ucProvisionFee.Text
                    oApplication.NotaryFee = ucNotaryFee.Text
                    oApplication.SurveyFee = ucSurveyFee.Text
                    oApplication.BBNFee = ucBBNFee.Text
                    oApplication.OtherFee = ucOtherFee.Text
                    oApplication.CrossDefaultApplicationId = ""
                    oApplication.Notes = txtAppNotes.Text
                    'oApplication.IsCOP = chkCOP.Checked
                    oApplication.IsCOP = chkCOP.SelectedValue
                    oApplication.LiniBisnis = cboLiniBisnis.SelectedValue
                    oApplication.TipeLoan = cboTipeLoan.SelectedValue
                    oApplication.HakOpsi = chkHakOpsi.Checked
                    oApplication.NoPJJ = ""
                    oApplication.IsNST = "0"
                    oApplication.HandlingFee = ucHandlingFee.Text
                    oApplication.UnitBisnis = cboUnitBisnis.SelectedValue
                    oApplication.ReferalFee = ucReferalFee.Text    'ucReferalFee.Text
                    oApplication.Lokasi = txtLokasi.Text
                    oApplication.PorsiInstitusi = UcPorsiInstitusi.Text
                    oApplication.NPP = txtNPP.Text
                    oApplication.PorsiPemberiReferal = UcPorsiPemberiReferal.Text
                    oApplication.NPWP = txtNPWP.Text
                    oApplication.Name = txtNama.Text
                    oApplication.UsrUpd = Me.UserID
                    oApplication.DtmUpd = Me.BusinessDate

                    If (cboInterestType.SelectedValue = "FX" And cboInstScheme.SelectedValue = "ST") Then
                        oApplication.StepUpStepDownType = rdoSTTYpe.SelectedValue
                    Else
                        oApplication.StepUpStepDownType = ""
                    End If

                    oApplication.IsAdminFeeCredit = chkIsAdminFeeCredit.Checked
                    oApplication.IsProvisiCredit = chkProvisiCredit.Checked
                    oApplication.BiayaPeriksaBPKB = CDbl(ucBiayaPeriksaBPKB.Text)

                    'oApplication.KodeDATI = Me.KodeDATI
                    oApplication.KodeDATI = hdfkabupaten.Value ' ucLookupKabupaten1.DatiID

                    oApplication.AdminFeeGross = CDbl(lblAdminFeeGross.Text)

                    'If oApplication.Refinancing Then
                    '    oApplication.RefinancingApplicationID = ucRefinancing1.ApplicationID.Trim
                    'Else
                    '    oApplication.RefinancingApplicationID = ""
                    'End If

                    ' oApplication.IsFleet = chkFleet.Checked

                    oApplication.RefinancingApplicationID = ""

                    oApplication.strConnection = GetConnectionString()
                    oApplication.isAdmCapitalized = CBool(cboAdmCapitalized.SelectedValue.ToString)
                    oApplication.isUpAdmCapitalized = CBool(cboUpAdmCapitalized.SelectedValue.ToString)
                    oApplication.FacilityNo = txtFacilityNo.Text
                    oApplication.Akad = cboAkad.Text

                    oMailingAddress.Address = ucMailingAddress.Address
                    oMailingAddress.RT = ucMailingAddress.RT
                    oMailingAddress.RW = ucMailingAddress.RW
                    oMailingAddress.Kecamatan = ucMailingAddress.Kecamatan
                    oMailingAddress.Kelurahan = ucMailingAddress.Kelurahan
                    oMailingAddress.ZipCode = ucMailingAddress.ZipCode
                    oMailingAddress.City = ucMailingAddress.City
                    oMailingAddress.AreaPhone1 = ucMailingAddress.AreaPhone1
                    oMailingAddress.AreaPhone2 = ucMailingAddress.AreaPhone2
                    oMailingAddress.AreaFax = ucMailingAddress.AreaFax
                    oMailingAddress.Phone1 = ucMailingAddress.Phone1
                    oMailingAddress.Phone2 = ucMailingAddress.Phone2
                    oMailingAddress.Fax = ucMailingAddress.Fax

                    Dim ErrorMessage As String = ""
                    Dim oReturn As New Parameter.Application

                    If Me.PageMode = "Edit" Then
                        oApplication.ApplicationID = Me.ApplicationID
                        oReturn = oController.ApplicationSaveEdit(oApplication,
                                                              oRefPersonal,
                                                              oRefAddress,
                                                              oMailingAddress,
                                                              oData1,
                                                              oData2,
                                                              oData3,
                                                              oGuarantorPersonal,
                                                              oGuarantorAddress)
                    Else
                        oReturn = oController.ApplicationSaveAdd(oApplication,
                                                             oRefPersonal,
                                                             oRefAddress,
                                                             oMailingAddress,
                                                             oData1,
                                                             oData2,
                                                             oData3,
                                                             oGuarantorPersonal,
                                                             oGuarantorAddress)

                        Me.ApplicationID = oReturn.ApplicationID
                        ucViewApplication1.CustomerID = Me.CustomerID
                        ucViewApplication1.ApplicationID = Me.ApplicationID
                        ucViewApplication1.bindData()
                        ucViewApplication1.initControls("OnNewApplication")
                        'createAssetDataCookies()
                        'ucApplicationTab1.ApplicationID = Me.ApplicationID
                        'ucApplicationTab1.setLink()
                        'Me.PageMode = "Edit"
                    End If

                    LabelValidationFalse()
                    If oReturn.Err <> "" Then
                        ShowMessage(lblMessage, ErrorMessage, True)
                    Else
                        ShowMessage(lblMessage, "Data saved!", False)
                        ucApplicationTab1.ApplicationID = Me.ApplicationID
                        ucApplicationTab1.setLink()
                        Me.PageMode = "Edit"
                    End If
                Else
                    ShowMessage(lblMessage, "Data Sudah Ada", True)
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Protected Sub Validator(ByRef oData1 As DataTable, ByRef oData2 As DataTable)
        Dim PriorTo As String
        Dim Mandatory As String
        Dim Checked As Boolean
        Dim PromiseDate As ucDateCE
        Dim validCheck As New Label
        Dim MasterTCID2, AGTCCLSequenceNo, Notes As String
        Dim MasterTCID As New TextBox
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1


        For intLoop = 0 To CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
            If intLoop <= DtgTC1count Then
                MasterTCID = CType(dtgTC.Items(intLoop).FindControl("MasterTCID"), TextBox)
                PriorTo = dtgTC.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(5).Text.Trim
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), ucDateCE)
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                Notes = CType(dtgTC.Items(intLoop).Cells(7).FindControl("txtTCNotes"), TextBox).Text

                If PromiseDate.Text <> "" Then
                    If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = True
                        Status = False
                    End If
                End If

                If PriorTo = "APK" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.Text = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If

                oRow = oData1.NewRow
                oRow("MasterTCID") = MasterTCID.Text.Trim
                oRow("PriorTo") = PriorTo
                oRow("IsChecked") = IIf(Checked = True, "1", "0")
                oRow("IsMandatory") = IIf(Mandatory = "v", "1", "0")
                oRow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
                oRow("Notes") = Notes
                oData1.Rows.Add(oRow)
            End If



            'Disable karena Syarat dan kondisi ceklist tidak dipakai
            'If intLoop <= DtgTC2count Then
            '    Context.Trace.Write("Validator Kondisi 4")
            '    MasterTCID = dtgTC2.Items(intLoop).Cells(8).Text
            '    AGTCCLSequenceNo = dtgTC2.Items(intLoop).Cells(9).Text
            '    PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
            '    Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
            '    Notes = CType(dtgTC2.Items(intLoop).Cells(7).FindControl("txtTCNotes2"), TextBox).Text
            '    Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
            '    PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), ucDateCE)
            '    validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)

            '    If PromiseDate.Text <> "" Then
            '        Context.Trace.Write("Validator Kondisi 5")
            '        If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
            '            Context.Trace.Write("Validator Kondisi 6")
            '            CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = True
            '        End If
            '    End If
            '    If PriorTo = "APK" And Mandatory = "v" Then
            '        Context.Trace.Write("Validator Kondisi 7")
            '        If Checked = False And PromiseDate.Text = "" Then
            '            Context.Trace.Write("Validator Kondisi 8")
            '            If Status <> False Then
            '                Status = False
            '                Context.Trace.Write("Validator Kondisi 8")
            '            End If
            '            validCheck.Visible = True
            '            Context.Trace.Write("Validator Kondisi 9")
            '        End If
            '    Else
            '        validCheck.Visible = False
            '    End If

            '    oRow = oData2.NewRow
            '    oRow("MasterTCID") = MasterTCID
            '    oRow("AGTCCLSequenceNo") = AGTCCLSequenceNo
            '    oRow("IsChecked") = IIf(Checked = True, "1", "0")
            '    oRow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
            '    oRow("Notes") = Notes
            '    oData2.Rows.Add(oRow)
            'End If
        Next

        'Dim RainCheck As Boolean = False
        'Dim intLoop2, intLoop3, intLoop4, intLoop5 As Integer

        'For intLoop = 0 To DtgTC1count
        '    MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
        '    Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
        '    For intLoop2 = 0 To DtgTC2count
        '        MasterTCID2 = dtgTC2.Items(intLoop2).Cells(8).Text
        '        If MasterTCID = MasterTCID2 Then
        '            If Checked = True Then
        '                CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = False

        '                If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = False Then
        '                    CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
        '                    If Status <> False Then
        '                        Status = False
        '                    End If
        '                Else
        '                    CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = False
        '                End If
        '            Else
        '                If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = True Then
        '                    For intLoop4 = 0 To intLoop2
        '                        If dtgTC2.Items(intLoop4).Cells(8).Text = MasterTCID Then
        '                            If CType(dtgTC2.Items(intLoop4).FindControl("chkTCCheck2"), CheckBox).Checked = False Then
        '                                CType(dtgTC2.Items(intLoop4).FindControl("lblVTC2Checked"), Label).Visible = True
        '                            Else
        '                                CType(dtgTC2.Items(intLoop4).FindControl("lblVTC2Checked"), Label).Visible = False
        '                            End If
        '                        End If
        '                    Next

        '                    'For intLoop5 = intLoop2 To DtgTC2count
        '                    '    If dtgTC2.Items(intLoop5).Cells(8).Text = MasterTCID Then
        '                    '        CType(dtgTC2.Items(intLoop5).FindControl("lblVTC2Checked"), Label).Visible = True
        '                    '    End If
        '                    'Next

        '                    CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True

        '                    If Status <> False Then
        '                        Status = False
        '                    End If

        '                    'For intLoop3 = 0 To intLoop
        '                    '    If dtgTC.Items(intLoop3).Cells(7).Text = MasterTCID2 Then
        '                    '        If CType(dtgTC.Items(intLoop3).FindControl("chkTCChecked"), CheckBox).Checked = False Then
        '                    '            CType(dtgTC.Items(intLoop3).FindControl("lblVTCChecked"), Label).Visible = True
        '                    '        End If
        '                    '    End If
        '                    'Next
        '                End If
        '            End If
        '        End If
        '    Next
        '    RainCheck = False
        'Next
    End Sub
#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Me.PageMode = "Edit" Then
            Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
        Else
            Response.Redirect("Application.aspx")
        End If
    End Sub

    Protected Sub calculateAdminFeeGross() Handles ucAdminFee.TextChanged, ucFiduciaFee.TextChanged, ucOtherFee.TextChanged
        Dim admin, fiducia, other, biayaPolis, notary, handling As Double

        admin = CDbl(IIf(IsNumeric(ucAdminFee.Text), ucAdminFee.Text, 0))
        fiducia = CDbl(IIf(IsNumeric(ucFiduciaFee.Text), ucFiduciaFee.Text, 0))
        notary = CDbl(IIf(IsNumeric(ucNotaryFee.Text), ucNotaryFee.Text, 0))
        other = CDbl(IIf(IsNumeric(ucOtherFee.Text), ucOtherFee.Text, 0))
        handling = CDbl(IIf(IsNumeric(ucHandlingFee.Text), ucHandlingFee.Text, 0))

        lblAdminFeeGross.Text = FormatNumber(admin + fiducia + other + biayaPolis + notary + handling, 0)
    End Sub

    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        'chkCOP.SelectedValue = "0"
        pnlDataAplikasi.Visible = True
        btnNext.Visible = False
        Me.ProductOffID = ucLookupProductOffering1.ProductOfferingID
        Me.ProductID = ucLookupProductOffering1.ProductID
        cboKegiatanUsaha.Enabled = False
        ' cboJenisPembiyaan.Enabled = False
        cboAkad.Enabled = False
        ucLookupProductOffering1.VisiblePnlLookupProdOff = False
        GetFee()
        TC(Me.PageMode)
        TC2(Me.PageMode)
        LabelValidationFalse()
        calculateAdminFeeGross()
    End Sub

    Private Sub cboKegiatanUsaha_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboKegiatanUsaha.SelectedIndexChanged
        Dim cbo = CType(sender, DropDownList)
        Dim value = cbo.SelectedValue
        If cbo.SelectedIndex = 0 Then
            refresh_cboJenisPembiayaan("")
        Else
            ucLookupProductOffering1.KegiatanUsaha = value
            refresh_cboJenisPembiayaan(value)
        End If
        'If cboJenisPembiyaan.SelectedValue.ToString.Trim = "FL" Then
        '    chkHakOpsi.Enabled = True
        'End If
    End Sub
    Private Sub cboAkad_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboAkad.SelectedIndexChanged
        Dim cbo = CType(sender, DropDownList)
        Dim value = cbo.SelectedValue
        If cbo.SelectedIndex = 0 Then
            refresh_cboAkad("")
        Else
            ucLookupProductOffering1.Akad = value
            refresh_cboAkad(value)
        End If
    End Sub
    Sub refresh_cboAkad(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboAkad.DataValueField = "Value"
        cboAkad.DataTextField = "Text"

        'If key = String.Empty Then
        '    cboAkad.DataSource = def
        '    cboAkad.DataBind()
        'Else
        '    Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
        '    cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        'End If
        cboAkad.DataBind()
        'cboAkad.SelectedIndex = cboAkad.Items.IndexOf(cboAkad.Items.FindByValue("IF"))
        ucLookupProductOffering1.Akad = cboAkad.SelectedValue
    End Sub
    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        'cboJenisPembiyaan.DataValueField = "Value"
        'cboJenisPembiyaan.DataTextField = "Text"

        'If key = String.Empty Then
        '    cboJenisPembiyaan.DataSource = def
        '    cboJenisPembiyaan.DataBind()
        'Else
        '    Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
        '    cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        'End If
        'cboJenisPembiyaan.DataBind()
        'cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue("IF"))
        'ucLookupProductOffering1.JenisPembiayaan = cboJenisPembiyaan.SelectedValue
    End Sub
    'Private Sub cboJenisPembiyaan_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboJenisPembiyaan.SelectedIndexChanged
    '    ucLookupProductOffering1.JenisPembiayaan = cboJenisPembiyaan.SelectedValue
    '    If cboJenisPembiyaan.SelectedValue.ToString.Trim = "FL" Then
    '        chkHakOpsi.Enabled = True
    '    Else
    '        chkHakOpsi.Enabled = False
    '    End If
    'End Sub


#Region "OnChange"
    Protected Sub btnLookupNPP_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupNPP.Click
        ucLookupNPP1.CmdWhere = "All"
        ucLookupNPP1.Sort = "NPP ASC"
        ucLookupNPP1.Popup()
    End Sub

    Public Sub CatSelectedNPP(ByVal NPPID As String, ByVal NPP As String, ByVal Name As String, ByVal NPWP As String)
        'Dim odata As New Parameter.Application
        'Dim oData2 As New DataTable
        'With odata
        '    .strConnection = GetConnectionString()
        '    .NPP = NPP
        'End With

        'odata = oController.getdataNPP(odata)

        'If Not odata Is Nothing Then
        '    oData2 = odata.ListData
        'End If
        'If oData2.Rows.Count > 0 Then
        '    oRow = oData2.Rows(0)
        '    txtNama.Text = oRow("Name").ToString
        '    txtNPWP.Text = oRow("NPWP").ToString
        'End If

        txtNPP.Text = NPP.ToString
        txtNPWP.Text = NPWP.ToString
        txtNama.Text = Name.ToString
        'Me.NPPID = NPPID.ToString

        ShowMessage(lblMessage, NPP.ToString & Name.ToString, True)
    End Sub

    Protected Sub GetCboSkePemb_(ByVal table As String, cboReference As DropDownList)
        Dim dtEntity As DataTable = Nothing
        Dim oCboSkePemb As New Parameter.Product
        oCboSkePemb.strConnection = GetConnectionString()
        oCboSkePemb.Table = table
        oCboSkePemb = m_controller.GetCboSkePemb(oCboSkePemb)
        If Not oCboSkePemb Is Nothing Then
            dtEntity = oCboSkePemb.ListData
        End If

        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        cboReference.Items.Insert(0, "Select One")
        cboReference.Items(0).Value = "Select One"

    End Sub

    Protected Sub GetCboJePemb_(ByVal table As String, cboReference As DropDownList)
        Dim dtEntity As DataTable = Nothing
        Dim oCboJenPemb As New Parameter.Product
        oCboJenPemb.strConnection = GetConnectionString()
        oCboJenPemb.Table = table
        oCboJenPemb = m_controller.GetCboJenPemb(oCboJenPemb)
        If Not oCboJenPemb Is Nothing Then
            dtEntity = oCboJenPemb.ListData
        End If

        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        cboReference.Items.Insert(0, "Select One")
        cboReference.Items(0).Value = "Select One"

        'If cboJenisPembiyaan.SelectedValue.ToString.Trim = "FL" Then
        '    chkHakOpsi.Enabled = True
        'End If
    End Sub

#End Region


    'Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList)
    '    Dim oAssetData As New Parameter.AssetData
    '    Dim oData As New DataTable

    '    oAssetData.strConnection = GetConnectionString()
    '    oAssetData.Table = Table
    '    oAssetData = oAssetDataController.GetCbo(oAssetData)

    '    oData = oAssetData.ListData
    '    cboName.DataSource = oData
    '    cboName.DataTextField = "Description"
    '    cboName.DataValueField = "ID"
    '    cboName.DataBind()
    '    cboName.Items.Insert(0, "Select One")
    '    cboName.Items(0).Value = "Select One"
    'End Sub
End Class