﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApplicationTransactionDetail.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ApplicationTransactionDetail" %>

<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTab.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RINCIAN DATA TRANSAKSI</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" />
    <link rel="Stylesheet" href="../../../../Include/Lookup.css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" />
    <<%--style type="text/css">
        .wp
        {
            background-color: #fff;
            width: 700px;            
            border: 1px solid #ccc;
            float: left;
        }
        
        .wp h4
        {
            margin: 0;
            color: #000;
            text-align: left;
        }
        
        .wp .gridwp
        {
            max-height: 300px;
            overflow: auto;
        }
        
        .wpbg
        {
            background-color: #000;
            filter: alpha(opacity=70);
            opacity: 0.7;
        }
        
        .wpbtnexit
        {
            position: absolute;
            float: right;
            top: 0;
            right: 0;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label runat="server" ID="lblMessage" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        ENTRI RINCIAN TRANSAKSI</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                    </div>
                    <div class="form_right">
                        <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        PERINCIAN TRANSAKSI
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="gridRincianTransaksi" runat="server" Width="100%" AutoGenerateColumns="False"
                        BorderWidth="0px" CellPadding="3" CellSpacing="1" DataKeyField="TransID" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:ButtonColumn ButtonType="LinkButton" CausesValidation="false" CommandName="DELETE"
                                Text="DELETE"></asp:ButtonColumn>
                            <asp:BoundColumn DataField="TransID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TransDesc" HeaderText="NAMA TRANSAKSI"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TransAmount" HeaderText="NILAI TRANSAKSI" HeaderStyle-CssClass="right_col"
                                ItemStyle-CssClass="right_col" DataFormatString="{0:N0}">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="FlagCustomer" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="FlagCustomerLabel" HeaderText="TAGIH KE CUSTOMER"></asp:BoundColumn>
                            <asp:BoundColumn DataField="FlagSupplier" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="FlagSupplierLabel" HeaderText="PENCAIRAN SUPPLIER"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:Button runat="server" ID="btnAdd" Text="Add" CssClass="small buttongo blue"
                        CausesValidation="false" />
                </div>
            </div>
            <div class="form_button">
                <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="small button blue" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="small button gray" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="UpdatePanel2">
        <ContentTemplate>
            <asp:Button runat="server" ID="btnPopupMasterTransaksi" Style="display: none" />
            <asp:ModalPopupExtender runat="server" ID="mpeMasterTransaksi" TargetControlID="btnPopupMasterTransaksi"
                PopupControlID="Panel1" BackgroundCssClass="wpbg" CancelControlID="btnExit">
            </asp:ModalPopupExtender>
            <asp:Panel runat="server" ID="Panel1">
                <div class="wp">
                    <asp:ImageButton runat="server" ID="btnExit" ImageUrl="../../../../Images/exit_lookup00.png"
                        CssClass="wpbtnexit" />
                    <div class="form_title">
                        <div class="title_strip">
                        </div>
                        <div class="form_single">
                            <h4>
                                ENTRI TRANSAKSI</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nilai Transaksi
                            </label>
                            <asp:TextBox runat="server" ID="txtNilaiTransaksi"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ID="rfNilaitransaksi" ControlToValidate="txtNilaiTransaksi"
                                ErrorMessage="Masukan nilai transaksi!" Display="Dynamic">
                            </asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="reNilaitransaksi" ControlToValidate="txtNilaiTransaksi"
                                ErrorMessage="Masukan angka!" Display="Dynamic" ValidationExpression="\d*">
                            </asp:RegularExpressionValidator>
                            <asp:RangeValidator runat="server" ID="rvNilaitransaksi" ControlToValidate="txtNilaiTransaksi"
                                ErrorMessage="Angka yang dimasukan salah, masukan 0 - 999999999999!" Type="Double"
                                MinimumValue="0" MaximumValue="999999999999">
                            </asp:RangeValidator>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Tagih ke Customer
                            </label>
                            <asp:DropDownList runat="server" ID="cboFlagCustomer">
                                <asp:ListItem Value="n">None</asp:ListItem>
                                <asp:ListItem Value="+" Text="(+) Plus"></asp:ListItem>
                                <asp:ListItem Value="-" Text="(-) Minus"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Potong Pencairan Supplier
                            </label>
                            <asp:DropDownList runat="server" ID="cboFlagSupplier">
                                <asp:ListItem Value="n">None</asp:ListItem>
                                <asp:ListItem Value="+" Text="(+) Plus"></asp:ListItem>
                                <asp:ListItem Value="-" Text="(-) Minus"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <div class="lookup_grid_wrapper">
                                <asp:DataGrid ID="gridMasterTransaksi" runat="server" Width="95%" AutoGenerateColumns="False"
                                    BorderWidth="0px" CellPadding="3" CellSpacing="1" DataKeyField="TransID" CssClass="grid_general">
                                    <HeaderStyle CssClass="th" />
                                    <ItemStyle CssClass="item_grid" />
                                    <Columns>
                                        <asp:ButtonColumn ButtonType="LinkButton" CausesValidation="true" CommandName="SELECT"
                                            Text="SELECT"></asp:ButtonColumn>
                                        <asp:BoundColumn DataField="TransID" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="TransDesc" HeaderText="NAMA TRANSAKSI"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
