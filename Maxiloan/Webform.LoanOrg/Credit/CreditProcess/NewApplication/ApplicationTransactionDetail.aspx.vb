﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

Public Class ApplicationTransactionDetail
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTab
    Private oController As New ApplicationDetailTransactionController    

#Region "Property"    
    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    
    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property

    Private Property ApplicationDetailTransactionDT As DataTable
        Get
            Return CType(ViewState("ApplicationDetailTransactionDT"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("ApplicationDetailTransactionDT") = value
        End Set
    End Property
   
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")            

            'GetCookies()

            InitialPageLoad()
            getMasterTransaksiKontrak()
            'fillFormTesting()
        End If
    End Sub

    'Sub GetCookies()
    '    Dim cookie As HttpCookie = Request.Cookies("RINCIAN_TRANSAKSI")

    '    Me.ApplicationID = cookie.Values("ApplicationID")
    '    Me.CustomerID = cookie.Values("CustomerID")
    '    Me.BranchID = cookie.Values("BranchID")
    'End Sub

    Private Sub InitialPageLoad()       
        lblMessage.Text = ""

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
        End With

        ucApplicationTab1.ApplicationID = Me.ApplicationID
        ucApplicationTab1.selectedTab("RincianTransaksi")
        ucApplicationTab1.setLink()

        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        reNilaitransaksi.Enabled = False
        rfNilaitransaksi.Enabled = False
        rvNilaitransaksi.Enabled = False

        getRincianTransaksi()
    End Sub

    Protected Sub fillFormTesting()
        txtNilaiTransaksi.Text = "200000"
    End Sub

    Protected Sub getRincianTransaksi()
        Dim oPar As New Parameter.ApplicationDetailTransaction

        With oPar
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With

        Try
            oPar = oController.getApplicationDetailTransaction(oPar)
            Me.ApplicationDetailTransactionDT = oPar.ApplicationDetailTransactionDT
            gridRincianTransaksi.DataSource = Me.ApplicationDetailTransactionDT
            gridRincianTransaksi.DataBind()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Protected Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        reNilaitransaksi.Enabled = True
        rfNilaitransaksi.Enabled = True
        rvNilaitransaksi.Enabled = True
        txtNilaiTransaksi.Text = "0"
        cboFlagCustomer.SelectedIndex = 0
        cboFlagSupplier.SelectedIndex = 0
        Me.mpeMasterTransaksi.Show()
    End Sub

    Protected Sub getMasterTransaksiKontrak()
        Dim cPaging As New GeneralPagingController
        Dim oPaging As New Parameter.GeneralPaging

        With oPaging
            .strConnection = GetConnectionString()
            .WhereCond = "IsActive = 1"
            .CurrentPage = 1
            .PageSize = 100
            .SortBy = "TransDesc ASC"
            .SpName = "spGetAgreementMasterTransaction"
        End With

        oPaging = cPaging.GetGeneralPaging(oPaging)

        gridMasterTransaksi.DataSource = oPaging.ListData
        gridMasterTransaksi.DataBind()
    End Sub

    Protected Sub gridMasterTransaksi_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles gridMasterTransaksi.SelectedIndexChanged
        Dim i As Integer = gridMasterTransaksi.SelectedIndex
        Dim oRow As DataRow

        oRow = Me.ApplicationDetailTransactionDT.NewRow

        oRow("TransID") = gridMasterTransaksi.Items(i).Cells(1).Text
        oRow("TransDesc") = gridMasterTransaksi.Items(i).Cells(2).Text
        oRow("TransAmount") = txtNilaiTransaksi.Text
        oRow("FlagCustomer") = cboFlagCustomer.SelectedValue
        oRow("FlagCustomerLabel") = cboFlagCustomer.SelectedItem.Text
        oRow("FlagSupplier") = cboFlagSupplier.SelectedValue
        oRow("FlagSupplierLabel") = cboFlagSupplier.SelectedItem.Text

        Me.ApplicationDetailTransactionDT.Rows.Add(oRow)

        gridRincianTransaksi.DataSource = Me.ApplicationDetailTransactionDT
        gridRincianTransaksi.DataBind()
    End Sub

    Private Sub gridRincianTransaksi_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gridRincianTransaksi.ItemCommand
        Select Case e.CommandName
            Case "DELETE"
                Me.ApplicationDetailTransactionDT.Rows(e.Item.ItemIndex).Delete()
                Me.ApplicationDetailTransactionDT.AcceptChanges()
                gridRincianTransaksi.DataSource = Me.ApplicationDetailTransactionDT
                gridRincianTransaksi.DataBind()
        End Select
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.ApplicationDetailTransaction
        With oPar
            .strConnection = GetConnectionString()
            .ApplicationDetailTransactionDT = Me.ApplicationDetailTransactionDT
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .BusinessDate = Me.BusinessDate
        End With

        Try
            oController.ApplicationDetailTransactionSave(oPar)
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("RincianTransaksi")
            ucApplicationTab1.setLink()
            ShowMessage(lblMessage, "Data saved!", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
End Class