﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewPlafon
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcAgreementList As UcAgreementList
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTab
    Protected WithEvents ucViewCustomerFacility1 As ucViewCustomerFacility
#Region "Constanta"
    Private oController As New UcAgreementListController
    Private oCustomClass As New Parameter.AccMntBase
#End Region

#Region "Property"
    Property Type() As String
        Get
            Return ViewState("Type").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID").ToString.Trim
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .initControls("OnNewApplication")
                .bindData()
                Me.CustomerID = .CustomerID
                Me.CustomerName = .CustomerName
                Me.FacilityNo = .NoFasilitas
            End With
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            loadDataFasilitas()

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Plafon")
            ucApplicationTab1.setLink()

            BindAgreement

            If Request("Application") <> "" Then
                Dim script As String
                script = "<script language=""JavaScript"">" & vbCrLf
                script &= "alert('Your Application Form No. is " & Request("Application").ToString.Trim & "');" & vbCrLf
                script &= "</script>"
                Response.Write(script)
            End If
        End If
    End Sub

    Private Sub loadDataFasilitas()
        With ucViewCustomerFacility1
            .FacilityNo = Me.FacilityNo
            .CustomerID = Me.CustomerID
            .LoadCustomerFacility()
        End With
    End Sub

    Public Sub BindAgreement()
        Dim DvAgreementList As New DataView

        With ocustomclass
            .strConnection = GetConnectionString()
            .NoFasilitas = Me.FacilityNo
            .SortBy = SortBy
        End With

        DvAgreementList = oController.UcAgreementListInv(oCustomClass).DefaultView
        DvAgreementList.Sort = Me.SortBy
        DtgList.DataSource = DvAgreementList
        DtgList.DataBind()

    End Sub
End Class