﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UploadNotariil.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.UploadNotariil" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"></asp:Label>
     <asp:FileUpload ID="files" runat="server" ViewStateMode="Enabled" />
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="small button blue"></asp:Button>
    </form>
</body>
</html>
