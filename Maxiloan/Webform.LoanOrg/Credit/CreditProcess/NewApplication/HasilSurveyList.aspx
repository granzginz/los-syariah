﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HasilSurveyList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.HasilSurveyList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hasil Survey</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                MinimumValue="1" ErrorMessage="No Halaman Salah!" CssClass="validator_general"></asp:RangeValidator>
            <h3>
                DAFTAR APLIKASI</h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:Panel runat="server" ID="pnlList">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" DataKeyField="CustomerID"
                    CssClass="grid_general">                    
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <HeaderStyle Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton CommandName="OPEN" CausesValidation="false" ID="lnkIncentive" runat="server">HASIL SURVEY</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkApplicationID" CausesValidation="false" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="NAME" HeaderText="NAMA CUSTOMER">
                            <HeaderStyle Width="25%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkCustName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierName" HeaderText="NAMA SUPPLIER">
                            <HeaderStyle Width="25%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkSupp" runat="server" Text='<%# Container.DataItem("SupplierName")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="" HeaderText="JENIS APLIKASI">
                            <HeaderStyle Width="10%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationType" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationType") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AO" HeaderText="CMO" Visible="False">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.DataItem("CustomerID")%>'>
                                </asp:Label>
                                <asp:Label ID="lblSupplierID" runat="server" Text='<%# Container.DataItem("SupplierID")%>'>
                                </asp:Label>
                                <asp:Label ID="lblBranchID" runat="server" Text='<%# Container.DataItem("BranchID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div>
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text" >1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"/>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </asp:Panel>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                CARI DATA HASIL SURVEY
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="Agreement.ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="Supplier.SupplierName">Nama Supplier</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                </label>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text = "Search" CssClass="small button blue">
        </asp:ImageButton>
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:ImageButton>
    </div>
    </form>
</body>
</html>
