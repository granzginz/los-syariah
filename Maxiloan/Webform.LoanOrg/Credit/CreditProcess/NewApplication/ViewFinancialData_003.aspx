﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewFinancialData_003.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewFinancialData_003" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTab.ascx" TagName="ucApplicationViewTab"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>View Financial Data 1</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <link rel="stylesheet" type="text/css" href="../../../../include/ui-lightness/jquery-ui-1.10.3.custom.css"  />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" class="tab_container_form">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucapplicationviewtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        VIEW APLIKASI</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA FINANSIAL 1</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="">
                            Angsuran Pertama
                        </label>
                        <asp:Label ID="lblFirstInstallment" runat="server"></asp:Label>
                    </div>  
                    <div class="form_right">
                        <label class="label">
                            Angsuran Tidak Bayar
                        </label>
                        <asp:Label ID="txtTidakAngsur" runat="server" class="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">
                            Refund Supplier digabung
                        </label>
                            <asp:RadioButtonList ID="rdoGabungRefundSupplier" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single" Enabled="false">
                                <asp:ListItem Text="Ya" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Tidak" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Potong Pencairan Dana
                        </label>
                        <asp:RadioButtonList runat="server" ID="optPotongPencairanDana" RepeatDirection="Horizontal"
                            CssClass="opt_single" Enabled="false">
                            <asp:ListItem Text="Ya" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Tidak" Value="0"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h5>
                            DATA FINANSIAL</h5>
                    </div>
                    <div class="form_right">
                        <h5>
                            TOTAL BAYAR PERTAMA</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            OTR Asset
                        </label>
                        <asp:Label ID="lblOTR" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Uang Muka
                        </label>
                        <asp:Label runat="server" ID="lblUangMuka" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Karoseri
                        </label>
                        <asp:Label runat="server" ID="lblKaroseri" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Administrasi
                        </label>
                        <asp:Label ID="lblAdminFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Total OTR
                        </label>
                        <asp:Label runat="server" ID="lblTotalOTR" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Fidusia
                        </label>
                        <asp:Label runat="server" ID="lblBiayaFidusia" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                        <label class="label_auto">
                            Uang Muka OTR
                        </label>
                        <asp:Label ID="txtDP" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                        <label class="label_auto numberAlign2"> %</label>
                        <asp:Label ID="txtDPPersen" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Biaya Lainnya
                    </label>
                    <asp:Label ID="lblOtherFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
            </div>

            <div class="form_box">
                <div class="form_left">
                        <label class="label_auto">
                            Uang Muka Karoseri
                        </label>
                        <asp:Label ID="txtDPKaroseri" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                        <label class="label_auto numberAlign2"> %</label>
                        <asp:Label ID="txtDPPersenKaroseri" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                      <label>
                            Biaya Provisi
                        </label>
                        <asp:Label ID="lblBiayaProvisi" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
            </div>

            <div class="form_box">
                <div class="form_left" 
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <asp:Label ID="lblAssetInsurance3" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                    <div class="form_right">
                      
                    </div>
            </div>

             <div class="form_box">
                <div class="form_left border_sum">
                    <label class="label_calc2">
                        +
                    </label>
                    <label>
                        Admin Pembiayaan
                    </label>
                    <asp:Label ID="lblAdminKredit" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                    <div class="form_right"> 
                    </div>
            </div>


            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Pokok Hutang (NTF)
                        </label>
                        <asp:Label ID="lblNTF" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>

                    <div class="form_right">
                        <label>
                            Asuransi Tunai
                        </label>
                        <asp:Label ID="lblAssetInsurance32" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left border_sum">
                        <label>Total Margin</label>
                            <label class="label_auto"> Efektif </label>
                            <asp:Label ID="txtEffectiveRate" runat="server" CssClass="label_auto"></asp:Label>
                            <label class="label_auto">  % &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <label class="label_auto"> Flat </label>
                            <asp:Label ID="txtFlatRate" runat="server" CssClass="label_auto"></asp:Label>
                            <label class="label_auto"> % </label>

                           <label class="label_calc2">
                             +
                         </label>
                            <asp:Label ID="lblTotalBunga" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                            
                            
                            
                    </div>
                    <div class="form_right  border_sum">
                        <label class="label_calc2">
                            +
                        </label>
                        <label>
                            Angsuran Pertama
                        </label>
                        <asp:Label ID="lblAngsuranPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                        Penambahan margin dari asuransi
                         <asp:Label ID="lblInsAmountToNPV" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>

                 <div class="form_right">
                        <label>
                            Total
                        </label>
                        <asp:Label ID="lblTotalBayarPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>   
            </div>
            <div class="form_box">
                <div>               
                     <div class="form_left">
                        <label>
                            Jumlah A/R (Nilai Kontrak)</label>
                            <asp:Label ID="lblNilaiKontrak" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                     </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                <label class="">
                        Frekuensi Angsuran
                    </label>
                    <asp:Label ID="lblPaymentFreq" runat="server" CssClass=""></asp:Label>
                    <asp:DropDownList ID="cboPaymentFreq" runat="server" AutoPostBack="false" CssClass="numberAlign2" Width="158px" visible="false" >
                                <asp:ListItem Value="1">Monthly</asp:ListItem>
                                <asp:ListItem Value="2">Quaterly</asp:ListItem>
                                <asp:ListItem Value="3">Semi Annually</asp:ListItem>
                                <asp:ListItem Value="6">Annually</asp:ListItem>
                            </asp:DropDownList>
                </div>
                <div class="form_right">
                    &nbsp;        
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Jangka Waktu
                    </label>
                    <asp:Label ID="txtNumInst" runat="server"></asp:Label>
                </div>
               
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left box_important">
                        <asp:Label ID="lblInstallment" runat="server" CssClass="label">Angsuran Per Frekuensi </asp:Label>
                        <asp:Label ID="txtInstAmt" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>                   
                    <div class="form_right">
                        <label class="">
                            Grace Period
                        </label>
                        <asp:Label ID="txtGracePeriod" runat="server" CssClass="regular_text"></asp:Label> &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblGracePeriod" runat="server" CssClass="regular_text"></asp:Label>
                    </div>
                </div>
            </div>

                <div class="form_box">
                    <div>
                        <div class="form_left" style="background-color:#FCFCCC">
                            <label>Biaya Provisi</label>
                            <asp:Label ID="txtBiayaProvisi" runat="server" CssClass="numberAlign2 reguler_text"></asp:Label>
                        </div>
                        <div class="form_right">
                  
                        </div>
                    </div>
                </div>

                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            Amortisasi</h4>
                    </div>
                </div>
                <div class="form_box" >
                <div>
                    <div class="form_single">
                        <asp:DataGrid id="dtgViewInstallment" runat="server" CssClass="grid_general" Width="100%" AllowSorting="True"
						    AutoGenerateColumns="False" BorderWidth="0px" cellpadding="3" cellspacing="1">
						    <ItemStyle HorizontalAlign="Right" CssClass="item_grid number_align"></ItemStyle>
						    <HeaderStyle HorizontalAlign="Right" CssClass="th"></HeaderStyle>
						    <Columns>
                                <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InstallmentAmount" HeaderText="ANGSURAN" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="PrincipalAmount" HeaderText="POKOK" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestAmount" HeaderText="MARGIN" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OutstandingPrincipal" HeaderText="SISA POKOK" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OutstandingInterest" HeaderText="SISA MARGIN" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                            </Columns>
					    </asp:DataGrid>
                    </div>
                </div>
            </div>
    
            <div class="tab_container_form_space"><br />
            </div>
            <div class="tab_container_button">
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    </form>
</body>
</html>
