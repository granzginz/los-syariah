﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewApplication.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewApplication" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewApplication</title>
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <link rel="stylesheet" href="../../../../Include/General.css" type="text/css"  />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - APLIKASI
            </h3>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
