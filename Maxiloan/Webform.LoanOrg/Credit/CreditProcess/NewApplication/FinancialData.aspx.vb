﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class FinancialData
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New FinancialDataController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Controls"
    Protected WithEvents Table1 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents Rangevalidator1 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents rfvGo As System.Web.UI.WebControls.RequiredFieldValidator    
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            If CheckForm(Me.Loginid, "FinancialData", Me.AppId) Then
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    txtGoPage.Text = "1"
                    If Request("cond") <> "" Then
                        Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' and " + Request("cond")
                    Else
                        Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                    End If
                    Me.Sort = "Agreement.ApplicationID ASC"

                    BindGrid(Me.CmdWhere)
                Else
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub
#Region "BindGrid"
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.FinancialData
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = m_controller.GetFinancialData(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))

        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If

        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If CheckFeature(Me.Loginid, "FinancialData", "Add", Me.AppId) Then
            If sessioninvalid() Then
                Exit Sub
            End If
        End If
        If e.CommandName = "Action" Then
            Dim oCustomer As Parameter.Customer
            Dim AppID As String = CType(e.Item.Cells(1).FindControl("lnkApplicationID"), HyperLink).Text
            Dim CustName As String = CType(e.Item.Cells(2).FindControl("lnkCustName"), HyperLink).Text
            Dim CustomerID As String = e.Item.Cells(5).Text.Trim
            'Dim EmployeeID As String = e.Item.Cells(6).Text.Trim
            Dim InterestType As String = e.Item.Cells(7).Text.Trim
            Dim InstallmentScheme As String = e.Item.Cells(8).Text.Trim
            Dim cookie As HttpCookie = Request.Cookies("Financial")

            If Not cookie Is Nothing Then
                cookie.Values("id") = AppID
                cookie.Values("Custid") = CustomerID
                cookie.Values("name") = CustName
                'cookie.Values("EmployeeID") = EmployeeID
                cookie.Values("InterestType") = InterestType
                cookie.Values("InstallmentScheme") = InstallmentScheme
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("Financial")
                cookieNew.Values.Add("id", AppID)
                cookieNew.Values.Add("Custid", CustomerID)
                cookieNew.Values.Add("name", CustName)
                'cookieNew.Values.Add("EmployeeID", EmployeeID)
                cookieNew.Values.Add("InterestType", InterestType)
                cookieNew.Values.Add("InstallmentScheme", InstallmentScheme)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("FinancialData_002.aspx")
        End If
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' "
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            If cboSearch.SelectedIndex = 0 Then
                Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' and " + cboSearch.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
            Else
                Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' and " + cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
            End If
        Else
            Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' "
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkApp As HyperLink
        If e.Item.ItemIndex >= 0 Then
            lnkApp = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            lnkApp.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApp.Text) & "')"
            'lnkApp.Attributes.Add("OnClick", "return OpenWin('" & lnkApp.Text & "','AccAcq');")

            Dim lnkCust As HyperLink
            lnkCust = CType(e.Item.FindControl("lnkCustName"), HyperLink)
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(e.Item.Cells(5).Text) & "')"

            'lnkCust.Attributes.Add("OnClick", "return OpenCust('" & e.Item.Cells(5).Text & "','AccAcq');")
            Dim lnkPO As HyperLink
            lnkPO = CType(e.Item.FindControl("lnkProdOff"), HyperLink)
            lnkPO.NavigateUrl = "javascript:OpenWinProductOffering('" & e.Item.Cells(9).Text & "'," & e.Item.Cells(11).Text & ",'" & e.Item.Cells(10).Text & "','AccAcq')"
            'lnkPO.Attributes.Add("OnClick", "return OpenWinProductOfferingBranchView('" & e.Item.Cells(9).Text & "','" & e.Item.Cells(10).Text & "','" & e.Item.Cells(11).Text & "','AccAcq');")


            Dim lnkAO As HyperLink
            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)
            lnkAO.NavigateUrl = "javascript:OpenAO('AccAcq', '" & e.Item.Cells(11).Text & "','" & e.Item.Cells(6).Text & "')"
            'lnkAO.Attributes.Add("OnClick", "return OpenAO('" & e.Item.Cells(11).Text & "','" & e.Item.Cells(6).Text & "','AccAcq');")
        End If
    End Sub

End Class