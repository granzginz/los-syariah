﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewFinancialData
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New FinancialDataController
#End Region
#Region "Property"
    Property App() As String
        Get
            Return viewstate("App").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("App") = Value
        End Set
    End Property
    Property PageSource() As String
        Get
            Return viewstate("PageSource").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("PageSource") = Value
        End Set
    End Property
    Property style() As String
        Get
            Return viewstate("style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.App = Request("ApplicationID").ToString
            Me.style = Request("Style").ToString
            Me.PageSource = Request("pageSource").ToString
            btnClose.Attributes.Add("onclick", "return Close()")
            btnBack.Visible = True
            If Me.PageSource = "" Then
                btnBack.Visible = False
            End If
            Bindgrid()
        End If
        lblMessage.Text = ""
    End Sub
    Sub Bindgrid()
        Dim oFinancialData As New Parameter.FinancialData
        Dim oData As New DataTable
        Dim oDataamortization As New DataTable
        Dim int As Integer
        oFinancialData.strConnection = GetConnectionString
        oFinancialData.AppID = Me.App

        oFinancialData = m_controller.GetViewFinancialData(oFinancialData)

        If Not oFinancialData Is Nothing Then
            oData = oFinancialData.Data1
            oDataamortization = oFinancialData.data2
        End If
        If oFinancialData.Output <> "" Then
            lblMessage.Text = oFinancialData.Output
            Exit Sub
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
        dtgAmortization.DataSource = oDataamortization.DefaultView
        dtgAmortization.DataBind()
        int = dtgAmortization.Items.Count - 1
        dtgAmortization.Items(0).Font.Bold = True
        dtgAmortization.Items(int).Font.Bold = True
    End Sub
    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)
        lblAppID.Text = Me.App
        lblCustName.Text = oRow.Item(0).ToString.Trim
        lblInterestType.Text = oRow.Item(1).ToString.Trim
        lblInstScheme.Text = oRow.Item(2).ToString.Trim
        lblInsPremium.Text = FormatNumber(oRow.Item(3), 2)
        lblInsAdv.Text = FormatNumber(oRow.Item(4), 2)
        lblInsCapitalized.Text = FormatNumber(oRow.Item(5), 2)
        lblOTR.Text = FormatNumber(oRow.Item(6), 2)
        lblNTF.Text = FormatNumber(oRow.Item(7), 2)
        lblEffectiveRate.Text = FormatNumber(oRow.Item(8), 5)
        lblSuppRate.Text = FormatNumber(oRow.Item(9), 5)
        lblPymtFreq.Text = oRow.Item(10).ToString.Trim
        lblNumInst.Text = oRow.Item(11).ToString.Trim
        lblInstAmt.Text = FormatNumber(oRow.Item(12), 2)
        lblGrace1.Text = oRow.Item(13).ToString.Trim
        lblGrace2.Text = oRow.Item(14).ToString.Trim
        lblDP.Text = FormatNumber(oRow.Item(15), 2)
        lblFlatRate.Text = FormatNumber(oRow.Item(16), 5)
        'Format(Today, "yyyyMMdd")
        lblFirstInst.Text = oRow.Item(17).ToString.Trim
        lblGross.Text = FormatNumber(oRow.Item(18), 5)
        If CDec(oRow.Item(8)) < CDec(oRow.Item(9)) Then
            lblLabel.Text = "Amount Subsidy"
            lblDiff.Text = FormatNumber(oRow.Item(19), 2)
        ElseIf CDec(oRow.Item(8)) > CDec(oRow.Item(9)) Then
            lblLabel.Text = "Refund Interest"
            lblDiff.Text = FormatNumber(oRow.Item(19), 2)
        Else
            lblLabel.Text = ""
            lblDiff.Text = ""
        End If
        lblTenor.Text = oRow.Item(20).ToString.Trim
    End Sub

    Private Sub imbBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        If Me.PageSource = "App" Then
            Response.Redirect("ViewApplication.aspx?ApplicationID=" & Me.App.Trim & "&Style=" & Me.style & "")
        End If
    End Sub

End Class