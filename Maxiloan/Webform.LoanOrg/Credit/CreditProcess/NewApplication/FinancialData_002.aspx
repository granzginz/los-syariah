﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FinancialData_002.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.FinancialData_002" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTab.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucSupplierEmployeeGrid.ascx"
    TagName="ucSupplierEmployeeGrid" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../Webform.UserController/ucFlagTransaksi.ascx" TagName="ucFlagTransaksi"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Financial Data</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Lookup.css" type="text/css" />
    
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../../../js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        /*function DeleteRow(grid, idx) {
        var sgrid = document.getElementById(grid);
        sgrid.deleteRow(idx);
        recalculate();
        }*/
        function calculateNPV(id) {
            var ntf = document.getElementById('lblNilaiNTFOTR').value;

            var flatRate = document.getElementById('txtFlatRate').value;
            var TenorYear = document.getElementById('lblNilaiTenor').value;

            //TenorYear = parseInt(TenorYear) / 12;
            if (id == "txtRefundBungaN_txtNumber") {  // ini kalo yang diisi nilai nya
                var nilAmount = document.getElementById('txtRefundBungaN_txtNumber').value;

                var x = (1 + (flatRate / 100))
                var result = ((parseInt(nilAmount.replace(/\s*,\s*/g, '')) * x) / TenorYear / parseInt(ntf.replace(/\s*,\s*/g, ''))) * 100;
                  
                var r_formated = parseFloat(Math.floor(result * 100) / 100);
                if (!r_formated.length) {
                    document.getElementById('ucRefundBunga_txtNumber').value = number_format(r_formated, 2);
                } else {
                    document.getElementById('ucRefundBunga_txtNumber').value = r_formated;
                }
                document.getElementById('txtRefundBungaN_txtNumber').value = nilAmount;

            } else {  // ini kalo yang diisi persennya
                var refundRate = document.getElementById('ucRefundBunga_txtNumber').value;

                var npv = (parseInt(ntf.replace(/\s*,\s*/g, '')) * (parseFloat(refundRate) / 100) * parseInt(TenorYear)) / (1 + ((parseFloat(flatRate) / 100)));

                npv = Math.round(npv, 0) / 1000;
                npv = Math.ceil(npv) * 1000;
                document.getElementById('txtRefundBungaN_txtNumber').value = number_format(npv, 0);
            }

        }
        //       
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }

        function NumInst(Tenor, Num, txtNumInst) {
            document.forms[0].txtNumInst.value = Tenor / Num;
        }
        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
        function GracePeriod(txtGracePeriod, cboGracePeriod, Value) {
            if (Value == 'AR') {
                document.forms[0].txtGracePeriod.value = '0'
                if (document.forms[0].txtGracePeriod.disabled == false) {
                    document.forms[0].txtGracePeriod.disabled = false;
                    document.forms[0].cboGracePeriod.disabled = false;
                }
            }
            else {
                document.forms[0].txtGracePeriod.value = '0'
                document.forms[0].txtGracePeriod.disabled = true;
                document.forms[0].cboGracePeriod.disabled = true;
            }
        }
        function recalculateAngs() {

            $('#lblAngsuranPertama').html(number_format($('#txtInstAmt_txtNumber').val()));

            var lblUangMuka = $('#lblUangMuka').html();
            var lblAdminFee = $('#lblAdminFee').html();
            var lblBiayaFidusia = $('#lblBiayaFidusia').html();
            var lblBiayaPolis = $('#lblBiayaPolis').html();
            var lblOtherFee = $('#lblOtherFee').html();
            var lblAssetInsurance32 = $('#lblAssetInsurance32').html();
            var lblAngsuranPertama = $('#lblAngsuranPertama').html();
            var lblBiayaProvisi = $('#lblBiayaProvisi').html();

            var xtotal = parseInt(lblUangMuka.replace(/\s*,\s*/g, '')) +
                         parseInt(lblAdminFee.replace(/\s*,\s*/g, '')) +
                         parseInt(lblBiayaFidusia.replace(/\s*,\s*/g, '')) +
                         parseInt(lblBiayaPolis.replace(/\s*,\s*/g, '')) +
                         parseInt(lblOtherFee.replace(/\s*,\s*/g, '')) +
                         parseInt(lblAssetInsurance32.replace(/\s*,\s*/g, '')) +
                         parseInt(lblAngsuranPertama.replace(/\s*,\s*/g, '')) +
                         parseInt(lblBiayaProvisi.replace(/\s*,\s*/g, ''));

            $('#lblTotalBayarPertama').html(number_format(xtotal));
        }

       
        function recalculateARGross() {
            var totalbunga = $('#lblTotalBunga_txtNumber').val();
            var ntf = $('#lblNTF').html();
            var nilaikontrak = parseInt(totalbunga.replace(/\s*,\s*/g, '')) +
                                parseInt(ntf.replace(/\s*,\s*/g, ''));
            $('#lblNilaiKontrak_txtNumber').val(number_format(nilaikontrak));
        }
        function hitungtxt(txtidA, txtidB, txtidS, toPersen) {
            var status = true;
            var A = $('#' + txtidA).val();
            var S;
            var N;
            var M;
            if ($('#' + txtidS).html() == "") {
                S = $('#' + txtidS).val();
            } else {
                S = $('#' + txtidS).html();
            }

            A = A.replace(/\s*,\s*/g, '');
            S = S.replace(/\s*,\s*/g, '');

            if (txtidA == "txtRefundBungaN_txtNumber" || txtidA == "ucRefundBunga_txtNumber") {
                if (txtidA == "ucRefundBunga_txtNumber") {
                    M = $('#MaxRefundBungaP').html();
                    if (A > parseInt(M)) {
                        status = false;
                        alert('Tidak Boleh lebih dari ' + M);
                    }
                } else {
                    M = $('#MaxRefundBungaN').html();
                    if (A > parseInt(M)) {
                        status = false;
                        alert('Tidak Boleh lebih dari ' + M);
                    }
                }
            }



            if (status == true) {
                if (toPersen == true) {
                    N = parseInt(A) / parseInt(S) * 100;
                    $('#' + txtidB).val(number_format(parseFloat((N * 10) / 10), 2));
                } else {
                    N = parseFloat(S) * (A / 100);
                    //$('#' + txtidA).val(number_format(A, 2));
                    $('#' + txtidB).val(number_format(N, 0));
                }
            } else {
                $('#' + txtidA).val(number_format(0, 2));
                $('#' + txtidB).val(number_format(0, 2));
            }


            if (txtidA == "txtBiayaProvisiN_txtNumber" || txtidA == "txtBiayaProvisi_txtNumber") {
                $('#lblBiayaProvisi').html(number_format($('#txtBiayaProvisiN_txtNumber').val(), 0));
                $('#xlblBiayaProvisiN').html(number_format($('#txtBiayaProvisiN_txtNumber').val(), 0));
                recalculateAngs();
            }
            if (txtidA == "txtRefundPremiN_txtNumber" || txtidA == "txtRefundPremi_txtNumber") {
                var selisihPremi = $('#lblSelisihPremi').html();
                var refundPemi = $('#txtRefundPremiN_txtNumber').val();
                var pendapatan = parseInt(selisihPremi.replace(/\s*,\s*/g, '')) - parseInt(refundPemi.replace(/\s*,\s*/g, ''));
                $('#lblPendapatanPremiN_txtNumber').val(number_format(pendapatan,0));
            }

        }
     
        
    </script>
</head>
<body>
    
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="UpdateProgressSave" runat="server" AssociatedUpdatePanelID="uPanelSave">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div>
                    <div class="form_left">
                        <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                    </div>
                    <div class="form_right">
                        <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ENTRI FINANSIAL</h4>
                </div>
            </div>
            <%--tidak perlu ditampilkan--%>
            <div class="form_box" style="display: none;">
                <div>
                    <div class="form_left">
                        <label>
                            Jenis Margin
                        </label>
                        <asp:Label ID="lblInterestType" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Skema Angsuran
                        </label>
                        <asp:Label ID="lblInstScheme" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_box_hide">
                        <label class="label_req">
                            Pola Angsuran
                        </label>
                        <asp:DropDownList ID="cboPolaAngsuran" runat="server">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="FDPK">FDPK</asp:ListItem>
                            <asp:ListItem Value="FLAT" Selected="True">FLAT</asp:ListItem>
                            <asp:ListItem Value="NOTARIIL">NOTARIIL</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvPolaAngsuran" runat="server" Display="Dynamic"
                            ControlToValidate="cboPolaAngsuran" ErrorMessage="Harap pilih pola angsuran!"
                            InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div> 
                    <div class="form_left">
                        <label class="label_req">
                            Angsuran Pertama
                        </label>
                        <asp:DropDownList ID="cboFirstInstallment" runat="server" AutoPostBack="true">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="AD">Advance</asp:ListItem>
                            <asp:ListItem Value="AR">Arrear</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ControlToValidate="cboFirstInstallment" ErrorMessage="Harap Pilih Angsuran Pertama"
                            InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>  
                    <div class="form_right">
                        <label class="label">
                            Angsuran Tidak Bayar
                        </label>
                        <uc4:ucnumberformat id="txtTidakAngsur" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <%--tidak perlu ditampilkan--%>
                    <div class="form_box_hide">
                        <label>
                            Grace Period Denda Keterlambatan
                        </label>
                        <asp:TextBox ID="txtGracePeriod2" runat="server" MaxLength="2" CssClass="numberAlign2 regular_text">0</asp:TextBox>
                        <asp:RangeValidator ID="rvGracePeriodLC" runat="server" ControlToValidate="txtGracePeriod2"
                            Type="Double" CssClass="validator_general"></asp:RangeValidator>
                    </div>
                    <div class="form_left">
                        <label class="label_general">
                            Refund Supplier digabung
                        </label>
                            <asp:RadioButtonList ID="rdoGabungRefundSupplier" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                                <asp:ListItem Text="Ya" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Tidak" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Potong Pencairan Dana
                        </label>
                        <asp:RadioButtonList runat="server" ID="optPotongPencairanDana" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Text="Ya" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Tidak" Value="0"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_box_hide">
                <div>
                    <div class="form_left">
                        <label>
                            Tanggal Angsuran I
                        </label>
                        <uc8:ucdatece runat="server" id="txtTglAngsuranI"></uc8:ucdatece>                        
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Angsuran Bayar di Dealer
                        </label>
                        <asp:RadioButtonList runat="server" ID="optBayarDealer" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Text="Ya" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Tidak" Value="0" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    
                </div>
            </div>
            <%--hide karena selalu monthly--%>
            <div class="form_box" style="display: none">
                <div>
                    <div class="form_left">
                        <label>
                            Cara Angsuran
                        </label>
                        <asp:DropDownList ID="cboPaymentFreq" runat="server">
                            <asp:ListItem Value="1" Selected="True">Monthly</asp:ListItem>
                            <asp:ListItem Value="2">Bimonthly</asp:ListItem>
                            <asp:ListItem Value="3">Quarterly</asp:ListItem>
                            <asp:ListItem Value="6">Semi Annualy</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h5>
                            DATA FINANSIAL</h5>
                    </div>
                    <div class="form_right">
                        <h5>
                            TOTAL BAYAR PERTAMA</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            OTR Asset
                        </label>
                        <asp:Label ID="lblOTR" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Uang Muka
                        </label>
                        <asp:Label runat="server" ID="lblUangMuka" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <%--tidak perlu dtampilkan--%>
                    <div class="form_right" style="display: none">
                        <label>
                            Total OTR
                        </label>
                        <asp:Label ID="lblOTRSupplier" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Karoseri
                        </label>
                        <asp:Label runat="server" ID="lblKaroseri" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Administrasi
                        </label>
                        <asp:Label ID="lblAdminFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                        <asp:Label ID="lblIsAdminFeeCredit" runat="server" Visible="false"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Total OTR
                        </label>
                        <asp:Label runat="server" ID="lblTotalOTR" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Fidusia
                        </label>
                        <asp:Label runat="server" ID="lblBiayaFidusia" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    
                        <label class="label_auto">
                            Uang Muka
                        </label>
                    <uc4:ucnumberformat id="txtDP" runat="server" TextCssClass="numberAlign2" />    
                        <label class="label_auto numberAlign2"> %
                        </label>
                    <uc4:ucnumberformat id="txtDPPersen" runat="server" TextCssClass="numberAlign2" />
                </div>
                <div class="form_right">
                    <label>
                        Biaya Polis
                    </label>
                    <asp:Label ID="lblBiayaPolis" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left border_sum">
                    <label class="label_calc2">
                        +
                    </label>
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <asp:Label ID="lblAssetInsurance3" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    
                </div>
                
                <div class="form_right">
                    <label>
                        Biaya Lainnya
                    </label>
                    <asp:Label ID="lblOtherFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
            </div>
            <%--tidak perlu dtampilkan--%>
            <div class="form_box_hide">
                <div>
                    <div class="form_right" style="display: none;">
                        <label class="label_req">
                            Uang Muka
                        </label>
                        <asp:Label ID="lblDPPersenSupplier" runat="server"></asp:Label>
                        <asp:TextBox runat="server" ID="txtDPSupplier" AutoPostBack="True"></asp:TextBox>
                    </div>
                </div>
            </div>
            <%--tidak perlu dtampilkan--%>
            <div class="form_box_hide">
                <div class="form_right" style="display: none;">
                    <label>
                        Biaya Administrasi
                    </label>
                    <asp:Label ID="lblAdminFee2" runat="server"></asp:Label>
                    <asp:Label ID="lblIsAdminFeeCredit2" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Pokok Hutang (NTF)
                        </label>
                        <asp:Label ID="lblNTF" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>

                    <div class="form_right">
                        <label>
                            Biaya Provisi
                        </label>
                        <asp:Label ID="lblBiayaProvisi" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>

                    <%--tidak perlu dtampilkan--%>
                    <div class="form_right" style="display: none;">
                        <label>
                            Pokok Hutang (NTF)
                        </label>
                        <asp:Label ID="lblNTFSupplier" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left border_sum">
                        <label>Total Margin</label>
                           <label class="label_calc2">
                             +
                         </label>

                           
                            <uc4:ucnumberformat id="lblTotalBunga" runat="server" textCssClass="numberAlign2 readonly_text" />
                            
                            <label class="label_auto numberAlign2"> % </label>
                            <asp:TextBox ID="txtEffectiveRate" runat="server" MaxLength="15" CssClass="numberAlign2 smaller_text readonly_text" ReadOnly="true"></asp:TextBox>
                            
                            
                            <label class="label_auto numberAlign2"> Efektif </label>

                            <label class="label_auto numberAlign2"> % </label>
                            <asp:TextBox runat="server" ID="txtFlatRate" CssClass="numberAlign2 smaller_text" AutoPostBack="true"></asp:TextBox>
                            
                            <label class="label_auto numberAlign2"> Flat </label>
                            
                            
                                
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                            ControlToValidate="txtEffectiveRate" ErrorMessage="Harap isi Margin Effective"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RVPOEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                            Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RangeValidator ID="RVPBEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                            Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RangeValidator ID="RVPEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                            Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            ControlToValidate="txtFlatRate" ErrorMessage="Harap isi Margin Flat!" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" Display="Dynamic" ControlToValidate="txtFlatRate"
                            Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"
                            ErrorMessage="Margin flat tidak valid!"></asp:RangeValidator>
                    </div>
                    </div>
                    <div class="form_right">
                        <label>
                            Asuransi Tunai
                        </label>
                        <asp:Label ID="lblAssetInsurance32" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    
                </div>
            <div class="form_box">
                <div>               
                     <div class="form_left">
                        <label>
                            Jumlah A/R (Nilai Kontrak)</label>
                           <uc4:ucnumberformat id="lblNilaiKontrak" runat="server" textCssClass="numberAlign2 readonly_text" />
                    </div>
                    <div class="form_right  border_sum">
                        <label class="label_calc2">
                            +
                        </label>
                        <label>
                            Angsuran Pertama
                        </label>
                        <asp:Label ID="lblAngsuranPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                        Penambahan margin dari asuransi
                         <asp:Label ID="lblInsAmountToNPV" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>

                 <div class="form_right">
                        <label>
                            Total
                        </label>
                        <asp:Label ID="lblTotalBayarPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>   
                    
                <%--tidak perlu dtampilkan--%>
                <div class="form_right" style="display: none;">
                    <label class="label_req">
                        Effective
                    </label>
                    <asp:TextBox ID="txtEffectiveRateSupplier" runat="server" MaxLength="15"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Jangka Waktu
                    </label>
                    <%--<asp:TextBox ID="txtNumInst" runat="server"></asp:TextBox>--%>
                    <uc4:ucnumberformat id="txtNumInst" runat="server"  TextCssClass="numberAlign2 smaller_text"/>
                    <div class="form_box_hide">
                        Tenor
                        <asp:Label ID="lblTenor" runat="server"></asp:Label>
                    </div>
                </div>
                 <div class="form_right"  style="background-color:#f9f9f9">
                        <h5>
                            PREMI ASURANSI</h5>
                    </div>
                

                <%--tidak perlu dtampilkan--%>
                <div class="form_right" style="display: none;">
                    <label>
                        AR Gross (Nilai Kontrak)
                    </label>
                    <asp:Label runat="server" ID="lblNilaiKontrakSupplier"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left box_important">
                        <asp:Label ID="lblInstallment" runat="server" CssClass="label label_req">Angsuran Per bulan</asp:Label>
                        <uc4:ucnumberformat id="txtInstAmt" runat="server" TextCssClass="numberAlign2" />
                    </div>
                    <%--tidak perlu dtampilkan--%>
                    <div class="form_box_hide">
                        <asp:Label ID="lblInstallmentSupplier" runat="server" CssClass="label">Jumlah Angsuran</asp:Label>
                        <asp:TextBox ID="txtInstAmtSupplier" runat="server" MaxLength="15">0</asp:TextBox>
                        <asp:RangeValidator ID="RVInstAmtSupplier" runat="server" ControlToValidate="txtInstAmtSupplier"
                            Type="Double" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                    </div>
                   
                    <div class="form_right">
                        <label>
                            Premi Asuransi Konsumen
                        </label>
                        <asp:Label runat="server" ID="lblPremiAsuransiGross" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
                <%--tidak perlu dtampilkan--%>
                <div class="form_right" style="display: none;">
                    <label class="label_req">
                        Jangka Waktu
                    </label>
                    <asp:TextBox ID="txtNumInstSupplier" runat="server"></asp:TextBox>Tenor
                    <asp:Label ID="lblTenorSupplier" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box_hide">
                <div>
                    <div class="form_left">
                        <label>
                            Margin Flat Per Bulan</label><asp:Label ID="lblFlatRate" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="lblFlatRateFormatted" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                        <label class="label_auto">
                            % per</label>
                        <asp:Label runat="server" ID="lblFlatRateTenor"></asp:Label>
                        <label class="label_auto">
                            bulan</label>
                    </div>
                    <%--tidak perlu dtampilkan--%>
                    <div class="form_right" style="display: none;">
                        <label>
                            Margin Flat
                        </label>
                        <asp:Label ID="lblFlatRateSupplier" runat="server"></asp:Label>
                        <label class="label_auto">
                            % per</label>
                        <asp:Label runat="server" ID="lblFlatRateTenorSupplier"></asp:Label>
                        <label class="label_auto">
                            bulan</label>
                    </div>
                </div>
            </div>
            <%--tidak perlu dtampilkan--%>
            <div class="form_box" style="display: none;">
                <div>
                    <div class="form_left">
                    </div>
                    <div class="form_right">
                        <label>
                            Rate Supplier
                        </label>
                        <asp:Label runat="server" ID="lblSupplierRate"></asp:Label>
                    </div>
                </div>
            </div>

            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#f9f9f9">
                        <h5>Biaya Provisi</h5>
                    </div>
                    <div class="form_right border_sum">
                        <label class="label_calc2">
                            -
                        </label>
                        <label>
                            Premi Asuransi Maskapai
                        </label>
                        <asp:Label runat="server" ID="lblPremiAsuransiNet" CssClass="numberAlign2 regular_text"></asp:Label>
                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#FCFCCC">
                        <label>Biaya Provisi</label>
                        <uc4:ucnumberformat runat="server" id="txtBiayaProvisiN" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitungtxt(this.id,'txtBiayaProvisi_txtNumber','lblNTF',true)"></uc4:ucnumberformat>
                        <label class="label_auto numberAlign2"> % </label>
                        <uc4:ucnumberformat runat="server" id="txtBiayaProvisi" TextCssClass="numberAlign2 smaller_text" OnClientChange="hitungtxt(this.id,'txtBiayaProvisiN_txtNumber','lblNTF',false)"></uc4:ucnumberformat>
                    </div>
                    <div class="form_right">
                        <label>
                            Selisih Premi</label>
                        <asp:Label runat="server" ID="lblSelisihPremi" CssClass="numberAlign2 regular_text" style="font-weight:bold"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#FCFCCC">
                        <label>Refund Biaya Provisi</label>
                        <uc4:ucnumberformat runat="server" id="txtRefundBiayaProvisiN" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitungtxt(this.id,'txtRefundBiayaProvisi_txtNumber','txtBiayaProvisiN_txtNumber',true)"></uc4:ucnumberformat>
                        <label class="label_auto numberAlign2"> % </label>
                        <uc4:ucnumberformat runat="server" id="txtRefundBiayaProvisi" TextCssClass="numberAlign2 smaller_text" OnClientChange="hitungtxt(this.id,'txtRefundBiayaProvisiN_txtNumber','txtBiayaProvisiN_txtNumber',false)"></uc4:ucnumberformat>
                    </div>
                    <div class="form_right  border_sum"  style="background-color:#FCFCCC">
                        <label class="label_calc2">
                            -
                        </label>
                        <label>Refund Premi</label>
                        <uc4:ucnumberformat runat="server" id="txtRefundPremiN" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitungtxt(this.id,'txtRefundPremi_txtNumber','lblPremiAsuransiGross',true)"></uc4:ucnumberformat>
                        <label class="label_auto numberAlign2"> % </label>
                        <uc4:ucnumberformat runat="server" id="txtRefundPremi" TextCssClass="numberAlign2 smaller_text" OnClientChange="hitungtxt(this.id,'txtRefundPremiN_txtNumber','lblPremiAsuransiGross',false)"></uc4:ucnumberformat>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left"  style="background-color:#f9f9f9"><h5>Refund Margin</h5>
                        <div style="display:none">
                        <asp:textbox ID="lblNilaiNTFOTR" runat="server" />
                        <asp:textbox ID="lblNilaiTenor" runat="server" />
                        <asp:textbox ID="lblNilaiRefundBunga" runat="server" />
                        </div>
                    </div>
                    <div class="form_right" style="background-color:#FCFCCC">
                        <label><b>Pendapatan / Pengeluaran Asuransi</b></label>
                        <uc4:ucnumberformat id="lblPendapatanPremiN" isReadOnly="true" runat="server" TextCssClass="numberAlign2 reguler_text" />
                        <div style="display:none">
                        <label class="label_auto numberAlign2"> % </label>
                        <uc4:ucnumberformat ID="lblPendapatanPremi" isReadOnly="true" runat="server" TextCssClass="numberAlign2 smaller_text" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#FCFCCC">
                        <label>Refund Margin</label>
                        <uc4:ucnumberformat runat="server" id="txtRefundBungaN" TextCssClass="numberAlign2 reguler_text" OnClientChange="calculateNPV(this.id);" ></uc4:ucnumberformat>
                        <label class="label_auto numberAlign2"> % </label>
                        <uc4:ucnumberformat runat="server" id="ucRefundBunga" TextCssClass="numberAlign2 smaller_text" OnClientChange="calculateNPV(this.id);"></uc4:ucnumberformat>
                    </div>
                    <div class="form_right" style="background-color:#f9f9f9">
                        <h5>REFUND BIAYA ADMIN</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#f9f9f9">
                        <h5>SUBSIDI DEALER</h5>
                    </div>
                    <div class="form_right">
                        <label>Biaya Admin</label>
                        <asp:Label ID="lblRefundAdmin" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Margin</label>
                        <uc4:ucnumberformat runat="server" id="txtSubsidiBungaDealer" TextCssClass="numberAlign2"></uc4:ucnumberformat>
                    </div>
                    <div class="form_right"  style="background-color:#FCFCCC">
                        <label>Refund Biaya Admin</label>
                        <uc4:ucnumberformat runat="server" id="txtRefundAdminN" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitungtxt(this.id,'ucRefundAdmin_txtNumber','lblRefundAdmin',true)"></uc4:ucnumberformat>
                        <label class="label_auto numberAlign2"> % </label>
                        <uc4:ucnumberformat runat="server" id="ucRefundAdmin" TextCssClass="numberAlign2 smaller_text" OnClientChange="hitungtxt(this.id,'txtRefundAdminN_txtNumber','lblRefundAdmin',false)"></uc4:ucnumberformat>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Angsuran</label>
                            <uc4:ucnumberformat runat="server" id="txtSubsidiAngsuran" TextCssClass="numberAlign2"></uc4:ucnumberformat>
                    </div>
                    <div class="form_right">&nbsp;</div>
                </div>
            </div>

            <div style="display:none">
                <asp:TextBox ID="txtCummulative" runat="server" MaxLength="15" Columns="4">0</asp:TextBox>
                <asp:TextBox ID="txtGracePeriod" runat="server" MaxLength="2" Columns="4" ReadOnly="True">0</asp:TextBox>
                <asp:TextBox ID="txtAmount" runat="server" MaxLength="15"></asp:TextBox>
                <asp:TextBox ID="txtStep" runat="server" MaxLength="15" Columns="4">0</asp:TextBox>
                <asp:DropDownList ID="cboGracePeriod" runat="server">
                            <asp:ListItem Value="I">Interest Only</asp:ListItem>
                            <asp:ListItem Value="R">Roll Over</asp:ListItem>
                        </asp:DropDownList>
                <asp:DropDownList ID="cboFloatingPeriod" runat="server">
                                <asp:ListItem Value="Select One">Select One</asp:ListItem>
                                <asp:ListItem Value="M">Monthly</asp:ListItem>
                                <asp:ListItem Value="Q">Quaterly</asp:ListItem>
                                <asp:ListItem Value="S">Semi Annually</asp:ListItem>
                                <asp:ListItem Value="A">Annually</asp:ListItem>
                            </asp:DropDownList>
                <asp:TextBox ID="txtGrossYieldRate" runat="server" MaxLength="15" ReadOnly="True"
                                Width="16px" Visible="False"></asp:TextBox>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
   
    

    <asp:UpdatePanel runat="server" ID="upPencairan" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlPencairan">
                <div class="form_box_title">
                    <div class="form_left">
                        <h5>
                            PERHITUNGAN MARGIN NET</h5>
                    </div>
                    <div class="form_right">
                        <h5>
                            TOTAL PENCAIRAN</h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_left">
                        <asp:GridView runat="server" ID="gvBungaNet" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc1" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Margin Nett</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Nilai" DataFormatString="{0:N0}" HeaderStyle-CssClass="th_right"
                                    ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblRate" Text='<%# math.round(Container.DataItem("Rate"),2) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalRate"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="BungaNett" />
                            </Columns>
                        </asp:GridView>
                        <asp:GridView runat="server" ID="gvNetRateStatus" AllowPaging="false" AutoGenerateColumns="false"
                            CssClass="grid_general" ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <label>
                                            Status Rate</label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RateTypeResult" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="form_right">
                        <asp:GridView runat="server" ID="gvPencairan" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc2" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Total Pencairan</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                                    FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblNilai" Text='<%# formatnumber(Container.DataItem("Nilai"),0) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalPencairan"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FlagCustomer" Visible="false" />
                                <asp:BoundField DataField="FlagSupplier" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="uPanelSave" runat="server">
        <ContentTemplate>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <div id="btnSaveEnable" class="small button gray" style="display:none">Save</div>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>    
        </ContentTemplate>
    </asp:UpdatePanel>
    
    </form>
</body>
</html>
