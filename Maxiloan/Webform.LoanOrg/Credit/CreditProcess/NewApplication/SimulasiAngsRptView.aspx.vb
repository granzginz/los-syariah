﻿
#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region


Public Class SimulasiAngsRptView
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property NTF() As Double
        Get
            Return CType(ViewState("NTF"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("NTF") = Value
        End Set
    End Property
    Private Property BungaEffective() As Double
        Get
            Return CType(ViewState("BungaEffective"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("BungaEffective") = Value
        End Set
    End Property
    Private Property TglEffective() As Date
        Get
            Return CType(ViewState("TglEffective"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("TglEffective") = Value
        End Set
    End Property
    Private Property AngsuranPertama() As Double
        Get
            Return CType(ViewState("AngsuranPertama"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("AngsuranPertama") = Value
        End Set
    End Property
    Private Property Tenor() As Integer
        Get
            Return CType(ViewState("Tenor"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("Tenor") = Value
        End Set
    End Property

#End Region

#Region "Constanta"
    Private oCustomClass As New Parameter.FinancialData
    Private m_coll As New FinancialDataController
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
    End Sub

#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim objReport As SimulasiAngsReport = New SimulasiAngsReport
        Dim oFinancialData1 As New Parameter.FinancialData
        oFinancialData1.strConnection = GetConnectionString()

        oData = m_coll.GetInstallmentSchSiml(oFinancialData1)

        updateDSWithHeader(oData)
        objReport.SetDataSource(oData)
        CrystalReportViewer1.ReportSource = objReport

        '========================================================
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_SimulasiAngs.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("SimulasiAngs.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_SimulasiAngs")

    End Sub
#End Region

    Private Sub updateDSWithHeader(ByRef oData As DataSet)
        With oData.Tables(0)
            .Columns.Add("NTF")
            .Columns.Add("BungaEffective")
            .Columns.Add("TglEffective")
            .Columns.Add("AngsuranPertama")
            .Columns.Add("Tenor")

            For i As Integer = 0 To .Rows.Count - 1
                .Rows(i)("NTF") = Me.NTF
                .Rows(i)("BungaEffective") = Me.BungaEffective
                .Rows(i)("TglEffective") = Me.TglEffective
                .Rows(i)("AngsuranPertama") = Me.AngsuranPertama
                .Rows(i)("Tenor") = Me.Tenor
            Next
        End With
    End Sub

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptSimulasiAng")

        Me.NTF = CDbl(cookie.Values("NTF"))
        Me.BungaEffective = CDbl(cookie.Values("BungaEffective"))
        Me.TglEffective = CDate(cookie.Values("TglEffective"))
        Me.AngsuranPertama = CDbl(cookie.Values("AngsuranPertama"))
        Me.Tenor = CInt(cookie.Values("Tenor"))
    End Sub

End Class