﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class DuplikasiAplikasi
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oUCReason As UCReason

#Region "Constants"
    Private m_controller As New Controller.GeneralPagingController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private LStrStatusNote As String
    Private LStrStatus As String
    Private Counter As Integer = 0

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents txtJumlahAplikasi As ucNumberFormat
#End Region

#Region "Properties"

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property SupplierName() As String
        Get
            Return CType(ViewState("SupplierName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierName") = Value
        End Set
    End Property

    Private Property EmployeeName() As String
        Get
            Return CType(ViewState("EmployeeName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("EmployeeName") = Value
        End Set
    End Property

    Private Property NewApplicationDate() As Date
        Get
            Return CType(ViewState("NewApplicationDate"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("NewApplicationDate") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Private Property CustomerType() As String
        Get
            Return CType(ViewState("CustomerType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerType") = Value
        End Set
    End Property


    Private Property IsActive() As Boolean
        Get
            Return CType(ViewState("IsActive"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsActive") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Property CreditScoreSchemeID() As String
        Get
            Return CType(ViewState("CreditScoreSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreSchemeID") = Value
        End Set
    End Property

    Private Property CreditScoreComponentID() As String
        Get
            Return CType(ViewState("CreditScoreComponentID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreComponentID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property

    Private Property ShowProcess() As Boolean
        Get
            Return CType(ViewState("ShowProcess"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("ShowProcess") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If SessionInvalid() Then
                Exit Sub
            End If
            If Not Me.IsPostBack Then
                Me.FormID = "DUPAPP"
                txtJumlahAplikasi.TextCssClass = "medium_text"

                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    Me.SearchBy = ""
                    Me.SortBy = ""
                End If
                InitialDefaultPanel()
                Me.Sort = ""
                Me.BranchID = Replace(Me.sesBranchId, "'", "")
                Me.CmdWhere = " AGR.BranchID = '" & sesBranchId.Replace("'", "") & "'"
                BindGridEntity(Me.CmdWhere)
                txtGoPage.Text = "1"
            End If


        Catch ex As Exception
            DisplayMessage(ex.ToString)
        End Try
    End Sub



#End Region

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()

        pnlSearch.Visible = True
        pnlList.Visible = True
        pnlProcess.Visible = False
        lblMessage.Text = ""        

    End Sub
#End Region


#Region "DisplayMessage"
    Private Sub DisplayMessage(ByVal strMsg As String)
        ShowMessage(lblMessage, strMsg, True)
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oEntity As New Parameter.GeneralPaging

        Try
            With oEntity
                .WhereCond = Me.CmdWhere
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = Me.Sort
                .strConnection = GetConnectionString()
                .SpName = "spAgreementDuplikasiPaging"
            End With
            oEntity = m_controller.GetGeneralPaging(oEntity)

            If Not oEntity Is Nothing Then
                dtEntity = oEntity.ListData
                recordCount = oEntity.TotalRecords
            Else
                recordCount = 0
            End If

            dtgPaging.DataSource = dtEntity.DefaultView
            dtgPaging.CurrentPageIndex = 0

            dtgPaging.DataBind()
            PagingFooter()
            pnlList.Visible = True

        Catch ex As Exception
            DisplayMessage(ex.ToString)
        End Try
    End Sub
#End Region

#Region "LinkTo"
    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strSupplierID & "','" & strStyle & "')"
    End Function

    Function LinkToEmployee(ByVal strBranchID As String, ByVal strAOID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinEmployee('" & strBranchID & "','" & strAOID & "','" & strStyle & "')"
    End Function
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim hyApplicationId As HyperLink
        If e.Item.ItemIndex >= 0 Then

            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyApplicationId = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(hyApplicationId.Text.Trim) & "')"
            '*** ApplicationId link
            hyTemp = CType(e.Item.FindControl("hyApplicationId"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(hyTemp.Text.Trim) & "')"

            '*** Supplier Name link
            lblTemp = CType(e.Item.FindControl("lblSupplierID"), Label)
            hyTemp = CType(e.Item.FindControl("hySupplierName"), HyperLink)
            hyTemp.NavigateUrl = LinkToSupplier(lblTemp.Text.Trim, "AccAcq")

            '*** AO Name link
            lblTemp = CType(e.Item.FindControl("lblAOID"), Label)
            hyTemp = CType(e.Item.FindControl("hyAOName"), HyperLink)
            hyTemp.NavigateUrl = LinkToEmployee(Me.sesBranchId.Replace("'", ""), lblTemp.Text.Trim, "AccAcq")
        End If
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oProduct As New Parameter.Product
        If e.CommandName = "Duplikasi" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
                Dim lblCustomerId As Label
                Dim hyApplicationID As HyperLink
                Dim hyAgreementNo As HyperLink
                Dim hyCustomerName As HyperLink

                hyApplicationID = CType(e.Item.FindControl("hyApplicationID"), HyperLink)
                hyAgreementNo = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
                lblCustomerId = CType(e.Item.FindControl("lblCustomerId"), Label)
                hyCustomerName = CType(e.Item.FindControl("hyCustomerName"), HyperLink)

                txtNotes.Text = ""
                hdnApplicationID.Value = hyApplicationID.Text
                With ucViewApplication1
                    .ApplicationID = hyApplicationID.Text
                    .bindData()
                    .initControls("OnNewApplication")
                End With
                ucViewCustomerDetail1.CustomerID = lblCustomerId.Text
                ucViewCustomerDetail1.bindCustomerDetail()
                With txtJumlahAplikasi
                    .RangeValidatorEnable = True
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorMaximumValue = "999"
                    .RangeValidatorMinimumValue = "0"
                End With

                Me.ShowProcess = True
                pnlList.Visible = False
                pnlSearch.Visible = False
                pnlProcess.Visible = True
            End If
        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        txtGoPage.Text = "1"
        InitialDefaultPanel()
        BindGridEntity("")
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.CmdWhere = "Agr.BranchID = '" & Me.sesBranchId.Replace("'", "") & "'"

        If txtSearch.Text.Trim <> "" Then
            Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
            Me.CmdWhere += " and " & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region


#Region "imgSave"
    Private Sub imgSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim customClass As New Parameter.Application
        Dim oController As New ApplicationController
        Dim ErrMessage As String = ""
        Try
            With customClass
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .ApplicationID = hdnApplicationID.Value.Trim
                .BusinessDate = Me.BusinessDate
                .Notes = txtNotes.Text.Trim
                .JumlahAplikasi = CInt(txtJumlahAplikasi.Text.Trim)
            End With

            ErrMessage = oController.DuplikasiApplication(customClass)

            If ErrMessage.Trim <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
            Else
                ShowMessage(lblMessage, "Duplikasi Kontrak Berhasil. Data disimpan", False)
            End If

            pnlSearch.Visible = True
            pnlList.Visible = True
            pnlProcess.Visible = False
            BindGridEntity(Me.CmdWhere)
        Catch ex As Exception
            DisplayMessage(ex.Message)
        Finally
            BindGridEntity(Me.CmdWhere)
        End Try

    End Sub
#End Region
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        InitialDefaultPanel()
        BindGridEntity(Me.CmdWhere)
    End Sub
End Class