﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewHasilSurvey.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewHasilSurvey" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/UcIncentiveGridView.ascx" TagName="UcIncentiveGridView"
    TagPrefix="uc1" %>
    <%@ Register Src="../../../../webform.UserController/ucApplicationViewTab.ascx" TagName="ucApplicationViewTab"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/UcIncentiveInternalGridView.ascx"
    TagName="UcIncentiveInternalGridView" TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucHasilSurveyViewTabPhone.ascx" TagName="ucHasilSurveyViewTabPhone"
    TagPrefix="uc10" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>VIEW REFUND - INSENTIF</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" />
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script type='text/javascript' language='javascript'>
        $(function () {
            $(".accordion").accordion();

        });
    </script>
    <script type='text/javascript' language='javascript'>

        function AddNewRecordInternal(clientid) {
            var grd = document.getElementById(clientid);
            if (grd.rows.length <= 2) {
                return false;
            }

            var tbod = grd.rows[0].parentNode;

            var htmlRoot = grd.rows[grd.rows.length - 1].cloneNode(true);

            grd.deleteRow(grd.rows.length - 1);

            var sel = grd.rows[grd.rows.length - 1].getElementsByTagName('select')[0];
            var sel_ = sel.value;

            var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);

            var len = grd.rows.length;
            newRow.cells[0].innerHTML = len;

            var inp1 = newRow.cells[1].getElementsByTagName('select')[0];
            inp1.id += len;
            inp1.value = '';

            var inp2 = newRow.cells[2].getElementsByTagName('input')[0];
            inp2.id += len;
            inp2.value = '';

            var inp3 = newRow.cells[3].getElementsByTagName('input')[0];
            inp3.id += len;
            inp3.value = '';

            var arrClientID = clientid.split("_");
            var nilaiAlokasi;

            if (arrClientID[0] == "UcTitipanRefundBunga") {
                nilaiAlokasi = $('#lblTitipanRefundBunga').html();
            } else if (arrClientID[0] == "UcTitipanProvisi") {
                nilaiAlokasi = $('#lblTitipanProvisi').html();
            } else if (arrClientID[0] == "UcTitipanBiayalainnya") {
                nilaiAlokasi = $('#lblTitipanBiayaLainnya').html();
            }

            inp2.setAttribute("OnChange", "handletxtProsentaseInternal_Change(this.value,'" + nilaiAlokasi + "','" + inp3.id + "')");
            inp3.setAttribute("OnChange", "handletxtInsentifInternal_Change(this.value,'" + nilaiAlokasi + "','" + inp2.id + "')");

            // grd.appendChild(newRow);
            $('#' + clientid).append(newRow);

            if (sel.value != "") {
                $("#" + inp1.id + " option[value=" + sel.value + "]").hide();
            }
            $('#' + clientid).append("<tr class='item_grid'>" + htmlRoot.innerHTML + "</tr>");
            return false;
        }
        function AddNewRecordJQ(clientid) {
            var grd = document.getElementById(clientid);
            var tbod = grd.rows[0].parentNode;

            if (grd.rows.length <= 2) {
                return false;
            }

            var htmlRoot = grd.rows[grd.rows.length - 1].cloneNode(true);

            grd.deleteRow(grd.rows.length - 1);


            var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);

            var len = grd.rows.length;
            newRow.cells[0].innerHTML = len;


            var inp2 = newRow.cells[2].getElementsByTagName('select')[0];
            inp2.id += len;
            inp2.value = '';

            var inp1 = newRow.cells[1].getElementsByTagName('select')[0];
            inp1.id += len;
            inp1.value = '';


            var inp6 = newRow.cells[6].getElementsByTagName('span')[0];
            inp6.id += len;
            inp6.value = '';

            var inp7 = newRow.cells[7].getElementsByTagName('span')[0];
            inp7.id += len;
            inp7.value = '';



            var inp3 = newRow.cells[3].getElementsByTagName('input')[0];
            inp3.id += len;
            inp3.value = '';

            var inp4 = newRow.cells[4].getElementsByTagName('input')[0];
            inp4.id += len;
            inp4.value = '';

            var arrClientID = clientid.split("_");
            var nilaiAlokasi;

            if (arrClientID[0] == "UcPremiAsuransi") {
                nilaiAlokasi = $('#lblAlokasiPremiAsuransi').html();
            } else if (arrClientID[0] == "UcProgresifPremiAsuransi") {
                nilaiAlokasi = $('#lblProgresifPremiAsuransi').html();
            } else if (arrClientID[0] == "UcRefundBunga") {
                nilaiAlokasi = $('#lblRefundBunga').html();
            } else if (arrClientID[0] == "UcPremiProvisi") {
                nilaiAlokasi = $('#lblPremiProvisi').html();
            } else if (arrClientID[0] == "UcBiayaLainnya") {
                nilaiAlokasi = $('#lblBiayaLainnya').html();
            }

            var inp5 = newRow.cells[5].getElementsByTagName('span')[0];
            inp5.id += len;
            inp5.value = '';


            var inp8 = newRow.cells[8].getElementsByTagName('span')[0];
            inp8.id += len;
            inp8.value = '';

            var inp9 = newRow.cells[9].getElementsByTagName('span')[0];
            inp9.id += len;
            inp9.value = '';

            inp1.setAttribute("OnChange", "handleddlJabatan_Change(this,'" + inp2.id + "');getPPH(this.value,'" + inp6.id + "','" + inp7.id + "')");
            inp2.setAttribute("OnChange", "handleddlpenerima_Change(this.value,'" + inp1.id + "','" + inp7.id + "')");
            inp3.setAttribute("OnChange", "handletxtProsentase_Change(this.value,'" + nilaiAlokasi + "','" + inp4.id + "')");
            inp4.setAttribute("OnChange", "handletxtInsentif_Change(this.value,'" + nilaiAlokasi + "','" + inp3.id + "')");

            // grd.appendChild(newRow);
            $('#' + clientid).append(newRow);
            $('#' + clientid).append("<tr class='item_grid'>" + htmlRoot.innerHTML + "</tr>");
            return false;
        }
        function rboCaraSurvey_onchange() {

            var _CaraSurvey = $('#cboCaraSurvey').val();
            if (_CaraSurvey === 'False') {
                $('#divCaraSurvey').css("display", "inherit");
                return;
            }
            else {
                $('#divCaraSurvey').css("display", "none");
                $('#ucHasilSurveyTabPhone1_txtTlpRumah').val('');
                $('#ucHasilSurveyTabPhone1_txtTlpKantor').val('');
                $('#ucHasilSurveyTabPhone1_txtHandphone').val('');
                $('#ucHasilSurveyTabPhone1_txtEmergencyContact').val('');
            }
        }
        function UploadDokument(ucASPXPath, ucTitle, divLookupContent, paramData) {
            paramData = paramData || '';
            $('#dialog').remove();
            var match = new RegExp(App, 'gi');
            var tmpStrPath = AppInfo.replace(match, '');
            tmpStrPath = tmpStrPath.substring(2, tmpStrPath.length - 1);
            var arrStrPath = tmpStrPath.split('/');
            tmpStrPath = '';
            $.each(arrStrPath, function (index, value) {
                if (arrStrPath.length - 1 != index) {
                    tmpStrPath += '../'
                }
            });



            $('#' + divLookupContent).prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><img src="' + tmpStrPath + 'Images/exit_lookup00.png" class="ui-custom-close" /><iframe src="' + ucASPXPath + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
            $('#dialog')
	        .data('fnName', paramData)
	        .dialog({
	            title: ucTitle,
	            bgiframe: false,
	            width: 900,
	            height: 500,
	            resizable: false,
	            modal: true,
	            closeOnEscape: true,
	            draggable: true,
	            create: function (event, ui) {
	                $(".ui-widget-header").hide();
	            },
	            open: function () {
	                $('.ui-custom-close').bind('click', function () {
	                    $('#dialog').dialog('close');
	                })
	            }
	        });
            return false;
        }
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" class="tab_container_form">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucapplicationviewtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        HASIL SURVEY</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA HASIL SURVEY</h4>
                </div>
                            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Cara Survey</label>  
                        <asp:Label runat="server" ID="LblCaraSurvey"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Surveyor</label>
                        <asp:Label runat="server" ID="LblSurveyor"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Tanggal Visit/Telepon</label>
                        <asp:Label runat="server" ID="LblSurveyDate"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Pembiayaan Analyst</label>
                        <asp:Label runat="server" ID="LblCreditAnalyst"></asp:Label>
                    </div>
                </div>
            </div>
           <%-- 
            <div  class="form_box"  runat="server" id="divCaraSurvey">                
                <uc10:ucHasilSurveyViewTabPhone runat="server" id="ucHasilSurveyViewTabPhone1"></uc10:ucHasilSurveyViewTabPhone>                
            </div>--%>
             <div class="form_box">
                 <div>
                    <div class="form_left">
                        <label>Tanggal Visit/Telepon</label>
                        <asp:Label runat="server" ID="LblAreaPhoneHome"></asp:Label>
                        <asp:Label ID="Label5" runat="server">-</asp:Label>
                        <asp:Label runat="server" ID="LblPhoneHome"></asp:Label>
                     </div>
                   </div>
            </div>
            <div class="form_box">
                 <div>
                    <div class="form_left">
                    <label>Telepon Kantor</label>
                    <asp:Label runat="server" ID="LblAreaPhoneOffice"></asp:Label>
                    <asp:Label ID="Label6" runat="server">-</asp:Label>
                    <asp:Label runat="server" ID="LblPhoneOffice"></asp:Label>
                    </div>
                 </div>
            </div>
            <div class="form_box">
                 <div>
                    <div class="form_left">
                    <label> Handphone</label>
                    <asp:Label runat="server" ID="LblHandphone"></asp:Label>
                    </div>
                 </div>
            </div>

             <div class="form_box">
                 <div>
                    <div class="form_left">
                    <label>Telp Emergency Contact</label>
                    <asp:Label runat="server" ID="LblEmergencyPhone"></asp:Label>
                    </div>
                 </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA HASIL SURVEY - CHARACTER</h4>
                </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Pihak yang turut Survey</label>  
                        <asp:Label runat="server" ID="LblPihakYangTurutSurvey"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanPihakYangTurutSurvey"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Lokasi yang disurvey</label>  
                        <asp:Label runat="server" ID="LblLokasiyangDisurvey"></asp:Label>                                                
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanLokasiYangDisurvey"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Lokasi survey sesuai KTP</label>  
                        <asp:Label runat="server" ID="LblSurveySesuaiKTP"></asp:Label>     
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label>
                            Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanSurveySesuaiKTP"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Waktu survey</label>
                        <asp:Label runat="server" ID="LblWaktuSurvey1"></asp:Label>
                        <asp:Label ID="lblMiring" runat="server">s/d</asp:Label>
                        <asp:Label runat="server" ID="LblWaktuSurvey2"></asp:Label>
                    </div>
                </div>
            </div>
        <div class="form_title">
            <div class="form_single">
                <h3>CEK LINGKUNGAN</h3>
            </div>
        </div>
      <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px" DataKeyField="Number"
                CellPadding="3" CellSpacing="1" CssClass="grid_general"
                Width="100%">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>                 
                    <asp:TemplateColumn HeaderText="NO" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="50px">                        
                        <ItemTemplate>
                            <asp:Label ID="lblCLNumber" runat="server" Text='<%# container.dataitem("Number") %>'></asp:Label>
                        </ItemTemplate>                        
                    </asp:TemplateColumn>             
                    
                    <asp:TemplateColumn HeaderText="NAMA" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="450px">                        
                        <ItemTemplate>
                            <asp:Label ID="txtCLNama" runat="server" Text='<%# container.dataitem("CLNama1") %>'></asp:Label>
                        </ItemTemplate>                        
                    </asp:TemplateColumn>                                       
                    <asp:TemplateColumn HeaderText="DOMISILI" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="450px">                        
                        <ItemTemplate>
                            <%--<asp:Label ID="cboCLDomisili" runat="server" Text='<%# container.dataitem("CLDomisili1") %>' OnDataBinding=""></asp:Label>--%>
                            <asp:Label ID="Label3" runat="server" OnDataBinding="lblCode_DataBinding"></asp:Label>
                         </ItemTemplate>                        
                    </asp:TemplateColumn>             
                    <asp:TemplateColumn HeaderText="LAMA TINGGAL" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="250px">                        
                        <ItemTemplate>
                            <asp:Label ID="txtCLLamaTinggal" runat="server" Text='<%# container.dataitem("CLLamaTinggal1") %>'></asp:Label>
                        </ItemTemplate>                        
                    </asp:TemplateColumn>                                       
                    <asp:TemplateColumn HeaderText="LAMA USAHA" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="250px">                        
                        <ItemTemplate>
                            <asp:Label ID="txtCLLamaUsaha" runat="server" Text='<%# container.dataitem("CLLamaUsaha1") %>'></asp:Label>
                        </ItemTemplate>                        
                    </asp:TemplateColumn>                                                                                     
                </Columns>
            </asp:DataGrid>
        </div>
    </div>              

            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Hasil cek lingkungan aspek sosial</label>
                        <asp:Label runat="server" ID="LblHasilCekLkgAspekSosial"></asp:Label>  
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Pengalaman Pembiayaan</label>  
                        <asp:Label runat="server" ID="LblPengalamanKredit"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Pembayaran</label>
                        <asp:Label runat="server" ID="LblPembayaran"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Bukti Bayar</label>  
                        <asp:Label runat="server" ID="LblBuktiBayar"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Pekerjaan Sekarang</label>  
                        <asp:Label runat="server" ID="LblPekerjaanSekarang"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Lamanya</label>
                            <asp:Label runat="server" ID="LblLamaPekerjaanSekarang"></asp:Label>
                            <label>tahun</label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Pekerjaan Sebelumnya</label>
                        <asp:Label runat="server" ID="LblPekerjaanSebelumnya"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Lamanya</label>
                        <asp:Label runat="server" ID="LblLamaPekerjaanSebelumnya"></asp:Label>
                        <label>tahun</label>
                    </div>
                </div>
            </div>           
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Pendidikan</label>  
                        <asp:Label runat="server" ID="LblPEducation"></asp:Label>
                    </div>
                </div>
            </div>          
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama yang Menemani survey</label>
                        <asp:Label runat="server" ID="LblNamaYangMenemaniSurvey1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Hubungan</label>
                        <asp:Label runat="server" ID="LblHubungan1"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama yang Menemani survey</label>
                        <asp:Label runat="server" ID="LblNamaYangMenemaniSurvey2"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Hubungan</label>
                        <asp:Label runat="server" ID="LblHubungan2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Hubungan sales dengan pemohon </label>  
                        <asp:Label runat="server" ID="LblHubSalesdgnPemohon"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>
                        <asp:Label runat="server" ID="LblKesimpulanCharacter"></asp:Label>
                    </div>
                </div>
            </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA HASIL SURVEY - CAPACITY</h4>
                </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Lama Usaha / Kerja</label>
                        <asp:Label runat="server" ID="LblLamaUsaha"></asp:Label>
                        <label>Tahun</label>
                    </div>
                    <div class="form_right">
                        <label>Nama Usaha / Pekerjaan</label>
                        <asp:Label runat="server" ID="LblNameUsaha"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Alamat Usaha / Bekerja</label>
                        <asp:Label runat="server" ID="LblAlamatUsaha"></asp:Label>  
                    </div>
                </div>
            </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>LAMPIRAN USAHA</h4>
            </div>
        </div>                
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Alamat Usaha / Bekerja</label> 
                        <asp:Label runat="server" ID="LblAlamatUsaha2"></asp:Label>   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Foto Tempat Usaha</label>  
                        <asp:Label runat="server" ID="LblFotoTempatUsaha"></asp:Label>   
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanFotoTempatUsaha"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Stock Barang Dagangan</label>  
                        <asp:Label runat="server" ID="LblStockBrgDagangan"></asp:Label>   
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanStockBarangDagangan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Bon-bon Bukti Usaha</label>  
                        <asp:Label runat="server" ID="LblBonBuktiUsaha"></asp:Label>   
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanbonBuktiUsaha"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>SKU No</label>  
                        <asp:Label runat="server" ID="LblSKUNo"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>
                        <asp:Label runat="server" ID="LblTglSKU"></asp:Label>
                    </div>
                </div>
            </div>           
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>SIUP No</label>  
                        <asp:Label runat="server" ID="LblSIUPNo"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>
                        <asp:Label runat="server" ID="LblTglSIUP"></asp:Label>
                    </div>
                </div>
            </div>          
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>TDP No</label>
                        <asp:Label runat="server" ID="LblTDPNo"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>
                        <asp:Label runat="server" ID="LblTglTDP"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>SKDP No</label>  
                        <asp:Label runat="server" ID="LblSKDPNo"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>
                        <asp:Label runat="server" ID="LblTglSKDP"></asp:Label>
                    </div>
                </div>
            </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>LAMPIRAN PEKERJAAN</h4>
            </div>
        </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Slip Gaji</label>  
                        <asp:Label runat="server" ID="LblSlipGaji"></asp:Label>   
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanSlipGaji"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">S.K Penghasilan</label>  
                        <asp:Label runat="server" ID="LblSKpenghasilan"></asp:Label>   
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanSKPenghasilan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Jabatan/Golongan</label>  
                        <asp:Label runat="server" ID="LblJabatan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">NPWP Pribadi</label>  
                        <asp:Label runat="server" ID="LblNPWPPribadi"></asp:Label>   
                    </div>
                    <div class="form_right">
                        <label>No NPWP</label>
                        <asp:Label runat="server" ID="LblNPWPNo"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Uraian Usaha/Pekerjaan</label>
                        <asp:Label runat="server" ID="LblUraianUsaha"></asp:Label>  
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Alur Usaha</label>  
                        <asp:Label runat="server" ID="LblAlurUsaha"></asp:Label>  
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kapasitas Konsumen</label> 
                        <asp:Label runat="server" ID="LblKapasitasKonsumen"></asp:Label>   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>  
                        <asp:Label runat="server" ID="LblKesimpulanCapacity"></asp:Label>   
                    </div>
                </div>
            </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA HASIL SURVEY - CONDITION</h4>
                </div>
            <div class="form_title">
                  <div>
                    <div class="form_left">
                    <h3>
                        ANALISA KEUANGAN</h3>
                    </div>
                    <div class="form_right">
                    <h3>
                        SKENARIO PENURUNAN PENDAPATAN</h3>
                    </div>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Total Penghasilan / Pendapatan</label>  
                        <asp:Label runat="server" ID="LblTotalPenghasilan1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Total Penghasilan / Pendapatan</label>  
                        <asp:Label runat="server" ID="LblTotalPenghasilan2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Biaya Usaha</label>
                        <asp:Label runat="server" ID="LblBiayaUsaha1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Biaya Usaha</label>  
                        <asp:Label runat="server" ID="LblBiayaUsaha2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Biaya Hidup</label>
                        <asp:Label runat="server" ID="LblBiayaHidup1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Biaya Hidup</label>  
                        <asp:Label runat="server" ID="LblBiayaHidup2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Biaya Cicilan</label>  
                        <asp:Label runat="server" ID="LblBiayaCicilan1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Biaya Cicilan</label>
                        <asp:Label runat="server" ID="LblBiayaCicilan2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Sisa sebelum tambah unit</label>  
                        <asp:Label runat="server" ID="LblSisaSebelumTambahUnit1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Sisa sebelum tambah unit</label>  
                        <asp:Label runat="server" ID="LblSisaSebelumTambahUnit2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Income dari unit</label>  
                        <asp:Label runat="server" ID="LblIncomeDariUnit1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Income dari unit</label>  
                        <asp:Label runat="server" ID="LblIncomeDariUnit2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Estimasi Angsuran</label>  
                        <asp:Label runat="server" ID="LblEstimasiAngsuran1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Estimasi Angsuran</label>  
                        <asp:Label runat="server" ID="LblEstimasiAngsuran2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Sisa Pendapatan</label>  
                        <asp:Label runat="server" ID="LblSisaPendapatan1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Sisa Pendapatan</label>  
                        <asp:Label runat="server" ID="LblSisaPendapatan2"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_title">
                    <div class="form_single">
                    <h3>
                        BUKTI PENDUKUNG</h3>
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Rekening Tabungan/Giro</label>  
                        <asp:Label runat="server" ID="LblRekeningTabunganGiro"></asp:Label>     
                    </div>
                    <div class="form_right">
                        <label>Nama Bank</label>  
                        <asp:Label runat="server" ID="LblNamaBank"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Laporan Keuangan</label>
                        <asp:Label runat="server" ID="LblLaporanKeuangan"></asp:Label>       
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:Label runat="server" ID="LblAlasanLaporanKeuangan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Pembiayaan dari Bank/LNKB</label>  
                        <asp:Label runat="server" ID="LblKreditdariBankLNKB"></asp:Label>       
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama Institusi</label>  
                        <asp:Label runat="server" ID="LblNamaInstitusi1"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label class="label_split" runat="server" id="lblTelepon1">
                            Angsuran
                        </label>
                        <asp:Label runat="server" ID="LblAngsuran1"></asp:Label>
                        <asp:Label ID="Label1" runat="server">Sisa</asp:Label>
                        <asp:Label runat="server" ID="LblSisa1"></asp:Label>
                        <label>bulan</label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama Institusi</label>  
                        <asp:Label runat="server" ID="LblNamaInstitusi2"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label class="label_split" runat="server" id="Label2">
                            Angsuran
                        </label>
                        <asp:Label runat="server" ID="LblAngsuran2"></asp:Label>
                        <asp:Label ID="lblStrip2" runat="server">Sisa</asp:Label>
                        <asp:Label runat="server" ID="LblSisa2"></asp:Label>
                        <label>bulan</label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Bukti Pembayaran Angsuran</label> 
                        <asp:Label runat="server" ID="LblBuktiPembayaranAngsuran"></asp:Label>        
                    </div>
                    <div class="form_right">
                    <label>Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanBuktiPembayaranAng"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>  
                        <asp:Label runat="server" ID="LblKesimpulanCondition"></asp:Label>
                    </div>
                </div>
            </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA HASIL SURVEY - CAPITAL</h4>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        KEKAYAAN YANG DIMILIKI</h3>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Status Kepemilikan</label>  
                        <asp:Label runat="server" ID="LblStatusKepemilikan"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Atas Nama</label>
                        <asp:Label runat="server" ID="LblAtasNama"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>PBB/AJB/SHM atas nama</label>
                        <asp:Label runat="server" ID="LblPBBAJBSHMAtasNama"></asp:Label>                       
                    </div>
                    <div class="form_right">
                        <label>Tahun tagihan</label>
                        <asp:Label runat="server" ID="LblTahunTagihan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">PBB/AJB/SHM</label>  
                        <asp:Label runat="server" ID="LblPBBAJBSHM"></asp:Label>   
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanPBBAJBSHM"></asp:Label>                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Rek listrik atas nama</label>  
                        <asp:Label runat="server" ID="LblRekListrikAtasNama"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Bulan tagihan</label>  
                        <asp:Label runat="server" ID="LblBulanTagihan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Rek listrik</label>  
                        <asp:Label runat="server" ID="LblRekListrik"></asp:Label>   
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:Label runat="server" ID="LblAlasanrekListrik"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_split" runat="server" id="lblLuasTanahBangunan">Luas Tanah / Bangunan</label>  
                        <asp:Label runat="server" ID="LblLuasTanah"></asp:Label>
                        <asp:Label ID="lblStrip1" runat="server">m2</asp:Label>
                        <asp:Label runat="server" ID="LblLuasBangunan"></asp:Label>
                        <label>m2</label>

                    </div>
                    <div class="form_right">
                        <label>Taksiran nilai jual</label> 
                        <asp:Label runat="server" ID="LblTaksiranNilaiJual"></asp:Label> 
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Lama tinggal</label>
                        <asp:Label runat="server" ID="LblLamaTinggal"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Asset lain</label>  
                        <asp:Label runat="server" ID="LblAssetLain"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Foto Rumah</label>
                        <asp:Label runat="server" ID="LblFotoRumah"></asp:Label>     
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:Label runat="server" ID="LblAlasanFotoRumah"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Kondisi bangunan</label>  
                        <asp:Label runat="server" ID="LblKondisiBangunan"></asp:Label>
                    </div>
                </div>
            </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>BUKTI ADMINISTRATIF KEPENDUDUKAN</h4>
            </div>
        </div>                
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>No KTP Pemohon</label>  
                        <asp:Label runat="server" ID="LblNoKTPPemohon"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>  
                        <asp:Label runat="server" ID="LblTglKTPPemohon"></asp:Label>
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">KTP Pemohon</label>  
                        <asp:Label runat="server" ID="LblKTPPemohon"></asp:Label>     
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:Label runat="server" ID="LblAlasanKTPPemohon"></asp:Label>
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>No KTP Pasangan</label>  
                        <asp:Label runat="server" ID="LblNoKTPPasangan"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>  
                        <asp:Label runat="server" ID="LblTglKTPPasangan"></asp:Label>
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">KTP Pasangan</label>  
                        <asp:Label runat="server" ID="LblKTPPasangan"></asp:Label>     
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:Label runat="server" ID="LblAlasanKTPPasangan"></asp:Label>
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>No Kartu Keluarga</label>  
                        <asp:Label runat="server" ID="LblNoKK"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>  
                        <asp:Label runat="server" ID="LblTglNoKK"></asp:Label>
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Buku Nikah</label>  
                        <asp:Label runat="server" ID="LblBukuNikah"></asp:Label>     
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:Label runat="server" ID="LblAlasanBukuNikah"></asp:Label>
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Jumlah tanggungan</label>
                        <asp:Label runat="server" ID="LblJumlahTanggungan"></asp:Label>
                        <label>orang</label>
                    </div>
                </div>
            </div>                  
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>  
                        <asp:Label runat="server" ID="LblKesimpulanCapital"></asp:Label>
                    </div>
                </div>
            </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA HASIL SURVEY - COLLATERAL</h4>
                </div>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        JAMINAN PEMBIAYAAN</h3>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama Asset yang diajukan</label>  
                        <asp:Label runat="server" ID="LblKendaraanYangDiajukan"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan Pembelian</label>
                        <asp:Label runat="server" ID="LblAlasanPembelian"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Lokasi penggunaan</label> 
                        <asp:Label runat="server" ID="LblLokasiPenggunaan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Pencocokan STNK dengan fisik</label>  
                        <asp:Label runat="server" ID="LblPencocokanSTNKdgnFisik"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Foto rumah</label>
                        <asp:Label runat="server" ID="LblFotoRumahCollateral"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">SP BPKB dikeluarkan oleh</label>  
                        <asp:Label runat="server" ID="LblBPKPDikeluarkanOleh"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Jumlah asset lain dimiliki</label>  
                        <asp:Label runat="server" ID="LblJumlahAssetLain"></asp:Label>
                        <label>Unit</label>
                    </div>
                    <div class="form_right">
                        <label>Jenis</label>  
                        <asp:Label runat="server" ID="LblJenisAssetLain"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Status asset lain lunas</label>
                        <asp:Label runat="server" ID="LblStatusAssetLainLunas"></asp:Label>
                        <label>Unit</label>
                    </div>
                    <div class="form_right">
                        <label>Status asset lain tidak lunas</label>  
                        <asp:Label runat="server" ID="LblStatusAssetLainTidakLunas"></asp:Label>
                        <label>Unit</label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>No BPKB</label>  
                        <asp:Label runat="server" ID="LblBPKBNo"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Nama BPKB</label>  
                        <asp:Label runat="server" ID="LblNamaBPKB"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">BPKB Diperlihatkan</label>  
                        <asp:Label runat="server" ID="LblBPKBDiperlihatkan"></asp:Label>     
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:Label runat="server" ID="LblAlasanBPKBDiperlihatkan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>
                        <asp:Label runat="server" ID="LblKesimpulanCollateral"></asp:Label>
                    </div>
                </div>
            </div>
            </div>            
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA HASIL SURVEY - CATATAN</h4>
                </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Kunjungan Rumah Bertemu</label>  
                        <asp:Label runat="server" ID="LblKunjunganRumahbertemu"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Kondisi Rumah</label>
                        <asp:Label runat="server" ID="LblKondisiRumah"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Perabotan Rumah</label>  
                        <asp:Label runat="server" ID="LblPerabotanRumah"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Garasi Mobil</label>
                        <asp:Label runat="server" ID="LblGarasiMobil"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Garasi untuk berapa unit</label>  
                        <asp:Label runat="server" ID="LblGarasiUnit"></asp:Label>
                        <label>unit</label>
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan</label>
                        <asp:Label runat="server" ID="LblAlasanGarasiUnit"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Lokasi Rumah</label>  
                        <asp:Label runat="server" ID="LblLokasiRumah"></asp:Label>
                    </div>
                    <div class="form_left">
                        <label>Jalan masuk dilalui</label>  
                        <asp:Label runat="server" ID="LblJalanMasukDilalui"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Kondisi Jalan Depan Rumah</label>  
                        <asp:Label runat="server" ID="LblKondisiJlnDpnRumah"></asp:Label>
                    </div>
                    <div class="form_left">
                        <label>Keadaan Lingkungan</label>  
                        <asp:Label runat="server" ID="LblKeadaanLingkungan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Kunjungan Kantor Bertemu</label>
                        <asp:Label runat="server" ID="LblKunjunganKantorBertemu"></asp:Label>
                    </div>
                    <div class="form_left">
                        <label>Jumlah Karyawan</label>  
                        <asp:Label runat="server" ID="LblJumlahKaryawan"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Lokasi tempat Usaha / Kantor</label>  
                        <asp:Label runat="server" ID="LblLokasiTempatUsaha"></asp:Label>                        
                    </div>
                    <div class="form_left">
                        <label>Alasan</label>  
                        <asp:Label runat="server" ID="LblAlasanLokasiTempatUsaha"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Aktivitas Tempat Usaha</label>  
                        <asp:Label runat="server" ID="LblAktivitasTmptUsaha"></asp:Label>     
                    </div>
                    <div class="form_left">
                        <label>Alasan</label> 
                        <asp:Label runat="server" ID="LblAlasanAktivitasTempatUsaha"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">TBO List</label> 
                        <asp:Label runat="server" ID="LblTBOList"></asp:Label>
                    </div>
                    <div class="form_left">
                        <label class="label_general">Usulan Lain</label>  
                        <asp:Label runat="server" ID="LblUsulanLain"></asp:Label>
                    </div>
                </div>
            </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>GAMBAR LOKASI</h4>
            </div>
        </div>              
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Denah Lokasi Rumah</label>  
                    </div>
                    <div class="form_right">
                        <label>Denah Lokasi Rumah</label>  
                    </div>
                </div>
            </div>
         <div class="form_box">
             <div class="form_left">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah1" runat="server" />
                   </div>
             </div>
             <div class="form_right">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah2" runat="server" />
                   </div>
             </div>
         </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Denah Lokasi Rumah</label>  
                    </div>
                    <div class="form_right">
                        <label>Denah Lokasi Rumah</label>  
                    </div>
                </div>
            </div>
         <div class="form_box">
             <div class="form_left">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah3" runat="server" />
                   </div>
             </div>
             <div class="form_right">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah4" runat="server" />
                   </div>
             </div>
         </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Denah Lokasi Rumah</label>  
                    </div>
                    <div class="form_right">                      
                    </div>
                </div>
            </div>
            <div class="form_box">
             <div class="form_left">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah5" runat="server" />
                   </div>
             </div>
             <div class="form_right">
                   <div>                       
                   </div>
             </div>
             </div>
            </div>                        
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA HASIL SURVEY - KYC</h4>
                </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>  
                        <asp:Label runat="server" ID="LblKesimpulanKYC"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
            <div>
                    <div class="form_left">
                        <label class="label_general">Prosedur KYC</label>  
                        <asp:HyperLink ID="hypProsedurKYC" runat="server" />
                    </div>
            </div>     
            </div>                   
            </div>  
                
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        HASIL APU PPT</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgView" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        BorderWidth="0" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No">
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNumber" runat="server" Text="0"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="KOMPONEN">
                                <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblKomponen" runat="server" Text='<%#container.dataitem("Komponen")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KLASIFIKASI">
                                <ItemStyle HorizontalAlign="left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblKlasifikasi" runat="server" Text='<%#container.dataitem("Klasifikasi")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NILAI">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNilai" runat="server" Text='<%#container.dataitem("Nilai")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BOBOT">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBobot" runat="server" Text='<%#container.dataitem("Bobot")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="HASIL">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblHasil" runat="server" Text='<%#container.dataitem("Hasil")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_single">
                     <Label>Score</Label> 
                      <asp:Label ID="lblScore" runat="server"  ></asp:Label>   
                 </div>
            </div> 
            <div class="form_box">
                <div class="form_single">
                    <Label>Result</Label> 
                    <asp:Label ID="lblResult" runat="server"></asp:Label>
                </div>
            </div>
                 
            <div class="tab_container_form_space"><br />
            </div>
            <div class="tab_container_button">
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>