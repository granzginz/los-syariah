﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewAssetData
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New AssetDataController
#End Region
#Region "Property"
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property PageSource() As String
        Get
            Return viewstate("PageSource").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("PageSource") = Value
        End Set
    End Property
    Property style() As String
        Get
            Return viewstate("style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("style") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID").ToString
            Me.style = Request("Style").ToString
            Me.PageSource = Request("pageSource").ToString
            'btnClose.Visible = True
            'If Me.PageSource = "" Then
            '    btnClose.Visible = False
            'End If
            Bindgrid()
        End If
        lblMessage.Text = ""
    End Sub
    Sub Bindgrid()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oDataAttribute As New DataTable
        Dim oDataAssetDoc As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.AppID = Me.ApplicationID
        oAssetData = m_controller.GetViewAssetData(oAssetData)

        If Not oAssetData Is Nothing Then
            oData = oAssetData.ListData
            oDataAttribute = oAssetData.DataAttribute
            oDataAssetDoc = oAssetData.DataAssetdoc
        End If
        If oAssetData.Output <> "" Then
            lblMessage.Text = oAssetData.Output
            Exit Sub
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
        dtgAttribute.DataSource = oDataAttribute.DefaultView
        dtgAttribute.DataBind()

        dtgAssetDoc.DataSource = oDataAssetDoc.DefaultView
        dtgAssetDoc.DataBind()
    End Sub

    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)

        lblAppID.Text = Me.ApplicationID
        lblCustName.Text = oRow.Item("Name").ToString.Trim
        lblSupplier.Text = oRow.Item("SupplierName").ToString.Trim
        lblSSI.Text = oRow.Item("Incentive").ToString.Trim
        lblAssetDesc.Text = oRow.Item("Description").ToString.Trim
        lblOTR.Text = FormatNumber(oRow.Item("TotalOTR"), 2)
        lblDP.Text = FormatNumber(oRow.Item("DownPayment"), 2)
        lblS1.Text = oRow.Item("SerialNo1Label").ToString.Trim
        lblS2.Text = oRow.Item("SerialNo2Label").ToString.Trim
        lblSerial1.Text = oRow.Item("SerialNo1").ToString.Trim
        lblSerial2.Text = oRow.Item("SerialNo2").ToString.Trim
        lblYear.Text = oRow.Item("ManufacturingYear").ToString.Trim
        lblUsedNew.Text = oRow.Item("UsedNew").ToString.Trim
        lblUsage.Text = oRow.Item("usage").ToString.Trim
        lblRegName.Text = oRow.Item("OldOwnerAsset").ToString.Trim
        lblRegAddress.Text = oRow.Item("OldOwnerAddress").ToString.Trim
        lblRegRT.Text = oRow.Item("OldOwnerRT").ToString.Trim
        lblRegRW.Text = oRow.Item("OldOwnerRW").ToString.Trim
        lblRegKelurahan.Text = oRow.Item("OldOwnerKelurahan").ToString.Trim
        lblRegKecamatan.Text = oRow.Item("OldOwnerKecamatan").ToString.Trim
        lblRegCity.Text = oRow.Item("OldOwnerCity").ToString.Trim
        lblRegZip.Text = oRow.Item("OldOwnerZipCode").ToString.Trim
        If oRow.Item("TaxDate").ToString.Trim <> "" Then
            lblRegTaxDate.Text = CDate(oRow.Item("TaxDate")).ToString("dd/MM/yyyy")
        End If
        lblRegNotes.Text = oRow.Item("Notes").ToString.Trim
        lblInsInsured.Text = oRow.Item("InsuredBy").ToString.Trim
        lblInsPaid.Text = oRow.Item("PaidBy").ToString.Trim
        lblAO.Text = oRow.Item("AO").ToString.Trim
        lblCA.Text = oRow.Item("CA").ToString.Trim
        lblSurveyor.Text = oRow.Item("Surveyor").ToString.Trim
        lblSalesman.Text = oRow.Item("Salesman").ToString.Trim
        lblSalesSpv.Text = oRow.Item("SalesSpv").ToString.Trim
        lblSuppAdmin.Text = oRow.Item("SuppAdmin").ToString.Trim
    End Sub
    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        If Me.PageSource = "App" Then
            Response.Redirect("ViewApplication.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&Style=" & Me.style & "")
        End If
    End Sub

End Class