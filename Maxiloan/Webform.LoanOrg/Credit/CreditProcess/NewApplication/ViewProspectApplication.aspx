﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewProspectApplication.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewProspectApplication" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Application</title>
    <link href="../../../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - PROSPECT APLIKASI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    ID Branch
                </label>
                <asp:Label ID="lblBranchID" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    No Aplikasi
                </label>
                <asp:Label ID="lblApplicationID" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlPersonal" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DATA PERSONAL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama
                    </label>
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Identitas
                    </label>
                    <asp:Label ID="lblIDType" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No Identitas
                    </label>
                    <asp:Label ID="lblIDNumber" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Kelamin
                    </label>
                    <asp:Label ID="lblGender" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        tempat / Tanggal Lahir
                    </label>
                    <asp:Label ID="lblBirthPlace" runat="server"></asp:Label>&nbsp;/
                    <asp:Label ID="lblBirthDate" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Status Rumah
                    </label>
                    <asp:Label ID="lblHomeStatus" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlCompany" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DATA CORPORATE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Perusahaan
                    </label>
                    <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Perusahaan
                    </label>
                    <asp:Label ID="lblCompanyType" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Status Perusahaan
                    </label>
                    <asp:Label ID="lblCompanyStatus" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Berdiri Sejak tahun
                    </label>
                    <asp:Label ID="lblSinceYear" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                ALAMAT
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Alamat
                </label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    RT/RW
                </label>
                <asp:Label ID="lblRT" runat="server"></asp:Label>/
                <asp:Label ID="lblRW" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kelurahan
                </label>
                <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kecamatan
                </label>
                <asp:Label ID="lblKecamaatan" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kota
                </label>
                <asp:Label ID="lblCity" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kode Pos
                </label>
                <asp:Label ID="lblZipCode" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Telepon-1
                </label>
                <asp:Label ID="lblAreaPhone1" runat="server"></asp:Label>-
                <asp:Label ID="lblPhone1" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Telepon-2
                </label>
                <asp:Label ID="lblAreaPhone2" runat="server"></asp:Label>-
                <asp:Label ID="lblPhone2" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    No Fax
                </label>
                <asp:Label ID="lblAreaFax" runat="server"></asp:Label>&nbsp;-
                <asp:Label ID="lblFax" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    No HandPhone
                </label>
                <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    e-Mail
                </label>
                <asp:Label ID="lblEmail" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlWorkData" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DATA PEKERJAAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Profesi
                    </label>
                    <asp:Label ID="lblProfession" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Lama Kerja
                    </label>
                    <asp:Label ID="lblLengthEmployment" runat="server"></asp:Label>&nbsp;Th
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DATA KONTRAK
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Produk
                </label>
                <asp:Label ID="lblProduct" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Asset
                </label>
                <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Harga OTR
                </label>
                <asp:Label ID="lblOTRPrice" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Uang Muka
                </label>
                <asp:Label ID="lblDownPayment" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Jangka Waktu/Tenor
                </label>
                <asp:Label ID="lblTenor" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Margin Flat
                </label>
                <asp:Label ID="lblFlatRate" runat="server"></asp:Label>&nbsp;%
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Angsuran Pertama
                </label>
                <asp:Label ID="lblFirstInstallment" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Jumlah Angsuran
                </label>
                <asp:Label ID="lblInstallmentAmount" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nilai Jual Kembali (%)
                </label>
                <asp:Label ID="lblResaleValue" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Resiko Angsuran
                </label>
                <asp:Label ID="lblResikoAngsuran" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Perkiraan Jatuh Tempo
                </label>
                <asp:Label ID="lblEstimatedDueDate" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Perusahaan Asuransi
                </label>
                <asp:Label ID="lblInsurance" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    No Polis yang Dimiliki
                </label>
                <asp:Label ID="lblExistingPolicyNumber" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_button">
        <a href="javascript:window.close();">
            <img height="20" src="../../../../Images/ButtonClose.gif" width="100" border="0"></a>
    </div>
    </form>
</body>
</html>
