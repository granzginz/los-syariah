﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewProspectApplication
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property Prospect() As String
        Get
            Return viewstate("Prospect").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Prospect") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_controller As New ApplicationController
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.Prospect = Request("ApplicationID").ToString
            Bindgrid()
        End If
        lblMessage.Text = ""
    End Sub
    Private Sub Bindgrid()
        Dim oProspect As New Parameter.Application
        Dim dttProspect As New DataTable
        With oProspect
            .SpName = "spViewProspectApplication"
            .WhereCond = Me.Prospect
            .strConnection = GetConnectionString
        End With

        dttProspect = m_controller.ViewProspectApplication(oProspect)

        If oProspect.Err <> "" Then
            lblMessage.Text = oProspect.Err
            Exit Sub
        End If
        If dttProspect.Rows.Count > 0 Then
            SetToLabel(dttProspect)
        End If
    End Sub
    Sub SetToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        Dim CustomerType As String
        oRow = oData.Rows(0)
        lblBranchID.Text = oRow.Item("BranchFullName").ToString.Trim
        lblApplicationID.Text = oRow.Item("ApplicationID").ToString.Trim
        CustomerType = oRow.Item("CustomerType").ToString.Trim
        If CustomerType = "C" Then
            pnlPersonal.Visible = False
            pnlWorkData.Visible = False
        Else
            pnlCompany.Visible = False
        End If
        lblName.Text = oRow.Item("Name").ToString.Trim
        lblIDType.Text = oRow.Item("IDType").ToString.Trim
        lblIDNumber.Text = oRow.Item("IDNumber").ToString.Trim
        lblGender.Text = oRow.Item("Gender").ToString.Trim
        lblBirthPlace.Text = oRow.Item("BirthPlace").ToString.Trim
        lblBirthDate.Text = oRow.Item("BirthDate").ToString.Trim
        lblHomeStatus.Text = oRow.Item("HomeStatus").ToString.Trim
        lblAddress.Text = oRow.Item("Address").ToString.Trim
        lblRT.Text = oRow.Item("RT").ToString.Trim
        lblRW.Text = oRow.Item("RW").ToString.Trim
        lblKelurahan.Text = oRow.Item("Kelurahan").ToString.Trim
        lblKecamaatan.Text = oRow.Item("Kecamatan").ToString.Trim
        lblCity.Text = oRow.Item("City").ToString.Trim
        lblZipCode.Text = oRow.Item("ZipCode").ToString.Trim
        lblAreaPhone1.Text = oRow.Item("AreaPhone1").ToString.Trim
        lblPhone1.Text = oRow.Item("Phone1").ToString.Trim
        lblAreaPhone2.Text = oRow.Item("AreaPhone2").ToString.Trim
        lblPhone2.Text = oRow.Item("Phone2").ToString.Trim
        lblAreaFax.Text = oRow.Item("AreaFax").ToString.Trim
        lblFax.Text = oRow.Item("Fax").ToString.Trim
        lblMobilePhone.Text = oRow.Item("MobilePhone").ToString.Trim
        lblEmail.Text = oRow.Item("Email").ToString.Trim
        lblProfession.Text = oRow.Item("ProfessionID").ToString.Trim
        lblLengthEmployment.Text = oRow.Item("EmploymentSinceYear").ToString.Trim
        lblProduct.Text = oRow.Item("ProductID").ToString.Trim & " " & oRow.Item("ProductOfferingID").ToString.Trim
        lblAssetDescription.Text = oRow.Item("Description").ToString.Trim
        lblOTRPrice.Text = FormatNumber(oRow.Item("OTRPrice"), 2)
        lblDownPayment.Text = FormatNumber(oRow.Item("DPAmount"), 2)
        lblTenor.Text = oRow.Item("Tenor").ToString.Trim
        lblInstallmentAmount.Text = FormatNumber(oRow.Item("InstallmentAmount"), 2)
        lblFlatRate.Text = oRow.Item("FlatRate").ToString.Trim
        lblFirstInstallment.Text = oRow.Item("FirstInstallment").ToString.Trim
        lblResaleValue.Text = FormatNumber(oRow.Item("ResaleValue"), 2)
        lblResikoAngsuran.Text = FormatNumber(oRow.Item("ResikoAngsuran"), 2)
        lblEstimatedDueDate.Text = oRow.Item("DueDate").ToString.Trim
        lblInsurance.Text = oRow.Item("InsuranceComBranchName").ToString.Trim
        lblExistingPolicyNumber.Text = oRow.Item("ExistingPolicyNo").ToString.Trim
        lblCompanyName.Text = oRow.Item("Name").ToString.Trim
        lblCompanyType.Text = oRow.Item("IndustryTypeID").ToString.Trim
        lblCompanyStatus.Text = oRow.Item("CompanyStatus").ToString.Trim
        lblSinceYear.Text = oRow.Item("EstablishedSinceYear").ToString.Trim

    End Sub

End Class