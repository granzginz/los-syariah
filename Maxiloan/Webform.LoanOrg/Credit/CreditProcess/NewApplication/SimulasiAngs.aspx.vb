﻿
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class SimulasiAngs
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private Const style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim RunRate As Double
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim m_controller As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController
    Dim TotAngs As Double
    Dim TotPokok As Double
    Dim TotBunga As Double
    Dim TotSisaPokok As Double
    Dim TotSisaBunga As Double

#End Region

#Region "Property"
    Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
    Property Installment() As Double
        Get
            Return CDbl(ViewState("Installment"))
        End Get
        Set(ByVal Value As Double)
            ViewState("Installment") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(ViewState("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(ViewState("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(ViewState("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            ViewState("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(ViewState("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("RejectMinimumIncome") = Value
        End Set
    End Property
    Property First() As String
        Get
            Return ViewState("First").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("First") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property Supplier() As String
        Get
            Return ViewState("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Supplier") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property

    Property IsIncentiveSupplier() As Boolean
        Get
            Return CType(ViewState("IsIncentiveSupplier"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsIncentiveSupplier") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            txtAmount.Text = "0"
            txtEffectiveDate.Text = Format(Now, "dd/MM/yyyy")
            pnlEntryInstallment.Visible = False
            pnlViewST.Visible = False
            pnlInstallGrid.Visible = False
            txtStep.Enabled = False
            txtCummulative.Enabled = False
            rvStep.Enabled = False
            rvCummulative.Enabled = False
            ChangeSimlType()

            TotAngs = 0
            TotBunga = 0
            TotPokok = 0
            TotSisaBunga = 0
            TotSisaPokok = 0

            If Request.QueryString("strFileLocation") <> "" Then
                OpenReport()

            End If
        End If
        lblMessage.Text = ""
        lblFlatRate.Visible = False
    End Sub

    Private Sub OpenReport()
        'Dim strHTTPServer As String
        'Dim StrHTTPApp As String
        'Dim strNameServer As String
        Dim strFileLocation As String

        'strHTTPServer = Request.ServerVariables("PATH_INFO")
        'strNameServer = Request.ServerVariables("SERVER_NAME")
        'StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        strFileLocation = "../../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
        Response.Write("<script language = javascript>" & vbCrLf _
        & "var x = screen.width;" & vbCrLf _
        & "var y = screen.height;" & vbCrLf _
        & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width= ' + x + ', height=' + y +' , menubar=0, scrollbars=yes') " & vbCrLf _
        & "</script>")
    End Sub
#End Region

    Sub Behaviour(ByVal textbox As TextBox, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As ImageButton)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MinimumValue = textbox.Text.Trim
                rv.MaximumValue = "999999999999999"
                rv.ErrorMessage = "Input harus >= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MaximumValue = textbox.Text.Trim
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus <= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "999999999999999"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input dengan Angka "
        End Select
    End Sub
    Sub BehaviourPersen(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As ImageButton)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                textbox.Text = value
                rv.Enabled = True
                rv.MinimumValue = value
                rv.MaximumValue = "100"
                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                textbox.Text = value
                rv.Enabled = True
                rv.MaximumValue = value
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >=0 and <= " & value

                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                textbox.Text = value
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "100"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >= 0 dan <= 100"
        End Select
    End Sub
    Sub BehaviourPersenGY(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As ImageButton)
        Select Case Key
            Case "L"
                imb.Visible = False
                lblMessage.Text = ""
            Case "N"
                If CDbl(textbox.Text.Trim) < CDbl(value) Then
                    If Flag = "Product" Then
                        lblMessage.Text = "Nilai Gross Yield adalah  " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari produk) "
                        Me.Status = True
                    ElseIf Flag = "ProductBranch" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) "
                        Me.Status = True
                    ElseIf Flag = "ProductOffering" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) "
                        Me.Status = True
                    End If
                End If
                imb.Visible = True
            Case "X"
                If CDbl(textbox.Text.Trim) > CDbl(value) Then
                    If Flag = "Product" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari produk) "
                        Me.Status = True
                    ElseIf Flag = "ProductBranch" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) "
                        Me.Status = True
                    ElseIf Flag = "ProductOffering" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) "
                        Me.Status = True
                    End If
                End If

                imb.Visible = True
            Case "D"
                rv.Enabled = True
                imb.Visible = True
                If (CDbl(textbox.Text.Trim) < 0 And CDbl(textbox.Text.Trim) > 100) Then
                    lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= 100!"
                    Me.Status = True
                End If
        End Select
    End Sub

#Region "ImbCalculate"
    Private Sub BtnCalcInst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCalcInst.Click
        Dim oEntities As New Parameter.FinancialData
        Dim Inst As Double

        If rdoSTTYpe.SelectedValue.Trim = "RL" Or rdoSTTYpe.SelectedValue.Trim = "LS" Then
            If CInt(txtCummulative.Text) > CInt(txtNumInst.Text) Then
                lblMessage.Text = "Kumulatif  < Jangka waktu Angsuran"
                Exit Sub
            End If

        End If

        If CInt(txtEffectiveRate.Text) <= 0 Then
            lblMessage.Text = "Bunga Efective harus > 0"
            Exit Sub
        End If

        BtnRecalcEffRate.Enabled = True
        BtnRecalcEffRate.Visible = True
        pnlViewST.Visible = False

        If cboInstScheme.SelectedValue.Trim = "RF" Then
            Try
                Inst = InstAmt(CDbl(txtEffectiveRate.Text))
                txtInstAmt.Text = (Math.Ceiling(Math.Round(Inst, 0) / 1000) * 1000).ToString
                FlatRate()

            Catch ex As Exception
                lblMessage.Text = "Bunga Effective salah "
            End Try
        ElseIf cboInstScheme.SelectedValue.Trim = "IR" Then
            BuatTabelInstallment()
            BtnSave.Enabled = True
            BtnSave.Visible = True
            txtInstAmt.Enabled = True
        ElseIf cboInstScheme.SelectedValue.Trim = "ST" Then
            If rdoSTTYpe.SelectedValue.Trim = "NM" Then
                BuatTabelInstallment()
            ElseIf rdoSTTYpe.SelectedValue.Trim = "RL" Then
                Entities.NTF = CDbl(txtNTF.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.CummulativeStart = CInt(txtCummulative.Text)
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                Entities = m_controller.GetInstAmtStepUpDownRegLeasing(Entities)
                txtInstAmt.Text = Math.Round(Entities.InstallmentAmount, 0).ToString.Trim
                BtnSave.Enabled = True
                BtnSave.Visible = True
                txtInstAmt.Enabled = True
            Else
                BuatTabelInstallment()
            End If
            '--- view installment ----'
            If rdoSTTYpe.SelectedValue.Trim <> "NM" And rdoSTTYpe.SelectedValue.Trim <> "RS" And rdoSTTYpe.SelectedValue.Trim <> "LS" Then

                ViewInstallment()
            End If
        Else
            'lblInstScheme.Text = "Even Principle Installment Scheme"
            Try
                With oEntities
                    .NTF = CDbl(txtNTF.Text)
                    .NumOfInstallment = CInt(txtNumInst.Text)
                    .GracePeriod = CInt(txtGracePeriod.Text.Trim)
                End With
                oEntities = m_controller.GetPrincipleAmount(oEntities)
                Me.PrincipleAmount = oEntities.Principal
            Catch ex As Exception
                lblMessage.Text = "Pokok Salah "
            End Try
        End If
    End Sub
    Private Function IsValidValidation(ByVal textbox As TextBox, ByVal CompareText As TextBox, ByVal Message As String) As Boolean
        If textbox.Text.Trim = "" Then
            lblMessage.Text = Message & " harap diisi"
            Return False
        ElseIf CDbl(textbox.Text) = 0 Then
            lblMessage.Text = Message & " harus > 0 !"
            Return False
        ElseIf CDbl(textbox.Text) > CDbl(CompareText.Text) Then
            lblMessage.Text = Message & " harus < Jangka waktu Angsuran"
            Return False
        End If
        Return True
    End Function
    Function InstAmt(ByVal Rate As Double) As Double
        Dim ReturnVal As Double
        RunRate = (Rate / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = Math.Round(m_controller.GetInstAmtAdv(RunRate, CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInst.Text), CType(txtNTF.Text, Double)), 0)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInst.Text) Then
                    lblMessage.Text = "Grace Period harus lebih kecil dari jangka waktu Angsuran"
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInst.Text) - CInt(txtGracePeriod.Text.Trim), _
                    CType(txtNTF.Text, Double)), 0)
                ElseIf cboGracePeriod.SelectedValue = "R" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArrRO(CDbl(txtEffectiveRate.Text), CInt(txtNumInst.Text), _
                    RunRate, CType(txtNTF.Text, Double), _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim)), 0)
                ElseIf CInt(txtGracePeriod.Text) = 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                    CInt(txtNumInst.Text), CType(txtNTF.Text, Double)), 0)
                End If
            Else
                ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                CInt(txtNumInst.Text), CType(txtNTF.Text, Double)), 0)
            End If
        End If
        Return ReturnVal
    End Function
    Function CountRate(ByVal NTF As Double) As Double
        Dim ReturnVal As Double
        Dim intJmlInst As Integer
        If cboInstScheme.SelectedValue.Trim = "ST" And rdoSTTYpe.SelectedValue.Trim = "RL" Then
            intJmlInst = CInt(txtCummulative.Text)
        Else
            intJmlInst = CInt(txtNumInst.Text)
        End If
        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = m_controller.GetEffectiveRateAdv(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
            Context.Trace.Write("Bila Value First Installment sama dengan AD =" & ReturnVal)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInst.Text) Then
                    lblMessage.Text = "Grace Period harus lebih kecil dari jangka waktu angsuran "
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" Then
                    ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst - CInt(txtGracePeriod.Text.Trim), CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
                Else
                    ReturnVal = m_controller.GetEffectiveRateRO(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim))
                End If
            Else
                ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
            End If
        End If
        Return ReturnVal
    End Function
    Private Sub FlatRate()
        Dim FlateRate As Double
        FlateRate = 100 * ((CDbl(txtInstAmt.Text) * CDbl(txtTenor.Text)) - CDbl(txtNTF.Text.Trim)) / CDbl(txtNTF.Text.Trim)
        lblFlatRate.Text = FormatNumber(FlateRate, 8) & "% per " & txtTenor.Text & " month "
        lblFlatRate.Visible = True
    End Sub
#End Region

#Region "Save"
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim FlatRate As Double
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData
        Dim dsAmor As New DataSet

        Me.Status = False

        GetGrossYield()



        If Me.NTFGrossYield <= 0 Then
            lblMessage.Text = "Simpan data gagal.  NTF untuk Gross Yield  <= 0, Gross yield adalah  " & Me.NTFGrossYield
            Exit Sub
        End If

        If cboInstScheme.SelectedValue.Trim = "RF" Then
            'untuk regular
            If CDbl(txtInstAmt.Text) <> InstAmt(CDbl(txtEffectiveRate.Text)) Then
                lblMessage.Text = "Jumlah Angsuran dan Bunga Effective Tidak syncronized. harap klik tombol Calculate Installment Amount untuk menghitung Effective Rate"
                Exit Sub
            End If

            'untuk regular
            If cboFirstInstallment.SelectedValue = "AD" Then
                dsAmor = MakeAmortTable1(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(txtNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue))
            ElseIf cboFirstInstallment.SelectedValue = "AR" Then
                If (txtGracePeriod.Text.Trim = "0" Or txtGracePeriod.Text.Trim = "") Then
                    dsAmor = MakeAmortTable2(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(txtNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue))
                ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "I" Then
                    dsAmor = MakeAmortTable6(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(txtNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "R" Then
                    dsAmor = MakeAmortTable3(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(txtNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                End If
            End If
            oFinancialData.MydataSet = dsAmor
        End If

        If cboInstScheme.SelectedValue.Trim = "RF" Then
            'untuk regular              
            If Me.Status = True Then
                Exit Sub
            End If

            oFinancialData.DiffRate = CDbl(txtAmount.Text.Trim)
            oFinancialData.GrossYield = GetGrossYield()

            If HitungUlangSisa Then
                dblInterestTotal = Math.Round(dblInterestTotal + SisaPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
            If HitungUlangKurang Then
                dblInterestTotal = Math.Round(dblInterestTotal - KurangPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
        End If

        If dblInterestTotal < Me.RejectMinimumIncome Then
            lblMessage.Text = "Total Interest < Reject Minimum Income! Process tidak dapat di lanjutkan "
        Else
            If cboInstScheme.SelectedValue.Trim = "RF" Then
                'untuk regular    
                FlatRate = GetFlatRate(dblInterestTotal, CDbl(txtNTF.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtTenor.Text))
                oFinancialData.FlatRate = FlatRate
                oFinancialData.InstallmentAmount = CDbl(txtInstAmt.Text)
                oFinancialData.NumOfInstallment = CInt(txtNumInst.Text)
                oFinancialData.FloatingPeriod = CType(IIf(cboInterestType.SelectedValue.Trim = "FL", cboFloatingPeriod.SelectedValue.ToString, ""), String)
            ElseIf cboInstScheme.SelectedValue.Trim = "IR" Then
                dsAmor = GetEntryInstallment()
                oFinancialData.MydataSet = dsAmor
                oFinancialData.IsSave = 1
            ElseIf cboInstScheme.SelectedValue.Trim = "ST" Then
                If rdoSTTYpe.SelectedValue.Trim = "NM" Then
                    dsAmor = GetEntryInstallmentStepUpDown()
                ElseIf rdoSTTYpe.SelectedValue.Trim = "RL" Then
                    Entities.NTF = CDbl(txtNTF.Text)
                    Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Entities.NumOfInstallment = CInt(txtNumInst.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities = m_controller.GetStepUpDownRegLeasingTBL(Entities)
                    dsAmor = Entities.MydataSet
                Else
                    dsAmor = GetInstallmentAmountStepUpDownLeasingTBL()
                End If
                oFinancialData.MydataSet = dsAmor
            End If

            Me.DiffRate = CDbl(txtAmount.Text)

            oFinancialData.BranchId = "XXX"
            oFinancialData.AppID = "XXXXXXXXXXXXXXXXXXXX"
            oFinancialData.NTF = CDbl(txtNTF.Text)
            oFinancialData.EffectiveRate = CDbl(txtEffectiveRate.Text)
            oFinancialData.SupplierRate = CDbl(txtEffectiveRate.Text)
            oFinancialData.PaymentFrequency = cboPaymentFreq.SelectedValue
            oFinancialData.FirstInstallment = cboFirstInstallment.SelectedValue
            oFinancialData.Tenor = CInt(txtTenor.Text)
            oFinancialData.GracePeriod = CInt(IIf(txtGracePeriod.Text.Trim = "", "0", txtGracePeriod.Text.Trim))
            oFinancialData.GracePeriodType = cboGracePeriod.SelectedValue
            oFinancialData.BusDate = Me.BusinessDate
            oFinancialData.DiffRate = Me.DiffRate
            oFinancialData.Flag = "Add"
            oFinancialData.InterestType = cboInterestType.SelectedValue.Trim
            oFinancialData.strConnection = GetConnectionString()

            Try
                If cboInstScheme.SelectedValue.Trim = "RF" Then
                    oFinancialData = m_controller.SaveFinancialDataSiml(oFinancialData)
                ElseIf cboInstScheme.SelectedValue.Trim = "IR" Then
                    oFinancialData = m_controller.SaveAmortisasiIRRSiml(oFinancialData)
                ElseIf cboInstScheme.SelectedValue.Trim = "ST" Then
                    oFinancialData = m_controller.SaveAmortisasiStepUpDownSiml(oFinancialData)
                Else
                    oFinancialData = m_controller.SaveAmortisasiSiml(oFinancialData)
                End If
                UpdateInstallmentSchDueDate()

            Catch ex As Exception
                lblMessage.Text = ex.Message
            End Try

            If oFinancialData.Output = "" Then
                Try
                    Dim oFinancialData1 As New Parameter.FinancialData
                    oFinancialData1.strConnection = GetConnectionString()
                    pnlInstallGrid.Visible = True
                    pnlInput.Visible = False
                    Datagrid1.DataSource = m_controller.GetInstallmentSchSiml(oFinancialData1)
                    Datagrid1.DataBind()

                    'set Data Finansial
                    setDataFinansialInSimulasi()
                Catch ex As Exception
                    lblMessage.Text = ex.Message
                End Try
            Else
                lblMessage.Text = "Error: Proses Simpan Data " & oFinancialData.Output & ""
            End If
        End If
    End Sub
#End Region


    Private Sub setDataFinansialInSimulasi()
        lblNTF.Text = FormatNumber(CStr(txtNTF.Text), 2).ToString
        lblTglEfektif.Text = txtEffectiveDate.Text
        lblBungaEfektif.Text = FormatNumber(CStr(txtEffectiveRate.Text), 2).ToString
        lblTenor.Text = txtTenor.Text
        lblAngsuranPertama.Text = FormatNumber(CStr(txtInstAmt.Text), 2).ToString
    End Sub

    Private Sub UpdateInstallmentSchDueDate()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spUpdateInstallmentSchSml"
            objCommand.Parameters.Add("@EffectiveDate", SqlDbType.Char, 8).Value = ConvertDate(txtEffectiveDate.Text)
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Value = "XXX"
            objCommand.Parameters.Add("@ApplicationId", SqlDbType.VarChar, 20).Value = "XXXXXXXXXXXXXXXXXXXX"
            objCommand.Parameters.Add("@PaymentFrequency", SqlDbType.Char, 1).Value = cboPaymentFreq.SelectedValue.Trim
            objCommand.Parameters.Add("@FirstInstallment", SqlDbType.Char, 2).Value = cboFirstInstallment.SelectedValue.Trim
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

#Region "View Installment Step Up Down"
    Private Sub ViewInstallment()
        Dim oFinancialData As New Parameter.FinancialData
        pnlViewST.Visible = True
        If cboInstScheme.SelectedValue.Trim = "ST" Then
            If rdoSTTYpe.SelectedValue.Trim = "NM" Then
                oFinancialData.MydataSet = GetEntryInstallmentStepUpDown()
            ElseIf rdoSTTYpe.SelectedValue.Trim = "RL" Then
                Entities.NTF = CDbl(txtNTF.Text)
                Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.CummulativeStart = CInt(txtCummulative.Text)
                Entities.NumOfInstallment = CInt(txtNumInst.Text)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities = m_controller.GetStepUpDownRegLeasingTBL(Entities)
                oFinancialData.MydataSet = Entities.MydataSet
            Else
                oFinancialData.MydataSet = GetInstallmentAmountStepUpDownLeasingTBL()
            End If
        Else
            pnlViewST.Visible = False
        End If


        oFinancialData.BranchId = "XXX"
        oFinancialData.AppID = "XXXXXXXXXXXXXXXXXXXX"
        oFinancialData.NTF = CDbl(txtNTF.Text)
        oFinancialData.EffectiveRate = CDbl(txtEffectiveRate.Text)
        oFinancialData.SupplierRate = CDbl(txtEffectiveRate.Text)
        oFinancialData.PaymentFrequency = cboPaymentFreq.SelectedValue
        oFinancialData.FirstInstallment = cboFirstInstallment.SelectedValue
        oFinancialData.Tenor = CInt(txtTenor.Text)
        oFinancialData.GracePeriod = CInt(IIf(txtGracePeriod.Text.Trim = "", "0", txtGracePeriod.Text.Trim))
        oFinancialData.GracePeriodType = cboGracePeriod.SelectedValue
        oFinancialData.BusDate = Me.BusinessDate
        oFinancialData.DiffRate = Me.DiffRate
        oFinancialData.Flag = "Add"
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData = m_controller.ListAmortisasiStepUpDown(oFinancialData)
        dtgViewInstallment.DataSource = oFinancialData.ListData
        dtgViewInstallment.DataBind()
    End Sub
#End Region

#Region "DiffRate, GrossYield, FlatRate"
    Function GetGrossYield() As Double
        Me.NTFGrossYield = CDbl(txtNTF.Text) + Me.DiffRate
        Return CountRate(Me.NTFGrossYield)
    End Function
    Public Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Integer) As Double
        Return (dblInterestTotal * 1200) / (dblNTF * CDbl(txtTenor.Text))
    End Function
#End Region

#Region "Make Amortization Table"
    Public Sub BuatTable()
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub

    Private Sub BuatTabelInstallment()
        Dim inc As Integer
        Dim intRowMax As Integer

        pnlEntryInstallment.Visible = True
        intRowMax = CInt(txtNumInst.Text) - 1

        If cboInstScheme.SelectedValue.Trim = "ST" Then
            If CInt(txtStep.Text) > CInt(txtNumInst.Text) Then
                lblMessage.Text = "Number of Step harus < Jangka waktu Angsuran"
                pnlEntryInstallment.Visible = False

                Exit Sub
            End If

            pnlEntryInstallment.Visible = True
            intRowMax = CInt(txtStep.Text) - 1
        End If

        myDataTable = New DataTable("Installment")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)


        For inc = 1 To intRowMax
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("NoOfStep") = 0
            myDataRow("Amount") = 0
            myDataTable.Rows.Add(myDataRow)
        Next inc
        If cboInstScheme.SelectedValue.Trim = "ST" Then
            dtgEntryInstallment.Columns(1).Visible = True
        Else
            dtgEntryInstallment.Columns(1).Visible = False
        End If
        dtgEntryInstallment.DataSource = myDataTable.DefaultView
        dtgEntryInstallment.DataBind()

    End Sub
    Public Function MakeAmortTable1(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Advance - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)

        Dim TotalPrincipal As Double

        PokokHutang(0) = dblNTF
        Bunga(0) = 0
        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Advance dan installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i = 2 Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = 0
                myDataRow("Principal") = dblInstAmt
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If

            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If

            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet

        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function

    Public Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Public Function MakeAmortTable3(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Roll Over)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = 0
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(CDbl(myDataRow("Installment")) - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Public Function MakeAmortTable6(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Interest Only)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = Math.Round(dblNTF * dblRunRate / m_controller.GetTerm(intPayFreq) / 100, 2)
                myDataRow("Interest") = myDataRow("Installment")
                myDataRow("Principal") = 0
            ElseIf i = intGracePeriod + 1 Then
                myDataRow("No") = i - 1
                myDataRow("Interest") = Math.Round(dblNTF * dblRunRate / m_controller.GetTerm(intPayFreq) / 100, 2)
                myDataRow("Installment") = dblInstAmt
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
#End Region

#Region "Entry Installment Irregular"
    Private Function GetEntryInstallment() As DataSet
        Dim inc As Integer
        Dim jmlRow As Integer
        Dim InstallmentAmount As Double
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.FinancialData

        jmlRow = dtgEntryInstallment.Items.Count
        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("NoOfStep") = CDbl(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        objDs.Tables.Add(myDataTable)
        Return objDs
    End Function
    Private Function GetEntryInstallmentStepUpDown() As DataSet
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim intNumOfInst As Integer
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)

        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
                inc3 = inc3 + 1
            Next
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        InstallmentAmount = CDbl(txtInstAmt.Text)
        If inc3 <= CInt(txtNumInst.Text) Then
            For inc = 1 To intSisa
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
                inc3 = inc3 + 1
            Next
        End If
        objDs.Tables.Add(myDataTable)
        Return objDs
    End Function

    Private Function GetInstallmentAmountStepUpDown() As Double
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(txtNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
            Next
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1 '((PokokHutang(0) - TotalInstallment) + TotalInterest) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        Return InstallmentAmount
    End Function
    Private Function GetInstallmentAmountStepUpDownLeasing() As Double
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(txtNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
            Next
        Next
        intSisa = (CInt(txtCummulative.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        Return InstallmentAmount
    End Function
    Private Function GetInstallmentAmountStepUpDownLeasingTBL() As DataSet
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(txtNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
            Next
        Next
        intSisa = (CInt(txtCummulative.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        For inc = 1 To intSisa - 1
            inc3 = inc3 + 1
            Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
            Principal(inc3) = InstallmentAmount - Interest(inc3)
            PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)

            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc3
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        For inc = 1 To intSisa
            inc3 = inc3 + 1
            Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
            Principal(inc3) = InstallmentAmount - Interest(inc3)
            PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)

            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc3
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        myDataSet.Tables.Add(myDataTable)
        Return myDataSet
    End Function

    Private Sub BtnEntryInstallment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnEntryInstallment.Click
        Dim oFinancialData As New Parameter.FinancialData
        Dim oDs As New DataSet
        If cboInstScheme.SelectedValue.Trim = "ST" Then
            If IsValidEntry(dtgEntryInstallment, CDbl(txtNTF.Text), CInt(txtNumInst.Text), CInt(txtCummulative.Text), rdoSTTYpe.SelectedValue.Trim) = False Then
                lblMessage.Text = "Angsuran Salah"
                Exit Sub
            End If

            If rdoSTTYpe.SelectedValue.Trim = "NM" Then
                txtInstAmt.Text = Math.Round(GetInstallmentAmountStepUpDown(), 0).ToString.Trim
            Else
                txtInstAmt.Text = Math.Round(GetInstallmentAmountStepUpDownLeasing(), 0).ToString.Trim
            End If

            BtnSave.Enabled = True
            BtnSave.Visible = True
            txtInstAmt.Enabled = True
        Else
            With oFinancialData
                .BranchId = "XXX"
                .AppID = "XXXXXXXXXXXXXXXXXXXX"
                .NTF = CDbl(txtNTF.Text)
                .EffectiveRate = CDbl(txtEffectiveRate.Text)
                .SupplierRate = CDbl(txtEffectiveRate.Text)
                .PaymentFrequency = cboPaymentFreq.SelectedValue
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .Tenor = CInt(txtTenor.Text)
                .GracePeriod = CInt(IIf(txtGracePeriod.Text.Trim = "", "0", txtGracePeriod.Text.Trim))
                .GracePeriodType = cboGracePeriod.SelectedValue
                .BusDate = Me.BusinessDate
                .DiffRate = Me.DiffRate
                .strConnection = GetConnectionString()
                .MydataSet = GetEntryInstallment()
                .IsSave = 0
                .Flag = "Add"
            End With
            oFinancialData = m_controller.SaveAmortisasiIRR(oFinancialData)
            txtInstAmt.Text = Math.Round(oFinancialData.InstallmentAmount, 0).ToString.Trim
        End If
        Call ViewInstallment()
    End Sub

    Private Function IsValidEntry(ByVal Dg As DataGrid, ByVal NTF As Double, ByVal NumOfIns As Integer, ByVal Cummulative As Integer, ByVal StepUpDownType As String) As Boolean
        Dim inc As Integer
        Dim intStep As Integer
        Dim intTotStep As Integer
        Dim dblAmount As Double
        Dim dblTotAmount As Double
        intTotStep = 0
        dblTotAmount = 0
        For inc = 0 To Dg.Items.Count - 1
            Dim txtStep As New TextBox
            Dim txtAmount As New TextBox
            txtStep = CType(Dg.Items(inc).FindControl("txtNoStep"), TextBox)
            txtAmount = CType(Dg.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            If StepUpDownType = "NM" Then
                dblAmount = CDbl(txtAmount.Text)
                dblTotAmount = dblTotAmount + dblAmount
                intStep = CInt(txtStep.Text)
                intTotStep = intTotStep + intStep
            ElseIf StepUpDownType = "RL" Then
                intStep = CInt(txtStep.Text)
                intTotStep = intTotStep + intStep
            Else
                intStep = CInt(txtStep.Text)
                dblAmount = CDbl(txtAmount.Text) * intStep
                intTotStep = intTotStep + intStep
                dblTotAmount = dblTotAmount + dblAmount
            End If
        Next
        If StepUpDownType = "NM" Then
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If dblTotAmount >= NTF Then
                Return False
            End If
        ElseIf StepUpDownType = "RL" Then
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If Cummulative < intTotStep Then
                Return False
            End If
        Else
            If Cummulative < intTotStep Then
                Return False
            End If
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If dblTotAmount >= NTF Then
                Return False
            End If
        End If
        'If intTotStep < Cummulative Then
        '    Return False
        'End If
        Return True
    End Function
#End Region

    Private Sub cboInterestType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboInterestType.SelectedIndexChanged
        ChangeSimlType()
    End Sub

    Private Sub cboInstScheme_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboInstScheme.SelectedIndexChanged
        ChangeSimlType()
    End Sub

    Private Sub ChangeSimlType()
        If cboInterestType.SelectedValue.Trim <> "FL" Then
            cboFloatingPeriod.Enabled = False
            reqFloatingPeriod.Enabled = False
        Else
            cboFloatingPeriod.Enabled = True
            reqFloatingPeriod.Enabled = True
        End If

        If cboInterestType.SelectedValue.Trim = "FX" Then
            cboFloatingPeriod.Visible = False
            lblFloatingPeriod.Visible = False
            If cboInstScheme.SelectedValue.Trim = "ST" Then
                If rdoSTTYpe.SelectedValue.Trim = "NM" Then
                    txtStep.Visible = True
                    lblNumberOfStep.Visible = True
                    txtCummulative.Visible = False
                    lblCummulative.Visible = False
                ElseIf rdoSTTYpe.SelectedValue.Trim = "RL" Then
                    txtStep.Visible = False
                    lblNumberOfStep.Visible = False
                    txtCummulative.Visible = True
                    lblCummulative.Visible = True
                ElseIf rdoSTTYpe.SelectedValue.Trim = "LS" Then
                    txtStep.Visible = True
                    lblNumberOfStep.Visible = True
                    txtCummulative.Visible = True
                    lblCummulative.Visible = True
                End If
            Else
                txtStep.Visible = False
                txtCummulative.Visible = False
                lblNumberOfStep.Visible = False
                lblCummulative.Visible = False
            End If
        End If

        If cboInterestType.SelectedValue.Trim = "FL" Then
            cboFloatingPeriod.Visible = True
            lblFloatingPeriod.Visible = True
            txtStep.Visible = False
            txtCummulative.Visible = False
            lblNumberOfStep.Visible = False
            lblCummulative.Visible = False
        End If
        If rdoSTTYpe.SelectedValue.Trim = "NM" Or rdoSTTYpe.SelectedValue.Trim = "LS" Then
            BtnRecalcEffRate.Visible = False
        Else
            BtnRecalcEffRate.Enabled = True
            BtnRecalcEffRate.Visible = True
        End If
        If cboInstScheme.SelectedValue.Trim = "RF" Then
            If cboFirstInstallment.SelectedValue = "AD" Then
                txtGracePeriod.Enabled = False
                txtGracePeriod.Text = "0"
                cboGracePeriod.Enabled = False
                cboGracePeriod.ClearSelection()
            Else
                txtGracePeriod.Enabled = True
                cboGracePeriod.Enabled = True
            End If
        ElseIf cboInstScheme.SelectedValue.Trim = "IR" Then
            lblInstallment.Text = "Last Installment"
            RVInstAmt.Enabled = False
            txtInstAmt.Enabled = False
            txtGracePeriod.Enabled = False
            cboGracePeriod.Enabled = False
            BtnSave.Enabled = False
            BtnSave.Visible = False
        ElseIf cboInstScheme.SelectedValue.Trim = "ST" Then
            RVInstAmt.Enabled = False
            txtInstAmt.Enabled = False
            txtGracePeriod.Enabled = False
            cboGracePeriod.Enabled = False
            BtnSave.Enabled = False
            BtnSave.Visible = False

            If rdoSTTYpe.SelectedValue.Trim = "NM" Then
                txtStep.Enabled = True
                txtCummulative.Enabled = False
                rvStep.Enabled = True
            ElseIf rdoSTTYpe.SelectedValue.Trim = "RL" Then
                txtStep.Enabled = False
                txtCummulative.Enabled = True
                rvCummulative.Enabled = True
            Else
                txtStep.Enabled = True
                txtCummulative.Enabled = True
                rvStep.Enabled = True
                rvCummulative.Enabled = True
            End If
        Else
            txtInstAmt.Enabled = False
            BtnRecalcEffRate.Visible = False
            If cboFirstInstallment.SelectedValue = "AD" Then
                txtGracePeriod.Enabled = False
                txtGracePeriod.Text = "0"
                cboGracePeriod.Enabled = False
                cboGracePeriod.ClearSelection()
            Else
                txtGracePeriod.Enabled = True
                cboGracePeriod.Enabled = True
            End If
        End If
    End Sub

    Private Sub rdoSTTYpe_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoSTTYpe.SelectedIndexChanged
        ChangeSimlType()
    End Sub

    Private Sub BtnRecalcEffRate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRecalcEffRate.Click
        If CInt(txtInstAmt.Text) <= 0 Then
            lblMessage.Text = "Jumlah Angsuran harus > 0"
            Exit Sub
        End If
        Dim Eff As Double
        Dim Supp As Double
        Try
            If cboInstScheme.SelectedValue.Trim = "IR" Then
                Entities.NTF = CDbl(txtNTF.Text)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.MydataSet = GetEntryInstallment()
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Eff = m_controller.GetNewEffRateIRR(Entities)
                txtEffectiveRate.Text = Eff.ToString.Trim
            ElseIf cboInstScheme.SelectedValue.Trim = "ST" Then
                If rdoSTTYpe.SelectedValue.Trim = "NM" Then
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(txtNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = cboInstScheme.SelectedValue.Trim
                    Entities.MydataSet = GetEntryInstallmentStepUpDown()
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Eff = m_controller.GetNewEffRateIRR(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    ViewInstallment()
                ElseIf rdoSTTYpe.SelectedValue.Trim = "RL" Then
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(txtNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateRegLeasing(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    ViewInstallment()
                Else
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(txtNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = cboInstScheme.SelectedValue.Trim
                    Entities.MydataSet = GetEntryInstallmentStepUpDown()
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateLeasing(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    ViewInstallment()
                End If
            Else
                Eff = CountRate(CDbl(txtNTF.Text))
                txtEffectiveRate.Text = Eff.ToString.Trim
                Supp = CountRate(CDbl(txtNTF.Text) - CDbl(txtAmount.Text.Trim))
                FlatRate()
            End If
        Catch ex As Exception
            lblMessage.Text = "Jumlah Angsuran Salah"
        End Try
    End Sub

    Private Sub BtnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        pnlInstallGrid.Visible = False
        pnlInput.Visible = True
    End Sub

    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            'set cookies
            Dim cookie As HttpCookie = Request.Cookies("RptSimulasiAng")
            If Not cookie Is Nothing Then
                cookie.Values("NTF") = txtNTF.Text
                cookie.Values("BungaEffective") = txtEffectiveRate.Text
                cookie.Values("TglEffective") = ConvertDate2(txtEffectiveDate.Text).ToString
                cookie.Values("AngsuranPertama") = txtInstAmt.Text
                cookie.Values("Tenor") = txtTenor.Text
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptSimulasiAng")
                cookieNew.Values.Add("NTF", txtNTF.Text)
                cookieNew.Values.Add("BungaEffective", txtEffectiveRate.Text)
                cookieNew.Values.Add("TglEffective", ConvertDate2(txtEffectiveDate.Text).ToString)
                cookieNew.Values.Add("AngsuranPertama", txtInstAmt.Text)
                cookieNew.Values.Add("Tenor", txtTenor.Text)
                Response.AppendCookie(cookieNew)
            End If

            Response.Redirect("SimulasiAngsRptView.aspx")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub Datagrid1_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Datagrid1.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            TotAngs += CDbl(CType(e.Item.FindControl("lblInstallmentAmount"), Label).Text)
            TotBunga += CDbl(CType(e.Item.FindControl("lblPrincipalAmount"), Label).Text)
            TotPokok += CDbl(CType(e.Item.FindControl("lblInterestAmount"), Label).Text)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("lblInstallmentAmountSum"), Label).Text = FormatNumber(TotAngs, 2)
            CType(e.Item.FindControl("lblPrincipalAmountSum"), Label).Text = FormatNumber(TotBunga, 2)
            CType(e.Item.FindControl("lblInterestAmountSum"), Label).Text = FormatNumber(TotPokok, 2)            
        End If
    End Sub
End Class