﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.IO
Imports System.Collections.Generic
Public Class HasilSurvey
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTab
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat


    Dim pathFolder As String = ""
    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property

    Private Property CSResult_Temp() As String
        Get
            Return CType(ViewState("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CSResult_Temp") = Value
        End Set
    End Property
    Private Property TotalDebet() As Decimal
        Get
            Return (CType(ViewState("TotalDebet"), Decimal))
        End Get
        Set(ByVal TotalDebet As Decimal)
            ViewState("TotalDebet") = TotalDebet
        End Set
    End Property
    Private Property TotalKredit() As Decimal
        Get
            Return (CType(ViewState("TotalKredit"), Decimal))
        End Get
        Set(ByVal TotalKredit As Decimal)
            ViewState("TotalKredit") = TotalKredit
        End Set
    End Property
    Private Property myDataTable() As DataTable
        Get
            Return CType(viewstate("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("myDataTable") = Value
        End Set
    End Property
#End Region
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            Me.ApplicationID = Request("ApplicationID")

            'GetCookies()
            InitialPageLoad()
            'fillFormTesting()
            getHasilSurvey()

            pnlBalance.Visible = False
            pnlHasilSurvey.Visible = True
            pnlScoring.Visible = False

            cekimage()

            If ucViewApplication1.isProceed = True Then
                btnSave.Visible = False
            End If
        End If
    End Sub

    'Sub GetCookies()
    '    Dim cookie As HttpCookie = Request.Cookies("HASIL_SURVEY")

    '    Me.ApplicationID = cookie.Values("ApplicationID")
    '    Me.CustomerID = cookie.Values("CustomerID")
    '    Me.BranchID = cookie.Values("BranchID")
    '    Me.SupplierID = cookie.Values("SupplierID")
    'End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
            End If
        End With

        ucApplicationTab1.ApplicationID = Me.ApplicationID
        ucApplicationTab1.selectedTab("Survey")
        ucApplicationTab1.setLink()



        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        Dim WhereSup As String = "SupplierID = '" & Me.SupplierID & "'"

        'FillCboEmp("BranchEmployee", cboCreditAnalyst, WhereBranch + " and (EmployeePosition='CA' or EmployeePosition='AO') ")
        'FillCboEmp("BranchEmployee", cboSurveyor, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")
        FillCboEmp("BranchEmployee", cboCreditAnalyst, WhereBranch + " and IsCA = 1 ")
        FillCboEmp("BranchEmployee", cboSurveyor, WhereBranch + " And IsSurveyor = 1")
        With txtLamaSurvey
            .Text = "1"
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "1"
            .RangeValidatorMaximumValue = "99"
            .TextCssClass = "numberAlign small_text"
            .setErrMsg()
        End With
        With txtJumlahKendaraan
            .Text = "1"
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "0"
            .RangeValidatorMaximumValue = "99"
            .TextCssClass = "numberAlign small_text"
            .setErrMsg()
        End With
        With txtOrderKe
            .Text = "1"
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "0"
            .RangeValidatorMaximumValue = "99"
            .TextCssClass = "numberAlign small_text"
            .setErrMsg()
        End With
    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            txtTanggalSurvey.Text = .AgreementSurveyDate.ToString("dd/MM/yyyy")

            cboSurveyor.SelectedIndex = cboSurveyor.Items.IndexOf(cboSurveyor.Items.FindByValue(oPar.SurveyorID))
            cboCreditAnalyst.SelectedIndex = cboCreditAnalyst.Items.IndexOf(cboCreditAnalyst.Items.FindByValue(oPar.CAID))
            txtSurveyorNotes.Text = .SurveyorNotes.ToString
            txtAnalisaNasabah.Text = .AnalisaNasabah.ToString
            txtAnalisaTempatTinggal.Text = .AnalisaTempatTinggal.ToString
            txtAnalisaKendaraan.Text = .AnalisaKendaraan.ToString

            txtLamaSurvey.Text = .LamaSurvey.ToString
            txtJumlahKendaraan.Text = FormatNumber(.JumlahKendaraan, 0)
            rboGarasi.SelectedIndex = rboGarasi.Items.IndexOf(rboGarasi.Items.FindByValue(oPar.Garasi.ToString.Trim))
            If oPar.PertamaKredit.ToString.Trim <> "" Then
                cboPertamaKredit.SelectedIndex = cboPertamaKredit.Items.IndexOf(cboPertamaKredit.Items.FindByValue(oPar.PertamaKredit.ToString))
            Else
                cboPertamaKredit.SelectedIndex = cboPertamaKredit.Items.IndexOf(cboPertamaKredit.Items.FindByValue("YA"))
            End If
            txtOrderKe.Text = FormatNumber(oPar.OrderKe.ToString, 0)

            'load Scoring data if exist
            If oPar.creditScore <> 0 Then
                SetScoring()
            End If

            If .isProceed = True Then
                btnProceed.Visible = False
            Else
                btnProceed.Visible = True
            End If

        End With
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect("HasilSurveyList.aspx")
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub

    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.HasilSurvey

        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .SurveyorID = cboSurveyor.SelectedValue
                .CAID = cboCreditAnalyst.SelectedValue
                .AgreementSurveyDate = Date.ParseExact(txtTanggalSurvey.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .BusinessDate = Me.BusinessDate
                .SurveyorNotes = txtSurveyorNotes.Text
                .LamaSurvey = CShort(txtLamaSurvey.Text)
                .JumlahKendaraan = CShort(txtJumlahKendaraan.Text)
                .Garasi = CBool(rboGarasi.SelectedValue)
                .PertamaKredit = cboPertamaKredit.SelectedValue
                .OrderKe = CShort(txtOrderKe.Text)
                .AnalisaNasabah = txtAnalisaNasabah.Text
                .AnalisaTempatTinggal = txtAnalisaTempatTinggal.Text
                .AnalisaKendaraan = txtAnalisaKendaraan.Text
            End With

            'save scoring
            Dim scoringData As New Parameter.CreditScoring
            With scoringData
                '.ApplicationStep = "SCO"
                .ApplicationStep = "APK"
                .CreditScoringDate = Me.BusinessDate
                .CreditScore = Me.CreditScore
                .CreditScoringResult = Me.CreditScoreResult
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
                .CSResult_Temp = Me.CSResult_Temp
            End With


            Dim oParBalance As New Parameter.HasilSurvey
            Dim dataBalance As DataTable
            With oParBalance
                .BusinessDate = Me.BusinessDate
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
            End With
            oController.HasilSurveyJournalSave(oParBalance)
            dataBalance = oParBalance.ListData

            dtgJournal.DataSource = dataBalance
            dtgJournal.DataBind()

            If TotalDebet = TotalKredit Then
                oController.HasilSurveySave(oPar)

                SetScoring()

                Dim errMsg As String = saveScoring()


                ucApplicationTab1.ApplicationID = Me.ApplicationID
                ucApplicationTab1.selectedTab("Survey")
                ucApplicationTab1.setLink()
                InitialPageLoad()
                getHasilSurvey()

                If errMsg = String.Empty Then
                    ShowMessage(lblMessage, "Data saved!", False)
                Else
                    ShowMessage(lblMessage, "Data saved! but scoring data error :  " & errMsg, True)
                End If
            Else
                pnlBalance.Visible = True
                pnlHasilSurvey.Visible = False
                pnlScoring.Visible = False
                ShowMessage(lblMessage, "Data Not Balance, Debit : " & FormatNumber(TotalDebet, 0) & " Credit : " & FormatNumber(TotalKredit, 0) & "", True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Scoring"


    Private Sub SetScoring()
        Dim scoringData As New Parameter.CreditScoring_calculate

        With scoringData
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationId = Me.ApplicationID
        End With
        scoringData = CreditScoringMdl.CalculateCreditScoring(scoringData)

        'set result        
        lblGrade.Text = scoringData.lblGrade
        lblResult.Text = scoringData.ReturnResult
        Me.CreditScore = scoringData.CreditScore
        Me.CreditScoreResult = scoringData.CreditScoreResult
        Me.CSResult_Temp = scoringData.CSResult_Temp
        Me.myDataTable = scoringData.DT
        If lblResult.Text <> "" Then
            pnlScoring.Visible = True
        End If
    End Sub

    Private Function saveScoring() As String
        Try
            Dim customClass As New Parameter.CreditScoring
            Dim scoringController As New CreditScoringController
            With customClass
                '.ApplicationStep = "SCO"
                .ApplicationStep = "APK"
                .CreditScoringDate = Me.BusinessDate
                .CreditScore = Me.CreditScore
                .CreditScoringResult = Me.CreditScoreResult
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
                .CSResult_Temp = Me.CSResult_Temp
                .ListData = Me.myDataTable
            End With
            scoringController.CreditScoringSaveAdd(customClass, True)
            Return String.Empty
        Catch ex As Exception
            Return ex.Message
        End Try

    End Function
#End Region

    


    

    'Protected Sub hitungLamaSurvey(ByVal sender As Object, ByVal e As EventArgs) Handles txtTanggalSurvey.TextChanged
    '    lblLamaSurvey.Text = DateDiff(DateInterval.Day, ConvertDate2(ucViewApplication1.TanggalAplikasi), ConvertDate2(txtTanggalSurvey.Text)).ToString
    'End Sub

    Private Sub dtgJournal_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgJournal.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblNo As New Label            
            Dim lblPost As New Label
            Dim lblAmount As New Label
            Dim lblDebet As New Label
            Dim lblKredit As New Label

            lblNo = CType(e.Item.FindControl("lblNo"), Label)            
            lblPost = CType(e.Item.FindControl("lblPost"), Label)
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            lblDebet = CType(e.Item.FindControl("lblDebet"), Label)
            lblKredit = CType(e.Item.FindControl("lblKredit"), Label)

            lblNo.Text = (e.Item.ItemIndex + 1).ToString

            If lblPost.Text.ToUpper.Trim = "C" Then
                lblKredit.Text = FormatNumber(lblAmount.Text, 0)
                TotalKredit = TotalKredit + CDec(lblKredit.Text)
            Else
                lblDebet.Text = FormatNumber(lblAmount.Text, 0)
                TotalDebet = TotalDebet + CDec(lblDebet.Text)
            End If

        ElseIf e.Item.ItemType = ListItemType.Footer Then
            Dim lblDebetTotal As New Label
            Dim lblKreditTotal As New Label

            lblDebetTotal = CType(e.Item.FindControl("lblDebetTotal"), Label)
            lblKreditTotal = CType(e.Item.FindControl("lblKreditTotal"), Label)
            lblDebetTotal.Text = FormatNumber(TotalDebet, 0)
            lblKreditTotal.Text = FormatNumber(TotalKredit, 0)
        End If
    End Sub

    Private Sub btnProceed_Click(sender As Object, e As System.EventArgs) Handles btnProceed.Click
        Dim ocustom As New Parameter.HasilSurvey

        With ocustom
            .strConnection = Me.GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.sesBranchId.Replace("'", "")
            .BusinessDate = Me.BusinessDate
            oController.Proceed(ocustom)
        End With

        ShowMessage(lblMessage, "Applikasi sudah di proses", False)
        btnProceed.Visible = False
    End Sub
#Region "Upload image Asset"
    Private Sub cekimage()
        If File.Exists(pathFile("Survey", Me.ApplicationID) + "Kantor.jpg") Then
            imgLokasiKantor.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Survey/" + Me.ApplicationID + "/Kantor.jpg"))
        Else
            imgLokasiKantor.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Survey", Me.ApplicationID) + "Rumah.jpg") Then
            imgLokasiRumah.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Survey/" + Me.ApplicationID + "/Rumah.jpg"))
        Else
            imgLokasiRumah.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

       

        imgLokasiKantor.Height = 200
        imgLokasiKantor.Width = 300
        imgLokasiRumah.Height = 200
        imgLokasiRumah.Width = 300


    End Sub
    Private Function pathFile(ByVal Group As String, ByVal ApplicationID As String) As String
        Dim strDirectory As String = ""
        'strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ApplicationID & "\"
        strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & ApplicationID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Function getpathFolder() As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function
    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function
    Private Sub btnupload_Click(sender As Object, e As System.EventArgs) Handles btnupload.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""

        If uplLokasiRumah.HasFile Then
            If uplLokasiRumah.PostedFile.ContentType = "image/jpeg" Then
                FileName = "Rumah"
                strExtension = Path.GetExtension(uplLokasiRumah.PostedFile.FileName)
                uplLokasiRumah.PostedFile.SaveAs(pathFile("Survey", Me.ApplicationID) + FileName + strExtension)
            End If
        End If
        If uplLokasiKantor.HasFile Then
            If uplLokasiKantor.PostedFile.ContentType = "image/jpeg" Then
                FileName = "Kantor"
                strExtension = Path.GetExtension(uplLokasiKantor.PostedFile.FileName)
                uplLokasiKantor.PostedFile.SaveAs(pathFile("Survey", Me.ApplicationID) + FileName + strExtension)
            End If
        End If
       
        cekimage()
    End Sub
#End Region
End Class