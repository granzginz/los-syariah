﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewApplication
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ApplicationController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim appid As String = ""
        Dim custid As String = ""
        lblMessage.Text = ""

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            appid = Request("ApplicationID").ToString
            custid = Request("CustID")

            Dim oApplication As New Parameter.Application
            Dim oData As New DataTable
            Dim oRow As DataRow

            oApplication.strConnection = GetConnectionString()
            oApplication.BranchId = Me.sesBranchId
            oApplication.AppID = appid
            oApplication = m_controller.GetViewApplication(oApplication)
            If Not oApplication Is Nothing Then
                oData = oApplication.ListData
            End If
            If oData.Rows.Count > 0 Then
                oRow = oData.Rows(0)
                custid = oRow("CustomerID").ToString.Trim
                custid = custid.Trim
                If oRow("ApplicationModule").ToString.Trim = "AGRI" Then
                    Response.Redirect("../ApplicationAgriculture/ViewApplicationAgriculture_003.aspx?ApplicationID=" & appid & "&CustID=" & custid)
                ElseIf oRow("ApplicationModule").ToString.Trim = "OPLS" Then
                    Response.Redirect("../OperatingLease/ViewApplicationOperatingLease_003.aspx?ApplicationID=" & appid & "&CustID=" & custid)
                ElseIf oRow("ApplicationModule").ToString.Trim = "FACT" Then
                    Response.Redirect("../NewFactoring/ViewApplicationFactoring_003.aspx?ApplicationID=" & appid & "&CustID=" & custid)
                ElseIf oRow("ApplicationModule").ToString.Trim = "MDKJ" Then
                    Response.Redirect("../NewModalKerja/ViewApplicationModalKerja_003.aspx?ApplicationID=" & appid & "&CustID=" & custid)
                Else
                    Response.Redirect("ViewApplication_003.aspx?ApplicationID=" & appid & "&CustID=" & custid)
                End If
            Else
                ShowMessage(lblMessage, "Aplikasi tidak dapat ditemukan", True)
            End If
        End If
    End Sub
   
End Class