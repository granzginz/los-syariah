﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SimulasiAngsFull.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.SimulasiAngsFull" %>

<%@ Register TagPrefix="uc1" TagName="ucNumber" Src="../../../../webform.UserController/ucNumber.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SimulasiAngsFull</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function NumInst(Tenor, Num, txtNumInst) {
            document.forms[0].txtNumInst.value = Tenor / Num;
        }
        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
        function GracePeriod(txtGracePeriod, cboGracePeriod, Value) {
            if (Value == 'AR') {
                document.forms[0].txtGracePeriod.value = '0'
                if (document.forms[0].txtGracePeriod.disabled == false) {
                    document.forms[0].txtGracePeriod.disabled = false;
                    document.forms[0].cboGracePeriod.disabled = false;
                }
            }
            else {
                document.forms[0].txtGracePeriod.value = '0'
                document.forms[0].txtGracePeriod.disabled = true;
                document.forms[0].cboGracePeriod.disabled = true;
            }
        }
        function InterestType() {
            var myID = 'cboInstScheme';
            var obj = eval('document.forms[0].' + myID);
            if (document.forms[0].cboInterestType.value == 'FX') {
                obj.disabled = false;
            }
            else {
                obj.disabled = true;
            }
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }
        function InstScheme() {
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;

            if (document.forms[0].cboInstScheme.value == 'ST') {
                document.forms[0].rdoSTTYpe(0).disabled = false;
                document.forms[0].rdoSTTYpe(1).disabled = false;
                document.forms[0].rdoSTTYpe(2).disabled = false;
            }
        }
        function rdoDisabled() {

            //alert(document.forms[0].rdoSTTYpe(0).disabled);								
            document.forms[0].rdoSTTYpe(0).checked = true;
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table id="TABLE1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
        <tr>
            <td align="center">
                <asp:Panel ID="pnlInput" runat="server">
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr>
                            <td>
                                <font color="red"><i> Mandatory</i></font>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblMessage" runat="server" ></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopi">
                            <td class="tdtopi" align="center">
                                SIMULASI ANGSURAN
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdgenap" style="height: 11px">
                                Jenis Margin
                            </td>
                            <td class="tdganjil" style="height: 11px">
                                <asp:DropDownList ID="cboInterestType" runat="server" onchange="InterestType();"
                                    AutoPostBack="True">
                                    <asp:ListItem Value="FX" Selected="True">Fixed Rate</asp:ListItem>
                                    <asp:ListItem Value="FL">Floating Rate</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" style="height: 21px">
                                Skema Angsuran
                            </td>
                            <td class="tdganjil" style="height: 21px">
                                <asp:DropDownList ID="cboInstScheme" runat="server" onchange="InstScheme();" AutoPostBack="True">
                                    <asp:ListItem Value="RF" Selected="True">Regular Fixed Installment Scheme</asp:ListItem>
                                    <asp:ListItem Value="IR">Irregular Installment</asp:ListItem>
                                    <asp:ListItem Value="ST">Step Up/Step Down</asp:ListItem>
                                    <asp:ListItem Value="EP">Even Principle</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Tipe Step Up Step Down
                            </td>
                            <td class="tdganjil">
                                <asp:RadioButtonList ID="rdoSTTYpe" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="NM">Basic Step Up/Step Down</asp:ListItem>
                                    <asp:ListItem Value="RL" Selected="True">Normal</asp:ListItem>
                                    <asp:ListItem Value="LS">Leasing Step Up/Step Down</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" border="0">
                        <tr class="tdjudul">
                            <td valign="top" colspan="2">
                                DATA FINANCIAL
                            </td>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Pokok Hutang (NTF)
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtNTF" runat="server" MaxLength="15"  Width="85%">0</asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator2" runat="server" MinimumValue="0" MaximumValue="999999999999999"
                                    ErrorMessage="Harap isi dengan Angka" Type="Double" ControlToValidate="txtNTF" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                            <td class="tdgenap">
                            </td>
                            <td class="tdganjil">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Margin Effective&nbsp;
                            </td>
                            <td class="tdganjil">
                                <font size="2"><font color="red">
                                    <asp:TextBox ID="txtEffectiveRate" runat="server" MaxLength="15" 
                                        Width="85%" Columns="18"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap isi dengan Margin Effective"
                                        ControlToValidate="txtEffectiveRate" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="RVPOEffectiveRate" runat="server" MinimumValue="0" MaximumValue="100"
                                        Type="Double" ControlToValidate="txtEffectiveRate" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                                    <asp:RangeValidator ID="RVPBEffectiveRate" runat="server" MinimumValue="0" MaximumValue="100"
                                        Type="Double" ControlToValidate="txtEffectiveRate" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                                    <asp:RangeValidator ID="RVPEffectiveRate" runat="server" MinimumValue="0" MaximumValue="100"
                                        Type="Double" ControlToValidate="txtEffectiveRate" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator></font></font>
                            </td>
                            <td class="tdgenap">
                                Margin Flat
                            </td>
                            <td class="tdganjil">
                                <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdgenap">
                            <td style="height: 48px">
                                Subsidi / Komisi(-)
                            </td>
                            <td class="tdganjil" style="height: 48px">
                                <font color="red">
                                    <asp:TextBox ID="txtAmount" runat="server" MaxLength="15"  Columns="18">0</asp:TextBox></font>
                            </td>
                            <td style="height: 48px">
                                Angsuran Pertama&nbsp;
                            </td>
                            <td class="tdganjil" style="height: 48px">
                                <asp:DropDownList ID="cboFirstInstallment" runat="server">
                                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                                    <asp:ListItem Value="AD">Advance</asp:ListItem>
                                    <asp:ListItem Value="AR">Arrear</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Angsuran Pertama"
                                    ControlToValidate="cboFirstInstallment" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr class="tdgenap">
                            <td>
                                Cara Angsuran&nbsp;
                            </td>
                            <td class="tdganjil">
                                <asp:DropDownList ID="cboPaymentFreq" runat="server">
                                    <asp:ListItem Value="1">Monthly</asp:ListItem>
                                    <asp:ListItem Value="2">Bimonthly</asp:ListItem>
                                    <asp:ListItem Value="3">Quarterly</asp:ListItem>
                                    <asp:ListItem Value="6">Semi Annualy</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                
                            </td>
                            <td class="tdganjil">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Jangka Waktu
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtNumInst" runat="server"  Columns="4">36</asp:TextBox>&nbsp;&nbsp;&nbsp;Tenor
                                <asp:TextBox ID="txtTenor" runat="server"  Columns="4">36</asp:TextBox>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Jangka waktu harus > 0"
                                    Type="Integer" ControlToValidate="txtNumInst" Display="Dynamic" ValueToCompare="0"
                                    Operator="GreaterThan" CssClass="validator_general"></asp:CompareValidator>
                            </td>
                            <td class="tdgenap">
                                <span style="font-size: 10pt; font-family: Arial; mso-fareast-font-family: 'Times New Roman';
                                    mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA">
                                    <asp:Label ID="lblFloatingPeriod" runat="server">Periode Floating</asp:Label></span>
                            </td>
                            <td class="tdganjil">
                                <font face="Tahoma, Verdana" size="2"><font color="red">
                                    <asp:DropDownList ID="cboFloatingPeriod" runat="server">
                                        <asp:ListItem Value="Select One">Select One</asp:ListItem>
                                        <asp:ListItem Value="M">Monthly</asp:ListItem>
                                        <asp:ListItem Value="Q">Quaterly</asp:ListItem>
                                        <asp:ListItem Value="S">Semi Annually</asp:ListItem>
                                        <asp:ListItem Value="A">Annually</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="reqFloatingPeriod" runat="server" ErrorMessage="Harap Pilih Floating Period"
                                        ControlToValidate="cboFloatingPeriod" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtGrossYieldRate" runat="server" MaxLength="15" 
                                        Width="16px" Columns="18" Visible="False" ReadOnly="True"></asp:TextBox></font></font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                <asp:Label ID="lblInstallment" runat="server">Jumlah Angsuran</asp:Label>
                            </td>
                            <td class="tdganjil">
                                <font face="Tahoma, Verdana" color="red" size="2">
                                    <asp:TextBox ID="txtInstAmt" runat="server" MaxLength="15"  Width="85%">0</asp:TextBox>
                                    <asp:RangeValidator ID="RVInstAmt" runat="server" MinimumValue="0" MaximumValue="999999999999999"
                                        ErrorMessage="Harap isi dengan Angka" Type="Double" ControlToValidate="txtInstAmt" CssClass="validator_general"></asp:RangeValidator></font>
                            </td>
                            <td class="tdgenap">
                                <asp:Label ID="lblNumberOfStep" runat="server">Number of Step</asp:Label>
                            </td>
                            <td class="tdganjil">
                                <font face="Tahoma, Verdana" color="red" size="2">
                                    <asp:TextBox ID="txtStep" runat="server" MaxLength="15"  Columns="4">0</asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap isi Number Of Step"
                                        ControlToValidate="txtStep" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rvStep" runat="server" MinimumValue="2" MaximumValue="999"
                                        ErrorMessage="Number Of Step harus > 1" Type="Double" ControlToValidate="txtstep"
                                        Display="Dynamic" CssClass="validator_general"></asp:RangeValidator></font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Grace Period
                            </td>
                            <td class="tdganjil">
                                <font face="Tahoma, Verdana" size="2">
                                    <asp:TextBox ID="txtGracePeriod" runat="server" MaxLength="2" 
                                        Columns="4" ReadOnly="True">0</asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Harap isi dengan Angka"
                                        ControlToValidate="txtGracePeriod" Display="Dynamic" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>&nbsp;&nbsp;&nbsp;
                                    <asp:DropDownList ID="cboGracePeriod" runat="server">
                                        <asp:ListItem Value="I">Interest Only</asp:ListItem>
                                        <asp:ListItem Value="R">Roll Over</asp:ListItem>
                                    </asp:DropDownList>
                                </font>
                            </td>
                            <td class="tdgenap">
                                <asp:Label ID="lblCummulative" runat="server">Devide Start from Inst. No.</asp:Label>
                            </td>
                            <td class="tdganjil">
                                <font face="Tahoma, Verdana" color="red" size="2">
                                    <asp:TextBox ID="txtCummulative" runat="server" MaxLength="15" 
                                        Columns="4">0</asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap isi dengan Kumulatif"
                                        ControlToValidate="txtCummulative" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rvCummulative" runat="server" MinimumValue="1" MaximumValue="999"
                                        ErrorMessage="Kumulatif harus > 0" Type="Double" ControlToValidate="txtCummulative"
                                        Display="Dynamic" CssClass="validator_general"></asp:RangeValidator></font>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr valign="bottom">
                            <td align="left">
                                <asp:ImageButton ID="imbCalcInst" runat="server" ImageUrl="../../../../Images/ButtonCalcInstallAmount.gif">
                                </asp:ImageButton>&nbsp;
                                <asp:ImageButton ID="imbRecalcEffRate" runat="server" ImageUrl="../../../../Images/ButtonRecalculateRate.gif">
                                </asp:ImageButton>&nbsp;
                                <asp:ImageButton ID="imbSave" runat="server" ImageUrl="../../../../Images/ButtonView.gif"
                                    CausesValidation="True"></asp:ImageButton>
                            </td>
                        </tr>
                        <tr valign="bottom">
                            <td align="center" width="100%">
                                <div align="left">
                                    &nbsp;</div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br/>
                <asp:Panel ID="pnlEntryInstallment" runat="server">
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopi">
                            <td class="tdtopi" align="center">
                                ANGSURAN
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="dtgEntryInstallment" runat="server" CssClass="grid_general" Width="100%"
                                    AllowSorting="True" AutoGenerateColumns="False" BorderWidth="0px" CellPadding="3"
                                    CellSpacing="1">
                                    
                                    <ItemStyle CssClass="item_grid"/>
                                    <HeaderStyle CssClass="th"/>
                                    <Columns>
                                        <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="ANGSURAN KE">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtNoStep" runat="server" >0</asp:TextBox>
                                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator5" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtNoStep" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="reqNoStep" runat="server" ControlToValidate="txtNoStep"
                                                    Display="Dynamic" ErrorMessage="Harap isi dengan Number Of Step" CssClass="validator_general"></asp:RequiredFieldValidator>
                                                <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtNoStep"
                                                    Display="Dynamic" MinimumValue="1" MaximumValue="999" Type="Integer" ErrorMessage="No of Step harus > 0" CssClass="validator_general"></asp:RangeValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="JUMLAH ANGSURAN">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtEntryInstallment" runat="server" >0</asp:TextBox>
                                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator6" runat="server" Display="Dynamic"
                                                    ControlToValidate="txtEntryInstallment" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="rfInstallment" runat="server" ControlToValidate="txtEntryInstallment"
                                                    Display="Dynamic" ErrorMessage="Harap isi Jumlah angsuran" CssClass="validator_general"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:ImageButton ID="ImgEntryInstallment" runat="server" ImageUrl="../../../../Images/ButtonView.gif">
                                    </asp:ImageButton>
                                </td>
                            </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlViewST" runat="server">
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopi">
                            <td class="tdtopi" align="center">
                                VIEW - ANGSURAN
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="dtgViewInstallment" runat="server" CssClass="grid_general" Width="100%"
                                    AllowSorting="True" AutoGenerateColumns="False" BorderWidth="0px" CellPadding="3"
                                    CellSpacing="1">
                                    
                                    <ItemStyle HorizontalAlign="Right" CssClass="tdganjil"></ItemStyle>
                                    <HeaderStyle CssClass="th"/>
                                    <Columns>
                                        <asp:BoundColumn DataField="InsSeqNo" HeaderText="No"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="JUMLAH ANGSURAN">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InstallmentAmount"),2) %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="POKOK">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.PrincipalAmount"),2) %>'
                                                    ID="Label2" NAME="Label2">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="MARGIN">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InterestAmount"),2) %>'
                                                    ID="Label3" NAME="Label3">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="SISA POKOK">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingPrincipal"),2) %>'
                                                    ID="Label4" NAME="Label4">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="SISA MARGIN">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingInterest"),2) %>'
                                                    ID="Label5" NAME="Label5">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
        <tr>
            <td align="center">
                <asp:Panel ID="pnlInstallGrid" runat="server">
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopi">
                            <td class="tdtopi" align="center">
                                VIEW - ANGSURAN
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="Datagrid1" runat="server" CssClass="grid_general" Width="100%" AllowSorting="True"
                                    AutoGenerateColumns="False" BorderWidth="0px" CellPadding="3" CellSpacing="1">
                                    
                                    <ItemStyle HorizontalAlign="Right" CssClass="tdganjil"></ItemStyle>
                                    <HeaderStyle CssClass="th"/>
                                    <Columns>
                                        <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="JUMLAHSURAN">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.Installment"),2) %>'>
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="POKOK">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.Principal"),2) %>'
                                                    ID="Label7" NAME="Label2">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="MARGIN">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.Interest"),2) %>'
                                                    ID="Label8" NAME="Label3">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="SISA POKOK">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.PrincBalance"),2) %>'
                                                    ID="Label9" NAME="Label4">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="SISA MARGIN">
                                            <ItemTemplate>
                                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.PrincInterest"),2) %>'
                                                    ID="Label10" NAME="Label5">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:ImageButton ID="imbBack" runat="server" ImageUrl="../../../../Images/ButtonBack.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
