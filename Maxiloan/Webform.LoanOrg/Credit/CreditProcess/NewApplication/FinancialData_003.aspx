﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FinancialData_003.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.FinancialData_003" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTab.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucSupplierEmployeeGrid.ascx"
    TagName="ucSupplierEmployeeGrid" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../Webform.UserController/ucFlagTransaksi.ascx" TagName="ucFlagTransaksi"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Financial Data</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Lookup.css" type="text/css" />
    
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../../../js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        /*function DeleteRow(grid, idx) {
        var sgrid = document.getElementById(grid);
        sgrid.deleteRow(idx);
        recalculate();
        }*/
        function calculateNPV(id) {
            var totalBunga = document.getElementById('lblTotalBunga').value;

            //TenorYear = parseInt(TenorYear) / 12;
            if (id == "txtRefundBungaN_txtNumber") {  // ini kalo yang diisi nilai nya
                var nilAmount = document.getElementById('txtRefundBungaN_txtNumber').value;

                var result = (parseInt(nilAmount.replace(/\s*,\s*/g, '')) / parseInt(totalBunga.replace(/\s*,\s*/g, ''))) * 100;
                  
//                var r_formated = parseFloat(Math.floor(result * 100) / 100);
//                if (!r_formated.length) {
//                    document.getElementById('ucRefundBunga_txtNumber').value = number_format(r_formated, 2);
//                } else {
//                    document.getElementById('ucRefundBunga_txtNumber').value = r_formated;
//                }
                document.getElementById('txtRefundBungaN_txtNumber').value = nilAmount;

            } else {  // ini kalo yang diisi persennya
                var refundRate = document.getElementById('ucRefundBunga_txtNumber').value;

                var npv = (parseInt(totalBunga.replace(/\s*,\s*/g, '')) * (parseFloat(refundRate) / 100));

//                npv = Math.round(npv, 0) / 1000;    
//                npv = Math.ceil(npv) * 1000;
                document.getElementById('txtRefundBungaN_txtNumber').value = number_format(npv, 0);
            }

        }
        //       
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }

        function NumInst(Tenor, Num, txtNumInst) {
            var newtenor = Tenor / Num;
            $('#' + txtNumInst).val(newtenor);
        }

        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
        function GracePeriod(txtGracePeriod, cboGracePeriod, Value) {
            if (Value == 'AR') {
                document.forms[0].txtGracePeriod.value = '0'
                if (document.forms[0].txtGracePeriod.disabled == false) {
                    document.forms[0].txtGracePeriod.disabled = false;
                    document.forms[0].cboGracePeriod.disabled = false;
                }
            }
            else {
                document.forms[0].txtGracePeriod.value = '0'
                document.forms[0].txtGracePeriod.disabled = true;
                document.forms[0].cboGracePeriod.disabled = true;
            }
        }
        function recalculateAngs() {
            if ($('#cboFirstInstallment').val() == 'AD') {
                $('#lblAngsuranPertama').html(number_format($('#txtInstAmt_txtNumber').val()));
            }

            var lblUangMuka = $('#lblUangMuka').html();
            var lblAdminFee = $('#lblAdminFee').html();
            var lblBiayaFidusia = $('#lblBiayaFidusia').html();
            // var lblBiayaPolis = $('#lblBiayaPolis').html();
            var lblOtherFee = $('#lblOtherFee').html();
            var lblAssetInsurance32 = $('#lblAssetInsurance32').html();
            var lblAngsuranPertama = $('#lblAngsuranPertama').html();
            var lblBiayaProvisi = $('#lblBiayaProvisi').html();

            var xtotal = parseInt(lblUangMuka.replace(/\s*,\s*/g, '')) +
                         parseInt(lblAdminFee.replace(/\s*,\s*/g, '')) +
                         parseInt(lblBiayaFidusia.replace(/\s*,\s*/g, '')) +
                         //parseInt(lblBiayaPolis.replace(/\s*,\s*/g, '')) + 
                         parseInt(lblOtherFee.replace(/\s*,\s*/g, '')) +
                         parseInt(lblAssetInsurance32.replace(/\s*,\s*/g, '')) +
                         parseInt(lblAngsuranPertama.replace(/\s*,\s*/g, '')) +
                         parseInt(lblBiayaProvisi.replace(/\s*,\s*/g, ''));

            $('#lblTotalBayarPertama').html(number_format(xtotal));
        }

       
        function recalculateARGross() {
            var totalbunga = $('#lblTotalBunga_txtNumber').val();
            var ntf = $('#lblNTF').html();
            var nilaikontrak = parseInt(totalbunga.replace(/\s*,\s*/g, '')) +
                                parseInt(ntf.replace(/\s*,\s*/g, ''));
            $('#lblNilaiKontrak_txtNumber').val(number_format(nilaikontrak));
        }
        function hitungtxt(txtidA, txtidB, txtidS, toPersen) {
            var status = true;
            var A = $('#' + txtidA).val();
            var S;
            var N;
            var M;
            if ($('#' + txtidS).html() == "") {
                S = $('#' + txtidS).val();
            } else {
                S = $('#' + txtidS).html();
            }

            A = A.replace(/\s*,\s*/g, '');
            S = S.replace(/\s*,\s*/g, '');

            if (txtidA == "txtRefundBungaN_txtNumber" || txtidA == "ucRefundBunga_txtNumber") {
                if (txtidA == "ucRefundBunga_txtNumber") {
                    M = $('#MaxRefundBungaP').html();
                    if (A > parseInt(M)) {
                        status = false;
                        alert('Tidak Boleh lebih dari ' + M);
                    }
                } else {
                    M = $('#MaxRefundBungaN').html();
                    if (A > parseInt(M)) {
                        status = false;
                        alert('Tidak Boleh lebih dari ' + M);
                    }
                }
            }



            if (status == true) {
                if (toPersen == true) {
                    N = parseInt(A) / parseInt(S) * 100;
                    $('#' + txtidB).val(number_format(parseFloat((N * 10) / 10), 2));
                } else {
                    N = parseFloat(S) * (A / 100);
                    //$('#' + txtidA).val(number_format(A, 2));
                    $('#' + txtidB).val(number_format(N, 0));
                }
            } else {
                $('#' + txtidA).val(number_format(0, 2));
                $('#' + txtidB).val(number_format(0, 2));
            }


            if (txtidA == "txtBiayaProvisiN_txtNumber" || txtidA == "txtBiayaProvisi_txtNumber") {
                $('#lblBiayaProvisi').html(number_format($('#txtBiayaProvisiN_txtNumber').val(), 0));
                $('#xlblBiayaProvisiN').html(number_format($('#txtBiayaProvisiN_txtNumber').val(), 0));
                recalculateAngs();
            }
            if (txtidA == "txtRefundPremiN_txtNumber" || txtidA == "txtRefundPremi_txtNumber") {
                var selisihPremi = $('#lblSelisihPremi').html();
                var refundPemi = $('#txtRefundPremiN_txtNumber').val();
                var pendapatan = parseInt(selisihPremi.replace(/\s*,\s*/g, '')) - parseInt(refundPemi.replace(/\s*,\s*/g, ''));
                $('#lblPendapatanPremiN_txtNumber').val(number_format(pendapatan,0));
            }

        }
     
        
    </script>
</head>
<body>
    
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div>
                    <div class="form_left">
                        <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                    </div>
                    <div class="form_right">
                        <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ENTRI FINANSIAL</h4>
                </div>
            </div>
            <%--tidak perlu ditampilkan--%>
            <div class="form_box" style="display: none;">
                <div>
                    <div class="form_left">
                        <label>
                            Jenis Margin
                        </label>
                        <asp:Label ID="lblInterestType" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Skema Angsuran
                        </label>
                        <asp:Label ID="lblInstScheme" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_box_hide">
                        <label class="label_req">
                            Pola Angsuran
                        </label>
                        <asp:DropDownList ID="cboPolaAngsuran" runat="server">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="FDPK">FDPK</asp:ListItem>
                            <asp:ListItem Value="FLAT" Selected="True">FLAT</asp:ListItem>
                            <asp:ListItem Value="NOTARIIL">NOTARIIL</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvPolaAngsuran" runat="server" Display="Dynamic"
                            ControlToValidate="cboPolaAngsuran" ErrorMessage="Harap pilih pola angsuran!"
                            InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div> 
                    <div class="form_left">
                        <label class="label_req">
                            Angsuran Pertama
                        </label>
                        <asp:DropDownList ID="cboFirstInstallment" runat="server" AutoPostBack="true">
                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                            <asp:ListItem Value="AD">Advance</asp:ListItem>
                            <asp:ListItem Value="AR">Arrear</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ControlToValidate="cboFirstInstallment" ErrorMessage="Harap Pilih Angsuran Pertama"
                            InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>  
                    <div class="form_right">
                        <label class="label">
                            Angsuran Tidak Bayar
                        </label>
                        <uc4:ucnumberformat id="txtTidakAngsur" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <%--tidak perlu ditampilkan--%>
                    <div class="form_box_hide">
                        <label>
                            Grace Period Ta' Widh Keterlambatan
                        </label>
                        <asp:TextBox ID="txtGracePeriod2" runat="server" MaxLength="2" CssClass="numberAlign2 regular_text">0</asp:TextBox>
                        <asp:RangeValidator ID="rvGracePeriodLC" runat="server" ControlToValidate="txtGracePeriod2"
                            Type="Double" CssClass="validator_general"></asp:RangeValidator>
                    </div>
                    <div class="form_left">
                        <%--<label class="label_general">
                            Refund Supplier digabung
                        </label>
                            <asp:RadioButtonList ID="rdoGabungRefundSupplier" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                                <asp:ListItem Text="Ya" Value="1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Tidak" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Potong Pencairan Dana
                        </label>
                        <asp:RadioButtonList runat="server" ID="optPotongPencairanDana" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Text="Ya" Value="1" ></asp:ListItem>
                            <asp:ListItem Text="Tidak" Value="0" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_box_hide">
                <div>
                    <div class="form_left">
                        <label>
                            Tanggal Angsuran I
                        </label>
                        <uc8:ucdatece runat="server" id="txtTglAngsuranI"></uc8:ucdatece>                        
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Angsuran Bayar di Dealer
                        </label>
                        <asp:RadioButtonList runat="server" ID="optBayarDealer" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Text="Ya" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Tidak" Value="0" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    
                </div>
            </div>
            <div class="form_box_header">
                <div>
                    <div class="form_single">
                        <h5>
                            DATA FINANSIAL</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        OTR Asset
                    </label>
                    <asp:Label ID="lblOTR" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                    <label class="label_req">Frekuensi Angsuran</label>
                    <asp:DropDownList ID="cboPaymentFreq" runat="server" AutoPostBack="true" CssClass="numberAlign2" Width="158px" OnSelectedIndexChanged="FrekuensiChanged" >
                        <asp:ListItem Value="1">Monthly</asp:ListItem>
                        <asp:ListItem Value="2">Quaterly</asp:ListItem>
                        <asp:ListItem Value="3">Semi Annually</asp:ListItem>
                        <asp:ListItem Value="6">Annually</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Karoseri
                    </label>
                    <asp:Label runat="server" ID="lblKaroseri" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                    <label class="label_req">Jangka Waktu</label>
                    <uc4:ucnumberformat id ="txtNumInst" runat="server" Width="200" TextCssClass="numberAlign2 smaller_text"/>
                    <asp:HiddenField ID="lblTenor" runat="server" />
                    <asp:HiddenField ID="lblAdminFee" runat="server" />
                    <asp:HiddenField ID="lblIsAdminFeeCredit" runat="server" />
                    <asp:HiddenField ID="lblBiayaFidusia" runat="server" />
                    <asp:HiddenField ID="lblOtherFee" runat="server" />
                    <asp:HiddenField ID="lblNTFSupplier" runat="server" />
                    <asp:HiddenField ID="lblOTRSupplier" runat="server" />
                    <asp:HiddenField ID="lblBiayaProvisi" runat="server" />
                    <asp:HiddenField ID="lblAngsuranPertama" runat="server" />
                    <asp:HiddenField ID="lblDPPersenSupplier" runat="server" />
                    <asp:HiddenField ID="txtDPSupplier" runat="server" />
                    <asp:HiddenField ID="lblAdminFee2" runat="server" />
                    <asp:HiddenField ID="lblIsAdminFeeCredit2" runat="server" />
                    <asp:HiddenField ID="lblAssetInsurance32" runat="server" />
                    <asp:HiddenField ID="txtEffectiveRateSupplier" runat="server" />
                    <asp:HiddenField ID="lblInsAmountToNPV" runat="server" />
                    <asp:HiddenField ID="lblTotalBayarPertama" runat="server" />
                    <asp:HiddenField ID="lblNilaiKontrakSupplier" runat="server" />
                    <asp:HiddenField ID="lblInstallmentSupplier" runat="server" />
                    <asp:HiddenField ID="txtInstAmtSupplier" runat="server" />
                    <asp:HiddenField ID="txtNumInstSupplier" runat="server" />
                    <asp:HiddenField ID="lblTenorSupplier" runat="server" />
                    <asp:HiddenField ID="lblSupplierRate" runat="server" />
                    <asp:HiddenField ID="txtAmount" runat="server" />
                    <asp:HiddenField ID="lblPremiAsuransiGross" runat="server" />
                    <asp:HiddenField ID="txtGrossYieldRate" runat="server" />
                    
                </div> 
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total OTR
                    </label>
                    <asp:Label runat="server" ID="lblTotalOTR" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right box_important">
                    <asp:Label ID="lblInstallment" runat="server" CssClass="label label_req">Angsuran Per Frekuensi </asp:Label>
                    <uc4:ucnumberformat id="txtInstAmt" runat="server" TextCssClass="numberAlign2" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                        <label class="label_auto">
                            Uang Muka OTR
                        </label>
                    <uc4:ucnumberformat id="txtDP" runat="server" TextCssClass="numberAlign2" />    
                        <label class="label_auto numberAlign2"> %
                        </label>
                    <uc4:ucnumberformat id="txtDPPersen" runat="server" TextCssClass="numberAlign2" />
                </div>
                <div class="form_right">
                    
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                        <label class="label_auto">
                            Uang Muka Karoseri
                        </label>
                    <uc4:ucnumberformat id="txtDPKaroseri" runat="server" TextCssClass="numberAlign2" />    
                        <label class="label_auto numberAlign2"> %
                        </label>
                    <uc4:ucnumberformat id="txtDPPersenKaroseri" runat="server" TextCssClass="numberAlign2" />
                </div>
                <div class="form_right">
                    
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <asp:Label ID="lblAssetInsurance3" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right" style="background-color:#FCFCCC">
                    <label>Biaya Provisi</label>
                        <uc4:ucnumberformat runat="server" id="txtBiayaProvisiN" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitungtxt(this.id,'txtBiayaProvisi_txtNumber','lblNTF',true)"></uc4:ucnumberformat>
                        <label class="label_auto numberAlign2"> % </label>
                        <uc4:ucnumberformat runat="server" id="txtBiayaProvisi" TextCssClass="numberAlign2 smaller_text" OnClientChange="hitungtxt(this.id,'txtBiayaProvisiN_txtNumber','lblNTF',false)"></uc4:ucnumberformat>
                </div>

            </div>
            <div class="form_box">
                <div class="form_left border_sum">
                    <label class="label_calc2">
                        +
                    </label>
                    <label>
                        Biaya Admin Pembiayaan
                    </label>
                    <asp:Label ID="lblAdmCapitalized" runat="server" CssClass="numberAlign2 regular_text">0</asp:Label>
                </div>
                <div class="form_right"></div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Pokok Hutang (NTF)
                        </label>
                        <asp:Label ID="lblNTF" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                       <label>Jumlah Step</label>
                        <asp:TextBox ID="txtStep" runat="server" MaxLength="15" Columns="4">0</asp:TextBox>
                        <asp:requiredfieldvalidator id="RequiredFieldValidator3" runat="server" ErrorMessage="You Must Fill Number Of Step"
									    ControlToValidate="txtStep" Display="Dynamic" CssClass="validator_general"></asp:requiredfieldvalidator>
                        <asp:rangevalidator id="rvStep" runat="server" ErrorMessage="Number Of Step Must > 1" ControlToValidate="txtstep"
									    Display="Dynamic" CssClass="validator_general" MaximumValue="999" MinimumValue="2" Type="Double"></asp:rangevalidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left ">
                    <label>Total Margin</label>
                        <asp:TextBox ID="lblTotalBungaDisplay" runat="server" CssClass="numberAlign2 readonly_text" ReadOnly="true"></asp:TextBox>
                        <asp:HiddenField ID="lblTotalBunga" runat="server"></asp:HiddenField>
                        <label class="label_auto numberAlign2"> % </label>
                        <asp:TextBox ID="txtEffectiveRate" runat="server" MaxLength="15" CssClass="numberAlign2 smaller_text" ></asp:TextBox>
                        <asp:requiredfieldvalidator id="RequiredFieldValidator6" runat="server" ErrorMessage="Please fill Effective Rate!"
											ControlToValidate="txtEffectiveRate" Display="Dynamic" CssClass="validator_general"></asp:requiredfieldvalidator><asp:rangevalidator id="Rangevalidator3" runat="server" ControlToValidate="txtEffectiveRate" Display="Dynamic" CssClass="validator_general"
											MaximumValue="100" MinimumValue="0" Type="Double"></asp:rangevalidator>
                        <label class="label_auto numberAlign2"> Efektif </label>
                        <label class="label_auto numberAlign2"> % </label>
                        <asp:TextBox runat="server" ID="txtFlatRate" CssClass="numberAlign2 smaller_text" AutoPostBack="true"></asp:TextBox>
                        <label class="label_auto numberAlign2"> Flat </label>    
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                        ControlToValidate="txtEffectiveRate" ErrorMessage="Harap isi Margin Effective"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RVPOEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                        Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RangeValidator ID="RVPBEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                        Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RangeValidator ID="RVPEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                        Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                        ControlToValidate="txtFlatRate" ErrorMessage="Harap isi Margin Flat!" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" Display="Dynamic" ControlToValidate="txtFlatRate"
                        Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"
                        ErrorMessage="Margin flat tidak valid!"></asp:RangeValidator>
                </div>
                <div class="form_right">
                    <label>
                        Dibagi mulai Angsuran Ke
                    </label>
                    <asp:TextBox ID="txtCummulative" runat="server" MaxLength="15" Columns="4" >0</asp:TextBox> <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                ControlToValidate="txtCummulative"  ErrorMessage="Harap isi Kumulatif" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rvCummulative" runat="server" Display="Dynamic" ControlToValidate="txtCummulative"
                                ErrorMessage="Kumulatif harus lebih besar dari 0" Type="Double" MinimumValue="1"
                                MaximumValue="999" CssClass="validator_general"></asp:RangeValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left border_sum">
                    <label>Upping Margin</label>
                    <label class="label_calc2">
                            +
                        </label>
                    <uc4:ucnumberformat id="txtUppingBunga" runat="server" TextCssClass="numberAlign2"  />
                </div>
                <div class="form_right">
                    <label class="">
                            Grace Period
                        </label>
                        <asp:TextBox ID="txtGracePeriod" runat="server" MaxLength="2" Columns="4" ReadOnly="false ">0</asp:TextBox>
                        <asp:DropDownList ID="cboGracePeriod" runat="server">
                            <asp:ListItem Value="I">Margin Only</asp:ListItem>
                            <asp:ListItem Value="R">Roll Over</asp:ListItem>
                        </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Jumlah A/R (Nilai Kontrak)</label>
                    <asp:Label ID="lblNilaiKontrak" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
              <%--  <div class="form_right">
                    Modify by WIra 20180115
                  <label class="">
                            Perhitungan Bunga
                        </label>
                        <asp:DropDownList ID="cboHitungBUnga" runat="server">
                            <asp:ListItem Value="ANUITAS">Anuitas</asp:ListItem>
                            <asp:ListItem Value="EFEKTIF">Efektif</asp:ListItem>
                        </asp:DropDownList>            
                </div>--%>
            </div>
            
            

    <asp:Panel ID="updPanelHitung" runat="server">     
            <div class="form_button">
            <asp:Button ID="imbCalcInst" runat="server" Text="Hitung Angsuran" CssClass="small button blue"
                CausesValidation="True"></asp:Button>
            <asp:Button ID="imbRecalcEffRate" runat="server" Text="Hitung Eff Margin" CssClass="small button blue"
                CausesValidation="True"></asp:Button>
            <asp:Button ID="imbCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False"></asp:Button>
            </div>
    </asp:Panel>

     <asp:Panel ID="pnlEntryInstallment" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    Installment</h4>
            </div>
        </div>
    <div class="form_box" >
        <div>
            <div class="form_single">
                <asp:datagrid id="dtgEntryInstallment" runat="server" CssClass="grid_general" Width="100%" AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="0px" cellpadding="3" cellspacing="1">
										<ItemStyle CssClass="item_grid"></ItemStyle>
										<HeaderStyle HorizontalAlign="Center" CssClass="th"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="No.Of Installment">
												<ItemTemplate>
													<asp:TextBox id="txtNoStep" runat="server" CssClass="inptype">0</asp:TextBox>
													<asp:regularexpressionvalidator id="Regularexpressionvalidator5" runat="server" Display="Dynamic" ControlToValidate="txtNoStep"
														ErrorMessage="Invalid numeric!" ValidationExpression="\d*" CssClass="validator_general"></asp:regularexpressionvalidator>
													<asp:RequiredFieldValidator ID="reqNoStep" Runat="server" ControlToValidate="txtNoStep" Display="Dynamic" CssClass="validator_general" ErrorMessage="You Must Fill Number Of Step"></asp:RequiredFieldValidator>
													<asp:RangeValidator ID="Rangevalidator1" Runat="server" ControlToValidate="txtNoStep" Display="Dynamic" CssClass="validator_general"
														MinimumValue="1" MaximumValue="999" Type="Integer" ErrorMessage="No of Step must > 0"></asp:RangeValidator>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="AMOUNT">
												<ItemTemplate>
													<asp:TextBox id="txtEntryInstallment" runat="server" CssClass="inptype">0</asp:TextBox>
													<asp:regularexpressionvalidator id="Regularexpressionvalidator6" runat="server" Display="Dynamic" CssClass="validator_general" ControlToValidate="txtEntryInstallment"
														ErrorMessage="Invalid numeric!" ValidationExpression="\d*"></asp:regularexpressionvalidator>
													<asp:RequiredFieldValidator ID="rfInstallment" Runat="server" ControlToValidate="txtEntryInstallment" Display="Dynamic" CssClass="validator_general"
														ErrorMessage="You Must Fill Installment Amount"></asp:RequiredFieldValidator>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
            </div>
        </div>
    </div>
    <div class="form_button" >
        <asp:Button ID="ImgEntryInstallment" runat="server" Text="View Installment" CssClass="small button blue"
                CausesValidation="True"></asp:Button>
    </div>
</asp:Panel>

<asp:Panel id="pnlViewST" Runat="server">
    <div class="form_title">
        <div class="form_single">
            <h4>
                View Installment</h4>
        </div>
    </div>
    <div class="form_box" >
    <div>
        <div class="form_single">
    <asp:DataGrid id="dtgViewInstallment" runat="server" CssClass="grid_general" Width="100%" AllowSorting="True"
								    AutoGenerateColumns="False" BorderWidth="0px" cellpadding="3" cellspacing="1">
								    <ItemStyle HorizontalAlign="Right" CssClass="item_grid number_align"></ItemStyle>
								    <HeaderStyle HorizontalAlign="Right" CssClass="th"></HeaderStyle>
								    <Columns>
									    <asp:BoundColumn DataField="InsSeqNo" HeaderText="No"></asp:BoundColumn>
									    <asp:TemplateColumn HeaderText="Installment" HeaderStyle-CssClass = "item_grid_right" ItemStyle-CssClass="item_grid_right" >
										    <ItemTemplate>
											    <asp:Label id="Label5" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InstallmentAmount"),0) %>'>
											    </asp:Label>
										    </ItemTemplate>
									    </asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="Principal" HeaderStyle-CssClass = "item_grid_right" ItemStyle-CssClass="item_grid_right" >
										    <ItemTemplate>
											    <asp:Label ID="Label1" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.PrincipalAmount"),0) %>'>
											    </asp:Label>
										    </ItemTemplate>
									    </asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="Interest" HeaderStyle-CssClass = "item_grid_right" ItemStyle-CssClass="item_grid_right" >
										    <ItemTemplate>
											    <asp:Label ID="Label2" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InterestAmount"),0) %>'>
											    </asp:Label>
										    </ItemTemplate>
									    </asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="OutStandingPrincipal" HeaderStyle-CssClass = "item_grid_right" ItemStyle-CssClass="item_grid_right" >
										    <ItemTemplate>
											    <asp:Label ID="Label3" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingPrincipal"),0) %>'>
											    </asp:Label>
										    </ItemTemplate>
									    </asp:TemplateColumn>
									    <asp:TemplateColumn HeaderText="OutStandingInterest" HeaderStyle-CssClass = "item_grid_right" ItemStyle-CssClass="item_grid_right" >
										    <ItemTemplate>
											    <asp:Label ID="Label4" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingInterest"),0) %>'>
											    </asp:Label>
										    </ItemTemplate>
									    </asp:TemplateColumn>
								    </Columns>
							    </asp:DataGrid>
        </div>
    </div>
</div>
</asp:Panel>
    
    <asp:Panel ID="uPanelSave" runat="server">
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <div id="btnSaveEnable" class="small button gray" style="display:none">Save</div>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>    
    </asp:Panel>

        </ContentTemplate>
         <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="imbCalcInst" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="imbRecalcEffRate" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ImgEntryInstallment" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="txtFlatRate" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtUppingBunga" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="cboPaymentFreq" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="cboFirstInstallment" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>

    </form>
</body>
</html>
