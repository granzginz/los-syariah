﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HasilSurvey.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.HasilSurvey" %>

<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTab.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hasil Survey</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        function showimagepreview(input, imgClient) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#' + imgClient).attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div>
                    <div class="form_left">
                        <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                    </div>
                    <div class="form_right">
                        <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                    </div>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlHasilSurvey">            
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        ENTRI HASIL SURVEY</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Tanggal Survey
                        </label>
                        <asp:TextBox runat="server" ID="txtTanggalSurvey"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="ceTanggalSurver" TargetControlID="txtTanggalSurvey"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                    <div class="form_right">
                        <label>
                            Surveyor
                        </label>
                        <asp:DropDownList runat="server" ID="cboSurveyor">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <div style="float:left;width:37.5%">
                            Lama Survey
                        </div>
                        <div style="float:left;width:50%;">
                            <div style="float:left">
                                <uc1:ucnumberformat id="txtLamaSurvey" runat="server" />
                            </div>
                            <span style="float:left"> &nbsp;hari  </span>
                        </div>
                    </div>
                    <div class="form_right">
                        <label>
                            Pembiayaan Analyst
                        </label>
                        <asp:DropDownList runat="server" ID="cboCreditAnalyst">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
           <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DATA LAINNYA
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <div style="float:left"><label>Jumlah Asset Yang Dimiliki?</label></div>
                    <div style="float:left"><uc1:ucnumberformat id="txtJumlahKendaraan" runat="server" /></div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_general">
                        Ada Garasi?</label>
                    <asp:RadioButtonList ID="rboGarasi" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
                        <asp:ListItem Value="True">Ya</asp:ListItem>
                        <asp:ListItem Value="False" Selected="True">Tidak</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Pertama Kali Pembiayaan</label>
                    <asp:DropDownList ID="cboPertamaKredit" runat="server">
                        <asp:ListItem Selected="True" Value="YA">Ya</asp:ListItem>
                        <asp:ListItem Value="RO">Repeat Order</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <div style="float:left">
                    <label>
                        Order Ke</label></div>
                    <div style="float:left">
                    <uc1:ucnumberformat id="txtOrderKe" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        HASIL ANALISA PEMBIAYAAN ANALYST</h4>
                </div>                
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:TextBox runat="server" ID="txtSurveyorNotes" TextMode="MultiLine" CssClass="multiline_textbox textboxsurvey"></asp:TextBox>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>ANALISA NASABAH</h4>
                </div>                
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:TextBox runat="server" ID="txtAnalisaNasabah" TextMode="MultiLine" CssClass="multiline_textbox textboxsurvey"></asp:TextBox>
                </div>
            </div> 
            <div class="form_box_title">
                <div class="form_single">
                    <h4>ANALISA TEMPAT TINGGAL</h4>
                </div>                
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:TextBox runat="server" ID="txtAnalisaTempatTinggal" TextMode="MultiLine" CssClass="multiline_textbox textboxsurvey"></asp:TextBox>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>ANALISA ASSET</h4>
                </div>                
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:TextBox runat="server" ID="txtAnalisaKendaraan" TextMode="MultiLine" CssClass="multiline_textbox textboxsurvey"></asp:TextBox>
                </div>
            </div>  
            <div class="form_box_title">
                <div class="form_single">
                    <h4>GAMBAR LOKASI</h4>
                </div>                
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label">
                            Gambar Lokasi Rumah
                        </label>
                        <asp:FileUpload ID="uplLokasiRumah" runat="server" onchange="showimagepreview(this,'imgLokasiRumah')" />
                    </div>
                    <div class="form_right">
                        <label class="label">
                            Gambar Lokasi Kantor
                        </label>
                        <asp:FileUpload ID="uplLokasiKantor" runat="server" onchange="showimagepreview(this,'imgLokasiKantor')" />
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left"><asp:Image ID="imgLokasiRumah" runat="server" /></div>
                    <div class="form_right"><asp:Image ID="imgLokasiKantor" runat="server" /></div>
                </div>
            </div>

            <div class="form_box">
                <div class="form_single box_important">
                    <asp:Panel ID="pnlScoring" runat="server" Visible="false">
                        <h3>
                            SCORE PEMBIAYAAN = 
                            <asp:Label ID="lblResult" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblGrade" runat="server"></asp:Label>
                        </h3>
                    </asp:Panel>
                </div>
            </div>
            </asp:Panel>            
            <asp:Panel runat="server" ID="pnlBalance">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        JOURNAL 
                    </h4>
                </div>
            </div>
            <div class="form_box">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgJournal" runat="server" CellSpacing="1" CellPadding="3" ShowFooter="true"
                    AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_general">                    
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemStyle CssClass="short_col"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA">     
                            <ItemStyle CssClass="middle_col" />
                            <ItemTemplate>                                
                                <asp:Label ID="lblPaymentAllocationDes" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PaymentAllocationDes") %>'>
                                </asp:Label>
                                <asp:Label ID="lblPaymentAllocationID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.PaymentAllocationID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblRefDesc" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.RefDesc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                Total 
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DEBET" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                                    FooterStyle-CssClass="item_grid_right ft">                                
                            <ItemTemplate>                                
                                <asp:Label ID="lblPost" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.Post") %>'>
                                </asp:Label>
                                <asp:Label ID="lblDebet" runat="server">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblDebetTotal" runat="server">
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KREDIT" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                                    FooterStyle-CssClass="item_grid_right ft">                                                                              
                            <ItemTemplate>                                
                                <asp:Label ID="lblAmount" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.Amount") %>'>
                                </asp:Label>
                                <asp:Label ID="lblKredit" runat="server">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblKreditTotal" runat="server">
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="txtTanggalSurvey" EventName="TextChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="form_button">
        <asp:Button runat="server" ID="btnProceed" Text="Proceed" CssClass="small button blue" />
        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="small button blue" />
        <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="small button gray" />
        <asp:Button runat="server" ID="btnupload" Text="Upload" CssClass="small button blue" />
    </div>
    </form>
</body>
</html>
