﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApplicationCreation.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ApplicationCreation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Application</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var x = screen.width
        var y = screen.height - 100
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>'; 
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinProductOfferingBranchView(pProductOfferingID, pProductID, pBranchID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductOfferingBranchView.aspx?ProductOfferingID=' + pProductOfferingID + '&ProductID=' + pProductID + '&BranchID=' + pBranchID, 'Product', 'left=50, top=10, width=900, height=650, menubar=0, scrollbars=1');
        }
    </script>

</head>
<body onload="gridGeneralSize('dtgPaging');" onresize="gridGeneralSize('dtgPaging')">
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR CUSTOMER</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" DataKeyField="CustomerID"
                        CellSpacing="1" CellPadding="3" BorderWidth="0px" OnSortCommand="Sorting" AutoGenerateColumns="False"
                        AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton CommandName="NewApp" ID="lnkNewApp" CausesValidation="false" runat="server"
                                        ToolTip="Buat Aplikasi Baru">BARU</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER" HeaderStyle-Width="250px">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Image ID="imgBadType" runat="server"></asp:Image>
                                    <asp:LinkButton ID="lnkCustName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CustomerType" SortExpression="CustomerType" HeaderText="P/C"
                                ItemStyle-CssClass="short_col"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT" ItemStyle-CssClass="address_col">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="BadType" HeaderText="BadType"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID" HeaderText="CustomerID">
                            </asp:BoundColumn>
                            <%--<asp:TemplateColumn HeaderText="CABANG">
                                <ItemTemplate>
                                    <%# Me.BranchName%>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:BoundColumn DataField="BranchInitialName" SortExpression="BranchInitialName" HeaderText="CABANG" ItemStyle-CssClass="address_col">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                        CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                        CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                        CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                        CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" CssClass="small buttongo blue" Text="Go"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtGopage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>
                    record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI APLIKASI BARU</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="vwCustomer.Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="vwCustomer.Address">Alamat</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">                
                <label class="label_general">
                    Jenis Customer</label>
                <asp:RadioButtonList ID="rboCust" runat="server" RepeatDirection="Horizontal"
                    CssClass="opt_single">
                    <asp:ListItem Selected="True" Value="False">Customer Baru</asp:ListItem>
                    <asp:ListItem Value="True">Customer Repeat Order</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue"
                CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
