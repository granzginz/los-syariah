﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetData_002.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.AssetData_002" %>

<%@ Register TagPrefix="cc1" Namespace="System.Web.UI.WebControls" Assembly="System.Web" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="uccmo" Src="../../../../webform.UserController/uccmo.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpAsset" Src="../../../../webform.UserController/ucLookUpAsset.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucsupplier" Src="../../../../webform.UserController/ucsupplier.ascx" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../../../Webform.UserController/UcBankAccount.ascx" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTab.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc7" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../../../Webform.UserController/ucAddressCity.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Asset Data</title>
   
       <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        $(document).ready(function () { 
            rboNamaBPKBSamaKontrakChange();
            //onclick_chkFU();
        });
        function rboNamaBPKBSamaKontrakChange() { 
            var customername = $('#HiddenFieldCustName').val(); 
            if (document.forms[0].rboNamaBPKBSamaKontrak_1.checked) { 
                document.getElementById('txtName').value = ''; 
            }
            else { 
                document.getElementById('txtName').value = customername; 
            }
        }

        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value;
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
        function onclick_chkFU(e) {
            var chek = e.children[0].checked;
            var indexVal = $(e).parent().parent().find('.ucDateCE').val();
            if (chek) { 
                $(e).parent().parent().find('.ddlNotes').attr("style", "display : inherit");
                $(e).parent().parent().find('.txtNotes').attr("style", "display : none");
                $(e).parent().parent().find('#dtgAssetDoc_UcTglFU_' + indexVal + '_txtDateCE_' + indexVal).attr("style", "display : inherit");
                //$(e).parent().parent().find('.txtTglFU').attr("style", "display : none"); 
            }
            else {
                $(e).parent().parent().find('.ddlNotes').attr("style", "display : none");
                $(e).parent().parent().find('.txtNotes').attr("style", "display : inherit");
                $(e).parent().parent().find('#dtgAssetDoc_UcTglFU_' + indexVal + '_txtDateCE_' + indexVal).attr("style", "display : none");
                //$(e).parent().parent().find('.txtTglFU').attr("style", "display : inherit"); 
            }
        }
        function UpdateKaroseri(a) {
            if (a == "True") {
                $('#rboKaroseri_0').prop('checked', true);
               // $('#divrbokaroseri').attr('style', 'display:none');
            } else {
                $('#rboKaroseri_1').prop('checked', true);
                //$('#divrbokaroseri').removeAttr('style');
            }
        }
        function CalculateTotal(value, tipe, typeDP) {
            var kar = $('#ucHargakaroseri_txtNumber').val();
            var otr = $('#ucOTR_txtNumber').val();
            var dp = $('#txtDP').val();
            var dpPersen = $('#txtDPPersen').val();
            var dpkar = $('#txtDPKaroseri').val();
            var dpPersenKar = $('#txtDPPersenKaroseri').val();
            var hitung;
            var hitungKar;
             
            if (kar == "undefined") {
                kar = "0";
            }

            if (tipe == 'A') {
                //hitung = parseInt(value.replace(/\s*,\s*/g, '')) / parseInt(otr.replace(/\s*,\s*/g, '')) * 100;
                //hitung = parseFloat(value.replace(/\s*,\s*/g, '')) / parseFloat(otr.replace(/\s*,\s*/g, '')) * 100; 
                if (typeDP == 'DP') {
                    //hitung = parseFloat(value.replace(/\s*,\s*/g, '')) / (parseFloat(otr.replace(/\s*,\s*/g, '')) + parseFloat(kar.replace(/\s*,\s*/g, ''))) * 100;
                    hitung = parseFloat(value.replace(/\s*,\s*/g, '')) / (parseFloat(otr.replace(/\s*,\s*/g, ''))) * 100;
                    $('#txtDPPersen').val(number_format(hitung));
                } else {
                    hitungKar = parseFloat(value.replace(/\s*,\s*/g, '')) / ( parseFloat(kar.replace(/\s*,\s*/g, ''))) * 100;
                    $('#txtDPPersenKaroseri').val(number_format(hitungKar));
                }
            } else {
                //hitung = parseInt(otr.replace(/\s*,\s*/g, '')) * parseInt(value.replace(/\s*,\s*/g, '')) / 100;
                //hitung = parseFloat(otr.replace(/\s*,\s*/g, '')) * parseFloat(value.replace(/\s*,\s*/g, '')) / 100; 
                if (typeDP == 'DP') {
                    //hitung = (parseFloat(otr.replace(/\s*,\s*/g, '')) + parseFloat(kar.replace(/\s*,\s*/g, ''))) * parseFloat(value.replace(/\s*,\s*/g, '')) / 100;
                    hitung = (parseFloat(otr.replace(/\s*,\s*/g, ''))) * parseFloat(value.replace(/\s*,\s*/g, '')) / 100;
                    $('#txtDP').val(number_format(hitung));
                } else {
                    hitungKar = ( parseFloat(kar.replace(/\s*,\s*/g, ''))) * parseFloat(value.replace(/\s*,\s*/g, '')) / 100;
                    $('#txtDPKaroseri').val(number_format(hitungKar));
                }                
            }
            var newDP = $('#txtDP').val();
            var newDPKar = $('#txtDPKaroseri').val();
            var calcPH = parseInt(otr.replace(/\s*,\s*/g, '')) + - parseFloat(newDP.replace(/\s*,\s*/g, ''))
            //window.alert(tes);
            //$('#lblTotalPembiayaan').html(number_format(parseInt(otr.replace(/\s*,\s*/g, '')) - parseInt(newDP.replace(/\s*,\s*/g, '')) + parseInt(kar.replace(/\s*,\s*/g, ''))));
            //$('#lblTotalPembiayaan').html(number_format(parseFloat(otr.replace(/\s*,\s*/g, '')) - parseFloat(newDP.replace(/\s*,\s*/g, ''))  + parseFloat(kar.replace(/\s*,\s*/g, ''))));
            //$('#lblTotalPembiayaan').html(number_format(parseFloat(otr.replace(/\s*,\s*/g, '')) + parseFloat(kar.replace(/\s*,\s*/g, '')) - parseFloat(newDP.replace(/\s*,\s*/g, '')) - parseFloat(newDPKar.replace(/\s*,\s*/g, ''))));

            $('#lblTotalPembiayaan').html(number_format(calcPH));
        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function showimagepreview(input,imgClient) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#' + imgClient).attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnlSupplier">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnlSupplier" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ENTRI ASSET</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                        <div style="float:left">
                            <label class="label_req">
                                Supplier/Dealer</label>                            
                            <asp:HiddenField runat="server" ID="hdfApplicationID" />      
                            <div style="display:none">
                            <asp:TextBox runat="server" ID="txtSupplierCode" Enabled="false" CssClass="small_text"></asp:TextBox>              
                            </div>
                            <asp:TextBox runat="server" ID="txtSupplierName" Enabled="false" CssClass="medium_text" AutoPostBack="true" ></asp:TextBox>
                        </div>
                        <div style="margin-top:2px; float:left">
                            <asp:Panel ID="pnlLookupSupplier" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Supplier.aspx?kode=" & txtSupplierCode.ClientID & "&nama=" & txtSupplierName.ClientID &"&ApplicationID="& hdfApplicationID.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                            <asp:RequiredFieldValidator ID="rfvtxtSupplier" runat="server" ErrorMessage="*" Display="Dynamic" 
                                CssClass="validator_general" ControlToValidate="txtSupplierName"></asp:RequiredFieldValidator>
                            </asp:Panel>
                        </div>
                        <div style="display: inline-block; padding-left:20px; margin-top:4px;">
                            <asp:Label runat="server" ID="lblSupplierStatus" ></asp:Label>
                        </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                        <div style="float:left">
                        <label class="label_req">
                            Nama Asset</label>                            
                        <asp:HiddenField runat="server" ID="hdfAssetCode" />     
                        <asp:HiddenField runat="server" ID="hdfAssetTypeID" />
                        
                            <asp:TextBox runat="server" ID="txtAssetName" Enabled="false" CssClass="medium_text"></asp:TextBox>
                        </div>
                        <div style="margin-top:2px">
                            <asp:Panel ID="pnlLookupAsset" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Asset.aspx?kode=" & hdfAssetCode.ClientID & "&nama=" & txtAssetName.ClientID &"&AssetTypeID="& hdfAssetTypeID.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" Display="Dynamic" 
                                CssClass="validator_general" ControlToValidate="txtAssetName"></asp:RequiredFieldValidator>                            
                            </asp:Panel>
                        </div> 
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Tahun Produksi
                        </label>
                        <asp:TextBox ID="txtYear" runat="server" MaxLength="4" CssClass="small_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFVYear" runat="server" ErrorMessage="Harap isi Tahun Produksi!"
                            ControlToValidate="txtYear" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" Type="Integer" MinimumValue="0"
                            ErrorMessage="Tahun harus < hari Ini" ControlToValidate="txtYear" Display="Dynamic"
                            CssClass="validator_general"></asp:RangeValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Tahun Salah"
                            ControlToValidate="txtYear" Display="Dynamic" ValidationExpression="\d{4}" CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="form_box" style="display:none" id="divrbokaroseri">
                <div>
                    <div class="form_left">
                        <label class="label_general">
                            Karoseri</label>
                        <asp:RadioButtonList runat="server" ID="rboKaroseri" CssClass="opt_single" RepeatDirection="Horizontal">
                            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
                            <asp:ListItem Value="False" Text="Tidak"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_button" runat="server" id="pnlNext">
                <asp:Button runat="server" ID="btnNext" Text="Next" CssClass="small button green" />
            </div>
            <asp:Panel runat="server" ID="pnlLookupKaroseri">
              <div class="form_title">
                <div class="form_single">
                    <h4>
                        SUPPLIER KAROSERI</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                        <div style="float:left">
                            <label class="label_req">
                                Supplier/Dealer</label>                            
                            <asp:HiddenField runat="server" ID="hdfKaroseriApplicationID" />      
                            <div style="display:none">
                            <asp:TextBox runat="server" ID="txtKaroseriSupplierCode" Enabled="false" CssClass="small_text"></asp:TextBox>              
                            </div>
                            <asp:TextBox runat="server" ID="txtKaroseriSupplierName" Enabled="false" CssClass="medium_text"></asp:TextBox>
                        </div>
                        <div style="margin-top:2px">
                            <asp:Panel ID="pnlLookupKaroseriSupplier" runat="server">
                            <button class="small buttongo blue" 
                            onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Supplier.aspx?kode=" & txtKaroseriSupplierCode.ClientID & "&nama=" & txtKaroseriSupplierName.ClientID &"&ApplicationID="& hdfKaroseriApplicationID.Value) %>','Daftar Supplier','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" Display="Dynamic" 
                                CssClass="validator_general" ControlToValidate="txtKaroseriSupplierName"></asp:RequiredFieldValidator>
                            </asp:Panel>               
                        </div>            
                </div>
            </div>
                <div class="form_box">
                    <div class="form_single">
                        <asp:Panel runat="server" ID="pnlSplitBayar" Visible="false">
                            <label>
                                Split Pembayaran
                            </label>
                            <asp:CheckBox ID="chkSplitBayar" runat="server" />
                        </asp:Panel>
                    </div>
                </div>
            </asp:Panel>
            <div class="form_box" runat="server" id="pnlOTR">
                <div class="form_left">
                    <label class="label_req">
                        Harga OTR
                    </label>
                    <%--<asp:TextBox ID="txtOTR" runat="server" MaxLength="15" AutoPostBack="true"></asp:TextBox>--%>
                    <uc7:ucnumberformat runat="server" id="ucOTR"></uc7:ucnumberformat>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Harap isi Harga OTR"
                            ControlToValidate="txtOTR" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <asp:Panel ID="pnlHargaKaroseri" runat="server" Visible="false">
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    Harga Karoseri
                                </label>
                                <%--<asp:TextBox ID="txtHargaKaroseri" runat="server" AutoPostBack="true"></asp:TextBox>--%>
                                <uc7:ucnumberformat runat="server" id="ucHargakaroseri"></uc7:ucnumberformat>
                            </div>
                        </div>
            </asp:Panel>
            <div class="form_button" runat="server" id="pnlNext2">
                <asp:Button runat="server" ID="btnNext2" Text="Next" CssClass="small button green" />
            </div>
            <asp:Panel runat="server" ID="pnlLast">
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Total Harga OTR
                            </label>
                            <div style="margin-left:-8px;display:inline">
                            <asp:Label ID="lblTotalHargaOTR" runat="server" CssClass="numberAlign regular_text"></asp:Label>
                            </div>
                        </div>
                        <div class="form_right">
                            <label>
                                Kondisi Asset
                            </label>
                            <asp:DropDownList ID="cboKondisiAsset" runat="server" Width="150PX"  AutoPostBack="true"  Enabled="false">
                                <asp:ListItem Text="BEKAS" Value="U" ></asp:ListItem>
                                <asp:ListItem Text="BARU" Value="N" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>Uang Muka OTR
                                <asp:textbox id="txtDPPersen" runat="server"  onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                            onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                            onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign small_text" OnChange="CalculateTotal(this.value,'P', 'DP')" autocomplete="off">0</asp:TextBox><label class="label_auto numberAlign"> %</label>
                            </label>
                                <asp:textbox id="txtDP" runat="server"   onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                            onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                            onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="CalculateTotal(this.value,'A', 'DP')" autocomplete="off">0</asp:TextBox> 
                                
                        </div>
                        <div class="form_right">
                            <label>
                                Grade
                            </label>
                            <asp:DropDownList ID="cboAssetGrade" runat="server" Width="150PX" Enabled="false"  AutoPostBack="true" />
                        </div>
                    </div>
                </div>

                 <asp:Panel ID="pnlDPKaroseri" runat="server" Visible="false">
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>Uang Muka Karoseri
                                <asp:textbox id="txtDPPersenKaroseri" runat="server"  onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                            onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                            onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign small_text" OnChange="CalculateTotal(this.value,'P', 'KR')" autocomplete="off">0</asp:TextBox><label class="label_auto numberAlign"> %</label>
                            </label>
                                <asp:textbox id="txtDPKaroseri" runat="server"   onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                            onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                            onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="CalculateTotal(this.value,'A', 'KR')" autocomplete="off">0</asp:TextBox> 
                                
                        </div>
                        <div class="form_right"> 
                        </div>
                    </div>
                </div>
                </asp:Panel>

                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Total Pembagian Netto
                            </label>
                            <div style="margin-left:-8px;display:inline">
                            <asp:Label runat="server" ID="lblTotalPembiayaan"  CssClass="numberAlign regular_text"></asp:Label>
                            </div>
                        </div>
                        <div class="form_right" style="display:none">
                            <label>
                                Uang Muka bayar di
                            </label>
                            <asp:DropDownList ID="cboUangMukaBayar" runat="server" Width="150PX">
                                <asp:ListItem Selected="True" Text="SUPPLIER" Value="S"></asp:ListItem>
                                <asp:ListItem Text="MULTI FINANCE" Value="A"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                MRP/Harga Pasar
                            </label>
                            <asp:Label ID="lblPHJMB" runat="server" CssClass="numberAlign regular_text"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label class="label_req">
                                Penggunaan
                            </label>
                            <asp:DropDownList ID="cboUsage" runat="server" Width="150PX" AutoPostBack="true" >
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="cboUsage"
                                CssClass="validator_general" Display="Dynamic" ErrorMessage="Harap pilih Penggunaan!"
                                InitialValue="Select One" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                         <div class="form_left">
                            <label>
                                Harga OTR Pembanding
                            </label>
                            <asp:Label ID="lblOTRPembanding" runat="server" CssClass="numberAlign"></asp:Label>
                        </div>
                        <div class="form_right">
                            <label>
                                Pencairan Ke
                            </label>
                            <asp:DropDownList ID="cboPencairanKe" runat="server" Width="150PX">
                                <asp:ListItem Text="SUPPLIER" Value="S" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="CUSTOMER" Value="A"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <%--<div class="form_box_title">
                <div class="form_single">
                    <h4>
                        ASSET INFO</h4>
                </div>
            </div>--%>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            ASSET INFO</h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <div class="form_left_uc">
                        <asp:DataGrid ID="dtgAttribute" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                            BorderWidth="0" BorderStyle="none" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid_attr" />
                            <Columns>
                                <asp:TemplateColumn>
                                    <ItemStyle CssClass="label_col item_grid_attr_collabel" />
                                    <ItemTemplate> 
                                        <asp:Label ID="lblName" Text='<%#Container.DataItem("Name")%>' runat="server" > 
                                        </asp:Label> 
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemStyle CssClass="left_col" />
                                    <ItemTemplate> 
                                        <asp:TextBox ID="txtAttribute" runat="server" MaxLength='<%# DataBinder.eval(Container,"DataItem.AttributeLength") %>'
                                            Text='<%# DataBinder.eval(Container,"DataItem.AttributeContent") %>'>
                                        </asp:TextBox>
                                       
                                        <asp:Label ID="lblVAttribute" runat="server" cssClass="validator_general">Attribute already exists!</asp:Label>
                                       <%-- <asp:RegularExpressionValidator ID="RVAttribute" Enabled='<%# DataBinder.eval(Container,"DataItem.AttributeType") %>'
                                            runat="server" Display="Dynamic" ControlToValidate="txtAttribute" ErrorMessage="Harap isi dengan Angka"
                                            ValidationExpression="\d*" cssClass="validator_general">
                                        </asp:RegularExpressionValidator>--%>
                                        <asp:RequiredFieldValidator ID="RFVlblName" runat="server" ControlToValidate="txtAttribute"
                                            Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn Visible="False" DataField="AttributeID"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div class="form_right_uc">
                        <div class="form_single_wb">
                            <asp:Label ID="lblSerial1" runat="server" ></asp:Label>
                           <%-- <asp:TextBox ID="txtSerial1" runat="server" MaxLength="50" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox>--%>
                             <asp:TextBox ID="txtSerial1" runat="server" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFVSerial1" runat="server" ControlToValidate="txtSerial1"
                                Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_single_wb">
                            <asp:Label ID="lblSerial2" runat="server" ></asp:Label>
                           <%-- <asp:TextBox ID="txtSerial2" runat="server" MaxLength="50"  onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox>--%>
                             <asp:TextBox ID="txtSerial2" runat="server" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFVSerial2" runat="server" ControlToValidate="txtSerial2"
                                Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <%--<div class="form_box">
                <div>
                    <div class="form_left">
                        <asp:Label ID="lblSerial1" runat="server" CssClass="label"></asp:Label>
                        <asp:TextBox ID="txtSerial1" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFVSerial1" runat="server" ControlToValidate="txtSerial1"
                            Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <asp:Label ID="lblSerial2" runat="server" CssClass="label"></asp:Label>
                        <asp:TextBox ID="txtSerial2" runat="server" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RFVSerial2" runat="server" ControlToValidate="txtSerial2"
                            Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>--%>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            REGISTRASI DOKUMEN ASSET</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_general">
                            Nama Dokumen Asset sama dengan Kontrak?
                        </label> 
                          <asp:RadioButtonList ID="rboNamaBPKBSamaKontrak"  onclick="<%#rboNamaBPKBSamaKontrakChange()%>" runat="server" CssClass="opt_single" RepeatDirection="Horizontal">
                            <asp:ListItem Value="True" >Ya</asp:ListItem>
                            <asp:ListItem Value="False" >Tidak</asp:ListItem>
                        </asp:RadioButtonList> 
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Dokumen Asset Pengganti?
                        </label>
                        <asp:RadioButtonList ID="rboBPKBPengganti" runat="server" AutoPostBack="True" CssClass="opt_single"
                            RepeatDirection="Horizontal">
                            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
                            <asp:ListItem Value="False" Text="Tidak" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Nama Dokumen Asset
                        </label>
                        <asp:HiddenField runat="server" ID="HiddenFieldCustName" />   
                        <asp:TextBox ID="txtName" runat="server" MaxLength="50" CssClass="long_text"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtName"
                            Enabled="true" ErrorMessage="Harap isi nama dokumen asset" CssClass="validator_general"
                            Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Dokumen Asset an Badan Usaha
                        </label>
                        <asp:RadioButtonList ID="rboBPKBanBadanUsaha" runat="server" AutoPostBack="true" OnSelectedIndexChanged="AssetDocumentFilter"
                            CssClass="opt_single" RepeatDirection="Horizontal">
                            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
                            <asp:ListItem Value="False" Text="Tidak" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <asp:Button ID="btnCopyAddress" runat="server" CausesValidation="False" Text="Copy Address"
                            CssClass="small buttongo blue"></asp:Button>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Izin Trayek
                        </label>
                        <asp:RadioButtonList ID="rboIjinTrayek" runat="server" AutoPostBack="True" CssClass="opt_single" OnSelectedIndexChanged="AssetDocumentFilter"
                            RepeatDirection="Horizontal">
                            <asp:ListItem Value="True" Text="Ya"></asp:ListItem>
                            <asp:ListItem Value="False" Text="Tidak" Selected="True"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <asp:Panel ID="pnlInformasiTrayek" runat="server" >
                    <div class="form_box" >
                        <div class="form_left">&nbsp;
                        </div>
                        <div class="form_right">
                            <label class="label">
                                Ijin Trayek Atas Nama
                            </label>
                            <asp:TextBox ID="txtTrayekAtasNama" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box" >
                        <div class="form_left">&nbsp;
                        </div>
                        <div class="form_right">
                            <label class="label">
                                Trayek Jurusan
                            </label>
                            <asp:TextBox ID="txtJurusan" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box" >
                        <div class="form_left">&nbsp;
                        </div>
                        <div class="form_right">
                            <label class="label">
                                Buku Keur
                            </label>
                            <asp:TextBox ID="txtBukuKeur" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </asp:Panel>
                <div class="form_box">
                    <uc1:ucAddress id="UCAddress" runat="server"></uc1:ucAddress>
                </div>
              
                <div class="form_box" runat="server" id="divTglSTNKNotes" visible="false">
                    <div class="form_single">
                        <label class="label_req label_general">
                            Alasan expired
                        </label>
                        <asp:TextBox ID="txtTglSTNKNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox_uc"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rvTglSTNKNotes" ControlToValidate="txtTglSTNKNotes"
                            Enabled="false" ErrorMessage="Harap isi alasan tanggal STNK expired!" CssClass="validator_general"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Keterangan Dokumen Asset
                        </label>
                        <asp:TextBox ID="txtAssetNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox_uc"></asp:TextBox>
                    </div>
                </div>
                <div id="divregstnk" runat="server">
                   <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                           REGISTRASI STNK</h4>
                    </div>
                </div>
                  <div class="form_box">
                    <div class="form_left">
                        <label  runat="server" id="lblTanggalSTNK">
                            Tanggal Pajak STNK
                        </label>
                        <asp:TextBox ID="txtTanggalSTNK" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTanggalSTNK"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator runat="server" ID="rfvTanggalSTNK" ControlToValidate="txtTanggalSTNK"
                            Enabled="true" ErrorMessage="Harap isi tanggal STNK!" CssClass="validator_general"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            ASURANSI ASSET</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Di Asuransi Oleh
                        </label>
                        <asp:DropDownList ID="cboInsuredBy" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap Pilih Di Asuransi Oleh"
                            ControlToValidate="cboInsuredBy" Display="Dynamic" InitialValue="Select One"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Di bayar oleh
                        </label>
                        <asp:DropDownList ID="cboPaidBy" runat="server">
                        </asp:DropDownList>
                        <asp:Label ID="lblVPaidBy" runat="server" Visible="False" CssClass="validator_general">Please Select One!</asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap Pilih Di bayar oleh"
                            ControlToValidate="cboPaidBy" Display="Dynamic" InitialValue="Select One"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box_title">
                    <div>
                        <div class="form_left">
                            <h4>
                                KARYAWAN</h4>
                        </div>
                        <div class="form_right">
                            <h4>
                                KARYAWAN SUPPLIER
                            </h4>
                            <label>
                            </label>
                        </div>
                    </div>
                </div>
                <asp:UpdatePanel ID="updPanelSalesman" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form_box">
                            <div>
                                <div class="form_left">
                                    <label class="label_req label_auto">
                                    CMO &nbsp;&nbsp;</label>
                                    <asp:DropDownList ID="cbocmo" runat="server" >
                                    </asp:DropDownList>
                                </div>
                                <div class="form_right">
                                    <label class="label_req">
                                        Salesman
                                    </label>
                                    <asp:DropDownList ID="cboSalesman" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Harap pilih Salesman"
                                        ControlToValidate="cboSalesman" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="form_box_hide">
                            <div>
                                <div class="form_left">
                                </div>
                                <div class="form_right">
                                    <label>
                                        Sales Supervisor
                                    </label>
                                    <asp:DropDownList ID="cboSalesSpv" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="cboSalesman" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <div class="form_box_hide">
                    <div>
                        <div class="form_left">
                            <label>
                            </label>
                        </div>
                        <div class="form_right">
                            <label>
                                Supplier Admin
                            </label>
                            <asp:DropDownList ID="cboSupplierAdm" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DOKUMEN ASSET</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <asp:DataGrid ID="dtgAssetDoc" runat="server" AutoGenerateColumns="False" BorderWidth="0"
                            BorderStyle="None" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" /> 
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <ItemStyle CssClass="short_col" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="AssetDocName" HeaderText="DOKUMEN" ItemStyle-Width="130px"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="NO DOKUMEN" ItemStyle-Width="250px">
                                    <%--<ItemStyle CssClass="short_col" />--%>
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsNoRequired" runat="server"  Visible="false"  Text='<%# DataBinder.eval(Container,"DataItem.IsNoRequired") %>'></asp:Label>
                                        <asp:TextBox ID="txtNumber" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.DocumentNo") %>'></asp:TextBox>
                                        <asp:Label ID="lblVNumber" runat="server" Visible="false" CssClass="validator_general">Nomor sudah ada!</asp:Label>
                                        <asp:Label ID="lblVNumber2" runat="server" Visible='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>'
                                            CssClass="validator_general">Harus diisi!</asp:Label>
                                        <asp:RequiredFieldValidator ID="RFVNumber" runat="server" ErrorMessage="Harap isi dengan Angka"
                                            ControlToValidate="txtNumber" Display="Dynamic" Enabled='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>'
                                            CssClass="validator_general">
                                        </asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TGL DOKUMEN" ItemStyle-Width="100px">
                                    <%--<ItemStyle CssClass="short_col" />--%>
                                    <ItemTemplate>
                                        <uc7:ucDateCE id="ucTglDokumen" runat="server" text='<%# DataBinder.eval(Container,"DataItem.TglDokument", "{0:dd/MM/yyyy}") %>' ></uc7:ucDateCE>    
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ADA">
                                    <ItemStyle CssClass="short_col" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk" runat="server" Checked='<%# DataBinder.eval(Container,"DataItem.IsDocExist") %>'>
                                        </asp:CheckBox>
                                        <asp:Label ID="lblVChk" runat="server"  Visible="false" CssClass="validator_general">Harus dicentang!</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TGL FU" ItemStyle-Width="100px">  
                                    <ItemTemplate>
                                        <asp:Label ID="lblTglFU" runat="server"  Visible="false"  Text='<%# DataBinder.eval(Container,"DataItem.TglJanji", "{0:dd/MM/yyyy}") %>'></asp:Label> 
                                        <uc7:ucDateCE id="UcTglFU" runat="server" ></uc7:ucDateCE>   
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="FU">
                                    <ItemStyle CssClass="short_col" />
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" ID="hdnIndex" CssClass="ucDateCE" Value="<%# Container.ItemIndex %>" style="display : none;"></asp:TextBox>        
                                        <asp:CheckBox ID="chkFU" runat="server" Checked='<%# DataBinder.eval(Container,"DataItem.IsFollowUp") %>' onchange="onclick_chkFU(this);">
                                        </asp:CheckBox>
                                        <asp:Label ID="lblVChkFU" runat="server"  Visible="false" CssClass="validator_general">Harus dicentang!</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CATATAN">
                                    <ItemTemplate>                                        
                                        <asp:Label ID="lblNotes" runat="server"  Visible="false"  Text='<%# DataBinder.eval(Container,"DataItem.Notes") %>'></asp:Label>                                           
                                        <asp:DropDownList runat="server" ID="ddlNotes" CssClass="ddlNotes">
                                            <asp:ListItem Text="INTERNAL MEMO" Value="Internal Memo" />
                                            <asp:ListItem Text="DISUSULKAN" Value="Disusulkan" />
                                        </asp:DropDownList>
                                        <asp:TextBox runat="server" ID="txtNotes" CssClass="txtNotes"></asp:TextBox>                                      
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn Visible="false">
                                    <ItemTemplate>
                                        <asp:TextBox runat="server" Visible="false" ID="MandatoryForNewAsset" Text='<%# DataBinder.eval(Container,"DataItem.MandatoryForNewAsset") %>' />
                                        <asp:TextBox runat="server" Visible="false" ID="MandatoryForUsedAsset" Text='<%# DataBinder.eval(Container,"DataItem.MandatoryForUsedAsset") %>' />
                                        <asp:TextBox runat="server" Visible="false" ID="AssetDocID" Text='<%# DataBinder.eval(Container,"DataItem.AssetDocID") %>' />
                                        <asp:TextBox runat="server" Visible="false" ID="IsMainDoc" Text='<%# DataBinder.eval(Container,"DataItem.IsMainDoc") %>' />
                                        <asp:TextBox runat="server" Visible="false" ID="IsValueNeeded" Text='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            FOTO ASSET</h4>
                    </div>
                </div>
                <asp:UpdatePanel ID="updpanelimageasset" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                     <div class="form_box">
                        <div class="form_left">
                            <label class="label">
                                Tampak Depan
                            </label>
                            <asp:FileUpload ID="uplTampakDepan" runat="server" onchange="showimagepreview(this,'imgTampakDepan')"  />
                        </div>
                        <div class="form_right">
                            <label class="label">
                                Tampak Belakang
                            </label>
                            <asp:FileUpload ID="uplTampakBelakang" runat="server"  onchange="showimagepreview(this,'imgTampakBelakang')" />
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <div>
                                <asp:Image ID="imgTampakDepan" runat="server" />
                            </div>
                        </div>
                        <div class="form_right">
                            <div>
                                <asp:Image ID="imgTampakBelakang" runat="server" />
                            </div>
                        </div>
                    </div>

                     <div class="form_box">
                        <div class="form_left">
                            <label class="label">
                                Tampak Kiri
                            </label>
                            <asp:FileUpload ID="uplTampakKiri" runat="server" onchange="showimagepreview(this,'imgTampakKiri')" />
                        </div>
                        <div class="form_right">
                            <label class="label">
                                Tampak Kanan
                            </label>
                            <asp:FileUpload ID="uplTampakKanan" runat="server" onchange="showimagepreview(this,'imgTampakKanan')"  />
                        </div>
                    </div> 
                    <div class="form_box">
                        <div class="form_left">
                            <div>
                                <asp:Image ID="imgTampakKiri" runat="server" />
                            </div>
                        </div>
                        <div class="form_right">
                            <div>
                                <asp:Image ID="imgTampakKanan" runat="server" />
                            </div>
                        </div>
                    </div>


                    <div class="form_box">
                        <div class="form_left">
                            <label class="label">
                                Tampak Dashboard
                            </label>
                            <asp:FileUpload ID="uplTampakDashboard" runat="server" onchange="showimagepreview(this,'imgTampakDashboard')"  />
                        </div>
                        <div class="form_right">
                            <%--<asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="small button blue"  />--%>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_left">
                            <div>
                                <asp:Image ID="imgTampakDashboard" runat="server" />
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger   ControlID="btnupload" />
                </Triggers>
                </asp:UpdatePanel>


                <div class="form_button">
                    <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="small button blue"  />
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"  CausesValidation="True" />
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNext2" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnCopyAddress" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cboAssetGrade" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="rboBPKBanBadanUsaha" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="rboIjinTrayek" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="cboUsage" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="cboKondisiAsset" EventName="SelectedIndexChanged" />  
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
