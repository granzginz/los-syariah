﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FinancialData.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.FinancialData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Financial Data</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div>
            <div class="form_single">
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                    MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                <h3>
                    DAFTAR APLIKASI</h3>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:Panel ID="pnlList" runat="server">
                <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" DataKeyField="CustomerID">
                    <AlternatingItemStyle></AlternatingItemStyle>
                    <ItemStyle></ItemStyle>
                    <HeaderStyle CssClass="th"/>
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton CommandName="Action" CausesValidation="false" ID="lnkAction" runat="server">DATA FINANCIAL</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Agreement.ApplicationID" HeaderText="NO APLIKASI">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Customer.Name" HeaderText="NAMA CUSTOMER">
                            <HeaderStyle></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkCustName" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProductOffering.Description" HeaderText="PRODUCT JUAL">
                            <HeaderStyle></HeaderStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkProdOff" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Description") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="BranchEmployee.EmployeeName" HeaderText="NAMA CMO">
                            <HeaderStyle></HeaderStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkAO" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.EmployeeName") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="CustomerID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="EmployeeID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="InterestType"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="InstallmentScheme"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="ProductOfferingID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="ProductID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="BranchID"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
                <div>
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text" >1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"/>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server"  Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtGopage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </asp:Panel>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_single">
                <h4>
                    CARI DATA FINANCIAL</h4>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan
            </label>
            <asp:DropDownList ID="cboSearch" runat="server">
                <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                <asp:ListItem Value="Agreement.ApplicationID">ID Application</asp:ListItem>
                <asp:ListItem Value="ProductOffering.Description">Product Jual</asp:ListItem>
                <asp:ListItem Value="BranchEmployee.EmployeeName">Nama CMO</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text = "Search" CssClass="small button blue">
        </asp:ImageButton>
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:ImageButton>
    </div>
    </form>
</body>
</html>
