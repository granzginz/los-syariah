﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AssetData.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.AssetData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Asset Data</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
            <h3>
                DAFTAR APLIKASI</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" DataKeyField="CustomerID">
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <HeaderStyle Width="12%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton CommandName="Action" CausesValidation="false" ID="lnkAction" runat="server">INPUT ASSET</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Agreement.ApplicationID" HeaderText="NO APLIKASI">
                            <HeaderStyle Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Customer.Name" HeaderText="NAMA CUSTOMER">
                            <HeaderStyle Width="35%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkCust" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProductOffering.Description" HeaderText="PRODUCT JUAL">
                            <HeaderStyle Width="35%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkProdOff" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Description") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="CustomerID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="AssetTypeID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ProductOfferingID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ProductID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="BranchID" Visible="false"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
                <div>
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text" >1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"/>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" 
                         Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" ControlToValidate="txtGopage" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" 
                         ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage"
                        Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <h4>
                    CARI DATA ASSET</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cari Berdasarkan</label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="Agreement.ApplicationID">No Aplikasi</asp:ListItem>
                        <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                        <asp:ListItem Value="ProductOffering.Description">Product Jual</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue"
                CausesValidation="False"/>
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False"/></div>
    </asp:Panel>
    </form>
</body>
</html>
