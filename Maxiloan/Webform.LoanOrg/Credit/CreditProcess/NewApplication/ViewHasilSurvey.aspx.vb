﻿#Region "Import"
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Linq
Imports System.Web.Services
Imports System.IO
Imports System.Collections.Generic
Imports System.Drawing

#End Region

Public Class ViewHasilSurvey
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTab
    Protected WithEvents UcBiayaLainnya As UcIncentiveGridView
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyViewTabPhone
    Dim pathFolder As String

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property SupplierID() As String
        Set(ByVal SupplierID As String)
            ViewState("SupplierID") = SupplierID
        End Set
        Get
            Return ViewState("SupplierID").ToString
        End Get
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property SupplierEmployeeID() As String
        Get
            Return ViewState("SupplierEmployeeID").ToString
        End Get
        Set(ByVal SupplierEmployeeID As String)
            ViewState("SupplierEmployeeID") = SupplierEmployeeID
        End Set
    End Property
    Property SupplierEmployeePosition() As String
        Get
            Return ViewState("SupplierEmployeePosition").ToString
        End Get
        Set(ByVal SupplierEmployeePosition As String)
            ViewState("SupplierEmployeePosition") = SupplierEmployeePosition
        End Set
    End Property
    Property TransID() As String
        Get
            Return ViewState("TransID").ToString
        End Get
        Set(ByVal TransID As String)
            ViewState("TransID") = TransID
        End Set
    End Property
    Property PolaTransaksi() As String
        Get
            Return ViewState("PolaTransaksi").ToString
        End Get
        Set(ByVal PolaTransaksi As String)
            ViewState("PolaTransaksi") = PolaTransaksi
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Property Style() As String
        Get
            Return ViewState("Style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property

    Public Property pageSource() As String
        Get
            Return CType(ViewState("pageSource"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("pageSource") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oController As New HasilSurveyController
    Private m_Controller As New AssetDataController
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then Exit Sub
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID").ToString.Trim

            InitialPageLoad()
            ModVar.StrConn = GetConnectionString()
            ModVar.SuppilerID = Me.SupplierID
            ModVar.ApplicationID = Me.ApplicationID
            'ModVar.BranchId = Me.sesBranchId.Replace("'", "")
            ModVar.BranchId = ucViewApplication1.BranchID.Replace("'", "")
            getHasilSurvey()
        End If
    End Sub    

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.SupplierID = .SupplierID
            Me.PolaTransaksi = .PolaTransaksi
        End With

        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        ucApplicationTab1.ApplicationID = Me.ApplicationID
        ucApplicationTab1.selectedTab("Survey")
        ucApplicationTab1.setLink()
    End Sub
  
    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            '.BranchId = Me.sesBranchId.Replace("'", "")
            .BranchId = ucViewApplication1.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            LblAreaPhoneHome.Text = .AreaPhoneHome
            LblAreaPhoneOffice.Text = .AreaPhoneOffice
            LblPhoneHome.Text = .PhoneHome
            LblPhoneOffice.Text = .PhoneOffice
            LblHandphone.Text = .SurveyCellphone
            LblEmergencyPhone.Text = .EmergencyPhoneContact
            LblSurveyDate.Text = .AgreementSurveyDate.ToString("dd/MM/yyyy")
            If oPar.CaraSurvey.ToString = "V" Then
                LblCaraSurvey.Text = "VISIT"
            ElseIf oPar.CaraSurvey.ToString = "T" Then
                LblCaraSurvey.Text = "TELEPON"
            End If
            LblLamaUsaha.Text = .LamaUsaha
            LblNameUsaha.Text = .NamaUsaha
            LblAlamatUsaha.Text = .AlamatUsaha1
            If oPar.AlamatUsaha2 = "P" Then
                LblAlamatUsaha2.Text = "Pemohon"
            ElseIf oPar.AlamatUsaha2 = "I" Then
                LblAlamatUsaha2.Text = "Istri/Suami"
            ElseIf oPar.AlamatUsaha2 = "K" Then
                LblAlamatUsaha2.Text = "Keluarga Besar"
            ElseIf oPar.AlamatUsaha2 = "L" Then
                LblAlamatUsaha2.Text = "Lainnya"
            End If
            If oPar.FotoTempatUsaha = "Y" Then
                LblFotoTempatUsaha.Text = "Ada"
            ElseIf oPar.FotoTempatUsaha = "T" Then
                LblFotoTempatUsaha.Text = "Tidak"
            End If
            LblAlasanFotoTempatUsaha.Text = .AlasanFotoTempatUsaha
            If oPar.StockBarangDagangan = "Y" Then
                LblStockBrgDagangan.Text = "Ada"
            ElseIf oPar.StockBarangDagangan = "T" Then
                LblStockBrgDagangan.Text = "Tidak"
            End If
            LblAlasanStockBarangDagangan.Text = .AlasanStockBarangDagangan
            If oPar.BonBuktiUsaha = "Y" Then
                LblBonBuktiUsaha.Text = "Ada"
            ElseIf oPar.BonBuktiUsaha = "T" Then
                LblBonBuktiUsaha.Text = "Tidak"
            End If
            LblAlasanbonBuktiUsaha.Text = .AlasanBonBuktiUsaha
            LblSKUNo.Text = .SKUNo
            LblTglSKU.Text = .TanggalSKU.ToString("dd/MM/yyyy")
            LblSIUPNo.Text = .SIUPNo
            LblTglSIUP.Text = .TanggalSIUP.ToString("dd/MM/yyyy")
            LblTDPNo.Text = .TDPNo
            LblTglTDP.Text = .TanggalTDPNo.ToString("dd/MM/yyyy")
            LblSKDPNo.Text = .SKDPNo
            LblTglSKDP.Text = .TanggalSKDP.ToString("dd/MM/yyyy")
            If oPar.SlipGaji = "Y" Then
                LblSlipGaji.Text = "Ada"
            ElseIf oPar.SlipGaji = "T" Then
                LblSlipGaji.Text = "Tidak"
            End If
            LblAlasanSlipGaji.Text = .AlasanSlipGaji
            If oPar.SKPenghasilan = "Y" Then
                LblSKpenghasilan.Text = "Ada"
            ElseIf oPar.SKPenghasilan = "T" Then
                LblSKpenghasilan.Text = "Tidak"
            End If
            LblAlasanSKPenghasilan.Text = .AlasanSKPenghasilan
            LblJabatan.Text = .Jabatan
            If oPar.NPWPPribadi = "Y" Then
                LblNPWPPribadi.Text = "Ada"
            ElseIf oPar.NPWPPribadi = "T" Then
                LblNPWPPribadi.Text = "Tidak"
            End If
            LblNPWPNo.Text = .NPWPNo
            LblUraianUsaha.Text = .UraianUsahaPekerjaan
            LblAlurUsaha.Text = .AlurUsaha
            LblKapasitasKonsumen.Text = .KapasitasKonsumen
            LblKesimpulanCapacity.Text = .AnalisaCapacity

            'LblStatusKepemilikan.Text = oPar.StatusKepemilikan
            LblAtasNama.Text = .AtasNama
            LblPBBAJBSHMAtasNama.Text = .PBBAJBSHMatasNama
            LblTahunTagihan.Text = .TahunTagihanPBBAJBSHM
            If oPar.PBBAJBSHM = "Y" Then
                LblPBBAJBSHM.Text = "Ada"
            ElseIf oPar.PBBAJBSHM = "T" Then
                LblPBBAJBSHM.Text = "Tidak"
            End If

            LblAlasanPBBAJBSHM.Text = .AlasanPBBAJBSHM
            LblRekListrikAtasNama.Text = .RekListrikAtasNama
            LblBulanTagihan.Text = .BulanTagihanRekListrik
            If oPar.RekListrik = "Y" Then
                LblRekListrik.Text = "Ada"
            ElseIf oPar.RekListrik = "T" Then
                LblRekListrik.Text = "Tidak"
            End If

            LblAlasanrekListrik.Text = .AlasanRekListrik
            LblLuasTanah.Text = .LuasTanah
            LblLuasBangunan.Text = .LuasBangunan
            LblTaksiranNilaiJual.Text = FormatNumber(CStr(.TaksiranNilaiJual), 0)
            LblLamaTinggal.Text = .LamaTinggal
            LblAssetLain.Text = .AssetLain
            If oPar.FotoRumah = "Y" Then
                LblFotoRumah.Text = "Ada"
            ElseIf oPar.FotoRumah = "T" Then
                LblFotoRumah.Text = "Tidak"
            End If

            LblAlasanFotoRumah.Text = .AlasanFotoRumah
            LblKondisiBangunan.Text = .KondisiBangunan
            LblNoKTPPemohon.Text = .NoKTPPemohon
            LblTglKTPPemohon.Text = .TanggalKTPPemohon.ToString("dd/MM/yyyy")
            If oPar.KTPPemohon = "Y" Then
                LblKTPPemohon.Text = "Ada"
            ElseIf oPar.KTPPemohon = "T" Then
                LblKTPPemohon.Text = "Tidak"
            End If

            LblAlasanKTPPemohon.Text = .AlasanKTPPemohon
            LblNoKTPPasangan.Text = .NoKTPPasangan
            LblTglKTPPasangan.Text = .TanggalNoKTPPasangan.ToString("dd/MM/yyyy")
            If oPar.KTPPasangan = "Y" Then
                LblKTPPasangan.Text = "Ada"
            ElseIf oPar.KTPPasangan = "T" Then
                LblKTPPasangan.Text = "Tidak"
            End If

            LblAlasanKTPPasangan.Text = .AlasanKTPPasangan
            LblNoKK.Text = .NoKK
            LblTglNoKK.Text = .TanggalNoKartuKeluarga.ToString("dd/MM/yyyy")
            If oPar.BukuNikah = "Y" Then
                LblBukuNikah.Text = "Ada"
            ElseIf oPar.BukuNikah = "T" Then
                LblBukuNikah.Text = "Tidak"
            End If

            LblAlasanBukuNikah.Text = .AlasanBukuNikah
            LblJumlahTanggungan.Text = .JumlahTanggungan
            LblKesimpulanCapital.Text = .AnalisaCapital

            LblKunjunganRumahbertemu.Text = .KunjunganRumahBertemu
            If oPar.KondisiRumah = "A" Then
                LblKondisiRumah.Text = "SANGAT TERAWAT"
            ElseIf oPar.KondisiRumah = "B" Then
                LblKondisiRumah.Text = "TERAWAT"
            ElseIf oPar.KondisiRumah = "C" Then
                LblKondisiRumah.Text = "TIDAK TERAWAT"
            End If

            If oPar.PerabotanRumah = "M" Then
                LblPerabotanRumah.Text = "MEWAH"
            ElseIf oPar.PerabotanRumah = "S" Then
                LblPerabotanRumah.Text = "STANDARD"
            ElseIf oPar.PerabotanRumah = "L" Then
                LblPerabotanRumah.Text = "LAINNYA"
            End If

            If oPar.GarasiMobil = "Y" Then
                LblGarasiMobil.Text = "Ada"
            ElseIf oPar.GarasiMobil = "T" Then
                LblGarasiMobil.Text = "Tidak Ada"
            End If

            LblGarasiUnit.Text = .GarasiUntukBerapaUnit
            LblAlasanGarasiUnit.Text = .AlasanGarasiUntukBerapaUnit
            If oPar.LokasiRumah = "U" Then
                LblLokasiRumah.Text = "JALAN UTAMA"
            ElseIf oPar.LokasiRumah = "B" Then
                LblLokasiRumah.Text = "JALAN BIASA"
            ElseIf oPar.LokasiRumah = "G" Then
                LblLokasiRumah.Text = "GANG"
            End If

            If oPar.JalanMasukDilalui = "2" Then
                LblJalanMasukDilalui.Text = "2 MOBIL"
            ElseIf oPar.JalanMasukDilalui = "1" Then
                LblJalanMasukDilalui.Text = "1 MOBIL"
            ElseIf oPar.JalanMasukDilalui = "0" Then
                LblJalanMasukDilalui.Text = "TDK MASUK MOBIL"
            End If

            If oPar.KondisiJalanDepanRumah = "B" Then
                LblKondisiJlnDpnRumah.Text = "Beton"
            ElseIf oPar.KondisiJalanDepanRumah = "A" Then
                LblKondisiJlnDpnRumah.Text = "Aspal"
            ElseIf oPar.KondisiJalanDepanRumah = "C" Then
                LblKondisiJlnDpnRumah.Text = "Conblock"
            ElseIf oPar.KondisiJalanDepanRumah = "L" Then
                LblKondisiJlnDpnRumah.Text = "Lainnya"
            End If

            If oPar.KeadaanLingkungan = "A" Then
                LblKeadaanLingkungan.Text = "Cluster"
            ElseIf oPar.KeadaanLingkungan = "B" Then
                LblKeadaanLingkungan.Text = "Perumahan"
            ElseIf oPar.KeadaanLingkungan = "C" Then
                LblKeadaanLingkungan.Text = "Perkampungan"
            End If

            LblKunjunganKantorBertemu.Text = .KunjunganKantorBertemu
            If oPar.JumlahKaryawan = "A" Then
                LblJumlahKaryawan.Text = "< 20"
            ElseIf oPar.JumlahKaryawan = "B" Then
                LblJumlahKaryawan.Text = "20 < 50"
            ElseIf oPar.JumlahKaryawan = "C" Then
                LblJumlahKaryawan.Text = "50 < 100"
            ElseIf oPar.JumlahKaryawan = "C" Then
                LblJumlahKaryawan.Text = "> 100"
            End If

            If oPar.LokasiTempatUsaha = "U" Then
                LblLokasiTempatUsaha.Text = "JALAN UTAMA"
            ElseIf oPar.LokasiTempatUsaha = "B" Then
                LblLokasiTempatUsaha.Text = "JALAN BIASA"
            ElseIf oPar.LokasiTempatUsaha = "G" Then
                LblLokasiTempatUsaha.Text = "GANG"
            End If

            LblAlasanLokasiTempatUsaha.Text = .Alasan
            If oPar.AktivitasTempatUsaha = "Y" Then
                LblAktivitasTmptUsaha.Text = "Ada"
            ElseIf oPar.AktivitasTempatUsaha = "T" Then
                LblAktivitasTmptUsaha.Text = "Tidak"
            End If

            LblAlasanAktivitasTempatUsaha.Text = .AlasanAktivitasTempatUsaha
            LblTBOList.Text = .TBOList
            LblUsulanLain.Text = .UsulanLain

            'LblPihakYangTurutSurvey.Text = oPar.PihakYangTurutSurvey
            LblAlasanPihakYangTurutSurvey.Text = .AlasanPihakYangTurutSurvey
            LblAlasanLokasiYangDisurvey.Text = .AlasanLokasiSurvey
            If oPar.LokasiSurvey = "R" Then
                LblLokasiyangDisurvey.Text = "Rumah"
            ElseIf oPar.LokasiSurvey = "U" Then
                LblLokasiyangDisurvey.Text = "Lokasi Usaha"
            ElseIf oPar.LokasiSurvey = "L" Then
                LblLokasiyangDisurvey.Text = "Lainnya"
            End If

            If oPar.LokasiSurveySesuaiKTP = "Y" Then
                LblSurveySesuaiKTP.Text = "Ya"
            ElseIf oPar.LokasiSurveySesuaiKTP = "T" Then
                LblSurveySesuaiKTP.Text = "Tidak"
            End If

            LblAlasanSurveySesuaiKTP.Text = .AlasanLokasiSurveySesuaiKTP
            LblWaktuSurvey1.Text = .WaktuSurvey1.ToString("dd/MM/yyyy")
            LblWaktuSurvey2.Text = .WaktuSurvey2.ToString("dd/MM/yyyy")

            If oPar.HasilCekLkgAspekSosial = "P" Then
                LblHasilCekLkgAspekSosial.Text = "POPULAR"
            ElseIf oPar.HasilCekLkgAspekSosial = "S" Then
                LblHasilCekLkgAspekSosial.Text = "SEDANG"
            ElseIf oPar.HasilCekLkgAspekSosial = "T" Then
                LblHasilCekLkgAspekSosial.Text = "TIDAK DIKENAL"
            ElseIf oPar.HasilCekLkgAspekSosial = "B" Then
                LblHasilCekLkgAspekSosial.Text = "BLACKLIST"
            End If

            If oPar.PengalamanKredit = "Y" Then
                LblPengalamanKredit.Text = "Ada"
            ElseIf oPar.PengalamanKredit = "T" Then
                LblPengalamanKredit.Text = "Tidak Ada"
            End If

            LblPembayaran.Text = .Pembayaran
            If oPar.BuktiBayar = "Y" Then
                LblBuktiBayar.Text = "Ada"
            ElseIf oPar.BuktiBayar = "T" Then
                LblBuktiBayar.Text = "Tidak Ada"
            End If

            LblPekerjaanSekarang.Text = .PekerjaanSekarang
            LblLamaPekerjaanSekarang.Text = .LamanyaPekerjaanSekarang
            LblPekerjaanSebelumnya.Text = .PekerjaanSebelumnya
            LblLamaPekerjaanSebelumnya.Text = .LamanyaPekerjaanSebelumnya
            'LblPEducation.Text = oPar.Pendidikan
            LblNamaYangMenemaniSurvey1.Text = .NamaYangMenemaniSurvey1
            LblHubungan1.Text = .Hubungan1
            LblNamaYangMenemaniSurvey2.Text = .NamaYangMenemaniSurvey2
            LblHubungan2.Text = .Hubungan2
            If oPar.HubunganSalesDgnPemohon = "B" Then
                LblHubSalesdgnPemohon.Text = "KONSUMEN BARU"
            ElseIf oPar.HubunganSalesDgnPemohon = "R" Then
                LblHubSalesdgnPemohon.Text = "KONSUMEN R/O"
            ElseIf oPar.HubunganSalesDgnPemohon = "K" Then
                LblHubSalesdgnPemohon.Text = "KELUARGA"
            ElseIf oPar.HubunganSalesDgnPemohon = "T" Then
                LblHubSalesdgnPemohon.Text = "TEMAN"
            End If

            LblKesimpulanCharacter.Text = .AnalisaCharacter

            LblKendaraanYangDiajukan.Text = .KendaraanYangDiajukan
            LblAlasanPembelian.Text = .AlasanPembelian
            LblLokasiPenggunaan.Text = .LokasiPenggunaan
            If oPar.PencocokanSTNKdenganFisik = "Y" Then
                LblPencocokanSTNKdgnFisik.Text = "Sudah"
            ElseIf oPar.PencocokanSTNKdenganFisik = "T" Then
                LblPencocokanSTNKdgnFisik.Text = "Belum"
            End If

            If oPar.BPKBDiKeluarkanOleh = "Y" Then
                LblBPKPDikeluarkanOleh.Text = "Authorized Dealer"
            ElseIf oPar.BPKBDiKeluarkanOleh = "T" Then
                LblBPKPDikeluarkanOleh.Text = "Non Authorized Dealer"
            End If

            If oPar.FotoRumah = "Y" Then
                LblFotoRumahCollateral.Text = "Ada"
            ElseIf oPar.FotoRumah = "T" Then
                LblFotoRumahCollateral.Text = "Tidak"
            End If

            LblJumlahAssetLain.Text = .JumlahAssetLain
            LblJenisAssetLain.Text = .JenisAssetLain
            LblStatusAssetLainLunas.Text = .StatusAssetLainLunas
            LblStatusAssetLainTidakLunas.Text = .StatusAssetLainTidakLunas
            LblBPKBNo.Text = .BPKBNo
            LblNamaBPKB.Text = .NamaBPKB
            If oPar.BPKBDiperlihatkan = "Y" Then
                LblBPKBDiperlihatkan.Text = "Ada"
            ElseIf oPar.BPKBDiperlihatkan = "T" Then
                LblBPKBDiperlihatkan.Text = "Tidak"
            End If

            LblAlasanBPKBDiperlihatkan.Text = .AlasanBPKBDiperlihatkan
            LblKesimpulanCollateral.Text = .AnalisaCollateral

            LblTotalPenghasilan1.Text = FormatNumber(CStr(.TotalPenghasilan1), 0)
            LblTotalPenghasilan2.Text = FormatNumber(CStr(.TotalPenghasilan2), 0)
            LblBiayaUsaha1.Text = FormatNumber(CStr(.BiayaUsaha1), 0)
            LblBiayaUsaha2.Text = FormatNumber(CStr(.BiayaUsaha2), 0)
            LblBiayaHidup1.Text = FormatNumber(CStr(.BiayaHidup1), 0)
            LblBiayaHidup2.Text = FormatNumber(CStr(.BiayaHidup2), 0)
            LblBiayaCicilan1.Text = FormatNumber(CStr(.BiayaCicilan1), 0)
            LblBiayaCicilan2.Text = FormatNumber(CStr(.BiayaCicilan2), 0)
            LblSisaSebelumTambahUnit1.Text = FormatNumber(CStr(.SisaSebelumTambahUnit1), 0)
            LblSisaSebelumTambahUnit2.Text = FormatNumber(CStr(.SisaSebelumTambahUnit2), 0)
            LblIncomeDariUnit1.Text = FormatNumber(CStr(.IncomeDariUnit1), 0)
            LblIncomeDariUnit2.Text = FormatNumber(CStr(.IncomeDariUnit2), 0)
            LblEstimasiAngsuran1.Text = FormatNumber(CStr(.EstimasiAngsuran1), 0)
            LblEstimasiAngsuran2.Text = FormatNumber(CStr(.EstimasiAngsuran2), 0)
            LblSisaPendapatan1.Text = FormatNumber(CStr(.SisaPendapatan1), 0)
            LblSisaPendapatan2.Text = FormatNumber(CStr(.SisaPendapatan2), 0)
            If oPar.RekeningTabunganGio = "Y" Then
                LblRekeningTabunganGiro.Text = "Ada"
            ElseIf oPar.RekeningTabunganGio = "T" Then
                LblRekeningTabunganGiro.Text = "Tidak"
            End If

            LblNamaBank.Text = .NamaBank
            If oPar.LaporanKeuangan = "Y" Then
                LblLaporanKeuangan.Text = "Ada"
            ElseIf oPar.LaporanKeuangan = "T" Then
                LblLaporanKeuangan.Text = "Tidak"
            End If

            LblAlasanLaporanKeuangan.Text = .AlasanLaporanKeuangan
            If oPar.KreditdariBankLKNB = "Y" Then
                LblKreditdariBankLNKB.Text = "Ada"
            ElseIf oPar.KreditdariBankLKNB = "T" Then
                LblKreditdariBankLNKB.Text = "Tidak"
            End If

            LblNamaInstitusi1.Text = .NamaInstitusi1
            LblAngsuran1.Text = FormatNumber(CStr(.Angsuran1), 0)
            LblSisa1.Text = FormatNumber(CStr(.Sisa1), 0)
            LblNamaInstitusi2.Text = .NamaInstitusi2
            LblAngsuran2.Text = FormatNumber(CStr(.Angsuran2), 0)
            LblSisa2.Text = FormatNumber(CStr(.Sisa2), 0)
            If oPar.BuktiPembayaranAngsuran = "Y" Then
                LblBuktiPembayaranAngsuran.Text = "Ada"
            ElseIf oPar.BuktiPembayaranAngsuran = "T" Then
                LblBuktiPembayaranAngsuran.Text = "Tidak"
            End If

            LblAlasanBuktiPembayaranAng.Text = .AlasanBuktiPembayaranAngsuran
            LblKesimpulanCondition.Text = .AnalisaCondition

            LblKesimpulanKYC.Text = .AnalisaKYC
        End With
        cekimage()
        BindgridCekLingkungan()
        getDataHomeStatus(oPar.StatusKepemilikan)
        getDataPHYS(oPar.PihakYangTurutSurvey)
        getDataEducation(oPar.Pendidikan)
        getDataSurveyor(oPar.SurveyorID)
        getDataCA(oPar.CAID)
    End Sub

    Private Function pathFile(ByVal Group As String, ByVal ExecutorID As String) As String
        Dim strDirectory As String = ""
        'strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        strDirectory = getpathFolder() & ucViewApplication1.BranchID.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Function getpathFolder() As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function
    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function

    Private Sub cekimage()
        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah1.jpg") Then
            'imgdenahlokasirumah1.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah1.jpg"))
            imgdenahlokasirumah1.ImageUrl = ResolveClientUrl(getDownloadPath(ucViewApplication1.BranchID.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah1.jpg"))
        Else
            imgdenahlokasirumah1.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah2.jpg") Then
            imgdenahlokasirumah2.ImageUrl = ResolveClientUrl(getDownloadPath(ucViewApplication1.BranchID.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah2.jpg"))
        Else
            imgdenahlokasirumah2.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah3.jpg") Then
            imgdenahlokasirumah3.ImageUrl = ResolveClientUrl(getDownloadPath(ucViewApplication1.BranchID.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah3.jpg"))
        Else
            imgdenahlokasirumah3.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah4.jpg") Then
            imgdenahlokasirumah4.ImageUrl = ResolveClientUrl(getDownloadPath(ucViewApplication1.BranchID.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah4.jpg"))
        Else
            imgdenahlokasirumah4.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah5.jpg") Then
            imgdenahlokasirumah5.ImageUrl = ResolveClientUrl(getDownloadPath(ucViewApplication1.BranchID.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah5.jpg"))
        Else
            imgdenahlokasirumah5.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        imgdenahlokasirumah1.Height = 200
        imgdenahlokasirumah1.Width = 300
        imgdenahlokasirumah2.Height = 200
        imgdenahlokasirumah2.Width = 300
        imgdenahlokasirumah3.Height = 200
        imgdenahlokasirumah3.Width = 300
        imgdenahlokasirumah4.Height = 200
        imgdenahlokasirumah4.Width = 300
        imgdenahlokasirumah5.Height = 200
        imgdenahlokasirumah5.Width = 300

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID.Trim) + "ProsedurKYC.pdf") Then
            'hypProsedurKYC.NavigateUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/ProsedurKYC.pdf"))
            hypProsedurKYC.NavigateUrl = ResolveClientUrl(getDownloadPath(ucViewApplication1.BranchID.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/ProsedurKYC.pdf"))
            hypProsedurKYC.Visible = True
            hypProsedurKYC.Text = "ProsedurKYC.pdf"
            hypProsedurKYC.Target = "_blank"
        Else
            hypProsedurKYC.Visible = False
        End If

    End Sub

    Sub BindgridCekLingkungan()
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)

        'Dim adapter As New SqlDataAdapter
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spHasilSurvey_002CharacterCLView"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim

            objReader = objCommand.ExecuteReader
            'adapter.SelectCommand = objCommand
            'adapter.Fill(dtGrid)

            dtg.DataSource = objReader
            dtg.DataBind()
            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Protected Sub lblCode_DataBinding(sender As Object, e As System.EventArgs)
        Dim lbl As Label = DirectCast(sender, Label)
        Dim code As String = Eval("CLDomisili1")
        Select Case code
            Case "B"
                lbl.Text = "BENAR"
                Exit Select
            Case "S"
                lbl.Text = "SALAH"
                Exit Select
        End Select
    End Sub

    Private Sub getDataHomeStatus(ByVal Code As String)
        Dim dt As New DataTable()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim adapter As New SqlDataAdapter

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandText = "SELECT * FROM dbo.TblHomeStatus WHERE ID = @Id"
            objCommand.Connection = objConnection
            adapter.SelectCommand = objCommand
            objCommand.Parameters.AddWithValue("@Id", Code)
            adapter.Fill(dt)
            If dt.Rows.Count > 0 Then
                LblStatusKepemilikan.Text = dt.Rows(0)("Description").ToString
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Private Sub getDataPHYS(ByVal Code As String)
        Dim dt As New DataTable()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim adapter As New SqlDataAdapter

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandText = "SELECT * FROM dbo.TblEmployeePosition WHERE EmployeePositionID = @Id"
            objCommand.Connection = objConnection
            adapter.SelectCommand = objCommand
            objCommand.Parameters.AddWithValue("@Id", Code)
            adapter.Fill(dt)
            If dt.Rows.Count > 0 Then
                LblPihakYangTurutSurvey.Text = dt.Rows(0)("EmployeePosition").ToString
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Private Sub getDataEducation(ByVal Code As String)
        Dim dt As New DataTable()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim adapter As New SqlDataAdapter

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandText = "SELECT * FROM dbo.TblEducation WHERE ID = @Id"
            objCommand.Connection = objConnection
            adapter.SelectCommand = objCommand
            objCommand.Parameters.AddWithValue("@Id", Code)
            adapter.Fill(dt)
            If dt.Rows.Count > 0 Then
                LblPEducation.Text = dt.Rows(0)("Description").ToString
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Private Sub getDataSurveyor(ByVal Code As String)
        Dim dt As New DataTable()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim adapter As New SqlDataAdapter

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandText = "SELECT * FROM dbo.BranchEmployee WHERE EmployeeID = @Id"
            objCommand.Connection = objConnection
            adapter.SelectCommand = objCommand
            objCommand.Parameters.AddWithValue("@Id", Code)
            adapter.Fill(dt)
            If dt.Rows.Count > 0 Then
                LblSurveyor.Text = dt.Rows(0)("EmployeeName").ToString
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Private Sub getDataCA(ByVal Code As String)
        Dim dt As New DataTable()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim adapter As New SqlDataAdapter

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandText = "SELECT * FROM dbo.BranchEmployee WHERE EmployeeID = @Id"
            objCommand.Connection = objConnection
            adapter.SelectCommand = objCommand
            objCommand.Parameters.AddWithValue("@Id", Code)
            adapter.Fill(dt)
            If dt.Rows.Count > 0 Then
                LblCreditAnalyst.Text = dt.Rows(0)("EmployeeName").ToString
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub
End Class