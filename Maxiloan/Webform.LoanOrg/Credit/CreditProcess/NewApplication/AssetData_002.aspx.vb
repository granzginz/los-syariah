﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class AssetData_002
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail

    ' Protected WithEvents ucAO1 As ucCMO
    'Protected WithEvents ucAO1 As ucAO
    Protected WithEvents UCAddress As ucAddressCity
    'Protected WithEvents UCAddress As UcCompanyAddress
    Protected WithEvents ucApplicationTab1 As ucApplicationTab
    Protected WithEvents ucOTR As ucNumberFormat
    Protected WithEvents ucHargakaroseri As ucNumberFormat
#End Region

#Region "Constanta"
    Dim style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim status As Boolean = True
    Dim pathFolder As String = ""

    Private m_controller As New AssetDataController
    Private oApplication As New Parameter.Application
    Private m_ControllerApp As New ApplicationController
    Private oAssetMasterPriceController As New AssetMasterPriceController
    Private oApplicationController As New ApplicationController
    Private m_Doc As New DocReceiveController
    Private time As String
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property AssetDesc() As String
        Get
            Return ViewState("AssetDesc").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AssetDesc") = Value
        End Set
    End Property
    Property AssetCode() As String
        Get
            Return ViewState("AssetCode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AssetCode") = Value
        End Set
    End Property
    Property DP() As Double
        Get
            Return CType(ViewState("DP"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("DP") = Value
        End Set
    End Property
    Property OTR() As Double
        Get
            Return CType(ViewState("OTR"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("OTR") = Value
        End Set
    End Property
    Property AOID() As String
        Get
            Return ViewState("AOID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AOID") = Value
        End Set
    End Property
    Property NU() As String
        Get
            Return ViewState("NU").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NU") = Value
        End Set
    End Property
    Property App() As String
        Get
            Return ViewState("App").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("App") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property SupplierKaroseriID() As String
        Get
            Return ViewState("SupplierKaroseriID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierKaroseriID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property Asset() As String
        Get
            Return ViewState("Asset").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Asset") = Value
        End Set
    End Property
    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property ProductID() As String
        Get
            Return ViewState("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property

    Property SalesID() As String
        Get
            Return ViewState("SalesID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SalesID") = Value
        End Set
    End Property
    Property SupervisorID() As String
        Get
            Return ViewState("SupervisorID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupervisorID") = Value
        End Set
    End Property
    Property AdminID() As String
        Get
            Return ViewState("AdminID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AdminID") = Value
        End Set
    End Property
    Property CustomerType() As String
        Get
            Return ViewState("CustomerType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerType") = Value
        End Set
    End Property
    Property Origination() As String
        Get
            Return ViewState("Origination").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Origination") = Value
        End Set
    End Property
    'Property DownPayment() As Integer
    '    Get
    '        Return CType(ViewState("DownPayment"), Integer)
    '    End Get
    '    Set(value As Integer)
    '        ViewState("DownPayment") = value
    '    End Set
    'End Property
    Property DownPayment() As Double
        Get
            Return CType(ViewState("DownPayment"), Double)
        End Get
        Set(value As Double)
            ViewState("DownPayment") = value
        End Set
    End Property
    Property DPKaroseriAmount() As Double
        Get
            Return CType(ViewState("DPKaroseriAmount"), Double)
        End Get
        Set(value As Double)
            ViewState("DPKaroseriAmount") = value
        End Set
    End Property
    Property isKaroseri() As Boolean
        Get
            Return CType(ViewState("isKaroseri"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState("isKaroseri") = value
        End Set
    End Property

    Property SupplierName() As String
        Get
            Return ViewState("SupplierName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierName") = Value
        End Set
    End Property
    Public Property AssetUsedNew() As String
        Get
            Return ViewState("AssetUsedNew").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AssetUsedNew") = Value
        End Set
    End Property
    Public Property AssetUsage() As String
        Get
            Return ViewState("AssetUsage").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AssetUsage") = Value
        End Set
    End Property
    Public Property ManufacturingYear() As String
        Get
            Return ViewState("ManufacturingYear").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ManufacturingYear") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("appid")
            hdfApplicationID.Value = Me.ApplicationID
            InitObjects()

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If

                .initControls("OnNewApplication")

                Me.CustomerID = .CustomerID
                Me.CustName = .CustomerName
                Me.Asset = .AssetTypeID
                Me.ProductID = .ProductID
                Me.ProductOfferingID = .ProductOfferingID
                Me.SupplierID = .SupplierID
                Me.CustomerType = .CustomerType
                Me.Origination = .Origination
                Me.AssetUsedNew = .KondisiAsset
                Me.AssetUsage = .AssetUsage
                Me.ManufacturingYear = .ManufacturingYear
                Me.OTR = .OTR

                If .DownPayment > 0 Then
                    Me.DownPayment = .DownPayment
                    Me.DPKaroseriAmount = .DPKaroseriAmount
                Else
                    txtDPPersen.Text = CStr(.DPPercentage)
                    txtDPPersenKaroseri.Text = CStr(.DPKaroseriPercentage)
                End If
                Me.SupplierName = .SupplierName
            End With

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            hdfKaroseriApplicationID.Value = Me.ApplicationID


            'ucLookupAsset1.ApplicationId = Me.ApplicationID
            hdfAssetTypeID.Value = Me.Asset
            ' ucAO1.BindData()


            FillCbo(cboInsuredBy, "tblInsuredBy")
            FillCbo(cboPaidBy, "tblPaidBy")
            'Option "at Cost" (AC)  sudah tidak dipakai
            FillCbo(cboUsage, "tblAssetUsage")
            FillCboGrade()


            'Dim WhereSup As String = "SupplierID = '" & Me.SupplierID & "'"

            'FillCboEmp("SupplierEmployee", cboSalesman, WhereSup)
            'FillCboEmp("SupplierEmployee", cboSalesSpv, WhereSup)
            'FillCboEmp("SupplierEmployee", cboSupplierAdm, WhereSup)

            Me.Mode = Request("Page")
            GetDefaultAssetData()


            BindAttributeEdit()
            BindAssetRegistration()
            'If Not Request("Page") = "Edit" Then
            '    Me.NU = Me.AssetUsedNew
            '    BindAttribute()
            'End If

            'Me.NU = Me.AssetUsedNew
            'BindAttribute()
            'txtName.Text = Me.CustName
            HiddenFieldCustName.Value = Me.CustName
            txtYear.Text = Me.ManufacturingYear
            ucOTR.Text = FormatNumber(Me.OTR, 0)
            txtSupplierCode.Text = Me.SupplierID
            cboPaidBy.Visible = True
            cboInsuredBy.Attributes.Add("OnChange", "return fInsured();")

            With UCAddress
                .TeleponFalse()
                .ValidatorTrue2()
            End With
            rboNamaBPKBSamaKontrak.Attributes.Add("onclick", "rboNamaBPKBSamaKontrakChange();")
            'If Request("Page") = "Edit" Then
            '    'GetDefaultAssetData()
            '    'BindAssetEdit()
            '    'BindAttributeEdit()
            '    'BindAssetRegistration()
            'Else
            '    'Modify by Wira 20171124
            '    txtSupplierCode.Text = Me.SupplierID
            '    'txtSupplierName.Text = Me.SupplierName
            '    'End Modify----

            '    txtKaroseriSupplierCode.Text = ""
            '    txtKaroseriSupplierName.Text = ""

            '    With UCAddress
            '        .TeleponFalse()
            '        .BindAddress()
            '        .ValidatorTrue2()
            '    End With

            '    'cboInsuredBy.SelectedValue = "CO"
            '    'If ucViewApplication1.PaketProgram = "RGL" Then
            '    '    cboPaidBy.SelectedValue = "CU"
            '    'Else
            '    '    cboPaidBy.SelectedValue = "OC"
            '    'End If

            '    Me.NU = Me.AssetUsedNew
            '    BindAttribute()
            '    txtName.Text = Me.CustName
            '    txtYear.Text = Me.ManufacturingYear
            '    ucOTR.Text = FormatNumber(Me.OTR, 0)
            '    'cboUsage.SelectedIndex = cboUsage.Items.IndexOf(cboUsage.Items.FindByValue(Me.AssetUsage.ToString.Trim))
            '    cboPaidBy.Visible = True
            '    cboInsuredBy.Attributes.Add("OnChange", "return fInsured();")
            '    'rboNamaBPKBSamaKontrak.Attributes.Add("onclick", "rboNamaBPKBSamaKontrakChange();")

            'End If

            GetSerial()
            GetFee()
            TanggalPajakSTNK()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Asset")
            ucApplicationTab1.setLink()

            ucOTR.RangeValidatorEnable = True
            ucOTR.RangeValidatorMinimumValue = "1"

            cboKondisiAsset.SelectedValue = Me.NU.Trim

            If ucViewApplication1.isProceed = True Then
                btnNext.Visible = False
            End If
            If cboKondisiAsset.SelectedValue = "U" Then
                divregstnk.Visible = True
                cboAssetGrade.Enabled = True
            Else
                divregstnk.Visible = False
                cboAssetGrade.Enabled = False
            End If
        End If
        TotalNetto()
    End Sub
#End Region

#Region "Initial Objects"
    Private Sub InitObjects()
        lblMessage.Text = ""
        lblMessage.Visible = False
        RangeValidator3.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
        'ucSupplier1.Visible = True
        'lblErrAccOff.Text = ""
        'txtSupplier.Enabled = False
        'txtSupplierKaroseri.Enabled = False
        'txtAsset.Enabled = False        

        'txtAO.Enabled = False
        'lblPF.Visible = False
        'lblSupplierRegular.Visible = False
        rboKaroseri.SelectedValue = "False"
        ucOTR.AutoPostBack = True
        ucHargakaroseri.AutoPostBack = True
        pnlLookupKaroseri.Visible = False
        pnlNext2.Visible = False
        pnlOTR.Visible = False
        pnlLast.Visible = False
        rboIjinTrayek.Enabled = False
        pnlInformasiTrayek.Visible = False
        rboNamaBPKBSamaKontrak.SelectedValue = True
    End Sub

    Sub InitialLabelValid()
        Dim count As Integer
        lblVPaidBy.Visible = False
        count = CInt(IIf(dtgAssetDoc.Items.Count > dtgAttribute.Items.Count, dtgAssetDoc.Items.Count, dtgAttribute.Items.Count))
        For Me.intLoop = 0 To count - 1
            If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label).Visible = False
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label).Visible = False
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber2"), Label).Visible = False
            End If
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label).Visible = False
            End If
        Next
    End Sub

    Sub TanggalPajakSTNK()
        Dim oProduct As New Parameter.Product
        Dim m_offering As New ProductController

        oProduct.ProductId = Me.ProductID
        oProduct.BranchId = Me.sesBranchId.Replace("'", "")
        oProduct.ProductOfferingID = Me.ProductOfferingID
        oProduct.strConnection = GetConnectionString()

        oProduct = m_offering.ProductOfferingView(oProduct)


        If oProduct.AssetUsedNew = "N" Then
            lblTanggalSTNK.Attributes("class") = ""
            rfvTanggalSTNK.Enabled = False
        Else
            lblTanggalSTNK.Attributes("class") = "label_req"
            rfvTanggalSTNK.Enabled = True
        End If

    End Sub
#End Region

#Region "FillCbo"
    Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = m_controller.GetCboEmp(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
    Sub FillCboGrade()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData = m_controller.GetGradeAsset(oAssetData)
        oData = oAssetData.ListData
        cboAssetGrade.DataSource = oData
        cboAssetGrade.DataTextField = "GradeCode"
        cboAssetGrade.DataValueField = "GradeValue"
        cboAssetGrade.DataBind()
    End Sub
#End Region

#Region "BindAsset, BindAttribute, GetAO, & GetSerial"
    Sub BindAsset(ByVal WhereCond As String)
        Dim oData As New DataTable       
        Dim oAssetData As New Parameter.AssetData

        oAssetData.strConnection = GetConnectionString()
        oAssetData.AssetID = Me.Asset
        oAssetData.CustomerType = Me.CustomerType
        oAssetData.Origination = Me.Origination
        oAssetData.WhereCond = WhereCond
        oAssetData = m_controller.GetAssetDocWhere(oAssetData)
        oData = oAssetData.ListData

        dtgAssetDoc.DataSource = oData
        dtgAssetDoc.DataBind()
    End Sub

    Sub BindAttribute()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.AssetID = Me.Asset
        oAssetData = m_controller.GetAttribute(oAssetData)
        oData = oAssetData.ListData
        dtgAttribute.DataSource = oData
        dtgAttribute.DataBind()
    End Sub

    Sub GetSerial()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oData2 As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.AssetID = Me.Asset
        oAssetData = m_controller.GetSerial(oAssetData)
        oData = oAssetData.ListData

        lblSerial1.Text = oData.Rows(0).Item(0).ToString
        lblSerial2.Text = oData.Rows(0).Item(1).ToString

        'oAssetData.strConnection = GetConnectionString()
        'oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
        'oAssetData.AppID = Me.ApplicationID
        'oAssetData = m_controller.GetUsedNew(oAssetData)
        'oData2 = oAssetData.ListData

        'Me.NU = oData2.Rows(0).Item(0).ToString

        If Me.NU.ToUpper.Trim = "U" Then
            RFVSerial1.Enabled = True
            RFVSerial1.ErrorMessage = "Harap isi " & oData.Rows(0).Item(0).ToString & "!"
            RFVSerial2.Enabled = True
            RFVSerial2.ErrorMessage = "Harap isi " & oData.Rows(0).Item(1).ToString & "!"
            rfvTanggalSTNK.Enabled = True
            lblTanggalSTNK.Attributes("class") = "label_req"
            lblSerial1.Attributes("class") = "label label_req"
            lblSerial2.Attributes("class") = "label label_req"
        Else
            'RFVSerial1.Enabled = False
            'RFVSerial2.Enabled = False
            'rfvTanggalSTNK.Enabled = False
            'lblTanggalSTNK.Attributes.Remove("class")
            'lblSerial1.Attributes("class") = "label"
            'lblSerial2.Attributes("class") = "label"

            RFVSerial1.Enabled = True
            RFVSerial1.ErrorMessage = "Harap isi " & oData.Rows(0).Item(0).ToString & "!"
            RFVSerial2.Enabled = True
            RFVSerial2.ErrorMessage = "Harap isi " & oData.Rows(0).Item(1).ToString & "!"
            rfvTanggalSTNK.Enabled = False
            lblTanggalSTNK.Attributes.Remove("class")
            lblSerial1.Attributes("class") = "label label_req"
            lblSerial2.Attributes("class") = "label label_req"
        End If
    End Sub

    'Sub GetAO()
    '    Dim oAssetData As New Parameter.AssetData
    '    Dim oData As New DataTable

    '    oAssetData.strConnection = GetConnectionString()
    '    oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
    '    oAssetData.SupplierID = Me.SupplierID
    '    oAssetData = m_controller.GetAO(oAssetData)
    '    oData = oAssetData.ListData
    '    If oData.Rows.Count > 0 Then

    '        ucAO1.LookupName = oData.Rows(0).Item(0).ToString.Trim
    '        ucAO1.LookupID = oData.Rows(0).Item(1).ToString.Trim
    '        'txtAO.Text = oData.Rows(0).Item(0).ToString.Trim
    '        'Me.AOID = oData.Rows(0).Item(1).ToString.Trim


    '    End If
    'End Sub
#End Region

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect("AssetData.aspx")
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub

#Region "ItemDataBound"
    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        Dim chkFU As New CheckBox
        Dim ddlNotes As New DropDownList
        Dim txtNotes As New TextBox
        Dim lblNotes As New Label
        Dim lblIsNoRequired As New Label
        Dim txtNumber As New TextBox
        Dim lblVNumber2 As New Label
        Dim RFVNumber As New RequiredFieldValidator
        Dim ucTglDokumen As New ucDateCE
        Dim TglFU As New ucDateCE
        Dim UcTglFU As New TextBox
        'Dim txtTglFU As New TextBox
        Dim lblTglFU As New Label

        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            chkFU = CType(e.Item.FindControl("chkFU"), CheckBox)
            ddlNotes = CType(e.Item.FindControl("ddlNotes"), DropDownList)
            txtNotes = CType(e.Item.FindControl("txtNotes"), TextBox)
            lblNotes = CType(e.Item.FindControl("lblNotes"), Label)
            lblIsNoRequired = CType(e.Item.FindControl("lblIsNoRequired"), Label)
            txtNumber = CType(e.Item.FindControl("txtNumber"), TextBox)
            lblVNumber2 = CType(e.Item.FindControl("lblVNumber2"), Label)
            RFVNumber = CType(e.Item.FindControl("RFVNumber"), RequiredFieldValidator)
            ucTglDokumen = CType(e.Item.FindControl("ucTglDokumen"), ucDateCE)
            TglFU = CType(e.Item.FindControl("UcTglFU"), ucDateCE)
            UcTglFU = CType(TglFU.FindControl("txtDateCE"), TextBox)
            lblTglFU = CType(e.Item.FindControl("lblTglFU"), Label)

            ddlNotes.Attributes.Remove("style")
            txtNotes.Attributes.Remove("style")

            If chkFU.Checked Then
                ddlNotes.SelectedValue = lblNotes.Text.Trim
                UcTglFU.Text = lblTglFU.Text.Trim
                ddlNotes.Attributes.Add("style", "display : inherit")
                txtNotes.Attributes.Add("style", "display : none")
                UcTglFU.Attributes.Add("style", "display : inherit")
            Else
                txtNotes.Text = lblNotes.Text.Trim
                If lblNotes.Text.Trim = "Disusulkan" Then
                    txtNotes.Text = ""
                End If
                If lblNotes.Text.Trim = "Internal Memo" Then
                    txtNotes.Text = ""
                End If
                ddlNotes.Attributes.Add("style", "display : none")
                txtNotes.Attributes.Add("style", "display : inherit")
                UcTglFU.Attributes.Add("style", "display : none")
            End If


            'If CBool(lblIsNoRequired.Text) = True Then
            '    txtNumber.Visible = True
            '    RFVNumber.Visible = True
            '    ucTglDokumen.Visible = True
            'Else
            '    txtNumber.Visible = False                
            '    RFVNumber.Visible = False
            '    lblVNumber2.Visible = False
            '    ucTglDokumen.Visible = False
            'End If


        End If
    End Sub
#End Region

#Region "Validator & save"
    Sub Validator(ByVal oData2 As DataTable, ByVal oData3 As DataTable)
        Dim ErrS1 As Boolean = False
        Dim ErrS2 As Boolean = False

        If cboPaidBy.SelectedValue = "Select One" And cboInsuredBy.SelectedValue <> "CU" Then
            lblVPaidBy.Visible = True
            status = False
        End If
        'If CDec(txtOTR.Text) < CDec(IIf(txtDP.Text.Trim = "", "0", txtDP.Text.Trim)) Then
        '    status = False            
        '    ShowMessage(lblMessage, "Harga OTR Price harus lebih besar dari Uang Muka", True)
        '    Exit Sub
        'End If

        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        'If IsNothing(Mode) Then Mode = ""

        'If (txtSerial1.Text.Trim <> "" Or txtSerial2.Text.Trim <> "") And Mode.ToUpper <> "EDIT" Then
        If (txtSerial1.Text.Trim <> "" Or txtSerial2.Text.Trim <> "") Then
            oAssetData.strConnection = GetConnectionString()
            oAssetData.Serial1 = txtSerial1.Text
            oAssetData.Serial2 = txtSerial2.Text
            oAssetData.AssetID = Me.Asset
            oAssetData.AppID = Me.ApplicationID
            oAssetData = m_controller.CheckSerial(oAssetData)
            oData = oAssetData.ListData

            If oData.Rows.Count <> 0 Then
                oData = oAssetData.ListData
                Dim intLoop As Integer
                For intLoop = 0 To oData.Rows.Count - 1
                    If txtSerial1.Text.Trim <> "" Then
                        If oData.Rows(intLoop).Item(0).ToString.Trim = txtSerial1.Text.Trim Then
                            ErrS1 = True
                        End If
                    End If
                    If txtSerial2.Text.Trim <> "" Then
                        If oData.Rows(intLoop).Item(1).ToString.Trim = txtSerial2.Text.Trim Then
                            ErrS2 = True
                        End If
                    End If
                Next
                If ErrS1 = True And ErrS2 = True Then
                    ShowMessage(lblMessage, "Nomor Chasis dan Nomor Mesin sudah ada!", True)
                    status = False
                Else
                    If ErrS1 = True Then
                        ShowMessage(lblMessage, "Nomor Chasis sudah ada!", True)
                        status = False
                    ElseIf ErrS2 = True Then
                        ShowMessage(lblMessage, "Nomor Mesin sudah ada!", True)
                        status = False
                    End If
                End If
            End If
        End If

        Dim AttributeId As String
        Dim txtAttribute As TextBox
        Dim lblVAttribute, lblVNumber, lblVNumber2 As Label
        Dim count As Integer
        Dim lblvChk As Label
        Dim chk As CheckBox
        Dim MandatoryNew, MandatoryUsed, isValueNeeded As TextBox
        Dim txtnumber As TextBox
        Dim value As String
        Dim ucTglDokumen As ucDateCE
        'Dim UcTglFU As ucDateCE
        Dim UcTglFU As New TextBox
        Dim lblNotes As New Label
        Dim ddlNotes As New DropDownList
        Dim txtNotes As New TextBox
        Dim lblTglFU As New Label
        Dim TglFU As New ucDateCE

        count = CInt(IIf(dtgAttribute.Items.Count > dtgAssetDoc.Items.Count, dtgAttribute.Items.Count, dtgAssetDoc.Items.Count))
        For intLoop = 0 To count - 1
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                AttributeId = dtgAttribute.Items(intLoop).Cells(2).Text.Trim
                txtAttribute = CType(dtgAttribute.Items(intLoop).FindControl("txtAttribute"), TextBox)
                lblVAttribute = CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label)

                If AttributeId = "LICPLATE" And txtAttribute.Text.Trim <> "" Then
                    oAssetData = New Parameter.AssetData
                    oData = New DataTable

                    If Request("Page") = "Edit" Then
                        oAssetData.strConnection = GetConnectionString()
                        oAssetData.Input = txtAttribute.Text
                        oAssetData.AssetID = Me.Asset
                        oAssetData.ApplicationId = Me.ApplicationID
                        oAssetData = m_controller.CheckAttribute(oAssetData)
                        oData = oAssetData.ListData
                    Else
                        oAssetData.strConnection = GetConnectionString()
                        oAssetData.Input = txtAttribute.Text
                        oAssetData.AssetID = Me.Asset
                        oAssetData.ApplicationId = ""
                        oAssetData = m_controller.CheckAttribute(oAssetData)
                        oData = oAssetData.ListData
                    End If

                    If oData.Rows.Count > 0 And txtAttribute.Text = "" Then
                        lblVAttribute.Visible = True
                        If status <> False Then
                            status = False
                        End If
                    Else
                        lblVAttribute.Visible = False
                    End If
                Else
                    lblVAttribute.Visible = False
                End If

                If AttributeId = "COLOR" Then
                    lblVAttribute.Visible = False
                End If

                objrow = oData2.NewRow
                objrow("AttributeID") = AttributeId
                objrow("AttributeContent") = txtAttribute.Text.Trim
                oData2.Rows.Add(objrow)
            End If
            If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                chk = CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox)
                lblvChk = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label)
                lblVNumber = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label)
                lblVNumber2 = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber2"), Label)
                value = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                txtnumber = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox)
                ucTglDokumen = CType(dtgAssetDoc.Items(intLoop).FindControl("ucTglDokumen"), ucDateCE)
                isValueNeeded = CType(dtgAssetDoc.Items(intLoop).FindControl("isValueNeeded"), TextBox)
                MandatoryNew = CType(dtgAssetDoc.Items(intLoop).FindControl("MandatoryForNewAsset"), TextBox)
                MandatoryUsed = CType(dtgAssetDoc.Items(intLoop).FindControl("MandatoryForUsedAsset"), TextBox)
                'UcTglFU = CType(dtgAssetDoc.Items(intLoop).FindControl("UcTglFU"), ucDateCE)
                TglFU = CType(dtgAssetDoc.Items(intLoop).FindControl("UcTglFU"), ucDateCE)
                UcTglFU = CType(TglFU.FindControl("txtDateCE"), TextBox)
                lblNotes = CType(dtgAssetDoc.Items(intLoop).FindControl("lblNotes"), Label)
                ddlNotes = CType(dtgAssetDoc.Items(intLoop).FindControl("ddlNotes"), DropDownList)
                txtNotes = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNotes"), TextBox)
                lblTglFU = CType(dtgAssetDoc.Items(intLoop).FindControl("lblTglFU"), Label)

                If Me.NU = "N" And CBool(MandatoryNew.Text) = True Then
                    If chk.Checked = False Then
                        If CBool(MandatoryNew.Text) = True Then
                            lblvChk.Visible = True
                        Else
                            lblvChk.Visible = False
                        End If
                        If status <> False Then
                            status = False
                        End If
                    Else
                        lblvChk.Visible = False
                        If value = "" And txtnumber.Visible Then
                            lblVNumber2.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        Else
                            lblVNumber2.Visible = False
                        End If
                    End If
                ElseIf Me.NU = "U" And CBool(MandatoryUsed.Text) = True Then
                    If chk.Checked = False Then
                        If CBool(MandatoryUsed.Text) = True Then
                            lblvChk.Visible = True
                        Else
                            lblvChk.Visible = False
                        End If
                        If status <> False Then
                            status = False
                        End If
                    Else
                        lblvChk.Visible = False
                        If value = "" And txtnumber.Visible Then
                            lblVNumber2.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        Else
                            lblVNumber2.Visible = False
                        End If
                    End If
                End If

                If txtnumber.Text.Trim <> "" Then
                    oAssetData = New Parameter.AssetData
                    oData = New DataTable
                    oAssetData.strConnection = GetConnectionString()
                    oAssetData.Input = txtnumber.Text.Trim
                    oAssetData.AssetDocID = CType(dtgAssetDoc.Items(intLoop).FindControl("AssetDocID"), TextBox).Text.Trim
                    oAssetData.ApplicationId = Me.ApplicationID
                    oAssetData.AssetID = Me.Asset
                    oAssetData = m_controller.CheckAssetDoc(oAssetData)
                    oData = oAssetData.ListData
                    'modify Nofi 01082019 karena IDCARD boleh sama
                    'If oData.Rows.Count > 0 Then
                    '    lblVNumber.Visible = True
                    '    If status <> False Then
                    '        status = False
                    '    End If
                    'End If
                End If
                objrow = oData3.NewRow
                objrow("AssetDocID") = CType(dtgAssetDoc.Items(intLoop).FindControl("AssetDocID"), TextBox).Text.Trim
                objrow("DocumentNo") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                objrow("IsMainDoc") = CType(dtgAssetDoc.Items(intLoop).FindControl("IsMainDoc"), TextBox).Text.Trim
                objrow("IsDocExist") = IIf(CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox).Checked = True, "1", "0").ToString
                objrow("IsFollowUp") = IIf(CType(dtgAssetDoc.Items(intLoop).FindControl("chkFU"), CheckBox).Checked = True, "1", "0").ToString


                If CType(dtgAssetDoc.Items(intLoop).FindControl("chkFU"), CheckBox).Checked Then
                    objrow("Notes") = CType(dtgAssetDoc.Items(intLoop).FindControl("ddlNotes"), DropDownList).SelectedValue.Trim
                    objrow("TglJanji") = ConvertDate2(CType(dtgAssetDoc.Items(intLoop).FindControl("UcTglFU"), ucDateCE).Text.Trim)
                    'ddlNotes.SelectedValue = lblNotes.Text.Trim
                    'UcTglFU.Text = lblTglFU.Text.Trim
                    ddlNotes.SelectedValue = objrow("Notes")
                    UcTglFU.Text = ConvertDate2(CType(dtgAssetDoc.Items(intLoop).FindControl("UcTglFU"), ucDateCE).Text.Trim).ToString("dd/MM/yyyy")
                    UcTglFU.Attributes.Add("style", "display : inherit")
                    ddlNotes.Attributes.Add("style", "display : inherit")
                    txtNotes.Attributes.Add("style", "display : none")
                Else
                    objrow("Notes") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNotes"), TextBox).Text.Trim
                    objrow("TglJanji") = CDate("01/01/1900")
                    'txtNotes.Text = txtNotes.Text.Trim
                    txtNotes.Text = objrow("Notes")
                    If txtNotes.Text.Trim = "Disusulkan" Then
                        txtNotes.Text = ""
                    End If
                    If txtNotes.Text.Trim = "Internal Memo" Then
                        txtNotes.Text = ""
                    End If
                    UcTglFU.Attributes.Add("style", "display : none")
                    ddlNotes.Attributes.Add("style", "display : none")
                    txtNotes.Attributes.Add("style", "display : inherit")
                End If
                If CType(dtgAssetDoc.Items(intLoop).FindControl("ucTglDokumen"), ucDateCE).Text.Trim <> "" Then
                    objrow("TglDokument") = ConvertDate2(CType(dtgAssetDoc.Items(intLoop).FindControl("ucTglDokumen"), ucDateCE).Text.Trim)
                Else
                    objrow("TglDokument") = CDate("01/01/1900") 'Me.BusinessDate
                End If

                oData3.Rows.Add(objrow)
            End If
        Next
        If status = False Then
            Exit Sub
        End If
    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblMessage.Visible = False
        'Modify by Wira 20161129, memberi validasi split pembayaran jika karoseri dengan PO beda

        Me.SupplierKaroseriID = txtKaroseriSupplierCode.Text.ToString.Trim

        'If Me.SupplierKaroseriID <> "" And Me.SupplierKaroseriID <> Me.SupplierID Then
        'Modify by Wira 20170907, request MAs Khoirul email tgl 2017-09-07 subject "Split Pembayaran Karoseri"

        If isKaroseri = True Then
            If chkSplitBayar.Checked = False Then
                ShowMessage(lblMessage, "Split Pembayaran harap dicentang terlebih dahulu, karena karoseri !", True)
                Exit Sub
            End If
        End If

        Try
            status = True
            Dim notValidValidators = Page.Validators.Cast(Of IValidator)().Where(Function(v) Not v.IsValid)

            If Page.IsValid Then

                Dim i As Integer
                Dim strName As String
                Dim strAttribute As String


                If Me.NU = "U" Then
                    For i = 0 To (dtgAttribute.Items.Count - 1)
                        strName = CType(dtgAttribute.Items(i).FindControl("lblName"), Label).Text
                        strAttribute = CType(dtgAttribute.Items(i).FindControl("txtAttribute"), TextBox).Text.Trim
                        If strName = "License Plate" Then
                            If strAttribute = "" Then
                                ShowMessage(lblMessage, "Harap isi No. Polisi", True)
                                Exit Sub
                            End If
                        Else
                            If strAttribute = "" Then
                                ShowMessage(lblMessage, "Harap isi Warna", True)
                            End If
                        End If
                    Next
                Else
                    For i = 0 To (dtgAttribute.Items.Count - 1)
                        strName = CType(dtgAttribute.Items(i).FindControl("lblName"), Label).Text
                        strAttribute = CType(dtgAttribute.Items(i).FindControl("txtAttribute"), TextBox).Text.Trim
                        If strName = "WARNA" Then
                            If strAttribute = " " Then
                                ShowMessage(lblMessage, "Harap isi Warna", True)
                            End If
                        End If
                    Next
                End If
                'modify nofi 20180320
                'Dim chk As New CheckBox
                'Dim chkFU As New TextBox
                'Dim TglFU As New ucDateCE
                'Dim UcTglFU As New TextBox
                'Dim lblNotes As New Label
                'Dim ddlNotes As New DropDownList
                'Dim txtNotes As New TextBox
                'Dim lblTglFU As New Label

                'For i = 0 To (dtgAssetDoc.Items.Count - 1)
                '    chk = CType(dtgAssetDoc.Items(intLoop).FindControl("chkFU"), CheckBox)
                '    chkFU = CType(chk.FindControl("hdnIndex"), TextBox)
                '    TglFU = CType(dtgAssetDoc.Items(intLoop).FindControl("UcTglFU"), ucDateCE)
                '    UcTglFU = CType(TglFU.FindControl("txtDateCE"), TextBox)
                '    lblNotes = CType(dtgAssetDoc.Items(intLoop).FindControl("lblNotes"), Label)
                '    ddlNotes = CType(dtgAssetDoc.Items(intLoop).FindControl("ddlNotes"), DropDownList)
                '    txtNotes = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNotes"), TextBox)
                '    lblTglFU = CType(dtgAssetDoc.Items(intLoop).FindControl("lblTglFU"), Label)
                '    'If chk.Checked = True Then
                '    If CType(dtgAssetDoc.Items(intLoop).FindControl("chkFU"), CheckBox).Checked Then
                '        ddlNotes.SelectedValue = lblNotes.Text.Trim
                '        UcTglFU.Text = lblTglFU.Text.Trim
                '        UcTglFU.Attributes.Add("style", "display : inherit")
                '        ddlNotes.Attributes.Add("style", "display : inherit")
                '        txtNotes.Attributes.Add("style", "display : none")
                '    Else
                '        txtNotes.Text = txtNotes.Text.Trim
                '        If lblNotes.Text.Trim = "Disusulkan" Then
                '            txtNotes.Text = ""
                '        End If
                '        If lblNotes.Text.Trim = "Internal Memo" Then
                '            txtNotes.Text = ""
                '        End If
                '        UcTglFU.Attributes.Add("style", "display : none")
                '        ddlNotes.Attributes.Add("style", "display : none")
                '        txtNotes.Attributes.Add("style", "display : inherit")
                '    End If
                'Next

                Dim oAssetData As New Parameter.AssetData
                Dim oAddress As New Parameter.Address
                Dim oData1 As New DataTable
                Dim oData2 As New DataTable
                Dim oData3 As New DataTable

                oData1.Columns.Add("AssetLevel", GetType(Integer))
                oData1.Columns.Add("AssetCode", GetType(String))

                oData2.Columns.Add("AttributeID", GetType(String))
                oData2.Columns.Add("AttributeContent", GetType(String))

                oData3.Columns.Add("AssetDocID", GetType(String))
                oData3.Columns.Add("DocumentNo", GetType(String))
                oData3.Columns.Add("IsMainDoc", GetType(String))
                oData3.Columns.Add("IsDocExist", GetType(String))
                oData3.Columns.Add("IsFollowUp", GetType(String))
                oData3.Columns.Add("Notes", GetType(String))
                oData3.Columns.Add("TglDokument", GetType(Date))
                oData3.Columns.Add("TglJanji", GetType(Date))

                Validator(oData2, oData3)

                If status = False Then Exit Sub

                'If txtTanggalSTNK.Text <> "" Then
                If txtTanggalSTNK.Text <> "" And cboKondisiAsset.SelectedValue = "U" Then
                    If ConvertDate2(txtTanggalSTNK.Text) <= Me.BusinessDate Then

                        If txtTglSTNKNotes.Text = "" Then
                            ShowMessage(lblMessage, "STNK sudah tidak berlaku, isi notes jika ingin melanjutkan aplikasi.", True)

                            divTglSTNKNotes.Visible = True
                            rvTglSTNKNotes.Enabled = True

                            status = False
                        Else
                            status = True
                        End If
                    Else
                        divTglSTNKNotes.Visible = False
                        rvTglSTNKNotes.Enabled = False
                        status = True
                    End If
                End If

                If status = False Then
                    If cboInsuredBy.SelectedValue = "CU" Then
                        cboPaidBy.ClearSelection()
                        cboPaidBy.Items.FindByValue("CU").Selected = True
                        cboPaidBy.Enabled = False
                    End If
                    Exit Sub
                End If

                Dim AssetCode As String = hdfAssetCode.Value
                'Dim AssetCode As String = Me.AssetCode

                Dim lArrValue As Array = CType(AssetCode.Split(CChar(".")), Array)
                Dim DataCount As Integer = UBound(lArrValue)
                Dim Asset As String = ""

                For Me.intLoop = 0 To DataCount
                    objrow = oData1.NewRow
                    objrow("AssetLevel") = intLoop + 1
                    If intLoop = 0 Then
                        Asset = lArrValue.GetValue(intLoop).ToString
                        objrow("AssetCode") = Asset
                    Else
                        Asset = Asset + "." + lArrValue.GetValue(intLoop).ToString
                        objrow("AssetCode") = Asset
                    End If
                    oData1.Rows.Add(objrow)
                Next
                'Modify by WIra 20171011
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                '----
                With oAssetData
                    .BranchId = Replace(Me.sesBranchId, "'", "")
                    .AppID = Me.ApplicationID
                    .ApplicationId = Me.ApplicationID
                    .SupplierID = Me.SupplierID
                    .OTR = CDec(ucOTR.Text)
                    '.DP = CDec(IIf(txtDP.Text.Trim = "", "0", txtDP.Text))
                    '.DP = CInt(txtDP.Text)
                    .DP = CDbl(txtDP.Text)
                    .AssetID = Me.Asset
                    'mdify nofi 04022019
                    .DPKaroseriAmount = CDbl(txtDPKaroseri.Text)

                    .AssetCode = hdfAssetCode.Value
                    '.AssetCode = Me.AssetCode

                    .Serial1 = txtSerial1.Text
                    .Serial2 = txtSerial2.Text
                    .UsedNew = cboKondisiAsset.SelectedValue
                    .AssetUsage = cboUsage.SelectedValue
                    .ManufacturingYear = CInt(txtYear.Text)
                    .OldOwnerAsset = txtName.Text
                    .OwnerAssetCompany = CBool(rboBPKBanBadanUsaha.SelectedValue)
                    .TaxDate = IIf(txtTanggalSTNK.Text <> "", ConvertDate(txtTanggalSTNK.Text), "").ToString
                    .Notes = txtAssetNotes.Text
                    .InsuredBy = cboInsuredBy.SelectedValue
                    '.PaidBy = IIf(cboInsuredBy.SelectedValue = "CU", "CU", cboPaidBy.SelectedValue).ToString
                    .PaidBy = cboPaidBy.SelectedValue
                    .SalesmanID = cboSalesman.SelectedValue
                    .SalesSupervisorID = cboSalesSpv.SelectedValue
                    .SupplierAdminID = cboSupplierAdm.SelectedValue

                    '.AOID = ucAO1.LookupID
                    '.AOID = Me.AOID

                    .AOID = cbocmo.SelectedValue.ToString

                    '.DateEntryAssetData = Me.BusinessDate
                    'Modify by Wira 20171011
                    .DateEntryAssetData = Me.BusinessDate + " " + time
                    .Pemakai = ""
                    .Lokasi = ""
                    .HargaLaku = 0
                    .SR1 = ""
                    .SR2 = ""
                    .HargaSR1 = 0
                    .HargaSR2 = 0
                    .SplitPembayaran = chkSplitBayar.Checked
                    .UangMukaBayarDi = cboUangMukaBayar.SelectedValue
                    .PencairanKe = cboPencairanKe.SelectedValue

                    .SupplierIDKaroseri = txtKaroseriSupplierCode.Text
                    '.SupplierIDKaroseri = Me.SupplierKaroseriID

                    .HargaKaroseri = CDbl(IIf(IsNumeric(ucHargakaroseri.Text), ucHargakaroseri.Text, 0))
                    .PHJMB = CDbl(IIf(IsNumeric(lblPHJMB.Text), lblPHJMB.Text, 0))
                    '.StatusKendaraan = cboStatusAsset.SelectedValue
                    .StatusKendaraan = "T" 'tersedia
                    .NamaBPKBSama = CBool(rboNamaBPKBSamaKontrak.SelectedValue)
                    .BPKBPengganti = CBool(rboBPKBPengganti.SelectedValue)
                    .IsIzinTrayek = CBool(rboIjinTrayek.SelectedValue)

                    If CBool(rboIjinTrayek.SelectedValue) = False Then
                        txtTrayekAtasNama.Text = ""
                        txtJurusan.Text = ""
                        txtBukuKeur.Text = ""
                    End If
                    .TrayekAtasNama = txtTrayekAtasNama.Text
                    .Jurusan = txtJurusan.Text
                    .BukuKeur = txtBukuKeur.Text

                    .AlasanSTNKExpired = txtTglSTNKNotes.Text.Trim

                    '.Flag = "Add"
                    .Flag = Me.Mode
                    .strConnection = GetConnectionString()
                    .KondisiAsset = cboKondisiAsset.SelectedValue.ToString
                    If cboKondisiAsset.SelectedValue = "U" Then
                        .GradeCode = cboAssetGrade.Text.ToString
                        .GradeValue = CDbl(cboAssetGrade.SelectedValue)
                    Else
                        .GradeCode = ""
                        .GradeValue = 0
                    End If
                End With

                oAddress.Address = UCAddress.Address
                oAddress.RT = UCAddress.RT
                oAddress.RW = UCAddress.RW
                oAddress.Kelurahan = UCAddress.Kelurahan
                oAddress.Kecamatan = UCAddress.Kecamatan
                oAddress.City = UCAddress.City
                oAddress.ZipCode = UCAddress.ZipCode

                Dim oReturn As New Parameter.AssetData

                oReturn = m_controller.AssetDataSaveAdd(oAssetData, oAddress, oData1, oData2, oData3)

                If oReturn.Output <> "" Then
                    ShowMessage(lblMessage, oReturn.Output, True)
                Else
                    With ucViewApplication1
                        .CustomerID = Me.CustomerID
                        .ApplicationID = Me.ApplicationID
                        .bindData()
                        .initControls("OnNewApplication")
                    End With

                    ucApplicationTab1.ApplicationID = Me.ApplicationID
                    ucApplicationTab1.setLink()
                    ShowMessage(lblMessage, "Data saved!", False)
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region

#Region "Copy Address"
    Private Sub btnCopyAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyAddress.Click
        Dim c_customer As New CustomerController
        Dim oCustomer As New Parameter.Customer

        With oCustomer
            .strConnection = GetConnectionString()
            .CustomerID = Me.CustomerID
        End With

        Dim dt As New DataTable
        Dim strCustomerType As String

        strCustomerType = c_customer.GetCustomerType(oCustomer)

        With oCustomer
            .strConnection = GetConnectionString()
            .CustomerID = Me.CustomerID
            .CustomerType = strCustomerType
        End With

        oCustomer = c_customer.GetViewCustomer(oCustomer)
        dt = oCustomer.listdata

        CheckNamaBPKBSama()

        If dt.Rows.Count > 0 Then
            With UCAddress
                .Address = CStr(dt.Rows(0).Item("LegalAddress")).Trim + " " + CStr(dt.Rows(0).Item("LegalRT")).Trim + " " + CStr(dt.Rows(0).Item("LegalRW")).Trim + " " + CStr(dt.Rows(0).Item("LegalKelurahan")).Trim + " " + CStr(dt.Rows(0).Item("LegalKecamatan")).Trim
                '.RT = CStr(dt.Rows(0).Item("LegalRT")).Trim
                '.RW = CStr(dt.Rows(0).Item("LegalRW")).Trim
                '.Kelurahan = CStr(dt.Rows(0).Item("LegalKelurahan")).Trim
                '.Kecamatan = CStr(dt.Rows(0).Item("LegalKecamatan")).Trim
                .City = CStr(dt.Rows(0).Item("LegalCity")).Trim
                .ZipCode = CStr(dt.Rows(0).Item("LegalZipCode")).Trim
                .AreaPhone1 = CStr(dt.Rows(0).Item("LegalAreaPhone1")).Trim
                .Phone1 = CStr(dt.Rows(0).Item("LegalPhone1")).Trim
                .AreaPhone2 = CStr(dt.Rows(0).Item("LegalAreaPhone2")).Trim
                .Phone2 = CStr(dt.Rows(0).Item("LegalPhone2")).Trim
                .AreaFax = CStr(dt.Rows(0).Item("LegalAreaFax")).Trim
                .Fax = CStr(dt.Rows(0).Item("LegalFax")).Trim
            End With
        End If

        UCAddress.BindAddress()
    End Sub

    Protected Sub CheckNamaBPKBSama()
        If rboNamaBPKBSamaKontrak.SelectedValue = "True" Then
            txtName.Text = Me.CustName
        Else
            If txtName.Text = Me.CustName Then txtName.Text = ""
        End If
    End Sub
#End Region

#Region "Lookup"
    'Protected Sub btnLookupSupplier_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupSupplier.Click
    '    ucSupplier1.ApplicationId = Me.ApplicationID
    '    ucSupplier1.Sort = "SupplierId ASC"
    '    ucSupplier1.Where = ""
    '    ucSupplier1.Popup()
    'End Sub

    'Public Sub SupplierSelected(ByVal strSelectedID1 As String, ByVal strSelectedName1 As String, ByVal isPF As Boolean)
    '    Me.SupplierID = strSelectedID1
    '    txtSupplier.Text = strSelectedName1
    '    lblPF.Visible = isPF
    '    lblSupplierRegular.Visible = Not isPF
    '    GetAO()

    '    Dim WhereSup As String = "SupplierID = '" & Me.SupplierID & "'"

    '    FillCboEmp("SupplierEmployee", cboSalesman, WhereSup)
    '    FillCboEmp("SupplierEmployee", cboSalesSpv, WhereSup)
    '    FillCboEmp("SupplierEmployee", cboSupplierAdm, WhereSup)
    'End Sub

    'Protected Sub btnLookupSupplierKaroseri_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupSupplierKaroseri.Click
    '    ucSupplierKaroseri.ApplicationId = Me.ApplicationID
    '    ucSupplierKaroseri.Sort = "SupplierId ASC"
    '    ucSupplierKaroseri.Where = ""
    '    ucSupplierKaroseri.Popup()
    'End Sub

    'Public Sub SupplierKaroseriSelected(ByVal strSelectedID1 As String, ByVal strSelectedName1 As String, ByVal isPF As Boolean)
    '    Me.SupplierKaroseriID = strSelectedID1
    '    txtSupplierKaroseri.Text = strSelectedName1
    'End Sub

    'Protected Sub btnLookupAsset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupAsset.Click
    '    ucLookupAsset1.AssetTypeID = Me.Asset
    '    ucLookupAsset1.ApplicationId = Me.ApplicationID
    '    ucLookupAsset1.CmdWhere = ""
    '    ucLookupAsset1.Popup()
    'End Sub

    'Public Sub CatSelectedAsset(ByVal CatAssetCode As String, ByVal CatDescription As String, ByVal isKaroseri As Boolean)
    '    Me.AssetCode = CatAssetCode
    '    txtAsset.Text = CatDescription
    '    'pnlLookupKaroseri.Visible = isKaroseri
    '    'pnlSplitBayar.Visible = isKaroseri
    '    'pnlHargaKaroseri.Visible = isKaroseri
    '    getPHJMB()
    '    'If Not isKaroseri Then
    '    '    Me.SupplierKaroseriID = ""
    '    '    txtSupplierKaroseri.Text = ""
    '    '    ucHargaKaroseri.Text = "0"
    '    '    chkSplitBayar.Checked = False
    '    'End If
    'End Sub

    'Protected Sub btnLookupAO_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupAO.Click
    '    ucAO1.Popup()
    'End Sub

    'Public Sub AOSelected(ByVal strSelectedID1 As String, ByVal strSelectedName1 As String)
    '    Me.AOID = strSelectedID1
    '    txtAO.Text = strSelectedName1
    'End Sub
#End Region


    Protected Sub txtYear_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtYear.TextChanged
        If Len(txtYear.Text) = 4 Then getPHJMB()
    End Sub

    Protected Sub getPHJMB()
        If txtAssetName.Text <> "" And Me.NU = "U" Then
            'If txtAsset.Text.Trim <> "" And Me.NU = "U" Then

            Dim oCustomClass As New Parameter.AssetMasterPrice

            With oCustomClass
                .strConnection = GetConnectionString()
                .ManufacturingYear = txtYear.Text
                .BranchId = Replace(Me.sesBranchId, "'", "")
                '.AssetCode = Me.AssetCode
                .AssetCode = hdfAssetCode.Value
            End With

            Try
                oCustomClass = oAssetMasterPriceController.GetAssetMasterPriceList(oCustomClass)
                lblPHJMB.Text = FormatNumber(oCustomClass.Price.ToString, 0)

                'If CDbl(ucOTR.Text) < CDbl(IIf(IsNumeric(lblPHJMB.Text), lblPHJMB.Text, 0)) Then
                '    ShowMessage(lblMessage, "Harga jual tidak boleh dibawah harga pasaran!", True)
                'End If
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)

            End Try
        End If
    End Sub

    Private Sub GetFee()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        With oApplication
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
            .ProductOffID = Me.ProductOfferingID
            .ProductID = Me.ProductID
        End With

        oApplication = oApplicationController.GetFee(oApplication)
        oData = oApplication.ListData
        'lblPersenUangMuka.Text = FormatNumber(oData.Rows(0).Item("DPPercentage").ToString, 0) & "%"
    End Sub

#Region "Calculate Pembiayaan"
    Protected Sub txtOTR_TextChanged() Handles ucOTR.TextChanged
        'CalculateTotalPembiayaan()
        calcHargaOTR()
    End Sub

    Sub calcHargaOTR()
        Dim otr, karoseri As Double

        otr = CDbl(IIf(IsNumeric(IIf(ucOTR.Text.Trim = "", "0", ucOTR.Text.Trim).ToString), IIf(ucOTR.Text.Trim = "", "0", ucOTR.Text.Trim).ToString, 0))
        karoseri = CDbl(IIf(IsNumeric(IIf(ucHargakaroseri.Text.Trim = "", "0", ucHargakaroseri.Text.Trim).ToString), IIf(ucHargakaroseri.Text.Trim = "", "0", ucHargakaroseri.Text.Trim).ToString, 0))


        lblTotalHargaOTR.Text = FormatNumber(CDbl(IIf(ucOTR.Text.Trim = "", "0", ucOTR.Text.Trim).ToString) + CDbl(cNum(IIf(ucHargakaroseri.Text.Trim = "", "0", ucHargakaroseri.Text.Trim).ToString)), 0)
    End Sub

    'Public Sub CalculateTotalPembiayaan()
    '    Dim dblOTR As Double
    '    Dim dblHargaKaroseri As Double
    '    Dim dblTotalHargaOTR As Double
    '    Dim dblDP As Double
    '    Dim dblTotalPembiayaan As Double

    '    dblOTR = CDbl(IIf(IsNumeric(txtOTR.Text), txtOTR.Text, 0))
    '    dblHargaKaroseri = CDbl(IIf(IsNumeric(ucHargaKaroseri.Text), ucHargaKaroseri.Text, 0))
    '    dblTotalHargaOTR = dblOTR + dblHargaKaroseri        
    '    lblTotalHargaOTR.Text = FormatNumber(dblTotalHargaOTR, 0)        
    '    dblDP = CDbl(IIf(IsNumeric(txtDP.Text), txtDP.Text, 0))
    '    dblTotalPembiayaan = dblTotalHargaOTR - dblDP
    '    lblTotalPembiayaan.Text = FormatNumber(dblTotalPembiayaan, 0)
    'End Sub

    'Protected Sub txtDP_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtDP.TextChanged
    '    CalculateTotalPembiayaan()
    'End Sub

    'Protected Sub ucHargaKaroseri_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ucHargaKaroseri.TextChanged
    '    CalculateTotalPembiayaan()
    'End Sub
#End Region

    'Protected Sub rboNamaBPKBSamaKontrak_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rboNamaBPKBSamaKontrak.SelectedIndexChanged
    '    CheckNamaBPKBSama()
    'End Sub

    Sub getAssetDataAwal()
        Try
            Dim controllerassetDataAwal As New EditApplicationController
            Dim EntitiesAssetData As New Parameter.Application
            Dim oDataAsset As New DataTable
            With EntitiesAssetData
                .ApplicationID = Me.ApplicationID
                .ProductOffID = Me.ProductOfferingID
                .strConnection = GetConnectionString()
                .SpName = "AssetDataGetInfoAssetDataAwal"
            End With
            EntitiesAssetData = controllerassetDataAwal.AssetDataGetInfoAssetDataAwal(EntitiesAssetData)
            If Not EntitiesAssetData Is Nothing Then
                oDataAsset = EntitiesAssetData.ListData
                If oDataAsset.Rows.Count > 0 Then
                    Me.SupplierID = oDataAsset.Rows(0).Item("SupplierID").ToString
                    txtSupplierCode.Text = oDataAsset.Rows(0).Item("SupplierID").ToString
                    txtSupplierName.Text = oDataAsset.Rows(0).Item("SupplierName").ToString
                    cboUsage.SelectedValue = oDataAsset.Rows(0).Item("AssetUsage").ToString
                    Me.NU = oDataAsset.Rows(0).Item("KondisiAsset").ToString
                    BindAttribute()
                Else
                    Me.NU = "N"
                    BindAttribute()
                    cboUsage.SelectedValue = "N"
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Function rboNamaBPKBSamaKontrakChange() As String
        Return "rboNamaBPKBSamaKontrakChange();"
    End Function


    Sub GetDefaultAssetData()
        Try
            Dim controllerassetData As New EditApplicationController
            Dim EntitiesAssetData As New Parameter.Application
            Dim oDataAsset As New DataTable

            With EntitiesAssetData
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString()
                .SpName = "EditAssetDataGetInfoAssetData"
            End With

            EntitiesAssetData = controllerassetData.EditAssetDataGetInfoAssetData(EntitiesAssetData)

            If Not EntitiesAssetData Is Nothing Then
                oDataAsset = EntitiesAssetData.ListData
                If oDataAsset.Rows.Count > 0 Then
                    Me.SupplierID = oDataAsset.Rows(0).Item("SupplierID").ToString

                    txtSupplierCode.Text = oDataAsset.Rows(0).Item("SupplierID").ToString
                    txtSupplierName.Text = oDataAsset.Rows(0).Item("SupplierName").ToString


                    hdfAssetCode.Value = oDataAsset.Rows(0).Item(1).ToString
                    txtAssetName.Text = oDataAsset.Rows(0).Item(2).ToString
                    Me.AssetCode = hdfAssetCode.Value
                    'Me.AssetCode = oDataAsset.Rows(0).Item(1).ToString
                    'txtAsset.Text = oDataAsset.Rows(0).Item(2).ToString


                    'pnlLookupKaroseri.Visible = CBool(oDataAsset.Rows(0).Item("Karoseri").ToString)

                    txtKaroseriSupplierCode.Text = oDataAsset.Rows(0).Item("SupplierIDKaroseri").ToString
                    txtKaroseriSupplierName.Text = oDataAsset.Rows(0).Item("SupplierNameKaroseri").ToString

                    'Me.SupplierKaroseriID = oDataAsset.Rows(0).Item("SupplierIDKaroseri").ToString
                    'txtSupplierKaroseri.Text = oDataAsset.Rows(0).Item("SupplierNameKaroseri").ToString


                    txtYear.Text = oDataAsset.Rows(0).Item(9).ToString
                    ucOTR.Text = FormatNumber(oDataAsset.Rows(0).Item(3), 0)
                    chkSplitBayar.Checked = CBool(oDataAsset.Rows(0).Item("SplitPembayaran").ToString)
                    ucHargakaroseri.Text = FormatNumber(oDataAsset.Rows(0).Item("HargaKaroseri"), 0)
                    lblTotalHargaOTR.Text = FormatNumber(CDbl(ucOTR.Text) + CDbl(ucHargakaroseri.Text), 0)
                    'txtDP.Text = oDataAsset.Rows(0).Item(4).ToString                
                    rboKaroseri.Items.FindByValue(oDataAsset.Rows(0).Item("isKaroseri").ToString).Selected = True
                    isKaroseri = CBool(oDataAsset.Rows(0).Item("isKaroseri").ToString)
                    'resetPanelKaroseri()

                    cboPencairanKe.SelectedIndex = cboPencairanKe.Items.IndexOf(cboPencairanKe.Items.FindByValue(oDataAsset.Rows(0).Item("PencairanKe").ToString))
                    'cboStatusAsset.SelectedIndex = cboStatusAsset.Items.IndexOf(cboStatusAsset.Items.FindByValue(oDataAsset.Rows(0).Item("StatusKendaraan").ToString))                

                    txtSerial1.Text = oDataAsset.Rows(0).Item(5).ToString
                    txtSerial2.Text = oDataAsset.Rows(0).Item(6).ToString
                    'rboNamaBPKBSamaKontrak.Items.FindByValue(oDataAsset.Rows(0).Item("NamaBPKBSama").ToString).Selected = True
                    rboNamaBPKBSamaKontrak.Items.FindByValue(oDataAsset.Rows(0).Item("NamaBPKBSama").ToString).Selected = True
                    rboBPKBPengganti.Items.FindByValue(oDataAsset.Rows(0).Item("isBPKBPengganti").ToString).Selected = True
                    rboIjinTrayek.Items.FindByValue(oDataAsset.Rows(0).Item("IsIzinTrayek")).Selected = True

                    If oDataAsset.Rows(0).Item(8).ToString = "C" Then
                        rboIjinTrayek.Enabled = True
                    Else
                        rboIjinTrayek.Enabled = False
                    End If
                    txtTrayekAtasNama.Text = oDataAsset.Rows(0).Item("TrayekAtasNama").ToString
                    txtBukuKeur.Text = oDataAsset.Rows(0).Item("BukuKeur").ToString
                    txtJurusan.Text = oDataAsset.Rows(0).Item("Jurusan").ToString

                    'CalculateTotalPembiayaan()
                    Me.NU = oDataAsset.Rows(0).Item("KondisiAsset").ToString

                    cboUsage.SelectedIndex = cboUsage.Items.IndexOf(cboUsage.Items.FindByValue(oDataAsset.Rows(0).Item(8).ToString))
                    cboKondisiAsset.SelectedIndex = cboKondisiAsset.Items.IndexOf(cboKondisiAsset.Items.FindByValue(oDataAsset.Rows(0).Item("KondisiAsset").ToString))


                    If oDataAsset.Rows(0).Item("TaxDate").ToString <> "" Then
                        txtTanggalSTNK.Text = Format(oDataAsset.Rows(0).Item("TaxDate"), "dd/MM/yyyy")
                    Else
                        txtTanggalSTNK.Text = ""
                    End If

                    'If txtTanggalSTNK.Text <> "" Then
                    If txtTanggalSTNK.Text <> "" And cboKondisiAsset.SelectedValue = "U" Then
                        If ConvertDate2(txtTanggalSTNK.Text) <= Me.BusinessDate Then
                            divTglSTNKNotes.Visible = True
                            txtTglSTNKNotes.Text = oDataAsset.Rows(0)("AlasanSTNKExpired").ToString
                        End If
                    End If

                    ' cboUangMukaBayar.SelectedIndex = cboUangMukaBayar.Items.IndexOf(cboUangMukaBayar.Items.FindByValue(oDataAsset.Rows(0).Item("UangMukaBayarDi").ToString))

                    txtAssetNotes.Text = oDataAsset.Rows(0).Item("Notes").ToString

                    cboInsuredBy.SelectedIndex = cboInsuredBy.Items.IndexOf(cboInsuredBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetInsuredBy").ToString))
                    cboPaidBy.SelectedIndex = cboPaidBy.Items.IndexOf(cboPaidBy.Items.FindByValue(oDataAsset.Rows(0).Item("InsAssetPaidBy").ToString))
                    cboAssetGrade.SelectedIndex = cboAssetGrade.Items.IndexOf(cboAssetGrade.Items.FindByValue(oDataAsset.Rows(0).Item("GradeCode").ToString))
                    If cboKondisiAsset.SelectedValue = "U" Then
                        cboAssetGrade.Enabled = True
                    Else
                        cboAssetGrade.Enabled = False
                    End If
                    Me.NU = cboKondisiAsset.SelectedValue

                    ' ucAO1.LookupID = oDataAsset.Rows(0).Item("AOID").ToString
                    'ucAO1.LookupName = oDataAsset.Rows(0).Item("AOName").ToString


                    'Me.AOID = oDataAsset.Rows(0).Item("AOID").ToString
                    'txtAO.Text = oDataAsset.Rows(0).Item("AOName").ToString

                    Me.SalesID = oDataAsset.Rows(0).Item("SalesmanID").ToString
                    Me.SupervisorID = oDataAsset.Rows(0).Item("SalesSupervisorID").ToString
                    Me.AdminID = oDataAsset.Rows(0).Item("SupplierAdminID").ToString
                    Me.AOID = oDataAsset.Rows(0).Item("AOID").ToString

                    cbocmo.SelectedIndex = cbocmo.Items.IndexOf(cbocmo.Items.FindByValue(oDataAsset.Rows(0).Item("AOID").ToString.Trim))
                    cboSalesman.SelectedIndex = cboSalesman.Items.IndexOf(cboSalesman.Items.FindByValue(oDataAsset.Rows(0).Item("SalesmanID").ToString.Trim))
                    'cboSalesSpv.SelectedIndex = cboSalesSpv.Items.IndexOf(cboSalesSpv.Items.FindByValue(oDataAsset.Rows(0).Item("SalesSupervisorID").ToString))
                    'cboSupplierAdm.SelectedIndex = cboSupplierAdm.Items.IndexOf(cboSupplierAdm.Items.FindByValue(oDataAsset.Rows(0).Item("SupplierAdminID").ToString))
                End If

            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Sub BindAttributeEdit()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.AppID = Me.ApplicationID
        oAssetData.AssetID = Me.Asset
        oAssetData.SpName = "spEditAssetDataAttribute"
        oAssetData = m_controller.EditGetAttribute(oAssetData)
        oData = oAssetData.ListData
        dtgAttribute.DataSource = oData
        dtgAttribute.DataBind()
    End Sub

    Sub BindAssetEdit(ByVal WhereCond As String)
        Dim oData As New DataTable
        Dim oDoc As New DocRec
        oDoc.strConnection = GetConnectionString()
        oDoc.WhereCond = WhereCond
        oDoc.SpName = "spEditAssetDataAssetDoc"
        oDoc = m_Doc.GetSPReport(oDoc)
        oData = oDoc.ListDataReport.Tables(0)
        dtgAssetDoc.DataSource = oData
        dtgAssetDoc.DataBind()
    End Sub

    Sub BindAssetRegistration()
        Dim entitiesAssetData As New Parameter.AssetData
        Dim oAssetDataController As New AssetDataController
        Dim oDataRegistration As New DataTable

        With entitiesAssetData
            .AppID = Me.ApplicationID
            .strConnection = GetConnectionString()
            .SpName = "spEditAssetDataAssetRegistration"
        End With

        entitiesAssetData = oAssetDataController.EditGetAssetRegistration(entitiesAssetData)

        If Not entitiesAssetData Is Nothing Then
            oDataRegistration = entitiesAssetData.ListData
            If oDataRegistration.Rows.Count > 0 Then
                txtName.Text = oDataRegistration.Rows(0).Item(1).ToString.Trim
                rboBPKBanBadanUsaha.SelectedIndex = rboBPKBanBadanUsaha.Items.IndexOf(rboBPKBanBadanUsaha.Items.FindByValue(oDataRegistration.Rows(0).Item("OwnerAssetCompany").ToString.Trim))
                txtAssetNotes.Text = oDataRegistration.Rows(0).Item(10).ToString.Trim
                UCAddress.Address = oDataRegistration.Rows(0).Item(2).ToString.Trim
                'UCAddress.RT = oDataRegistration.Rows(0).Item(6).ToString.Trim
                'UCAddress.RW = oDataRegistration.Rows(0).Item(7).ToString.Trim
                'UCAddress.Kelurahan = oDataRegistration.Rows(0).Item(3).ToString.Trim
                'UCAddress.Kecamatan = oDataRegistration.Rows(0).Item(4).ToString.Trim
                UCAddress.City = oDataRegistration.Rows(0).Item(5).ToString.Trim
                UCAddress.ZipCode = oDataRegistration.Rows(0).Item(8).ToString.Trim
                UCAddress.BindAddress()
                UCAddress.TeleponFalse()
                UCAddress.ValidatorTrue2()
            End If
        End If
    End Sub

    Protected Sub rboKaroseri_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rboKaroseri.SelectedIndexChanged
        resetPanelKaroseri()
    End Sub

    Sub resetPanelKaroseri()
        pnlLookupKaroseri.Visible = isKaroseri
        pnlSplitBayar.Visible = isKaroseri
        pnlHargaKaroseri.Visible = isKaroseri
        pnlDPKaroseri.Visible = isKaroseri
        If isKaroseri = False Then
            txtKaroseriSupplierCode.Text = ""
            txtKaroseriSupplierName.Text = ""

            ucHargakaroseri.Text = "0"
            calcHargaOTR()
            chkSplitBayar.Checked = False
        End If
    End Sub

    Private Sub ucHargaKaroseri_TextChanged() Handles ucHargakaroseri.TextChanged
        calcHargaOTR()
    End Sub
    Private Sub TotalNetto()
        If txtDP.Text <> "" Then
            lblTotalPembiayaan.Text = FormatNumber(CStr(CInt(Replace(lblTotalHargaOTR.Text, ",", "")) - CInt(Replace(txtDP.Text, ",", ""))), 0)
        End If

    End Sub

    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        Dim controllerassetData As New EditApplicationController
        Dim EntitiesAssetData As New Parameter.Application
        Dim oDataAsset As New DataTable

        With EntitiesAssetData
            .ApplicationID = Me.ApplicationID
            .strConnection = GetConnectionString()
            .SpName = "EditAssetDataGetInfoAssetData"
        End With

        EntitiesAssetData = controllerassetData.EditAssetDataGetInfoAssetData(EntitiesAssetData)
        If Not EntitiesAssetData Is Nothing Then
            oDataAsset = EntitiesAssetData.ListData
            If oDataAsset.Rows(0).Item("SupplierID").trim <> "" Then
                Me.Mode = "Edit"
            Else
                Me.Mode = "Add"
            End If

        End If

        Me.SupplierID = txtSupplierCode.Text.ToString.Trim

        Me.AssetCode = hdfAssetCode.Value
        GetAssetMaster()
        AssetDocumentFilter()
        getSupplierStatus(Me.SupplierID)

        For index = 0 To dtgAttribute.Items.Count - 1
            CType(dtgAttribute.Items(index).FindControl("lblVAttribute"), Label).Visible = False
        Next

        Dim WhereSup As String = "SupplierID = '" & txtSupplierCode.Text.ToString.Trim & "' and SupplierEmployeePosition = 'SL' "
        FillCboEmp("SupplierEmployee", cboSalesman, WhereSup)

        WhereSup = "SupplierID = '" & txtSupplierCode.Text.ToString.Trim & "' and SupplierEmployeePosition = 'SV'"
        FillCboEmp("SupplierEmployee", cboSalesSpv, WhereSup)

        WhereSup = "SupplierID = '" & txtSupplierCode.Text.ToString.Trim & "' and SupplierEmployeePosition = 'AM'"
        FillCboEmp("SupplierEmployee", cboSupplierAdm, WhereSup)

        'WhereSup = "BranchID = '" & Me.sesBranchId.Replace("'", "") & "' and EmployeePosition = 'AO'"
        WhereSup = "BranchID = '" & Me.sesBranchId.Replace("'", "") & "' and IsCMO = 1 "
        FillCboEmp("BranchEmployee", cbocmo, WhereSup)


        'modify nofi 02032018 
        If Not EntitiesAssetData Is Nothing Then
            If oDataAsset.Rows.Count > 0 Then
                cboSalesman.SelectedIndex = cboSalesman.Items.IndexOf(cboSalesman.Items.FindByValue(Me.SalesID.Trim))
                cboSalesSpv.SelectedIndex = cboSalesSpv.Items.IndexOf(cboSalesSpv.Items.FindByValue(Me.SupervisorID.Trim))
                cboSupplierAdm.SelectedIndex = cboSupplierAdm.Items.IndexOf(cboSupplierAdm.Items.FindByValue(Me.AdminID.Trim))
                cbocmo.SelectedIndex = cbocmo.Items.IndexOf(cbocmo.Items.FindByValue(Me.AOID.Trim))
                If cboSalesman.Items.Count <= 1 Then
                    ShowMessage(lblMessage, "Supplier tidak mempunyai salesman, Isi Salesman pada master supplier terlebih dahulu!", True)
                    Exit Sub
                End If
            End If
        End If


        'If Request("Page") = "Edit" Then
        '    cboSalesman.SelectedIndex = cboSalesman.Items.IndexOf(cboSalesman.Items.FindByValue(Me.SalesID.Trim))
        '    cboSalesSpv.SelectedIndex = cboSalesSpv.Items.IndexOf(cboSalesSpv.Items.FindByValue(Me.SupervisorID.Trim))
        '    cboSupplierAdm.SelectedIndex = cboSupplierAdm.Items.IndexOf(cboSupplierAdm.Items.FindByValue(Me.AdminID.Trim))
        '    cbocmo.SelectedIndex = cbocmo.Items.IndexOf(cbocmo.Items.FindByValue(Me.AOID.Trim))
        'Else
        '    'cboUsage.SelectedIndex = -1
        '    'cek apakah supplier terpilih mempunyai salesman atau tidak
        '    If cboSalesman.Items.Count <= 1 Then
        '        ShowMessage(lblMessage, "Supplier tidak mempunyai salesman, Isi Salesman pada master supplier terlebih dahulu!", True)
        '        Exit Sub
        '    End If
        'End If

        'Modify by Wira 20171227
        GetCMOFromProspect()
        '---

        pnlNext.Visible = False
        pnlNext2.Visible = True
        pnlOTR.Visible = True
        resetPanelKaroseri()

        txtYear.Enabled = False
        rboKaroseri.Enabled = False

        pnlLookupSupplier.Visible = False
        pnlLookupAsset.Visible = False


    End Sub

    Private Sub btnNext2_Click(sender As Object, e As System.EventArgs) Handles btnNext2.Click
        pnlLast.Visible = True
        pnlNext2.Visible = False
        pnlLookupKaroseriSupplier.Visible = False

        chkSplitBayar.Enabled = False
        ucOTR.Enabled = False
        ucHargakaroseri.Enabled = False
        getPHJMB()

        If Me.DownPayment > 0 Then
            txtDP.Text = FormatNumber(Me.DownPayment, 0)
            txtDPPersen.Text = FormatNumber(Me.DownPayment / CDbl(ucOTR.Text) * 100)
            'txtDPPersen.Text = FormatNumber(Me.DownPayment / CDbl(lblTotalHargaOTR.Text) * 100)
            txtDPKaroseri.Text = FormatNumber(Me.DPKaroseriAmount, 0)
            txtDPPersenKaroseri.Text = FormatNumber(Me.DPKaroseriAmount / CDbl(ucHargakaroseri.Text) * 100)
        Else
            Dim totaldp As Double = CDbl(ucOTR.Text) * CDbl(txtDPPersen.Text) / 100
            'Dim totaldp As Double = CDbl(lblTotalHargaOTR.Text) * CDbl(txtDPPersen.Text) / 100
            txtDP.Text = FormatNumber(totaldp, 0)
        End If
        'lblTotalPembiayaan.Text = FormatNumber(CStr(CDbl(lblTotalHargaOTR.Text) - CDbl(txtDP.Text)), 0)
        lblTotalPembiayaan.Text = FormatNumber(CStr(CDbl(lblTotalHargaOTR.Text) - CDbl(txtDP.Text) - CDbl(txtDPKaroseri.Text)), 0)
        cekimage()

        If Me.NU.Trim = "U" Then
            Dim g As Double = CDbl(cboAssetGrade.SelectedValue)
            Dim txt As String = " &nbsp;&nbsp; (Grade " & cboAssetGrade.SelectedItem.Text & " = " & FormatNumber(g, 0) & "%" & "x" & lblTotalHargaOTR.Text & ")"
            lblOTRPembanding.Text = FormatNumber(CDbl(lblTotalHargaOTR.Text) * g / 100, 0) & txt
        Else
            lblOTRPembanding.Text = ""
        End If

    End Sub

    Sub GetAssetMaster()
        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objcommand.Connection = objconnection
        objcommand.CommandType = CommandType.StoredProcedure
        objcommand.CommandText = "spAssetMasterView"
        objcommand.Parameters.Add("@AssetCode", SqlDbType.VarChar, 50).Value = Me.AssetCode
        objread = objcommand.ExecuteReader
        If objread.Read Then
            Me.Origination = CStr(objread("Origination")).Trim
            Me.isKaroseri = CBool(objread("Karoseri").ToString.Trim)
        End If

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objconnection.Dispose()
    End Sub

    Private Sub cboSalesman_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboSalesman.SelectedIndexChanged
        Dim SPVID As String
        Dim suppController As New Controller.SupplierController
        Dim oCustom As New Parameter.Supplier
        With oCustom
            .strConnection = GetConnectionString()
            .SupplierID = txtSupplierCode.Text
            .SupplierEmployeeID = cboSalesman.SelectedValue
        End With
        SPVID = suppController.GetSupplierEmployeeSPVBySales(oCustom).SupervisorID
        cboSalesSpv.SelectedIndex = cboSalesSpv.Items.IndexOf(cboSalesSpv.Items.FindByValue(SPVID))
    End Sub

   
    Sub AssetDocumentFilter()
        Dim BPKBdanBadanUsaha As String = ""
        Dim IjinTrayek As String = ""
        Dim notIn As String = ""
        Dim IsCP As String = ""

        If rboIjinTrayek.SelectedValue = False Then
            IjinTrayek = "'IZNTRAY'"
            pnlInformasiTrayek.Visible = False
            txtTrayekAtasNama.Text = ""
            txtJurusan.Text = ""
            txtBukuKeur.Text = ""
        Else
            pnlInformasiTrayek.Visible = True
        End If
        If CBool(rboBPKBanBadanUsaha.SelectedValue) Then
            BPKBdanBadanUsaha = "'LEPASHAK'"

        End If

        If IjinTrayek <> "" And BPKBdanBadanUsaha <> "" Then
            notIn = "and  AssetDocumentList.AssetDocID not in (" & IjinTrayek & ", " & BPKBdanBadanUsaha & ")"
        ElseIf IjinTrayek <> "" And BPKBdanBadanUsaha = "" Then
            notIn = "and  AssetDocumentList.AssetDocID not in (" & IjinTrayek & ")"
        ElseIf IjinTrayek = "" And BPKBdanBadanUsaha <> "" Then
            notIn = "and  AssetDocumentList.AssetDocID not in (" & BPKBdanBadanUsaha & ")"
        End If

        'Modify by Wira 20170329, agar bisa membedakan document list antara Commercial dengan Passenger
        If cboUsage.SelectedValue = "N" Then
            IsCP = " and AssetDocumentList.ispassenger=1"
        Else
            IsCP = " and AssetDocumentList.isCommercial=1"
        End If

        'modify nofi 02032018
        'BindAssetEdit(" AssetDocumentList.AssetTypeID = '" + Me.Asset + "'  and  AgreementAsset.ApplicationID = '" _
        '+ Me.ApplicationID + "' and " _
        '+ " AssetDocumentList.Origination in ('CKD', '" + Me.Origination + "') " + notIn + IsCP)

        If Me.Mode = "Edit" Then
            BindAssetEdit(" AssetDocumentList.AssetTypeID = '" + Me.Asset + "'  and  AgreementAsset.ApplicationID = '" _
                    + Me.ApplicationID + "' and " _
                    + " AssetDocumentList.Origination in ('CKD','CBU', '" + Me.Origination + "') " + notIn + IsCP)
        Else
            BindAsset(" AssetDocumentList.AssetTypeID = '" + Me.Asset + "' and " _
                    + " AssetDocumentList.Origination in ('CKD','CBU', '" + Me.Origination + "')" + notIn + IsCP)
        End If

    End Sub

#Region "Upload image Asset"
    Private Sub cekimage()
        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakDepan.jpg") Then
            imgTampakDepan.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "\Asset\" + Me.ApplicationID + "\TampakDepan.jpg"))
        Else
            imgTampakDepan.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakBelakang.jpg") Then
            imgTampakBelakang.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakBelakang.jpg"))
        Else
            imgTampakBelakang.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakKiri.jpg") Then
            imgTampakKiri.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakKiri.jpg"))
        Else
            imgTampakKiri.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakKanan.jpg") Then
            imgTampakKanan.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakKanan.jpg"))
        Else
            imgTampakKanan.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakDashboard.jpg") Then
            imgTampakDashboard.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakDashboard.jpg"))
        Else
            imgTampakDashboard.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If


        imgTampakDepan.Height = 200
        imgTampakDepan.Width = 300
        imgTampakBelakang.Height = 200
        imgTampakBelakang.Width = 300
        imgTampakKiri.Height = 200
        imgTampakKiri.Width = 300
        imgTampakKanan.Height = 200
        imgTampakKanan.Width = 300
        imgTampakDashboard.Height = 200
        imgTampakDashboard.Width = 300

    End Sub
    Private Function pathFile(ByVal Group As String, ByVal ApplicationID As String) As String
        Dim strDirectory As String = ""
        strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & ApplicationID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Function getpathFolder() As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function
    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function
    Private Sub btnupload_Click(sender As Object, e As System.EventArgs) Handles btnupload.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""

        If uplTampakDepan.HasFile Then
            If uplTampakDepan.PostedFile.ContentType = "image/jpeg" Then
                FileName = "TampakDepan"
                strExtension = Path.GetExtension(uplTampakDepan.PostedFile.FileName)
                Dim bmpPostedImage As Bitmap = New Bitmap(uplTampakDepan.PostedFile.InputStream)
                Dim objImage As Image = ImageResize(bmpPostedImage, 800, 600)
                objImage.Save(pathFile("Asset", Me.ApplicationID) + FileName + strExtension, System.Drawing.Imaging.ImageFormat.Jpeg)
            End If
            'If uplTampakDepan.PostedFile.ContentType = "image/jpeg/jpg" Then
            '    FileName = "TampakDepan"
            '    strExtension = Path.GetExtension(uplTampakDepan.PostedFile.FileName)
            '    Dim bmpPostedImage As Bitmap = New Bitmap(uplTampakDepan.PostedFile.InputStream)
            '    Dim objImage As Image = ImageResize(bmpPostedImage, 800, 600)
            '    objImage.Save(pathFile("Asset", Me.ApplicationID) + FileName + strExtension, System.Drawing.Imaging.ImageFormat.Jpeg)
            'End If
        End If
        If uplTampakBelakang.HasFile Then
            If uplTampakBelakang.PostedFile.ContentType = "image/jpeg" Then
                FileName = "TampakBelakang"
                strExtension = Path.GetExtension(uplTampakBelakang.PostedFile.FileName)
                Dim bmpPostedImage As Bitmap = New Bitmap(uplTampakBelakang.PostedFile.InputStream)
                Dim objImage As Image = ImageResize(bmpPostedImage, 800, 600)
                objImage.Save(pathFile("Asset", Me.ApplicationID) + FileName + strExtension, System.Drawing.Imaging.ImageFormat.Jpeg)
            End If
        End If
        If uplTampakKiri.HasFile Then
            If uplTampakKiri.PostedFile.ContentType = "image/jpeg" Then
                FileName = "TampakKiri"
                strExtension = Path.GetExtension(uplTampakKiri.PostedFile.FileName)
                Dim bmpPostedImage As Bitmap = New Bitmap(uplTampakKiri.PostedFile.InputStream)
                Dim objImage As Image = ImageResize(bmpPostedImage, 800, 600)
                objImage.Save(pathFile("Asset", Me.ApplicationID) + FileName + strExtension, System.Drawing.Imaging.ImageFormat.Jpeg)
            End If
        End If
        If uplTampakKanan.HasFile Then
            If uplTampakKanan.PostedFile.ContentType = "image/jpeg" Then
                FileName = "TampakKanan"
                strExtension = Path.GetExtension(uplTampakKanan.PostedFile.FileName)
                Dim bmpPostedImage As Bitmap = New Bitmap(uplTampakKanan.PostedFile.InputStream)
                Dim objImage As Image = ImageResize(bmpPostedImage, 800, 600)
                objImage.Save(pathFile("Asset", Me.ApplicationID) + FileName + strExtension, System.Drawing.Imaging.ImageFormat.Jpeg)
            End If
        End If
        If uplTampakDashboard.HasFile Then
            If uplTampakDashboard.PostedFile.ContentType = "image/jpeg" Then
                FileName = "TampakDashboard"
                strExtension = Path.GetExtension(uplTampakDashboard.PostedFile.FileName)
                Dim bmpPostedImage As Bitmap = New Bitmap(uplTampakDashboard.PostedFile.InputStream)
                Dim objImage As Image = ImageResize(bmpPostedImage, 800, 600)
                objImage.Save(pathFile("Asset", Me.ApplicationID) + FileName + strExtension, System.Drawing.Imaging.ImageFormat.Jpeg)
            End If
        End If
        cekimage()
    End Sub
    Public Shared Function ImageResize(ByVal SourceImage As Image, ByVal NewHeight As Int32, ByVal NewWidth As Int32) As Image

        Dim bitmap As System.Drawing.Bitmap = New System.Drawing.Bitmap(NewWidth, NewHeight, SourceImage.PixelFormat)

        If bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format1bppIndexed Or _
            bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format4bppIndexed Or _
            bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format8bppIndexed Or _
            bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Undefined Or _
            bitmap.PixelFormat = Drawing.Imaging.PixelFormat.DontCare Or _
            bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppArgb1555 Or _
            bitmap.PixelFormat = Drawing.Imaging.PixelFormat.Format16bppGrayScale Then
            Throw New NotSupportedException("Pixel format of the image is not supported.")
        End If

        Dim graphicsImage As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(bitmap)

        graphicsImage.SmoothingMode = Drawing.Drawing2D.SmoothingMode.HighQuality
        graphicsImage.InterpolationMode = Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
        graphicsImage.DrawImage(SourceImage, 0, 0, bitmap.Width, bitmap.Height)
        graphicsImage.Dispose()
        Return bitmap

    End Function
#End Region

    Private Sub cboAssetGrade_SelectedIndexChanged(sender As Object, _
                                                   e As System.EventArgs) Handles cboAssetGrade.SelectedIndexChanged
        If Me.NU.Trim = "U" Then
            Dim g As Double = CDbl(cboAssetGrade.SelectedValue)
            Dim txt As String = " &nbsp;&nbsp; (Grade " & cboAssetGrade.SelectedItem.Text & " = " & FormatNumber(g, 0) & "%" & "x" & lblTotalHargaOTR.Text & ")"
            lblOTRPembanding.Text = FormatNumber(CDbl(lblTotalHargaOTR.Text) * g / 100, 0) & txt
        Else
            lblOTRPembanding.Text = ""
        End If
    End Sub

    Private Sub cboUsage_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboUsage.SelectedIndexChanged
        If cboUsage.SelectedValue = "C" Then
            rboIjinTrayek.Enabled = True
        Else
            rboIjinTrayek.SelectedIndex = 1
            rboIjinTrayek.Enabled = False
        End If
        AssetDocumentFilter()
    End Sub

    Private Sub cboKondisiAsset_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboKondisiAsset.SelectedIndexChanged
        If cboKondisiAsset.SelectedValue = "U" Then
            divregstnk.Visible = True
            cboAssetGrade.Enabled = True
        Else
            divregstnk.Visible = False
            cboAssetGrade.Enabled = False
        End If
        Me.NU = cboKondisiAsset.SelectedValue
        GetSerial()
        For index = 0 To dtgAttribute.Items.Count - 1
            CType(dtgAttribute.Items(index).FindControl("lblVAttribute"), Label).Visible = False
        Next
    End Sub

    Private Sub getSupplierStatus(supplierid As String)
        Dim supController As New SupplierController
        Dim supData As New Parameter.Supplier
        Dim dt As New DataTable

        With supData
            .SupplierID = supplierid
            .strConnection = GetConnectionString()
        End With

        supData = supController.SupplierView(supData)
        lblSupplierStatus.Text = ""

        If Not supData Is Nothing Then
            dt = supData.ListData
            If dt.Rows.Count > 0 Then
                lblSupplierStatus.Text = "Status: " & dt.Rows(0).Item("SupplierBadStatus").ToString
            End If
        End If
    End Sub

    Sub GetCMOFromProspect()
        Dim objProsCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objProsReader As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objProsCommand.Parameters.Clear()
        objProsCommand.CommandType = CommandType.StoredProcedure
        objProsCommand.CommandText = "spConsumerProspect2"
        objProsCommand.Parameters.Add("@CustomerID", SqlDbType.Char, 20).Value = Me.CustomerID.Trim
        objProsCommand.Connection = objconnection
        objProsReader = objProsCommand.ExecuteReader()

        'If objProsReader.Read Then
        '    Me.AOID = objProsReader.Item("AOID").ToString.Trim
        '    cbocmo.SelectedIndex = cbocmo.Items.IndexOf(cbocmo.Items.FindByValue(Me.AOID.Replace(" ", "")))
        'End If
        'objProsReader.Close()

        If cbocmo.SelectedIndex <> cbocmo.Items.IndexOf(cbocmo.Items.FindByValue("Select One")) Then
            cbocmo.Enabled = False
        End If
    End Sub

    Private Sub dtgAttribute_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dtgAttribute.ItemDataBound
        Dim lblName, lblNameReq As Label
        Dim RFVlblName As RequiredFieldValidator
        Dim txtAttribute As TextBox
        If e.Item.ItemIndex >= 0 Then
            lblName = CType(e.Item.FindControl("lblName"), Label)
            lblNameReq = CType(e.Item.FindControl("lblNameReq"), Label)
            RFVlblName = CType(e.Item.FindControl("RFVlblName"), RequiredFieldValidator)
            txtAttribute = CType(e.Item.FindControl("txtAttribute"), TextBox)
            If lblName.Text = "WARNA" Then
                RFVlblName.Enabled = True
                RFVlblName.ErrorMessage = "Harap isi Warna!"
                lblName.Attributes("Class") = "label label_req"
            Else
                If Me.NU.Trim = "N" Then
                    txtAttribute.Enabled = False
                Else
                    txtAttribute.Enabled = True
                End If
                RFVlblName.Enabled = False
            End If
        End If
    End Sub
End Class