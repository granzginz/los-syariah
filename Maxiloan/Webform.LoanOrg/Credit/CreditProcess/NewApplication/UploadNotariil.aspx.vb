﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Public Class UploadNotariil
    Inherits Maxiloan.Webform.WebBased

    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ApplicationID = Request("applicationid").ToString.Trim
    End Sub
    Public Function fileUploadCheck() As Boolean
        If files.PostedFile Is Nothing Then Return False
        If files.PostedFile.ContentLength = 0 Then Return False
        Return True
    End Function
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If fileUploadCheck() Then
            Dim instllmentAmount As Double
            Dim instllmentAmountDesc As Double = 0
            Dim lbltotalbunga As Double = 0
            Dim tglpertama As Date

            Dim appPath As String = HttpContext.Current.Request.ApplicationPath
            Dim physicalPath As String = HttpContext.Current.Request.MapPath(appPath)
            Dim directory As String = physicalPath & "\xml\"
            Dim FileName As String = files.PostedFile.FileName
            Dim fullFileName As String = directory + Me.Session.SessionID + Me.Loginid + "_" + Me.ApplicationID.ToString.Trim + "_" + FileName

          
            files.PostedFile.SaveAs(fullFileName)

            

            Try

                'Dim strConn As String = "Provider= Microsoft.Jet.OLEDB.4.0;Data Source=" & fullFileName & ";Extended Properties=""Excel 8.0;HDR=YES;"""
                Dim strConn As String = "Provider= Microsoft.ACE.OLEDB.15.0;Data Source=" & fullFileName & ";Extended Properties=""Excel 12.0;HDR=YES;"""
                Dim objComm As New OleDb.OleDbCommand
                Dim objConn As New OleDb.OleDbConnection(strConn)

                If objConn.State = ConnectionState.Closed Then objConn.Open()

                objComm.Connection = objConn
                objComm.CommandType = CommandType.Text
                objComm.CommandText = "select * from [Sheet1$]"
                Dim oReader As OleDb.OleDbDataReader


                Dim dt1 As New DataTable
                Dim oCols(9) As String

                oReader = objComm.ExecuteReader(CommandBehavior.CloseConnection)
                dt1.Columns.Add("BranchID") '0
                dt1.Columns.Add("ApplicationId") '1
                dt1.Columns.Add("InsSeqNo") '2
                dt1.Columns.Add("DueDate") '3
                dt1.Columns.Add("InstallmentAmount") '4
                dt1.Columns.Add("InterestAmount") '5
                dt1.Columns.Add("PrincipalAmount") '6
                dt1.Columns.Add("OutstandingPrincipal") '7
                dt1.Columns.Add("OutstandingInterest") '8
                dt1.Columns.Add("BalonInterest") '9

                Dim i As Integer = 0

                While oReader.Read
                    If i = 0 Then
                        instllmentAmount = Math.Round(CDbl(oReader.Item(7).ToString), 0)
                        tglpertama = CDate(oReader.Item(0).ToString)
                    End If
                    oCols(0) = "121"
                    oCols(1) = Me.ApplicationID.Trim
                    oCols(2) = CStr(CDbl(oReader.Item(1).ToString))
                    If Month(CDate(oReader.Item(0).ToString)) = 12 Then
                        instllmentAmountDesc = instllmentAmountDesc + Math.Round(CDbl(oReader.Item(7).ToString), 0) - instllmentAmount
                    End If
                    oCols(3) = oReader.Item(0).ToString
                    oCols(4) = CStr(Math.Round(CDbl(oReader.Item(7).ToString), 0))
                    lbltotalbunga = lbltotalbunga + CDbl(oReader.Item(3).ToString) + CDbl(oReader.Item(6).ToString)
                    oCols(5) = CStr(CDbl(oReader.Item(3).ToString) + CDbl(oReader.Item(6).ToString))
                    oCols(6) = CStr(Math.Round(CDbl(oReader.Item(2).ToString), 0))
                    oCols(7) = CStr(Math.Round(CDbl(oReader.Item(9).ToString), 0))
                    oCols(8) = CStr(Math.Round(CDbl(oReader.Item(10).ToString), 0))
                    oCols(9) = CStr(Math.Round(CDbl(oReader.Item(6).ToString), 0))
                    i += 1
                    dt1.Rows.Add(oCols)
                End While


                objConn.Close()
                Session.Add("balon", dt1)
                Session.Add("totalBunga", lbltotalbunga)

                Response.Write("<script language = javascript>" & vbCrLf _
               & "parent.$('#txtNumInst_txtNumber').val('" & i & "');" & vbCrLf _
               & "parent.$('#lblTenor').val('" & i & "');" & vbCrLf _
               & "parent.$('#lblTotalBunga_txtNumber').val('" & FormatNumber(lbltotalbunga.ToString, 0) & "')" & vbCrLf _
               & "parent.$('#txtTglAngsuranI_txtDateCE').val('" & tglpertama.ToString("dd/MM/yyyy") & "');" & vbCrLf _
               & "parent.$('#txtInstAmt_txtNumber').val('" & FormatNumber(instllmentAmount.ToString, 0) & "');" & vbCrLf _
               & "parent.$('#txtTidakAngsur_txtNumber').val('" & FormatNumber(instllmentAmountDesc, 0) & "');" & vbCrLf _
               & "parent.calculateNPV();" & vbCrLf _
               & "parent.recalculateAngs();" & vbCrLf _
               & "parent.recalculateARGross();" & vbCrLf _
               & "parent.$('#dialog').dialog('close')" & vbCrLf _
               & "</script>")

            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            Finally
                files.FileContent.Dispose()
                'System.IO.File.Delete(fullFileName)
            End Try
        End If

    End Sub
End Class