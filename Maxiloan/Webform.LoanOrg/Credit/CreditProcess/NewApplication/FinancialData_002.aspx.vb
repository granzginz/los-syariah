﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services


Public Class FinancialData_002
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTab
    Protected WithEvents txtDP As ucNumberFormat
    Protected WithEvents txtNumInst As ucNumberFormat
    'Protected WithEvents txtInstAmtFlat As ucNumberFormat
    Protected WithEvents txtInstAmt As ucNumberFormat
    Protected WithEvents ucSupplierEmployeeGrid1 As ucSupplierEmployeeGrid
    Protected WithEvents txtTidakAngsur As ucNumberFormat
    Protected WithEvents txtDPPersen As ucNumberFormat

    Protected WithEvents txtTglAngsuranI As ucDateCE
    Protected WithEvents lblTotalBunga As ucNumberFormat
    Protected WithEvents lblNilaiKontrak As ucNumberFormat



    Protected WithEvents txtRefundBunga As ucNumberFormat
    
    Protected WithEvents lblPendapatanPremiN As ucNumberFormat
    Protected WithEvents lblPendapatanPremi As ucNumberFormat
    

    Protected WithEvents txtSubsidiBungaDealer As ucNumberFormat
    Protected WithEvents txtSubsidiAngsuran As ucNumberFormat


#Region "Refund"
    Protected WithEvents txtBiayaProvisiN As ucNumberFormat
    Protected WithEvents txtBiayaProvisi As ucNumberFormat
    Protected WithEvents txtRefundBiayaProvisiN As ucNumberFormat
    Protected WithEvents txtRefundBiayaProvisi As ucNumberFormat

    Protected WithEvents txtRefundPremi As ucNumberFormat
    Protected WithEvents txtRefundPremiN As ucNumberFormat

    Protected WithEvents ucRefundBunga As ucNumberFormat
    Protected WithEvents txtRefundBungaN As ucNumberFormat

    Protected WithEvents txtRefundAdminN As ucNumberFormat
    Protected WithEvents ucRefundAdmin As ucNumberFormat

#End Region


#End Region


#Region "Constanta"
    Private Const style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim RunRate As Double
    'Dim FlatRate As Decimal
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim m_controller As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController

    Private oRefundInsentifController As New RefundInsentifController
    Private oController As New ApplicationDetailTransactionController

    Private TotalInsGrid As Double
    Private TotalProsentase As Double
    Private TotalBungaNett As Decimal
    Private TotalPencairan As Double
    Private TotalBungaNettAmount As Double

#End Region

#Region "Property"
    Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
    Property Installment() As Double
        Get
            Return CDbl(ViewState("Installment"))
        End Get
        Set(ByVal Value As Double)
            ViewState("Installment") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(ViewState("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(ViewState("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(ViewState("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            ViewState("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(ViewState("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("RejectMinimumIncome") = Value
        End Set
    End Property
    Property First() As String
        Get
            Return ViewState("First").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("First") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return ViewState("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Supplier") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return ViewState("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Property InstallmentScheme() As String
        Get
            Return ViewState("InstallmentScheme").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Property IsIncentiveSupplier() As Boolean
        Get
            Return CType(ViewState("IsIncentiveSupplier"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsIncentiveSupplier") = Value
        End Set
    End Property
    Property RefundInterest As Double
        Get
            Return CType(ViewState("RefundInterest"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundInterest") = value
        End Set
    End Property
    Property AdminFee As Double
        Get
            Return CType(ViewState("AdminFee"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("AdminFee") = value
        End Set
    End Property
    Property Provisi As Double
        Get
            Return CType(ViewState("Provisi"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("Provisi") = value
        End Set
    End Property
    Private Property dtDistribusiNilaiInsentif As DataTable
        Get
            Return CType(ViewState("dtDistribusiNilaiInsentif"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtDistribusiNilaiInsentif") = value
        End Set
    End Property
    Private Property ApplicationDetailTransactionDT As DataTable
        Get
            Return CType(ViewState("ApplicationDetailTransactionDT"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("ApplicationDetailTransactionDT") = value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property NilaiTransaksi() As Decimal
        Get
            Return CType(ViewState("NilaiTransaksi"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("NilaiTransaksi") = Value
        End Set
    End Property
    Private Property EffectiveRate() As Decimal
        Get
            Return CType(ViewState("EffectiveRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffectiveRate") = Value
        End Set
    End Property
    Private Property FlatRate() As Decimal
        Get
            Return CType(ViewState("FlatRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("FlatRate") = Value
        End Set
    End Property
    Private Property rvEffValue As String
        Get
            Return CType(ViewState("rvEffValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffValue") = value
        End Set
    End Property
    Private Property rvEffKey As String
        Get
            Return CType(ViewState("rvEffKey"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffKey") = value
        End Set
    End Property
    Private Property rvEffMinValue As String
        Get
            Return CType(ViewState("rvEffMinValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffMinValue") = value
        End Set
    End Property
    Private Property rvEffMaxValue As String
        Get
            Return CType(ViewState("rvEffMaxValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffMaxValue") = value
        End Set
    End Property
    Private Property rvEffErrorMsg As String
        Get
            Return CType(ViewState("rvEffErrorMsg"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffErrorMsg") = value
        End Set
    End Property
    Property RefundBungaAmount As Double
        Get
            Return CType(ViewState("RefundBungaAmount"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundBungaAmount") = value
        End Set
    End Property
    Property IncomeRatioPercentage() As Decimal
        Get
            Return CDec(ViewState("IncomeRatioPercentage"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("IncomeRatioPercentage") = Value
        End Set
    End Property
    Property CustomerTotalIncome() As Double
        Get
            Return CDbl(ViewState("CustomerTotalIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("CustomerTotalIncome") = Value
        End Set
    End Property
    Public Property OtrNtf() As String
        Get
            Return CStr(ViewState("OtrNtf"))
        End Get
        Set(value As String)
            ViewState("OtrNtf") = value
        End Set
    End Property
#End Region

    Public Property Balon() As DataTable
        Get
            Return CType(ViewState("Balon"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("Balon") = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Session.Remove("balon")
            Me.ApplicationID = Request("ApplicationID")

            Me.EffectiveRate = 0
            Me.rvEffValue = CStr(0)

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()
                .initControls("OnNewApplication")

                Me.CustomerID = .CustomerID
                Me.CustName = .CustomerName
                Me.InterestType = .InterestType.ToString
                Me.InstallmentScheme = .InstallmentScheme.ToString

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If
            End With

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial")
            ucApplicationTab1.setLink()
            txtAmount.Text = "0"

            'GetCookies()

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()


            initObjects()
            Bindgrid_002()
            'showPencairan()


            GetRefundPremiumToSupplier()

            If ucViewApplication1.isProceed = True Then
                btnSave.Visible = False
            End If

            '  ClientScript.RegisterStartupScript(Me.GetType, "checkbutton", "$(function() { checkbutton();})", True)
        End If
    End Sub

#Region "Load Financial Data"
    Sub Bindgrid_002()
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_002"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)

        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.ListData
        End If

        Dim strPOEffectiveRate As String
        Dim strPOEffectiveRateBehaviour As String
        Dim strPBEffectiveRate As String
        Dim strPBEffectiveRateBehaviour As String
        Dim strPEffectiveRate As String
        Dim strPEffectiveRateBehaviour As String

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            lblAssetInsurance3.Text = FormatNumber(objrow.Item("InsAssetCapitalized"), 0)
            lblAssetInsurance32.Text = FormatNumber(objrow.Item("PaidAmountByCust"), 0)
            'lblTotalOTR.InnerText = FormatNumber(objrow.Item(3), 2)
            lblOTR.Text = FormatNumber(objrow.Item("OTRKendaraan"), 0)
            lblKaroseri.Text = FormatNumber(objrow.Item("HargaKaroseri"), 0)
            lblTotalOTR.Text = FormatNumber(objrow.Item("TotalOTR"), 0)
            lblOTRSupplier.Text = FormatNumber(objrow.Item(3), 2)
            txtDP.Text = FormatNumber(Math.Round(CDbl(objrow.Item(4)), 0), 0)
            txtDPSupplier.Text = txtDP.Text
            If Not IsDBNull(objrow("EffectiveDate")) Then txtTglAngsuranI.Text = Format(objrow("EffectiveDate"), "dd/MM/yyyy")
            lblNTF.Text = FormatNumber(objrow.Item(5), 0)
            lblNTFSupplier.Text = FormatNumber(objrow.Item(5), 2)
            lblTenor.Text = objrow.Item(8).ToString.Trim
            lblFlatRateTenor.Text = objrow.Item(8).ToString.Trim
            lblFlatRateTenorSupplier.Text = objrow.Item(8).ToString.Trim
            txtNumInst.Text = objrow.Item(8).ToString.Trim
            lblTenorSupplier.Text = objrow.Item(8).ToString.Trim
            txtNumInstSupplier.Text = objrow.Item(8).ToString.Trim

            EffectiveRate = CDec(objrow.Item("EffectiveRate"))
            'txtEffectiveRate.Text = EffectiveRate.ToString.Trim
            txtEffectiveRate.Text = Str(CDec(Int(CDbl(EffectiveRate) * 100) / 100)) ' objrow.Item("EffectiveRate").ToString.Trim

            txtEffectiveRateSupplier.Text = objrow.Item(6).ToString.Trim
            lblSupplierRate.Text = objrow.Item(6).ToString.Trim

            lblBiayaPolis.Text = FormatNumber(objrow.Item("BiayaPolisAgreement"), 0)
            lblAdminFee.Text = FormatNumber(objrow.Item("AdminFee"), 0)
            lblOtherFee.Text = FormatNumber(objrow.Item("OtherFee"), 0)

            lblAdminFee2.Text = lblAdminFee.Text
            lblIsAdminFeeCredit.Text = "(" & objrow.Item("IsAdminFeeCredit").ToString & ")"
            lblIsAdminFeeCredit2.Text = lblIsAdminFeeCredit.Text


            ' Bagin Effective Rate dan pengecekannya
            strPOEffectiveRate = objrow.Item("POEffectiveRate").ToString.Trim
            strPOEffectiveRateBehaviour = objrow.Item("POEffectiveRatePatern").ToString.Trim

            strPBEffectiveRate = objrow.Item(13).ToString.Trim
            strPBEffectiveRateBehaviour = objrow.Item(14).ToString.Trim

            strPEffectiveRate = objrow.Item(15).ToString.Trim
            strPEffectiveRateBehaviour = objrow.Item(16).ToString.Trim

            'BehaviourPersen("ProductBranch", txtEffectiveRate, strPBEffectiveRate, RVPBEffectiveRate, strPBEffectiveRateBehaviour, btnCalcInst)
            'BehaviourPersen("Product", txtEffectiveRate, strPEffectiveRate, RVPEffectiveRate, strPEffectiveRateBehaviour, btnCalcInst)
            'BehaviourPersen("ProductOffering", txtEffectiveRate, strPOEffectiveRate, RVPOEffectiveRate, strPOEffectiveRateBehaviour, btnCalcInst)
            rvEffKey = strPOEffectiveRateBehaviour
            rvEffValue = strPOEffectiveRate
            RVPBEffectiveRate.Enabled = False
            RVPEffectiveRate.Enabled = False
            RVPOEffectiveRate.Enabled = False

            cboPolaAngsuran.SelectedValue = objrow.Item("PolaAngsuran").ToString.Trim

            If Me.InstallmentScheme <> "IR" Then
                'Behaviour(txtInstAmt, RVInstAmt, objrow.Item("InstallmentAmountBehaviour").ToString.Trim, btnRecalcEffRate)
                'btnRecalcEffRate.Visible = txtInstAmt.Behaviour(txtInstAmt.txtNumber, txtInstAmt.rv, objrow.Item("InstallmentAmountBehaviour").ToString)
                'txtInstAmt.Behaviour(txtInstAmt.txtNumber, txtInstAmt.rv, objrow.Item("InstallmentAmountBehaviour").ToString)
                txtInstAmt.setBehaviour("InstallmentAmount", objrow)
                'Behaviour(txtInstAmtSupplier, RVInstAmtSupplier, objrow.Item("InstallmentAmountBehaviour").ToString.Trim, btnRecalcEffRate)
            End If

            Me.RejectMinimumIncome = CDbl(objrow.Item(11))
            Me.NTFGrossYield = CDbl(objrow.Item(12))
            Me.RefundInterest = CDbl(objrow.Item("RefundInterest"))
            Me.AdminFee = CDbl(objrow.Item("AdminFee"))
            Me.Provisi = CDbl(objrow.Item("ProvisionFee"))
            Me.CustomerTotalIncome = CDbl(objrow.Item("CustomerTotalIncome"))
            Me.IncomeRatioPercentage = CDec(objrow.Item("IncomeInsRatioPercentage"))

            txtGracePeriod2.Text = objrow.Item("GracePeriodLateCharges").ToString


            txtGracePeriod.Text = "0"
            cboPaymentFreq.Attributes.Add("OnChange", "return NumInst('" & lblTenor.Text & "',this.value, '" & txtNumInst.ClientId & "');")
            'cboFirstInstallment.Attributes.Add("OnChange", "return GracePeriod('" & txtGracePeriod.ClientID & "','" & cboGracePeriod.ClientID & "',this.value)")

            oApplication.BranchId = Replace(Me.sesBranchId, "'", "")
            oApplication.ApplicationID = Me.ApplicationID
            oApplication.strConnection = GetConnectionString()
            oApplication = m_ControllerApp.GetDataAppIDProspect(oApplication)

            If oApplication.Err = "" Then
                If Me.ApplicationID.Trim = oApplication.ApplicationID.Trim Then
                    txtInstAmt.Text = CStr(oApplication.InstallmentAmount)
                    cboFirstInstallment.SelectedIndex = cboFirstInstallment.Items.IndexOf(cboFirstInstallment.Items.FindByValue(oApplication.FirstInstallment))
                Else
                    txtInstAmt.Text = CInt(objrow.Item(9)).ToString.Trim
                End If
            End If

            cboFirstInstallment.SelectedValue = objrow.Item("FirstInstallment").ToString
            txtDP.Text = FormatNumber(objrow.Item("DownPayment"), 0)
            txtTidakAngsur.Text = FormatNumber(objrow.Item("InstallmentUnpaid"), 0)
            txtFlatRate.Text = FormatNumber(objrow.Item("FlatRate"), 2)

            If CDbl(txtFlatRate.Text.Trim) = 0 Then
                cboFirstInstallment.SelectedValue = "AD"
                Dim oClass As New Parameter.FinancialData
                With oClass
                    .Tenor = CInt(txtNumInst.Text)
                    .EffectiveRate = CDec(objrow.Item("POEffectiveRate")) / 100
                    .FirstInstallment = cboFirstInstallment.SelectedValue
                End With
                Me.FlatRate = RateConvertion.cEffToFlat(oClass)
                txtFlatRate.Text = FormatNumber(Me.FlatRate, 2)
            End If




            'txtEffectiveRate.ReadOnly = True


            Dim pgross, pinsnet, pselisih, ppolis, potherfee As Double

            lblPremiAsuransiGross.Text = FormatNumber(CDbl(objrow.Item("PremiGross")) + CDbl(objrow.Item("BiayaPolis")), 0)
            lblPremiAsuransiNet.Text = FormatNumber(objrow.Item("PremiNett"), 0)

            pgross = CDbl(IIf(IsNumeric(lblPremiAsuransiGross.Text), lblPremiAsuransiGross.Text, 0))
            pinsnet = CDbl(IIf(IsNumeric(lblPremiAsuransiNet.Text), lblPremiAsuransiNet.Text, 0))
            ppolis = CDbl(IIf(IsNumeric(objrow.Item("BiayaPolis")), objrow.Item("BiayaPolis"), 0))
            potherfee = CDbl(IIf(IsNumeric(objrow.Item("OtherFee")), objrow.Item("OtherFee"), 0))

            pselisih = pgross - pinsnet
            lblSelisihPremi.Text = FormatNumber(pselisih, 0)
            'lblPremiAlokasi.Text = FormatNumber(pselisih + potherfee, 0)



            lblBiayaFidusia.Text = FormatNumber(CDec(objrow("FiduciaFee")), 0)

            lblInsAmountToNPV.Text = FormatNumber(objrow("InsAmountToNPV"), 0)
            lblTotalBunga.Text = FormatNumber(CDbl(objrow("TotalBunga")), 0)
            'lblNilaiKontrak.Text = FormatNumber(CDbl(objrow("NilaiKontrak")), 0)
            lblNilaiKontrak.Text = FormatNumber(CDbl(lblNTF.Text) + CDbl(lblTotalBunga.Text), 0)

            CalculateDPPersen()

            'If CDec(objrow("InstallmentAmount")) <> 0 Then
            txtInstAmt.Text = FormatNumber(CDec(objrow("InstallmentAmount")), 0)
            'End If

            lblAngsuranPertama.Text = IIf(cboFirstInstallment.SelectedValue = "AD", txtInstAmt.Text, "0").ToString
            lblBiayaProvisi.Text = FormatNumber(objrow("ProvisionFee"), 0)



            lblPendapatanPremiN.Text = FormatNumber(objrow("PendapatanPremi"), 0)
            lblPendapatanPremi.Text = FormatNumber(objrow("PendapatanPremiPercent"), 2)

            txtBiayaProvisiN.Text = FormatNumber(objrow("ProvisionFee"), 0)
            txtBiayaProvisi.Text = FormatNumber(objrow("ProvisiPercent"), 2)
            txtSubsidiBungaDealer.Text = FormatNumber(objrow("SubsidiBungaDealer"), 0)
            txtSubsidiAngsuran.Text = FormatNumber(objrow("SubsidiAngsuran"), 0)


            lblRefundAdmin.Text = FormatNumber(objrow.Item("AdminFee"), 0)
            'ucRefundBunga.Text = FormatNumber(objrow("refundBungaPercent"), 2)




            lblNilaiNTFOTR.Text = objrow(objrow("OtrNtf").ToString.Trim).ToString
            Me.OtrNtf = objrow("OtrNtf").ToString.Trim
            lblNilaiRefundBunga.Text = objrow("NilaiRefundBunga").ToString
            lblNilaiTenor.Text = CStr(Math.Ceiling(CDbl(objrow("tenor").ToString) / 12))

           
            'Refund Admin
            Dim refundAdmin_ As Double
            Dim refundAdminP As Double

            If CInt(objrow.Item("SupplierRefundAdminPercent")) > 0 Then
                refundAdmin_ = CInt(objrow.Item("AdminFee")) * (CInt(objrow.Item("SupplierRefundAdminPercent")) / 100)
                ucRefundAdmin.Text = FormatNumber(CInt(objrow.Item("SupplierRefundAdminPercent")), 2)
                txtRefundAdminN.Text = FormatNumber(refundAdmin_, 0)
                txtRefundAdminN.RangeValidatorEnable = True
                txtRefundAdminN.RangeValidatorMinimumValue = "0"
                txtRefundAdminN.RangeValidatorMaximumValue = refundAdmin_.ToString

                ucRefundAdmin.RangeValidatorEnable = True
                ucRefundAdmin.RangeValidatorMinimumValue = "0"
                ucRefundAdmin.RangeValidatorMaximumValue = objrow.Item("SupplierRefundAdminPercent").ToString

            Else
                refundAdminP = CInt(objrow.Item("SupplierRefundAdminAmount")) / CInt(objrow.Item("AdminFee")) * 100
                txtRefundAdminN.Text = FormatNumber(CInt(objrow.Item("SupplierRefundAdminAmount")), 0)
                ucRefundAdmin.Text = FormatNumber(refundAdminP, 2)

                ucRefundAdmin.RangeValidatorEnable = True
                ucRefundAdmin.RangeValidatorMinimumValue = "0"
                ucRefundAdmin.RangeValidatorMaximumValue = refundAdminP.ToString

                txtRefundAdminN.RangeValidatorEnable = True
                txtRefundAdminN.RangeValidatorMinimumValue = "0"
                txtRefundAdminN.RangeValidatorMaximumValue = objrow.Item("SupplierRefundAdminAmount").ToString
            End If


            If CInt(objrow("refundAdminPercent")) > 0 Then
                ucRefundAdmin.Text = FormatNumber(objrow("refundAdminPercent").ToString, 0)
            End If
            If CInt(objrow("refundAdmin")) > 0 Then
                txtRefundAdminN.Text = FormatNumber(objrow("refundAdmin").ToString, 2)
            End If



            'Refund Bunga
            ucRefundBunga.Text = FormatNumber(CDbl(lblNilaiRefundBunga.Text) * 100, 2)
            ucRefundBunga.RangeValidatorEnable = True
            ucRefundBunga.RangeValidatorMinimumValue = "-1"
            ucRefundBunga.RangeValidatorMaximumValue = CStr(CDbl(lblNilaiRefundBunga.Text) * 100)
            'If CInt(objrow.Item("SupplierRefundInterestPercent")) > 0 Then
            '    ucRefundBunga.Text = FormatNumber(objrow("SupplierRefundInterestPercent"), 2)
            'End If

            calculateNPV()
            If CInt(objrow("refundBungaPercent")) > 0 Then
                ucRefundBunga.Text = FormatNumber(objrow("refundBungaPercent"), 2)
            End If
            If CInt(objrow("refundBunga")) > 0 Then
                txtRefundBungaN.Text = FormatNumber(objrow("refundBunga"), 0)
            End If

            'Refund Premi
            Dim refundPremi_ As Double
            Dim refundPremiP As Double

            If CInt(objrow.Item("SupplierRefundPremiPercent")) > 0 Then
                refundPremi_ = CInt(lblPremiAsuransiGross.Text) * (CInt(objrow.Item("SupplierRefundPremiPercent")) / 100)
                txtRefundPremi.Text = FormatNumber(CInt(objrow.Item("SupplierRefundPremiPercent")), 2)
                txtRefundPremiN.Text = FormatNumber(refundPremi_, 0)
                txtRefundPremi.RangeValidatorEnable = True
                txtRefundPremi.RangeValidatorMinimumValue = "0"
                txtRefundPremi.RangeValidatorMaximumValue = objrow.Item("SupplierRefundPremiPercent").ToString
                txtRefundPremiN.RangeValidatorEnable = True
                txtRefundPremiN.RangeValidatorMinimumValue = "0"
                txtRefundPremiN.RangeValidatorMaximumValue = refundPremi_.ToString

            Else
                refundPremiP = CInt(objrow.Item("SupplierRefundPremiAmount")) / CInt(lblPremiAsuransiGross.Text) * 100
                txtRefundPremiN.Text = FormatNumber(CInt(objrow.Item("SupplierRefundPremiAmount")), 0)
                txtRefundPremi.Text = FormatNumber(refundPremiP, 2)

                txtRefundPremi.RangeValidatorEnable = True
                txtRefundPremi.RangeValidatorMinimumValue = "0"
                txtRefundPremi.RangeValidatorMaximumValue = CStr(IIf(refundPremiP < 0, "0", refundPremiP.ToString))
                txtRefundPremiN.RangeValidatorEnable = True
                txtRefundPremiN.RangeValidatorMinimumValue = "0"
                txtRefundPremiN.RangeValidatorMaximumValue = objrow.Item("SupplierRefundPremiAmount").ToString

            End If
            lblPendapatanPremiN.Text = FormatNumber(CInt(lblSelisihPremi.Text) - CInt(txtRefundPremiN.Text), 0)

            If CInt(objrow("refundPremiPercent")) > 0 Then
                txtRefundPremi.Text = FormatNumber(objrow("refundPremiPercent").ToString, 0)
            End If
            If CInt(objrow("refundPremi")) > 0 Then
                txtRefundPremiN.Text = FormatNumber(objrow("refundPremi").ToString, 2)
            End If


            If objrow("GabungRefundSupplier").ToString = "True" Then
                rdoGabungRefundSupplier.SelectedValue = "1"
            Else
                rdoGabungRefundSupplier.SelectedValue = "0"
            End If




            If FormatNumber(CInt(lblNTF.Text)) <> FormatNumber(objrow.Item("OldNTF")) Then
                CalculateInstallment()
            End If

            With ucRefundBunga
                .AutoPostBack = False
                .TextCssClass = "numberAlign2 smaller_text"
            End With
            With txtRefundAdminN
                .AutoPostBack = False
            End With

            CalculateTotalPembiayaan()
            CalculateTotalBayarPertama()


        End If
    End Sub
 
    Sub Behaviour(ByVal textbox As TextBox, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As Button)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MinimumValue = textbox.Text.Trim
                rv.MaximumValue = "999999999999999"
                rv.ErrorMessage = "Input Harus  >= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MaximumValue = textbox.Text.Trim
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input Harus <= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "999999999999999"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Harap isi dengan Angka"
        End Select
    End Sub


    Function checkBehaviourPersenOnSave() As Boolean
        Select Case Me.rvEffKey
            Case "L"
                rvEffMinValue = Me.rvEffValue
                rvEffMaxValue = Me.rvEffValue
                rvEffErrorMsg = "Bunga efektif harus = " & FormatNumber(Me.rvEffValue, 2)

            Case "N"
                rvEffMinValue = Me.rvEffValue
                rvEffMaxValue = "100"
                rvEffErrorMsg = "Bunga efektif harus >= " & FormatNumber(Me.rvEffValue, 2) & " dan <= 100 (dari Produk Jual) "

            Case "X"
                rvEffMinValue = "0"
                rvEffMaxValue = Me.rvEffValue
                rvEffErrorMsg = "Bunga efektif harus >=0 dan <= " & FormatNumber(Me.rvEffValue, 2) & " (dari Produk Jual) "

            Case "D"
                rvEffMinValue = "0"
                rvEffMaxValue = "100"
                rvEffErrorMsg = "Bunga efektif harus >= 0 dan <= 100"
        End Select


        If Not (CDec(txtEffectiveRate.Text) >= CDec(rvEffMinValue) And CDec(txtEffectiveRate.Text) <= CDec(rvEffMaxValue)) Then
            ShowMessage(lblMessage, rvEffErrorMsg, True)
            Return False
        Else
            Return True
        End If
    End Function

    Sub BehaviourPersen(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As Button)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                rv.MinimumValue = value
                rv.MaximumValue = "100"
                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                rv.MaximumValue = value
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >=0 dan <= " & value

                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "100"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >= 0 dan <= 100"
        End Select
    End Sub

    Sub BehaviourPersenGY(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As Button)
        Select Case Key
            Case "L"
                imb.Visible = False
                lblMessage.Text = ""
                lblMessage.Visible = False
            Case "N"
                If CDbl(textbox.Text.Trim) < CDbl(value) Then
                    If Flag = "Product" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk) ", True)
                        Me.Status = True
                    ElseIf Flag = "ProductBranch" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) ", True)
                        Me.Status = True
                    ElseIf Flag = "ProductOffering" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) ", True)
                        Me.Status = True
                    End If
                End If
                imb.Visible = True
            Case "X"
                If CDbl(textbox.Text.Trim) > CDbl(value) Then
                    If Flag = "Product" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk) ", True)
                        Me.Status = True
                    ElseIf Flag = "ProductBranch" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) ", True)
                        Me.Status = True
                    ElseIf Flag = "ProductOffering" Then
                        ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) ", True)
                        Me.Status = True
                    End If
                End If

                imb.Visible = True
            Case "D"
                rv.Enabled = True
                imb.Visible = True
                If (CDbl(textbox.Text.Trim) < 0 And CDbl(textbox.Text.Trim) > 100) Then
                    ShowMessage(lblMessage, "Nilai dari Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= 100!", True)
                    Me.Status = True
                End If
        End Select
    End Sub
#End Region

    '#Region "GetCookies"
    '    Sub GetCookies()
    '        Dim cookie As HttpCookie = Request.Cookies("Financial")
    '        Me.ApplicationID = cookie.Values("id")
    '        Me.CustomerID = cookie.Values("Custid")
    '        Me.CustName = cookie.Values("name")
    '        Me.InterestType = cookie.Values("InterestType")
    '        Me.InstallmentScheme = cookie.Values("InstallmentScheme")
    '    End Sub
    '#End Region

    Protected Sub CalculateDP() Handles txtDPPersen.TextChanged
        txtDP.Text = FormatNumber(CDbl(lblTotalOTR.Text) * CDec(IIf(IsNumeric(txtDPPersen.Text), txtDPPersen.Text, 0)) / 100, 0)
        lblUangMuka.Text = FormatNumber(txtDP.Text, 0)
        'CalculateInstallment()

        CalculateTotalPembiayaan()
        
    End Sub

    Protected Sub CalculateDPPersen() Handles txtDP.TextChanged
        txtDPPersen.Text = FormatNumber(CDec(IIf(IsNumeric(txtDP.Text), txtDP.Text, 0)) / CDbl(lblTotalOTR.Text) * 100, 2)
        'CalculateInstallment()
        lblUangMuka.Text = FormatNumber(txtDP.Text, 0)
        CalculateTotalPembiayaan()
        
    End Sub

#Region "Calculate Installment"
    'Protected Sub CalculateInstallment2() Handles btnCalcInst.Click
    '    Dim inst As Double

    '    inst = InstAmtFlat(CDec(txtFlatRate.Text))
    'txtInstAmtFlat.Text = FormatNumber(Math.Ceiling(Math.Round(inst, 0) / 1000) * 1000, 0)
    'End Sub

    Protected Sub CalculateInstallment() Handles txtFlatRate.TextChanged, txtDP.TextChanged, txtNumInst.TextChanged, txtDPPersen.TextChanged, cboFirstInstallment.SelectedIndexChanged
        Dim oEntities As New Parameter.FinancialData
        Dim Inst As Double
        'Dim EffRateToUse As Double = EffectiveRate        
        lblMessage.Visible = False

        If cboPolaAngsuran.SelectedValue = "NOTARIIL" Then
            Exit Sub
        End If

        CalculateTotalPembiayaan()

        If Me.StepUpDownType = "RL" Or Me.StepUpDownType = "LS" Then
            If CInt(txtCummulative.Text) > CInt(txtNumInst.Text) Then
                ShowMessage(lblMessage, "Kumulatif  < Jangka waktu Angsuran", True)
                Exit Sub
            End If
        End If

        'If EffRateToUse <= 0 Then
        '    ShowMessage(lblMessage, "Bunga Efective Harus > 0!", True)
        '    Exit Sub
        'End If

        Me.FlatRate = CDec(txtFlatRate.Text)

        If FlatRate < 0 Then
            ShowMessage(lblMessage, "Bunga harus >= 0!", True)
            Exit Sub
        Else
            Dim oClass As New Parameter.FinancialData

            With oClass
                .Tenor = CInt(txtNumInst.Text)
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .FlatRate = Me.FlatRate / 100
            End With

            EffectiveRate = RateConvertion.cFlatToEff(oClass)
            txtEffectiveRate.Text = Str(CDec(Int(CDbl(EffectiveRate) * 100) / 100)) 'FormatNumber(EffectiveRate, 2)

        End If

        'btnRecalcEffRate.Enabled = True
        'btnRecalcEffRate.Visible = True


        If Me.InstallmentScheme = "RF" Then


            Try
                'Inst = InstAmt(EffRateToUse)
                Inst = InstAmt(EffectiveRate)
                txtInstAmt.Text = FormatNumber(Math.Ceiling(Math.Round(Inst, 0) / 1000) * 1000, 0)
                lblAngsuranPertama.Text = IIf(cboFirstInstallment.SelectedValue = "AD", txtInstAmt.Text, "0").ToString
                'CountFlatRate()
                CalculateTotalBunga()

                'lblAngsuranTidakSama.Text = Math.Abs(CDbl(txtInstAmt.Text) - CDbl(txtInstAmtSupplier.Text)).ToString
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
                Exit Sub
            End Try


        ElseIf Me.InstallmentScheme = "IR" Then
            BuatTabelInstallment()
            btnSave.Enabled = True
            btnSave.Visible = True
            txtInstAmt.Enabled = True
        ElseIf Me.InstallmentScheme = "ST" Then
            If Me.StepUpDownType = "NM" Then
                BuatTabelInstallment()
            ElseIf Me.StepUpDownType = "RL" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.CummulativeStart = CInt(txtCummulative.Text)
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                'Entities.EffectiveRate = EffRateToUse
                Entities.EffectiveRate = EffectiveRate
                Entities = m_controller.GetInstAmtStepUpDownRegLeasing(Entities)
                txtInstAmt.Text = FormatNumber(Math.Round(Entities.InstallmentAmount, 0), 0)
                btnSave.Enabled = True
                btnSave.Visible = True
                txtInstAmt.Enabled = True
            Else
                BuatTabelInstallment()
            End If

        Else
            Try
                With oEntities
                    .NTF = CDbl(lblNTF.Text)
                    .NumOfInstallment = CInt(txtNumInst.Text)
                    .GracePeriod = CInt(txtGracePeriod.Text.Trim)
                End With
                oEntities = m_controller.GetPrincipleAmount(oEntities)
                Me.PrincipleAmount = oEntities.Principal
            Catch ex As Exception
                ShowMessage(lblMessage, "Angsuran Pokok Salah", True)
            End Try
        End If


        CalculateTotalBayarPertama()
    End Sub

   

    Private Function IsValidValidation(ByVal textbox As TextBox, ByVal CompareText As ucNumberFormat, ByVal Message As String) As Boolean
        If textbox.Text.Trim = "" Then
            ShowMessage(lblMessage, Message & " Harap diisi", True)
            Return False
        ElseIf CDbl(textbox.Text) = 0 Then
            ShowMessage(lblMessage, Message & " Harap isi > 0 ", True)
            Return False
        ElseIf CDbl(textbox.Text) > CDbl(CompareText.Text) Then
            ShowMessage(lblMessage, Message & " Harap isi < Jangka Waktu Angsuran", True)
            Return False
        End If
        Return True
    End Function




    Private Sub RecalculateEffRate() Handles txtInstAmt.TextChanged

        If cboPolaAngsuran.SelectedValue = "NOTARIIL" Then
            Exit Sub
        End If

        If CInt(txtInstAmt.Text) <= 0 Then
            ShowMessage(lblMessage, "Jumlah Angsuran harus > 0", True)
            Exit Sub
        End If



        Dim Eff As Double
        Dim Supp As Double

        Try
            If Me.InstallmentScheme = "IR" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue

                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Eff = m_controller.GetNewEffRateIRR(Entities)
                txtEffectiveRate.Text = Eff.ToString.Trim
                'count SupplierRate
                Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue

                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Supp = m_controller.GetNewEffRateIRR(Entities)
                lblSupplierRate.Text = Supp.ToString.Trim
            ElseIf Me.InstallmentScheme = "ST" Then
                If Me.StepUpDownType = "NM" Then
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme

                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Eff = m_controller.GetNewEffRateIRR(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme

                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Supp = m_controller.GetNewEffRateIRR(Entities)
                    lblSupplierRate.Text = Supp.ToString.Trim

                ElseIf Me.StepUpDownType = "RL" Then
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateRegLeasing(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Supp = m_controller.GetNewEffRateRegLeasing(Entities)
                    lblSupplierRate.Text = Supp.ToString.Trim

                Else
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme

                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateLeasing(Entities)
                    txtEffectiveRate.Text = Eff.ToString.Trim
                    'count SupplierRate
                    Entities.NTF = CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme

                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Supp = m_controller.GetNewEffRateLeasing(Entities)
                    lblSupplierRate.Text = Supp.ToString.Trim

                End If
            Else
                'count EffectiveRate

                Eff = CountRate(CDbl(lblNTF.Text))
                txtEffectiveRate.Text = Eff.ToString.Trim

                '' cara ambil eff nya samain kaya yang di lostfocus txteffrate
                'Dim oClass2 As New Parameter.FinancialData

                'With oClass2
                '    .Tenor = CInt(txtNumInst.Text)
                '    .FirstInstallment = cboFirstInstallment.SelectedValue
                '    .FlatRate = Me.FlatRate / 100
                'End With

                'EffectiveRate = RateConvertion.cFlatToEff(oClass2)

                'txtEffectiveRate.Text = EffectiveRate.ToString.Trim
                '' end off



                'count SupplierRate
                'Supp = CountRate(CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim))
                'lblSupplierRate.Text = CStr(CDbl(Supp))
                lblSupplierRate.Text = txtEffectiveRate.Text


                'CountFlatRate()

            End If

            'format txtboxeffectiveRate
            EffectiveRate = CDec(txtEffectiveRate.Text)
            txtEffectiveRate.Text = Str(CDec(Int(CDbl(EffectiveRate) * 100) / 100))



            'hitung flat rate
            Dim oClass As New Parameter.FinancialData

            With oClass
                .Tenor = CInt(txtNumInst.Text)
                .EffectiveRate = Me.EffectiveRate / 100
                .FirstInstallment = cboFirstInstallment.SelectedValue
            End With

            Me.FlatRate = RateConvertion.cEffToFlat(oClass)
            txtFlatRate.Text = FormatNumber(Me.FlatRate, 2)

            CalculateTotalBunga()
            lblAngsuranPertama.Text = IIf(cboFirstInstallment.SelectedValue = "AD", txtInstAmt.Text, "0").ToString
            CalculateTotalBayarPertama()

        Catch ex As Exception
            ShowMessage(lblMessage, "Jumlah Angsuran Salah", True)
        End Try
    End Sub

  

    Function InstAmtFlat(ByVal FlatRate As Decimal) As Double
        RunRate = FlatRate / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100)
        If cboFirstInstallment.SelectedValue = "AD" Then
            Return m_controller.GetInstAmtAdvFlat(FlatRate, m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)), CInt(txtNumInst.Text), CDbl(lblNTF.Text))
        Else
            Return 0
        End If
    End Function

    Function InstAmt(ByVal Rate As Double) As Double
        Dim ReturnVal As Double

        RunRate = (Rate / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = Math.Round(m_controller.GetInstAmtAdv(RunRate, CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInst.Text) Then
                    ShowMessage(lblMessage, "Grace Period harus lebih kecil dari Jangka Waktu Angsuran", True)

                    Return 0
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInst.Text) - CInt(txtGracePeriod.Text.Trim), _
                    CType(lblNTF.Text, Double)), 0)
                ElseIf cboGracePeriod.SelectedValue = "R" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArrRO(CDbl(txtEffectiveRate.Text), CInt(txtNumInst.Text), _
                    RunRate, CType(lblNTF.Text, Double), _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim)), 0)
                ElseIf CInt(txtGracePeriod.Text) = 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                    CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
                End If
            Else
                ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
            End If
        End If
        Return ReturnVal
    End Function

    Function InstAmtSupplier(ByVal Rate As Double) As Double
        Dim ReturnVal As Double

        RunRate = (Rate / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = Math.Round(m_controller.GetInstAmtAdv(RunRate, CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInstSupplier.Text), CType(lblNTFSupplier.Text, Double)), 0)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInstSupplier.Text) Then
                    ShowMessage(lblMessage, "Grace Period harus lebih kecil dari Jangka Waktu Angsuran", True)
                    Return 0
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInstSupplier.Text) - CInt(txtGracePeriod.Text.Trim), _
                    CType(lblNTFSupplier.Text, Double)), 0)
                ElseIf cboGracePeriod.SelectedValue = "R" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArrRO(CDbl(txtEffectiveRateSupplier.Text), CInt(txtNumInstSupplier.Text), _
                    RunRate, CType(lblNTFSupplier.Text, Double), _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim)), 0)
                ElseIf CInt(txtGracePeriod.Text) = 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                    CInt(txtNumInstSupplier.Text), CType(lblNTFSupplier.Text, Double)), 0)
                End If
            Else
                ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                CInt(txtNumInstSupplier.Text), CType(lblNTFSupplier.Text, Double)), 0)
            End If
        End If
        Return ReturnVal
    End Function

    Function CountRate(ByVal NTF As Double) As Double
        Dim ReturnVal As Double
        Dim intJmlInst As Integer

        If Me.InstallmentScheme = "ST" And Me.StepUpDownType = "RL" Then
            intJmlInst = CInt(txtCummulative.Text)
        Else
            intJmlInst = CInt(txtNumInst.Text)
        End If

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = m_controller.GetEffectiveRateAdv(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
            Context.Trace.Write("Bila Angsuran Pertama sama dengan AD =" & ReturnVal)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInst.Text) Then
                    ShowMessage(lblMessage, "Grace Period harus lebih kecil dari Jangka Waktu Angsuran", True)
                    Return 0
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst - CInt(txtGracePeriod.Text.Trim), CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
                ElseIf cboGracePeriod.SelectedValue = "R" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = m_controller.GetEffectiveRateRO(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim))
                Else
                    ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
                End If
            Else
                ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst, CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
            End If
        End If
        Return ReturnVal
    End Function

    Function CountRateSupplier(ByVal NTF As Double) As Double
        Dim ReturnVal As Double
        Dim intJmlInst As Integer = CInt(txtNumInst.Text)

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = m_controller.GetEffectiveRateAdv(intJmlInst, CDbl(txtInstAmtSupplier.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInstSupplier.Text) Then
                    ShowMessage(lblMessage, "Grace Period harus lebih kecil dari Jangka Waktu Angsuran", True)
                    Return 0
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" Then
                    ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst - CInt(txtGracePeriod.Text.Trim), CDbl(txtInstAmtSupplier.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
                Else
                    ReturnVal = m_controller.GetEffectiveRateRO(intJmlInst, CDbl(txtInstAmtSupplier.Text), NTF, CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim))
                End If
            Else
                ReturnVal = m_controller.GetEffectiveRateArr(intJmlInst, CDbl(txtInstAmtSupplier.Text), NTF, CInt(cboPaymentFreq.SelectedValue))
            End If
        End If

        Return ReturnVal
    End Function

    Private Sub CountFlatRate()
        lblFlatRate.Text = CDec(100 * ((CDbl(cNum(txtInstAmt.Text)) * CDbl(lblTenor.Text)) - CDbl(lblNTF.Text.Trim)) / CDbl(lblNTF.Text.Trim)).ToString
        lblFlatRateFormatted.Text = FormatNumber(lblFlatRate.Text, 2)
        'lblFlatRate.Visible = True
    End Sub

    Private Sub FlatRateForSupplier()
        Dim FlateRate As Double

        FlateRate = 100 * ((CDbl(txtInstAmtSupplier.Text) * CDbl(lblTenorSupplier.Text)) - CDbl(lblNTFSupplier.Text.Trim)) / CDbl(lblNTFSupplier.Text.Trim)
        lblFlatRateSupplier.Text = FlateRate.ToString
        lblFlatRateSupplier.Visible = True
    End Sub

    Private Sub CalculateTotalBunga()
        'lblTotalBunga.Text = FormatNumber((CDbl(lblNTF.Text) * CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * Me.FlatRate / 100) / (CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue))), 0)
        'Math.Ceiling(Math.Round((CDbl(lblNTF.Text) * CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * Me.FlatRate / 100), 0) / 1000) * 1000
        If cboPolaAngsuran.SelectedValue.ToString <> "NOTARIIL" Then
            'lblTotalBunga.Text = FormatNumber(Math.Ceiling(Math.Round((CDbl(lblNTF.Text) * CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * Me.FlatRate / 100), 0) / 1000) * 1000, 0)
            lblTotalBunga.Text = FormatNumber((CDbl(lblNTF.Text) * CInt(txtNumInst.Text) / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * Me.FlatRate / 100), 0)
            lblNilaiKontrak.Text = FormatNumber(CDbl(lblNTF.Text) + CDbl(lblTotalBunga.Text) + CDbl(lblInsAmountToNPV.Text), 0)
        End If


        calculateNPV()

        txtBiayaProvisiN.Text = FormatNumber(CDbl(lblNilaiKontrak.Text) * (CDbl(txtBiayaProvisi.Text) / 100), 0)


        Dim rasioAngs As Decimal = CDec(CDbl(txtInstAmt.Text) / Me.CustomerTotalIncome * 100)

        If rasioAngs > Me.IncomeRatioPercentage Then
            ShowMessage(lblMessage, "Ratio angsuran terhadap penghasilan " & FormatNumber(rasioAngs, 2) & "%, harus lebih kecil dari " & FormatNumber(Me.IncomeRatioPercentage, 2) & "%", True)
        End If

    End Sub

    Private Sub CalculateTotalBungaSupplier()
        lblNilaiKontrakSupplier.Text = FormatNumber(CDbl(lblNTFSupplier.Text))
    End Sub

#End Region

#Region "Check_GrossYeildRate"
    Private Sub Check_GrossYeildRate()
        Dim oFinancialData As New Parameter.FinancialData
        Dim dtEntity As New DataTable

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_002"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)
        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.ListData
        End If

        Dim strPOGrossYieldRate As String
        Dim strPOGrossYieldRateBehaviour As String
        Dim strPBGrossYieldRate As String
        Dim strPBGrossYieldRateBehaviour As String
        Dim strPGrossYieldRate As String
        Dim strPGrossYieldRateBehaviour As String

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            ' Bagin GrossYieldRate dan pengecekannya
            txtGrossYieldRate.Text = CType(GetGrossYield(), String)
            strPGrossYieldRate = objrow.Item(17).ToString.Trim
            strPGrossYieldRateBehaviour = objrow.Item(18).ToString.Trim

            strPBGrossYieldRate = objrow.Item(19).ToString.Trim
            strPBGrossYieldRateBehaviour = objrow.Item(20).ToString.Trim

            strPOGrossYieldRate = objrow.Item(21).ToString.Trim
            strPOGrossYieldRateBehaviour = objrow.Item(22).ToString.Trim


            'TODO Enable gross yield dan pelajari rumusnya
            'BehaviourPersenGY("ProductOffering", txtGrossYieldRate, strPOGrossYieldRate, RVPOEffectiveRate, strPOGrossYieldRateBehaviour, btnCalcInst)

            'If Me.Status = True Then
            '    Exit Sub
            'End If

            'BehaviourPersenGY("ProductBranch", txtGrossYieldRate, strPBGrossYieldRate, RVPBEffectiveRate, strPBGrossYieldRateBehaviour, btnCalcInst)
            'If Me.Status = True Then
            '    Exit Sub
            'End If

            'BehaviourPersenGY("Product", txtGrossYieldRate, strPGrossYieldRate, RVPEffectiveRate, strPGrossYieldRateBehaviour, btnCalcInst)
            'If Me.Status = True Then
            '    Exit Sub
            'End If
        End If
    End Sub
#End Region

   
#Region "Save"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try


            'RecalculateEffRate()


            'Dim FlatRate As Double
            Dim dtEntity As New DataTable
            Dim oFinancialData As New Parameter.FinancialData
            Dim effRateToUse As Double = EffectiveRate

            'If lblSupplierRate.Text.Trim = "" Then
            '    ShowMessage(lblMessage, "Harap Klik Tombol Recalculate Effective Rate Untuk menghitung Rate Supplier", True)
            '    Exit Sub
            'End If

            Me.Status = False



            If Not checkBehaviourPersenOnSave() Then Exit Sub

            If Me.NTFGrossYield <= 0 Then
                ShowMessage(lblMessage, "Simpan data Gagal. NTF untuk Gross Yield <= 0, NTF Gross yield sekarang adalah " & Me.NTFGrossYield, True)
                Exit Sub
            End If



            If cboPolaAngsuran.SelectedValue = "NOTARIIL" Then

                BuatTable()
                If Not IsNothing(Session("balon")) Then
                    Me.Balon = CType(Session("balon"), DataTable)
                End If

                oFinancialData.MydataSet = InstallmentBalon()

            Else


                If Me.InstallmentScheme = "RF" Then
                    'If CDbl(txtInstAmt.Text) <> InstAmt(effRateToUse) Then
                    If CDbl(txtInstAmt.Text) <> Math.Ceiling(Math.Round(InstAmt(effRateToUse), 0) / 1000) * 1000 Then
                        ShowMessage(lblMessage, "Jumlah Angsuran dan Bunga Effective tidak syncronized. Harap Klik tombol Calculate Installment Amount untuk menghitung Bunga Effective", True)
                        Exit Sub
                    End If

                    'If cboFirstInstallment.SelectedValue = "AD" Then
                    '    oFinancialData.MydataSet = MakeAmortTable1(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), effRateToUse, CInt(cboPaymentFreq.SelectedValue))
                    'ElseIf cboFirstInstallment.SelectedValue = "AR" Then
                    '    If (txtGracePeriod.Text.Trim = "0" Or txtGracePeriod.Text.Trim = "") Then
                    '        oFinancialData.MydataSet = MakeAmortTable2(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), effRateToUse, CInt(cboPaymentFreq.SelectedValue))
                    '    ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "I" Then
                    '        oFinancialData.MydataSet = MakeAmortTable6(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), effRateToUse, CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                    '    ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "R" Then
                    '        oFinancialData.MydataSet = MakeAmortTable3(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), effRateToUse, CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                    '    End If
                    'End If
                End If

                If Me.InstallmentScheme = "RF" Then
                    'If (lblSupplierRate.Text.Trim <> CStr(CountRate(CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)))) Then
                    '    ShowMessage(lblMessage, "Subsidi/Komisi dan Rate Supplier tidak syncronized. Harap klik tombol Recalculate Effective Rate!", True)
                    '    Exit Sub
                    'End If
                    Check_GrossYeildRate()
                    If Me.Status = True Then
                        Exit Sub
                    End If

                    oFinancialData.DiffRate = CDbl(txtAmount.Text.Trim)
                    oFinancialData.GrossYield = GetGrossYield()

                    If HitungUlangSisa Then
                        dblInterestTotal = Math.Round(dblInterestTotal + SisaPrincipal, 2)
                        oFinancialData.InterestTotal = dblInterestTotal
                    End If
                    If HitungUlangKurang Then
                        dblInterestTotal = Math.Round(dblInterestTotal - KurangPrincipal, 2)
                        oFinancialData.InterestTotal = dblInterestTotal
                    End If
                End If
            End If

            If dblInterestTotal < Me.RejectMinimumIncome Then
                ShowMessage(lblMessage, "Total Bunga < Reject Minimum Income, tidak dapat diproses!", True)
            Else
                If Me.InstallmentScheme = "RF" Then
                    'FlatRate = GetFlatRate(dblInterestTotal, CDbl(lblNTF.Text), CInt(cboPaymentFreq.SelectedValue), CInt(lblTenor.Text))
                    oFinancialData.FlatRate = FlatRate
                    oFinancialData.InstallmentAmount = CDbl(txtInstAmt.Text)
                    oFinancialData.NumOfInstallment = CInt(txtNumInst.Text)
                    oFinancialData.FloatingPeriod = CType(IIf(Me.InterestType = "FL", cboFloatingPeriod.SelectedValue.ToString, ""), String)
                ElseIf Me.InstallmentScheme = "IR" Then
                    oFinancialData.IsSave = 1
                ElseIf Me.InstallmentScheme = "ST" Then
                    If Me.StepUpDownType = "NM" Then

                    ElseIf Me.StepUpDownType = "RL" Then
                        Entities.NTF = CDbl(lblNTF.Text)
                        Entities.EffectiveRate = effRateToUse
                        Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                        Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                        Entities.CummulativeStart = CInt(txtCummulative.Text)
                        Entities.NumOfInstallment = CInt(txtNumInst.Text)
                        Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                        Entities = m_controller.GetStepUpDownRegLeasingTBL(Entities)
                        oFinancialData.MydataSet = Entities.MydataSet
                    Else

                    End If
                End If

                Me.DiffRate = CDbl(txtAmount.Text)



                With oFinancialData
                    .BusinessDate = Me.BusinessDate
                    .BranchId = Replace(Me.sesBranchId, "'", "")
                    .AppID = Me.ApplicationID
                    .NTF = CDbl(lblNTF.Text)
                    .EffectiveRate = effRateToUse
                    .SupplierRate = .EffectiveRate
                    .PaymentFrequency = cboPaymentFreq.SelectedValue
                    .FirstInstallment = cboFirstInstallment.SelectedValue
                    .Tenor = CInt(lblTenor.Text)
                    .GracePeriod = CInt(IIf(txtGracePeriod2.Text.Trim = "", "0", txtGracePeriod2.Text.Trim))
                    .GracePeriodType = cboGracePeriod.SelectedValue
                    .BusDate = Me.BusinessDate
                    .DiffRate = Me.DiffRate
                    .Flag = "Add"
                    .strConnection = GetConnectionString()
                    .AdministrationFee = CDbl(lblAdminFee.Text)
                    .PolaAngsuran = cboPolaAngsuran.SelectedValue.ToString
                    .TidakAngsur = CDbl(IIf(IsNumeric(txtTidakAngsur.Text), txtTidakAngsur.Text, "0"))
                    .PotongDanaCair = CBool(optPotongPencairanDana.SelectedValue)
                    .AngsuranBayarDealer = CBool(optBayarDealer.SelectedValue)

                    .TotalBunga = CDbl(lblTotalBunga.Text)
                    .NilaiKontrak = CDbl(lblNilaiKontrak.Text)


                    .InstallmentUnpaid = CDbl(txtTidakAngsur.Text)
                    .DownPayment = CDbl(txtDP.Text)
                    .FlatRate = CDbl(txtFlatRate.Text)

                    If txtTglAngsuranI.Text <> String.Empty Then
                        .EffectiveDate = ConvertDate2(txtTglAngsuranI.Text)
                    Else
                        .EffectiveDate = CDate("01/01/1900")
                    End If

                    .ProvisionFee = CDbl(txtBiayaProvisiN.Text)
                    .AlokasiInsentifRefundBunga = 0
                    .AlokasiInsentifRefundBungaPercent = 0
                    .TitipanRefundBunga = 0
                    .TitipanRefundBungaPercent = 0
                    .AlokasiInsentifPremi = 0
                    .AlokasiInsentifPremiPercent = 0
                    .AlokasiProgresifPremi = 0
                    .AlokasiProgresifPremiPercent = 0
                    .SubsidiBungaPremi = 0
                    .SubsidiBungaPremiPercent = 0
                    .PendapatanPremi = 0
                    .PendapatanPremiPercent = 0
                    .ProvisiPercent = 0
                    .AlokasiInsentifProvisi = 0
                    .AlokasiInsentifProvisiPercent = 0
                    .SubsidiBungaProvisi = 0
                    .SubsidiBungaProvisiPercent = 0
                    .TitipanProvisi = 0
                    .TitipanProvisiPercent = 0
                    .AlokasiInsentifBiayaLain = 0
                    .AlokasiInsentifBiayaLainPercent = 0
                    .SubsidiBungaBiayaLain = 0
                    .SubsidiBungaBiayaLainPercent = 0
                    .TitipanBiayaLain = 0
                    .TitipanBiayaLainPercent = 0
                    .SubsidiBungaDealer = CDbl(txtSubsidiBungaDealer.Text)
                    .SubsidiAngsuran = CDbl(txtSubsidiAngsuran.Text)

                    .BungaNettEff = 0
                    .BungaNettFlat = 0

                    .RefundBungaPercent = CDec(ucRefundBunga.Text)
                    .RefundBungaAmount = CDbl(txtRefundBungaN.Text)
                    .RefundPremiPercent = CDec(txtRefundPremi.Text)
                    .RefundPremiAmount = CDbl(txtRefundPremiN.Text)
                    .RefundAdminPercent = CDec(ucRefundAdmin.Text)
                    .RefundAdminAmount = CDbl(txtRefundAdminN.Text)
                    .RefundProvisiPercent = CDec(txtRefundBiayaProvisi.Text)
                    .RefundProvisiAmount = CDbl(txtRefundBiayaProvisiN.Text)

                    .GabungRefundSupplier = CBool(rdoGabungRefundSupplier.SelectedValue.ToString)

                    'tidak ada subsidi atau provisi maka ke supplier ratenya sama!
                    '.SupplierRate = CDbl(IIf(lblSupplierRate.Text.Trim = "", "0", lblSupplierRate.Text.Trim))
                    '.TotalOTRSupplier = CDbl(lblOTRSupplier.Text)
                    '.DPSupplier = CDbl(txtDPSupplier.Text)
                    '.NTFSupplier = CDbl(lblNTFSupplier.Text)
                    '.TenorSupplier = CInt(lblTenorSupplier.Text)
                    '.FlatRateSupplier = CDec(lblFlatRateSupplier.Text)
                    '.EffectiveRateSupplier = CDec(txtEffectiveRateSupplier.Text)
                    '.TotalBungaSupplier = CDbl(lblTotalBungaSupplier.Text)
                    '.NilaiKontrakSupplier = CDbl(lblNilaiKontrakSupplier.Text)
                    '.InstallmentAmountSupplier = CDbl(txtInstAmtSupplier.Text)
                    '.AngsuranTidakSamaSupplier = CDbl(lblAngsuranTidakSama.Text)
                End With

                ' mengembalikan nilai yang sudah di ubah ke dalam textboxt view

                Try
                    If Me.InstallmentScheme = "RF" Then
                        oFinancialData = m_controller.SaveFinancialData(oFinancialData)
                    ElseIf Me.InstallmentScheme = "IR" Then
                        oFinancialData = m_controller.SaveAmortisasiIRR(oFinancialData)
                    ElseIf Me.InstallmentScheme = "ST" Then
                        oFinancialData = m_controller.SaveAmortisasiStepUpDown(oFinancialData)
                    Else
                        oFinancialData = m_controller.SaveAmortisasi(oFinancialData)
                    End If

                    'Dim rasioAngs As Decimal = CDec(CDbl(txtInstAmt.Text) / Me.CustomerTotalIncome * 100)

                    'If rasioAngs > Me.IncomeRatioPercentage Then
                    '    ShowMessage(lblMessage, "Data Financial sudah tersimpan tapi rasio angsuran " & FormatNumber(rasioAngs, 2) & ", harus lebih kecil dari " & FormatNumber(Me.IncomeRatioPercentage, 2), True)
                    'End If
                Catch ex As Exception
                    Dim err As New MaxiloanExceptions
                    err.WriteLog("FinancialData_002.aspx", "imbSave_Click", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                    ShowMessage(lblMessage, ex.Message, True)
                End Try



                'validate insentif
                Dim msg As String = String.Empty

                'msg = validateinsentif()

                'If msg = String.Empty Then
                '    saveIncentive()
                '    saveDetailTransaction()

                'End If

                If msg = String.Empty And oFinancialData.Output = "" Then
                    'next tab
                    'ShowMessage(lblMessage, "All Data saved!", False)


                    getSuppDistribution()

                    showPencairan()
                    saveBungaNett()

                    ShowMessage(lblMessage, "Data saved!", False)


                    ucApplicationTab1.ApplicationID = Me.ApplicationID
                    ucApplicationTab1.selectedTab("Financial")
                    ucApplicationTab1.setLink()
                ElseIf oFinancialData.Output = "" And msg <> String.Empty Then
                    'ShowMessage(lblMessage, "Financial Data saved! but incentive data error", True)
                ElseIf oFinancialData.Output <> "" And msg = String.Empty Then
                    ShowMessage(lblMessage, oFinancialData.Output, True)
                Else
                    ShowMessage(lblMessage, "Proses Simpan data Gagal " & oFinancialData.Output & "", True)
                End If

            End If


        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("FinancialData_002.aspx", "imbSave_Click", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region




#Region "DiffRate, GrossYield, FlatRate"
    Function DiffRateNormal(ByVal NumInst As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(lblSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate
        For i = 0 To NumInst - 1
            InstallmentAmt(i) = (InstAmtEffective - InstAmtSupp)
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
    End Function
    Function DiffRateRO(ByVal NumInst As Integer, ByVal GracePeriod As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(lblSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate
        For i = 0 To GracePeriod - 1
            InstallmentAmt(i) = 0
        Next i
        For i = GracePeriod To NumInst - 1
            InstallmentAmt(i) = InstAmtEffective - InstAmtSupp
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
    End Function
    Function DiffRateI(ByVal NumInst As Integer, ByVal GracePeriod As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(lblSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate

        For i = 0 To GracePeriod - 1
            InstallmentAmt(i) = InstAmtEffective - (Math.Round(CDbl(lblNTF.Text) * RunRateSupp / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) / 100, 2))
        Next i
        For i = 0 To NumInst - 1
            InstallmentAmt(i) = InstAmtEffective - InstAmtSupp
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
    End Function

    Function GetGrossYield() As Double
        'Me.NTFGrossYield = Me.NTFGrossYield + Me.DiffRate
        'Return CountRate(Me.NTFGrossYield)
        Return CountRate(CDbl(lblNTF.Text))
    End Function

    'Public Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Integer) As Double
    '    Return (dblInterestTotal * 1200) / (dblNTF * CDbl(lblTenor.Text))
    'End Function
#End Region

#Region "Make Amortization Table"
    Public Sub BuatTable()
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "RefundInterest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "AdminFee"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Provisi"
        myDataTable.Columns.Add(myDataColumn)
    End Sub

    Private Sub BuatTabelInstallment()
        Dim inc As Integer
        Dim intRowMax As Integer
        intRowMax = CInt(txtNumInst.Text) - 1

        If Me.InstallmentScheme = "ST" Then
            If CInt(txtStep.Text) > CInt(txtNumInst.Text) Then
                ShowMessage(lblMessage, "Number of Step harus < Jangka waktu Angsuran", True)
                Exit Sub
            End If
            intRowMax = CInt(txtStep.Text) - 1
        End If

        myDataTable = New DataTable("Installment")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)


        For inc = 1 To intRowMax
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("NoOfStep") = 0
            myDataRow("Amount") = 0
            myDataTable.Rows.Add(myDataRow)
        Next inc


    End Sub
    Public Function InstallmentBalon() As DataSet
        Dim dtbalon As New DataTable
        Dim oFinancialData As Parameter.FinancialData
        Dim dtrow As DataRow
        Dim TotalInterest As Double = 0

        If Not IsNothing(Me.Balon) Then
            dtbalon = Me.Balon

            For Each dtrow In dtbalon.Rows
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = dtrow.Item("InsSeqNo").ToString
                myDataRow("Installment") = Math.Round(CDbl(dtrow.Item("InstallmentAmount").ToString), 2)
                myDataRow("Principal") = Math.Round(CDbl(dtrow.Item("PrincipalAmount").ToString), 2)
                myDataRow("Interest") = Math.Round(CDbl(dtrow.Item("InterestAmount").ToString), 2)
                myDataRow("PrincBalance") = Math.Round(CDbl(dtrow.Item("OutstandingPrincipal").ToString), 2)
                myDataRow("PrincInterest") = Math.Round(CDbl(dtrow.Item("OutstandingInterest").ToString), 2)
                myDataTable.Rows.Add(myDataRow)

                TotalInterest = TotalInterest + Math.Round(CDbl(dtrow.Item("InterestAmount").ToString), 2)
            Next
        Else

            Dim oFinancialDataBalon As New Parameter.FinancialData

            oFinancialDataBalon.BranchId = Replace(Me.sesBranchId, "'", "")
            oFinancialDataBalon.AppID = Me.ApplicationID
            oFinancialDataBalon.strConnection = GetConnectionString()

            dtbalon = m_controller.GetInstallmentSchedule(oFinancialDataBalon)

            For Each dtrow In dtbalon.Rows
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = dtrow.Item("InsSeqNo").ToString
                myDataRow("Installment") = Math.Round(CDbl(dtrow.Item("InstallmentAmount").ToString), 2)
                myDataRow("Principal") = Math.Round(CDbl(dtrow.Item("PrincipalAmount").ToString), 2)
                myDataRow("Interest") = Math.Round(CDbl(dtrow.Item("InterestAmount").ToString), 2)
                myDataRow("PrincBalance") = Math.Round(CDbl(dtrow.Item("OutstandingPrincipal").ToString), 2)
                myDataRow("PrincInterest") = Math.Round(CDbl(dtrow.Item("OutstandingInterest").ToString), 2)
                myDataRow("RefundInterest") = Math.Round(CDbl(dtrow.Item("RefundInterest").ToString), 2)
                myDataRow("AdminFee") = Math.Round(CDbl(dtrow.Item("AdminFee").ToString), 2)
                myDataRow("Provisi") = Math.Round(CDbl(dtrow.Item("Provisi").ToString), 2)
                myDataTable.Rows.Add(myDataRow)
            Next

        End If


        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)

        If Not IsNothing(Me.Balon) Then
            Dim custRow As DataRow

            For Each custRow In myDataSet.Tables("AmortTable").Rows
                custRow("RefundInterest") = Math.Round(CDbl(custRow("Interest")) / CDbl(TotalInterest) * Me.RefundInterest, 2)
                custRow("AdminFee") = Math.Round(CDbl(custRow("Interest")) / CDbl(TotalInterest) * Me.AdminFee, 2)
                custRow("Provisi") = Math.Round(CDbl(custRow("Interest")) / CDbl(TotalInterest) * Me.Provisi, 2)
            Next
        End If

        Return myDataSet
    End Function
    Public Function MakeAmortTable1(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Advance - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)

        Dim TotalPrincipal As Double

        PokokHutang(0) = dblNTF
        Bunga(0) = 0
        BuatTable()

        For Me.i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            'Advance dan installment I
            If i = 1 Then
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i = 2 Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = 0
                myDataRow("Principal") = dblInstAmt
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If

            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If

            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For Me.i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet

        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            custRow("RefundInterest") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.RefundInterest, 2)
            custRow("AdminFee") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.AdminFee, 2)
            custRow("Provisi") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.Provisi, 2)
        Next

        Return myDataSet
    End Function

    Public Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()

            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If

            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            custRow("RefundInterest") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.RefundInterest, 2)
            custRow("AdminFee") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.AdminFee, 2)
            custRow("Provisi") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.Provisi, 2)
        Next

        Return myDataSet
    End Function

    Public Function MakeAmortTable3(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Roll Over)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = 0
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(CDbl(myDataRow("Installment")) - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            custRow("RefundInterest") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.RefundInterest, 2)
            custRow("AdminFee") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.AdminFee, 2)
            custRow("Provisi") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.Provisi, 2)
        Next
        Return myDataSet
    End Function

    Public Function MakeAmortTable6(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Interest Only)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = Math.Round(dblNTF * dblRunRate / m_controller.GetTerm(intPayFreq) / 100, 2)
                myDataRow("Interest") = myDataRow("Installment")
                myDataRow("Principal") = 0
            ElseIf i = intGracePeriod + 1 Then
                myDataRow("No") = i - 1
                myDataRow("Interest") = Math.Round(dblNTF * dblRunRate / m_controller.GetTerm(intPayFreq) / 100, 2)
                myDataRow("Installment") = dblInstAmt
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            custRow("RefundInterest") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.RefundInterest, 2)
            custRow("AdminFee") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.AdminFee, 2)
            custRow("Provisi") = Math.Round(CDbl(custRow("Interest")) / CDbl(lblTotalBunga.Text) * Me.Provisi, 2)
        Next

        Return myDataSet
    End Function
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect("FinancialData.aspx")
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As New HttpCookie("Incentive")
        cookie.Values.Add("ApplicationID", Me.ApplicationID)
        cookie.Values.Add("CustomerID", Me.CustomerID)
        cookie.Values.Add("CustomerName", Me.CustName)

        Response.AppendCookie(cookie)
    End Sub
#End Region



#Region "Calculate Total Pembiayaan"
    'Protected Sub txtDP_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtDP.OnTextChanged
    '    CalculateTotalPembiayaan()
    'End Sub

    Protected Sub CalculateTotalPembiayaan()
        Dim ntf_ As Double = CDbl(lblOTR.Text) + CDbl(lblKaroseri.Text) - CDbl(txtDP.Text) + CDbl(lblAssetInsurance3.Text) - CDbl(IIf(lblIsAdminFeeCredit.Text = "(Paid)", 0, lblIsAdminFeeCredit.Text))
        lblNTF.Text = FormatNumber(ntf_, 0)
        If Me.OtrNtf = "ntf" Then
            lblNilaiNTFOTR.Text = ntf_.ToString
        End If
    End Sub

    Protected Sub CalculateTotalPembiayaanSupplier()
        lblNTFSupplier.Text = FormatNumber(CDbl(lblOTRSupplier.Text) - CDbl(txtDPSupplier.Text) + CDbl(lblAssetInsurance3.Text) - CDbl(IIf(lblIsAdminFeeCredit.Text = "(Paid)", 0, lblIsAdminFeeCredit.Text)), 0)
    End Sub

    Private Sub txtDPSupplier_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDPSupplier.TextChanged
        CalculateTotalPembiayaanSupplier()
    End Sub

#End Region

    Protected Sub initObjects()
        lblSupplierRate.Text = ""
        lblInterestType.Text = IIf(Me.InterestType = "FX", "Fixed Rate", "FloatingRate").ToString
        pnlPencairan.Visible = False
        txtStep.Enabled = False
        txtCummulative.Enabled = False



        Entities.strConnection = GetConnectionString()
        Entities.BranchId = Replace(Me.sesBranchId, "'", "")
        Entities.AppID = Me.ApplicationID
        Me.StepUpDownType = m_controller.GetStepUpStepDownType(Entities)



        If Me.InterestType <> "FL" Then
            cboFloatingPeriod.Enabled = False
        Else
            cboFloatingPeriod.Enabled = True
        End If

        If Me.InstallmentScheme = "RF" Then
            lblInstScheme.Text = "Regular Fixed Installment Scheme"
            If cboFirstInstallment.SelectedValue = "AD" Then
                txtGracePeriod.Enabled = False
                txtGracePeriod.Text = "0"
                cboGracePeriod.Enabled = False
                cboGracePeriod.ClearSelection()
            Else
                txtGracePeriod.Enabled = True
                cboGracePeriod.Enabled = True
            End If
        ElseIf Me.InstallmentScheme = "IR" Then
            lblInstScheme.Text = "Irregular Installment Scheme"
            lblInstallment.Text = "Last Installment"
            'RVInstAmt.Enabled = False
            txtInstAmt.rv.Enabled = False
            RVInstAmtSupplier.Enabled = False
            txtInstAmt.Enabled = False
            txtGracePeriod.Enabled = False
            cboGracePeriod.Enabled = False
            btnSave.Enabled = False
            btnSave.Visible = False
        ElseIf Me.InstallmentScheme = "ST" Then
            lblInstScheme.Text = "Step Up / Step Down Installment Scheme"
            'RVInstAmt.Enabled = False
            txtInstAmt.rv.Enabled = False
            RVInstAmtSupplier.Enabled = False
            txtInstAmt.Enabled = False
            txtGracePeriod.Enabled = False
            cboGracePeriod.Enabled = False
            btnSave.Enabled = False
            btnSave.Visible = False
        Else
            lblInstScheme.Text = "Even Principle Installment Scheme"
            txtInstAmt.Enabled = False
            If cboFirstInstallment.SelectedValue = "AD" Then
                txtGracePeriod.Enabled = False
                txtGracePeriod.Text = "0"
                cboGracePeriod.Enabled = False
                cboGracePeriod.ClearSelection()
            Else
                txtGracePeriod.Enabled = True
                cboGracePeriod.Enabled = True
            End If
        End If
        If Me.InterestType = "FX" Then
            cboFloatingPeriod.Visible = False
            If Me.InstallmentScheme = "ST" Then
                If Me.StepUpDownType = "NM" Then
                    txtStep.Visible = True
                    txtCummulative.Visible = False
                ElseIf Me.StepUpDownType = "RL" Then
                    txtStep.Visible = False
                    txtCummulative.Visible = True
                ElseIf Me.StepUpDownType = "LS" Then
                    txtStep.Visible = True
                    txtCummulative.Visible = True
                End If
            Else
                txtStep.Visible = False
                txtCummulative.Visible = False
            End If
        End If
        If Me.InterestType = "FL" Then
            cboFloatingPeriod.Visible = True
            txtStep.Visible = False
            txtCummulative.Visible = False
        End If

        
        lblMessage.Text = ""
        lblMessage.Visible = False
        lblFlatRate.Visible = False

        txtTidakAngsur.RequiredFieldValidatorEnable = False
        txtDP.RequiredFieldValidatorEnable = True
        txtDP.AutoPostBack = True
        txtDP.TextCssClass = "numberAlign2 regular_text"
        txtNumInst.RequiredFieldValidatorEnable = True
        txtNumInst.TextCssClass = "numberAlign2 regular_text"

        With txtNumInst
            .AutoPostBack = True
            .RequiredFieldValidatorEnable = True
            .TextCssClass = "numberAlign2 regular_text"
            .Text = "0"
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "1"
        End With

        txtInstAmt.RequiredFieldValidatorEnable = True
        txtInstAmt.AutoPostBack = True
        txtDPPersen.RequiredFieldValidatorEnable = True
        txtDPPersen.AutoPostBack = True
        txtDPPersen.TextCssClass = "numberAlign2 smaller_text"

       
    End Sub

    Protected Sub FillTestingData()
        cboPolaAngsuran.SelectedIndex = 1
        cboFirstInstallment.SelectedIndex = 1
    End Sub

#Region "incentive"

    Private Sub GetRefundPremiumToSupplier()
        Dim data As New Supplier
        data.strConnection = GetConnectionString()
        data.AppID = Me.ApplicationID

        data = m_controller.GetRefundPremiumToSupplier(data)
        Me.SupplierID = data.SupplierID
    End Sub

    Private Function validateinsentif() As String
        Dim errmsg As String = String.Empty
        'loop rincian transaksi get each transaction total

        For i As Integer = 0 To ApplicationDetailTransactionDT.Rows.Count - 1
            If CBool(ApplicationDetailTransactionDT.Rows(i)("isDistributable")) = False Then Continue For
            Dim transAmount As Decimal = CDec(ApplicationDetailTransactionDT.Rows(i)("txtTransAmount"))
            Dim transID As String = ApplicationDetailTransactionDT.Rows(i)("TransID").ToString

            'errmsg = validateInsentifPerTransaction(transID, transAmount)
            If errmsg <> String.Empty Then Return errmsg
        Next


        Return String.Empty
    End Function

    Private Function validateInsentifPerTransaction(ByVal transID As String, ByVal transAmount As Decimal) As String
        Dim dblTotalInsentifSupplierEmployee As Double

        For i As Integer = 0 To Me.dtDistribusiNilaiInsentif.Rows.Count - 1
            If dtDistribusiNilaiInsentif.Rows(i).Item("transId").ToString.Trim.ToLower = transID.Trim.ToLower Then
                dblTotalInsentifSupplierEmployee += CDbl(Me.dtDistribusiNilaiInsentif.Rows(i).Item("IncentiveForRecepient"))
                TotalInsGrid = TotalInsGrid + dblTotalInsentifSupplierEmployee
            End If

        Next

        If transAmount <> dblTotalInsentifSupplierEmployee Then
            Return "Total distribusi insentif " & transID & "tidak sama dengan nilai insentif!"
        End If

        Return String.Empty
    End Function
#End Region


#Region "rincian transaksi"

    
    

#End Region


    Private Sub CalculateTotalBayarPertama()
        Dim um, ba, bf, at, ap, ot, bp, bpr As Double

        um = CDbl(IIf(IsNumeric(lblUangMuka.Text), lblUangMuka.Text, 0))
        ba = CDbl(IIf(IsNumeric(lblAdminFee.Text), lblAdminFee.Text, 0))
        bf = CDbl(IIf(IsNumeric(lblBiayaFidusia.Text), lblBiayaFidusia.Text, 0))
        at = CDbl(IIf(IsNumeric(lblAssetInsurance32.Text), lblAssetInsurance32.Text, 0))
        ap = CDbl(IIf(IsNumeric(lblAngsuranPertama.Text), lblAngsuranPertama.Text, 0))
        ot = CDbl(IIf(IsNumeric(lblOtherFee.Text), lblOtherFee.Text, 0))
        bp = CDbl(IIf(IsNumeric(lblBiayaPolis.Text), lblBiayaPolis.Text, 0))
        bpr = CDbl(IIf(IsNumeric(lblBiayaProvisi.Text), lblBiayaProvisi.Text, 0))


        lblTotalBayarPertama.Text = FormatNumber(um + ba + bf + at + ap + ot + bp + bpr, 0)

    End Sub

    
    Public Sub getSuppDistribution()
        pnlPencairan.Visible = True
        upPencairan.Update()
    End Sub

    Public Sub showPencairan()
        Dim oClass As New Parameter.ApplicationDetailTransaction

        With oClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
        End With

        Try
            oClass = oController.getBungaNett(oClass)


            gvBungaNet.DataSource = oClass.Tabels.Tables(0)
            gvBungaNet.DataBind()

            gvPencairan.DataSource = oClass.Tabels.Tables(1)
            gvPencairan.DataBind()

            'gvNetRateStatus.DataSource = oClass.Tabels.Tables(4)
            'gvNetRateStatus.DataBind()


        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub


    Private Sub gvBungaNet_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBungaNet.RowDataBound


        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblRate As Label = CType(e.Row.Cells(2).FindControl("lblRate"), Label)


            If gvBungaNet.DataKeys(e.Row.RowIndex).Item("TransID").ToString <> "EFF" Then

                Dim nilai As Double = CType(e.Row.Cells(1).Text, Double)

                lblRate.Text = getRateDetailTransaksi(CDbl(lblNTF.Text) + nilai).ToString
            End If



            If e.Row.Cells(3).Text = "+" Then
                TotalBungaNett += CDec(lblRate.Text)
            Else
                TotalBungaNett -= CDec(lblRate.Text)
            End If



        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalRate As Label = CType(e.Row.Cells(2).FindControl("lblTotalRate"), Label)
            lblTotalRate.Text = Math.Round(TotalBungaNett, 2).ToString


        End If

    End Sub

    Private Sub gvPencairan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPencairan.RowDataBound




        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblNilai As Label = CType(e.Row.Cells(1).FindControl("lblNilai"), Label)

            If e.Row.Cells(3).Text = "+" Then
                TotalPencairan += CDbl(lblNilai.Text)
            Else
                TotalPencairan -= CDbl(lblNilai.Text)
            End If


        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalPencairan As Label = CType(e.Row.Cells(1).FindControl("lblTotalPencairan"), Label)
            'lblTotalPencairan.Text = FormatNumber(Math.Ceiling(Math.Round(TotalPencairan, 0) / 1000) * 1000, 0)
            'lblTotalPencairan.Text = FormatNumber(Math.Round(TotalPencairan / 10, 0) * 10, 0)
            lblTotalPencairan.Text = FormatNumber(TotalPencairan, 0)


        End If
    End Sub

    Private Function getMaxRefundBunga() As String
        Dim oClass As New Parameter.GeneralSetting
        Dim oCtr As New GeneralSettingController

        With oClass
            .strConnection = GetConnectionString()
            .GSID = "RFBNG"
            .ModuleID = "CRD"
        End With

        Try
            Return oCtr.GetGeneralSettingValue(oClass)
        Catch ex As Exception
            Return "0"
        End Try
    End Function
    Private Function getTotalAlokasiPremiGross() As String
        Dim oClass As New Parameter.GeneralSetting
        Dim oCtr As New GeneralSettingController

        With oClass
            .strConnection = GetConnectionString()
            .GSID = "PREMIGROSSTP"
            .ModuleID = "CRD"
        End With

        Try
            Return oCtr.GetGeneralSettingValue(oClass)
        Catch ex As Exception
            Return "0"
        End Try
    End Function
    Private Sub calculateNPV()
        Dim ntf As Double
        Dim refundRate As Decimal
        Dim flatRate As Decimal
        Dim TenorYear As Integer

        'ntf = CDbl(IIf(IsNumeric(lblNTF.Text), lblNTF.Text, 0))
        'refundRate = CDec(IIf(IsNumeric(ucRefundBunga.Text), ucRefundBunga.Text, 0))
        'flatRate = CDec(IIf(IsNumeric(txtFlatRate.Text), txtFlatRate.Text, 0))
        'TenorYear = CInt(IIf(IsNumeric(txtNumInst.Text), CInt(txtNumInst.Text) / 12, 0))

        ntf = CDbl(IIf(IsNumeric(lblNilaiNTFOTR.Text), lblNilaiNTFOTR.Text, 0))
        refundRate = CDec(IIf(IsNumeric(lblNilaiRefundBunga.Text), lblNilaiRefundBunga.Text, 0))
        flatRate = CDec(IIf(IsNumeric(txtFlatRate.Text), txtFlatRate.Text, 0))
        TenorYear = CInt(IIf(IsNumeric(lblNilaiTenor.Text), CInt(lblNilaiTenor.Text), 0))

        Me.RefundBungaAmount = CreditProcess.getNPV(ntf, refundRate, flatRate, TenorYear)
        txtRefundBungaN.Text = FormatNumber(Me.RefundBungaAmount, 0)

    End Sub

    Function getRateDetailTransaksi(TransAmount As Double) As Decimal
        Dim xrate As Decimal
        If cboFirstInstallment.Text = "AD" Then
            xrate = RateConvertion.GetEffectiveRateAdv(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), TransAmount, 1)
        Else
            xrate = RateConvertion.GetEffectiveRateArr(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), TransAmount, 1)
        End If

        Return Math.Round(Math.Abs(Me.EffectiveRate - xrate), 2)
    End Function


    Private Sub saveBungaNett()
        Try
            Dim oBungaNet As New Parameter.FinancialData

            With oBungaNet
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .EffectiveRate = TotalBungaNett / 100
                .Tenor = CInt(txtNumInst.Text)
                .BungaNettEff = TotalBungaNett
                .BungaNettFlat = cEffToFlat(oBungaNet)
            End With

            oBungaNet = m_controller.saveBungaNett(oBungaNet)

        Catch ex As Exception
            'skip

        End Try
    End Sub

End Class