﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ViewApplication_003
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''jlookupContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents jlookupContent As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''upnl1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upnl1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''hdnGridGeneralSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnGridGeneralSize As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblKegiatanUsaha control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblKegiatanUsaha As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboKegiatanUsaha control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboKegiatanUsaha As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblLiniBisnis control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLiniBisnis As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboLiniBisnis control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboLiniBisnis As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblJenisPembiyaan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblJenisPembiyaan As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboJenisPembiyaan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboJenisPembiyaan As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblTipeLoan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTipeLoan As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboTipeLoan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboTipeLoan As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblProductOffering control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProductOffering As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''hyNoFasilitas control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hyNoFasilitas As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''lblInstScheme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInstScheme As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboInstScheme control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboInstScheme As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblInterestType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInterestType As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboInterestType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboInterestType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''rdoSTTYpe control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdoSTTYpe As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''chkCOP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkCOP As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkHakOpsi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkHakOpsi As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''lblWayPymt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblWayPymt As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ucAdminFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucAdminFee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''chkFiducia control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkFiducia As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''ucFiduciaFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucFiduciaFee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblSourceApp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSourceApp As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboSourceApp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboSourceApp As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ucNotaryFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucNotaryFee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ucOtherFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucOtherFee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblAdminFeeGross control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAdminFeeGross As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtAppNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAppNotes As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtKabupaten control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtKabupaten As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''dtgTC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgTC As Global.System.Web.UI.WebControls.DataGrid
    
    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button
End Class
