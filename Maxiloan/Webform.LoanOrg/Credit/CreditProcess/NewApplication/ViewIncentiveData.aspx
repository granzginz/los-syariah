﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewIncentiveData.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewIncentiveData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/UcIncentiveGridView.ascx" TagName="UcIncentiveGridView"
    TagPrefix="uc1" %>
    <%@ Register Src="../../../../webform.UserController/ucApplicationViewTab.ascx" TagName="ucApplicationViewTab"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/UcIncentiveInternalGridView.ascx"
    TagName="UcIncentiveInternalGridView" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>VIEW REFUND - INSENTIF</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" />
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script type='text/javascript' language='javascript'>
        $(function () {
            $(".accordion").accordion();

        });
    </script>
    <script type='text/javascript' language='javascript'>

        function AddNewRecordInternal(clientid) {
            var grd = document.getElementById(clientid);
            if (grd.rows.length <= 2) {
                return false;
            }

            var tbod = grd.rows[0].parentNode;

            var htmlRoot = grd.rows[grd.rows.length - 1].cloneNode(true);

            grd.deleteRow(grd.rows.length - 1);

            var sel = grd.rows[grd.rows.length - 1].getElementsByTagName('select')[0];
            var sel_ = sel.value;

            var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);

            var len = grd.rows.length;
            newRow.cells[0].innerHTML = len;

            var inp1 = newRow.cells[1].getElementsByTagName('select')[0];
            inp1.id += len;
            inp1.value = '';

            var inp2 = newRow.cells[2].getElementsByTagName('input')[0];
            inp2.id += len;
            inp2.value = '';

            var inp3 = newRow.cells[3].getElementsByTagName('input')[0];
            inp3.id += len;
            inp3.value = '';

            var arrClientID = clientid.split("_");
            var nilaiAlokasi;

            if (arrClientID[0] == "UcTitipanRefundBunga") {
                nilaiAlokasi = $('#lblTitipanRefundBunga').html();
            } else if (arrClientID[0] == "UcTitipanProvisi") {
                nilaiAlokasi = $('#lblTitipanProvisi').html();
            } else if (arrClientID[0] == "UcTitipanBiayalainnya") {
                nilaiAlokasi = $('#lblTitipanBiayaLainnya').html();
            }

            inp2.setAttribute("OnChange", "handletxtProsentaseInternal_Change(this.value,'" + nilaiAlokasi + "','" + inp3.id + "')");
            inp3.setAttribute("OnChange", "handletxtInsentifInternal_Change(this.value,'" + nilaiAlokasi + "','" + inp2.id + "')");

            // grd.appendChild(newRow);
            $('#' + clientid).append(newRow);

            if (sel.value != "") {
                $("#" + inp1.id + " option[value=" + sel.value + "]").hide();
            }
            $('#' + clientid).append("<tr class='item_grid'>" + htmlRoot.innerHTML + "</tr>");
            return false;
        }
        function AddNewRecordJQ(clientid) {
            var grd = document.getElementById(clientid);
            var tbod = grd.rows[0].parentNode;

            if (grd.rows.length <= 2) {
                return false;
            }

            var htmlRoot = grd.rows[grd.rows.length - 1].cloneNode(true);

            grd.deleteRow(grd.rows.length - 1);


            var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);

            var len = grd.rows.length;
            newRow.cells[0].innerHTML = len;


            var inp2 = newRow.cells[2].getElementsByTagName('select')[0];
            inp2.id += len;
            inp2.value = '';

            var inp1 = newRow.cells[1].getElementsByTagName('select')[0];
            inp1.id += len;
            inp1.value = '';


            var inp6 = newRow.cells[6].getElementsByTagName('span')[0];
            inp6.id += len;
            inp6.value = '';

            var inp7 = newRow.cells[7].getElementsByTagName('span')[0];
            inp7.id += len;
            inp7.value = '';



            var inp3 = newRow.cells[3].getElementsByTagName('input')[0];
            inp3.id += len;
            inp3.value = '';

            var inp4 = newRow.cells[4].getElementsByTagName('input')[0];
            inp4.id += len;
            inp4.value = '';

            var arrClientID = clientid.split("_");
            var nilaiAlokasi;

            if (arrClientID[0] == "UcPremiAsuransi") {
                nilaiAlokasi = $('#lblAlokasiPremiAsuransi').html();
            } else if (arrClientID[0] == "UcProgresifPremiAsuransi") {
                nilaiAlokasi = $('#lblProgresifPremiAsuransi').html();
            } else if (arrClientID[0] == "UcRefundBunga") {
                nilaiAlokasi = $('#lblRefundBunga').html();
            } else if (arrClientID[0] == "UcPremiProvisi") {
                nilaiAlokasi = $('#lblPremiProvisi').html();
            } else if (arrClientID[0] == "UcBiayaLainnya") {
                nilaiAlokasi = $('#lblBiayaLainnya').html();
            }

            var inp5 = newRow.cells[5].getElementsByTagName('span')[0];
            inp5.id += len;
            inp5.value = '';


            var inp8 = newRow.cells[8].getElementsByTagName('span')[0];
            inp8.id += len;
            inp8.value = '';

            var inp9 = newRow.cells[9].getElementsByTagName('span')[0];
            inp9.id += len;
            inp9.value = '';

            inp1.setAttribute("OnChange", "handleddlJabatan_Change(this,'" + inp2.id + "');getPPH(this.value,'" + inp6.id + "','" + inp7.id + "')");
            inp2.setAttribute("OnChange", "handleddlpenerima_Change(this.value,'" + inp1.id + "','" + inp7.id + "')");
            inp3.setAttribute("OnChange", "handletxtProsentase_Change(this.value,'" + nilaiAlokasi + "','" + inp4.id + "')");
            inp4.setAttribute("OnChange", "handletxtInsentif_Change(this.value,'" + nilaiAlokasi + "','" + inp3.id + "')");

            // grd.appendChild(newRow);
            $('#' + clientid).append(newRow);
            $('#' + clientid).append("<tr class='item_grid'>" + htmlRoot.innerHTML + "</tr>");
            return false;
        }
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" class="tab_container_form">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucapplicationviewtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        VIEW - INSENTIF DATA</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA INCENTIVE</h4>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ALOKASI REFUND PREMI
                        &nbsp;&nbsp;
                        <asp:Label runat="server" ID="lblRefundPremi" /></h4>
                </div>
            </div>
            <uc1:ucincentivegridview id="UcRefundPremi" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ALOKASI REFUND MARGIN
                        &nbsp;&nbsp;
                        <asp:Label runat="server" ID="lblRefundBunga" />
                        </h4>
                </div>
            </div>
            <uc1:ucincentivegridview id="UcRefundBunga" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ALOKASI REFUND BIAYA PROVISI
                        &nbsp;&nbsp;
                    <asp:Label runat="server" ID="lblPremiProvisi" />
                    </h4>
                </div>
            </div>
            <uc1:ucincentivegridview id="UcPremiProvisi" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ALOKASI REFUND BIAYA LAINNYA
                        &nbsp;&nbsp;
                        <asp:Label runat="server" ID="lblBiayaLainnya" />
                        </h4>
                </div>
            </div>
            <uc1:ucincentivegridview id="UcBiayaLainnya" runat="server" />
            <%---------------------------------------------------------------------------------------------------------------------------------------------------------------%>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        SUMMARY INSENTIF PER ORANG &nbsp;&nbsp;<asp:Label runat="server" ID="Label1" />
                    </label>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgSum" runat="server" Width="100%" AutoGenerateColumns="False"
                            BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn HeaderText="JABATAN" DataField="EmployeePosition"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="PENERIMA" DataField="EmployeeName"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="INSENTIF" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="th_right"
                                    DataField="nilaiAlokasi" DataFormatString="{0:N0}"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="tab_container_form_space"><br />
            </div>
            <div class="tab_container_button">
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
