﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewFinancialData.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewFinancialData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Financial Data</title>
    <link href="../../../../Include/general.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - FINANCIAL
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:Label ID="lblCustName" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Aplikasi
                </label>
                <asp:Label ID="lblAppID" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Jenis Margin
                </label>
                <asp:Label ID="lblInterestType" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Skema Angsuran
                </label>
                <asp:Label ID="lblInstScheme" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    PREMI ASURANSI
                </label>
                PREMI ASURANSI BAYAR DIMUKA
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Asuransi Asset
                </label>
                <asp:Label ID="lblInsPremium" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Asuransi Asset
                </label>
                <asp:Label ID="lblInsAdv" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                ASURANSI DIKREDITKAN/CAPITALIZED
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Asuransi Asset
                </label>
                <asp:Label ID="lblInsCapitalized" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DATA FINANCIAL
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Total Harga OTR
                </label>
                <asp:Label ID="lblOTR" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Uang Muka
                </label>
                <asp:Label ID="lblDP" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    NTF / Pokok Hutang
                </label>
                <asp:Label ID="lblNTF" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Margin Effective
                </label>
                <asp:Label ID="lblEffectiveRate" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
            </div>
            <div class="form_right">
                <label>
                    Margin Flat
                </label>
                <asp:Label ID="lblFlatRate" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Margin Supplier
                </label>
                <asp:Label ID="lblSuppRate" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Frequency Pembayaran
                </label>
                <asp:Label ID="lblPymtFreq" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Angsuran Pertama
                </label>
                <asp:Label ID="lblFirstInst" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Jangka Waktu Angsuran
                </label>
                <asp:Label ID="lblNumInst" runat="server" EnableViewState="False"></asp:Label>&nbsp;Tenor
                <asp:Label ID="lblTenor" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Jumlah Angsuran
                </label>
                <asp:Label ID="lblInstAmt" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Gross Yield
                </label>
                <asp:Label ID="lblGross" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Grace Period
                </label>
                <asp:Label ID="lblGrace1" runat="server" EnableViewState="False"></asp:Label>&nbsp;
                <asp:Label ID="lblGrace2" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblLabel" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblDiff" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                AMORTISASI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgAmortization" runat="server" EnableViewState="False" Width="100%"
                CssClass="grid_general" CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                    <asp:BoundColumn DataField="InstallmentAmount" HeaderText="ANGSURAN" DataFormatString="{0:###,###,##.#0;;0.00}"
                        ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                    <asp:BoundColumn DataField="PrincipalAmount" HeaderText="POKOK" DataFormatString="{0:###,###,##.#0;;0.00}"
                        ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                    <asp:BoundColumn DataField="InterestAmount" HeaderText="MARGIN" DataFormatString="{0:###,###,##.#0;;0.00}"
                        ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                    <asp:BoundColumn DataField="OutstandingPrincipal" HeaderText="SISA POKOK" DataFormatString="{0:###,###,##.#0;;0.00}"
                        ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                    <asp:BoundColumn DataField="OutstandingInterest" HeaderText="SISA MARGIN" DataFormatString="{0:###,###,##.#0;;0.00}"
                        ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    <div class="form_button">
        <asp:Button runat="server" ID="btnClose" CausesValidation="false" Text="Close" CssClass="small button gray" UseSubmitBehavior="false"   />
        <asp:Button ID="btnBack" runat="server" EnableViewState="False" Text="Back" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
