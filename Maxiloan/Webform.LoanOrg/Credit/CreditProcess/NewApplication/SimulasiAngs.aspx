﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SimulasiAngs.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.SimulasiAngs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumber" Src="../../../../webform.UserController/ucNumber.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SimulasiAngs</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function NumInst(Tenor, Num, txtNumInst) {
            document.forms[0].txtNumInst.value = Tenor / Num;
        }
        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
        function GracePeriod(txtGracePeriod, cboGracePeriod, Value) {
            if (Value == 'AR') {
                document.forms[0].txtGracePeriod.value = '0'
                if (document.forms[0].txtGracePeriod.disabled == false) {
                    document.forms[0].txtGracePeriod.disabled = false;
                    document.forms[0].cboGracePeriod.disabled = false;
                }
            }
            else {
                document.forms[0].txtGracePeriod.value = '0'
                document.forms[0].txtGracePeriod.disabled = true;
                document.forms[0].cboGracePeriod.disabled = true;
            }
        }
    </script>
</head>
<body>
    <script type="text/javascript">

        // mini jQuery plugin that formats to two decimal places
        (function ($) {
            $.fn.currencyFormat = function () {
                this.each(function (i) {
                    $(this).change(function (e) {
                        if (isNaN(parseFloat(this.value))) return;
                        this.value = parseFloat(this.value).toFixed(2);
                    });
                });
                return this; //for chaining
            }
        })(jQuery);

        // apply the currencyFormat behaviour to elements with 'currency' as their class
        $(function () {
            $('.currency').currencyFormat();
        });

    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlInput" runat="server">
        <asp:Label ID="lblMessage" runat="server" ForeColor="red"></asp:Label>
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    SIMULASI ANGSURAN</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Margin
                </label>
                <asp:DropDownList ID="cboInterestType" runat="server" Enabled="False" onchange="InterestType();">
                    <asp:ListItem Value="FX">Fixed Rate</asp:ListItem>
                    <asp:ListItem Value="FL">Floating Rate</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Skema Angsuran
                </label>
                <asp:DropDownList ID="cboInstScheme" runat="server" Enabled="False" onchange="InstScheme();">
                    <asp:ListItem Value="RF">Regular Fixed Installment Scheme</asp:ListItem>
                    <asp:ListItem Value="IR">Irregular Installment</asp:ListItem>
                    <asp:ListItem Value="ST">Step Up/Step Down</asp:ListItem>
                    <asp:ListItem Value="EP">Even Principle</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Tipe Step Up Step Down
                </label>
                <asp:RadioButtonList ID="rdoSTTYpe" runat="server" Enabled="False" RepeatDirection="Horizontal"
                    AutoPostBack="True" CssClass="opt_single">
                    <asp:ListItem Value="NM">Basic Step Up/Step Down</asp:ListItem>
                    <asp:ListItem Value="RL" Selected="True">Normal</asp:ListItem>
                    <asp:ListItem Value="LS">Leasing Step Up/Step Down</asp:ListItem>
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DATA FINANCIAL</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Pokok Hutang (NTF)
                </label>
                <asp:TextBox ID="txtNTF" runat="server" MaxLength="15" CssClass="currency">0</asp:TextBox>
                <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="txtNTF"
                    Type="Double" ErrorMessage="Harap isi dengan Angka" MaximumValue="999999999999999"
                    MinimumValue="0"></asp:RangeValidator>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Effective
                </label>
                <asp:TextBox runat="server" ID="txtEffectiveDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtEffectiveDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Margin Effective
                </label>
                <asp:TextBox ID="txtEffectiveRate" runat="server" MaxLength="15"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEffectiveDate"
                    ErrorMessage="Harap isi Tanggal Effective" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RVPOEffectiveRate" runat="server" ControlToValidate="txtEffectiveRate"
                    Type="Double" MaximumValue="100" MinimumValue="0" Display="Dynamic"></asp:RangeValidator>
                <asp:RangeValidator ID="RVPBEffectiveRate" runat="server" ControlToValidate="txtEffectiveRate"
                    Type="Double" MaximumValue="100" MinimumValue="0" Display="Dynamic"></asp:RangeValidator>
                <asp:RangeValidator ID="RVPEffectiveRate" runat="server" ControlToValidate="txtEffectiveRate"
                    Type="Double" MaximumValue="100" MinimumValue="0" Display="Dynamic"></asp:RangeValidator>
            </div>
            <div class="form_right">
                <label>
                    Margin Flat
                </label>
                <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Subsidi / Komisi(-)
                </label>
                <asp:TextBox ID="txtAmount" runat="server" MaxLength="15" Columns="18">0</asp:TextBox>
            </div>
            <div class="form_right">
                <label>
                    Angsuran Pertama
                </label>
                <asp:DropDownList ID="cboFirstInstallment" runat="server" CssClass="select_auto">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AD">Advance</asp:ListItem>
                    <asp:ListItem Value="AR">Arrear</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="cboFirstInstallment"
                    ErrorMessage="Harap Pilih Angsuran Pertama" Display="Dynamic" InitialValue="Select One"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cara Angsuran
                </label>
                <asp:DropDownList ID="cboPaymentFreq" runat="server" CssClass="select_auto">
                    <asp:ListItem Value="1">Monthly</asp:ListItem>
                    <asp:ListItem Value="2">Bimonthly</asp:ListItem>
                    <asp:ListItem Value="3">Quarterly</asp:ListItem>
                    <asp:ListItem Value="6">Semi Annualy</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                &nbsp;
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jangka Waktu
                </label>
                <asp:TextBox ID="txtNumInst" runat="server" Columns="4">36</asp:TextBox>&nbsp;&nbsp;&nbsp;Tenor
                <asp:TextBox ID="txtTenor" runat="server" Columns="4">36</asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtNumInst"
                    Type="Integer" ErrorMessage="Jangka waktu harus > 0" Display="Dynamic" Operator="GreaterThan"
                    ValueToCompare="0"></asp:CompareValidator>
            </div>
            <div class="form_right">
                <asp:Label ID="lblFloatingPeriod" runat="server">Periode Floating</asp:Label>
                <asp:DropDownList ID="cboFloatingPeriod" runat="server" CssClass="select_auto">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="M">Monthly</asp:ListItem>
                    <asp:ListItem Value="Q">Quaterly</asp:ListItem>
                    <asp:ListItem Value="S">Semi Annually</asp:ListItem>
                    <asp:ListItem Value="A">Annually</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="reqFloatingPeriod" runat="server" ControlToValidate="cboFloatingPeriod"
                    ErrorMessage="Harap Pilih Floating Period" Display="Dynamic" InitialValue="Select One"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtGrossYieldRate" runat="server" Width="16px" MaxLength="15" Columns="18"
                    ReadOnly="True" Visible="False"></asp:TextBox>&nbsp;
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <asp:Label ID="lblInstallment" runat="server" CssClass="label">Jumlah Angsuran</asp:Label>
                <asp:TextBox ID="txtInstAmt" runat="server" MaxLength="15">0</asp:TextBox>
                <asp:RangeValidator ID="RVInstAmt" runat="server" ControlToValidate="txtInstAmt"
                    Type="Double" ErrorMessage="Harap isi dengan Angka" MaximumValue="999999999999999"
                    MinimumValue="0"></asp:RangeValidator>
            </div>
            <div class="form_right">
                <asp:Label ID="lblNumberOfStep" runat="server">Number of Step</asp:Label>
                <asp:TextBox ID="txtStep" runat="server" MaxLength="15" Columns="4">0</asp:TextBox>&nbsp;
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtStep"
                    ErrorMessage="Harap isi Number Of Step" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="rvStep" runat="server" ControlToValidate="txtstep" Type="Double"
                    ErrorMessage="Number Of Step harus > 1" MaximumValue="999" MinimumValue="2" Display="Dynamic"></asp:RangeValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Grace Period
                </label>
                <asp:TextBox ID="txtGracePeriod" runat="server" MaxLength="2" Columns="4" ReadOnly="True">0</asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtGracePeriod"
                    ErrorMessage="Harap isi dengan Angka" Display="Dynamic" ValidationExpression="\d*"></asp:RegularExpressionValidator>&nbsp;&nbsp;&nbsp;
                <asp:DropDownList ID="cboGracePeriod" runat="server" CssClass="select_auto">
                    <asp:ListItem Value="I">Interest Only</asp:ListItem>
                    <asp:ListItem Value="R">Roll Over</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <asp:Label ID="lblCummulative" runat="server">Devide Start from Inst. No.</asp:Label>
                <asp:TextBox ID="txtCummulative" runat="server" MaxLength="15" Columns="4">0</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCummulative"
                    ErrorMessage="Harap isi dengan Kumulatif" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="rvCummulative" runat="server" ControlToValidate="txtCummulative"
                    Type="Double" ErrorMessage="Kumulatif harus > 0" MaximumValue="999" MinimumValue="1"
                    Display="Dynamic"></asp:RangeValidator>&nbsp;
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnCalcInst" runat="server" Text="Calculate Installment" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnRecalcEffRate" runat="server" Text="ReCalculate rate" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnSave" runat="server" CausesValidation="True" Text="View" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlEntryInstallment" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    ANGSURAN</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgEntryInstallment" runat="server" Width="100%" CellSpacing="1"
                    CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False" AllowSorting="True">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Angsuran Ke">
                            <ItemTemplate>
                                <asp:TextBox ID="txtNoStep" runat="server">0</asp:TextBox>
                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator5" runat="server" Display="Dynamic"
                                    ControlToValidate="txtNoStep" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="reqNoStep" runat="server" ControlToValidate="txtNoStep"
                                    Display="Dynamic" ErrorMessage="Harap isi Number Of Step"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtNoStep"
                                    Display="Dynamic" MinimumValue="1" MaximumValue="999" Type="Integer" ErrorMessage="No of Step harus > 0"></asp:RangeValidator>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH">
                            <ItemTemplate>
                                <asp:TextBox ID="txtEntryInstallment" runat="server">0</asp:TextBox>
                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator6" runat="server" Display="Dynamic"
                                    ControlToValidate="txtEntryInstallment" ErrorMessage="Harap isi dengan Angka"
                                    ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="rfInstallment" runat="server" ControlToValidate="txtEntryInstallment"
                                    Display="Dynamic" ErrorMessage="Harap isi Jumlah Angsuran"></asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnEntryInstallment" runat="server" Text="View" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlViewST" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW - ANGSURAN</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgViewInstallment" runat="server" Width="100%" CssClass="grid_general"
                    CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False"
                    AllowSorting="True">
                    <ItemStyle CssClass="item_grid"></ItemStyle>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="InsSeqNo" HeaderText="No"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="POKOK">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.PrincipalAmount"),2) %>'
                                    ID="Label2" NAME="Label2">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="MARGIN">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InterestAmount"),2) %>'
                                    ID="Label3" NAME="Label3">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SISA POKOK">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingPrincipal"),2) %>'
                                    ID="Label4" NAME="Label4">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SISA MARGIN">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingInterest"),2) %>'
                                    ID="Label5" NAME="Label5">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlInstallGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DATA FINANSIAL</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    NTF</label>
                <asp:Label ID="lblNTF" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Tgl Efektif</label>
                <asp:Label ID="lblTglEfektif" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Margin Efektif</label>
                <asp:Label ID="lblBungaEfektif" runat="server" />
            </div>
            <div class="form_right">
                <label>
                    Tenor</label>
                <asp:Label ID="lblTenor" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Angsuran Pertama</label>
                <asp:Label ID="lblAngsuranPertama" runat="server" />
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW - ANGSURAN</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="Datagrid1" runat="server" Width="100%" CellSpacing="1" CellPadding="3"
                    BorderWidth="0px" AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general"
                    ShowFooter="true">
                    <ItemStyle CssClass="item_grid"></ItemStyle>
                    <HeaderStyle CssClass="th"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="InsSeqNo" HeaderText="No"><FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DueDate" HeaderText="Tgl JT" DataFormatString="{0:dd/MM/yyyy}"><FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Angsuran" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                            <ItemTemplate>
                                <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InstallmentAmount"),2) %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                            <FooterTemplate>
                                <asp:Label ID="lblInstallmentAmountSum" runat="server">Total&nbsp;
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Pokok" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.PrincipalAmount"),2) %>'
                                    ID="lblPrincipalAmount" NAME="Label2">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                            <FooterTemplate>
                                <asp:Label ID="lblPrincipalAmountSum" runat="server">Total&nbsp;
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Margin" ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InterestAmount"),2) %>'
                                    ID="lblInterestAmount" NAME="Label3">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                            <FooterTemplate>
                                <asp:Label ID="lblInterestAmountSum" runat="server">Total&nbsp;
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sisa Pokok" ItemStyle-CssClass="item_grid_right"
                            HeaderStyle-CssClass="th_right">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutstandingPrincipal"),2) %>'
                                    ID="lblOutstandingPrincipal" NAME="Label4">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                            <FooterTemplate>
                                <asp:Label ID="lblOutstandingPrincipalSum" runat="server">Total&nbsp;
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sisa Margin" ItemStyle-CssClass="item_grid_right"
                            HeaderStyle-CssClass="th_right">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutstandingInterest"),2) %>'
                                    ID="lblOutstandingInterest" NAME="Label5">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterStyle BorderStyle="None" HorizontalAlign="Right" />
                            <FooterTemplate>
                                <asp:Label ID="lblOutstandingInterestSum" runat="server">Total&nbsp;
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnPrint" runat="server" Text="View Report" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnBack" runat="server" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
