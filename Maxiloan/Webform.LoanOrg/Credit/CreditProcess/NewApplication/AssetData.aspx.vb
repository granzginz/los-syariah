﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class AssetData
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New AssetDataController
    Dim oAssetData As New Parameter.AssetData
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Protected WithEvents Rangevalidator1 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents rfvGo As System.Web.UI.WebControls.RequiredFieldValidator
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        lblMessage.Visible = False

        If Not Page.IsPostBack Then
            If CheckForm(Me.Loginid, "AssetData", Me.AppId) Then
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    txtGoPage.Text = "1"
                    If Request("cond") <> "" Then
                        Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' and " + Request("cond")
                    Else
                        Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                    End If
                    Me.Sort = "Agreement.ApplicationID ASC"
                    BindGrid(Me.CmdWhere)
                Else
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub
#Region "BindGrid"
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.AssetData
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = m_controller.GetAssetData(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If CheckFeature(Me.Loginid, "AssetData", "Add", Me.AppId) Then
            If sessioninvalid() Then
                Exit Sub
            End If
        End If
        If e.CommandName = "Action" Then            
            Dim AppID As String = CType(e.Item.Cells(1).FindControl("lnkApplicationID"), HyperLink).Text
            Dim AssetID As String = e.Item.Cells(5).Text.Trim
            Dim CustId As String = e.Item.Cells(4).Text.Trim
            Dim CustName As String = CType(e.Item.Cells(2).FindControl("lnkCust"), HyperLink).Text
            Dim ProductOfferingID As String = e.Item.Cells(6).Text.Trim
            Dim ProductID As String = e.Item.Cells(7).Text.Trim
            Dim cookie As HttpCookie = Request.Cookies("AssetData")

            If Not cookie Is Nothing Then
                cookie.Values("Custid") = CustId
                cookie.Values("name") = CustName
                cookie.Values("AssetID") = AssetID
                cookie.Values("ApplicationID") = AppID
                cookie.Values("ProductOfferingID") = ProductOfferingID
                cookie.Values("ProductID") = ProductID
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("AssetData")
                cookieNew.Values.Add("Custid", CustId)
                cookieNew.Values.Add("name", CustName)
                cookieNew.Values.Add("AssetID", AssetID)
                cookieNew.Values.Add("ApplicationID", AppID)
                cookieNew.Values.Add("ProductOfferingID", ProductOfferingID)
                cookieNew.Values.Add("ProductID", ProductID)
                Response.AppendCookie(cookieNew)
            End If

            Response.Redirect("AssetData_002.aspx")
        End If
    End Sub
#End Region

#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' "
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            If cboSearch.SelectedIndex = 1 Then
                Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' and " + cboSearch.SelectedItem.Value + " = '" + Replace(txtSearch.Text.Trim, "'", "''") + "'"
            Else
                Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' and " + cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
            End If
        Else
            Me.CmdWhere = "Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' "
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkApp As HyperLink


        If e.Item.ItemIndex >= 0 Then

            lnkApp = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            lnkApp.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApp.Text.Trim) & "')"

            Dim lnkCust As HyperLink
            lnkCust = CType(e.Item.FindControl("lnkCust"), HyperLink)
            lnkCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(e.Item.Cells(4).Text) & "')"

            'lnkCust.Attributes.Add("OnClick", "return OpenCust('" & e.Item.Cells(4).Text & "','AccAcq');")
            Dim lnkPO As HyperLink
            lnkPO = CType(e.Item.FindControl("lnkProdOff"), HyperLink)
            lnkPO.NavigateUrl = "javascript:OpenWinProductOffering('" & e.Item.Cells(6).Text & "'," & Me.sesBranchId.Replace(",", "") & ",'" & e.Item.Cells(7).Text & "','AccAcq')"

            'lnkPO.Attributes.Add("OnClick", "return OpenWinProductOfferingBranchView('" & e.Item.Cells(6).Text & "','" & e.Item.Cells(7).Text & "','" & e.Item.Cells(8).Text & "','AccAcq');")
        End If
    End Sub

End Class