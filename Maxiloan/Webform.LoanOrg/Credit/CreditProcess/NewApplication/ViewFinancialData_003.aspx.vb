﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services


Public Class ViewFinancialData_003
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTab
#End Region


#Region "Constanta"
    Private m_controller As New FinancialDataController
#End Region

#Region "Property"
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        lblMessage.Text = ""
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("CustID")
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .CustomerID = Me.CustomerID
                .bindData()
                .initControls("OnNewApplication")
            End With
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            BindEdit()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial")
            ucApplicationTab1.setLink()
        End If
    End Sub

#End Region
    
#Region "Load Financial Data"
    Sub BindEdit()
        Dim oData As New DataTable
        Dim oDataamortization As New DataTable
        Dim oFinancialData As New Parameter.FinancialData
        Dim int As Integer

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData = m_controller.GetViewFinancialData(oFinancialData)

        If Not oFinancialData Is Nothing Then
            oData = oFinancialData.Data1
            oDataamortization = oFinancialData.data2
        End If
        If oFinancialData.Output <> "" Then
            lblMessage.Text = oFinancialData.Output
            Exit Sub
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
        dtgViewInstallment.DataSource = oDataamortization.DefaultView
        dtgViewInstallment.DataBind()
        Int = dtgViewInstallment.Items.Count - 1
        dtgViewInstallment.Items(0).Font.Bold = True
        dtgViewInstallment.Items(Int).Font.Bold = True
    End Sub

    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)

        lblFirstInstallment.Text = oRow("FirstInstallment").ToString.Trim
        txtTidakAngsur.Text = FormatNumber(oRow.Item("InstallmentUnpaid"), 0)
        If oRow.Item("GabungRefundSupplier").ToString = "True" Then
            rdoGabungRefundSupplier.SelectedIndex = 0
        Else
            rdoGabungRefundSupplier.SelectedIndex = 1
        End If
        If oRow.Item("PotongDanaCair").ToString = "True" Then
            optPotongPencairanDana.SelectedIndex = 0
        Else
            optPotongPencairanDana.SelectedIndex = 1
        End If
        lblOTR.Text = FormatNumber(oRow.Item("TotalOTR"), 0)
        lblKaroseri.Text = FormatNumber(oRow.Item("HargaKaroseri"), 0)
        'lblTotalOTR.Text = FormatNumber(oRow.Item("TotalOTR"), 0)
        'Modify by Wira 20160810.. Karoseri dijumlahkan
        lblTotalOTR.Text = FormatNumber(CDbl(lblOTR.Text) + CDbl(lblKaroseri.Text))

        Dim DPKaroseriAmount As Double = CDbl(oRow.Item("DPKaroseriAmount"))

        txtDP.Text = FormatNumber(oRow.Item(15), 2)
        txtDPKaroseri.Text = FormatNumber(oRow.Item("DPKaroseriAmount"), 2)
        'txtDPPersen.Text = FormatNumber(CDec(IIf(IsNumeric(txtDP.Text), txtDP.Text, 0)) / CDbl(lblTotalOTR.Text) * 100, 2)
        txtDPPersen.Text = FormatNumber(CDec(IIf(IsNumeric(txtDP.Text), txtDP.Text, 0)) / CDbl(lblOTR.Text) * 100, 2)
        If DPKaroseriAmount > 0 Then
            txtDPPersenKaroseri.Text = FormatNumber(CDec(IIf(IsNumeric(txtDPKaroseri.Text), txtDPKaroseri.Text, 0)) / CDbl(lblKaroseri.Text) * 100, 2)
        Else
            txtDPPersenKaroseri.Text = CType(0, String)
        End If

        'lblUangMuka.Text = FormatNumber(txtDP.Text, 0)
        lblUangMuka.Text = FormatNumber(CDbl(txtDP.Text) + CDec(IIf(IsNumeric(txtDPKaroseri.Text), txtDPKaroseri.Text, 0)), 0)
        lblAdminFee.Text = FormatNumber(oRow.Item("AdminFee"), 0)
        lblOtherFee.Text = FormatNumber(oRow.Item("OtherFee"), 0)
        lblBiayaFidusia.Text = FormatNumber(CDec(oRow.Item("FiduciaFee")), 0)
        lblAssetInsurance3.Text = FormatNumber(oRow.Item("InsAssetCapitalized"), 0)
        lblNTF.Text = FormatNumber(oRow.Item(7), 2)
        lblAssetInsurance32.Text = FormatNumber(oRow.Item("PaidAmountByCust"), 0)
        lblTotalBunga.Text = FormatNumber(CDbl(oRow.Item("TotalBunga")), 0)
        txtEffectiveRate.Text = FormatNumber(CDec(oRow.Item("EffectiveRate")), 7)
        txtFlatRate.Text = FormatNumber(oRow.Item("FlatRate"), 2)
        lblAngsuranPertama.Text = FormatNumber(IIf(lblFirstInstallment.Text = "Advance", CDec(oRow.Item("InstallmentAmount").ToString.Trim), 0).ToString, 0)
        lblInsAmountToNPV.Text = FormatNumber(oRow.Item("InsAmountToNPV"), 0)

        lblNilaiKontrak.Text = FormatNumber(CDbl(lblNTF.Text) + CDbl(lblTotalBunga.Text), 0)
        cboPaymentFreq.SelectedIndex = cboPaymentFreq.Items.IndexOf(cboPaymentFreq.Items.FindByValue(oRow("PaymentFrequency").ToString))
        lblPaymentFreq.Text = cboPaymentFreq.SelectedItem.Text
        txtNumInst.Text = oRow.Item(11).ToString.Trim
        txtInstAmt.Text = FormatNumber(oRow.Item(12), 2)
        txtGracePeriod.Text = oRow.Item(13).ToString.Trim
        lblGracePeriod.Text = oRow.Item(14).ToString.Trim
        txtBiayaProvisi.Text = FormatNumber(oRow.Item("ProvisiPercent"), 2) & "% &nbsp;&nbsp;" & FormatNumber(oRow.Item("ProvisionFee"), 0)
        lblBiayaProvisi.Text = FormatNumber(oRow.Item("ProvisionFee"), 0)
        lblAdminKredit.Text = FormatNumber(oRow.Item("AdmCapitalized"), 0)
        CalculateTotalBayarPertama()
    End Sub

    Private Sub CalculateTotalBayarPertama()
        Dim um, ba, bf, at, ap, ot, bpr As Double

        um = CDbl(IIf(IsNumeric(lblUangMuka.Text), lblUangMuka.Text, 0))
        ba = CDbl(IIf(IsNumeric(lblAdminFee.Text), lblAdminFee.Text, 0))
        bf = CDbl(IIf(IsNumeric(lblBiayaFidusia.Text), lblBiayaFidusia.Text, 0))
        at = CDbl(IIf(IsNumeric(lblAssetInsurance32.Text), lblAssetInsurance32.Text, 0))
        ap = CDbl(IIf(IsNumeric(lblAngsuranPertama.Text), lblAngsuranPertama.Text, 0))
        ot = CDbl(IIf(IsNumeric(lblOtherFee.Text), lblOtherFee.Text, 0))
        bpr = CDbl(IIf(IsNumeric(lblBiayaProvisi.Text), lblBiayaProvisi.Text, 0))
        lblTotalBayarPertama.Text = FormatNumber(um + ba + bf + at + ap + ot + bpr, 0)

    End Sub

#End Region


End Class