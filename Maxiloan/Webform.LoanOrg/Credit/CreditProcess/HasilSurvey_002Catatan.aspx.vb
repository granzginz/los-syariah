﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic
Imports System.Drawing

Public Class HasilSurvey_002Catatan
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucHasilSurveyTab1 As ucHasilSurveyTab
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyTabPhone
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat

    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController
    Private m_controller As New AssetDataController
    Private m_Employee As New EmployeeController

    Dim pathFolder As String = ""
    Private time As String

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Protected Property LinkFotoRumah1() As String
        Get
            Return CType(ViewState("LinkFotoRumah1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LinkFotoRumah1") = Value
        End Set
    End Property

    Protected Property LinkFotoRumah2() As String
        Get
            Return CType(ViewState("LinkFotoRumah2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LinkFotoRumah2") = Value
        End Set
    End Property

    Protected Property LinkFotoRumah3() As String
        Get
            Return CType(ViewState("LinkFotoRumah3"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LinkFotoRumah3") = Value
        End Set
    End Property

    Protected Property LinkFotoTempatUsaha1() As String
        Get
            Return CType(ViewState("LinkFotoTempatUsaha1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LinkFotoTempatUsaha1") = Value
        End Set
    End Property

    Protected Property LinkFotoTempatUsaha2() As String
        Get
            Return CType(ViewState("LinkFotoTempatUsaha2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LinkFotoTempatUsaha2") = Value
        End Set
    End Property

    Protected Property LinkFotoTempatUsaha3() As String
        Get
            Return CType(ViewState("LinkFotoTempatUsaha3"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LinkFotoTempatUsaha3") = Value
        End Set
    End Property

    Private Property CustomerType() As String
        Get
            Return (CType(ViewState("CustomerType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerType") = Value
        End Set
    End Property

    Private Property DateEntryKYC() As Date
        Get
            Return CType(ViewState("DateEntryKYC"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryKYC") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            Me.CustomerID = Request("id")
            'Me.ProspectAppID = "-"

            'InitialPageLoad()
            'getHasilSurvey()
            'Modify by Wira 20171013
            'Ambil data hasil survey dari mobile jika add ke agreement survey
            Me.BranchID = Request("branchID")
            Me.PageAddEdit = Request("page")
            Me.ProspectAppID = Request("prospectappid")
            InitialPageLoad()
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------
            If Me.ProspectAppID <> Nothing And Me.ProspectAppID <> "-" And Request("prospectappid") <> "" And Me.PageAddEdit = "Add" Then
                getHasilMobileSurvey()
            Else
                getHasilSurvey()
            End If
        End If
    End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        Me.ApplicationID = Request("appid")

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
                btnupload.Visible = False
            End If
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("Catatan")
        ucHasilSurveyTab1.setLink()


        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With
    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            txtKunjunganRumahbertemu.Text = .KunjunganRumahBertemu
            cboKondisiRumah.SelectedIndex = cboKondisiRumah.Items.IndexOf(cboKondisiRumah.Items.FindByValue(.KondisiRumah))
            'cboKondisiKesehatan.SelectedIndex = cboKondisiKesehatan.Items.IndexOf(cboKondisiKesehatan.Items.FindByValue(.KondisiKesehatan))
            cboPerabotanRumah.SelectedIndex = cboPerabotanRumah.Items.IndexOf(cboPerabotanRumah.Items.FindByValue(.PerabotanRumah))
            rboGarasiMobil.SelectedIndex = rboGarasiMobil.Items.IndexOf(rboGarasiMobil.Items.FindByValue(.GarasiMobil))
            txtGarasiUnit.Text = .GarasiUntukBerapaUnit
            txtAlasanGarasiUnit.Text = .AlasanGarasiUntukBerapaUnit
            cboLokasiRumah.SelectedIndex = cboLokasiRumah.Items.IndexOf(cboLokasiRumah.Items.FindByValue(.LokasiRumah))
            cboJalanMasukDilalui.SelectedIndex = cboJalanMasukDilalui.Items.IndexOf(cboJalanMasukDilalui.Items.FindByValue(.JalanMasukDilalui))
            cboKondisiJlnDpnRumah.SelectedIndex = cboKondisiJlnDpnRumah.Items.IndexOf(cboKondisiJlnDpnRumah.Items.FindByValue(.KondisiJalanDepanRumah))
            cboKeadaanLingkungan.SelectedIndex = cboKeadaanLingkungan.Items.IndexOf(cboKeadaanLingkungan.Items.FindByValue(.KeadaanLingkungan))
            txtKunjunganKantorBertemu.Text = .KunjunganKantorBertemu
            cboJumlahKaryawan.SelectedIndex = cboJumlahKaryawan.Items.IndexOf(cboJumlahKaryawan.Items.FindByValue(.JumlahKaryawan))
            cboLokasiTempatUsaha.SelectedIndex = cboLokasiTempatUsaha.Items.IndexOf(cboLokasiTempatUsaha.Items.FindByValue(.LokasiTempatUsaha))
            txtAlasanLokasiTempatUsaha.Text = .Alasan
            rboAktivitasTmptUsaha.SelectedIndex = rboAktivitasTmptUsaha.Items.IndexOf(rboAktivitasTmptUsaha.Items.FindByValue(.AktivitasTempatUsaha))
            txtAlasanAktivitasTempatUsaha.Text = .AlasanAktivitasTempatUsaha
            txtTBOList.Text = .TBOList
            txtUsulanLain.Text = .UsulanLain
        End With
        cekimage()
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../CreditProcess/HasilSurvey_002Collateral.aspx?appid=" + Me.ApplicationID + "&page=" + Request("page") + "&id=" + Request("id") + "&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.HasilSurvey

        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .CustomerIDSurvey = Me.CustomerID
                .KunjunganRumahBertemu = txtKunjunganRumahbertemu.Text
                .KondisiRumah = cboKondisiRumah.SelectedValue
                .PerabotanRumah = cboPerabotanRumah.SelectedValue
                .GarasiMobil = rboGarasiMobil.SelectedValue
                .GarasiUntukBerapaUnit = txtGarasiUnit.Text
                .AlasanGarasiUntukBerapaUnit = txtAlasanGarasiUnit.Text
                .LokasiRumah = cboLokasiRumah.SelectedValue
                .JalanMasukDilalui = cboJalanMasukDilalui.SelectedValue
                .KondisiJalanDepanRumah = cboKondisiJlnDpnRumah.SelectedValue
                .KeadaanLingkungan = cboKeadaanLingkungan.SelectedValue
                .KunjunganKantorBertemu = txtKunjunganKantorBertemu.Text
                .JumlahKaryawan = cboJumlahKaryawan.SelectedValue
                .LokasiTempatUsaha = cboLokasiTempatUsaha.SelectedValue
                .Alasan = txtAlasanLokasiTempatUsaha.Text
                .AktivitasTempatUsaha = rboAktivitasTmptUsaha.SelectedValue
                .AlasanAktivitasTempatUsaha = txtAlasanAktivitasTempatUsaha.Text
                .TBOList = txtTBOList.Text
                .UsulanLain = txtUsulanLain.Text
            End With

            oController.HasilSurvey002CatatanSave(oPar)

            Dim errMsg As String

            ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
            ucHasilSurveyTab1.selectedTab("Catatan")
            ucHasilSurveyTab1.setLink()
            InitialPageLoad()
            getHasilSurvey()

            If errMsg = String.Empty Then
                ShowMessage(lblMessage, "Data saved!", False)
            End If

            CekTabAplikasiSurvey()
            If Me.ProspectAppID <> "-" And Me.DateEntryKYC = "1/1/1900" Then
                Response.Redirect("HasilSurvey_002KYC.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Add&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim & "&branchID=" & Me.BranchID.Trim & "&ActivityDateStart=" & Me.ActivityDateStart)
            Else
                Response.Redirect("HasilSurvey_002KYC.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Upload image eksekutor"
    Private Sub cekimage()
        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah1.jpg") Then
            imgdenahlokasirumah1.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah1.jpg"))
        Else
            imgdenahlokasirumah1.ImageUrl = ResolveClientUrl("../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah2.jpg") Then
            imgdenahlokasirumah2.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah2.jpg"))
        Else
            imgdenahlokasirumah2.ImageUrl = ResolveClientUrl("../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah3.jpg") Then
            imgdenahlokasirumah3.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah3.jpg"))
        Else
            imgdenahlokasirumah3.ImageUrl = ResolveClientUrl("../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah4.jpg") Then
            imgdenahlokasirumah4.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah4.jpg"))
        Else
            imgdenahlokasirumah4.ImageUrl = ResolveClientUrl("../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("HasilSurvey", Me.CustomerID) + "LampiranDenahLokasiRumah5.jpg") Then
            imgdenahlokasirumah5.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/LampiranDenahLokasiRumah5.jpg"))
        Else
            imgdenahlokasirumah5.ImageUrl = ResolveClientUrl("../../../xml/NoFoto.png")
        End If

        imgdenahlokasirumah1.Height = 200
        imgdenahlokasirumah1.Width = 300
        imgdenahlokasirumah2.Height = 200
        imgdenahlokasirumah2.Width = 300
        imgdenahlokasirumah3.Height = 200
        imgdenahlokasirumah3.Width = 300
        imgdenahlokasirumah4.Height = 200
        imgdenahlokasirumah4.Width = 300
        imgdenahlokasirumah5.Height = 200
        imgdenahlokasirumah5.Width = 300
    End Sub

    Private Function pathFile(ByVal Group As String, ByVal ExecutorID As String) As String
        Dim strDirectory As String = ""
        'strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Function getpathFolder() As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function

    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function

    Private Sub btnupload_Click(sender As Object, e As System.EventArgs) Handles btnupload.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""
        If upldenahlokasirumah1.HasFile Then
            If upldenahlokasirumah1.PostedFile.ContentType = "image/jpeg" Then
                FileName = "LampiranDenahLokasiRumah1"
                strExtension = Path.GetExtension(upldenahlokasirumah1.PostedFile.FileName)
                upldenahlokasirumah1.PostedFile.SaveAs(pathFile("HasilSurvey", Me.CustomerID) + FileName + strExtension)
            End If
        End If
        If upldenahlokasirumah2.HasFile Then
            If upldenahlokasirumah2.PostedFile.ContentType = "image/jpeg" Then
                FileName = "LampiranDenahLokasiRumah2"
                strExtension = Path.GetExtension(upldenahlokasirumah2.PostedFile.FileName)
                upldenahlokasirumah2.PostedFile.SaveAs(pathFile("HasilSurvey", Me.CustomerID) + FileName + strExtension)
            End If
        End If
        If upldenahlokasirumah3.HasFile Then
            If upldenahlokasirumah3.PostedFile.ContentType = "image/jpeg" Then
                FileName = "LampiranDenahLokasiRumah3"
                strExtension = Path.GetExtension(upldenahlokasirumah3.PostedFile.FileName)
                upldenahlokasirumah3.PostedFile.SaveAs(pathFile("HasilSurvey", Me.CustomerID) + FileName + strExtension)
            End If
        End If
        If upldenahlokasirumah4.HasFile Then
            If upldenahlokasirumah4.PostedFile.ContentType = "image/jpeg" Then
                FileName = "LampiranDenahLokasiRumah4"
                strExtension = Path.GetExtension(upldenahlokasirumah4.PostedFile.FileName)
                upldenahlokasirumah4.PostedFile.SaveAs(pathFile("HasilSurvey", Me.CustomerID) + FileName + strExtension)
            End If
        End If
        If upldenahlokasirumah5.HasFile Then
            If upldenahlokasirumah5.PostedFile.ContentType = "image/jpeg" Then
                FileName = "LampiranDenahLokasiRumah5"
                strExtension = Path.GetExtension(upldenahlokasirumah5.PostedFile.FileName)
                upldenahlokasirumah5.PostedFile.SaveAs(pathFile("HasilSurvey", Me.CustomerID) + FileName + strExtension)
            End If
        End If
        cekimage()
    End Sub
#End Region

    Sub getHasilMobileSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilMobileSurvey(oPar)

        With oPar
            Me.ProspectAppID = .ProspectAppID
            txtKunjunganRumahbertemu.Text = .KunjunganRumahBertemu
            cboKondisiRumah.SelectedIndex = cboKondisiRumah.Items.IndexOf(cboKondisiRumah.Items.FindByValue(.KondisiRumah))
            'cboKondisiKesehatan.SelectedIndex = cboKondisiKesehatan.Items.IndexOf(cboKondisiKesehatan.Items.FindByValue(.KondisiKesehatan))
            cboPerabotanRumah.SelectedIndex = cboPerabotanRumah.Items.IndexOf(cboPerabotanRumah.Items.FindByValue(.PerabotanRumah))
            rboGarasiMobil.SelectedIndex = rboGarasiMobil.Items.IndexOf(rboGarasiMobil.Items.FindByValue(.GarasiMobil))
            txtGarasiUnit.Text = .GarasiUntukBerapaUnit
            txtAlasanGarasiUnit.Text = .AlasanGarasiUntukBerapaUnit
            cboLokasiRumah.SelectedIndex = cboLokasiRumah.Items.IndexOf(cboLokasiRumah.Items.FindByValue(.LokasiRumah))
            cboJalanMasukDilalui.SelectedIndex = cboJalanMasukDilalui.Items.IndexOf(cboJalanMasukDilalui.Items.FindByValue(.JalanMasukDilalui))
            cboKondisiJlnDpnRumah.SelectedIndex = cboKondisiJlnDpnRumah.Items.IndexOf(cboKondisiJlnDpnRumah.Items.FindByValue(.KondisiJalanDepanRumah))
            cboKeadaanLingkungan.SelectedIndex = cboKeadaanLingkungan.Items.IndexOf(cboKeadaanLingkungan.Items.FindByValue(.KeadaanLingkungan))
            txtKunjunganKantorBertemu.Text = .KunjunganKantorBertemu
            cboJumlahKaryawan.SelectedIndex = cboJumlahKaryawan.Items.IndexOf(cboJumlahKaryawan.Items.FindByValue(.JumlahKaryawan))
            cboLokasiTempatUsaha.SelectedIndex = cboLokasiTempatUsaha.Items.IndexOf(cboLokasiTempatUsaha.Items.FindByValue(.LokasiTempatUsaha))
            txtAlasanLokasiTempatUsaha.Text = .Alasan
            rboAktivitasTmptUsaha.SelectedIndex = rboAktivitasTmptUsaha.Items.IndexOf(rboAktivitasTmptUsaha.Items.FindByValue(.AktivitasTempatUsaha))
            txtAlasanAktivitasTempatUsaha.Text = .AlasanAktivitasTempatUsaha
            txtTBOList.Text = .TBOList
            txtUsulanLain.Text = .UsulanLain
            Me.LinkFotoRumah1 = .LinkFotoRumah1
            Me.LinkFotoRumah2 = .LinkFotoRumah2
            Me.LinkFotoRumah3 = .LinkFotoRumah3
            Me.LinkFotoTempatUsaha1 = .LinkFotoTempatUsaha1
            Me.LinkFotoTempatUsaha2 = .LinkFotoTempatUsaha2
            Me.LinkFotoTempatUsaha3 = .LinkFotoTempatUsaha3
            Me.CustomerType = .CustomerType
        End With

        If CustomerType.Trim = "P" Then
            SaveImageFromMobile(Me.LinkFotoRumah1.ToString, "LampiranDenahLokasiRumah1")
            SaveImageFromMobile(Me.LinkFotoRumah2.ToString, "LampiranDenahLokasiRumah2")
            SaveImageFromMobile(Me.LinkFotoRumah3.ToString, "LampiranDenahLokasiRumah3")
        Else
            SaveImageFromMobile(Me.LinkFotoTempatUsaha1.ToString, "LampiranDenahLokasiRumah1")
            SaveImageFromMobile(Me.LinkFotoTempatUsaha2.ToString, "LampiranDenahLokasiRumah2")
            SaveImageFromMobile(Me.LinkFotoTempatUsaha3.ToString, "LampiranDenahLokasiRumah3")
        End If
        cekimage()
    End Sub

    Public Function SaveImageFromMobile(ImgStr As String, ImgName As String) As Boolean

        Dim path__1 As [String] = HttpContext.Current.Server.MapPath("~/ImageStorage")
        'Path
        'Check if directory exist
        If Not System.IO.Directory.Exists(path__1) Then
            'Create directory if it doesn't exist
            System.IO.Directory.CreateDirectory(path__1)
        End If

        Dim imageName As String = ImgName & Convert.ToString(".jpg")

        'set the image path
        Dim imgPath As String = Path.Combine(path__1, imageName)
        Dim imgPathx As String = pathFile("HasilSurvey", Me.CustomerID) + imageName
        Dim imageBytes As Byte() = Convert.FromBase64String(ImgStr)

        'File.WriteAllBytes(imgPath, imageBytes)
        File.WriteAllBytes(imgPathx, imageBytes)
        Return True
    End Function

    'Public Sub MyCapture(filename As String)
    '    Dim screenshot As New Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, True)
    '    screenshot.ReadPixels(New Rect(0, 0, Screen.width, Screen.height), 0, 0)
    '    screenshot.Apply()

    '    Dim newScreenshot As Texture2D = ScaleTexture(screenshot, 1024, 576)

    '    Dim bytes As Byte() = newScreenshot.EncodeToPNG()
    '    File.WriteAllBytes(filename, bytes)
    'End Sub
    'Private Function ScaleTexture(source As Texture2D, targetWidth As Integer, targetHeight As Integer) As Texture2D
    '    Dim result As New Texture2D(targetWidth, targetHeight, source.format, True)
    '    Dim rpixels As Color() = result.GetPixels(0)
    '    Dim incX As Single = (CSng(1) / source.width) * (CSng(source.width) / targetWidth)
    '    Dim incY As Single = (CSng(1) / source.height) * (CSng(source.height) / targetHeight)
    '    For px As Integer = 0 To rpixels.Length - 1
    '        rpixels(px) = source.GetPixelBilinear(incX * (CSng(px) Mod targetWidth), incY * CSng(Mathf.Floor(px \ targetWidth)))
    '    Next
    '    result.SetPixels(rpixels, 0)
    '    result.Apply()
    '    Return result
    'End Function
    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryKYC = .DateEntryKYC
        End With
    End Sub
End Class