﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreditScoring.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CreditScoring" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %> 
<%@ Register Src="../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Credit Scoring</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
<%--        function OpenWinApplication(pApplicationID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'Application', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }--%>

<%--        function OpenWinSupplier(pSupplierID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/ViewSupplier.aspx?style=' + pStyle + '&SupplierID=' + pSupplierID, 'Supplier', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }--%>

<%--        function OpenWinEmployee(pBranchID, pAOID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pAOID, 'Employee', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }--%>

<%--        function OpenWinCustomer(pID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }--%>

        function OpenWinApplication(pApplicationID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinSupplier(pSupplierID, pStyle) {
            window.open(ServerName + App + '/Webform.SalesMarketing/ViewSupplier.aspx?style=' + pStyle + '&SupplierID=' + pSupplierID, 'Supplier', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinEmployee(pBranchID, pAOID, pStyle) {
            window.open(ServerName + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pAOID, 'Employee', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function fBack() {
            history.back(-1);
            return false;
        }
        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini  ?")) {
                return true;
            }
            else {
                return false;
            }
        }
//        function click() {
//            if (event.button == 2) {
//                alert('Anda tidak Berhak');
//            }
//        }
//        document.onmousedown = click			
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PROSES SCORING PEMBIAYAAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server" Width="100%">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR APLIKASI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0" CellPadding="3" CellSpacing="1" DataKeyField="ApplicationID"
                    CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAction" runat="server" CommandName="View" Text='SCORING' CausesValidation="false" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO APLIKASI" SortExpression="ApplicationID">
                            <ItemStyle HorizontalAlign="left" Width="12%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>' NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER" SortExpression="CustomerName">
                            <ItemStyle HorizontalAlign="left" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>' NavigateUrl='' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SUPPLIER" SortExpression="SupplierName">
                            <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hynSupplierName" runat="server" Text='<%#container.dataitem("SupplierName")%>' NavigateUrl='' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CMO" SortExpression="EmployeeName">
                            <ItemStyle HorizontalAlign="left" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hynEmployeeName" runat="server" Text='<%#container.dataitem("EmployeeName")%>' NavigateUrl='' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL APLIKASI" SortExpression="NewApplicationDate">
                            <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNewApplicationDate" runat="server" Text='<%#container.dataitem("NewApplicationDate")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID CUSTOMER" Visible="False">
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerID" runat="server" Text='<%#container.dataitem("CustomerID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="AOID" Visible="False">
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAOID" runat="server" Text='<%#container.dataitem("AOID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SupplierID" Visible="False">
                            <ItemStyle HorizontalAlign="Center" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblSupplierID" runat="server" Text='<%#container.dataitem("SupplierID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <uc2:ucGridNav id="GridNavigator" runat="server"/>  
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI SCORING PEMBIAYAAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="EmployeeName">Nama CMO</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Aplikasi
                </label>
                <asp:TextBox runat="server" ID="txtApplicationDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" TargetControlID="txtApplicationDate" Format="dd/MM/yyyy" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlScoring" runat="server" Width="100%">
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label> Nama Customer </label>
                    <asp:HyperLink ID="hplCustomerName_Scoring" runat="server" />
                </div>
                <div class="form_right">
                    <label>
                        No Aplikasi
                    </label>
                    <asp:HyperLink ID="hplApplicationID_Scoring" runat="server"></asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <asp:Button ID="btnCreditScoring" runat="server" CausesValidation="False" Text="Scoring Pembiayaan" CssClass="small buttongo blue" />
            </div>
        </div>
        <asp:Panel ID="pnlView" runat="server" Visible="False">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        HASIL SCORING PEMBIAYAAN</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgView" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderWidth="0" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No">
                                <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblNumber" runat="server" Text="0"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            
                            <asp:TemplateColumn HeaderText="KOMPONEN SCORING">
                                <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#container.dataitem("ScoreDescription")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KOMPONEN CONTENT">
                                <ItemStyle HorizontalAlign="left" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblValue" runat="server" Text='<%#container.dataitem("QueryResult")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NILAI SCORE">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblScoreValue" runat="server" Text='<%#container.dataitem("ScoreValue")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NILAI SCORE ABSOLUTE">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblScoreAbsolute" runat="server" Text='<%#container.dataitem("ScoreAbsolute")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="STATUS SCORE">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblWeight" runat="server" Text='<%#container.dataitem("ScoreStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID" Visible="False">
                                <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%#container.dataitem("ScoreComponentID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                     <Label>Cut Off Score </Label> 
                     <asp:Label ID="lblGrade" runat="server"></asp:Label>
                 </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                     <Label>Score Pembiayaan</Label> 
                      <asp:Label ID="lblResult" runat="server"  ></asp:Label>   
                 </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                     <Label>Absolute Score </Label> 
                      <asp:Label ID="lblAbsoluteScore" runat="server"  ></asp:Label>   
                    <asp:label id="lblTingkatResiko" runat="server"></asp:label>
                 </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <Label>Decision</Label> 
                    <asp:Label ID="lblDecision" runat="server"></asp:Label>
                </div>
            </div>

        </asp:Panel>
        <div class="form_button"> 
          
            <asp:Button ID="btnSave" runat="server" CausesValidation="False" Text="Approve" CssClass="small button blue" Visible ="false" />
            <asp:Button ID="btnReject" runat="server" CausesValidation="False" Text="Reject" CssClass="small button blue" Visible ="false" /> 
            <asp:Button ID="btnProceed" runat="server" CausesValidation="False" Text="Proceed" CssClass="small button green" Visible ="false" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
        </div>
    </asp:Panel>
        <asp:Panel runat="server" ID="pnlProceed">
            <div class="form_box_header">
                <div class="form_single">
                        <h5>
                            Approval Scoring</h5>
                    </div>
            </div> 
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> Di Approve Oleh</label>
                        <asp:DropDownList ID="cboApprovedBy" runat="server"/>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic" ControlToValidate="cboApprovedBy" ErrorMessage="Harap diisi di Approve Oleh" 
                            InitialValue="0" CssClass="validator_general" enabled="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req"> Note</label> 
                        <asp:TextBox ID="txtNoteApprove" runat="server" CssClass="multiline_textbox multiline_new" TextMode="MultiLine"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvtxtNoteApprove" runat="server" Display="Dynamic" ControlToValidate="txtNoteApprove" ErrorMessage="Harap diisi" 
                            CssClass="validator_general" enabled="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                <asp:Button ID="SaveProceed" runat="server" CausesValidation="false" Text="Save" CssClass="small button green"></asp:Button> 
                </div>
        </asp:Panel>
    </form>
</body>
</html>
