﻿Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class ApprovalBM_002
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New RCAController
    Private m_Appcontroller As New ApplicationController
    Private time As String
#End Region

#Region "Property"
    Property NTF() As Double
        Get
            Return CDbl(ViewState("NTF"))
        End Get
        Set(ByVal Value As Double)
            ViewState("NTF") = Value
        End Set
    End Property
    Property SchemeID() As String
        Get
            Return ViewState("SchemeID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SchemeID") = Value
        End Set
    End Property
    Property AgreementDate() As String
        Get
            Return ViewState("AgreementDate").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementDate") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return ViewState("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateStart = Me.BusinessDate + " " + time

        If Not Page.IsPostBack Then
            lblMessage.Text = ""
            lblMessage.Visible = False
            'drdContract.Attributes.Add("onChange", "javascript:ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);")
            btnSave.Attributes.Add("OnClick", "return checksubmit();")

            Me.CustomerName = Request("Name")
            Me.CustomerID = Request("CustomerID")
            Me.ApplicationID = Request("App")
            Me.SchemeID = Request("Scheme")
            Me.NTF = CDbl(Request("NTF"))
            lblApplicationID.NavigateUrl = String.Format("javascript:OpenWinApplication('{0}','AccAcq')", Me.ApplicationID.Trim)
            lblCustName.NavigateUrl = String.Format("javascript:OpenWinCustomer('{0}','AccAcq')", CustomerID.Trim)

            lblCustName.Text = Me.CustomerName
            lblApplicationID.Text = Me.ApplicationID
            lblNPM.Text = FormatNumber(NTF.ToString, 0)
            'Bind()
            'bindComboCompany()
            'Response.Write(GenerateScript(getComboContract, getComboBatchDate))

        End If
        'LabelValidationFalse()
    End Sub
    'Private Sub btnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReject.Click 
    '    Try
    '        m_controller.ApprovalBMSave(getParam(), False)
    '        Response.Redirect("ApprovalBM.aspx")
    '    Catch exp As Exception
    '        ShowMessage(lblMessage, exp.Message, True)
    '    End Try
    'End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try
            m_controller.ApprovalBMSave(getParam())
            'Modifyby Wira 20171017
            ActivityLog()
            Response.Redirect("ApprovalBM.aspx")
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
    Private Function getParam() As Parameter.RCA
        Dim oRCA As New Parameter.RCA

        With oRCA
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .BusinessDate = Me.BusinessDate
            .AppID = Me.ApplicationID
            .SchemeID = Me.SchemeID
            .Notes = txtRecommendation.Text.Trim
            .Argumentasi = "" 'txtArgumentasi.Text.Trim
            .RefundAmount = Me.NTF
            .LoginId = Me.Loginid
            .RequestBy = "" 'cboApprovedBy.SelectedValue
            .AgreementNo = "-" ' txtAgreementNo.Text.Trim
            .strConnection = GetConnectionString()
            .FundingCoyId = "" 'drdCompany.SelectedItem.Value
            .FundingContractNo = "" 'tempChild.Value.Trim
            .FundingBatchNo = "" 'tempChild2.Value.Trim
        End With

        Return oRCA
    End Function

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("ApprovalBM.aspx")
    End Sub

#Region "Activity Log"
    Sub ActivityLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Application
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ApplicationID = Me.ApplicationID.Trim
        oApplication.ActivityType = "ABM"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 12

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Application

        oReturn = m_Appcontroller.ActivityLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub
#End Region
End Class