﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="POExtendList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.POExtendList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POExtendList</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var x = screen.width;
        var y = screen.height - 100;		

        function OpenWinSupplier(pSupplierID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/ViewSupplier.aspx?style=' + pStyle + '&SupplierID=' + pSupplierID, 'Supplier', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinAgreementNo(pApplicationID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?ApplicationID=' + pApplicationID + '&style=' + pStyle, 'AgreementNo', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinPOViewExtend(pBranchID, pApplicationID, pPONo, pSupplierID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/POViewforExtend.aspx?BranchID=' + pBranchID + '&ApplicationID=' + pApplicationID + '&PONo=' + pPONo + '&SupplierID=' + pSupplierID + '&style=' + pStyle, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }			
    </script>
</head>
<body>
    <table cellspacing="0" cellpadding="0" align="center" width="95%" border="0">
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    <table cellspacing="1" cellpadding="3" width="100%" align="center" border="0">
        <tr valign="top" align="center">
            <td>
                <form id="form1" runat="server">
                <div id="lblTrans" runat="server">
                </div>
                <table id="Table2" cellspacing="0" cellpadding="0" width="95%" align="center"
                    border="0">
                    <tr class="trtopikuning">
                        <td class="tdtopi" align="center">
                            DAFTAR PERPANJANGAN PURCHASE ORDER
                        </td>
                    </tr>
                </table>
                <!-- Datagrid goes here-->
                <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" align="center"
                    border="0">
                    <tr>
                        <td>
                            <asp:DataGrid ID="dtgPOExtend" runat="server" Width="100%" BackColor="White" BorderStyle="None"
                                BorderColor="#CCCCCC" OnSortCommand="Sorting" AutoGenerateColumns="False" AllowPaging="True"
                                HorizontalAlign="Center" AllowSorting="True" DataKeyField="PONo">
                                <SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
                                
                                <ItemStyle CssClass="item_grid"/>
                                <HeaderStyle CssClass="tdjudul"></HeaderStyle>
                                <FooterStyle Font-Bold="True" Height="20px" CssClass="tdjudul"></FooterStyle>
                                <Columns>
                                    <asp:TemplateColumn SortExpression="PERFORM" HeaderText="PILIH">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton CommandName="Extend" runat="server" ID="lnkExtend" Text="EXTEND"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="PONo" HeaderText="NO PO">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynPONo" runat="server" Text='<%# Container.dataitem("PONo")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblApplicationID" runat="server" Visible="False" Text='<%# Container.dataitem("ApplicationID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%# Container.dataitem("CustomerName")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynAgreementNo" runat="server" Text='<%# Container.dataitem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblAgreementNo" Visible="False" runat="server" Text='<%# Container.dataitem("AgreementNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SupplierName" HeaderText="NAMA SUPPLIER">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hynSupplierName" runat="server" Text='<%# Container.dataitem("SupplierName")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="PODate" SortExpression="PODATE" HeaderText="TGL PO"
                                        DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="POExpiredDate" SortExpression="PoExpiredDate" HeaderText="PO EXP"
                                        DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="StrRemain" SortExpression="StrRemain" HeaderText="SISA (HARI)">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="POExtendCounter" SortExpression="POExtendCounter" HeaderText="EXTEND">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="POTotal" SortExpression="POTotal" HeaderText="NILAI PO"
                                        DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="POExpiredDate" HeaderText="POExpiredDate" Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPOExpiredDate" runat="server" Text='<%# Container.dataitem("POExpiredDate")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="POExtendCounter" HeaderText="POExtendCounter"
                                        Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPOExtendCounter" runat="server" Text='<%# Container.dataitem("POExtendCounter")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="PoOrgExpDate" HeaderText="Agreement" Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPOOriginalExpiredDate" runat="server" Text='<%# Container.dataitem("POOriginalExpiredDate")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="POTotal" HeaderText="POTotal" Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPOTotal" runat="server" Text='<%# Container.dataitem("POTotal")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="PODate" HeaderText="PODate" Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPODate" runat="server" Text='<%# Container.dataitem("PODate")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="POExtend" HeaderText="Agreement" Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPOExtendDays" runat="server" Text='<%# Container.dataitem("POExtendDays")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Remain" HeaderText="Remain" Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRemain" runat="server" Text='<%# Container.dataitem("Remain")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="CustomerID" HeaderText="CustomerID" Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerID" runat="server" Text='<%# Container.dataitem("CustomerID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="SupplierID" HeaderText="SupplierID" Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSupplierID" runat="server" Text='<%# Container.dataitem("SupplierID")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AccountPayableNo" HeaderText="AccountPayableNo"
                                        Visible="false">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountPayableNo" runat="server" Text='<%# Container.dataitem("AccountPayableNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <PagerStyle Visible="False"></PagerStyle>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                <table class="nav_tbl" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td class="nav_tbl_td1">
                            <table class="nav_command_tbl" cellspacing="0" cellpadding="0">
                                <tr>
                                </tr>
                            </table>
                        </td>
                        <td class="nav_tbl_td2">
                            <table class="nav_page_tbl" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                        </asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                        </asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                        </asp:ImageButton>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                        </asp:ImageButton>
                                    </td>
                                    <td>
                                        Page&nbsp;
                                        <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text" >1</asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                            EnableViewState="False"/>
                                    </td>
                                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtpage" MinimumValue="1"
                                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                                         CssClass="validator_general"></asp:RangeValidator>
                                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtpage"
                                        ErrorMessage="No Halaman salah"  
                                         Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator></tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="nav_totpage">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                        </td>
                    </tr>
                </table>
                <table id="Table1" height="20" cellspacing="0" cellpadding="0" width="95%" align="center"
                    border="0">
                    <tr class="trtopikuning">
                        <td class="tdtopi" align="center">
                            CARI PURCHASE ORDER UNTUK PERPANJANGAN
                        </td>
                    </tr>
                </table>
                <table id="Table12" cellspacing="1" cellpadding="3" width="95%" align="center" border="0">
                    <tr>
                        <td class=" tdgenap" width="30%">
                            Sudah jatuh Tempo
                        </td>
                        <td class="tdganjil" width="70%">
                            <asp:RadioButtonList ID="isExpired" runat="server" Width="96px" RepeatDirection="Horizontal"
                                Height="12px">
                                <asp:ListItem Value="1" Selected="True">Yes</asp:ListItem>
                                <asp:ListItem Value="0">No</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap" width="30%">
                            Cari Berdasarkan
                        </td>
                        <td class="tdganjil" width="70%">
                            <asp:DropDownList ID="cboSearch" runat="server">
                                <asp:ListItem Value="PONo">No Purchase Order</asp:ListItem>
                                <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;<asp:TextBox ID="txtSearch" runat="server" Width="130" ></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table id="Table4" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue"
                                CausesValidation="True" />
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                                CausesValidation="True"></asp:ImageButton>
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
    </table>
</body>
</html>
