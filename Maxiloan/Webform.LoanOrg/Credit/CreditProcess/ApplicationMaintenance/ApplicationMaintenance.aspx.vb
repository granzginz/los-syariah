﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class ApplicationMaintenance
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ApplicationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property Err() As String
        Get
            Return CType(viewstate("Err"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Err") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(viewstate("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property CustID() As String
        Get
            Return CType(viewstate("CustID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustID") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Private Property Type() As String
        Get
            Return CType(viewstate("Type"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Type") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property ProductID() As String
        Get
            Return CType(viewstate("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductID") = Value
        End Set
    End Property
    Private Property ProductOfferingID() As String
        Get
            Return CType(viewstate("ProductOfferingID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProductOfferingID") = Value
        End Set
    End Property
    Private Property InsAssetPaidBy() As String
        Get
            Return CType(viewstate("InsAssetPaidBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsAssetPaidBy") = Value
        End Set
    End Property
    Private Property InsAssetInsuredBy() As String
        Get
            Return CType(viewstate("InsAssetInsuredBy"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsAssetInsuredBy") = Value
        End Set
    End Property
    Private Property SupplierGroupID() As String
        Get
            Return CType(viewstate("SupplierGroupID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierGroupID") = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return CType(viewstate("InterestType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InterestType") = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return CType(viewstate("InstallmentScheme"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InstallmentScheme") = Value
        End Set
    End Property
    Public Property NumberInstallment() As Double
        Get
            Return CType(viewstate("NumberInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("NumberInstallment") = Value
        End Set
    End Property
    Public Property NumberAgreementAsset() As Double
        Get
            Return CType(viewstate("NumberAgreementAsset"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("NumberAgreementAsset") = Value
        End Set
    End Property
    Public Property AssetID() As String
        Get
            Return CType(viewstate("AssetID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetID") = Value
        End Set
    End Property
    Public Property DateEntryApplicationData() As Date
        Get
            Return CType(viewstate("DateEntryApplicationData"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("DateEntryApplicationData") = Value
        End Set
    End Property
    Public Property DateEntryAssetData() As Date
        Get
            Return CType(viewstate("DateEntryAssetData"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("DateEntryAssetData") = Value
        End Set
    End Property
    Public Property DateEntryInsuranceData() As Date
        Get
            Return CType(viewstate("DateEntryInsuranceData"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("DateEntryInsuranceData") = Value
        End Set
    End Property
    Public Property DateEntryFinancialData() As Date
        Get
            Return CType(viewstate("DateEntryFinancialData"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("DateEntryFinancialData") = Value
        End Set
    End Property
    Public Property DateEntryIncentiveData() As Date
        Get
            Return CType(viewstate("DateEntryIncentiveData"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("DateEntryIncentiveData") = Value
        End Set
    End Property
    Public Property ApplicationModule() As String
        Get
            Return CType(ViewState("ApplicationModule"), String)
        End Get
        Set(value As String)
            ViewState("ApplicationModule") = value
        End Set
    End Property

#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        lblMessage.Text = ""
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            If Request.QueryString("Err") <> "" Then
                Me.Err = Request.QueryString("Err")                
                ShowMessage(lblMessage, Me.Err, True)
            End If
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                txtGoPage.Text = "1"
                If CheckForm(Me.Loginid, "EditApplication", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If

                If Request("cond") <> "" Then
                    Me.CmdWhere = " ApplicationStep in ('APK') and " & Request("cond")
                Else
                    Me.CmdWhere = " ApplicationStep in ('APK') "
                End If
                Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                Me.Sort = "ApplicationID ASC"
                BindGrid()
                InitialPanel()
            Else
                NotAuthorized()
            End If
        End If
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlModify.Visible = False
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
#End Region
#Region "BindGrid"
    Sub BindGrid()
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Application
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = m_controller.GetApplicationMaintenance(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region
#Region "LinkTo"
    Function LinkToOpenWinApplication2(ByVal strApplicationID As String, ByVal strCustID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication2('" & strApplicationID & "','" & strCustID & "','" & strStyle & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

    Function LinkToSupplier(ByVal strStyle As String, ByVal strSupplierID As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function

    Function LinkToEmployee(ByVal strStyle As String, ByVal strBranchID As String, ByVal strAOID As String) As String
        Return "javascript:OpenWinEmployee('" & strStyle & "','" & strBranchID & "','" & strAOID & "')"
    End Function
#End Region
#Region "Modify"
    Sub Modify()

        Dim Null As Date
        Null = CType("1/1/1900", Date)

        pnlList.Visible = False
        pnlModify.Visible = True
        'Back for Asset, Insurance, and Financial Edit

        hplApplication.NavigateUrl = "EditApplication.aspx?ApplicationID=" & Me.ApplicationID & "&BranchID=" & Me.BranchID & "&name=" & Me.Name & "&type=" & Me.Type & "&ProductID=" & Me.ProductID & "&ProductOfferingID=" & Me.ProductOfferingID & "&CustomerID=" & Me.CustID & ""

        '======================= Link Asset Data ======================================
        'beri link bila dateentryassetdata<> null or (dateentryassetdata=null and dateentryapplication <> null)

        If Me.DateEntryAssetData <> Null Then
            hplAsset.NavigateUrl = "EditAssetData.aspx?ApplicationID=" & Me.ApplicationID
            'If Me.NumberAgreementAsset > 0 Then
            '    LinkEditAsset = "EditAssetData.aspx?ApplicationID=" & Me.ApplicationID
            'Else
            '    LinkEditAsset = ""
            'End If
        Else
            hplAsset.NavigateUrl = ""
            hplInsurance.NavigateUrl = ""
            hplFinancial.NavigateUrl = ""
            hplIncentive.NavigateUrl = ""
            Exit Sub
        End If


        '====================== Link Insurance ===========================
        'beri link bila datentryinsurancedata=null dan dateentryassetdata <> null

        If Me.DateEntryInsuranceData <> Null Then
            If Me.InsAssetInsuredBy = "CO" And Me.InsAssetPaidBy = "CU" Then
                hplInsurance.NavigateUrl = "../New/EditInsurance.aspx?PageSource=CompanyCustomer&Applicationid='" & Me.ApplicationID & "'&SupplierGroupID='" & Me.SupplierGroupID & "'"
            End If
            If Me.InsAssetInsuredBy = "CO" And Me.InsAssetPaidBy = "AC" Then
                hplInsurance.NavigateUrl = "../New/EditInsurance.aspx?PageSource=CompanyAtCost&Applicationid='" & Me.ApplicationID & "'&SupplierGroupID='" & Me.SupplierGroupID & "'"
            End If
            If Me.InsAssetInsuredBy = "CU" And Me.InsAssetPaidBy = "CU" Then
                hplInsurance.NavigateUrl = "EditAppInsuranceByCust.aspx?PageSource=CustToCust&Applicationid='" & Me.ApplicationID & "'&SupplierGroupID='" & Me.SupplierGroupID & "'"
            End If
        Else
            hplInsurance.NavigateUrl = ""
            hplFinancial.NavigateUrl = ""
            hplIncentive.NavigateUrl = ""
            Exit Sub
        End If

        '===================== Link Financial ====================================
        'beri link bila dateentryfinancialdata=null dan dateentryinsurancedata <> null

        Context.Trace.Write("Me.NumberInstallment" & Me.NumberInstallment)

        If Me.DateEntryFinancialData <> Null Then
            hplFinancial.NavigateUrl = "EditFinancialData_002.aspx"
            'If Me.NumberInstallment > 0 Then
            '    LinkEditFinancial = "EditFinancialData_002.aspx"
            'Else
            '    LinkEditFinancial = ""
            'End If
        Else
            hplFinancial.NavigateUrl = ""
            hplIncentive.NavigateUrl = ""
            Exit Sub
        End If
        '===================== Link Incentive ====================================
        'beri link bila dateentryfinancialdata=null dan dateentryinsurancedata <> null

        Context.Trace.Write("Me.NumberInstallment" & Me.NumberInstallment)

        If Me.DateEntryIncentiveData <> Null Then
            hplIncentive.NavigateUrl = "EditIncentiveData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&CustomerName=" & Me.Name.Trim & "&CustomerID=" & Me.CustID & ""
            'If Me.NumberInstallment > 0 Then
            '    LinkEditFinancial = "EditFinancialData_002.aspx"
            'Else
            '    LinkEditFinancial = ""
            'End If
        Else
            hplIncentive.NavigateUrl = ""
            Exit Sub
        End If

    End Sub
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        InitialPanel()
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Modify" Then
            If CheckFeature(Me.Loginid, "EditApplication", "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            Me.ApplicationID = Replace(CType(e.Item.FindControl("lnkApplication"), HyperLink).Text.Trim, ",", "")
            Me.Type = CType(e.Item.FindControl("lblType"), Label).Text.Trim
            Me.CustID = CType(e.Item.FindControl("lblCustomerID"), Label).Text.Trim
            Me.Name = CType(e.Item.FindControl("lnkName"), HyperLink).Text.Trim
            Me.BranchID = Me.sesBranchId.Replace("'", "")
            Me.ProductID = CType(e.Item.FindControl("lblProductID"), Label).Text.Trim
            Me.ProductOfferingID = CType(e.Item.FindControl("lblProductOfferingID"), Label).Text.Trim
            Me.ApplicationModule = CType(e.Item.FindControl("lblApplicationModule"), Label).Text.Trim

            'lblApplicationID.Text = Me.ApplicationID
            'lblApplicationID.NavigateUrl = LinkTo(Me.ApplicationID, "AccAcq")

            'lblCustomerName.Text = Me.Name
            'lblCustomerName.NavigateUrl = LinkToCustomer(Me.CustID, "AccAcq")

            ''== ambil data insurance untuk applicationid tertentu
            ''SpName = SpInsEditInsuranceSelect ; Parameter ApplicationID
            'Dim ControllerInsurance As New NewAppInsuranceByCompanyController
            'Dim EntitiesInsurance As New Parameter.NewAppInsuranceByCompany
            'With EntitiesInsurance
            '    .ApplicationID = Me.ApplicationID
            '    .SpName = "SpInsEditInsuranceSelect"
            '    .strConnection = GetConnectionString
            'End With

            'EntitiesInsurance = ControllerInsurance.EditInsuranceSelect(EntitiesInsurance)
            'With EntitiesInsurance
            '    Me.InsAssetInsuredBy = .InsAssetInsuredBy
            '    Me.InsAssetPaidBy = .InsAssetPaidBy
            '    Me.SupplierGroupID = .SupplierGroupID
            '    Me.InterestType = .InterestType
            '    Me.InstallmentScheme = .InstallmentScheme
            '    Me.NumberInstallment = .NumberInstallment
            '    Me.NumberAgreementAsset = .NumberAgreementAsset

            '    Me.AssetID = .AssetID
            '    'Dim Null1 As Date = CType("1/1/1900", Date)
            '    'If .DateEntryApplicationData.ToString <> "-" Then
            '    Me.DateEntryApplicationData = CType(.DateEntryApplicationData, Date)
            '    'Else
            '    '    Me.DateEntryApplicationData = Null1
            '    'End If

            '    'If .DateEntryAssetData.ToString <> "-" Then
            '    Me.DateEntryAssetData = CType(.DateEntryAssetData, Date)
            '    'Else
            '    '    Me.DateEntryAssetData = Null1
            '    'End If

            '    'If .DateEntryInsuranceData.ToString <> "-" Then
            '    Me.DateEntryInsuranceData = CType(.DateEntryInsuranceData, Date)
            '    'Else
            '    '    Me.DateEntryInsuranceData = Null1
            '    'End If

            '    'If .DateEntryFinancialData.ToString <> "-" Then
            '    Me.DateEntryFinancialData = CType(.DateEntryFinancialData, Date)
            '    Me.DateEntryIncentiveData = CType(.DateEntryIncentiveData, Date)
            '    'Else
            '    '    Me.DateEntryFinancialData = Null1
            '    'End If



            'End With
            'SendCookiesFinancial()
            'SendCookiesAssetData()
            'Context.Trace.Write("Me.ApplicationID = " & Me.ApplicationID)
            'Context.Trace.Write("Me.InsAssetInsuredBy = " & Me.InsAssetInsuredBy)
            'Context.Trace.Write("Me.InsAssetPaidBy = " & Me.InsAssetPaidBy)
            'Context.Trace.Write("Me.SupplierGroupID = " & Me.SupplierGroupID)
            'Context.Trace.Write("Me.DateEntryApplicationData =" & Me.DateEntryApplicationData)
            'Context.Trace.Write("Me.DateEntryAssetData =" & Me.DateEntryAssetData)
            'Context.Trace.Write("Me.DateEntryInsuranceData =" & Me.DateEntryInsuranceData)
            'Context.Trace.Write("Me.DateEntryFinancialData =" & Me.DateEntryFinancialData)
            'Modify()

            If Me.ApplicationModule = "AGRI" Then
                Response.Redirect("../ApplicationAgriculture/ApplicationAgriculture_003.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustID.ToString.Trim)
            ElseIf Me.ApplicationModule = "OPLS" Then
                Response.Redirect("../OperatingLease/ApplicationOperatingLease_003.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustID.ToString.Trim)
            ElseIf Me.ApplicationModule = "FACT" Then
                Response.Redirect("../NewFactoring/ApplicationFactoring_003.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustID.ToString.Trim)
            ElseIf Me.ApplicationModule = "INVS" Then
                Response.Redirect("../NewInvestasi/ApplicationInvestasi_003.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustID.ToString.Trim)
            ElseIf Me.ApplicationModule = "MDKJ" Then
                Response.Redirect("../NewModalKerja/ApplicationModalKerja_003.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustID.ToString.Trim)
            Else
                Response.Redirect("../NewApplication/Application_003.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustID.ToString.Trim)
            End If

        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkApplication As New HyperLink
        Dim lnkSupplier As New HyperLink
        Dim lnkName As New HyperLink
        Dim lnkAO As New HyperLink

        Dim lblSupplierID As New Label
        Dim lblAOID As New Label
        Dim lblCustomerID As New Label
        Dim lblApplicationStep As New Label
        Dim lblDateReturnApp As New Label

        If e.Item.ItemIndex >= 0 Then
            lnkApplication = CType(e.Item.FindControl("lnkApplication"), HyperLink)
            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), HyperLink)
            lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)

            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationStep = CType(e.Item.FindControl("lblApplicationStep"), Label)
            lblDateReturnApp = CType(e.Item.FindControl("lblDateReturnApp"), Label)

            lnkApplication.NavigateUrl = LinkToOpenWinApplication2(lnkApplication.Text.Trim, lblCustomerID.Text.Trim, "AccAcq")
            lnkSupplier.NavigateUrl = LinkToSupplier("AccAcq", lblSupplierID.Text.Trim)
            lnkName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            lnkAO.NavigateUrl = LinkToEmployee("AccAcq", Me.sesBranchId.Replace("'", ""), lblAOID.Text.Trim)

           
            If lblDateReturnApp.Text <> "" And Not (lblDateReturnApp Is Nothing) Then
                lblApplicationStep.Text = "RETURN"
            End If

        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGrid()
    End Sub
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim Search As String = ""
        If txtSearch.Text.Trim <> "" Then
            If cboSearch.SelectedIndex = 2 Then
                Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
            Else
                Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + txtSearch.Text.Trim + "%'"
            End If
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGrid()
    End Sub
#End Region
#Region "SendCookiesFinancial"
    Public Sub SendCookiesFinancial()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_EDIT_FINANCIAL)
        If Not cookie Is Nothing Then
            cookie.Values("id") = Me.ApplicationID
            cookie.Values("Custid") = Me.CustID
            cookie.Values("name") = Me.Name
            cookie.Values("InterestType") = Me.InterestType
            cookie.Values("InstallmentScheme") = Me.InstallmentScheme
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_EDIT_FINANCIAL)
            cookieNew.Values.Add("id", Me.ApplicationID)
            cookieNew.Values.Add("Custid", Me.CustID)
            cookieNew.Values.Add("name", Me.Name)
            cookieNew.Values.Add("InterestType", Me.InterestType)
            cookieNew.Values.Add("InstallmentScheme", Me.InstallmentScheme)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region
#Region "SendCookiesAssetData"
    Public Sub SendCookiesAssetData()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_EDIT_ASSET_DATA)
        If Not cookie Is Nothing Then
            cookie.Values("id") = AppID
            cookie.Values("Custid") = CustID
            cookie.Values("name") = Me.Name
            cookie.Values("AssetID") = Me.AssetID
            cookie.Values("ApplicationID") = Me.ApplicationID
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_EDIT_ASSET_DATA)
            cookieNew.Values.Add("id", AppID)
            cookieNew.Values.Add("Custid", CustID)
            cookieNew.Values.Add("name", Me.Name)
            cookieNew.Values.Add("AssetID", Me.AssetID)
            cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

    Protected Sub dtgPaging_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtgPaging.SelectedIndexChanged

    End Sub
End Class