﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class EditAssetData
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCSupplier As ucLookUpSupplier   
    Protected WithEvents lblAO As UCAO
    Protected WithEvents UCAsset As ucLookUpAsset
    Protected WithEvents UCAddress As UcCompanyAddress   
    Protected WithEvents uscTax As ValidDate
#Region "Constanta"
    Dim style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim status As Boolean = True
    Dim m_controller As New AssetDataController
#End Region
#Region "Property"
    Property AOID() As String
        Get
            Return viewstate("AOID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AOID") = Value
        End Set
    End Property
    Property NU() As String
        Get
            Return viewstate("NU").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("NU") = Value
        End Set
    End Property
    Property App() As String
        Get
            Return viewstate("App").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("App") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return viewstate("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Supplier") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return viewstate("CustName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustName") = Value
        End Set
    End Property
    Property Asset() As String
        Get
            Return viewstate("Asset").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Asset") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        'Me.Supplier = UCSupplier.SupplierID
        Dim applicationid As String = Replace(Request.QueryString("ApplicationID"), "'", "")

        lblMessage.Text = ""
        InitialLabelValid()

        If Not Page.IsPostBack Then
            GetCookies()
            lblAO.Style = "ACCACQ"
            Dim entitiesAssetData As New Parameter.AssetData
            Dim oAssetDataController As New AssetDataController
            context.Trace.Write("ApplicationID = " & applicationid)

            '==================== Ambil Default Supplier ==========================
            With entitiesAssetData
                .AppID = applicationid
                .strConnection = GetConnectionString
                .SpName = "spEditAssetDataGetDefaultSupplierID"
            End With

            context.Trace.Write("Tahap1")
            Me.ApplicationID = applicationid

            entitiesAssetData = oAssetDataController.EditAssetDataGetDefaultSupplierID(entitiesAssetData)

            If Not entitiesAssetData Is Nothing Then
                With entitiesAssetData
                    'UCSupplier.SupplierID = .SupplierID
                    'UCSupplier.SupplierName = .SupplierName
                    'UCSupplier.NewAmountFee = .NewAmountFee.ToString
                    'UCSupplier.UsedAmountFee = .UsedAmountFee.ToString

                    context.Trace.Write("SupplierID = " & .SupplierID)
                    context.Trace.Write("SupplierName = " & .SupplierName)

                End With
                context.Trace.Write("Tahap2")
            End If

            context.Trace.Write("Tahap3")

            UCSupplier.ApplicationId = applicationid
            'UCSupplier.Style = style
            UCSupplier.BindData()
            UCAsset.ApplicationId = Me.ApplicationID
            UCAsset.Style = style
            UCAsset.BindData()
            UCAddress.Style = style
            UCAddress.BindAddress()
            UCAddress.ValidatorFalse()
            lblCustName.Text = Me.CustName
            If Request("page") = "Edit" Then
                Panel2()
            Else
                InitialPanel()
            End If
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccACq" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"

        End If

        UCAddress.ValidatorFalse()
    End Sub
#End Region
#Region "Initial Label Validator"
    Sub InitialLabelValid()
        Dim count As Integer
        lblVPaidBy.Visible = False
        count = CInt(IIf(dtgAssetDoc.Items.Count > dtgAttribute.Items.Count, dtgAssetDoc.Items.Count, dtgAttribute.Items.Count))
        For intLoop = 0 To count - 1
            If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label).Visible = False
                CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label).Visible = False
            End If
            If intLoop <= dtgAttribute.Items.Count - 1 Then
                CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label).Visible = False
            End If
        Next
    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_EDIT_ASSET_DATA)
        Me.App = cookie.Values("id")
        Me.CustomerID = cookie.Values("Custid")
        Me.CustName = cookie.Values("name")
        Me.Asset = cookie.Values("AssetID")
    End Sub
#End Region
#Region "Panel"
    Sub InitialPanel()
        pnl1.Visible = True
        pnl2.Visible = False
        lblSupplier.Visible = False
        UCSupplier.Visible = True
        lblreqPO.Visible = True
        lblErrAccOff.Text = ""
    End Sub
    Sub Panel2()
        pnl1.Visible = False
        pnl2.Visible = True
        lblSupplier.Visible = True
        UCSupplier.Visible = False
        lblreqPO.Visible = False
    End Sub
#End Region
#Region "FillCbo"
    Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = m_controller.GetCboEmp(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "0"
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"

    End Sub
#End Region
#Region "BindAsset, BindAttribute, GetAO, & GetSerial"
    Sub BindAsset()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.AppID = Me.ApplicationID
        oAssetData.AssetID = Me.Asset
        oAssetData.SpName = "spEditAssetDataAssetDoc"
        oAssetData = m_controller.EditAssetDataDocument(oAssetData)
        oData = oAssetData.ListData
        dtgAssetDoc.DataSource = oData
        dtgAssetDoc.DataBind()
    End Sub
    Sub BindAttribute()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.AppID = Me.ApplicationID
        oAssetData.AssetID = Me.Asset
        oAssetData.SpName = "spEditAssetDataAttribute"
        oAssetData = m_controller.EditGetAttribute(oAssetData)
        oData = oAssetData.ListData
        dtgAttribute.DataSource = oData
        dtgAttribute.DataBind()
    End Sub
    Sub GetSerial()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oData2 As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.AssetID = Me.Asset
        oAssetData = m_controller.GetSerial(oAssetData)
        oData = oAssetData.ListData
        'lblSerial1.Text = oData.Rows(0).Item(0).ToString
        'lblSerial2.Text = oData.Rows(0).Item(1).ToString
        lblSerial1.Text = "Chassis"
        lblSerial2.Text = "Engine"
        oAssetData.strConnection = GetConnectionString
        oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
        oAssetData.AppID = Me.App
        oAssetData = m_controller.GetUsedNew(oAssetData)
        oData2 = oAssetData.ListData
       
    End Sub
    Sub GetAO()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString
        oAssetData.BranchId = Replace(Me.sesBranchId, "'", "")
        oAssetData.SupplierID = Me.Supplier
        oAssetData.ApplicationId = Me.ApplicationID

        oAssetData = m_controller.GetAO(oAssetData)
        oData = oAssetData.ListData

        If oData.Rows.Count > 0 Then
            lblAO.EmployeeName = oData.Rows(0).Item(0).ToString.Trim
            lblAO.EmployeeID = oData.Rows(0).Item(1).ToString.Trim
            Me.AOID = oData.Rows(0).Item(1).ToString.Trim
        End If
    End Sub
#End Region
#Region "ImbOK, ImbCancel click"
    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOK.Click

        Context.Trace.Write("Tombol Ok di Click")
        'imbSave.Visible = False
        'lblSupplier.Text = UCSupplier.SupplierName
        'lblUsedAMountFee.Text = UCSupplier.UsedAmountFee
        pnlAgenFee.Visible = True

        Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        Dim WhereSup As String = "SupplierID = '" & Me.Supplier & "'"
        'Me.Supplier = UCSupplier.SupplierID
        UCSupplier.Visible = False
        lblSupplier.Visible = True
        lblreqPO.Visible = False
        RangeValidator3.MaximumValue = CStr(Year(Me.BusinessDate) + 1)
        Panel2()

        '================================================
        GetDefaultAssetData()
        '================================================

        BindAsset()
        BindAttribute()
        GetSerial()
        GetAO()

        '========================== Ambil Default Asset Registration ======================
        Dim entitiesAssetData As New Parameter.AssetData
        Dim oAssetDataController As New AssetDataController
        Dim oDataRegistration As New DataTable

        With entitiesAssetData
            .AppID = Me.ApplicationID
            .strConnection = GetConnectionString
            .SpName = "spEditAssetDataAssetRegistration"
        End With

        entitiesAssetData = oAssetDataController.EditGetAssetRegistration(entitiesAssetData)

        If Not entitiesAssetData Is Nothing Then
            oDataRegistration = entitiesAssetData.ListData
            txtName.Text = oDataRegistration.Rows(0).Item(1).ToString
            uscTax.dateValue = oDataRegistration.Rows(0).Item(9).ToString
            txtAssetNotes.Text = oDataRegistration.Rows(0).Item(10).ToString
            UCAddress.Address = oDataRegistration.Rows(0).Item(2).ToString
            UCAddress.RT = oDataRegistration.Rows(0).Item(6).ToString
            UCAddress.RW = oDataRegistration.Rows(0).Item(7).ToString
            UCAddress.Kelurahan = oDataRegistration.Rows(0).Item(3).ToString
            UCAddress.Kecamatan = oDataRegistration.Rows(0).Item(4).ToString
            UCAddress.City = oDataRegistration.Rows(0).Item(5).ToString
            UCAddress.ZipCode = oDataRegistration.Rows(0).Item(8).ToString
            UCAddress.BindAddress()
        End If

        '=================== get Default AssetInsuranceEmployee ===================
        Dim EntitiesAssetInsemployee As New Parameter.AssetData
        Dim oAssetInsEmployeeController As New AssetDataController
        Dim oDataInsEmployee As New DataTable

        With EntitiesAssetInsemployee
            .AppID = Me.ApplicationID
            .strConnection = GetConnectionString
            .SpName = "SpEditAssetInsuranceEmployee"
        End With

        EntitiesAssetInsemployee = oAssetInsEmployeeController.EditAssetInsuranceEmployee(EntitiesAssetInsemployee)



        If Not EntitiesAssetInsemployee Is Nothing Then
            oDataInsEmployee = EntitiesAssetInsemployee.ListData

            LblInsuredByHidden.Text = oDataInsEmployee.Rows(0).Item(1).ToString
            FillCbo(cboInsuredBy, "tblInsuredBy")
            cboInsuredBy.Items.FindByValue(LblInsuredByHidden.Text).Selected = True

            LblPaidByHidden.Text = oDataInsEmployee.Rows(0).Item(2).ToString
            FillCbo(cboPaidBy, "tblPaidBy")
            cboPaidBy.Items.FindByValue(LblPaidByHidden.Text).Selected = True

            LblCAHidden.Text = oDataInsEmployee.Rows(0).Item(4).ToString
            FillCboEmp("BranchEmployee", cboCA, WhereBranch + " and (EmployeePosition='CA' or EmployeePosition='AO') ")
            Try
                cboCA.Items.FindByValue(LblCAHidden.Text).Selected = True
            Catch ex As Exception
            End Try


            LblSurveyorHidden.Text = oDataInsEmployee.Rows(0).Item(5).ToString
            FillCboEmp("BranchEmployee", cboSurveyor, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")
            Try
                cboSurveyor.Items.FindByValue(LblSurveyorHidden.Text).Selected = True
            Catch ex As Exception
            End Try

            LblSalesmanHidden.Text = oDataInsEmployee.Rows(0).Item(6).ToString
            FillCboEmp("SupplierEmployee", cboSalesman, WhereSup + " and SupplierEmployeePosition='SL'")

            Try
                'cboSalesman.SelectedIndex = cboSalesman.Items.IndexOf(cboSalesman.Items.FindByValue(LblSalesmanHidden.Text))
                cboSalesman.Items.FindByValue(LblSalesmanHidden.Text).Selected = True
            Catch ex As Exception
            End Try

            LblSalesSupervisorHidden.Text = oDataInsEmployee.Rows(0).Item(7).ToString
            FillCboEmp("SupplierEmployee", cboSalesSpv, WhereSup + " and SupplierEmployeePosition='SV'")
            Try
                cboSalesSpv.Items.FindByValue(LblSalesSupervisorHidden.Text).Selected = True
            Catch ex As Exception
            End Try

            LblSupplierAdminHidden.Text = oDataInsEmployee.Rows(0).Item(8).ToString
            FillCboEmp("SupplierEmployee", cboSupplierAdm, WhereSup + " and SupplierEmployeePosition='AM'")
            Try
                cboSupplierAdm.Items.FindByValue(LblSupplierAdminHidden.Text).Selected = True
            Catch ex As Exception
            End Try


        End If

        InitialLabelValid()
        cboPaidBy.Visible = True
        cboInsuredBy.Attributes.Add("OnChange", "return fInsured();")
        lblSupplier.NavigateUrl = "javascript:OpenWinSupplier('" & "AccAcq" & "', '" & Server.UrlEncode(Me.Supplier.Trim) & "')"
        Context.Trace.Write("End Sub dari Tombol Ok di Click")
    End Sub
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("ApplicationMaintenance.aspx")
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
#End Region
#Region "Validator & save"
    Sub Validator(ByVal oData2 As DataTable, ByVal oData3 As DataTable)

        Try

            Dim ErrS1 As Boolean = False
            Dim ErrS2 As Boolean = False

            If cboPaidBy.SelectedValue = "Select One" And cboInsuredBy.SelectedValue <> "CU" Then
                lblVPaidBy.Visible = True
                status = False
            End If
            If CDec(txtOTR.Text) < CDec(IIf(txtDP.Text.Trim = "", "0", txtDP.Text.Trim)) Then
                status = False
                lblMessage.Text = "Harga OTR harus lebih besar dari Uang muka"
                Exit Sub
            End If
            Dim oAssetData As New Parameter.AssetData
            Dim oData As New DataTable
            If txtSerial1.Text.Trim <> "" Or txtSerial2.Text.Trim <> "" Then
                oAssetData.strConnection = GetConnectionString
                oAssetData.Serial1 = txtSerial1.Text
                oAssetData.Serial2 = txtSerial2.Text
                oAssetData.AssetID = Me.Asset
                oAssetData.AppID = Me.ApplicationID
                oAssetData = m_controller.CheckSerial(oAssetData)
                oData = oAssetData.ListData
                If oData.Rows.Count <> 0 Then
                    oData = oAssetData.ListData
                    Dim intLoop As Integer
                    For intLoop = 0 To oData.Rows.Count - 1
                        If txtSerial1.Text.Trim <> "" Then
                            If oData.Rows(intLoop).Item(0).ToString.Trim = txtSerial1.Text.Trim Then
                                ErrS1 = True
                            End If
                        End If
                        If txtSerial2.Text.Trim <> "" Then
                            If oData.Rows(intLoop).Item(1).ToString.Trim = txtSerial2.Text.Trim Then
                                ErrS2 = True
                            End If
                        End If
                    Next
                    If ErrS1 = True And ErrS2 = True Then
                        lblMessage.Text = "No Serial 1 dan No Serial 2 sudah ada"
                        status = False
                    Else
                        If ErrS1 = True Then
                            lblMessage.Text = "No Serial 1 Sudah ada"
                            status = False
                        ElseIf ErrS2 = True Then
                            lblMessage.Text = "No Serial 2 Sudah ada"
                            status = False
                        End If
                    End If
                End If
            End If
            Dim AttributeId As String
            Dim txtAttribute As TextBox
            Dim lblVAttribute, lblVNumber, lblVNumber2 As Label
            Dim count As Integer
            Dim lblvChk As Label
            Dim chk As CheckBox
            Dim MandatoryNew, MandatoryUsed As String
            Dim txtnumber As TextBox
            Dim isValueNeeded, value As String
            count = CInt(IIf(dtgAttribute.Items.Count > dtgAssetDoc.Items.Count, dtgAttribute.Items.Count, dtgAssetDoc.Items.Count))
            For intLoop = 0 To count - 1
                If intLoop <= dtgAttribute.Items.Count - 1 Then
                    AttributeId = dtgAttribute.Items(intLoop).Cells(2).Text.Trim
                    txtAttribute = CType(dtgAttribute.Items(intLoop).FindControl("txtAttribute"), TextBox)
                    lblVAttribute = CType(dtgAttribute.Items(intLoop).FindControl("lblVAttribute"), Label)
                    If AttributeId = "LICPLATE" And txtAttribute.Text.Trim <> "" Then
                        oAssetData = New Parameter.AssetData
                        oData = New DataTable
                        oAssetData.strConnection = GetConnectionString
                        oAssetData.Input = txtAttribute.Text
                        oAssetData.AssetID = Me.Asset
                        oAssetData.ApplicationId = Me.ApplicationID
                        oAssetData = m_controller.CheckAttribute(oAssetData)
                        oData = oAssetData.ListData
                        If oData.Rows.Count > 0 Then
                            lblVAttribute.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        End If
                    End If
                    objrow = oData2.NewRow
                    objrow("AttributeID") = AttributeId
                    objrow("AttributeContent") = txtAttribute.Text.Trim
                    oData2.Rows.Add(objrow)
                End If
                If intLoop <= dtgAssetDoc.Items.Count - 1 Then
                    chk = CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox)
                    lblvChk = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVChk"), Label)
                    lblVNumber = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber"), Label)
                    lblVNumber2 = CType(dtgAssetDoc.Items(intLoop).FindControl("lblVNumber2"), Label)
                    value = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                    isValueNeeded = dtgAssetDoc.Items(intLoop).Cells(9).Text.Trim
                    MandatoryNew = dtgAssetDoc.Items(intLoop).Cells(5).Text.Trim
                    MandatoryUsed = dtgAssetDoc.Items(intLoop).Cells(6).Text.Trim
                    If Me.NU = "N" And MandatoryNew = "1" Then
                        If chk.Checked = False Then
                            lblvChk.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        Else
                            If isValueNeeded = "True" And value = "" Then
                                lblVNumber2.Visible = True
                                If status <> False Then
                                    status = False
                                End If
                            End If
                        End If
                    ElseIf Me.NU = "U" And MandatoryUsed = "1" Then
                        If chk.Checked = False Then
                            lblvChk.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        Else
                            If isValueNeeded = "True" And value = "" Then
                                lblVNumber2.Visible = True
                                If status <> False Then
                                    status = False
                                End If
                            End If
                        End If
                    End If
                    txtnumber = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox)
                    If txtnumber.Text.Trim <> "" Then
                        oAssetData = New Parameter.AssetData
                        oData = New DataTable
                        oAssetData.strConnection = GetConnectionString
                        oAssetData.Input = txtnumber.Text.Trim
                        oAssetData.AssetDocID = dtgAssetDoc.Items(intLoop).Cells(7).Text.Trim
                        oAssetData.AssetID = Me.Asset
                        oAssetData.ApplicationId = Me.ApplicationID
                        oAssetData = m_controller.CheckAssetDoc(oAssetData)
                        oData = oAssetData.ListData
                        If oData.Rows.Count > 0 Then
                            lblVNumber.Visible = True
                            If status <> False Then
                                status = False
                            End If
                        End If
                    End If
                    objrow = oData3.NewRow
                    objrow("AssetDocID") = dtgAssetDoc.Items(intLoop).Cells(7).Text.Trim
                    objrow("DocumentNo") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNumber"), TextBox).Text.Trim
                    objrow("IsMainDoc") = dtgAssetDoc.Items(intLoop).Cells(8).Text.Trim
                    objrow("IsDocExist") = IIf(CType(dtgAssetDoc.Items(intLoop).FindControl("chk"), CheckBox).Checked = True, "1", "0").ToString
                    objrow("Notes") = CType(dtgAssetDoc.Items(intLoop).FindControl("txtNotes"), TextBox).Text.Trim
                    oData3.Rows.Add(objrow)
                End If
            Next

            If status = False Then
                Exit Sub
            End If

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("EditAssetData.aspx.vb", "Validator", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)

        End Try


    End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        status = True

        If (lblAO.EmployeeName = "") Then
            lblErrAccOff.Visible = True
            lblErrAccOff.Text = "Harap isi Setting di Supplier terlebih dahulu"
            Exit Sub
        End If

        'If lblAO.Text.Trim = "" Then
        '    lblErrAccOff.Visible = True
        '    lblErrAccOff.Text = "Please fill in Supplier Setting for this AO first!"
        '    Exit Sub
        'End If

        Dim oAssetData As New Parameter.AssetData
        Dim oAddress As New Parameter.Address
        Dim oData1 As New DataTable
        Dim oData2 As New DataTable
        Dim oData3 As New DataTable

        oData1.Columns.Add("AssetLevel", GetType(Integer))
        oData1.Columns.Add("AssetCode", GetType(String))

        oData2.Columns.Add("AttributeID", GetType(String))
        oData2.Columns.Add("AttributeContent", GetType(String))

        oData3.Columns.Add("AssetDocID", GetType(String))
        oData3.Columns.Add("DocumentNo", GetType(String))
        oData3.Columns.Add("IsMainDoc", GetType(String))
        oData3.Columns.Add("IsDocExist", GetType(String))
        oData3.Columns.Add("Notes", GetType(String))

        Validator(oData2, oData3)
        If status = False Then
            If cboInsuredBy.SelectedValue = "CU" Then
                cboPaidBy.ClearSelection()
                cboPaidBy.Items.FindByValue("CU").Selected = True
                cboPaidBy.Enabled = False
            End If
            Exit Sub
        End If

        Dim AssetCode As String = UCAsset.AssetCode
        Dim lArrValue As Array = CType(AssetCode.Split(CChar(".")), Array)
        Dim DataCount As Integer = UBound(lArrValue)
        Dim Asset As String = ""
        For intLoop = 0 To DataCount
            objrow = oData1.NewRow
            objrow("AssetLevel") = intLoop + 1
            If intLoop = 0 Then
                Asset = lArrValue.GetValue(intLoop).ToString
                objrow("AssetCode") = Asset
            Else
                Asset = Asset + "." + lArrValue.GetValue(intLoop).ToString
                objrow("AssetCode") = Asset
            End If
            oData1.Rows.Add(objrow)
        Next
        With oAssetData
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .AppID = Me.ApplicationID
            .SupplierID = Me.Supplier
            .OTR = CDec(txtOTR.Text)
            .DP = CDec(IIf(txtDP.Text.Trim = "", "0", txtDP.Text))
            .AssetID = Me.Asset
            .AssetCode = UCAsset.AssetCode
            .Serial1 = txtSerial1.Text
            .Serial2 = txtSerial2.Text
            .UsedNew = Me.NU
            .AssetUsage = cboUsage.SelectedValue
            .ManufacturingYear = CInt(txtYear.Text)
            .OldOwnerAsset = txtName.Text
            .TaxDate = IIf(uscTax.dateValue <> "", ConvertDate(uscTax.dateValue), "").ToString
            .Notes = txtAssetNotes.Text
            .InsuredBy = cboInsuredBy.SelectedValue
            .PaidBy = IIf(cboInsuredBy.SelectedValue = "CU", "CU", cboPaidBy.SelectedValue).ToString
            .AOID = lblAO.EmployeeID
            .CAID = cboCA.SelectedValue
            .SurveyorID = cboSurveyor.SelectedValue
            .SalesmanID = cboSalesman.SelectedValue
            .SalesSupervisorID = cboSalesSpv.SelectedValue
            .SupplierAdminID = cboSupplierAdm.SelectedValue
            .IsIncentiveSupplier = rboSSI.SelectedValue
            .DateEntryAssetData = Me.BusinessDate
            .Pemakai = txtPemakai.Text
            .Lokasi = txtLokasi.Text
            .HargaLaku = CDbl(IIf(txtHargaLaku.Text.Trim = "", 0, txtHargaLaku.Text))
            .SR1 = txtSR1.Text
            .SR2 = txtSR2.Text
            .HargaSR1 = CDbl(IIf(txtHargaSR1.Text.Trim = "", 0, txtHargaSR1.Text))
            .HargaSR2 = CDbl(IIf(txtHargaSR2.Text.Trim = "", 0, txtHargaSR2.Text.Trim))
            .Flag = "Edit"
            .strConnection = GetConnectionString
        End With

        oAddress.Address = UCAddress.Address
        oAddress.RT = UCAddress.RT
        oAddress.RW = UCAddress.RW
        oAddress.Kelurahan = UCAddress.Kelurahan
        oAddress.Kecamatan = UCAddress.Kecamatan
        oAddress.City = UCAddress.City
        oAddress.ZipCode = UCAddress.ZipCode

        Dim oReturn As New Parameter.AssetData   

        oReturn = m_controller.AssetDataSaveAdd(oAssetData, oAddress, oData1, oData2, oData3)

        Dim oAsset As New Parameter.AssetData

        If oReturn.Output <> "" Then
            lblMessage.Text = oReturn.Output
        Else

            'If chkContinue.Checked = False Then
            Response.Redirect("ApplicationMaintenance.aspx?Err=" & oReturn.Message)
            'Else
            '    oAsset.SupplierID = Me.Supplier
            '    oAsset.strConnection = GetConnectionString
            '    SupplierGroupID = m_controller.GetSupplierGroupID(oAsset)

            '    If cboInsuredBy.SelectedValue = "CU" Then
            '        Response.Redirect("EditAppInsuranceByCust.aspx?PageSource=CustToCost&ApplicationID=" & Me.ApplicationID & "&SupplierGroupID=" & SupplierGroupID & "")
            '    Else
            '        If cboPaidBy.SelectedValue = "CU" Then
            '            Response.Redirect("EditInsurance.aspx?PageSource=CompanyCustomer&ApplicationID=" & Me.ApplicationID & "&SupplierGroupID=" & SupplierGroupID & "")
            '        Else
            '            Response.Redirect("EditInsurance.aspx?PageSource=CompanyAtCost&ApplicationID=" & Me.ApplicationID & "")
            '        End If
            '    End If
            '    Response.Redirect("ApplicationMaintenance.aspx")
            'End If
        End If
        'Catch ex As Exception            
        '    lblMessage.Text = ex.Message
        'End Try


    End Sub
#End Region
    Sub GetDefaultAssetData()
        Try
            Dim controllerassetData As New EditApplicationController
            Dim EntitiesAssetData As New Parameter.Application
            Dim oDataAsset As New DataTable

            With EntitiesAssetData
                .ApplicationID = Me.ApplicationID
                .strConnection = GetConnectionString
                .SpName = "EditAssetDataGetInfoAssetData"
            End With

            EntitiesAssetData = controllerassetData.EditAssetDataGetInfoAssetData(EntitiesAssetData)

            If Not EntitiesAssetData Is Nothing Then
                oDataAsset = EntitiesAssetData.ListData
                UCAsset.AssetCode = oDataAsset.Rows(0).Item(1).ToString
                UCAsset.Description = oDataAsset.Rows(0).Item(2).ToString
                txtOTR.Text = oDataAsset.Rows(0).Item(3).ToString
                txtDP.Text = oDataAsset.Rows(0).Item(4).ToString
                txtSerial1.Text = oDataAsset.Rows(0).Item(5).ToString
                txtSerial2.Text = oDataAsset.Rows(0).Item(6).ToString
                txtYear.Text = oDataAsset.Rows(0).Item(9).ToString
                lblNewUsed.Text = oDataAsset.Rows(0).Item(7).ToString
                Me.NU = Replace(lblNewUsed.Text, "'", "")
                LblUsageHidden.Text = oDataAsset.Rows(0).Item(8).ToString
                FillCbo(cboUsage, "TblAssetUsage")
                cboUsage.Items.FindByValue(LblUsageHidden.Text).Selected = True
                txtPemakai.Text = oDataAsset.Rows(0).Item("Pemakai").ToString
                txtLokasi.Text = oDataAsset.Rows(0).Item("Lokasi").ToString
                txtHargaLaku.Text = oDataAsset.Rows(0).Item("HargaLaku").ToString
                txtSR1.Text = oDataAsset.Rows(0).Item("SR1").ToString
                txtSR2.Text = oDataAsset.Rows(0).Item("SR2").ToString
                txtHargaSR1.Text = oDataAsset.Rows(0).Item("HargaSR1").ToString
                txtHargaSR2.Text = oDataAsset.Rows(0).Item("HargaSR2").ToString
            End If
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("EditAssetData.aspx.vb", "GetDefaultAssetData", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Response.Write(ex.StackTrace)
        End Try

    End Sub

    Private Sub btnCopyAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyAddress.Click
        Dim c_customer As New CustomerController
        Dim oCustomer As New Parameter.Customer

        With oCustomer
            .strConnection = getConnectionString
            .CustomerID = Me.CustomerID
        End With

        Dim dt As New DataTable
        Dim strCustomerType As String

        strCustomerType = c_customer.GetCustomerType(oCustomer)

        With oCustomer
            .strConnection = getConnectionString
            .CustomerID = Me.CustomerID
            .CustomerType = strCustomerType
        End With

        oCustomer = c_customer.GetViewCustomer(oCustomer)
        dt = oCustomer.listdata

        txtName.Text = Me.CustName

        If dt.Rows.Count > 0 Then
            With UCAddress
                .Address = CStr(dt.Rows(0).Item("LegalAddress")).Trim
                .RT = CStr(dt.Rows(0).Item("LegalRT")).Trim
                .RW = CStr(dt.Rows(0).Item("LegalRW")).Trim
                .Kelurahan = CStr(dt.Rows(0).Item("LegalKelurahan")).Trim
                .Kecamatan = CStr(dt.Rows(0).Item("LegalKecamatan")).Trim
                .City = CStr(dt.Rows(0).Item("LegalCity")).Trim
                .ZipCode = CStr(dt.Rows(0).Item("LegalZipCode")).Trim
                .AreaPhone1 = CStr(dt.Rows(0).Item("LegalAreaPhone1")).Trim
                .Phone1 = CStr(dt.Rows(0).Item("LegalPhone1")).Trim
                .AreaPhone2 = CStr(dt.Rows(0).Item("LegalAreaPhone2")).Trim
                .Phone2 = CStr(dt.Rows(0).Item("LegalPhone2")).Trim
                .AreaFax = CStr(dt.Rows(0).Item("LegalAreaFax")).Trim
                .Fax = CStr(dt.Rows(0).Item("LegalFax")).Trim
            End With
        End If

        UCAddress.BindAddress()
    End Sub

End Class