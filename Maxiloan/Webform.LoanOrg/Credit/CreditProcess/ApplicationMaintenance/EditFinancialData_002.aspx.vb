﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Interface
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class EditFinancialData_002
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private Const style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim RunRate As Double
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim m_controller As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController
#End Region
#Region "Property"
    Property Status() As Boolean
        Get
            Return CType(viewstate("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("Status") = Value
        End Set
    End Property
    Property Installment() As Double
        Get
            Return CDbl(viewstate("Installment"))
        End Get
        Set(ByVal Value As Double)
            viewstate("Installment") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(viewstate("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            viewstate("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(viewstate("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            viewstate("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(viewstate("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            viewstate("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(viewstate("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            viewstate("RejectMinimumIncome") = Value
        End Set
    End Property
    Property First() As String
        Get
            Return viewstate("First").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("First") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return viewstate("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return viewstate("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Supplier") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return viewstate("CustName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return viewstate("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("InterestType") = Value
        End Set
    End Property
    Property InstallmentScheme() As String
        Get
            Return viewstate("InstallmentScheme").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("InstallmentScheme") = Value
        End Set
    End Property
    Property IsIncentiveSupplier() As Boolean
        Get
            Return CType(viewstate("IsIncentiveSupplier"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsIncentiveSupplier") = Value
        End Set
    End Property
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            txtAmount.Text = "0"

            GetCookies()

            txtSupplierRate.Text = ""
            hyAppId.Text = Me.ApplicationID
            hyAppId.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Me.ApplicationID & "')"
            hyCustName.Text = Me.CustName
            hyCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Me.CustomerID & "')"
            lblInterestType.Text = IIf(Me.InterestType = "FX", "Fixed Rate", "FloatingRate").ToString
            pnlEntryInstallment.Visible = False
            pnlViewST.Visible = False
            txtStep.Enabled = False
            txtCummulative.Enabled = False
            rvStep.Enabled = False
            rvCummulative.Enabled = False
            Entities.strConnection = GetConnectionString
            Entities.BranchId = Replace(Me.sesBranchId, "'", "")
            Entities.AppID = Me.ApplicationID
            Me.StepUpDownType = m_controller.GetStepUpStepDownType(Entities)
            If Me.InterestType <> "FL" Then
                cboFloatingPeriod.Enabled = False
                reqFloatingPeriod.Enabled = False
            Else
                cboFloatingPeriod.Enabled = True
                reqFloatingPeriod.Enabled = True
            End If
            If Me.InstallmentScheme = "RF" Then
                lblInstScheme.Text = "Regular Fixed Installment Scheme"
                If cboFirstInstallment.SelectedValue = "AD" Then
                    txtGracePeriod.Enabled = False
                    txtGracePeriod.Text = "0"
                    cboGracePeriod.Enabled = False
                    cboGracePeriod.ClearSelection()
                Else
                    txtGracePeriod.Enabled = True
                    cboGracePeriod.Enabled = True
                End If
            ElseIf Me.InstallmentScheme = "IR" Then
                lblInstScheme.Text = "Irregular Installment Scheme"
                lblInstallment.Text = "Last Installment"
                RVInstAmt.Enabled = False
                txtInstAmt.Enabled = False
                txtGracePeriod.Enabled = False
                cboGracePeriod.Enabled = False
                imbSave.Enabled = False
                imbSave.Visible = False
            ElseIf Me.InstallmentScheme = "ST" Then
                lblInstScheme.Text = "Step Up / Step Down Installment Scheme"
                RVInstAmt.Enabled = False
                txtInstAmt.Enabled = False
                txtGracePeriod.Enabled = False
                cboGracePeriod.Enabled = False
                imbSave.Enabled = False
                imbSave.Visible = False

                If Me.StepUpDownType = "NM" Then
                    txtStep.Enabled = True
                    txtCummulative.Enabled = False
                    rvStep.Enabled = True
                    lblStepUpDownType.Text = "Basic Step Up/Step Down"
                ElseIf Me.StepUpDownType = "RL" Then
                    txtStep.Enabled = False
                    txtCummulative.Enabled = True
                    rvCummulative.Enabled = True
                    lblStepUpDownType.Text = "Normal"
                Else
                    txtStep.Enabled = True
                    txtCummulative.Enabled = True
                    rvStep.Enabled = True
                    rvCummulative.Enabled = True
                    lblStepUpDownType.Text = "Leasing Step Up/Step Down"
                End If
            Else
                lblInstScheme.Text = "Even Principle Installment Scheme"
                txtInstAmt.Enabled = False
                'imbRecalcEffRate.Enabled = False
                imbRecalcEffRate.Visible = False
                If cboFirstInstallment.SelectedValue = "AD" Then
                    txtGracePeriod.Enabled = False
                    txtGracePeriod.Text = "0"
                    cboGracePeriod.Enabled = False
                    cboGracePeriod.ClearSelection()
                Else
                    txtGracePeriod.Enabled = True
                    cboGracePeriod.Enabled = True
                End If
            End If
            If Me.InterestType = "FX" Then
                cboFloatingPeriod.Visible = False
                lblFloatingPeriod.Visible = False
                If Me.InstallmentScheme = "ST" Then
                    If Me.StepUpDownType = "NM" Then
                        txtStep.Visible = True
                        lblNumberOfStep.Visible = True
                        txtCummulative.Visible = False
                        lblCummulative.Visible = False
                    ElseIf Me.StepUpDownType = "RL" Then
                        txtStep.Visible = False
                        lblNumberOfStep.Visible = False
                        txtCummulative.Visible = True
                        lblCummulative.Visible = True
                    ElseIf Me.StepUpDownType = "LS" Then
                        txtStep.Visible = True
                        lblNumberOfStep.Visible = True
                        txtCummulative.Visible = True
                        lblCummulative.Visible = True
                    End If
                Else
                    txtStep.Visible = False
                    txtCummulative.Visible = False
                    lblNumberOfStep.Visible = False
                    lblCummulative.Visible = False
                End If
            End If
            If Me.InterestType = "FL" Then
                cboFloatingPeriod.Visible = True
                lblFloatingPeriod.Visible = True
                txtStep.Visible = False
                txtCummulative.Visible = False
                lblNumberOfStep.Visible = False
                lblCummulative.Visible = False
            End If
            If Me.StepUpDownType = "NM" Or Me.StepUpDownType = "LS" Then
                '  imbRecalcEffRate.Enabled = False
                imbRecalcEffRate.Visible = False

            Else
                imbRecalcEffRate.Enabled = True
                imbRecalcEffRate.Visible = True

            End If
            Bindgrid_002()
            FlatRate()
        End If

        lblMessage.Text = ""
        lblFlatRate.Visible = False

    End Sub
#End Region
#Region "Bindgrid_002"
    Sub Bindgrid_002()
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData
        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString
        oFinancialData.SpName = "spFinancialData_002"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)
        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.listdata
        End If
        Dim strPOEffectiveRate As String
        Dim strPOEffectiveRateBehaviour As String
        Dim strPBEffectiveRate As String
        Dim strPBEffectiveRateBehaviour As String
        Dim strPEffectiveRate As String
        Dim strPEffectiveRateBehaviour As String

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            lblAssetInsurance1.Text = FormatNumber(objrow.Item(0), 2)
            lblAssetInsurance2.Text = FormatNumber(objrow.Item(1), 2)
            lblAssetInsurance3.Text = FormatNumber(objrow.Item(2), 2)
            lblOTR.Text = FormatNumber(objrow.Item(3), 2)
            lblDP.Text = FormatNumber(objrow.Item(4), 2)
            lblNTF.Text = FormatNumber(objrow.Item(5), 2)
            txtEffectiveRate.Text = objrow.Item(6).ToString.Trim
            txtSupplierRate.Text = objrow.Item(6).ToString.Trim

            txtAdminFee.Text = objrow.Item("AdminFee").ToString.Trim
            lblIsAdminFeeCredit.Text = "(" & objrow.Item("IsAdminFeeCredit").ToString & ")"
            lblAgentFee.Text = FormatNumber(objrow.Item("AgentFee"), 2)
            txtProvisi.Text = objrow.Item("ProvisionFee").ToString.Trim
            lblIsProvisiCredit.Text = "(" & objrow.Item("IsProvisiCredit").ToString & ")"

            strPOEffectiveRate = objrow.Item(6).ToString.Trim
            strPOEffectiveRateBehaviour = objrow.Item(7).ToString.Trim

            strPBEffectiveRate = objrow.Item(13).ToString.Trim
            strPBEffectiveRateBehaviour = objrow.Item(14).ToString.Trim

            strPEffectiveRate = objrow.Item(15).ToString.Trim
            strPEffectiveRateBehaviour = objrow.Item(16).ToString.Trim

            BehaviourPersen("ProductBranch", txtEffectiveRate, strPBEffectiveRate, RVPBEffectiveRate, strPBEffectiveRateBehaviour, imbCalcInst)
            BehaviourPersen("Product", txtEffectiveRate, strPEffectiveRate, RVPEffectiveRate, strPEffectiveRateBehaviour, imbCalcInst)
            BehaviourPersen("ProductOffering", txtEffectiveRate, strPOEffectiveRate, RVPOEffectiveRate, strPOEffectiveRateBehaviour, imbCalcInst)

            lblTenor.Text = objrow.Item(8).ToString.Trim
            txtNumInst.Text = objrow.Item(8).ToString.Trim
            txtInstAmt.Text = CInt(objrow.Item(9)).ToString.Trim
            If Me.InstallmentScheme <> "IR" Then
                Behaviour(txtInstAmt, RVInstAmt, objrow.Item(10).ToString.Trim, imbRecalcEffRate)
            End If



            Me.RejectMinimumIncome = CDbl(objrow.Item(11))
            Me.NTFGrossYield = CDbl(objrow.Item(12))

            txtGracePeriod2.Text = objrow.Item("GracePeriodLateCharges").ToString.Trim
            Behaviour(txtGracePeriod2, rvGracePeriodLC, objrow.Item("GracePeriodLateChargesPatern").ToString, imbRecalcEffRate)

            txtGracePeriod.Text = "0"
            txtAmount.Text = objrow.Item(29).ToString.Trim
            txtInstAmt.Text = objrow.Item(25).ToString.Trim
            cboGracePeriod.SelectedIndex = cboGracePeriod.Items.IndexOf(cboGracePeriod.Items.FindByValue(objrow("GracePeriodType").ToString))
            cboFloatingPeriod.SelectedIndex = cboFloatingPeriod.Items.IndexOf(cboFloatingPeriod.Items.FindByValue(objrow("FloatingPeriod").ToString))
            cboPaymentFreq.SelectedIndex = cboPaymentFreq.Items.IndexOf(cboPaymentFreq.Items.FindByValue(objrow("PaymentFrequency").ToString))
            cboFirstInstallment.SelectedIndex = cboFirstInstallment.Items.IndexOf(cboFirstInstallment.Items.FindByValue(objrow("FirstInstallment").ToString))
            cboPaymentFreq.Attributes.Add("OnChange", "return NumInst('" & lblTenor.Text & "',this.value, '" & txtNumInst.ClientID & "');")
            cboFirstInstallment.Attributes.Add("OnChange", "return GracePeriod('" & txtGracePeriod.ClientID & "','" & cboGracePeriod.ClientID & "',this.value)")
        End If
    End Sub
    Sub Behaviour(ByVal textbox As TextBox, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As ImageButton)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MinimumValue = textbox.Text.Trim
                rv.MaximumValue = "999999999999999"
                rv.ErrorMessage = "Input harus >= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MaximumValue = textbox.Text.Trim
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus <= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "999999999999999"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Harap isi denga angka"
        End Select
    End Sub
    Sub BehaviourPersen(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As ImageButton)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                textbox.Text = value
                rv.Enabled = True
                rv.MinimumValue = value
                rv.MaximumValue = "100"
                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                textbox.Text = value
                rv.Enabled = True
                rv.MaximumValue = value
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >=0 dan <= " & value

                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                textbox.Text = value
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "100"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >= 0 dan <= 100"
        End Select
    End Sub
    Sub BehaviourPersenGY(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As ImageButton)
        Select Case Key
            Case "L"
                imb.Visible = False
                lblMessage.Text = ""
            Case "N"
                If CDbl(textbox.Text.Trim) < CDbl(value) Then
                    If Flag = "Product" Then
                        lblMessage.Text = "Nilai Gross Yield adalah  " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk) "
                        Me.Status = True
                    ElseIf Flag = "ProductBranch" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) "
                        Me.Status = True
                    ElseIf Flag = "ProductOffering" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) "
                        Me.Status = True
                    End If
                End If
                imb.Visible = True
            Case "X"
                If CDbl(textbox.Text.Trim) > CDbl(value) Then
                    If Flag = "Product" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk) "
                        Me.Status = True
                    ElseIf Flag = "ProductBranch" Then
                        lblMessage.Text = "Nilai Gross Yield adalah" & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) "
                        Me.Status = True
                    ElseIf Flag = "ProductOffering" Then
                        lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) "
                        Me.Status = True
                    End If
                End If

                imb.Visible = True
            Case "D"
                rv.Enabled = True
                imb.Visible = True
                If (CDbl(textbox.Text.Trim) < 0 And CDbl(textbox.Text.Trim) > 100) Then
                    lblMessage.Text = "Nilai Gross Yield adalah " & FormatNumber(textbox.Text.Trim, 2) & ", Gross Yield harus >=0 dan <= 100!"
                    Me.Status = True
                End If
        End Select
    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()

        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_EDIT_FINANCIAL)
        Me.ApplicationID = cookie.Values("id")
        Me.CustomerID = cookie.Values("Custid")
        Me.CustName = cookie.Values("name")
        Me.InterestType = cookie.Values("InterestType")
        Me.InstallmentScheme = cookie.Values("InstallmentScheme")
    End Sub
#End Region
#Region "ImbCalculate"
    Private Sub imbCalcInst_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCalcInst.Click
        Dim oEntities As New Parameter.FinancialData
        Dim Inst As Double
        If Me.StepUpDownType = "RL" Or Me.StepUpDownType = "LS" Then
            If CInt(txtCummulative.Text) > CInt(txtNumInst.Text) Then
                lblMessage.Text = "Kumulatif harus < Jangka Waktu Angsuran"
                Exit Sub
            End If

        End If
        If CInt(txtEffectiveRate.Text) <= 0 Then
            lblMessage.Text = "Bunga Efective harus lebih besar dari 0"
            Exit Sub
        End If
        imbRecalcEffRate.Enabled = True
        imbRecalcEffRate.Visible = True

        If Me.InstallmentScheme = "RF" Then
            'lblInstScheme.Text = "Regular Fixed Installment Scheme"
            Try
                Inst = InstAmt(CDbl(txtEffectiveRate.Text))
                'txtSupplierRate.Text = txtEffectiveRate.Text.Trim
                txtInstAmt.Text = (Math.Ceiling(Math.Round(Inst, 0) / 1000) * 1000).ToString
                FlatRate()

            Catch ex As Exception
                lblMessage.Text = "Bunga Effective Salah"
            End Try
        ElseIf Me.InstallmentScheme = "IR" Then
            'lblInstScheme.Text = "Irregular Installment Scheme"
            BuatTabelInstallment()
            imbSave.Enabled = True
            imbSave.Visible = True
            txtInstAmt.Enabled = True
        ElseIf Me.InstallmentScheme = "ST" Then
            'lblInstScheme.Text = "Step Up / Step Down Installment Scheme"
            If Me.StepUpDownType = "NM" Then
                BuatTabelInstallment()
            ElseIf Me.StepUpDownType = "RL" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.CummulativeStart = CInt(txtCummulative.Text)
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                Entities = m_controller.GetInstAmtStepUpDownRegLeasing(Entities)
                txtInstAmt.Text = Math.Round(Entities.InstallmentAmount, 0).ToString.Trim
                imbSave.Enabled = True
                imbSave.Visible = True
                txtInstAmt.Enabled = True
            Else
                BuatTabelInstallment()
            End If
            '--- view installment ----'
            If Me.StepUpDownType <> "NM" And Me.StepUpDownType <> "RS" And Me.StepUpDownType <> "LS" Then

                ViewInstallment()
            End If
        Else
            'lblInstScheme.Text = "Even Principle Installment Scheme"
            Try
                With oEntities
                    .NTF = CDbl(lblNTF.Text)
                    .NumOfInstallment = CInt(txtNumInst.Text)
                    .GracePeriod = CInt(txtGracePeriod.Text.Trim)
                End With
                oEntities = m_controller.GetPrincipleAmount(oEntities)
                Me.PrincipleAmount = oEntities.Principal
            Catch ex As Exception
                lblMessage.Text = "Pokok Salah"
            End Try
        End If
    End Sub

    Private Function IsValidValidation(ByVal textbox As TextBox, ByVal CompareText As TextBox, ByVal Message As String) As Boolean
        If textbox.Text.Trim = "" Then
            lblMessage.Text = Message & " Harus diisi"
            Return False
        ElseIf CDbl(textbox.Text) = 0 Then
            lblMessage.Text = Message & " harus > 0 "
            Return False
        ElseIf CDbl(textbox.Text) > CDbl(CompareText.Text) Then
            lblMessage.Text = Message & "Harus < Jangka waktu Angsuran"
            Return False
        End If
        Return True
    End Function


    Private Sub imbRecalcEffRate_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbRecalcEffRate.Click
        If CInt(txtInstAmt.Text) <= 0 Then
            lblMessage.Text = "Jumlah Angsuran harus > 0"
            Exit Sub
        End If
        Dim Eff As Double
        Dim Supp As Double
        Try
            If Me.InstallmentScheme = "IR" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.MydataSet = GetEntryInstallment()
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Eff = m_controller.GetNewEffRateIRR(Entities)
                txtEffectiveRate.Text = Eff.ToString.Trim
            ElseIf Me.InstallmentScheme = "ST" Then
                If Me.StepUpDownType = "NM" Then
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme
                    Entities.MydataSet = GetEntryInstallmentStepUpDown()
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Eff = m_controller.GetNewEffRateIRR(Entities)
                ElseIf Me.StepUpDownType = "RL" Then
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateRegLeasing(Entities)
                Else
                    If IsValidValidation(txtStep, txtNumInst, "Number Of Step ") = False Then
                        Exit Sub
                    End If
                    If IsValidValidation(txtCummulative, txtNumInst, "Cummulative ") = False Then
                        Exit Sub
                    End If
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.InstallmentScheme = Me.InstallmentScheme
                    Entities.MydataSet = GetEntryInstallmentStepUpDown()
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Eff = m_controller.GetNewEffRateLeasing(Entities)
                End If

                txtEffectiveRate.Text = Eff.ToString.Trim
            Else
                'count EffectiveRate
                Eff = CountRate(CDbl(lblNTF.Text))
                Context.Trace.Write("Eff = " & Eff)
                txtEffectiveRate.Text = Eff.ToString.Trim

                'count SupplierRate
                Supp = CountRate(CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim))
                context.Trace.Write("Supp = " & Supp)
                txtSupplierRate.Text = CStr(CDbl(Supp))
                FlatRate()
            End If

        Catch ex As Exception
            lblMessage.Text = "Jumlah Angsuran Salah"
        End Try
    End Sub
    Function InstAmt(ByVal Rate As Double) As Double
        Dim ReturnVal As Double

        RunRate = (Rate / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))

        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = Math.Round(m_controller.GetInstAmtAdv(RunRate, CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInst.Text) Then
                    lblMessage.Text = "Grace Period harus < Jangka Waktu Angsuran"
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtNumInst.Text) - CInt(txtGracePeriod.Text.Trim), _
                    CType(lblNTF.Text, Double)), 0)
                ElseIf cboGracePeriod.SelectedValue = "R" And CInt(txtGracePeriod.Text) > 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArrRO(CDbl(txtEffectiveRate.Text), CInt(txtNumInst.Text), _
                    RunRate, CType(lblNTF.Text, Double), _
                    CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim)), 0)
                ElseIf CInt(txtGracePeriod.Text) = 0 Then
                    ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                    CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
                End If
            Else
                ReturnVal = Math.Round(m_controller.GetInstAmtArr(RunRate, CInt(cboPaymentFreq.SelectedValue), _
                CInt(txtNumInst.Text), CType(lblNTF.Text, Double)), 0)
            End If
        End If
        Return ReturnVal
    End Function
    'Nama sebelumnya adalah EffRate, kemudian diganti dengan CountRate
    Function CountRate(ByVal NTF As Double) As Double
        Dim ReturnVal As Double
        If cboFirstInstallment.SelectedValue = "AD" Then
            ReturnVal = Math.Round(m_controller.GetEffectiveRateAdv(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue)), 5)
            Context.Trace.Write("Bila Value First Installment sama dengan AD =" & ReturnVal)
        Else
            If txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "" Then
                If CInt(txtGracePeriod.Text.Trim) > CInt(txtNumInst.Text) Then
                    lblMessage.Text = "Grace Period harus < Jangka Waktu Angsuran"
                    Exit Function
                End If
                If cboGracePeriod.SelectedValue = "I" Then
                    ReturnVal = Math.Round(m_controller.GetEffectiveRateArr(CInt(txtNumInst.Text) - CInt(txtGracePeriod.Text.Trim), CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue)), 5)
                Else
                    ReturnVal = Math.Round(m_controller.GetEffectiveRateRO(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text.Trim)), 5)
                End If
            Else
                ReturnVal = Math.Round(m_controller.GetEffectiveRateArr(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), NTF, CInt(cboPaymentFreq.SelectedValue)), 5)
            End If
        End If
        Return ReturnVal
    End Function
    Private Sub FlatRate()
        Dim FlateRate As Double
        FlateRate = 100 * ((CDbl(txtInstAmt.Text) * CDbl(lblTenor.Text)) - CDbl(lblNTF.Text.Trim)) / CDbl(lblNTF.Text.Trim)
        lblFlatRate.Text = FormatNumber(FlateRate, 2) & "% per " & lblTenor.Text & " month "
        lblFlatRate.Visible = True
    End Sub
#End Region
#Region "Check_GrossYeildRate"
    Private Sub Check_GrossYeildRate()
        Dim oFinancialData As New Parameter.FinancialData
        Dim dtEntity As New DataTable

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString
        oFinancialData.SpName = "spFinancialData_002"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)
        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.listdata
        End If

        Dim strPOGrossYieldRate As String
        Dim strPOGrossYieldRateBehaviour As String
        Dim strPBGrossYieldRate As String
        Dim strPBGrossYieldRateBehaviour As String
        Dim strPGrossYieldRate As String
        Dim strPGrossYieldRateBehaviour As String

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)

            txtGrossYieldRate.Text = CType(GetGrossYield(), String)
            strPGrossYieldRate = objrow.Item(17).ToString.Trim
            strPGrossYieldRateBehaviour = objrow.Item(18).ToString.Trim

            strPBGrossYieldRate = objrow.Item(19).ToString.Trim
            strPBGrossYieldRateBehaviour = objrow.Item(20).ToString.Trim

            strPOGrossYieldRate = objrow.Item(21).ToString.Trim
            strPOGrossYieldRateBehaviour = objrow.Item(22).ToString.Trim

            BehaviourPersenGY("ProductOffering", txtGrossYieldRate, strPOGrossYieldRate, RVPOEffectiveRate, strPOGrossYieldRateBehaviour, imbCalcInst)
            If Me.Status = True Then
                Exit Sub
            End If

            BehaviourPersenGY("ProductBranch", txtGrossYieldRate, strPBGrossYieldRate, RVPBEffectiveRate, strPBGrossYieldRateBehaviour, imbCalcInst)
            If Me.Status = True Then
                Exit Sub
            End If

            BehaviourPersenGY("Product", txtGrossYieldRate, strPGrossYieldRate, RVPEffectiveRate, strPGrossYieldRateBehaviour, imbCalcInst)
            If Me.Status = True Then
                Exit Sub
            End If
        End If
    End Sub
#End Region
#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim FlatRate As Double
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData

        If txtSupplierRate.Text.Trim = "" Then
            lblMessage.Text = "Harap Klik tombol Recalculate Effective Rate untuk menghitung Bunga Supplier"
            Exit Sub
        End If

        Me.Status = False
        If Me.NTFGrossYield <= 0 Then
            lblMessage.Text = "Simpan Data gagal, NTF untuk Gross Yield <= 0, NTF Gross Yield sekarang adalah  = " & Me.NTFGrossYield & ", Diff Rate = " & Me.DiffRate
            Exit Sub
        End If
        If Me.InstallmentScheme = "RF" Then
            'If CDbl(txtInstAmt.Text) <> InstAmt(CDbl(txtEffectiveRate.Text)) Then
            If CDbl(txtInstAmt.Text) <> Math.Ceiling(Math.Round(InstAmt(CDbl(txtEffectiveRate.Text)), 0) / 1000) * 1000 Then
                lblMessage.Text = "Jumlah Angsuran dan Bunga Effective tidak syncronized. Harap Klik tombol Calculate Installment untuk menghitung ulang Bunga Effective"
                Exit Sub
            End If

            If cboFirstInstallment.SelectedValue = "AD" Then
                oFinancialData.MydataSet = MakeAmortTable1(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue))
            ElseIf cboFirstInstallment.SelectedValue = "AR" Then
                If (txtGracePeriod.Text.Trim = "0" Or txtGracePeriod.Text.Trim = "") Then
                    oFinancialData.MydataSet = MakeAmortTable2(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue))
                ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "I" Then
                    oFinancialData.MydataSet = MakeAmortTable6(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                ElseIf (txtGracePeriod.Text.Trim <> "0" Or txtGracePeriod.Text.Trim <> "") And cboGracePeriod.SelectedValue = "R" Then
                    oFinancialData.MydataSet = MakeAmortTable3(CInt(txtNumInst.Text), CDbl(txtInstAmt.Text), CDbl(lblNTF.Text), CDbl(txtEffectiveRate.Text), CInt(cboPaymentFreq.SelectedValue), CInt(txtGracePeriod.Text))
                End If
            End If
        End If

        If Me.InstallmentScheme = "RF" Then  'untuk regular  
            If (txtSupplierRate.Text.Trim <> CStr(CountRate(CDbl(lblNTF.Text) - CDbl(txtAmount.Text.Trim)))) Then
                lblMessage.Text = "Subsidi/Komisi dan Bunga Supplier tidak syncronized. harap klik tombol Recalculate Effective"
                Exit Sub
            End If
            Check_GrossYeildRate()
            If Me.Status = True Then
                Exit Sub
            End If
            oFinancialData.DiffRate = CDbl(txtAmount.Text.Trim)
            oFinancialData.GrossYield = GetGrossYield()

            If HitungUlangSisa Then
                dblInterestTotal = Math.Round(dblInterestTotal + SisaPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
            If HitungUlangKurang Then
                dblInterestTotal = Math.Round(dblInterestTotal - KurangPrincipal, 2)
                oFinancialData.InterestTotal = dblInterestTotal
            End If
        End If


        If dblInterestTotal < Me.RejectMinimumIncome Then
            lblMessage.Text = "Interest Total < Reject Minimum Income! Process tidak dapat dilanjutkan"
        Else
            If Me.InstallmentScheme = "RF" Then  'untuk regular    
                FlatRate = Math.Round(GetFlatRate(dblInterestTotal, CDbl(lblNTF.Text), CInt(cboPaymentFreq.SelectedValue), CInt(lblTenor.Text)), 5)
                oFinancialData.FlatRate = FlatRate
                oFinancialData.InstallmentAmount = CDbl(txtInstAmt.Text)
                oFinancialData.NumOfInstallment = CInt(txtNumInst.Text)
                oFinancialData.FloatingPeriod = CType(IIf(Me.InterestType = "FL", cboFloatingPeriod.SelectedValue.ToString, ""), String)
            ElseIf Me.InstallmentScheme = "IR" Then
                oFinancialData.MydataSet = GetEntryInstallment()
                oFinancialData.IsSave = 1
            ElseIf Me.InstallmentScheme = "ST" Then
                If Me.StepUpDownType = "NM" Then
                    oFinancialData.MydataSet = GetEntryInstallmentStepUpDown()
                ElseIf Me.StepUpDownType = "RL" Then
                    Entities.NTF = CDbl(lblNTF.Text)
                    Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                    Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                    Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                    Entities.CummulativeStart = CInt(txtCummulative.Text)
                    Entities.NumOfInstallment = CInt(txtNumInst.Text)
                    Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                    Entities = m_controller.GetStepUpDownRegLeasingTBL(Entities)
                    oFinancialData.MydataSet = Entities.MydataSet
                Else
                    oFinancialData.MydataSet = GetInstallmentAmountStepUpDownLeasingTBL()
                End If

            End If

            Me.DiffRate = CDbl(txtAmount.Text)

            oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
            oFinancialData.AppID = Me.ApplicationID
            oFinancialData.NTF = CDbl(lblNTF.Text)
            oFinancialData.EffectiveRate = CDbl(txtEffectiveRate.Text)
            oFinancialData.SupplierRate = CDbl(IIf(txtSupplierRate.Text.Trim = "", "0", txtSupplierRate.Text.Trim))
            oFinancialData.PaymentFrequency = cboPaymentFreq.SelectedValue
            oFinancialData.FirstInstallment = cboFirstInstallment.SelectedValue
            oFinancialData.Tenor = CInt(lblTenor.Text)
            oFinancialData.GracePeriod = CInt(IIf(txtGracePeriod2.Text.Trim = "", "0", txtGracePeriod2.Text.Trim))
            oFinancialData.GracePeriodType = cboGracePeriod.SelectedValue
            oFinancialData.BusDate = Me.BusinessDate
            oFinancialData.DiffRate = Me.DiffRate
            oFinancialData.AdministrationFee = CDbl(txtAdminFee.Text)
            oFinancialData.ProvisionFee = CDbl(txtProvisi.Text)
            oFinancialData.Flag = "Edit"
            oFinancialData.strConnection = GetConnectionString


            Try
                If Me.InstallmentScheme = "RF" Then
                    oFinancialData = m_controller.SaveFinancialData(oFinancialData)
                ElseIf Me.InstallmentScheme = "IR" Then
                    oFinancialData = m_controller.SaveAmortisasiIRR(oFinancialData)
                ElseIf Me.InstallmentScheme = "ST" Then
                    oFinancialData = m_controller.SaveAmortisasiStepUpDown(oFinancialData)
                Else
                    oFinancialData = m_controller.SaveAmortisasi(oFinancialData)
                End If

            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                err.WriteLog("FinancialData_002.aspx", "imbSave_Click", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            End Try

            If oFinancialData.Output = "" Then
                CheckAgreementIsIncentiveSupplier()
                Response.Redirect("ApplicationMaintenance.aspx")
            Else
                lblMessage.Text = "Error: Proses Simpan Data " & oFinancialData.Output & ""
            End If
        End If

    End Sub
#End Region
#Region "View Installment Step Up Down"
    Private Sub ViewInstallment()
        Dim oFinancialData As New Parameter.FinancialData
        If Me.InstallmentScheme = "ST" Then
            If Me.StepUpDownType = "NM" Then
                oFinancialData.MydataSet = GetEntryInstallmentStepUpDown()
            ElseIf Me.StepUpDownType = "RL" Then
                Entities.NTF = CDbl(lblNTF.Text)
                Entities.EffectiveRate = CDbl(txtEffectiveRate.Text)
                Entities.PaymentFrequency = cboPaymentFreq.SelectedValue
                Entities.FirstInstallment = cboFirstInstallment.SelectedValue
                Entities.CummulativeStart = CInt(txtCummulative.Text)
                Entities.NumOfInstallment = CInt(txtNumInst.Text)
                Entities.InstallmentAmount = CDbl(txtInstAmt.Text)
                Entities = m_controller.GetStepUpDownRegLeasingTBL(Entities)
                oFinancialData.MydataSet = Entities.MydataSet
            Else
                oFinancialData.MydataSet = GetInstallmentAmountStepUpDownLeasingTBL()
            End If
        End If
        pnlViewST.Visible = True

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.NTF = CDbl(lblNTF.Text)
        oFinancialData.EffectiveRate = CDbl(txtEffectiveRate.Text)
        oFinancialData.SupplierRate = CDbl(IIf(txtSupplierRate.Text.Trim = "", "0", txtSupplierRate.Text.Trim))
        oFinancialData.PaymentFrequency = cboPaymentFreq.SelectedValue
        oFinancialData.FirstInstallment = cboFirstInstallment.SelectedValue
        oFinancialData.Tenor = CInt(lblTenor.Text)
        oFinancialData.GracePeriod = CInt(IIf(txtGracePeriod2.Text.Trim = "", "0", txtGracePeriod2.Text.Trim))
        oFinancialData.GracePeriodType = cboGracePeriod.SelectedValue
        oFinancialData.BusDate = Me.BusinessDate
        oFinancialData.DiffRate = Me.DiffRate
        oFinancialData.Flag = "Edit"
        oFinancialData.strConnection = GetConnectionString
        oFinancialData = m_controller.ListAmortisasiStepUpDown(oFinancialData)
        dtgViewInstallment.DataSource = oFinancialData.listdata
        dtgViewInstallment.DataBind()
    End Sub
#End Region
#Region "DiffRate, GrossYield, FlatRate"
    Function DiffRateNormal(ByVal NumInst As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(txtSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate
        For i = 0 To NumInst - 1
            InstallmentAmt(i) = (InstAmtEffective - InstAmtSupp)
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
    End Function
    Function DiffRateRO(ByVal NumInst As Integer, ByVal GracePeriod As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(txtSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate
        For i = 0 To GracePeriod - 1
            InstallmentAmt(i) = 0
        Next i
        For i = GracePeriod To NumInst - 1
            InstallmentAmt(i) = InstAmtEffective - InstAmtSupp
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
    End Function
    Function DiffRateI(ByVal NumInst As Integer, ByVal GracePeriod As Integer) As Double
        Dim InstallmentAmt(NumInst - 1) As Double
        Dim InstAmtEffective As Double = InstAmt(CDbl(txtEffectiveRate.Text))
        Dim InstAmtSupp As Double = InstAmt(CDbl(txtSupplierRate.Text))
        Dim RunRateSupp As Double = RunRate
        For i = 0 To GracePeriod - 1
            InstallmentAmt(i) = InstAmtEffective - (Math.Round(CDbl(lblNTF.Text) * RunRateSupp / m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) / 100, 2))
        Next i
        For i = 0 To NumInst - 1
            InstallmentAmt(i) = InstAmtEffective - InstAmtSupp
        Next i
        Return NPV(RunRateSupp, InstallmentAmt)
        'Return InstAmtSupp
    End Function
    Function GetGrossYield() As Double
        Me.NTFGrossYield = Me.NTFGrossYield + Me.DiffRate
        Return CountRate(Me.NTFGrossYield)
    End Function
    Public Function GetFlatRate(ByVal dblInterestTotal As Double, ByVal dblNTF As Double, ByVal intPayFreq As Integer, ByVal intNumInstallment As Integer) As Double
        Return (dblInterestTotal * 1200) / (dblNTF * CDbl(lblTenor.Text))
    End Function
#End Region
#Region "Make Amortization Table"
    Public Sub BuatTable()
        ' Create new DataColumn, set DataType, ColumnName and add to DataTable.
        myDataTable = New DataTable("AmortTable")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Installment"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Principal"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Interest"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincBalance"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "PrincInterest"
        myDataTable.Columns.Add(myDataColumn)

        'myDataColumn = New DataColumn
        'myDataColumn.DataType = System.Type.GetType("System.Double")
        'myDataColumn.ColumnName = "InterestTotal"
        'myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "InstallmentSisa"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepFrom"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "StepTo"
        myDataTable.Columns.Add(myDataColumn)
    End Sub
    Private Sub BuatTabelInstallment()
        Dim inc As Integer
        Dim intRowMax As Integer
        pnlEntryInstallment.Visible = True
        intRowMax = CInt(txtNumInst.Text) - 1
        If Me.InstallmentScheme = "ST" Then
            If CInt(txtStep.Text) > CInt(txtNumInst.Text) Then
                lblMessage.Text = "Number of Step harus < Jangka Waktu Angsuran"
                pnlEntryInstallment.Visible = False

                Exit Sub
            End If

            pnlEntryInstallment.Visible = True
            intRowMax = CInt(txtStep.Text) - 1
        End If
        myDataTable = New DataTable("Installment")

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)


        For inc = 1 To intRowMax
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("NoOfStep") = 0
            myDataRow("Amount") = 0
            myDataTable.Rows.Add(myDataRow)
        Next inc
        If Me.InstallmentScheme = "ST" Then
            dtgEntryInstallment.Columns(1).Visible = True
        Else
            dtgEntryInstallment.Columns(1).Visible = False
        End If
        dtgEntryInstallment.DataSource = myDataTable.DefaultView
        dtgEntryInstallment.DataBind()

    End Sub
    Public Function MakeAmortTable1(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Advance - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        PokokHutang(0) = dblNTF
        Bunga(0) = 0
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Advance dan installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i = 2 Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = 0
                myDataRow("Principal") = dblInstAmt
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Public Function MakeAmortTable2(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer) As DataSet
        'Regular Installment - Arrear - No Grace Period
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim TotalPrincipal As Double
        Bunga(0) = 0
        PokokHutang(0) = dblNTF
        BuatTable()
        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            ElseIf i = intNumInstallment + 1 Then
                myDataRow("PrincBalance") = 0
            Else
                myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)

        Dim custRow As DataRow

        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next

        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Public Function MakeAmortTable3(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Roll Over)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = 0
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(CDbl(myDataRow("Installment")) - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next
        Dim myDataSet As DataSet
        myDataSet = New DataSet
        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
    Public Function MakeAmortTable6(ByVal intNumInstallment As Integer, ByVal dblInstAmt As Double, ByVal dblNTF As Double, ByVal dblRunRate As Double, ByVal intPayFreq As Integer, ByVal intGrace As Integer) As DataSet
        'Regular Installment - Arrear - Grace Period (Interest Only)
        ReDim PokokHutang(intNumInstallment + 1)
        ReDim Bunga(intNumInstallment + 1)
        ReDim PokokBunga(intNumInstallment + 1)
        Dim intGracePeriod As Integer = intGrace + 1
        Dim TotalPrincipal As Double
        Dim SisaPrincipal As Double
        Dim KurangPrincipal As Double
        Dim HitungUlangSisa As Boolean = False
        Dim HitungUlangKurang As Boolean = False

        Bunga(0) = 0
        PokokHutang(0) = dblNTF

        BuatTable()

        For i = 1 To intNumInstallment + 1
            myDataRow = myDataTable.NewRow()
            If i = 1 Then 'Installment I
                myDataRow("No") = 0
                myDataRow("Installment") = 0
                myDataRow("Principal") = 0
                myDataRow("Interest") = 0
            ElseIf i <= intGracePeriod Then
                myDataRow("No") = i - 1
                myDataRow("Installment") = Math.Round(dblNTF * dblRunRate / m_controller.GetTerm(intPayFreq) / 100, 2)
                myDataRow("Interest") = myDataRow("Installment")
                myDataRow("Principal") = 0
            ElseIf i = intGracePeriod + 1 Then
                myDataRow("No") = i - 1
                myDataRow("Interest") = Math.Round(dblNTF * dblRunRate / m_controller.GetTerm(intPayFreq) / 100, 2)
                myDataRow("Installment") = dblInstAmt
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            Else
                myDataRow("No") = i - 1
                myDataRow("Installment") = dblInstAmt
                myDataRow("Interest") = Math.Round((PokokHutang(i - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                myDataRow("Principal") = Math.Round(dblInstAmt - CDbl(myDataRow("Interest")), 2)
            End If
            If i = 1 Then
                myDataRow("PrincBalance") = PokokHutang(0)
            Else
                If i = intNumInstallment + 1 Then
                    myDataRow("PrincBalance") = 0
                Else
                    myDataRow("PrincBalance") = Math.Round(PokokHutang(i - 1) - CDbl(myDataRow("Principal")), 2)
                End If
            End If
            myDataRow("PrincInterest") = 0
            PokokHutang(i) = CDbl(myDataRow("PrincBalance"))
            Bunga(i) = CDbl(myDataRow("Interest"))
            TotalPrincipal = Math.Round(TotalPrincipal + CDbl(myDataRow("Principal")), 2)
            myDataTable.Rows.Add(myDataRow)
        Next i

        If TotalPrincipal > dblNTF Then
            SisaPrincipal = Math.Round(TotalPrincipal - dblNTF, 2)
        Else
            KurangPrincipal = Math.Round(dblNTF - TotalPrincipal, 2)
        End If

        If SisaPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) + SisaPrincipal, 2)
        End If
        If KurangPrincipal > 0 Then
            Bunga(intNumInstallment + 1) = Math.Round(Bunga(intNumInstallment + 1) - KurangPrincipal, 2)
        End If

        PokokBunga(intNumInstallment + 1) = 0

        For i = intNumInstallment To 0 Step -1
            PokokBunga(i) = Math.Round(Bunga(i + 1) + PokokBunga(i + 1), 2)
            dblInterestTotal = Math.Round(dblInterestTotal + Bunga(i + 1), 2)
        Next

        Dim myDataSet As DataSet
        myDataSet = New DataSet

        myDataSet.Tables.Add(myDataTable)
        Dim custRow As DataRow
        i = 1
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If i <= intNumInstallment + 1 Then
                custRow("PrincInterest") = PokokBunga(i)
                'custRow("InterestTotal") = dblInterestTotal
                i = i + 1
            End If
        Next
        For Each custRow In myDataSet.Tables("AmortTable").Rows
            If CInt(custRow("No")) = intNumInstallment Then
                If SisaPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) - SisaPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) + SisaPrincipal, 2)
                    HitungUlangSisa = True
                End If
                If KurangPrincipal > 0 Then
                    custRow("Principal") = Math.Round(CDbl(custRow("Principal")) + KurangPrincipal, 2)
                    custRow("Interest") = Math.Round(CDbl(custRow("Interest")) - KurangPrincipal, 2)
                    HitungUlangKurang = True
                End If
            End If
        Next
        Return myDataSet
    End Function
#End Region
#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("ApplicationMaintenance.aspx")
    End Sub
#End Region
#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As New HttpCookie("Incentive")
        cookie.Values.Add("ApplicationID", Me.ApplicationID)
        cookie.Values.Add("CustomerID", Me.CustomerID)
        cookie.Values.Add("CustomerName", Me.CustName)

        Response.AppendCookie(cookie)
    End Sub
#End Region
#Region "Entry Installment Irregular"
    Private Function GetEntryInstallment() As DataSet
        Dim inc As Integer
        Dim jmlRow As Integer
        Dim InstallmentAmount As Double
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.FinancialData

        jmlRow = dtgEntryInstallment.Items.Count
        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "NoOfStep"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc
            myDataRow("NoOfStep") = CDbl(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        objDs.Tables.Add(myDataTable)
        Return objDs
    End Function
    Private Function GetEntryInstallmentStepUpDown() As DataSet
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim objDs As New DataSet
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)

        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
                inc3 = inc3 + 1
            Next
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        InstallmentAmount = CDbl(txtInstAmt.Text)
        If inc3 <= CInt(txtNumInst.Text) Then
            For inc = 1 To intSisa
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
                inc3 = inc3 + 1
            Next
        End If
        objDs.Tables.Add(myDataTable)
        Return objDs
    End Function

    Private Function GetInstallmentAmountStepUpDown() As Double
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(lblNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
            Next
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1 '((PokokHutang(0) - TotalInstallment) + TotalInterest) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        Return InstallmentAmount
    End Function
    Private Function GetInstallmentAmountStepUpDownLeasing() As Double
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(lblNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
            Next
        Next
        intSisa = (CInt(txtCummulative.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        Return InstallmentAmount
    End Function
    Private Function GetInstallmentAmountStepUpDownLeasingTBL() As DataSet
        Dim inc As Integer
        Dim inc2 As Integer
        Dim inc3 As Integer
        Dim intSisa As Integer
        Dim jmlRow As Integer
        Dim jmlRow2 As Integer
        Dim InstallmentAmount As Double
        Dim dblRunRate As Double
        Dim dblPokokHutangST As Double
        Dim intPayFreq As Integer
        Dim intNumOfInst As Integer
        Dim oFinancialData As New Parameter.FinancialData
        myDataTable = New DataTable
        jmlRow = dtgEntryInstallment.Items.Count
        intNumOfInst = CInt(txtNumInst.Text)
        Dim PokokHutang(intNumOfInst) As Double
        Dim Interest(intNumOfInst) As Double
        Dim Principal(intNumOfInst) As Double
        dblRunRate = CDbl(txtEffectiveRate.Text)
        intPayFreq = CInt(cboPaymentFreq.SelectedValue)
        PokokHutang(0) = CDbl(lblNTF.Text)
        Principal(0) = 0
        Interest(0) = 0
        inc3 = 0
        myDataTable = New DataTable("Installment")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Int32")
        myDataColumn.ColumnName = "No"
        myDataTable.Columns.Add(myDataColumn)

        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.Double")
        myDataColumn.ColumnName = "Amount"
        myDataTable.Columns.Add(myDataColumn)

        For inc = 0 To jmlRow - 1
            Dim txtEntryInstallment As New TextBox
            jmlRow2 = CInt(CType(dtgEntryInstallment.Items(inc).FindControl("txtNoStep"), TextBox).Text)
            txtEntryInstallment = CType(dtgEntryInstallment.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            InstallmentAmount = CDbl(txtEntryInstallment.Text)
            For inc2 = 1 To jmlRow2
                inc3 = inc3 + 1
                If inc3 = 1 And cboFirstInstallment.SelectedValue = "AD" Then
                    Interest(inc3) = 0
                Else
                    Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
                End If
                Principal(inc3) = InstallmentAmount - Interest(inc3)
                PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)
                myDataRow = myDataTable.NewRow()
                myDataRow("No") = inc3
                myDataRow("Amount") = InstallmentAmount
                myDataTable.Rows.Add(myDataRow)
            Next
        Next
        intSisa = (CInt(txtCummulative.Text) - inc3)
        RunRate = (CDbl(txtEffectiveRate.Text) / (m_controller.GetTerm(CInt(cboPaymentFreq.SelectedValue)) * 100))
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        For inc = 1 To intSisa - 1
            inc3 = inc3 + 1
            Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
            Principal(inc3) = InstallmentAmount - Interest(inc3)
            PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)

            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc3
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        intSisa = (CInt(txtNumInst.Text) - inc3)
        dblPokokHutangST = PokokHutang(inc3) * -1
        If cboFirstInstallment.SelectedValue = "AD" Then
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.BegOfPeriod)
        Else
            InstallmentAmount = Pmt(RunRate, intSisa, dblPokokHutangST, 0, DueDate.EndOfPeriod)
        End If
        For inc = 1 To intSisa
            inc3 = inc3 + 1
            Interest(inc3) = Math.Round((PokokHutang(inc3 - 1) * dblRunRate) / (m_controller.GetTerm(intPayFreq) * 100), 2)
            Principal(inc3) = InstallmentAmount - Interest(inc3)
            PokokHutang(inc3) = PokokHutang(inc3 - 1) - Principal(inc3)

            myDataRow = myDataTable.NewRow()
            myDataRow("No") = inc3
            myDataRow("Amount") = InstallmentAmount
            myDataTable.Rows.Add(myDataRow)
        Next
        myDataSet.Tables.Add(myDataTable)
        Return myDataSet
    End Function

    Private Sub ImgEntryInstallment_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgEntryInstallment.Click
        Dim oFinancialData As New Parameter.FinancialData
        Dim oDs As New DataSet
        If Me.InstallmentScheme = "ST" Then
            If IsValidEntry(dtgEntryInstallment, CDbl(lblNTF.Text), CInt(txtNumInst.Text), CInt(txtCummulative.Text), Me.StepUpDownType) = False Then
                lblMessage.Text = "Jumlah Angsuran Salah"
                Exit Sub
            End If
            If Me.StepUpDownType = "NM" Then
                txtInstAmt.Text = Math.Round(GetInstallmentAmountStepUpDown(), 0).ToString.Trim
            Else
                txtInstAmt.Text = Math.Round(GetInstallmentAmountStepUpDownLeasing(), 0).ToString.Trim
            End If
            imbSave.Enabled = True
            imbSave.Visible = True
            txtInstAmt.Enabled = True
        Else
            With oFinancialData
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .AppID = Me.ApplicationID
                .NTF = CDbl(lblNTF.Text)
                .EffectiveRate = CDbl(txtEffectiveRate.Text)
                .SupplierRate = CDbl(IIf(txtSupplierRate.Text.Trim = "", "0", txtSupplierRate.Text.Trim))
                .PaymentFrequency = cboPaymentFreq.SelectedValue
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .Tenor = CInt(lblTenor.Text)
                .GracePeriod = CInt(IIf(txtGracePeriod2.Text.Trim = "", "0", txtGracePeriod2.Text.Trim))
                .GracePeriodType = cboGracePeriod.SelectedValue
                .BusDate = Me.BusinessDate
                .DiffRate = Me.DiffRate
                .strConnection = GetConnectionString
                .MydataSet = GetEntryInstallment()
                .IsSave = 0
            End With
            oFinancialData = m_controller.SaveAmortisasiIRR(oFinancialData)
            txtInstAmt.Text = Math.Round(oFinancialData.InstallmentAmount, 0).ToString.Trim
        End If
        Call ViewInstallment()
    End Sub

    Private Function IsValidEntry(ByVal Dg As DataGrid, ByVal NTF As Double, ByVal NumOfIns As Integer, ByVal Cummulative As Integer, ByVal StepUpDownType As String) As Boolean
        Dim inc As Integer
        Dim intStep As Integer
        Dim intTotStep As Integer
        Dim dblAmount As Double
        Dim dblTotAmount As Double
        intTotStep = 0
        dblTotAmount = 0
        For inc = 0 To Dg.Items.Count - 1
            Dim txtStep As New TextBox
            Dim txtAmount As New TextBox
            txtStep = CType(Dg.Items(inc).FindControl("txtNoStep"), TextBox)
            txtAmount = CType(Dg.Items(inc).FindControl("txtEntryInstallment"), TextBox)
            If StepUpDownType = "NM" Then
                dblAmount = CDbl(txtAmount.Text)
                dblTotAmount = dblTotAmount + dblAmount
                intStep = CInt(txtStep.Text)
                intTotStep = intTotStep + intStep
            ElseIf StepUpDownType = "RL" Then
                intStep = CInt(txtStep.Text)
                intTotStep = intTotStep + intStep
            Else
                intStep = CInt(txtStep.Text)
                dblAmount = CDbl(txtAmount.Text) * intStep
                intTotStep = intTotStep + intStep
                dblTotAmount = dblTotAmount + dblAmount
            End If
        Next
        If StepUpDownType = "NM" Then
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If dblTotAmount >= NTF Then
                Return False
            End If
        ElseIf StepUpDownType = "RL" Then
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If Cummulative < intTotStep Then
                Return False
            End If
        Else
            If Cummulative < intTotStep Then
                Return False
            End If
            If intTotStep >= NumOfIns Then
                Return False
            End If
            If dblTotAmount >= NTF Then
                Return False
            End If
        End If
        'If intTotStep < Cummulative Then
        '    Return False
        'End If
        Return True
    End Function
#End Region
#Region "CheckAgreementIsIncentiveSupplier"
    Public Sub CheckAgreementIsIncentiveSupplier()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVCheckAgreementCheckIsIncentiveSupplier"
            objCommand.Parameters.Add("@ApplicationId", SqlDbType.VarChar, 20).Value = Me.ApplicationID
            objCommand.Parameters.Add("@IsIncentiveSupplier", SqlDbType.Bit).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.IsIncentiveSupplier = CBool(IIf(IsDBNull(objCommand.Parameters("@IsIncentiveSupplier").Value), "", objCommand.Parameters("@IsIncentiveSupplier").Value))
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

#End Region
    Private Sub imgSaveAdminFee_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSaveAdminFee.Click
        Try
            With Entities
                .strConnection = GetConnectionString
                .AppID = Me.ApplicationID
                .AdministrationFee = CDbl(txtAdminFee.Text)
                .ProvisionFee = CDbl(txtProvisi.Text)
            End With

            m_controller.saveDefaultAdminFee(Entities)

            Response.Redirect("EditFinancialData_002.aspx")
        Catch ex As Exception

            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
End Class