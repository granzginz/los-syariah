﻿#Region "Import"

#End Region

Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class EditAppInsuranceByCust
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oExpiredDate As ValidDate
    Protected WithEvents oCoverageType As UcCoverageType
#Region "Property"
    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return (CType(ViewState("CustomerName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Private Property CoverageTypeID() As String
        Get
            Return (CType(ViewState("CoverageTypeID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CoverageTypeID") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property ExpiredDate() As String
        Get
            Return (CType(ViewState("ExpiredDate"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ExpiredDate") = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return (CType(ViewState("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return (CType(ViewState("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oController As New NewAppInsuranceByCustController

#End Region
#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If


        'If Not CheckForm(Me.Loginid, CommonVariableHelper.FORM_NAME_NEW_APPLICATION_BY_COMPANY, CommonVariableHelper.APPLICATION_NAME) Then
        '    Exit Sub
        'End If

        If Not IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then

                LblErrorMessages.Text = ""
                Me.ApplicationID = Request.QueryString("ApplicationID")
                oExpiredDate.Display = "Dynamic"
                oExpiredDate.FillRequired = False
                oExpiredDate.isCalendarPostBack = False
                oExpiredDate.FieldRequiredMessage = "Harap isi tanggal jatuh tempo"
                oExpiredDate.ValidationErrMessage = "harap isi dengan format dd/MM/yyyy"
                Me.ExpiredDate = oExpiredDate.dateValue

                'oCoverageType.SetValidatorFalse()
                oCoverageType.FillRequired = True
                Dim oNewAppInsByCust As New Parameter.NewAppInsuranceByCust

                With oNewAppInsByCust
                    .ApplicationID = Me.ApplicationID
                    .strConnection = GetConnectionString()
                End With

                Try
                    oNewAppInsByCust = oController.GetInsuredByCustomer(oNewAppInsByCust)
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

                With oNewAppInsByCust
                    LblBranchID.Text = .BranchId
                    LblApplicationID.Text = .ApplicationID
                    LblCustomerName.Text = .CustomerName
                    Me.CustomerName = LblCustomerName.Text
                    LblInsuredByName.Text = .InsAssetInsuredByName
                    LblPaidByName.Text = .InsAssetPaidByName
                    LblMinimumTenor.Text = CType(.MinimumTenor, String)
                    LblMaximumTenor.Text = CType(.MaximumTenor, String)
                    Me.InterestType = .InterestType
                    Me.InstallmentScheme = .InstallmentScheme
                    Me.CustomerID = .CustomerID

                End With
            End If
        End If


    End Sub

#End Region
#Region "Save "


    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOK.Click


        Me.CoverageTypeID = oCoverageType.CoverageTypeID.Trim
        If Me.CoverageTypeID = "0" Then
            Me.CoverageTypeID = "-"
        End If




        If validasi() Then
            'save
            Dim oNewAppInsByCust As New Parameter.NewAppInsuranceByCust

            If TxtAmountCoverage.Text.Trim = "" Then TxtAmountCoverage.Text = "0"

            With oNewAppInsByCust
                .BranchId = LblBranchID.Text
                .ApplicationID = Me.ApplicationID
                .InsuranceCompanyName = TxtInsuranceCompany.Text
                .SumInsured = CType(TxtAmountCoverage.Text, Decimal)
                .CoverageType = Me.CoverageTypeID
                .PolicyNo = TxtPolicyNumber.Text
                .ExpiredDate = ConvertDate2(Me.ExpiredDate)
                .InsNotes = txtinsurancenotes.Text
                .Tenor = CType(TxtTenorCredit.Text, Double)
                .BusinessDate = Me.BusinessDate

                .strConnection = GetConnectionString
            End With

            Try
                oController.ProcessSaveInsuranceByCustomer(oNewAppInsByCust)
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try


            'If CheckBoxEntryFinancial.Checked Then
            '    SendCookies()
            '    Response.Redirect("FinancialData_002.aspx")

            'Else
            '    Response.Redirect("NewAppInsurance.aspx")
            'End If

        Else
            'tidak disave
        End If


    End Sub
#End Region
#Region "Validasi "


    Public Function validasi() As Boolean

        Dim MinimumTenor As Int16
        Dim MaximumTenor As Int16
        Dim TenorEntry As Int16
        MinimumTenor = CType(LblMinimumTenor.Text, Int16)
        MaximumTenor = CType(LblMaximumTenor.Text, Int16)
        TenorEntry = CType(TxtTenorCredit.Text, Int16)


        If TenorEntry < MinimumTenor Or TenorEntry > MaximumTenor Then
            LblErrorMessages.Text = "Jangka Waktu harus diantara  " & CType(LblMinimumTenor.Text, Int16) & " dan " & CType(LblMaximumTenor.Text, Int16)
            Return False
        End If

        If Me.ExpiredDate <> "" Then
            Dim endTenordate As Date
            endTenordate = DateAdd(DateInterval.Month, TenorEntry, Me.BusinessDate)
            If ConvertDate2(Me.ExpiredDate) < endTenordate Then
                LblErrorMessages.Text = "Tanggal jatuh Tempo harus lebih besar dari  " & endTenordate
                Return False
            End If
        Else

            Me.ExpiredDate = "01/01/1900"
        End If

        Return True

    End Function

#End Region
#Region "Cancel "

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        Response.Redirect("NewAppInsurance.aspx")

    End Sub

#End Region
#Region "SendCookies"
    Sub SendCookies()

        Dim cookie As New HttpCookie("Financial")
        cookie.Values.Add("id", Me.ApplicationID)
        cookie.Values.Add("custid", Me.CustomerID)
        cookie.Values.Add("name", Me.CustomerName)
        cookie.Values.Add("EmployeeID", "")
        cookie.Values.Add("InterestType", Me.InterestType)
        cookie.Values.Add("InstallmentScheme", Me.InstallmentScheme)
        Response.AppendCookie(cookie)


    End Sub
#End Region

End Class