﻿#Region "Import"
Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class EditIncentiveData
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property RefundToSupplierAmount() As Decimal
        Get
            Return (CType(viewstate("RefundToSupplierAmount"), Decimal))
        End Get
        Set(ByVal Value As Decimal)
            viewstate("RefundToSupplierAmount") = Value
        End Set
    End Property
    Private Property BID() As String
        Get
            Return CType(viewstate("BID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BID") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
    Private Property SalesSupervisorID() As String
        Get
            Return CType(viewstate("SalesSupervisorID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SalesSupervisorID") = Value
        End Set
    End Property
    Private Property SalesmanID() As String
        Get
            Return CType(viewstate("SalesmanID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SalesmanID") = Value
        End Set
    End Property
    Private Property SupplierAdminID() As String
        Get
            Return CType(viewstate("SupplierAdminID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierAdminID") = Value
        End Set
    End Property
    Private Property AccountNOAM() As String
        Get
            Return CType(viewstate("AccountNOAM"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNOAM") = Value
        End Set
    End Property
    Private Property AccountNOAH() As String
        Get
            Return CType(viewstate("AccountNOAH"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNOAH") = Value
        End Set
    End Property
    Private Property AccountNOSV() As String
        Get
            Return CType(viewstate("AccountNOSV"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNOSV") = Value
        End Set
    End Property
    Private Property AccountNOSL() As String
        Get
            Return CType(viewstate("AccountNOSL"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNOSL") = Value
        End Set
    End Property
    Private Property AccountNOBM() As String
        Get
            Return CType(viewstate("AccountNOBM"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNOBM") = Value
        End Set
    End Property
    Private Property AccountNOGM() As String
        Get
            Return CType(viewstate("AccountNOGM"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountNOGM") = Value
        End Set
    End Property
    Private Property isDataExistsGM() As Boolean
        Get
            Return CType(viewstate("isDataExistsGM"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("isDataExistsGM") = Value
        End Set
    End Property
    Private Property isDataExistsBM() As Boolean
        Get
            Return CType(viewstate("isDataExistsBM"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("isDataExistsBM") = Value
        End Set
    End Property
    Private Property isDataExistsAH() As Boolean
        Get
            Return CType(viewstate("isDataExistsAH"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("isDataExistsAH") = Value
        End Set
    End Property
    Private Property RefundInterest() As Decimal
        Get
            Return CType(viewstate("RefundInterest"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("RefundInterest") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oController As New IncentiveCardController
    Private m_Controller As New SupplierController
    Dim oSupplier As New Parameter.IncentiveCard
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            GetCookies()
            InitialPageLoad()
        End If
    End Sub
#Region "GetCookies"
    Sub GetCookies()
        Dim CustomerID As String
        Dim CustomerName As String
        If Request.QueryString("ApplicationID") <> "" Then Me.ApplicationID = Request.QueryString("ApplicationID")
        If Request.QueryString("CustomerName") <> "" Then CustomerName = Request.QueryString("CustomerName")
        If Request.QueryString("CustomerID") <> "" Then CustomerID = Request.QueryString("CustomerID")
        hyApplicationIDDist.Text = Me.ApplicationID.Trim
        hyApplicationIDDist.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Me.ApplicationID.Trim & "')"
        HyCustDist.Text = CustomerName.Trim
        CustomerID = CustomerID.Trim
        HyCustDist.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(CustomerID.Trim) & "')"
    End Sub
#End Region
#Region "Button Click"
    Private Sub ImbCalculate_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImbCalculate.Click
        pnlAfterExecute.Visible = True
        PnlDistribution.Visible = True
        lblTotal.Text = FormatNumber(CStr(CDec(txtInctvAmountADH.Text) + CDec(txtInctvAmountCompany.Text) + CDec(txtInctvAmountGM.Text) + CDec(txtInctvAmountPM.Text) + CDec(txtInctvAmountSalesPerson.Text) + CDec(txtInctvAmountSalesSpv.Text) + CDec(txtInctvAmountSuppAdm.Text)), 0)
        txtRfnSupplier.Text = FormatNumber(CStr(CDec(txtInctvAmountADH.Text) + CDec(txtInctvAmountCompany.Text) + CDec(txtInctvAmountGM.Text) + CDec(txtInctvAmountPM.Text) + CDec(txtInctvAmountSalesPerson.Text) + CDec(txtInctvAmountSalesSpv.Text) + CDec(txtInctvAmountSuppAdm.Text)), 0)
        Me.RefundInterest = CDec(txtRfnSupplier.Text)
    End Sub
    Private Sub ImbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        InitialPageLoad()
    End Sub
    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        lblMessage.Text = ""
        If (CInt(txtInctvAmountGM.Text) > 0 And Me.AccountNOGM.Trim = "N/A") Or (CInt(txtInctvAmountPM.Text) > 0 And Me.AccountNOBM.Trim = "N/A") Or (CInt(txtInctvAmountADH.Text) > 0 And Me.AccountNOAH.Trim = "N/A") Or (CInt(txtInctvAmountSalesSpv.Text) > 0 And Me.AccountNOSV.Trim = "N/A") Or (CInt(txtInctvAmountSalesPerson.Text) > 0 And Me.AccountNOSL.Trim = "N/A") Or (CInt(txtInctvAmountSuppAdm.Text) > 0 And Me.AccountNOAM.Trim = "N/A") Then
            lblMessage.Text = "Rekening Bank Karyawan Supplier tidak terdaftar"

            Exit Sub
        End If

        If Me.RefundInterest <> (CDec(IIf(txtInctvAmountGM.Text.Trim <> "", txtInctvAmountGM.Text.Trim, "0")) + CDec(IIf(txtInctvAmountCompany.Text.Trim <> "", txtInctvAmountCompany.Text.Trim, "0")) + CDec(IIf(txtInctvAmountPM.Text.Trim <> "", txtInctvAmountPM.Text.Trim, "0")) + CDec(IIf(txtInctvAmountADH.Text.Trim <> "", txtInctvAmountADH.Text.Trim, "0")) + CDec(IIf(txtInctvAmountSalesSpv.Text.Trim <> "", txtInctvAmountSalesSpv.Text.Trim, "0")) + CDec(IIf(txtInctvAmountSalesPerson.Text.Trim <> "", txtInctvAmountSalesPerson.Text.Trim, "0")) + CDec(IIf(txtInctvAmountSuppAdm.Text.Trim <> "", txtInctvAmountSuppAdm.Text.Trim, "0"))) Then
            lblMessage.Text = "Total Incentive tidak sama dengan Refund Premi ke Supplier"

            Exit Sub
        Else
            Try
                With oSupplier
                    .strConnection = GetConnectionString
                    .CompanyAmount = CDec(IIf(txtInctvAmountCompany.Text.Trim <> "", txtInctvAmountCompany.Text.Trim, "0"))
                    .GMAmount = CDec(IIf(txtInctvAmountGM.Text.Trim <> "", txtInctvAmountGM.Text.Trim, "0"))
                    .BMAmount = CDec(IIf(txtInctvAmountPM.Text.Trim <> "", txtInctvAmountPM.Text.Trim, "0"))
                    .ADHAmount = CDec(IIf(txtInctvAmountADH.Text.Trim <> "", txtInctvAmountADH.Text.Trim, "0"))
                    .SPVAmount = CDec(IIf(txtInctvAmountSalesSpv.Text.Trim <> "", txtInctvAmountSalesSpv.Text.Trim, "0"))
                    .SPAmount = CDec(IIf(txtInctvAmountSalesPerson.Text.Trim <> "", txtInctvAmountSalesPerson.Text.Trim, "0"))
                    .ADAmount = CDec(IIf(txtInctvAmountSuppAdm.Text.Trim <> "", txtInctvAmountSuppAdm.Text.Trim, "0"))
                    .SalesmanID = Me.SalesmanID.Trim
                    .SalesSupervisorID = Me.SalesSupervisorID.Trim
                    .SupplierAdminID = Me.SupplierAdminID.Trim
                    .BranchId = Me.BID.Trim
                    .ApplicationID = Me.ApplicationID.Trim
                    .RefundPremi = Me.RefundToSupplierAmount
                    .RefundInterest = Me.RefundInterest
                    .SupplierID = Me.SupplierID.Trim
                    .BusinessDate = Me.BusinessDate
                End With
                oController.SupplierINCTVDailyDEdit(oSupplier)
                InitialPageLoad()
                lblMessage.Text = "Simpan data Berhasil"

                Response.Redirect("ApplicationMaintenance.aspx")
            Catch ex As Exception
                lblMessage.Text = "Error: " & ex.Message & "<br>" & ex.Source

                Exit Sub
            End Try
        End If
    End Sub
    Private Sub ImbCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImbCancel.Click
        'InitialPageLoad()
        Response.Redirect("ApplicationMaintenance.aspx")
    End Sub
#End Region
#Region "Private Sub"
    Private Sub InitialPageLoad()
        pnlAfterExecute.Visible = False
        PnlDistribution.Visible = True
        lblMessage.Text = ""

        GetRefundPremiumToSupplier()
        GetdataFromSupplierIncentiveDailyD()
        GetIncentiveDistributionData()
    End Sub
    Sub GetIncentiveDistributionData()
        Dim oData As New DataTable
        Dim oData2 As New DataTable
        CheckisDataExistsForGM()
        CheckisDataExistsForBM()
        CheckisDataExistsForAH()
        Dim oEntity As New Parameter.Supplier
        Dim oRow As DataRow
        Dim oRow2 As DataRow
        Me.RefundToSupplierAmount = CDec(LblPremiumToSupplier.Text)

        oEntity.strConnection = GetConnectionString
        oEntity.SupplierID = Me.SupplierID
        oEntity.BranchId = Me.BID
        oEntity.WhereCond = Me.ApplicationID
        oEntity = m_Controller.GetSupplierBranchEdit(oEntity)
        oData = oEntity.ListData
        oData2 = oEntity.ListData3
        oRow = oData.Rows(0)
        oRow2 = oData2.Rows(0)
        If oData.Rows.Count > 0 Then
            lblPCompany.Text = oRow(5).ToString.Trim
            lblPGM.Text = oRow(6).ToString.Trim
            lblPBM.Text = oRow(7).ToString.Trim
            lblPADH.Text = oRow(8).ToString.Trim
            lblPSalesSpv.Text = oRow(9).ToString.Trim
            lblPSalesPerson.Text = oRow(10).ToString.Trim
            lblPSuppAdm.Text = oRow(11).ToString.Trim
            lblFGM.Text = oRow(12).ToString.Trim
            lblFBM.Text = oRow(13).ToString.Trim
            lblFADH.Text = oRow(14).ToString.Trim
            lblFSalesSpv.Text = oRow(15).ToString.Trim
            lblFSalesPerson.Text = oRow(16).ToString.Trim
            lblFSuppAdm.Text = oRow(17).ToString.Trim
            If Me.isDataExistsGM = False Then
                txtInctvAmountGM.Enabled = False
                txtInctvAmountGM.Text = "0"
            End If
            If Me.isDataExistsBM = False Then
                txtInctvAmountPM.Enabled = False
                txtInctvAmountPM.Text = "0"
            End If
            If Me.isDataExistsAH = False Then
                txtInctvAmountADH.Enabled = False
                txtInctvAmountADH.Text = "0"
            End If
        End If
        If oData2.Rows.Count > 0 Then
            LblAccountAM.Text = oRow2(0).ToString.Trim
            LblAccountSL.Text = oRow2(1).ToString.Trim
            LblAccountAH.Text = oRow2(2).ToString.Trim
            LblAccountBM.Text = oRow2(3).ToString.Trim
            LblAccountGM.Text = oRow2(4).ToString.Trim
            LblAccountSV.Text = oRow2(5).ToString.Trim

            Me.AccountNOAM = oRow2(6).ToString.Trim
            Me.AccountNOSL = oRow2(7).ToString.Trim
            Me.AccountNOAH = oRow2(8).ToString.Trim
            Me.AccountNOBM = oRow2(9).ToString.Trim
            Me.AccountNOGM = oRow2(10).ToString.Trim
            Me.AccountNOSV = oRow2(11).ToString.Trim
        End If
    End Sub
#End Region
#Region "GetRefundPremiumToSupplier"
    Public Sub GetRefundPremiumToSupplier()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVGetRefundPremiumToSupplier"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID
            objCommand.Parameters.Add("@RefundToSupp", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@SupplierID", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@PremiumBaseforRefundSupp", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@RefundPremi", SqlDbType.Char, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@SalesmanID", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@SalesSupervisorID", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@SupplierAdminID", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@RefundInterest", SqlDbType.Decimal).Direction = ParameterDirection.Output

            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()

            Me.RefundInterest = CDec(IIf(IsDBNull(objCommand.Parameters("@RefundInterest").Value), "", objCommand.Parameters("@RefundInterest").Value))
            Me.RefundToSupplierAmount = CDec(IIf(IsDBNull(objCommand.Parameters("@RefundToSupp").Value), 0, objCommand.Parameters("@RefundToSupp").Value))
            Me.SupplierID = CStr(IIf(IsDBNull(objCommand.Parameters("@SupplierID").Value), "", objCommand.Parameters("@SupplierID").Value))
            Me.BID = CStr(IIf(IsDBNull(objCommand.Parameters("@BranchID").Value), "", objCommand.Parameters("@BranchID").Value))
            Me.SalesmanID = CStr(IIf(IsDBNull(objCommand.Parameters("@SalesmanID").Value), "", objCommand.Parameters("@SalesmanID").Value))
            Me.SalesSupervisorID = CStr(IIf(IsDBNull(objCommand.Parameters("@SalesSupervisorID").Value), "", objCommand.Parameters("@SalesSupervisorID").Value))
            Me.SupplierAdminID = CStr(IIf(IsDBNull(objCommand.Parameters("@SupplierAdminID").Value), "", objCommand.Parameters("@SupplierAdminID").Value))

            LblPremiumToSupplier.Text = CStr(Me.RefundToSupplierAmount)
            lblPremiumBase.Text = CStr(IIf(IsDBNull(objCommand.Parameters("@PremiumBaseforRefundSupp").Value), "", objCommand.Parameters("@PremiumBaseforRefundSupp").Value))
            lblRefundPremi.Text = CStr(IIf(IsDBNull(objCommand.Parameters("@RefundPremi").Value), "", objCommand.Parameters("@RefundPremi").Value)) + " %"
            lblRefundInterest.Text = CStr(Me.RefundInterest)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
#End Region
#Region "CheckisDataExistsForGMorBMorADH"
    Public Sub CheckisDataExistsForGM()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVisDataExistsForGM"
            objCommand.Parameters.Add("@SupplierID", SqlDbType.Char, 10).Value = Me.SupplierID.Trim
            objCommand.Parameters.Add("@isDataExists", SqlDbType.Bit).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.isDataExistsGM = CBool(IIf(IsDBNull(objCommand.Parameters("@isDataExists").Value), "", objCommand.Parameters("@isDataExists").Value))
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
    Public Sub CheckisDataExistsForBM()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVisDataExistsForBM"
            objCommand.Parameters.Add("@SupplierID", SqlDbType.Char, 10).Value = Me.SupplierID.Trim
            objCommand.Parameters.Add("@isDataExists", SqlDbType.Bit).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.isDataExistsBM = CBool(IIf(IsDBNull(objCommand.Parameters("@isDataExists").Value), "", objCommand.Parameters("@isDataExists").Value))
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
    Public Sub CheckisDataExistsForAH()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVisDataExistsForAH"
            objCommand.Parameters.Add("@SupplierID", SqlDbType.Char, 10).Value = Me.SupplierID.Trim
            objCommand.Parameters.Add("@isDataExists", SqlDbType.Bit).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.isDataExistsAH = CBool(IIf(IsDBNull(objCommand.Parameters("@isDataExists").Value), "", objCommand.Parameters("@isDataExists").Value))
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

#End Region
#Region "GetDataSupplierINCTVDailyD"
    Public Sub GetdataFromSupplierIncentiveDailyD()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVGetDailyDView"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID
            objCommand.Parameters.Add("@IncentiveForSupplier", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForGM", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForBM", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForADH", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForSPV", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForSL", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForAM", SqlDbType.Decimal).Direction = ParameterDirection.Output

            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()

            txtInctvAmountCompany.Text = CStr((IIf(IsDBNull(objCommand.Parameters("@IncentiveForSupplier").Value), "0", objCommand.Parameters("@IncentiveForSupplier").Value)))
            txtInctvAmountGM.Text = CStr((IIf(IsDBNull(objCommand.Parameters("@IncentiveForGM").Value), "0", objCommand.Parameters("@IncentiveForGM").Value)))
            txtInctvAmountPM.Text = CStr((IIf(IsDBNull(objCommand.Parameters("@IncentiveForBM").Value), "0", objCommand.Parameters("@IncentiveForBM").Value)))
            txtInctvAmountADH.Text = CStr((IIf(IsDBNull(objCommand.Parameters("@IncentiveForADH").Value), "0", objCommand.Parameters("@IncentiveForADH").Value)))
            txtInctvAmountSalesSpv.Text = CStr((IIf(IsDBNull(objCommand.Parameters("@IncentiveForSPV").Value), "0", objCommand.Parameters("@IncentiveForSPV").Value)))
            txtInctvAmountSalesPerson.Text = CStr((IIf(IsDBNull(objCommand.Parameters("@IncentiveForSL").Value), "0", objCommand.Parameters("@IncentiveForSL").Value)))
            txtInctvAmountSuppAdm.Text = CStr((IIf(IsDBNull(objCommand.Parameters("@IncentiveForAM").Value), "0", objCommand.Parameters("@IncentiveForAM").Value)))
            lblTotal.Text = FormatNumber(CStr(CDec(txtInctvAmountADH.Text) + CDec(txtInctvAmountCompany.Text) + CDec(txtInctvAmountGM.Text) + CDec(txtInctvAmountPM.Text) + CDec(txtInctvAmountSalesPerson.Text) + CDec(txtInctvAmountSalesSpv.Text) + CDec(txtInctvAmountSuppAdm.Text)), 0)

            lblRefundInterest.Text = FormatNumber(CStr(CDec(txtInctvAmountADH.Text) + _
            CDec(txtInctvAmountCompany.Text) + _
            CDec(txtInctvAmountGM.Text) + _
            CDec(txtInctvAmountPM.Text) + _
            CDec(txtInctvAmountSalesPerson.Text) + _
            CDec(txtInctvAmountSalesSpv.Text) + _
            CDec(txtInctvAmountSuppAdm.Text)), 0)

        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
#End Region

End Class