﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApplicationMaintenance.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ApplicationMaintenance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Application Maintenance</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinSupplier(pStyle, pSupplierID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/ViewSupplier.aspx?style=' + pStyle + '&SupplierID=' + pSupplierID, 'Supplier', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinEmployee(pStyle, pBranchID, pAOID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pAOID, 'Employee', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinApplication2(pApplicationID, pCustID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            //window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID + '&CustID=' + pCustID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
            window.open('http://<%=Request.servervariables("HTTP_HOST")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID + '&CustID=' + pCustID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
}

    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR APLIKASI</h3>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlList">
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:ButtonColumn Text="MODIFY" HeaderText="PILIH" CommandName="Modify">                            
                        </asp:ButtonColumn>
                        <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="DATA ID">                            
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkApplication" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">                            
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierName" HeaderText="NAMA SUPPLIER">                            
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkSupplier" Text='<%# DataBinder.eval(Container,"DataItem.SupplierName")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AO" HeaderText="NAMA ACCOUNT OFFICER">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAO" Text='<%# DataBinder.eval(Container,"DataItem.AO")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="STEP APLIKASI">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblApplicationStep" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationStep")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                        <asp:TemplateColumn SortExpression="ApplicationStep" HeaderText="STEP APLIKASI">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblApplicationStep" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationStepDesc")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="CustomerID">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCustomerID" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="Type">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblType" Text='<%# DataBinder.eval(Container,"DataItem.Type")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="SupplierID">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSupplierID" Text='<%# DataBinder.eval(Container,"DataItem.SupplierID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="AOID">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAOID" Text='<%# DataBinder.eval(Container,"DataItem.AOID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ProductID">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProductID" Text='<%# DataBinder.eval(Container,"DataItem.ProductID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="False" HeaderText="ProductOfferingID">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblProductOfferingID" Text='<%# DataBinder.eval(Container,"DataItem.ProductOfferingID")%>'>
                                </asp:Label>
                                <asp:Label runat="server" ID="lblDateReturnApp" Text='<%# DataBinder.eval(Container,"DataItem.DateReturnApp")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="true" HeaderText="TYPE">                            
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblApplicationModule" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationModule")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                         CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" 
                        Display="Dynamic"  CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI APLIKASI</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Name Customer</asp:ListItem>
                    <asp:ListItem Value="ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                    <asp:ListItem Value="AO">Nama CMO</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CssClass="small button blue" Text="Find"
                CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlModify" runat="server" Width="100%">
        <table border="0" cellspacing="0" cellpadding="0" width="95%">
            <tr class="trtopi">
                <td class="tdtopi" align="center">
                    PERUBAHAN DATA
                </td>
            </tr>
        </table>
        <table class="tablegrid" border="0" cellspacing="1" cellpadding="2" width="95%">
            <tr>
                <td class="tdgenap">
                    <asp:Label ID="lbl1" runat="server">No Aplikasi</asp:Label>
                </td>
                <td class="tdganjil">
                    <asp:HyperLink ID="lblApplicationID" runat="server"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    <asp:Label ID="Label1" runat="server">Nama Customer</asp:Label>
                </td>
                <td class="tdganjil">
                    <asp:HyperLink ID="lblCustomerName" runat="server"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdjudul" height="20" width="25%" align="center">
                    PERUBAHAN :
                </td>
                <td class="tdjudul" height="20" align="center">
                    PENJELASAN :
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    <asp:HyperLink ID="hplApplication" runat="server">DATA APLIKASI</asp:HyperLink>
                </td>
                <td class="tdganjil">
                    1. Cara Pembayaran, Tgl Kontrak, Biaya-Biaya, No Reference, Alamat Tagih, Syarat
                    dan Kondisi, Syarat dan Kondisi Check List<br />
                    2. Jenis Margin, Skema Angsuran, Step Up/Step Down Type --&gt; Harus entry ulang
                    data Financial dari menu Aplikasi baru
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    <asp:HyperLink ID="hplAsset" runat="server">DATA ASSET</asp:HyperLink>
                </td>
                <td class="tdganjil">
                    1. Supplier, Asset, No Serial, Attribute, Registrasi Asset, Karyawan Supplier<br />
                    2. Harga OTR, Uang Muka, Asuransi Asset --&gt; harus entry ulang Asuransi dan Data
                    Financial Data dari menu Aplikasi Baru
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    <asp:HyperLink ID="hplInsurance" runat="server">DATA ASURANSI</asp:HyperLink>
                </td>
                <td class="tdganjil">
                    Perusahaan Asuransi, Jangka Waktu, Nilai Pertanggungan, Jenis Cover, Discount Premi,
                    Jumlah dibayar, Jumlah dibiayakan, Catatan Accesories
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    <asp:HyperLink ID="hplFinancial" runat="server">DATA FINANCIAL</asp:HyperLink>
                </td>
                <td class="tdganjil">
                    Margin Effective Margin Supplier, Jadwal Angsuran, Angsuran Pertama, Jumlah Angsuran,
                    Grace Period
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    <asp:HyperLink ID="hplIncentive" runat="server">DATA INCENTIVE</asp:HyperLink>
                </td>
                <td class="tdganjil">
                    Incentive Supplier, Refund Premi, Refund Margin
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" width="95%">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imbBack" runat="server" ImageUrl="../../../../images/buttonback.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
