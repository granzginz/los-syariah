﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class EditApplication
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UCMailingAddress As UcCompanyAddress
    Protected WithEvents UCRefAddress As UcCompanyAddress
    Protected WithEvents uscAgreementDate As ValidDate
    Protected WithEvents uscSurveyDate As ValidDate   
    Protected WithEvents UCGuarantor As ucLookUpCustomer
    Protected WithEvents UCSpouse As ucLookUpCustomer
#Region "Constanta"
    Private m_controller As New EditApplicationController
    Dim Status As Boolean
    Dim style As String = "ACCACQ"
    Dim bind As Boolean = True
    Dim objrow As DataRow
    Dim intLoop As Integer
#End Region
#Region "Property"
    Property Fiducia() As String
        Get
            Return viewstate("Fiducia").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Fiducia") = Value
        End Set
    End Property
    Private Property myDataTable() As DataTable
        Get
            Return CType(viewstate("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("myDataTable") = Value
        End Set
    End Property
    Property Type() As String
        Get
            Return viewstate("Type").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Type") = Value
        End Set
    End Property
    Property ProductID() As String
        Get
            Return viewstate("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ProductID") = Value
        End Set
    End Property
    Property ProductOffID() As String
        Get
            Return viewstate("ProductOffID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ProductOffID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property TotalCountApplicationID() As Integer
        Get
            Return CType(Viewstate("TotalCountApplicationID"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("TotalCountApplicationID") = Value
        End Set
    End Property
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If Not Page.IsPostBack Then
            imbSave.Attributes.Add("OnClick", "return checksubmit();")

            Me.ApplicationID = Request("ApplicationID")
            Me.BranchID = Request("BranchID")
            Me.CustomerName = Request("name")
            Me.Type = Request("type")
            Me.ProductOffID = Request("ProductOfferingID")
            Me.ProductID = Request("ProductID")
            Me.CustomerID = Request("CustomerID")

            lblApplicationForm.Text = Me.ApplicationID
            'lblProductOffering.Text = Me.ProductOffID
            ''-----Added to check the count number of designated applicationid----------
            Dim oCustomclass As New Parameter.Application
            With oCustomclass
                .strConnection = GetConnectionString()
                .CustomerId = Me.CustomerID
            End With
            Dim cookie As HttpCookie = Request.Cookies("AssetData")
            'If Not cookie Is Nothing Then
            '    cookie.Values("TotalApplication") = Me.TotalCountApplicationID
            '    Response.AppendCookie(cookie)
            'End If
            Me.TotalCountApplicationID = m_controller.GetCountAppliationId(oCustomclass)

            If Me.Type = "C" Then
                PnlSpouse.Visible = False
            Else
                PnlSpouse.Visible = True
            End If

            UCMailingAddress.Style = style
            UCMailingAddress.BindAddress()
            UCRefAddress.Style = style
            UCRefAddress.BindAddress()
            'UCGuarantor.Style = style
            'UCGuarantor.BindData()
            'UCSpouse.Style = style
            'UCSpouse.BindData()
            UCMailingAddress.ValidatorTrue()
            UCRefAddress.ValidatorFalse()
            lblCustName.Text = Me.CustomerName
            lblCustName.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"

            Panel2()
            BindEdit()
            AddRecordCD()
        End If

        uscAgreementDate.isCalendarPostBack = False
        uscAgreementDate.Display = "Dynamic"
        uscAgreementDate.FillRequired = False
        uscAgreementDate.ValidationErrMessage = "Harap isi Tanggal Kontrak dengan format dd/MM/yyyy"

        uscSurveyDate.isCalendarPostBack = False
        uscSurveyDate.Display = "Dynamic"
        uscSurveyDate.FillRequired = False
        uscSurveyDate.ValidationErrMessage = "Harap isi tanggal Survery dengan format dd/MM/yyyy"

        LabelValidationFalse()

    End Sub
#End Region
#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString
        oApplication.BranchId = Me.sesBranchId
        oApplication.AppID = Me.ApplicationID
        oApplication = m_controller.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        If oData.Rows.Count > 0 Then
            objrow = oData.Rows(0)
            lblProductOffering.Text = objrow("Description").ToString
            cboInterestType.Items.FindByValue(objrow("InterestType").ToString).Selected = True
            cboInstScheme.Items.FindByValue(objrow("InstallmentScheme").ToString).Selected = True
            'UCGuarantor.CustomerName = objrow("Guarantor").ToString
            UCGuarantor.CustomerID = objrow("GuarantorID").ToString
            'UCGuarantor.BindData()
            'UCSpouse.CustomerName = objrow("Spouse").ToString
            UCSpouse.CustomerID = objrow("SpouseID").ToString
            'UCSpouse.BindData()
            cboWayPymt.Items.FindByValue(objrow("WayOfPayment").ToString).Selected = True
            cboSourceApp.Items.FindByValue(objrow("ApplicationSource").ToString).Selected = True

            If objrow("AgreementDate").ToString.Trim <> "" Then
                uscAgreementDate.dateValue = Format(objrow("AgreementDate"), "dd/MM/yyyy")
            End If

            uscSurveyDate.dateValue = Format(objrow("SurveyDate"), "dd/MM/yyyy")
            txtAdminFee.Text = objrow("AdminFee").ToString.Trim
            txtFiduciaFee.Text = objrow("FiduciaFee").ToString.Trim
            txtProvisionFee.Text = objrow("ProvisionFee").ToString.Trim
            txtNotaryFee.Text = objrow("NotaryFee").ToString.Trim
            txtSurveyFee.Text = objrow("SurveyFee").ToString.Trim

            Try
                CboStatusFiducia.Items.FindByValue(objrow("IsFiduciaCovered").ToString).Selected = True
            Catch ex As Exception
            End Try

            TxtAdditionalAdmin.Text = objrow("AddAdminFee").ToString
            chkIsAdminFeeCredit.Checked = CBool(objrow("isAdminFeeCredit"))
            chkIsProvisiCredit.Checked = CBool(objrow("isProvisiCredit"))
            txtBBNFee.Text = objrow("BBNFee").ToString.Trim

            If objrow("StepUpStepDownType").ToString <> "" Then _
            rdoSTTYpe.Items.FindByValue(objrow("StepUpStepDownType").ToString).Selected = True

            If txtBBNFee.Text.Trim = "" Then
                txtBBNFee.Text = "0"
            End If

            txtOtherFee.Text = objrow("OtherFee").ToString.Trim
            If txtOtherFee.Text.Trim = "" Then
                txtOtherFee.Text = "0"
            End If

            txtLatePenalty.Text = objrow("PercentagePenalty").ToString.Trim
            lblRecourse.Text = objrow("IsRecourse").ToString.Trim
            txtAppNotes.Text = objrow("Notes").ToString.Trim
            txtRefName.Text = objrow("ReferenceName").ToString.Trim
            txtRefJobTitle.Text = objrow("ReferenceJobTitle").ToString.Trim
            UCRefAddress.Address = objrow("ReferenceAddress").ToString.Trim
            UCRefAddress.RT = objrow("ReferenceRT").ToString.Trim
            UCRefAddress.RW = objrow("ReferenceRW").ToString.Trim
            UCRefAddress.Kelurahan = objrow("ReferenceKelurahan").ToString.Trim
            UCRefAddress.Kecamatan = objrow("ReferenceKecamatan").ToString.Trim
            UCRefAddress.City = objrow("ReferenceCity").ToString.Trim
            UCRefAddress.ZipCode = objrow("ReferenceZipCode").ToString.Trim
            UCRefAddress.AreaPhone1 = objrow("ReferenceAreaPhone1").ToString.Trim
            UCRefAddress.Phone1 = objrow("ReferencePhone1").ToString.Trim
            UCRefAddress.AreaPhone2 = objrow("ReferenceAreaPhone2").ToString.Trim
            UCRefAddress.Phone2 = objrow("ReferencePhone2").ToString.Trim
            UCRefAddress.AreaFax = objrow("ReferenceAreaFax").ToString.Trim
            UCRefAddress.Fax = objrow("ReferenceFax").ToString.Trim
            UCRefAddress.BindAddress()
            txtRefMobile.Text = objrow("ReferenceMobilePhone").ToString.Trim
            txtRefEmail.Text = objrow("ReferenceEmail").ToString.Trim
            txtRefNotes.Text = objrow("ReferenceNotes").ToString.Trim
            UCMailingAddress.Address = objrow("MailingAddress").ToString.Trim
            UCMailingAddress.RT = objrow("MailingRT").ToString.Trim
            UCMailingAddress.RW = objrow("MailingRW").ToString.Trim
            UCMailingAddress.Kelurahan = objrow("MailingKelurahan").ToString.Trim
            UCMailingAddress.Kecamatan = objrow("MailingKecamatan").ToString.Trim
            UCMailingAddress.City = objrow("MailingCity").ToString.Trim
            UCMailingAddress.ZipCode = objrow("MailingZipCode").ToString.Trim
            UCMailingAddress.AreaPhone1 = objrow("MailingAreaPhone1").ToString.Trim
            UCMailingAddress.Phone1 = objrow("MailingPhone1").ToString.Trim
            UCMailingAddress.AreaPhone2 = objrow("MailingAreaPhone2").ToString.Trim
            UCMailingAddress.Phone2 = objrow("MailingPhone2").ToString.Trim
            UCMailingAddress.AreaFax = objrow("MailingAreaFax").ToString.Trim
            UCMailingAddress.Fax = objrow("MailingFax").ToString.Trim
            UCMailingAddress.BindAddress()
            Me.ProductID = objrow("ProductID").ToString.Trim
            Me.ProductOffID = objrow("ProductOfferingID").ToString.Trim

            TC("Edit")
            TC2("Edit")
            fillAddress(Me.CustomerID)
            BindCD()
            lblApplicationForm.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationForm.Text.Trim) & "')"
            lblProductOffering.NavigateUrl = "javascript:OpenWinProductOffering('" & Me.ProductID & "'," & Me.sesBranchId.Replace(",", "") & ",'" & Me.ProductOffID & "','AccAcq')"
        End If
    End Sub
#End Region
#Region "LabelValidationFalse"
    Sub LabelValidationFalse()
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim DtgCDcount As Integer = dtgCrossDefault.Items.Count - 1
        Dim countDtgTemp As Integer = CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
        Dim countDtg As Integer = CInt(IIf(countDtgTemp > DtgCDcount, countDtgTemp, DtgCDcount))
        Dim validCheck As New Label
        Dim Checked As Boolean
        Dim PriorTo As String
        Dim PromiseDate As ValidDate
        Dim Mandatory As String
        For intLoop = 0 To countDtg
            If intLoop <= DtgCDcount Then
                CType(dtgCrossDefault.Items(intLoop).FindControl("lblVCDAgreementNo"), Label).Visible = False
            End If
            If intLoop <= DtgTC1count Then
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("uscTCPromiseDate"), ValidDate)
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = False
                If PriorTo = "APK" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
            If intLoop <= DtgTC2count Then
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("uscTCPromiseDate2"), ValidDate)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = False
                If PriorTo = "APK" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
        Next
        lblVSurveyDate.Visible = False
        lblVSurveyDate2.Visible = False
    End Sub
#End Region
#Region "BindCD"
    Sub BindCD()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        Dim txtAgreement As TextBox
        Dim lblAgreement As Label
        Dim Name As Label
        Dim AgreementDate As Label
        Dim DefaultStatus As Label
        Dim ContractStatus As Label
        Dim myDataColumn As DataColumn
        Dim myDataRow As DataRow

        myDataTable = New DataTable("CDAgreementNo")
        myDataColumn = New DataColumn
        myDataColumn.DataType = System.Type.GetType("System.String")
        myDataColumn.ColumnName = "AgreementNo"
        myDataTable.Columns.Add(myDataColumn)

        oApplication.AppID = Me.ApplicationID
        oApplication.strConnection = GetConnectionString
        oApplication = m_controller.GetCD(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgCrossDefault.DataSource = oData
        dtgCrossDefault.DataBind()

        'Buat row baru 
        myDataRow = myDataTable.NewRow()

        For intLoop = 0 To oData.Rows.Count - 1

            txtAgreement = CType(dtgCrossDefault.Items(intLoop).FindControl("txtCDAgreement"), TextBox)
            lblAgreement = CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDAgreementNo"), Label)
            AgreementDate = CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDAgreementDate"), Label)
            txtAgreement.Text = oData.Rows(intLoop).Item(0).ToString.Trim
            lblAgreement.Text = oData.Rows(intLoop).Item(0).ToString.Trim
            If Not IsDBNull(oData.Rows(intLoop).Item(2)) Then
                AgreementDate.Text = Format(oData.Rows(intLoop).Item(2), "dd/MM/yyyy")
            End If
            txtAgreement.Visible = False
            lblAgreement.Visible = True

            myDataRow("AgreementNo") = lblAgreement.Text.Trim

            'tambah row
            myDataTable.Rows.Add(myDataRow)
        Next

        Me.myDataTable = myDataTable
    End Sub
#End Region
#Region "BindTC"
    Sub TC(ByVal AddEdit As String)
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.AppID = Me.ApplicationID
        oApplication.strConnection = GetConnectionString
        oApplication.BranchId = Me.sesBranchId
        oApplication.AddEdit = AddEdit
        oApplication = m_controller.GetTC(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC.DataSource = oData.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()

        If AddEdit = "Edit" Then
            Dim intLoop As Integer

            For intLoop = 0 To dtgTC.Items.Count - 1
                If oData.Rows(intLoop).Item(4).ToString.Trim <> "" Then
                    CType(dtgTC.Items(intLoop).FindControl("uscTCPromiseDate"), ValidDate).dateValue = Format(oData.Rows(intLoop).Item(4), "dd/MM/yyyy")
                End If
            Next
        End If
    End Sub
    Sub TC2(ByVal AddEdit As String)
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.AppID = Me.ApplicationID
        oApplication.strConnection = GetConnectionString
        oApplication.BranchId = Me.sesBranchId
        oApplication.AddEdit = AddEdit
        oApplication = m_controller.GetTC2(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        dtgTC2.DataSource = oData.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()
        If AddEdit = "Edit" Then
            Dim intLoop As Integer
            For intLoop = 0 To dtgTC2.Items.Count - 1
                If oData.Rows(intLoop).Item(5).ToString.Trim <> "" Then
                    CType(dtgTC2.Items(intLoop).FindControl("uscTCPromiseDate2"), ValidDate).dateValue = Format(oData.Rows(intLoop).Item(5), "dd/MM/yyyy")
                End If
            Next
        End If
    End Sub
#End Region
#Region "Add - Delete CD"
    Private Sub AddRecordCD()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim txtAgreement As TextBox
        Dim No, lblAgreement, Name, AgreementDate, DefaultStatus, ContractStatus As Label
        With objectDataTable
            .Columns.Add(New DataColumn("Agreement", GetType(String)))
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("AgreementDate", GetType(String)))
            .Columns.Add(New DataColumn("DefaultStatus", GetType(String)))
            .Columns.Add(New DataColumn("ContractStatus", GetType(String)))
            .Columns.Add(New DataColumn("Visible", GetType(Boolean)))
        End With

        For intLoopGrid = 0 To dtgCrossDefault.Items.Count - 1
            lblAgreement = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDAgreementNo"), Label)
            txtAgreement = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("txtCDAgreement"), TextBox)
            Name = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDName"), Label)
            AgreementDate = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDAgreementDate"), Label)
            DefaultStatus = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDDefaultStatus"), Label)
            ContractStatus = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDContractStatus"), Label)
            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("Agreement") = txtAgreement.Text.Trim
            oRow("Name") = Name.Text
            oRow("AgreementDate") = AgreementDate.Text
            oRow("DefaultStatus") = DefaultStatus.Text
            oRow("ContractStatus") = ContractStatus.Text
            oRow("Visible") = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("txtCDAgreement"), TextBox).Visible
            objectDataTable.Rows.Add(oRow)
        Next
        oRow = objectDataTable.NewRow()
        oRow("Agreement") = ""
        oRow("Name") = ""
        oRow("AgreementDate") = ""
        oRow("DefaultStatus") = ""
        oRow("ContractStatus") = ""
        oRow("Visible") = True
        objectDataTable.Rows.Add(oRow)
        '------Bind------'
        dtgCrossDefault.DataSource = objectDataTable
        dtgCrossDefault.DataBind()

        For intLoopGrid = 0 To dtgCrossDefault.Items.Count - 1
            oRow = objectDataTable.Rows(intLoopGrid)
            lblAgreement = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDAgreementNo"), Label)
            txtAgreement = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("txtCDAgreement"), TextBox)
            AgreementDate = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDAgreementDate"), Label)

            lblAgreement.Visible = CBool(IIf(CBool(oRow.Item("Visible")) = True, False, True))
            txtAgreement.Visible = CBool(oRow.Item("Visible"))
            lblAgreement.Text = oRow.Item("Agreement").ToString
            txtAgreement.Text = oRow.Item("Agreement").ToString
            AgreementDate.Text = oRow.Item("AgreementDate").ToString.Trim
            If UCase(oRow.Item("DefaultStatus").ToString.Trim) = "NM" And UCase(oRow.Item("ContractStatus").ToString.Trim) = "AKT" Then
                CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblVCDAgreementNo"), Label).Visible = False
            Else
                If (oRow.Item("DefaultStatus").ToString.Trim <> "" And oRow.Item("ContractStatus").ToString.Trim <> "") Then
                    CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblVCDAgreementNo"), Label).Visible = True
                Else
                    CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblVCDAgreementNo"), Label).Visible = False
                End If
            End If
        Next
    End Sub
    Sub DeleteBaris(ByVal Index As Integer)
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim No As String
        Dim lblAgreement, AgreementDate, DefaultStatus, ContractStatus, Name As Label
        Dim txtAgreement As TextBox
        Dim oNewDataTable As New DataTable
        With oNewDataTable
            .Columns.Add(New DataColumn("Agreement", GetType(String)))
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("AgreementDate", GetType(String)))
            .Columns.Add(New DataColumn("DefaultStatus", GetType(String)))
            .Columns.Add(New DataColumn("ContractStatus", GetType(String)))
            .Columns.Add(New DataColumn("Visible", GetType(Boolean)))
        End With

        For intLoopGrid = 0 To dtgCrossDefault.Items.Count - 1
            lblAgreement = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDAgreementNo"), Label)
            txtAgreement = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("txtCDAgreement"), TextBox)
            Name = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDName"), Label)
            AgreementDate = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDAgreementDate"), Label)
            DefaultStatus = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDDefaultStatus"), Label)
            ContractStatus = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDContractStatus"), Label)
            '----- Add row -------'
            If intLoopGrid <> Index Then
                oRow = oNewDataTable.NewRow()
                oRow("Agreement") = txtAgreement.Text.Trim
                oRow("Name") = Name.Text
                oRow("AgreementDate") = AgreementDate.Text
                oRow("DefaultStatus") = DefaultStatus.Text
                oRow("ContractStatus") = ContractStatus.Text
                oRow("Visible") = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("txtCDAgreement"), TextBox).Visible
                oNewDataTable.Rows.Add(oRow)
            End If
        Next
        dtgCrossDefault.DataSource = oNewDataTable
        dtgCrossDefault.DataBind()

        For intLoopGrid = 0 To dtgCrossDefault.Items.Count - 1
            oRow = oNewDataTable.Rows(intLoopGrid)
            lblAgreement = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDAgreementNo"), Label)
            txtAgreement = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("txtCDAgreement"), TextBox)
            Name = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDName"), Label)
            AgreementDate = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDAgreementDate"), Label)
            DefaultStatus = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDDefaultStatus"), Label)
            ContractStatus = CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblCDContractStatus"), Label)

            lblAgreement.Visible = CBool(IIf(CBool(oRow.Item("Visible")) = True, False, True))
            txtAgreement.Visible = CBool(oRow.Item("Visible"))
            lblAgreement.Text = oRow.Item("Agreement").ToString
            txtAgreement.Text = oRow.Item("Agreement").ToString
            Name.Text = oRow.Item("Name").ToString
            AgreementDate.Text = oRow.Item("AgreementDate").ToString
            DefaultStatus.Text = oRow.Item("DefaultStatus").ToString
            ContractStatus.Text = oRow.Item("ContractStatus").ToString
            If UCase(oRow.Item("DefaultStatus").ToString.Trim) = "NM" And UCase(oRow.Item("ContractStatus").ToString.Trim) = "AKT" Then
                CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblVCDAgreementNo"), Label).Visible = False
            Else
                If (oRow.Item("DefaultStatus").ToString.Trim <> "" And oRow.Item("ContractStatus").ToString.Trim <> "") Then
                    CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblVCDAgreementNo"), Label).Visible = True
                Else
                    CType(dtgCrossDefault.Items(intLoopGrid).FindControl("lblVCDAgreementNo"), Label).Visible = False
                End If
            End If
        Next
    End Sub
#End Region
#Region "FillAddress"
    Sub fillAddress(ByVal id As String)
        Dim oData As New DataTable
        Dim oApplication As New Parameter.Application
        Dim oRow As DataRow
        oApplication.strConnection = GetConnectionString
        oApplication.CustomerId = Me.CustomerID
        oApplication.Type = Me.Type
        oApplication.ApplicationID = Me.ApplicationID
        oApplication.BranchId = Me.BranchID
        oApplication = m_controller.GetAddress(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        cboCopyAddress.DataSource = oData.DefaultView
        cboCopyAddress.DataTextField = "Combo"
        cboCopyAddress.DataValueField = "Type"
        cboCopyAddress.DataBind()

        oRow = oData.Rows(0)

        If Me.Type <> "P" Then
            txtRefName.Text = objrow("ReferenceName").ToString
            txtRefJobTitle.Text = objrow("ReferenceJobTitle").ToString
            UCRefAddress.Address = objrow("ReferenceAddress").ToString.Trim
            UCRefAddress.RT = objrow("ReferenceRT").ToString.Trim
            UCRefAddress.RW = objrow("ReferenceRW").ToString.Trim
            UCRefAddress.Kelurahan = objrow("ReferenceKelurahan").ToString.Trim
            UCRefAddress.Kecamatan = objrow("ReferenceKecamatan").ToString.Trim
            UCRefAddress.City = objrow("ReferenceCity").ToString.Trim
            UCRefAddress.ZipCode = objrow("ReferenceZipCode").ToString.Trim
            UCRefAddress.AreaPhone1 = objrow("ReferenceAreaPhone1").ToString.Trim
            UCRefAddress.Phone1 = objrow("ReferencePhone1").ToString.Trim
            UCRefAddress.AreaPhone2 = objrow("ReferenceAreaPhone2").ToString.Trim
            UCRefAddress.Phone2 = objrow("ReferencePhone2").ToString.Trim
            UCRefAddress.AreaFax = objrow("ReferenceAreaFax").ToString.Trim
            UCRefAddress.Fax = objrow("ReferenceFax").ToString.Trim
            txtRefMobile.Text = objrow("ReferenceMobilePhone").ToString.Trim
            txtRefEmail.Text = objrow("ReferenceEmail").ToString.Trim
            txtRefNotes.Text = objrow("ReferenceNotes").ToString.Trim
        End If

        UCMailingAddress.Address = oRow.Item("Address").ToString
        UCMailingAddress.RT = oRow.Item("RT").ToString.Trim
        UCMailingAddress.RW = oRow.Item("RW").ToString.Trim
        UCMailingAddress.Kelurahan = oRow.Item("Kelurahan").ToString.Trim
        UCMailingAddress.Kecamatan = oRow.Item("Kecamatan").ToString.Trim
        UCMailingAddress.City = oRow.Item("City").ToString.Trim
        UCMailingAddress.ZipCode = oRow.Item("ZipCode").ToString.Trim
        UCMailingAddress.AreaPhone1 = oRow.Item("AreaPhone1").ToString.Trim
        UCMailingAddress.Phone1 = oRow.Item("Phone1").ToString.Trim
        UCMailingAddress.AreaPhone2 = oRow.Item("AreaPhone2").ToString.Trim
        UCMailingAddress.Phone2 = oRow.Item("Phone2").ToString.Trim
        UCMailingAddress.AreaFax = oRow.Item("AreaFax").ToString.Trim
        UCMailingAddress.Fax = oRow.Item("Fax").ToString.Trim
        UCMailingAddress.BindAddress()


        Div1.InnerHtml = GenerateScript(oData)
    End Sub
    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboCopyAddress.Items.Count - 1
            DataRow = DtTable.Select(" Type = '" & cboCopyAddress.Items(j).Value.Trim & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("Type")).Trim Then
                        strType = CStr(DataRow(i)("Type")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & DataRow(i)("Address").ToString.Trim & "','" & DataRow(i)("RT").ToString.Trim & "' " & _
                                        ",'" & DataRow(i)("RW").ToString.Trim & "','" & DataRow(i)("Kelurahan").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Kecamatan").ToString.Trim & "','" & DataRow(i)("City").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("ZipCode").ToString.Trim & "','" & DataRow(i)("AreaPhone1").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Phone1").ToString.Trim & "','" & DataRow(i)("AreaPhone2").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Phone2").ToString.Trim & "','" & DataRow(i)("AreaFax").ToString.Trim & "'" & _
                                        ",'" & DataRow(i)("Fax").ToString.Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next
        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If
        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        strScript &= "</script>"
        Return strScript
    End Function
#End Region
#Region "Panel"
    Sub InitialPanel()
        pnl1.Visible = True
        pnl2.Visible = False
        lblProductOffering.Visible = False
    End Sub
    Sub Panel2()
        pnl1.Visible = False
        pnl2.Visible = True
        lblProductOffering.Visible = True
    End Sub
#End Region
    '#Region "Fee & Behaviour"
    '    Sub GetFee()
    '        Dim oApplication As New Parameter.Application
    '        Dim oData As New DataTable
    '        oApplication.strConnection = GetConnectionString
    '        oApplication.BranchId = Me.sesBranchId
    '        oApplication.ProductOffID = Me.ProductOffID
    '        oApplication.ProductID = Me.ProductID
    '        oApplication = m_controller.GetFee(oApplication)
    '        If Not oApplication Is Nothing Then
    '            oData = oApplication.ListData
    '            objrow = oData.Rows(0)
    '            Behaviour("AdminFee", txtAdminFee, RVAdmin)
    '            Behaviour("FiduciaFee", txtFiduciaFee, RVFiducia)
    '            Behaviour("ProvisionFee", txtProvisionFee, RVProvision)
    '            Behaviour("NotaryFee", txtNotaryFee, RVNotary)
    '            Behaviour("SurveyFee", txtSurveyFee, RVSurvey)
    '            Behaviour("PenaltyPercentage", txtLatePenalty, RVPenalty)
    '            RVPenalty.MaximumValue = "100"
    '            lblRecourse.Text = IIf(CType(objrow("IsRecourse"), Boolean) = True, "Yes", "No").ToString
    '        End If
    '    End Sub
    '    Sub Behaviour(ByVal ColumnName As String, ByVal TextName As TextBox, ByVal RangeValid As RangeValidator)
    '        TextName.Text = CInt(objrow(ColumnName)).ToString.Trim
    '        Select Case objrow(ColumnName + "Behaviour").ToString.Trim
    '            Case "D"
    '                RangeValid.MinimumValue = "0"
    '                RangeValid.MaximumValue = "999999999999999"

    '                RangeValid.ErrorMessage = "Invalid Input!"
    '                RangeValid.Enabled = True
    '                TextName.ReadOnly = False
    '            Case "L"
    '                RangeValid.MinimumValue = "0"
    '                RangeValid.MaximumValue = "999999999999999"
    '                RangeValid.ErrorMessage = "Invalid Input!"
    '                RangeValid.Enabled = True
    '                TextName.ReadOnly = True
    '            Case "N"
    '                TextName.ReadOnly = False
    '                RangeValid.MinimumValue = CInt(objrow(ColumnName)).ToString.Trim
    '                RangeValid.MaximumValue = "999999999999999"
    '                RangeValid.ErrorMessage = "Input must >= " & objrow(ColumnName).ToString.Trim & "!"
    '                RangeValid.Enabled = True
    '            Case "X"
    '                TextName.ReadOnly = False
    '                RangeValid.MaximumValue = CInt(objrow(ColumnName)).ToString.Trim
    '                'RangeValid.MinimumValue = "0.0000000000001"
    '                RangeValid.MinimumValue = "0"
    '                RangeValid.ErrorMessage = "Input must <= " & objrow(ColumnName).ToString.Trim & "!"
    '                RangeValid.Enabled = True
    '        End Select
    '    End Sub
    '#End Region
#Region "ImbOK, ImbCDAdd, ImbCancel click"
    Private Sub imbCDAdd_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCDAdd.Click
        AddRecordCD()
    End Sub
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("ApplicationMaintenance.aspx")

    End Sub
#End Region
#Region "Save"
    'Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
    '    Response.redirect("ApplicationMaintenance.aspx")
    'End Sub
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        If Me.Page.IsValid Then
            Dim oApplication As New Parameter.Application
            Dim oRefAddress As New Parameter.Address
            Dim oRefPersonal As New Parameter.Personal
            Dim oMailingAddress As New Parameter.Address
            Dim oData1 As New DataTable
            Dim oData2 As New DataTable
            Dim oData3 As New DataTable
            Dim MasterTCID, PriorTo, AGTCCLSequenceNo, IsChecked, IsMandatory, PromiseDate, Notes, AgreementNo As String
            Dim CheckedDate As Date
            Dim oCustomclass As New Parameter.Application

            With oCustomclass
                .strConnection = getconnectionstring()
                .CustomerId = Me.CustomerID
            End With

            If Me.TotalCountApplicationID = m_controller.GetCountAppliationId(oCustomclass) Then
                Status = True
                ShowDetail()
                If UCGuarantor.CustomerID = Me.CustomerID Then
                    If UCSpouse.CustomerID = Me.CustomerID Then
                        lblMessage.Text = "Penjamin dan Pasangan tidak boleh orang yang sama"                        
                    Else
                        lblMessage.Text = "Penjamin tidak boleh orang yang sama"
                    End If
                    Exit Sub
                Else
                    If UCSpouse.CustomerID = Me.CustomerID Then
                        lblMessage.Text = "Pasangan tidak boleh orang yang sama"
                        Exit Sub
                    End If
                End If

                oData3.Columns.Add("AgreementNo", GetType(String))

                For intLoop = 0 To dtgCrossDefault.Items.Count - 1
                    If CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDAgreementNo"), Label).Text.Trim <> "" Then
                        objrow = oData3.NewRow
                        objrow("AgreementNo") = CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDAgreementNo"), Label).Text
                        oData3.Rows.Add(objrow)
                    End If
                Next

                If Status = False Then
                    LabelValidationFalse()
                    lblMessage.Text = "No Kontrak Salah"
                    Exit Sub
                End If

                oData1.Columns.Add("MasterTCID", GetType(String))
                oData1.Columns.Add("PriorTo", GetType(String))
                oData1.Columns.Add("IsChecked", GetType(String))
                oData1.Columns.Add("IsMandatory", GetType(String))
                oData1.Columns.Add("PromiseDate", GetType(String))
                oData1.Columns.Add("Notes", GetType(String))

                oData2.Columns.Add("MasterTCID", GetType(String))
                oData2.Columns.Add("AGTCCLSequenceNo", GetType(String))
                oData2.Columns.Add("IsChecked", GetType(String))
                oData2.Columns.Add("PromiseDate", GetType(String))
                oData2.Columns.Add("Notes", GetType(String))

                Validator(oData1, oData2)
                If Status = False Then
                    cek_Check()
                    Exit Sub
                End If

                oApplication.BusinessDate = Me.BusinessDate
                oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                oApplication.CustomerId = Me.CustomerID
                oApplication.ProductID = Me.ProductID
                oApplication.ProductOffID = Me.ProductOffID
                oApplication.ContractStatus = "PRP"
                oApplication.DefaultStatus = "NM"
                oApplication.ApplicationStep = "APK"
                oApplication.NumOfAssetUnit = 1
                oApplication.InterestType = cboInterestType.SelectedValue
                oApplication.InstallmentScheme = cboInstScheme.SelectedValue
                oApplication.GuarantorID = UCGuarantor.CustomerID
                oApplication.SpouseID = UCSpouse.CustomerID
                oApplication.WayOfPayment = cboWayPymt.SelectedValue
                oApplication.AgreementDate = IIf(uscAgreementDate.dateValue = "", "", ConvertDate(uscAgreementDate.dateValue)).ToString
                oApplication.SurveyDate = ConvertDate(uscSurveyDate.dateValue)
                oApplication.ApplicationSource = cboSourceApp.SelectedValue
                oApplication.PercentagePenalty = txtLatePenalty.Text
                oApplication.AdminFee = txtAdminFee.Text

                If TxtAdditionalAdmin.Text.Trim = "" Then
                    oApplication.AdditionalAdminFee = "0"
                Else
                    oApplication.AdditionalAdminFee = TxtAdditionalAdmin.Text
                End If

                oApplication.IsAdminFeeCredit = chkIsAdminFeeCredit.Checked
                oApplication.IsProvisiCredit = chkIsProvisiCredit.Checked

                If CboStatusFiducia.SelectedValue = "True" Then
                    Me.Fiducia = "1"
                Else
                    Me.Fiducia = "0"
                End If

                oApplication.StatusFiduciaFee = Me.Fiducia
                oApplication.FiduciaFee = txtFiduciaFee.Text
                oApplication.ProvisionFee = txtProvisionFee.Text
                oApplication.NotaryFee = txtNotaryFee.Text
                oApplication.SurveyFee = txtSurveyFee.Text
                oApplication.BBNFee = txtBBNFee.Text
                oApplication.OtherFee = txtOtherFee.Text
                oApplication.Notes = txtAppNotes.Text
                oRefPersonal.PersonName = txtRefName.Text
                oRefPersonal.PersonTitle = txtRefJobTitle.Text
                oRefPersonal.MobilePhone = txtRefMobile.Text
                oRefPersonal.Email = txtRefEmail.Text
                oApplication.RefNotes = txtRefNotes.Text
                oApplication.IsNST = "0"

                If (cboInterestType.SelectedValue = "FX" And cboInstScheme.SelectedValue = "ST") Then
                    oApplication.StepUpStepDownType = rdoSTTYpe.SelectedValue
                Else
                    oApplication.StepUpStepDownType = ""
                End If
                oApplication.strConnection = GetConnectionString

                oRefAddress.Address = UCRefAddress.Address
                oRefAddress.RT = UCRefAddress.RT
                oRefAddress.RW = UCRefAddress.RW
                oRefAddress.Kecamatan = UCRefAddress.Kecamatan
                oRefAddress.Kelurahan = UCRefAddress.Kelurahan
                oRefAddress.City = UCRefAddress.City
                oRefAddress.ZipCode = UCRefAddress.ZipCode
                oRefAddress.AreaPhone1 = UCRefAddress.AreaPhone1
                oRefAddress.AreaPhone2 = UCRefAddress.AreaPhone2
                oRefAddress.AreaFax = UCRefAddress.AreaFax
                oRefAddress.Phone1 = UCRefAddress.Phone1
                oRefAddress.Phone2 = UCRefAddress.Phone2
                oRefAddress.Fax = UCRefAddress.Fax

                oMailingAddress.Address = UCMailingAddress.Address
                oMailingAddress.RT = UCMailingAddress.RT
                oMailingAddress.RW = UCMailingAddress.RW
                oMailingAddress.Kecamatan = UCMailingAddress.Kecamatan
                oMailingAddress.Kelurahan = UCMailingAddress.Kelurahan
                oMailingAddress.ZipCode = UCMailingAddress.ZipCode
                oMailingAddress.City = UCMailingAddress.City
                oMailingAddress.AreaPhone1 = UCMailingAddress.AreaPhone1
                oMailingAddress.AreaPhone2 = UCMailingAddress.AreaPhone2
                oMailingAddress.AreaFax = UCMailingAddress.AreaFax
                oMailingAddress.Phone1 = UCMailingAddress.Phone1
                oMailingAddress.Phone2 = UCMailingAddress.Phone2
                oMailingAddress.Fax = UCMailingAddress.Fax

                Dim ErrorMessage As String = ""
                Dim oReturn As New Parameter.Application
                oApplication.AppID = Me.ApplicationID
                oReturn = m_controller.ApplicationSaveEdit(oApplication, oRefPersonal, oRefAddress, _
                oMailingAddress, oData1, oData2, oData3, Me.myDataTable)

                Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx?Err=" & oReturn.Err)

            Else
                lblMessage.Text = "Data sudah ada"

            End If
        End If
    End Sub
    Sub Validator(ByRef oData1 As DataTable, ByRef oData2 As DataTable)
        Dim PriorTo As String
        Dim Mandatory As String
        Dim Checked As Boolean
        Dim PromiseDate As ValidDate
        Dim validCheck As New Label
        Dim MasterTCID, MasterTCID2, AGTCCLSequenceNo, Notes, AgreementNo As String
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1

        If uscSurveyDate.dateValue = "" Then
            lblVSurveyDate.Visible = True
            Status = False
        Else
            If ConvertDate2(uscSurveyDate.dateValue) > Me.BusinessDate Then
                lblVSurveyDate2.Visible = True
                Status = False
            Else
                lblVSurveyDate2.Visible = False
                Status = True
            End If
        End If

        For intLoop = 0 To CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
            If intLoop <= DtgTC1count Then
                MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("uscTCPromiseDate"), ValidDate)
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                Notes = CType(dtgTC.Items(intLoop).Cells(6).FindControl("txtTCNotes"), TextBox).Text
                If PromiseDate.dateValue <> "" Then
                    If CInt(ConvertDate(PromiseDate.dateValue)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = True
                    End If
                End If
                If PriorTo = "APK" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.dateValue = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                    validCheck.Visible = True
                End If
                objrow = oData1.NewRow
                objrow("MasterTCID") = MasterTCID
                objrow("PriorTo") = PriorTo
                objrow("IsChecked") = IIf(Checked = True, "1", "0")
                objrow("IsMandatory") = IIf(Mandatory = "v", "1", "0")
                objrow("PromiseDate") = IIf(PromiseDate.dateValue <> "", ConvertDate(PromiseDate.dateValue), "")
                objrow("Notes") = Notes
                oData1.Rows.Add(objrow)
            End If
            If intLoop <= DtgTC2count Then
                MasterTCID = dtgTC2.Items(intLoop).Cells(8).Text
                AGTCCLSequenceNo = dtgTC2.Items(intLoop).Cells(9).Text
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Notes = CType(dtgTC2.Items(intLoop).Cells(7).FindControl("txtTCNotes2"), TextBox).Text
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("uscTCPromiseDate2"), ValidDate)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                If PromiseDate.dateValue <> "" Then
                    If CInt(ConvertDate(PromiseDate.dateValue)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = True
                    End If
                End If
                If PriorTo = "APK" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.dateValue = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                        validCheck.Visible = True
                    End If
                End If
                objrow = oData2.NewRow
                objrow("MasterTCID") = MasterTCID
                objrow("AGTCCLSequenceNo") = AGTCCLSequenceNo
                objrow("IsChecked") = IIf(Checked = True, "1", "0")
                objrow("PromiseDate") = IIf(PromiseDate.dateValue <> "", ConvertDate(PromiseDate.dateValue), "")
                objrow("Notes") = Notes
                oData2.Rows.Add(objrow)
            End If
        Next
        Dim RainCheck As Boolean = False
        Dim intLoop2, intLoop3, intLoop4, intLoop5 As Integer
        For intLoop = 0 To DtgTC1count
            MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
            Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
            For intLoop2 = 0 To DtgTC2count
                MasterTCID2 = dtgTC2.Items(intLoop2).Cells(8).Text
                If MasterTCID = MasterTCID2 Then
                    If Checked = True Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True

                        If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = False Then
                            CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                        Else
                            CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                        End If
                    Else
                        If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = True Then
                            For intLoop4 = 0 To intLoop2
                                If dtgTC2.Items(intLoop4).Cells(8).Text = MasterTCID Then
                                    CType(dtgTC2.Items(intLoop4).FindControl("lblVTC2Checked"), Label).Visible = True
                                End If
                            Next

                            For intLoop5 = intLoop2 To DtgTC2count
                                If dtgTC2.Items(intLoop5).Cells(8).Text = MasterTCID Then
                                    CType(dtgTC2.Items(intLoop5).FindControl("lblVTC2Checked"), Label).Visible = True
                                End If
                            Next


                            CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                            For intLoop3 = 0 To intLoop
                                If dtgTC.Items(intLoop3).Cells(7).Text = MasterTCID2 Then
                                    If CType(dtgTC.Items(intLoop3).FindControl("chkTCChecked"), CheckBox).Checked = False Then
                                        CType(dtgTC.Items(intLoop3).FindControl("lblVTCChecked"), Label).Visible = True
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            Next
            RainCheck = False
        Next
    End Sub
#End Region
#Region "cek_Check"
    Sub cek_Check()
        Dim CheckTC As Boolean
        Dim CheckTC2 As Boolean
        Dim counter, counter2 As Integer
        Dim PromiseDate As ValidDate
        Dim PromiseDate2 As ValidDate

        For counter = 0 To dtgTC.Items.Count - 1
            CheckTC = CType(dtgTC.Items(counter).FindControl("chkTCChecked"), CheckBox).Checked
            If CheckTC Then
                PromiseDate = CType(dtgTC.Items(counter).FindControl("uscTCPromiseDate"), ValidDate)
                PromiseDate.dateValue = ""
            End If
        Next

        For counter = 0 To dtgTC2.Items.Count - 1
            CheckTC2 = CType(dtgTC2.Items(counter).FindControl("chkTCCheck2"), CheckBox).Checked
            If CheckTC2 Then
                PromiseDate2 = CType(dtgTC2.Items(counter).FindControl("uscTCPromiseDate2"), ValidDate)
                PromiseDate2.dateValue = ""
            End If
        Next
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgCrossDefault_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCrossDefault.ItemCommand
        If e.CommandName = "CDDelete" Then
            DeleteBaris(e.Item.ItemIndex)
        End If
    End Sub
#End Region
#Region "Show Detail"
    Private Sub imbCDShow_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCDShow.Click
        ShowDetail()
    End Sub
    Sub ShowDetail()
        Dim txtAgreement As TextBox
        Dim lblAgreement As Label
        Dim Name As Label
        Dim AgreementDate As Label
        Dim DefaultStatus As Label
        Dim ContractStatus As Label
        Dim oEntities As New Parameter.Application
        Dim oReturn As New Parameter.Application
        Dim odata As New DataTable
        Dim oRow As DataRow

        Status = True

        For intLoop = 0 To dtgCrossDefault.Items.Count - 1
            txtAgreement = CType(dtgCrossDefault.Items(intLoop).FindControl("txtCDAgreement"), TextBox)
            Name = CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDName"), Label)
            lblAgreement = CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDAgreementNo"), Label)
            AgreementDate = CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDAgreementDate"), Label)
            DefaultStatus = CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDDefaultStatus"), Label)
            ContractStatus = CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDContractStatus"), Label)
            If txtAgreement.Text.Trim <> "" And Name.Text.Trim = "" Then
                oEntities.AgreementNo = txtAgreement.Text.Trim
                oEntities.strConnection = GetConnectionString
                oReturn = m_controller.GetShowDataAgreement(oEntities)
                odata = oReturn.ListData
                If odata.Rows.Count > 0 Then
                    oRow = odata.Rows(0)
                    Name.Text = oRow.Item(0).ToString.Trim
                    If oRow.Item(1).ToString.Trim <> "" Then
                        AgreementDate.Text = Format(oRow.Item(1), "dd/MM/yyyy")
                    End If
                    DefaultStatus.Text = oRow.Item(2).ToString.Trim
                    ContractStatus.Text = oRow.Item(3).ToString.Trim
                    lblAgreement.Text = txtAgreement.Text
                    CType(dtgCrossDefault.Items(intLoop).FindControl("txtCDAgreement"), TextBox).Visible = False
                    CType(dtgCrossDefault.Items(intLoop).FindControl("lblCDAgreementNo"), Label).Visible = True
                End If
            End If
            If UCase(DefaultStatus.Text.Trim) = "NM" And UCase(ContractStatus.Text.Trim) = "AKT" Then
                CType(dtgCrossDefault.Items(intLoop).FindControl("lblVCDAgreementNo"), Label).Visible = False
            Else
                If (DefaultStatus.Text <> "" And ContractStatus.Text <> "") Then
                    CType(dtgCrossDefault.Items(intLoop).FindControl("lblVCDAgreementNo"), Label).Visible = True
                    If Status <> False Then
                        Status = False
                    End If
                Else
                    CType(dtgCrossDefault.Items(intLoop).FindControl("lblVCDAgreementNo"), Label).Visible = False
                End If
            End If
        Next
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            CType(e.Item.FindControl("uscTCPromiseDate"), ValidDate).FillRequired = False
            CType(e.Item.FindControl("chkTCChecked"), CheckBox).Attributes.Add("OnClick", _
                "return CheckTC('" & Trim(CType(e.Item.FindControl("uscTCPromiseDate"),  _
                ValidDate).ClientID) & "',this.checked);")
        End If
    End Sub
    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            CType(e.Item.FindControl("uscTCPromiseDate2"), ValidDate).FillRequired = False
            CType(e.Item.FindControl("chkTCCheck2"), CheckBox).Attributes.Add("OnClick", _
         "return CheckTC('" & Trim(CType(e.Item.FindControl("uscTCPromiseDate2"),  _
         ValidDate).ClientID) & "',this.checked);")
        End If
    End Sub
    Private Sub dtgCrossDefault_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCrossDefault.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblCDNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
#End Region

End Class