﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditAssetData.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.EditAssetData" %>

<%@ Register TagPrefix="uc1" TagName="UCAO" Src="../../../../Webform.UserController/UCAO.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpAsset" Src="../../../../Webform.UserController/ucLookUpAsset.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpSupplier" Src="../../../../Webform.UserController/ucLookUpSupplier.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditAssetData</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value;
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
        function calculateTotalPembiayaan() {
            var oOTR = document.getElementById('<%= txtOTR.ClientID %>');
            var oDownPayment = document.getElementById('<%= txtDP.ClientID %>');
            var oTotalPembiayaan = document.getElementById('<%= txtTotalPembiayaan.ClientID %>');

            oTotalPembiayaan.value = parseFloat(oOTR.value - oDownPayment.value);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td align="center">
                <table cellspacing="0" cellpadding="0" width="95%" border="0">
                    <tr>
                        <td>
                            <font color="red">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <font color="red"><i>*) Mandatory</i></font>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="95%" border="0">
                    <tr class="trtopikuning">
                        <td class="tdtopi" align="center">
                            EDIT DATA ASSET
                        </td>
                    </tr>
                </table>
                <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                    <tr>
                        <td class="tdgenap" width="25%">
                            Nama Customer
                        </td>
                        <td class="tdganjil">
                            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr class="tdjudul">
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Nama Supplier&nbsp;<font color="red">*)</font>
                        </td>
                        <td class="tdganjil" align="left">
                            <asp:HyperLink ID="lblSupplier" runat="server"></asp:HyperLink><uc1:uclookupsupplier
                                id="UCSupplier" runat="server"></uc1:uclookupsupplier>
                            <asp:Label ID="lblreqPO" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlAgenFee" runat="server" Width="100%" Visible="False">
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdgenap" style="font-weight: bold; color: blue" width="25%">
                                Agent Fee Untuk Motor Bekas
                            </td>
                            <td class="tdganjil">
                                <asp:Label ID="lblUsedAMountFee" runat="server" ForeColor="LimeGreen" Font-Bold="True"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnl1" runat="server" Width="100%">
                    <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr>
                            <td class="tdganjil" valign="bottom" align="left" height="30">
                                <asp:ImageButton ID="imbOK" runat="server" CausesValidation="true" ImageUrl="../../../../images/buttonok.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnl2" runat="server" Width="100%">
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdgenap" width="25%">
                                Skema Incentive Supplier
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:RadioButtonList ID="rboSSI" runat="server" Width="137px" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="1" Selected="True">On</asp:ListItem>
                                    <asp:ListItem Value="0">Off</asp:ListItem>
                                </asp:RadioButtonList>
                                <font color="red"></font>
                            </td>
                        </tr>
                        <tr class="tdjudul">
                            <td colspan="4">
                                DATA ASSET
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Kendaraan&nbsp;<font color="red">*)</font>
                            </td>
                            <td class="tdganjil" align="left" colspan="3">
                                <uc1:uclookupasset id="UCAsset" runat="server"></uc1:uclookupasset>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Harga OTR (On The Road)&nbsp;<font color="red">*)</font>
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:TextBox ID="txtOTR" runat="server" onchange="calculateTotalPembiayaan()" CssClass="inptype"
                                    MaxLength="15" Columns="18"></asp:TextBox>
                                <asp:RangeValidator ID="RangeValidator2" runat="server" MaximumValue="999999999999999999"
                                    Type="Double" MinimumValue="0.0000000000001" ErrorMessage="Harap isi dengan Angka"
                                    ControlToValidate="txtOTR" Display="Dynamic"></asp:RangeValidator>&nbsp;
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Harap isi harga OTR"
                                    ControlToValidate="txtOTR" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Uang Muka
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:TextBox ID="txtDP" runat="server" onchange="calculateTotalPembiayaan()" CssClass="inptype"
                                    MaxLength="15" Columns="18"></asp:TextBox>&nbsp;
                                <asp:RangeValidator ID="RangeValidator1" runat="server" MaximumValue="999999999999.99"
                                    Type="Double" MinimumValue="0" ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtDP"
                                    Display="Dynamic"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Total Pembiayaan
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:TextBox ID="txtTotalPembiayaan" runat="server" CssClass="inptype" Columns="18"
                                    Enabled="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                <asp:Label ID="lblSerial1" runat="server"></asp:Label>
                            </td>
                            <td class="tdganjil" width="25%">
                                <font color="red">
                                    <asp:TextBox ID="txtSerial1" runat="server" Width="95%" CssClass="inptype" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFVSerial1" runat="server" ControlToValidate="txtSerial1"
                                        Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator></font>
                            </td>
                            <td class="tdgenap">
                                Asset : Baru/Bekas
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:Label ID="lblNewUsed" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                <asp:Label ID="lblSerial2" runat="server"></asp:Label>
                            </td>
                            <td class="tdganjil" width="25%">
                                <font color="red">
                                    <asp:TextBox ID="txtSerial2" runat="server" Width="95%" CssClass="inptype" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFVSerial2" runat="server" ControlToValidate="txtSerial2"
                                        Display="Dynamic" Enabled="False"></asp:RequiredFieldValidator></font>
                            </td>
                            <td class="tdgenap">
                                Penggunaan&nbsp;<font color="red">*)</font>
                                <asp:Label ID="Label1" runat="server">Label</asp:Label>
                                <asp:Label ID="LblUsageHidden" runat="server"></asp:Label>
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:DropDownList ID="cboUsage" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih salah satu"
                                    ControlToValidate="cboUsage" Display="Dynamic" InitialValue="Select One"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Tahun Produksi&nbsp;<font color="red">*)</font>
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:TextBox ID="txtYear" runat="server" CssClass="inptype" MaxLength="4" Columns="7"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFVYear" runat="server" ErrorMessage="Harap isi tahun Produksi"
                                    ControlToValidate="txtYear" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="RangeValidator3" runat="server" Type="Integer" MinimumValue="0"
                                    ErrorMessage="Tahun harus <= hari ini" ControlToValidate="txtYear" Display="Dynamic"></asp:RangeValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Tahun Salah"
                                    ControlToValidate="txtYear" Display="Dynamic" ValidationExpression="\d{4}"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Pemakai
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtPemakai" runat="server" Width="90%" CssClass="inptype" MaxLength="50"></asp:TextBox>
                            </td>
                            <td class="tdgenap">
                                Lokasi
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtLokasi" runat="server" Width="90%" CssClass="inptype" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Harga Cepat Laku
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:TextBox ID="txtHargaLaku" runat="server" CssClass="inptype" MaxLength="15" Columns="18">0</asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator6" runat="server" MaximumValue="999999999999.99"
                                    Type="Double" MinimumValue="0" ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtHargaLaku"
                                    Display="Dynamic"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Pembanding Showroom 1
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtSR1" runat="server" Width="90%" CssClass="inptype" MaxLength="50"></asp:TextBox>
                            </td>
                            <td class="tdgenap">
                                Harga Pembanding Showroom 1
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtHargaSR1" runat="server" CssClass="inptype" MaxLength="15" Columns="18">0</asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator4" runat="server" MaximumValue="999999999999.99"
                                    Type="Double" MinimumValue="0" ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtHargaSR1"
                                    Display="Dynamic"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Pembanding Showroom 2
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtSR2" runat="server" Width="90%" CssClass="inptype" MaxLength="50"></asp:TextBox>
                            </td>
                            <td class="tdgenap">
                                Harga Pembanding Showroom 2
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtHargaSR2" runat="server" CssClass="inptype" MaxLength="15" Columns="18">0</asp:TextBox>
                                <asp:RangeValidator ID="Rangevalidator5" runat="server" MaximumValue="999999999999.99"
                                    Type="Double" MinimumValue="0" ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtHargaSR2"
                                    Display="Dynamic"></asp:RangeValidator>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr>
                            <td>
                                <asp:DataGrid ID="dtgAttribute" runat="server" Width="100%" CssClass="tablegrid"
                                    CellSpacing="1" CellPadding="2" BorderWidth="0px" AutoGenerateColumns="False"
                                    ShowHeader="False">
                                    <ItemStyle CssClass="tdganjil"></ItemStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="Name">
                                            <ItemStyle Width="25%" CssClass="tdgenap"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemStyle HorizontalAlign="Left" Width="75%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAttribute" runat="server" Width="75%" CssClass="inptype" MaxLength='<%# DataBinder.eval(Container,"DataItem.AttributeLength") %>'
                                                    Text='<%# DataBinder.eval(Container,"DataItem.AttributeContent") %>'>
                                                </asp:TextBox>
                                                <asp:Label ID="lblVAttribute" runat="server" ForeColor="Red">Attribute sudah ada</asp:Label>
                                                <asp:RegularExpressionValidator ID="RVAttribute" Enabled='<%# DataBinder.eval(Container,"DataItem.AttributeType") %>'
                                                    runat="server" Display="Dynamic" ControlToValidate="txtAttribute" ErrorMessage="Harap isi dengan Angka"
                                                    ValidationExpression="\d*">
                                                </asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="AttributeID"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdjudul" colspan="4">
                                EDIT REGISTRASI BPKB
                                <asp:Button ID="btnCopyAddress" runat="server" CausesValidation="False" Text="Copy Address">
                                </asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Nama
                            </td>
                            <td class="tdganjil" colspan="3">
                                <font face="Tahoma, Verdana" size="2">
                                    <asp:TextBox ID="txtName" runat="server" Width="90%" CssClass="inptype" MaxLength="50"></asp:TextBox></font>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr>
                            <td class="tdganjil">
                                <uc1:uccompanyadress id="UCAddress" runat="server"></uc1:uccompanyadress>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" border="0">
                        <tr>
                            <td class="tdgenap">
                                Tanggal STNK
                            </td>
                            <td class="tdganjil" colspan="3">
                                <uc1:validdate id="uscTax" runat="server"></uc1:validdate>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Keterangan BPKB
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:TextBox ID="txtAssetNotes" runat="server" Width="305px" CssClass="inptype" TextMode="MultiLine"
                                    Height="54px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="tdjudul">
                            <td colspan="4">
                                ASURANSI KENDARAAN&nbsp;<font color="red">*)</font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Di Asuransi oleh
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:DropDownList ID="cboInsuredBy" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Salah satu"
                                    ControlToValidate="cboInsuredBy" Display="Dynamic" InitialValue="Select One"></asp:RequiredFieldValidator>
                                <asp:Label ID="LblInsuredByHidden" runat="server" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Di Bayar Oleh
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:DropDownList ID="cboPaidBy" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="lblVPaidBy" runat="server" ForeColor="Red" Visible="False">Please Select One!</asp:Label>
                                <asp:Label ID="LblPaidByHidden" runat="server" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdjudul">
                            <td colspan="2">
                                KARYAWAN&nbsp;<font color="red">*)</font>
                            </td>
                            <td colspan="2">
                                KARYAWAN SUPPLIER&nbsp;<font color="red">*)</font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Nama CMO
                            </td>
                            <td class="tdganjil" width="25%">
                                <uc1:UCAO id="lblAO" runat="server"></uc1:UCAO>
                                <asp:Label ID="lblErrAccOff" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                            </td>
                            <asp:Label ID="lblAO1" runat="server"></asp:Label>
                            <td class="tdgenap">
                                Salesman
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:DropDownList ID="cboSalesman" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblSalesmanHidden" runat="server" Visible="False"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Harap pilih salah satu"
                                    ControlToValidate="cboSalesman" Display="Dynamic" InitialValue="Select One"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Credit Analyst
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:DropDownList ID="cboCA" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblCAHidden" runat="server" Visible="False"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Salah Satu"
                                    ControlToValidate="cboCA" Display="Dynamic" InitialValue="Select One"></asp:RequiredFieldValidator>
                            </td>
                            <td class="tdgenap">
                                Sales Supervisor
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:DropDownList ID="cboSalesSpv" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblSalesSupervisorHidden" runat="server" Visible="False"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Surveyor
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:DropDownList ID="cboSurveyor" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblSurveyorHidden" runat="server" Visible="False"></asp:Label>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap pilih Salah satu"
                                    ControlToValidate="cboSurveyor" Display="Dynamic" InitialValue="Select One"></asp:RequiredFieldValidator>
                            </td>
                            <td class="tdgenap">
                                Supplier Admin
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:DropDownList ID="cboSupplierAdm" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblSupplierAdminHidden" runat="server" Visible="False"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopikuning">
                            <td class="tdtopi" align="center">
                                Edit DOKUMEN ASSET
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="dtgAssetDoc" runat="server" Width="100%" CssClass="tablegrid" CellSpacing="1"
                                    CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tdganjil"></ItemStyle>
                                    <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Width="7%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="AssetDocName" HeaderText="DOKUMEN">
                                            <HeaderStyle Width="20%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="NO DOKUMEN">
                                            <HeaderStyle Width="20%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtNumber" runat="server" Width="95%" CssClass="inptype" Text='<%# DataBinder.eval(Container,"DataItem.DocumentNo") %>'>
                                                </asp:TextBox>
                                                <asp:Label ID="lblVNumber" runat="server" ForeColor="Red">No Sudah Ada</asp:Label>
                                                <asp:Label ID="lblVNumber2" runat="server" Visible='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>'
                                                    ForeColor="Red">Number must be filled!</asp:Label>
                                                <asp:RequiredFieldValidator ID="RFVNumber" Enabled='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>'
                                                    runat="server" ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtNumber"
                                                    Display="Dynamic">
                                                </asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="PERIKSA">
                                            <HeaderStyle Width="12%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk" runat="server" Checked='<%# DataBinder.eval(Container,"DataItem.IsDocExist") %>'>
                                                </asp:CheckBox>
                                                <asp:Label ID="lblVChk" runat="server" ForeColor="Red">Harus diperiksa</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CATATAN">
                                            <HeaderStyle Width="36%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtNotes" runat="server" Width="95%" CssClass="inptype" Text='<%# DataBinder.eval(Container,"DataItem.Notes") %>'>
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="MandatoryForNewAsset"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="MandatoryForUsedAsset"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="AssetDocID"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="IsMainDoc"></asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="IsValueNeeded"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr valign="bottom">
                            <td align="left">
                                &nbsp;
                                <asp:ImageButton ID="imbSave" runat="server" CausesValidation="True" ImageUrl="../../../../Images/ButtonSave.gif">
                                </asp:ImageButton>&nbsp;
                                <asp:ImageButton ID="imbCancel" runat="server" CausesValidation="False" ImageUrl="../../../../Images/ButtonCancel.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </form>
</body>
</html>
