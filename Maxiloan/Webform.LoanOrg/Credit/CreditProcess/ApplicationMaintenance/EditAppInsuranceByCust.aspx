﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditAppInsuranceByCust.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.EditAppInsuranceByCust" %>

<%@ Register TagPrefix="uc1" TagName="UcCoverageType" Src="../../../../Webform.UserController/UcCoverageType.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../Webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditAppInsuranceByCust</title>
</head>
<body>
    <form id="form1" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="95%" border="0">
        <tr>
            <td colspan="3">
                <font color="red"><i>
                    <asp:Label ID="LblErrorMessages" runat="server"></asp:Label>&nbsp;<font color="red"><i>
                        Mandatory</i></font></i></font>
            </td>
        </tr>
    </table>
    <table id="Table2" cellspacing="0" cellpadding="0" width="95%" border="0">
        <tr class="trtopikuning">
            <td class="tdtopi" align="center">
                EDIT - ASURANSI OLEH CUSTOMER
            </td>
        </tr>
    </table>
    <table class="tablegrid" id="Table3" cellspacing="1" cellpadding="2" width="95%"
        border="0">
        <tr>
            <td class="tdgenap" width="21%">
                No Aplikasi
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <asp:Label ID="LblApplicationID" runat="server"></asp:Label>
                <asp:Label ID="LblBranchID" runat="server" Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                Nama Customer
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <asp:Label ID="LblCustomerName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                Asuransi Oleh
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <asp:Label ID="LblInsuredByName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                Dibayar Oleh
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <asp:Label ID="LblPaidByName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" style="height: 34px" width="21%">
                Jangk Waktu Pembiayaan&nbsp;
            </td>
            <td class="tdganjil" style="height: 34px" width="79%" colspan="3">
                <asp:TextBox ID="TxtTenorCredit" runat="server" MaxLength="2" 
                    Width="64px"></asp:TextBox>
                <asp:Label ID="LblMinimumTenor" runat="server" Visible="False"></asp:Label><asp:Label
                    ID="LblMaximumTenor" runat="server" Visible="False"></asp:Label><asp:RequiredFieldValidator
                        ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi Jangka Waktu Pembiayaan"
                        ControlToValidate="TxtTenorCredit" Display="Dynamic"  CssClass="validator_general"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                            ID="RegularExpressionValidator1" runat="server" ErrorMessage="Jangka waktu Pembiayaan harus angka"
                            ControlToValidate="TxtTenorCredit" Display="Dynamic" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                Perusahaan Asuransi
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <font color="red">
                    <asp:TextBox ID="TxtInsuranceCompany" runat="server" MaxLength="50" 
                        Width="272px"></asp:TextBox></font>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                Nilai Pertanggungan
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <asp:TextBox ID="TxtAmountCoverage" runat="server" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TxtAmountCoverage"
                    ErrorMessage="Harap isi dengan angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                Jenis Cover
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <uc1:uccoveragetype id="oCoverageType" runat="server">
                </uc1:uccoveragetype>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                No Polis
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <asp:TextBox ID="TxtPolicyNumber" runat="server" MaxLength="50" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                Tanggal Jatuh Tempo
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <uc1:validdate id="oExpiredDate" runat="server">
                </uc1:validdate>
            </td>
        </tr>
        <tr>
            <td class="tdgenap" width="21%">
                Catatan
            </td>
            <td class="tdganjil" width="79%" colspan="3">
                <asp:TextBox ID="txtinsurancenotes" runat="server" MaxLength="500" 
                    Width="392px" Height="112px" TextMode="MultiLine"></asp:TextBox>
            </td>
        </tr>
    </table>
    <table id="Table4" cellspacing="0" cellpadding="0" width="95%" border="0">
        <tr>
            <td align="left">
                <asp:ImageButton ID="imbOK" runat="server" ImageUrl="../../../../Images/ButtonSave.gif">
                </asp:ImageButton>&nbsp;
                <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../../../../Images/ButtonCancel.gif"
                    CausesValidation="False"/>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
