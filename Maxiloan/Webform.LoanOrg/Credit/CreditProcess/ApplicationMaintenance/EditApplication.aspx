﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditApplication.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.EditApplication" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../../../Webform.UserController/ucLookUpCustomer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditApplication</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function SelectFromArray(itemArray) {
            var objAdd = 'document.forms[0].UCMailingAddress_';
            var pAddress = eval(objAdd + 'txtAddress');
            var pRT = eval(objAdd + 'txtRT');
            var pRW = eval(objAdd + 'txtRW');
            var pKelurahan = eval(objAdd + 'oLookUpZipCode_txtKelurahan');
            var pKecamatan = eval(objAdd + 'oLookUpZipCode_txtKecamatan');
            var pCity = eval(objAdd + 'oLookUpZipCode_txtCity');
            var pZipCode = eval(objAdd + 'oLookUpZipCode_txtZipCode');
            var pAreaPhone1 = eval(objAdd + 'txtAreaPhone1');
            var pPhone1 = eval(objAdd + 'txtPhone1');
            var pAreaPhone2 = eval(objAdd + 'txtAreaPhone2');
            var pPhone2 = eval(objAdd + 'txtPhone2');
            var pAreaFax = eval(objAdd + 'txtAreaFax');
            var pFax = eval(objAdd + 'txtFax');

            pAddress.value = itemArray[0][0];
            pRT.value = itemArray[0][1];
            pRW.value = itemArray[0][2];
            pKelurahan.value = itemArray[0][3];
            pKecamatan.value = itemArray[0][4];
            pCity.value = itemArray[0][5];
            pZipCode.value = itemArray[0][6];
            pAreaPhone1.value = itemArray[0][7];
            pPhone1.value = itemArray[0][8];
            pAreaPhone2.value = itemArray[0][9];
            pPhone2.value = itemArray[0][10];
            pAreaFax.value = itemArray[0][11];
            pFax.value = itemArray[0][12];
            return false;
        }

        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date + '_txtDate');
            var calendar = eval('document.forms[0].all.' + date + '_txtDate_imgCalender');
            if (chk == true) {
                text.disabled = true;
                calendar.disabled = true;
                text.value = '';
                return true;
            }
            else {
                text.disabled = false;
                calendar.disabled = false;
                return true;
            }
        }
        function InterestType() {
            var myID = 'cboInstScheme';
            var obj = eval('document.forms[0].' + myID);
            if (document.forms[0].cboInterestType.value == 'FX') {
                obj.disabled = false;
            }
            else {
                obj.disabled = true;
            }
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }

        function InstScheme() {
            if (document.forms[0].cboInstScheme.value == 'ST') {
                document.forms[0].rdoSTTYpe(0).disabled = false;
                document.forms[0].rdoSTTYpe(1).disabled = false;
                document.forms[0].rdoSTTYpe(2).disabled = false;
            }
            else {
                document.forms[0].rdoSTTYpe(0).disabled = true;
                document.forms[0].rdoSTTYpe(1).disabled = true;
                document.forms[0].rdoSTTYpe(2).disabled = true;
            }
        }
        function rdoDisabled() {
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }
								
    </script>
</head>
<body>
    <div id="Div1" runat="server">
    </div>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
    <ContentTemplate>
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td align="center">
                <table cellspacing="0" cellpadding="0" width="95%" border="0">
                    <tr>
                        <td colspan="3">
                            <font color="red"><i> Mandatory&nbsp;&nbsp;&nbsp;&nbsp;</i>
                                <asp:Label ID="lblMessage" runat="server"></asp:Label></font>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="95%" border="0">
                    <tr class="trtopikuning">
                        <td class="tdtopi" align="center">
                            EDIT APLIKASI
                        </td>
                    </tr>
                </table>
                <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                    <tr>
                        <td class="tdgenap" width="25%">
                            Nama Customer
                        </td>
                        <td class="tdganjil">
                            <asp:HyperLink ID="lblCustName" runat="server"></asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlEdit" runat="server" Width="100%">
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdgenap" width="25%">
                                No Aplikasi
                            </td>
                            <td class="tdganjil">
                                <asp:HyperLink ID="lblApplicationForm" runat="server"></asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                    <tr>
                        <td class="tdgenap" width="25%">
                            Product Jual
                        </td>
                        <td class="tdganjil" align="left">
                            <asp:HyperLink ID="lblProductOffering" runat="server"></asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnl1" runat="server" Width="100%">
                    <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr>
                            <td class="tdganjil" valign="bottom" align="right" height="30">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnl2" runat="server" Width="100%">
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdgenap" width="25%">
                                Jenis Margin&nbsp;
                            </td>
                            <td class="tdganjil">
                                <asp:DropDownList ID="cboInterestType" runat="server" onchange="InterestType();">
                                    <asp:ListItem Value="FX">Fixed Rate</asp:ListItem>
                                    <asp:ListItem Value="FL">Floating Rate</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Skema Angsuran&nbsp;
                            </td>
                            <td class="tdganjil">
                                <asp:DropDownList ID="cboInstScheme" runat="server" onchange="InstScheme();">
                                    <asp:ListItem Value="RF">Regular Fixed Installment Scheme</asp:ListItem>
                                    <asp:ListItem Value="IR">Irregular Installment</asp:ListItem>
                                    <asp:ListItem Value="ST">Step Up/Step Down</asp:ListItem>
                                    <asp:ListItem Value="EP">Even Principle</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Tipe Step Up Step Down
                            </td>
                            <td class="tdganjil">
                                <asp:RadioButtonList ID="rdoSTTYpe" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="NM" Selected>Normal</asp:ListItem>
                                    <asp:ListItem Value="RL">Regular Leasing</asp:ListItem>
                                    <asp:ListItem Value="LS">Leasing</asp:ListItem>
                                </asp:RadioButtonList>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopikuning">
                            <td class="tdtopi" align="center">
                                EDIT DATA APLIKASI
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" border="0">
                        <asp:Panel ID="PnlSpouse" runat="server">
                            <tbody>
                                <tr>
                                    <td class="tdgenap" width="25%">
                                        Nama Pansangan
                                    </td>
                                    <td class="tdganjil" align="left" colspan="3">
                                        <uc1:ucLookUpCustomer id="UCSpouse" runat="server" Visible="False"></uc1:ucLookUpCustomer>
                                    </td>
                                </tr>
                                </tbody>
                        </asp:Panel>
                        <tr>
                            <td class="tdgenap">
                                Emergency Kontak
                            </td>
                            <td class="tdganjil">
                                <uc1:ucLookUpCustomer id="UCGuarantor" runat="server" Visible="False"></uc1:ucLookUpCustomer>
                            </td>
                            <td class="tdgenap">
                                Tanggal Kontrak
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <uc1:ValidDate id="uscAgreementDate" runat="server"></uc1:ValidDate></font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Cara Pembayaran&nbsp;
                            </td>
                            <td class="tdganjil">
                                <asp:DropDownList ID="cboWayPymt" runat="server">
                                    <asp:ListItem Value="SB">Slip Setoran Bank</asp:ListItem>
                                    <asp:ListItem Value="CA">Cash</asp:ListItem>
                                    <asp:ListItem Value="TF">Transfer</asp:ListItem>
                                    <asp:ListItem Value="PD">PDC</asp:ListItem>
                                    <asp:ListItem Value="SI">Standing Instructions</asp:ListItem>
                                    <asp:ListItem Value="PM">Pickup Monthly</asp:ListItem>
                                    <asp:ListItem Value="PG">Potong Gaji</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdgenap">
                                Tanggal Survey&nbsp;
                            </td>
                            <td class="tdganjil" width="25%">
                                <uc1:ValidDate id="uscSurveyDate" runat="server"></uc1:ValidDate>
                                <asp:Label ID="lblVSurveyDate" runat="server" >Fill Survey Date!</asp:Label>&nbsp;
                                <asp:Label ID="lblVSurveyDate2" runat="server" >Survey Date must <= Today!</asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Sumber Aplikasi&nbsp;
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:DropDownList ID="cboSourceApp" runat="server">
                                    <asp:ListItem Value="D">Direct RO</asp:ListItem>
                                    <asp:ListItem Value="I">Indirect</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="tdgenap">
                                &nbsp;
                            </td>
                            <td class="tdganjil" width="25%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdjudul" colspan="2">
                                BIAYA-BIAYA
                            </td>
                            <td class="tdjudul" colspan="2">
                                DATA FINANCIAL
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Biaya Administrasi
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtAdminFee" runat="server" Columns="18" MaxLength="12" ></asp:TextBox>
                                    <asp:CheckBox ID="chkIsAdminFeeCredit" runat="server" Checked="True" Text="Pembiayaan">
                                    </asp:CheckBox></font>
                            </td>
                            <td class="tdgenap">
                                Denda Keterlambatan
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtLatePenalty" runat="server" Width="56px" Columns="19" MaxLength="12"
                                         Enabled="False"></asp:TextBox>&nbsp;<span style="font-size: 12pt;
                                            font-family: Tahoma; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US;
                                            mso-fareast-language: EN-US; mso-bidi-language: AR-SA"><font face="Verdana">‰</font></span></font>&nbsp;/
                                day
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Fiducia
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <p>
                                        <asp:TextBox ID="txtFiduciaFee" runat="server" Columns="18" MaxLength="12" ></asp:TextBox>
                                        <asp:RangeValidator ID="RVFiducia" runat="server" MaximumValue="99999999" MinimumValue="0"
                                            ErrorMessage="*" Type="Double" ControlToValidate="txtFiduciaFee" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                                        <asp:RangeValidator ID="RangeValidator9" runat="server" MaximumValue="99999999" MinimumValue="0"
                                            ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtFiduciaFee" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator><br/>
                                        <asp:DropDownList ID="CboStatusFiducia" runat="server">
                                            <asp:ListItem Value="0">Select One</asp:ListItem>
                                            <asp:ListItem Value="False">Optional</asp:ListItem>
                                            <asp:ListItem Value="True">Mandatory</asp:ListItem>
                                        </asp:DropDownList>
                                    </p>
                                </font>
                            </td>
                            <td class="tdjudul" colspan="2">
                                <strong>LAINNYA</strong>
                            </td>
                        </tr>
                        <tr class="tdgenap">
                            <td>
                                Provisi
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtProvisionFee" runat="server" Columns="18" MaxLength="12" ></asp:TextBox>
                                    <asp:CheckBox ID="chkIsProvisiCredit" runat="server" Text="Pembiayaan"></asp:CheckBox></font>
                            </td>
                            <td>
                                Recourse
                            </td>
                            <td class="tdganjil">
                                <asp:Label ID="lblRecourse" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdgenap">
                            <td>
                                Notaris
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtNotaryFee" runat="server" Columns="18" MaxLength="12" ></asp:TextBox>
                                    <asp:RangeValidator ID="RVNotary" runat="server" MaximumValue="99999999" MinimumValue="0"
                                        ErrorMessage="*" Type="Double" ControlToValidate="txtNotaryFee" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator></font>
                            </td>
                            <td>
                                Tambahan administrasi
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="TxtAdditionalAdmin" runat="server" Width="109px" ></asp:TextBox>
                                <asp:RangeValidator ID="RVAdditionalAdmin" runat="server" MaximumValue="99999999"
                                    MinimumValue="0" ErrorMessage="*" Type="Double" ControlToValidate="TxtAdditionalAdmin"
                                    Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr class="tdgenap">
                            <td>
                                Survey
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtSurveyFee" runat="server" Columns="18" MaxLength="12" ></asp:TextBox></font>
                            </td>
                            <td style="font-weight: bold; color: blue">
                            </td>
                            <td class="tdganjil">
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Biaya Balik Nama (BBN)
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtBBNFee" runat="server" Columns="18" MaxLength="12" ></asp:TextBox>
                                    <asp:RangeValidator ID="RangeValidator6" runat="server" MaximumValue="999999999999.99"
                                        MinimumValue="0" ErrorMessage="Harap isi dengan Angka" Type="Double" ControlToValidate="txtBBNFee"
                                        Display="Dynamic" CssClass="validator_general"></asp:RangeValidator></font>
                            </td>
                            <td class="tdgenap">
                                &nbsp;
                            </td>
                            <td class="tdganjil">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Biaya Lainnya
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtOtherFee" runat="server" Columns="18" MaxLength="12" ></asp:TextBox>
                                    <asp:RangeValidator ID="RangeValidator7" runat="server" MaximumValue="999999999999.99"
                                        MinimumValue="0" ErrorMessage="Harap isi dengan Angka" Type="Double" ControlToValidate="txtOtherFee"
                                        Display="Dynamic" CssClass="validator_general"></asp:RangeValidator></font>
                            </td>
                            <td class="tdgenap">
                                &nbsp;
                            </td>
                            <td class="tdganjil">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" border="0">
                        <tr class="tdjudul">
                            <td valign="top">
                                CATATAN APLIKASI
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtAppNotes" runat="server" Width="95%"  Height="68px"
                                        TextMode="MultiLine"></asp:TextBox></font>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" border="0">
                        <tr class="tdjudul">
                            <td valign="top">
                                EMERGENCY CONTACT
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdgenap" width="25%">
                                Nama
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtRefName" runat="server" Columns="18" MaxLength="14" ></asp:TextBox></font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Hubungan
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtRefJobTitle" runat="server" Columns="18" MaxLength="14" ></asp:TextBox></font>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr>
                            <td class="tdganjil">
                                <uc1:UcCompanyAdress id="UCRefAddress" runat="server"></uc1:UcCompanyAdress>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdgenap" width="25%">
                                No HandPhone
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtRefMobile" runat="server" Columns="24" MaxLength="20" ></asp:TextBox>&nbsp;
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="No HandPhone Salah"
                                        ControlToValidate="txtRefMobile" Display="Dynamic" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator></font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                e-Mail
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtRefEmail" runat="server" Columns="35" MaxLength="30" ></asp:TextBox>&nbsp;
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Alamat e-mail Salah"
                                        ControlToValidate="txtRefEmail" Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="validator_general"></asp:RegularExpressionValidator></font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Catatan
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:TextBox ID="txtRefNotes" runat="server" Width="95%"  Height="68px"
                                        TextMode="MultiLine"></asp:TextBox></font>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" border="0">
                        <tr class="tdjudul">
                            <td valign="top">
                                ALAMAT SURAT MENYURAT<font style="color: red"><strong></strong></font>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                        <tr>
                            <td class="tdgenap" width="25%">
                                Copy Alamat Dari
                            </td>
                            <td class="tdganjil">
                                <font size="2">
                                    <asp:DropDownList ID="cboCopyAddress" runat="server" onchange="SelectFromArray((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]);">
                                    </asp:DropDownList>
                                    &nbsp; </font>
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr>
                            <td class="tdganjil">
                                <uc1:UcCompanyAdress id="UCMailingAddress" runat="server"></uc1:UcCompanyAdress>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopikuning">
                            <td class="tdtopi" align="center">
                                KONTRAK LAIN YANG DIMILIKI
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="dtgCrossDefault" runat="server" Width="100%" CssClass="grid_general"
                                    AutoGenerateColumns="False" BorderWidth="0px" CellPadding="3" CellSpacing="1">
                                    
                                    <ItemStyle CssClass="item_grid"/>
                                    <HeaderStyle CssClass="th"/>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCDNo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn SortExpression="Name" HeaderText="NO KONTRAK">
                                            <HeaderStyle Width="20%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCDAgreementNo" runat="server"></asp:Label>
                                                <asp:TextBox ID="txtCDAgreement" runat="server" Columns="25" MaxLength="20" ></asp:TextBox>&nbsp;
                                                <asp:Label ID="lblVCDAgreementNo" runat="server" >No Kontrak Salah</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER">
                                            <HeaderStyle Width="35%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCDName" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>'
                                                    runat="server">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="TGL KONTRAK">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCDAgreementDate" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="STATUS DEFAULT">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCDDefaultStatus" Text='<%# DataBinder.Eval(Container, "DataItem.DefaultStatus") %>'
                                                    runat="server">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="STATUS CONTRACT">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblCDContractStatus" Text='<%# DataBinder.Eval(Container, "DataItem.ContractStatus") %>'
                                                    runat="server">
                                                </asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="DELETE">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imbCDDelete" CommandName="CDDelete" runat="server" ImageUrl="../../../../images/icondelete.gif"
                                                    CausesValidation="False"/>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr valign="bottom">
                            <td align="left">
                                <asp:ImageButton ID="imbCDAdd" runat="server" ImageUrl="../../../../images/buttonadd.gif"
                                    CausesValidation="False"/>&nbsp;
                                <asp:ImageButton ID="imbCDShow" runat="server" ImageUrl="../../../../images/ButtonShowDataAgree.gif"
                                    CausesValidation="False"/>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopikuning">
                            <td class="tdtopi" align="center">
                                SYARAT &amp; KONDISI
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="dtgTC" runat="server" Width="100%" CssClass="grid_general" AutoGenerateColumns="False"
                                    BorderWidth="0px" CellPadding="3" CellSpacing="1" DataKeyField="TCName" PageSize="3">
                                    
                                    <ItemStyle CssClass="item_grid"/>
                                    <HeaderStyle CssClass="th"/>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                            <HeaderStyle Width="25%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                            <HeaderStyle Width="7%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="PERIKSA">
                                            <HeaderStyle Width="8%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkTCChecked" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                                    runat="server"></asp:CheckBox>
                                                <asp:Label ID="lblVTCChecked" runat="server" >Harus diperiksa</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="TGL JANJI">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <uc1:ValidDate id="uscTCPromiseDate" runat="server"></uc1:ValidDate>
                                                <asp:Label ID="lblVPromiseDate" runat="server" >Tgl Janji harus > hari ini</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CATATAN">
                                            <HeaderStyle Width="30%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                                    runat="server" Width="95%" >
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopikuning">
                            <td class="tdtopi" align="center">
                                SYARAT &amp; KONDISI CHECK LIST
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="dtgTC2" runat="server" Width="100%" CssClass="grid_general" AutoGenerateColumns="False"
                                    BorderWidth="0px" CellPadding="3" CellSpacing="1" DataKeyField="TCName" PageSize="3">
                                    
                                    <ItemStyle CssClass="item_grid"/>
                                    <HeaderStyle CssClass="th"/>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                            <HeaderStyle Width="7%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="PERIKSA">
                                            <HeaderStyle Width="8%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkTCCheck2" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                                    runat="server"></asp:CheckBox>
                                                <asp:Label ID="lblVTC2Checked" runat="server" >Harus diperiksa</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="TGL JANJI">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <uc1:ValidDate id="uscTCPromiseDate2" runat="server"></uc1:ValidDate>
                                                <asp:Label ID="lblVPromiseDate2" runat="server" >Tgl Janji harus > hari ini</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CATATAN">
                                            <HeaderStyle Width="30%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                                    Width="95%" >
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr valign="bottom">
                            <td align="left">
                                <asp:ImageButton ID="imbSave" runat="server" ImageUrl="../../../../Images/ButtonSave.gif"
                                    CausesValidation="True"></asp:ImageButton>&nbsp;
                                <asp:ImageButton ID="imbCancel" runat="server" ImageUrl="../../../../Images/ButtonCancel.gif"
                                    CausesValidation="False"/><a href="CustomerList.htm"></a>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        </table>
    </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
