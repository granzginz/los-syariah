﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditIncentiveData.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.EditIncentiveData" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditIncentiveData</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var x = screen.width;
        var y = screen.height - 100;		
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinApplication(pApplicationID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'Application', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br/>
    <asp:Label ID="lblMessage"  runat="server"  Width="600px"></asp:Label><br/>
    <asp:Panel ID="PnlDistribution"  Width="99.69%" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr class="trtopi">
                <td align="center" class="tdtopi">
                    EDIT DATA INCENTIVE
                </td>
            </tr>
            </tr>
        </table>
        <table 
            border="0">
            <tr>
                <td class="tdgenap">
                    No Aplikasi
                </td>
                <td class="tdganjil">
                    <asp:HyperLink ID="hyApplicationIDDist" runat="server"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Nama Customer
                </td>
                <td class="tdganjil">
                    <asp:HyperLink ID="HyCustDist" runat="server"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Refund Premi Supplier
                </td>
                <td class="tdganjil">
                    <asp:Label ID="lblRefundPremi" runat="server" Width="144px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Premi Pada Supplier
                </td>
                <td class="tdganjil">
                    <asp:Label ID="lblPremiumBase" runat="server" Width="144px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Refund Asuransi
                </td>
                <td class="tdganjil">
                    <asp:Label ID="LblPremiumToSupplier" runat="server" Width="144px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Refund Margin
                </td>
                <td class="tdganjil">
                    <asp:Label ID="lblRefundInterest" runat="server" Width="144px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table cellspacing="1" cellpadding="2" width="100%" border="0">
                        <tr class="tdjudul">
                            <td width="20%">
                                PENERIMA
                            </td>
                            <td width="20%">
                                PROSENTASE
                            </td>
                            <td width="20%">
                                JUMLAH TETAP
                            </td>
                            <td width="20%">
                                JUMLAH INCENTIVE
                            </td>
                            <td width="20%">
                                NO REKENING KARYAWAN SUPPLIER
                            </td>
                        </tr>
                        <tr class="tdganjil">
                            <td>
                                Perusahaan
                            </td>
                            <td>
                                <asp:Label ID="lblPCompany" runat="server"></asp:Label>
                            </td>
                            <td>
                                -
                            </td>
                            <td>
                                <asp:TextBox ID="txtInctvAmountCompany" Width="80%" runat="server" MaxLength="15"
                                    ></asp:TextBox>
                                <asp:RangeValidator ID="RgvtxtInctvAmountCompany" runat="server" Display="Dynamic"
                                    ControlToValidate="txtInctvAmountCompany" ErrorMessage="Harap isi dengan Angka"
                                    MaximumValue="99999999999" Type="Double" MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>
                                <asp:Label ID="lblInctvAmountCompany"  runat="server" >This Amount will be added in PO</asp:Label>
                            </td>
                            <td>
                                -
                            </td>
                        </tr>
                        <tr class="tdgenap">
                            <td>
                                GM
                            </td>
                            <td>
                                <asp:Label ID="lblPGM" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFGM" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtInctvAmountGM" Width="80%" runat="server" MaxLength="15" ></asp:TextBox>
                                <asp:RangeValidator ID="RgvtxtInctvAmountGM" runat="server" Display="Dynamic" ControlToValidate="txtInctvAmountGM"
                                    ErrorMessage="Harap isi dengan Angka" MaximumValue="99999999999"
                                    Type="Double" MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                            <td>
                                <asp:Label ID="LblAccountGM" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdganjil">
                            <td>
                                BM
                            </td>
                            <td>
                                <asp:Label ID="lblPBM" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFBM" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtInctvAmountPM" Width="80%" runat="server" MaxLength="15" ></asp:TextBox>
                                <asp:RangeValidator ID="RgvtxtInctvAmountPM" runat="server" Display="Dynamic" ControlToValidate="txtInctvAmountPM"
                                    ErrorMessage="Harap isi dengan Angka" MaximumValue="99999999999"
                                    Type="Double" MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                            <td>
                                <asp:Label ID="LblAccountBM" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdgenap">
                            <td>
                                ADH
                            </td>
                            <td>
                                <asp:Label ID="lblPADH" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFADH" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtInctvAmountADH" Width="80%" runat="server" MaxLength="15" ></asp:TextBox>
                                <asp:RangeValidator ID="RgvtxtInctvAmountADH" runat="server" Display="Dynamic" ControlToValidate="txtInctvAmountADH"
                                    ErrorMessage="Harap isi dengan Angka" MaximumValue="99999999999"
                                    Type="Double" MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                            <td>
                                <asp:Label ID="LblAccountAH" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdganjil">
                            <td>
                                Sales Supervisor
                            </td>
                            <td>
                                <asp:Label ID="lblPSalesSpv" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFSalesSpv" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtInctvAmountSalesSpv" Width="80%" runat="server" MaxLength="15"
                                    ></asp:TextBox>
                                <asp:RangeValidator ID="RgvtxtInctvAmountSalesSpv" runat="server" Display="Dynamic"
                                    ControlToValidate="txtInctvAmountSalesSpv" ErrorMessage="Harap isi dengan Angka"
                                    MaximumValue="99999999999" Type="Double" MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                            <td>
                                <asp:Label ID="LblAccountSV" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdgenap">
                            <td>
                                Sales Person
                            </td>
                            <td>
                                <asp:Label ID="lblPSalesPerson" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFSalesPerson" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtInctvAmountSalesPerson" Width="80%" runat="server" MaxLength="15"
                                    ></asp:TextBox>
                                <asp:RangeValidator ID="RgvtxtInctvAmountSalesPerson" runat="server" Display="Dynamic"
                                    ControlToValidate="txtInctvAmountSalesPerson" ErrorMessage="Harap isi dengan Angka"
                                    MaximumValue="99999999999" Type="Double" MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                            <td>
                                <asp:Label ID="LblAccountSL" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdganjil">
                            <td>
                                Supplier Admin
                            </td>
                            <td>
                                <asp:Label ID="lblPSuppAdm" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFSuppAdm" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtInctvAmountSuppAdm" Width="80%" runat="server" MaxLength="15"
                                    ></asp:TextBox>
                                <asp:RangeValidator ID="RgvtxtInctvAmountSuppAdm" runat="server" Display="Dynamic"
                                    ControlToValidate="txtInctvAmountSuppAdm" ErrorMessage="Harap isi dengan Angka"
                                    MaximumValue="99999999999" Type="Double" MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                            <td>
                                <asp:Label ID="LblAccountAM" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdjudul">
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                Total :
                            </td>
                            <td>
                                <asp:Label ID="lblTotal" Width="80%" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="ImbCalculate" runat="server" ImageUrl="../../../../Images/ButtonCalculate.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="ImbCancel" runat="server" ImageUrl="../../../../Images/ButtonCancel.gif"
                        CausesValidation="False"/>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br/>
    <asp:Panel ID="pnlAfterExecute"  Width="99.69%" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr class="trtopi">
                <td class="tdtopi" align="left">
                    REFUND PREMI KE SUPPLIER
                </td>
            </tr>
        </table>
        <table 
            border="0">
            <tr>
                <td class="tdgenap">
                    Refund Premi ke Supplier
                </td>
                <td class="tdganjil">
                    <asp:Label ID="txtRfnSupplier" runat="server" Width="144px"></asp:Label>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td class="tdganjil" align="left">
                    <asp:ImageButton ID="imbSave" runat="server" ImageUrl="../../../../Images/ButtonSave.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
