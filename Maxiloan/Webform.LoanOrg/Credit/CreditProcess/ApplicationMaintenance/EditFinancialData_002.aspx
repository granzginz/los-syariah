﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditFinancialData_002.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.EditFinancialData_002" %>

<%@ Register TagPrefix="uc1" TagName="ucNumber" Src="../../../../Webform.UserController/ucNumber.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditFinancialData_002</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function NumInst(Tenor, Num, txtNumInst) {
            document.forms[0].txtNumInst.value = Tenor / Num;
        }
        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
        function GracePeriod(txtGracePeriod, cboGracePeriod, Value) {
            if (Value == 'AR') {
                document.forms[0].txtGracePeriod.value = '0'
                document.forms[0].txtGracePeriod.disabled = false;
                document.forms[0].cboGracePeriod.disabled = false;
            }
            else {
                document.forms[0].txtGracePeriod.value = '0'
                document.forms[0].txtGracePeriod.disabled = true;
                document.forms[0].cboGracePeriod.disabled = true;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
        <tr>
            <td align="center">
                <table cellspacing="0" cellpadding="0" width="95%" border="0">
                    <tr>
                        <td>
                            <font color="red"><i> Mandatory</i></font>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblMessage" runat="server" ></asp:Label>
                        </td>
                    </tr>
                </table>
                <table cellspacing="0" cellpadding="0" width="95%" border="0">
                    <tr class="trtopi">
                        <td class="tdtopi" align="center">
                            EDIT DATA FINANCIAL
                        </td>
                    </tr>
                </table>
                <table class="tablegrid" cellspacing="1" cellpadding="2" width="95%" border="0">
                    <tr>
                        <td class="tdgenap" width="30%">
                            No Aplikasi
                        </td>
                        <td class="tdganjil">
                            <asp:HyperLink ID="hyAppId" runat="server"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap" width="30%">
                            Nama Customer
                        </td>
                        <td class="tdganjil">
                            <asp:HyperLink ID="hyCustName" runat="server"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr class="tdjudul">
                        <td colspan="2" height="19">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Jenis Margin
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="lblInterestType" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Skema Angsuran
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="lblInstScheme" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Tipe Step Up Step Down
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="lblStepUpDownType" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" border="0">
                </table>
                <br/>
                <table cellspacing="0" cellpadding="0" width="95%" border="0">
                    <tr class="trtopi">
                        <td class="tdtopi" align="center">
                            DATA FINANCIAL
                        </td>
                    </tr>
                </table>
                <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" border="0">
                    <tbody>
                        <tr>
                            <td class="tdjudul" colspan="2">
                                PREMI ASURANSI
                            </td>
                            <td class="tdjudul" colspan="2">
                                UANG MUKA ASURANSI
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" valign="top" width="25%">
                                Asuransi Asset
                            </td>
                            <td class="tdganjil" valign="top" width="25%">
                                <asp:Label ID="lblAssetInsurance1" runat="server"></asp:Label>
                            </td>
                            <td class="tdgenap" width="25%">
                                Asuransi Asset
                            </td>
                            <td class="tdganjil" width="25%">
                                <font face="Tahoma, Verdana" size="2">
                                    <asp:Label ID="lblAssetInsurance2" runat="server"></asp:Label></font>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdjudul" colspan="4">
                                HUTANG ASURANSI
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Asuransi Asset
                            </td>
                            <td class="tdganjil" colspan="3">
                                <asp:Label ID="lblAssetInsurance3" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="tdjudul">
                            <td valign="top" colspan="2">
                                DATA FINANCIAL
                            </td>
                            <td colspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" style="height: 19px">
                                Total OTR
                            </td>
                            <td class="tdganjil" style="height: 19px">
                                <asp:Label ID="lblOTR" runat="server"></asp:Label>
                            </td>
                            <td class="tdgenap" style="height: 19px">
                                Uang Muka
                            </td>
                            <td class="tdganjil" style="height: 19px">
                                <asp:Label ID="lblDP" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                                Pokok Hutang
                            </td>
                            <td class="tdganjil">
                                <asp:Label ID="lblNTF" runat="server"></asp:Label>
                            </td>
                            <td class="tdgenap">
                                Biaya Administrasi
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox ID="txtAdminFee" runat="server" ></asp:TextBox><asp:Label
                                    ID="lblIsAdminFeeCredit" runat="server"></asp:Label><br/>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic"
                                    ControlToValidate="txtAdminFee" ErrorMessage="Harap isi dengan Angka"  CssClass="validator_general"></asp:RequiredFieldValidator></FONT><asp:RangeValidator
                                        ID="Rangevalidator2" runat="server" Display="Dynamic" ControlToValidate="txtAdminFee"
                                        ErrorMessage="Harap isi dengan Angka" Type="Double" MinimumValue="-999999999999999"
                                        MaximumValue="999999999999999" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap">
                            </td>
                            <td class="tdganjil">
                            </td>
                            <td class="tdgenap">
                                Biaya Provisi
                            </td>
                            <td class="tdganjil">
                                <asp:TextBox runat="server" ID="txtProvisi" ></asp:TextBox><asp:Label
                                    ID="lblIsProvisiCredit" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ErrorMessage="Harap isi dengan Angka"
                                    ControlToValidate="txtProvisi" Display="Dynamic" CssClass="validator_general" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <asp:RangeValidator ID="Rangevalidator3" runat="server" ErrorMessage="Harap isi dengan Angka"
                                    ControlToValidate="txtProvisi" Display="Dynamic" MaximumValue="999999999999999"
                                    MinimumValue="-999999999999999" Type="Double" CssClass="validator_general"></asp:RangeValidator>
                            </td>
                            <tr>
                                <td class="tdgenap">
                                </td>
                                <td class="tdganjil">
                                </td>
                                <td class="tdgenap">
                                </td>
                                <td class="tdganjil">
                                    <asp:ImageButton ID="imgSaveAdminFee" runat="server" ImageUrl="../../../../Images/Buttonsave.gif"
                                        ToolTip="Save Biaya Admin dan Provisi"></asp:ImageButton>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdgenap">
                                    Margin Effective&nbsp;
                                </td>
                                <td class="tdganjil">
                                    <font face="Tahoma, Verdana" size="2"><font color="red">
                                        <asp:TextBox ID="txtEffectiveRate" runat="server" MaxLength="15" 
                                            Columns="18"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                            ControlToValidate="txtEffectiveRate" ErrorMessage="Harap isi dengan margin Effective" CssClass="validator_general"></asp:RequiredFieldValidator><asp:RangeValidator
                                                ID="RVPOEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                                                Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator><asp:RangeValidator
                                                    ID="RVPBEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                                                    Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator><asp:RangeValidator
                                                        ID="RVPEffectiveRate" runat="server" Display="Dynamic" ControlToValidate="txtEffectiveRate"
                                                        Type="Double" MinimumValue="0" MaximumValue="100" CssClass="validator_general"></asp:RangeValidator></font></font>
                                </td>
                                <td class="tdgenap">
                                    Margin Flat
                                </td>
                                <td class="tdganjil">
                                    <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr class="tdgenap">
                                <td>
                                    Subsidi / Komisi(-)&nbsp;
                                </td>
                                <td class="tdganjil">
                                    <font color="red">
                                        <asp:TextBox ID="txtAmount" runat="server" MaxLength="15"  Columns="18"></asp:TextBox><font
                                            color="red">
                                            <asp:RequiredFieldValidator ID="RFVAmount" runat="server" Display="Dynamic" ControlToValidate="txtAmount"
                                                ErrorMessage="Harap isi dengan angka" CssClass="validator_general"></asp:RequiredFieldValidator><asp:RangeValidator
                                                    ID="RVAmount" runat="server" Display="Dynamic" ControlToValidate="txtAmount"
                                                    ErrorMessage="Harap isi dengan angka" Type="Double" MinimumValue="-999999999999999"
                                                    MaximumValue="999999999999999" CssClass="validator_general"></asp:RangeValidator></font></font>
                                </td>
                                <td>
                                    Margin Supplier
                                </td>
                                <td class="tdganjil">
                                    <asp:TextBox ID="txtSupplierRate" runat="server" MaxLength="15" 
                                        Columns="18" ReadOnly="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="tdgenap">
                                <td>
                                    Cara Angsuran&nbsp;
                                </td>
                                <td class="tdganjil">
                                    <asp:DropDownList ID="cboPaymentFreq" runat="server">
                                        <asp:ListItem Value="1">Monthly</asp:ListItem>
                                        <asp:ListItem Value="2">Bimonthly</asp:ListItem>
                                        <asp:ListItem Value="3">Quarterly</asp:ListItem>
                                        <asp:ListItem Value="6">Semi Annualy</asp:ListItem>
                                    </asp:DropDownList>
                                    <font color="red">
                                        <asp:Label ID="LblDefaultPaymentFr" runat="server" Visible="False"></asp:Label></font>
                                </td>
                                <td>
                                    Angsuran Pertama&nbsp;
                                </td>
                                <td class="tdganjil">
                                    <asp:DropDownList ID="cboFirstInstallment" runat="server">
                                        <asp:ListItem Value="Select One">Select One</asp:ListItem>
                                        <asp:ListItem Value="AD">Advance</asp:ListItem>
                                        <asp:ListItem Value="AR">Arrear</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                        ControlToValidate="cboFirstInstallment" ErrorMessage="Harap Pilih Angsuran Pertama"
                                        InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdgenap">
                                    Jangka Waktu
                                </td>
                                <td class="tdganjil">
                                    <asp:TextBox ID="txtNumInst" runat="server"  Columns="4" ReadOnly="True"></asp:TextBox>&nbsp;&nbsp;&nbsp;Tenor
                                    <asp:Label ID="lblTenor" runat="server"></asp:Label><asp:CompareValidator ID="CompareValidator1"
                                        runat="server" Display="Dynamic" ControlToValidate="txtNumInst" ErrorMessage="Jumlah Angsuran harus > 0"
                                        Type="Integer" Operator="GreaterThan" ValueToCompare="0" CssClass="validator_general"></asp:CompareValidator>
                                </td>
                                <td class="tdgenap">
                                    <span style="font-size: 10pt; font-family: Arial; mso-fareast-font-family: 'Times New Roman';
                                        mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: AR-SA">
                                        <asp:Label ID="lblFloatingPeriod" runat="server">Periode Floating</asp:Label></span>
                                </td>
                                <td class="tdganjil">
                                    <font face="Tahoma, Verdana" size="2"><font color="red">
                                        <asp:DropDownList ID="cboFloatingPeriod" runat="server">
                                            <asp:ListItem Value="Select One">Select One</asp:ListItem>
                                            <asp:ListItem Value="M">Monthly</asp:ListItem>
                                            <asp:ListItem Value="Q">Quaterly</asp:ListItem>
                                            <asp:ListItem Value="S">Semi Annually</asp:ListItem>
                                            <asp:ListItem Value="A">Annually</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqFloatingPeriod" runat="server" Display="Dynamic"
                                            ControlToValidate="cboFloatingPeriod" ErrorMessage="Harap Pilih Periode Floating"
                                            InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator><asp:TextBox ID="txtGrossYieldRate"
                                                runat="server" MaxLength="15"  Columns="18" ReadOnly="True"
                                                Visible="False" Width="16px"></asp:TextBox></font></font>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdgenap">
                                    <asp:Label ID="lblInstallment" runat="server">Jumlah Angsuran</asp:Label>
                                </td>
                                <td class="tdganjil">
                                    <font face="Tahoma, Verdana" color="red" size="2">
                                        <asp:TextBox ID="txtInstAmt" runat="server" MaxLength="15"  Width="85%"
                                             Font-Size="Small"></asp:TextBox><asp:RangeValidator ID="RVInstAmt"
                                                runat="server" ControlToValidate="txtInstAmt" Type="Double" CssClass="validator_general"></asp:RangeValidator></font>
                                </td>
                                <td class="tdgenap">
                                    <asp:Label ID="lblNumberOfStep" runat="server">Number of Step</asp:Label>
                                </td>
                                <td class="tdganjil">
                                    <font face="Tahoma, Verdana" color="red" size="2">
                                        <asp:TextBox ID="txtStep" runat="server" MaxLength="15"  Columns="4">0</asp:TextBox><asp:RequiredFieldValidator
                                            ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ControlToValidate="txtStep"
                                            ErrorMessage="Harap isi Number Of Step" CssClass="validator_general"></asp:RequiredFieldValidator><asp:RangeValidator
                                                ID="rvStep" runat="server" Display="Dynamic" ControlToValidate="txtstep" ErrorMessage="Number Of Step harus  > 1"
                                                Type="Double" MinimumValue="2" MaximumValue="999" CssClass="validator_general"></asp:RangeValidator></font>
                                </td>
                            </tr>
                            <tr>
                                <td class="tdgenap">
                                    Grace Period Denda Keterlambatan
                                </td>
                                <td class="tdganjil">
                                    <font face="Tahoma, Verdana" size="2">
                                        <asp:TextBox ID="txtGracePeriod2" runat="server"  Columns="4" MaxLength="2"></asp:TextBox>
                                        <asp:RangeValidator ID="rvGracePeriodLC" runat="server" ControlToValidate="txtGracePeriod2"
                                            Type="Double" CssClass="validator_general"></asp:RangeValidator></font>
                                </td>
                                <td class="tdgenap">
                                    <asp:Label ID="lblCummulative" runat="server">Kumulatif Mulai dari Angs No.</asp:Label>
                                </td>
                                <td class="tdganjil">
                                    <font face="Tahoma, Verdana" color="red" size="2">
                                        <asp:TextBox ID="txtCummulative" runat="server" MaxLength="15" 
                                            Columns="4">0</asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                runat="server" Display="Dynamic" ControlToValidate="txtCummulative" ErrorMessage="Harap diisi Kumulatif" CssClass="validator_general"></asp:RequiredFieldValidator><asp:RangeValidator
                                                    ID="rvCummulative" runat="server" Display="Dynamic" ControlToValidate="txtCummulative"
                                                    ErrorMessage="Kumulatif harus > 0" Type="Double" MinimumValue="1" MaximumValue="999" CssClass="validator_general"></asp:RangeValidator></font><asp:Label
                                                        ID="LblDefaultGracePeriod" runat="server" Visible="False"></asp:Label>
                                </td>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                Grace Period
            </td>
            <td class="tdganjil">
                <asp:TextBox ID="txtGracePeriod" runat="server" MaxLength="2" 
                    Columns="4"></asp:TextBox>
                <asp:DropDownList ID="cboGracePeriod" runat="server">
                    <asp:ListItem Value="I">Interest Only</asp:ListItem>
                    <asp:ListItem Value="R">Roll Over</asp:ListItem>
                </asp:DropDownList>
                <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" Display="Dynamic"
                    ControlToValidate="txtGracePeriod" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
            </td>
            <td class="tdgenap">
                Agent Fee
            </td>
            <td class="tdganjil">
                <asp:Label ID="lblAgentFee" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="95%" border="0">
        <tr valign="bottom">
            <td align="left">
                <asp:ImageButton ID="imbCalcInst" runat="server" ImageUrl="../../../../Images/ButtonCalcInstallAmount.gif">
                </asp:ImageButton>&nbsp;
                <asp:ImageButton ID="imbRecalcEffRate" runat="server" ImageUrl="../../../../Images/ButtonRecalculateRate.gif">
                </asp:ImageButton>
            </td>
        </tr>
        <tr valign="bottom">
            <td align="center" width="100%">
                <div align="left">
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <br/>
    <asp:Panel ID="pnlEntryInstallment" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr class="trtopi">
                <td class="tdtopi" align="center">
                    ANGSURAN
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr align="center">
                <td>
                    <asp:DataGrid ID="dtgEntryInstallment" runat="server" CssClass="grid_general" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False" BorderWidth="0px" CellPadding="3"
                        CellSpacing="1">
                        
                        <ItemStyle CssClass="item_grid"/>
                        <HeaderStyle CssClass="th"/>
                        <Columns>
                            <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="No.Of Step">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtNoStep" runat="server" >0</asp:TextBox>
                                    <asp:RegularExpressionValidator ID="Regularexpressionvalidator5" runat="server" Display="Dynamic"
                                        ControlToValidate="txtNoStep" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator ID="reqNoStep" runat="server" ControlToValidate="txtNoStep"
                                        Display="Dynamic" ErrorMessage="Harap isi dengan Number Of Step" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtNoStep"
                                        Display="Dynamic" MinimumValue="1" MaximumValue="999" Type="Integer" ErrorMessage="No of Step harus > 0" CssClass="validator_general"></asp:RangeValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                           <asp:TemplateColumn HeaderText="JUMLAH">
										<ItemTemplate>
											<asp:TextBox id="txtEntryInstallment" runat="server" >0</asp:TextBox>
											<asp:regularexpressionvalidator id="Regularexpressionvalidator6" runat="server" Display="Dynamic" ControlToValidate="txtEntryInstallment"
												ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
											<asp:RequiredFieldValidator ID="rfInstallment" Runat="server" ControlToValidate="txtEntryInstallment" Display="Dynamic"
												ErrorMessage="Harap isi Jumlah Angsuran" CssClass="validator_general"></asp:RequiredFieldValidator>
										</ItemTemplate>
							</asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <asp:ImageButton ID="ImgEntryInstallment" runat="server" ImageUrl="../../../../Images/ButtonCalcInstallAmount.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlViewST" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr class="trtopi">
                <td class="tdtopi" align="center">
                    VIEW - INSTALLMENT
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr align="center">
                <td>
                    <asp:DataGrid ID="dtgViewInstallment" runat="server" CssClass="grid_general" Width="100%"
                        AllowSorting="True" AutoGenerateColumns="False" BorderWidth="0px" CellPadding="3"
                        CellSpacing="1">
                        
                        <ItemStyle HorizontalAlign="Right" CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle CssClass="th"/>
                        <Columns>
                            <asp:BoundColumn DataField="InsSeqNo" HeaderText="NO"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ANGSURAN">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InstallmentAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.PrincipalAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.InterestAmount"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA POKOK">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingPrincipal"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA MARGIN">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# formatnumber(DataBinder.Eval(Container, "DataItem.OutStandingInterest"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table cellspacing="0" cellpadding="0" width="95%" border="0">
        <tr valign="bottom">
            <td align="left" width="50%">
                <asp:ImageButton ID="imbSave" runat="server" ImageUrl="../../../../Images/ButtonSave.gif">
                </asp:ImageButton>&nbsp;
                <asp:ImageButton ID="imbCancel" runat="server" ImageUrl="../../../../Images/ButtonCancel.gif"
                    CausesValidation="False"/>
            </td>
        </tr>
    </table>
    
    </form>
</body>
</html>
