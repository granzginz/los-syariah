﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class HasilSurvey_002KYC
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucHasilSurveyTab1 As ucHasilSurveyTab
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyTabPhone
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat

    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController
    Private m_controller As New AssetDataController
    Private m_Employee As New EmployeeController
    Private time As String
    Dim pathFolder As String = ""

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Private Property DateEntryKYC() As Date
        Get
            Return CType(ViewState("DateEntryKYC"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryKYC") = Value
        End Set
    End Property

    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            Me.CustomerID = Request("id")
            'Me.ProspectAppID = "-"

            'GetCookies()
            'Modify by WIra 20171016
            Me.ProspectAppID = Request("prospectappid")
            Me.PageAddEdit = Request("page")

            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            InitialPageLoad()
            'fillFormTesting()
            'getHasilSurvey()

            If Me.ProspectAppID <> Nothing And Me.ProspectAppID <> "-" And Request("prospectappid") <> "" And Me.PageAddEdit = "Add" Then
                getHasilMobileSurvey()
            Else
                getHasilSurvey()
            End If
        End If

        If ucViewApplication1.isProceed = True Then
            btnSave.Visible = False
        End If
        btnProceed.Visible = False
    End Sub

    'Sub GetCookies()
    '    Dim cookie As HttpCookie = Request.Cookies("HASIL_SURVEY")

    '    Me.ApplicationID = cookie.Values("ApplicationID")
    '    Me.CustomerID = cookie.Values("CustomerID")
    '    Me.BranchID = cookie.Values("BranchID")
    '    Me.SupplierID = cookie.Values("SupplierID")
    'End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        Me.ApplicationID = Request("appid")

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
                btnProceed.Visible = False
            End If
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("KYC")
        ucHasilSurveyTab1.setLink()


        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        'Dim scoringData As New Parameter.HasilSurvey
        'Dim DtUserList As New DataTable
        'With scoringData
        '    .strConnection = GetConnectionString()
        '    .BranchId = Me.BranchID
        '    .ApplicationId = Me.ApplicationID
        'End With
        'scoringData = oController.getHasilScore(scoringData)
        'DtUserList = scoringData.ListData
        'dtgView.DataSource = DtUserList.DefaultView
        'dtgView.DataBind()

        'Dim tot As Double = 0#
        'Dim lblHasil As Label
        'Dim lblNumber As Label
        'Dim Counter As Integer = 0
        'For x = 0 To dtgView.Items.Count - 1
        '    lblHasil = CType(dtgView.Items(x).FindControl("lblHasil"), Label)
        '    tot += CDbl(lblHasil.Text)

        '    lblNumber = CType(dtgView.Items(x).FindControl("lblNumber"), Label)
        '    Counter += 1
        '    lblNumber.Text = FormatNumber(Counter, 0)
        'Next
        ''lblScore.Text = (tot / dtgView.Items.Count).ToString()
        'lblScore.Text = tot.ToString()

        'If lblScore.Text <= 40 Then
        '    lblResult.Text = "Rendah"
        '    If lblScore.Text > 40 And lblScore.Text <= 60 Then
        '        lblResult.Text = "Sedang"
        '        If lblScore.Text > 60 Then
        '            lblResult.Text = "Tinggi"
        '        End If
        '    End If
        'End If
    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            txtKesimpulan.Text = .AnalisaKYC
        End With

        If oPar.isProceed = True Then
            btnProceed.Visible = False
        Else
            btnProceed.Visible = True
        End If
        cekimage()
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../CreditProcess/HasilSurvey_002Catatan.aspx?appid=" + Me.ApplicationID + "&page=" + Request("page") + "&id=" + Request("id") + "&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.HasilSurvey

        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .CustomerIDSurvey = Me.CustomerID
                .AnalisaKYC = txtKesimpulan.Text                
            End With

            oController.HasilSurvey002KYCSave(oPar)

            Dim errMsg As String

            ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
            ucHasilSurveyTab1.selectedTab("KYC")
            ucHasilSurveyTab1.setLink()
            InitialPageLoad()
            getHasilSurvey()

            If errMsg = String.Empty Then
                ShowMessage(lblMessage, "Data saved!", False)

                If oPar.isProceed = True Then
                    btnProceed.Visible = False
                Else
                    btnProceed.Visible = True
                End If

                btnSave.Visible = False

            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub btnProceed_Click(sender As Object, e As System.EventArgs) Handles btnProceed.Click
        Dim ocustom As New Parameter.HasilSurvey

        With ocustom
            .strConnection = Me.GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.sesBranchId.Replace("'", "")
            .BusinessDate = Me.BusinessDate
            oController.Proceed(ocustom)
            oController.HasilSurvey002AplStepSave(ocustom)
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("KYC")
        ucHasilSurveyTab1.setLink()
        InitialPageLoad()
        getHasilSurvey()

        btnProceed.Visible = False
        ShowMessage(lblMessage, "Aplikasi sudah di proses", False)

    End Sub

    Private Function pathFile(ByVal Group As String, ByVal ExecutorID As String) As String
        Dim strDirectory As String = ""
        'strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & ExecutorID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Sub btnupload_Click(sender As Object, e As System.EventArgs) Handles btnupload.Click
        Dim FileName As String = ""
        Dim strExtension As String = ""
        If uplProsedurKYC.HasFile Then
            If uplProsedurKYC.PostedFile.ContentType = "application/pdf" Then
                FileName = "ProsedurKYC"
                strExtension = Path.GetExtension(uplProsedurKYC.PostedFile.FileName)
                uplProsedurKYC.PostedFile.SaveAs(pathFile("HasilSurvey", Me.CustomerID) + FileName + strExtension)
            End If
        End If
        cekimage()
    End Sub

    Private Sub cekimage()
        If File.Exists(pathFile("HasilSurvey", Me.CustomerID.Trim) + "ProsedurKYC.pdf") Then
            'hypProsedurKYC.NavigateUrl = ResolveClientUrl("../../../xml/" & sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/ProsedurKYC.pdf")
            hypProsedurKYC.NavigateUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/HasilSurvey/" + Me.CustomerID + "/ProsedurKYC.pdf"))
            hypProsedurKYC.Visible = True
            hypProsedurKYC.Text = "ProsedurKYC.pdf"
            hypProsedurKYC.Target = "_blank"
        Else
            hypProsedurKYC.Visible = False
        End If
    End Sub

    Private Function getpathFolder() As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function

    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function

    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryKYC = .DateEntryKYC
        End With
    End Sub

    Sub getHasilMobileSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilMobileSurvey(oPar)

        With oPar       
            txtKesimpulan.Text = .AnalisaKYC
        End With
    End Sub
End Class