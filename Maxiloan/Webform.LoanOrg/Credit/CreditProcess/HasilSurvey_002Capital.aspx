﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HasilSurvey_002Capital.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.HasilSurvey_002Capital" %>

<%@ Register Src="../../../webform.UserController/UcLookUpProductOffering.ascx"
    TagName="UcLookUpProductOffering" TagPrefix="uc1" %>

   <%-- <%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>--%>
<%@ Register Src="../../../webform.UserController/ucLookUpCustomer.ascx" TagName="ucLookUpCustomer"
    TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../webform.UserController/ucGuarantor.ascx" TagName="ucGuarantor"
    TagPrefix="uc4" %>
<%@ Register Src="../../../webform.UserController/ucReferensi.ascx" TagName="ucReferensi"
    TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucLookupKabupaten.ascx" TagName="ucLookupKabupaten"
    TagPrefix="uc6" %>
<%@ Register Src="../../../webform.UserController/ucHasilSurveyTab.ascx" TagName="ucHasilSurveyTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc8" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<%@ Register Src="../../../webform.UserController/ucRefinancing.ascx" TagName="ucRefinancing"
    TagPrefix="uc9" %>
<%@ Register Src="../../../webform.UserController/ucHasilSurveyTabPhone.ascx" TagName="ucHasilSurveyTabPhone"
    TagPrefix="uc10" %>
<%@ Register Src="../../../webform.UserController/ucCoverageTypeApk.ascx" TagName="ucCoverageTypeApk"
    TagPrefix="uc11" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc12" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../../Webform.UserController/ucAddress.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucProductOffering" Src="../../../../Webform.UserController/ucProductOffering.ascx" %>--%>

<%@ Register TagPrefix="uc1" TagName="ucProdOffering" Src="../../../Webform.UserController/UcLookUpPdctOffering.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucKabupaten" Src="../../../Webform.UserController/ucKabupaten.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../Include/Buttons.css" />
    
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        function calculateAdminFeeGross() {
            var admin = $('#ucAdminFee_txtNumber').val();
            var fiducia = $('#ucFiduciaFee_txtNumber').val();
            var other = $('#ucOtherFee_txtNumber').val();
            var biayaPolis = $('#ucBiayaPolis_txtNumber').val()

            if (!admin) { admin = "0" };
            if (!fiducia) { admin = "0" };
            if (!other) { admin = "0" };
            if (!biayaPolis) { admin = "0" };

            var total = parseInt(admin.replace(/\s*,\s*/g, '')) +
                        parseInt(fiducia.replace(/\s*,\s*/g, '')) +
                        parseInt(other.replace(/\s*,\s*/g, '')) +
                        parseInt(biayaPolis.replace(/\s*,\s*/g, ''));
            $('#lblAdminFeeGross').html(number_format(total));

        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function SelectFromArray(itemArray) {
            var objAdd = 'document.forms[0].UCMailingAddress_';
            var pAddress = eval(objAdd + 'txtAddress');
            var pRT = eval(objAdd + 'txtRT');
            var pRW = eval(objAdd + 'txtRW');
            var pKelurahan = eval(objAdd + 'oLookUpKodePos_txtKelurahan'); 
            var pKecamatan = eval(objAdd + 'oLookUpKodePos_txtKecamatan');
            var pCity = eval(objAdd + 'oLookUpKodePos_txtCity');
            var pZipCode = eval(objAdd + 'oLookUpKodePos_txtZipCode'); 
            var pAreaPhone1 = eval(objAdd + 'txtAreaPhone1');
            var pPhone1 = eval(objAdd + 'txtPhone1');
            var pAreaPhone2 = eval(objAdd + 'txtAreaPhone2');
            var pPhone2 = eval(objAdd + 'txtPhone2');
            var pAreaFax = eval(objAdd + 'txtAreaFax');
            var pFax = eval(objAdd + 'txtFax');

            pAddress.value = itemArray[0][0];
            pRT.value = itemArray[0][1];
            pRW.value = itemArray[0][2];
            pKelurahan.value = itemArray[0][3];
            pKecamatan.value = itemArray[0][4];
            pCity.value = itemArray[0][5];
            pZipCode.value = itemArray[0][6];
            pAreaPhone1.value = itemArray[0][7];
            pPhone1.value = itemArray[0][8];
            pAreaPhone2.value = itemArray[0][9];
            pPhone2.value = itemArray[0][10];
            pAreaFax.value = itemArray[0][11];
            pFax.value = itemArray[0][12];
            return false;
        }

        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date);
            if (chk == true) {
                text.disabled = true;
                text.value = '';
            }
            else {
                text.disabled = false;
            }

            return true;

        }

        function InterestType() {
            var myID = 'cboInstScheme';
            var obj = eval('document.forms[0].' + myID);
            if (document.forms[0].cboInterestType.value == 'FX') {
                obj.disabled = false;
            }
            else {
                obj.disabled = true;
            }
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }

        function InstScheme() {
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;

            if (document.forms[0].cboInstScheme.value == 'ST') {
                document.forms[0].rdoSTTYpe(0).disabled = false;
                document.forms[0].rdoSTTYpe(1).disabled = false;
                document.forms[0].rdoSTTYpe(2).disabled = false;
            }
        }

        function rdoDisabled() {
            document.forms[0].rdoSTTYpe(0).checked = true;
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }
        function rboRefinancing_onchange() {
            var value = $('#rboRefinancing').find(':checked')[0].value;

            if (value === 'False') 
            {
                $('#divRefinancing').css("display", "inherit");
                return;
            }
            else 
            {
                $('#divRefinancing').css("display", "none");
                $('#ucRefinancing1_txtAgreementNo').val('');
                $('#ucRefinancing1_txtNilaiPelunasan').val('');
            }
        }
        function rboCaraSurvey_onchange() {

            var _CaraSurvey = $('#cboCaraSurvey').val();
            if (_CaraSurvey === 'False') {
                $('#divCaraSurvey').css("display", "inherit");
                return;
            }
            else {
                $('#divCaraSurvey').css("display", "none");
                $('#ucHasilSurveyTabPhone1_txtTlpRumah').val('');
                $('#ucHasilSurveyTabPhone1_txtTlpKantor').val('');
                $('#ucHasilSurveyTabPhone1_txtHandphone').val('');
                $('#ucHasilSurveyTabPhone1_txtEmergencyContact').val('');
            }
        }

        function UploadDokument(ucASPXPath, ucTitle, divLookupContent, paramData) {
            paramData = paramData || '';
            $('#dialog').remove();
            var match = new RegExp(App, 'gi');
            var tmpStrPath = AppInfo.replace(match, '');
            tmpStrPath = tmpStrPath.substring(2, tmpStrPath.length - 1);
            var arrStrPath = tmpStrPath.split('/');
            tmpStrPath = '';
            $.each(arrStrPath, function (index, value) {
                if (arrStrPath.length - 1 != index) {
                    tmpStrPath += '../'
                }
            });



            $('#' + divLookupContent).prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><img src="' + tmpStrPath + 'Images/exit_lookup00.png" class="ui-custom-close" /><iframe src="' + ucASPXPath + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
            $('#dialog')
	        .data('fnName', paramData)
	        .dialog({
	            title: ucTitle,
	            bgiframe: false,
	            width: 900,
	            height: 500,
	            resizable: false,
	            modal: true,
	            closeOnEscape: true,
	            draggable: true,
	            create: function (event, ui) {
	                $(".ui-widget-header").hide();
	            },
	            open: function () {
	                $('.ui-custom-close').bind('click', function () {
	                    $('#dialog').dialog('close');
	                })
	            }
	        });
            return false;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinZipCode(pZipCode, pKelurahan, pKecamatan, pCity, pStyle) {
            window.open(ServerName + App + '/General/LookUpZipCode.aspx?Zipcode=' + pZipCode + '&Kelurahan=' + pKelurahan + '&Kecamatan=' + pKecamatan + '&City=' + pCity + '&Style=' + pStyle, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=1, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinProductOfferingLookup(pProductOfferingID, pProductOfferingDescription, pProductID, pAssetTypeID, pStyle, pBranchID) {
            window.open(ServerName + App + '/General/LookUpProductOffering.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID + '&AssetTypeID=' + pAssetTypeID + '&branchid=' + pBranchID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinKabupaten(pProductOfferingID, pProductOfferingDescription, pProductID, pStyle) {
            window.open(ServerName + App + '/General/LookupKabupaten.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc7:ucHasilSurveyTab id="ucHasilSurveyTab1" runat="server" />
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        KEKAYAAN YANG DIMILIKI</h3>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">Status Kepemilikan Rumah</label>  
                        <asp:DropDownList ID="cboStatusKepemilikan" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboStatusKepemilikan" ErrorMessage="Harap pilih Status Kepemilikan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>
                            Atas Nama</label>
                        <asp:TextBox ID="txtAtasNama" width="255px" runat="server" MaxLength="35"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>PBB/AJB/SHM atas nama</label>  
                        <asp:TextBox ID="txtPBBAJBSHMAtasNama" width="255px" runat="server" MaxLength="35"></asp:TextBox>                                      
                    </div>
                    <div class="form_right">
                        <label>
                            Tahun tagihan</label>
                            <asp:TextBox ID="txtTahunTagihan" MaxLength="4" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">PBB/AJB/SHM</label>  
                        <asp:RadioButtonList ID="rboPBBAJBSHM" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan</label>
                        <asp:TextBox ID="txtAlasanPBBAJBSHM" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Rek listrik atas nama</label>  
                        <asp:TextBox ID="txtRekListrikAtasNama" width="255px" runat="server" MaxLength="35"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Bulan tagihan</label>  
                        <asp:TextBox ID="txtBulanTagihan" MaxLength="2" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Rek listrik</label>  
                        <asp:RadioButtonList ID="rboRekListrik" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanrekListrik" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_split" runat="server" id="lblLuasTanahBangunan">Luas Tanah / Bangunan</label>  
                        <asp:TextBox ID="txtLuasTanah" MaxLength="10" runat="server" CssClass="smaller_text" onkeypress="return numbersonly2(event)"></asp:TextBox>                        
                        <asp:Label ID="lblStrip1" runat="server">m2</asp:Label>
                        <asp:TextBox ID="txtLuasBangunan" MaxLength="10" runat="server" CssClass="smaller_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <label>m2</label>

                    </div>
                    <div class="form_right">
                        <label>Taksiran nilai jual</label>  
                        <asp:TextBox ID="txtTaksiranNilaiJual" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">Lama tinggal</label>  
                        <asp:TextBox ID="txtLamaTinggal" runat="server" CssClass="small_text" MaxLength="4" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic" ControlToValidate="txtLamaTinggal" 
                            ErrorMessage="Harap isi Lama Tinggal"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>Asset lain</label>  
                        <asp:TextBox ID="txtAssetLain" runat="server" CssClass="small_text" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Foto Rumah</label>  
                        <asp:RadioButtonList ID="rboFotoRumah" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanFotoRumah" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">Kondisi bangunan</label>  
                        <%--<asp:TextBox ID="txtKondisiBangunan" runat="server" MaxLength="50"></asp:TextBox>--%>
                        <asp:DropDownList ID="cboKondisiBangunan" runat="server">
                            <asp:ListItem Value="Permanent dan Terawat" Selected="True">Permanent dan Terawat</asp:ListItem>
                            <asp:ListItem Value="Permanent dan Tidak Terawat">Permanent dan Tidak Terawat</asp:ListItem>
                            <asp:ListItem Value="Semi Permanent dan Terawat">Semi Permanent dan Terawat</asp:ListItem>
                            <asp:ListItem Value="Semi Permanent dan Tidak Terawat">Semi Permanent dan Tidak Terawat</asp:ListItem>
                            <asp:ListItem Value="Tidak Permanent">Tidak Permanent</asp:ListItem>
                        </asp:DropDownList>  
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic" ControlToValidate="cboKondisiBangunan" 
                            ErrorMessage="Harap pilih Kondisi Bangunan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Daya Listrik</label>  
                        <%--<asp:TextBox ID="txtKondisiBangunan" runat="server" MaxLength="50"></asp:TextBox>--%>
                        <asp:DropDownList ID="cboDayaListrik" runat="server">
                            <asp:ListItem Value=">2.200 Watt" Selected="True">>2.200 Watt</asp:ListItem>
                            <asp:ListItem Value="2.200 Watt">2.200 Watt</asp:ListItem>
                            <asp:ListItem Value="1.300 Watt">1.300 Watt</asp:ListItem>
                            <asp:ListItem Value="900 Watt">900 Watt</asp:ListItem>
                            <asp:ListItem Value="450 Watt">450 Watt</asp:ListItem>
                        </asp:DropDownList>  
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic" ControlToValidate="cboDayaListrik" 
                            ErrorMessage="Harap pilih Daya Listrik"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div> 
            </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Uang Muka Murni</label>  
                         <asp:TextBox ID="txtDP" runat="server" MaxLength="25" onkeypress="return numbersonly2(event)" disabled="false"></asp:TextBox>
                     </div>
                </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>BUKTI ADMINISTRATIF KEPENDUDUKAN</h4>
            </div>
        </div>                
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>No KTP Pemohon</label>  
                        <asp:TextBox ID="txtNoKTPPemohon" runat="server" MaxLength="25" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>  
                        <asp:TextBox ID="txtTglKTPPemohon" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:CalendarExtender ID="txtTglKTPPemohon_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtTglKTPPemohon" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="txtTglKTPPemohon"></asp:RequiredFieldValidator>                 
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">KTP Pemohon</label>  
                        <asp:RadioButtonList ID="rboKTPPemohon" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanKTPPemohon" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>No KTP Pasangan</label>  
                        <asp:TextBox ID="txtNoKTPPasangan" runat="server" MaxLength="25" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>  
                        <asp:TextBox ID="txtTglKTPPasangan" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="txtTglKTPPasangan_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtTglKTPPasangan" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="txtTglKTPPasangan"></asp:RequiredFieldValidator>                 
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">KTP Pasangan</label>  
                        <asp:RadioButtonList ID="rboKTPPasangan" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanKTPPasangan" width="255px" runat="server"  MaxLength="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>No Kartu Keluarga</label>  
                        <asp:TextBox ID="txtNoKK" runat="server" MaxLength="20" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label>Tanggal</label>  
                        <asp:TextBox ID="txtTglNoKK" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="txtTglNoKK_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtTglNoKK" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="txtTglNoKK"></asp:RequiredFieldValidator>                 
                    </div>
                </div>
            </div>     
<%--            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">No KTP Pemohon</label>  
                        <asp:RadioButtonList ID="rboKTPPemohon" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="True">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="False">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanKTPPemohon" width="255px" runat="server"></asp:TextBox>                   
                    </div>
                </div>
            </div>    --%>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Buku Nikah</label>  
                        <asp:RadioButtonList ID="rboBukuNikah" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanBukuNikah" width="255px" runat="server" MaxLength ="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>     
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Jumlah tanggungan</label>
                        <asp:TextBox ID="txtJumlahTanggungan" MaxLength="4" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <label>orang</label>
                    </div>
                </div>
            </div>                  
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>  
                        <asp:TextBox runat="server" ID="txtKesimpulan" TextMode="MultiLine" Width="1000px" ></asp:TextBox>
                    </div>
                </div>
            </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>                
        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />--%>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <%--<asp:AsyncPostBackTrigger ControlID="cboKegiatanUsaha" EventName="SelectedIndexChanged" />--%>
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
