#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CreditScoring
    Inherits Maxiloan.Webform.WebBased    

#Region "Constanta"
    Private m_controller As New CreditScoringController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private LStrStatusNote As String
    Private LStrStatus As String
    Private Counter As Integer = 0
    Protected WithEvents GridNavigator As ucGridNav
    Private Const SCHEME_ID As String = "SCO"
#End Region

#Region "Property"
    Private Property myDataTable() As DataTable
        Get
            Return CType(viewstate("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("myDataTable") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CSResult_Temp() As String
        Get
            Return CType(viewstate("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CSResult_Temp") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property SupplierName() As String
        Get
            Return CType(viewstate("SupplierName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierName") = Value
        End Set
    End Property

    Private Property EmployeeName() As String
        Get
            Return CType(viewstate("EmployeeName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EmployeeName") = Value
        End Set
    End Property

    Private Property NewApplicationDate() As Date
        Get
            Return CType(viewstate("NewApplicationDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("NewApplicationDate") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Private Property CustomerType() As String
        Get
            Return CType(viewstate("CustomerType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerType") = Value
        End Set
    End Property


    Private Property IsActive() As Boolean
        Get
            Return CType(viewstate("IsActive"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsActive") = Value
        End Set
    End Property

    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Private Property CreditScoreSchemeID() As String
        Get
            Return CType(viewstate("CreditScoreSchemeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreSchemeID") = Value
        End Set
    End Property

    Private Property CreditScoreComponentID() As String
        Get
            Return CType(viewstate("CreditScoreComponentID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreComponentID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(viewstate("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(viewstate("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CreditScoreResult") = Value
        End Set
    End Property

    Private Property CutOff() As Decimal
        Get
            Return CType(ViewState("CutOff"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CutOff") = Value
        End Set
    End Property

    Private Property CREDITSCORINGCALCULATE As Parameter.CreditScoring_calculate
        Get
            Return CType(ViewState("CREDITSCORINGCALCULATE"), Parameter.CreditScoring_calculate)
        End Get
        Set(ByVal Value As Parameter.CreditScoring_calculate)
            ViewState("CREDITSCORINGCALCULATE") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        If Not Me.IsPostBack Then
            Me.FormID = "CreditScoring"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                InitialDefaultPanel()

                Me.Sort = "a.ApplicationID ASC"
                Me.CmdWhere = ""
                Me.BranchID = Replace(Me.sesBranchId, "'", "")
                txtApplicationDate.Attributes.Add("readonly", "true")

                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    BindGridEntity(Me.CmdWhere)
                Else
                    Response.Redirect("../../../error_notauthorized.aspx")
                End If
                pnlProceed.Visible = False
            End If
        End If
    End Sub
#End Region
    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        BindGridEntity(Me.CmdWhere, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlScoring.Visible = False
        pnlView.Visible = False
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        lblMessage.Visible = False
        Dim dtEntity As DataTable
        Dim oCreditScoring As New Parameter.CreditScoring
        InitialDefaultPanel()

        'oCreditScoring.PageSize = CType(pageSize, Int16)
        'oCreditScoring.WhereCond = cmdWhere
        'oCreditScoring.SortBy = Me.Sort
        'oCreditScoring.CurrentPage = currentPage
        'oCreditScoring.BranchId = Me.BranchID
        'oCreditScoring.strConnection = GetConnectionString()
        'oCreditScoring = m_controller.CreditScoringPaging(oCreditScoring)

        With oCreditScoring
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .BranchId = Me.BranchID
            .strConnection = GetConnectionString()
        End With

        oCreditScoring = m_controller.CreditScoringPaging(oCreditScoring)

        If Not oCreditScoring Is Nothing Then
            recordCount = oCreditScoring.TotalRecords
            dtgPaging.DataSource = oCreditScoring.ListData.DefaultView
            dtgPaging.DataBind()
            dtgPaging.CurrentPageIndex = 0
        Else
            recordCount = 0
        End If
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub

    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strSupplierID & "','" & strStyle & "')"
    End Function

    Function LinkToEmployee(ByVal strBranchID As String, ByVal strAOID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinEmployee('" & strBranchID & "','" & strAOID & "','" & strStyle & "')"
    End Function
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkApp As HyperLink
        Dim hynSupplierName As HyperLink
        Dim hynEmployeeName As HyperLink
        Dim hynCustomerName As New HyperLink

        Dim lblCustomerID As Label
        Dim lblSupplierID As Label
        Dim lblAOID As Label
        Dim lblNumber As Label
        If e.Item.ItemIndex >= 0 Then
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)

            lnkApp = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            lnkApp.NavigateUrl = LinkTo(dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString, "AccAcq")

            hynSupplierName = CType(e.Item.FindControl("hynSupplierName"), HyperLink)
            hynSupplierName.NavigateUrl = LinkToSupplier(lblSupplierID.Text.Trim, "AccAcq")

            hynEmployeeName = CType(e.Item.FindControl("hynEmployeeName"), HyperLink)
            hynEmployeeName.NavigateUrl = LinkToEmployee(Me.BranchID, lblAOID.Text.Trim, "AccAcq")

            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            hynCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")

        End If
    End Sub
#End Region

#Region "dtgView_ItemDataBound"
    Private Sub dtgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgView.ItemDataBound
        Dim lnkApp As HyperLink
        Dim lblNumber As Label
        If e.Item.ItemIndex >= 0 Then
            lblNumber = CType(e.Item.FindControl("lblNumber"), Label)
            Counter += 1
            lblNumber.Text = FormatNumber(Counter, 0)
        End If
    End Sub
#End Region

#Region " Navigation "
    'Private Sub PagingFooter()
    '    lblPage.Text = currentPage.ToString()
    '    totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '    If totalPages = 0 Then           
    '        lblTotPage.Text = "1"
    '        rgvGo.MaximumValue = "1"
    '    Else
    '        lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
    '        rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
    '    End If
    '    lblTotRec.Text = recordCount.ToString

    '    If currentPage = 1 Then
    '        imbPrevPage.Enabled = False
    '        imbFirstPage.Enabled = False
    '        If totalPages > 1 Then
    '            imbNextPage.Enabled = True
    '            imbLastPage.Enabled = True
    '        Else
    '            imbPrevPage.Enabled = False
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '            imbFirstPage.Enabled = False
    '        End If
    '    Else
    '        imbPrevPage.Enabled = True
    '        imbFirstPage.Enabled = True
    '        If currentPage = totalPages Then
    '            imbNextPage.Enabled = False
    '            imbLastPage.Enabled = False
    '        Else
    '            imbLastPage.Enabled = True
    '            imbNextPage.Enabled = True
    '        End If
    '    End If
    'End Sub
    'Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    Select Case e.CommandName
    '        Case "First" : currentPage = 1
    '        Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
    '        Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
    '        Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
    '    End Select
    '    BindGridEntity(Me.CmdWhere)
    'End Sub
    'Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
    '    If IsNumeric(txtGoPage.Text) Then
    '        If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
    '            currentPage = CType(txtGoPage.Text, Int32)
    '            BindGridEntity(Me.CmdWhere)
    '        End If
    '    End If
    'End Sub
#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim oProduct As New Parameter.Product
        lblMessage.Text = ""
        lblMessage.Visible = False
        If e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "ADD", Me.AppId) Then
                pnlList.Visible = False
                pnlScoring.Visible = True

                Dim lnkApplicationID As New HyperLink
                lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)

                Dim lblCustomerID As New Label
                lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)

                Dim hynCustomerName As New HyperLink
                hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)

                hplApplicationID_Scoring.Text = lnkApplicationID.Text
                hplCustomerName_Scoring.Text = hynCustomerName.Text

                Me.ApplicationID = lnkApplicationID.Text
                Me.CustomerID = lblCustomerID.Text
                btnSave.Visible = False

                hplApplicationID_Scoring.NavigateUrl = LinkTo(Me.ApplicationID, "AccAcq")
                hplCustomerName_Scoring.NavigateUrl = LinkToCustomer(Me.CustomerID, "AccAcq")

                lblResult.Text = ""
                pnlView.Visible = False
            End If
        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        Me.CmdWhere = ""
        txtSearch.Text = ""
        txtApplicationDate.Text = ""
        BindGridEntity(Me.CmdWhere)
        'txtGoPage.Text = "1"
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.CmdWhere = ""

        If txtSearch.Text.Trim <> "" Then
            'If Right(txtSearch.Text.Trim, 1) = "%" Then
            '    Me.CmdWhere = cboSearch.SelectedItem.Value & " like '" & txtSearch.Text.Trim & "'"
            'Else
            '    Me.CmdWhere = cboSearch.SelectedItem.Value & " = '" & txtSearch.Text.Trim & "'"
            'End If
            Me.CmdWhere = cboSearch.SelectedItem.Value & " like '" & txtSearch.Text.Trim & "'"
        End If
        If txtApplicationDate.Text <> "" Then
            If Me.CmdWhere <> "" Then
                Me.CmdWhere &= " and NewApplicationDate = '" & ConvertDate2(txtApplicationDate.Text).ToString("yyyyMMdd") & "'"
            Else
                Me.CmdWhere &= " NewApplicationDate = '" & ConvertDate2(txtApplicationDate.Text).ToString("yyyyMMdd") & "'"
            End If
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
     
#Region "imgSave"
    Private Sub imgSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim customClass As New Parameter.CreditScoring
        Dim ErrMessage As String = ""
        Dim ErrMessage1 As String = ""
        With customClass
            .ApplicationStep = "SCO"
            .CreditScoringDate = Me.BusinessDate
            .CreditScore = Me.CreditScore
            .CreditScoringResult = Me.CreditScoreResult
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .strConnection = GetConnectionString()
            .ListData = Me.myDataTable
            .CSResult_Temp = Me.CSResult_Temp
        End With
        Try
            m_controller.CreditScoringSaveAdd(customClass)
            Me.CmdWhere = ""
            BindGridEntity(Me.CmdWhere)
            pnlList.Visible = True
            pnlScoring.Visible = False
            ShowMessage(lblMessage, "Data saved!", False)
        Catch exp As Exception
            pnlList.Visible = False
            pnlScoring.Visible = True
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
#End Region

#Region "imgCreditScoring_click"
    Private Sub imgCreditScoring_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreditScoring.Click
        'lblResult.Text = CalculateCreditScoring(Me.BranchID, Me.ApplicationID)

        Dim scoringData As New Parameter.CreditScoring_calculate

        With scoringData
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationId = Me.ApplicationID
        End With
        scoringData = CreditScoringMdl.FicoCalculateCreditScoring(scoringData)
        CREDITSCORINGCALCULATE = scoringData
        'set result

        'scoringData.DT = _scoreResults.ToDataTable
        'scoringData.CreditScore = _scoreResults.TotalScore
        'scoringData.CreditScoreResult = _scoreResults.TotalScore

        myDataTable = scoringData.ScoreResults.ToDataTable
        CreditScoreComponentID = scoringData.CreditScoreComponentID
        lblGrade.Text = scoringData.ScoreResults.CuttOff
        lblResult.Text = scoringData.ScoreResults.TotalScore
        lblAbsoluteScore.Text = scoringData.ScoreResults.ResultCalc
        lblDecision.Text = scoringData.ScoreResults.FinalDecision

        Me.CreditScoreSchemeID = scoringData.CreditScoreSchemeID
        Me.CutOff = scoringData.ScoreResults.CuttOff


        If lblResult.Text <> "" Then

            'btnSave.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO APPROVE") Or (scoringData.ScoreResults.FinalDecision.Trim = "MANUAL APPROVE")
            'btnReject.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO REJECT") Or (scoringData.ScoreResults.FinalDecision.Trim = "MANUAL APPROVE")
            'btnProceed.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO REJECT") Or (scoringData.ScoreResults.FinalDecision.Trim = "MANUAL APPROVE")

            'If scoringData.ScoreResults.ResultCalc >= "2,5" And scoringData.ScoreResults.ResultCalc < "3,5" Then
            '    lblTingkatResiko.Text = "Allowable Risk"
            '    If scoringData.ScoreResults.ResultCalc >= "3,5" And scoringData.ScoreResults.ResultCalc < "4" Then
            '        lblTingkatResiko.Text = "Average Risk"
            '        If scoringData.ScoreResults.ResultCalc >= "4" And scoringData.ScoreResults.ResultCalc < "4,6" Then
            '            lblTingkatResiko.Text = "Acceptable Risk"
            '            If scoringData.ScoreResults.ResultCalc >= "4,6" Then
            '                lblTingkatResiko.Text = "Minimum Risk"
            '            End If
            '        End If
            '    End If
            'Else lblTingkatResiko.Text = "Reject"
            'End If

            If scoringData.ScoreResults.ResultCalc >= "3.0" Then
                lblTingkatResiko.Text = "Allowable Risk"
                If scoringData.ScoreResults.ResultCalc >= "3.5" Then lblTingkatResiko.Text = "Average Risk"
                If scoringData.ScoreResults.ResultCalc >= "4" Then lblTingkatResiko.Text = "Acceptable Risk"
                If scoringData.ScoreResults.ResultCalc >= "4.6" Then lblTingkatResiko.Text = "Minimum Risk"
            Else
                lblTingkatResiko.Text = "Reject"
            End If

            'btnSave.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO APPROVE")
            'btnReject.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO REJECT") Or (scoringData.ScoreResults.FinalDecision.Trim = "MANUAL APPROVE")
            'btnProceed.Visible = (scoringData.ScoreResults.FinalDecision.Trim = "AUTO REJECT") Or (scoringData.ScoreResults.FinalDecision.Trim = "MANUAL APPROVE")

            btnSave.Visible = (scoringData.ScoreResults.ResultCalc >= "4")
            btnReject.Visible = (scoringData.ScoreResults.ResultCalc < "4")
            btnProceed.Visible = (scoringData.ScoreResults.ResultCalc < "4")

            pnlList.Visible = False
                pnlScoring.Visible = True
                pnlView.Visible = True

                dtgView.DataSource = Me.myDataTable.Select("ComponentValue ='CRDITSCORE'")
                dtgView.CurrentPageIndex = 0

                dtgView.DataBind()

            End If

    End Sub
#End Region
    Sub doSave(csRes As String) 
        Try
            CREDITSCORINGCALCULATE.ScoreResults.CSResult_Temp = csRes
            CREDITSCORINGCALCULATE.ScoreResults.ApplicationStep = "SCO"
            Dim result = m_controller.DoCreaditScoringSave(GetConnectionString(), Loginid, BranchID, ApplicationID, CREDITSCORINGCALCULATE.ScoreResults, BusinessDate)
            Me.CmdWhere = ""
            BindGridEntity(Me.CmdWhere)
            pnlList.Visible = True
            pnlScoring.Visible = False

            If (result.Trim.ToUpper.Contains("OK")) Then
                ShowMessage(lblMessage, "Data saved!", False)
            Else
                ShowMessage(lblMessage, result, True)
            End If
            Me.CmdWhere = ""
            BindGridEntity(Me.CmdWhere)
            pnlList.Visible = True
            pnlScoring.Visible = False
        Catch exp As Exception
            pnlList.Visible = False
            pnlScoring.Visible = True
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        doSave("A")
    End Sub

    Private Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        doSave("R")
    End Sub
    Private Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        pnlList.Visible = False
        dtgPaging.Visible = False
        pnlScoring.Visible = True
        pnlView.Visible = True
        pnlProceed.Visible = True

        Dim oData As New DataTable
        oData = Get_UserApproval("SCO", Me.sesBranchId.Replace("'", ""), CDec(1))
        cboApprovedBy.DataSource = oData.DefaultView
        cboApprovedBy.DataTextField = "Name"
        cboApprovedBy.DataValueField = "ID"
        cboApprovedBy.DataBind()
        cboApprovedBy.Items.Insert(0, "Select One")
        cboApprovedBy.Items(0).Value = "0"
    End Sub
    Private Sub Imagebutton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.CmdWhere = ""
        BindGridEntity(Me.CmdWhere)
        pnlList.Visible = True
        pnlScoring.Visible = False
        Response.Redirect("creditscoring.aspx")
    End Sub
    Private Sub SaveProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SaveProceed.Click
        If txtNoteApprove.Text = "" Then
            ShowMessage(lblMessage, "Harap isi Note", True)
            Exit Sub
        End If
        Try
            Dim oCustomClass As New Parameter.CreditScoring With {
               .strConnection = GetConnectionString().Trim,
               .ApplicationID = Me.ApplicationID
           }
            Dim oEntitiesApproval As New Parameter.Approval
            With oEntitiesApproval
                .BranchId = Me.sesBranchId.Replace("'", "")
                .SchemeID = SCHEME_ID
                .RequestDate = Me.BusinessDate
                .ApprovalNote = txtNoteApprove.Text.Trim
                .ApprovalValue = 1
                .UserRequest = Me.Loginid
                .UserApproval = cboApprovedBy.SelectedValue
                .AprovalType = Approval.ETransactionType.AmortisasiBiaya_Approval
                '.AprovalType = SCHEME_ID
                .Argumentasi = txtNoteApprove.Text.Trim
                .TransactionNo = Me.ApplicationID
            End With

            Dim oController As New CreditScoringController
            oCustomClass.Approval = oEntitiesApproval
            Dim resultapp = oController.GetApprovalCreditScoring(oCustomClass, "R")

            Dim oscore As New Parameter.ScoreResults
            With oscore
                .ScoreSchemeID = Me.CreditScoreSchemeID
            End With

            Dim result = oController.DoCreditScoringProceed(GetConnectionString, sesBranchId.Replace("'", "").Trim, Me.ApplicationID, Loginid, BusinessDate)
            If (result.Trim.Contains("SCORE")) Then
                imbReset_Click(Nothing, Nothing)
                ShowMessage(lblMessage, String.Format("Score Prospect id {0} telah berhasil dengan {1}", Me.ApplicationID, result.Trim), False)

            ElseIf (result.Trim.Contains("ASSESSMENT")) Then
                ApplicationID = ID
                'lblProspekId.Text = ID
                'lblScoreResult.Text = result.Split(",")(1)

                Dim script = "window.onload = function() { showDialog(); };"
                ClientScript.RegisterStartupScript(Me.GetType(), "showDialog", script, True)

            Else
                ShowMessage(lblMessage, result, True)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        imbReset_Click(Nothing, Nothing)
        'pnlProceed.Visible = False
        Response.Redirect("creditscoring.aspx")
    End Sub
End Class