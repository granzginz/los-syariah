﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalBM_002.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ApprovalBM_002" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" src="../../../Maxiloan.js" type="text/javascript"></script>
  
    <script language="javascript" type="text/javascript">
        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date + '_txtDate');
            var calendar = eval('document.forms[0].all.' + date + '_txtDate_imgCalender');
            if (chk == true) {
                text.disabled = true;
                calendar.disabled = true;
                text.value = '';
                return true;
            }
            else {
                text.disabled = false;
                calendar.disabled = false;
                return true;
            }
        }


        function OpenWinApplication(pApplicationID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'Application', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
     
 

        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }
        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            eval('document.forms[0].tempChild').value = "";
            var tmp;
            var child = eval('document.forms[0].' + pChild);
            var tampung = eval('document.forms[0].' + pTampung);
            var tampungname = eval('document.forms[0].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('All', '');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms[0].' + tampungGrandChild).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
        function setCboBatchDate(l, j) {
            eval('document.forms[0].' + tampungGrandChild2).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <input id="tempParent" type="hidden" name="Hidden1" runat="server" />
    <input id="tempParentName" type="hidden" name="Hidden1" runat="server" />
    <input id="tempChild" type="hidden" name="tempChild" runat="server" />
    <input id="tempChildName" type="hidden" name="tempChildName" runat="server" />
    <input id="tempChild2" type="hidden" name="tempChild2" runat="server" />
    <input id="tempGrandChild" type="hidden" name="tempGrandChild" runat="server" />
    <input id="tempGrandChildName" type="hidden" name="tempGrandChildName" runat="server" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>PERMINTAAN APPROVAL PEMBIAYAAN</h3>
        </div>
    </div>
    <div class="form_box">
       
            <div class="form_left">
                <label>Nama Customer</label>
                <asp:HyperLink ID="lblCustName" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>No. Aplikasi</label>
                <asp:HyperLink ID="lblApplicationID" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
        </div>
            <div class="form_box">
         <div class="form_left">  </div>

         <div class="form_right">
                <label>Amount To Finance</label>
                <asp:Label ID="lblNPM" runat="server" />
            </div> 
        </div>

         <div class="form_box">
        <div class="form_single">
            <label>
                Rekomendasi BM</label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:TextBox ID="txtRecommendation" runat="server" style="width:100%" TextMode="MultiLine" class="multiline_textbox"></asp:TextBox>
        </div>
    </div>
  <%--  <div class="form_box">
        <div class="form_single">
            <label>
                Hal-hal yang Menyimpang &amp; argumentasi</label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:TextBox ID="txtArgumentasi" runat="server" style="width:100%" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
        </div>
    </div>--%>

    <div class="form_button"> 
        <asp:Button ID="btnSave" runat="server" Text="Proceed" CssClass="small button blue" />
        
        <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Back" CssClass="small button gray" />
    </div>
    </form>
</body>
</html>
