﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="POExtendDetail.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.POExtendDetail" %>

<%@ Register TagPrefix="oDate1" TagName="ValidDate" Src="../../../UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>POExtendDetail</title>
    <script language="javascript" type="text/javascript">	
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);								
        var x = screen.width;
        var y = screen.height - 100;		
			
		function OpenWinSupplier(pSupplierID,pStyle){
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);			
			window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/ViewSupplier.aspx?style=' + pStyle + '&SupplierID=' + pSupplierID, 'Supplier', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
			}
			
		function OpenWinCustomer(pID,pStyle){
				var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
				var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);			
				window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID  + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
			}
		
		function OpenWinAgreementNo(pApplicationID,pStyle){
				var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
				var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);			
				window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?ApplicationID=' + pApplicationID  + '&style=' + pStyle, 'AgreementNo', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
			}		
		
		<!--
			var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
			var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
			var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
			
			function PoWinOpen(type,agreementno,supplierid,checkbutton,MailTypeID,MailTransNo,custid,applicationID)
			{
				if(type=='Supplier'){
					window.open(ServerName + App +  '/SmartSearch/SupplierDetail.aspx?agreementno=' + agreementno + '&supplierid=' + supplierid + '&checkbutton=' + checkbutton, null, 'left=50, top=10, width=900, height=700, menubar=0,scrollbars=1');
				}else if(type=='PO'){
					window.open(ServerName + App +  '/Utility/el_ViewMail.aspx?MailTypeID=' + MailTypeID + '&MailTransNo=' + MailTransNo, null, 'left=50, top=10, width=900, height=700, menubar=0,scrollbars=1');				
				}else if(type=='CustomerName'){
					window.open(ServerName + App +  '/SmartSearch/ViewSearch.aspx?custid='+ custid , null, 'left=50, top=10, width=900, height=700, menubar=0,scrollbars=1');				
				}else if(type=='Agreement'){
					window.open(ServerName + App +  '/SmartSearch/perAgreement.aspx?agreementno=' + agreementno + '&applicationid=' + applicationID  , null, 'left=50, top=10, width=900, height=700, menubar=0,scrollbars=1');				
				}
			} 
			//-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table id="Table1" height="20" cellspacing="0" cellpadding="0" width="95%" align="center"
        border="0">
        <tr class="trtopikuning">
            <td class="tdtopi" align="center">
                DETAIL PERPANJANG PURCHASE ORDER
            </td>
        </tr>
    </table>
    <table id="Table2" cellspacing="1" cellpadding="3" width="95%" align="center" border="0">
        <tr>
            <td width="20%" class="tdgenap">
                NO PO
            </td>
            <td class="tdganjil">
                <asp:HyperLink ID="lblPONo" runat="server"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                Nama Customer
            </td>
            <td class="tdganjil">
                <asp:HyperLink ID="lblName" runat="server"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                No Kontrak
            </td>
            <td class="tdganjil">
                <asp:HyperLink ID="lblAgreement" runat="server"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                Nama Supplier
            </td>
            <td class="tdganjil">
                <asp:HyperLink ID="lblSupplier" runat="server"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                Tanggal PO
            </td>
            <td class="tdganjil">
                <asp:Label ID="lblPODate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                PO Jatuh Tempo/expired
            </td>
            <td class="tdganjil">
                <odate1:validdate id="oDate1" runat="server">
                </odate1:validdate>
                <asp:Label ID="lblErr" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                Sisa hari
            </td>
            <td class="tdganjil">
                <asp:Label ID="lblRemain" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                Perpanjang
            </td>
            <td class="tdganjil">
                <asp:Label ID="LblExtend" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="tdgenap">
                Nilai PO
            </td>
            <td class="tdganjil">
                <asp:Label ID="lblPOAmount" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table id="Table4" height="20" cellspacing="0" cellpadding="2" width="95%" align="center"
        border="0">
        <tr>
            <td align="left">
                <asp:ImageButton ID="btnSave" runat="server" ImageUrl="../../../Images/ButtonSave.gif"
                    CausesValidation="False" />&nbsp;
                <asp:ImageButton ID="btnBack" runat="server" ImageUrl="../../../Images/ButtonCancel.gif"
                    CausesValidation="False" />
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
