﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RCA
    Inherits Maxiloan.Webform.WebBased    

#Region "Constanta"
    Private m_controller As New rcacontroller
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private m_TCcontroller As New ApplicationController
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Private x_controller As New DataUserControlController

#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(viewstate("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property CustName() As String
        Get
            Return CType(viewstate("CustName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            If CheckForm(Me.Loginid, "RCA", Me.AppId) Then
                lblMessage.Text = ""
                lblMessage.Visible = False
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    txtScoringDate.Attributes.Add("readonly", "true")
                    txtGoPage.Text = "1"
                    If Request("cond") <> "" Then
                        Me.CmdWhere = Request("cond")
                    Else
                        Me.CmdWhere = "ALL"
                    End If
                    Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                    Me.Sort = "ApplicationID ASC"
                    BindGrid()
                    Me.BranchID = Me.sesBranchId.Replace("'", "")
                    InitialPanel()
                Else
                    Response.Redirect("../../../error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub

    Sub InitialPanel()
        pnlList.Visible = True
    End Sub

#End Region

#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strSupplierID & "','" & strStyle & "')"
    End Function

    Function LinkToEmployee(ByVal strBranchID As String, ByVal strAOID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinEmployee('" & strBranchID & "','" & strAOID & "','" & strStyle & "')"
    End Function

    Function LinkToAgreementNo(ByVal strAgreementNo As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinAgreementNo('" & strAgreementNo & "','" & strStyle & "')"
    End Function

#End Region

#Region "BindGrid"
    Sub BindGrid()
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.RCA

        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetRCA(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region

#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim SchemeId As String
        Dim NTF As String
        If e.CommandName = "ApplicationID" Then
        ElseIf e.CommandName = "Name" Then
        ElseIf e.CommandName = "CA" Then
        ElseIf e.CommandName = "AO" Then
        ElseIf e.CommandName = "REQ" Then
            If CheckFeature(Me.Loginid, "RCA", "Add", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.ApplicationID = CType(e.Item.Cells(1).FindControl("lnkApplicationID"), HyperLink).Text.Trim
            Me.CustName = CType(e.Item.Cells(2).FindControl("lnkName"), HyperLink).Text.Trim
            Me.CustomerID = CType(e.Item.FindControl("lblCustomerID"), Label).Text.Trim
            SchemeId = CType(e.Item.FindControl("lblSchemeID"), Label).Text.Trim
            NTF = CType(e.Item.FindControl("lblNTF"), Label).Text.Trim
            Response.Redirect("RCA_002.aspx?App=" & Me.ApplicationID & "&Name=" & Me.CustName & "&CustomerID=" & Me.CustomerID & "&Scheme=" & SchemeId & "&NTF=" & NTF & "")
        ElseIf e.CommandName = "Return" Then
            pnlList.Visible = False
            pnlReturn.Visible = True
            ' pnlPO.Visible = False

            Dim lblCustomerID As New Label
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Dim hypAgreementNo As New HyperLink
            hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            Dim lblApplicationID As New Label
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)

            hdnApplicationID.Value = lblApplicationID.Text.Trim
            ucViewApplication1.ApplicationID = lblApplicationID.Text.Trim
            ucViewApplication1.CustomerID = lblCustomerID.Text.Trim
            ucViewApplication1.bindData()
            ucViewApplication1.initControls("OnNewApplication")
            ucViewCustomerDetail1.CustomerID = lblCustomerID.Text.Trim
            ucViewCustomerDetail1.bindCustomerDetail()
        End If   
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
#End Region

#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtScoringDate.Text = ""
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        BindGrid()
    End Sub

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim Search As String = ""
        If txtSearch.Text.Trim <> "" Then
            Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
            Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + tmptxtSearchBy + "%'"
            'Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
        Else
            Me.CmdWhere = "ALL"
        End If

        If txtScoringDate.Text.Trim <> "" Then
            Me.CmdWhere2 = "ScoringDate = '" & ConvertDate2(txtScoringDate.Text.Trim) & "' and BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        Else
            Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        End If
        BindGrid()
    End Sub
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkApplicationID As HyperLink
        Dim lnkName As HyperLink
        Dim lnkAO As HyperLink
        Dim lnkCA As New HyperLink

        Dim lblCustomerID As Label
        Dim lblApplicationID As Label
        Dim lblAOID As Label
        Dim lblCAID As Label

        If e.Item.ItemIndex >= 0 Then
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)
            lblCAID = CType(e.Item.FindControl("lblCAID"), Label)

            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)
            lnkCA = CType(e.Item.FindControl("lnkCA"), HyperLink)


            lnkApplicationID.NavigateUrl = LinkTo(lblApplicationID.Text.Trim, "AccAcq")
            lnkName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            lnkAO.NavigateUrl = LinkToEmployee(Replace(Me.sesBranchId, "'", ""), lblAOID.Text.Trim, "AccAcq")
            lnkCA.NavigateUrl = LinkToEmployee(Replace(Me.sesBranchId, "'", ""), lblCAID.Text.Trim, "AccAcq")

        End If
    End Sub
#End Region

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Dim ParDO As New Parameter._DO
        Dim m_DO As New DOController

        ParDO.strConnection = GetConnectionString()
        ParDO.BusinessDate = Me.BusinessDate
        ParDO.BranchId = Me.sesBranchId.Replace("'", "").Trim
        ParDO.ApplicationID = hdnApplicationID.Value
        ParDO.AlasanReturn = txtAlasanReturn.Text.Trim

        Try
            m_DO.ApplicationReturnUpdate(ParDO)

            'BindGrid(Me.CmdWhere)
            BindGrid()
            pnlList.Visible = True
            'pnlPO.Visible = False
            pnlReturn.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Me.CmdWhere = ""
        BindGrid()
        pnlList.Visible = True
        'pnlPO.Visible = False
        pnlReturn.Visible = False
    End Sub

End Class