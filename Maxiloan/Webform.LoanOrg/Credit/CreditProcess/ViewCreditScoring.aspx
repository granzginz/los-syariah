﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCreditScoring.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewCreditScoring" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewCreditScoring</title>
    <link href="../../../Include/general.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlScoring" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    VIEW - SCORING
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    No Aplikasi</label>
                <asp:Literal ID="ltlProspectAppId" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Customer</label>
                <asp:Literal ID="ltlCustomerName" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Scoring Pembiayaan</label>
                <asp:Literal ID="ltlCreditScoring" runat="server"></asp:Literal>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Hasil</label>
                <asp:Literal ID="ltlResult" runat="server"></asp:Literal>
            </div>
        </div>
        <asp:Panel ID="pnlView" runat="server" Visible="False">
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        HASIL SCORING
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgView" runat="server" EnableViewState="False" Width="100%" CssClass="grid_general"
                        CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblNumber" runat="server" Text="0"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KOMPONEN SCORING">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#container.dataitem("Description")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KOMPONEN CONTENT">
                                <ItemTemplate>
                                    <asp:Label ID="lblValue" runat="server" Text='<%#container.dataitem("Value")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NILAI">
                                <ItemTemplate>
                                    <asp:Label ID="lblScoreValue" runat="server" Text='<%#container.dataitem("ScoreValue")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BOBOT">
                                <ItemTemplate>
                                    <asp:Label ID="lblWeight" runat="server" Text='<%#container.dataitem("Weight")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%#container.dataitem("ID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SCORING">
                                <ItemTemplate>
                                    <asp:Label ID="lblScore" runat="server" Text='<%#container.dataitem("Score")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                    Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                <asp:Button ID="btnBack" runat="server" EnableViewState="False" Text="Back" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
