﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApplicationReject.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ApplicationReject" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AppDeviations</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" Visible="false" runat="server"></asp:Label>
    <input id="hdnApplicationId" type="hidden" name="hdnApplicationId" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>                
                APPLICATION REJECT
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server" Width="100%">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR APLIKASI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0" DataKeyField="ApplicationID" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAction" runat="server" CommandName="Edit" Text='REJECT' CausesValidation="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO APLIKASI" SortExpression="ApplicationID">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER" SortExpression="CustomerName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SUPPLIER" SortExpression="SupplierName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynSupplierName" runat="server" Text='<%#container.dataitem("SupplierName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CMO" SortExpression="EmployeeName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynEmployeeName" runat="server" Text='<%#container.dataitem("EmployeeName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="CustomerID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SupplierID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="AOID" Visible="false"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"
                        Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI APLIKASI 
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="b.Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="a.ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="d.EmployeeName">CMO</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server" Visible="False">
        <div class="form_box">           
            <div class="form_left">
                <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
            </div>
            <div class="form_right">
                <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
            </div>
        </div>
        <div class="form_box">
	        <div class="form_single">
                <label>Alasan Reject</label>
                <asp:DropDownList runat="server" ID="cboReject"></asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                ControlToValidate="cboReject"></asp:RequiredFieldValidator>
	        </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
