﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.IO
Imports System.Collections.Generic
#End Region


Public Class CreditAssesment
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTab
    Protected WithEvents CreditAssessmentTab1 As CreditAssessmentTab
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat
    Protected WithEvents txtPhoneDate As ucDateCE

    Private oAssetDataController As New AssetDataController
    Private oController As New CreditAssesmentController
    Private oCustomerController As New CustomerController
    Private time As String

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property

    Private Property CSResult_Temp() As String
        Get
            Return CType(ViewState("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CSResult_Temp") = Value
        End Set
    End Property
    Private Property TotalDebet() As Decimal
        Get
            Return (CType(ViewState("TotalDebet"), Decimal))
        End Get
        Set(ByVal TotalDebet As Decimal)
            ViewState("TotalDebet") = TotalDebet
        End Set
    End Property
    Private Property TotalKredit() As Decimal
        Get
            Return (CType(ViewState("TotalKredit"), Decimal))
        End Get
        Set(ByVal TotalKredit As Decimal)
            ViewState("TotalKredit") = TotalKredit
        End Set
    End Property
    Private Property myDataTable() As DataTable
        Get
            Return CType(viewstate("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("myDataTable") = Value
        End Set

    End Property
    Private Property SchemeID() As String
        Get
            Return CType(ViewState("SchemeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SchemeID") = Value
        End Set
    End Property
    Private Property NTF() As String
        Get
            Return CType(ViewState("NTF"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NTF") = Value
        End Set
    End Property
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        lblMessage.Visible = False

        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateStart = Me.BusinessDate + " " + time

        If Not Page.IsPostBack Then 
            Me.FormID = "CREDITASSESMENT"
            Me.ApplicationID = ""
            If Request("appid") <> "" Then
                Me.ApplicationID = Request("appid")
            End If 
            BranchID = Replace(Me.sesBranchId, "'", "")
            getCreditAssesment()
        End If


    End Sub

    Private Sub InitialPageLoad()
        
        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID
 
        End With
         
        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        FillCboEmp("BranchEmployee", cboCreditAnalyst, WhereBranch + " and IsCA = 1 ")

    End Sub

    Sub getCreditAssesment()
        Dim oPar As New Parameter.CreditAssesment

        With oPar
            .strConnection = GetConnectionString() 
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getCreditAssesment(oPar)
        InitialPageLoad()
        With oPar
            TxtPhoneHome.Text = .TeleponRumah.ToString
            TxtPhoneOff.Text = .TeleponKantor.ToString
            TxtPhoneEC.Text = .TeleponEC.ToString
            TxtPhoneGuarantee.Text = .TeleponPenjamin.ToString
            TxtNotes.Text = .SurveyorNotes.ToString
            TxtAreaPhoneHm.Text = .TeleponRumahArea.ToString
            TxtAreaPhoneOff.Text = .TeleponKantorArea.ToString
            TxtAreaPhoneEc.Text = .TeleponECArea.ToString
            TxtAreaPhoneGuarantee.Text = .TeleponPenjaminArea.ToString
            'txtPhoneDate.Text = .TanggalTelepon.ToString("dd/MM/yyyy")
            txtPhoneDate.Text = IIf(.TanggalTelepon.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalTelepon.ToString("dd/MM/yyyy"))
            cboCreditAnalyst.SelectedIndex = cboCreditAnalyst.Items.IndexOf(cboCreditAnalyst.Items.FindByValue(.CreditAnalyst))
            TxtHp.Text = .NoHp.ToString
        End With

        CreditAssessmentTab1.ApplicationID = Me.ApplicationID
        CreditAssessmentTab1.selectedTab("VerifikasiPhone")
        CreditAssessmentTab1.setLink(oPar.ApplicationID.Trim <> "")


    End Sub
 
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click 
        Response.Redirect("creditAssesmentList.aspx")
    End Sub

    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.CreditAssesment
        Dim errMsg As String = ""

        Try
            If cboCreditAnalyst.SelectedItem.Value.Trim = "" Then
                ShowMessage(lblMessage, "Harap pilih CreditAnalyst", True)
                Exit Sub
            End If
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .TeleponRumah = TxtPhoneHome.Text
                .TeleponKantor = TxtPhoneOff.Text
                .TeleponEC = TxtPhoneEC.Text
                .TeleponPenjamin = TxtPhoneGuarantee.Text
                .SurveyorNotes = TxtNotes.Text
                .NoHp = TxtHp.Text
                .TeleponRumahArea = TxtAreaPhoneHm.Text
                .TeleponKantorArea = TxtAreaPhoneOff.Text
                .TeleponECArea = TxtAreaPhoneEc.Text
                .TeleponPenjaminArea = TxtAreaPhoneGuarantee.Text 
                .CreditAnalyst = cboCreditAnalyst.SelectedValue
                .TanggalTelepon = ConvertDate2(txtPhoneDate.Text)
                .part = "V"
            End With


            oController.CreditAssesmentSave(oPar)
            If errMsg = String.Empty Or errMsg = "" Then
                ShowMessage(lblMessage, "Data saved!", False)
            Else
                ShowMessage(lblMessage, errMsg, True)
            End If
            getCreditAssesment()
            'Modify by Wira 20171019
            Response.Redirect("CreditAssesment_002.aspx?pnl=tabRekening&branchID=" + Me.BranchID + "&appid=" + Me.ApplicationID.Trim + "&ActivityDateStart=" & Me.ActivityDateStart & "")

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        'End If
    End Sub


    'Private Sub btnProceed_Click(sender As Object, e As System.EventArgs) Handles btnProceed.Click
    '    Dim ocustom As New Parameter.CreditAssesment

    '    With ocustom
    '        .strConnection = Me.GetConnectionString()
    '        .ApplicationID = Me.ApplicationID
    '        .BranchId = Me.sesBranchId.Replace("'", "")
    '        .BusinessDate = Me.BusinessDate
    '        oController.Proceed(ocustom)
    '    End With

    '    ShowMessage(lblMessage, "Applikasi sudah di proses", False)
    '    btnProceed.Visible = False
    'End Sub
    '#Region "Bind"
    '    Sub Bind()
    '        Dim oData As New DataTable
    '        oData = Get_UserApproval(Me.SchemeID, Replace(Me.sesBranchId, "'", ""), Me.NTF)
    '        cboApprovedBy.DataSource = oData.DefaultView
    '        cboApprovedBy.DataTextField = "Name"
    '        cboApprovedBy.DataValueField = "ID"
    '        cboApprovedBy.DataBind()
    '        cboApprovedBy.Items.Insert(0, "Select One")
    '        cboApprovedBy.Items(0).Value = "0"

    '    End Sub
    '#End Region
    'Private Sub BindBankType()
    '    Dim oDataTable As DataTable = New DataTable
    '    Dim oRow As DataRow
    '    Dim i As Integer
    '    Dim splitListData() As String
    '    Dim splitRow() As String
    '    Dim strList As String
    '    oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
    '    oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))
    '    strList = "B,BANK-C,CASH"
    '    oRow = oDataTable.NewRow()
    '    oRow("ID") = ""
    '    oRow("Description") = ""

    '    splitListData = Split(strList, "-")
    '    For i = 0 To UBound(splitListData)
    '        splitRow = Split(splitListData(i), ",")
    '        oRow("ID") = splitRow(0)
    '        oRow("Description") = splitRow(1)
    '        oDataTable.Rows.Add(oRow)
    '        oRow = oDataTable.NewRow()
    '    Next

    'cmbAccountType.DataValueField = "ID"
    'cmbAccountType.DataTextField = "Description"
    'cmbAccountType.DataSource = oDataTable
    'cmbAccountType.DataBind()
    'cmbAccountType.Dispose()
    'cmbAccountType.Items.Insert(0, "Select One")
    'cmbAccountType.Items(0).Value = "0"
    'cmbAccountType.SelectedIndex = 0
    ' End Sub

End Class