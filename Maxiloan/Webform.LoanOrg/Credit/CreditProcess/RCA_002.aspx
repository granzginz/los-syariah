﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RCA_002.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.RCA_002" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Persetujuan Kredit</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script language="JavaScript" src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date + '_txtDate');
            var calendar = eval('document.forms[0].all.' + date + '_txtDate_imgCalender');
            if (chk == true) {
                text.disabled = true;
                calendar.disabled = true;
                text.value = '';
                return true;
            }
            else {
                text.disabled = false;
                calendar.disabled = false;
                return true;
            }
        }


        function OpenWinApplication(pApplicationID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'Application', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {            
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <%--<script language="javascript" type="text/javascript">
		<!--
        function fConfirm() {
            if (window.confirm("Apakah Yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }	
			-->
    </script>--%>
    <script language="javascript" type="text/javascript">

        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }
        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            eval('document.forms[0].tempChild').value = "";
            var tmp;
            var child = eval('document.forms[0].' + pChild);
            var tampung = eval('document.forms[0].' + pTampung);
            var tampungname = eval('document.forms[0].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('All', '');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms[0].' + tampungGrandChild).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
        function setCboBatchDate(l, j) {
            eval('document.forms[0].' + tampungGrandChild2).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
        function drdCompanyChange() { }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <input id="tempParent" type="hidden" name="Hidden1" runat="server" />
    <input id="tempParentName" type="hidden" name="Hidden1" runat="server" />
    <input id="tempChild" type="hidden" name="tempChild" runat="server" />
    <input id="tempChildName" type="hidden" name="tempChildName" runat="server" />
    <input id="tempChild2" type="hidden" name="tempChild2" runat="server" />
    <input id="tempGrandChild" type="hidden" name="tempGrandChild" runat="server" />
    <input id="tempGrandChildName" type="hidden" name="tempGrandChildName" runat="server" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PERMINTAAN APPROVAL PEMBIAYAAN</h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustName" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    No. Aplikasi
                </label>
                <asp:HyperLink ID="lblApplicationID" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
        </div>
    </div>
<%--    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Akan diSetujui Oleh
                </label>
                <asp:DropDownList ID="cboApprovedBy" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Akan disetujui Oleh"
                    Display="Dynamic" InitialValue="0" ControlToValidate="cboApprovedBy" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
            <div class="form_right">
                <label>
                    No. Kontrak
                </label>
                <asp:TextBox ID="txtAgreementNo" runat="server" MaxLength="20">-</asp:TextBox><asp:Label
                    ID="lblMsgAgreementNo" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
    </div>--%>
    <div class="form_box_hide">
        <div>
            <div class="form_left">
                <label>
                    Funding Bank / cabang
                </label>
                <asp:DropDownList ID="drdCompany" runat="server" />
                <%--onchange="<%#drdCompanyChange()%>">
                </asp:DropDownList>--%>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak Bank
                </label>
                <asp:DropDownList ID="drdContract" runat="server" />
                <%-- onchange="<%#drdContractChange()%>">
                </asp:DropDownList>--%>
            </div>
        </div>
    </div>
    <div class="form_box_hide">
        <div>
            <div class="form_left">
                <label>
                    No. Batch
                </label>
                <asp:DropDownList ID="drdBatchDate" runat="server" onchange="setCboBatchDate(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);">
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                </label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Rekomendasi Approval</label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:TextBox ID="txtRecommendation" runat="server" style="width:100%" TextMode="MultiLine" class="multiline_textbox"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Hal-hal yang Menyimpang &amp; argumentasi</label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <asp:TextBox ID="txtArgumentasi" runat="server" style="width:100%" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT &amp; KONDISI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="grid_wrapper_ws">
            <asp:DataGrid ID="dtgTC" runat="server" Width="1450px" DataKeyField="TCName" PageSize="3"
                AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_ws">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <HeaderStyle Width="50px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                        <ItemStyle HorizontalAlign="Left" Width="300px"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                        <HeaderStyle Width="50px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="PERIKSA">
                        <HeaderStyle Width="150px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkTCChecked" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                runat="server"></asp:CheckBox>
                            <asp:Label ID="lblVTCChecked" runat="server">harus diperiksa</asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                        <HeaderStyle Width="100px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="TANGGAL JANJI">
                        <HeaderStyle Width="350px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtPromiseDate" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="calExPromiseDate" TargetControlID="txtPromiseDate"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:Label ID="lblVPromiseDate" runat="server">Tanggal Janji harus > hari ini</asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="CATATAN">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:TextBox ID="txtTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                runat="server" Width="95%">
                            </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="form_single">
            <h4>
                SYARAT &amp; KONDISI CHECK LIST
            </h4>
        </div>
    </div>
    <div class="form_box_hide">
        <div class="grid_wrapper_ws">
            <asp:DataGrid ID="dtgTC2" runat="server" Width="1450px" DataKeyField="TCName" PageSize="3"
                AutoGenerateColumns="False" BorderWidth="0px" CellPadding="3" CellSpacing="1"
                CssClass="grid_ws">
                <HeaderStyle CssClass="th" />
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <HeaderStyle Width="50px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                        <HeaderStyle Width="300px"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST">
                        <HeaderStyle Width="100px"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                        <HeaderStyle Width="50px"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="PERIKSA">
                        <HeaderStyle Width="150px"></HeaderStyle>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkTCCheck2" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                runat="server"></asp:CheckBox>
                            <asp:Label ID="lblVTC2Checked" runat="server">Harus diperiksa</asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY">
                        <HeaderStyle Width="50px"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="TANGGAL JANJI" Visible="true">
                        <HeaderStyle Width="350px"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtPromiseDate2" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender runat="server" ID="calExPromiseDate" TargetControlID="txtPromiseDate2"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:Label ID="lblVPromiseDate2" runat="server">Tanggal Janji harus > hari ini</asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="CATATAN">
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                Width="95%">
                            </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                    </asp:BoundColumn>
                    <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnView" runat="server" Visible="False" CausesValidation="True" Text="View"
            CssClass="small button blue" />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
        <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Back" CssClass="small button gray" />
    </div>
    </form>
</body>
</html>
