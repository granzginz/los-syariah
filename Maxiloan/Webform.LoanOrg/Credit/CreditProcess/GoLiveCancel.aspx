﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GoLiveCancel.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.GoLiveCancel" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Batal Pencairan</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                BATAL PENCAIRAN</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn>
                            <HeaderStyle Width="3%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <HeaderTemplate>
                                <asp:CheckBox AutoPostBack="True" ID="chkAll" runat="server" OnCheckedChanged="SelectAll">
                                </asp:CheckBox>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkItem" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                            <HeaderStyle Width="10%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAgreementNo" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                                <asp:Label runat="server" ID="lblAgreementNo" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.AgreementNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                            <HeaderStyle Width="17%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                                <asp:Label runat="server" ID="lblCustomerID" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="SupplierName" HeaderText="NAMA SUPPLIER">
                            <HeaderStyle Width="17%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkSupplier" Text='<%# DataBinder.eval(Container,"DataItem.SupplierName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                                <asp:Label runat="server" ID="lblSupplierID" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.SupplierID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="CONTACTPERSONNAME" SortExpression="CONTACTPERSONNAME"
                            HeaderText="KONTAK">
                            <HeaderStyle Width="17%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="TELP SUPPLIER">
                            <HeaderStyle Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="AO" HeaderText="NAMA ACCOUNT OFFICER">
                            <HeaderStyle Width="17%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAO" Text='<%# DataBinder.eval(Container,"DataItem.AO")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                                <asp:Label runat="server" ID="lblAOID" Text='<%# DataBinder.eval(Container,"DataItem.AOID")%>'
                                    Visible="False">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="DeliveryOrderDate" SortExpression="DeliveryOrderDate"
                            HeaderText="TGL DO" DataFormatString="{0:dd/MM/yyyy}">
                            <HeaderStyle Width="7%"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="ApplicationID" HeaderText="ApplicationID">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="AgreementDate" HeaderText="AgreementDate"
                            DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="SurveyDate"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="AADate"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="FirstInstallment"></asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="Application Id" Visible="False">
                            <HeaderStyle Width="17%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblApplicationId" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" EnableViewState="False" Text="Go" CssClass="small buttongo blue" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtGoPage"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbSave" runat="server" CssClass="small button blue" CausesValidation="true"
                Text="Save"></asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI KONTRAK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label class="medium_text">
                        Cari Berdasarkan
                    </label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                        <asp:ListItem Value="AgreementNo">No. Kontrak</asp:ListItem>
                        <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                        <asp:ListItem Value="AO">Nama Account Officer</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="imbSearch" runat="server" CausesValidation="true" CssClass="small button blue"
                Text="Find"></asp:Button>
            <asp:Button ID="imbReset" runat="server" CausesValidation="False" CssClass="small button gray"
                Text="Reset"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
