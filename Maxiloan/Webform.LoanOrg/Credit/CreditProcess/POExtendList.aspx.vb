﻿#Region "Import"
Imports System.Data.SqlClient
Imports System.Data
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class POExtendList
    Inherits Maxiloan.Webform.WebBased

#Region "Declaration"   
    Protected WithEvents frm_Net As System.Web.UI.HtmlControls.HtmlForm   
    Protected WithEvents divScript As System.Web.UI.HtmlControls.HtmlGenericControl
#End Region

#Region "Constanta"
    Private m_controller As New POController
    Private oPOExtend As New Parameter.PO
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property cmdwhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "POExtend"
            Me.cmdwhere = "Remain < 0"
            Me.SortBy = "PONo ASC"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                If Request.QueryString("Result") = "OK" Then
                    lblMessage.Text = "Perpanjangan Purchase Order Berhasil"
                Else
                    lblMessage.Text = ""
                End If

                BindGrid()
            End If
        End If
    End Sub
#End Region
#Region "BindGrid"
    Sub BindGrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        With oPOExtend
            .strConnection = GetConnectionString
            .WhereCond = Me.cmdwhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .BusinessDate = Me.BusinessDate
        End With
        oPOExtend = m_controller.POExtendPaging(oPOExtend)

        DtUserList = oPOExtend.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oPOExtend.TotalRecords
        Try
            dtgPOExtend.DataSource = DvUserList
            dtgPOExtend.DataBind()
        Catch en As System.Web.HttpException
            dtgPOExtend.CurrentPageIndex = 0
            dtgPOExtend.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            txtGoPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region
#Region "LinkTo"
    Function LinkToPONo(ByVal strStyle As String, ByVal strAccountPayableNo As String, ByVal strBranchID As String) As String
        Return "javascript:OpenViewApSupplier('" & strStyle & "', '" & strAccountPayableNo & "','" & strBranchID & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strSupplierID & "','" & strStyle & "')"
    End Function

    Function LinkToAgreementNo(ByVal strApplicationId As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinAgreementNo('" & strApplicationId & "','" & strStyle & "')"
    End Function

    Function LinkToPOViewExtend(ByVal strBranchID As String, ByVal strApplicationID As String, ByVal strPONo As String, ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinPOViewExtend('" & strBranchID & "','" & strApplicationID & "','" & strPONo & "','" & strSupplierID & "','" & strStyle & "')"
    End Function

#End Region
#Region "imgReset"
    Private Sub imgReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        viewstate("Search") = ""
        isExpired.SelectedIndex = 0
        dtgPOExtend.CurrentPageIndex = 0
        lblMessage.Text = ""
        BindGrid()
    End Sub
#End Region
#Region "dtgPOExtend_ItemCommand"
    Protected Sub dtgPOExtend_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPOExtend.ItemCommand
        Dim hynAgreementNo As New HyperLink
        Dim hynPONo As New HyperLink
        Dim hynCustomerName As New HyperLink
        Dim hynSupplierName As New HyperLink

        Dim lblAgreementNo As New Label
        Dim lblSupplierID As New Label
        Dim lblCustomerID As New Label
        Dim lblAccountPayableNo As New Label
        Dim lblApplicationID As New Label
        Dim lblPOExpiredDate As New Label
        Dim lblPOOriginalExpiredDate As New Label
        Dim lblPOExtendCounter As New Label
        Dim lblPOExtendDays As New Label
        Dim lblRemain As New Label
        Dim lblPODate As New Label
        Dim lblPOTotal As New Label
        If e.CommandName = "Extend" Then
            hynAgreementNo = CType(e.Item.FindControl("hynAgreementNo"), HyperLink)
            hynPONo = CType(e.Item.FindControl("hynPONo"), HyperLink)
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            hynSupplierName = CType(e.Item.FindControl("hynSupplierName"), HyperLink)

            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblAccountPayableNo = CType(e.Item.FindControl("lblAccountPayableNo"), Label)

            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblPOExpiredDate = CType(e.Item.FindControl("lblPOExpiredDate"), Label)
            lblPOOriginalExpiredDate = CType(e.Item.FindControl("lblPOOriginalExpiredDate"), Label)
            lblPOExtendCounter = CType(e.Item.FindControl("lblPOExtendCounter"), Label)
            lblPOExtendDays = CType(e.Item.FindControl("lblPOExtendDays"), Label)
            lblRemain = CType(e.Item.FindControl("lblRemain"), Label)
            lblPODate = CType(e.Item.FindControl("lblPODate"), Label)
            lblPOTotal = CType(e.Item.FindControl("lblPOTotal"), Label)


            Dim cookie As HttpCookie = Request.Cookies("POExtend")
            If Not cookie Is Nothing Then
                cookie.Values("PONo") = hynPONo.Text.Trim
                cookie.Values("CustomerID") = lblCustomerID.Text.Trim
                cookie.Values("CustomerName") = hynCustomerName.Text
                cookie.Values("SupplierID") = lblSupplierID.Text.Trim
                cookie.Values("SupplierName") = hynSupplierName.Text
                cookie.Values("ApplicationID") = lblApplicationID.Text.Trim
                cookie.Values("POExpiredDate") = lblPOExpiredDate.Text.Trim
                cookie.Values("POOriginalExpiredDate") = lblPOOriginalExpiredDate.Text.Trim
                cookie.Values("POExtendCounter") = lblPOExtendCounter.Text.Trim
                cookie.Values("POExtendDays") = lblPOExtendDays.Text.Trim
                cookie.Values("Remain") = lblRemain.Text.Trim
                cookie.Values("AgreementNo") = lblAgreementNo.Text.Trim
                cookie.Values("PODate") = lblPODate.Text.Trim
                cookie.Values("POTotal") = lblPOTotal.Text.Trim
                cookie.Values("AccountPayableNo") = lblAccountPayableNo.Text.Trim
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("POExtend")
                cookieNew.Values.Add("PONo", hynPONo.Text.Trim)
                cookieNew.Values.Add("CustomerID", lblCustomerID.Text.Trim)
                cookieNew.Values.Add("CustomerName", hynCustomerName.Text)
                cookieNew.Values.Add("SupplierID", lblSupplierID.Text.Trim)
                cookieNew.Values.Add("SupplierName", hynSupplierName.Text)
                cookieNew.Values.Add("ApplicationID", lblApplicationID.Text.Trim)
                cookieNew.Values.Add("POExpiredDate", lblPOExpiredDate.Text.Trim)
                cookieNew.Values.Add("POOriginalExpiredDate", lblPOOriginalExpiredDate.Text.Trim)
                cookieNew.Values.Add("POExtendCounter", lblPOExtendCounter.Text.Trim)
                cookieNew.Values.Add("POExtendDays", lblPOExtendDays.Text.Trim)
                cookieNew.Values.Add("Remain", lblRemain.Text.Trim)
                cookieNew.Values.Add("AgreementNo", lblAgreementNo.Text.Trim)
                cookieNew.Values.Add("PODate", lblPODate.Text.Trim)
                cookieNew.Values.Add("POTotal", lblPOTotal.Text.Trim)
                cookieNew.Values.Add("AccountPayableNo", lblAccountPayableNo.Text.Trim)
                Response.AppendCookie(cookieNew)
            End If
            response.redirect("POExtendDetail.aspx")
            'strAddress = "POExtendDetail.aspx?PONo=" & hynPONo.Text.Trim & ""
            'strAddress &= "&CustomerID=" & lblCustomerID.Text.Trim & "&CustomerName=" & hynCustomerName.Text & ""
            'strAddress &= "&SupplierID=" & lblSupplierID.Text.Trim & "&SupplierName=" & hynSupplierName.Text & ""
            ''strAddress &= "&AccountPayableNo=" & lblAccountPayableNo.Text.Trim & ""
            'strAddress &= "&ApplicationID=" & lblApplicationID.Text.Trim & ""
            'strAddress &= "&POExpiredDate=" & lblPOExpiredDate.Text.Trim & ""
            'strAddress &= "&POOriginalExpiredDate=" & lblPOOriginalExpiredDate.Text.Trim & ""
            'strAddress &= "&POExtendCounter=" & lblPOExtendCounter.Text.Trim & ""
            'strAddress &= "&POExtendDays=" & lblPOExtendDays.Text.Trim & ""
            'strAddress &= "&Remain=" & lblRemain.Text.Trim & ""
            'strAddress &= "&AgreementNo=" & lblAgreementNo.Text.Trim & ""
            'strAddress &= "&PODate=" & lblPODate.Text.Trim & ""
            'strAddress &= "&POTotal=" & lblPOTotal.Text.Trim & ""
            'Server.Transfer(strAddress)
        End If
    End Sub
#End Region
#Region "dtgPOExtend_ItemDataBound"
    Private Sub dtgPOExtend_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPOExtend.ItemDataBound
        Dim hynAgreementNo As New HyperLink
        Dim hynPONo As New HyperLink
        Dim hynCustomerName As New HyperLink
        Dim hynSupplierName As New HyperLink

        Dim lblAgreementNo As New Label
        Dim lblSupplierID As New Label
        Dim lblCustomerID As New Label
        Dim lblAccountPayableNo As New Label
        Dim lblApplicationID As New Label

        If e.Item.ItemIndex >= 0 Then
            hynAgreementNo = CType(e.Item.FindControl("hynAgreementNo"), HyperLink)
            hynPONo = CType(e.Item.FindControl("hynPONo"), HyperLink)
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            hynSupplierName = CType(e.Item.FindControl("hynSupplierName"), HyperLink)

            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblAccountPayableNo = CType(e.Item.FindControl("lblAccountPayableNo"), Label)
            hynAgreementNo.NavigateUrl = LinkToAgreementNo(lblApplicationID.Text.Trim, "AccAcq")
            hynPONo.NavigateUrl = LinkToPOViewExtend(Me.BranchID, lblApplicationID.Text.Trim, hynPONo.Text.Trim, lblSupplierID.Text.Trim, "AccAcq")
            hynCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            hynSupplierName.NavigateUrl = LinkToSupplier(lblSupplierID.Text.Trim, "AccAcq")
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPOExtend.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGrid()
    End Sub
#End Region
#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If isExpired.SelectedItem.Value = "1" Then
            Me.cmdwhere = "Remain < 0"
        ElseIf isExpired.SelectedItem.Value = "0" Then
            Me.cmdwhere = "Remain >=0"
        End If

        Dim strwhere As String
        If txtSearch.Text.Trim <> "" Then
            If Me.cmdwhere.Trim <> "" Then
                If Right(RTrim(txtSearch.Text), 1) = "%" Then
                    strwhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text + "'"
                    strwhere = Replace(strwhere, "=", "like")
                    Me.cmdwhere = Me.cmdwhere + " and " + strwhere
                Else
                    Me.cmdwhere = Me.cmdwhere + " and " + cboSearch.SelectedItem.Value + " = '" + txtSearch.Text + "'"
                End If
            Else
                If Right(RTrim(txtSearch.Text), 1) = "%" Then
                    strwhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text + "'"
                    strwhere = Replace(strwhere, "=", "like")
                    Me.cmdwhere = strwhere
                Else
                    Me.cmdwhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text + "'"
                End If
            End If

        End If
        BindGrid()
    End Sub
#End Region
#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        isExpired.SelectedIndex = 0
        txtSearch.Text = ""
        Me.cmdwhere = "Remain < 0"
        BindGrid()
    End Sub
#End Region

End Class