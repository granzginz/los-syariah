﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InvoiceData.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.InvoiceData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerFacility.ascx" TagName="ucViewCustomerFacility" 
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTabFactoring.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc7" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invoice Data</title>
   
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    
    <%--<script language="javascript" type="text/javascript">
       
        function hitungUlang()
        {
            var bungaPercent = document.getElementById('hdfBungaPercent').value;
            var minimalRetensiPercent = document.getElementById('hdfRentensiPercent').value;
            var retensiPercent = document.getElementById('ucRentensiPercent_txtNumber').value;

            if (retensiPercent < minimalRetensiPercent)
            {
                $('#ucRentensiPercent_txtNumber').val(number_format(minimalRetensiPercent, 2));
                retensiPercent = minimalRetensiPercent;
            }

            var AmountString = document.getElementById('ucInvoiceAmount_txtNumber').value;
            var Amount = parseInt(AmountString.replace(/\s*,\s*/g, ''));
            var jk = hitungJangkaWaktu();

            var retensi = Amount * retensiPercent / 100;
            var dibiayai = Amount - retensi;
            //var bunga = (dibiayai * bungaPercent / 100) * jk / 365;
            var bunga = (dibiayai * bungaPercent / 100) * jk / 360;
  
            $('#lblJangkaWaktu').html(jk);
            $('#lblRentensi').html(number_format(retensi, 0));
            $('#lblBunga').html(number_format(bunga, 0));
            $('#lblTagihanYangDibiayai').html(number_format(dibiayai, 0));
        }

        function hitungJangkaWaktu()
        {
            var _tglCair = document.getElementById('hdfTanggalPencairan').value;
            var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);
            console.log(tglCair)

            var _invoiceDueDate = document.getElementById('txtInvoiceDueDate').value;
            dateString = _invoiceDueDate.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var invoiceDueDate = new Date(dateString[3], dateString[2] - 1, dateString[1]);
            console.log(invoiceDueDate)

            var days = daysBetween(tglCair, invoiceDueDate);
            return days;
        }

        function treatAsUTC(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }

        function daysBetween(startDate, endDate) {
            var millisecondsPerDay = 24 * 60 * 60 * 1000;
            return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
        }


        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>--%>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnlSupplier">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnlSupplier">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <uc1:ucViewCustomerFacility id="ucViewCustomerFacility1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h4>ENTRI INVOICE DATA</h4>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hdfTanggalPencairan" />
            <asp:HiddenField runat="server" ID="hdfApplicationID" />
            <div class="form_box">
                <div class="form_single">
                    <label>Tanggal Pencairan</label>
                    <asp:Label runat="server" ID="lblTanggalPencairan" ></asp:Label>    
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlListInvoice">
            <div class="form_button">
                <asp:Button ID="btnAdd" runat="server" Text="Add Invoice" CssClass="small button green"  CausesValidation="False" />
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h5>DAFTAR INVOICE</h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InvoiceSeqNo" HeaderText="No" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" >
                                <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../../../../Images/icondelete.gif"
                                            CommandName="Delete" OnClientClick="return DeleteConfirm()"  OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# DataBinder.eval(Container, "DataItem.ApplicationID") & "," & DataBinder.eval(Container, "DataItem.InvoiceSeqNo")  %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                               <%-- <asp:TemplateColumn HeaderText="Edit" HeaderStyle-Width="30px" >
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="../../../../Images/iconedit.gif"
                                            CommandName="EditInvoice" OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# DataBinder.eval(Container, "DataItem.ApplicationID") & "," & DataBinder.eval(Container, "DataItem.InvoiceSeqNo")  %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>--%>
                                <asp:BoundColumn Visible="True" DataField="InvoiceNo" HeaderText="Invoice No" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDate" HeaderText="Inv Date"  DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDueDate" HeaderText="Inv Due Date"  DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceAmount" HeaderText="Invoice Amount"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="TotalPembiayaan" HeaderText="Dibiayai"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="JangkaWaktu" HeaderText="Periode"  DataFormatString="{0:0 days}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Bunga"  DataFormatString="{0:0,0.00}" ></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Next" CssClass="small button blue"  CausesValidation="True" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
            </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInputInvoice">
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Invoice No</label>
                    <asp:HiddenField runat="server" ID="hdfInvoiceSeqNo" />
                    <asp:TextBox runat="server" ID="txtInvoiceNo" ></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="rfvInvoiceNo" ControlToValidate="txtInvoiceNo"
                        Enabled="true" ErrorMessage="*" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_single">    
                    <label class="label_req">Invoice Date</label>
                    <asp:TextBox ID="txtInvoiceDate" runat="server" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtInvoiceDate" Format="dd/MM/yyyy" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvInvoiceDate" ControlToValidate="txtInvoiceDate"
                        Enabled="true" ErrorMessage="*" CssClass="validator_general" Display="Dynamic" />
                </div>
            </div> 
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Invoice Due Date</label>
                    <asp:TextBox ID="txtInvoiceDueDate" runat="server" CssClass="small_text" onchange="getBungaAmount()"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtInvoiceDueDate" Format="dd/MM/yyyy" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvInvoiceDueDate" ControlToValidate="txtInvoiceDueDate"
                        Enabled="true" ErrorMessage="*" CssClass="validator_general" Display="Dynamic" />
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_single">
                    <label class="label_req">Invoice Amount</label>
                    <uc7:ucnumberformat runat="server" id="ucInvoiceAmount" OnClientChange="hitungUlang()"></uc7:ucnumberformat>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Retensi</label>
                     <asp:HiddenField runat="server" ID="hdfRentensiPercent" />
                     <div style="margin-left:-8px;display:inline">
                        <uc7:ucnumberformat runat="server" id="ucRentensiPercent" width="70" OnClientChange="hitungUlang()"></uc7:ucnumberformat> %
                        <asp:Label runat="server" ID="lblRentensi"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                     <div class="numberAlign2">
                        <uc7:ucnumberformat runat="server" id="ucRetensiAmount" OnClientChange="hitungUlangByRate()"></uc7:ucnumberformat>
                        <asp:HiddenField runat="server" ID="hdfretensiamount" />
                     </div>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Tagihan yang dibiayai</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblTagihanYangDibiayai" CssClass="regular_text" visible="false"></asp:Label>
                        <asp:Label runat="server" ID="lblTagihanYangDibiayaiAmount" CssClass="numberAlign2 regular_text" visible="false"></asp:Label>

                        <uc7:ucnumberformat runat="server" id="lblTagihanYangDibiayai2" width="70" OnClientChange="hitungUlangByTotalRate()"></uc7:ucnumberformat> %
                        <asp:Label runat="server" ID="Label1"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                     <div class="numberAlign2">
                        <uc7:ucnumberformat runat="server" id="lblTagihanYangDibiayaiAmount2" OnClientChange="hitungUlangByTotal()"></uc7:ucnumberformat>
                     </div>
                     </div>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Bunga</label>
                     <asp:HiddenField runat="server" ID="hdfBungaPercent" />
                     <asp:HiddenField runat="server" ID="hdfBunga" />
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblBungaPercent" CssClass="regular_text" ></asp:Label>
                        <asp:Label runat="server" ID="lblBunga" CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Jangka Waktu</label>
                    <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblJangkaWaktu" >0</asp:Label>
                    </div>
                </div>
            </div> 
            <div class="form_button">
                <asp:Button ID="btnSaveInvoice" runat="server" Text="Save" CssClass="small button blue"  CausesValidation="True" OnClientClick="validationfacility()"/>
                <asp:Button ID="btnCancelInvoice" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
            </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSaveInvoice" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>

   <script language="javascript" type="text/javascript">
       $(document).ready(function () {
           getInvoicedate()
            hitungUlang()
            hitungUlangByRate()
            hitungJangkaWaktu()
        });

        function hitungUlang()
        {

            //Ambil Nilai Invoice
            let InvoiceAmount_ = document.getElementById('ucInvoiceAmount_txtNumber').value;
            let InvoiceAmount = parseInt(InvoiceAmount_.replace(/\s*,\s*/g, ''));
            console.log(InvoiceAmount)

            //Ambil Retensi %
            let RentensiPercent_ = document.getElementById('ucRentensiPercent_txtNumber').value;
            let RentensiPercent = parseFloat(RentensiPercent_.replace(/\s*,\s*/g, ''));
            console.log(RentensiPercent)

            //Hitung Nilai Retensi
            let totalBiaya = InvoiceAmount * RentensiPercent / 100;
            console.log(totalBiaya);

            //Taruh Nilai Retensi di textbox
            $('#ucRetensiAmount_txtNumber').val(number_format(totalBiaya, 0));
            $('#<%=hdfretensiamount.ClientID %>').val(totalBiaya.toFixed(0));

            //Hitung Tagihan Dibiayai %
            let totalTagihanDibiayai = 100 - RentensiPercent;
            console.log(totalTagihanDibiayai);
            //$('#lblTagihanYangDibiayai2').html(number_format(totalTagihanDibiayai, 6));
            $('#lblTagihanYangDibiayai2_txtNumber').val(number_format(totalTagihanDibiayai, 6));

            //Ambil Tagihan Dibiayai % 
            //let TagihanYangDibiayai_ = document.getElementById('lblTagihanYangDibiayai2').innerHTML;
            let TagihanYangDibiayai_ = document.getElementById('lblTagihanYangDibiayai2_txtNumber').value;
            let TagihanYangDibiayai = parseInt(TagihanYangDibiayai_.replace(/\s*,\s*/g, ''));
            console.log('tagihan dibiayai :', TagihanYangDibiayai)

           //Ambil Nilai Retensi
           let RetensiAmount_ = document.getElementById('ucRetensiAmount_txtNumber').value;
           let RetensiAmount = parseInt(RetensiAmount_.replace(/\s*,\s*/g, ''));
           console.log(RetensiAmount)

            //Total Nilai Tagihan Dibiayai 
            //let totalDibiayai = InvoiceAmount * TagihanYangDibiayai / 100;
            let totalDibiayai = InvoiceAmount - RetensiAmount;
            console.log(totalDibiayai);
            //$('#lblTagihanYangDibiayaiAmount2').html(number_format(totalDibiayai, 0));
            $('#lblTagihanYangDibiayaiAmount2_txtNumber').val(number_format(totalDibiayai, 0));
           
            ////Hitung Jangka Waktu
            //let jk = hitungJangkaWaktu();
            //$('#lblJangkaWaktu').html(number_format(jk, 0));
            //console.log(jk);

            ////Ambil Bunga Percent %
            //let BungaPercent_ = document.getElementById('hdfBungaPercent').value;
            //let BungaPercent = parseFloat(BungaPercent_.replace(/\s*,\s*/g, ''));
            //console.log(BungaPercent)

            //let bungaAmount = (totalDibiayai * BungaPercent / 100) * jk / 360;
            //console.log(bungaAmount);
            //$('#lblBunga').html(number_format(bungaAmount, 2));

            getBungaAmount()

            //Ambil Retensi Percent Awal %
            let minimalRetensiPercent_ = document.getElementById('hdfRentensiPercent').value;
            let minimalRetensiPercent = parseFloat(minimalRetensiPercent_.replace(/\s*,\s*/g, ''));
            console.log(minimalRetensiPercent)

            //Validasi Retensi Tidak Boleh Kurang Dari Retensi Awal dan hitung ulang
            if (RentensiPercent < minimalRetensiPercent) {
                $('#ucRentensiPercent_txtNumber').val(number_format(minimalRetensiPercent, 6));
                RentensiPercent = minimalRetensiPercent;
                hitungUlangByRate()
            }
        }

       function hitungUlangByRate() {
           //Ambil Nilai Invoice
           let InvoiceAmount_ = document.getElementById('ucInvoiceAmount_txtNumber').value;
           let InvoiceAmount = parseInt(InvoiceAmount_.replace(/\s*,\s*/g, ''));
           console.log(InvoiceAmount)

           //Ambil Nilai Retensi
           let RetensiAmount_ = document.getElementById('ucRetensiAmount_txtNumber').value;
           let RetensiAmount = parseInt(RetensiAmount_.replace(/\s*,\s*/g, ''));
           console.log(RetensiAmount)

           //Hitung Retensi %
           let Rate = (RetensiAmount / InvoiceAmount) * 100;
           console.log(Rate);

           //Taruh Retensi % di textbox
           $('#ucRentensiPercent_txtNumber').val(number_format(Rate, 6));
           
           //Hitung Tagihan Dibiayai %
           let totalTagihanDibiayai = 100 - Rate;
           console.log(totalTagihanDibiayai);
           //$('#lblTagihanYangDibiayai2').html(number_format(totalTagihanDibiayai, 6));
            $('#lblTagihanYangDibiayai2_txtNumber').val(number_format(totalTagihanDibiayai, 6));

           //Ambil Tagihan Dibiayai % 
           //let TagihanYangDibiayai_ = document.getElementById('lblTagihanYangDibiayai2').innerHTML;
           let TagihanYangDibiayai_ = document.getElementById('lblTagihanYangDibiayai2_txtNumber').value;
           let TagihanYangDibiayai = parseFloat(TagihanYangDibiayai_.replace(/\s*,\s*/g, ''));
           console.log(TagihanYangDibiayai)

           //Total Nilai Tagihan Dibiayai 
           //let totalDibiayai = InvoiceAmount * TagihanYangDibiayai / 100;
           let totalDibiayai = InvoiceAmount - RetensiAmount;
           console.log(totalDibiayai);
           //$('#lblTagihanYangDibiayaiAmount2').html(number_format(totalDibiayai, 0));
           $('#lblTagihanYangDibiayaiAmount2_txtNumber').val(number_format(totalDibiayai, 0));

           getBungaAmount()
       }

       function hitungUlangByTotal() {
           /* Ambil Nilai Invoice */
           let InvoiceAmount_ = document.getElementById('ucInvoiceAmount_txtNumber').value;
           let InvoiceAmount = parseInt(InvoiceAmount_.replace(/\s*,\s*/g, ''));
           console.log('Nilai Invoice : ', InvoiceAmount)

           /* Ambil Nilai Total Biaya */
           let TotalDibiayai_ = document.getElementById('lblTagihanYangDibiayaiAmount2_txtNumber').value;
           let TotalDibiayai = parseInt(TotalDibiayai_.replace(/\s*,\s*/g, ''));
           console.log('Nilai Dibiayai : ', TotalDibiayai)

           /* Hitung Dibiayai % */
           let Rate = (TotalDibiayai / InvoiceAmount) * 100;
           console.log('Dibiayai % : ',Rate);
           $('#lblTagihanYangDibiayai2_txtNumber').val(number_format(Rate, 6));

           /* Retensi % */
           let Retensi = (100 - Rate)
           $('#ucRentensiPercent_txtNumber').val(number_format(Retensi, 6));

           /* Total Nilai Tagihan Dibiayai */
           let RetensiAmount = InvoiceAmount - TotalDibiayai;
           console.log('Nilai Retensi : ',RetensiAmount);
           $('#ucRetensiAmount_txtNumber').val(number_format(RetensiAmount, 0));
           $('#<%=hdfretensiamount.ClientID %>').val(RetensiAmount.toFixed(0));

            getBungaAmount()
       }

       function hitungUlangByTotalRate() {
           /* Ambil Nilai Invoice */
           let InvoiceAmount_ = document.getElementById('ucInvoiceAmount_txtNumber').value;
           let InvoiceAmount = parseInt(InvoiceAmount_.replace(/\s*,\s*/g, ''));
           console.log('Nilai Invoice : ', InvoiceAmount)

           /* Ambil Tagihan Dibiayai % */
           let TagihanYangDibiayai_ = document.getElementById('lblTagihanYangDibiayai2_txtNumber').value;
           let TagihanYangDibiayai = parseFloat(TagihanYangDibiayai_.replace(/\s*,\s*/g, ''));
           console.log('Tagihan Dibiayai % : ',TagihanYangDibiayai)

           /* Retensi % */
           let Retensi = (100 - TagihanYangDibiayai)
           $('#ucRentensiPercent_txtNumber').val(number_format(Retensi, 6));

           hitungUlang()

           getBungaAmount()
       }

       function getBungaAmount() {
           /* Ambil Nilai Total Biaya */
           let TotalDibiayai_ = document.getElementById('lblTagihanYangDibiayaiAmount2_txtNumber').value;
           let TotalDibiayai = parseInt(TotalDibiayai_.replace(/\s*,\s*/g, ''));
           console.log('Nilai Dibiayai : ', TotalDibiayai)

           /* Hitung Jangka Waktu */
           let jk = hitungJangkaWaktu();
           $('#lblJangkaWaktu').html(number_format(jk, 0));
           console.log('Jangka Waktu : ',jk);

           /* Ambil Bunga Percent % */
           let BungaPercent_ = document.getElementById('hdfBungaPercent').value;
           let BungaPercent = parseFloat(BungaPercent_.replace(/\s*,\s*/g, ''));
           console.log('Bunga % : ',BungaPercent)

           let bungaAmount = (TotalDibiayai * BungaPercent / 100) * jk / 360;
           console.log(bungaAmount); 
           $('#lblBunga').html(number_format(bungaAmount, 2));
           $('#hdfBunga').val(number_format(bungaAmount, 2));
       }

       function getInvoicedate() {
           //Ambil Tanggal Pencairan
           var _tglCair = document.getElementById('hdfTanggalPencairan').value;
           //var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
           //var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);
           console.log('tanggal cair', _tglCair)
           $('#txtInvoiceDate').val(_tglCair)
       }

        function hitungJangkaWaktu()
        {
            var _tglCair = document.getElementById('hdfTanggalPencairan').value;
            var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);
            console.log(tglCair)

            var _invoiceDueDate = document.getElementById('txtInvoiceDueDate').value;
            dateString = _invoiceDueDate.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var invoiceDueDate = new Date(dateString[3], dateString[2] - 1, dateString[1]);
            console.log(invoiceDueDate)

            var days = daysBetween(tglCair, invoiceDueDate);
            return days;

            validationfacility()
        }

        function treatAsUTC(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }

        function daysBetween(startDate, endDate) {
            var millisecondsPerDay = 24 * 60 * 60 * 1000;
            return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
        }


        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

</body>
</html>
