﻿#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class ViewJournalDetailFactoringData
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Public Code"
    Private Property Tr_Nomor() As String
        Get
            Return CType(ViewState("tr_nomor"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("tr_nomor") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region
    Private Total As Double
    Private TotalD As Double
    Private TotalC As Double

#Region "Page_Load"
    Protected Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.Tr_Nomor = Request("tr_nomor")
            PageHeader()
            Bindgrid()
        End If
    End Sub
#End Region

#Region "PageHeader"
    Private Sub PageHeader()
        Dim strSQL As String
        Dim dtvoucher As Date
        Dim dtref As Date
        Dim objcommand As New SqlCommand
        Dim objcon As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        add_lbltr_nomor.Text = Me.Tr_Nomor
        strSQL = "select tr_date,tr_desc,reff_no,reff_date " &
                 " from gljournalh where tr_nomor=@TrNomor"
        Try
            If objcon.State = ConnectionState.Closed Then objcon.Open()
            objcommand.Parameters.Add("@TrNomor", SqlDbType.VarChar, 20).Value = Me.Tr_Nomor
            With objcommand
                .Connection = objcon
                .CommandText = strSQL
                objread = .ExecuteReader()
                If objread.Read() Then
                    dtvoucher = CDate(objread("tr_date"))
                    Try
                        add_lbltrdate.Text = dtvoucher.ToString("dd/MM/yyyy")
                    Catch en As Exception
                        add_lbltrdate.Text = ""
                    End Try
                    Try
                        add_lbldesc.Text = CType(objread("tr_desc"), String)
                    Catch en As Exception
                        add_lbldesc.Text = ""
                    End Try
                    Try
                        add_lblref.Text = CType(objread("reff_no"), String)
                    Catch es As Exception
                        add_lblref.Text = ""
                    End Try
                    Try
                        dtref = CDate(objread("reff_date"))
                        add_lblrefdate.Text = dtref.ToString("dd/MM/yyyy")
                    Catch es As Exception
                        add_lblrefdate.Text = ""
                    End Try
                End If
                objread.Close()
            End With
        Catch oExcept As Exception
            oExcept.Message.ToString()
        End Try

        strSQL = "select distinct coaco,coabranch,transactionid "
        strSQL &= " from gljournald where tr_nomor=@TrNomor"
        Try
            With objcommand
                .Connection = objcon
                .CommandText = strSQL
                objread = .ExecuteReader()
                If objread.Read() Then
                    add_lblcompanybranch.Text = CType(objread("coaco"), String) & "-" & CType(objread("coabranch"), String)
                    add_lbltransaction.Text = CType(objread("transactionid"), String)
                End If
                objread.Close()
            End With
        Catch oExcept As Exception
            oExcept.Message.ToString()
        Finally
            objcommand.Parameters.Clear()
            objcommand.Dispose()
            If objcon.State = ConnectionState.Open Then objcon.Close()
            objcon.Dispose()
        End Try

    End Sub
#End Region
#Region "Bindgrid"
    Sub Bindgrid(Optional ByVal pStrPageCommand As String = "")
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        Me.ApplicationID = Request("ApplicationID").ToString
        Me.SearchBy = " tr_nomor='" & Me.Tr_Nomor & "' order by SequenceNo asc"
        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""

            .SpName = "spJournal2"
        End With
        oContract = cContract.GetGeneralPaging(oContract)

        DtUserList = oContract.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oContract.TotalRecords
        Try
            DtgIndType.DataSource = DvUserList
            DtgIndType.DataBind()
        Catch en As System.Web.HttpException
            DtgIndType.CurrentPageIndex = 0
            DtgIndType.DataBind()
        End Try
    End Sub
#End Region
#Region "btn_Trans_view_back_Click"
    Private Sub btn_Trans_view_back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Trans_view_back.Click
        'Server.Transfer("viewJournalFactoringData.aspx?ApplicationID=" & Me.ApplicationID & "", False)
        Response.Redirect("viewJournalFactoringData.aspx?ApplicationID=" & Me.ApplicationID & "", False)
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgIndType.SortCommand
        Me.SortBy = e.SortExpression
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid()
    End Sub
#End Region
    Private Sub DtgIndType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgIndType.ItemDataBound
        Dim i As Integer
        Dim lblPost As Label
        Dim lblAmount As Label
        Dim lblAmountD As Label
        Dim lblAmountC As Label
        Dim add_lbltotal As Label
        Dim add_lbltotalD As Label
        Dim add_lbltotalC As Label
        Dim lblSelilih As Label

        If e.Item.ItemIndex >= 0 Then
            lblPost = CType(e.Item.FindControl("lblPost"), Label)
            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)

            lblAmountD = CType(e.Item.FindControl("lblAmountD"), Label)
            lblAmountC = CType(e.Item.FindControl("lblAmountC"), Label)



            If lblPost.Text.Trim = "C" Then
                Total = Total - CType(lblAmount.Text.Trim, Double)
            End If

            If lblPost.Text.Trim = "D" Then
                Total = Total + CType(lblAmount.Text.Trim, Double)
            End If

            TotalC = TotalC + CType(lblAmountC.Text.Trim, Double)
            TotalD = TotalD + CType(lblAmountD.Text.Trim, Double)

        End If
        If e.Item.ItemType = ListItemType.Footer Then
            add_lbltotal = CType(e.Item.FindControl("add_lbltotal"), Label)
            add_lbltotal.Text = FormatNumber(Total, 2)
            add_lbltotalD = CType(e.Item.FindControl("add_lbltotalD"), Label)
            add_lbltotalD.Text = FormatNumber(TotalD, 2)
            add_lbltotalC = CType(e.Item.FindControl("add_lbltotalC"), Label)
            add_lbltotalC.Text = FormatNumber(TotalC, 2)

            lblSelilih = CType(e.Item.FindControl("lblSelilih"), Label)
            lblSelilih.Text = "Selisih: " + Total.ToString
        End If
    End Sub

End Class