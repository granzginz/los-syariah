﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewActivityLogFact
	Inherits Maxiloan.Webform.WebBased

	Protected WithEvents ucApplicationTab1 As ucApplicationViewTabFactoring

#Region "Property"
	Property ApplicationID() As String
		Get
			Return ViewState("ApplicationID").ToString
		End Get
		Set(ByVal Value As String)
			ViewState("ApplicationID") = Value
		End Set
	End Property
	Property AgreementNo() As String
		Get
			Return ViewState("AgreementNo").ToString
		End Get
		Set(ByVal Value As String)
			ViewState("AgreementNo") = Value
		End Set
	End Property

	Property CustomerName() As String
		Get
			Return ViewState("CustomerName").ToString
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerName") = Value
		End Set
	End Property

	Property CustomerID() As String
		Get
			Return ViewState("CustomerID").ToString
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property
#End Region

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		If SessionInvalid() Then
			Exit Sub
		End If

		If Not Page.IsPostBack Then
			Me.FormID = "ViewAcLog"
			Me.ApplicationID = Request("ApplicationID").ToString
			BindEdit()
			initObjects()
			Bindgrid()
			ucApplicationTab1.ApplicationID = Me.ApplicationID
			ucApplicationTab1.setLink()

			Dim lb As New HyperLink
			lb = CType(Me.FindControl("lblCustomerName"), HyperLink)
			lb.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Me.CustomerID & "')"
		End If
	End Sub

	Sub Bindgrid()
		Dim objCommand As New SqlCommand
		Dim objConnection As New SqlConnection(GetConnectionString)

		Dim objReaderGrid As SqlDataReader

		Try
			If objConnection.State = ConnectionState.Closed Then objConnection.Open()

			objCommand.CommandType = CommandType.StoredProcedure
			objCommand.CommandText = "spViewActivityLog"
			objCommand.Connection = objConnection
			objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
			objReaderGrid = objCommand.ExecuteReader

			dtg.DataSource = objReaderGrid
			dtg.DataBind()
			objReaderGrid.Close()
		Catch ex As Exception
			Response.Write(ex.Message)
		Finally
			If objConnection.State = ConnectionState.Open Then objConnection.Close()
			objConnection.Dispose()
			objCommand.Dispose()
		End Try
	End Sub

	Sub BindEdit()
		Dim oApplication As New Parameter.Application
		Dim oData As New DataTable
		Dim oDataTC As New DataTable
		Dim oRow As DataRow
		Dim oController As New ApplicationController

		oApplication.strConnection = GetConnectionString()
		oApplication.BranchId = Replace(Me.sesBranchId, "'", "")
		oApplication.AppID = Me.ApplicationID
		oApplication = oController.GetViewApplication(oApplication)

		If Not oApplication Is Nothing Then
			oData = oApplication.ListData
			oDataTC = oApplication.DataTC
		End If

		If oData.Rows.Count > 0 Then
			oRow = oData.Rows(0)

			lblCustomerName.Text = (oRow("Name").ToString)
			lblAgreementNo.Text = (oRow("ApplicationID").ToString)
			Me.CustomerName = lblCustomerName.Text.Trim
			Me.CustomerID = (oRow("CustomerID").ToString)
		End If
	End Sub

#Region "Init Objects"
	Protected Sub initObjects()
		ucApplicationTab1.selectedTab("ActivityLog")
	End Sub

#End Region

	Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
		Dim lb As New Label
		If e.Item.ItemIndex >= 0 Then
			Me.CustomerName = e.Item.Cells(0).Text
			CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
		End If
	End Sub
End Class