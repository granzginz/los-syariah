﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic


Public Class ApplicationFactoring_003
    Inherits Maxiloan.Webform.WebBased
    Private oController As New ApplicationController
    Private m_controller As New ProductController

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplicationMK
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucLookupProductOffering1 As UcLookUpPdctOffering

    Protected WithEvents ucViewCustomerFacility1 As ucViewCustomerFacility
    Protected WithEvents ucMailingAddress As UcCompanyAddress

    Protected WithEvents ucApplicationTab1 As ucApplicationTabFactoring
    Protected WithEvents ucPiutangDibiayai As ucNumberFormat
	Protected WithEvents ucRetensi As ucNumberFormat
	Protected WithEvents ucBunga As ucNumberFormat
    Protected WithEvents ucDenda As ucNumberFormat

#End Region

#Region "Constanta"
    Dim oRow As DataRow
    Dim Status As Boolean
#End Region

#Region "Properties"
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property

    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property ProductID() As String
        Get
            Return ViewState("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property

    Property ProductOffID() As String
        Get
            Return ViewState("ProductOffID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOffID") = Value
        End Set
    End Property

    Property KodeDATI() As String
        Get
            Return ViewState("KodeDATI").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("KodeDATI") = Value
        End Set
    End Property

    Property Type() As String
        Get
            Return ViewState("Type").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Property Desc() As String
        Get
            Return ViewState("Desc").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Desc") = Value
        End Set
    End Property

    Property TotalCountApplicationID() As Integer
        Get
            Return CType(ViewState("TotalCountApplicationID"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("TotalCountApplicationID") = Value
        End Set
    End Property

    Property PageMode As String
        Get
            Return CType(ViewState("PageMode"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PageMode") = value
        End Set
    End Property
    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then Exit Sub

        If Not Page.IsPostBack Then
            ucMailingAddress.IsRequiredZipcode = False
            Me.CustomerID = Request("id")
            Me.ProspectAppID = "-"
            Me.FacilityNo = Request("fno")
            FillCboEmp()
            Dim oCustomclass As New Parameter.Application

            With oCustomclass
                .strConnection = GetConnectionString()
                .CustomerId = Me.CustomerID
            End With

            Me.TotalCountApplicationID = oController.GetCountAppliationId(oCustomclass)

            initObjects()

            Me.PageMode = Request("page")
            LoadingKegiatanUsaha()

            If Me.PageMode = "Edit" Then
                Me.ApplicationID = Request("appid")
                With ucViewApplication1
                    .ApplicationID = Me.ApplicationID
                    .CustomerID = Me.CustomerID
                    .bindData()
                    .initControls("OnNewApplication")
                    If .IsAppTimeLimitReached Then
                        ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                        btnSave.Visible = False
                    End If
                End With
                BindEdit()
                ucApplicationTab1.ApplicationID = Me.ApplicationID
                ucApplicationTab1.setLink()
                loadDataFasilitas()

            Else
                loadDataFasilitas()
                cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(ucViewCustomerFacility1.FasilitasInfo.KegiatanUsaha))
                refresh_cboJenisPembiayaan(ucViewCustomerFacility1.FasilitasInfo.KegiatanUsaha)
                cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(ucViewCustomerFacility1.FasilitasInfo.JenisPembiayaan))
                ucLookupProductOffering1.KegiatanUsaha = ucViewCustomerFacility1.FasilitasInfo.KegiatanUsaha
				ucRetensi.Text = FormatNumber(ucViewCustomerFacility1.FasilitasInfo.Retensi, 6)
				ucBunga.Text = FormatNumber(ucViewCustomerFacility1.FasilitasInfo.EffectiveRate, 6)
				ucPiutangDibiayai.Text = FormatNumber(100 - ucViewCustomerFacility1.FasilitasInfo.Retensi, 6)
				ucDenda.Text = FormatNumber(ucViewCustomerFacility1.FasilitasInfo.LateChargeRate, 2)
			End If

            If ucViewApplication1.isProceed = True Then
                btnSave.Visible = False
            End If

        End If
    End Sub
    Sub FillCboEmp()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim m_controller As New AssetDataController


        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = "BranchID = '" & Me.sesBranchId.Replace("'", "") & "' and EmployeePosition = 'AO'"
        oAssetData.Table = "BranchEmployee"
        oAssetData = m_controller.GetCboEmp(oAssetData)
        oData = oAssetData.ListData
        cbocmo.DataSource = oData
        cbocmo.DataTextField = "Name"
        cbocmo.DataValueField = "ID"
        cbocmo.DataBind()
        cbocmo.Items.Insert(0, "Select One")
        cbocmo.Items(0).Value = "Select One"
    End Sub
    Private Sub LoadingKegiatanUsaha()
        Me.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataSource = KegiatanUsahaS
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"
        cboKegiatanUsaha.SelectedIndex = 0


        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"
        cboJenisPembiyaan.DataSource = def
        cboJenisPembiyaan.Items.Insert(0, "Select One")
        cboJenisPembiyaan.Items(0).Value = "SelectOne"
        cboJenisPembiyaan.DataBind()

    End Sub

    Private Sub loadDataFasilitas()
        With ucViewCustomerFacility1
            .FacilityNo = Me.FacilityNo
            .CustomerID = Me.CustomerID
            .LoadCustomerFacility()
        End With
    End Sub

#Region "Init Objects"

    Protected Sub initObjects()
        ucViewApplication1.CustomerID = Me.CustomerID
        ucViewApplication1.bindData()
        ucViewApplication1.initControls("OnNewApplication")
        ucViewCustomerDetail1.CustomerID = Me.CustomerID
        ucViewCustomerDetail1.bindCustomerDetail()

        Me.Type = ucViewCustomerDetail1.CustomerType

        ucLookupProductOffering1.BranchID = Me.sesBranchId.Replace("'", "")

        ucApplicationTab1.selectedTab("Application")
        fillAddress(Me.CustomerID)

        ucMailingAddress.ValidatorTrue()
        ucMailingAddress.showMandatoryAll()
        pnlDataAplikasi.Visible = False
    End Sub

#End Region

#Region "FillAddress"
    Private Sub fillAddress(ByVal id As String)
        Dim oData As New DataTable
        Dim oApplication As New Parameter.Application
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        oApplication.CustomerId = id
        oApplication.Type = Me.Type
        oApplication = oController.GetAddress(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        cboCopyAddress.DataSource = oData.DefaultView
        cboCopyAddress.DataTextField = "Combo"
        cboCopyAddress.DataValueField = "Type"
        cboCopyAddress.DataBind()

        If Me.Type = "C" Then
            If oData.Rows.Count > 0 Then
                If Me.PageMode <> "Edit" Then
                    oRow = oData.Rows(0)
                    ucMailingAddress.Address = oRow.Item(0).ToString.Trim
                    ucMailingAddress.RT = oRow.Item(1).ToString.Trim
                    ucMailingAddress.RW = oRow.Item(2).ToString.Trim
                    ucMailingAddress.Kelurahan = oRow.Item(3).ToString.Trim
                    ucMailingAddress.Kecamatan = oRow.Item(4).ToString.Trim
                    ucMailingAddress.City = oRow.Item(5).ToString.Trim
                    ucMailingAddress.ZipCode = oRow.Item(6).ToString.Trim
                    ucMailingAddress.AreaPhone1 = oRow.Item(7).ToString.Trim
                    ucMailingAddress.Phone1 = oRow.Item(8).ToString.Trim
                    ucMailingAddress.AreaPhone2 = oRow.Item(9).ToString.Trim
                    ucMailingAddress.Phone2 = oRow.Item(10).ToString.Trim
                    ucMailingAddress.AreaFax = oRow.Item(11).ToString.Trim
                    ucMailingAddress.Fax = oRow.Item(12).ToString.Trim
                    ucMailingAddress.BindAddress()
                End If
            End If
        Else
            If Me.PageMode <> "Edit" Then
                Dim index As Integer
                For intLoop = 0 To oData.Rows.Count - 1
                    If oData.Rows(intLoop).Item("Type").ToString.Trim = "Residence" Then
                        index = intLoop
                    End If
                Next

                cboCopyAddress.SelectedIndex = cboCopyAddress.Items.IndexOf(cboCopyAddress.Items.FindByValue("Residence"))

                oRow = oData.Rows(index)
                ucMailingAddress.Address = oRow.Item(0).ToString.Trim
                ucMailingAddress.RT = oRow.Item(1).ToString.Trim
                ucMailingAddress.RW = oRow.Item(2).ToString.Trim
                ucMailingAddress.Kelurahan = oRow.Item(3).ToString.Trim
                ucMailingAddress.Kecamatan = oRow.Item(4).ToString.Trim
                ucMailingAddress.City = oRow.Item(5).ToString.Trim
                ucMailingAddress.ZipCode = oRow.Item(6).ToString.Trim
                ucMailingAddress.AreaPhone1 = oRow.Item(7).ToString.Trim
                ucMailingAddress.Phone1 = oRow.Item(8).ToString.Trim
                ucMailingAddress.AreaPhone2 = oRow.Item(9).ToString.Trim
                ucMailingAddress.Phone2 = oRow.Item(10).ToString.Trim
                ucMailingAddress.AreaFax = oRow.Item(11).ToString.Trim
                ucMailingAddress.Fax = oRow.Item(12).ToString.Trim
                ucMailingAddress.BindAddress()
            End If
        End If

        If Not ClientScript.IsStartupScriptRegistered("copyAddress") Then ClientScript.RegisterStartupScript(Me.GetType, "copyAddress", GenerateScript(oData))
    End Sub

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim strType As String
        Dim DataRow As DataRow()
        Dim i As Int32
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboCopyAddress.Items.Count - 1
            DataRow = DtTable.Select(" Type = '" & cboCopyAddress.Items(j).Value.Trim & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("Type")).Trim Then
                        strType = CStr(DataRow(i)("Type")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If
                    If strScript1 = "" Then
                        strScript1 = " new Array('" & DataRow(i)("Address").ToString.Trim.Replace(vbLf, "") & "','" & DataRow(i)("RT").ToString.Trim & "' " &
                                        ",'" & DataRow(i)("RW").ToString.Trim & "','" & DataRow(i)("Kelurahan").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("Kecamatan").ToString.Trim & "','" & DataRow(i)("City").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("ZipCode").ToString.Trim & "','" & DataRow(i)("AreaPhone1").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("Phone1").ToString.Trim & "','" & DataRow(i)("AreaPhone2").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("Phone2").ToString.Trim & "','" & DataRow(i)("AreaFax").ToString.Trim & "'" &
                                        ",'" & DataRow(i)("Fax").ToString.Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next
        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If
        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        strScript &= "</script>"
        Return strScript
    End Function

#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.AppID = Me.ApplicationID
        oApplication = oController.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)

            cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(oRow("KegiatanUsaha").ToString))
            refresh_cboJenisPembiayaan(oRow("KegiatanUsaha").ToString)
            cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(oRow("JenisPembiayaan").ToString))
            cboPayment.SelectedIndex = cboPayment.Items.IndexOf(cboPayment.Items.FindByValue(oRow("FactoringPaymentMethod").ToString.Trim))
            cboCaraBayar.SelectedIndex = cboCaraBayar.Items.IndexOf(cboCaraBayar.Items.FindByValue(oRow("WayOfPayment").ToString))

			ucPiutangDibiayai.Text = FormatNumber(oRow("FactoringPiutangDibiayai"), 6)
			ucRetensi.Text = FormatNumber(oRow("FactoringRetensi"), 6)
			ucBunga.Text = FormatNumber(oRow("FactoringInterest"), 2)
			ucDenda.Text = FormatNumber(oRow("PercentagePenalty"), 2)
			txtTanggalPencairan.Text = Format(IIf(String.IsNullOrEmpty(oRow("FactoringPencairanDate").ToString), "", oRow("FactoringPencairanDate")), "dd/MM/yyyy")

            hdfkabupaten.Value = oRow("kodedati").ToString
            txtKabupaten.Text = oRow("NamaKabKot").ToString
            txtAppNotes.Text = oRow("Notes").ToString.Trim

            With ucMailingAddress
                .Address = oRow("MailingAddress").ToString.Trim
                .RT = oRow("MailingRT").ToString.Trim
                .RW = oRow("MailingRW").ToString.Trim
                .Kelurahan = oRow("MailingKelurahan").ToString.Trim
                .Kecamatan = oRow("MailingKecamatan").ToString.Trim
                .City = oRow("MailingCity").ToString.Trim
                .ZipCode = oRow("MailingZipCode").ToString.Trim
                .AreaPhone1 = oRow("MailingAreaPhone1").ToString.Trim
                .Phone1 = oRow("MailingPhone1").ToString.Trim
                .AreaPhone2 = oRow("MailingAreaPhone2").ToString.Trim
                .Phone2 = oRow("MailingPhone2").ToString.Trim
                .AreaFax = oRow("MailingAreaFax").ToString.Trim
                .Fax = oRow("MailingFax").ToString.Trim
                .BindAddress()
            End With


            ucLookupProductOffering1.ProductID = oRow("ProductID").ToString.Trim
            ucLookupProductOffering1.ProductOfferingID = oRow("ProductOfferingID").ToString.Trim
            ucLookupProductOffering1.ProductOfferingDescription = oRow("Description").ToString.Trim

            ucLookupProductOffering1.KegiatanUsaha = oRow("KegiatanUsaha").ToString.Trim

            Me.ProductID = oRow("ProductID").ToString.Trim
            Me.ProductOffID = oRow("ProductOfferingID").ToString.Trim
            Me.FacilityNo = oRow("NoFasilitas").ToString.Trim

            cbocmo.SelectedIndex = cbocmo.Items.IndexOf(cbocmo.Items.FindByValue(oRow("AOID").ToString))

        End If
    End Sub
#End Region


#Region "BindTC"
    Protected Sub TC(ByVal AddEdit As String)
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        If AddEdit = "Add" Then
            oApplication.Type = Me.Type
            oApplication.ProductID = ucLookupProductOffering1.ProductID
        Else
            oApplication.AppID = Me.ApplicationID
        End If

        With oApplication
            .strConnection = GetConnectionString()
            .BranchId = Me.sesBranchId
            .AddEdit = AddEdit
            .PersonalCustomerType = ucViewCustomerDetail1.PersonalCustomerType
        End With

        oApplication = oController.GetTC(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC.DataSource = oData.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()
        'dtgTC.Width = CType(IIf(hdnGridGeneralSize.Value = "", "100%", hdnGridGeneralSize.Value), Unit)
        If AddEdit = "Edit" Then
            Dim intLoop As Integer
            For intLoop = 0 To dtgTC.Items.Count - 1
                If oData.Rows(intLoop).Item(4).ToString.Trim <> "" Then
                    CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), ucDateCE).Text = Format(oData.Rows(intLoop).Item(4), "dd/MM/yyyy")
                End If
            Next
        End If
    End Sub

    Sub TC2(ByVal AddEdit As String)
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        If AddEdit = "Add" Then
            oApplication.Type = Me.Type
            oApplication.ProductID = Me.ProductID
        Else
            oApplication.AppID = Me.ApplicationID
        End If

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.AddEdit = AddEdit
        oApplication = oController.GetTC2(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        dtgTC2.DataSource = oData.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()

        If AddEdit = "Edit" Then
            Dim intLoop As Integer
            For intLoop = 0 To dtgTC2.Items.Count - 1
                If oData.Rows(intLoop).Item("PromiseDate").ToString.Trim <> "" Then
                    CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), ucDateCE).Text = Format(oData.Rows(intLoop).Item("PromiseDate"), "dd/MM/yyyy")
                End If
            Next
        End If
    End Sub
    Private Function pathFile(ByVal Group As String, ByVal CustomerID As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & CustomerID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chkTCChecked As CheckBox
            Dim hyupload As HyperLink = CType(e.Item.FindControl("hyupload"), HyperLink)
            Dim txtDateCE As TextBox = CType(e.Item.FindControl("txtPromiseDate").FindControl("txtDateCE"), TextBox)
            Dim MasterTCID As TextBox = CType(e.Item.FindControl("MasterTCID"), TextBox)
            Dim imgAtteced As Image = CType(e.Item.FindControl("imgAtteced"), Image)


            If File.Exists(pathFile("Dokumen", Me.CustomerID) + MasterTCID.Text.ToString.Trim + ".jpg") Then
                imgAtteced.Visible = True
            End If

            chkTCChecked = CType(e.Item.FindControl("chkTCChecked"), CheckBox)

            If chkTCChecked.Checked Then
                txtDateCE.Enabled = False
            Else
                txtDateCE.Enabled = True
            End If

            hyupload.Attributes.Add("OnClick", "UploadDokument('" & ResolveClientUrl("~/webform.Utility/jLookup/UploadDokument.aspx?doc=" & MasterTCID.Text.Trim & "&CustomerID=" & Me.CustomerID) & "','Upload Dokument','" & jlookupContent.ClientID & "') ")

            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            chkTCChecked.Attributes.Add("OnClick",
                "return CheckTC('" & txtDateCE.ClientID & "',this.checked);")
        End If
    End Sub

    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim chkTCCheck2 As CheckBox
            Dim txtDateCE As TextBox = CType(e.Item.FindControl("txtPromiseDate2").FindControl("txtDateCE"), TextBox)


            chkTCCheck2 = CType(e.Item.FindControl("chkTCCheck2"), CheckBox)

            If chkTCCheck2.Checked Then
                txtDateCE.Enabled = False
            Else
                txtDateCE.Enabled = True
            End If


            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            chkTCCheck2.Attributes.Add("OnClick",
            "return CheckTC('" & txtDateCE.ClientID & "',this.checked);")
        End If
    End Sub
#End Region

#Region "LabelValidationFalse"
    Protected Sub LabelValidationFalse()
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim countDtg As Integer = CInt(IIf(DtgTC1count > DtgTC2count, DtgTC1count, DtgTC2count))
        Dim validCheck As New Label
        Dim Checked As Boolean
        Dim PriorTo As String
        Dim PromiseDate As ucDateCE
        Dim Mandatory As String

        For intLoop = 0 To countDtg
            If intLoop <= DtgTC1count Then
                PriorTo = dtgTC.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(5).Text.Trim
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), ucDateCE)
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = False
                If PriorTo = "APK" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If

            If intLoop <= DtgTC2count Then
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), ucDateCE)
                validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = False
                If PriorTo = "APK" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
        Next
    End Sub
#End Region

    Protected Sub cek_Check()
        Dim CheckTC As Boolean
        Dim CheckTC2 As Boolean
        Dim counter As Integer
        Dim PromiseDate As ucDateCE
        Dim PromiseDate2 As ucDateCE

        For counter = 0 To dtgTC.Items.Count - 1
            CheckTC = CType(dtgTC.Items(counter).FindControl("chkTCChecked"), CheckBox).Checked
            If CheckTC Then
                PromiseDate = CType(dtgTC.Items(counter).FindControl("txtPromiseDate"), ucDateCE)
                PromiseDate.Text = ""
            End If
        Next

        For counter = 0 To dtgTC2.Items.Count - 1
            CheckTC2 = CType(dtgTC2.Items(counter).FindControl("chkTCCheck2"), CheckBox).Checked
            If CheckTC2 Then
                PromiseDate2 = CType(dtgTC2.Items(counter).FindControl("txtPromiseDate2"), ucDateCE)
                PromiseDate2.Text = ""
            End If
        Next
    End Sub

#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.Page.IsValid Then
                lblMessage.Visible = False

                Dim oApplication As New Parameter.Application
                Dim oValidasiCollector As New Parameter.Application
                Dim oRefAddress As New Parameter.Address
                Dim oRefPersonal As New Parameter.Personal
                Dim oMailingAddress As New Parameter.Address
                Dim oGuarantorPersonal As New Parameter.Personal
                Dim oGuarantorAddress As New Parameter.Address
                Dim oData1 As New DataTable
                Dim oData2 As New DataTable
                Dim oData3 As New DataTable
                Dim oCustomclass As New Parameter.Application
                Dim PenaltyRate As Decimal

                With oCustomclass
                    .strConnection = GetConnectionString()
                    .CustomerId = Me.CustomerID
                End With

                'oCustomclass.Kelurahan = ucMailingAddress.Kelurahan
                'oCustomclass.ZipCode = ucMailingAddress.ZipCode
                'Validasi Collector
                oCustomclass.Kelurahan = ucMailingAddress.Kelurahan
                oCustomclass.ZipCode = ucMailingAddress.ZipCode
                oValidasiCollector = oController.ValidasiCollector(oCustomclass)
                If oValidasiCollector.ListData IsNot Nothing Then
                    If oValidasiCollector.ListData.Rows.Count < 1 Then
                        ShowMessage(lblMessage, "Collector belum disetting!", True)
                        Exit Sub
                    End If
                End If

                Dim oApplicationFee As New Parameter.Application
                Dim oData As New DataTable
                oApplicationFee.strConnection = GetConnectionString()
                oApplicationFee.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                oApplicationFee.ProductOffID = Me.ProductOffID
                oApplicationFee.ProductID = Me.ProductID
                oApplicationFee = oController.GetFee(oApplicationFee)
                If Not oApplicationFee Is Nothing Then
                    oData = oApplicationFee.ListData
                    If oData.Rows.Count > 0 Then
                        oRow = oData.Rows(0)
						PenaltyRate = oRow("PenaltyPercentage")
					End If
                End If

                If Me.TotalCountApplicationID = oController.GetCountAppliationId(oCustomclass) Then
                    Status = True
                    oData3.Columns.Add("AgreementNo", GetType(String))

                    If Status = False Then
                        LabelValidationFalse()
                        ShowMessage(lblMessage, "No. Kontrak Salah!", True)
                        Exit Sub
                    End If

                    oData1.Columns.Add("MasterTCID", GetType(String))
                    oData1.Columns.Add("PriorTo", GetType(String))
                    oData1.Columns.Add("IsChecked", GetType(String))
                    oData1.Columns.Add("IsMandatory", GetType(String))
                    oData1.Columns.Add("PromiseDate", GetType(String))
                    oData1.Columns.Add("Notes", GetType(String))

                    oData2.Columns.Add("MasterTCID", GetType(String))
                    oData2.Columns.Add("AGTCCLSequenceNo", GetType(String))
                    oData2.Columns.Add("IsChecked", GetType(String))
                    oData2.Columns.Add("PromiseDate", GetType(String))
                    oData2.Columns.Add("Notes", GetType(String))

                    Validator(oData1, oData2)

                    If Status = False Then
                        cek_Check()
                        Exit Sub
                    End If

                    oApplication.BusinessDate = Me.BusinessDate
                    oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
                    oApplication.CustomerId = Me.CustomerID
                    oApplication.JenisPembiayaan = cboJenisPembiyaan.SelectedValue
                    oApplication.KegiatanUsaha = cboKegiatanUsaha.SelectedValue.ToString.Trim
                    oApplication.PolaTransaksi = Me.ProductID
                    oApplication.ProductID = Me.ProductID
                    oApplication.ProductOffID = Me.ProductOffID
                    oApplication.ContractStatus = "PRP"
                    oApplication.DefaultStatus = "NM"
                    oApplication.ApplicationStep = "APK"
                    oApplication.NumOfAssetUnit = 1
                    oApplication.InterestType = "FX"
                    oApplication.InstallmentScheme = "RF"
                    oApplication.ApplicationModule = "FACT"
                    oApplication.LiniBisnis = ""
                    oApplication.TipeLoan = "R"
                    oApplication.OTR = 0

                    oApplication.WayOfPayment = cboCaraBayar.SelectedValue.Trim
                    oApplication.AgreementDate = ""
                    oApplication.SurveyDate = ConvertDate(Me.BusinessDate.ToString("dd/MM/yyyy"))
                    oApplication.ApplicationSource = "D"
                    oApplication.PercentagePenalty = PenaltyRate
                    oApplication.AdminFee = 0
                    oApplication.FactoringPaymentMethod = cboPayment.SelectedValue.Trim

                    oApplication.AdditionalAdminFee = "0"
                    oApplication.StatusFiduciaFee = False
                    oApplication.NoApplicationData = False
                    oApplication.ProspectAppID = Me.ProspectAppID
                    oApplication.FiduciaFee = 0
                    oApplication.ProvisionFee = 0
                    oApplication.NotaryFee = 0
                    oApplication.SurveyFee = 0
                    oApplication.BBNFee = 0
                    oApplication.OtherFee = 0
                    oApplication.CrossDefaultApplicationId = ""
                    oApplication.Notes = txtAppNotes.Text
                    oApplication.NoPJJ = ""
                    oApplication.IsNST = "0"
                    oApplication.StepUpStepDownType = ""
                    oApplication.IsAdminFeeCredit = False
                    oApplication.IsProvisiCredit = False
                    oApplication.HakOpsi = False
                    oApplication.IsCOP = 0
                    oApplication.IsFleet = False
                    oApplication.Refinancing = False
                    oApplication.BiayaPeriksaBPKB = 0
                    oApplication.KodeDATI = hdfkabupaten.Value ' ucLookupKabupaten1.DatiID
                    oApplication.AdminFeeGross = 0
                    oApplication.RefinancingApplicationID = ""
                    oApplication.strConnection = GetConnectionString()
                    oApplication.FactoringPiutangDibiayai = ucPiutangDibiayai.Text
                    oApplication.FactoringInterest = ucBunga.Text 'ucViewCustomerFacility1.FasilitasInfo.EffectiveRate
                    oApplication.FactoringRetensi = ucRetensi.Text
                    oApplication.FactoringPPN = 0
                    oApplication.FactoringPencairanDate = IIf(txtTanggalPencairan.Text <> "", ConvertDate(txtTanggalPencairan.Text), "").ToString
                    oApplication.DtmUpd = Now()
                    oApplication.UsrUpd = Me.UserID
                    oApplication.FacilityNo = Me.FacilityNo
                    oApplication.EffectiveRate = ucBunga.Text
                    oApplication.AOID = cbocmo.SelectedValue.Trim


                    oMailingAddress.Address = ucMailingAddress.Address
                    oMailingAddress.RT = ucMailingAddress.RT
                    oMailingAddress.RW = ucMailingAddress.RW
                    oMailingAddress.Kecamatan = ucMailingAddress.Kecamatan
                    oMailingAddress.Kelurahan = ucMailingAddress.Kelurahan
                    oMailingAddress.ZipCode = ucMailingAddress.ZipCode
                    oMailingAddress.City = ucMailingAddress.City
                    oMailingAddress.AreaPhone1 = ucMailingAddress.AreaPhone1
                    oMailingAddress.AreaPhone2 = ucMailingAddress.AreaPhone2
                    oMailingAddress.AreaFax = ucMailingAddress.AreaFax
                    oMailingAddress.Phone1 = ucMailingAddress.Phone1
                    oMailingAddress.Phone2 = ucMailingAddress.Phone2
                    oMailingAddress.Fax = ucMailingAddress.Fax

                    Dim ErrorMessage As String = ""
                    Dim oReturn As New Parameter.Application

                    If Me.PageMode = "Edit" Then
                        oApplication.ApplicationID = Me.ApplicationID
                        oReturn = oController.ApplicationSaveEdit(oApplication,
                                                                  oRefPersonal,
                                                                  oRefAddress,
                                                                  oMailingAddress,
                                                                  oData1,
                                                                  oData2,
                                                                  oData3,
                                                                  oGuarantorPersonal,
                                                                  oGuarantorAddress)
                    Else
                        oReturn = oController.ApplicationSaveAdd(oApplication,
                                                                 oRefPersonal,
                                                                 oRefAddress,
                                                                 oMailingAddress,
                                                                 oData1,
                                                                 oData2,
                                                                 oData3,
                                                                 oGuarantorPersonal,
                                                                 oGuarantorAddress)

                        Me.ApplicationID = oReturn.ApplicationID
                        ucViewApplication1.CustomerID = Me.CustomerID
                        ucViewApplication1.ApplicationID = Me.ApplicationID
                        ucViewApplication1.bindData()
                        ucViewApplication1.initControls("OnNewApplication")
                        'createAssetDataCookies()
                        'ucApplicationTab1.ApplicationID = Me.ApplicationID
                        'ucApplicationTab1.setLink()
                        'Me.PageMode = "Edit"
                    End If

                    LabelValidationFalse()
                    If oReturn.Err <> "" Then
                        ShowMessage(lblMessage, ErrorMessage, True)
                    Else
                        ShowMessage(lblMessage, "Data saved!", False)
                        ucApplicationTab1.ApplicationID = Me.ApplicationID
                        ucApplicationTab1.setLink()
                        Me.PageMode = "Edit"
                    End If
                Else
                    ShowMessage(lblMessage, "Data Sudah Ada", True)
                End If
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub


    Protected Sub Validator(ByRef oData1 As DataTable, ByRef oData2 As DataTable)
        Dim PriorTo As String
        Dim Mandatory As String
        Dim Checked As Boolean
        Dim PromiseDate As ucDateCE
        Dim validCheck As New Label
        Dim Notes As String
        Dim MasterTCID As New TextBox
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1

        For intLoop = 0 To CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
            If intLoop <= DtgTC1count Then
                MasterTCID = CType(dtgTC.Items(intLoop).FindControl("MasterTCID"), TextBox)
                PriorTo = dtgTC.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(5).Text.Trim
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), ucDateCE)
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                Notes = CType(dtgTC.Items(intLoop).Cells(7).FindControl("txtTCNotes"), TextBox).Text

                If PromiseDate.Text <> "" Then
                    If CInt(ConvertDate(PromiseDate.Text)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = True
                        Status = False
                    End If
                End If

                If PriorTo = "APK" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.Text = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If

                oRow = oData1.NewRow
                oRow("MasterTCID") = MasterTCID.Text.Trim
                oRow("PriorTo") = PriorTo
                oRow("IsChecked") = IIf(Checked = True, "1", "0")
                oRow("IsMandatory") = IIf(Mandatory = "v", "1", "0")
                oRow("PromiseDate") = IIf(PromiseDate.Text <> "", ConvertDate(PromiseDate.Text), "")
                oRow("Notes") = Notes
                oData1.Rows.Add(oRow)
            End If

        Next

    End Sub
#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Me.PageMode = "Edit" Then
            Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
        Else
            Response.Redirect("Application.aspx")
        End If
    End Sub

    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        pnlDataAplikasi.Visible = True
        btnNext.Visible = False
        Me.ProductOffID = ucLookupProductOffering1.ProductOfferingID
        Me.ProductID = ucLookupProductOffering1.ProductID
        cboKegiatanUsaha.Enabled = False
        cboJenisPembiyaan.Enabled = False
        ucLookupProductOffering1.VisiblePnlLookupProdOff = False
        TC(Me.PageMode)
        TC2(Me.PageMode)
        LabelValidationFalse()
    End Sub

    Private Sub cboKegiatanUsaha_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboKegiatanUsaha.SelectedIndexChanged
        Dim cbo = CType(sender, DropDownList)
        Dim value = cbo.SelectedValue
        If cbo.SelectedIndex = 0 Then
            refresh_cboJenisPembiayaan("")
        Else
            ucLookupProductOffering1.KegiatanUsaha = value
            refresh_cboJenisPembiayaan(value)
        End If
    End Sub
    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"

        If key = String.Empty Then
            cboJenisPembiyaan.DataSource = def
            cboJenisPembiyaan.DataBind()
        Else
            Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
            cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        End If
        cboJenisPembiyaan.DataBind()
        cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue("IF"))
        ucLookupProductOffering1.JenisPembiayaan = cboJenisPembiyaan.SelectedValue

    End Sub
    Private Sub cboJenisPembiyaan_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboJenisPembiyaan.SelectedIndexChanged
        ucLookupProductOffering1.JenisPembiayaan = cboJenisPembiyaan.SelectedValue
    End Sub


End Class