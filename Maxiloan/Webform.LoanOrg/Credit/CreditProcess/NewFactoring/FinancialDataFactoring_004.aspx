﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FinancialDataFactoring_004.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.FinancialDataFactoring_004" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTabFactoring.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucSupplierEmployeeGrid.ascx"
    TagName="ucSupplierEmployeeGrid" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../Webform.UserController/ucFlagTransaksi.ascx" TagName="ucFlagTransaksi"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Financial Data</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Lookup.css" type="text/css" />
    
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../../../js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
        function calculateNPV(id) {
            var ntf = document.getElementById('lblNilaiNTFOTR').value;
            var totalBunga = document.getElementById('lblNilaiTotalBunga').value;
            
            if (id == "txtRefundBungaN_txtNumber") {  // ini kalo yang diisi nilai nya
                var nilAmount = document.getElementById('txtRefundBungaN_txtNumber').value;
                var result = (nilAmount / totalBunga) * 100;

                var r_formated = parseFloat(Math.floor(result * 100) / 100);
                if (!r_formated.length) {
                    document.getElementById('txtRefundBunga_txtNumber').value = number_format(r_formated, 2);
                } else {
                    document.getElementById('txtRefundBunga_txtNumber').value = r_formated;
                }
                document.getElementById('txtRefundBungaN_txtNumber').value = nilAmount;

            } else {  // ini kalo yang diisi persennya
                var refundRate = document.getElementById('txtRefundBunga_txtNumber').value;

                var npv = ((totalBunga / 100) * refundRate) ;

//                npv = Math.round(npv, 0) / 1000;
//                npv = Math.ceil(npv) * 1000;
                document.getElementById('txtRefundBungaN_txtNumber').value = number_format(npv, 0);
            }

        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }

        
        function hitungtxt(txtidA, txtidB, txtidS, toPersen) {
            var status = true;
            var A = $('#' + txtidA).val();
            var S;
            var N;
            var M;
            if ($('#' + txtidS).html() == "") {
                S = $('#' + txtidS).val();
            } else {
                S = $('#' + txtidS).html();
            }

            A = A.replace(/\s*,\s*/g, '');
            S = S.replace(/\s*,\s*/g, '');

            if (status == true) {
                if (toPersen == true) {
                    N = parseInt(A) / parseInt(S) * 100;
                    $('#' + txtidB).val(number_format(parseFloat((N * 10) / 10), 2));
                } else {
                    N = parseFloat(S) * (A / 100);
                    //$('#' + txtidA).val(number_format(A, 2));
                    $('#' + txtidB).val(number_format(N, 0));
                }
            } else {
                $('#' + txtidA).val(number_format(0, 2));
                $('#' + txtidB).val(number_format(0, 2));
            }

        }
        function hitPotongCair(){
            var uangMuka = document.getElementById('UctxtUangMukaMF_txtNumber').value;
            var adminFee = document.getElementById('uctxtAdminFeeMF_txtNumber').value;
            var fiducia = document.getElementById('UctxtBiayaFidusiaMF_txtNumber').value;
            var otherfee = document.getElementById('UctxtOtherFeeMf_txtNumber').value;
            var provisi = document.getElementById('UctxtBiayaProvisiMF_txtNumber').value;
            var asuransiTunai = document.getElementById('uctxtasuransitunaiMF_txtNumber').value;
            var angsuranPertama = document.getElementById('uctxtAngsuranPertamaMF_txtNumber').value;
            var uangmukakr = document.getElementById('UctxtUangMukaKaroseriMF_txtNumber').value;
            
            var total = parseFloat(uangMuka.replace(/\s*,\s*/g, ''))
                       + parseFloat(adminFee.replace(/\s*,\s*/g, ''))
                       + parseFloat(fiducia.replace(/\s*,\s*/g, ''))
                       + parseFloat(otherfee.replace(/\s*,\s*/g, ''))
                       + parseFloat(provisi.replace(/\s*,\s*/g, ''))
                       + parseFloat(asuransiTunai.replace(/\s*,\s*/g, ''))
                       + parseFloat(angsuranPertama.replace(/\s*,\s*/g, ''))
                       + parseFloat(uangmukakr.replace(/\s*,\s*/g, ''));

            var bayarPertama = document.getElementById('lblTotalBayarPertama').innerHTML;

            $('#txtpotongpencairan').val(number_format(total, 0));
            $('#txttotalBayarPertama').val(number_format(parseFloat(bayarPertama.replace(/\s*,\s*/g, '')) - total), 0);
        }

    </script>
</head>
<body>
    
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div>
                    <div class="form_left">
                        <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                    </div>
                    <div class="form_right">
                        <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                    </div>
                </div>
            </div>   
            <div class="form_box"> 
                <div class="form_left" style="background-color:#f9f9f9">
                    <h5>PREMI ASURANSI</h5>
                </div> 
                <div class="form_right" style="background-color:#f9f9f9">
                    <h5>BIAYA LAINNYA</h5>
                </div>  
            </div>
            <div class="form_box"> 
                <div class="form_left">
                    <label>Premi Asuransi Gross</label>
                    <asp:Label runat="server" ID="lblPremiAsuransiGross" CssClass="numberAlign2 regular_text"></asp:Label>
                </div> 
                <div class="form_right">
                    <label>Biaya Lainnya</label>
                    <asp:Label ID="lblOtherFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>  
            </div>
            <div class="form_box"> 
                <div class="form_left">
                    <label>Premi Asuransi Maskapai Nett</label>
                    <asp:Label runat="server" ID="lblPremiAsuransiNet" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                    <label>Refund Biaya Lainnya</label>
                    <uc4:ucnumberformat runat="server" id="txtRefundLainN" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitungtxt(this.id,'txtRefundLain_txtNumber','lblOtherFee',true)"></uc4:ucnumberformat>
                    <label class="label_auto numberAlign2"> % </label>
                    <uc4:ucnumberformat runat="server" id="txtRefundLain" TextCssClass="numberAlign2 smaller_text" OnClientChange="hitungtxt(this.id,'txtRefundLainN_txtNumber','lblOtherFee',false)"></uc4:ucnumberformat>
                </div>  
            </div>
            <div class="form_box" > 
                <div class="form_left">
                    <label>Diskon Premi</label>
                    <asp:Label runat="server" ID="lblSelisihPremi" CssClass="numberAlign2 regular_text" style="font-weight:bold"></asp:Label>
                </div> 
                <div class="form_right" style="background-color:#f9f9f9">
                    <h5>BIAYA PROVISI</h5>
                </div>  
            </div>
            <div class="form_box" > 
                <div class="form_left" visible="false">
                    <label>Refund Premi</label>
                    <uc4:ucnumberformat runat="server" id="txtRefundPremiN" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitungtxt(this.id,'txtRefundPremi_txtNumber','lblCreditProtectionGross',true)"></uc4:ucnumberformat>
                    <label class="label_auto numberAlign2"> % </label>
                    <uc4:ucnumberformat runat="server" id="txtRefundPremi" TextCssClass="numberAlign2 smaller_text" OnClientChange="hitungtxt(this.id,'txtRefundPremiN_txtNumber','lblCreditProtectionGross',false)"></uc4:ucnumberformat>                       
                </div> 
                <div class="form_right">
                    <label>Biaya Provisi</label>
                    <asp:Label ID="lblBiayaProvisi" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>  
            </div>
            <div class="form_box"> 
                <div class="form_left">
                    <label>Pendapatan / Pengeluaran Asuransi</label>
                    <uc4:ucnumberformat id="lblPendapatanPremiN" isReadOnly="true" runat="server" TextCssClass="numberAlign2 reguler_text" />
                    <div style="display:none">
                    <label class="label_auto numberAlign2"> % </label>
                    <uc4:ucnumberformat ID="lblPendapatanPremi" isReadOnly="true" runat="server" TextCssClass="numberAlign2 smaller_text" />
                    </div>
                </div> 
                <div class="form_right">
                    <label>Refund Biaya Provisi</label>
                    <uc4:ucnumberformat runat="server" id="txtRefundBiayaProvisiN" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitungtxt(this.id,'txtRefundBiayaProvisi_txtNumber','lblBiayaProvisi',true)"></uc4:ucnumberformat>
                    <label class="label_auto numberAlign2"> % </label>
                    <uc4:ucnumberformat runat="server" id="txtRefundBiayaProvisi" TextCssClass="numberAlign2 smaller_text" OnClientChange="hitungtxt(this.id,'txtRefundBiayaProvisiN_txtNumber','lblBiayaProvisi',false)"></uc4:ucnumberformat>
                </div>  
            </div>  
            <div class="form_box_header">
                <div class="form_left">
                    <h5>TOTAL BAYAR PERTAMA</h5>
                </div>
                <div class="form_right">
                    <h5>BAYAR KE BNI Multifinance</h5>
                </div>
            </div>  
            <div class="form_box">
               <div class="form_left">
                    <label>Biaya Administrasi</label>
                    <asp:Label ID="lblAdminFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    <asp:Label ID="lblIsAdminFeeCredit" runat="server" Visible="false"></asp:Label>
                </div>
                <div class="form_right">
                    <uc4:ucnumberformat runat="server" id="uctxtAdminFeeMF" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitPotongCair()"></uc4:ucnumberformat>
                </div>
            </div> 
            <div class="form_box">
               <div class="form_left">
                    <label>Biaya Lainnya</label>
                    <asp:Label ID="lblOtherFeeBayarPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                    <uc4:ucnumberformat runat="server" id="UctxtOtherFeeMf" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitPotongCair()"></uc4:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
               <div class="form_left">
                    <label>Biaya Provisi</label>
                    <asp:Label ID="lblBiayaProvisiBayarPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                    <uc4:ucnumberformat runat="server" id="UctxtBiayaProvisiMF" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitPotongCair()"></uc4:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
               <div class="form_left">
                    <label>Asuransi Tunai</label>
                    <asp:Label ID="lblAssetInsurance32" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                    <uc4:ucnumberformat runat="server" id="uctxtasuransitunaiMF" TextCssClass="numberAlign2 reguler_text" OnClientChange="hitPotongCair()"></uc4:ucnumberformat>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_left">
                    <label>Total</label>
                    <asp:Label ID="lblTotalBayarPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_right">
                     <label>BAYAR KE BNI Multifinance</label>
                    <asp:TextBox ID="txtpotongpencairan" runat="server" CssClass="numberAlign2 regular_text" Enabled="false">0</asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                 </div>
                <div class="form_right box_important">
                    <label>Total Bayar Pertama</label>
                    <asp:TextBox ID="txttotalBayarPertama" runat="server" CssClass="numberAlign2 regular_text" Enabled="false">0</asp:TextBox>
                </div>
             </div>         

            <div class="form_button">
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <asp:Button ID="btnBack" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div> 

            <asp:Panel runat="server" ID="pnlPencairan">
                <div class="form_box_title">
                    <div class="form_left">
                        <h5>
                            PERHITUNGAN BUNGA NET</h5>
                    </div>
                    <div class="form_right">
                        <h5>
                            TOTAL PENCAIRAN</h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_left">
                        <asp:GridView runat="server" ID="gvBungaNet" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc1" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Bunga Nett</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Nilai" DataFormatString="{0:N0}" HeaderStyle-CssClass="th_right"
                                    ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft" />
                                <asp:TemplateField  HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblRate" Text='<%# math.round(Container.DataItem("Rate"),2) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalRate"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="BungaNett" />
                            </Columns>
                        </asp:GridView>
                        <asp:GridView runat="server" ID="gvNetRateStatus" AllowPaging="false" AutoGenerateColumns="false"
                            CssClass="grid_general" ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <label>
                                            Status Rate</label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RateTypeResult" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="form_right">
                        <asp:GridView runat="server" ID="gvPencairan" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc2" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Total Pencairan</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                                    FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblNilai" Text='<%# formatnumber(Container.DataItem("Nilai"),0) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalPencairan"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FlagCustomer" Visible="false" />
                                <asp:BoundField DataField="FlagSupplier" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSave">
            <div class="form_button">       
                 <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green"
                    CausesValidation="True"></asp:Button>
                 <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>    
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    
    </form>
</body>
</html>
