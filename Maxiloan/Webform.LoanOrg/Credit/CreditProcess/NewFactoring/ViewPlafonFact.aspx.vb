﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewPlafonFact
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucViewApplication1 As ucViewApplicationMK
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabFactoring
    Protected WithEvents ucViewCustomerFacility1 As ucViewCustomerFacility

    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oController As New UcAgreementListController
    Private oCustomClass As New Parameter.AccMntBase

#Region "Property"
    Property Type() As String
        Get
            Return ViewState("Type").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Property ApplicationReq() As String
        Get
            Return ViewState("ApplicationReq").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationReq") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationReq = " a.ApplicationID ='" & Request("ApplicationID").ToString & "'"

            Mode = Request("Page")
            ucApplicationTab1.ApplicationID = Request("ApplicationID")
            ucApplicationTab1.selectedTab("Plafon")
            ucApplicationTab1.setLink()

            With ucViewApplication1
                .ApplicationID = Request("ApplicationID")
                .initControls("OnNewApplication")
                .bindData()
                Me.CustomerID = .CustomerID
                Me.CustomerName = .CustomerName
                Me.FacilityNo = .NoFasilitas
            End With
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            loadDataFasilitas()

            BindAgreement()

            If Request("Application") <> "" Then
                Dim script As String
                script = "<script language=""JavaScript"">" & vbCrLf
                script &= "alert('Your Application Form No. is " & Request("Application").ToString.Trim & "');" & vbCrLf
                script &= "</script>"
                Response.Write(script)
            End If
        End If

    End Sub

    Private Sub loadDataFasilitas()
        With ucViewCustomerFacility1
            .FacilityNo = Me.FacilityNo
            .CustomerID = Me.CustomerID
            .LoadCustomerFacility()
        End With
    End Sub

    Public Sub BindAgreement()
        Dim DvAgreementList As New DataView

        With ocustomclass
            .strConnection = GetConnectionString()
            .NoFasilitas = Me.FacilityNo
            .SortBy = SortBy
        End With

        DvAgreementList = oController.UcAgreementListFactoring(oCustomClass).DefaultView
        DvAgreementList.Sort = Me.SortBy
        DtgList.DataSource = DvAgreementList
        DtgList.DataBind()

    End Sub

End Class