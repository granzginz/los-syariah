﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class ViewChangeDueDate
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication2 As ucViewApplicationMK
    Protected WithEvents ucViewCustomerDetail2 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabFactoring
    Protected WithEvents ucInvoiceAmount As ucNumberFormat
#End Region

#Region "Constanta"
    Dim style As String = "ACCACQ"
    Private oApplication As New Parameter.Application
    Private oFactoringInvoice As New Parameter.FactoringInvoice
    Private m_ControllerApp As New ApplicationController
    Private m_controller As New FactoringInvoiceController
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo")
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Property ApplicationData() As DataTable
        Get
            Return ViewState("ApplicationData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ApplicationData") = Value
        End Set
    End Property
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property

#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            hdfApplicationID.Value = Me.ApplicationID
            With ucViewApplication2
                .ApplicationID = Me.ApplicationID
                .bindData()

                'modify Nofi 13012020 
                'If .IsAppTimeLimitReached Then
                '    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                'End If

                .initControls("OnNewApplication")

                Me.BranchID = .BranchID
                Me.CustomerID = .CustomerID
                Me.ProductOfferingID = .ProductOfferingID
                Me.SupplierID = .SupplierID
            End With

            ucViewCustomerDetail2.CustomerID = Me.CustomerID
            ucViewCustomerDetail2.bindCustomerDetail()
            Mode = Request("Page")

            pnlListInvoice.Visible = True
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("ChangeDueDate")
            ucApplicationTab1.setLink()

            Me.InvoiceSeqNo = 0
            Me.InvoiceSeqNo = Request("invseqno")
            BindDataEdit()

        End If

    End Sub
#End Region

    Private Sub BindDataEdit()
        Dim oData As New DataTable
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
            ApplicationData = oData
        End If
        Dim oRow As DataRow = ApplicationData.Rows(0)

        oFactoringInvoice.strConnection = GetConnectionString()
        oFactoringInvoice.BranchId = Me.BranchID
        oFactoringInvoice.ApplicationID = Me.ApplicationID

        oFactoringInvoice = m_controller.GetInvoiceChangeDueDateList(oFactoringInvoice)

        ItemData = oFactoringInvoice.Listdata

        dtgInvoice.DataSource = ItemData.DefaultView
        dtgInvoice.DataBind()

        lblSupplierName.Text = oRow("SupplierName").ToString
        lblTanggalPencairan.Text = Format(IIf(String.IsNullOrEmpty(oRow("FactoringPencairanDate").ToString), "", oRow("FactoringPencairanDate")), "dd/MM/yyyy")

    End Sub




End Class