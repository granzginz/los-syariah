﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class InvoiceData
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabFactoring
	Protected WithEvents ucInvoiceAmount, UcRetensiAmount As ucNumberFormat
	Protected WithEvents ucRentensiPercent, lblTagihanYangDibiayai2, lblTagihanYangDibiayaiAmount2 As ucNumberFormat
	Protected WithEvents ucViewCustomerFacility1 As ucViewCustomerFacility
#End Region

#Region "Constanta"
    Dim style As String = "ACCACQ"
    Private oApplication As New Parameter.Application
    Private oFactoringInvoice As New Parameter.FactoringInvoice
    Private m_ControllerApp As New ApplicationController
    Private m_controller As New FactoringInvoiceController
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo")
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property Retensi() As Decimal
        Get
            Return ViewState("Retensi")
        End Get
        Set(ByVal Value As Decimal)
            ViewState("Retensi") = Value
        End Set
    End Property
	Property CustomerID() As String
		Get
			Return ViewState("CustomerID").ToString
		End Get
		Set(ByVal Value As String)
			ViewState("CustomerID") = Value
		End Set
	End Property
	Property FacilityMaturityDate() As Date
		Get
			Return ViewState("FacilityMaturityDate")
		End Get
		Set(ByVal Value As Date)
			ViewState("FacilityMaturityDate") = Value
		End Set
	End Property
	Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Property ApplicationData() As DataTable
        Get
            Return ViewState("ApplicationData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ApplicationData") = Value
        End Set
    End Property
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property

#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("appid")
            hdfApplicationID.Value = Me.ApplicationID
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If

                .initControls("OnNewApplication")

                Me.BranchID = .BranchID
                Me.CustomerID = .CustomerID
                Me.ProductOfferingID = .ProductOfferingID
                Me.SupplierID = .SupplierID
            End With

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            Mode = Request("Page")

            pnlInputInvoice.Visible = False
            pnlListInvoice.Visible = False
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Invoice")
            ucApplicationTab1.setLink()

            Me.InvoiceSeqNo = 0
            Me.InvoiceSeqNo = Request("invseqno")
            BindDataEdit()

            Select Case Mode
                Case "Edit"
                    pnlListInvoice.Visible = True
                Case "Detail"
                    hdfInvoiceSeqNo.Value = Request("invseqno")
                    pnlInputInvoice.Visible = True
                    ClearInvoiceForm()
                    If (Me.InvoiceSeqNo > 0) Then
                        BindInvoiceForm()
                    End If
            End Select
        End If

    End Sub
#End Region

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub

    Private Sub BindDataEdit()
        Dim oData As New DataTable
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
            ApplicationData = oData
        End If
        Dim oRow As DataRow = ApplicationData.Rows(0)

        oFactoringInvoice.strConnection = GetConnectionString()
        oFactoringInvoice.BranchId = Me.BranchID
        oFactoringInvoice.ApplicationID = Me.ApplicationID


        oFactoringInvoice = m_controller.GetInvoiceList(oFactoringInvoice)

        ItemData = oFactoringInvoice.Listdata

        dtgInvoice.DataSource = ItemData.DefaultView
        dtgInvoice.DataBind()

        lblTanggalPencairan.Text = Format(IIf(String.IsNullOrEmpty(oRow("FactoringPencairanDate").ToString), "", oRow("FactoringPencairanDate")), "dd/MM/yyyy")
        Me.FacilityNo = oRow("NoFasilitas").ToString.Trim
        loadDataFasilitas()
    End Sub

    Private Sub loadDataFasilitas()
        With ucViewCustomerFacility1
            .FacilityNo = Me.FacilityNo
			.CustomerID = Me.CustomerID
			.LoadCustomerFacility()
			Me.FacilityMaturityDate = .FacilityMaturityDate
		End With
    End Sub


    Private Sub BindInvoiceForm()
        Dim oRow As DataRow = ApplicationData.Rows(0)
        oFactoringInvoice.strConnection = GetConnectionString()
        oFactoringInvoice.BranchId = Me.BranchID
        oFactoringInvoice.ApplicationID = Me.ApplicationID
        oFactoringInvoice.InvoiceSeqNo = Me.InvoiceSeqNo
        oFactoringInvoice = m_controller.GetInvoiceDetail(oFactoringInvoice)

        hdfInvoiceSeqNo.Value = oFactoringInvoice.InvoiceSeqNo
        txtInvoiceNo.Text = oFactoringInvoice.InvoiceNo
        txtInvoiceDate.Text = Format(DateTime.ParseExact(oFactoringInvoice.InvoiceDate, "yyyyMMdd", Nothing), "dd/MM/yyyy")
        txtInvoiceDueDate.Text = Format(DateTime.ParseExact(oFactoringInvoice.InvoiceDueDate, "yyyyMMdd", Nothing), "dd/MM/yyyy")
        ucInvoiceAmount.Text = FormatNumber(oFactoringInvoice.InvoiceAmount, 0)
        lblRentensi.Text = FormatNumber(oFactoringInvoice.RetensiAmount, 0)
        hdfRentensiPercent.Value = oRow("FactoringRetensi").ToString
        Retensi = oRow("FactoringRetensi")
		ucRentensiPercent.Text = FormatNumber(oRow("FactoringRetensi").ToString, 6) & " %"
		'lblTagihanYangDibiayai.Text = FormatNumber(oFactoringInvoice.TotalPembiayaan, 0)
		lblTagihanYangDibiayai2.Text = FormatNumber(oFactoringInvoice.TotalPembiayaan, 0)
		lblBunga.Text = FormatNumber(oFactoringInvoice.InterestAmount, 0)
        hdfBungaPercent.Value = oRow("FactoringInterest").ToString
        lblBungaPercent.Text = FormatNumber(oRow("FactoringInterest").ToString, 2) & " %"

        Dim tglPencairan As Date = oRow("FactoringPencairanDate")
        Dim invoiceDueDate As Date = DateTime.ParseExact(oFactoringInvoice.InvoiceDueDate, "yyyyMMdd", Nothing)
        Dim jangkaWaktu As Long = DateDiff(DateInterval.Day, tglPencairan, invoiceDueDate)

        lblJangkaWaktu.Text = jangkaWaktu.ToString
        hdfTanggalPencairan.Value = Format(oRow("FactoringPencairanDate"), "dd/MM/yyyy")
    End Sub

    Private Sub ClearInvoiceForm()
        Dim oRow As DataRow = ApplicationData.Rows(0)
        hdfInvoiceSeqNo.Value = ""
        txtInvoiceNo.Text = ""
        txtInvoiceDate.Text = ""
        txtInvoiceDueDate.Text = ""
        ucInvoiceAmount.Text = ""
        lblRentensi.Text = ""
        hdfRentensiPercent.Value = oRow("FactoringRetensi").ToString
		ucRentensiPercent.Text = FormatNumber(oRow("FactoringRetensi").ToString, 6)

		'lblTagihanYangDibiayai.Text = ""
		lblTagihanYangDibiayai2.Text = FormatNumber(oRow("FactoringPiutangDibiayai").ToString, 6) '& " %"
		lblBunga.Text = ""
        hdfBungaPercent.Value = oRow("FactoringInterest").ToString
		lblBungaPercent.Text = FormatNumber(oRow("FactoringInterest").ToString, 2) & " %"
		lblJangkaWaktu.Text = ""
        hdfTanggalPencairan.Value = Format(oRow("FactoringPencairanDate"), "dd/MM/yyyy")
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Response.Redirect("InvoiceData.aspx?appid=" & Me.ApplicationID & "&page=Detail")
    End Sub

    Private Sub btnCancelInvoice_Click(sender As Object, e As EventArgs) Handles btnCancelInvoice.Click
        Response.Redirect("InvoiceData.aspx?appid=" & Me.ApplicationID & "&page=Edit")
    End Sub

    Private Sub btnSaveInvoice_Click(sender As Object, e As EventArgs) Handles btnSaveInvoice.Click
        Dim oRow As DataRow = ApplicationData.Rows(0)
        Dim oItem As New Parameter.FactoringInvoice
        If hdfInvoiceSeqNo.Value = "" Then hdfInvoiceSeqNo.Value = "0"
        oItem.ApplicationID = Me.ApplicationID
        oItem.BranchId = Me.BranchID
        oItem.InvoiceSeqNo = CDec(hdfInvoiceSeqNo.Value)
		oItem.InvoiceNo = txtInvoiceNo.Text

		oItem.InvoiceAmount = CDec(ucInvoiceAmount.Text)
        oItem.Retensi = CDec(ucRentensiPercent.Text)
		'oItem.RetensiAmount = oItem.Retensi * oItem.InvoiceAmount / 100
		oItem.RetensiAmount = CDec(UcRetensiAmount.Text)
		oItem.InvoiceDate = ConvertDate2(txtInvoiceDate.Text) 'IIf(txtInvoiceDate.Text <> "", ConvertDate2(txtInvoiceDate.Text), "").ToString
		oItem.InvoiceDueDate = ConvertDate2(txtInvoiceDueDate.Text) 'IIf(txtInvoiceDueDate.Text <> "", ConvertDate2(txtInvoiceDueDate.Text), "").ToString



        Dim tglPencairan As Date = oRow("FactoringPencairanDate")
        Dim invoiceDueDate As Date = oItem.InvoiceDueDate 'DateTime.ParseExact(txtInvoiceDueDate.Text, "yyyyMMdd", Nothing)
        Dim jangkaWaktu As Long = DateDiff(DateInterval.Day, tglPencairan, invoiceDueDate)

        Dim bungaPercent As Decimal = CDec(oRow("FactoringInterest"))
        oItem.JangkaWaktu = jangkaWaktu 'CInt(lblJangkaWaktu.Text)
		'oItem.TotalPembiayaan = oItem.InvoiceAmount - oItem.RetensiAmount
		'oItem.TotalPembiayaan = FormatNumber((oItem.InvoiceAmount - oItem.RetensiAmount), 2)
		oItem.TotalPembiayaan = CDbl(lblTagihanYangDibiayaiAmount2.text)
		'oItem.InterestAmount = (oItem.TotalPembiayaan * bungaPercent / 100) * jangkaWaktu / 360
		oItem.InterestAmount = CDbl(hdfBunga.Value)
		oItem.UsrUpd = Me.UserID
        If ValidateSaveInvoice(oItem) = True Then
            Try
                oItem.strConnection = GetConnectionString()
                If oItem.InvoiceSeqNo > 0 Then
                    m_controller.InvoiceSaveEdit(oItem)
                Else
                    m_controller.InvoiceSaveAdd(oItem)
                End If
                Response.Redirect("InvoiceData.aspx?appid=" & Me.ApplicationID & "&page=Edit")
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

    Private Function ValidateSaveInvoice(item As Parameter.FactoringInvoice) As Boolean
        If item.InvoiceNo.Trim = "" Then
            ShowMessage(lblMessage, "Please fill invoice number", True)
            Return False
        End If
        If item.InvoiceDate > item.InvoiceDueDate Then
            ShowMessage(lblMessage, "Invoice date and invoice due date range not valid", True)
            Return False
        End If
        If item.InvoiceAmount < 1 Then
            ShowMessage(lblMessage, "Invoice amount is not valid", True)
            Return False
        End If
        If item.JangkaWaktu < 1 Then
            ShowMessage(lblMessage, "Jangka waktu is not valid", True)
            Return False
        End If
		If (item.Retensi < Retensi) Then
			ShowMessage(lblMessage, "Melewati nilai retensi minimal", True)
			Return False
		End If
		If item.InvoiceDueDate > Me.FacilityMaturityDate Then
			ShowMessage(lblMessage, "Invoice Due Date melebihi Tanggal Akhir Fasilitas", True)
			Return False
		End If
		Return True
    End Function

    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim cmd As String() = Split(e.CommandArgument.ToString, ",")
        If e.CommandName.ToLower = "delete" Then
            Me.ApplicationID = cmd(0)
            Me.InvoiceSeqNo = cmd(1)
            Dim oItem As New Parameter.FactoringInvoice
            Try
                oItem.strConnection = GetConnectionString()
                oItem.ApplicationID = Me.ApplicationID
                oItem.InvoiceSeqNo = Me.InvoiceSeqNo
                m_controller.InvoiceDelete(oItem)
                Response.Redirect("InvoiceData.aspx?appid=" & Me.ApplicationID & "&page=Edit")
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try

            Response.Redirect("InvoiceData.aspx?appid=" & Me.ApplicationID & "&page=Edit")
        End If
        If e.CommandName.ToLower = "editinvoice" Then
            Me.ApplicationID = cmd(0)
            Me.InvoiceSeqNo = cmd(1)
            Response.Redirect("InvoiceData.aspx?appid=" & Me.ApplicationID & "&page=Detail" & "&invseqno=" & Me.InvoiceSeqNo.ToString)
        End If
    End Sub

	Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
		BindDataEdit()
		Dim amount As Double = ucViewCustomerFacility1.FasilitasInfo.AvailableAmount
		Dim totalinvoice As Double = 0

		For Each row As DataRow In ItemData.Rows
			totalinvoice = totalinvoice + row("TotalPembiayaan")
		Next

		If (totalinvoice > ucViewCustomerFacility1.FasilitasInfo.AvailableAmount) Then
            ShowMessage(lblMessage, "Total pembiayaan melebihi sisa plafond yang tersedia", True)
            Exit Sub
        End If

        If ItemData.Rows.Count < 1 Then
            ShowMessage(lblMessage, "Please insert invoice data", True)
        Else
            Dim oCustomclass As New Parameter.Application

            With oCustomclass
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
            End With
            Try
                m_controller.InvoiceSaveApplication(oCustomclass)
                Response.Redirect("FinancialFactoringData.aspx?appid=" & Me.ApplicationID)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

End Class