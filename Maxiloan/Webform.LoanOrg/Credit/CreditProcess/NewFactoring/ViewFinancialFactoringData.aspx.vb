﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class ViewFinancialFactoringData
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplicationMK
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabFactoring
    Protected WithEvents ucViewCustomerFacility1 As ucViewCustomerFacility
#End Region

#Region "Constanta"
    Dim style As String = "ACCACQ"
    Private oApplication As New Parameter.Application
    Private oFactoringInvoice As New Parameter.FactoringInvoice
    Private m_ControllerApp As New ApplicationController
    Private m_controller As New FactoringInvoiceController
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo")
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Property ApplicationData() As DataTable
        Get
            Return ViewState("ApplicationData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ApplicationData") = Value
        End Set
    End Property
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property
    Property MetodePembayaran() As String
        Get
            Return ViewState("MetodePembayaran").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("MetodePembayaran") = Value
        End Set
    End Property
    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            hdfApplicationID.Value = Me.ApplicationID
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()
                .initControls("OnNewApplication")

                Me.BranchID = .BranchID
                Me.CustomerID = .CustomerID
                Me.ProductOfferingID = .ProductOfferingID
                Me.SupplierID = .SupplierID
            End With

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            Mode = Request("Page")

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial")
            ucApplicationTab1.setLink()

            Me.InvoiceSeqNo = 0
            Me.InvoiceSeqNo = Request("invseqno")
            BindDataEdit()

        End If
    End Sub
#End Region


    Public Function getPPNPercent() As Decimal
        Dim generalSet As New Parameter.GeneralSetting
        Dim conGeneral As New GeneralSettingController

        With generalSet
            .strConnection = GetConnectionString()
            .GSID = "PPNFACTORING"
        End With

        generalSet = conGeneral.GetGeneralSettingByID(generalSet)

        If generalSet.ListData.Rows.Count > 0 Then
            Return CDec(generalSet.ListData.Rows(0).Item("GSValue").ToString.Trim)
        Else
            Return 0
        End If
    End Function

    Private Sub BindDataEdit()
        Dim oData, fData As New DataTable
        Dim oApp = New Parameter.FactoringInvoice
        Dim HandlingFee, AdminFee, NotaryFee, ProvisionFee, SurveyFee, FactoringDiscountCharges, BiayaPolis, InsuranceFee As Decimal
        Dim MaskAssBranchName As String
        Dim InsuranceMasterHPPRateID As String
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication.ApplicationID = Me.ApplicationID
        oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
            ApplicationData = oData
        End If
        Dim oRow As DataRow = ApplicationData.Rows(0)

        Me.FacilityNo = oRow("NoFasilitas").ToString
        loadDataFasilitas()

        oFactoringInvoice.strConnection = GetConnectionString()
        oFactoringInvoice.BranchId = Me.BranchID
        oFactoringInvoice.ApplicationID = Me.ApplicationID

        oFactoringInvoice = m_controller.GetInvoiceList(oFactoringInvoice)

        ItemData = oFactoringInvoice.Listdata

        lblSupplierName.Text = oRow("SupplierName").ToString
        lblTanggalPencairan.Text = Format(IIf(String.IsNullOrEmpty(oRow("FactoringPencairanDate").ToString), "", oRow("FactoringPencairanDate")), "dd/MM/yyyy")

        Dim totalPembiayaan As Object = ItemData.Compute("SUM(TotalPembiayaan)", String.Empty)
        Dim totalInterest As Object = ItemData.Compute("SUM(InterestAmount)", String.Empty)
        Dim ppnPercent As Decimal = getPPNPercent()
        Dim ppn, totalbiaya As Decimal
        Dim totalInvoice As Object = ItemData.Rows.Count

        hdfTotalInvoice.Value = Convert.ToDecimal(totalPembiayaan).ToString
        hdfTotalBunga.Value = Convert.ToDecimal(totalInterest).ToString
        hdfPPN.Value = ppnPercent.ToString

        lblTotalInvoice.Text = FormatNumber(Convert.ToDecimal(totalPembiayaan), 0)
        lblTotalBunga.Text = FormatNumber(Convert.ToDecimal(totalInterest), 0)
        lblInvoiceCount.Text = totalInvoice.ToString

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.ApplicationID = Me.ApplicationID
        oApp = m_controller.GetFactoringFee(oApplication)
        fData = oApp.Listdata
        If (fData.Rows.Count > 0) Then
            Dim frow As DataRow = fData.Rows(0)
            HandlingFee = frow("HandlingFee")
            NotaryFee = frow("NotaryFee")
            AdminFee = frow("AdminFee")
            ProvisionFee = frow("ProvisionFee")
            SurveyFee = frow("SurveyFee")
            BiayaPolis = frow("BiayaPolis")
            InsuranceFee = frow("InsuranceFee")
            MaskAssBranchName = frow("MaskAssBranchName")
            FactoringDiscountCharges = frow("FactoringDiscountCharges")
            InsuranceMasterHPPRateID = frow("InsuranceMasterHPPRateID")

            UcHandlingFee.Text = FormatNumber(HandlingFee, 0)
            UcBiayaAdmin.Text = FormatNumber(AdminFee, 0)
            UcBiayaProvisi.Text = FormatNumber(ProvisionFee, 0)
            UcBiayaNotaris.Text = FormatNumber(NotaryFee, 0)
            UcBiayaSurvey.Text = FormatNumber(SurveyFee, 0)
            UcDiscountCharges.Text = FormatNumber(FactoringDiscountCharges, 0)
            LblFocusMaskAssIDName.Text = MaskAssBranchName.ToString
            lblAsuransiKredit.Text = FormatNumber(InsuranceFee, 0)
            lblBiayaPolis.Text = FormatNumber(BiayaPolis, 0)
            lblRateTypeInsuranceDesc.Text = InsuranceMasterHPPRateID.ToString

            'totalbiaya = HandlingFee + NotaryFee + AdminFee + ProvisionFee + SurveyFee - FactoringDiscountCharges
            totalbiaya = HandlingFee + NotaryFee + AdminFee + ProvisionFee + SurveyFee + InsuranceFee + BiayaPolis - FactoringDiscountCharges
        Else
            UcHandlingFee.Text = 0
            UcBiayaAdmin.Text = 0
            UcBiayaProvisi.Text = 0
            UcBiayaNotaris.Text = 0
            UcBiayaSurvey.Text = 0
            UcDiscountCharges.Text = 0
            LblFocusMaskAssIDName.Text = ""
            lblAsuransiKredit.Text = 0
            lblBiayaPolis.Text = 0
            lblRateTypeInsuranceDesc.Text = ""

            totalbiaya = 0
        End If
        ppn = (totalInterest + totalbiaya) * ppnPercent / 100
        lblTotalBiaya.Text = FormatNumber(totalbiaya, 0)
        lblPPN.Text = FormatNumber(ppn, 0)
        'lblJumlahPencairan.Text = FormatNumber(totalPembiayaan - totalInterest - ppn - totalbiaya, 0)

        MetodePembayaran = oRow("FactoringPaymentMethod").ToString
        If (MetodePembayaran = "D") Then
            lblJumlahPencairan.Text = FormatNumber(totalPembiayaan - totalInterest, 0)
        Else
            lblJumlahPencairan.Text = FormatNumber(totalPembiayaan, 0)
        End If
    End Sub

    Private Sub loadDataFasilitas()
        With ucViewCustomerFacility1
            .FacilityNo = Me.FacilityNo
            .CustomerID = Me.CustomerID
            .LoadCustomerFacility()
        End With
    End Sub
End Class