﻿
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Public Class InvoiceDeductions
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New InvoiceController
    Protected WithEvents txtInvInvoiceAmount As ucNumberFormat
    Protected WithEvents txtjmlPemotonganNew As ucNumberFormat
    Protected WithEvents txtFirstInstallmentDate As ucDateCE
    Protected WithEvents UcBankAccount As UcBankAccount
    Private Property InvoiceSupp() As Parameter.InvoiceSupp
        Get
            Return CType(ViewState("InvoiceSupp"), Parameter.InvoiceSupp)
        End Get
        Set(ByVal Value As Parameter.InvoiceSupp)
            ViewState("InvoiceSupp") = Value
        End Set
    End Property
    Private Property InvoiceSuppDeductions() As IList(Of Parameter.InvoiceSuppDeduction)
        Get
            Return CType(ViewState("InvoiceSuppDeduction"), IList(Of Parameter.InvoiceSuppDeduction))
        End Get
        Set(ByVal Value As IList(Of Parameter.InvoiceSuppDeduction))
            ViewState("InvoiceSuppDeduction") = Value
        End Set
    End Property
    Private Property PotonganPencairan() As IList(Of Parameter.CommonValueText)
        Get
            Return CType(ViewState("("), IList(Of Parameter.CommonValueText))
        End Get
        Set(ByVal Value As IList(Of Parameter.CommonValueText))
            ViewState("(") = Value
        End Set
    End Property

    Private Property SumAmountDeduction() As Double
        Get
            Return CType(ViewState("SumAmountDeduction"), Double)
        End Get
        Set(value As Double)
            ViewState("SumAmountDeduction") = value
        End Set
    End Property

    Function LinkToStringBuild(ByVal iD As String, openWin As String, ByVal style As String) As String
        Return String.Format("javascript:{0}('{1}' , '{2}')", openWin, style, iD)
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InvoiceSupp = CType(Session("INVOCICESUPP"), Parameter.InvoiceSupp)
            PotonganPencairan = m_controller.LoadCombo(GetConnectionString(), "POTONGPENCAIRAN")

            InvoiceSuppDeductions = New List(Of Parameter.InvoiceSuppDeduction)
            'If InvoiceSupp.NilPelunasan > 0 Then
            '    Dim item = New Parameter.InvoiceSuppDeduction With {.Seq = 1, .DeductionID = "POT04", .DeductionDescription = "Pelunasan Kontrak Baru", .DeductionAmount = CDec(InvoiceSupp.NilPelunasan.ToString)}
            '    InvoiceSuppDeductions.Add(item)
            'End If
            BindPotongan()


            InvoiceSupp.InvoiceSuppDeductions = InvoiceSuppDeductions
            initView()
        End If
    End Sub
    Sub BindPotongan()
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Application
        Dim m_controller As New ApplicationController
        pnlList.Visible = True

        oCustomClass.ApplicationID = InvoiceSupp.ApplicationID
        oCustomClass.BranchId = InvoiceSupp.BranchID
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetApplicationDeduction(oCustomClass)
        dtEntity = oCustomClass.ListData
        SumAmountDeduction = 0
        If oCustomClass.ListData.Rows.Count > 0 Then
            For i As Integer = 0 To oCustomClass.ListData.Rows.Count - 1
                SumAmountDeduction += CDec(oCustomClass.ListData.Rows(i).Item("Amount"))
                Dim item = New Parameter.InvoiceSuppDeduction With {.Seq = i + 1, .DeductionID = oCustomClass.ListData.Rows(i).Item("DeductionID").ToString, .DeductionDescription = oCustomClass.ListData.Rows(i).Item("DeductionDescription").ToString, .ApplicationIDTo = oCustomClass.ListData.Rows(i).Item("ApplicationIDTo").ToString, .DeductionAmount = CDec(oCustomClass.ListData.Rows(i).Item("Amount").ToString)}
                InvoiceSuppDeductions.Add(item)
            Next

        End If

        dtgDeduction.DataSource = InvoiceSuppDeductions
        dtgDeduction.DataBind()

    End Sub

    Sub initView()

        txtFirstInstallmentDate.IsRequired = True

        hySupplierName.Text = InvoiceSupp.SupplierName
        hySupplierName.NavigateUrl = LinkToStringBuild(InvoiceSupp.SupplierID, "OpenWinSupplier", "AccAcq")


        hyNoKontrak.Text = InvoiceSupp.AgreementNo
        hyNoKontrak.NavigateUrl = LinkToStringBuild(InvoiceSupp.ApplicationID, "OpenAgreementNo", "AccAcq")

        hyNamaKonsumen.Text = InvoiceSupp.CustomerName
        hyNamaKonsumen.NavigateUrl = LinkToStringBuild(InvoiceSupp.CustomerID, "OpenCustomer", "AccAcq")

        lblTglInvoice.Text = InvoiceSupp.InvoiceDate.ToString("dd/MM/yyyy") 'String.Format("{0:d}", InvoiceSupp.InvoiceDate)
        lblNilaiInvoice.Text = String.Format("{0:#,##0}", InvoiceSupp.InvAmountOriginal)
        lblNoInvoice.Text = CType(IIf(InvoiceSupp.InvoiceNo = String.Empty, "AutoNumber", InvoiceSupp.InvoiceNo), String)
        lblJumPotongan.Text = FormatNumber(SumAmountDeduction, 0) ' FormatNumber(InvoiceSupp.NilPelunasan, 0)
        lblTglRencanaCair.Text = InvoiceSupp.APDueDate.ToString("dd/MM/yyyy") 'String.Format("{0:d}", InvoiceSupp.APDueDate)
        lblJumlahPencairan.Text = String.Format("{0:#,##0}", InvoiceSupp.InvAmountOriginal - SumAmountDeduction)

        'ddlRekBank.DataSource = m_controller.LoadCombo(GetConnectionString(), "SUPPBANK", InvoiceSupp.SupplierID)
        'ddlRekBank.DataTextField = "Text"
        'ddlRekBank.DataValueField = "Value"
        'ddlRekBank.DataBind()

        dtgDeduction.DataSource = InvoiceSuppDeductions
        dtgDeduction.DataBind()

        lblRefund.Text = FormatNumber(InvoiceSupp.Refound, 0)
        pnlAddDeduction.Visible = False



        lblTotInv.Text = String.Format("{0:#,##0}", InvoiceSupp.InvAmountOriginal - SumAmountDeduction + InvoiceSupp.Refound)

        Dim objcommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objcommand.Connection = objconnection
        objcommand.CommandType = CommandType.StoredProcedure
        objcommand.CommandText = "spGetSupplierAccount"
        objcommand.Parameters.Add("@SupplierID", SqlDbType.VarChar, 10).Value = InvoiceSupp.SupplierID
        objread = objcommand.ExecuteReader



        With UcBankAccount
            .RequiredBankName()
            .ValidatorTrue()
            .Style = "Marketing"
            .BindBankAccount()

            If objread.Read Then
                .AccountNo = CStr(objread("SupplierAccountNO")).Trim
                .AccountName = CStr(objread("SupplierAccountName")).Trim
                .BankID = CStr(objread("SupplierBankID")).Trim
                .BankCodeBank = CStr(objread("SupplierBankID")).Trim
                .BankBranchName = CStr(objread("SupplierBankBranch")).Trim
                .BankBranchId = CStr(objread("SupplierBankBranchID")).Trim
                .BankCodeCabang = CStr(objread("SupplierBankBranchID")).Trim
                .City = CStr(objread("City")).Trim
            End If
        End With

        If objconnection.State = ConnectionState.Closed Then objconnection.Open()
        objconnection.Dispose()

    End Sub

    Private Sub btnAddDeduction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddDeduction.Click
        pnlAddDeduction.Visible = True
        divAddDeduc.Visible = False

        ddlJenisPemotonganNew.DataSource = PotonganPencairan
        ddlJenisPemotonganNew.DataTextField = "Text"
        ddlJenisPemotonganNew.DataValueField = "Value"
        ddlJenisPemotonganNew.DataBind()


        txtjmlPemotonganNew.Text = "0"
        txtjmlPemotonganNew.RequiredFieldValidatorEnable = True
        txtjmlPemotonganNew.RangeValidatorEnable = True
        txtjmlPemotonganNew.RangeValidatorMinimumValue = "0"
        txtjmlPemotonganNew.RangeValidatorMaximumValue = InvoiceSupp.InvoiceAmount.ToString()
        txtjmlPemotonganNew.rv.ErrorMessage = String.Format("Maximum jumlah pemotongan {0:#,##0}.", InvoiceSupp.InvoiceAmount)
    End Sub
    Private Sub btnAddCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddCancel.Click
        pnlAddDeduction.Visible = False
        divAddDeduc.Visible = True
    End Sub
    Private Sub btnAddOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOk.Click
        Dim seq = InvoiceSuppDeductions.Count + 1
        Dim item = New Parameter.InvoiceSuppDeduction With {.Seq = seq, .DeductionID = ddlJenisPemotonganNew.SelectedValue, .DeductionDescription = ddlJenisPemotonganNew.SelectedItem.Text, .DeductionAmount = CDec(txtjmlPemotonganNew.Text)}



        InvoiceSuppDeductions.Add(item)

        pnlAddDeduction.Visible = False
        divAddDeduc.Visible = True
        loadStores(True)
    End Sub

    Sub loadStores(Optional reCnt As Boolean = False)
        dtgDeduction.DataSource = InvoiceSuppDeductions
        dtgDeduction.DataBind()

        If reCnt = True Then
            InvoiceSupp.InvoiceSuppDeductions = InvoiceSuppDeductions
            lblJumPotongan.Text = FormatNumber(InvoiceSupp.NilPelunasan, 0) ' String.Format("{0:#,##0}", SumAmountDeduction)
            lblJumlahPencairan.Text = FormatNumber(CDbl(lblNilaiInvoice.Text) - InvoiceSupp.NilPelunasan, 0) ' String.Format("{0:#,##0}", CDbl(lblNilaiInvoice.Text) - SumAmountDeduction)
            lblTotInv.Text = FormatNumber(CDbl(lblJumlahPencairan.Text) + InvoiceSupp.Refound, 0) ' String.Format("{0:#,##0}", CDbl(lblJumlahPencairan.Text) + InvoiceSupp.Refound)


        End If
    End Sub

    'Protected Sub gridView_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs)
    '    Dim k As String = dtgDeduction.DataKeys(e.RowIndex).Values("Seq").ToString()
    '    SumAmountDeduction = 0
    '    Dim remove = InvoiceSuppDeductions.Where(Function(x) x.Seq = CDec(k)).SingleOrDefault()
    '    If (Not remove Is Nothing) Then
    '        InvoiceSuppDeductions.Remove(remove)
    '        Dim s As Integer = 1
    '        For Each it In InvoiceSuppDeductions
    '            it.Seq = s
    '            s += 1
    '            SumAmountDeduction += it.DeductionAmount
    '        Next 
    '        loadStores(True) 
    '    End If 
    'End Sub

    Function deductKey(k As String) As Parameter.InvoiceSuppDeduction
        Return InvoiceSuppDeductions.Where(Function(x) x.Seq = CDec(k)).SingleOrDefault()
    End Function
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelC.Click
        Response.Redirect("Invoice.aspx")
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'InvoiceSupp.SuppBankId = ddlRekBank.SelectedValue


        Dim oSupplier As New Parameter.Supplier
        Dim s_controller As New SupplierController

        oSupplier.strConnection = GetConnectionString()
        oSupplier.SupplierID = InvoiceSupp.SupplierID

        oSupplier.ID = "1"
        oSupplier.BankID = UcBankAccount.BankID
        oSupplier.BankBranch = UcBankAccount.BankBranch
        oSupplier.AccountNo = UcBankAccount.AccountNo
        oSupplier.AccountName = UcBankAccount.AccountName
        oSupplier.BankBranchId = UcBankAccount.BankBranchId
        oSupplier.TanggalEfektif = Me.BusinessDate
        oSupplier.UntukBayar = "PENCAIRAN"
        oSupplier.DefaultAccount = True


        oSupplier = s_controller.SupplierAccountSave2(oSupplier)
        If oSupplier.Output <> "" Then
            ShowMessage(lblMessage, oSupplier.Output, True)
            Exit Sub
        End If

        InvoiceSupp.SuppBankId = UcBankAccount.BankID
        InvoiceSupp.FirstInstallmentDate = ConvertDate2(txtFirstInstallmentDate.Text)
        Try
            Dim Err As String = m_controller.InvoiceSuppSave(GetConnectionString(), InvoiceSupp)
            If Err = "" Then
                Response.Redirect("Invoice.aspx")
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub

    Private Sub dtgDeduction_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dtgDeduction.ItemDataBound
        Dim lnkApplicationTo As HyperLink
        If e.Item.ItemIndex >= 0 Then

            lnkApplicationTo = CType(e.Item.FindControl("lnkApplicationTo"), HyperLink)
            If lnkApplicationTo.Text.Trim.Length > 0 Then
                lnkApplicationTo.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lnkApplicationTo.Text.Trim) & "')"
            End If
        End If
    End Sub

    'Private Sub dtgDeduction_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dtgDeduction.RowDataBound
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim delButton As New Button
    '        Dim seqE As New Label
    '        delButton = CType(e.Row.FindControl("ButtonDelete"), Button)
    '        seqE = CType(e.Row.FindControl("lblseqE"), Label)
    '        If SumAmountDeduction > 0 And seqE.Text = "1" Then
    '            delButton.Visible = False
    '        End If
    '    End If
    'End Sub
End Class