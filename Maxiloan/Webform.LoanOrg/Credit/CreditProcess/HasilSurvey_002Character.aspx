﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HasilSurvey_002Character.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.HasilSurvey_002Character" %>

<%@ Register Src="../../../webform.UserController/UcLookUpProductOffering.ascx"
    TagName="UcLookUpProductOffering" TagPrefix="uc1" %>

   <%-- <%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>--%>
<%@ Register Src="../../../webform.UserController/ucLookUpCustomer.ascx" TagName="ucLookUpCustomer"
    TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../webform.UserController/ucGuarantor.ascx" TagName="ucGuarantor"
    TagPrefix="uc4" %>
<%@ Register Src="../../../webform.UserController/ucReferensi.ascx" TagName="ucReferensi"
    TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucLookupKabupaten.ascx" TagName="ucLookupKabupaten"
    TagPrefix="uc6" %>
<%@ Register Src="../../../webform.UserController/ucHasilSurveyTab.ascx" TagName="ucHasilSurveyTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc8" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<%@ Register Src="../../../webform.UserController/ucRefinancing.ascx" TagName="ucRefinancing"
    TagPrefix="uc9" %>
<%@ Register Src="../../../webform.UserController/ucHasilSurveyTabPhone.ascx" TagName="ucHasilSurveyTabPhone"
    TagPrefix="uc10" %>
<%@ Register Src="../../../webform.UserController/ucCoverageTypeApk.ascx" TagName="ucCoverageTypeApk"
    TagPrefix="uc11" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc12" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../../Webform.UserController/ucAddress.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucProductOffering" Src="../../../../Webform.UserController/ucProductOffering.ascx" %>--%>

<%@ Register TagPrefix="uc1" TagName="ucProdOffering" Src="../../../Webform.UserController/UcLookUpPdctOffering.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucKabupaten" Src="../../../Webform.UserController/ucKabupaten.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../Include/Buttons.css" />
    
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            PengalamanKredit();
        });

        function calculateAdminFeeGross() {
            var admin = $('#ucAdminFee_txtNumber').val();
            var fiducia = $('#ucFiduciaFee_txtNumber').val();
            var other = $('#ucOtherFee_txtNumber').val();
            var biayaPolis = $('#ucBiayaPolis_txtNumber').val()

            if (!admin) { admin = "0" };
            if (!fiducia) { admin = "0" };
            if (!other) { admin = "0" };
            if (!biayaPolis) { admin = "0" };

            var total = parseInt(admin.replace(/\s*,\s*/g, '')) +
                        parseInt(fiducia.replace(/\s*,\s*/g, '')) +
                        parseInt(other.replace(/\s*,\s*/g, '')) +
                        parseInt(biayaPolis.replace(/\s*,\s*/g, ''));
            $('#lblAdminFeeGross').html(number_format(total));

        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function SelectFromArray(itemArray) {
            var objAdd = 'document.forms[0].UCMailingAddress_';
            var pAddress = eval(objAdd + 'txtAddress');
            var pRT = eval(objAdd + 'txtRT');
            var pRW = eval(objAdd + 'txtRW');
            var pKelurahan = eval(objAdd + 'oLookUpKodePos_txtKelurahan'); 
            var pKecamatan = eval(objAdd + 'oLookUpKodePos_txtKecamatan');
            var pCity = eval(objAdd + 'oLookUpKodePos_txtCity');
            var pZipCode = eval(objAdd + 'oLookUpKodePos_txtZipCode'); 
            var pAreaPhone1 = eval(objAdd + 'txtAreaPhone1');
            var pPhone1 = eval(objAdd + 'txtPhone1');
            var pAreaPhone2 = eval(objAdd + 'txtAreaPhone2');
            var pPhone2 = eval(objAdd + 'txtPhone2');
            var pAreaFax = eval(objAdd + 'txtAreaFax');
            var pFax = eval(objAdd + 'txtFax');

            pAddress.value = itemArray[0][0];
            pRT.value = itemArray[0][1];
            pRW.value = itemArray[0][2];
            pKelurahan.value = itemArray[0][3];
            pKecamatan.value = itemArray[0][4];
            pCity.value = itemArray[0][5];
            pZipCode.value = itemArray[0][6];
            pAreaPhone1.value = itemArray[0][7];
            pPhone1.value = itemArray[0][8];
            pAreaPhone2.value = itemArray[0][9];
            pPhone2.value = itemArray[0][10];
            pAreaFax.value = itemArray[0][11];
            pFax.value = itemArray[0][12];
            return false;
        }

        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date);
            if (chk == true) {
                text.disabled = true;
                text.value = '';
            }
            else {
                text.disabled = false;
            }

            return true;

        }

        function PengalamanKredit() { 
            var myID = 'txtPembayaran';
            var obj = eval('document.forms[0].' + myID);
            if (document.forms[0].rboPengalamanKredit.value == 'Y') {
                obj.disabled = false;
                document.forms[0].rboBuktiBayar_0.disabled = false;
                document.forms[0].rboBuktiBayar_1.disabled = false;
            }
            else {
                obj.disabled = true;
                document.forms[0].rboBuktiBayar_0.disabled = true;
                document.forms[0].rboBuktiBayar_1.disabled = true;
            }
        }

        
        function rboRefinancing_onchange() {
            var value = $('#rboRefinancing').find(':checked')[0].value;

            if (value === 'False') 
            {
                $('#divRefinancing').css("display", "inherit");
                return;
            }
            else 
            {
                $('#divRefinancing').css("display", "none");
                $('#ucRefinancing1_txtAgreementNo').val('');
                $('#ucRefinancing1_txtNilaiPelunasan').val('');
            }
        }
        function rboCaraSurvey_onchange() {

            var _CaraSurvey = $('#cboCaraSurvey').val();
            if (_CaraSurvey === 'False') {
                $('#divCaraSurvey').css("display", "inherit");
                return;
            }
            else {
                $('#divCaraSurvey').css("display", "none");
                $('#ucHasilSurveyTabPhone1_txtTlpRumah').val('');
                $('#ucHasilSurveyTabPhone1_txtTlpKantor').val('');
                $('#ucHasilSurveyTabPhone1_txtHandphone').val('');
                $('#ucHasilSurveyTabPhone1_txtEmergencyContact').val('');
            }
        }

        function UploadDokument(ucASPXPath, ucTitle, divLookupContent, paramData) {
            paramData = paramData || '';
            $('#dialog').remove();
            var match = new RegExp(App, 'gi');
            var tmpStrPath = AppInfo.replace(match, '');
            tmpStrPath = tmpStrPath.substring(2, tmpStrPath.length - 1);
            var arrStrPath = tmpStrPath.split('/');
            tmpStrPath = '';
            $.each(arrStrPath, function (index, value) {
                if (arrStrPath.length - 1 != index) {
                    tmpStrPath += '../'
                }
            });



            $('#' + divLookupContent).prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><img src="' + tmpStrPath + 'Images/exit_lookup00.png" class="ui-custom-close" /><iframe src="' + ucASPXPath + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
            $('#dialog')
	        .data('fnName', paramData)
	        .dialog({
	            title: ucTitle,
	            bgiframe: false,
	            width: 900,
	            height: 500,
	            resizable: false,
	            modal: true,
	            closeOnEscape: true,
	            draggable: true,
	            create: function (event, ui) {
	                $(".ui-widget-header").hide();
	            },
	            open: function () {
	                $('.ui-custom-close').bind('click', function () {
	                    $('#dialog').dialog('close');
	                })
	            }
	        });
            return false;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinZipCode(pZipCode, pKelurahan, pKecamatan, pCity, pStyle) {
            window.open(ServerName + App + '/General/LookUpZipCode.aspx?Zipcode=' + pZipCode + '&Kelurahan=' + pKelurahan + '&Kecamatan=' + pKecamatan + '&City=' + pCity + '&Style=' + pStyle, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=1, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinProductOfferingLookup(pProductOfferingID, pProductOfferingDescription, pProductID, pAssetTypeID, pStyle, pBranchID) {
            window.open(ServerName + App + '/General/LookUpProductOffering.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID + '&AssetTypeID=' + pAssetTypeID + '&branchid=' + pBranchID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinKabupaten(pProductOfferingID, pProductOfferingDescription, pProductID, pStyle) {
            window.open(ServerName + App + '/General/LookupKabupaten.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc7:ucHasilSurveyTab id="ucHasilSurveyTab1" runat="server" />
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        CHARACTER</h3>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Pihak yang turut Survey</label>  
                        <asp:DropDownList ID="cboPihakYangTurutSurvey" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan</label>
                        <asp:TextBox ID="txtAlasanPihakYangTurutSurvey" runat="server" Width="250px" MaxLength="50" CssClass="inptype"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Lokasi yang disurvey</label>  
                        <asp:CheckBoxList ID="rboLokasiyangDisurvey" runat="server" RepeatDirection="Horizontal" CssClass="opt_single">
                            <asp:ListItem Value="R" Selected="True">Rumah</asp:ListItem>
                            <asp:ListItem Value="U">Lokasi Usaha</asp:ListItem>
                            <asp:ListItem Value="L">Lainnya</asp:ListItem>
                        </asp:CheckBoxList>
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan</label>
                        <asp:TextBox ID="txtAlasanLokasiYangDisurvey" width="255px" runat="server"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Lokasi survey sesuai KTP</label>  
                        <asp:RadioButtonList ID="rboSurveySesuaiKTP" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ya</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label>
                            Alasan</label>
                        <asp:TextBox ID="txtAlasanSurveySesuaiKTP" width="255px" runat="server"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Waktu survey</label>  
                        <asp:TextBox ID="txtWaktuSurvey1" runat="server" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:Label ID="lblMiring" runat="server">s/d</asp:Label>
                        <asp:TextBox ID="txtWaktuSurvey2" runat="server" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:CalendarExtender ID="txtWaktuSurvey1_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtWaktuSurvey1" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="txtWaktuSurvey1"></asp:RequiredFieldValidator>
                         <asp:CalendarExtender ID="txtWaktuSurvey2_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtWaktuSurvey2" Format ="dd/MM/yyyy"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="txtWaktuSurvey2"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
        <div class="form_title">
            <div class="form_single">
                <h3>CEK LINGKUNGAN</h3>
            </div>
        </div>
      <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px" DataKeyField="Number"
                CellPadding="3" CellSpacing="1" CssClass="grid_general"
                Width="100%">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>                 
                    <asp:TemplateColumn HeaderText="NO" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="50px">                        
                        <ItemTemplate>
                            <asp:Label ID="lblCLNumber" runat="server" Text='<%# container.dataitem("Number") %>'></asp:Label>
                        </ItemTemplate>                        
                    </asp:TemplateColumn>             
                    
                    <asp:TemplateColumn HeaderText="NAMA" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="450px">                        
                        <ItemTemplate>
                            <asp:TextBox ID="txtCLNama" runat="server" Text='<%# container.dataitem("CLNama1") %>' MaxLength="50"></asp:TextBox>
                        </ItemTemplate>                        
                    </asp:TemplateColumn>                                       
                    <asp:TemplateColumn HeaderText="DOMISILI" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="450px">                        
                        <ItemTemplate>
                            <asp:DropDownList ID="cboCLDomisili" runat="server" Text='<%# container.dataitem("CLDomisili1") %>'> 
                            <asp:ListItem Value="x" >Select One</asp:ListItem>
                            <asp:ListItem Value="B">BENAR</asp:ListItem>
                            <asp:ListItem Value="S">SALAH</asp:ListItem>
                            </asp:DropDownList>
                         </ItemTemplate>                        
                    </asp:TemplateColumn>             
                    <asp:TemplateColumn HeaderText="LAMA TINGGAL" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="250px">                        
                        <ItemTemplate>
                            <asp:TextBox ID="txtCLLamaTinggal" runat="server" Text='<%# container.dataitem("CLLamaTinggal1") %>' Width="80px" onkeypress="return numbersonly2(event)" MaxLength="2"></asp:TextBox>
                        </ItemTemplate>                        
                    </asp:TemplateColumn>                                       
                    <asp:TemplateColumn HeaderText="LAMA USAHA" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="250px">                        
                        <ItemTemplate>
                            <asp:TextBox ID="txtCLLamaUsaha" runat="server" Text='<%# container.dataitem("CLLamaUsaha1") %>' Width="80px" onkeypress="return numbersonly2(event)" MaxLength="2"></asp:TextBox>
                        </ItemTemplate>                        
                    </asp:TemplateColumn>                                                                                     
                </Columns>
            </asp:DataGrid>
        </div>
    </div>              

            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">Hasil cek lingkungan aspek sosial</label>  
                        <asp:DropDownList ID="cboHasilCekLkgAspekSosial" runat="server">
                            <asp:ListItem Value="Baik Sekali" Selected="True">Baik Sekali</asp:ListItem>
                            <asp:ListItem Value="Baik">Baik</asp:ListItem>
                            <asp:ListItem Value="Cukup">Cukup</asp:ListItem>
                            <asp:ListItem Value="Kurang">Kurang</asp:ListItem>
                            <asp:ListItem Value="Buruk">Buruk</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="cboHasilCekLkgAspekSosial"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Integritas</label>  
                        <asp:DropDownList ID="cboIntegritas" runat="server">
                            <asp:ListItem Value="Sangat Tinggi" Selected="True">SANGAT TINGGI</asp:ListItem>
                            <asp:ListItem Value="Tinggi">TINGGI</asp:ListItem>
                            <asp:ListItem Value="Sedang">SEDANG</asp:ListItem>
                            <asp:ListItem Value="Rendah">RENDAH</asp:ListItem>
                            <asp:ListItem Value="Sangat Rendah">SANGAT RENDAH</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="cboIntegritas"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Pengalaman Pembiayaan</label>  
                        <asp:RadioButtonList ID="rboPengalamanKredit" runat="server" RepeatDirection="Horizontal"  CssClass="opt_single" onchange="PengalamanKredit();">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak Ada</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>
                            Pembayaran</label>
                        <asp:TextBox ID="txtPembayaran" runat="server" Width="250px" MaxLength="50" CssClass="inptype"></asp:TextBox>
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Bukti Bayar</label>  
                        <asp:RadioButtonList ID="rboBuktiBayar" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak Ada</asp:ListItem>
                            
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Pekerjaan Sekarang</label>  
                        <asp:TextBox ID="txtPekerjaanSekarang" runat="server" Width="355px" MaxLength="50" CssClass="inptype"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Lamanya</label>
                            <asp:TextBox ID="txtLamaPekerjaanSekarang" Width="48px" runat="server" onkeypress="return numbersonly2(event)" enabled="false"></asp:TextBox>                   
                            <label>tahun</label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="txtLamaPekerjaanSekarang"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Pekerjaan Sebelumnya</label>  
                        <asp:TextBox ID="txtPekerjaanSebelumnya" runat="server" Width="355px" MaxLength="50" CssClass="inptype"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label>
                            Lamanya</label>
                            <asp:TextBox ID="txtLamaPekerjaanSebelumnya" Width="48px" runat="server" onkeypress="return numbersonly2(event)"></asp:TextBox>                   
                            <label>tahun</label>
                    </div>
                </div>
            </div>           
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Pendidikan</label>  
                        <asp:DropDownList ID="cboPEducation" runat="server" disabled="true">
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label>Hubungan Bank dengan Leasing</label>  
                        <asp:DropDownList ID="cboHubBankLeasing" runat="server" disabled="true">
                            <asp:ListItem Value="SANGAT BAIK">SANGAT BAIK</asp:ListItem>
                            <asp:ListItem Value="BAIK">BAIK</asp:ListItem>
                            <asp:ListItem Value="CUKUP">CUKUP</asp:ListItem>
                            <asp:ListItem Value="KURANG">KURANG</asp:ListItem>
                            <asp:ListItem Value="SANGAT KURANG">SANGAT KURANG</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>          
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama yang Menemani survey</label>  
                        <asp:TextBox ID="txtNamaYangMenemaniSurvey1" runat="server" Width="355px" MaxLength="50" CssClass="inptype"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label>
                            Hubungan</label>
                        <asp:TextBox ID="txtHubungan1" runat="server" Width="250px" MaxLength="50" CssClass="inptype"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama yang Menemani survey</label>  
                        <asp:TextBox ID="txtNamaYangMenemaniSurvey2" runat="server" Width="355px" MaxLength="50" CssClass="inptype"></asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label>
                            Hubungan</label>
                        <asp:TextBox ID="txtHubungan2" runat="server" Width="250px" MaxLength="50" CssClass="inptype"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Hubungan sales dengan pemohon </label>  
                        <asp:DropDownList ID="cboHubSalesdgnPemohon" runat="server">
                            <asp:ListItem Value="B" Selected="True">KONSUMEN BARU</asp:ListItem>
                            <asp:ListItem Value="R">KONSUMEN R/O</asp:ListItem>
                            <asp:ListItem Value="K">KELUARGA</asp:ListItem>
                            <asp:ListItem Value="T">TEMAN</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboHubSalesdgnPemohon" ErrorMessage="Harap pilih Hubungan Sales dengan Pemohon"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>  
                        <asp:TextBox runat="server" ID="txtKesimpulan" TextMode="MultiLine" Width="1000px" ></asp:TextBox>
                    </div>
                </div>
            </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
           
        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />--%>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <%--<asp:AsyncPostBackTrigger ControlID="cboKegiatanUsaha" EventName="SelectedIndexChanged" />--%>
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
