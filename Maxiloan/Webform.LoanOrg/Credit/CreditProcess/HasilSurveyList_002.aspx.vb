﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class HasilSurveyList_002
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ApplicationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oController As New HasilSurveyController
#End Region
#Region "Property"
    Private Property Err() As String
        Get
            Return CType(ViewState("Err"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Err") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(ViewState("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property CustID() As String
        Get
            Return CType(ViewState("CustID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustID") = Value
        End Set
    End Property
    Private Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property
    Private Property Type() As String
        Get
            Return CType(ViewState("Type"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property ProductID() As String
        Get
            Return CType(ViewState("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property
    Private Property ProductOfferingID() As String
        Get
            Return CType(ViewState("ProductOfferingID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Private Property InsAssetPaidBy() As String
        Get
            Return CType(ViewState("InsAssetPaidBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsAssetPaidBy") = Value
        End Set
    End Property
    Private Property InsAssetInsuredBy() As String
        Get
            Return CType(ViewState("InsAssetInsuredBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsAssetInsuredBy") = Value
        End Set
    End Property
    Private Property SupplierGroupID() As String
        Get
            Return CType(ViewState("SupplierGroupID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierGroupID") = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return CType(ViewState("InterestType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return CType(ViewState("InstallmentScheme"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Public Property NumberInstallment() As Double
        Get
            Return CType(ViewState("NumberInstallment"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("NumberInstallment") = Value
        End Set
    End Property
    Public Property NumberAgreementAsset() As Double
        Get
            Return CType(ViewState("NumberAgreementAsset"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("NumberAgreementAsset") = Value
        End Set
    End Property
    Public Property AssetID() As String
        Get
            Return CType(ViewState("AssetID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetID") = Value
        End Set
    End Property
    Public Property DateEntryApplicationData() As Date
        Get
            Return CType(ViewState("DateEntryApplicationData"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryApplicationData") = Value
        End Set
    End Property
    Public Property DateEntryAssetData() As Date
        Get
            Return CType(ViewState("DateEntryAssetData"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryAssetData") = Value
        End Set
    End Property
    Public Property DateEntryInsuranceData() As Date
        Get
            Return CType(ViewState("DateEntryInsuranceData"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryInsuranceData") = Value
        End Set
    End Property
    Public Property DateEntryFinancialData() As Date
        Get
            Return CType(ViewState("DateEntryFinancialData"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryFinancialData") = Value
        End Set
    End Property
    Public Property DateEntryIncentiveData() As Date
        Get
            Return CType(ViewState("DateEntryIncentiveData"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryIncentiveData") = Value
        End Set
    End Property

    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Private Property DateEntryHasilSurvey() As Date
        Get
            Return CType(ViewState("DateEntryHasilSurvey"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryHasilSurvey") = Value
        End Set
    End Property

    Private Property AgreementSurveyID() As String
        Get
            Return CType(ViewState("AgreementSurveyID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementSurveyID") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        lblMessage.Text = ""
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            If Request.QueryString("Err") <> "" Then
                Me.Err = Request.QueryString("Err")
                ShowMessage(lblMessage, Me.Err, True)
            End If
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                txtGoPage.Text = "1"
                If CheckForm(Me.Loginid, "EditApplication", Me.AppId) Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                If Request("cond") <> "" Then
                    Me.CmdWhere = " ApplicationStep in ('APK') and " & Request("cond")
                Else
                    Me.CmdWhere = " ApplicationStep in ('APK') "
                End If
                Me.CmdWhere2 = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
                Me.Sort = "ApplicationID ASC"
                BindGrid()
                InitialPanel()
            Else
                NotAuthorized()
            End If
        End If
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlModify.Visible = False
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
#End Region
#Region "BindGrid"
    Sub BindGrid()
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Application
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        'oCustomClass = m_controller.GetApplicationMaintenance(oCustomClass)
        oCustomClass = m_controller.GetApplicationHasilSurveydanKYC(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region
#Region "LinkTo"
    Function LinkToOpenWinApplication2(ByVal strApplicationID As String, ByVal strCustID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication2('" & strApplicationID & "','" & strCustID & "','" & strStyle & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

    Function LinkToSupplier(ByVal strStyle As String, ByVal strSupplierID As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function

    Function LinkToEmployee(ByVal strStyle As String, ByVal strBranchID As String, ByVal strAOID As String) As String
        Return "javascript:OpenWinEmployee('" & strStyle & "','" & strBranchID & "','" & strAOID & "')"
    End Function
#End Region
#Region "Modify"
    Sub Modify()

        Dim Null As Date
        Null = CType("1/1/1900", Date)

        pnlList.Visible = False
        pnlModify.Visible = True
        'Back for Asset, Insurance, and Financial Edit

        hplApplication.NavigateUrl = "EditApplication.aspx?ApplicationID=" & Me.ApplicationID & "&BranchID=" & Me.BranchID & "&name=" & Me.Name & "&type=" & Me.Type & "&ProductID=" & Me.ProductID & "&ProductOfferingID=" & Me.ProductOfferingID & "&CustomerID=" & Me.CustID & ""

        '======================= Link Asset Data ======================================
        'beri link bila dateentryassetdata<> null or (dateentryassetdata=null and dateentryapplication <> null)

        If Me.DateEntryAssetData <> Null Then
            hplAsset.NavigateUrl = "EditAssetData.aspx?ApplicationID=" & Me.ApplicationID
            'If Me.NumberAgreementAsset > 0 Then
            '    LinkEditAsset = "EditAssetData.aspx?ApplicationID=" & Me.ApplicationID
            'Else
            '    LinkEditAsset = ""
            'End If
        Else
            hplAsset.NavigateUrl = ""
            hplInsurance.NavigateUrl = ""
            hplFinancial.NavigateUrl = ""
            hplIncentive.NavigateUrl = ""
            Exit Sub
        End If


        '====================== Link Insurance ===========================
        'beri link bila datentryinsurancedata=null dan dateentryassetdata <> null

        If Me.DateEntryInsuranceData <> Null Then
            If Me.InsAssetInsuredBy = "CO" And Me.InsAssetPaidBy = "CU" Then
                hplInsurance.NavigateUrl = "../New/EditInsurance.aspx?PageSource=CompanyCustomer&Applicationid='" & Me.ApplicationID & "'&SupplierGroupID='" & Me.SupplierGroupID & "'"
            End If
            If Me.InsAssetInsuredBy = "CO" And Me.InsAssetPaidBy = "AC" Then
                hplInsurance.NavigateUrl = "../New/EditInsurance.aspx?PageSource=CompanyAtCost&Applicationid='" & Me.ApplicationID & "'&SupplierGroupID='" & Me.SupplierGroupID & "'"
            End If
            If Me.InsAssetInsuredBy = "CU" And Me.InsAssetPaidBy = "CU" Then
                hplInsurance.NavigateUrl = "EditAppInsuranceByCust.aspx?PageSource=CustToCust&Applicationid='" & Me.ApplicationID & "'&SupplierGroupID='" & Me.SupplierGroupID & "'"
            End If
        Else
            hplInsurance.NavigateUrl = ""
            hplFinancial.NavigateUrl = ""
            hplIncentive.NavigateUrl = ""
            Exit Sub
        End If

        '===================== Link Financial ====================================
        'beri link bila dateentryfinancialdata=null dan dateentryinsurancedata <> null

        Context.Trace.Write("Me.NumberInstallment" & Me.NumberInstallment)

        If Me.DateEntryFinancialData <> Null Then
            hplFinancial.NavigateUrl = "EditFinancialData_002.aspx"
            'If Me.NumberInstallment > 0 Then
            '    LinkEditFinancial = "EditFinancialData_002.aspx"
            'Else
            '    LinkEditFinancial = ""
            'End If
        Else
            hplFinancial.NavigateUrl = ""
            hplIncentive.NavigateUrl = ""
            Exit Sub
        End If
        '===================== Link Incentive ====================================
        'beri link bila dateentryfinancialdata=null dan dateentryinsurancedata <> null

        Context.Trace.Write("Me.NumberInstallment" & Me.NumberInstallment)

        If Me.DateEntryIncentiveData <> Null Then
            hplIncentive.NavigateUrl = "EditIncentiveData.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&CustomerName=" & Me.Name.Trim & "&CustomerID=" & Me.CustID & ""
            'If Me.NumberInstallment > 0 Then
            '    LinkEditFinancial = "EditFinancialData_002.aspx"
            'Else
            '    LinkEditFinancial = ""
            'End If
        Else
            hplIncentive.NavigateUrl = ""
            Exit Sub
        End If

    End Sub
    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        InitialPanel()
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Modify" Then
            If CheckFeature(Me.Loginid, "EditApplication", "Edit", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            Me.ApplicationID = Replace(CType(e.Item.FindControl("lnkApplication"), HyperLink).Text.Trim, ",", "")
            Me.Type = CType(e.Item.FindControl("lblType"), Label).Text.Trim
            Me.CustID = CType(e.Item.FindControl("lblCustomerID"), Label).Text.Trim
            Me.Name = CType(e.Item.FindControl("lnkName"), HyperLink).Text.Trim
            Me.BranchID = Me.sesBranchId.Replace("'", "")
            Me.ProductID = CType(e.Item.FindControl("lblProductID"), Label).Text.Trim
            Me.ProductOfferingID = CType(e.Item.FindControl("lblProductOfferingID"), Label).Text.Trim
            Me.ProductOfferingID = CType(e.Item.FindControl("lblProductOfferingID"), Label).Text.Trim

            'Response.Redirect("../CreditProcess/HasilSurvey_002.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustID.ToString.Trim)

            'Modify by Wira 20171013
            'Add ProspectAppID & send cookies propespectappid
            Me.ProspectAppID = CType(e.Item.FindControl("lblProspectAppID"), Label).Text.Trim
            Me.AgreementSurveyID = CType(e.Item.FindControl("lblAgreementSurveyID"), Label).Text.Trim
            'CekTabAplikasiSurvey()
            'If Me.ProspectAppID <> "-" And Me.DateEntryHasilSurvey = Nothing Then
            If Me.ProspectAppID <> "-" And Me.AgreementSurveyID = "-" Then
                Response.Redirect("../CreditProcess/HasilSurvey_002.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Add&id=" & Me.CustID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            Else
                Response.Redirect("../CreditProcess/HasilSurvey_002.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            End If
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkApplication As New HyperLink
        Dim lnkSupplier As New HyperLink
        Dim lnkName As New HyperLink
        Dim lnkAO As New HyperLink

        Dim lblSupplierID As New Label
        Dim lblAOID As New Label
        Dim lblCustomerID As New Label
        Dim lblApplicationStep As New Label
        Dim lblDateReturnApp As New Label

        If e.Item.ItemIndex >= 0 Then
            lnkApplication = CType(e.Item.FindControl("lnkApplication"), HyperLink)
            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), HyperLink)
            lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)

            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationStep = CType(e.Item.FindControl("lblApplicationStep"), Label)
            lblDateReturnApp = CType(e.Item.FindControl("lblDateReturnApp"), Label)

            lnkApplication.NavigateUrl = LinkToOpenWinApplication2(lnkApplication.Text.Trim, lblCustomerID.Text.Trim, "AccAcq")
            lnkSupplier.NavigateUrl = LinkToSupplier("AccAcq", lblSupplierID.Text.Trim)
            lnkName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            lnkAO.NavigateUrl = LinkToEmployee("AccAcq", Me.sesBranchId.Replace("'", ""), lblAOID.Text.Trim)


            If lblDateReturnApp.Text <> "" And Not (lblDateReturnApp Is Nothing) Then
                lblApplicationStep.Text = "RETURN"
            End If

        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGrid()
    End Sub
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim Search As String = ""
        If txtSearch.Text.Trim <> "" Then
            If cboSearch.SelectedIndex = 2 Then
                Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + Replace(txtSearch.Text.Trim, "'", "''") + "%'"
            Else
                Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + txtSearch.Text.Trim + "%'"
            End If
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGrid()
    End Sub
#End Region
#Region "SendCookiesFinancial"
    Public Sub SendCookiesFinancial()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_EDIT_FINANCIAL)
        If Not cookie Is Nothing Then
            cookie.Values("id") = Me.ApplicationID
            cookie.Values("Custid") = Me.CustID
            cookie.Values("name") = Me.Name
            cookie.Values("InterestType") = Me.InterestType
            cookie.Values("InstallmentScheme") = Me.InstallmentScheme
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_EDIT_FINANCIAL)
            cookieNew.Values.Add("id", Me.ApplicationID)
            cookieNew.Values.Add("Custid", Me.CustID)
            cookieNew.Values.Add("name", Me.Name)
            cookieNew.Values.Add("InterestType", Me.InterestType)
            cookieNew.Values.Add("InstallmentScheme", Me.InstallmentScheme)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region
#Region "SendCookiesAssetData"
    Public Sub SendCookiesAssetData()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_EDIT_ASSET_DATA)
        If Not cookie Is Nothing Then
            cookie.Values("id") = AppId
            cookie.Values("Custid") = CustID
            cookie.Values("name") = Me.Name
            cookie.Values("AssetID") = Me.AssetID
            cookie.Values("ApplicationID") = Me.ApplicationID
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_EDIT_ASSET_DATA)
            cookieNew.Values.Add("id", AppId)
            cookieNew.Values.Add("Custid", CustID)
            cookieNew.Values.Add("name", Me.Name)
            cookieNew.Values.Add("AssetID", Me.AssetID)
            cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

    Protected Sub dtgPaging_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dtgPaging.SelectedIndexChanged

    End Sub

    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryHasilSurvey = .DateEntryHasilSurvey
        End With
    End Sub
End Class