﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController

#End Region

Public Class GoLive
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ApplicationController
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Private x_controller As New DataUserControlController

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private time As String
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property CmdWhere2() As String
        Get
            Return CType(viewstate("vwsCmdWhere2"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere2") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property AgreementDate() As String
        Get
            Return CType(viewstate("AgreementDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementDate") = Value
        End Set
    End Property
    Private Property AADate() As String
        Get
            Return CType(viewstate("AADate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AADate") = Value
        End Set
    End Property
    Private Property CustName() As String
        Get
            Return CType(viewstate("CustName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustName") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property SurveyDate() As String
        Get
            Return viewstate("SurveyDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SurveyDate") = Value
        End Set
    End Property
    Private Property DeliveryOrderDate() As String
        Get
            Return viewstate("DeliveryOrderDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("DeliveryOrderDate") = Value
        End Set
    End Property
    Private Property FirstInstallment() As String
        Get
            Return viewstate("FirstInstallment").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("FirstInstallment") = Value
        End Set
    End Property


    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        'If Me.IsHoBranch = False Then
        '    NotAuthorized()
        '    Exit Sub
        'End If
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateStart = Me.BusinessDate + " " + time

        lblMessage.Text = ""
        lblMessage.Visible = False

        If Not Page.IsPostBack Then
            '  If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
            txtGoPage.Text = "1"
            If CheckForm(Me.Loginid, "GoLive", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            If Request("cond") <> "" Then
                Me.CmdWhere = Request("cond")
            Else
                Me.CmdWhere = "ALL"
            End If
            Me.Sort = "AgreementNo ASC"
            'BindGrid()
            InitialPanel()

            With oBranch
                If Me.IsHoBranch Then
                    .DataSource = x_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Enabled = True
                Else
                    .DataSource = x_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Enabled = False
                End If

            End With

            'Else
            '    NotAuthorized()
            'End If



        End If
    End Sub
    Sub InitialPanel()
        pnlList.Visible = False
        pnlSearch.Visible = True
        txtTanggalAktivasi.Text = Format(CDate(Me.BusinessDate), "dd/MM/yyyy")
    End Sub
    Sub NotAuthorized()
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
    End Sub
#End Region
#Region "BindGrid"
    Sub BindGrid()
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Application
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.WhereCond2 = Me.CmdWhere2
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = m_controller.GetGoLive(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenApplicationId('" & strStyle & "','" & strApplicationID & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function

    Function LinkToEmployee(ByVal strBranchID As String, ByVal strAOID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinEmployee('" & strStyle & "','" & strBranchID & "','" & strAOID & "')"
    End Function

    Function LinkToAgreementNo(ByVal strAgreementNo As String, ByVal strStyle As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "','" & strAgreementNo & "')"
    End Function

#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = "2" '(System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = "2" '(System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid()
            End If
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand        
        Try
            If e.CommandName = "GoLive" Then
                If CheckFeature(Me.Loginid, "GoLive", "Add", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If
                End If

                Me.AgreementDate = e.Item.Cells(9).Text.Trim
                Me.ApplicationID = e.Item.Cells(8).Text.Trim
                Me.SurveyDate = e.Item.Cells(10).Text.Trim
                Me.AADate = e.Item.Cells(11).Text.Trim
                Me.FirstInstallment = e.Item.Cells(12).Text.Trim
                Me.DeliveryOrderDate = e.Item.Cells(7).Text.Trim                
                Me.AgreementNo = CType(e.Item.Cells(1).FindControl("lnkAgreementNo"), HyperLink).Text.Trim
                Me.CustName = CType(e.Item.Cells(2).FindControl("lnkName"), HyperLink).Text.Trim
                Me.CustomerID = CType(e.Item.FindControl("lblCustomerID"), Label).Text.Trim                
                Response.Redirect("GoLive_002.aspx?ANo=" & Me.AgreementNo & "&Name=" & Me.CustName & "&App=" & Me.ApplicationID & "&AgDate=" & Me.AgreementDate & "&CustomerID=" & Me.CustomerID & "&DeliveryOrderDate=" & Me.DeliveryOrderDate & "&SurveyDate=" & Me.DeliveryOrderDate & "&AADate=" & Me.AADate & "&FirstInstallment=" & Me.FirstInstallment & "")

            ElseIf e.CommandName = "Return" Then
                pnlList.Visible = False
                pnlReturn.Visible = True                

                Dim lblCustomerID As New Label
                lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
                Dim hypAgreementNo As New HyperLink
                hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
                Dim lblApplicationID As New Label
                lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)

                hdnApplicationID.Value = lblApplicationID.Text.Trim
                ucViewApplication1.ApplicationID = lblApplicationID.Text.Trim
                ucViewApplication1.CustomerID = lblCustomerID.Text.Trim
                ucViewApplication1.bindData()
                ucViewApplication1.initControls("OnNewApplication")
                ucViewCustomerDetail1.CustomerID = lblCustomerID.Text.Trim
                ucViewCustomerDetail1.bindCustomerDetail()
            End If

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("GoLive.Aspx", "ItemCommand", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid()
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        InitialPanel()
        BindGrid()
    End Sub
    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim Search As String = ""
        Me.CmdWhere2 = "BranchID = '" & oBranch.SelectedValue.ToString.Trim & "'"
        If txtSearch.Text.Trim <> "" Then            
            Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
            Me.CmdWhere = cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        pnlList.Visible = True
        BindGrid()
    End Sub
#End Region
#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblAgreementNo As New Label
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            Dim lblAplicationId As New Label
            lblAplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            Dim lblSupplierID As New Label
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            Dim lblCustomerID As New Label
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Dim lblAOID As New Label
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)

            Dim lnkAgreementNo As New HyperLink
            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            Dim lnkName As New HyperLink
            lnkName = CType(e.Item.FindControl("lnkName"), HyperLink)
            Dim lnkSupplier As New HyperLink
            lnkSupplier = CType(e.Item.FindControl("lnkSupplier"), HyperLink)
            Dim lnkAO As New HyperLink
            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)

            lnkAgreementNo.NavigateUrl = LinkToAgreementNo(lblAplicationId.Text.Trim, "AccAcq")
            lnkName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            lnkSupplier.NavigateUrl = LinkToSupplier(lblSupplierID.Text.Trim, "AccAcq")
            lnkAO.NavigateUrl = LinkToEmployee(Me.sesBranchId.Replace("'", ""), lblAOID.Text.Trim, "AccAcq")

            Dim lnkviewamor As New HyperLink
            lnkviewamor = CType(e.Item.FindControl("lnkviewamor"), HyperLink)
            lnkviewamor.NavigateUrl = "../../../Webform.Reports.RDLC/LoanOrg/Credit/ViewAmorTemp.aspx?ApplicationID=" & lblAplicationId.Text.Trim & "&tgl=" & txtTanggalAktivasi.Text
        End If
    End Sub
#End Region

    Public Sub SelectAll(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chkSender As CheckBox = CType(sender, CheckBox)
        Dim chkItem As CheckBox
        Dim x As Integer

        For x = 0 To dtgPaging.Items.Count - 1
            chkItem = CType(dtgPaging.Items(x).FindControl("chkItem"), CheckBox)
            chkItem.Checked = chkSender.Checked
        Next
    End Sub

    Protected Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim lblApplicationID, lblBranchID As Label
        Dim chkDtList As CheckBox
        Dim dt As DataTable = createNewDT()

        For i = 0 To dtgPaging.Items.Count - 1
            chkDtList = CType(dtgPaging.Items(i).FindControl("chkItem"), CheckBox)
            If Not IsNothing(chkDtList) Then
                If chkDtList.Checked Then
                    lblApplicationID = CType(dtgPaging.Items(i).FindControl("lblApplicationID"), Label)
                    lblBranchID = CType(dtgPaging.Items(i).FindControl("lblBranchID"), Label)
                    dt.Rows.Add((i + 1).ToString, lblApplicationID.Text.Trim, lblBranchID.Text.Trim)
                End If
            End If
        Next

        Dim oGoLive As New Parameter.Application
        'oGoLive.BusinessDate = Me.BusinessDate 'ConvertDate2(txtTanggalAktivasi.Text)
        oGoLive.BusinessDate = ConvertDate2(txtTanggalAktivasi.Text)
        oGoLive.ListData = dt
        oGoLive.strConnection = GetConnectionString()

        Try
            oGoLive = m_controller.GoLiveSave(oGoLive)
            'Modify by Wira 20171019
            ActivityLog(dt)

            BindGrid()
        Catch ex As Exception
            ShowMessage(lblMessage, TranslateMessage(ex.Message), True)
        End Try
    End Sub

    Protected Function TranslateMessage(ByVal msg As String) As String
        Dim rtn As String = ""
        Dim msgcode As String = ""
        If msg.Length > 4 Then
            msgcode = msg.Substring(0, 4)
            Select Case msgcode
                Case "0001"
                    rtn = "Setup kolektor bedasarkan kelurahan dan kode pos pada aplikasi ini belum dilakukan!"
                Case "0002"
                    rtn = "Setup deskcoll belum dilakukan!"
                Case "0003"
                    rtn = "Company ID On This Branch Not Exists"
                Case Else
                    rtn = msg
            End Select
        End If
        Return rtn
    End Function

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Dim ParDO As New Parameter._DO
        Dim m_DO As New DOController

        ParDO.strConnection = GetConnectionString()
        ParDO.BusinessDate = Me.BusinessDate
        ParDO.BranchId = Me.sesBranchId.Replace("'", "").Trim
        ParDO.ApplicationID = hdnApplicationID.Value
        ParDO.AlasanReturn = txtAlasanReturn.Text.Trim

        Try
            m_DO.ApplicationReturnUpdate(ParDO)

            BindGrid()
            pnlList.Visible = True            
            pnlReturn.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Me.CmdWhere = "ALL"
        BindGrid()

        pnlList.Visible = True        
        pnlReturn.Visible = False
    End Sub

    Private Function createNewDT() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add("id")
        dt.Columns.Add("value1")
        dt.Columns.Add("value2")

        Return dt
    End Function

#Region "Activity Log"
    Sub ActivityLog(dt As DataTable)
        Dim row As DataRow                

        For Each row In dt.Rows
            Me.BranchID = row("value2")
            Me.ApplicationID = row("value1")

            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateEnd = Me.BusinessDate + " " + time

            Dim oApplication As New Parameter.Application
            oApplication.strConnection = GetConnectionString()
            oApplication.BranchId = Replace(Me.BranchID, "'", "").ToString
            oApplication.ApplicationID = Me.ApplicationID.Trim
            oApplication.ActivityType = "AKR"
            oApplication.ActivityDateStart = Me.ActivityDateStart
            oApplication.ActivityDateEnd = Me.ActivityDateEnd
            oApplication.ActivityUser = Me.Loginid
            oApplication.ActivitySeqNo = 18

            Dim ErrorMessage As String = ""
            Dim oReturn As New Parameter.Application

            oReturn = m_controller.ActivityLogSave(oApplication)

            If oReturn.Err <> "" Then
                ShowMessage(lblMessage, ErrorMessage, True)
                Exit Sub
            End If
        Next row

    End Sub

    Private Sub txtTanggalAktivasi_TextChanged(sender As Object, e As EventArgs) Handles txtTanggalAktivasi.TextChanged
        Dim lnkviewamor As HyperLink
        Dim lblAplicationId As New Label
        For i = 0 To dtgPaging.Items.Count - 1
            lnkviewamor = CType(dtgPaging.Items(i).FindControl("lnkviewamor"), HyperLink)
            lblAplicationId = CType(dtgPaging.Items(i).FindControl("lblApplicationId"), Label)
            lnkviewamor.NavigateUrl = "../../../Webform.Reports.RDLC/LoanOrg/Credit/ViewAmorTemp.aspx?ApplicationID=" & lblAplicationId.Text.Trim & "&tgl=" & txtTanggalAktivasi.Text
        Next


    End Sub
#End Region
End Class