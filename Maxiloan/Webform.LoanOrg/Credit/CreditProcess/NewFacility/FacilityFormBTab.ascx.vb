﻿Public Class FacilityFormBTab
    Inherits System.Web.UI.UserControl
    Public Property CKEEditorFormB As String
        Get
            Return CType(ViewState("CKEEditorFormB"), String)
        End Get
        Set(value As String)
            ViewState("CKEEditorFormB") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CKEEditorFormB = CKEditor1.Text
    End Sub

    Sub ViewCKE(CKExx)
        CKEditor1.Text = CKExx
        CKEditor1.Enabled = False
    End Sub
End Class