﻿Public Class FacilityFormETab
    Inherits System.Web.UI.UserControl

    Public Property CKEEditorFormE As String
        Get
            Return CType(ViewState("CKEEditorFormE"), String)
        End Get
        Set(value As String)
            ViewState("CKEEditorFormE") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CKEEditorFormE = CKEditor1.Text
    End Sub

    Sub ViewCKE(CKExx)
        CKEditor1.Text = CKExx
        CKEditor1.Enabled = False
    End Sub
End Class