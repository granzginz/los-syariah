﻿Public Class FacilityFormCTab
    Inherits System.Web.UI.UserControl

    Public Property CKEEditorFormC As String
        Get
            Return CType(ViewState("CKEEditorFormC"), String)
        End Get
        Set(value As String)
            ViewState("CKEEditorFormC") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CKEEditorFormC = CKEditor1.Text
    End Sub

    Sub ViewCKE(CKExx)
        CKEditor1.Text = CKExx
        CKEditor1.Enabled = False
    End Sub
End Class