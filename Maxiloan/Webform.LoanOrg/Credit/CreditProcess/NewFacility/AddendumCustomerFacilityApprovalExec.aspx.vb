﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.CompilerServices
Imports Decrypt
#End Region

Public Class AddendumCustomerFacilityApprovalExec
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_controller As New SupplierController
    Private oCustomClass As New Parameter.CustomerFacility
    Private oController As New CustomerFacilityController
#End Region

#Region "Property"

    Property NoFasilitas() As String
        Get
            Return ViewState("NoFasilitas").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitas") = Value
        End Set
    End Property

    Property NoFasilitasAddendum() As String
        Get
            Return ViewState("NoFasilitasAddendum").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitasAddendum") = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            Return CType(ViewState("TotalAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalAmount") = Value
        End Set
    End Property

    Property RequestDate() As String
        Get
            Return ViewState("RequestDate").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("RequestDate") = Value
        End Set
    End Property

    Property isFinal() As Boolean
        Get
            Return ViewState("isFinal").ToString
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isFinal") = Value
        End Set
    End Property

    Property NextPersonApproval() As String
        Get
            Return ViewState("NextPersonApproval").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NextPersonApproval") = Value
        End Set
    End Property

    Property UserApproval() As String
        Get
            Return ViewState("UserApproval").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("UserApproval") = Value
        End Set
    End Property

    Property ApprovalLoginID() As String
        Get
            Return ViewState("ApprovalLoginID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApprovalLoginID") = Value
        End Set
    End Property

    Property ApprovalNo() As String
        Get
            Return ViewState("ApprovalNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApprovalNo") = Value
        End Set
    End Property

    Private oEncrypt As am_futility
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.NoFasilitas = Request.QueryString("NoFasilitas")
            Me.TotalAmount = Request.QueryString("TotalAmount")
            Me.RequestDate = Request.QueryString("RequestDate")
            Me.ApprovalNo = Request.QueryString("ApprovalNo")
            Me.NoFasilitasAddendum = Request.QueryString("NoFasilitasAddendum")
        End If

        lblMessage.Visible = False
        Dim oInqPCReimburse As New Parameter.CustomerFacility
        Dim pInqPCReimburse As New Parameter.CustomerFacility
        With oCustomClass
            .strConnection = GetConnectionString()
            .NoFasilitas = Me.NoFasilitas
            .ApprovalSchemeID = "AFAC"
            .LoginId = Me.Loginid
            .IsReRequest = 0
            .NoFasilitasAddendum = Me.NoFasilitasAddendum
        End With

        'jika login yang approved bukan login yang semestinya, maka munculkan security code
        oInqPCReimburse = oController.CustFacApproveIsValidApproval(oCustomClass)
        Me.UserApproval = LTrim(RTrim(oInqPCReimburse.UserApproval))

        If Me.UserApproval <> Me.Loginid = True Then
            pnlSecurityCode.Visible = True
        Else
            pnlSecurityCode.Visible = False
        End If

        'Cek userscheme apakah bisa final?
        pInqPCReimburse = oController.CustFacApproveisFinal(oCustomClass)
        Me.NextPersonApproval = LTrim(RTrim(pInqPCReimburse.NextPersonApproval))
        If pInqPCReimburse.IsFinal = False Then
            Me.isFinal = False
        Else
            Me.isFinal = True
        End If

        BindData()
        pnlDeclineFinal.Visible = False
        pnlNextPerson.Visible = False

        Me.oEncrypt = New am_futility()
    End Sub

    Sub BindData()
        Dim hyNoFasilitasa As HyperLink
        Dim hypNoFasilitasAddenduma As HyperLink

        hypNoFasilitas.Text = Me.NoFasilitas
        lblAmount.Text = FormatNumber(Me.TotalAmount, 2)
        lblRequestDate.Text = Me.RequestDate

        hypNoFasilitasAddendum.Text = Me.NoFasilitasAddendum


        hyNoFasilitasa = CType((hypNoFasilitas), HyperLink)
        hyNoFasilitasa.NavigateUrl = LinkToViewCustomerFacility(hyNoFasilitasa.Text.Trim, "ACCMNT", "ALL")

        hypNoFasilitasAddenduma = CType((hypNoFasilitasAddendum), HyperLink)
        hypNoFasilitasAddenduma.NavigateUrl = LinkToViewAddendum(hypNoFasilitasAddendum.Text.Trim, "ACCMNT", "ALL")
    End Sub

    Private Function Is_ValidSecurityCode(ByVal pStrLoginID As String, ByVal pStrDate As DateTime, ByVal pStrApprovalNo As String, ByVal pStrEntry As String) As Boolean
        Dim apvcode As apvcode = New apvcode()
        Dim flag As Boolean = True
        Dim text As String = Me.Get_Text("Select ApprovalID From ApprovalStatus Where ApprovalNo = '" & pStrApprovalNo & "'")
        Dim str As String = apvcode.ApvCodeGen(CObj(pStrLoginID), CObj(pStrDate), CObj((text.Trim() & Me.cboApproval.SelectedItem.Value)), "", "")
        If Microsoft.VisualBasic.CompilerServices.Operators.CompareString(pStrEntry.Trim(), str.Trim(), False) <> 0 Then flag = False
        Return flag
    End Function

    Private Function Get_Text(ByVal pStrSQL As String) As String
        Dim sqlCommand As SqlCommand = New SqlCommand()
        Dim sqlConnection As SqlConnection = New SqlConnection(Me.GetConnectionString())
        Dim str As String = ""

        If Microsoft.VisualBasic.CompilerServices.Operators.CompareString(pStrSQL, "", False) <> 0 Then
            If sqlConnection.State = ConnectionState.Closed Then sqlConnection.Open()
            sqlCommand.CommandText = pStrSQL
            sqlCommand.Connection = sqlConnection
            str = Conversions.ToString(sqlCommand.ExecuteScalar())
        End If

        Return str
    End Function

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim pCustomClass As New Parameter.CustomerFacility
        Dim Result As String


        If Me.txtSecurityCode.Visible Then
            Dim connection As SqlConnection = New SqlConnection(Me.GetConnectionString())
            Dim cmdText As String = "SELECT ISNULL(securityCode,'') securityCode FROM " & Me.AppMgrDB & ".dbo.sec_msUser where loginID = '" + Me.UserApproval & "'"
            If connection.State = ConnectionState.Closed Then connection.Open()
            If Microsoft.VisualBasic.CompilerServices.Operators.CompareString(Me.txtSecurityCode.Text.Trim().ToLower(), Me.oEncrypt.encryptto(Conversions.ToString(New SqlCommand(cmdText, connection).ExecuteScalar()), "1").ToLower(), False) <> 0 Then
                Me.ShowMessage(Me.lblMessage, "Harap diisi Security Code yang benar", True)
                cboApproval.SelectedIndex = 0
                Exit Sub
            End If
            connection.Close()
        End If

        If (cboApproval.SelectedValue = "A" Or cboApproval.SelectedValue = "") And Me.isFinal = False And (cboNextPerson.SelectedValue = "Select One" Or cboNextPerson.SelectedValue = "") Then
            ShowMessage(lblMessage, "Silahkan pilih next approval person", True)
            cboApproval.SelectedIndex = 0
            Exit Sub
        End If


        With pCustomClass
            .strConnection = GetConnectionString()
            .ApprovalSchemeID = "AFAC"
            .NoFasilitas = Me.NoFasilitas
            .ApprovalNo = Me.ApprovalNo
            .ApprovalResult = cboApproval.SelectedValue
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .BusinessDate = Me.BusinessDate
            .ApprovalDate = Me.BusinessDate
            .Notes = txtNotes.Text
            .SecurityCode = txtSecurityCode.Text.Trim
            .UserApproval = Me.Loginid
            .UserSecurityCode = If(Me.UserApproval <> Me.Loginid = True, Me.Loginid, "")
            .IsEverRejected = 0
            .LoginId = Me.Loginid
            .NextPersonApproval = cboNextPerson.SelectedValue
        End With

        If cboApproval.SelectedValue = "J" Then
            If rdoDeclineFinal.SelectedValue = 1 Then

                Try
                    Result = oController.AddendumCustFacApproveSave(pCustomClass)
                    If Result <> Nothing Then
                        Response.Redirect("AddendumCustomerFacilityApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("AddendumCustomerFacilityApproval.aspx?message=Proses Approve Berhasil")
                    End If
                Catch ex As Exception

                End Try

            ElseIf rdoDeclineFinal.SelectedValue = 0 Then

                Try
                    Result = oController.AddendumCustFacApproveSaveToNextPerson(pCustomClass)
                    If Result <> Nothing Then
                        ShowMessage(lblMessage, Result, True)
                        Response.Redirect("AddendumCustomerFacilityApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("AddendumCustomerFacilityApproval.aspx?message=Proses Approve Request To Next Person Berhasil")
                    End If
                Catch ex As Exception

                End Try

            End If
        ElseIf cboApproval.SelectedValue = "A" Then
            If Me.isFinal = True Then

                Try
                    Result = oController.AddendumCustFacApproveSave(pCustomClass)
                    If Result <> Nothing Then
                        Response.Redirect("AddendumCustomerFacilityApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("AddendumCustomerFacilityApproval.aspx?message=Proses Approve Berhasil")
                    End If
                Catch ex As Exception

                End Try

            ElseIf Me.isFinal = False Then

                Try
                    Result = oController.AddendumCustFacApproveSaveToNextPerson(pCustomClass)
                    If Result <> Nothing Then
                        ShowMessage(lblMessage, Result, True)
                        Response.Redirect("AddendumCustomerFacilityApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("AddendumCustomerFacilityApproval.aspx?message=Proses Approve Request To Next Person Berhasil")
                    End If
                Catch ex As Exception

                End Try

            End If
        End If

    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("AddendumCustomerFacilityApproval.aspx")
    End Sub

    Protected Sub cboApproval_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboApproval.SelectedIndexChanged
        Dim WhereUser As String = "BranchID = '" & Me.NextPersonApproval & "'"
        If cboApproval.SelectedValue = "A" Then
            If Me.isFinal = False Then
                pnlNextPerson.Visible = True
                FillCboEmp()
            End If
        ElseIf cboApproval.SelectedValue = "J" Then
            pnlDeclineFinal.Visible = True
        End If

    End Sub

    Protected Sub FillCboEmp()
        Dim oPCReimburse As New Parameter.CustomerFacility
        Dim oData As New DataTable

        oPCReimburse.strConnection = GetConnectionString()
        oPCReimburse.LoginId = Me.NextPersonApproval
        oPCReimburse = oController.GetCboUserApproval(oPCReimburse)

        oData = oPCReimburse.ListData
        cboNextPerson.DataSource = oData
        cboNextPerson.DataTextField = "FullName"
        cboNextPerson.DataValueField = "LoginID"
        cboNextPerson.DataBind()
        cboNextPerson.Items.Insert(0, "Select One")
        cboNextPerson.Items(0).Value = "Select One"
    End Sub

    Protected Sub rdoDeclineFinal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoDeclineFinal.SelectedIndexChanged
        If rdoDeclineFinal.SelectedValue = 0 Then
            pnlDeclineFinal.Visible = True
            pnlNextPerson.Visible = True
            FillCboEmp()
        ElseIf rdoDeclineFinal.SelectedValue = 1 Then
            pnlDeclineFinal.Visible = True
        End If
    End Sub

#Region "linkTo"
    Function LinkToViewCustomerFacility(ByVal strNoFasilitas As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewCustomerFacility('" & strNoFasilitas & "','" & strBranch & "')"
    End Function

    Function LinkToViewAddendum(ByVal strNoFasilitas As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewAddendum('" & strNoFasilitas & "','" & strBranch & "')"
    End Function


#End Region


End Class