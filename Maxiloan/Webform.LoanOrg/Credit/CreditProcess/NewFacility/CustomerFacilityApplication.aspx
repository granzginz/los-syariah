﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerFacilityApplication.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerFacilityApplication" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../../../webform.UserController/ucLookUpCustomer.ascx"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Company</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
     <style >
        .multiline_textbox {
            width: 60% !important;
        }
    </style>
</head>
<body>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div runat="server" id="jlookupContent" />
 
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <asp:Label ID="lblMessage" runat="server" Visible="false" onclick="hideMessage();"></asp:Label>
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        NEW DRAW DOWN
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class=""> Nama Fasilitas</label>
                    <asp:Label ID="lblNamaFasilitas" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class=""> Nama Customer</label>
                    <input id="hdnCustomerID" type="hidden" name="hdnCustomerID" runat="server" />
                    <asp:Label ID="lblNamaCustomer" runat="server"></asp:Label>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Jenis Pembiayaan
                    </label>
                    <asp:Label ID="lblKegiatanUsaha" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Skema Pembiayaan
                    </label>
                    <asp:Label ID="lblJenisPembiyaan" runat="server"></asp:Label>
                </div>    
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Periode Pencairan
                    </label>
                    <asp:Label ID="lblPeriodePencairan" runat="server"></asp:Label>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label  class="">Jumlah Plafond</label>
                    <asp:Label ID="lblFasilitasAmount" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label  class="">
                        Flat Rate
                    </label>
                    <asp:Label ID="lblFlatRate" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Minimum Pencairan Per Batch
                    </label>
                    <asp:Label ID="lblMinimumPencairan" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Type Fasilitas
                    </label>
                    <asp:Label ID="lblFasilitasType" runat="server"></asp:Label>
                </div>    
            </div>
           <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Tanggal Pencairan
                    </label>
                    <uc1:ucdatece id="ucMutasiDate" runat="server"></uc1:ucdatece>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Jumlah Pencairan
                    </label>
                    <uc1:ucnumberformat runat="server" id="ucJumlahPencairan" ></uc1:ucnumberformat>
                </div>
            </div> 
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h5>
                        DATA PENCAIRAN</h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgPaging" runat="server" DataKeyField=""
                            CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False"
                            CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn DataField="SeqNo" HeaderText="No." ItemStyle-CssClass=""></asp:BoundColumn>
                                <asp:BoundColumn DataField="MutasiDate" HeaderText="Tanggal Mutasi" ItemStyle-CssClass="" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InstallmentAmount" HeaderText="Pencairan" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="PaidInstallment" HeaderText="Bayar Pokok" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="OutstandingPrincipal" HeaderText="OS Pokok" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="UnusedPrincipal" HeaderText="Unused" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestPeriodFrom" HeaderText="Margin Dari" ItemStyle-CssClass="" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestPeriodTo" HeaderText="Margin s/d" ItemStyle-CssClass="" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestPeriod" HeaderText="Margin Hari" ItemStyle-CssClass="" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestAmount" HeaderText="Jumlah Margin" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestAmountPaid" HeaderText="Bayar Margin" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestAmountTotal" HeaderText="Jumlah Kewajiban" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="PaidStatus" SortExpression="Status" HeaderText="Status" ItemStyle-CssClass=""></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>  
        </ContentTemplate>
    </asp:UpdatePanel>
   <%-- </asp:Content>--%>
 </form>
</body>
</html> 
