﻿Public Class FacilityFormDTab
    Inherits System.Web.UI.UserControl

    Public Property CKEEditorFormD As String
        Get
            Return CType(ViewState("CKEEditorFormD"), String)
        End Get
        Set(value As String)
            ViewState("CKEEditorFormD") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CKEEditorFormD = CKEditor1.Text
    End Sub

    Sub ViewCKE(CKExx)
        CKEditor1.Text = CKExx
        CKEditor1.Enabled = False
    End Sub
End Class