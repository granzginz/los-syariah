﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.BusinessProcess
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class AddendumCustomerFacilityApproval
    Inherits Maxiloan.Webform.WebBased

#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property FilterBy() As String
        Get
            Return CType(ViewState("FilterBy"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FilterBy") = Value
        End Set
    End Property
    Private Property BindMenu() As String
        Get
            Return CStr(ViewState("BindMenu"))
        End Get
        Set(ByVal Value As String)
            ViewState("BindMenu") = Value
        End Set
    End Property
    Public Property TotalAmount() As Double
        Get
            Return CType(ViewState("TotalAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalAmount") = Value
        End Set
    End Property

    Private Property RequestDate() As String
        Get
            Return CStr(ViewState("RequestDate"))
        End Get
        Set(ByVal Value As String)
            ViewState("RequestDate") = Value
        End Set
    End Property

#End Region
#Region "PrivateConst"
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private oCustomClass As New Parameter.CustomerFacility
    Private oController As New CustomerFacilityController
    Private m_controller As New DataUserControlController

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "APPROVALFACADN"
        pnlSearch.Visible = True
        pnlDatagrid.Visible = False

        If Not IsPostBack Then
            Dim dtbranch As New DataTable
            dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
            With cboParent
                If Me.IsHoBranch Then
                    .DataSource = m_controller.GetBranchAll(GetConnectionString)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    .Items.Insert(1, "ALL")
                    .Items(0).Value = "ALL"
                Else
                    .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                    .DataValueField = "ID"
                    .DataTextField = "Name"
                    .DataBind()
                    .Items.Insert(0, "Select One")
                    .Items(0).Value = "0"
                    Dim strBranch() As String
                    strBranch = Split(Me.sesBranchId, ",")
                    If UBound(strBranch) > 0 Then
                        .Items.Insert(1, "ALL")
                        .Items(1).Value = "ALL"
                    End If
                End If
            End With

            BindComboReqBy()
            BindComboApprovedBy()

            Me.CmdWhere = ""
            Me.SortBy = ""

            If Request.QueryString("message") <> "" Then

                ShowMessage(lblMessage, Request.QueryString("message"), False)
            End If
            If Request.QueryString("message1") <> "" Then

                ShowMessage(lblMessage, Request.QueryString("message"), True)
            End If
        End If

    End Sub
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            rvGo.MaximumValue = CType(totalPages, String)
            'Trim(lblTotPage.Text)
            '
        End If
        lbltotrec.Text = recordCount.ToString
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select

        'SavePage()
        Me.BindMenu = ""
        BindGridAprroval(Me.CmdWhere, Me.SortBy)
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And (CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer)) Then
                    'SavePage()
                    Me.BindMenu = ""
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridAprroval(Me.CmdWhere, Me.SortBy)
                End If
            End If
        End If
        pnlSearch.Visible = True
        pnlDatagrid.Visible = True

    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Me.TotalAmount = 0.0
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridAprroval(Me.CmdWhere, Me.SortBy)
        pnlDatagrid.Visible = True
    End Sub

#End Region

#Region " BindGrid"

    Sub BindGridAprroval(ByVal cmdWhere As String, ByVal sortBy As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oInqPCReimburse As New Parameter.CustomerFacility

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = sortBy
        End With

        oInqPCReimburse = oController.AddendumCustomerFacilityApprovalList(oCustomClass)

        With oInqPCReimburse
            lbltotrec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInqPCReimburse.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = sortBy
        DtgCustomerFacility.DataSource = dtvEntity
        Try
            DtgCustomerFacility.DataBind()
        Catch
            DtgCustomerFacility.CurrentPageIndex = 0
            DtgCustomerFacility.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region
    Private Sub DtgCustomerFacility_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgCustomerFacility.ItemDataBound
        Dim lblAmount As Label
        Dim lblTotAmount As Label
        Dim lblRequestDate As Label
        Dim hyNoFasilitas As HyperLink
        Dim hypNoFasilitasAddendum As HyperLink

        If e.Item.ItemIndex >= 0 Then

            hyNoFasilitas = CType(e.Item.FindControl("hyNoFasilitas"), HyperLink)
            hyNoFasilitas.NavigateUrl = LinkToViewCustomerFacility(hyNoFasilitas.Text.Trim, "ACCMNT", cboParent.SelectedItem.Value.Trim)

            hypNoFasilitasAddendum = CType(e.Item.FindControl("hypNoFasilitasAddendum"), HyperLink)
            hypNoFasilitasAddendum.NavigateUrl = LinkToViewAddendum(hypNoFasilitasAddendum.Text.Trim, "ACCMNT", cboParent.SelectedItem.Value.Trim)

            lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
            Me.TotalAmount += CDbl(lblAmount.Text)

            lblRequestDate = CType(e.Item.FindControl("lblRequestDate"), Label)
            Me.RequestDate = lblRequestDate.Text

        End If

        If e.Item.ItemType = ListItemType.Footer Then
            lblTotAmount = CType(e.Item.FindControl("lblTotAmount"), Label)
            lblTotAmount.Text = FormatNumber(Me.TotalAmount, 2)
        End If
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If CheckFeature(Me.Loginid, Me.FormID, "SRCH", "MAXILOAN") Then
            Dim cmdwhere As String
            Dim filterby As String
            Me.SortBy = ""
            Me.CmdWhere = ""
            cmdwhere = ""
            filterby = ""
            Me.TotalAmount = 0.0

            If cboSearchBy.SelectedItem.Value.Trim <> "0" Then
                If txtSearchBy.Text.Trim <> "" Then
                    If cboSearchBy.SelectedItem.Value.Trim = "NoFasilitas" Then
                        If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                            cmdwhere = cmdwhere + "AddendumCustomerFacility.NoFasilitas like '" & txtSearchBy.Text.Trim & "' and "
                        Else
                            cmdwhere = cmdwhere + "AddendumCustomerFacility.NoFasilitas = '" & txtSearchBy.Text.Trim & "' and "
                        End If
                        filterby = "No Fasilitas = " & txtSearchBy.Text.Trim & " and "
                    ElseIf cboSearchBy.SelectedItem.Value.Trim = "Description" Then
                        If Right(txtSearchBy.Text.Trim, 1) = "%" Then
                            cmdwhere = cmdwhere + "AddendumCustomerFacility.NamaFasilitas like '" & txtSearchBy.Text.Trim & "' and "
                        Else
                            cmdwhere = cmdwhere + "AddendumCustomerFacility.NamaFasilitas = '" & txtSearchBy.Text.Trim & "' and "
                        End If
                        filterby = "Description = " & txtSearchBy.Text.Trim & " and "
                    End If
                End If
            End If

            If cboStatus.SelectedItem.Value.Trim <> "ALL" Then
                cmdwhere = cmdwhere + "SAS.IsFinal = '" & cboStatus.SelectedItem.Value.Trim & "' and "
            End If

            If cboHasilApproval.SelectedItem.Value.Trim <> "ALL" Then
                cmdwhere = cmdwhere + "SAS.ApprovalResult = '" & cboHasilApproval.SelectedItem.Value.Trim & "' and "
            End If

            If cboParent.SelectedItem.Value.Trim <> "ALL" Then
                cmdwhere = cmdwhere + "AddendumCustomerFacility.branchID = '" & cboParent.SelectedItem.Value.Trim & "' and "
            Else
                If cmdwhere.Trim <> "" Then
                    cmdwhere = Left(cmdwhere, Len(cmdwhere.Trim) - 4)
                End If
            End If

            If cboRequestFrom.SelectedItem.Value.Trim <> "Select One" Then
                cmdwhere = cmdwhere + "SAS.UserRequest = '" & cboRequestFrom.SelectedItem.Value.Trim & "' and "
            End If

            If cboApprBy.SelectedItem.Value.Trim <> "Select One" Then
                cmdwhere = cmdwhere + "SAS.UserApproval = '" & cboApprBy.SelectedItem.Value.Trim & "'"
            Else
                If cmdwhere.Trim <> "" Then
                    cmdwhere = Left(cmdwhere, Len(cmdwhere.Trim) - 4)
                End If
            End If

            Me.CmdWhere = cmdwhere
            If filterby <> "" Then
                filterby = Left(filterby, Len(filterby.Trim) - 4)
            End If
            Me.FilterBy = filterby
            BindGridAprroval(Me.CmdWhere, Me.SortBy)
            pnlSearch.Visible = True
            pnlDatagrid.Visible = True
        End If

    End Sub

    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Server.Transfer("AddendumCustomerFacilityApproval.aspx")
    End Sub

    Private Sub DtgCustomerFacility_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles DtgCustomerFacility.SortCommand
        Me.TotalAmount = 0.0
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression + " ASC"
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridAprroval(Me.CmdWhere, Me.SortBy)
    End Sub

    Sub BindComboReqBy()
        Dim oPCReimburse As New Parameter.CustomerFacility
        Dim oData As New DataTable

        oPCReimburse.strConnection = GetConnectionString()
        oPCReimburse.ApprovalSchemeID = "AFAC"
        oPCReimburse = oController.GetCboUserAprPCAll(oPCReimburse)

        oData = oPCReimburse.ListData
        cboRequestFrom.DataSource = oData
        cboRequestFrom.DataTextField = "FullName"
        cboRequestFrom.DataValueField = "LoginID"
        cboRequestFrom.DataBind()
        cboRequestFrom.Items.Insert(0, "Select One")
        cboRequestFrom.Items(0).Value = "Select One"

    End Sub

    Private Sub BindComboApprovedBy()
        Dim oPCReimburse As New Parameter.CustomerFacility
        Dim oData As New DataTable

        oPCReimburse.strConnection = GetConnectionString()
        oPCReimburse.ApprovalSchemeID = "AFAC"
        oPCReimburse = oController.GetCboUserAprPCAll(oPCReimburse)

        oData = oPCReimburse.ListData
        cboApprBy.DataSource = oData
        cboApprBy.DataTextField = "FullName"
        cboApprBy.DataValueField = "LoginID"
        cboApprBy.DataBind()
        cboApprBy.Items.Insert(0, "Select One")
        cboApprBy.Items(0).Value = "Select One"

    End Sub

#Region "linkTo"
    Function LinkToViewCustomerFacility(ByVal strRequestNo As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewCustomerFacility('" & strRequestNo & "','" & strBranch & "')"
    End Function

    Function LinkToViewAddendum(ByVal strRequestNo As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewAddendum('" & strRequestNo & "','" & strBranch & "')"
    End Function
#End Region


End Class