﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AddendumCustomerFacilityAdd.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.AddendumCustomerFacilityAdd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../../../webform.UserController/ucLookUpCustomer.ascx"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET, Version=3.6.6.2, Culture=neutral, PublicKeyToken=e379cdf2f8354999" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="FacilityFormETab.ascx" TagName="formE" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AddendumCustomerFacilityAdd</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>


     <style>
        .multiline_textbox {
            width: 60% !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function OpenWinViewCustomerFacility(id) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewFacility/ViewCustomerFacility.aspx?NoFasilitas=' + id, 'CustomerLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
    
<body>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div runat="server" id="jlookupContent" />
     
 
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <asp:Label ID="lblMessage" runat="server" Visible="false" onclick="hideMessage();"></asp:Label>
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        ADDENDUM FACILITY
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label> No Fasilitas</label>
                    <asp:LinkButton ID="lblNoFasilitas"  runat="server" width="300"></asp:LinkButton>
                </div>
                <div class="form_right">
                    <label class="label_req"> No Fasilitas Addendum</label>
                    <asp:Textbox ID="txtNoFasilitasAddendum"  runat="server" width="300"></asp:Textbox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                       display="Dynamic"
                       ControlToValidate="txtNoFasilitasAddendum" CssClass="validator_general" ErrorMessage="No Fasilitas Addendum Harap Diisi" InitialValue=""></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label> Nama Fasilitas</label>
                    <asp:Label ID="lblNamaFasilitas"  runat="server" width="400"></asp:Label>
                    <asp:Label ID="lblCustomerId"  runat="server" width="400" visible="false"></asp:Label>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label>Tanggal Fasilitas</label>
                    <asp:Label id="lblFacilityStartDate" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label  runat="server" id="Label2" class="label_req">
                        Tanggal Akhir Fasilitas
                    </label>
                    <uc1:ucdatece id="UcFacilityMaturityDate" runat="server"></uc1:ucdatece>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label> Nama Customer</label>
                    <asp:Label ID="lblNmCustomer"  runat="server" width="400"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Jenis Pembiayaan</label>  
                        <asp:Label ID="lblKegiatanUsaha"  runat="server" width="400"></asp:Label>
                        <asp:DropDownList ID="cboKegiatanUsaha" runat="server" AutoPostBack="true"  Width="300px" visible="false"/>
                         <asp:RequiredFieldValidator ID="Requiredfieldvalidator65" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboKegiatanUsaha" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Skema Pembiayaan
                        </label>
                        <asp:Label ID="lblJenisPembiyaan"  runat="server" width="400"></asp:Label>
                        <asp:DropDownList runat="server" ID="cboJenisPembiyaan"  Width="300px" AutoPostBack="true" visible="false"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboJenisPembiyaan" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>Application Module</label>
                        <asp:Label ID="lblApplicationModule"  runat="server" width="400"></asp:Label>
                        <asp:DropDownList runat="server" ID="cboApplicationModule" onChange="cboApplicationModule_onchange(this);" visible="false">
                            <asp:ListItem Value="SelectOne">Select One</asp:ListItem>
                            <asp:ListItem Value="FACT">FACTORING</asp:ListItem>
                            <asp:ListItem Value="MDKJ">MODAL KERJA</asp:ListItem>
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="rewCboApplicationModule" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboApplicationModule" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="label_req">
                        Periode Pencairan
                    </label>
                    <asp:Label id="lblDrawDownStartDate" runat="server"></asp:Label>
                    <label class="label_auto">
                        s/d
                    </label>
                    <uc1:ucdatece id="ucDrawDownMaturityDate" runat="server"></uc1:ucdatece>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label  runat="server" id="lblFasilitasAmount" class="label_req">Jumlah Plafond</label>
                    <uc1:ucnumberformat runat="server" id="ucFasilitasAmount" ></uc1:ucnumberformat>
                </div>
                <div class="form_right">
                    <label  runat="server" id="lblEffectiveRate" class="label_req">
                        Suku Margin Effektive
                    </label>
                    <uc1:ucnumberformat runat="server" id="UcEffectiveRate" TextCssClass="small_text"></uc1:ucnumberformat> %
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Minimum Pencairan Per Batch
                    </label>
                    <uc1:ucnumberformat id="txtMaxDD" runat="server" />
                </div>
                <div class="form_right" id="retensi">
                    <div id="panelretensi" runat="server">
                    <label  runat="server" id="Label3" class="label_req">
                        Retensi
                    </label>
                    <uc1:ucnumberformat runat="server" id="UcRetensi" TextCssClass="small_text"></uc1:ucnumberformat> %
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label  runat="server" id="lblFasilitasType">
                        Type Fasilitas
                    </label>
                    <asp:DropDownList runat="server" ID="listFasilitasType">
                        <asp:ListItem Value="R">Revolving</asp:ListItem>
                        <asp:ListItem Value="N">Non Revolving</asp:ListItem>
                    </asp:DropDownList>
                </div> 
                <div class="form_right" >
                    <label class="label_req">Persentasi Denda</label>
                    <uc1:ucNumberFormat runat="server" ID="ucLateChargePersen" TextCssClass="small_text" /> per mile
                </div>   
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Admin Fee
                    </label>
                    <uc1:ucnumberformat id="txtAdminFee" runat="server" />
                </div>
                <div class="form_right">
                    <label>Jumlah Drawdown</label>
                    <uc1:ucnumberformat runat="server" id="ucDrawdownAmount" ></uc1:ucnumberformat>
                </div>
            </div>        
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Commintment Fee
                    </label>
                    <uc1:ucnumberformat id="txtCommitmentFee" runat="server" />
                </div>
                <div class="form_right">
                    <label>Jumlah Bayar</label>
                    <uc1:ucnumberformat runat="server" id="ucPaidAmount" ></uc1:ucnumberformat>
                </div>
            </div>        
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Provision Fee / Annual Fee
                    </label>
                    <uc1:ucnumberformat id="UcProvisionFee" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Notary Fee
                    </label>
                    <uc1:ucnumberformat id="UcNotaryFee" runat="server" />
                </div>
            </div>        
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Handling Fee
                    </label>
                    <uc1:ucnumberformat id="UcHandlingFee" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <uc1:ucnumberformat id="UcAsuransiKredit" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Biaya Polis
                    </label>
                    <uc1:ucnumberformat id="UcBiayaPolis" runat="server" />
                </div>
            </div>        
            <div class="form_title">
                <div class="form_left">
                    <h4>
                        <asp:Label ID="Label5" runat="server"></asp:Label>
                        MEMO
                    </h4>
                </div>
            </div>
        <div class="form_box">
        <div class="form_single" id="tabMPPNew" runat="server" >
            <asp:TabContainer runat="server" ID="Tabs" style="height: auto;" ActiveTabIndex="0" Width="100%">
                <asp:TabPanel runat="server" ID="pnlTabFormE" HeaderText="MEMO ADDENDUM">
                    <contenttemplate> 
                         <uc1:formE id="TabFormE" runat="server"/>  
                    </contenttemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </div>
        </div>
            <div class="form_box" runat="server" id="panelApproval">
                <div class="form_left">
                    <label class="label_req"> Akan diSetujui Oleh </label>
                    <asp:DropDownList ID="cboApprovedBy" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Akan disetujui Oleh" Display="Dynamic" InitialValue="0" ControlToValidate="cboApprovedBy" CssClass="validator_general" />-
                </div>
            </div>          
            <div class="form_button">
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button blue" />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
   <%-- </asp:Content>--%>
 </form>
    <script type="text/javascript">
            function cboApplicationModule_onchange() {
                var _AppModule = $('#cboApplicationModule').val();

                if (_AppModule === 'MDKJ') {
                    $('#retensi').css("visibility", "hidden");
                    $('#UcRetensi').attr("style", "display : none");
                    ValidatorEnable(UcRetensi, false);
                    $('#UcRetensi').hide();
                }
                    else
                {
                    $('#retensi').css("visibility", "");
                    $('#UcRetensi').attr("style", "display : none");
                    ValidatorEnable(UcRetensi, true);
                    $('#UcRetensi').show();
                }
            }
    </script>
</body>
</html> 
