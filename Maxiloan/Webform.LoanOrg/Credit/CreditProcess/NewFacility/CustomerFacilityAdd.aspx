﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerFacilityAdd.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerFacilityAdd" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../../../webform.UserController/ucLookUpCustomer.ascx"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET, Version=3.6.6.2, Culture=neutral, PublicKeyToken=e379cdf2f8354999" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="FacilityFormATab.ascx" TagName="formA" TagPrefix="uc1" %>
<%@ Register Src="FacilityFormBTab.ascx" TagName="formB" TagPrefix="uc2" %>
<%@ Register Src="FacilityFormCTab.ascx" TagName="formC" TagPrefix="uc3" %>
<%@ Register Src="FacilityFormDTab.ascx" TagName="formD" TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Company</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>


     <style>
        .multiline_textbox {
            width: 60% !important;
        }
    </style>
</head>
    
<body>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div runat="server" id="jlookupContent" />
     
 
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <asp:Label ID="lblMessage" runat="server" Visible="false" onclick="hideMessage();"></asp:Label>
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        CREATE NEW FACILITY
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req"> No Fasilitas</label>
                    <asp:TextBox ID="txtNoFasilitas" Enabled="true" runat="server" width="300"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req"> Nama Fasilitas</label>
                    <asp:TextBox ID="txtNamaFasilitas" Enabled="true" runat="server" width="400"></asp:TextBox>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label  runat="server" id="Label1" class="label_req">Tanggal Fasilitas</label>
                    <uc1:ucdatece id="UcFacilityStartDate" runat="server"></uc1:ucdatece>
                </div>
                <div class="form_right">
                    <label  runat="server" id="Label2" class="label_req">
                        Tanggal Akhir Fasilitas
                    </label>
                    <uc1:ucdatece id="UcFacilityMaturityDate" runat="server"></uc1:ucdatece>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req"> Nama Customer</label>
                    <input id="hdnCustomerID" type="hidden" name="hdnCustomerID" runat="server" />
                    <asp:TextBox ID="txtNmCustomer" Enabled="false" runat="server" width="400"></asp:TextBox>
                        <asp:Button ID="btnLookupCustomer" runat="server" CausesValidation="False" 
                            CssClass="small buttongo blue" Text="..." />
                        <uc1:uclookupcustomer id="ucLookUpCustomer1" runat="server" oncatselected="CatSelectedCustomer" />
                        </uc1:ucLookUpCustomer>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_req">Jenis Pembiayaan</label>  
                        <asp:DropDownList ID="cboKegiatanUsaha" runat="server" AutoPostBack="true"  Width="300px"/>
                         <asp:RequiredFieldValidator ID="Requiredfieldvalidator65" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboKegiatanUsaha" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Skema Pembiayaan
                        </label>
                        <asp:DropDownList runat="server" ID="cboJenisPembiyaan"  Width="300px" AutoPostBack="true"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboJenisPembiyaan" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Application Module</label>
                        <asp:DropDownList runat="server" ID="cboApplicationModule" onChange="cboApplicationModule_onchange(this);">
                            <asp:ListItem Value="SelectOne">Select One</asp:ListItem>
                            <asp:ListItem Value="FACT">FACTORING</asp:ListItem>
                            <asp:ListItem Value="MDKJ">MODAL KERJA</asp:ListItem>
                            <asp:ListItem Value="INV">INVESTASI</asp:ListItem>
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="rewCboApplicationModule" runat="server" 
                            display="Dynamic"
                            ControlToValidate="cboApplicationModule" CssClass="validator_general" ErrorMessage="*" InitialValue="SelectOne"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Periode Pencairan
                    </label>
                    <uc1:ucdatece id="ucDrawDownStartDate" runat="server"></uc1:ucdatece>
                    <label class="label_auto">
                        s/d
                    </label>
                    <uc1:ucdatece id="ucDrawDownMaturityDate" runat="server"></uc1:ucdatece>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label  runat="server" id="lblFasilitasAmount" class="label_req">Jumlah Plafond</label>
                    <uc1:ucnumberformat runat="server" id="ucFasilitasAmount" ></uc1:ucnumberformat>
                </div>
                <div class="form_right">
                    <label  runat="server" id="lblEffectiveRate" class="label_req">
                        Suku Margin Effektive
                    </label>
                    <uc1:ucnumberformat runat="server" id="UcEffectiveRate" TextCssClass="small_text"></uc1:ucnumberformat> %
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="">
                        Minimum Pencairan Per Batch
                    </label>
                    <uc1:ucnumberformat id="txtMaxDD" runat="server" />
                </div>
                <div class="form_right" id="retensi">
                    <div id="panelretensi" runat="server">
                    <label  runat="server" id="Label3" class="label_req">
                        Retensi
                    </label>
                    <uc1:ucnumberformat runat="server" id="UcRetensi" TextCssClass="small_text"></uc1:ucnumberformat> %
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label  runat="server" id="lblFasilitasType">
                        Type Fasilitas
                    </label>
                    <asp:DropDownList runat="server" ID="listFasilitasType">
                        <asp:ListItem Value="R">Revolving</asp:ListItem>
                        <asp:ListItem Value="N">Non Revolving</asp:ListItem>
                    </asp:DropDownList>
                </div> 
                <div class="form_right" >
                    <label class="label_req">Persentasi Ta'widh</label>
                    <uc1:ucNumberFormat runat="server" ID="ucLateChargePersen" TextCssClass="small_text" /> per mile
                </div>   
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Admin Fee
                    </label>
                    <uc1:ucnumberformat id="txtAdminFee" runat="server" />
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Commintment Fee
                    </label>
                    <uc1:ucnumberformat id="txtCommitmentFee" runat="server" />
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Provision Fee / Annual Fee
                    </label>
                    <uc1:ucnumberformat id="UcProvisionFee" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Notary Fee
                    </label>
                    <uc1:ucnumberformat id="UcNotaryFee" runat="server" />
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Handling Fee
                    </label>
                    <uc1:ucnumberformat id="UcHandlingFee" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <uc1:ucnumberformat id="UcAsuransiKredit" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Biaya Polis
                    </label>
                    <uc1:ucnumberformat id="UcBiayaPolis" runat="server" />
                </div>
            </div>        
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        <asp:Label ID="Label5" runat="server"></asp:Label>
                        MEMO PENGUSULAN PEMBIAYAAN (MPP)
                    </h4>
                </div>
            </div>
        <div class="form_box">
        <div class="form_single" id="tabMPPNew" runat="server" >
            <asp:TabContainer runat="server" ID="Tabs" style="height: auto;" ActiveTabIndex="0" Width="100%">
                <asp:TabPanel runat="server" ID="pnlTabFormA" HeaderText="FORM A">
                    <contenttemplate> 
                         <uc1:formA id="TabFormA" runat="server"/>  
                    </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabFormB" HeaderText="FORM B">
                    <contenttemplate>
                          <uc2:formB id="TabFormB" runat="server"/>  
                    </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabFormC" HeaderText="FORM C">
                    <contenttemplate>
                         <uc3:formC id="TabFormC" runat="server"/>                    
                    </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabFormD" HeaderText="FORM D">
                    <contenttemplate>
                          <uc4:formD  id="TabFormD" runat="server"/> 
                    </contenttemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </div>
        </div>
            <div class="form_box" runat="server" id="panelApproval">
                <div class="form_left">
                    <label class="label_req"> Akan diSetujui Oleh </label>
                    <asp:DropDownList ID="cboApprovedBy" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Akan disetujui Oleh" Display="Dynamic" InitialValue="0" ControlToValidate="cboApprovedBy" CssClass="validator_general" />-
                </div>
            </div>          
            <div class="form_button">
                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button blue" />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray" CausesValidation="False" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
   <%-- </asp:Content>--%>
 </form>
    <script type="text/javascript">
            function cboApplicationModule_onchange() {
                var _AppModule = $('#cboApplicationModule').val();

                if (_AppModule === 'MDKJ') {
                    $('#retensi').css("visibility", "hidden");
                    $('#UcRetensi').attr("style", "display : none");
                    ValidatorEnable(UcRetensi, false);
                    $('#UcRetensi').hide();
                }
                    else
                {
                    $('#retensi').css("visibility", "");
                    $('#UcRetensi').attr("style", "display : none");
                    ValidatorEnable(UcRetensi, true);
                    $('#UcRetensi').show();
                }
            }
    </script>
</body>
</html> 
