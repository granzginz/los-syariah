﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
#End Region

Public Class CustomerFacilityApprovalExec
    Inherits Maxiloan.Webform.WebBased
#Region "Constanta"
    Private m_controller As New SupplierController
    Private oCustomClass As New Parameter.CustomerFacility
    Private oController As New CustomerFacilityController
#End Region

#Region "Property"

    Property NoFasilitas() As String
        Get
            Return ViewState("NoFasilitas").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitas") = Value
        End Set
    End Property

    Public Property TotalAmount() As Double
        Get
            Return CType(ViewState("TotalAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotalAmount") = Value
        End Set
    End Property

    Property RequestDate() As String
        Get
            Return ViewState("RequestDate").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("RequestDate") = Value
        End Set
    End Property

    Property isFinal() As Boolean
        Get
            Return ViewState("isFinal").ToString
        End Get
        Set(ByVal Value As Boolean)
            ViewState("isFinal") = Value
        End Set
    End Property

    Property NextPersonApproval() As String
        Get
            Return ViewState("NextPersonApproval").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NextPersonApproval") = Value
        End Set
    End Property

    Property UserApproval() As String
        Get
            Return ViewState("UserApproval").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("UserApproval") = Value
        End Set
    End Property

    Property ApprovalNo() As String
        Get
            Return ViewState("ApprovalNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApprovalNo") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.ApprovalNo = Request.QueryString("ApprovalNo")
            Me.NoFasilitas = Request.QueryString("NoFasilitas")
            Me.TotalAmount = Request.QueryString("TotalAmount")
            Me.RequestDate = Request.QueryString("RequestDate")
        End If

        lblMessage.Visible = False
        Dim oInqPCReimburse As New Parameter.CustomerFacility
        Dim pInqPCReimburse As New Parameter.CustomerFacility
        With oCustomClass
            .strConnection = GetConnectionString()
            .NoFasilitas = Me.NoFasilitas
            .ApprovalSchemeID = "RCA5"
            .LoginId = Me.Loginid
            .IsReRequest = 0
        End With

        'jika login yang approved bukan login yang semestinya, maka munculkan security code
        oInqPCReimburse = oController.CustFacApproveIsValidApproval(oCustomClass)
        Me.UserApproval = LTrim(RTrim(oInqPCReimburse.UserApproval))

        If Me.UserApproval <> Me.Loginid = True Then
            pnlSecurityCode.Visible = True
        Else
            pnlSecurityCode.Visible = False
        End If

        'Cek userscheme apakah bisa final?
        pInqPCReimburse = oController.CustFacApproveisFinal(oCustomClass)
        Me.NextPersonApproval = LTrim(RTrim(pInqPCReimburse.NextPersonApproval))
        If pInqPCReimburse.IsFinal = False Then
            Me.isFinal = False
        Else
            Me.isFinal = True
        End If

        BindData()
        BindgridApprovalHistory()
        pnlDeclineFinal.Visible = False
        pnlNextPerson.Visible = False
    End Sub

    Private Sub BindgridApprovalHistory()
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objconnection
            objCommand.CommandText = "spApprovalHistoryList"
            objCommand.Parameters.Add("@ApprovalNo", SqlDbType.VarChar, 50).Value = Me.ApprovalNo
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objread = objCommand.ExecuteReader()
            dtgHistory.DataSource = objread
            dtgHistory.DataBind()
            objread.Close()
            If dtgHistory.Items.Count > 0 Then
                dtgHistory.Visible = True
            Else
                dtgHistory.Visible = False
            End If
        Catch ex As Exception
            Trace.Write(ex.Message)
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub

    Sub BindData()
        Dim hyNoFasilitasa As HyperLink

        hypNoFasilitas.Text = Me.NoFasilitas
        lblAmount.Text = FormatNumber(Me.TotalAmount, 2)
        lblRequestDate.Text = Me.RequestDate

        hyNoFasilitasa = CType((hypNoFasilitas), HyperLink)
        hyNoFasilitasa.NavigateUrl = LinkToViewCustomerFacility(hyNoFasilitasa.Text.Trim, "ACCMNT", "ALL")
    End Sub

    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Dim pCustomClass As New Parameter.CustomerFacility
        Dim Result As String

        If cboApproval.SelectedValue = "A" And Me.isFinal = False And cboNextPerson.SelectedValue = "Select One" Then
            ShowMessage(lblMessage, "Silahkan pilih next approval person", True)
            cboApproval.SelectedIndex = 0
            Exit Sub
        End If

        With pCustomClass
            .strConnection = GetConnectionString()
            .ApprovalSchemeID = "RCA5"
            .NoFasilitas = Me.NoFasilitas
            .ApprovalResult = cboApproval.SelectedValue
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .BusinessDate = Me.BusinessDate
            .ApprovalDate = Me.BusinessDate
            .notes = txtNotes.Text
            .SecurityCode = txtSecurityCode.Text.Trim
            .UserApproval = Me.Loginid
            .UserSecurityCode = If(Me.UserApproval <> Me.Loginid = True, Me.Loginid, "")
            .IsEverRejected = 0
            .LoginId = Me.Loginid
            .NextPersonApproval = cboNextPerson.SelectedValue
        End With

        If cboApproval.SelectedValue = "J" Then
            If rdoDeclineFinal.SelectedValue = 1 Then

                Try
                    Result = oController.CustFacApproveSave(pCustomClass)
                    If Result <> Nothing Then
                        Response.Redirect("CustomerFacilityApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("CustomerFacilityApproval.aspx?message=Proses Approve Berhasil")
                    End If
                Catch ex As Exception

                End Try

            ElseIf rdoDeclineFinal.SelectedValue = 0 Then

                Try
                    Result = oController.CustFacApproveSaveToNextPerson(pCustomClass)
                    If Result <> Nothing Then
                        ShowMessage(lblMessage, Result, True)
                        Response.Redirect("CustomerFacilityApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("CustomerFacilityApproval.aspx?message=Proses Approve Request To Next Person Berhasil")
                    End If
                Catch ex As Exception

                End Try

            End If
        ElseIf cboApproval.SelectedValue = "A" Then
            If Me.isFinal = True Then

                Try
                    Result = oController.CustFacApproveSave(pCustomClass)
                    If Result <> Nothing Then
                        Response.Redirect("CustomerFacilityApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("CustomerFacilityApproval.aspx?message=Proses Approve Berhasil")
                    End If
                Catch ex As Exception

                End Try

            ElseIf Me.isFinal = False Then

                Try
                    Result = oController.CustFacApproveSaveToNextPerson(pCustomClass)
                    If Result <> Nothing Then
                        ShowMessage(lblMessage, Result, True)
                        Response.Redirect("CustomerFacilityApproval.aspx?message1=" & Result & "")
                        Exit Sub
                    Else
                        Response.Redirect("CustomerFacilityApproval.aspx?message=Proses Approve Request To Next Person Berhasil")
                    End If
                Catch ex As Exception

                End Try

            End If
        End If

    End Sub

    Private Sub BtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Response.Redirect("CustomerFacilityApproval.aspx")
    End Sub

    Protected Sub cboApproval_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboApproval.SelectedIndexChanged
        Dim WhereUser As String = "BranchID = '" & Me.NextPersonApproval & "'"
        If cboApproval.SelectedValue = "A" Then
            If Me.isFinal = False Then
                pnlNextPerson.Visible = True
                FillCboEmp()
            End If
        ElseIf cboApproval.SelectedValue = "J" Then
            pnlDeclineFinal.Visible = True
        End If

    End Sub

    Protected Sub FillCboEmp()
        Dim oPCReimburse As New Parameter.CustomerFacility
        Dim oData As New DataTable

        oPCReimburse.strConnection = GetConnectionString()
        oPCReimburse.LoginId = Me.NextPersonApproval
        oPCReimburse = oController.GetCboUserApproval(oPCReimburse)

        oData = oPCReimburse.ListData
        cboNextPerson.DataSource = oData
        cboNextPerson.DataTextField = "FullName"
        cboNextPerson.DataValueField = "LoginID"
        cboNextPerson.DataBind()
        cboNextPerson.Items.Insert(0, "Select One")
        cboNextPerson.Items(0).Value = "Select One"
    End Sub

    Protected Sub rdoDeclineFinal_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoDeclineFinal.SelectedIndexChanged
        If rdoDeclineFinal.SelectedValue = 0 Then
            pnlDeclineFinal.Visible = True
            pnlNextPerson.Visible = True
            FillCboEmp()
        ElseIf rdoDeclineFinal.SelectedValue = 1 Then
            pnlDeclineFinal.Visible = True
        End If
    End Sub

#Region "linkTo"
    Function LinkToViewCustomerFacility(ByVal strNoFasilitas As String, ByVal strStyle As String, ByVal strBranch As String) As String
        Return "javascript:OpenWinViewCustomerFacility('" & strNoFasilitas & "','" & strBranch & "')"
    End Function
#End Region


End Class