﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Data.SqlClient
#End Region


Public Class ViewCustomerFacility
    Inherits Maxiloan.Webform.WebBased
#Region "Property"

    Protected WithEvents ucLookUpCustomer1 As ucLookUpCustomer
    Protected WithEvents UcFacilityStartDate As ucDateCE
    Protected WithEvents UcFacilityMaturityDate As ucDateCE
    Protected WithEvents ucDrawDownStartDate As ucDateCE
    Protected WithEvents ucDrawDownMaturityDate As ucDateCE
    Protected WithEvents ucFasilitasAmount As ucNumberFormat
    Protected WithEvents UcEffectiveRate As ucNumberFormat
    Protected WithEvents UcRetensi As ucNumberFormat
    Protected WithEvents txtMaxDD As ucNumberFormat
    Protected WithEvents txtAdminFee As ucNumberFormat
    Protected WithEvents txtCommitmentFee As ucNumberFormat
    Protected WithEvents UcProvisionFee As ucNumberFormat
    Protected WithEvents UcNotaryFee As ucNumberFormat
    Protected WithEvents UcHandlingFee As ucNumberFormat
    Protected WithEvents UcAsuransiKredit As ucNumberFormat
    Protected WithEvents UcBiayaPolis As ucNumberFormat
    Protected WithEvents ucLateChargePersen As ucNumberFormat

    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New CustomerFacilityController
    Private ocustomclass As New Parameter.CustomerFacility
    Private m_controller As New ProductController

    Private Property NoFasilitas() As String
        Get
            Return CType(ViewState("NoFasilitas"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitas") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property

    Property ApprovalNo() As String
        Get
            Return ViewState("ApprovalNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApprovalNo") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If CheckFeature(Me.Loginid, "CustomerFacility", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        If Not Page.IsPostBack Then
            Me.NoFasilitas = Request("NoFasilitas")
            BindEdit(NoFasilitas)
            BindgridApprovalHistory()
        End If
    End Sub

    Sub BindEdit(NoFasilitas)

        If Me.NoFasilitas <> "" Then
            With ocustomclass
                .BranchId = Me.BranchID
                .strConnection = GetConnectionString()
                .NoFasilitas = Me.NoFasilitas
            End With
            ocustomclass = m_controllerApp.GetFacilityDetail(ocustomclass)

            lblNoFasilitas.Text = ocustomclass.NoFasilitas
            lblNamaFasilitas.Text = ocustomclass.NamaFasilitas
            lblFacilityStartDate.Text = ocustomclass.FacilityStartDate
            lblFacilityMaturityDate.Text = ocustomclass.FacilityMaturityDate
            lblNmCustomer.Text = ocustomclass.CustomerName
            lblKegiatanUsaha.Text = ocustomclass.KegiatanUsaha
            lblJenisPembiayaan.Text = ocustomclass.JenisPembiayaan
            lblApplicationModule.Text = ocustomclass.ApplicationModule
            lblDrawDownStartDate.Text = ocustomclass.DrawDownStartDate
            lblDrawDownMaturityDate.Text = ocustomclass.DrawDownMaturityDate
            lblFasilitasAmount.Text = FormatNumber(ocustomclass.FasilitasAmount, 2)
            lblEffectiveRate.Text = ocustomclass.EffectiveRate
            lblMaxDD.Text = FormatNumber(ocustomclass.MinimumPencairan, 2)
            lblRetensi.Text = ocustomclass.Retensi
            lblLatechargesPercnetage.Text = ocustomclass.LateChargePersen
            lblAdminFee.Text = FormatNumber(ocustomclass.AdminFee, 2)
            lblCommitmentFee.Text = FormatNumber(ocustomclass.CommitmentFee, 2)
            lblProvisionFee.Text = FormatNumber(ocustomclass.ProvisionFee, 2)
            lblNotaryFee.Text = FormatNumber(ocustomclass.NotaryFee, 2)
            lblHandlingFee.Text = FormatNumber(ocustomclass.HandlingFee, 2)
            lblAsuransiKredit.Text = ocustomclass.RateAsuransiKredit
            lblBiayaPolis.Text = ocustomclass.BiayaPolis
            TabFormA.ViewCKE(ocustomclass.MPPA)
            TabFormB.ViewCKE(ocustomclass.MPPB)
            TabFormC.ViewCKE(ocustomclass.MPPC)
            TabFormD.ViewCKE(ocustomclass.MPPD)
            Me.ApprovalNo = ocustomclass.ApprovalNo

        End If
    End Sub

    Private Sub BindgridApprovalHistory()
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objconnection
            objCommand.CommandText = "spApprovalHistoryList"
            objCommand.Parameters.Add("@ApprovalNo", SqlDbType.VarChar, 50).Value = Me.ApprovalNo
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objread = objCommand.ExecuteReader()

            dtgHistory.DataSource = objread
            dtgHistory.DataBind()
            objread.Close()
            If dtgHistory.Items.Count > 0 Then
                dtgHistory.Visible = True
                pnlHistory.Visible = True
            Else
                dtgHistory.Visible = False
                pnlHistory.Visible = False
            End If

        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub
    Private Sub dtgHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgHistory.ItemDataBound
        If e.Item.ItemIndex >= 0 Then e.Item.Cells(0).Text = CStr(e.Item.ItemIndex + 1)
    End Sub
End Class