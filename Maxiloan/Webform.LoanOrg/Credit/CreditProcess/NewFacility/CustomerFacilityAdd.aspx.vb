﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region


Public Class CustomerFacilityAdd
    Inherits Maxiloan.Webform.WebBased
#Region "Property"

    Protected WithEvents ucLookUpCustomer1 As ucLookUpCustomer
    Protected WithEvents UcFacilityStartDate As ucDateCE
    Protected WithEvents UcFacilityMaturityDate As ucDateCE
    Protected WithEvents ucDrawDownStartDate As ucDateCE
    Protected WithEvents ucDrawDownMaturityDate As ucDateCE
    Protected WithEvents ucFasilitasAmount As ucNumberFormat
    Protected WithEvents UcEffectiveRate As ucNumberFormat
    Protected WithEvents UcRetensi As ucNumberFormat
    Protected WithEvents txtMaxDD As ucNumberFormat
    Protected WithEvents txtAdminFee As ucNumberFormat
    Protected WithEvents txtCommitmentFee As ucNumberFormat
    Protected WithEvents UcProvisionFee As ucNumberFormat
    Protected WithEvents UcNotaryFee As ucNumberFormat
    Protected WithEvents UcHandlingFee As ucNumberFormat
    Protected WithEvents UcAsuransiKredit As ucNumberFormat
    Protected WithEvents UcBiayaPolis As ucNumberFormat
    Protected WithEvents ucLateChargePersen As ucNumberFormat

    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New CustomerFacilityController
    Private ocustomclass As New Parameter.CustomerFacility
    Private m_controller As New ProductController

    Private Property NoFasilitas() As String
        Get
            Return CType(ViewState("NoFasilitas"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitas") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If CheckFeature(Me.Loginid, "CustomerFacility", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.NoFasilitas = Request("NoFasilitas")
            LoadingKegiatanUsaha()
            btnNext.Visible = True
            btnSave.Visible = False
            panelApproval.Visible = False
            If Me.NoFasilitas <> "" Then
                ' Bind data for edit maintenance
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .NoFasilitas = Me.NoFasilitas
                End With
                ocustomclass = m_controllerApp.GetFacilityDetail(ocustomclass)
                BindEdit(ocustomclass)
            End If
        End If
    End Sub

    Private Sub LoadingKegiatanUsaha()
        Me.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataSource = KegiatanUsahaS
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"
        'cboKegiatanUsaha.SelectedIndex = 0
        cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue("M"))


        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"
        cboJenisPembiyaan.DataSource = def
        cboJenisPembiyaan.Items.Insert(0, "Select One")
        cboJenisPembiyaan.Items(0).Value = "SelectOne"
        cboJenisPembiyaan.DataBind()

        refresh_cboJenisPembiayaan("M")

    End Sub

    Private Sub cboKegiatanUsaha_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboKegiatanUsaha.SelectedIndexChanged
        Dim cbo = CType(sender, DropDownList)
        Dim value = cbo.SelectedValue
        If cbo.SelectedIndex = 0 Then
            refresh_cboJenisPembiayaan("")
        Else
            refresh_cboJenisPembiayaan(value)
        End If
    End Sub

    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"

        If key = String.Empty Then
            cboJenisPembiyaan.DataSource = def
            cboJenisPembiyaan.DataBind()
        Else
            Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
            cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        End If
        cboJenisPembiyaan.DataBind()
        cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue("IF"))

    End Sub

    Private Sub loadApprovalData()
        Dim oData As New DataTable
        oData = Get_UserApproval("RCA5", Me.sesBranchId.Replace("'", ""), CDec(ucFasilitasAmount.Text))
        cboApprovedBy.DataSource = oData.DefaultView
        cboApprovedBy.DataTextField = "Name"
        cboApprovedBy.DataValueField = "ID"
        cboApprovedBy.DataBind()
        cboApprovedBy.Items.Insert(0, "Select One")
        cboApprovedBy.Items(0).Value = "0"
    End Sub

#Region "Edit"
    Sub BindEdit(ocustomclass)
        'cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(oRow("KegiatanUsaha").ToString))
        'refresh_cboJenisPembiayaan(oRow("KegiatanUsaha").ToString)
        'cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(oRow("JenisPembiayaan").ToString))
    End Sub

#End Region
    Function Validator() As Boolean
        If (txtNamaFasilitas.Text = "") Then
            ShowMessage(lblMessage, "harap isi nama fasilitas", True)
            Return False
        End If
        If (hdnCustomerID.Value = "") Then
            ShowMessage(lblMessage, "harap isi data customer", True)
            Return False
        End If

        If (IsDate(ConvertDate2(ucDrawDownStartDate.Text)) And IsDate(ConvertDate2(ucDrawDownMaturityDate.Text))) Then
            Dim startDate As Date = ConvertDate2(ucDrawDownStartDate.Text)
            Dim endDate As Date = ConvertDate2(ucDrawDownMaturityDate.Text)
            If (startDate >= endDate) Then
                ShowMessage(lblMessage, "periode tanggal tidak benar", True)
                Return False
            End If
        Else
            ShowMessage(lblMessage, "periode tanggal tidak benar", True)
            Return False
        End If
        If (CDec(ucFasilitasAmount.Text) < 1) Then
            ShowMessage(lblMessage, "harap isi jumlah plafond", True)
            Return False
        End If
        If (CDec(UcEffectiveRate.Text) > 100 Or CDec(UcEffectiveRate.Text) < 0) Then
            ShowMessage(lblMessage, "harap isi Effective rate dengan nilai 1 - 100", True)
            Return False
        End If
        If (CDec(UcProvisionFee.Text) > 100 Or CDec(UcProvisionFee.Text) < 0) Then
            ShowMessage(lblMessage, "harap isi provision fee dengan nilai 1 - 100", True)
            Return False
        End If
        Return True
    End Function

#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If Validator() = False Then
            Exit Sub
        End If

        Try
            Dim oCustomerFacility As New Parameter.CustomerFacility
            Dim oFinancial As New Parameter.FinancialData
            Dim tenor As Integer
            Dim startDate As Date = ConvertDate2(ucDrawDownStartDate.Text)
            Dim endDate As Date = ConvertDate2(ucDrawDownMaturityDate.Text)

            Dim strA As String = TabFormA.CKEEditorFormA
            Dim strA1 As String = Server.HtmlEncode(strA)
            Dim strA2 As String = Server.HtmlDecode(strA)

            Dim strB As String = TabFormB.CKEEditorFormB
            Dim strB1 As String = Server.HtmlEncode(strB)
            Dim strB2 As String = Server.HtmlDecode(strB)

            Dim strC As String = TabFormC.CKEEditorFormC
            Dim strC1 As String = Server.HtmlEncode(strC)
            Dim strC2 As String = Server.HtmlDecode(strC)

            Dim strD As String = TabFormD.CKEEditorFormD
            Dim strD1 As String = Server.HtmlEncode(strD)
            Dim strD2 As String = Server.HtmlDecode(strD)

            tenor = DateDiff(DateInterval.Month, startDate, endDate)
            oFinancial.FlatRate = CDec(UcEffectiveRate.Text) / 100
            oFinancial.Tenor = tenor

            oCustomerFacility.strConnection = GetConnectionString()
            oCustomerFacility.NoFasilitas = txtNoFasilitas.Text.Trim
            oCustomerFacility.CustomerID = hdnCustomerID.Value
            oCustomerFacility.FasilitasAmount = CDec(ucFasilitasAmount.Text)
            'oCustomerFacility.DrawDownAmount = CDec(ucFasilitasAmount.Text)
            'oCustomerFacility.AvailableAmount = CDec(ucFasilitasAmount.Text)
            'Modify by Wira 20181101
            oCustomerFacility.DrawDownAmount = 0
            oCustomerFacility.AvailableAmount = CDec(ucFasilitasAmount.Text) - oCustomerFacility.DrawDownAmount
            'End Modify

            oCustomerFacility.NamaFasilitas = txtNamaFasilitas.Text
            oCustomerFacility.BusinessDate = Me.BusinessDate
            oCustomerFacility.FacilityStartDate = IIf(UcFacilityStartDate.Text <> "", ConvertDate(UcFacilityStartDate.Text), "").ToString
            oCustomerFacility.FacilityMaturityDate = IIf(UcFacilityMaturityDate.Text <> "", ConvertDate(UcFacilityMaturityDate.Text), "").ToString
            oCustomerFacility.DrawDownStartDate = IIf(ucDrawDownStartDate.Text <> "", ConvertDate(ucDrawDownStartDate.Text), "").ToString
            oCustomerFacility.DrawDownMaturityDate = IIf(ucDrawDownMaturityDate.Text <> "", ConvertDate(ucDrawDownMaturityDate.Text), "").ToString
            oCustomerFacility.Tenor = tenor
            'oCustomerFacility.FlatRate = CDec(UcFlatRate.Text)
            'oCustomerFacility.EffectiveRate = cFlatToEff(oFinancial)
            oCustomerFacility.EffectiveRate = CDec(UcEffectiveRate.Text)
            oCustomerFacility.MinimumPencairan = CDec(txtMaxDD.Text)
            oCustomerFacility.Retensi = CDec(UcRetensi.Text)
            oCustomerFacility.FasilitasType = listFasilitasType.SelectedValue
            oCustomerFacility.AdminFee = CDec(txtAdminFee.Text)
            oCustomerFacility.CommitmentFee = CDec(txtCommitmentFee.Text)
            oCustomerFacility.ProvisionFee = CDec(UcProvisionFee.Text)
            oCustomerFacility.NotaryFee = CDec(UcNotaryFee.Text)
            oCustomerFacility.HandlingFee = CDec(UcHandlingFee.Text)
            oCustomerFacility.RateAsuransiKredit = CDec(UcAsuransiKredit.Text)
            oCustomerFacility.BiayaPolis = CDec(UcBiayaPolis.Text)
            oCustomerFacility.LoginId = Me.Loginid
            oCustomerFacility.NextPersonApproval = cboApprovedBy.SelectedValue
            oCustomerFacility.BranchId = Me.sesBranchId.Replace("'", "")
            oCustomerFacility.Status = "N"
            oCustomerFacility.JenisPembiayaan = cboJenisPembiyaan.SelectedValue
            oCustomerFacility.KegiatanUsaha = cboKegiatanUsaha.SelectedValue.ToString.Trim
            oCustomerFacility.InstallmentScheme = "PR"
            oCustomerFacility.LateChargePersen = ucLateChargePersen.Text
            oCustomerFacility.ApplicationModule = cboApplicationModule.SelectedValue
            oCustomerFacility.MPPA = strA2
            oCustomerFacility.MPPB = strB2
            oCustomerFacility.MPPC = strC2
            oCustomerFacility.MPPD = strD2

            m_controllerApp.CustomerFacilitySaveAdd(oCustomerFacility)

            ShowMessage(lblMessage, "Data saved!", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("CustomerFacility.aspx")
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        loadApprovalData()

        ucFasilitasAmount.Enabled = False

        btnSave.Visible = True
        btnNext.Visible = False
        panelApproval.Visible = True

        cboApplicationModule.Enabled = False
        If cboApplicationModule.SelectedValue = "MDKJ" Or cboApplicationModule.SelectedValue = "INV" Then
            panelretensi.Visible = False
        Else
            panelretensi.Visible = True
        End If

    End Sub


#End Region


#Region "LookupCustomer"
    Protected Sub btnLookupCustomer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupCustomer.Click
        ucLookUpCustomer1.CmdWhere = "All"
        ucLookUpCustomer1.Sort = "CustomerID ASC"
        ucLookUpCustomer1.Popup()
    End Sub
    Public Sub CatSelectedCustomer(ByVal CustomerID As String,
                                           ByVal Name As String,
                                           ByVal Address As String,
                                           ByVal BadType As String,
                                           ByVal IDType As String,
                                     ByVal IDTypeDescription As String,
                                     ByVal IDNumber As String,
                                     ByVal BirthPlace As String,
                                     ByVal BirthDate As DateTime,
                                     ByVal CustomerType As String)
        txtNmCustomer.Text = Name
        hdnCustomerID.Value = CustomerID
    End Sub
#End Region

End Class