﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerFacility.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerFacility" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Facility</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var x = screen.width
        var y = screen.height - 100
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }        
        function OpenWinViewCustomerFacility(id) {
            var x = screen.width; var y = screen.height - 100;
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewFacility/ViewCustomerFacility.aspx?NoFasilitas=' + id, 'CustomerLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body onload="gridGeneralSize('dtgPaging');" onresize="gridGeneralSize('dtgPaging')">
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR CUSTOMER FACILITY</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" DataKeyField="CustomerID"
                        CellSpacing="1" CellPadding="3" BorderWidth="0px" OnSortCommand="Sorting" AutoGenerateColumns="False"
                        AllowSorting="True" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER" HeaderStyle-Width="250px">
                                <ItemStyle CssClass="name_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Image ID="imgBadType" runat="server"></asp:Image>
                                    <asp:LinkButton ID="lnkCustName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CustomerName") %>'>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="NoFasilitas" HeaderText="NO FASILITAS">
                                <ItemStyle Width="15%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyNoFasilitas" runat="server" Text='<%#Container.DataItem("NoFasilitas")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="CustomerType" SortExpression="CustomerType" HeaderText="P/C" ItemStyle-CssClass="short_col"></asp:BoundColumn>
                            <asp:BoundColumn DataField="FasilitasStartDate" SortExpression="FasilitasStartDate" HeaderText="Tanggal Fasilitas" ItemStyle-CssClass="" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="FasilitasAmount" SortExpression="FasilitasAmount" HeaderText="Amount" ItemStyle-CssClass="" DataFormatString="{0:0,0}" ></asp:BoundColumn>
                            <asp:BoundColumn DataField="Status" SortExpression="Status" HeaderText="Status" ItemStyle-CssClass=""></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CustomerID" HeaderText="CustomerID" ></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="BadType" HeaderText="BadType"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../../Images/grid_navbutton01.png"
                        CausesValidation="False" CommandName="First" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../../Images/grid_navbutton02.png"
                        CausesValidation="False" CommandName="Prev" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../../Images/grid_navbutton03.png"
                        CausesValidation="False" CommandName="Next" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../../Images/grid_navbutton04.png"
                        CausesValidation="False" CommandName="Last" OnCommand="NavigationLink_Click">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" CssClass="small buttongo blue" Text="Go"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" Type="Integer" MaximumValue="999999999"
                        ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtGopage" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        Type="integer" ControlToValidate="txtGoPage" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>
                    record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI DATA</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="vwCustomer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNewApp" runat="server" Text="New Fasilitas" CssClass="small button blue " />
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="small button blue"
                CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
