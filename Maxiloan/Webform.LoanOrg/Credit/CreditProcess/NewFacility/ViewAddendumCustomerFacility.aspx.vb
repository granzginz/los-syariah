﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region


Public Class ViewAddendumCustomerFacility
    Inherits Maxiloan.Webform.WebBased
#Region "Property"

    Protected WithEvents ucLookUpCustomer1 As ucLookUpCustomer
    Protected WithEvents UcFacilityMaturityDate1 As ucDateCE
    Protected WithEvents ucDrawDownStartDate As ucDateCE
    Protected WithEvents ucDrawDownMaturityDate1 As ucDateCE
    Protected WithEvents ucFasilitasAmount1 As ucNumberFormat
    Protected WithEvents UcEffectiveRate1 As ucNumberFormat
    Protected WithEvents UcRetensi1 As ucNumberFormat
    Protected WithEvents txtMaxDD1 As ucNumberFormat
    Protected WithEvents txtAdminFee1 As ucNumberFormat
    Protected WithEvents txtCommitmentFee1 As ucNumberFormat
    Protected WithEvents UcProvisionFee1 As ucNumberFormat
    Protected WithEvents UcNotaryFee1 As ucNumberFormat
    Protected WithEvents UcHandlingFee1 As ucNumberFormat
    Protected WithEvents UcAsuransiKredit1 As ucNumberFormat
    Protected WithEvents UcBiayaPolis1 As ucNumberFormat
    Protected WithEvents ucLateChargePersen1 As ucNumberFormat
    Protected WithEvents ucDrawdownAmount1 As ucNumberFormat
    Protected WithEvents ucPaidAmount1 As ucNumberFormat

    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New CustomerFacilityController
    Private ocustomclass As New Parameter.CustomerFacility
    Private m_controller As New ProductController

    Private Property NoFasilitas() As String
        Get
            Return CType(ViewState("NoFasilitas"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitas") = Value
        End Set
    End Property

    Property NoFasilitasAddendum() As String
        Get
            Return ViewState("NoFasilitasAddendum").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitasAddendum") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property
    Private Property SeqNo As String
        Get
            Return CType(ViewState("SeqNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SeqNo") = Value
        End Set
    End Property
    Private Property FacilityId() As String
        Get
            Return CType(ViewState("FacilityId"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityId") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If CheckFeature(Me.Loginid, "AddendumCustFacility", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.NoFasilitasAddendum = Request.QueryString("NoFasilitasAddendum")
            Me.SeqNo = Request.QueryString("AddendumSeqNo")
            'LoadingKegiatanUsaha()
            'btnNext.Visible = True
            'btnSave.Visible = False
            'panelApproval.Visible = False
            If Me.NoFasilitasAddendum <> "" Then
                ' Bind data for edit maintenance
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .NoFasilitasAddendum = Me.NoFasilitasAddendum
                End With
                'ocustomclass = m_controllerApp.GetFacilityDetail(ocustomclass)
                BindEdit(ocustomclass)
            End If
        End If
    End Sub

    'Private Sub LoadingKegiatanUsaha()
    '    Me.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
    '    cboKegiatanUsaha.DataSource = KegiatanUsahaS
    '    cboKegiatanUsaha.DataValueField = "Value"
    '    cboKegiatanUsaha.DataTextField = "Text"
    '    cboKegiatanUsaha.DataBind()
    '    cboKegiatanUsaha.Items.Insert(0, "Select One")
    '    cboKegiatanUsaha.Items(0).Value = "SelectOne"
    '    'cboKegiatanUsaha.SelectedIndex = 0
    '    cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue("M"))


    '    Dim def = New List(Of Parameter.CommonValueText)
    '    def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

    '    cboJenisPembiyaan.DataValueField = "Value"
    '    cboJenisPembiyaan.DataTextField = "Text"
    '    cboJenisPembiyaan.DataSource = def
    '    cboJenisPembiyaan.Items.Insert(0, "Select One")
    '    cboJenisPembiyaan.Items(0).Value = "SelectOne"
    '    cboJenisPembiyaan.DataBind()

    '    refresh_cboJenisPembiayaan("M")

    'End Sub

    'Private Sub cboKegiatanUsaha_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboKegiatanUsaha.SelectedIndexChanged
    '    Dim cbo = CType(sender, DropDownList)
    '    Dim value = cbo.SelectedValue
    '    If cbo.SelectedIndex = 0 Then
    '        refresh_cboJenisPembiayaan("")
    '    Else
    '        refresh_cboJenisPembiayaan(value)
    '    End If
    'End Sub

    'Sub refresh_cboJenisPembiayaan(key As String)
    '    Dim def = New List(Of Parameter.CommonValueText)
    '    def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
    '    cboJenisPembiyaan.DataValueField = "Value"
    '    cboJenisPembiyaan.DataTextField = "Text"

    '    If key = String.Empty Then
    '        cboJenisPembiyaan.DataSource = def
    '        cboJenisPembiyaan.DataBind()
    '    Else
    '        Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
    '        cboJenisPembiyaan.DataSource = k.JenisPembiayaan
    '    End If
    '    cboJenisPembiyaan.DataBind()
    '    cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue("IF"))

    'End Sub

    'Private Sub loadApprovalData()
    '    Dim oData As New DataTable
    '    oData = Get_UserApproval("AFAC", Me.sesBranchId.Replace("'", ""), CDec(ucFasilitasAmount1.Text))
    '    cboApprovedBy.DataSource = oData.DefaultView
    '    cboApprovedBy.DataTextField = "Name"
    '    cboApprovedBy.DataValueField = "ID"
    '    cboApprovedBy.DataBind()
    '    cboApprovedBy.Items.Insert(0, "Select One")
    '    cboApprovedBy.Items(0).Value = "0"
    'End Sub

#Region "Edit"

    Sub BindEdit(NoFasilitasAddendum)

        If Me.NoFasilitasAddendum <> "" Then
            With ocustomclass
                .BranchId = Me.BranchID
                .strConnection = GetConnectionString()
                .NoFasilitasAddendum = Me.NoFasilitasAddendum
            End With
            ocustomclass = m_controllerApp.GetAddendumFacilityDetail(ocustomclass)
            'Dim oCustomerFacility As New Parameter.CustomerFacility

            'lblCustomerId.Text = ocustomclass.CustomerID
            lblNoFasilitas.Text = ocustomclass.NoFasilitas
            lblNamaFasilitas.Text = ocustomclass.NamaFasilitas
            lblNoFasilitasAddendum.Text = ocustomclass.NoFasilitasAddendum
            lblFacilityStartDate.Text = ocustomclass.FacilityStartDate
            lblFacilityMaturityDate.Text = ocustomclass.FacilityMaturityDate
            lblNmCustomer.Text = ocustomclass.CustomerName
            'cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(ocustomclass.KegiatanUsaha.Replace(" ", "")))
            lblKegiatanUsaha.Text = ocustomclass.KegiatanUsaha
            'refresh_cboJenisPembiayaan(ocustomclass.KegiatanUsaha.ToString)
            'cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(ocustomclass.JenisPembiayaan.Replace(" ", "")))
            lblJenisPembiyaan.Text = ocustomclass.JenisPembiayaan
            'lblJenisPembiyaan.Text = cboJenisPembiyaan.SelectedItem.Text
            'cboApplicationModule.SelectedIndex = cboApplicationModule.Items.IndexOf(cboApplicationModule.Items.FindByValue(ocustomclass.ApplicationModule.Replace(" ", "")))
            'lblApplicationModule.Text = cboApplicationModule.Items.FindByValue(ocustomclass.ApplicationModule.Replace(" ", "")).ToString
            lblApplicationModule.Text = ocustomclass.ApplicationModule
            lblDrawDownStartDate.Text = ocustomclass.DrawDownStartDate
            lblDrawDownMaturityDate.Text = ocustomclass.DrawDownMaturityDate
            lblFasilitasAmount.Text = FormatNumber(ocustomclass.FasilitasAmount, 2)
            lblEffectiveRate.Text = ocustomclass.EffectiveRate
            lblMaxDD.Text = FormatNumber(ocustomclass.MinimumPencairan, 2)
            lblRetensi.Text = ocustomclass.Retensi
            lblLateChargePersen.Text = ocustomclass.LateChargePersen
            lblAdminFee.Text = FormatNumber(ocustomclass.AdminFee, 2)
            lblCommitmentFee.Text = FormatNumber(ocustomclass.CommitmentFee, 2)
            lblProvisionFee.Text = FormatNumber(ocustomclass.ProvisionFee, 2)
            lblNotaryFee.Text = FormatNumber(ocustomclass.NotaryFee, 2)
            lblHandlingFee.Text = FormatNumber(ocustomclass.HandlingFee, 2)
            lblAsuransiKredit.Text = ocustomclass.RateAsuransiKredit
            lblBiayaPolis.Text = ocustomclass.BiayaPolis
            lblDrawdownAmount.Text = ocustomclass.DrawDownAmount
            lblPaidAmount.Text = ocustomclass.PaidAmount
            lblFasilitasType.Text = ocustomclass.FasilitasType
            Me.FacilityId = ocustomclass.FacilityId

            'cboKegiatanUsaha.Enabled = False
            'cboJenisPembiyaan.Enabled = False
            'cboApplicationModule.Enabled = False
            'ucDrawdownAmount1.Enabled = False
            'ucPaidAmount1.Enabled = False

            'lblNoFasilitas.Attributes.Add("onclick", "return OpenWinViewCustomerFacility('" & ocustomclass.NoFasilitas & "','" & Me.BranchID & "')")
        End If
    End Sub

#End Region
    'Function Validator() As Boolean

    '    If (IsDate(ConvertDate2(lblDrawDownStartDate.Text)) And IsDate(ConvertDate2(ucDrawDownMaturityDate1.Text))) Then
    '        Dim startDate As Date = ConvertDate2(lblDrawDownStartDate.Text)
    '        Dim endDate As Date = ConvertDate2(ucDrawDownMaturityDate1.Text)
    '        If (startDate >= endDate) Then
    '            ShowMessage(lblMessage, "periode tanggal tidak benar", True)
    '            Return False
    '        End If
    '    Else
    '        ShowMessage(lblMessage, "periode tanggal tidak benar", True)
    '        Return False
    '    End If
    '    If (CDec(ucFasilitasAmount1.Text) < 1) Then
    '        ShowMessage(lblMessage, "harap isi jumlah plafond", True)
    '        Return False
    '    End If
    '    If (CDec(UcEffectiveRate1.Text) > 100 Or CDec(UcEffectiveRate1.Text) < 0) Then
    '        ShowMessage(lblMessage, "harap isi Effective rate dengan nilai 1 - 100", True)
    '        Return False
    '    End If
    '    If (CDec(UcProvisionFee1.Text) > 100 Or CDec(UcProvisionFee1.Text) < 0) Then
    '        ShowMessage(lblMessage, "harap isi provision fee dengan nilai 1 - 100", True)
    '        Return False
    '    End If
    '    Return True
    'End Function

#Region "Save"
    'Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

    '    If Validator() = False Then
    '        Exit Sub
    '    End If

    '    Try
    '        Dim oCustomerFacility As New Parameter.CustomerFacility
    '        Dim oFinancial As New Parameter.FinancialData
    '        Dim tenor As Integer
    '        Dim startDate As Date = ConvertDate2(lblDrawDownStartDate.Text)
    '        Dim endDate As Date = ConvertDate2(ucDrawDownMaturityDate1.Text)

    '        Dim strE As String = TabFormE.CKEEditorFormE
    '        Dim strE1 As String = Server.HtmlEncode(strE)
    '        Dim strE2 As String = Server.HtmlDecode(strE)

    '        tenor = DateDiff(DateInterval.Month, startDate, endDate)
    '        oFinancial.FlatRate = CDec(UcEffectiveRate1.Text) / 100
    '        oFinancial.Tenor = tenor

    '        oCustomerFacility.strConnection = GetConnectionString()
    '        oCustomerFacility.NoFasilitas = lblNoFasilitas.Text.Trim
    '        oCustomerFacility.NoAddendum = txtNoFasilitasAddendum.Text.Trim
    '        oCustomerFacility.CustomerID = lblCustomerId.Text
    '        oCustomerFacility.FasilitasAmount = CDec(ucFasilitasAmount1.Text)
    '        oCustomerFacility.DrawDownAmount = CDec(ucDrawdownAmount1.Text)
    '        oCustomerFacility.PaidAmount = CDec(ucPaidAmount1.Text)
    '        'Modify by Wira 20181101
    '        oCustomerFacility.DrawDownAmount = 0
    '        oCustomerFacility.AvailableAmount = CDec(ucFasilitasAmount1.Text) - oCustomerFacility.DrawDownAmount
    '        'End Modify

    '        oCustomerFacility.NamaFasilitas = lblNamaFasilitas.Text
    '        oCustomerFacility.BusinessDate = Me.BusinessDate
    '        oCustomerFacility.FacilityStartDate = IIf(lblFacilityStartDate.Text <> "", ConvertDate(lblFacilityStartDate.Text), "").ToString
    '        oCustomerFacility.FacilityMaturityDate = IIf(UcFacilityMaturityDate1.Text <> "", ConvertDate(UcFacilityMaturityDate1.Text), "").ToString
    '        oCustomerFacility.DrawDownStartDate = IIf(lblDrawDownStartDate.Text <> "", ConvertDate(lblDrawDownStartDate.Text), "").ToString
    '        oCustomerFacility.DrawDownMaturityDate = IIf(ucDrawDownMaturityDate1.Text <> "", ConvertDate(ucDrawDownMaturityDate1.Text), "").ToString
    '        oCustomerFacility.Tenor = tenor
    '        'oCustomerFacility.FlatRate = CDec(UcFlatRate.Text)
    '        'oCustomerFacility.EffectiveRate = cFlatToEff(oFinancial)
    '        oCustomerFacility.EffectiveRate = CDec(UcEffectiveRate1.Text)
    '        oCustomerFacility.MinimumPencairan = CDec(txtMaxDD1.Text)
    '        oCustomerFacility.Retensi = CDec(UcRetensi1.Text)
    '        oCustomerFacility.FasilitasType = listFasilitasType.SelectedValue
    '        oCustomerFacility.AdminFee = CDec(txtAdminFee1.Text)
    '        oCustomerFacility.CommitmentFee = CDec(txtCommitmentFee1.Text)
    '        oCustomerFacility.ProvisionFee = CDec(UcProvisionFee1.Text)
    '        oCustomerFacility.NotaryFee = CDec(UcNotaryFee1.Text)
    '        oCustomerFacility.HandlingFee = CDec(UcHandlingFee1.Text)
    '        oCustomerFacility.RateAsuransiKredit = CDec(UcAsuransiKredit1.Text)
    '        oCustomerFacility.BiayaPolis = CDec(UcBiayaPolis1.Text)
    '        oCustomerFacility.LoginId = Me.Loginid
    '        oCustomerFacility.NextPersonApproval = cboApprovedBy.SelectedValue
    '        oCustomerFacility.BranchId = Me.sesBranchId.Replace("'", "")
    '        oCustomerFacility.Status = "N"
    '        oCustomerFacility.JenisPembiayaan = cboJenisPembiyaan.SelectedValue
    '        oCustomerFacility.KegiatanUsaha = cboKegiatanUsaha.SelectedValue.ToString.Trim
    '        oCustomerFacility.InstallmentScheme = "PR"
    '        oCustomerFacility.LateChargePersen = ucLateChargePersen1.Text
    '        oCustomerFacility.ApplicationModule = cboApplicationModule.SelectedValue
    '        oCustomerFacility.MPPE = strE2
    '        oCustomerFacility.FacilityId = Me.FacilityId

    '        m_controllerApp.AddendumCustomerFacilitySaveAdd(oCustomerFacility)

    '        ShowMessage(lblMessage, "Data saved!", False)
    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '        Exit Sub
    '    End Try

    'End Sub

    'Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Response.Redirect("AddendumCustomerFacility.aspx")
    'End Sub

    'Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
    '    loadApprovalData()

    '    ucFasilitasAmount1.Enabled = False

    '    btnSave.Visible = True
    '    btnNext.Visible = False
    '    panelApproval.Visible = True

    '    cboApplicationModule.Enabled = False
    '    If cboApplicationModule.SelectedValue = "MDKJ" Then
    '        panelretensi.Visible = False
    '    Else
    '        panelretensi.Visible = True
    '    End If

    'End Sub


#End Region



End Class