﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region


Public Class CustomerFacilityApplication
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucMutasiDate As ucDateCE
    Protected WithEvents ucJumlahPencairan As ucNumberFormat

    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New CustomerFacilityController
    Private ocustomclass As New Parameter.CustomerFacility

    Private Property NoFasilitas() As String
        Get
            Return CType(ViewState("NoFasilitas"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoFasilitas") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property oCustomerFacility() As Parameter.CustomerFacility
        Get
            Return CType(ViewState("oCustomerFacility"), Parameter.CustomerFacility)
        End Get
        Set(ByVal Value As Parameter.CustomerFacility)
            ViewState("oCustomerFacility") = Value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If CheckFeature(Me.Loginid, "CustomerFacility", "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        If Not Page.IsPostBack Then
            Me.NoFasilitas = Request("NoFasilitas")

            If Me.NoFasilitas <> "" Then
                ' Bind data for edit maintenance
                With ocustomclass
                    .strConnection = GetConnectionString()
                    .NoFasilitas = Me.NoFasilitas
                End With
                ocustomclass = m_controllerApp.GetFacilityDetail(ocustomclass)
                oCustomerFacility = ocustomclass
                BindEdit(ocustomclass)
            End If
        End If
    End Sub


#Region "Edit"
    Sub BindEdit(ocustomclass As Parameter.CustomerFacility)
        Me.BranchID = ocustomclass.BranchId
        lblNamaCustomer.Text = ocustomclass.CustomerName
        hdnCustomerID.Value = ocustomclass.CustomerID
        lblNamaFasilitas.Text = ocustomclass.NamaFasilitas
        lblJenisPembiyaan.Text = ocustomclass.JenisPembiayaan
        lblKegiatanUsaha.Text = ocustomclass.KegiatanUsaha
        lblPeriodePencairan.Text = ocustomclass.DrawDownStartDate & " s/d " & ocustomclass.DrawDownMaturityDate
        lblFasilitasAmount.Text = FormatNumber(Convert.ToDecimal(ocustomclass.FasilitasAmount), 0)
        lblFlatRate.Text = FormatNumber(Convert.ToDecimal(ocustomclass.FlatRate), 0) & " %"
        lblMinimumPencairan.Text = FormatNumber(Convert.ToDecimal(ocustomclass.MinimumPencairan), 0)
        lblFasilitasType.Text = ocustomclass.FasilitasType
    End Sub

#End Region
    Function Validator() As Boolean
        If (CDec(ucJumlahPencairan.Text) < 1) Then
            ShowMessage(lblMessage, "harap isi jumlah pencairan", True)
            Return False
        End If
        If (CDec(oCustomerFacility.MinimumPencairan) > CDec(ucJumlahPencairan.Text)) Then
            ShowMessage(lblMessage, "pencairan harus lebih dari nilai minimum", True)
            Return False
        End If

    End Function

#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If Validator() = False Then
            Exit Sub
        End If

        Try


            ShowMessage(lblMessage, "Data saved!", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("CustomerFacility.aspx")
    End Sub

#End Region


#Region "LookupCustomer"

#End Region

End Class