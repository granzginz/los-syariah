﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FacilityFormDTab.ascx.vb" Inherits="Maxiloan.Webform.LoanOrg.FacilityFormDTab" %>
<%@ Register Assembly="CKEditor.NET, Version=3.6.6.2, Culture=neutral, PublicKeyToken=e379cdf2f8354999" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

 <div class="form_box_title">
    <div class="form_single"> <h4> FORM D - DATA UMUM CALON NASABAH </h4> </div>
 </div>
                              
        <div class="form_box">
            <div class="form_left">
                    <CKEditor:CKEditorControl ID="CKEditor1" BasePath="~/ckeditor" runat="server" >
                    </CKEditor:CKEditorControl>                 
                <script>
                    CKEDITOR.replace('CKEditor1');
                    // resize the editor after it has been fully initialized
                    CKEDITOR.on('instanceLoaded', function (e) { e.editor.resize(700, 350) });
            </script>
            </div>
        </div>