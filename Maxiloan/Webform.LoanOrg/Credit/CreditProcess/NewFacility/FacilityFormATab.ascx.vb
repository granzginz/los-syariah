﻿Public Class FacilityFormATab
    Inherits System.Web.UI.UserControl

    Public Property CKEEditorFormA As String
        Get
            Return CType(ViewState("CKEEditorFormA"), String)
        End Get
        Set(value As String)
            ViewState("CKEEditorFormA") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CKEEditorFormA = CKEditor1.Text
    End Sub

    Sub ViewCKE(CKExx)
        CKEditor1.Text = CKExx
        CKEditor1.Enabled = False
    End Sub
End Class