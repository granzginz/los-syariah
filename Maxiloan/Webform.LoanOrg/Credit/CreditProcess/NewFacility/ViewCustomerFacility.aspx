﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCustomerFacility.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewCustomerFacility" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../../../webform.UserController/ucLookUpCustomer.ascx"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET, Version=3.6.6.2, Culture=neutral, PublicKeyToken=e379cdf2f8354999" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Src="FacilityFormATab.ascx" TagName="formA" TagPrefix="uc1" %>
<%@ Register Src="FacilityFormBTab.ascx" TagName="formB" TagPrefix="uc2" %>
<%@ Register Src="FacilityFormCTab.ascx" TagName="formC" TagPrefix="uc3" %>
<%@ Register Src="FacilityFormDTab.ascx" TagName="formD" TagPrefix="uc4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Facility</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
     <style>
        .multiline_textbox {
            width: 60% !important;
        }
    </style>
</head>
    
<body>



    <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div runat="server" id="jlookupContent" />
     
 
    <asp:Panel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <asp:Label ID="lblMessage" runat="server" Visible="false" onclick="hideMessage();"></asp:Label>
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        VIEW FACILITY
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label> No Fasilitas</label>
                    <asp:Label ID="lblNoFasilitas"  runat="server" width="300"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label> Nama Fasilitas</label>
                    <asp:Label ID="lblNamaFasilitas"  runat="server" width="400"></asp:Label>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label>Tanggal Fasilitas</label>
                    <asp:Label id="lblFacilityStartDate" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Akhir Fasilitas
                    </label>
                    <asp:Label id="lblFacilityMaturityDate" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label> Nama Customer</label>
                    <asp:Label ID="lblNmCustomer"  runat="server" width="400"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label>Jenis Pembiayaan</label>
                        <asp:Label ID="lblKegiatanUsaha"  runat="server" width="400"></asp:Label>  
                       
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Skema Pembiayaan
                        </label>
                        <asp:Label ID="lblJenisPembiayaan"  runat="server" width="400"></asp:Label>  
                    </div>
                    <div class="form_right">
                        <label>Application Module</label>
                        <asp:Label ID="lblApplicationModule"  runat="server" width="400"></asp:Label>                      
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Periode Pencairan
                    </label>
                    <asp:Label id="lblDrawDownStartDate" runat="server"></asp:Label>
                    <label class="label_auto">
                        s/d
                    </label>
                    <asp:Label id="lblDrawDownMaturityDate" runat="server"></asp:Label>
                </div>
            </div>
             <div class="form_box">
                <div class="form_left">
                    <label>Jumlah Plafond</label>
                   <asp:Label id="lblFasilitasAmount" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Suku Margin Effektive
                    </label>
                    <asp:Label id="lblEffectiveRate" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Minimum Pencairan Per Batch
                    </label>
                    <asp:Label id="lblMaxDD" runat="server" />
                </div>
                <div class="form_right" id="retensi">
                    <div id="panelretensi" runat="server">
                    <label>
                        Retensi
                    </label>
                    <asp:Label ID="lblRetensi"  runat="server" width="400"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label  runat="server" id="lblFasilitasType">
                        Type Fasilitas
                    </label>

                </div> 
                <div class="form_right" >
                    <label>Persentasi Ta'widh</label>
                  <asp:Label ID="lblLatechargesPercnetage"  runat="server" width="400"></asp:Label>
                </div>   
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Admin Fee
                    </label>
                    <asp:Label id="lblAdminFee" runat="server" />
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Commintment Fee
                    </label>
                    <asp:Label id="lblCommitmentFee" runat="server" />
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Provision Fee / Annual Fee
                    </label>
                    <asp:Label id="lblProvisionFee" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Notary Fee
                    </label>
                    <asp:Label id="lblNotaryFee" runat="server" />
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Handling Fee
                    </label>
                    <asp:Label id="lblHandlingFee" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <asp:Label id="lblAsuransiKredit" runat="server" /> %
                </div>
            </div>        
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Biaya Polis
                    </label>
                    <asp:Label id="lblBiayaPolis" runat="server" />
                </div>
            </div>        
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        <asp:Label ID="Label5" runat="server"></asp:Label>
                        MEMO PENGUSULAN PEMBIAYAAN (MPP)
                    </h4>
                </div>
            </div>
        <div class="form_box">
        <div class="form_single" id="tabMPPNew" runat="server" >
            <asp:TabContainer runat="server" ID="Tabs" style="height: auto;" ActiveTabIndex="0" Width="100%">
                <asp:TabPanel runat="server" ID="pnlTabFormA" HeaderText="FORM A">
                    <contenttemplate> 
                         <uc1:formA id="TabFormA" runat="server"/>  
                    </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabFormB" HeaderText="FORM B">
                    <contenttemplate>
                          <uc2:formB id="TabFormB" runat="server"/>  
                    </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabFormC" HeaderText="FORM C">
                    <contenttemplate>
                         <uc3:formC id="TabFormC" runat="server"/>                    
                    </contenttemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="pnlTabFormD" HeaderText="FORM D">
                    <contenttemplate>
                          <uc4:formD  id="TabFormD" runat="server"/> 
                    </contenttemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </div>
        </div>  
    <asp:Panel ID="pnlHistory" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    APPROVAL HISTORY
                </h4>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgHistory" runat="server" EnableViewState="False" Width="100%"
                CssClass="grid_general" CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn HeaderText="NO">
                        <HeaderStyle Width="5%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <%--<asp:BoundColumn DataField="UserApproval" HeaderText="NAMA">--%>
                    <asp:BoundColumn DataField="UserApprovalName" HeaderText="NAMA">
                        <HeaderStyle Width="20%"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ApprovalDate" HeaderText="TGL APPROVED">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="WOA" HeaderText="JENIS">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Duration" HeaderText="LAMA">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="IsFinal" HeaderText="IS FINAL">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ApprovalNote" HeaderText="CATATAN APPROVAL">
                        <HeaderStyle Width="30%"></HeaderStyle>
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>       
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
        </ContentTemplate>
    </asp:Panel>
   <%-- </asp:Content>--%>
 </form>
</body>
</html> 
