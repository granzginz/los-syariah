﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

#End Region

Public Class NewAppInsurance
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomclass As New Parameter.GeneralPaging
    Private oController As New GeneralPagingController
    Protected WithEvents oSearchBy As UcSearchBy    
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Protected WithEvents lblMessage As System.Web.UI.WebControls.Label
    Protected WithEvents imbFirstPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbPrevPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbNextPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imbLastPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtGoPage As System.Web.UI.WebControls.TextBox
    Protected WithEvents imbGoPage As System.Web.UI.WebControls.ImageButton
    Protected WithEvents rgvGo As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents lblPage As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotPage As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotRec As System.Web.UI.WebControls.Label
    Protected WithEvents rfvGo As System.Web.UI.WebControls.RequiredFieldValidator
    Private recordCount As Int64 = 1
#End Region

#Region "LinkTo "
    Function LinkToEmployee(ByVal strBranchID As String, ByVal strAOID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinEmployee('" & strBranchID & "','" & strAOID & "','" & strStyle & "')"
    End Function
#End Region

#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(viewstate("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            InitialDefaultPanel()
            oSearchBy.ListData = "DBO.CUSTOMER.NAME,Customer Name-dbo.Agreement.ApplicationID, ApplicationID- ProductOffering.Description, Product Offering Name - BranchEmployee.EmployeeName, AOName "
            oSearchBy.BindData()
            Me.SearchBy = ""
            BindGridEntity(Me.SearchBy)
        End If
    End Sub
#End Region

#Region "IntialDefaultPanel"
    Sub InitialDefaultPanel()
        lblMessage.Text = ""
        lblMessage.Visible = False
        PnlListNewAppInsurance.Visible = False
    End Sub
#End Region

#Region "Bind Grid "
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As New DataTable

        PnlListNewAppInsurance.Visible = True        
        If Me.Sort = "" Then Me.Sort = "Agreement.ApplicationID ASC"       
        Me.CmdWhere = cmdWhere & " AND Agreement.BranchID='" & Replace(Me.sesBranchId, "'", "") & "' "        

        With oCustomclass
            .strConnection = GetConnectionString
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy.Trim
            .SpName = "spPagingInsNewApplication"
        End With

        oCustomclass = oController.GetGeneralPaging(oCustomclass)

        If Not oCustomclass Is Nothing Then
            dtEntity = oCustomclass.ListData
            recordCount = oCustomclass.TotalRecords
            lblTotRec.Text = recordCount.ToString
        Else
            recordCount = 0
            lblTotRec.Text = recordCount.ToString
        End If

        dtgPagingNewAppInsurance.DataSource = dtEntity.DefaultView
        dtgPagingNewAppInsurance.CurrentPageIndex = 0
        dtgPagingNewAppInsurance.DataBind()

        PagingFooter()
    End Sub
#End Region

#Region "Paging Footer"
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then            
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

#End Region

#Region "Navigation Link Click "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "Go Page "
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPagingNewAppInsurance.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "Get HyperLink"
    Function GetHyperLink(ByVal str1 As String, ByVal str2 As String) As String
        If str1 = "CO" And str2 = "CU" Then
            Return "NewAppInsuranceByCompany.aspx?PageSource=CompanyCustomer"
        End If

        If str1 = "CO" And str2 = "AC" Then
            Return "NewAppInsuranceByCompany.aspx?PageSource=CompanyAtCost"
        End If

        If str1 = "CU" And str2 = "CU" Then
            Return "NewAppInsuranceByCust.aspx?PageSource=CustToCust"
        End If
    End Function
#End Region

#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.SearchBy = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        If Me.SortBy = "" Then Me.SortBy = "GSID"
        BindGridEntity(Me.SearchBy)
    End Sub
#End Region

#Region "Handle Link Data Bound "
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPagingNewAppInsurance.ItemDataBound
        Dim lnkApp As LinkButton
        Dim lnkAO As HyperLink
        Dim lblAOID As Label

        If e.Item.ItemIndex >= 0 Then
            lnkApp = CType(e.Item.FindControl("lnkApplicationID"), LinkButton)
            lnkApp.Attributes.Add("OnClick", "return OpenWin('" & lnkApp.Text & "','AccAcq');")
            Dim lnkCust As LinkButton
            lnkCust = CType(e.Item.FindControl("lnkCust"), LinkButton)
            lnkCust.Attributes.Add("OnClick", "return OpenCust('" & e.Item.Cells(4).Text & "','AccAcq');")
            Dim lnkPO As LinkButton
            lnkPO = CType(e.Item.FindControl("lnkProdOff"), LinkButton)
            lnkPO.Attributes.Add("OnClick", "return OpenWinProductOfferingBranchView('" & e.Item.Cells(1).Text & "','" & e.Item.Cells(2).Text & "','" & e.Item.Cells(3).Text & "','AccAcq');")

            lnkAO = CType(e.Item.FindControl("lnkAO"), HyperLink)
            lblAOID = CType(e.Item.FindControl("lblAOID"), Label)
            lnkAO.NavigateUrl = LinkToEmployee(e.Item.Cells(3).Text, lblAOID.Text.Trim, "AccAcq")
        End If
    End Sub
#End Region

    Private Sub imbSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.SearchBy = "And Agreement.BranchID = '" & Replace(Me.sesBranchId, "'", "") & "' "
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '" & oSearchBy.Text.Trim.Replace("'", "''") & "' "
        End If
        BindGridEntity(Me.SearchBy)
    End Sub
End Class