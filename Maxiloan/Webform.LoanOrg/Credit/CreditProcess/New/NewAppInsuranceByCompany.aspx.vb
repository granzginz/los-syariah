﻿#Region "Imports"
Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Webform
#End Region

Public Class NewAppInsuranceByCompany
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property DiscountMin() As Double
        Get
            Return (CType(ViewState("DiscountMin"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiscountMin") = Value
        End Set
    End Property
    Private Property DiscountMax() As Double
        Get
            Return (CType(ViewState("DiscountMax"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiscountMax") = Value
        End Set
    End Property
    Private Property InsuranceComBranchID_lbl() As String
        Get
            Return (CType(ViewState("InsuranceComBranchID_lbl"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InsuranceComBranchID_lbl") = Value
        End Set
    End Property
    Private Property ExistingPolicyNo() As String
        Get
            Return (CType(ViewState("ExistingPolicyNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ExistingPolicyNo") = Value
        End Set
    End Property
    Private Property Tenor() As String
        Get
            Return (CType(ViewState("Tenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Tenor") = Value
        End Set
    End Property
    Private Property Tenor2() As String
        Get
            Return (CType(ViewState("Tenor2"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Tenor2") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property SupplierGroupID() As String
        Get
            Return (CType(ViewState("SupplierGroupID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierGroupID") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return (CType(ViewState("CustomerName"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Private Property PageSource() As String
        Get
            Return (CType(ViewState("PageSource"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PageSource") = Value
        End Set
    End Property
    Private Property AdminFeeBehavior() As String
        Get
            Return (CType(ViewState("AdminFeeBehavior"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AdminFeeBehavior") = Value
        End Set
    End Property
    Private Property NilaiAdminFeeAwal() As Double
        Get
            Return (CType(ViewState("NilaiAdminFeeAwal"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("NilaiAdminFeeAwal") = Value
        End Set
    End Property
    Private Property StampDutyFeeBehavior() As String
        Get
            Return (CType(ViewState("StampDutyFeeBehavior"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("StampDutyFeeBehavior") = Value
        End Set
    End Property
    Private Property NilaiStampDutyFeeAwal() As Double
        Get
            Return (CType(ViewState("NilaiStampDutyFeeAwal"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("NilaiStampDutyFeeAwal") = Value
        End Set
    End Property
    Private Property MaximumTenor() As String
        Get
            Return (CType(ViewState("MaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("MaximumTenor") = Value
        End Set
    End Property
    Private Property MinimumTenor() As String
        Get
            Return (CType(ViewState("MinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("MinimumTenor") = Value
        End Set
    End Property
    Private Property PBMaximumTenor() As String
        Get
            Return (CType(ViewState("PBMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PBMaximumTenor") = Value
        End Set
    End Property
    Private Property PBMinimumTenor() As String
        Get
            Return (CType(ViewState("PBMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PBMinimumTenor") = Value
        End Set
    End Property
    Private Property PMaximumTenor() As String
        Get
            Return (CType(ViewState("PMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PMaximumTenor") = Value
        End Set
    End Property
    Private Property PMinimumTenor() As String
        Get
            Return (CType(ViewState("PMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("PMinimumTenor") = Value
        End Set
    End Property
    Private Property ModuleID() As String
        Get
            Return (CType(ViewState("ModuleID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ModuleID") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return (CType(ViewState("Mode"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property ApplicationTypeID() As String
        Get
            Return CType(ViewState("ApplicationTypeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationTypeID") = Value
        End Set
    End Property
    Private Property CoverageTypeMs() As DataTable
        Get
            Return CType(ViewState("CoverageTypeMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("CoverageTypeMs") = Value
        End Set
    End Property
    Private Property PaidByCustMs() As DataTable
        Get
            Return CType(ViewState("PaidByCustMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("PaidByCustMs") = Value
        End Set
    End Property
    Private Property SRCCMs() As DataTable
        Get
            Return CType(ViewState("SRCCMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("SRCCMs") = Value
        End Set
    End Property
    Private Property TPLMs() As DataTable
        Get
            Return CType(ViewState("TPLMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TPLMs") = Value
        End Set
    End Property
    Private Property FloodMs() As DataTable
        Get
            Return CType(ViewState("FloodMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("FloodMs") = Value
        End Set
    End Property
    Private Property InsuranceComBranchID() As String
        Get
            Return CType(ViewState("InsuranceComBranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsuranceComBranchID") = Value
        End Set
    End Property
    Private Property InsuranceType() As String
        Get
            Return CType(ViewState("InsuranceType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsuranceType") = Value
        End Set
    End Property
    Private Property AssetUsageID() As String
        Get
            Return CType(ViewState("AssetUsageID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetUsageID") = Value
        End Set
    End Property
    Private Property AssetNewUsed() As String
        Get
            Return CType(ViewState("AssetNewUsed"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AssetNewUsed") = Value
        End Set
    End Property
    Private Property ManufacturingYear() As String
        Get
            Return (CType(ViewState("ManufacturingYear"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ManufacturingYear") = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return (CType(ViewState("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return (CType(ViewState("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Public Property AmountCoverage() As Double
        Get
            Return (CType(ViewState("AmountCoverage"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("AmountCoverage") = Value
        End Set
    End Property
    Private Property PremiumAmountByCustBeforeDisc() As Double
        Get
            Return CType(ViewState("PremiumAmountByCustBeforeDisc"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("PremiumAmountByCustBeforeDisc") = Value
        End Set
    End Property
    Private Property InsLength() As Int16
        Get
            Return CType(ViewState("InsLength"), Int16)
        End Get
        Set(ByVal Value As Int16)
            ViewState("InsLength") = Value
        End Set
    End Property
    Private Property IsPreview() As String
        Get
            Return (CType(ViewState("IsPreview"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("IsPreview") = Value
        End Set
    End Property
    Private Property RiotMs() As DataTable
        Get
            Return CType(ViewState("RiotMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("RiotMs") = Value
        End Set
    End Property
    Private Property TipeAsuransi() As DataTable
        Get
            Return CType(ViewState("TipeAsuransi"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TipeAsuransi") = Value
        End Set
    End Property
    Private Property RateCardID() As String
        Get
            Return (CType(ViewState("RateCardID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("RateCardID") = Value
        End Set
    End Property
    Private Property idxGridInscoSelected As Integer
        Get
            Return CType(ViewState("idxGridInscoSelected"), Integer)
        End Get
        Set(ByVal value As Integer)
            ViewState("idxGridInscoSelected") = value
        End Set
    End Property
    Private Property TPLAmountDefault As Double
        Get
            Return CType(ViewState("TPLAmountDefault"), Double)
        End Get
        Set(value As Double)
            ViewState("TPLAmountDefault") = value
        End Set
    End Property
    Private Property totalPremi As Double
        Get
            Return CType(lblPremi.Text, Double)
        End Get
        Set(value As Double)
            lblPremi.Text = FormatNumber(value, 0)
        End Set
    End Property
    Public Property Discount As Double
        Get
            Return CType(lbldiscount.Text, Double)
        End Get
        Set(value As Double)
            lbldiscount.Text = FormatNumber(value, 0)
        End Set
    End Property
    Public Property PremiSetelahDiscount As Double
        Get
            Return CType(lblPremiDiscount.Text, Double)
        End Get
        Set(value As Double)
            lblPremiDiscount.Text = FormatNumber(value, 0)
        End Set
    End Property
    Public Property PremiDiBayar As Double
        Get
            Return CType(lblBayarTunai.Text, Double)
        End Get
        Set(value As Double)
            lblBayarTunai.Text = FormatNumber(value, 0)
        End Set
    End Property
    Public Property PremiDiKredit As Double
        Get
            Return CType(lblPremiDiKredit.Text, Double)
        End Get
        Set(value As Double)
            lblPremiDiKredit.Text = FormatNumber(value, 0)
        End Set
    End Property
    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property ProductID() As String
        Get
            Return ViewState("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE As String = CommonCacheHelper.CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE
    Private oCustomclass As New Parameter.NewAppInsuranceByCompany
    Private oController As New NewAppInsuranceByCompanyController
    Private oControllerPremium As New InsuranceApplicationController
    Private oInsAppController As New InsuranceApplicationController
    Private oCustomClassResult As New Parameter.InsuranceCalculationResult
    Private oInsCalResultController As New InsuranceCalculationResultController
    Private oInsCoAllocationDetailListController As New InsCoAllocationDetailListController
    Private Const COMPANY_CUSTOMER As String = "CompanyCustomer"
    Private Const CACHE_APPLICATION_TYPE As String = "CacheApplicationType"
#End Region

#Region "Controls"
    Protected WithEvents oApplicationType As UcApplicationType
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTab
    Protected WithEvents txtNilaiAksesoris As ucNumberFormat
    Protected WithEvents txtRate As ucNumberFormat
    Protected WithEvents txtAmountRate As ucNumberFormat

    Protected WithEvents txtBayarTunai As ucNumberFormat
    Protected WithEvents txtBiayaPolis As ucNumberFormat
    Protected WithEvents txtBiayaMaterai As ucNumberFormat

    Protected WithEvents ucNilaiPertanggunganCP As ucNumberFormat
    Protected WithEvents ucNilaiPertanggunganAJK As ucNumberFormat
    Protected WithEvents ucRateCP As ucNumberFormat
    Protected WithEvents ucRateAJK As ucNumberFormat
    Protected WithEvents ucPremiCP As ucNumberFormat
    Protected WithEvents ucPremiAJK As ucNumberFormat
    Protected WithEvents ucBiayaPolisCP As ucNumberFormat
    Protected WithEvents ucBiayaPolisAJK As ucNumberFormat

    Protected WithEvents cmbInsuranceComBranch As UcInsuranceBranchName
    Protected WithEvents cmbInsuranceComBranchCP As UcInsuranceBranchName
    Protected WithEvents cmbInsuranceComBranchAJK As UcInsuranceBranchName

#End Region

#Region "Default Panel"
    Sub InitialDefaultPanel()
        lblMessage.Visible = False
        PnlGrid.Visible = False
        LblApplicationType.Visible = False
        pnlInsSelection.Visible = False
        btnOK.Visible = True
        txtNilaiAksesoris.RequiredFieldValidatorEnable = False
        txtNilaiAksesoris.AutoPostBack = True
        oApplicationType.SupplierGroupID = Me.SupplierGroupID
        txtBiayaPolis.isReadOnly = True
        txtBiayaMaterai.isReadOnly = True 
        With txtRate
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .RangeValidatorMinimumValue = "0"
            .RangeValidatorMaximumValue = "100"
            .TextCssClass = "smaller_text"
            .AutoPostBack = True
        End With

        With txtAmountRate
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .AutoPostBack = True
        End With

        txtBayarTunai.OnClientChange = "hitung(this.value)"
        txtBiayaPolis.OnClientChange = "hitungdaripolis(this.value)"
        txtBiayaMaterai.OnClientChange = "hitungdarimaterai(this.value)"

        With cmbInsuranceComBranch
            .FillRequired = True
        End With
        With cmbInsuranceComBranchCP
            .FillRequired = True
        End With
        With cmbInsuranceComBranchAJK
            .FillRequired = True
        End With
    End Sub
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Visible = False
        If Not IsPostBack Then
            InitialDefaultPanel()
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                Me.ApplicationID = Request("ApplicationID")
                Me.PageSource = Request("PageSource")
                Me.SupplierGroupID = Request("SupplierGroupID")
                Me.Mode = Request("Edit")

                If Not getInsEntryStep1() Then Exit Sub
                Me.TPLAmountDefault = getDefaultTPLAmount(CDbl(lblOTR.Text))
            End If

            buttonSwitch("first")

            With ucViewApplication1
                .CustomerID = Me.CustomerID
                .ApplicationID = Me.ApplicationID
                .bindData()

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnOK.Visible = False
                End If

                .initControls("OnNewApplication")
                .CustomerID = Me.CustomerID
                Me.ProductID = .ProductID
                Me.ProductOfferingID = .ProductOfferingID

                If .CustomerType = "P" And .isPAAvailable = True Then
                    rdoPerluasanPersolanAccident.Enabled = True
                Else
                    rdoPerluasanPersolanAccident.Enabled = False
                End If
            End With

            With ucViewCustomerDetail1
                .CustomerID = Me.CustomerID
                .bindCustomerDetail()
            End With

            With ucApplicationTab1
                .ApplicationID = Me.ApplicationID
                .selectedTab("Asuransi")
                .setLink()
            End With

            If ucViewApplication1.isProceed = True Then
                btnOK.Visible = False
            End If
        End If
    End Sub
#End Region


    Protected Function getInsEntryStep1() As Boolean
        Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany
        Dim oInsuranceCom As New Parameter.NewAppInsuranceByCompany

        With oNewAppInsuranceByCompany
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With
        With oInsuranceCom
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With

        Try
            oNewAppInsuranceByCompany = oController.GetInsuranceEntryStep1List(oNewAppInsuranceByCompany)

            With oNewAppInsuranceByCompany
                Me.CustomerName = .CustomerName
                Me.CustomerID = .CustomerID
                lblAssetNewused.Text = .AssetUsageNewUsed
                Me.AssetNewUsed = lblAssetNewused.Text
                lblInsuranceAssetType.Text = .InsuranceAssetDescr
                LblInsuredBy.Text = .InsAssetInsuredByName
                LblPaidBy.Text = .InsAssetPaidByName
                LblAssetUsageID.Text = .AssetUsageID.Trim
                Me.AssetUsageID = LblAssetUsageID.Text.Trim
                LblAssetUsageDescr.Text = .AssetUsageDescr
                txtTenor.Text = CType(.Tenor2, String)
                TxtTenorCredit.Text = CType(.Tenor, String)
                lblOTR.Text = FormatNumber(.TotalOTR, 0)
                txtNilaiAksesoris.Text = FormatNumber(.NilaiAksesoris)
                txtJenisAksesoris.Text = .JenisAksesoris
                TxtInsNotes.Text = CType(.InsNotes, String).Trim
                txtRate.Text = FormatNumber(.sellingRate, 2)
                txtBayarTunai.Text = FormatNumber(.PaidByCust, 0)


                Me.AmountCoverage = CDbl(lblOTR.Text) + CDbl(txtNilaiAksesoris.Text)
                lblAmountCoverage.Text = FormatNumber(Me.AmountCoverage, 0)
                LblMinimumTenor.Text = CType(.MinimumTenor, String)
                LblMaximumTenor.Text = CType(.MaximumTenor, String)
                LblBranchID.Text = .BranchId
                lblWilayahAsuransi.Text = CType(.DescWil, String)
                lblTahunKendaraan.Text = CType(.ManufacturingYear, String)

                If Me.EmpPos = "BM" Then
                    cmbInsuranceComBranch.loadInsBranch(False)
                    cmbInsuranceComBranch.Enabled = True
                Else
                    If .IsBMSave Then
                        cmbInsuranceComBranch.loadInsBranch(False)
                        cmbInsuranceComBranch.Enabled = False
                    Else
                        cmbInsuranceComBranch.loadInsBranch(True)
                        cmbInsuranceComBranch.Enabled = True
                    End If
                End If

                cmbInsuranceComBranch.SelectedValue = .InsuranceComBranchID
                Me.NilaiAdminFeeAwal = 0
                Me.NilaiStampDutyFeeAwal = 0
                If .InsuranceComBranchID <> "" Then
                    txtBiayaPolis.Text = FormatNumber(.BiayaPolis, 2)
                    txtBiayaMaterai.Text = FormatNumber(.BiayaMaterai, 2)
                    Me.NilaiAdminFeeAwal = .BiayaPolis
                    Me.NilaiStampDutyFeeAwal = .BiayaMaterai
                End If

                cmbInsuranceComBranchCP.loadInsBranchbyProduct("CP")
                cmbInsuranceComBranchAJK.loadInsBranchbyProduct("JK")
                cmbInsuranceComBranchAJK.Enabled = True
                cmbInsuranceComBranchCP.Enabled = True

                cmbInsuranceComBranchCP.SelectedValue = .InsuranceComBranchIDCP
                cmbInsuranceComBranchAJK.SelectedValue = .InsuranceComBranchIDAJK

                ucNilaiPertanggunganCP.Text = FormatNumber(.TotalOTR, 0)
                ucNilaiPertanggunganAJK.Text = FormatNumber(.TotalOTR, 0)
                ucRateCP.Text = FormatNumber(.RateCP, 2)
                ucRateAJK.Text = FormatNumber(.RateAJK, 2)
                ucBiayaPolisCP.Text = FormatNumber(.BiayaPolisCP, 2)
                ucBiayaPolisAJK.Text = FormatNumber(.BiayaPolisAJK, 2)
                ucPremiCP.Text = FormatNumber(.RateCP * .TotalOTR / 100, 2)
                ucPremiAJK.Text = FormatNumber(.RateAJK * .TotalOTR / 100, 2)

                If Me.NilaiAdminFeeAwal = 0 And Me.NilaiStampDutyFeeAwal = 0 Then
                    'Get Biaya polis dan biaya materai dari table InsuranceComBranch
                    oInsuranceCom.InsuranceComBranchID = .InsuranceComBranchID
                    Dim insCom As DataTable = oController.GetInsurancehComByBranch(oInsuranceCom)
                    If insCom.Rows.Count > 0 Then
                        Me.NilaiAdminFeeAwal = CDbl(IIf(IsNumeric(insCom.Rows(0).Item("AdminFee")), insCom.Rows(0).Item("AdminFee"), 0))
                        Me.NilaiStampDutyFeeAwal = CDbl(IIf(IsNumeric(insCom.Rows(0).Item("StampDutyFee")), insCom.Rows(0).Item("StampDutyFee"), 0))
                        txtBiayaPolis.Text = FormatNumber(Me.NilaiAdminFeeAwal, 2)
                        txtBiayaMaterai.Text = FormatNumber(Me.NilaiStampDutyFeeAwal, 2)
                    End If
                End If

                calculateAmountRateGross()
                calculatePremiGross()

                rgvTenorCredit.MinimumValue = CType(.MinimumTenor, String)
                rgvTenorCredit.MaximumValue = CType(.MaximumTenor, String)
                rgvTenorCredit.ErrorMessage = "Tenor harus >= " & CType(.MinimumTenor, String) & " dan <= " & CType(.MaximumTenor, String)
                rgvTenor.MinimumValue = CType(6, String)
                rgvTenor.MaximumValue = CType(.MaximumTenor, String)
                rgvTenor.ErrorMessage = "Tenor harus >= " & CType(6, String) & " dan <= " & CType(.MaximumTenor, String)


                Me.MinimumTenor = LblMinimumTenor.Text
                Me.MaximumTenor = LblMaximumTenor.Text
                Me.PMaximumTenor = CStr(.PMaximumTenor)
                Me.PMinimumTenor = CStr(.PMinimumTenor)
                Me.PBMaximumTenor = CStr(.PBMaximumTenor)
                Me.PBMinimumTenor = CStr(.PBMinimumTenor)
                Me.BranchID = .BranchId
                Me.InsuranceType = .InsuranceType
                Me.ManufacturingYear = .ManufacturingYear
                Me.RateCardID = .SesionID

                If .PerluasanPA = True Then
                    rdoPerluasanPersolanAccident.SelectedValue = "True"
                Else
                    rdoPerluasanPersolanAccident.SelectedValue = "False"
                End If

                chkCP.Checked = .CreditProtection
                chkAJK.Checked = .JaminanKredit
            End With

            Return True

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try


    End Function

    Private Sub DisplayAdditionalInsurance()

        panelJaminanKredit.Visible = chkAJK.Checked
        panelCreditProtection.Visible = chkCP.Checked
        divPerusahaanCreditProtection.Visible = chkCP.Checked
        divPerusahaanJaminanKredit.Visible = chkAJK.Checked
        cmbInsuranceComBranchAJK.FillRequired = chkAJK.Checked
        cmbInsuranceComBranchCP.FillRequired = chkCP.Checked

    End Sub

#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        'Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
        Response.Redirect(Request.RawUrl.ToString())
    End Sub
#End Region

#Region "Ketika Tombol OK di Klik "
    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        buttonSwitch("second")
        Me.InsuranceComBranchID_lbl = cmbInsuranceComBranch.SelectedValue.Trim
        lblMessage.Visible = False
        Me.ApplicationTypeID = oApplicationType.AppTypeID.Trim
        LblApplicationType.Text = oApplicationType.AppTypeName.Trim
        LblApplicationType.Visible = True
        txtTenor.Text = CStr(IIf(txtTenor.Text = "", 0, txtTenor.Text))
        LblTenor.Text = txtTenor.Text
        TxtTenorCredit.Text = CStr(IIf(TxtTenorCredit.Text = "", 0, TxtTenorCredit.Text))
        LblTenorCredit.Text = TxtTenorCredit.Text
        Me.Tenor = TxtTenorCredit.Text
        Me.Tenor2 = txtTenor.Text

        Dim jmltenor As Int16 = CType(LblTenorCredit.Text, Int16)
        Dim jmltenor2 As Int16 = CType(LblTenor.Text, Int16)
        Me.AmountCoverage = CType(lblAmountCoverage.Text, Double)

        txtJenisAksesoris.Enabled = False
        txtNilaiAksesoris.Enabled = False
        txtTenor.Enabled = False
        TxtTenorCredit.Enabled = False
        txtRate.Enabled = False
        chkAJK.Enabled = False
        chkCP.Enabled = False

        oApplicationType.Visible = False
        Dim oSetJmlGrid As Int16
        Dim oSetJmlGrid2 As Int16
        Dim oSimpleRule As New CommonSimpleRuleHelper
        Dim tempTipeAsuransi As New DataTable

        tempTipeAsuransi.Columns.Add("Tahun")
        tempTipeAsuransi.Columns.Add("Tipe")

        oSetJmlGrid = oSimpleRule.SetJumlahGrid(jmltenor)
        oSetJmlGrid2 = oSimpleRule.SetJumlahGrid(jmltenor2)
        'Modify wira 20200903
        If oSetJmlGrid = 0 Then
            oSetJmlGrid = jmltenor / 12
        End If
        If oSetJmlGrid2 = 0 Then
            oSetJmlGrid2 = jmltenor / 12
        End If
        ' End Modify
        For i = 0 To oSetJmlGrid - 1
            Dim oRow As DataRow = tempTipeAsuransi.NewRow
            oRow.Item("Tahun") = CStr(i + 1)
            oRow.Item("Tipe") = "ARK"
            tempTipeAsuransi.Rows.Add(oRow)
        Next

        TipeAsuransi = tempTipeAsuransi
        dtgTipeAsuransi.DataSource = TipeAsuransi
        dtgTipeAsuransi.DataBind()

        DisplayAdditionalInsurance()

    End Sub



    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        buttonSwitch("third")

        ' Disabled pilihan tipe asuransi
        Dim CoverageTpye As ucCoverageTypeApk
        Dim CoverageTpyeCombo As DropDownList
        For i = 0 To CType(dtgTipeAsuransi.Items.Count - 1, Int16)
            CoverageTpye = CType(dtgTipeAsuransi.Items(i).FindControl("DDLCoverageTypeDgrid"), ucCoverageTypeApk)
            CoverageTpyeCombo = CType(CoverageTpye.FindControl("ddlCoverageTypeAPK"), DropDownList)
            CoverageTpyeCombo.Enabled = False
            TipeAsuransi.Rows(i).Item("Tipe") = CoverageTpye.SelectedValue
        Next
        cmbInsuranceComBranch.Enabled = False
        cmbInsuranceComBranchCP.Enabled = False
        cmbInsuranceComBranchAJK.Enabled = False

        Me.IsPreview = "0"
        Dim jmltenor As Int16 = CType(LblTenorCredit.Text, Int16)
        Dim jmltenor2 As Int16 = CType(LblTenor.Text, Int16)
        Dim oSetJmlGrid As Int16
        Dim oSetJmlGrid2 As Int16
        Dim oSimpleRule As New CommonSimpleRuleHelper

        oSetJmlGrid = oSimpleRule.SetJumlahGrid(jmltenor)
        'Modify wira 20200903
        If oSetJmlGrid = 0 Then
            oSetJmlGrid = jmltenor / 12
        End If
        ' End Modify
        Me.InsLength = CType(TxtTenorCredit.Text, Int16)
        Context.Trace.Write("Tenor Credit = " + TxtTenorCredit.Text)

        oSetJmlGrid2 = oSimpleRule.SetJumlahGrid(jmltenor2)
        'Modify wira 20200903
        If oSetJmlGrid2 = 0 Then
            oSetJmlGrid2 = jmltenor / 12
        End If
        ' End Modify
        Me.InsLength = CType(txtTenor.Text, Int16)
        Context.Trace.Write("Tenor = " + txtTenor.Text)

        Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany

        With oNewAppInsuranceByCompany
            .JmlGrid = oSetJmlGrid
            .JmlGrid2 = oSetJmlGrid2
            .BranchId = Me.BranchID.Trim
            .strConnection = GetConnectionString()
        End With

        oNewAppInsuranceByCompany = oController.GetInsuranceEntryStep2List(oNewAppInsuranceByCompany)

        Dim dtEntity As New DataTable

        If Not oNewAppInsuranceByCompany Is Nothing Then
            dtEntity = oNewAppInsuranceByCompany.ListData
        End If



        Me.DiscountMin = CDbl(dtEntity.Rows(0).Item("DiscountMin"))
        Me.DiscountMax = CDbl(dtEntity.Rows(0).Item("DiscountMax"))

        DgridInsurance.DataSource = dtEntity
        DgridInsurance.DataBind()

        fillAdditionalAsuransi()
    End Sub

    Private Sub fillAdditionalAsuransi()
        If chkAJK.Checked Or chkCP.Checked Then
            Dim InsurancedValue As Double = CDbl(IIf(IsNumeric(lblOTR.Text), lblOTR.Text, "0"))
            Dim oProduct As New Parameter.Product
            Dim m_offering As New ProductController

            oProduct.ProductId = Me.ProductID
            oProduct.BranchId = Me.sesBranchId.Replace("'", "")
            oProduct.ProductOfferingID = Me.ProductOfferingID
            oProduct.strConnection = GetConnectionString()

            oProduct = m_offering.ProductOfferingView(oProduct)

            If chkAJK.Checked Then
                Dim oInsuranceCom As New Parameter.InsCoBranch
                With oInsuranceCom
                    .strConnection = GetConnectionString()
                    .InsCoBranchID = cmbInsuranceComBranchAJK.SelectedValue
                    .InsuranceProductID = "JK"
                    .Tenor = CInt(Me.Tenor) / 12
                End With
                oInsuranceCom = oController.GetInsurancehComByBranchProduct(oInsuranceCom)
                ucBiayaPolisAJK.Text = FormatNumber(oInsuranceCom.BiayaPolisAdditional, 0)
                setBehaviour(oProduct.AsuransiKreditBehaviour, ucRateAJK, oProduct.AsuransiKredit)
                ucRateAJK.Text = FormatNumber(oProduct.AsuransiKredit, 2)
                ucPremiAJK.Text = FormatNumber(oProduct.AsuransiKredit * InsurancedValue / 100, 0)
            End If
            If chkCP.Checked Then
                Dim oInsuranceCom As New Parameter.InsCoBranch
                With oInsuranceCom
                    .strConnection = GetConnectionString()
                    .InsCoBranchID = cmbInsuranceComBranchAJK.SelectedValue
                    .InsuranceProductID = "CP"
                    .Tenor = CInt(Me.Tenor) / 12
                End With
                oInsuranceCom = oController.GetInsurancehComByBranchProduct(oInsuranceCom)
                ucBiayaPolisCP.Text = FormatNumber(oInsuranceCom.BiayaPolisAdditional, 0)
                setBehaviour(oProduct.CreditProtectionBehaviour, ucRateCP, oProduct.CreditProtection)
                ucRateCP.Text = FormatNumber(oProduct.CreditProtection, 2)
                ucPremiCP.Text = FormatNumber(oProduct.CreditProtection * InsurancedValue / 100, 0)
            End If
        End If
    End Sub

    Public Sub setBehaviour(ByVal Pattern As String, ByRef ucnmber As ucNumberFormat, ByVal value As Double)
        Select Case Pattern
            Case "D"
                ucnmber.RangeValidatorMinimumValue = "0"
                ucnmber.RangeValidatorMaximumValue = "999999999999999"
                ucnmber.RangeValidatorErrorMessage = "Input Salah"
                ucnmber.RangeValidatorEnable = True
            Case "L"
                ucnmber.RangeValidatorMinimumValue = "0"
                ucnmber.RangeValidatorMaximumValue = "999999999999999"
                ucnmber.RangeValidatorErrorMessage = "Input Salah"
                ucnmber.RangeValidatorEnable = True
            Case "N"
                ucnmber.RangeValidatorMinimumValue = value.ToString.Trim
                ucnmber.RangeValidatorMaximumValue = "999999999999999"
                ucnmber.RangeValidatorErrorMessage = "Input harus >= " & value.ToString.Trim & "!"
                ucnmber.RangeValidatorEnable = True
            Case "X"
                ucnmber.RangeValidatorMinimumValue = "0"
                ucnmber.RangeValidatorMaximumValue = value.ToString.Trim
                ucnmber.RangeValidatorErrorMessage = "Input harus <= " & value.ToString.Trim & "!"
                ucnmber.RangeValidatorEnable = True
        End Select
    End Sub
#End Region

#Region "Item DataBound DgridInsurance "
    Private Sub DgridInsurance_DataBinding(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DgridInsurance.ItemDataBound
        Dim CboCoverageRate As New DataTable("CoverageRate")
        Dim DDLCoverageTypeRate As ucCoverageTypeRate
        Dim LblHiddenCoverageTypeRate As Label
        Dim LblPremiumDGrid As Label
        Dim LblHiddenCoverageTypeDGrid As Label
        Dim coverageType As String
        Dim tahun As Integer

        If e.Item.ItemIndex = -1 Then
            '============= Handle CoverageType =============
            'CboCoverageRate = oController.GetInsuranceBranchNameRateCard(GetConnectionString, Me.ApplicationID, cmbInsuranceComBranch.SelectedValue.Trim, coverageType, tahun)
            'Me.CoverageTypeMs = CboCoverageRate
        Else
            coverageType = TipeAsuransi.Rows(e.Item.ItemIndex).Item("Tipe")
            tahun = CInt(TipeAsuransi.Rows(e.Item.ItemIndex).Item("Tahun"))

            '============= Handle CoverageType =============
            CboCoverageRate = oController.GetInsuranceBranchNameRateCard(GetConnectionString, Me.ApplicationID, cmbInsuranceComBranch.SelectedValue.Trim, coverageType, tahun)
            Me.CoverageTypeMs = CboCoverageRate

            DDLCoverageTypeRate = CType(e.Item.FindControl("ddlCoverageTypeRate"), ucCoverageTypeRate)
            LblHiddenCoverageTypeRate = CType(e.Item.FindControl("LblHiddenCoverageTypeRate"), Label)
            LblHiddenCoverageTypeDGrid = CType(e.Item.FindControl("LblHiddenCoverageTypeDGrid"), Label)
            LblHiddenCoverageTypeDGrid.Text = coverageType
            CboCoverageRate = Me.CoverageTypeMs
            Dim cbodvCoverageType As DataView
            cbodvCoverageType = CboCoverageRate.DefaultView

            With DDLCoverageTypeRate
                .DataSource = cbodvCoverageType
                .DataTextField = "Description"
                .DataValueField = "ID"
                .DataBind()
                .ClearSelection()
                .SelectedValue = Trim(LblHiddenCoverageTypeRate.Text)
                .GridIndex = e.Item.ItemIndex
            End With

            LblPremiumDGrid = CType(e.Item.FindControl("LblPremiumDGrid"), Label)

            '============= Handle SRCC =============

            '================= Handle TPL =======================

            Dim txtNilaiTPL As ucNumberFormat
            Dim txtNilaiPA As ucNumberFormat
            Dim txtNilaiPADriver As ucNumberFormat

            txtNilaiTPL = CType(e.Item.FindControl("txtNilaiTPL"), ucNumberFormat)
            txtNilaiTPL.Text = FormatNumber(0, 0)
            txtNilaiTPL.TextCssClass = "small_text numberAlign"
            txtNilaiPA = CType(e.Item.FindControl("txtNilaiPA"), ucNumberFormat)
            txtNilaiPA.Text = FormatNumber(0, 0)
            txtNilaiPA.TextCssClass = "small_text numberAlign"
            txtNilaiPADriver = CType(e.Item.FindControl("txtNilaiPADriver"), ucNumberFormat)
            txtNilaiPADriver.Text = FormatNumber(0, 0)
            txtNilaiPADriver.TextCssClass = "small_text numberAlign"

            '============= Handle FLOOD =============

            '============= Handle RIOT =============

            '============= Handle PA PAX =============

            '============= Handle PA DRIVER =============
            Dim DDLBolPADRIVEROption As DropDownList

            DDLBolPADRIVEROption = CType(e.Item.FindControl("DDLBolPADRIVEROption"), DropDownList)
            If LblHiddenCoverageTypeDGrid.Text = "ARK" Then DDLBolPADRIVEROption.SelectedValue = "1" Else DDLBolPADRIVEROption.SelectedValue = "0"

        End If


    End Sub


    Private Sub dtgTipeAsuransi_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTipeAsuransi.ItemDataBound
        Dim CboCoverageTypeDT As New DataTable("CoverageTypeMs")
        Dim DDLCoverageTypePilihan As ucCoverageTypeApk
        Dim LblHiddenCoverageTypeDGrid As Label

        If e.Item.ItemIndex = -1 Then
            '============= Handle CoverageType =============

            Dim strsqlQuery As String
            strsqlQuery = CommonVariableHelper.SQL_QUERY_COVERAGE_TYPEMS
            Dim objAdapter As New SqlDataAdapter(strsqlQuery, GetConnectionString)
            objAdapter.Fill(CboCoverageTypeDT)
            Me.CoverageTypeMs = CboCoverageTypeDT
        Else

            '============= Handle CoverageType =============

            DDLCoverageTypePilihan = CType(e.Item.FindControl("DDLCoverageTypeDgrid"), ucCoverageTypeApk)
            LblHiddenCoverageTypeDGrid = CType(e.Item.FindControl("LblHiddenCoverageTypeDGrid"), Label)
            CboCoverageTypeDT = Me.CoverageTypeMs
            Dim cbodvCoverageType As DataView
            cbodvCoverageType = CboCoverageTypeDT.DefaultView

            With DDLCoverageTypePilihan
                .DataSource = cbodvCoverageType
                .DataTextField = "Description"
                .DataValueField = "ID"
                .DataBind()
                .ClearSelection()
                .SelectedValue = Trim(LblHiddenCoverageTypeDGrid.Text)
                .GridIndex = e.Item.ItemIndex
            End With

        End If
    End Sub

#End Region


    Private Sub DisplayGridInsco()
        Dim oInsCoAllocationInsCO As New Parameter.InsCoAllocationDetailList
        Dim dtEntityInsCO As New DataTable
        Dim dtEntityInsCOD As New DataTable

        btnSaveInscoSelec.Visible = False
        imbCancelInscoSelec.Visible = False
        pnlPaidHPP.Visible = False
        pnlInsSelection.Visible = False
        With oInsCoAllocationInsCO
            .ApplicationID = Me.ApplicationID
            'TODO  1 cover period seharusnya diambil dari combo box, 
            'di spShadowInsNewAppInsuranceByCompanyLastProcess1 CoverPeriod di hardcode "FT"
            '.CoverPeriodSelection = "FT"
            .CoverPeriodSelection = DDLCoverPeriod.SelectedItem.Value
            .BranchId = Me.BranchID
            .strConnection = GetConnectionString()

            .strWithTPL = ""
            .strWithTPLAmount = ""
            .strWithFlood = ""
            .strWithEQVET = ""
            .strWithSRCC = ""
            .strWithTerrorism = ""
            .strWithPA = ""
            .strWithPAAmount = ""
            .strWithPADriver = ""
            .strWithPADriverAmount = ""
            .strCoverageType = ""
            .NumRows = CType(DgridInsurance.Items.Count, Int32)


            For loopCheckPaid = 0 To CType(DgridInsurance.Items.Count - 1, Int16)
                .strWithTPL += CType(DgridInsurance.Items(loopCheckPaid).FindControl("DDLBolTPLOption"), DropDownList).SelectedValue & ","
                .strWithFlood += CType(DgridInsurance.Items(loopCheckPaid).FindControl("DDLBolFLOODOption"), DropDownList).SelectedValue & ","
                .strWithEQVET += CType(DgridInsurance.Items(loopCheckPaid).FindControl("DDLBolIQVETOption"), DropDownList).SelectedValue & ","
                .strWithSRCC += CType(DgridInsurance.Items(loopCheckPaid).FindControl("DDLBolSRCCOption"), DropDownList).SelectedValue & ","
                .strWithTerrorism += CType(DgridInsurance.Items(loopCheckPaid).FindControl("DDLBolTERROROption"), DropDownList).SelectedValue & ","
                .strWithPA += CType(DgridInsurance.Items(loopCheckPaid).FindControl("DDLBolPAPAXOption"), DropDownList).SelectedValue & ","
                .strWithPADriver += CType(DgridInsurance.Items(loopCheckPaid).FindControl("DDLBolPADRIVEROption"), DropDownList).SelectedValue & ","
                .strWithTPLAmount += CDbl(CType(DgridInsurance.Items(loopCheckPaid).FindControl("txtNilaiTPL"), ucNumberFormat).Text) & ","
                .strWithPAAmount += CDbl(CType(DgridInsurance.Items(loopCheckPaid).FindControl("txtNilaiPA"), ucNumberFormat).Text) & ","
                .strWithPADriverAmount += CDbl(CType(DgridInsurance.Items(loopCheckPaid).FindControl("txtNilaiPADriver"), ucNumberFormat).Text) & ","
                .strCoverageType += CType(DgridInsurance.Items(loopCheckPaid).FindControl("LblHiddenCoverageTypeDGrid"), Label).Text & ","
            Next

            .strWithTPL = .strWithTPL.Substring(0, .strWithTPL.Length - 1)
            .strWithTPLAmount = .strWithTPLAmount.Substring(0, .strWithTPLAmount.Length - 1)
            .strWithFlood = .strWithFlood.Substring(0, .strWithFlood.Length - 1)
            .strWithEQVET = .strWithEQVET.Substring(0, .strWithEQVET.Length - 1)
            .strWithSRCC = .strWithSRCC.Substring(0, .strWithSRCC.Length - 1)
            .strWithTerrorism = .strWithTerrorism.Substring(0, .strWithTerrorism.Length - 1)
            .strWithPA = .strWithPA.Substring(0, .strWithPA.Length - 1)
            .strWithPAAmount = .strWithPAAmount.Substring(0, .strWithPAAmount.Length - 1)
            .strWithPADriver = .strWithPADriver.Substring(0, .strWithPADriver.Length - 1)
            .strWithPADriverAmount = .strWithPADriverAmount.Substring(0, .strWithPADriverAmount.Length - 1)
            .strCoverageType = .strCoverageType.Substring(0, .strCoverageType.Length - 1)

        End With

        Try
            oInsCoAllocationInsCO = oInsCoAllocationDetailListController.GetGridInsCoPremium(oInsCoAllocationInsCO)
            If oInsCoAllocationInsCO.ErrStr <> "" Then
                ShowMessage(lblMessage, oInsCoAllocationInsCO.ErrStr, True)
                Exit Sub
            Else
                lblMessage.Visible = False
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

        If Not oInsCoAllocationInsCO Is Nothing Then
            dtEntityInsCOD = oInsCoAllocationInsCO.ListData2
            dtEntityInsCO = oInsCoAllocationInsCO.ListData
        End If

        gridInsco.DataSource = dtEntityInsCO
        gridInsco.DataBind()

        gridDetailInsco.DataSource = dtEntityInsCOD
        gridDetailInsco.DataBind()


        btnSaveInscoSelec.Visible = True
        imbCancelInscoSelec.Visible = True
        pnlPaidHPP.Visible = True


        pnlInsSelection.Visible = True

        ReBindGridInsurance()
        ReBindGridInsuranceAdditional()
    End Sub

#Region "ReBindGridInsurance"
    Public Sub ReBindGridInsurance()
        Dim oGrid2 As New Parameter.InsuranceCalculationResult

        With oGrid2
            .BranchId = Me.BranchID.Trim
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With

        oGrid2 = oInsCalResultController.DisplayResultOnGrid(oGrid2)
        Dim dtEntity As New DataTable

        If Not oGrid2 Is Nothing Then
            dtEntity = oGrid2.ListData
        End If

        Dgrid2InsurancePaid.DataSource = dtEntity
        Dgrid2InsurancePaid.DataBind()
    End Sub

    Public Sub ReBindGridInsuranceAdditional()
        If (chkCP.Checked) Then
            panelGridCreditProtection.Visible = True
            Dim oGrid2 As New Parameter.InsuranceCalculationResult

            With oGrid2
                .BranchId = Me.BranchID.Trim
                .ApplicationID = Me.ApplicationID.Trim
                .strConnection = GetConnectionString()
            End With

            oGrid2 = oInsCalResultController.DisplayResultOnGridAdditional(oGrid2, "CP")
            Dim dtEntity As New DataTable

            If Not oGrid2 Is Nothing Then
                dtEntity = oGrid2.ListData
            End If

            GridCreditProtection.DataSource = dtEntity
            GridCreditProtection.DataBind()

        End If
        If (chkAJK.Checked) Then
            panelGridJaminanKredit.Visible = True
            Dim oGrid2 As New Parameter.InsuranceCalculationResult

            With oGrid2
                .BranchId = Me.BranchID.Trim
                .ApplicationID = Me.ApplicationID.Trim
                .strConnection = GetConnectionString()
            End With

            oGrid2 = oInsCalResultController.DisplayResultOnGridAdditional(oGrid2, "JK")
            Dim dtEntity As New DataTable

            If Not oGrid2 Is Nothing Then
                dtEntity = oGrid2.ListData
            End If

            GridJaminanKredit.DataSource = dtEntity
            GridJaminanKredit.DataBind()
        End If
    End Sub
#End Region


#Region "Tombol Cancel Paling Bawah !"
    Private Sub btnCancelPalingBawah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imbCancelInscoSelec.Click
        'Response.Redirect("NewAppInsurance.aspx")
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
#End Region

    Private Sub imbPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click

        PremiDiBayar = 0
        Discount = 0
        PremiSetelahDiscount = 0
        totalPremi = 0

        Dim biayapolisAJK As Double = ucBiayaPolisAJK.Text
        Dim biayapolisCP As Double = ucBiayaPolisCP.Text

        ' -------=========||| Untuk Receive Biaya Polis dan Biaya Materai |||===========---------------------
        Dim oInsuranceCom As New Parameter.NewAppInsuranceByCompany
        With oInsuranceCom
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With
        oInsuranceCom.InsuranceComBranchID = cmbInsuranceComBranch.SelectedValue
        Dim insCom As DataTable = oController.GetInsurancehComByBranch(oInsuranceCom)
        If insCom.Rows.Count > 0 Then
            Me.NilaiAdminFeeAwal = CDbl(IIf(IsNumeric(insCom.Rows(0).Item("AdminFee")), insCom.Rows(0).Item("AdminFee"), 0))
            Me.NilaiStampDutyFeeAwal = CDbl(IIf(IsNumeric(insCom.Rows(0).Item("StampDutyFee")), insCom.Rows(0).Item("StampDutyFee"), 0))

            txtBiayaPolis.Text = FormatNumber(Me.NilaiAdminFeeAwal + biayapolisCP + biayapolisAJK, 2)
            txtBiayaMaterai.Text = FormatNumber(Me.NilaiStampDutyFeeAwal, 2)

        End If

        ProsesAsuransi()
        PremiDiBayar = PremiDiBayar + CDec(txtBiayaPolis.Text) + CDec(txtBiayaMaterai.Text)

        If lblMessage.Text = "" Then
            buttonSwitch("forth")
        End If

    End Sub

    Private Sub cekMixedIns()
        Dim i As Integer
        Dim j As Integer
        Dim lblYearNumRate As Label
        Dim ddlCoverageTypeDgrid As ucCoverageTypeApk
        Dim strPrevInsType As String = ""

        j = 0

        For i = 0 To DgridInsurance.Items.Count - 1
            lblYearNumRate = CType(DgridInsurance.Items(i).Cells(1).FindControl("lblYearNumRate"), Label)
            ddlCoverageTypeDgrid = CType(DgridInsurance.Items(i).Cells(1).FindControl("DDLCoverageTypeDgrid"), ucCoverageTypeApk)

            If ddlCoverageTypeDgrid.SelectedValue.Trim <> strPrevInsType Then
                j = 1
                lblYearNumRate.Text = j.ToString
                strPrevInsType = ddlCoverageTypeDgrid.SelectedValue.Trim
            Else
                j += 1
                lblYearNumRate.Text = j.ToString
            End If
        Next
    End Sub

   
    Private Sub gridInsco_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridInsco.ItemDataBound
        If e.Item.ItemIndex > -1 Then
            Dim i As Integer = e.Item.ItemIndex
            Dim lblGridAdminFee As Label
            Dim lblGridGrandTotal As Label
            Dim lblGridMeteraiFee As Label
            If cmbInsuranceComBranch.SelectedValue.Trim = CType(e.Item.FindControl("lblGridInsuranceComBranchID"), Label).Text.Trim Then
                idxGridInscoSelected = i
                e.Item.CssClass = "item_grid item_gridselected"

                lblGridGrandTotal = CType(e.Item.FindControl("lblGridGrandTotal"), Label)
                lblGridAdminFee = CType(e.Item.FindControl("lblGridAdminFee"), Label)
                lblGridMeteraiFee = CType(e.Item.FindControl("lblGridMeteraiFee"), Label)

                txtAmountRate.Text = FormatNumber(CDbl(lblGridGrandTotal.Text) - CDbl(lblGridAdminFee.Text) - CDbl(lblGridMeteraiFee.Text), 0)
                calculatePremiGross()
            End If
        End If
    End Sub

    Public Sub calculateAmountRateGross() Handles txtRate.TextChanged
        Dim rate, npt As Double

        npt = CDbl(IIf(IsNumeric(lblAmountCoverage.Text), lblAmountCoverage.Text, "0"))
        rate = CDbl(IIf(IsNumeric(txtRate.Text), txtRate.Text, "0"))

        txtAmountRate.Text = FormatNumber(npt * rate / 100, 0)
    End Sub

    Public Sub calculateRateGross() Handles txtAmountRate.TextChanged
        Dim rateamt, npt As Double

        npt = CDbl(IIf(IsNumeric(lblAmountCoverage.Text), lblAmountCoverage.Text, "0"))
        rateamt = CDbl(IIf(IsNumeric(txtAmountRate.Text), txtAmountRate.Text, "0"))

        txtRate.Text = FormatNumber(rateamt / npt * 100, 2)
    End Sub

    Public Sub calculatePremiGross() Handles txtNilaiAksesoris.TextChanged, txtRate.TextChanged, txtAmountRate.TextChanged ' txtBayarTunai.TextChanged
        Dim otr, aks, rateamt, byrtunai, bpolis, bmaterai As Double

        otr = CDbl(IIf(IsNumeric(lblOTR.Text), lblOTR.Text, "0"))
        aks = CDbl(IIf(IsNumeric(txtNilaiAksesoris.Text), txtNilaiAksesoris.Text, "0"))
        bpolis = CDbl(IIf(IsNumeric(txtBiayaPolis.Text), txtBiayaPolis.Text, "0"))
        bmaterai = CDbl(IIf(IsNumeric(txtBiayaMaterai.Text), txtBiayaMaterai.Text, "0"))

        lblAmountCoverage.Text = FormatNumber(otr + aks, 0)

        rateamt = CDbl(IIf(IsNumeric(txtAmountRate.Text), txtAmountRate.Text, "0"))

        byrtunai = CDbl(IIf(IsNumeric(txtBayarTunai.Text), txtBayarTunai.Text, "0"))
    End Sub

    Public Function getDefaultTPLAmount(ByVal OTRAmount As Double) As Double
        Dim generalSet As New Parameter.GeneralSetting
        Dim conGeneral As New GeneralSettingController

        With generalSet
            .strConnection = GetConnectionString()

            If OTRAmount <= 150000000 Then
                .GSID = "TPLOTR<150"
            Else
                .GSID = "TPLOTR>150"
            End If
        End With

        generalSet = conGeneral.GetGeneralSettingByID(generalSet)

        If generalSet.ListData.Rows.Count > 0 Then
            Return CDbl(generalSet.ListData.Rows(0).Item("GSValue").ToString.Trim)
        Else
            Return 0
        End If
    End Function

    Private Sub ProsesAsuransi()
        'Save Insurance Asset dan Insurance Asset Detail harus sukses baru proses save HPP
        If saveInsuranceAsset() And saveInsuranceAssetDetail() Then
            saveInsco()
        Else
            Exit Sub
        End If
    End Sub


    Protected Function saveInsuranceAsset() As Boolean
        Dim customClass As New Parameter.InsuranceCalculation

        With customClass
            .InsuranceComBranchID = cmbInsuranceComBranch.SelectedValue
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .ApplicationTypeID = Me.ApplicationTypeID
            .UsageID = Me.AssetUsageID
            .NewUsed = Me.AssetNewUsed
            .strConnection = GetConnectionString()
        End With

        Try
            oInsAppController.ProcessNewAppInsuranceByCompanySaveAddTemporary(customClass)
            Return True
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Return False
        End Try
    End Function

    Protected Function saveInsuranceAssetDetail() As Boolean
        Dim customClass As New Parameter.InsuranceCalculation
        Dim LblYearInsurance As New Label
        Dim lblYearNumRate As Label
        Dim CoverageTpye As Label
        Dim ratecard As ucCoverageTypeRate

        Dim SRCC As DropDownList

        Dim Flood As DropDownList

        Dim PA As DropDownList
        Dim PADriver As DropDownList
        Dim ucTPLOption As DropDownList
        Dim txtNilaiTPL As ucNumberFormat

        For i = 0 To CType(DgridInsurance.Items.Count - 1, Int16)
            LblYearInsurance = CType(DgridInsurance.Items(i).FindControl("LblYearInsurance"), Label)
            lblYearNumRate = CType(DgridInsurance.Items(i).FindControl("lblYearNumRate"), Label)
            CoverageTpye = CType(DgridInsurance.Items(i).FindControl("LblHiddenCoverageTypeDGrid"), Label)
            ratecard = CType(DgridInsurance.Items(i).FindControl("DDLCoverageTypeRate"), ucCoverageTypeRate)
            SRCC = CType(DgridInsurance.Items(i).FindControl("DDLBolSRCCOption"), DropDownList)

            Flood = CType(DgridInsurance.Items(i).FindControl("DDLBolFLOODOption"), DropDownList)
            PA = CType(DgridInsurance.Items(i).FindControl("DDLBolPAPAXOption"), DropDownList)
            PADriver = CType(DgridInsurance.Items(i).FindControl("DDLBolPADRIVEROption"), DropDownList)

            ucTPLOption = CType(DgridInsurance.Items(i).FindControl("DDLBolTPLOption"), DropDownList)
            txtNilaiTPL = CType(DgridInsurance.Items(i).FindControl("txtNilaiTPL"), ucNumberFormat)

            Dim strdatemanufacturing As DateTime = CType("01/01/" & Me.ManufacturingYear, DateTime)

            If strdatemanufacturing < CType("01/01/1900", DateTime) Then strdatemanufacturing = CType("01/01/1900", DateTime)

            With customClass
                .InsuranceComBranchID = cmbInsuranceComBranch.SelectedValue
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .ApplicationTypeID = Me.ApplicationTypeID
                .UsageID = Me.AssetUsageID
                .NewUsed = Me.AssetNewUsed
                .strConnection = GetConnectionString()
                .YearNum = CType(LblYearInsurance.Text, Int16)
                .YearNumRate = 0 ' CType(lblYearNumRate.Text, Int16)
                .CoverageType = CoverageTpye.Text.Trim
                .BolSRCC = CType(SRCC.SelectedItem.Value.Trim, Boolean)
                .BolFlood = CType(Flood.SelectedItem.Value.Trim, Boolean)
                .BolRiot = False
                .BolPA = CType(PA.SelectedItem.Value.Trim, Boolean)
                .bolPADriver = CType(PADriver.SelectedItem.Value.Trim, Boolean)
                .AmountCoverage = Me.AmountCoverage
                .InsuranceType = Me.InsuranceType
                .UsageID = Me.AssetUsageID
                .NewUsed = Me.AssetNewUsed
                .BranchId = Me.BranchID
                .InsLength = Me.InsLength
                .BusinessDate = Me.BusinessDate
                .DateMonthYearManufacturingYear = strdatemanufacturing
                .IsPageSourceCompanyCustomer = CBool(IIf(Me.PageSource.Trim = COMPANY_CUSTOMER, True, False))
                .DiscountToCust = 0
                .bolTPL = CType(ucTPLOption.SelectedValue, Boolean)
                .PaidAmountByCust = 0
                .TPL = CType(txtNilaiTPL.Text, Double)
                .MasterRateCardID = ratecard.SelectedValue.Trim
            End With

            Try
                oInsAppController.ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(customClass)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
                Return False
            End Try
        Next

        Return True
    End Function

    Protected Function saveInsco() As Boolean
        Dim oCustomClass As New Parameter.InsuranceCalculationResult
        Dim paidByCust As Double = CDbl(IIf(IsNumeric(txtBayarTunai.Text), txtBayarTunai.Text, 0))
        Dim bpolis As Double = CDbl(IIf(IsNumeric(txtBiayaPolis.Text), txtBiayaPolis.Text, 0))
        Dim bmaterai As Double = CDbl(IIf(IsNumeric(txtBiayaMaterai.Text), txtBiayaMaterai.Text, 0))
        Dim nilaiAkse As Double = CDbl(IIf(IsNumeric(txtNilaiAksesoris.Text), txtNilaiAksesoris.Text, 0))
        Dim rate As Decimal = CDec(IIf(IsNumeric(txtRate.Text), txtRate.Text, 0))

        With oCustomClass
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AmountCoverage = Me.AmountCoverage
            .AdminFeeToCust = 0
            .MeteraiFeeToCust = 0
            .DiscToCustAmount = 0
            .PaidAmountByCust = paidByCust
            .PremiumBaseForRefundSupp = 0
            .AccNotes = ""
            .InsNotes = TxtInsNotes.Text.Trim
            .InsLength = Me.InsLength
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
            .PremiumToCustAmount = CDbl(txtAmountRate.Text)
            .sellingRate = rate
            .InsuranceComBranchID = cmbInsuranceComBranch.SelectedValue.Trim
            .AdditionalCapitalized = 0
            .AdditionalInsurancePremium = 0
            .RefundToSupplier = 0
            .JenisAksesoris = txtJenisAksesoris.Text
            .NilaiAksesoris = nilaiAkse
            .BiayaPolis = bpolis
            .BiayaMaterai = bmaterai
            .LoginId = Me.Loginid

            .PremiCP = CDbl(IIf(IsNumeric(ucPremiCP.Text), ucPremiCP.Text, 0))
            .BiayaPolisCP = CDbl(IIf(IsNumeric(ucBiayaPolisCP.Text), ucBiayaPolisCP.Text, 0))
            .RateCP = CDbl(IIf(IsNumeric(ucRateCP.Text), ucRateCP.Text, 0))
            .InsComBranchIDCP = cmbInsuranceComBranchCP.SelectedValue.Trim
            .CreditProtection = chkCP.Checked

            .PremiAJK = CDbl(IIf(IsNumeric(ucPremiAJK.Text), ucPremiAJK.Text, 0))
            .BiayaPolisAJK = CDbl(IIf(IsNumeric(ucBiayaPolisAJK.Text), ucBiayaPolisAJK.Text, 0))
            .RateAJK = CDbl(IIf(IsNumeric(ucRateAJK.Text), ucRateAJK.Text, 0))
            .InsComBranchIDAJK = cmbInsuranceComBranchAJK.SelectedValue.Trim
            .JaminanKredit = chkAJK.Checked

        End With

        Try
            oInsAppController.ProcessSaveInsuranceApplicationLastProcess(oCustomClass)
            DisplayGridInsco()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Function

    Public Sub DDLBolOptionHeader_OnTextChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim DDLOptionHeader As DropDownList, DDLOptionHeaderName As String
        DDLOptionHeader = CType(sender, DropDownList)
        DDLOptionHeaderName = DDLOptionHeader.ID

        For Each dt As DataGridItem In DgridInsurance.Items
            CType(dt.FindControl(DDLOptionHeaderName.Substring(0, DDLOptionHeaderName.Length - 6)), DropDownList).SelectedValue = DDLOptionHeader.SelectedValue
        Next

    End Sub


    Private Sub Dgrid2InsurancePaid_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Dgrid2InsurancePaid.ItemDataBound
        Dim txtPaidByCust As New TextBox
        Dim txtMainPremiumToCust As New TextBox
        Dim lblPaidAmtByCust As New TextBox
        Dim cboPaidByCust As DropDownList
        Dim cboPlusDisc As DropDownList
        Dim txtDiscToCust As TextBox
        Dim txtTotal As New TextBox

        If e.Item.ItemIndex > -1 Then

            txtPaidByCust = CType(e.Item.FindControl("txtPaidByCust"), TextBox)
            txtMainPremiumToCust = CType(e.Item.FindControl("txtMainPremiumToCust"), TextBox)
            lblPaidAmtByCust = CType(e.Item.FindControl("lblPaidAmtByCust"), TextBox)
            cboPaidByCust = CType(e.Item.FindControl("cboPaidByCust"), DropDownList)
            txtDiscToCust = CType(e.Item.FindControl("txtDiscToCust"), TextBox)
            txtTotal = CType(e.Item.FindControl("txtTotal"), TextBox)
            cboPlusDisc = CType(e.Item.FindControl("cboPlusDisc"), DropDownList)

            If ucViewApplication1.PaketProgram <> "RGL" Then
                cboPaidByCust.Enabled = False
                cboPlusDisc.Enabled = False
            End If

            cboPaidByCust.SelectedValue = txtPaidByCust.Text.Trim
            If txtPaidByCust.Text = "ONLOAN" Then
                lblPaidAmtByCust.Text = "0"
            Else
                lblPaidAmtByCust.Text = FormatNumber(txtMainPremiumToCust.Text, 0)
                PremiDiBayar = PremiDiBayar + CInt(lblPaidAmtByCust.Text)
            End If

            totalPremi = totalPremi + CInt(txtMainPremiumToCust.Text)
            Discount = Discount + CInt(txtDiscToCust.Text)

            PremiSetelahDiscount = totalPremi - Discount
            PremiDiKredit = PremiDiBayar - PremiSetelahDiscount

            cboPlusDisc.Attributes.Add("OnChange", "PilihPlusMinus(this.value,'" & txtDiscToCust.ClientID & "')")
            cboPaidByCust.Attributes.Add("OnChange", "PilihDibayar(this.value,'" & lblPaidAmtByCust.ClientID & "','" & txtTotal.ClientID & "')")
            txtDiscToCust.Attributes.Add("OnChange", "inputDiscount(this.value,'" & txtMainPremiumToCust.ClientID & "','" & lblPaidAmtByCust.ClientID & "','" & txtTotal.ClientID & "','" & cboPaidByCust.ClientID & "','" & cboPlusDisc.ClientID & "')")
        End If

    End Sub


    Public Sub buttonSwitch(mode As String)
        Select Case mode
            Case "first"
                btnOK.Visible = True
                btnReset.Visible = False
                pnlTipeAsuransi.Visible = False
                btnNext.Visible = False
                PnlGrid.Visible = False
                pnlInsSelection.Visible = False
                btnSaveInscoSelec.Visible = False
            Case "second"
                btnOK.Visible = False
                btnReset.Visible = False
                pnlTipeAsuransi.Visible = True
                btnNext.Visible = True
                PnlGrid.Visible = False
                pnlInsSelection.Visible = False
                btnSaveInscoSelec.Visible = False
            Case "third"
                btnOK.Visible = False
                btnReset.Visible = True
                pnlTipeAsuransi.Visible = True
                btnNext.Visible = False
                PnlGrid.Visible = True
                pnlInsSelection.Visible = False
                btnSaveInscoSelec.Visible = False
            Case "forth"
                btnOK.Visible = False
                btnReset.Visible = False
                pnlTipeAsuransi.Visible = True
                btnNext.Visible = False
                PnlGrid.Visible = True
                pnlInsSelection.Visible = True
                btnSaveInscoSelec.Visible = True
        End Select
    End Sub


    Private Sub btnSaveInscoSelec_Click(sender As Object, e As System.EventArgs) Handles btnSaveInscoSelec.Click
        If SessionInvalid() Then
            Exit Sub
        End If

        Dim PaidByCreditProtection As String = ""
        Dim PremiAdditionalSignCreditProtection As String = ""
        Dim PremiAdditionalAmountCreditProtection As Double
        Dim PremiNettCreditProtection As Double

        Dim PaidByJaminanCredit As String = ""
        Dim PremiAdditionalSignJaminanCredit As String = ""
        Dim PremiAdditionalAmountJaminanCredit As Double
        Dim PremiNettJaminanCredit As Double

        Dim biayapolisAJK As Double = ucBiayaPolisAJK.Text
        Dim biayapolisCP As Double = ucBiayaPolisCP.Text


        Dim lblGrandTotal As Label
        Dim lblMainPremiumSave As Label
        Dim lblSRCCPremiumSave As Label
        Dim lblTPLPremiumSave As Label
        Dim lblFloodPremiumSave As Label
        Dim lblLoadingFeeSave As Label
        Dim lblAdminFeeSave As Label
        Dim lblMeteraiFeeSave As Label
        Dim lblGrandTotalSave As Label
        Dim lblPAPass As Label
        Dim lblPADriver As Label
        Dim lblGridInscoDEQVETPremium As Label
        Dim lblGridInscoDTerrorismPremium As Label


        lblGrandTotal = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridGrandTotal"), Label)
        lblMainPremiumSave = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridMainPremium"), Label)
        lblSRCCPremiumSave = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridSRCCPremium"), Label)
        lblTPLPremiumSave = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridTPLPremium"), Label)
        lblFloodPremiumSave = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridFloodPremium"), Label)
        lblLoadingFeeSave = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridLoadingFee"), Label)
        lblAdminFeeSave = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridAdminFee"), Label)
        lblMeteraiFeeSave = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridMeteraiFee"), Label)
        lblGrandTotalSave = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridGrandTotal"), Label)
        lblPAPass = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblPAPass"), Label)
        lblPADriver = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblPADriver"), Label)
        'lblGridInscoDEQVETPremium = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridInscoDEQVETPremium"), Label)
        'lblGridInscoDTerrorismPremium = CType(gridInsco.Items(idxGridInscoSelected).FindControl("lblGridInscoDTerrorismPremium"), Label)

        Dim oSaveInsuranceCompanySelection As New Parameter.InsCoAllocationDetailList
        Dim oController As New InsCoAllocationDetailListController

        'Tampung data customerpaid
        Dim YearNum_ As New Label
        Dim PaidByCust_ As DropDownList
        Dim Discount_ As New TextBox
        Dim Premi_ As New TextBox
        Dim myDataRow As DataRow
        Dim datacolumn As DataColumn
        Dim DTpaidCust As New DataTable
        Dim PlusMinus As DropDownList


        datacolumn = New DataColumn
        datacolumn.ColumnName = "YearNum"
        DTpaidCust.Columns.Add(datacolumn)

        datacolumn = New DataColumn
        datacolumn.ColumnName = "PaidByCust"
        DTpaidCust.Columns.Add(datacolumn)

        datacolumn = New DataColumn
        datacolumn.ColumnName = "DiscToCust"
        DTpaidCust.Columns.Add(datacolumn)

        datacolumn = New DataColumn
        datacolumn.ColumnName = "ApplicationID"
        DTpaidCust.Columns.Add(datacolumn)

        datacolumn = New DataColumn
        datacolumn.ColumnName = "PlusMinus"
        DTpaidCust.Columns.Add(datacolumn)

        For i = 0 To Dgrid2InsurancePaid.Items.Count - 1
            YearNum_ = CType(Dgrid2InsurancePaid.Items(i).FindControl("lblYearNum"), Label)
            PaidByCust_ = CType(Dgrid2InsurancePaid.Items(i).FindControl("cboPaidByCust"), DropDownList)
            Discount_ = CType(Dgrid2InsurancePaid.Items(i).FindControl("txtDiscToCust"), TextBox)
            PlusMinus = CType(Dgrid2InsurancePaid.Items(i).FindControl("cboPlusDisc"), DropDownList)
            myDataRow = DTpaidCust.NewRow()
            myDataRow("YearNum") = YearNum_.Text.Trim
            myDataRow("PaidByCust") = PaidByCust_.Text.Trim
            myDataRow("DiscToCust") = CDbl(Discount_.Text.Trim)
            myDataRow("ApplicationID") = Me.ApplicationID
            myDataRow("PlusMinus") = PlusMinus.SelectedValue.Trim
            DTpaidCust.Rows.Add(myDataRow)
        Next

        If (chkCP.Checked) Then
            For i = 0 To GridCreditProtection.Items.Count - 1
                PaidByCust_ = CType(GridCreditProtection.Items(i).FindControl("cboPaidByCust"), DropDownList)
                Discount_ = CType(GridCreditProtection.Items(i).FindControl("txtDiscToCust"), TextBox)
                Premi_ = CType(GridCreditProtection.Items(i).FindControl("txtMainPremiumToCust"), TextBox)
                PlusMinus = CType(GridCreditProtection.Items(i).FindControl("cboPlusDisc"), DropDownList)
                PaidByCreditProtection = PaidByCust_.Text.Trim
                PremiAdditionalAmountCreditProtection = CDbl(Discount_.Text.Trim)
                PremiAdditionalSignCreditProtection = PlusMinus.SelectedValue.Trim
                PremiNettCreditProtection = IIf(PlusMinus.SelectedValue.Trim = "+", CDbl(Premi_.Text.Trim) + PremiAdditionalAmountCreditProtection, CDbl(Premi_.Text.Trim) - PremiAdditionalAmountCreditProtection)
            Next
        End If

        If (chkAJK.Checked) Then
            For i = 0 To GridJaminanKredit.Items.Count - 1
                PaidByCust_ = CType(GridJaminanKredit.Items(i).FindControl("cboPaidByCust"), DropDownList)
                Discount_ = CType(GridJaminanKredit.Items(i).FindControl("txtDiscToCust"), TextBox)
                Premi_ = CType(GridJaminanKredit.Items(i).FindControl("txtMainPremiumToCust"), TextBox)
                PlusMinus = CType(GridJaminanKredit.Items(i).FindControl("cboPlusDisc"), DropDownList)
                PaidByJaminanCredit = PaidByCust_.Text.Trim
                PremiAdditionalAmountJaminanCredit = CDbl(Discount_.Text.Trim)
                PremiAdditionalSignJaminanCredit = PlusMinus.SelectedValue.Trim
                PremiNettJaminanCredit = IIf(PlusMinus.SelectedValue.Trim = "+", CDbl(Premi_.Text.Trim) + PremiAdditionalAmountJaminanCredit, CDbl(Premi_.Text.Trim) - PremiAdditionalAmountJaminanCredit)
            Next
        End If


        With oSaveInsuranceCompanySelection
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .StartDate = Me.BusinessDate
            .EndDate = DateAdd(DateInterval.Month, CDbl(TxtTenorCredit.Text), Me.BusinessDate)
            .Tenor2 = txtTenor.Text
            .InsuranceComBranchID = cmbInsuranceComBranch.SelectedValue.Trim
            .StatMonth = CType(Me.BusinessDate.Month, Int16)
            .StatYear = CType(Me.BusinessDate.Year, Int16)
            .BusinessDate = Me.BusinessDate
            .CoyID = Me.SesCompanyID
            '.CoverPeriodSelection = "FT"
            .CoverPeriodSelection = DDLCoverPeriod.SelectedItem.Value
            .IsInscoSelectOnEntriApp = True
            .PaidAmountByCust = CDbl(lblBayarTunai.Text)
            .strConnection = GetConnectionString()

            ''input dari textboxt premi gross
            '.TotalPremiManual = CType(lblPremiDiscount.Text, Double) 'CType(txtAmountRate.Text, Double)
            'If cboAppandTo.SelectedValue = "PH" Then
            '    .TotalPremiumToInsco = CType(lblPremiDiKredit.Text, Double)  'CType(lblGrandTotalSave.Text, Double)
            '    .InsAmountToNPV = 0
            'Else
            '    .TotalPremiumToInsco = 0
            '    .InsAmountToNPV = CType(lblPremiDiKredit.Text, Double)
            'End If

            .TotalPremiManual = CType(txtAmountRate.Text, Double)
            '.TotalPremiumToInsco = CType(lblGrandTotalSave.Text, Double) '- CType(lblMeteraiFeeSave.Text, Double) - CType(lblAdminFeeSave.Text, Double)
            'Modify by Wira 20171115
            'TotalPremiumToInsco ambil dari premi di kredit
            .TotalPremiumToInsco = CType(lblPremiDiKredit.Text, Double)
            .InsAmountToNPV = 0

            .MainPremiumToInsco = CType(lblMainPremiumSave.Text, Double)
            .RSCCToInsco = CType(lblSRCCPremiumSave.Text, Double)
            .TPLToInsco = CType(lblTPLPremiumSave.Text, Double)
            .FloodToInsco = CType(lblFloodPremiumSave.Text, Double)
            .LoadingFeeToInsco = CType(lblLoadingFeeSave.Text, Double)
            .AdminFee = CType(lblAdminFeeSave.Text, Double)
            .MeteraiFee = CType(lblMeteraiFeeSave.Text, Double)
            .PAPassanger = CDbl(lblPAPass.Text)
            .PADriver = CDbl(lblPADriver.Text)
            .BiayaPolis = CDbl(txtBiayaPolis.Text) - biayapolisAJK - biayapolisCP

            '.EQVETToInsco = CType(lblGridInscoDEQVETPremium.Text, Double)
            '.TERRORISMToInsco = CType(lblGridInscoDTerrorismPremium.Text, Double)

            .PerluasanPA = CBool(rdoPerluasanPersolanAccident.SelectedValue.ToString.Trim)
            .DataTablePaid = DTpaidCust

            .PaidByCreditProtection = PaidByCreditProtection
            .PremiAdditionalSignCreditProtection = PremiAdditionalSignCreditProtection
            .PremiAdditionalAmountCreditProtection = PremiAdditionalAmountCreditProtection
            .PremiNettCreditProtection = PremiNettCreditProtection

            .PaidByJaminanCredit = PaidByJaminanCredit
            .PremiAdditionalSignJaminanCredit = PremiAdditionalSignJaminanCredit
            .PremiAdditionalAmountJaminanCredit = PremiAdditionalAmountJaminanCredit
            .PremiNettJaminanCredit = PremiNettJaminanCredit
        End With

        

        Try
            oController.SaveInsuranceCompanySelectionLastProcess(oSaveInsuranceCompanySelection)
            calculatePremiGross()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.setLink()
            ShowMessage(lblMessage, "Data saved!", False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub GridCreditProtection_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridCreditProtection.ItemDataBound
        Dim txtPaidByCust As New TextBox
        Dim txtMainPremiumToCust As New TextBox
        Dim lblPaidAmtByCust As New TextBox
        Dim cboPaidByCust As DropDownList
        Dim cboPlusDisc As DropDownList
        Dim txtDiscToCust As TextBox
        Dim txtTotal As New TextBox

        If e.Item.ItemIndex > -1 Then

            txtPaidByCust = CType(e.Item.FindControl("txtPaidByCust"), TextBox)
            txtMainPremiumToCust = CType(e.Item.FindControl("txtMainPremiumToCust"), TextBox)
            lblPaidAmtByCust = CType(e.Item.FindControl("lblPaidAmtByCust"), TextBox)
            cboPaidByCust = CType(e.Item.FindControl("cboPaidByCust"), DropDownList)
            txtDiscToCust = CType(e.Item.FindControl("txtDiscToCust"), TextBox)
            txtTotal = CType(e.Item.FindControl("txtTotal"), TextBox)
            cboPlusDisc = CType(e.Item.FindControl("cboPlusDisc"), DropDownList)

            If ucViewApplication1.PaketProgram <> "RGL" Then
                cboPaidByCust.Enabled = False
                cboPlusDisc.Enabled = False
            End If

            cboPaidByCust.SelectedValue = txtPaidByCust.Text.Trim
            If txtPaidByCust.Text = "ONLOAN" Then
                lblPaidAmtByCust.Text = "0"
            Else
                lblPaidAmtByCust.Text = FormatNumber(txtMainPremiumToCust.Text, 0)
                PremiDiBayar = PremiDiBayar + CInt(lblPaidAmtByCust.Text)
            End If

            totalPremi = totalPremi + CInt(txtMainPremiumToCust.Text)
            Discount = Discount + CInt(txtDiscToCust.Text)

            PremiSetelahDiscount = totalPremi - Discount
            PremiDiKredit = PremiDiBayar - PremiSetelahDiscount

            cboPlusDisc.Attributes.Add("OnChange", "PilihPlusMinus(this.value,'" & txtDiscToCust.ClientID & "')")
            cboPaidByCust.Attributes.Add("OnChange", "PilihDibayar(this.value,'" & lblPaidAmtByCust.ClientID & "','" & txtTotal.ClientID & "')")
            txtDiscToCust.Attributes.Add("OnChange", "inputDiscount(this.value,'" & txtMainPremiumToCust.ClientID & "','" & lblPaidAmtByCust.ClientID & "','" & txtTotal.ClientID & "','" & cboPaidByCust.ClientID & "','" & cboPlusDisc.ClientID & "')")
        End If


    End Sub

    Private Sub GridJaminanKredit_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridJaminanKredit.ItemDataBound
        Dim txtPaidByCust As New TextBox
        Dim txtMainPremiumToCust As New TextBox
        Dim lblPaidAmtByCust As New TextBox
        Dim cboPaidByCust As DropDownList
        Dim cboPlusDisc As DropDownList
        Dim txtDiscToCust As TextBox
        Dim txtTotal As New TextBox

        If e.Item.ItemIndex > -1 Then

            txtPaidByCust = CType(e.Item.FindControl("txtPaidByCust"), TextBox)
            txtMainPremiumToCust = CType(e.Item.FindControl("txtMainPremiumToCust"), TextBox)
            lblPaidAmtByCust = CType(e.Item.FindControl("lblPaidAmtByCust"), TextBox)
            cboPaidByCust = CType(e.Item.FindControl("cboPaidByCust"), DropDownList)
            txtDiscToCust = CType(e.Item.FindControl("txtDiscToCust"), TextBox)
            txtTotal = CType(e.Item.FindControl("txtTotal"), TextBox)
            cboPlusDisc = CType(e.Item.FindControl("cboPlusDisc"), DropDownList)

            If ucViewApplication1.PaketProgram <> "RGL" Then
                cboPaidByCust.Enabled = False
                cboPlusDisc.Enabled = False
            End If

            cboPaidByCust.SelectedValue = txtPaidByCust.Text.Trim
            If txtPaidByCust.Text = "ONLOAN" Then
                lblPaidAmtByCust.Text = "0"
            Else
                lblPaidAmtByCust.Text = FormatNumber(txtMainPremiumToCust.Text, 0)
                PremiDiBayar = PremiDiBayar + CInt(lblPaidAmtByCust.Text)
            End If

            totalPremi = totalPremi + CInt(txtMainPremiumToCust.Text)
            Discount = Discount + CInt(txtDiscToCust.Text)

            PremiSetelahDiscount = totalPremi - Discount
            PremiDiKredit = PremiDiBayar - PremiSetelahDiscount

            cboPlusDisc.Attributes.Add("OnChange", "PilihPlusMinus(this.value,'" & txtDiscToCust.ClientID & "')")
            cboPaidByCust.Attributes.Add("OnChange", "PilihDibayar(this.value,'" & lblPaidAmtByCust.ClientID & "','" & txtTotal.ClientID & "')")
            txtDiscToCust.Attributes.Add("OnChange", "inputDiscount(this.value,'" & txtMainPremiumToCust.ClientID & "','" & lblPaidAmtByCust.ClientID & "','" & txtTotal.ClientID & "','" & cboPaidByCust.ClientID & "','" & cboPlusDisc.ClientID & "')")
        End If

    End Sub
End Class