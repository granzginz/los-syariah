﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewAppInsurance.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.NewAppInsurance" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../../../webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NewAppInsurance</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function OpenWin(pApplicationID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinProductOfferingBranchView(pProductOfferingID, pProductID, pBranchID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.SalesMarketing/Setting/ProductMnt/ProductOfferingBranchView.aspx?Style=AccAcq&ProductOfferingID=' + pProductOfferingID + '&ProductID=' + pProductID + '&BranchID=' + pBranchID, 'Product', 'left=50, top=10, width=900, height=650, menubar=0, scrollbars=1');
        }

        function OpenWinEmployee(pBranchID, pAOID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pAOID, 'Employee', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <div>
                <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            </div>
            <h3>
                DAFTAR APLIKASI
            </h3>
        </div>
    </div>
    <asp:Panel ID="PnlListNewAppInsurance" runat="server">
        <div class="form_box">
            <div class="form_single">
                <asp:DataGrid ID="dtgPagingNewAppInsurance" runat="server" HorizontalAlign="Center"
                    Width="100%" DataKeyField="ApplicationID" CellSpacing="1" CellPadding="3" BorderWidth="0px"
                    OnSortCommand="Sorting" AutoGenerateColumns="False" AllowSorting="True">
                    <AlternatingItemStyle></AlternatingItemStyle>
                    <ItemStyle></ItemStyle>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server">
                            <a href='<%# GetHyperLink(DataBinder.eval(Container,"DataItem.InsAssetInsuredBy"), DataBinder.eval(Container,"DataItem.InsAssetPaidBy")) %>&Applicationid=<%# DataBinder.eval(Container,"DataItem.ApplicationID") %>&SupplierGroupID=<%# DataBinder.eval(Container,"DataItem.SupplierGroupID") %>'>
                                ASURANSI
                            </a>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="ProductOfferingID" HeaderText="PRODOCT OFFERING ID">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="ProductID" HeaderText="PRODOCT ID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="BranchID" HeaderText="BRANCH ID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="CustomerID" HeaderText="CUSTOMER ID">
                        </asp:BoundColumn>
                        <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkApplicationID" runat="server" CausesValidation="false" Text='<%#Container.DataItem("ApplicationID")%>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCust" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.CustomerName") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ProductOfferingID" HeaderText="PRODUK JUAL">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkProdOff" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.ProductOfferingID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CMO" SortExpression="AOName">
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAO" Text='<%# DataBinder.eval(Container,"DataItem.AOName")%>'
                                    NavigateUrl=''>

                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="AOID" Visible="False">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAOID" Text='<%# DataBinder.eval(Container,"DataItem.AOID")%>'
                                    Visible="false">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="InsAssetInsuredByName" SortExpression="InsAssetInsuredByName"
                            HeaderText="ASURANSI OLEH"></asp:BoundColumn>
                        <asp:BoundColumn DataField="InsAssetPaidByName" SortExpression="InsAssetPaidByName"
                            HeaderText="BAYAR OLEH"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="SupplierGroupID" HeaderText="SupplierGroupID">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
                <div>
                    <asp:ImageButton ID="imbFirstPage" runat="server" OnCommand="NavigationLink_Click"
                        CommandName="First" CausesValidation="False" ImageUrl="../../../../Images/grid_navbutton01.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" OnCommand="NavigationLink_Click"
                        CommandName="Prev" CausesValidation="False" ImageUrl="../../../../Images/grid_navbutton02.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" OnCommand="NavigationLink_Click"
                        CommandName="Next" CausesValidation="False" ImageUrl="../../../../Images/grid_navbutton03.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" OnCommand="NavigationLink_Click"
                        CommandName="Last" CausesValidation="False" ImageUrl="../../../../Images/grid_navbutton04.png">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" cssClass="small_text" >1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"/>
                    <asp:RangeValidator ID="rgvGo" runat="server" 
                         Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" ControlToValidate="txtGopage" CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" 
                         ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage"
                        Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    MENCARI ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text = "Search" CssClass="small button blue">
            </asp:ImageButton>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:ImageButton>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
