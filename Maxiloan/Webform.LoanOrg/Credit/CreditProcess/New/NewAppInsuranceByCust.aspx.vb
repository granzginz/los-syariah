﻿#Region "Import"
Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class NewAppInsuranceByCust
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oApplicationType As UcApplicationType
    Protected WithEvents oExpiredDate As ValidDate
    Protected WithEvents oCoverageType As UcCoverageType
#Region "Property"
    Private Property ExistingPolicyNo() As String
        Get
            Return (CType(viewstate("ExistingPolicyNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ExistingPolicyNo") = Value
        End Set
    End Property
    Private Property Tenor() As String
        Get
            Return (CType(viewstate("Tenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Tenor") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return (CType(viewstate("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property PBMaximumTenor() As String
        Get
            Return (CType(viewstate("PBMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PBMaximumTenor") = Value
        End Set
    End Property
    Private Property PBMinimumTenor() As String
        Get
            Return (CType(viewstate("PBMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PBMinimumTenor") = Value
        End Set
    End Property
    Private Property PMaximumTenor() As String
        Get
            Return (CType(viewstate("PMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PMaximumTenor") = Value
        End Set
    End Property
    Private Property PMinimumTenor() As String
        Get
            Return (CType(viewstate("PMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PMinimumTenor") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return (CType(viewstate("CustomerName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Private Property CoverageTypeID() As String
        Get
            Return (CType(viewstate("CoverageTypeID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CoverageTypeID") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property ExpiredDate() As String
        Get
            Return (CType(viewstate("ExpiredDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ExpiredDate") = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return (CType(viewstate("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InterestType") = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return (CType(viewstate("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InstallmentScheme") = Value
        End Set
    End Property

#End Region
#Region "Constanta"
    Private oController As New NewAppInsuranceByCustController

#End Region
#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oApplication As New Parameter.Application
        Dim m_ControllerApp As New ApplicationController
        If sessioninvalid() Then
            Exit Sub
        End If


        'If Not CheckForm(Me.Loginid, CommonVariableHelper.FORM_NAME_NEW_APPLICATION_BY_COMPANY, CommonVariableHelper.APPLICATION_NAME) Then
        '    Exit Sub
        'End If

        If Not IsPostBack Then
            oApplicationType.SupplierGroupID = Request.QueryString("SupplierGroupID")
            'Me.Tenor = Request.QueryString("Tenor")
            'Me.ExistingPolicyNo = Request.QueryString("ExistingPolicyNo")
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then

                LblErrorMessages.Text = ""
                Me.ApplicationID = Request.QueryString("ApplicationID")
                oExpiredDate.Display = "Dynamic"
                oExpiredDate.FillRequired = False
                oExpiredDate.isCalendarPostBack = False
                oExpiredDate.FieldRequiredMessage = "Harap isi Tanggal Jatuh Tempo"
                oExpiredDate.ValidationErrMessage = "harap isi tanggal dengan format dd/MM/yyyy"
                Me.ExpiredDate = oExpiredDate.dateValue
                TxtPolicyNumber.Text = Me.ExistingPolicyNo
                TxtTenorCredit.Text = Me.Tenor
                oCoverageType.FillRequired = True
                Dim oNewAppInsByCust As New Parameter.NewAppInsuranceByCust
                oApplication.BranchId = Replace(Me.sesBranchId, "'", "")
                oApplication.ApplicationID = Me.ApplicationID
                oApplication.strConnection = GetConnectionString()
                oApplication = m_ControllerApp.GetDataAppIDProspect(oApplication)
                If oApplication.Err = "" Then
                    If Me.ApplicationID.Trim = oApplication.ApplicationID.Trim Then
                        TxtPolicyNumber.Text = oApplication.ExistingPolicy
                        TxtTenorCredit.Text = CStr(oApplication.Tenor)
                    End If
                End If
                With oNewAppInsByCust
                    .ApplicationID = Me.ApplicationID
                    .strConnection = GetConnectionString()
                End With

                Try
                    oNewAppInsByCust = oController.GetInsuredByCustomer(oNewAppInsByCust)
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try

                With oNewAppInsByCust
                    LblBranchID.Text = .BranchId
                    LblApplicationID.Text = .ApplicationID
                    LblCustomerName.Text = .CustomerName
                    Me.CustomerName = LblCustomerName.Text
                    LblInsuredByName.Text = .InsAssetInsuredByName
                    LblPaidByName.Text = .InsAssetPaidByName
                    LblMinimumTenor.Text = CType(.MinimumTenor, String)
                    LblMaximumTenor.Text = CType(.MaximumTenor, String)
                    Me.PBMaximumTenor = CType(.PBMaximumTenor, String)
                    Me.PBMinimumTenor = CType(.PBMinimumTenor, String)
                    Me.PMaximumTenor = CType(.PMaximumTenor, String)
                    Me.PMinimumTenor = CType(.PMinimumTenor, String)
                    Me.InterestType = .InterestType
                    Me.InstallmentScheme = .InstallmentScheme
                    Me.CustomerID = .CustomerID

                End With
            End If
        End If


    End Sub

#End Region
#Region "Save "


    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOK.Click


        Me.CoverageTypeID = oCoverageType.CoverageTypeID.Trim
        If Me.CoverageTypeID = "0" Then
            Me.CoverageTypeID = "-"
        End If




        If validasi() Then
            'save
            Dim oNewAppInsByCust As New Parameter.NewAppInsuranceByCust

            If TxtAmountCoverage.Text.Trim = "" Then TxtAmountCoverage.Text = "0"

            With oNewAppInsByCust
                .BranchId = LblBranchID.Text
                .ApplicationID = Me.ApplicationID
                .InsuranceCompanyName = TxtInsuranceCompany.Text
                .SumInsured = CType(TxtAmountCoverage.Text, Decimal)
                .CoverageType = Me.CoverageTypeID
                .PolicyNo = TxtPolicyNumber.Text
                .ExpiredDate = ConvertDate2(Me.ExpiredDate)
                .InsNotes = txtinsurancenotes.Text
                .Tenor = CType(TxtTenorCredit.Text, Double)
                .BusinessDate = Me.BusinessDate
                .ApplicationTypeDescr = oApplicationType.AppTypeID.Trim

                .strConnection = GetConnectionString
            End With

            Try
                oController.ProcessSaveInsuranceByCustomer(oNewAppInsByCust)
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try


            If CheckBoxEntryFinancial.Checked Then
                SendCookies()
                Response.Redirect("FinancialData_002.aspx")

            Else
                Response.Redirect("NewAppInsurance.aspx")
            End If

        Else
            'tidak disave
        End If


    End Sub
#End Region
#Region "Validasi "


    Public Function validasi() As Boolean

        Dim MinimumTenor As Int16
        Dim MaximumTenor As Int16
        Dim TenorEntry As Int16
        MinimumTenor = CType(LblMinimumTenor.Text, Int16)
        MaximumTenor = CType(LblMaximumTenor.Text, Int16)
        TenorEntry = CType(TxtTenorCredit.Text, Int16)

        If TenorEntry < MinimumTenor Or TenorEntry > MaximumTenor Then
            LblErrorMessages.Text = "Jangka Waktu harus diantara " & CType(LblMinimumTenor.Text, Int16) & " dan " & CType(LblMaximumTenor.Text, Int16) & " (dari Produk Jual)!"
            Return False
        End If
        If TenorEntry < CType(Me.PBMinimumTenor, Int16) Or TenorEntry > CType(Me.PBMaximumTenor, Int16) Then
            LblErrorMessages.Text = "Jangka Waktu harus diantara " & Me.PBMinimumTenor & " dan " & Me.PBMaximumTenor & " (dari Produk Cabang)! "
            Return False
        End If
        If TenorEntry < CType(Me.PMinimumTenor, Int16) Or TenorEntry > CType(Me.PMaximumTenor, Int16) Then
            LblErrorMessages.Text = "Jangka Waktu harus diantara " & Me.PBMinimumTenor & " dan " & Me.PMaximumTenor & " (dari Produk)!"
            Return False
        End If

        If Me.ExpiredDate <> "" Then
            Dim endTenordate As Date
            endTenordate = DateAdd(DateInterval.Month, TenorEntry, Me.BusinessDate)
            If ConvertDate2(Me.ExpiredDate) < endTenordate Then
                LblErrorMessages.Text = "Tgl Jatuh tempo harus lebih besar dari " & endTenordate
                Return False
            End If
        Else

            Me.ExpiredDate = "01/01/1900"
        End If

        Return True

    End Function

#End Region
#Region "Cancel "

    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
        Response.Redirect("NewAppInsurance.aspx")

    End Sub

#End Region
#Region "SendCookies"
    Sub SendCookies()

        Dim cookie As New HttpCookie("Financial")
        cookie.Values.Add("id", Me.ApplicationID)
        cookie.Values.Add("custid", Me.CustomerID)
        cookie.Values.Add("name", Me.CustomerName)
        cookie.Values.Add("EmployeeID", "")
        cookie.Values.Add("InterestType", Me.InterestType)
        cookie.Values.Add("InstallmentScheme", Me.InstallmentScheme)
        Response.AppendCookie(cookie)


    End Sub
#End Region

End Class