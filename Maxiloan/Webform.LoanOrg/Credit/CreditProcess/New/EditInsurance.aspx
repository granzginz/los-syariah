﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditInsurance.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.EditInsurance" %>

<%@ Register TagPrefix="uc1" TagName="UcApplicationType" Src="../../../../Webform.UserController/UcApplicationType.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditInsurance</title>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinApplication(pApplicationID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'Application', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;		
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnl1" runat="server" Width="100%" HorizontalAlign="center">
        <asp:Label ID="LblErrorMessages" runat="server" Font-Size="8" 
            ></asp:Label>
        <table id="Table3" cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr>
                <td colspan="3">
                    <font color="red"><em> Mandatory</em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Asuransi dihitung Berdasarkan<strong><em>Per Tahun</em></strong></font>
                </td>
            </tr>
        </table>
        <table id="Table9" cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr class="trtopi">
                <td class="tdtopi" align="center">
                    EDIT ASURANSI
                </td>
            </tr>
        </table>
        <table class="tablegrid" id="Table5" cellspacing="1" cellpadding="2" width="95%"
            border="0">
            <tr>
                <td class="tdgenap">
                    No Aplikasi
                </td>
                <td class="tdganjil">
                    <asp:Label ID="LblApplicationID" runat="server" Visible="False"></asp:Label>
                    <asp:Label ID="LblInsuranceType" runat="server" Visible="False"></asp:Label>
                    <asp:HyperLink ID="HplinkApplicationID" runat="server" NavigateUrl=""></asp:HyperLink>
                </td>
                <td class="tdgenap">
                    Nama Customer
                </td>
                <td class="tdganjil">
                    <asp:Label ID="lblCustomerName" runat="server" Visible="false">lblCustomerName</asp:Label>
                    <asp:HyperLink ID="HpLinkCustName" runat="server" NavigateUrl=""></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Nama Asset
                </td>
                <td class="tdganjil" colspan="3">
                    <asp:Label ID="LblAssetDescr" runat="server">LblAssetDescr</asp:Label>
                    <asp:Label ID="LblManufacturingYear" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Jenis Asuransi Asset
                </td>
                <td class="tdganjil">
                    <asp:Label ID="lblInsuranceAssetType" runat="server">lblInsuranceAssetType</asp:Label>
                </td>
                <td class="tdgenap">
                    Penggunaan Asset
                </td>
                <td class="tdganjil">
                    <asp:Label ID="LblAssetUsageDescr" runat="server">LblAssetUsageDescr</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Asset : New/Used
                </td>
                <td class="tdganjil">
                    <asp:Label ID="LblAssetNewused" runat="server">LblAssetNewused</asp:Label>
                </td>
                <td class="tdgenap">
                    &nbsp;
                </td>
                <td class="tdganjil">
                    &nbsp;
                    <asp:Label ID="LblAssetUsageID" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Diasuransi Oleh
                </td>
                <td class="tdganjil">
                    <asp:Label ID="LblInsuredBy" runat="server">LblInsuredBy</asp:Label>
                </td>
                <td class="tdgenap">
                    Dibayar Oleh
                </td>
                <td class="tdganjil">
                    <asp:Label ID="LblPaidBy" runat="server">LblPaidBy</asp:Label>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Perusahaan Asuransi
                </td>
                <td class="tdganjil">
                    &nbsp;
                    <asp:DropDownList ID="cmbInsuranceComBranch" runat="server" Enabled="False">
                    </asp:DropDownList>
                    <asp:Label ID="LblFocusMaskAssID" runat="server"></asp:Label>
                    <asp:Label ID="LblFocusInsuranceComBranchName" runat="server"></asp:Label>
                </td>
                <td class="tdgenap">
                    Jangka Waktu Pembiayaan&nbsp;
                </td>
                <td class="tdganjil">
                    &nbsp;
                    <asp:TextBox ID="TxtTenorCredit" runat="server" Width="125px" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" Display="Dynamic"
                        ValidationExpression="\d*" ControlToValidate="TxtTenorCredit" ErrorMessage="Harap isi Jangka Waktu Pembiayaan harus angka" CssClass="validator_general"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="tdgenap">
                    Nilai Pertanggungan&nbsp;
                </td>
                <td class="tdganjil">
                    &nbsp;
                    <asp:TextBox ID="TxtAmountCoverage" runat="server" Width="115px" ></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                        ValidationExpression="\d*" ControlToValidate="TxtAmountCoverage" ErrorMessage="Harap isi Nilai pertanggungan dengan Angka " CssClass="validator_general"></asp:RegularExpressionValidator>
                </td>
                <td class="tdgenap">
                    Jenis Aplikasi
                </td>
                <td class="tdganjil">
                    <asp:Label ID="LblApplicationType" runat="server"></asp:Label>
                    <asp:Label ID="LblDefaultApplicationType" runat="server"></asp:Label>&nbsp;
                    <uc1:ucapplicationtype id="oApplicationType" runat="server"></uc1:ucapplicationtype>
                </td>
            </tr>
        </table>
        <table id="Table6" cellspacing="0" cellpadding="0" width="95%" border="0">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="imbOK" ImageUrl="../../../../Images/ButtonOk.gif" runat="server">
                    </asp:ImageButton>&nbsp;
                    <asp:Button Runat="server" ID="btnReset" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"/>
                </td>
            </tr>
        </table>
        <p>
        </p>
        <p>
            <asp:Label ID="LblMinimumTenor" runat="server" Font-Size="8pt" Visible="False" Font-Names="Verdana"></asp:Label>
            <asp:Label ID="LblMaximumTenor" runat="server" Font-Size="8pt" Visible="False" Font-Names="Verdana"></asp:Label>
            <asp:Label ID="LblBranchID" runat="server" Font-Size="8pt" Visible="False" Font-Names="Verdana"></asp:Label>
            <asp:Panel ID="PnlGrid" runat="server" Height="88px">
                <asp:Label ID="LblTenorCredit" runat="server" Font-Size="8pt" Visible="False" Font-Names="Verdana"></asp:Label>
                <asp:Label ID="LblJmlGrid" runat="server" Font-Size="8pt" Visible="False" Font-Names="Verdana"></asp:Label>
                <table cellspacing="0" cellpadding="0" width="95%" border="0">
                    <tr class="trtopi">
                        <td class="tdtopi" align="center">
                            EDIT DATA ASURANSI
                        </td>
                    </tr>
                </table>
                <asp:DataGrid ID="DgridInsurance" runat="server" Width="95%" CssClass="grid_general"
                    AutoGenerateColumns="False">
                    
                    <ItemStyle CssClass="item_grid"/>
                    <HeaderStyle CssClass="th"/>
                    <Columns>
                        <asp:TemplateColumn HeaderText="TAHUN">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="LblYearInsurance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearInsurance") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JENIS COVER">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenCoverageTypeDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CoverageType") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="DDLCoverageTypeDgrid" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PREMI">
                            <ItemTemplate>
                                <asp:Label ID="LblPremiumDGrid" Visible="true" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Premium", "{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TPL">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenTPLDgrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.TPL") %>'>
                                </asp:Label>
                                <asp:Label ID="LblTPLAmount" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmount","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="DDLTPLPilihan" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SRCC">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenSRCCDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SRCC") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="DDLBolSRCCPilihan" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblSRCCAmountDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCAmount","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="FLOOD">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenFloodDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Flood") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="DDLFloodPilihan" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblFloodAmountDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLOODAmount","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="RIOT">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenRiotDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Riot") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="DDLRiotPilihan" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblRiotAmountDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RiotAmount","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PA">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenPADGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.PA") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="DDLPAPilihan" runat="server">
                                </asp:DropDownList>
                                <asp:Label ID="LblPAAmountDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAAmount","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="LOADING FEE">
                            <ItemTemplate>
                                <asp:Label ID="LblLoadingFeeDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFee","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="(A) TOTAL (Standard rate)">
                            <HeaderStyle ></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="LblTotalDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="YEAR RATE">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblYearNumRate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNumRate") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <table id="Table7" cellspacing="1" cellpadding="1" width="95%" border="0">
                    <tr>
                        <td align="left">
                            <asp:ImageButton ID="Imagebutton2" ImageUrl="../../../../Images/ButtonPreview.gif"
                                runat="server" CausesValidation="False"/>
                        </td>
                    </tr>
                </table>
                <br/>
                <asp:Panel ID="PnlDGridPaid" runat="server" Height="140px">
                    <table cellspacing="0" cellpadding="0" width="95%" border="0">
                        <tr class="trtopi">
                            <td class="tdtopi" align="center">
                                EDIT BAYAR ASURANSI
                            </td>
                        </tr>
                    </table>
                    <asp:DataGrid ID="DgridInsurancePaid" runat="server" Width="95%" CssClass="grid_general"
                        AutoGenerateColumns="False">
                        
                        <ItemStyle CssClass="item_grid"/>
                        <HeaderStyle CssClass="th"/>
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearInsurance") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DIBAYAR CUST">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblHiddenPaidByCust" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.PaidByCust") %>'>
                                    </asp:Label>
                                    <asp:DropDownList ID="DDLPaidByCustDGrid" runat="server" Visible="True">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="TOTAL">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total","{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="(B) DISCOUNT">
                                <HeaderStyle ></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDiscountDtg" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.DiscToCust") %>'>
                                    </asp:TextBox>
                                    <asp:Label ID="lblDiscountValidator" runat="server" ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR CUST (Diisi asuransi yang dibayar customer /thn">
                                <HeaderStyle ></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPaidAmountByCustDtg" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.PaidAmtByCust") %>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <table id="Table7" cellspacing="1" cellpadding="1" width="95%" border="0">
                        <tr>
                            <td align="left">
                                <asp:ImageButton ID="Imagebutton1" ImageUrl="../../../../Images/ButtonCalculate.gif"
                                    runat="server" CausesValidation="False"/>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>
            <p>
            </p>
            <asp:Panel ID="PnlDGrid2Insurance" runat="server" Height="140px">
                <br/>
                <table ID="Table4" border="0" cellpadding="0" cellspacing="0" width="95%">
                    <tr class="trtopi">
                        <td align="center" class="tdtopi">
                            EDIT DATA ASURANSI
                        </td>
                    </tr>
                </table>
                <asp:DataGrid ID="Dgrid2Insurance" runat="server" AutoGenerateColumns="False" 
                    CssClass="grid_general" Width="95%">
                    <AlternatingItemStyle CssClass="tdgenap" />
                    <ItemStyle CssClass="tdganjil" />
                    <HeaderStyle CssClass="tdjudul" HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TAHUN">
                            <HeaderStyle Width="5%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="LblYearInsurance2" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JENIS COVER">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2Coverage" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.CoverageType") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PREMI">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2Premium" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SRCC" Visible="False">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="LblGridSRCC" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.BolSRCC") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH SRCC">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2SRCCAmount" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TPL" Visible="False">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2TPL" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH TPL">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2TPLAmount" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.TPLPremiumToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="FLOOD" Visible="False">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2Flood" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.BolFlood") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH FLOOD">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2FloodAmount" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.FLOODPremiumToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="RIOT" Visible="False">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2Riot" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.BolRiot") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH RIOT">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2RiotAmount" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.RiotPremiumToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PA" Visible="False">
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2PA" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.BolPA") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH PA">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2PAAmount" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.PAPremiumToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="LOADING FEE">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2LoadingFee" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFeeToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="(A) TOTAL (Standard rate)">
                            <HeaderStyle  />
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="LblGrid2TotalPremiumTOCustAmount" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.PremiumToCustAmount","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DIBAYAR CUST" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="LbGrid2lPaidByCust" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.PaidByCust") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <br/>
                <table ID="Table4" border="0" cellpadding="0" cellspacing="0" width="95%">
                    <tr class="trtopi">
                        <td align="center" class="tdtopi">
                            EDIT BAYAR ASURANSI
                        </td>
                    </tr>
                </table>
                <asp:DataGrid ID="Dgrid2InsurancePaid" runat="server" 
                    AutoGenerateColumns="False" CssClass="grid_general" Width="95%">
                    <AlternatingItemStyle CssClass="tdgenap" />
                    <ItemStyle CssClass="tdganjil" />
                    <HeaderStyle CssClass="tdjudul" HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TAHUN">
                            <HeaderStyle Width="5%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DIBAYAR CUST">
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.PaidByCust") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>                         
                        <asp:TemplateColumn HeaderText="(B) DISCOUNT">
                            <HeaderStyle Width="25%" />
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.DiscToCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TOTAL PREMI SETELAH DISCOUNT">
                            <HeaderStyle Width="25%" />
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" 
                                    Text='<%# Format(Container.DataItem("PremiumToCustAmount") - Container.DataItem("DiscToCust"), "###,###,##0.00") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR CUST (Diisi asuransi yang dibayar customer /thn">
                            <HeaderStyle Width="25%" />
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" 
                                    Text='<%# DataBinder.Eval(Container, "DataItem.PaidAmtByCust","{0:###,###,##0.00}") %>'>
                            </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <br>
                </br>
                <br>
                </br>
            </asp:Panel>
            <asp:Panel ID="PnlEntry" runat="server" Height="88px">
                <script language="javascript" type="text/javascript">

		<!--
                function DisplayNilaiTotalPremiumByCust() {
                    var totPremiumByCust;
                    totPremiumByCust = 0;
                    var numericDiscountPremium;
                    numericDiscountPremium = 0;
                    var numericPremiumToCust;
                    numericPremiumToCust = 0;
                    var TempPremiumByCust;
                    TempPremiumByCust = 0

                    numericDiscountPremium = parseFloat(document.Form1.TxtDiscountPremium.value);
                    TempPremiumByCust = parseFloat(document.all["LblPremiumToCust"].innerHTML) - parseFloat(numericDiscountPremium);
                    document.all["LblTotalPremiumByCust"].innerHTML = TempPremiumByCust;
                    DisplayNilayAmountCapitalized();
                    document.all["LblPremiumToCustFormat"].innerHTML = formatNumber((document.all["LblPremiumToCust"].innerHTML));
                    document.all["LbTotalPremiumByCustformat"].innerHTML = formatNumber(TempPremiumByCust);
                }

                function DisplayNilayAmountCapitalized() {
                    var PaidAmountByCust;
                    PaidAmountByCust = 0;
                    var TotPremiumByCust;
                    TotPremiumByCust = 0;
                    var AmountCapitalized;
                    AmountCapitalized = 0;
                    var AdditionalInsurancePremium;
                    AdditionalInsurancePremium = 0;

                    AdditionalInsurancePremium = parseFloat(document.Form1.TxtAdditionalInsurancePremium.value);
                    PaidAmountByCust = parseFloat(document.Form1.TxtPaidAmountByCustomer.value);
                    AmountCapitalized = (parseFloat(document.all["LblTotalPremiumByCust"].innerHTML) - parseFloat(PaidAmountByCust)) + parseFloat(AdditionalInsurancePremium);
                    document.all["LblAmountCapitalized"].innerHTML = AmountCapitalized;
                    document.all["LblAmountCapitalizedFormat"].innerHTML = formatNumber(AmountCapitalized);

                }

                function formatNumber(pValue) {
                    var lArrPre = pValue.toString().split('.')
                    var lStrValue = '';
                    var lBytLength = 0;

                    lBytLength = lArrPre[0].length - 3;
                    while (lBytLength > 0) {
                        lStrValue = ',' + lArrPre[0].substr(lBytLength, 3) + lStrValue;
                        lBytLength -= 3;
                    }
                    lBytLength += 3;
                    lStrValue = lArrPre[0].substr(0, lBytLength) + lStrValue;

                    if (lArrPre.length == 2) {
                        lStrValue += '.' + lArrPre[1];
                    } else {
                        lStrValue += '.00';
                    }
                    return lStrValue;
                }
	
			
// -->
            </script>
                <asp:Label ID="LblErrorMessages2" runat="server" ></asp:Label>
                <br/>
                <table ID="Table1" border="0" cellpadding="2" cellspacing="1" class="tablegrid" 
                    width="95%">
                    <tr>
                        <td class="tdgenap" width="25%">
                            Premi SRCC
                        </td>
                        <td class="tdganjil" width="25%">
                            <asp:Label ID="LblTotalSRCCPremium" runat="server"></asp:Label>
                        </td>
                        <td class="tdgenap" width="25%">
                            Premi TPL
                        </td>
                        <td class="tdganjil" width="25%">
                            <asp:Label ID="LblTotalTPLPremium" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Premi Flood
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="LblTotalFloodPremium" runat="server"></asp:Label>
                        </td>
                        <td class="tdgenap">
                            Loading Fee
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="LblTotalLoadingFee" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Premi Riot
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="LblTotalRiotPremium" runat="server"></asp:Label>
                        </td>
                        <td class="tdgenap">
                            Premi PA
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="LblTotalPAPremium" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Biaya Admin
                        </td>
                        <td class="tdganjil">
                            <asp:TextBox ID="txtInsAdminFee" runat="server"  
                                Enabled="False" Visible="False"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                ControlToValidate="txtInsAdminFee" Display="Dynamic" 
                                ErrorMessage="Harap isi Biaya Admin dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                            <asp:Label ID="LblInsAdminFeeBehaviour" runat="server" Visible="False"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                ControlToValidate="txtInsAdminFee" Display="Dynamic" 
                                ErrorMessage="Harap isi Biaya Admin" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </td>
                        <td class="tdgenap">
                            Biaya Materai
                        </td>
                        <td class="tdganjil">
                            <asp:TextBox ID="TxtInsStampDutyFee" runat="server"  
                                Enabled="False" Visible="False"></asp:TextBox>
                            <asp:Label ID="LblInsStampDutyBehavior" runat="server" Visible="False"></asp:Label>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                ControlToValidate="TxtInsStampDutyFee" Display="Dynamic" 
                                ErrorMessage="Harap isi Biaya Materai dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                ControlToValidate="TxtInsStampDutyFee" Display="Dynamic" 
                                ErrorMessage="Harap isi Biaya Materai" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Total Premi Standard
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="LblTotalStandardPremium" runat="server"></asp:Label>
                        </td>
                        <td class="tdgenap">
                            &nbsp;
                        </td>
                        <td class="tdganjil">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Premi Pada Customer
                        </td>
                        <td class="tdganjil">
                            <p>
                                <asp:Label ID="LblPremiumToCust" runat="server" ForeColor="white"></asp:Label>
                                <br/>
                                <asp:Label ID="LblPremiumToCustFormat" runat="server"></asp:Label>
                                <br>
                                </br>
                            </p>
                        </td>
                        <td class="tdgenap">
                            Discount Premi
                        </td>
                        <td class="tdganjil">
                            <input ID="TxtDiscountPremium" runat="server" class="inptype" maxlength="15" 
                                name="TxtDiscountPremium" onkeyup="DisplayNilaiTotalPremiumByCust()" size="17" 
                                style="width: 136px; height: 17px" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                                ControlToValidate="TxtDiscountPremium" Display="Dynamic" Enabled="False" 
                                ErrorMessage="Harap isi Discount dengan Angka" ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                ControlToValidate="TxtDiscountPremium" Display="Dynamic" 
                                ErrorMessage="Harap isi Discount Premi" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Total Premi Oleh Customer
                        </td>
                        <td class="tdganjil">
                            <p>
                                <asp:Label ID="LblTotalPremiumByCust" runat="server" ForeColor="white"></asp:Label>
                                <br/>
                                <asp:Label ID="LbTotalPremiumByCustformat" runat="server"></asp:Label>
                                <br>
                                </br>
                            </p>
                        </td>
                        <td class="tdgenap">
                            Tambahan Premi Asuransi
                        </td>
                        <td class="tdganjil">
                            <input ID="TxtAdditionalInsurancePremium" runat="server" class="inptype" 
                                disabled maxlength="15" name="TxtAdditionalInsurancePremium" 
                                onkeyup="DisplayNilayAmountCapitalized()" size="18" value="0" /> </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Total Dibayar oleh Customer
                        </td>
                        <td class="tdganjil">
                            <input ID="TxtPaidAmountByCustomer" runat="server" class="inptype" disabled 
                                maxlength="15" name="TxtPaidAmountByCustomer" 
                                onkeyup="DisplayNilayAmountCapitalized()" size="18" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                ControlToValidate="TxtPaidAmountByCustomer" Display="Dynamic" 
                                ErrorMessage="Harap isi Jumlah dibayar" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                ControlToValidate="TxtPaidAmountByCustomer" Display="Dynamic" 
                                ErrorMessage="Harap isi Jumlah dibayar Customer dengan Angka" 
                                ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                            </td>
                        <td class="tdgenap">
                            Jumlah dibiayakan/Capitalized
                        </td>
                        <td class="tdganjil">
                            <asp:Label ID="LblAmountCapitalized" runat="server" ForeColor="#ffffff"></asp:Label>
                            <br/>
                            <asp:Label ID="LblAmountCapitalizedFormat" runat="server"></asp:Label>
                            <br>
                            </br>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Premi Dasar untuk Refund Showroom
                        </td>
                        <td class="tdganjil">
                            <asp:TextBox ID="TxtPremiumBase" runat="server"  
                                MaxLength="15" Width="144px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                                ControlToValidate="TxtPremiumBase" Display="Dynamic" 
                                ErrorMessage="Harap isi Premi Dasar Refund Showroom" 
                                ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                        </td>
                        <td class="tdgenap">
                            Jumlah Admin dibiayakan/Capitalized
                        </td>
                        <td class="tdganjil">
                            <asp:TextBox ID="TxtAdditionalCap" runat="server"  
                                MaxLength="12"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" 
                                ControlToValidate="TxtAdditionalCap" Display="Dynamic" 
                                ErrorMessage="Harap isi Jumlah Admin Capitalized dengan angka" 
                                ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                            <asp:RangeValidator ID="rgvAdminAmountCapitalized" runat="server" 
                                ControlToValidate="TxtAdditionalCap" Display="Dynamic" 
                                ErrorMessage="range validator" CssClass="validator_general"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                        </td>
                        <td class="tdganjil" colspan="3">
                            <asp:TextBox ID="txtRfnSupplier" runat="server"  
                                MaxLength="15" Visible="False" Width="144px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="Regularexpressionvalidator10" 
                                runat="server" ControlToValidate="TxtPremiumBase" Display="Dynamic" 
                                ErrorMessage="Harap isi Refund Premi ke Supplier dengan Angka" 
                                ValidationExpression="\d*" CssClass="validator_general"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdgenap">
                            Catatan Accessories
                        </td>
                        <td class="tdganjil">
                            <asp:TextBox ID="TxtAccNotes" runat="server"  Height="80px" 
                                MaxLength="500" TextMode="MultiLine" Width="167px"></asp:TextBox>
                        </td>
                        <td class="tdgenap">
                            Catatan Asuransi
                        </td>
                        <td class="tdganjil">
                            <asp:TextBox ID="TxtInsNotes" runat="server"  Height="82px" 
                                MaxLength="500" TextMode="MultiLine" Width="160px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="95%">
                    <tr>
                        <td height="30" width="50%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="95%">
                    <tr>
                        <td align="left">
                            <asp:ImageButton ID="ImbSave" runat="server" 
                                ImageUrl="../../../../Images/Buttonsave.gif" />
                            &nbsp;
                            <asp:ImageButton ID="ImbCancelPalingBawah" runat="server" 
                                CausesValidation="False" ImageUrl="../../../../Images/Buttoncancel.gif" />
                        </td>
                    </tr>
                </table>
                <br>
                </br>
            </asp:Panel>
            <p>
            </p>
            <p>
            </p>
            <p>
            </p>
        </p>
    </asp:Panel>
    </form>
    <P></P>
</body>
</html>
