﻿#Region "Imports"
Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Webform
#End Region

Public Class ViewNewAppInsuranceByCompany
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

#End Region


#Region "Controls"
    Private m_controller As New ApplicationController
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTab
    Protected WithEvents UcInsuranceData As UcInsuranceData
    Protected WithEvents UcInsuranceDataAgri As UcInsuranceDataAgriculture

#End Region


#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        lblMessage.Text = ""
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("CustID")
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .CustomerID = Me.CustomerID
                .bindData()
                .initControls("OnNewApplication")
            End With
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Asuransi")
            ucApplicationTab1.setLink()
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            Dim oApplication As New Parameter.Application
            Dim oData As New DataTable
            Dim oRow As DataRow

            oApplication.strConnection = GetConnectionString()
            oApplication.AppID = Me.ApplicationID
            oApplication = m_controller.GetViewApplication(oApplication)
            If Not oApplication Is Nothing Then
                oData = oApplication.ListData
            End If
            If oData.Rows.Count > 0 Then
                oRow = oData.Rows(0)
                If oRow("ApplicationModule").ToString.Trim = "AGRI" Then
                    InsuranceDataAgri.Visible = True
                    InsuranceData.Visible = False
                    UcInsuranceDataAgri.ApplicationID = Me.ApplicationID
                    UcInsuranceDataAgri.BranchID = oRow("BranchID").ToString.Trim
                    UcInsuranceDataAgri.BindData()
                Else
                    InsuranceDataAgri.Visible = False
                    InsuranceData.Visible = True
                    UcInsuranceData.ApplicationID = Me.ApplicationID
                    UcInsuranceData.BranchID = oRow("BranchID").ToString.Trim
                    UcInsuranceData.BindData()
                End If
            Else
                ShowMessage(lblMessage, "Aplikasi tidak dapat ditemukan", True)
            End If
        End If
    End Sub
#End Region


End Class