﻿#Region "Imports"
Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Webform

#End Region

Public Class EditInsurance
    Inherits Maxiloan.Webform.WebBased


#Region "Property"
    Private Property DiscountMin() As Double
        Get
            Return (CType(viewstate("DiscountMin"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("DiscountMin") = Value
        End Set
    End Property

    Private Property DiscountMax() As Double
        Get
            Return (CType(viewstate("DiscountMax"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("DiscountMax") = Value
        End Set
    End Property

    Private Property RateCardID() As String
        Get
            Return (CType(viewstate("RateCardID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("RateCardID") = Value
        End Set
    End Property
    Private Property InsuranceComBranchID_lbl() As String
        Get
            Return (CType(viewstate("InsuranceComBranchID_lbl"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceComBranchID_lbl") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return (CType(viewstate("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property SupplierGroupID() As String
        Get
            Return (CType(viewstate("SupplierGroupID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierGroupID") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return (CType(viewstate("CustomerName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Private Property PageSource() As String
        Get
            Return (CType(viewstate("PageSource"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PageSource") = Value
        End Set
    End Property

    Private Property AdminFeeBehavior() As String
        Get
            Return (CType(viewstate("AdminFeeBehavior"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AdminFeeBehavior") = Value
        End Set
    End Property

    Private Property NilaiAdminFeeAwal() As Double
        Get
            Return (CType(viewstate("NilaiAdminFeeAwal"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("NilaiAdminFeeAwal") = Value
        End Set
    End Property


    Private Property StampDutyFeeBehavior() As String
        Get
            Return (CType(viewstate("StampDutyFeeBehavior"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StampDutyFeeBehavior") = Value
        End Set
    End Property

    Private Property NilaiStampDutyFeeAwal() As Double
        Get
            Return (CType(viewstate("NilaiStampDutyFeeAwal"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("NilaiStampDutyFeeAwal") = Value
        End Set
    End Property
    Private Property MaximumTenor() As String
        Get
            Return (CType(viewstate("MaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("MaximumTenor") = Value
        End Set
    End Property
    Private Property MinimumTenor() As String
        Get
            Return (CType(viewstate("MinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("MinimumTenor") = Value
        End Set
    End Property
    Private Property PMaximumTenor() As String
        Get
            Return (CType(viewstate("PMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PMaximumTenor") = Value
        End Set
    End Property
    Private Property PMinimumTenor() As String
        Get
            Return (CType(viewstate("PMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PMinimumTenor") = Value
        End Set
    End Property
    Private Property PBMinimumTenor() As String
        Get
            Return (CType(viewstate("PBMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PBMinimumTenor") = Value
        End Set
    End Property
    Private Property PBMaximumTenor() As String
        Get
            Return (CType(viewstate("PBMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PBMaximumTenor") = Value
        End Set
    End Property
    Private Property ModuleID() As String
        Get
            Return (CType(viewstate("ModuleID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ModuleID") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property ApplicationTypeID() As String
        Get
            Return CType(viewstate("ApplicationTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationTypeID") = Value
        End Set
    End Property

    Private Property CoverageTypeMs() As DataTable
        Get
            Return CType(viewstate("CoverageTypeMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("CoverageTypeMs") = Value
        End Set
    End Property

    Private Property PaidByCustMs() As DataTable
        Get
            Return CType(viewstate("PaidByCustMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("PaidByCustMs") = Value
        End Set
    End Property

    Private Property SRCCMs() As DataTable
        Get
            Return CType(viewstate("SRCCMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("SRCCMs") = Value
        End Set
    End Property

    Private Property TPLMs() As DataTable
        Get
            Return CType(viewstate("TPLMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("TPLMs") = Value
        End Set
    End Property


    Private Property FloodMs() As DataTable
        Get
            Return CType(viewstate("FloodMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("FloodMs") = Value
        End Set
    End Property


    Private Property InsuranceComBranchID() As String
        Get
            Return CType(viewstate("InsuranceComBranchID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceComBranchID") = Value
        End Set
    End Property

    Private Property InsuranceType() As String
        Get
            Return CType(viewstate("InsuranceType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceType") = Value
        End Set
    End Property

    Private Property AssetUsageID() As String
        Get
            Return CType(viewstate("AssetUsageID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetUsageID") = Value
        End Set
    End Property

    Private Property AssetNewUsed() As String
        Get
            Return CType(viewstate("AssetNewUsed"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetNewUsed") = Value
        End Set
    End Property

    Private Property ManufacturingYear() As String
        Get
            Return (CType(viewstate("ManufacturingYear"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ManufacturingYear") = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return (CType(viewstate("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InterestType") = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return (CType(viewstate("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InstallmentScheme") = Value
        End Set
    End Property

    Public Property AmountCoverage() As Double
        Get
            Return (CType(viewstate("AmountCoverage"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("AmountCoverage") = Value
        End Set
    End Property


    Private Property PremiumAmountByCustBeforeDisc() As Double
        Get
            Return CType(viewstate("PremiumAmountByCustBeforeDisc"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("PremiumAmountByCustBeforeDisc") = Value
        End Set
    End Property
    Private Property InsLength() As Int16
        Get
            Return CType(viewstate("InsLength"), Int16)
        End Get
        Set(ByVal Value As Int16)
            viewstate("InsLength") = Value
        End Set
    End Property
    Private Property Status() As Boolean
        Get
            Return CType(viewstate("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("Status") = Value
        End Set
    End Property
    Private Property RefundToSupplier() As Double
        Get
            Return CDbl(viewstate("RefundToSupplier"))
        End Get
        Set(ByVal Value As Double)
            viewstate("RefundToSupplier") = Value
        End Set
    End Property
    Private Property IsPreview() As String
        Get
            Return (CType(viewstate("IsPreview"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("IsPreview") = Value
        End Set
    End Property
    Private Property RiotMs() As DataTable
        Get
            Return CType(viewstate("RiotMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("RiotMs") = Value
        End Set
    End Property
    Private Property PAMs() As DataTable
        Get
            Return CType(viewstate("PAMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("PAMs") = Value
        End Set
    End Property

#End Region
#Region "Constanta"

    Private CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE As String = CommonCacheHelper.CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE
    'Private oController As New cNewAppInsuranceByCompanyController
    'Private oControllerPremium As New cInsuranceApplicationController
    'Private oInsAppController As New cInsuranceApplicationController
    'Private oCustomClassResult As New Parameter.eInsuranceCalculationResult
    'Private oInsCalResultController As New cInsuranceCalculationResultController
    Private oCustomClass As New Parameter.NewAppInsuranceByCompany
    Private oController As New NewAppInsuranceByCompanyController
    Private oControllerPremium As New InsuranceApplicationController
    'Dim m_controller As New DataUserControlController
    Private oInsAppController As New InsuranceApplicationController
    Private oCustomClassResult As New Parameter.InsuranceCalculationResult
    Private oInsCalResultController As New InsuranceCalculationResultController
    Private Const COMPANY_CUSTOMER As String = "CompanyCustomer"
    Protected WithEvents oApplicationType As UcApplicationType
    Private Const CACHE_APPLICATION_TYPE As String = "CacheApplicationType"
    '  Dim InterestType As String
    ' Dim InstallmentScheme As String

#End Region
#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

#End Region
#Region "Default Panel"

    Sub InitialDefatultPanel()
        PnlGrid.Visible = False
        PnlEntry.Visible = False
        LblApplicationType.Visible = False
        PnlDGrid2Insurance.Visible = False
        imbOK.Visible = True
        cmbInsuranceComBranch.Enabled = False
    End Sub
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If


        'If Not CheckForm(Me.Loginid, CommonVariableHelper.FORM_NAME_NEW_APPLICATION_BY_COMPANY, CommonVariableHelper.APPLICATION_NAME) Then
        '    Exit Sub
        'End If

        If Not IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then

                InitialDefatultPanel()
                Me.ApplicationID = Replace(Request.QueryString("ApplicationID"), "'", "")
                Me.PageSource = Replace(Request.QueryString("PageSource"), "'", "")
                Me.SupplierGroupID = Replace(Request.QueryString("SupplierGroupID"), "'", "")
                Context.Trace.Write(" Me.ApplicationID " & Me.ApplicationID)
                Context.Trace.Write(" Me.PageSource " & Me.PageSource)
                Context.Trace.Write(" Me.SupplierGroupID " & Me.SupplierGroupID)
                HplinkApplicationID.Text = Me.ApplicationID
                HplinkApplicationID.NavigateUrl = LinkTo(Me.ApplicationID, "Insurance")
                ' ---- value = 'CompanyCustomer'
                ' ----- value = 'CompanyAtCost'
                oApplicationType.SupplierGroupID = Me.SupplierGroupID
                Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany
                With oNewAppInsuranceByCompany
                    .ApplicationID = Me.ApplicationID.Trim
                    .strConnection = GetConnectionString()
                    .SpName = "spInsEDITCompanyPaidByCustomerSelect"
                End With
                Try
                    oNewAppInsuranceByCompany = oController.EditGetInsuranceEntryStep1List(oNewAppInsuranceByCompany)
                Catch ex As Exception
                    LblErrorMessages.Text = MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND
                    Dim err As New MaxiloanExceptions
                    err.WriteLog("", "", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                End Try

                With oNewAppInsuranceByCompany
                    lblCustomerName.Text = .CustomerName
                    HpLinkCustName.Text = .CustomerName
                    Me.CustomerName = lblCustomerName.Text
                    Me.CustomerID = .CustomerID
                    HpLinkCustName.NavigateUrl = LinkToCustomer(Me.CustomerID, "Insurance")
                    LblAssetNewused.Text = .AssetUsageNewUsed
                    Me.AssetNewUsed = LblAssetNewused.Text
                    LblAssetDescr.Text = .AssetMasterDescr
                    lblInsuranceAssetType.Text = .InsuranceAssetDescr
                    LblInsuredBy.Text = .InsAssetInsuredByName
                    LblPaidBy.Text = .InsAssetPaidByName
                    LblApplicationID.Text = .ApplicationID
                    Me.PBMinimumTenor = CType(.PBMinimumTenor, String)
                    Me.PBMaximumTenor = CType(.PBMaximumTenor, String)
                    Me.PMinimumTenor = CType(.PMinimumTenor, String)
                    Me.PMaximumTenor = CType(.PMaximumTenor, String)
                    Me.RefundToSupplier = CType(.RefundToSupplier, Double)
                    txtRfnSupplier.Text = CStr(.RefundToSupplier)
                    Try
                        LblAssetUsageID.Text = .AssetUsageID.Trim
                    Catch ex As Exception
                        LblErrorMessages.Text = MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND & MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND_ASSETUSAGE & " for this applicationID = " & Me.ApplicationID
                        imbOK.Visible = False
                        Dim err As New MaxiloanExceptions
                        err.WriteLog("", "", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                        Exit Sub
                    End Try

                    Me.AssetUsageID = LblAssetUsageID.Text.Trim
                    LblAssetUsageDescr.Text = .AssetUsageDescr
                    TxtTenorCredit.Text = CType(.Tenor, String)
                    TxtAmountCoverage.Text = CType(.TotalOTR, String)
                    Me.AmountCoverage = CType(TxtAmountCoverage.Text, Double)
                    LblMinimumTenor.Text = CType(.MinimumTenor, String)
                    LblMaximumTenor.Text = CType(.MaximumTenor, String)
                    LblBranchID.Text = .BranchId
                    Me.MinimumTenor = LblMinimumTenor.Text
                    Me.MaximumTenor = LblMaximumTenor.Text
                    Me.BranchID = .BranchId
                    txtInsAdminFee.Text = CType(.InsAdminFee, String)
                    TxtInsStampDutyFee.Text = CType(.InsStampDutyFee, String)
                    LblInsAdminFeeBehaviour.Text = .InsAdminFeeBehaviour
                    LblInsStampDutyBehavior.Text = .InsStampDutyFeeBehaviour

                    If LblInsAdminFeeBehaviour.Text.Trim.ToUpper = "L" Then
                        txtInsAdminFee.Enabled = False
                    Else
                        txtInsAdminFee.Enabled = False
                    End If
                    If LblInsStampDutyBehavior.Text.Trim.ToUpper = "L" Then
                        TxtInsStampDutyFee.Enabled = False
                    Else
                        TxtInsStampDutyFee.Enabled = False
                    End If

                    Me.AdminFeeBehavior = LblInsAdminFeeBehaviour.Text.Trim.ToUpper
                    Me.StampDutyFeeBehavior = LblInsStampDutyBehavior.Text.Trim.ToUpper
                    Me.NilaiAdminFeeAwal = .InsAdminFee
                    Me.NilaiStampDutyFeeAwal = .InsStampDutyFee

                    LblInsuranceType.Text = .InsuranceType
                    Me.InsuranceType = LblInsuranceType.Text
                    LblManufacturingYear.Text = .ManufacturingYear
                    Me.ManufacturingYear = LblManufacturingYear.Text
                    LblDefaultApplicationType.Text = .ApplicationTypeDescr
                    Me.RateCardID = .SesionID
                End With

                CheckProspect()
            End If
        End If
    End Sub
#End Region
#Region "Check Prospect "

    Public Function CheckProspect() As Boolean
        Try

            Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany
            With oNewAppInsuranceByCompany
                .ApplicationID = Me.ApplicationID.Trim
                .strConnection = GetConnectionString
                .SpName = "spInsEDITCheckProspectSelect"
            End With

            oNewAppInsuranceByCompany = oController.CheckProspect(oNewAppInsuranceByCompany)

            If oNewAppInsuranceByCompany.MaskAssID <> "" And oNewAppInsuranceByCompany.InsuranceComBranchID <> "" Then
                'Bila prospectnya ada
                cmbInsuranceComBranch.Visible = False
                LblFocusInsuranceComBranchName.Text = oNewAppInsuranceByCompany.InsuranceComBranchName
                LblFocusInsuranceComBranchName.Visible = True
                Me.InsuranceComBranchID = LblFocusMaskAssID.Text
            Else
                'Bila prospectnya tidak ada
                cmbInsuranceComBranch.Visible = True
                LblFocusInsuranceComBranchName.Visible = False
                FillInsuranceComBranch()
                Me.InsuranceComBranchID = cmbInsuranceComBranch.SelectedValue.Trim

            End If

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("", "", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try


    End Function
#End Region
#Region "FillInsuranceComBranch"



    Public Sub FillInsuranceComBranch()

        Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany
        Dim oData As New DataTable
        oNewAppInsuranceByCompany.strConnection = GetConnectionString
        oNewAppInsuranceByCompany.BranchId = Me.BranchID

        Dim DtFillInsurance As New DataTable
        DtFillInsurance = CType(Me.Cache.Item(CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE & Me.BranchID), DataTable)

        If DtFillInsurance Is Nothing Then
            Dim DtFillInsuranceCache As New DataTable
            DtFillInsuranceCache = oController.FillInsuranceComBranch(oNewAppInsuranceByCompany)
            Me.Cache.Insert(CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE & Me.BranchID, DtFillInsuranceCache, Nothing, DateTime.Now.AddHours(CommonCacheHelper.DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtFillInsurance = CType(Me.Cache.Item(CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE & Me.BranchID), DataTable)
        End If

        If Not DtFillInsurance Is Nothing Then
            'oData = oNewAppInsuranceByCompany.ListData
            oData = DtFillInsurance
            cmbInsuranceComBranch.DataSource = oData.DefaultView
            cmbInsuranceComBranch.DataValueField = "ID"
            cmbInsuranceComBranch.DataTextField = "Name"
            cmbInsuranceComBranch.DataBind()
            cmbInsuranceComBranch.Items.Insert(0, "Select One")
            cmbInsuranceComBranch.Items(0).Value = "0"

        End If

    End Sub

#End Region
#Region "Reset"


    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("../../../../Webform.LoanOrg/Credit/CreditProcess/ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
#End Region
#Region "Check Validasi Step 1 "
    Function CheckValidasiStep1() As Boolean
        Dim IntMinimumTenor As Int16
        Dim IntMaximumTenor As Int16
        Dim TenorEntryByUser As Int16
        IntMinimumTenor = CType(LblMinimumTenor.Text, Int16)
        IntMaximumTenor = CType(LblMaximumTenor.Text, Int16)
        TenorEntryByUser = CType(TxtTenorCredit.Text, Int16)

        If TenorEntryByUser < IntMinimumTenor Or TenorEntryByUser > IntMaximumTenor Then
            LblErrorMessages.Text = "Jangka waktu Kredit harus diantara  " & LblMinimumTenor.Text & "  dan " & LblMaximumTenor.Text & " (dari Produk Jual) "
            Me.Status = False
            Exit Function
        End If

        If TenorEntryByUser < CType(Me.PBMinimumTenor, Int16) Or TenorEntryByUser > CType(Me.PBMaximumTenor, Int16) Then
            LblErrorMessages.Text = "Jangka waktu Kredit harus diantara " & Me.PBMinimumTenor & "  dan " & Me.PBMaximumTenor & " (dari Produk Cabang) "
            Me.Status = False
            Exit Function
        End If

        If TenorEntryByUser < CType(Me.PMinimumTenor, Int16) Or TenorEntryByUser > CType(Me.PMaximumTenor, Int16) Then
            LblErrorMessages.Text = "Jangka waktu Kredit harus diantara " & Me.PMinimumTenor & "  dan " & Me.PMaximumTenor & " (dari Produk) "
            Me.Status = False
            Exit Function
        End If
    End Function
#End Region
#Region "Ketika Tombol OK di Klik "
    Private Sub imbOK_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbOK.Click
        Dim CheckStep1 As Boolean
        Me.Status = True
        'CheckStep1 = CheckValidasiStep1()
        'If CheckStep1 = False Then
        '    Exit Sub
        'End If


        'CheckValidasiStep1()
        'If Me.Status = False Then
        '    Exit Sub
        'End If
        PnlDGridPaid.Visible = False

        'Tombol Save Dibawah dihilangin dulu
        LblDefaultApplicationType.Visible = False
        'ImbSave.Visible = False di non aktifkan sejak selasa 6 apr 2004
        Me.IsPreview = "0"
        Me.InsuranceComBranchID_lbl = cmbInsuranceComBranch.SelectedValue.Trim

        LblErrorMessages.Text = ""
        Me.ApplicationTypeID = oApplicationType.AppTypeID.Trim
        LblApplicationType.Text = oApplicationType.AppTypeName.Trim
        LblApplicationType.Visible = True
        PnlGrid.Visible = True
        PnlEntry.Visible = True
        imbOK.Visible = False
        btnReset.Visible = False
        TxtTenorCredit.Text = CStr(IIf(TxtTenorCredit.Text = "", 0, TxtTenorCredit.Text))
        LblTenorCredit.Text = TxtTenorCredit.Text
        Dim jmltenor As Int16 = CType(LblTenorCredit.Text, Int16)
        Me.AmountCoverage = CType(TxtAmountCoverage.Text, Double)
        TxtAmountCoverage.Enabled = False
        TxtTenorCredit.Enabled = False
        cmbInsuranceComBranch.Enabled = False
        oApplicationType.Visible = False

        Dim oSetJmlGrid As Int16
        Dim oSimpleRule As New CommonSimpleRuleHelper
        oSetJmlGrid = oSimpleRule.SetJumlahGrid(jmltenor)
        Me.InsLength = CType(TxtTenorCredit.Text, Int16)
        Context.Trace.Write("Tenor = " + TxtTenorCredit.Text)

        Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany
        With oNewAppInsuranceByCompany
            .JmlGrid = oSetJmlGrid
            .BranchId = Me.BranchID.Trim
            .ApplicationID = Me.ApplicationID
            .SpName = "spPagingInsEDITNewAppInsByCompanyGridTenor"
            .strConnection = GetConnectionString
        End With

        oNewAppInsuranceByCompany = oController.EditGetInsuranceEntryStep2List(oNewAppInsuranceByCompany)
        Dim dtEntity As New DataTable

        If Not oNewAppInsuranceByCompany Is Nothing Then
            dtEntity = oNewAppInsuranceByCompany.ListData
        End If

        Me.DiscountMin = CDbl(dtEntity.Rows(0).Item("DiscountMin"))
        Me.DiscountMax = CDbl(dtEntity.Rows(0).Item("DiscountMax"))

        DgridInsurance.DataSource = dtEntity
        DgridInsurance.DataBind()
        DgridInsurancePaid.DataSource = dtEntity
        DgridInsurancePaid.DataBind()
        '=============== 
        DisplayResult1()
        ImbSave.Visible = False




    End Sub

#End Region
#Region "Item DataBound DgridInsurance "

    Private Sub DgridInsurance_DataBinding(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DgridInsurance.ItemDataBound

        Dim CboCoverageTypeDT As New DataTable("CoverageTypeMs")
        Dim DDLCoverageTypePilihan As DropDownList
        Dim CboBooleanSRCCDT As New DataTable("BolSRCC")
        Dim DDLBooleanSRCCPilihan As DropDownList
        Dim CboTPLPDT As New DataTable("TPLMs")
        Dim DDLTPLPilihan As New DropDownList
        Dim CboBooleanFloodDT As New DataTable("BolFlood")
        Dim DDLBooleanFloodPilihan As DropDownList
        Dim CboPaidByCustDT As New DataTable("PaidByCustMs")
        Dim DDLPaidByCustPilihan As DropDownList
        Dim CboBooleanRiotDT As New DataTable("BolRiot")
        Dim DDLBooleanRiotPilihan As DropDownList
        Dim CboBooleanPADT As New DataTable("BolPA")
        Dim DDLBooleanPAPilihan As DropDownList

        Dim LblHiddenCoverageTypeDGrid As Label
        Dim LblHiddenSRCC As Label
        Dim LblHiddenTPL As Label
        Dim LblHiddenFlood As Label
        Dim LblHiddenRiot As Label
        Dim LblHiddenPA As Label
        Dim LblHiddenPaidByCust As Label

        Dim LblPremiumDGrid As Label
        Dim LblTPLAmount As Label
        Dim LblSRCCAmountDGrid As Label
        Dim LblFloodAmountDGrid As Label
        Dim LblRiotAmountDGrid As Label
        Dim LblPAAmountDGrid As Label

        If e.Item.ItemIndex > -1 Then
            '============= Handle CoverageType =============
            DDLCoverageTypePilihan = CType(e.Item.FindControl("DDLCoverageTypeDgrid"), DropDownList)
            LblHiddenCoverageTypeDGrid = CType(e.Item.FindControl("LblHiddenCoverageTypeDGrid"), Label)
            CboCoverageTypeDT = Me.CoverageTypeMs
            Dim cbodvCoverageType As DataView
            cbodvCoverageType = CboCoverageTypeDT.DefaultView
            DDLCoverageTypePilihan.DataSource = cbodvCoverageType
            DDLCoverageTypePilihan.DataTextField = "Description"
            DDLCoverageTypePilihan.DataValueField = "ID"
            DDLCoverageTypePilihan.DataBind()
            DDLCoverageTypePilihan.ClearSelection()
            DDLCoverageTypePilihan.Items.FindByValue(Trim(LblHiddenCoverageTypeDGrid.Text)).Selected = True
            LblPremiumDGrid = CType(e.Item.FindControl("LblPremiumDGrid"), Label)

            '============= Handle SRCC =============

            DDLBooleanSRCCPilihan = CType(e.Item.FindControl("DDLBolSRCCPilihan"), DropDownList)
            LblHiddenSRCC = CType(e.Item.FindControl("LblHiddenSRCCDGrid"), Label)
            CboBooleanSRCCDT = Me.SRCCMs
            Dim CbodvSRCC As DataView
            CbodvSRCC = CboBooleanSRCCDT.DefaultView
            DDLBooleanSRCCPilihan.DataSource = CbodvSRCC
            DDLBooleanSRCCPilihan.DataTextField = "Description"
            DDLBooleanSRCCPilihan.DataValueField = "ID"
            DDLBooleanSRCCPilihan.DataBind()
            DDLBooleanSRCCPilihan.ClearSelection()
            Dim strBol As Int16
            If LblHiddenSRCC.Text.ToUpper.Trim = "TRUE" Then
                strBol = 1
            Else
                strBol = 0
            End If
            DDLBooleanSRCCPilihan.Items.FindByValue(Trim(strBol.ToString)).Selected = True
            LblSRCCAmountDGrid = CType(e.Item.FindControl("LblSRCCAmountDGrid"), Label)

            '================= Handle TPL =======================

            DDLTPLPilihan = CType(e.Item.FindControl("DDLTPLPilihan"), DropDownList)
            LblHiddenTPL = CType(e.Item.FindControl("LblHiddenTPLDgrid"), Label)
            CboCoverageTypeDT = Me.TPLMs
            Dim CbodvTPL As DataView
            CbodvTPL = CboCoverageTypeDT.DefaultView
            DDLTPLPilihan.DataSource = CbodvTPL
            DDLTPLPilihan.DataTextField = "TPLAmount"
            DDLTPLPilihan.DataValueField = "TPLAmount"
            DDLTPLPilihan.DataBind()
            DDLTPLPilihan.ClearSelection()
            'ori
            'DDLTPLPilihan.Items.FindByValue(Trim(LblHiddenTPL.Text.ToString)).Selected = True

            'rmcs:start
            If LblHiddenTPL.Text <> "0" Then
                DDLTPLPilihan.Items.FindByValue(Trim(LblHiddenTPL.Text.ToString)).Selected = True
                DDLTPLPilihan.SelectedValue = LblHiddenTPL.Text
            End If
            'rmcs:end


            LblTPLAmount = CType(e.Item.FindControl("LblTPLAmount"), Label)

            '============= Handle FLOOD =============
            DDLBooleanFloodPilihan = CType(e.Item.FindControl("DDLFloodPilihan"), DropDownList)
            LblHiddenFlood = CType(e.Item.FindControl("LblHiddenFloodDGrid"), Label)
            CboBooleanFloodDT = Me.FloodMs
            Dim CboDvFlood As DataView
            CboDvFlood = CboBooleanFloodDT.DefaultView
            DDLBooleanFloodPilihan.DataSource = CboDvFlood
            DDLBooleanFloodPilihan.DataTextField = "Description"
            DDLBooleanFloodPilihan.DataValueField = "ID"
            DDLBooleanFloodPilihan.DataBind()
            DDLBooleanFloodPilihan.ClearSelection()
            Dim strBol2 As Int16
            If LblHiddenFlood.Text.ToUpper.Trim = "TRUE" Then
                strBol2 = 1
            Else
                strBol2 = 0
            End If
            DDLBooleanFloodPilihan.Items.FindByValue(Trim(strBol2.ToString)).Selected = True
            LblFloodAmountDGrid = CType(e.Item.FindControl("LblFloodAmountDGrid"), Label)

            '============= Handle RIOT =============
            DDLBooleanRiotPilihan = CType(e.Item.FindControl("DDLRiotPilihan"), DropDownList)
            LblHiddenRiot = CType(e.Item.FindControl("LblHiddenRiotDGrid"), Label)
            CboBooleanRiotDT = Me.RiotMs
            Dim CboDvRiot As DataView
            CboDvRiot = CboBooleanRiotDT.DefaultView
            DDLBooleanRiotPilihan.DataSource = CboDvRiot
            DDLBooleanRiotPilihan.DataTextField = "Description"
            DDLBooleanRiotPilihan.DataValueField = "ID"
            DDLBooleanRiotPilihan.DataBind()
            DDLBooleanRiotPilihan.ClearSelection()
            Dim strBol3 As Int16
            If LblHiddenRiot.Text.ToUpper.Trim = "TRUE" Then
                strBol3 = 1
            Else
                strBol3 = 0
            End If
            DDLBooleanRiotPilihan.Items.FindByValue(Trim(strBol3.ToString)).Selected = True
            LblRiotAmountDGrid = CType(e.Item.FindControl("LblRiotAmountDGrid"), Label)

            '============= Handle PA =============
            DDLBooleanPAPilihan = CType(e.Item.FindControl("DDLPAPilihan"), DropDownList)
            LblHiddenPA = CType(e.Item.FindControl("LblHiddenPADGrid"), Label)
            CboBooleanPADT = Me.PAMs
            Dim CboDvPA As DataView
            CboDvPA = CboBooleanPADT.DefaultView
            DDLBooleanPAPilihan.DataSource = CboDvPA
            DDLBooleanPAPilihan.DataTextField = "Description"
            DDLBooleanPAPilihan.DataValueField = "ID"
            DDLBooleanPAPilihan.DataBind()
            DDLBooleanPAPilihan.ClearSelection()
            Dim strBol4 As Int16
            If LblHiddenPA.Text.ToUpper.Trim = "TRUE" Then
                strBol4 = 1
            Else
                strBol4 = 0
            End If
            DDLBooleanPAPilihan.Items.FindByValue(Trim(strBol4.ToString)).Selected = True
            LblPAAmountDGrid = CType(e.Item.FindControl("LblPAAmountDGrid"), Label)


            '============= Handle PaidByCust =============
            'DDLPaidByCustPilihan = CType(e.Item.Cells(9).FindControl("DDLPaidByCustDGrid"), DropDownList)
            'LblHiddenPaidByCust = CType(e.Item.FindControl("LblHiddenPaidByCust"), Label)
            'CboPaidByCustDT = Me.PaidByCustMs
            'LblHiddenPaidByCust.Visible = False


            'Dim CbodvPaidByCust As DataView
            'CbodvPaidByCust = CboPaidByCustDT.DefaultView
            'DDLPaidByCustPilihan.DataSource = CbodvPaidByCust
            'DDLPaidByCustPilihan.DataTextField = "Description"
            'DDLPaidByCustPilihan.DataValueField = "ID"
            'DDLPaidByCustPilihan.DataBind()
            'DDLPaidByCustPilihan.ClearSelection()

            'Context.Trace.Write("Nilai PaidByCust dari data= '" & LblHiddenPaidByCust.Text.Trim & "'")
            ''DDLPaidByCustPilihan.Items.FindByValue("PAID      ").Selected = True
            'DDLPaidByCustPilihan.Items.FindByValue(LblHiddenPaidByCust.Text).Selected = True
            If Me.IsPreview = "1" Then
                DDLCoverageTypePilihan.Enabled = False
                DDLBooleanSRCCPilihan.Visible = False
                LblSRCCAmountDGrid.Visible = True
                DDLTPLPilihan.Visible = False
                LblTPLAmount.Visible = True
                DDLBooleanFloodPilihan.Visible = False
                LblFloodAmountDGrid.Visible = True
                DDLBooleanRiotPilihan.Visible = False
                LblRiotAmountDGrid.Visible = True
                DDLBooleanPAPilihan.Visible = False
                LblPAAmountDGrid.Visible = True
            Else
                DDLCoverageTypePilihan.Enabled = True
                DDLBooleanSRCCPilihan.Visible = True
                LblSRCCAmountDGrid.Visible = False
                DDLTPLPilihan.Visible = True
                LblTPLAmount.Visible = False
                DDLBooleanFloodPilihan.Visible = True
                LblFloodAmountDGrid.Visible = False
                DDLBooleanRiotPilihan.Visible = True
                LblRiotAmountDGrid.Visible = False
                DDLBooleanPAPilihan.Visible = True
                LblPAAmountDGrid.Visible = False
            End If

        Else
            '============= Handle CoverageType =============

            Dim strsqlQuery As String
            strsqlQuery = CommonVariableHelper.SQL_QUERY_COVERAGE_TYPEMS
            Dim objAdapter As New SqlDataAdapter(strsqlQuery, GetConnectionString)
            objAdapter.Fill(CboCoverageTypeDT)
            Me.CoverageTypeMs = CboCoverageTypeDT

            '============= Handle SRCC =============
            Dim strsqlQuerySRCC As String
            Dim SBuilder As New StringBuilder
            SBuilder.Append("create table #TempTable (")
            SBuilder.Append("ID  int ,")
            SBuilder.Append("Description char(8) ) ")
            SBuilder.Append("EXEC ('")
            SBuilder.Append("INSERT INTO #TEMPTABLE Select ''1'' as ID ,''Yes'' as Description ")
            SBuilder.Append("INSERT INTO #TEMPTABLE Select ''0'' as ID ,''No'' as Description ")
            SBuilder.Append("Select ID,Description From #TempTable ')")
            strsqlQuerySRCC = SBuilder.ToString
            Dim objAdapterSRCC As New SqlDataAdapter(strsqlQuerySRCC, GetConnectionString)
            objAdapterSRCC.Fill(CboBooleanSRCCDT)
            Me.SRCCMs = CboBooleanSRCCDT


            '============= Handle TPL =============
            Dim strQueryTPL As String
            'ori
            'strQueryTPL = CommonVariableHelper.SQL_QUERY_TPL & " Where RateCardID = '" & Me.RateCardID & "'"

            'rmcs:start
            strQueryTPL = "if exists(select TPLAmount, TPLPremium from newinsurancetpltocust Where RateCardID = '" & Me.RateCardID.Trim & "') " & _
            "select TPLAmount, TPLPremium from newinsurancetpltocust Where RateCardID = '" & Me.RateCardID.Trim & "' else select 0 as TPLAmount, 0 as TPLPremium"
            'rmcs:end

            Dim ObjAdapterTPL As New SqlDataAdapter(strQueryTPL, GetConnectionString)
            ObjAdapterTPL.Fill(CboTPLPDT)
            Me.TPLMs = CboTPLPDT

            '============= Handle Flood =============
            Dim strsqlQueryFlood As String
            Dim SBuilder2 As New StringBuilder
            SBuilder2.Append("create table #TempTable (")
            SBuilder2.Append("ID  int ,")
            SBuilder2.Append("Description char(8) ) ")
            SBuilder2.Append("EXEC ('")
            SBuilder2.Append("INSERT INTO #TEMPTABLE Select ''1'' as ID ,''Yes'' as Description ")
            SBuilder2.Append("INSERT INTO #TEMPTABLE Select ''0'' as ID ,''No'' as Description ")
            SBuilder2.Append("Select ID,Description From #TempTable ')")
            strsqlQueryFlood = SBuilder2.ToString
            Dim objAdapterFlood As New SqlDataAdapter(strsqlQueryFlood, GetConnectionString)
            objAdapterFlood.Fill(CboBooleanFloodDT)
            Me.FloodMs = CboBooleanFloodDT


            '============= Handle PaidByCust =============

            'Dim strsqlQuery2 As String
            'If Me.PageSource = COMPANY_CUSTOMER Then
            '    strsqlQuery2 = CommonVariableHelper.SQL_QUERY_PAIDBYCUST_MS
            'Else
            '    strsqlQuery2 = CommonVariableHelper.SQL_QUERY_PAIDBYCUST_MS_PAGESOURCE_COMPANY_AT_CUST
            'End If

            'Dim objAdapter2 As New SqlDataAdapter(strsqlQuery2, GetConnectionString)
            'objAdapter2.Fill(CboPaidByCustDT)
            'Me.PaidByCustMs = CboPaidByCustDT

            '============= Handle Riot =============
            Dim strsqlQueryRiot As String
            Dim SBuilder3 As New StringBuilder
            SBuilder3.Append("create table #TempTable (")
            SBuilder3.Append("ID  int ,")
            SBuilder3.Append("Description char(8) ) ")
            SBuilder3.Append("EXEC ('")
            SBuilder3.Append("INSERT INTO #TEMPTABLE Select ''1'' as ID ,''Yes'' as Description ")
            SBuilder3.Append("INSERT INTO #TEMPTABLE Select ''0'' as ID ,''No'' as Description ")
            SBuilder3.Append("Select ID,Description From #TempTable ')")
            strsqlQueryRiot = SBuilder3.ToString
            Dim objAdapterRiot As New SqlDataAdapter(strsqlQueryRiot, GetConnectionString)
            objAdapterRiot.Fill(CboBooleanRiotDT)
            Me.RiotMs = CboBooleanRiotDT

            '============= Handle PA =============
            Dim strsqlQueryPA As String
            Dim SBuilder4 As New StringBuilder
            SBuilder4.Append("create table #TempTable (")
            SBuilder4.Append("ID  int ,")
            SBuilder4.Append("Description char(8) ) ")
            SBuilder4.Append("EXEC ('")
            SBuilder4.Append("INSERT INTO #TEMPTABLE Select ''1'' as ID ,''Yes'' as Description ")
            SBuilder4.Append("INSERT INTO #TEMPTABLE Select ''0'' as ID ,''No'' as Description ")
            SBuilder4.Append("Select ID,Description From #TempTable ')")
            strsqlQueryPA = SBuilder4.ToString
            Dim objAdapterPA As New SqlDataAdapter(strsqlQueryPA, GetConnectionString)
            objAdapterPA.Fill(CboBooleanPADT)
            Me.PAMs = CboBooleanPADT

        End If
    End Sub



#End Region
#Region "Ketika Tombol Calculate di click !"


    Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click

        PreviewPremiumAmount()

        'ImbSave.Visible = True ' ditambahkan sejak selasa 6 apr 2004 
        Dim BolValidasiStep1 As Boolean = True
        If Me.PageSource = COMPANY_CUSTOMER Then

            '============= Validasi dulu bila companycustomer =====
            Dim loopCheckPaid As Int16
            Dim DDLPaid As New DropDownList

            For loopCheckPaid = 0 To CType(DgridInsurance.Items.Count - 1, Int16)

                'Dim DDLPaid As New DropDownList
                DDLPaid = CType(DgridInsurancePaid.Items(loopCheckPaid).FindControl("DDLPaidByCustDGrid"), DropDownList)
                Dim ParamPaidCustStatus As String
                ParamPaidCustStatus = DDLPaid.SelectedItem.Value.ToUpper

                If DDLPaid.SelectedItem.Value.ToUpper.Trim <> "PAID" And loopCheckPaid = 0 Then
                    LblErrorMessages.Text = "Premi Tahun Pertama harus dibayar oleh Customer"
                    BolValidasiStep1 = False

                    'Bila Validasi Tidak Ok maka...Tombol Calculate masih boleh dipijit
                    Imagebutton1.Enabled = True
                    Exit For
                End If

            Next

            If BolValidasiStep1 = False Then Exit Sub

            Dim lblDiscountValidator As Label
            Dim LblYearIns As New Label
            Dim PaidAmt As TextBox
            Dim Disc As TextBox
            Dim Total As Label

            For loopCheckPaid = 0 To CType(DgridInsurance.Items.Count - 1, Int16)
                LblYearIns = CType(DgridInsurance.Items(loopCheckPaid).FindControl("LblYearInsurance"), Label)
                PaidAmt = CType(DgridInsurancePaid.Items(loopCheckPaid).FindControl("txtPaidAmountByCustDtg"), TextBox)
                lblDiscountValidator = CType(DgridInsurancePaid.Items(loopCheckPaid).FindControl("lblDiscountValidator"), Label)
                If PaidAmt.Text = "" Then
                    PaidAmt.Text = "0"
                End If
                Disc = CType(DgridInsurancePaid.Items(loopCheckPaid).FindControl("txtDiscountDtg"), TextBox)
                If Disc.Text = "" Then
                    Disc.Text = "0"
                End If
                If CDbl(Disc.Text) >= Me.DiscountMin And CDbl(Disc.Text) <= Me.DiscountMax Then
                    lblDiscountValidator.Text = ""
                Else
                    lblDiscountValidator.Text = "Discount harus diantara " & Me.DiscountMin.ToString & " dan " & Me.DiscountMax.ToString
                    BolValidasiStep1 = False
                    Exit For
                End If
                Total = CType(DgridInsurance.Items(loopCheckPaid).FindControl("LblTotalDGrid"), Label)
                DDLPaid = CType(DgridInsurancePaid.Items(loopCheckPaid).FindControl("DDLPaidByCustDGrid"), DropDownList)

                If DDLPaid.SelectedItem.Value.ToUpper.Trim = "PAID" Then
                    'If CDbl(PaidAmt.Text) <= 0 Or PaidAmt.Text = "" Then
                    '    LblErrorMessages.Text = "Fill Paid Amount By Customer for Insurance Year " + LblYearIns.Text
                    '    BolValidasiStep1 = False
                    '    Exit For
                    'End If
                    If CDbl(PaidAmt.Text) < 0 Then
                        LblErrorMessages.Text = "Jumlah dibayar Customer untuk Asuransi Tahun  " + LblYearIns.Text + " tidak boleh negatif"
                        BolValidasiStep1 = False
                        Exit For
                    End If
                    If (CDbl(Total.Text) - CDbl(Disc.Text)) < CDbl(PaidAmt.Text) Then
                        LblErrorMessages.Text = "Jumlah dibayar Customer untuk Asuransi Tahun " + LblYearIns.Text + " harus <= Total - Discount"
                        BolValidasiStep1 = False
                        Exit For
                    End If

                End If
            Next
        End If

        If BolValidasiStep1 = False Then Exit Sub
        ImbSave.Visible = True ' ditambahkan sejak selasa 6 apr 2004 
        'Bila Validasi Ok maka...proses lanjut
        'Tombol Calculate di freeze !
        Imagebutton1.Enabled = False
        PnlDGrid2Insurance.Visible = True
        PnlGrid.Visible = False

        ' ============================== Proses Kalkulasi di Grid ====================
        LblErrorMessages.Text = ""
        LblErrorMessages2.Text = ""
        Dim LoopCalculate As Int16
        Dim customClass As New Parameter.InsuranceCalculation

        Dim strcompanycustomer As Boolean
        If Me.PageSource.Trim = COMPANY_CUSTOMER Then
            strcompanycustomer = True
        Else
            strcompanycustomer = False
        End If
        ' -------=========||| Untuk InsuranceAsset |||===========---------------------
        With customClass
            .InsuranceComBranchID = Me.InsuranceComBranchID
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .ApplicationTypeID = Me.ApplicationTypeID
            .UsageID = Me.AssetUsageID
            .NewUsed = Me.AssetNewUsed
            .strConnection = GetConnectionString
        End With

        Try
            oInsAppController.ProcessNewAppInsuranceByCompanySaveAddTemporary(customClass)
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            Response.Write(ex.Message)
            err.WriteLog("NewAppInsuranceByCompany.aspx.vb", "sub Calculate", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            ImbSave.Visible = False
            Exit Sub
        End Try

        Dim LblYearInsurance As New Label
        Dim lblYearNumRate As Label
        Dim CoverageTpye As DropDownList
        Dim SRCC As DropDownList
        Dim TPLSelection As DropDownList
        Dim ParamSRCC As String
        Dim Flood As DropDownList
        Dim PaidByCust As DropDownList
        Dim PaidAmtByCust As TextBox
        Dim Discount As TextBox
        Dim Riot As DropDownList
        Dim PA As DropDownList

        For LoopCalculate = 0 To CType(DgridInsurance.Items.Count - 1, Int16)
            LblYearInsurance = CType(DgridInsurance.Items(LoopCalculate).FindControl("LblYearInsurance"), Label)
            lblYearNumRate = CType(DgridInsurance.Items(LoopCalculate).FindControl("lblYearNumRate"), Label)
            CoverageTpye = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLCoverageTypeDgrid"), DropDownList)
            SRCC = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLBolSRCCPilihan"), DropDownList)
            TPLSelection = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLTPLPilihan"), DropDownList)
            Flood = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLFloodPilihan"), DropDownList)
            Riot = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLRiotPilihan"), DropDownList)
            PA = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLPAPilihan"), DropDownList)
            PaidByCust = CType(DgridInsurancePaid.Items(LoopCalculate).FindControl("DDLPaidByCustDGrid"), DropDownList)
            PaidAmtByCust = CType(DgridInsurancePaid.Items(LoopCalculate).FindControl("txtPaidAmountByCustDtg"), TextBox)
            If PaidAmtByCust.Text = "" Then
                PaidAmtByCust.Text = "0"
            End If

            Discount = CType(DgridInsurancePaid.Items(LoopCalculate).FindControl("txtDiscountDtg"), TextBox)
            If Discount.Text = "" Then
                Discount.Text = "0"
            End If
            ' -------=========||| Untuk InsuranceAssetDetail  |||===========---------------------
            Dim strdatemanufacturing As DateTime = CType("01/01/" & Me.ManufacturingYear, DateTime)
            If strdatemanufacturing < CType("01/01/1900", DateTime) Then strdatemanufacturing = CType("01/01/1900", DateTime)

            With customClass
                .YearNum = CType(LblYearInsurance.Text, Int16)
                .YearNumRate = CType(lblYearNumRate.Text, Int16)
                .CoverageType = CoverageTpye.SelectedItem.Value.Trim
                .BolSRCC = CType(SRCC.SelectedItem.Value.Trim, Boolean)
                If TPLSelection.SelectedIndex > 0 Then
                    .TPL = CType(TPLSelection.SelectedItem.Value.Trim, Double)
                Else
                    .TPL = 0
                End If
                .BolFlood = CType(Flood.SelectedItem.Value.Trim, Boolean)
                .BolRiot = CType(Riot.SelectedItem.Value.Trim, Boolean)
                .BolPA = CType(PA.SelectedItem.Value.Trim, Boolean)
                .PaidByCustStatus = PaidByCust.SelectedItem.Value.Trim
                .AmountCoverage = Me.AmountCoverage
                .InsuranceType = Me.InsuranceType
                .UsageID = Me.AssetUsageID
                .NewUsed = Me.AssetNewUsed
                .BranchId = Me.BranchID
                .InsLength = Me.InsLength
                Context.Trace.Write("Ketika Tombol Calculate diclik -> Inslength = " + CType(Me.InsLength, String))
                .BusinessDate = Me.BusinessDate
                .DateMonthYearManufacturingYear = strdatemanufacturing
                .IsPageSourceCompanyCustomer = strcompanycustomer
                If PaidByCust.SelectedItem.Value.Trim = "PDC" Then
                    .PaidAmountByCust = 0
                Else
                    .PaidAmountByCust = CDbl(PaidAmtByCust.Text)
                End If
                .DiscountToCust = CDbl(Discount.Text)
                .strConnection = GetConnectionString
            End With

            Try
                oInsAppController.ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(customClass)
            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                err.WriteLog("NewAppInsuranceByCompany.aspx", "Calculate", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                Response.Write(ex.Message)
                ImbSave.Visible = False
                Exit For
            End Try

        Next

        ReBindGridInsurance()
        DisplayResult1()


    End Sub


#End Region
#Region "ReBindGridInsurance"

    Public Sub ReBindGridInsurance()

        Dim oGrid2 As New Parameter.InsuranceCalculationResult
        With oGrid2
            .BranchId = Me.BranchID.Trim
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString
        End With

        oGrid2 = oInsCalResultController.DisplayResultOnGrid(oGrid2)
        Dim dtEntity As New DataTable

        If Not oGrid2 Is Nothing Then
            dtEntity = oGrid2.ListData
        End If

        Dgrid2Insurance.DataSource = dtEntity
        Dgrid2Insurance.DataBind()
        Dgrid2InsurancePaid.DataSource = dtEntity
        Dgrid2InsurancePaid.DataBind()

    End Sub

#End Region
#Region "Proses Menampilkan hasil perhitungan ke layar "

    Public Sub DisplayResult1()


        Dim oResultInsCalculationResult As New Parameter.InsuranceCalculationResult

        With oCustomClassResult
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .strConnection = GetConnectionString
        End With

        Try
            oResultInsCalculationResult = oInsCalResultController.DisplayResultInsCalculationStep1(oCustomClassResult)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        With oResultInsCalculationResult

            LblTotalSRCCPremium.Text = FormatNumber(.TotalSRCPremiumToCust, 2, , , )
            LblTotalFloodPremium.Text = FormatNumber(.TotalFloodPremiumToCust, 2, , , )
            LblTotalRiotPremium.Text = FormatNumber(.TotalRiotPremiumToCust, 2, , , )
            LblTotalPAPremium.Text = FormatNumber(.TotalPAPremiumToCust, 2, , , )
            LblTotalTPLPremium.Text = FormatNumber(.TotalTPLPremiumToCust, 2, , , )
            LblTotalLoadingFee.Text = FormatNumber(.TotalLoadingFeeToCust, 2, , , )
            LblTotalStandardPremium.Text = FormatNumber(.TotalStdPremium, 2, , , )
            LblPremiumToCust.Text = CStr(.TotalPremiumToCustBeforeDiscount) 'diremark sejak tgl 30 september 2003 jam 13:25
            'LblPremiumToCust.Text = FormatNumber(.TotalPremiumToCustBeforeDiscount, 2, , , )

            TxtDiscountPremium.Value = CType(.TotalDiscountToCust, String)

            LblTotalPremiumByCust.Text = CType(CDbl(LblPremiumToCust.Text) - CDbl(TxtDiscountPremium.Value), String)
            'LblTotalPremiumByCust.Text = FormatNumber(.TotalPremiumToCustBeforeDiscount, 2, , , )

            Me.PremiumAmountByCustBeforeDisc = .TotalPremiumToCustBeforeDiscount
            Me.InterestType = .InterestType
            Me.InstallmentScheme = .InstallmentScheme
            Me.CustomerID = .CustomerID
            LblAmountCapitalized.Text = "0"
            'TxtPremiumBase.Text = LblPremiumToCust.Text
            TxtPremiumBase.Text = "0"
            'TxtPaidAmountByCustomer.Value = CType(.TotalPremiumToCustBeforeDiscount, String)
            TxtPaidAmountByCustomer.Value = CType(.TotalPaidAmountByCust, String)
            'TxtDiscountPremium.Value = CType(.TotalDiscountToCust, String)
            If TxtAdditionalCap.Text = "" Then TxtAdditionalCap.Text = "0"

            TxtAccNotes.Text = .AccNotes
            TxtInsNotes.Text = .InsNotes

            LblPremiumToCustFormat.Text = FormatNumber(.TotalPremiumToCustBeforeDiscount, 2, , , )
            LbTotalPremiumByCustformat.Text = FormatNumber(CDbl(LblPremiumToCustFormat.Text) - CDbl(TxtDiscountPremium.Value), 2, , , )
            'LbTotalPremiumByCustformat.Text = FormatNumber(.TotalPremiumToCustBeforeDiscount, 2, , , )
            LblAmountCapitalizedFormat.Text = FormatNumber(CDbl(LblPremiumToCustFormat.Text) - (CDbl(TxtPaidAmountByCustomer.Value) + CDbl(TxtDiscountPremium.Value)), 2, , , )

            rgvAdminAmountCapitalized.MinimumValue = "0"
            rgvAdminAmountCapitalized.MaximumValue = CType(.AdminFee, String)
            rgvAdminAmountCapitalized.ErrorMessage = "Biaya Admin harus diantara 0 dan " + CType(.AdminFee, String)
        End With

        'Tombol  save dibawah diaktifkan
        ' ImbSave.Visible = True di non ajtifkan sejak selasa 6 apr 2004

        If Me.PageSource = COMPANY_CUSTOMER Then
            txtInsAdminFee.Enabled = False
            TxtInsStampDutyFee.Enabled = False
            TxtDiscountPremium.Disabled = True
            'TxtDiscountPremium.Disabled = False
            'TxtDiscountPremium.Value = "0"
            TxtPaidAmountByCustomer.Disabled = True
            'TxtPaidAmountByCustomer.Disabled = False
        Else
            txtInsAdminFee.Enabled = False
            TxtInsStampDutyFee.Enabled = False
            TxtDiscountPremium.Disabled = True
            TxtDiscountPremium.Value = "0"
            TxtPaidAmountByCustomer.Disabled = True
            TxtPaidAmountByCustomer.Value = "0"
        End If

    End Sub
#End Region
#Region "Tombol Cancel Paling Bawah !"

    Private Sub ImbCancelPalingBawah_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImbCancelPalingBawah.Click
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
#End Region
#Region "Sequence Validasi'"

    Public Function SequenceValidasi() As Boolean

        If TxtAdditionalCap.Text = "" Then
            TxtAdditionalCap.Text = "0"
        End If
        If TxtPremiumBase.Text = "" Then
            TxtPremiumBase.Text = "0"
        End If

        'bila typenya company at cust maka kasih return true aja
        '03 oktober 2003 koreksi nilai javascriptnya...
        Dim premiumbycustamount As Double = Me.PremiumAmountByCustBeforeDisc - CType(TxtDiscountPremium.Value, Double)
        Dim refreshamountcapitalized As Double = premiumbycustamount - CType(TxtPaidAmountByCustomer.Value, Double)

        If Not Me.PageSource = COMPANY_CUSTOMER Then
            Return True
            LblErrorMessages2.Text = ""
            Exit Function
        End If

        Context.Trace.Write("TxtDiscountPremium = " + TxtDiscountPremium.Value)
        Context.Trace.Write("TxtPaidAmountByCustomer = " + TxtPaidAmountByCustomer.Value)
        Context.Trace.Write("Total Premium By Cust = " & premiumbycustamount & "")
        Context.Trace.Write("Paid Amount By Cust = " & TxtPaidAmountByCustomer.Value & "")
        Context.Trace.Write("Amount Capitalized = " & refreshamountcapitalized & "")


        If CType(TxtPaidAmountByCustomer.Value, Decimal) > (CType(LblPremiumToCust.Text, Decimal) - CType(TxtDiscountPremium.Value, Decimal)) Then
            LblErrorMessages2.Text = "Jumlah diKreditkan/Capitalized harus >= 0 "
            LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
            LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
            Return False
        End If

        If Not ValidasiAngkaPertama((CType(LblPremiumToCust.Text, Decimal) - CType(TxtDiscountPremium.Value, Decimal)), CType(TxtPremiumBase.Text, Decimal)) Then
            LblErrorMessages2.Text = "Premi Dasar untuk Refund Showrom tidak boleh lebih besar dari Total Premi pada Customer "
            LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
            LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
            LblPremiumToCustFormat.Text = FormatNumber(LblPremiumToCust.Text, 2)
            LbTotalPremiumByCustformat.Text = FormatNumber(LblTotalPremiumByCust.Text, 2)
            Return False
        End If

        If Not ValidasiAngkaPertama(CType(LblPremiumToCust.Text, Decimal), CType(TxtDiscountPremium.Value, Decimal)) Then
            LblErrorMessages2.Text = "Jumlah Discount Premi tidak boleh lebih besar dari Jumlah Premi pada Customer"
            LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
            LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
            Return False
        End If


        'LblTotalPremiumByCust.Text  remark 03 oktober 2003 
        If Not ValidasiAngkaPertama(CType(premiumbycustamount, Decimal), CType(TxtPaidAmountByCustomer.Value, Decimal)) Then
            LblErrorMessages2.Text = "Jumlah dibayar oleh Customer tidak boleh lebih besar dari Total Premi pada Customer "
            'Context.Trace.Write("Total Premium By Cust = " & LblTotalPremiumByCust.Text & "")
            LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
            LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
            Return False
        End If

        If Me.AdminFeeBehavior = "N" Then
            'Minimum , Admin Fee Bisa diedit menjadi lebih besar
            'tapi tidak boleh kurang dari nilai awal
            If Not ValidasiAngkaPertama(CType(txtInsAdminFee.Text, Decimal), CType(Me.NilaiAdminFeeAwal, Decimal)) Then
                LblErrorMessages2.Text = "Biaya Admin tidak boleh lebih kecil dari  " & Me.NilaiAdminFeeAwal
                LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
                LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
                Return False
            End If
        End If

        If Me.AdminFeeBehavior = "X" Then
            'Maximum, Admin Fee bisa diedit menjadi lebih kecil
            'Tapi tidak boleh lebik dari nilai awal
            If ValidasiAngkaPertama(CType(txtInsAdminFee.Text, Decimal), CType(Me.NilaiAdminFeeAwal, Decimal)) Then
                LblErrorMessages2.Text = "Biaya Admin Tidak boleh lebih besar dari  " & Me.NilaiAdminFeeAwal
                LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
                LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
                Return False
            End If
        End If

        If Me.StampDutyFeeBehavior = "N" Then
            'Minimum , Stamp duty fee Bisa diedit menjadi lebih besar
            'tapi tidak boleh kurang dari nilai awal
            If Not ValidasiAngkaPertama(CType(TxtInsStampDutyFee.Text, Decimal), CType(Me.NilaiAdminFeeAwal, Decimal)) Then
                LblErrorMessages2.Text = "Biaya Materai tidak boleh lebih kecil dari " & Me.NilaiStampDutyFeeAwal
                LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
                LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
                Return False
            End If
        End If

        If Me.StampDutyFeeBehavior = "X" Then
            'Maximum, Stamp duty  Fee bisa diedit menjadi lebih kecil
            'Tapi tidak boleh lebik dari nilai awal
            If ValidasiAngkaPertama(CType(TxtInsStampDutyFee.Text, Decimal), CType(Me.NilaiAdminFeeAwal, Decimal)) Then
                LblErrorMessages2.Text = "Biaya Materai tidak boleh lebih besar dari " & Me.NilaiStampDutyFeeAwal
                LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
                LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
                Return False
            End If
        End If

        LblErrorMessages2.Text = ""
        Return True

    End Function

#End Region
#Region "Ketika Tombol Save Paling Bawah Dipijit "
    Private Sub ImbSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImbSave.Click
        If Not SequenceValidasi() Then
            Exit Sub
        End If

        Dim oCustomClass As New Parameter.InsuranceCalculationResult

        If Me.InsuranceComBranchID_lbl = "" Then
            Me.InsuranceComBranchID_lbl = "0"
        End If

        With oCustomClass
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AmountCoverage = Me.AmountCoverage
            .AdminFeeToCust = CType(txtInsAdminFee.Text, Double)
            .MeteraiFeeToCust = CType(TxtInsStampDutyFee.Text, Double)
            .DiscToCustAmount = CType(Replace(TxtDiscountPremium.Value, ",", ""), Double)
            .PaidAmountByCust = CType(Replace(TxtPaidAmountByCustomer.Value, ",", ""), Double)
            .PremiumBaseForRefundSupp = CType(TxtPremiumBase.Text, Double)
            .AccNotes = TxtAccNotes.Text.Trim
            .InsNotes = TxtInsNotes.Text.Trim
            .InsLength = Me.InsLength
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString
            .PremiumToCustAmount = Me.PremiumAmountByCustBeforeDisc - CType(TxtDiscountPremium.Value, Double)
            .InsuranceComBranchID = Me.InsuranceComBranchID_lbl
            .AdditionalCapitalized = CDbl(TxtAdditionalCap.Text)
            If TxtAdditionalInsurancePremium.Value Is Nothing Then
                .AdditionalInsurancePremium = 0
            Else
                .AdditionalInsurancePremium = CDbl(TxtAdditionalInsurancePremium.Value)
            End If

            .RefundToSupplier = CDbl(txtRfnSupplier.Text)
        End With

        Try
            oInsAppController.ProcessSaveInsuranceApplicationLastProcess(oCustomClass)
            Response.Redirect("../../../../Webform.LoanOrg/Credit/CreditProcess/ApplicationMaintenance/ApplicationMaintenance.aspx")
        Catch ex As Exception
            Response.Write(ex.Message)
            Dim err As New MaxiloanExceptions
            err.WriteLog("EditInsurance.aspx", "Save", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        End Try


        'If CheckBoxEntryFinancial.Checked Then
        '    SendCookies()
        '    Response.Redirect("../../../../Webform.LoanOrg/Credit/CreditProcess/ApplicationMaintenance/EditFinancialData_002.aspx")
        'Else
        '    Response.Redirect("../../../../Webform.LoanOrg/Credit/CreditProcess/ApplicationMaintenance/ApplicationMaintenance.aspx")

        'End If


    End Sub
#End Region
#Region "SendCookies"
    Sub SendCookies()

        Dim cookie As New HttpCookie("Financial")
        cookie.Values.Add("id", Me.ApplicationID)
        cookie.Values.Add("custid", Me.CustomerID)
        cookie.Values.Add("name", Me.CustomerName)
        cookie.Values.Add("EmployeeID", "")
        cookie.Values.Add("InterestType", Me.InterestType)
        cookie.Values.Add("InstallmentScheme", Me.InstallmentScheme)
        Response.AppendCookie(cookie)
    End Sub
#End Region
#Region "Function Validasi ketika tombol save dipencet"

    Public Function ValidasiAngkaPertama(ByVal angkapertama As Decimal, ByVal angkakedua As Decimal) As Boolean

        If angkapertama >= angkakedua Then
            'bila angka pertama lebih besar dari angka kedua maka BENAR
            Return True
        Else
            'bila angka pertama lebih kecil dari angka kedua maka Salah
            Return False
        End If


    End Function

#End Region


    Private Sub DgridInsurancePaid_DataBinding(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DgridInsurancePaid.ItemDataBound
        Dim CboPaidByCustDT As New DataTable("PaidByCustMs")
        Dim DDLPaidByCustPilihan As DropDownList
        Dim LblHiddenPaidByCust As Label

        If e.Item.ItemIndex > -1 Then

            DDLPaidByCustPilihan = CType(e.Item.FindControl("DDLPaidByCustDGrid"), DropDownList)
            LblHiddenPaidByCust = CType(e.Item.FindControl("LblHiddenPaidByCust"), Label)
            CboPaidByCustDT = Me.PaidByCustMs
            LblHiddenPaidByCust.Visible = False


            Dim CbodvPaidByCust As DataView
            CbodvPaidByCust = CboPaidByCustDT.DefaultView
            DDLPaidByCustPilihan.DataSource = CbodvPaidByCust
            DDLPaidByCustPilihan.DataTextField = "Description"
            DDLPaidByCustPilihan.DataValueField = "ID"
            DDLPaidByCustPilihan.DataBind()
            DDLPaidByCustPilihan.ClearSelection()

            Context.Trace.Write("Nilai PaidByCust dari data= '" & LblHiddenPaidByCust.Text.Trim & "'")
            'DDLPaidByCustPilihan.Items.FindByValue("PAID      ").Selected = True
            If LblHiddenPaidByCust.Text = "NOTPAID" Then
                DDLPaidByCustPilihan.Items.FindByValue("PAID").Selected = True
            Else
                DDLPaidByCustPilihan.Items.FindByValue(LblHiddenPaidByCust.Text).Selected = True
            End If
            'DDLPaidByCustPilihan.Items.FindByValue(LblHiddenPaidByCust.Text).Selected = True
        Else
            Dim strsqlQuery2 As String
            If Me.PageSource = COMPANY_CUSTOMER Then
                'strsqlQuery2 = CommonVariableHelper.SQL_QUERY_PAIDBYCUST_MS
                strsqlQuery2 = "Select rtrim(ltrim(upper(ID))) as ID, description from tblpaidbycuststatus Where ID <> 'NOTPAID' Order by ID"
            Else
                strsqlQuery2 = CommonVariableHelper.SQL_QUERY_PAIDBYCUST_MS_PAGESOURCE_COMPANY_AT_CUST
            End If

            Dim objAdapter2 As New SqlDataAdapter(strsqlQuery2, GetConnectionString)
            objAdapter2.Fill(CboPaidByCustDT)
            Me.PaidByCustMs = CboPaidByCustDT
        End If

    End Sub

    Private Sub Imagebutton2_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton2.Click
        Me.IsPreview = "1"
        PreviewPremiumAmount()

    End Sub

    Private Sub cekMixedIns()
        Dim oRow As Date
        Dim i As Integer
        Dim j As Integer
        Dim lblYearNumRate As Label
        Dim DDLCoverageTypeDgrid As DropDownList
        Dim strPrevInsType As String = ""

        j = 0

        For i = 0 To DgridInsurance.Items.Count - 1
            lblYearNumRate = CType(DgridInsurance.Items(i).Cells(1).FindControl("lblYearNumRate"), Label)
            DDLCoverageTypeDgrid = CType(DgridInsurance.Items(i).Cells(1).FindControl("DDLCoverageTypeDgrid"), DropDownList)

            If DDLCoverageTypeDgrid.SelectedValue.Trim <> strPrevInsType Then
                j = 1
                lblYearNumRate.Text = j.ToString
                strPrevInsType = DDLCoverageTypeDgrid.SelectedValue.Trim
            Else
                j += 1
                lblYearNumRate.Text = j.ToString
            End If
        Next
    End Sub

    Public Sub PreviewPremiumAmount()
        cekMixedIns()
        PnlDGridPaid.Visible = True

        Dim strYear As String
        Dim strPrice As String
        Dim strYearRate As String

        Dim lblPrice As TextBox
        Dim lblYear As Label
        Dim LblYearInsurance As New Label
        Dim lblYearNumRate As Label
        Dim CoverageType As DropDownList
        Dim SRCC As DropDownList
        Dim TPLSelection As DropDownList
        Dim ParamSRCC As String
        Dim Flood As DropDownList
        Dim PaidByCust As DropDownList
        Dim PaidAmtByCust As TextBox
        Dim Discount As TextBox
        Dim Riot As DropDownList
        Dim PA As DropDownList

        Dim strCoverageType As String
        Dim strBolSRCC As String
        Dim strBolFlood As String
        Dim strTPL As String
        Dim strBolRiot As String
        Dim strBolPA As String
        Dim strdatemanufacturing As DateTime = CType("01/01/" & Me.ManufacturingYear, DateTime)
        If strdatemanufacturing < CType("01/01/1900", DateTime) Then strdatemanufacturing = CType("01/01/1900", DateTime)

        Dim LoopCalculate As Int16
        For LoopCalculate = 0 To CType(DgridInsurance.Items.Count - 1, Int16)
            LblYearInsurance = CType(DgridInsurance.Items(LoopCalculate).FindControl("LblYearInsurance"), Label)
            lblYearNumRate = CType(DgridInsurance.Items(LoopCalculate).FindControl("lblYearNumRate"), Label)
            CoverageType = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLCoverageTypeDgrid"), DropDownList)
            SRCC = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLBolSRCCPilihan"), DropDownList)
            TPLSelection = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLTPLPilihan"), DropDownList)
            Flood = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLFloodPilihan"), DropDownList)
            Riot = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLRiotPilihan"), DropDownList)
            PA = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLPAPilihan"), DropDownList)

            strYear &= CStr(IIf(strYear = "", "", ",")) & LblYearInsurance.Text.Trim.Replace("'", "")
            strYearRate &= CStr(IIf(strYearRate = "", "", ",")) & lblYearNumRate.Text.Trim.Replace("'", "")
            strCoverageType &= CStr(IIf(strCoverageType = "", "", ",")) & CoverageType.SelectedValue.Trim.Replace("'", "")
            strBolSRCC &= CStr(IIf(strBolSRCC = "", "", ",")) & SRCC.SelectedValue.Trim.Replace("'", "")
            strBolFlood &= CStr(IIf(strBolFlood = "", "", ",")) & Flood.SelectedValue.Trim.Replace("'", "")
            strTPL &= CStr(IIf(strTPL = "", "", ",")) & TPLSelection.SelectedValue.Trim.Replace("'", "")
            strBolRiot &= CStr(IIf(strBolRiot = "", "", ",")) & Riot.SelectedValue.Trim.Replace("'", "")
            strBolPA &= CStr(IIf(strBolPA = "", "", ",")) & PA.SelectedValue.Trim.Replace("'", "")
        Next

        Dim strcompanycustomer As Boolean
        If Me.PageSource.Trim = COMPANY_CUSTOMER Then
            strcompanycustomer = True
        Else
            strcompanycustomer = False
        End If

        Dim dtpremium As DataTable

        Dim customClass As New Parameter.InsuranceCalculation
        With customClass
            .IsPageSourceCompanyCustomer = strcompanycustomer
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .YearNumDtg = strYear
            .YearNumRateDtg = strYearRate
            .CoverageTypeDtg = strCoverageType
            .ApplicationTypeID = Me.ApplicationTypeID
            .AmountCoverage = Me.AmountCoverage
            .InsuranceType = Me.InsuranceType
            .UsageID = Me.AssetUsageID
            .NewUsed = Me.AssetNewUsed
            .BolSRCCDtg = strBolSRCC
            .BolFloodDtg = strBolFlood
            .DateMonthYearManufacturingYear = strdatemanufacturing
            .TPLAmountToCustDtg = strTPL
            .BusinessDate = Me.BusinessDate
            .NumRows = DgridInsurance.Items.Count
            .strConnection = GetConnectionString
            .BolRiotDtg = strBolRiot
            .BolPADtg = strBolPA
        End With

        customClass = oControllerPremium.DisplaySelectedPremiumOnGrid(customClass)

        'If Not customClass Is Nothing Then
        dtpremium = customClass.ListData
        'Else

        'End If
        'ReBindGridInsurance()
        DgridInsurance.DataSource = dtpremium.DefaultView
        DgridInsurance.CurrentPageIndex = 0
        DgridInsurance.DataBind()

    End Sub

    Private Sub DgridInsurance_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DgridInsurance.SelectedIndexChanged

    End Sub

End Class