﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class AppDeviations
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail

#Region "Constanta"
    Private Dcontroller As New DataUserControlController
    Private m_controller As New AppDeviationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region


#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
#End Region
#Region "LinkTo"
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function
    Function LinkToSupplier(ByVal strStyle As String, ByVal strSupplierID As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function

    Function LinkToEmployee(ByVal strStyle As String, ByVal strBranchID As String, ByVal strAOID As String) As String
        Return "javascript:OpenWinEmployee('" & strStyle & "','" & strBranchID.Replace("'", "") & "','" & strAOID & "')"
    End Function
#End Region

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "APPDEVIATION"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.Sort = "ApplicationId ASC"
            Me.CmdWhere = ""
            InitialPanel()
            BindGrid(Me.CmdWhere)
        End If
    End Sub
#End Region
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.AppDeviation
        oCustomClass.PageSize = pageSize
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
        oCustomClass = m_controller.GetAppDeviation(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Sub BindGridApprovalScheme(ByVal ApprovalSchemeID As String, ByVal NTF As Decimal)
        lblPokokHutang.Text = FormatNumber(NTF, 0)


        Dim dtEntity As New DataTable
        Dim oRow As DataRow
        Dim oCustomClass As New Parameter.ApprovalScheme
        Dim MaximalAmaount As Decimal = 0

        With dtEntity
            .Columns.Add(New DataColumn("ApprovalSchemeID", GetType(String)))
            .Columns.Add(New DataColumn("ApprovalSeqNum", GetType(String)))
            .Columns.Add(New DataColumn("ParentApprovalSeqNum", GetType(String)))
            .Columns.Add(New DataColumn("Description", GetType(String)))
            .Columns.Add(New DataColumn("Level_", GetType(String)))
            .Columns.Add(New DataColumn("MaxLimit", GetType(String)))
            .Columns.Add(New DataColumn("CanFinalReject", GetType(String)))
            .Columns.Add(New DataColumn("CanFinalApprove", GetType(String)))
            .Columns.Add(New DataColumn("CanEscalation", GetType(String)))
            .Columns.Add(New DataColumn("RejectAction", GetType(String)))            
        End With
        

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.ApprovalSchemeID = ApprovalSchemeID
        oCustomClass = m_controller.GetApprovalPath(oCustomClass)



        If oCustomClass.Listdata.Rows.Count > 0 Then
            Dim amountNext As Decimal = 0

            For index As Integer = 0 To oCustomClass.Listdata.Rows.Count - 1

                If (index + 1) >= oCustomClass.Listdata.Rows.Count Then
                    amountNext = 0
                Else
                    amountNext = CDec(oCustomClass.Listdata.Rows(index + 1).Item("MaxLimit"))
                End If



                If amountNext <> 0 Then
                    If CDec(oCustomClass.Listdata.Rows(index).Item("MaxLimit")) >= NTF And
                        amountNext <= NTF Then                        
                        MaximalAmaount = CDec(oCustomClass.Listdata.Rows(index).Item("MaxLimit"))
                        Exit For
                    End If
                End If
            Next

            For index = 0 To oCustomClass.Listdata.Rows.Count - 1
                If MaximalAmaount >= CDec(oCustomClass.Listdata.Rows(index).Item("MaxLimit")) Then
                    oRow = dtEntity.NewRow()
                    oRow("ApprovalSchemeID") = oCustomClass.Listdata.Rows(index).Item("ApprovalSchemeID")
                    oRow("ApprovalSeqNum") = oCustomClass.Listdata.Rows(index).Item("ApprovalSeqNum")
                    oRow("ParentApprovalSeqNum") = oCustomClass.Listdata.Rows(index).Item("ParentApprovalSeqNum")
                    oRow("Description") = oCustomClass.Listdata.Rows(index).Item("Description")
                    oRow("Level_") = oCustomClass.Listdata.Rows(index).Item("Level_")
                    oRow("MaxLimit") = oCustomClass.Listdata.Rows(index).Item("MaxLimit")
                    oRow("CanFinalReject") = oCustomClass.Listdata.Rows(index).Item("CanFinalReject")
                    oRow("CanFinalApprove") = oCustomClass.Listdata.Rows(index).Item("CanFinalApprove")
                    oRow("CanEscalation") = oCustomClass.Listdata.Rows(index).Item("CanEscalation")
                    oRow("RejectAction") = oCustomClass.Listdata.Rows(index).Item("RejectAction")
                    dtEntity.Rows.Add(oRow)
                End If
            Next

        End If


        If dtEntity.Rows.Count > 0 Then
            dtgGridShema.DataSource = dtEntity
            dtgGridShema.DataBind()
        Else
            pnlList.Visible = False
            pnlView.Visible = True
            pnlGridShema.Visible = True
            pnlGridMember.Visible = False
        End If
        
    End Sub
    Sub BindGridApprovalMember(ByVal ApprovalSchemeID As String, ByVal ApprovalSeqNum As Integer)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.ApprovalScheme                

        oCustomClass.strConnection = GetConnectionString()
        oCustomClass.ApprovalSchemeID = ApprovalSchemeID
        oCustomClass.ApprovalSeqNum = ApprovalSeqNum
        oCustomClass = m_controller.GetApprovalPathTreeMember(oCustomClass)
        dtEntity = oCustomClass.Listdata
        dtgGridMember.DataSource = dtEntity
        dtgGridMember.DataBind()
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lnkApplicationID As New HyperLink
        Dim hynCustomerName As New HyperLink
        Dim hynSupplierName As New HyperLink
        Dim hynEmployeeName As New HyperLink
        If e.Item.ItemIndex >= 0 Then            

            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            hynSupplierName = CType(e.Item.FindControl("hynSupplierName"), HyperLink)
            hynEmployeeName = CType(e.Item.FindControl("hynEmployeeName"), HyperLink)

            lnkApplicationID.NavigateUrl = LinkTo(lnkApplicationID.Text.Trim, "AccAcq")
            hynCustomerName.NavigateUrl = LinkToCustomer(e.Item.Cells(5).Text.Trim, "AccAcq")
            hynSupplierName.NavigateUrl = LinkToSupplier("AccAcq", e.Item.Cells(6).Text.Trim)
            hynEmployeeName.NavigateUrl = LinkToEmployee("AccAcq", Me.sesBranchId.Trim, e.Item.Cells(7).Text.Trim)

        End If
    End Sub
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String = ""
        Dim dt As DataTable = Nothing
        Dim oCustomClass As New Parameter.AppDeviation
        Dim lnkApplicationID As New HyperLink
        Dim lblApprovalSchemeID As New Label
        Dim lblAmountNTF As New Label
        Dim strCustomerID As String

        Dim oGroupSupplierAccount As New Parameter.AppDeviation
        If e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "View", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            pnlList.Visible = False
            pnlView.Visible = True
            pnlGridShema.Visible = True            
            pnlGridMember.Visible = False

            lnkApplicationID = CType(e.Item.FindControl("lnkApplicationID"), HyperLink)
            lblApprovalSchemeID = CType(e.Item.FindControl("lblApprovalSchemeID"), Label)
            lblAmountNTF = CType(e.Item.FindControl("lblAmountNTF"), Label)

            BindGridApprovalScheme(lblApprovalSchemeID.Text, CDec(lblAmountNTF.Text))

            hdnApplicationId.Value = lnkApplicationID.Text.Trim
            strCustomerID = e.Item.Cells(5).Text.Trim

            ucViewApplication1.ApplicationID = hdnApplicationId.Value
            ucViewApplication1.CustomerID = strCustomerID
            ucViewApplication1.bindData()
            ucViewApplication1.initControls("OnNewApplication")
            ucViewCustomerDetail1.CustomerID = strCustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            oCustomClass.ApplicationId = lnkApplicationID.Text.Trim
            oCustomClass.strConnection = GetConnectionString()
            oCustomClass = m_controller.GetDeviationView(oCustomClass)
            If Not oCustomClass Is Nothing Then
                dt = oCustomClass.Listdata

                lblHargaOTRBerlaku.Text = CStr("PHJMB = " & FormatNumber(dt.Rows(0).Item("PHJMB").ToString.Trim, 2))
                lblHargaOTRAplikasi.Text = CStr(FormatNumber(dt.Rows(0).Item("TotalOTR").ToString.Trim, 2))
                lblHargaOTRPenyimpangan.Text = CStr(IIf(CBool(dt.Rows(0).Item("isOTRDev").ToString.Trim), "YES", "NO"))

                lblDownPaymentBerlaku.Text = CStr(FormatNumber(dt.Rows(0).Item("OfferingDPPercentage").ToString.Trim, 2) & " %")
                lblDownPaymentAplikasi.Text = CStr(FormatNumber(dt.Rows(0).Item("AgreementDPPercentage").ToString.Trim, 2) & " %")
                lblDownPaymentPenyimpangan.Text = CStr(IIf(CBool(dt.Rows(0).Item("isDPDev").ToString.Trim), "YES", "NO"))

                lblEfectiveRateBerlaku.Text = CStr(FormatNumber(dt.Rows(0).Item("OfferingEffectiveRate").ToString.Trim, 2) & " %")
                lblEfectiveRateAplikasi.Text = CStr(FormatNumber(dt.Rows(0).Item("EffectiveRate").ToString.Trim, 2) & " %")
                lblEfectiveRatePenyimpangan.Text = CStr(IIf(CBool(dt.Rows(0).Item("isEffRateDev").ToString.Trim), "YES", "NO"))

                lblAdminFeeBerlaku.Text = CStr(FormatNumber(dt.Rows(0).Item("OfferingAdminFee").ToString.Trim, 2))
                lblAdminFeeAplikasi.Text = CStr(FormatNumber(dt.Rows(0).Item("AdminFee").ToString.Trim, 2))
                lblAdminFeePenyimpangan.Text = CStr(IIf(CBool(dt.Rows(0).Item("isAdminFeeDev").ToString.Trim), "YES", "NO"))

                lblInsuranceDiscountBerlaku.Text = CStr("Max " & FormatNumber(dt.Rows(0).Item("OfferingInsuranceDiscPercentage").ToString.Trim, 2) & " %")
                lblInsuranceDiscountAplikasi.Text = CStr(FormatNumber(dt.Rows(0).Item("InsuranceRefundPercentage").ToString.Trim, 2) & " %")
                lblInsuranceDiscountPenyimpangan.Text = CStr(IIf(CBool(dt.Rows(0).Item("isInsDiscDev").ToString.Trim), "YES", "NO"))

                lblUmurKendaraanBerlaku.Text = CStr(CInt(dt.Rows(0).Item("GSYearsAsset").ToString.Trim) & " Tahun")
                lblUmurKendaraanAplikasi.Text = CStr(CInt(dt.Rows(0).Item("YearsAsset").ToString.Trim) & " Tahun")
                lblUmurKendaraanPenyimpangan.Text = CStr(IIf(CBool(dt.Rows(0).Item("isYearsAssetDev").ToString.Trim), "YES", "NO"))

                lblPenghasilanBerlaku.Text = CStr(FormatNumber(dt.Rows(0).Item("OfferingIncomeInstallmentRatio").ToString.Trim, 2) & " %")
                lblPenghasilanAplikasi.Text = CStr(FormatNumber(dt.Rows(0).Item("IncomeInstallmentRatio").ToString.Trim, 2) & " %")
                lblPenghasilanPenyimpangan.Text = CStr(IIf(CBool(dt.Rows(0).Item("isIncInsRatioDev").ToString.Trim), "YES", "NO"))
            End If
        End If
    End Sub
    Private Sub dtgGridShema_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgGridShema.ItemCommand
        Dim lblApprovalSeqNum As Label

        If e.CommandName = "member" Then
            pnlList.Visible = False
            pnlView.Visible = True
            pnlGridShema.Visible = True
            pnlGridMember.Visible = True
            lblApprovalSeqNum = CType(e.Item.FindControl("lblApprovalSeqNum"), Label)
            BindGridApprovalMember(dtgGridShema.DataKeys.Item(e.Item.ItemIndex).ToString(), CInt(lblApprovalSeqNum.Text))
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
    Sub InitialPanel()
        pnlList.Visible = True
        pnlView.Visible = False
        pnlGridShema.Visible = False
        pnlGridMember.Visible = False
        txtPage.Text = "1"
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrint.Click
        Dim customClass As New Parameter.AppDeviation
        Dim ErrMessage As String = ""
        Dim status As Boolean = False

        With customClass
            .ApplicationId = hdnApplicationId.Value.Trim
            .strConnection = GetConnectionString()
        End With

        m_controller.AppDeviationPrint(customClass)
        ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
        BindGrid(Me.CmdWhere)
        pnlList.Visible = True
        pnlView.Visible = False
        pnlGridShema.Visible = False
        pnlGridMember.Visible = False
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        pnlList.Visible = True
        pnlView.Visible = False
        pnlGridShema.Visible = False
        pnlGridMember.Visible = False
    End Sub
    Protected Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonSearch.Click
        Dim search As String = ""

        If txtSearch.Text.Contains("%") Then
            search = "'%" & txtSearch.Text.Replace("%", " ").Trim & "%'"
        Else
            search = "'%" & txtSearch.Text.Trim & "%'"
        End If

        CmdWhere = "" & cboSearch.SelectedValue & " like " & search & ""

        BindGrid(CmdWhere)
    End Sub
    Protected Sub ButtonReset_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonReset.Click
        BindGrid("")
    End Sub

End Class