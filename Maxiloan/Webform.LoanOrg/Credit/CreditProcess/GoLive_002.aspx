﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GoLive_002.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.GoLive_002" %>

<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../Webform.UserController/ValidDate.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ValidDate" Src="../../../Webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GoLive_002</title>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script language="JavaScript" src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date + '_txtDate');
            var calendar = eval('document.forms[0].all.' + date + '_txtDate_imgCalender');
            if (chk == true) {
                text.disabled = true;
                calendar.disabled = true;
                text.value = '';
                return true;
            }
            else {
                text.disabled = false;
                calendar.disabled = false;
                return true;
            }
        }		
    </script>
    <script language="javascript" type="text/javascript">
		<!--
        function fConfirm() {
            if (window.confirm("Apakah Yakin mau hapus data ini ?"))
                return true;
            else
                return false;
        }

        function fback() {
            history.go(-1);
            return false;
        }	
			-->
    </script>
    <script language="javascript" type="text/javascript">

        function findchild(parent, arrchild) {
            var tmp = '';
            var i = 0;
            for (i = 0; i < arrchild.length; i++) {
                if (arrchild[i].parent == parent) {
                    if (tmp == '') {
                        tmp = '' + i;
                    }
                    else {
                        tmp += '~' + i;
                    }
                }
            }
            return tmp;
        }
        function ChangeMultiCombo(parent, pChild, ArrParent, pTampung) {
            eval('document.forms[0].tempChild').value = "";
            var tmp;
            var child = eval('document.forms[0].' + pChild);
            var tampung = eval('document.forms[0].' + pTampung);
            var tampungname = eval('document.forms[0].' + pTampung + 'Name');
            tampungname.value = parent.options[parent.selectedIndex].text;
            tampung.value = parent.options[parent.selectedIndex].value;
            for (var i = child.options.length - 1; i >= 0; i--) {
                child.options[i] = null;
            }
            if (parent.options.length == 0) return;

            tmp = findchild(parent.options[parent.selectedIndex].value, ArrParent);
            if (tmp == '') return;
            child.options[0] = new Option('Select One', '0');
            var arrTmp = tmp.split('~');
            for (var i = 0; i < arrTmp.length; i++) {
                child.options[i + 1] = new Option(ArrParent[arrTmp[i]].text, ArrParent[arrTmp[i]].value);
            }

        }
        function setCboDetlVal(l, j) {
            eval('document.forms[0].' + tampungGrandChild).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
        function setCboBatchDate(l, j) {
            eval('document.forms[0].' + tampungGrandChild2).value = l;
            eval('document.forms[0].' + tampungGrandChildName).value = j;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="tempParent" type="hidden" name="Hidden1" runat="server" />
    <input id="tempParentName" type="hidden" name="Hidden1" runat="server" />
    <input id="tempChild" type="hidden" name="tempChild" runat="server" />
    <input id="tempChildName" type="hidden" name="tempChildName" runat="server" />
    <input id="tempChild2" type="hidden" name="tempChild2" runat="server" />
    <input id="tempGrandChild" type="hidden" name="tempGrandChild" runat="server" />
    <input id="tempGrandChildName" type="hidden" name="tempGrandChildName" runat="server" />
    <asp:Panel ID="pnlList" runat="server">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td align="center">
                    <table border="0" cellspacing="0" cellpadding="0" width="95%">
                        <tr>
                            <td colspan="3">
                                <font color="red"><i>*) Mandatory&nbsp;&nbsp;&nbsp;&nbsp;</i>
                                    <asp:Label ID="lblMessage" runat="server"></asp:Label></font>
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellspacing="0" cellpadding="0" width="95%">
                        <tr class="trtopi">
                            <td class="tdtopi" align="center">
                                PROSES LOAN ACTIVATION
                            </td>
                        </tr>
                    </table>
                    <table class="tablegrid" border="0" cellspacing="1" cellpadding="2" width="95%">
                        <tr>
                            <td class="tdgenap" width="25%">
                                Nama Customer
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:HyperLink ID="lblCustName" runat="server" NavigateUrl=""></asp:HyperLink>
                            </td>
                            <td class="tdgenap" width="25%">
                                No Kontrak
                            </td>
                            <td class="tdganjil" width="25%">
                                <asp:HyperLink ID="lblAgreementNo" runat="server" NavigateUrl=""></asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Tanggal Kontrak&nbsp;<font color="red">*)</font>
                            </td>
                            <td class="tdganjil" colspan="3">
                                <uc2:ValidDate id="uscAgreementDate" runat="server"></uc2:ValidDate>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Tanggal Effective&nbsp;<font color="red">*)</font>
                            </td>
                            <td class="tdganjil" colspan="3">
                                <uc2:ValidDate id="uscEffectiveDate" runat="server"></uc2:ValidDate>
                            </td>
                        </tr>
                        <tr class="tdganjil">
                            <td class="tdgenap" width="25%">
                                Funding Bank / Cabang
                            </td>
                            <td class="tdganjil">
                                <asp:DropDownList ID="drdCompany" runat="server" onchange="<%#drdCompanyChange()%>"
                                    Visible="False">
                                </asp:DropDownList>
                                <asp:Label ID="lblCompany" runat="server"></asp:Label>
                            </td>
                            <td class="tdgenap" width="25%">
                                &nbsp;
                            </td>
                            <td class="tdganjil" width="25%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr class="tdganjil">
                            <td class="tdgenap" width="25%">
                                No Kontrak Bank
                            </td>
                            <td class="tdganjil">
                                <asp:DropDownList ID="drdContract" runat="server" onchange="<%#drdContractChange()%>"
                                    Visible="False">
                                </asp:DropDownList>
                                <asp:Label ID="LblContract" runat="server"></asp:Label>
                            </td>
                            <td class="tdgenap" width="25%">
                                &nbsp;
                            </td>
                            <td class="tdganjil" width="25%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Tanggal Batch
                            </td>
                            <td class="tdganjil">
                                <asp:DropDownList ID="drdBatchDate" runat="server" onchange="setCboBatchDate(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);"
                                    Visible="False">
                                </asp:DropDownList>
                                <asp:Label ID="LblBatchdate" runat="server"></asp:Label>
                            </td>
                            <td class="tdgenap" width="25%">
                                &nbsp;
                            </td>
                            <td class="tdganjil" width="25%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="tdgenap" width="25%">
                                Catatan
                            </td>
                            <td class="tdganjil" colspan="3" align="left">
                                <asp:TextBox ID="txtNotes" runat="server" Width="90%" TextMode="MultiLine" CssClass="inptype"
                                    Height="51px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table border="0" cellspacing="0" cellpadding="0" width="95%">
                        <tr class="trtopi">
                            <td class="tdtopi" align="center">
                                SYARAT &amp; KONDISI
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellspacing="0" cellpadding="0" width="95%">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="dtgTC" runat="server" Width="100%" CssClass="tablegrid" CellSpacing="1"
                                    CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False" PageSize="3" DataKeyField="TCName">
                                    <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tdganjil"></ItemStyle>
                                    <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                            <HeaderStyle Width="25%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                            <HeaderStyle Width="7%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="PERIKSA">
                                            <HeaderStyle Width="8%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkTCChecked" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                                    runat="server"></asp:CheckBox>
                                                <asp:Label ID="lblVTCChecked" runat="server" ForeColor="Red">Harus diperiksa</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="TGL JANJI">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <uc1:ValidDate id="uscTCPromiseDate" runat="server"></uc1:ValidDate>
                                                <asp:Label ID="lblVPromiseDate" runat="server" ForeColor="Red">Tgl Janji harus > hari ini</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CATATAN">
                                            <HeaderStyle Width="30%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                                    runat="server" Width="95%" CssClass="inptype">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table border="0" cellspacing="0" cellpadding="0" width="95%">
                        <tr class="trtopi">
                            <td class="tdtopi" align="center">
                                SYARAT &amp; KONDISI CHECK LIST
                            </td>
                        </tr>
                    </table>
                    <table border="0" cellspacing="0" cellpadding="0" width="95%">
                        <tr align="center">
                            <td>
                                <asp:DataGrid ID="dtgTC2" runat="server" Width="100%" CssClass="tablegrid" CellSpacing="1"
                                    CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False" PageSize="3" DataKeyField="TCName">
                                    <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                                    <ItemStyle CssClass="tdganjil"></ItemStyle>
                                    <HeaderStyle HorizontalAlign="Center" CssClass="tdjudul"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="NO">
                                            <HeaderStyle Width="5%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                            <HeaderStyle Width="7%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="PERIKSA">
                                            <HeaderStyle Width="8%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkTCCheck2" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                                    runat="server"></asp:CheckBox>
                                                <asp:Label ID="lblVTC2Checked" runat="server" ForeColor="Red">Harus diperiksa</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY">
                                            <HeaderStyle Width="10%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="TGL JANJI" Visible="true">
                                            <HeaderStyle Width="15%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <uc1:ValidDate id="uscTCPromiseDate2" runat="server"></uc1:ValidDate>
                                                <asp:Label ID="lblVPromiseDate2" runat="server" ForeColor="Red">Tgl Janji harus > hari ini</asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="CATATAN">
                                            <HeaderStyle Width="30%"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                                    Width="95%" CssClass="inptype">
                                                </asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <table border="0" cellspacing="0" cellpadding="0" width="95%">
                        <tr valign="bottom">
                            <td align="left">
                                <asp:ImageButton ID="imbSave" runat="server" CausesValidation="True" ImageUrl="../../../Images/ButtonSave.gif">
                                </asp:ImageButton>&nbsp;
                                <asp:ImageButton ID="imbCancel" runat="server" CausesValidation="False" ImageUrl="../../../Images/ButtonCancel.gif">
                                </asp:ImageButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Width="100%" HorizontalAlign="center" Visible="False">
        <table border="0" cellspacing="0" cellpadding="0" width="95%">
            <tr class="trtopi">
                <td class="tdtopi" align="center">
                    PROSES LOAN ACTIVATION
                </td>
            </tr>
        </table>
        <table class="tablegrid" border="0" cellspacing="1" cellpadding="3" width="95%">
            <tr align="center">
                <td class="tdgenap" height="50" width="100%">
                    <asp:Label ID="lblMessageConfirm" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table border="0" cellspacing="0" cellpadding="0" width="95%">
            <tr valign="bottom">
                <td align="left">
                    <asp:ImageButton ID="imgOK" runat="server" CausesValidation="True" ImageUrl="../../../Images/ButtonOK.gif">
                    </asp:ImageButton>&nbsp;
                    <asp:ImageButton ID="imgCancel" runat="server" CausesValidation="False" ImageUrl="../../../Images/ButtonCancel.gif">
                    </asp:ImageButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
