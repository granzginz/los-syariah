﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.IO
Imports System.Collections.Generic
#End Region


Public Class CreditAssesment_002
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTab
    Protected WithEvents CreditAssessmentTab1 As CreditAssessmentTab
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat

    Private oAssetDataController As New AssetDataController
    Private oController As New CreditAssesmentController
    Private oCustomerController As New CustomerController
     
 Protected WithEvents TxtSaldoAwal As ucNumberFormat
    Protected WithEvents TxtJumlahPemasukan As ucNumberFormat
    Protected WithEvents TxtSaldoAkhir As ucNumberFormat
    Protected WithEvents TxtJumlahPengeluran As ucNumberFormat
    Protected WithEvents TxtSaldoRataRata As ucNumberFormat
    Private time As String
    Private m_Appcontroller As New ApplicationController
#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property

    Private Property CSResult_Temp() As String
        Get
            Return CType(ViewState("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CSResult_Temp") = Value
        End Set
    End Property
    Private Property TotalDebet() As Decimal
        Get
            Return (CType(ViewState("TotalDebet"), Decimal))
        End Get
        Set(ByVal TotalDebet As Decimal)
            ViewState("TotalDebet") = TotalDebet
        End Set
    End Property
    Private Property TotalKredit() As Decimal
        Get
            Return (CType(ViewState("TotalKredit"), Decimal))
        End Get
        Set(ByVal TotalKredit As Decimal)
            ViewState("TotalKredit") = TotalKredit
        End Set
    End Property
    Private Property myDataTable() As DataTable
        Get
            Return CType(ViewState("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("myDataTable") = Value
        End Set

    End Property
    Private Property SchemeID() As String
        Get
            Return CType(ViewState("SchemeID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SchemeID") = Value
        End Set
    End Property
    Private Property NTF() As String
        Get
            Return CType(ViewState("NTF"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NTF") = Value
        End Set
    End Property
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'Modify by Wira 20171019
        If Request("ActivityDateStart") = Nothing Then
            time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
            Me.ActivityDateStart = Me.BusinessDate + " " + time
        Else
            Me.ActivityDateStart = Request("ActivityDateStart")
        End If

        If Not Page.IsPostBack Then
            btnSave.Attributes.Add("OnClick", "return checksubmit();")
            'Me.SchemeID = Request("Scheme")
            Me.NTF = CDbl(Request("NTF"))
            Me.FormID = "CREDITASSESMENT"
            Me.ApplicationID = ""
            If Request("appid") <> "" Then
                Me.ApplicationID = Request("appid")
                Me.SchemeID = oController.getApprovalSchemeID(GetConnectionString, Me.ApplicationID)
            End If
            'BranchID = Replace(Me.sesBranchId, "'", "")
            Bind()
            'getCreditAssesment()
            BindBankType()
            InitialPageLoad()


            getCreditAssesment()
          
            'End If
        End If
      
    End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID 
        End With

        CreditAssessmentTab1.ApplicationID = Me.ApplicationID
        CreditAssessmentTab1.selectedTab("Rekening")
        CreditAssessmentTab1.setLink(True)
         
        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With
 
    End Sub

    Sub getCreditAssesment()
        Dim oPar As New Parameter.CreditAssesment

        With oPar
            .strConnection = GetConnectionString() 
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getCreditAssesment(oPar)
        btnProceed.Visible = oPar.RequestBy.Trim <> ""

        With oPar 
            cmbAccountType.SelectedIndex = cmbAccountType.Items.IndexOf(cmbAccountType.Items.FindByValue(.JenisRekening))
            TxtSaldoAwal.Text = .SaldoAwal.ToString
            TxtSaldoAkhir.Text = .SaldoAkhir.ToString
            TxtSaldoRataRata.Text = .SaldoRataRata.ToString
            TxtJumlahPemasukan.Text = .JumlahPemasukan.ToString
            TxtJumlahPengeluran.Text = .JumlahPengeluaran.ToString
            TxtJumlahhariTransaksi.Text = .JumlahHariTransaksi.ToString
            TxtKesimpulan.Text = .NotesBank

            cboApprovedBy.SelectedIndex = cboApprovedBy.Items.IndexOf(cboApprovedBy.Items.FindByValue(.RequestBy))
            'txtAgreementNo.Text = .AgreementNo
        End With
    End Sub
     

    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
       
        Dim oPar As New Parameter.CreditAssesment
        Dim errMsg As String = ""
        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID

                .JenisRekening = cmbAccountType.SelectedValue

                .SaldoRataRata = CDec(TxtSaldoRataRata.Text.Trim)
                .SaldoAwal = CDec(TxtSaldoAwal.Text.Trim)
                .JumlahPemasukan = CDec(TxtJumlahPemasukan.Text.Trim)
                .JumlahPengeluaran = CDec(TxtJumlahPengeluran.Text.Trim)
                .SaldoAkhir = CDec(TxtSaldoAkhir.Text.Trim)
                .JumlahHariTransaksi = CInt(TxtJumlahhariTransaksi.Text.Trim)
                .NotesBank = TxtKesimpulan.Text
                .RequestBy = cboApprovedBy.SelectedValue
                .AgreementNo = "-" 'txtAgreementNo.Text.Trim
                .SchemeID = Me.SchemeID
                .RefundAmount = Me.NTF
                .LoginId = Me.Loginid
                .BusinessDate = Me.BusinessDate
                .part = "R"
            End With
            If cboApprovedBy.SelectedIndex > 0 Then
                'If txtAgreementNo.Text.Trim = "" Then
                '    lblMsgAgreementNo.Text = "AgreementNo is required!"
                '    lblMsgAgreementNo.Visible = True
                '    Exit Sub
                'End If
            Else
                lblMessage.Text = "Harap Pilih Akan disetujui Oleh"

            End If

            oController.CreditAssesmentSave(oPar)
            If errMsg = String.Empty Or errMsg = "" Then
                ShowMessage(lblMessage, "Data saved!", False)
            Else
                ShowMessage(lblMessage, errMsg, True)
            End If
            getCreditAssesment()
           
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        'End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Response.Redirect("creditAssesmentList.aspx")
    End Sub
    Private Sub btnProceed_Click(sender As Object, e As System.EventArgs) Handles btnProceed.Click
        Dim ocustom As New Parameter.CreditAssesment

        With ocustom
            .strConnection = Me.GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.sesBranchId.Replace("'", "")
            .BusinessDate = Me.BusinessDate

            .SchemeID = Me.SchemeID
            .RefundAmount = Me.NTF
            .LoginId = Me.Loginid

            .RequestBy = cboApprovedBy.SelectedValue
            .NotesBank = TxtKesimpulan.Text

            oController.Proceed(ocustom)
        End With
        'Modify by Wira 20171019
        ActivityLog()

        Response.Redirect(String.Format("creditAssesmentList.aspx?result=Applikasi {0} sudah di Proceed.", Me.ApplicationID))
        'ShowMessage(lblMessage, "Applikasi sudah di proses", False)
        'btnProceed.Visible = False
    End Sub
#Region "Bind"
    Sub Bind()
        Dim oData As New DataTable
        oData = Get_UserApproval(Me.SchemeID, Replace(Me.sesBranchId, "'", ""), Me.NTF)
        cboApprovedBy.DataSource = oData.DefaultView
        cboApprovedBy.DataTextField = "Name"
        cboApprovedBy.DataValueField = "ID"
        cboApprovedBy.DataBind()
        cboApprovedBy.Items.Insert(0, "Select One")
        cboApprovedBy.Items(0).Value = "0"

    End Sub
#End Region
    Private Sub BindBankType()
        Dim oDataTable As DataTable = New DataTable
        Dim oRow As DataRow
        Dim i As Integer
        Dim splitListData() As String
        Dim splitRow() As String
        Dim strList As String
        oDataTable.Columns.Add("ID", System.Type.GetType("System.String"))
        oDataTable.Columns.Add("Description", System.Type.GetType("System.String"))
        strList = "B,BANK-C,CASH"
        oRow = oDataTable.NewRow()
        oRow("ID") = ""
        oRow("Description") = ""

        splitListData = Split(strList, "-")
        For i = 0 To UBound(splitListData)
            splitRow = Split(splitListData(i), ",")
            oRow("ID") = splitRow(0)
            oRow("Description") = splitRow(1)
            oDataTable.Rows.Add(oRow)
            oRow = oDataTable.NewRow()
        Next

        cmbAccountType.DataValueField = "ID"
        cmbAccountType.DataTextField = "Description"
        cmbAccountType.DataSource = oDataTable
        cmbAccountType.DataBind()
        cmbAccountType.Dispose()
        cmbAccountType.Items.Insert(0, "Select One")
        cmbAccountType.Items(0).Value = "0"
        cmbAccountType.SelectedIndex = 0
    End Sub

#Region "Activity Log"
    Sub ActivityLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Application
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.BranchID, "'", "").ToString
        oApplication.ApplicationID = Me.ApplicationID.Trim
        oApplication.ActivityType = "CAS"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 13

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Application

        oReturn = m_Appcontroller.ActivityLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub
#End Region
End Class