﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class FinancialModalKerjaData
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabModalKerja
    Protected WithEvents UcBiayaAdmin As ucNumberFormat
    Protected WithEvents UcBiayaProvisi As ucNumberFormat
    Protected WithEvents UcBiayaSurvey As ucNumberFormat
    Protected WithEvents UcDiscountCharges As ucNumberFormat
    Protected WithEvents UcBiayaNotaris As ucNumberFormat
    Protected WithEvents UcBiayaHandling As ucNumberFormat
    Protected WithEvents UcAsuransiKredit As ucNumberFormat
    Protected WithEvents UcBiayaPolis As ucNumberFormat
    Protected WithEvents ucViewCustomerFacility1 As ucViewCustomerFacility
    Protected WithEvents cmbInsuranceComBranch As UcInsuranceBranchName

#End Region

#Region "Constanta"
    Dim style As String = "ACCACQ"
    Private oApplication As New Parameter.Application
    Private oFactoringInvoice As New Parameter.ModalKerjaInvoice
    Private m_ControllerApp As New ApplicationController
    Private m_controller As New ModalKerjaInvoiceController
    Private m_controllerCustomer As New CustomerFacilityController
    Private oController As New NewAppInsuranceByCompanyController
    Private m_controllerUc As New DataUserControlController
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo")
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Property MetodePembayaran() As String
        Get
            Return ViewState("MetodePembayaran").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("MetodePembayaran") = Value
        End Set
    End Property
    Property ApplicationData() As DataTable
        Get
            Return ViewState("ApplicationData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ApplicationData") = Value
        End Set
    End Property
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property

#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("appid")
            hdfApplicationID.Value = Me.ApplicationID
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If

                .initControls("OnNewApplication")

                Me.BranchID = .BranchID
                Me.CustomerID = .CustomerID
                Me.ProductOfferingID = .ProductOfferingID
                Me.SupplierID = .SupplierID
            End With

            'With cmbInsuranceComBranch
            '    .FillRequired = False
            'End With
            'cmbInsuranceComBranch.loadInsBranch(False)
            'cmbInsuranceComBranch.Enabled = True

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            Mode = Request("Page")

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial")
            ucApplicationTab1.setLink()

            Me.InvoiceSeqNo = 0
            Me.InvoiceSeqNo = Request("invseqno")

            LoadingcboRateType()
            LoadingInsuranceComBranch()

            BindDataEdit()

        End If
    End Sub
#End Region
    Private Sub cboInsuranceComBranch_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboInsuranceComBranch.SelectedIndexChanged
        FillCbo_("tblInsuranceComBranch", cboRateType)
    End Sub

    Private Sub LoadingcboRateType()
        FillCbo_("Load", cboRateType)
    End Sub

    Protected Sub FillCbo_(ByVal table As String, cboRateType As DropDownList)
        Dim dtEntity As DataTable = Nothing
        Dim oInsurance As New Parameter.NewAppInsuranceByCompany
        oInsurance.strConnection = GetConnectionString()
        oInsurance.Table = table
        oInsurance.ApplicationID = Me.ApplicationID
        oInsurance.InsuranceComBranchID = cboInsuranceComBranch.SelectedValue
        oInsurance = oController.GetInsuranceComBranch(oInsurance)

        If Not oInsurance Is Nothing Then
            dtEntity = oInsurance.ListData
        End If

        cboRateType.DataSource = dtEntity.DefaultView
        cboRateType.DataTextField = "Description"
        cboRateType.DataValueField = "ID"
        cboRateType.DataBind()
        cboRateType.Items.Insert(0, "Select One")
        cboRateType.Items(0).Value = ""
    End Sub
    Private Sub LoadingInsuranceComBranch()
        With cboInsuranceComBranch
            .DataSource = m_controllerUc.GetInsuranceBranchName(GetConnectionString, Me.sesBranchId)
            .DataValueField = "ID"
            .DataTextField = "Name"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
    End Sub

    Private Sub loadDataFasilitas()
        With ucViewCustomerFacility1
            .FacilityNo = Me.FacilityNo
            .CustomerID = Me.CustomerID
            .LoadCustomerFacility()
        End With
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub

    Public Function getPPNPercent() As Decimal
        Dim generalSet As New Parameter.GeneralSetting
        Dim conGeneral As New GeneralSettingController

        With generalSet
            .strConnection = GetConnectionString()
            .GSID = "PPNFACTORING"
        End With

        generalSet = conGeneral.GetGeneralSettingByID(generalSet)

        If generalSet.ListData.Rows.Count > 0 Then
            Return CDec(generalSet.ListData.Rows(0).Item("GSValue").ToString.Trim)
        Else
            Return 0
        End If
    End Function

    Private Sub BindDataEdit()
        Dim oNewAppInsuranceByCompany As New Parameter.NewAppInsuranceByCompany
        Dim oData, fData As New DataTable
        Dim oApp = New Parameter.FactoringInvoice
        Dim AdminFee, NotaryFee, ProvisionFee, SurveyFee, FactoringDiscountCharges As Decimal
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication.ApplicationID = Me.ApplicationID
        oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
            ApplicationData = oData
        End If
        Dim oRow As DataRow = ApplicationData.Rows(0)

        Me.FacilityNo = oRow("NoFasilitas").ToString

        loadDataFasilitas()

        oFactoringInvoice.strConnection = GetConnectionString()
        oFactoringInvoice.BranchId = Me.BranchID
        oFactoringInvoice.ApplicationID = Me.ApplicationID

        oFactoringInvoice = m_controller.GetInvoiceList(oFactoringInvoice)

        ItemData = oFactoringInvoice.Listdata

        lblTanggalPencairan.Text = Format(IIf(String.IsNullOrEmpty(oRow("FactoringPencairanDate").ToString), "", oRow("FactoringPencairanDate")), "dd/MM/yyyy")

        Dim totalPencairanInvoice As Object = ItemData.Compute("SUM(InvoiceAmount)", String.Empty)
        Dim totalPembiayaan As Object = ItemData.Compute("SUM(TotalPembiayaan)", String.Empty)
        Dim totalInvoice As Object = ItemData.Rows.Count

        Dim totalPlafond As Decimal = ucViewCustomerFacility1.FasilitasInfo.FasilitasAmount
        Dim ppnPercent As Decimal = getPPNPercent()
        Dim ppn, totalbiaya, biayaprovisi, asuransi, handlingfee, biayapolis, handlingfeeamount As Decimal

        'hdfTotalInvoice.Value = Convert.ToDecimal(totalPembiayaan).ToString
        hdfTotalInvoice.Value = Convert.ToDecimal(totalPencairanInvoice).ToString

        hdfTotalPlafond.Value = totalPlafond.ToString

        'hdfPPN.Value = ppnPercent.ToString
        NotaryFee = IIf(IsDBNull(oRow("NotaryFee")), ucViewCustomerFacility1.FasilitasInfo.NotaryFee, oRow("NotaryFee"))
        AdminFee = IIf(IsDBNull(oRow("AdminFee")), ucViewCustomerFacility1.FasilitasInfo.AdminFee, oRow("AdminFee"))
        ProvisionFee = IIf(IsDBNull(oRow("ProvisionFee")), ucViewCustomerFacility1.FasilitasInfo.ProvisionFee, oRow("ProvisionFee"))
        handlingfee = IIf(IsDBNull(oRow("HandlingFee")), ucViewCustomerFacility1.FasilitasInfo.HandlingFee, oRow("HandlingFee"))
        SurveyFee = oRow("SurveyFee")
        FactoringDiscountCharges = oRow("FactoringDiscountCharges")

        'lblTotalInvoice.Text = FormatNumber(Convert.ToDecimal(totalPembiayaan), 0)
        lblTotalInvoice.Text = FormatNumber(Convert.ToDecimal(totalPencairanInvoice), 0)

        lblInvoiceCount.Text = totalInvoice.ToString

        biayaprovisi = FormatNumber(ProvisionFee, 0)
        lblBiayaProvisi.Text = FormatNumber(ProvisionFee, 0)
        UcBiayaProvisi.Text = FormatNumber(ProvisionFee / totalPembiayaan * 100, 2)

        lblBiayaHandling.Text = FormatNumber(handlingfee, 0)
        UcBiayaHandling.Text = FormatNumber(handlingfee / totalPembiayaan * 100, 2)

        MetodePembayaran = oRow("FactoringPaymentMethod").ToString

        With oNewAppInsuranceByCompany
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString()
        End With
        oNewAppInsuranceByCompany = oController.GetInsuranceEntryFactoring(oNewAppInsuranceByCompany)
        'cmbInsuranceComBranch.SelectedValue = oNewAppInsuranceByCompany.InsuranceComBranchID
        cboInsuranceComBranch.SelectedValue = oNewAppInsuranceByCompany.InsuranceComBranchID
        If cboInsuranceComBranch.SelectedValue = "" Then
            FillCbo_("Load", cboRateType)
        Else
            FillCbo_("tblInsuranceComBranch", cboRateType)
        End If
        cboRateType.SelectedValue = oNewAppInsuranceByCompany.InsRateCategory

        asuransi = Convert.ToDecimal((oNewAppInsuranceByCompany.Rate / 100) * totalPembiayaan)
        lblAsuransiKredit.Text = FormatNumber(asuransi, 0)
        UcAsuransiKredit.Text = FormatNumber(Convert.ToDecimal(oNewAppInsuranceByCompany.Rate), 0)
        biayapolis = Convert.ToDecimal(oRow("BiayaPolis"))
        UcBiayaPolis.Text = FormatNumber(biayapolis, 0)

        UcBiayaAdmin.Text = FormatNumber(AdminFee, 0)

        UcBiayaNotaris.Text = FormatNumber(NotaryFee, 0)
        UcBiayaSurvey.Text = FormatNumber(SurveyFee, 0)
        UcDiscountCharges.Text = FormatNumber(FactoringDiscountCharges, 0)

        totalbiaya = NotaryFee + AdminFee + biayaprovisi + SurveyFee + asuransi + handlingfee + biayapolis - FactoringDiscountCharges


        lblTotalBiaya.Text = FormatNumber(totalbiaya, 0)
        lblJumlahPencairan.Text = FormatNumber(totalPembiayaan, 0)

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim oCustomclass As New Parameter.Application
        Dim AdminFee, NotaryFee, ProvisionFee, SurveyFee, FactoringDiscountCharges, HandlingFee, PolisFee, PolisAmount, FactoringPolis,
        totalInterest, totalbiaya, ppn, ppnPercent, totalPembiayaan, installmentAmount, HandlingFeeAmount As Decimal

        totalPembiayaan = CDec(hdfTotalInvoice.Value)
        NotaryFee = CDec(UcBiayaNotaris.Text)
        AdminFee = CDec(UcBiayaAdmin.Text)
        ProvisionFee = CDec(UcBiayaProvisi.Text) / 100 * totalPembiayaan
        SurveyFee = CDec(UcBiayaSurvey.Text)
        FactoringDiscountCharges = CDec(UcDiscountCharges.Text)
        'ppnPercent = CDec(hdfPPN.Value)

        FactoringPolis = CDec(UcAsuransiKredit.Text)
        PolisAmount = (FactoringPolis / 100) * totalPembiayaan
        HandlingFee = (CDec(UcBiayaHandling.Text) / 100) * totalPembiayaan
        PolisFee = CDec(UcBiayaPolis.Text)
        HandlingFeeAmount = (HandlingFee / 100) * totalPembiayaan

        totalbiaya = HandlingFee + NotaryFee + AdminFee + ProvisionFee + SurveyFee + PolisFee - FactoringDiscountCharges


        'ppn = (totalInterest + totalbiaya) * ppnPercent / 100

        If (MetodePembayaran = "D") Then
            installmentAmount = totalPembiayaan - totalInterest
        Else
            installmentAmount = totalPembiayaan
        End If

        With oCustomclass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
            .InstallmentAmount = installmentAmount
            .FactoringPPNAmount = ppn
            .FactoringDiscountCharges = FactoringDiscountCharges
            .AdminFee = AdminFee
            .NotaryFee = NotaryFee
            .ProvisionFee = ProvisionFee
            .SurveyFee = SurveyFee
            .HandlingFee = HandlingFee
            .BiayaPolis = PolisFee
            .FactoringPolis = FactoringPolis
            .FactoringPolisAmount = PolisAmount
            .FactoringInsuranceComID = cboInsuranceComBranch.SelectedValue
            .FactoringPPN = ppnPercent
            .FactoringPPNAmount = ppn
            .InsRateCategory = cboRateType.SelectedValue
        End With
        Try
            Dim rtnMessage As String = m_controller.FactoringApplicationFinancialSave(oCustomclass)
            If rtnMessage = "" Then
                ShowMessage(lblMessage, "Data saved!", False)
                BindDataEdit()
            Else
                ShowMessage(lblMessage, rtnMessage, True)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Private Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click
        Dim ocustom As New Parameter.HasilSurvey
        Dim oController As New HasilSurveyController
        With ocustom
            .strConnection = Me.GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.sesBranchId.Replace("'", "")
            .BusinessDate = Me.BusinessDate
            oController.Proceed(ocustom)
        End With

        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub

End Class