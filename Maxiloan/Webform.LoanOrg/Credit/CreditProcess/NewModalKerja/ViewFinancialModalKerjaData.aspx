﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewFinancialModalKerjaData.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewFinancialModalKerjaData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplicationMK.ascx" TagName="ucViewApplicationMK"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabModalKerja.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerFacility.ascx" TagName="ucViewCustomerFacility" 
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Financial Data</title>
   
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
       
        function hitungUlang()
        {
            var UcPlafond = document.getElementById('UcPlafond_txtNumber').value;
            var UcBiayaAdmin = document.getElementById('UcBiayaAdmin_txtNumber').value;
            //var UcBiayaProvisi = document.getElementById('UcBiayaProvisi_txtNumber').value;
            var UcBiayaSurvey = document.getElementById('UcBiayaSurvey_txtNumber').value;
            var UcBiayaNotaris = document.getElementById('UcBiayaNotaris_txtNumber').value;
            
            var BiayaAdmin = parseInt(UcBiayaAdmin.replace(/\s*,\s*/g, ''));
            //var BiayaProvisi = parseInt(UcBiayaProvisi.replace(/\s*,\s*/g, ''));
            var BiayaSurvey = parseInt(UcBiayaSurvey.replace(/\s*,\s*/g, ''));
            var BiayaNotaris = parseInt(UcBiayaNotaris.replace(/\s*,\s*/g, ''));
            var Plafond = parseInt(UcPlafond.replace(/\s*,\s*/g, ''));

            //var totalbiaya = BiayaAdmin + BiayaNotaris + BiayaProvisi + BiayaSurvey;
            var totalbiaya = BiayaAdmin + BiayaNotaris + BiayaSurvey;

            $('#lblTotalBiaya').html(number_format(totalbiaya, 0));
        }

        function hitungJangkaWaktu()
        {
            var _tglCair = document.getElementById('hdfTanggalPencairan').value;
            var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);

            var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);

            var _invoiceDueDate = document.getElementById('txtInvoiceDueDate').value;
            dateString = _invoiceDueDate.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var invoiceDueDate = new Date(dateString[3], dateString[2] - 1, dateString[1]);

            var days = daysBetween(tglCair, invoiceDueDate);
            return days;
        }

        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnlSupplier">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnlSupplier" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="form_single">
                    <asp:HiddenField runat="server" ID="hdfApplicationID" />
                    <h4>
                        VIEW FINANCIAL DATA</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc3:ucViewApplicationMK id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <uc1:ucViewCustomerFacility id="ucViewCustomerFacility1" runat="server" />
            <asp:HiddenField runat="server" ID="HiddenField1" />
            <div  class="form_box_hide">
                <div class="form_box">
                 <div class="form_left">
                    <label class="">Plafond</label>
                     <asp:Label runat="server" ID="UcPlafond"  CssClass="regular_text" ></asp:Label>
                </div>
                <div class="form_right">
                    <label  runat="server" id="lblFasilitasType">
                        Type Fasilitas
                    </label>
                    <asp:Label runat="server" ID="lbllistFasilitasType"  CssClass="regular_text" ></asp:Label>
                    <asp:DropDownList runat="server" ID="listFasilitasType" Visible="false">
                        <asp:ListItem Value="R">Revolving</asp:ListItem>
                        <asp:ListItem Value="N">Non Revolving</asp:ListItem>
                    </asp:DropDownList>
                </div>    
            </div> 
            <div class="form_box">
                <div class="form_left">
                        <label class="">Suku Bunga</label>
                        <asp:Label runat="server" ID="UcSukuBunga"  CssClass=" regular_text" ></asp:Label> %
                </div>
                <div class="form_right">
                         <label>Skema Angsuran</label>
                        <asp:Label runat="server" ID="lblInstScheme"  CssClass="regular_text" ></asp:Label>
                        <asp:DropDownList ID="cboInstScheme" runat="server" Visible="false">
                            <asp:ListItem Value="DL">Demand Loan</asp:ListItem>
                            <asp:ListItem Value="RK">PRK</asp:ListItem>
                        </asp:DropDownList>
                    </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label class="">Total Pembiayaan</label>
                    <asp:Label runat="server" ID="UcTtlPembiayaan"  CssClass="regular_text" ></asp:Label>
                </div>
                <div class="form_right">
                    <label class="">Masa Berlaku</label>
                    <asp:Label runat="server" ID="UcMasaBerlaku"  CssClass=" regular_text" ></asp:Label> Months
                </div>
            </div>
            </div>
            
            <div class="form_title">
                <div class="form_single">
                    <h4>ENTRI FINANCIAL DATA</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Tanggal Pencairan</label>
                    <asp:Label runat="server" ID="lblTanggalPencairan" ></asp:Label>    
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Total Invoice</label>
                     <div style="margin-left:-8px;display:inline">
                         <asp:HiddenField runat="server" ID="hdfTotalInvoice" />
                        <asp:Label runat="server" ID="lblTotalInvoice"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
                <div class="form_right">
                    <label>Dari invoice sebanyak</label>
                    <asp:Label ID="lblInvoiceCount" runat="server" />
                </div>
            </div>  
             <div class="form_box">
                 <div class="form_left">
                    <label class="">Jumlah Pencairan</label>
                     <div style="margin-left:-8px;display:inline">
                         <asp:HiddenField runat="server" ID="hdfTotalPencairan" />
                        <asp:Label runat="server" ID="lblJumlahPencairan"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div> 

            <div class="form_box">
                 <div class="form_left">
                    <h4>Biaya-biaya</h4>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Perusahaan Asuransi</label>
                     <asp:Label runat="server" ID="LblFocusMaskAssIDName"  CssClass="regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Tipe Rate Asuransi</label>
                     <asp:Label runat="server" ID="lblRateTypeInsuranceDesc"  CssClass="regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Asuransi Pembiayaan</label>
                    <asp:Label runat="server" ID="lblAsuransiKredit"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Polis</label>
                    <asp:Label runat="server" ID="lblBiayaPolis"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Admin</label>
                    <asp:Label runat="server" ID="UcBiayaAdmin"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Provisi</label>
                     <%--<asp:Label runat="server" ID="UcBiayaProvisiPercent"  CssClass=" regular_text" ></asp:Label> %--%>
                     <asp:Label runat="server" ID="UcBiayaProvisi"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Handling</label>
                     <%--<asp:Label runat="server" ID="UcHandlingFeePercent"  CssClass=" regular_text" ></asp:Label> %--%>
                    <asp:Label runat="server" ID="UcHandlingFee"  CssClass="numberAlign2 regular_text" ></asp:Label> 
                </div>
            </div> 
             
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Notaris</label>
                     <asp:Label runat="server" ID="UcBiayaNotaris"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Survey</label>
                     <asp:Label runat="server" ID="UcBiayaSurvey"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Discount Charges</label>
                    <asp:Label runat="server" ID="UcDiscountCharges"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Total Biaya</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblTotalBiaya"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div>
            <div class="tab_container_form_space"><br /><br /><br /><br /></div>
                <div class="tab_container_button">
                    <div class="form_button">
                        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                    </div>
                </div>
        </ContentTemplate>
        
    </asp:UpdatePanel>
    </form>
</body>
</html>
