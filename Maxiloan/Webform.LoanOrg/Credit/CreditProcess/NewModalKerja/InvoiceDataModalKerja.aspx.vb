﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic
Imports DocumentFormat.OpenXml.Spreadsheet
Imports DocumentFormat.OpenXml.Packaging


#End Region

Public Class InvoiceDataModalKerja
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    'Protected WithEvents ucApplicationTab1 As ucApplicationTabFactoring
    Protected WithEvents ucApplicationTab1 As ucApplicationTabModalKerja
    Protected WithEvents ucInvoiceAmount As ucNumberFormat
    Protected WithEvents UcTagihanDibiayai As ucNumberFormat
    Protected WithEvents ucRentensiPercent As ucNumberFormat
    Protected WithEvents ucViewCustomerFacility1 As ucViewCustomerFacility
    Protected WithEvents UcBankAccount1 As UcBankAccount
#End Region

#Region "Constanta"
    Dim style As String = "ACCACQ"
    Private oApplication As New Parameter.Application
    Private oModalKerjaInvoice As New Parameter.ModalKerjaInvoice
    Private m_ControllerApp As New ApplicationController
    Private m_controller As New ModalKerjaInvoiceController
    Private strFileTemp As String
    Private FileName As String
    Private FileDirectory As String
    Private FileType As String
    Private TextFile As String
    Private myCmd As New SqlCommand
    Private myAdapter As New SqlDataAdapter
    Private myDS As New DataSet
    Private objCon As SqlConnection
    Protected WithEvents ucDateCE As ucDateCE
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
    Property FacilityMaturityDate() As Date
        Get
            Return ViewState("FacilityMaturityDate")
        End Get
        Set(ByVal Value As Date)
            ViewState("FacilityMaturityDate") = Value
        End Set
    End Property
    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo")
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property Retensi() As Decimal
        Get
            Return ViewState("Retensi")
        End Get
        Set(ByVal Value As Decimal)
            ViewState("Retensi") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Property ApplicationData() As DataTable
        Get
            Return ViewState("ApplicationData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ApplicationData") = Value
        End Set
    End Property
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property
    Private Property TotPRAmount() As Double
        Get
            Return (CType(ViewState("TotPRAmount"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("TotPRAmount") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("appid")
            hdfApplicationID.Value = Me.ApplicationID
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If

                .initControls("OnNewApplication")

                Me.BranchID = .BranchID
                Me.CustomerID = .CustomerID
                Me.ProductOfferingID = .ProductOfferingID
                Me.SupplierID = .SupplierID
            End With

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            Mode = Request("Page")

            pnlInputInvoice.Visible = False
            pnlListInvoice.Visible = False
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Invoice")
            ucApplicationTab1.setLink()

            Me.InvoiceSeqNo = 0
            Me.InvoiceSeqNo = Request("invseqno")
            BindDataEdit()
            loadDataInvoice()

            Select Case Mode
                Case "Edit"
                    pnlListInvoice.Visible = True
                Case "Detail"
                    hdfInvoiceSeqNo.Value = Request("invseqno")
                    pnlInputInvoice.Visible = True
                    ClearInvoiceForm()
                    If (Me.InvoiceSeqNo > 0) Then
                        BindInvoiceForm()
                    Else
                        UcBankAccount1.BindBankAccount()
                    End If
            End Select
            UploadAnuitas.Visible = False
        End If

    End Sub
#End Region

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub

    Private Sub BindDataEdit()
        Dim oData As New DataTable
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
            ApplicationData = oData
        End If
        Dim oRow As DataRow = ApplicationData.Rows(0)

        oModalKerjaInvoice.strConnection = GetConnectionString()
        oModalKerjaInvoice.BranchId = Me.BranchID
        oModalKerjaInvoice.ApplicationID = Me.ApplicationID

        oModalKerjaInvoice = m_controller.GetInvoiceList(oModalKerjaInvoice)

        ItemData = oModalKerjaInvoice.Listdata

        dtgInvoice.DataSource = ItemData.DefaultView
        dtgInvoice.DataBind()

        lblTanggalPencairan.Text = CDate(IIf(String.IsNullOrEmpty(oRow("FactoringPencairanDate")), "1900-01-01", oRow("FactoringPencairanDate"))).ToShortDateString
        Me.FacilityNo = oRow("NoFasilitas").ToString.Trim
        loadDataFasilitas()
    End Sub

    Private Sub loadDataFasilitas()
        With ucViewCustomerFacility1
            .FacilityNo = Me.FacilityNo
            .CustomerID = Me.CustomerID
            .LoadCustomerFacility()
            Me.FacilityMaturityDate = .FacilityMaturityDate
        End With
    End Sub

    Private Sub loadDataInvoice()
        oModalKerjaInvoice.strConnection = GetConnectionString()
        oModalKerjaInvoice.ApplicationID = Me.ApplicationID
        oModalKerjaInvoice = m_controller.GetInvoiceList2(oModalKerjaInvoice)

        txtInvoiceNoBatch.Text = oModalKerjaInvoice.InvoiceNo
        Me.InvoiceNo = oModalKerjaInvoice.InvoiceNo
        If oModalKerjaInvoice.IsFinal = True Then cbAnuitas.Checked = True Else cbAnuitas.Checked = False
        If oModalKerjaInvoice.InvoiceDueDate = Nothing Then
            txtduedate.Text = ""
        Else
            txtduedate.Text = oModalKerjaInvoice.InvoiceDueDate.ToString("dd/MM/yyyy")
        End If
        hdfInvoiceSeqNoH.Value = oModalKerjaInvoice.InvoiceSeqNo
    End Sub


    Private Sub BindInvoiceForm()
        Dim oRow As DataRow = ApplicationData.Rows(0)
        oModalKerjaInvoice.strConnection = GetConnectionString()
        oModalKerjaInvoice.BranchId = Me.BranchID
        oModalKerjaInvoice.ApplicationID = Me.ApplicationID
        oModalKerjaInvoice.InvoiceSeqNo = Me.InvoiceSeqNo
        oModalKerjaInvoice = m_controller.GetInvoiceDetail(oModalKerjaInvoice)

        hdfInvoiceSeqNo.Value = oModalKerjaInvoice.InvoiceSeqNo
        txtInvoiceNo.Text = oModalKerjaInvoice.InvoiceNo
        txtInvoiceDate.Text = Convert.ToDateTime(oModalKerjaInvoice.InvoiceDate).ToString("dd/MM/yyyy")
        txtInvoiceDueDate.Text = Convert.ToDateTime(oModalKerjaInvoice.InvoiceDueDate).ToString("dd/MM/yyyy")
        ucInvoiceAmount.Text = FormatNumber(oModalKerjaInvoice.InvoiceAmount, 0)
        lblRentensi.Text = FormatNumber(oModalKerjaInvoice.RetensiAmount, 0)
        hdfRentensiPercent.Value = oRow("FactoringRetensi").ToString
        Retensi = oRow("FactoringRetensi")
        ucRentensiPercent.Text = FormatNumber(oRow("FactoringRetensi").ToString, 2)
        'lblTagihanYangDibiayai.Text = FormatNumber(oModalKerjaInvoice.TotalPembiayaan, 0)
        'lblTagihanYangDibiayai.Text = FormatNumber(oRow("FactoringPiutangDibiayai").ToString, 2) & " %"
        UcTagihanDibiayai.Text = 0
        lblBungaPercent.Text = FormatNumber(oRow("FactoringInterest").ToString, 2) & " %"

        UcBankAccount1.BankBranchId = oModalKerjaInvoice.BankBranchTo.Trim
        UcBankAccount1.BankID = oModalKerjaInvoice.BankNameTo
        UcBankAccount1.BindBankAccount()
        UcBankAccount1.AccountNo = oModalKerjaInvoice.AccountNoTo
        UcBankAccount1.AccountName = oModalKerjaInvoice.AccountNameTo

        Dim tglPencairan As Date = oRow("FactoringPencairanDate")
        'Dim invoiceDueDate As Date = DateTime.ParseExact(oModalKerjaInvoice.InvoiceDueDate, "yyyyMMdd", Nothing)
        Dim invoiceDueDate As Date = oModalKerjaInvoice.InvoiceDueDate
        Dim jangkaWaktu As Long = DateDiff(DateInterval.Day, tglPencairan, invoiceDueDate)
        lblJangkaWaktu.Text = jangkaWaktu.ToString

        hdfTanggalPencairan.Value = CDate(oRow("FactoringPencairanDate")).ToShortDateString
    End Sub

    Private Sub ClearInvoiceForm()
        Dim oRow As DataRow = ApplicationData.Rows(0)
        hdfInvoiceSeqNo.Value = ""
        txtInvoiceNo.Text = ""
        txtInvoiceDate.Text = ""
        txtInvoiceDueDate.Text = ""
        ucInvoiceAmount.Text = ""
        lblRentensi.Text = ""
        hdfRentensiPercent.Value = oRow("FactoringRetensi").ToString
        ucRentensiPercent.Text = FormatNumber(oRow("FactoringRetensi").ToString, 2)

        lblBungaPercent.Text = FormatNumber(oRow("FactoringInterest").ToString, 2) & " %"
        'lblTagihanYangDibiayai.Text = FormatNumber(oRow("FactoringPiutangDibiayai").ToString, 2) & " %"
        UcTagihanDibiayai.Text = 0
        'lblTagihanYangDibiayai.Text = ""

        'hdfTanggalPencairan.Value = Format(oRow("FactoringPencairanDate"), "dd/MM/yyyy")
        hdfTanggalPencairan.Value = CDate(oRow("FactoringPencairanDate")).ToShortDateString
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        saveInvoiceHead()
        Response.Redirect("InvoiceDataModalKerja.aspx?appid=" & Me.ApplicationID & "&page=Detail")
    End Sub

    Private Sub btnCancelInvoice_Click(sender As Object, e As EventArgs) Handles btnCancelInvoice.Click
        Response.Redirect("InvoiceDataModalKerja.aspx?appid=" & Me.ApplicationID & "&page=Edit")
    End Sub

    Private Sub btnSaveInvoice_Click(sender As Object, e As EventArgs) Handles btnSaveInvoice.Click
        Dim oRow As DataRow = ApplicationData.Rows(0)
        Dim oItem As New Parameter.ModalKerjaInvoice
        If hdfInvoiceSeqNo.Value = "" Then hdfInvoiceSeqNo.Value = "0"
        oItem.ApplicationID = Me.ApplicationID
        oItem.BranchId = Me.BranchID
        oItem.InvoiceSeqNo = CDec(hdfInvoiceSeqNo.Value)
        oItem.InvoiceNo = txtInvoiceNo.Text
        oItem.InvoiceAmount = CDec(ucInvoiceAmount.Text)
        oItem.Retensi = CDec(ucRentensiPercent.Text)
        oItem.RetensiAmount = oItem.Retensi * oItem.InvoiceAmount / 100
        oItem.InvoiceDate = IIf(txtInvoiceDate.Text <> "", ConvertDate2(txtInvoiceDate.Text), "").ToString
        oItem.InvoiceDueDate = IIf(txtInvoiceDueDate.Text <> "", ConvertDate2(txtInvoiceDueDate.Text), "").ToString
        oItem.BankBranchTo = CInt(IIf(UcBankAccount1.BankBranchId.Trim = "", "0", UcBankAccount1.BankBranchId.Trim))
        oItem.BankNameTo = UcBankAccount1.BankID
        oItem.AccountNameTo = UcBankAccount1.AccountName
        oItem.AccountNoTo = UcBankAccount1.AccountNo
        oItem.PiutangDibiayai = CDec(UcTagihanDibiayai.Text)

        Dim tglPencairan As Date = oRow("FactoringPencairanDate")
        Dim invoiceDueDate As Date = oItem.InvoiceDueDate
        'Dim PercentPencairan As String = CDec(hdfpersenbiaya.Value)
        Dim PercentPencairan As String = CDec(UcTagihanDibiayai.Text)
        Dim bungaPercent As Decimal = CDec(oRow("FactoringInterest"))
        'oItem.TotalPembiayaan = oItem.InvoiceAmount - oItem.RetensiAmount
        'oItem.TotalPembiayaan = oItem.InvoiceAmount * (PercentPencairan / 100) - oItem.RetensiAmount
        oItem.TotalPembiayaan = oItem.InvoiceAmount * (PercentPencairan / 100)
        oItem.UsrUpd = Me.UserID
        If ValidateSaveInvoice(oItem) = True Then
            Try
                oItem.strConnection = GetConnectionString()
                If oItem.InvoiceSeqNo > 0 Then
                    m_controller.InvoiceSaveEdit(oItem)
                    'saveInvoiceHead()
                Else
                    m_controller.InvoiceSaveAdd(oItem)
                    'saveInvoiceHead()
                End If
                Response.Redirect("InvoiceDataModalKerja.aspx?appid=" & Me.ApplicationID & "&page=Edit")
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

    Private Function ValidateSaveInvoice(item As Parameter.ModalKerjaInvoice) As Boolean
        If item.InvoiceNo.Trim = "" Then
            ShowMessage(lblMessage, "Please fill invoice number", True)
            Return False
        End If
        If item.InvoiceDate > item.InvoiceDueDate Then
            ShowMessage(lblMessage, "Invoice date and invoice due date range not valid", True)
            Return False
        End If
        If item.InvoiceAmount < 1 Then
            ShowMessage(lblMessage, "Invoice amount is not valid", True)
            Return False
        End If
        If (item.Retensi < Retensi) Then
            ShowMessage(lblMessage, "Melewati nilai retensi minimal", True)
            Return False
        End If
        If item.InvoiceDueDate > Me.FacilityMaturityDate Then
            ShowMessage(lblMessage, "Invoice Due Date melebihi Tanggal Akhir Fasilitas", True)
            Return False
        End If
        Return True
    End Function

    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Dim cmd As String() = Split(e.CommandArgument.ToString, ",")
        If e.CommandName.ToLower = "delete" Then
            Me.ApplicationID = cmd(0)
            Me.InvoiceSeqNo = cmd(1)
            Dim oItem As New Parameter.ModalKerjaInvoice
            Try
                oItem.strConnection = GetConnectionString()
                oItem.ApplicationID = Me.ApplicationID
                oItem.InvoiceSeqNo = Me.InvoiceSeqNo
                m_controller.InvoiceDelete(oItem)
                Response.Redirect("InvoiceDataModalKerja.aspx?appid=" & Me.ApplicationID & "&page=Edit")
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try

            Response.Redirect("InvoiceDataModalKerja.aspx?appid=" & Me.ApplicationID & "&page=Edit")
        End If
        If e.CommandName.ToLower = "editinvoice" Then
            Me.ApplicationID = cmd(0)
            Me.InvoiceSeqNo = cmd(1)
            Response.Redirect("InvoiceDataModalKerja.aspx?appid=" & Me.ApplicationID & "&page=Detail" & "&invseqno=" & Me.InvoiceSeqNo.ToString)
        End If
    End Sub

    Private Sub saveInvoiceHead()
        Dim oItem As New Parameter.ModalKerjaInvoice
        If hdfInvoiceSeqNoH.Value = "" Then hdfInvoiceSeqNoH.Value = "0"
        oItem.ApplicationID = Me.ApplicationID
        oItem.BranchId = Me.sesBranchId.Replace("'", "")
        oItem.InvoiceDate = IIf(ucViewApplication1.TanggalAplikasi <> "", ConvertDate2(ucViewApplication1.TanggalAplikasi), "").ToString
        oItem.DueDate = IIf(txtduedate.Text <> "", ConvertDate2(txtduedate.Text), "").ToString
        oItem.InvoiceNoBatch = txtInvoiceNoBatch.Text
        If cbAnuitas.Checked = True Then oItem.IsFinal = True Else oItem.IsFinal = False

        If oItem.InvoiceNoBatch.Trim = "" Then
            ShowMessage(lblMessage, "Please fill invoice number", True)
        ElseIf txtduedate.Text = "" Then
            ShowMessage(lblMessage, "Please fill Tanggal jatuh Tempo", True)
            Exit Sub
        Else
            Try
                oItem.strConnection = GetConnectionString()
                If hdfInvoiceSeqNoH.Value = 1 Then
                    m_controller.InvoiceSaveEdit2(oItem)
                Else
                    m_controller.InvoiceSaveAdd2(oItem)
                End If
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            BindDataEdit()

            Dim amount As Double = ucViewCustomerFacility1.FasilitasInfo.AvailableAmount
            Dim totalinvoice As Double = 0
            Dim totalprinciple As Double = 0
            Dim oDataAmortisasi As New DataTable
            Dim intLoop As Integer
            Dim objrow As DataRow
            Dim No, ApplicationID, DueDate, BranchID, PrincipalAmount, InterestAmount, InterestRate, InvoiceNo As String
            Dim ucDueDate As New ucDateCE


            oDataAmortisasi.Columns.Add("No", GetType(String))
            oDataAmortisasi.Columns.Add("ApplicationID", GetType(String))
            oDataAmortisasi.Columns.Add("DueDate", GetType(String))
            oDataAmortisasi.Columns.Add("BranchID", GetType(String))
            oDataAmortisasi.Columns.Add("PrincipalAmount", GetType(String))
            oDataAmortisasi.Columns.Add("InterestAmount", GetType(String))
            oDataAmortisasi.Columns.Add("InterestRate", GetType(String))
            oDataAmortisasi.Columns.Add("InvoiceNo", GetType(String))

            For intLoop = 0 To Datagrid1.Items.Count - 1
                No = CType(Datagrid1.Items(intLoop).Cells(0).FindControl("lblNo"), Label).Text
                ApplicationID = CType(Datagrid1.Items(intLoop).Cells(1).FindControl("txtApplicationID"), TextBox).Text
                BranchID = CType(Datagrid1.Items(intLoop).Cells(2).FindControl("txtBranchID"), TextBox).Text
                DueDate = CType(Datagrid1.Items(intLoop).Cells(3).FindControl("ucDueDate"), ucDateCE).Text
                PrincipalAmount = CType(Datagrid1.Items(intLoop).Cells(4).FindControl("txtPrincipalAmount"), TextBox).Text
                InterestAmount = CType(Datagrid1.Items(intLoop).Cells(5).FindControl("txtInterestAmount"), TextBox).Text
                InterestRate = CType(Datagrid1.Items(intLoop).Cells(6).FindControl("txtInterestRate"), TextBox).Text
                InvoiceNo = CType(Datagrid1.Items(intLoop).Cells(7).FindControl("txtInvoiceNo"), TextBox).Text
                If No.Trim > 0 Then
                    objrow = oDataAmortisasi.NewRow
                    objrow("No") = No
                    objrow("ApplicationID") = ApplicationID
                    objrow("DueDate") = ConvertDate(DueDate)
                    objrow("BranchID") = Me.sesBranchId.Replace("'", "")
                    objrow("PrincipalAmount") = PrincipalAmount
                    objrow("InterestAmount") = InterestAmount
                    objrow("InterestRate") = InterestRate
                    objrow("InvoiceNo") = InvoiceNo
                    oDataAmortisasi.Rows.Add(objrow)
                End If
            Next

            For Each row As DataRow In ItemData.Rows
                totalinvoice = totalinvoice + row("TotalPembiayaan")
            Next

            For Each row As DataRow In oDataAmortisasi.Rows
                totalprinciple = totalprinciple + row("PrincipalAmount")
            Next

            If cbAnuitas.Checked = True Then
                If (totalinvoice > ucViewCustomerFacility1.FasilitasInfo.AvailableAmount) Then
                    ShowMessage(lblMessage, "Total pembiayaan melebihi sisa plafond yang tersedia", True)
                    Exit Sub
                End If
            End If

            'validasi
            If cbAnuitas.Checked = True Then
                If totalprinciple <> totalinvoice Then
                    ShowMessage(lblMessage, "Total pokok amortisasi tidak sama dengan invoice", True)
                    Exit Sub
                End If
            End If

            If ItemData.Rows.Count < 1 Then
                ShowMessage(lblMessage, "Please insert invoice data", True)
            Else
                Dim oCustomclass As New Parameter.Application

                With oCustomclass
                    .strConnection = GetConnectionString()
                    .ApplicationID = Me.ApplicationID
                    .BranchId = Me.BranchID
                    .cbanuitas = cbAnuitas.Checked
                End With
                Try
                    m_controller.InvoiceSaveApplication(oCustomclass, oDataAmortisasi)
                    'saveInvoiceHead()
                    Response.Redirect("FinancialModalKerjaData.aspx?appid=" & Me.ApplicationID)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub

    Private Sub cbAnuitas_CheckedChanged(sender As Object, e As EventArgs) Handles cbAnuitas.CheckedChanged
        If CBool(cbAnuitas.Checked) Then
            UploadAnuitas.Visible = True
            AddRecord()
        Else
            UploadAnuitas.Visible = False
        End If
        'saveInvoiceHead()
    End Sub

    Private Sub txtduedate_TextChanged(sender As Object, e As EventArgs) Handles txtduedate.TextChanged
        'saveInvoiceHead()
    End Sub

    'Private Sub txtPrincipalAmount_TextChanged(sender As Object, e As EventArgs) Handles txtPrincipalAmount.TextChanged
    '    Dim tempTotalPRAmount As Double
    '    Dim lblTotPRAmount As New TextBox
    '    Dim lblPRAmount As TextBox
    '    lblPRAmount = txtPrincipalAmount.Text
    '    tempTotalPRAmount += CDbl(lblPRAmount.Text)
    'End Sub
    'Private Sub Datagrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Datagrid1.ItemDataBound
    '    If SessionInvalid() Then
    '        Exit Sub
    '    End If
    '    Dim tempTotalPRAmount As Double
    '    Dim lblTotPRAmount As New TextBox
    '    Dim lblPRAmount As TextBox

    '    With oModalKerjaInvoice
    '        If e.Item.ItemIndex > 0 Then
    '            lblPRAmount = CType(e.Item.FindControl("txtPrincipalAmount"), TextBox)
    '            tempTotalPRAmount += CDbl(lblPRAmount.Text)
    '        End If

    '        If e.Item.ItemType = ListItemType.Footer Then
    '            lblTotPRAmount = CType(e.Item.FindControl("txtTotTransAmount"), TextBox)
    '            lblTotPRAmount.Text = FormatNumber(tempTotalPRAmount.ToString, 2)
    '            Me.TotPRAmount = CDbl(lblTotPRAmount.Text.Trim)
    '        End If
    '    End With
    'End Sub
#Region "Add-Delete Baris"
    Private Sub imbPAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPAdd.Click
        AddRecord()
    End Sub
    Private Sub AddRecord()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo As New Label
        Dim txtApplicationID As New TextBox
        Dim txtBranchID As New TextBox
        Dim txtPrincipalAmount As New TextBox
        Dim txtInterestAmount As New TextBox
        Dim txtInterestRate As New TextBox
        Dim txtInvoiceNo As New TextBox
        Dim plus As Integer = 0
        Dim ucDueDate As ucDateCE


        With objectDataTable
            .Columns.Add(New DataColumn("No", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("DueDate", GetType(String)))
            .Columns.Add(New DataColumn("BranchID", GetType(String)))
            .Columns.Add(New DataColumn("PrincipalAmount", GetType(String)))
            .Columns.Add(New DataColumn("InterestAmount", GetType(String)))
            .Columns.Add(New DataColumn("InterestRate", GetType(String)))
            .Columns.Add(New DataColumn("InvoiceNo", GetType(String)))
        End With

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            lblNo = CType(Datagrid1.Items(intLoopGrid).Cells(0).FindControl("lblNo"), Label)
            txtApplicationID = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtApplicationID"), TextBox)
            ucDueDate = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("ucDueDate"), ucDateCE)
            txtBranchID = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("txtBranchID"), TextBox)
            txtPrincipalAmount = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("txtPrincipalAmount"), TextBox)
            txtInterestAmount = CType(Datagrid1.Items(intLoopGrid).Cells(5).FindControl("txtInterestAmount"), TextBox)
            txtInterestRate = CType(Datagrid1.Items(intLoopGrid).Cells(6).FindControl("txtInterestRate"), TextBox)
            txtInvoiceNo = CType(Datagrid1.Items(intLoopGrid).Cells(7).FindControl("txtInvoiceNo"), TextBox)
            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("No") = CType(lblNo.Text, String)
            'oRow("Name") = CType(txtName.Text, String)
            'oRow("IDNumber") = CType(txtIDNumber.Text, String)
            oRow("ApplicationID") = Me.ApplicationID
            oRow("DueDate") = ""
            oRow("BranchID") = Me.sesBranchId.Replace("'", "")
            oRow("PrincipalAmount") = ""
            oRow("InterestAmount") = ""
            oRow("InterestRate") = ""
            oRow("InvoiceNo") = Me.InvoiceNo
            'oRow("BirthDate") = CType(ucTglLahir.Text, String)
            'oRow("Relationship") = CType(cboRelationship.SelectedValue, String)
            objectDataTable.Rows.Add(oRow)


        Next

        'If Datagrid1.Items.Count = 0 Then
        '    'add data pasangan
        '    oRow = objectDataTable.NewRow()
        '    oRow("No") = Datagrid1.Items.Count + 1
        '    oRow("Name") = Me.NamaPasangan
        '    oRow("IDNumber") = Me.IDNumber
        '    oRow("BirthDate") = Me.BirthDate
        '    oRow("Relationship") = "SP"
        '    objectDataTable.Rows.Add(oRow)

        '    plus += 1
        'End If

        oRow = objectDataTable.NewRow()
        oRow("No") = Datagrid1.Items.Count + 1 + plus
        oRow("ApplicationID") = Me.ApplicationID
        oRow("DueDate") = ""
        oRow("BranchID") = Me.sesBranchId.Replace("'", "")
        oRow("PrincipalAmount") = ""
        oRow("InterestAmount") = ""
        oRow("InterestRate") = ""
        oRow("InvoiceNo") = Me.InvoiceNo

        objectDataTable.Rows.Add(oRow)
        Datagrid1.DataSource = objectDataTable
        Datagrid1.DataBind()

        'fillCboRelationship()

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            lblNo = CType(Datagrid1.Items(intLoopGrid).Cells(0).FindControl("lblNo"), Label)
            txtApplicationID = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtApplicationID"), TextBox)
            ucDueDate = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("ucDueDate"), ucDateCE)
            txtBranchID = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("txtBranchID"), TextBox)
            txtPrincipalAmount = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("txtPrincipalAmount"), TextBox)
            txtInterestAmount = CType(Datagrid1.Items(intLoopGrid).Cells(5).FindControl("txtInterestAmount"), TextBox)
            txtInterestRate = CType(Datagrid1.Items(intLoopGrid).Cells(6).FindControl("txtInterestRate"), TextBox)
            txtInvoiceNo = CType(Datagrid1.Items(intLoopGrid).Cells(7).FindControl("txtInvoiceNo"), TextBox)

            lblNo.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim
            txtApplicationID.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            ucDueDate.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            txtBranchID.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            txtPrincipalAmount.Text = objectDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            txtInterestAmount.Text = objectDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            txtInterestRate.Text = objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim
            txtInvoiceNo.Text = objectDataTable.Rows(intLoopGrid).Item(7).ToString.Trim
        Next
    End Sub
    Sub DeleteBaris(ByVal Index As Integer)
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo As New Label
        Dim txtName As New TextBox
        Dim txtIDNumber As New TextBox
        'Dim uscBirthDate As TextBox
        'Dim uscBirthDate As ucDateCE
        'Dim cboRelationship As New DropDownList
        Dim imbDelete As New ImageButton

        Dim objectDataTable As New DataTable
        Dim txtApplicationID As New TextBox
        Dim txtBranchID As New TextBox
        Dim txtPrincipalAmount As New TextBox
        Dim txtInterestAmount As New TextBox
        Dim txtInterestRate As New TextBox
        Dim txtInvoiceNo As New TextBox
        Dim plus As Integer = 0
        Dim ucDueDate As ucDateCE

        With objectDataTable
            .Columns.Add(New DataColumn("No", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("DueDate", GetType(String)))
            .Columns.Add(New DataColumn("BranchID", GetType(String)))
            .Columns.Add(New DataColumn("PrincipalAmount", GetType(String)))
            .Columns.Add(New DataColumn("InterestAmount", GetType(String)))
            .Columns.Add(New DataColumn("InterestRate", GetType(String)))
            .Columns.Add(New DataColumn("InvoiceNo", GetType(String)))
        End With

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            lblNo = CType(Datagrid1.Items(intLoopGrid).Cells(0).FindControl("lblNo"), Label)
            txtApplicationID = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtApplicationID"), TextBox)
            ucDueDate = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("ucDueDate"), ucDateCE)
            txtBranchID = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("txtBranchID"), TextBox)
            txtPrincipalAmount = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("txtPrincipalAmount"), TextBox)
            txtInterestAmount = CType(Datagrid1.Items(intLoopGrid).Cells(5).FindControl("txtInterestAmount"), TextBox)
            txtInterestRate = CType(Datagrid1.Items(intLoopGrid).Cells(6).FindControl("txtInterestRate"), TextBox)
            txtInvoiceNo = CType(Datagrid1.Items(intLoopGrid).Cells(7).FindControl("txtInvoiceNo"), TextBox)
            '----- Add row -------'
            If intLoopGrid <> Index Then
                oRow = objectDataTable.NewRow()
                oRow("No") = CType(lblNo.Text, String)
                oRow("ApplicationID") = CType(txtApplicationID.Text, String)
                oRow("DueDate") = CType(ucDueDate.Text, String)
                oRow("BranchID") = CType(txtBranchID.Text, String)
                oRow("PrincipalAmount") = CType(txtPrincipalAmount.Text, String)
                oRow("InterestAmount") = CType(txtInterestAmount.Text, String)
                oRow("InterestRate") = CType(txtInterestRate.Text, String)
                oRow("InvoiceNo") = CType(txtInvoiceNo.Text, String)
                objectDataTable.Rows.Add(oRow)
            End If



        Next
        Datagrid1.DataSource = objectDataTable
        Datagrid1.DataBind()
        'fillCboRelationship()

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            lblNo = CType(Datagrid1.Items(intLoopGrid).Cells(0).FindControl("lblNo"), Label)
            txtApplicationID = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtApplicationID"), TextBox)
            ucDueDate = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("ucDueDate"), ucDateCE)
            txtBranchID = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("txtBranchID"), TextBox)
            txtPrincipalAmount = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("txtPrincipalAmount"), TextBox)
            txtInterestAmount = CType(Datagrid1.Items(intLoopGrid).Cells(5).FindControl("txtInterestAmount"), TextBox)
            txtInterestRate = CType(Datagrid1.Items(intLoopGrid).Cells(6).FindControl("txtInterestRate"), TextBox)
            txtInvoiceNo = CType(Datagrid1.Items(intLoopGrid).Cells(7).FindControl("txtInvoiceNo"), TextBox)

            lblNo.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim
            txtApplicationID.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            ucDueDate.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            txtBranchID.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            txtPrincipalAmount.Text = objectDataTable.Rows(intLoopGrid).Item(4).ToString.Trim
            txtInterestAmount.Text = objectDataTable.Rows(intLoopGrid).Item(5).ToString.Trim
            txtInterestRate.Text = objectDataTable.Rows(intLoopGrid).Item(6).ToString.Trim
            txtInvoiceNo.Text = objectDataTable.Rows(intLoopGrid).Item(7).ToString.Trim
        Next
    End Sub
#Region "ItemCommand"
    Private Sub Datagrid1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid1.ItemCommand
        If e.CommandName = "Delete" Then
            DeleteBaris(e.Item.ItemIndex)
        End If
    End Sub
#End Region
#End Region
End Class