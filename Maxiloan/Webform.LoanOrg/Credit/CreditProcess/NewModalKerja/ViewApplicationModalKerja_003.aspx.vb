﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class ViewApplicationModalKerja_003
    Inherits Maxiloan.Webform.WebBased
    Private oController As New ApplicationController
    Private m_controller As New ProductController

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplicationMK
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucMailingAddress As UcCompanyAddress
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabModalKerja
#End Region

#Region "Constanta"
    Dim oRow As DataRow
    Dim Status As Boolean
#End Region

#Region "Properties"
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property ProductID() As String
        Get
            Return ViewState("ProductID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property

    Property ProductOffID() As String
        Get
            Return ViewState("ProductOffID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOffID") = Value
        End Set
    End Property

    Property KodeDATI() As String
        Get
            Return ViewState("KodeDATI").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("KodeDATI") = Value
        End Set
    End Property

    Property Type() As String
        Get
            Return ViewState("Type").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Property Desc() As String
        Get
            Return ViewState("Desc").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Desc") = Value
        End Set
    End Property

    Property TotalCountApplicationID() As Integer
        Get
            Return CType(ViewState("TotalCountApplicationID"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("TotalCountApplicationID") = Value
        End Set
    End Property

    Property PageMode As String
        Get
            Return CType(ViewState("PageMode"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PageMode") = value
        End Set
    End Property
    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        lblMessage.Text = ""
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            LoadingKegiatanUsaha()
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()
                .initControls("OnNewApplication")
                Me.CustomerID = .CustomerID
            End With
            initObjects()
            BindEdit()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.setLink()
        End If
    End Sub

    Private Sub LoadingKegiatanUsaha()
        Me.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataSource = KegiatanUsahaS
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"
        cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue("M"))   'Set Default for MultiGuna
        cboKegiatanUsaha.Enabled = False


        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"
        cboJenisPembiyaan.DataSource = def
        cboJenisPembiyaan.Items.Insert(0, "Select One")
        cboJenisPembiyaan.Items(0).Value = "SelectOne"
        cboJenisPembiyaan.DataBind()

        refresh_cboJenisPembiayaan("M")
    End Sub

#Region "Init Objects"

    Protected Sub initObjects()
        ucViewApplication1.CustomerID = Me.CustomerID
        ucViewApplication1.bindData()
        ucViewApplication1.initControls("OnNewApplication")
        ucViewCustomerDetail1.CustomerID = Me.CustomerID
        ucViewCustomerDetail1.bindCustomerDetail()
        ucMailingAddress.ValidatorTrue()
        ucMailingAddress.showMandatoryAll()
        ucApplicationTab1.selectedTab("Application")
    End Sub

#End Region

#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.sesBranchId
        oApplication.AppID = Me.ApplicationID
        oApplication = oController.GetViewApplication(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)

            cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(oRow("KegiatanUsaha").ToString))
            lblKegiatanUsaha.Text = cboKegiatanUsaha.SelectedItem.Text
            refresh_cboJenisPembiayaan(oRow("KegiatanUsaha").ToString)
            cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(oRow("JenisPembiayaan").ToString))
            lblJenisPembiyaan.Text = cboJenisPembiyaan.SelectedItem.Text
            cboCaraBayar.SelectedIndex = cboCaraBayar.Items.IndexOf(cboCaraBayar.Items.FindByValue(oRow("WayOfPayment").ToString))
            lblCaraBayar.Text = cboCaraBayar.SelectedItem.Text
            lblCMO.Text = oRow("AOName").ToString

            txtKabupaten.Text = oRow("NamaKabKot").ToString
            txtAppNotes.Text = oRow("Notes").ToString.Trim

            With ucMailingAddress
                .Address = oRow("MailingAddress").ToString.Trim
                .RT = oRow("MailingRT").ToString.Trim
                .RW = oRow("MailingRW").ToString.Trim
                .Kelurahan = oRow("MailingKelurahan").ToString.Trim
                .Kecamatan = oRow("MailingKecamatan").ToString.Trim
                .City = oRow("MailingCity").ToString.Trim
                .ZipCode = oRow("MailingZipCode").ToString.Trim
                .AreaPhone1 = oRow("MailingAreaPhone1").ToString.Trim
                .Phone1 = oRow("MailingPhone1").ToString.Trim
                .AreaPhone2 = oRow("MailingAreaPhone2").ToString.Trim
                .Phone2 = oRow("MailingPhone2").ToString.Trim
                .AreaFax = oRow("MailingAreaFax").ToString.Trim
                .Fax = oRow("MailingFax").ToString.Trim
                .BindAddress()
            End With

            lblProductOffering.Text = oRow("ProdOff").ToString.Trim
            Me.ProductID = oRow("ProductID").ToString.Trim
            Me.ProductOffID = oRow("ProductOfferingID").ToString.Trim

        End If
    End Sub
#End Region

    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"

        If key = String.Empty Then
            cboJenisPembiyaan.DataSource = def
            cboJenisPembiyaan.DataBind()
        Else
            Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
            cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        End If
        cboJenisPembiyaan.DataBind()
        cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue("IF"))

    End Sub

End Class