﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services


Public Class FinancialDataModalKerja_004
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabModalKerja
    Protected WithEvents txtRefundBungaN As ucNumberFormat
    Protected WithEvents txtRefundBunga As ucNumberFormat
    Protected WithEvents txtSubsidiBungaDealer As ucNumberFormat
    Protected WithEvents txtRefundPremiN As ucNumberFormat
    Protected WithEvents txtRefundPremi As ucNumberFormat
    Protected WithEvents txtSubsidiBungaATPM As ucNumberFormat
    Protected WithEvents lblPendapatanPremiN As ucNumberFormat
    Protected WithEvents lblPendapatanPremi As ucNumberFormat
    Protected WithEvents txtSubsidiAngsuran As ucNumberFormat
    Protected WithEvents txtSubsidiAngsuranATPM As ucNumberFormat
    Protected WithEvents txtSubsidiUangMuka As ucNumberFormat
    Protected WithEvents txtRefundLainN As ucNumberFormat
    Protected WithEvents txtRefundLain As ucNumberFormat
    Protected WithEvents txtSubsidiUangMukaATPM As ucNumberFormat
    Protected WithEvents txtDealerDiscount As ucNumberFormat
    Protected WithEvents txtBiayaMaintenance As ucNumberFormat
    Protected WithEvents txtRefundBiayaProvisiN As ucNumberFormat
    Protected WithEvents txtRefundBiayaProvisi As ucNumberFormat
    Protected WithEvents txtSubsidiRiskATPM As ucNumberFormat
    Protected WithEvents txtSubsidiRisk As ucNumberFormat
    Protected WithEvents lblPendapatanPremiCPN As ucNumberFormat
    Protected WithEvents lblPendapatanPremiJKN As ucNumberFormat
    Protected WithEvents txtRefundPremiCPN As ucNumberFormat
    Protected WithEvents txtRefundPremiCP As ucNumberFormat
    Protected WithEvents txtRefundPremiJKN As ucNumberFormat
    Protected WithEvents txtRefundPremiJk As ucNumberFormat
    Protected WithEvents txtRefundUppingBungaN As ucNumberFormat
    Protected WithEvents txtRefundUppingBunga As ucNumberFormat
    Protected WithEvents txtSubsidiDPUppingBungaN As ucNumberFormat
    Protected WithEvents txtSubsidiDPUppingBunga As ucNumberFormat
    Protected WithEvents UctxtUangMukaMF As ucNumberFormat
    Protected WithEvents uctxtAdminFeeMF As ucNumberFormat
    Protected WithEvents UctxtBiayaFidusiaMF As ucNumberFormat
    Protected WithEvents UctxtOtherFeeMf As ucNumberFormat
    Protected WithEvents UctxtBiayaProvisiMF As ucNumberFormat
    Protected WithEvents uctxtasuransitunaiMF As ucNumberFormat
    Protected WithEvents uctxtAngsuranPertamaMF As ucNumberFormat
    Protected WithEvents UctxtUangMukaKaroseriMF As ucNumberFormat

#End Region


#Region "Constanta"
    Private Const style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim RateIRR As Double
    Dim ExcludeAmount As Double
    '-- Income
    Dim AdminFee, SubsidiBunga, IncomePremi, ProvisionFee, UppingBunga As Double
    '-- Cost
    Dim IncentiveDealer, InsuranceRefund, ProvisionRefund, OtherRefund, RefundUppingBunga As Double

    Dim m_controller As New FactoringInvoiceController
    Dim Entities As New Parameter.FactoringInvoice
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController

    Private oRefundInsentifController As New RefundInsentifController
    Private oController As New ApplicationDetailTransactionController

    Private TotalInsGrid As Double
    Private TotalProsentase As Double
    Private TotalBungaNett As Decimal
    Private TotalPencairan As Double
    Private TotalBungaNettAmount As Double

#End Region

#Region "Property"
    Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
    Property InstallmentAmount() As Double
        Get
            Return CDbl(ViewState("InstallmentAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("InstallmentAmount") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return ViewState("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Supplier") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return ViewState("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Property InstallmentScheme() As String
        Get
            Return ViewState("InstallmentScheme").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Property IsIncentiveSupplier() As Boolean
        Get
            Return CType(ViewState("IsIncentiveSupplier"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsIncentiveSupplier") = Value
        End Set
    End Property
    Property RefundInterest As Double
        Get
            Return CType(ViewState("RefundInterest"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundInterest") = value
        End Set
    End Property
    Property Provisi As Double
        Get
            Return CType(ViewState("Provisi"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("Provisi") = value
        End Set
    End Property
    Private Property dtDistribusiNilaiInsentif As DataTable
        Get
            Return CType(ViewState("dtDistribusiNilaiInsentif"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtDistribusiNilaiInsentif") = value
        End Set
    End Property
    Private Property ApplicationDetailTransactionDT As DataTable
        Get
            Return CType(ViewState("ApplicationDetailTransactionDT"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("ApplicationDetailTransactionDT") = value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property NilaiTransaksi() As Decimal
        Get
            Return CType(ViewState("NilaiTransaksi"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("NilaiTransaksi") = Value
        End Set
    End Property
    Private Property EffectiveRate() As Decimal
        Get
            Return CType(ViewState("EffectiveRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffectiveRate") = Value
        End Set
    End Property
    Private Property FlatRate() As Decimal
        Get
            Return CType(ViewState("FlatRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("FlatRate") = Value
        End Set
    End Property

    Property RefundBungaAmount As Double
        Get
            Return CType(ViewState("RefundBungaAmount"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundBungaAmount") = value
        End Set
    End Property

    Public Property Balon() As DataTable
        Get
            Return CType(ViewState("Balon"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("Balon") = value
        End Set
    End Property


    Public Property FirstInstallment As String
        Get
            Return CStr(ViewState("FirstInstallment"))
        End Get
        Set(value As String)
            ViewState("FirstInstallment") = value
        End Set
    End Property

    Public Property Tenor As Integer
        Get
            Return CInt(ViewState("Tenor"))
        End Get
        Set(value As Integer)
            ViewState("Tenor") = value
        End Set
    End Property

    Public Property OtrNtf() As String
        Get
            Return CStr(ViewState("OtrNtf"))
        End Get
        Set(value As String)
            ViewState("OtrNtf") = value
        End Set
    End Property

    Public Property ntf() As Double
        Get
            Return CDbl(ViewState("NTF"))
        End Get
        Set(value As Double)
            ViewState("NTF") = value
        End Set
    End Property
    Public Property UangMukaBayarDi() As String
    Public Property DownPaymentMF() As Double
    Public Property DPKaroseriAmuntMF() As Double
    Public Property AdminFeeMf() As Double
    Public Property FiduciaFeeMf() As Double
    Public Property OtherFeeMf() As Double
    Public Property ProvisionFeeMf() As Double
    Public Property UangMuka() As Double
    Public Property OtherFee() As Double

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Session.Remove("balon")
            Me.ApplicationID = Request("appid")

            Me.EffectiveRate = 0

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                'hdfApplicationID.Value = Me.ApplicationID
                .bindData()
                .initControls("OnNewApplication")

                Me.CustomerID = .CustomerID
                Me.CustName = .CustomerName
                Me.InterestType = .InterestType.ToString
                Me.InstallmentScheme = .InstallmentScheme.ToString

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If
            End With

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial2")
            ucApplicationTab1.setLink()
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            initObjects()
            Bindgrid_002()
            GetRefundPremiumToSupplier()


            If ucViewApplication1.isProceed = True Then
                btnSave.Visible = False
            End If
        End If
    End Sub

#Region "Load Financial Data"
    Sub Bindgrid_002()
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FactoringInvoice

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_003"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)

        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.Listdata
        End If

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            'lblNilaiNTFOTR.Text = objrow(objrow("OtrNtf").ToString.Trim).ToString
            Me.OtrNtf = objrow("OtrNtf").ToString.Trim
            Me.ntf = CDbl(objrow("NTF"))
            'lblNilaiRefundBunga.Text = objrow("NilaiRefundBunga").ToString
            'lblNilaiTenor.Text = CStr(Math.Ceiling(CDbl(objrow("tenor").ToString) / 12))
            'txtFlatRate.Text = FormatNumber(objrow.Item("FlatRate"), 2)
            'lblTotalBunga.Text = FormatNumber(CDbl(objrow("TotalBunga")), 0)
            'lblTotalBunga.Text = FormatNumber(CDbl(objrow.Item("TotalOTR")) - CDbl(objrow.Item("DownPayment"))).ToString()
            'lblNilaiTotalBunga.Text = (CDbl(objrow.Item("OTRKendaraan")) - CDbl(objrow.Item("DownPayment"))).ToString() ' objrow("TotalBunga").ToString.Trim
            'lblNilaiTotalBunga.Text = (CDbl(objrow.Item("TotalOTR")) - CDbl(objrow.Item("DownPayment"))).ToString()
            'lblNilaiTotalBunga.Text = objrow("TotalBunga").ToString.Trim
            'lblPremiAsuransiGross.Text = FormatNumber(CDbl(objrow.Item("PremiGross")), 0)
            lblAssetInsurance32.Text = FormatNumber(CDbl(objrow.Item("PaidAmountByCust")), 0)

            lblIsAdminFeeCredit.Text = objrow.Item("IsAdminFeeCredit").ToString

            'lblAngsuranPertama.Text = IIf(objrow.Item("FirstInstallment").ToString = "AD", FormatNumber(objrow.Item("InstallmentAmount"), 0), "0").ToString
            lblBiayaProvisi.Text = FormatNumber(objrow("ProvisionFeeMF"), 0)


            calculateNPV()
            'If CDbl(objrow("refundBungaPercent")) > 0 Then
            '    txtRefundBunga.Text = FormatNumber(objrow("refundBungaPercent"), 2)
            'Else
            '    txtRefundBunga.Text = "0"
            'End If
            'If CDbl(objrow("refundBunga")) > 0 Then
            '    txtRefundBungaN.Text = FormatNumber(objrow("refundBunga"), 0)
            'Else
            '    txtRefundBungaN.Text = "0"
            'End If

            'txtRefundBunga.RangeValidatorEnable = True
            'txtRefundBunga.RangeValidatorMaximumValue = "100"
            'txtRefundBunga.RangeValidatorMinimumValue = "0"
            'txtRefundBungaN.RangeValidatorEnable = True
            'txtRefundBungaN.RangeValidatorMaximumValue = objrow("TotalBunga").ToString.Trim
            'txtRefundBungaN.RangeValidatorMaximumValue = (CDbl(objrow.Item("TotalOTR")) - CDbl(objrow.Item("DownPayment"))).ToString()
            'txtRefundBungaN.RangeValidatorMinimumValue = "0"

            'lblPremiAsuransiNet.Text = FormatNumber(objrow.Item("PremiNett"), 0)
            Dim pgross, pinsnet, pselisih, ppolis, potherfee As Double
            'pgross = CDbl(IIf(IsNumeric(lblPremiAsuransiGross.Text), lblPremiAsuransiGross.Text, 0))
            'pinsnet = CDbl(IIf(IsNumeric(lblPremiAsuransiNet.Text), lblPremiAsuransiNet.Text, 0))
            ppolis = CDbl(IIf(IsNumeric(objrow.Item("BiayaPolis")), objrow.Item("BiayaPolis"), 0))
            potherfee = CDbl(IIf(IsNumeric(objrow.Item("OtherFee")), objrow.Item("OtherFee"), 0))

            pselisih = pgross - pinsnet
            'lblSelisihPremi.Text = FormatNumber(pselisih, 0)

            'txtSubsidiBungaDealer.Text = FormatNumber(objrow("SubsidiBungaDealer"), 0)

            'Refund Premi
            Dim refundPremi_ As Double
            Dim refundPremiP As Double
            If CDbl(objrow.Item("RefundPremiPercent")) > 0 Then
                txtRefundPremi.Text = FormatNumber(CDbl(objrow.Item("RefundPremiPercent")), 2)
                txtRefundPremiN.Text = FormatNumber(CDbl(objrow.Item("RefundPremi")), 0)
                txtRefundPremi.RangeValidatorEnable = True
                txtRefundPremi.RangeValidatorMinimumValue = "0"
                txtRefundPremi.RangeValidatorMaximumValue = "100"
                txtRefundPremiN.RangeValidatorEnable = True
                txtRefundPremiN.RangeValidatorMinimumValue = "0"
                txtRefundPremiN.RangeValidatorMaximumValue = "99999999999999999"

            Else
                txtRefundPremiN.Text = "0"
                txtRefundPremi.Text = "0"

                txtRefundPremi.RangeValidatorEnable = True
                txtRefundPremi.RangeValidatorMinimumValue = "0"
                txtRefundPremi.RangeValidatorMaximumValue = "100"
                txtRefundPremiN.RangeValidatorEnable = True
                txtRefundPremiN.RangeValidatorMinimumValue = "0"
                txtRefundPremiN.RangeValidatorMaximumValue = "99999999999999999"

            End If
            'lblPendapatanPremiN.Text = FormatNumber(CDbl(lblSelisihPremi.Text) - CDbl(txtRefundPremiN.Text), 0)
            'txtSubsidiBungaATPM.Text = FormatNumber(objrow("SubsidiBungaATPM"), 0)
            'lblPendapatanPremiN.Text = FormatNumber(objrow("PendapatanPremi"), 0)
            'lblPendapatanPremi.Text = FormatNumber(objrow("PendapatanPremiPercent"), 2)
            'txtSubsidiAngsuran.Text = FormatNumber(objrow("SubsidiAngsuran"), 0)
            'txtSubsidiAngsuranATPM.Text = FormatNumber(objrow("SubsidiAngsuranATPM"), 0)
            lblOtherFee.Text = FormatNumber(objrow.Item("OtherFee"), 0)
            'txtSubsidiUangMuka.Text = FormatNumber(objrow.Item("SubsidiUangMuka"), 0)
            'txtSubsidiUangMukaATPM.Text = FormatNumber(objrow.Item("SubsidiUangMukaATPM"), 0)

            'txtSubsidiRisk.Text = FormatNumber(objrow("SubsidiRisk"), 0)
            'txtSubsidiRiskATPM.Text = FormatNumber(objrow("SubsidiRiskATPM"), 0)
            'lblUppingBunga.Text = FormatNumber(objrow("UppingBungaAmount"), 0)


            txtRefundLainN.Text = FormatNumber(objrow.Item("RefundBiayaLain"), 0)
            txtRefundLain.Text = FormatNumber(objrow.Item("RefundBiayaLainPercent"), 2)
            txtRefundLain.RangeValidatorEnable = True
            txtRefundLain.RangeValidatorMinimumValue = "0"
            txtRefundLain.RangeValidatorMaximumValue = CStr(IIf(potherfee > 0, "100", "0"))
            txtRefundLainN.RangeValidatorEnable = True
            txtRefundLainN.RangeValidatorMinimumValue = "0"
            txtRefundLainN.RangeValidatorMaximumValue = CStr(IIf(potherfee > 0, potherfee.ToString, "0"))

            'txtDealerDiscount.Text = FormatNumber(objrow.Item("DealerDiscount"), 0)

            'txtBiayaMaintenance.Text = FormatNumber(objrow("BiayaMaintenance"), 0)

            txtRefundBiayaProvisiN.Text = FormatNumber(objrow("RefundBiayaProvisi"), 0)
            txtRefundBiayaProvisi.Text = FormatNumber(objrow("RefundBiayaProvisiPercent"), 2)

            txtRefundBiayaProvisi.RangeValidatorEnable = True
            txtRefundBiayaProvisi.RangeValidatorMinimumValue = "0"
            txtRefundBiayaProvisi.RangeValidatorMaximumValue = "100"
            txtRefundBiayaProvisiN.RangeValidatorEnable = True
            txtRefundBiayaProvisiN.RangeValidatorMinimumValue = "0"
            txtRefundBiayaProvisiN.RangeValidatorMaximumValue = objrow("ProvisionFeeMF").ToString.Trim

            'txtSupplierCode.Text = objrow.Item("SupplierATPM").ToString.Trim
            'txtSupplierName.Text = objrow.Item("SupplierNameATPM").ToString.Trim

            EffectiveRate = CDec(objrow.Item("EffectiveRate"))

            Me.RefundInterest = CDbl(objrow.Item("RefundInterest"))

            Me.FirstInstallment = objrow.Item("FirstInstallment").ToString
            Me.Tenor = CInt(objrow.Item("Tenor").ToString.Trim)
            Me.InstallmentAmount = CDbl(objrow("InstallmentAmount").ToString.Trim)


            lblPremiAsuransiGross.Text = FormatNumber(CDbl(objrow.Item("PremiGross")), 0)
            lblPremiAsuransiNet.Text = FormatNumber(CDbl(objrow.Item("PremiNett")), 0)
            'lblCreditProtectionGross.Text = FormatNumber(CDbl(objrow.Item("PremiCreditProtection")), 0)
            'lblCreditProtectionNett.Text = FormatNumber(CDbl(objrow.Item("PremiNettCreditProtection")), 0)
            'lblCreditProtectionSelisih.Text = FormatNumber(CDbl(objrow.Item("PremiNettCreditProtection")) - CDbl(objrow.Item("PremiNettCreditProtectionInsco")), 0)
            'lblCreditProtectionSelisih.Text = FormatNumber(CDbl(objrow.Item("PremiCreditProtection")) - CDbl(objrow.Item("PremiNettCreditProtection")), 0)

            'If CDbl(objrow.Item("RefundPremiCPPercent")) > 0 Then
            '    txtRefundPremiCP.Text = FormatNumber(CDbl(objrow.Item("RefundPremiCPPercent")), 2)
            '    txtRefundPremiCPN.Text = FormatNumber(CDbl(objrow.Item("RefundPremiCP")), 0)
            '    txtRefundPremiCP.RangeValidatorEnable = True
            '    txtRefundPremiCP.RangeValidatorMinimumValue = "0"
            '    txtRefundPremiCP.RangeValidatorMaximumValue = "100"
            '    txtRefundPremiCPN.RangeValidatorEnable = True
            '    txtRefundPremiCPN.RangeValidatorMinimumValue = "0"
            '    txtRefundPremiCPN.RangeValidatorMaximumValue = "99999999999999999"

            'Else
            '    txtRefundPremiCPN.Text = "0"
            '    txtRefundPremiCP.Text = "0"
            '    txtRefundPremiCP.RangeValidatorEnable = True
            '    txtRefundPremiCP.RangeValidatorMinimumValue = "0"
            '    txtRefundPremiCP.RangeValidatorMaximumValue = "100"
            '    txtRefundPremiCPN.RangeValidatorEnable = True
            '    txtRefundPremiCPN.RangeValidatorMinimumValue = "0"
            '    txtRefundPremiCPN.RangeValidatorMaximumValue = "99999999999999999"

            'End If
            'lblPendapatanPremiCPN.Text = FormatNumber(CDbl(objrow.Item("PremiNettCreditProtection")) - CDbl(objrow.Item("PremiNettCreditProtectionInsco")) - CDbl(txtRefundPremiCPN.Text), 0)

            'lblJaminanKreditGross.Text = FormatNumber(CDbl(objrow.Item("PremiNettJaminanCredit")), 0)
            'lblJaminanKreditNett.Text = FormatNumber(CDbl(objrow.Item("PremiNettJaminanCreditInsco")), 0)
            'lblJaminanKreditGross.Text = FormatNumber(CDbl(objrow.Item("PremiJaminanCredit")), 0)
            'lblJaminanKreditNett.Text = FormatNumber(CDbl(objrow.Item("PremiNettJaminanCredit")), 0)
            'lblJaminanKreditSelisih.Text = FormatNumber(CDbl(objrow.Item("PremiNettJaminanCredit")) - CDbl(objrow.Item("PremiNettJaminanCreditInsco")), 0)
            'lblJaminanKreditSelisih.Text = FormatNumber(CDbl(objrow.Item("PremiJaminanCredit")) - CDbl(objrow.Item("PremiNettJaminanCredit")), 0)

            Me.DownPaymentMF = CDbl(objrow.Item("DownPaymentMF"))
            Me.DPKaroseriAmuntMF = CDbl(objrow.Item("DPKaroseriAmountMF"))
            Me.AdminFeeMf = CDbl(objrow.Item("AdminFeeMf"))
            Me.FiduciaFeeMf = CDbl(objrow.Item("FiduciaFeeMf"))
            Me.OtherFeeMf = CDbl(objrow.Item("OtherFeeMf"))
            Me.ProvisionFeeMf = CDbl(objrow.Item("ProvisionFeeMf"))


            Me.AdminFee = CDbl(objrow.Item("AdminFee"))
            Me.Provisi = CDbl(objrow.Item("ProvisionFee"))
            Me.UangMuka = CDbl(objrow.Item("DownPayment"))
            Me.OtherFee = CDbl(objrow.Item("OtherFee"))


            'lblBiayaFidusia.Text = FormatNumber(CDec(objrow("FiduciaFee")), 0)
            lblOtherFeeBayarPertama.Text = FormatNumber(CDbl(objrow.Item("OtherFee")), 0)
            lblBiayaProvisiBayarPertama.Text = FormatNumber(objrow("ProvisionFeeMF"), 0)
            'lblUangMuka.Text = FormatNumber(CDbl(objrow.Item("DownPayment")), 0)
            lblAdminFee.Text = FormatNumber(CDbl(objrow.Item("AdminFee")), 0)

            'lblUangMukaKaroseri.Text = FormatNumber(CDbl(objrow.Item("DPKaroseriAmount")), 0)

            'UctxtUangMukaMF.Text = FormatNumber(CDbl(objrow.Item("DownPaymentMF")), 0)
            'UctxtUangMukaKaroseriMF.Text = FormatNumber(CDbl(objrow.Item("DPKaroseriAmountMF")), 0)
            uctxtAdminFeeMF.Text = FormatNumber(objrow.Item("AdminFeeMF"), 0)
            UctxtOtherFeeMf.Text = FormatNumber(objrow.Item("OtherFeeMF"), 0)
            UctxtBiayaProvisiMF.Text = FormatNumber(objrow("ProvisionFeeMF"), 0)
            'UctxtBiayaFidusiaMF.Text = FormatNumber(CDec(objrow("FiduciaFeeMF")), 0)
            uctxtasuransitunaiMF.Text = FormatNumber(objrow("PaidAmountByCustMF"), 0)
            'uctxtAngsuranPertamaMF.Text = FormatNumber(objrow("InstallmentAmountFirstMF"), 0)


            'txtpotongpencairan.Text = FormatNumber(CDbl(objrow.Item("DownPaymentMF")) + CDbl(objrow.Item("AdminFeeMF")) + CDbl(objrow.Item("OtherFeeMF")) + 
            '                                       CDbl(objrow("ProvisionFeeMF")) + CDbl(objrow("PaidAmountByCustMF")) + CDbl(objrow("InstallmentAmountFirstMF")), 0)


            txtpotongpencairan.Text = FormatNumber(CDbl(objrow.Item("DownPaymentMF")) + CDbl(objrow.Item("DPKaroseriAmountMF")) + CDbl(objrow.Item("AdminFeeMF")) + CDbl(objrow.Item("OtherFeeMF")) +
                                                   CDbl(objrow("ProvisionFeeMF")) + CDbl(objrow("PaidAmountByCustMF")) + CDbl(objrow("InstallmentAmountFirstMF")), 0)

            'UctxtUangMukaMF.RangeValidatorEnable = True
            'UctxtUangMukaMF.RangeValidatorMinimumValue = "0"
            'UctxtUangMukaMF.RangeValidatorMaximumValue = objrow.Item("DownPayment").ToString
            'UctxtUangMukaMF.Text = FormatNumber(Me.DownPaymentMF, 0)


            'UctxtUangMukaKaroseriMF.RangeValidatorEnable = True
            'UctxtUangMukaKaroseriMF.RangeValidatorMinimumValue = "0"
            'UctxtUangMukaKaroseriMF.RangeValidatorMaximumValue = objrow.Item("DPKaroseriAmount").ToString

            uctxtAdminFeeMF.RangeValidatorEnable = True
            uctxtAdminFeeMF.RangeValidatorMinimumValue = "0"
            uctxtAdminFeeMF.RangeValidatorMaximumValue = objrow.Item("AdminFeeMf").ToString
            'uctxtAdminFeeMF.Text = FormatNumber(Me.AdminFeeMf, 0)

            'UctxtBiayaFidusiaMF.RangeValidatorEnable = True
            'UctxtBiayaFidusiaMF.RangeValidatorMinimumValue = "0"
            'UctxtBiayaFidusiaMF.RangeValidatorMaximumValue = objrow.Item("FiduciaFee").ToString
            'UctxtBiayaFidusiaMF.Text = FormatNumber(Me.FiduciaFeeMf, 0)

            UctxtOtherFeeMf.RangeValidatorEnable = True
            UctxtOtherFeeMf.RangeValidatorMinimumValue = "0"
            UctxtOtherFeeMf.RangeValidatorMaximumValue = objrow.Item("OtherFeeMF").ToString
            'UctxtOtherFeeMf.Text = FormatNumber(Me.OtherFeeMf, 0)


            UctxtBiayaProvisiMF.RangeValidatorEnable = True
            UctxtBiayaProvisiMF.RangeValidatorMinimumValue = "0"
            UctxtBiayaProvisiMF.RangeValidatorMaximumValue = objrow.Item("ProvisionFeeMF").ToString
            'UctxtBiayaProvisiMF.Text = FormatNumber(Me.ProvisionFeeMf, 0)

            uctxtasuransitunaiMF.RangeValidatorEnable = True
            uctxtasuransitunaiMF.RangeValidatorMinimumValue = "0"
            uctxtasuransitunaiMF.RangeValidatorMaximumValue = objrow.Item("PaidAmountByCustMF").ToString


            'uctxtAngsuranPertamaMF.RangeValidatorEnable = True
            'uctxtAngsuranPertamaMF.RangeValidatorMinimumValue = "0"
            'uctxtAngsuranPertamaMF.RangeValidatorMaximumValue = objrow.Item("InstallmentAmount").ToString

            'Me.UangMukaBayarDi = objrow.Item("UangMukaBayarDi").ToString

            'If CDbl(objrow.Item("RefundPremiJKPercent")) > 0 Then
            '    txtRefundPremiJk.Text = FormatNumber(CDbl(objrow.Item("RefundPremiJKPercent")), 2)
            '    txtRefundPremiJKN.Text = FormatNumber(CDbl(objrow.Item("RefundPremiJK")), 0)
            '    txtRefundPremiJk.RangeValidatorEnable = True
            '    txtRefundPremiJk.RangeValidatorMinimumValue = "0"
            '    txtRefundPremiJk.RangeValidatorMaximumValue = "100"
            '    txtRefundPremiJKN.RangeValidatorEnable = True
            '    txtRefundPremiJKN.RangeValidatorMinimumValue = "0"
            '    txtRefundPremiJKN.RangeValidatorMaximumValue = "99999999999999999"

            'Else
            '    txtRefundPremiJKN.Text = "0"
            '    txtRefundPremiJk.Text = "0"
            '    txtRefundPremiJk.RangeValidatorEnable = True
            '    txtRefundPremiJk.RangeValidatorMinimumValue = "0"
            '    txtRefundPremiJk.RangeValidatorMaximumValue = "100"
            '    txtRefundPremiJKN.RangeValidatorEnable = True
            '    txtRefundPremiJKN.RangeValidatorMinimumValue = "0"
            '    txtRefundPremiJKN.RangeValidatorMaximumValue = "99999999999999999"
            'End If
            'lblPendapatanPremiJKN.Text = FormatNumber(CDbl(objrow.Item("PremiNettJaminanCredit")) - CDbl(objrow.Item("PremiNettJaminanCreditInsco")) - CDbl(txtRefundPremiJKN.Text), 0)


            'If CDbl(objrow.Item("RefundUppingBungaPercent")) > 0 Then
            '    txtRefundUppingBunga.Text = FormatNumber(CDbl(objrow.Item("RefundUppingBungaPercent")), 2)
            '    txtRefundUppingBungaN.Text = FormatNumber(CDbl(objrow.Item("RefundUppingBunga")), 0)
            '    txtRefundUppingBunga.RangeValidatorEnable = True
            '    txtRefundUppingBunga.RangeValidatorMinimumValue = "0"
            '    txtRefundUppingBunga.RangeValidatorMaximumValue = "100"
            '    txtRefundUppingBungaN.RangeValidatorEnable = True
            '    txtRefundUppingBungaN.RangeValidatorMinimumValue = "0"
            '    txtRefundUppingBungaN.RangeValidatorMaximumValue = "99999999999999999"

            'Else
            '    txtRefundUppingBungaN.Text = "0"
            '    txtRefundUppingBunga.Text = "0"
            '    txtRefundUppingBunga.RangeValidatorEnable = True
            '    txtRefundUppingBunga.RangeValidatorMinimumValue = "0"
            '    txtRefundUppingBunga.RangeValidatorMaximumValue = "100"
            '    txtRefundUppingBungaN.RangeValidatorEnable = True
            '    txtRefundUppingBungaN.RangeValidatorMinimumValue = "0"
            '    txtRefundUppingBungaN.RangeValidatorMaximumValue = "99999999999999999"
            'End If

            'If CDbl(objrow.Item("SubsidiUppingBungaPercent")) > 0 Then
            '    txtSubsidiDPUppingBunga.Text = FormatNumber(CDbl(objrow.Item("SubsidiUppingBungaPercent")), 2)
            '    txtSubsidiDPUppingBungaN.Text = FormatNumber(CDbl(objrow.Item("SubsidiUppingBunga")), 0)
            '    txtSubsidiDPUppingBunga.RangeValidatorEnable = True
            '    txtSubsidiDPUppingBunga.RangeValidatorMinimumValue = "0"
            '    txtSubsidiDPUppingBunga.RangeValidatorMaximumValue = "100"
            '    txtSubsidiDPUppingBungaN.RangeValidatorEnable = True
            '    txtSubsidiDPUppingBungaN.RangeValidatorMinimumValue = "0"
            '    txtSubsidiDPUppingBungaN.RangeValidatorMaximumValue = "99999999999999999"

            'Else
            '    txtSubsidiDPUppingBungaN.Text = "0"
            '    txtSubsidiDPUppingBunga.Text = "0"
            '    txtSubsidiDPUppingBunga.RangeValidatorEnable = True
            '    txtSubsidiDPUppingBunga.RangeValidatorMinimumValue = "0"
            '    txtSubsidiDPUppingBunga.RangeValidatorMaximumValue = "100"
            '    txtSubsidiDPUppingBungaN.RangeValidatorEnable = True
            '    txtSubsidiDPUppingBungaN.RangeValidatorMinimumValue = "0"
            '    txtSubsidiDPUppingBungaN.RangeValidatorMaximumValue = "99999999999999999"
            'End If
            CalculateTotalBayarPertama()
            txttotalBayarPertama.Text = FormatNumber(CDbl(lblTotalBayarPertama.Text) - CDbl(txtpotongpencairan.Text), 0)

        End If
    End Sub

    Private Sub CalculateTotalBayarPertama()
        Dim um, ba, bf, at, ap, ot, bp, bpr, kr As Double

        'um = CDbl(IIf(IsNumeric(lblUangMuka.Text), lblUangMuka.Text, 0))
        ba = CDbl(IIf(IsNumeric(lblAdminFee.Text), lblAdminFee.Text, 0))
        'bf = CDbl(IIf(IsNumeric(lblBiayaFidusia.Text), lblBiayaFidusia.Text, 0))
        at = CDbl(IIf(IsNumeric(lblAssetInsurance32.Text), lblAssetInsurance32.Text, 0))
        'ap = CDbl(IIf(IsNumeric(lblAngsuranPertama.Text), lblAngsuranPertama.Text, 0))
        ot = CDbl(IIf(IsNumeric(lblOtherFee.Text), lblOtherFee.Text, 0))
        'bp = CDbl(IIf(IsNumeric(lblBiayaPolis.Text), lblBiayaPolis.Text, 0))
        bpr = CDbl(IIf(IsNumeric(lblBiayaProvisi.Text), lblBiayaProvisi.Text, 0))
        'kr = CDbl(IIf(IsNumeric(lblUangMukaKaroseri.Text), lblUangMukaKaroseri.Text, 0))

        'lblTotalBayarPertama.Text = FormatNumber(um + ba + bf + at + ap + ot + bpr, 0)
        lblTotalBayarPertama.Text = FormatNumber(um + ba + bf + at + ap + ot + bpr + kr, 0)

    End Sub

    Private Sub calculateNPV()
        Dim ntf As Double
        Dim refundRate As Decimal
        Dim flatRate As Decimal
        Dim TenorYear As Integer

        'ntf = CDbl(IIf(IsNumeric(lblNilaiNTFOTR.Text), lblNilaiNTFOTR.Text, 0))
        'refundRate = CDec(IIf(IsNumeric(lblNilaiRefundBunga.Text), lblNilaiRefundBunga.Text, 0))
        'flatRate = CDec(IIf(IsNumeric(txtFlatRate.Text), txtFlatRate.Text, 0))
        'TenorYear = CInt(IIf(IsNumeric(lblNilaiTenor.Text), CInt(lblNilaiTenor.Text), 0))

        Me.RefundBungaAmount = CreditProcess.getNPV(ntf, refundRate, flatRate, TenorYear)
        'txtRefundBungaN.Text = FormatNumber(Me.RefundBungaAmount, 0)
    End Sub

    Private Sub reCalculateIRR()
        Dim dtEntity As New DataTable
        Dim InstSchedule As New DataTable
        Dim oFinancialData As New Parameter.FactoringInvoice
        Dim oInstallmentSchedule As New Parameter.FactoringInvoice
        Dim NTF As Double
        Dim payfreq As Integer
        Dim InstCount As Integer
        Dim FirstInstallment As String

        '-- Income
        AdminFee = 0
        SubsidiBunga = 0
        IncomePremi = 0
        ProvisionFee = 0
        OtherFee = 0
        UppingBunga = 0
        '-- Cost
        IncentiveDealer = 0
        InsuranceRefund = 0
        ProvisionRefund = 0
        OtherRefund = 0
        RefundUppingBunga = 0

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_003"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)

        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.Listdata
        End If

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            NTF = CDbl(IIf(IsNumeric(objrow("NTF")), objrow("NTF"), 0))

            'IncomePremi = CDbl(objrow.Item("PremiGross")) - CDbl(objrow.Item("PremiNett")) + CDbl(objrow.Item("PremiNettCreditProtection")) + CDbl(objrow.Item("PremiNettJaminanCredit")) - CDbl(objrow.Item("PremiNettCreditProtectionInsco")) - CDbl(objrow.Item("PremiNettJaminanCreditInsco"))
            'Modify by Wira 20180103 , hasil investigasi pak Dede terkait EIR premi
            IncomePremi = CDbl(objrow.Item("PremiGross")) - CDbl(objrow.Item("RefundPremi")) + CDbl(objrow.Item("PremiNettCreditProtection")) + CDbl(objrow.Item("PremiNettJaminanCredit")) - CDbl(objrow.Item("PremiNettCreditProtectionInsco")) - CDbl(objrow.Item("PremiNettJaminanCreditInsco"))

            'SubsidiBunga = CDbl(txtSubsidiBungaATPM.Text) + CDbl(txtSubsidiBungaDealer.Text)
            AdminFee = CDbl(IIf(IsNumeric(objrow("AdminFee")), objrow("AdminFee"), 0)) ' + CDbl(IIf(IsNumeric(objrow("FiduciaFee")), objrow("FiduciaFee"), 0))
            ProvisionFee = CDbl(objrow.Item("ProvisionFee"))
            OtherFee = CDbl(objrow.Item("OtherFee"))
            FirstInstallment = objrow.Item("FirstInstallment").ToString.Trim
            UppingBunga = CDbl(objrow.Item("UppingBungaAmount"))

            'IncentiveDealer = CDbl(txtRefundBungaN.Text)
            'InsuranceRefund = CDbl(txtRefundPremiN.Text) + CDbl(txtRefundPremiCPN.Text) + CDbl(txtRefundPremiJKN.Text)
            'InsuranceRefund = CDbl(txtRefundPremiCPN.Text)
            ProvisionRefund = CDbl(txtRefundBiayaProvisiN.Text)
            OtherRefund = CDbl(txtRefundLainN.Text)
            'RefundUppingBunga = CDbl(txtRefundUppingBungaN.Text)

            'payfreq = CInt(IIf(IsNumeric(objrow("PaymentFrequency")), objrow("PaymentFrequency"), 0))
            'Modify by Wira 20170329 atas arahan Pak Dede, karena keliru rate IRR
            payfreq = m_controller.GetTerm(CInt(IIf(IsNumeric(objrow("PaymentFrequency")), objrow("PaymentFrequency"), 0)))

            oInstallmentSchedule.BranchId = Replace(Me.sesBranchId, "'", "")
            oInstallmentSchedule.ApplicationID = Me.ApplicationID
            oInstallmentSchedule.strConnection = GetConnectionString()
            oInstallmentSchedule.SpName = "spFinancialData_004"
            oInstallmentSchedule = m_controller.GetFinancialData_002(oInstallmentSchedule)
            InstSchedule = oInstallmentSchedule.Listdata
            InstCount = InstSchedule.Rows.Count

            Dim arrVal(InstCount) As Double
            Dim i As Integer = 0

            For Each row As DataRow In InstSchedule.Rows
                If i = 0 Then
                    If FirstInstallment = "AD" Then
                        NTF = NTF - CDbl(IIf(IsNumeric(row("InstallmentAmount")), row("InstallmentAmount"), "0"))
                    End If
                    arrVal(i) = -1 * (NTF - ((IncomePremi + SubsidiBunga + AdminFee + ProvisionFee + OtherFee + UppingBunga) - (IncentiveDealer + InsuranceRefund + ProvisionRefund + OtherRefund + RefundUppingBunga)))
                    i += 1
                End If
                arrVal(i) = CDbl(IIf(IsNumeric(row("InstallmentAmount")), row("InstallmentAmount"), "0"))
                i += 1
            Next
            If FirstInstallment = "AD" Then
                arrVal(InstCount) = 0
            End If

            'RateIRR = IRR(arrVal, 0.00001) * (12 / payfreq) * 100
            'Modify by Wira 20170329 atas arahan Pak Dede, karena keliru rate IRR
            RateIRR = IRR(arrVal, 0.00001) * payfreq * 100

            ExcludeAmount = (IncomePremi + SubsidiBunga + AdminFee + ProvisionFee + OtherFee + UppingBunga) - (IncentiveDealer + InsuranceRefund + ProvisionRefund + OtherRefund + RefundUppingBunga)
        End If

    End Sub

#End Region


#Region "Save"

    Private Function IsValidValidation(ByVal textbox As TextBox, ByVal CompareText As ucNumberFormat, ByVal Message As String) As Boolean
        If textbox.Text.Trim = "" Then
            ShowMessage(lblMessage, Message & " Harap diisi", True)
            Return False
        ElseIf CDbl(textbox.Text) = 0 Then
            ShowMessage(lblMessage, Message & " Harap isi > 0 ", True)
            Return False
        ElseIf CDbl(textbox.Text) > CDbl(CompareText.Text) Then
            ShowMessage(lblMessage, Message & " Harap isi < Jangka Waktu Angsuran", True)
            Return False
        End If
        Return True
    End Function

#End Region

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ofinancialData As New Parameter.FactoringInvoice
        Try

            With ofinancialData
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .ApplicationID = Me.ApplicationID
            End With

            ofinancialData = m_controller.SaveFinancialDataTab2End(ofinancialData)
            showPencairan()
            saveBungaNett()
            ShowMessage(lblMessage, "Data saved!", False)
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial2")
            ucApplicationTab1.setLink()
        Catch ex As Exception
            ShowMessage(lblMessage, "error - " & ex.Message, True)
        End Try

    End Sub

#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As New HttpCookie("Incentive")
        cookie.Values.Add("ApplicationID", Me.ApplicationID)
        cookie.Values.Add("CustomerID", Me.CustomerID)
        cookie.Values.Add("CustomerName", Me.CustName)

        Response.AppendCookie(cookie)
    End Sub
#End Region

    Protected Sub initObjects()
        pnlPencairan.Visible = False
        pnlSave.Visible = False
        lblMessage.Text = ""
        lblMessage.Visible = False

    End Sub

#Region "incentive"

    Private Sub GetRefundPremiumToSupplier()
        Dim data As New Supplier
        data.strConnection = GetConnectionString()
        data.AppID = Me.ApplicationID

        data = m_controller.GetRefundPremiumToSupplier(data)
        Me.SupplierID = data.SupplierID
    End Sub


    Private Function validateinsentif() As String
        Dim errmsg As String = String.Empty
        'loop rincian transaksi get each transaction total

        For i As Integer = 0 To ApplicationDetailTransactionDT.Rows.Count - 1
            If CBool(ApplicationDetailTransactionDT.Rows(i)("isDistributable")) = False Then Continue For
            Dim transAmount As Decimal = CDec(ApplicationDetailTransactionDT.Rows(i)("txtTransAmount"))
            Dim transID As String = ApplicationDetailTransactionDT.Rows(i)("TransID").ToString

            'errmsg = validateInsentifPerTransaction(transID, transAmount)
            If errmsg <> String.Empty Then Return errmsg
        Next


        Return String.Empty
    End Function

    Private Function validateInsentifPerTransaction(ByVal transID As String, ByVal transAmount As Decimal) As String
        Dim dblTotalInsentifSupplierEmployee As Double

        For i As Integer = 0 To Me.dtDistribusiNilaiInsentif.Rows.Count - 1
            If dtDistribusiNilaiInsentif.Rows(i).Item("transId").ToString.Trim.ToLower = transID.Trim.ToLower Then
                dblTotalInsentifSupplierEmployee += CDbl(Me.dtDistribusiNilaiInsentif.Rows(i).Item("IncentiveForRecepient"))
                TotalInsGrid = TotalInsGrid + dblTotalInsentifSupplierEmployee
            End If

        Next

        If transAmount <> dblTotalInsentifSupplierEmployee Then
            Return "Total distribusi insentif " & transID & "tidak sama dengan nilai insentif!"
        End If

        Return String.Empty
    End Function
#End Region


    Public Sub showPencairan()
        Dim oClass As New Parameter.ApplicationDetailTransaction

        With oClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
        End With

        Try
            oClass = oController.getBungaNett(oClass)

            gvBungaNet.DataSource = oClass.Tabels.Tables(0)
            gvBungaNet.DataBind()

            gvPencairan.DataSource = oClass.Tabels.Tables(1)
            gvPencairan.DataBind()

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub


    Private Sub gvBungaNet_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBungaNet.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblRate As Label = CType(e.Row.Cells(2).FindControl("lblRate"), Label)

            If gvBungaNet.DataKeys(e.Row.RowIndex).Item("TransID").ToString <> "EFF" Then
                Dim nilai As Double = CType(e.Row.Cells(1).Text, Double)
                lblRate.Text = getRateDetailTransaksi(ntf + nilai).ToString
            End If

            If e.Row.Cells(3).Text = "+" Then
                TotalBungaNett += CDec(lblRate.Text)
            Else
                TotalBungaNett -= CDec(lblRate.Text)
            End If

        ElseIf e.Row.RowType = DataControlRowType.Footer Then
            Dim lblTotalRate As Label = CType(e.Row.Cells(2).FindControl("lblTotalRate"), Label)
            lblTotalRate.Text = Math.Round(TotalBungaNett, 2).ToString
        End If

    End Sub

    Private Sub gvPencairan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPencairan.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblNilai As Label = CType(e.Row.Cells(1).FindControl("lblNilai"), Label)

            If e.Row.Cells(3).Text = "+" Then
                TotalPencairan += CDbl(lblNilai.Text)
            Else
                TotalPencairan -= CDbl(lblNilai.Text)
            End If


        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalPencairan As Label = CType(e.Row.Cells(1).FindControl("lblTotalPencairan"), Label)
            'lblTotalPencairan.Text = FormatNumber(Math.Ceiling(Math.Round(TotalPencairan, 0) / 1000) * 1000, 0)
            'lblTotalPencairan.Text = FormatNumber(Math.Round(TotalPencairan / 10, 0) * 10, 0)
            lblTotalPencairan.Text = FormatNumber(TotalPencairan, 0)


        End If
    End Sub


    Function getRateDetailTransaksi(TransAmount As Double) As Decimal
        Dim xrate As Decimal
        If Me.FirstInstallment = "AD" Then
            xrate = RateConvertion.GetEffectiveRateAdv(CInt(Tenor), CDbl(Me.InstallmentAmount), TransAmount, 1)
        Else
            xrate = RateConvertion.GetEffectiveRateArr(CInt(Tenor), CDbl(Me.InstallmentAmount), TransAmount, 1)
        End If

        Return Math.Round(Math.Abs(Me.EffectiveRate - xrate), 2)
    End Function


    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        reCalculateIRR()
        If saveData() = True Then
            showPencairan()
            pnlPencairan.Visible = True
            pnlSave.Visible = True
        End If
        'saveBungaNett()
    End Sub

    Private Sub saveBungaNett()
        Try
            Dim oBungaNet As New Parameter.FactoringInvoice

            With oBungaNet
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .FirstInstallment = Me.FirstInstallment
                .EffectiveRate = TotalBungaNett / 100
                .Tenor = Tenor
                .BungaNettEff = TotalBungaNett
                .BungaNettFlat = cEffToFlat(oBungaNet)
            End With

            oBungaNet = m_controller.saveBungaNett(oBungaNet)

        Catch ex As Exception
            'skip
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    'Private Function saveBungaNett() As Boolean
    '    Try
    '        showPencairan()
    '        Dim oBungaNet As New Parameter.FinancialData

    '        With oBungaNet
    '            .strConnection = GetConnectionString()
    '            .ApplicationID = Me.ApplicationID
    '            .FirstInstallment = Me.FirstInstallment
    '            .EffectiveRate = TotalBungaNett / 100
    '            .Tenor = Tenor
    '            .BungaNettEff = TotalBungaNett
    '            .BungaNettFlat = cEffToFlat(oBungaNet)
    '        End With

    '        oBungaNet = m_controller.saveBungaNett(oBungaNet)

    '    Catch ex As Exception
    '        ShowMessage(lblMessage, ex.Message, True)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub

    Private Function saveData() As Boolean
        Dim oFinancialData As New Parameter.FactoringInvoice
        Try
            With oFinancialData
                .strConnection = GetConnectionString()
                .BusinessDate = Me.BusinessDate
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .ApplicationID = Me.ApplicationID

                '.SubsidiBungaATPM = CDbl(txtSubsidiBungaATPM.Text)
                '.SubsidiAngsuranATPM = CDbl(txtSubsidiAngsuranATPM.Text)
                '.SubsidiUangMuka = CDbl(txtSubsidiUangMuka.Text)
                '.SubsidiUangMukaATPM = CDbl(txtSubsidiUangMukaATPM.Text)
                '.DealerDiscount = CDbl(txtDealerDiscount.Text)
                '.BiayaMaintenance = CDbl(txtBiayaMaintenance.Text)
                '.RefundBungaPercent = CDec(txtRefundBunga.Text)
                '.RefundBungaAmount = CDbl(txtRefundBungaN.Text)
                .RefundPremiPercent = CDec(txtRefundPremi.Text)
                .RefundPremiAmount = CDbl(txtRefundPremiN.Text)
                .RefundProvisiPercent = CDec(txtRefundBiayaProvisi.Text)
                .RefundProvisiAmount = CDbl(txtRefundBiayaProvisiN.Text)
                .AlokasiInsentifBiayaLain = CDec(txtRefundLainN.Text)
                .AlokasiInsentifBiayaLainPercent = CDbl(txtRefundLain.Text)
                '.SubsidiBungaDealer = CDbl(txtSubsidiBungaDealer.Text)
                '.SubsidiAngsuran = CDbl(txtSubsidiAngsuran.Text)

                .RateIRR = RateIRR
                .ExcludeAmount = ExcludeAmount
                '.SupplierID = txtSupplierCode.Text.ToString.Trim
                '.SupplierName = txtSupplierName.Text.ToString.Trim

                .AdminFee = AdminFee
                .ProvisionFee = ProvisionFee
                .SubsidiBunga = SubsidiBunga
                .IncomePremi = IncomePremi
                .OtherFee = OtherFee
                .IncentiveDealer = IncentiveDealer
                .InsuranceRefund = InsuranceRefund
                .ProvisionRefund = ProvisionRefund
                .OtherRefund = OtherRefund

                '.SubsidiRisk = CDbl(txtSubsidiRisk.Text)
                '.SubsidiRiskATPM = CDbl(txtSubsidiRiskATPM.Text)
                '.RefundCreditProtectionPercent = CDec(txtRefundPremiCP.Text)
                '.RefundCreditProtectionAmount = CDbl(txtRefundPremiCPN.Text)
                '.RefundJaminanKreditPercent = CDec(txtRefundPremiJk.Text)
                '.RefundJaminanKreditAmount = CDbl(txtRefundPremiJKN.Text)
                '.RefundUppingBungaPercent = CDec(txtRefundUppingBunga.Text)
                '.RefundUppingBungaAmount = CDbl(txtRefundUppingBungaN.Text)
                '.SubsidiDPUppingBungaPercent = CDec(txtSubsidiDPUppingBunga.Text)
                '.SubsidiDPUppingBungaAmount = CDbl(txtSubsidiDPUppingBungaN.Text)

                '.DownPaymentMf = CDbl(UctxtUangMukaMF.Text)
                '.DPKaroseriAmountMF = CDbl(UctxtUangMukaKaroseriMF.Text)
                .AdminFeeMf = CDbl(uctxtAdminFeeMF.Text)
                '.FiduciaFeeMf = CDbl(UctxtBiayaFidusiaMF.Text)
                .OtherFeeMf = CDbl(UctxtOtherFeeMf.Text)
                .ProvisionFeeMf = CDbl(UctxtBiayaProvisiMF.Text)

                .AsuransiTunaiMF = CDbl(uctxtasuransitunaiMF.Text)
                '.AngsuranPertamaMf = CDbl(uctxtAngsuranPertamaMF.Text)

            End With

            oFinancialData = m_controller.SaveFinancialDataTab2(oFinancialData)

        Catch ex As Exception
            ShowMessage(lblMessage, "error - " & ex.Message, True)
            Return False
        End Try
        Return True

    End Function



End Class