﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class ViewPaymentHistoryModalKerjaData
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplicationMK
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucMailingAddress As UcCompanyAddress
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabModalKerja
    Private m_ControllerApp As New ApplicationController
    Private oModalKerjaInvoice As New Parameter.ModalKerjaInvoice
    Private m_controller As New ModalKerjaInvoiceController
#End Region

#Region "Constanta"
    Dim oRow As DataRow
    Dim Status As Boolean
#End Region

#Region "Properties"
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property

    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        lblMessage.Text = ""
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()

                .initControls("OnNewApplication")

                Me.BranchID = .BranchID
                Me.CustomerID = .CustomerID
            End With
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            Mode = Request("Page")

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("PaymentHistory")
            ucApplicationTab1.setLink()
            DoBind()
        End If
    End Sub

    Private Sub DoBind()

        Dim oController As New UCPaymentInfoController
        Dim oCustomClass As New Parameter.AccMntBase
        Dim oclass As New Parameter.ModalKerjaInvoice
        With oclass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With

        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .ValueDate = ConvertDate2(Me.BusinessDate.ToString("dd/MM/yyyy"))
            .BranchId = Me.BranchID
        End With

        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication.ApplicationID = Me.ApplicationID
        oApplication = m_ControllerApp.GetApplicationEdit(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        Dim oRow As DataRow = oData.Rows(0)
        Me.FacilityNo = oRow("NoFasilitas").ToString

        If (oRow("ContractStatus").ToString = "AKT") Then
            oModalKerjaInvoice.strConnection = GetConnectionString()
            oModalKerjaInvoice.BranchId = Me.BranchID
            oModalKerjaInvoice.ApplicationID = Me.ApplicationID
            oModalKerjaInvoice = m_controller.GetPaymentHistory(oModalKerjaInvoice)

            ItemData = oModalKerjaInvoice.Listdata
            dtgPaymentHistory.DataSource = ItemData.DefaultView
            dtgPaymentHistory.DataBind()
        End If
    End Sub

End Class