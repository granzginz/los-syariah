﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="IncentiveDataModalKerja.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.IncentiveDataModalKerja" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTabModalKerja.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/UcIncentiveGridFACT.ascx" TagName="UcIncentiveGrid"
    TagPrefix="uc1" %>
<%--<%@ Register Src="../../../../webform.UserController/UcIncentiveInternalGrid.ascx" TagName="UcIncentiveInternalGrid"
    TagPrefix="uc1" %>--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ENTRI REFUND - INSENTIF</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" />

    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script type='text/javascript' language='javascript'>
        $(function () {
            $(".accordion").accordion();

        });
    </script>
    <script type='text/javascript' language='javascript'>

        function AddNewRecordInternal(clientid) {
            var grd = document.getElementById(clientid);
            if (grd.rows.length <= 2) {
                return false;
            }

            var tbod = grd.rows[0].parentNode;

            var htmlRoot = grd.rows[grd.rows.length - 1].cloneNode(true);

            grd.deleteRow(grd.rows.length - 1);

            var sel = grd.rows[grd.rows.length - 1].getElementsByTagName('select')[0];
            var sel_ = sel.value;

            var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);

            var len = grd.rows.length;
            newRow.cells[0].innerHTML = len;

            var inp1 = newRow.cells[1].getElementsByTagName('select')[0];
            inp1.id += len;
            inp1.value = '';

            var inp2 = newRow.cells[2].getElementsByTagName('input')[0];
            inp2.id += len;
            inp2.value = '';

            var inp3 = newRow.cells[3].getElementsByTagName('input')[0];
            inp3.id += len;
            inp3.value = '';

            var arrClientID = clientid.split("_");
            var nilaiAlokasi;

            if (arrClientID[0] == "UcTitipanRefundBunga") {
                nilaiAlokasi = $('#lblTitipanRefundBunga').html();
            } else if (arrClientID[0] == "UcTitipanProvisi") {
                nilaiAlokasi = $('#lblTitipanProvisi').html();
            } else if (arrClientID[0] == "UcTitipanBiayaLain") {
                nilaiAlokasi = $('#lblTitipanBiayaLain').html();
            }

            inp2.setAttribute("OnChange", "handletxtProsentaseInternal_Change(this.value,'" + nilaiAlokasi + "','" + inp3.id + "')");
            inp3.setAttribute("OnChange", "handletxtInsentifInternal_Change(this.value,'" + nilaiAlokasi + "','" + inp2.id + "')");

            // grd.appendChild(newRow);
            $('#' + clientid).append(newRow);

            if (sel.value != "") {
                $("#" + inp1.id + " option[value=" + sel.value + "]").hide();
            }
            $('#' + clientid).append("<tr class='item_grid'>" + htmlRoot.innerHTML + "</tr>");
            return false;
        }
        function AddNewRecordJQ(clientid) {
            var grd = document.getElementById(clientid);
            var tbod = grd.rows[0].parentNode;

            if (grd.rows.length <= 2) {
                return false;
            }

            var htmlRoot = grd.rows[grd.rows.length - 1].cloneNode(true);

            grd.deleteRow(grd.rows.length - 1);


            var newRow = grd.rows[grd.rows.length - 1].cloneNode(true);

            var len = grd.rows.length;
            newRow.cells[0].innerHTML = len;


            var inp2 = newRow.cells[2].getElementsByTagName('select')[0];
            inp2.id += len;
            inp2.value = '';

            var inp1 = newRow.cells[1].getElementsByTagName('select')[0];
            inp1.id += len;
            inp1.value = '';


            //            var inp6 = newRow.cells[6].getElementsByTagName('span')[0];
            //            inp6.id += len;
            //            inp6.value = '';

            //            var inp7 = newRow.cells[7].getElementsByTagName('span')[0];
            //            inp7.id += len;
            //            inp7.value = '';



            var inp3 = newRow.cells[3].getElementsByTagName('input')[0];
            inp3.id += len;
            inp3.value = '';

            var inp4 = newRow.cells[4].getElementsByTagName('input')[0];
            inp4.id += len;
            inp4.value = '';

            var arrClientID = clientid.split("_");
            var nilaiAlokasi;

            if (arrClientID[0] == "UcPremiAsuransi") {
                nilaiAlokasi = $('#lblRefundPremi').html();
            } else if (arrClientID[0] == "UcProgresifPremiAsuransi") {
                nilaiAlokasi = $('#lblProgresifPremiAsuransi').html();
            } else if (arrClientID[0] == "UcRefundBunga") {
                nilaiAlokasi = $('#lblRefundBunga').html();
            } else if (arrClientID[0] == "UcPremiProvisi") {
                nilaiAlokasi = $('#lblRefundBiayaProvisi').html();
            } else if (arrClientID[0] == "UcBiayaLain") {
                nilaiAlokasi = $('#lblRefundBiayaLain').html();
            }

            //var inp5 = newRow.cells[5].getElementsByTagName('span')[0];
            var inp5 = newRow.cells[5].getElementsByTagName('select')[0];
            inp5.id += len;
            inp5.value = '';


            //            var inp8 = newRow.cells[8].getElementsByTagName('span')[0];
            //            inp8.id += len;
            //            inp8.value = '';

            //            var inp9 = newRow.cells[9].getElementsByTagName('span')[0];
            //            inp9.id += len;
            //            inp9.value = '';

            var inp6 = newRow.cells[6].getElementsByTagName('select')[0];
            inp6.id += len;


            inp1.setAttribute("OnChange", "handleddlJabatan_Change(this,'" + inp2.id + "')");
            //inp1.setAttribute("OnChange", "handleddlJabatan_Change(this,'" + inp2.id + "');getPPH(this.value,'" + inp6.id + "','" + inp7.id + "')");
            //inp2.setAttribute("OnChange", "handleddlpenerima_Change(this.value,'" + inp1.id + "','" + inp7.id + "')");
            inp3.setAttribute("OnChange", "handletxtProsentase_Change(this.value,'" + inp6.id + "','" + inp4.id + "')");
            inp4.setAttribute("OnChange", "handletxtInsentif_Change(this.value,'" + inp6.id + "','" + inp3.id + "')");

            // grd.appendChild(newRow);
            $('#' + clientid).append(newRow);
            $('#' + clientid).append("<tr class='item_grid'>" + htmlRoot.innerHTML + "</tr>");
            return false;
        }
        function btnSaveClick() {
            //alert(window.location.href + "&btnSave=1");
            window.location.href = window.location.href + "&btnSave=1";
        }
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="ScriptManager1">
        </asp:ScriptManager>
        <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
        <asp:UpdatePanel runat="server" ID="updatePanel1">
            <contenttemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_box_header">
                <div class="accordion">
                    <h3><label class="drop_down">ALOKASI REFUND PREMI  &nbsp;&nbsp;<asp:Label runat="server" ID="lblRefundPremi" />
                      &nbsp;&nbsp;<asp:Label runat="server" ID="lblRefundPremiPersen" /></h3>
                    <div><p><uc1:ucincentivegrid id="UcRefundPremi" runat="server" /></p></div> 
                    <h3><label class="drop_down">ALOKASI REFUND BIAYA LAINNYA  &nbsp;&nbsp;<asp:Label runat="server" ID="lblRefundBiayaLain" />
                        &nbsp;&nbsp;<asp:Label runat="server" ID="lblRefundBiayaLainPersen" /></h3>
                    <div><p><uc1:ucincentivegrid id="UcRefundBiayaLain" runat="server" /></p></div>
                    <h3><label class="drop_down">ALOKASI REFUND BIAYA PROVISI  &nbsp;&nbsp;<asp:Label runat="server" ID="lblRefundBiayaProvisi" />
                        &nbsp;&nbsp;<asp:Label runat="server" ID="lblRefundBiayaProvisiPersen" /></h3>
                    <div><p><uc1:ucincentivegrid id="UcRefundBiayaProvisi" runat="server" /></p></div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>SUMMARY INSENTIF PER ORANG &nbsp;&nbsp;<asp:Label runat="server" ID="Label1" /></label>
                </div>
            </div>
            <div class="form_box_header">
                    <div>
                        <p>
                            <div class="form_box_header">
                                <div class="form_single">
                                    <div class="grid_wrapper_ns">
                                        <asp:DataGrid ID="dtgSum" runat="server" Width="100%" AutoGenerateColumns="False"
                                            BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                            <HeaderStyle CssClass="th" />
                                            <ItemStyle CssClass="item_grid" />
                                            <FooterStyle CssClass="item_grid" />
                                            <Columns>
                                                <asp:BoundColumn HeaderText="JABATAN" DataField="EmployeePosition"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="PENERIMA" DataField="EmployeeName"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="INSENTIF" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="th_right" DataField="nilaiAlokasi" DataFormatString="{0:N0}"></asp:BoundColumn>                                                    
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>                                    
                            </div>
                            <p>
                            </p>
                        </p>
                    </div>

            </div>
            <asp:Panel runat="server" ID="pnlBalance">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        JOURNAL 
                    </h4>
                </div>
            </div>
            <div class="form_box">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgJournal" runat="server" CellSpacing="1" CellPadding="3" ShowFooter="true"
                    AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_general">                    
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <FooterStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemStyle CssClass="short_col"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA">     
                            <ItemStyle CssClass="middle_col" />
                            <ItemTemplate>                                
                                <asp:Label ID="lblPaymentAllocationDes" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.PaymentAllocationDes") %>'>
                                </asp:Label>
                                <asp:Label ID="lblPaymentAllocationID" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.PaymentAllocationID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblRefDesc" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.RefDesc") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                Total 
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DEBET" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                                    FooterStyle-CssClass="item_grid_right ft">                                
                            <ItemTemplate>                                
                                <asp:Label ID="lblPost" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.Post") %>'>
                                </asp:Label>
                                <asp:Label ID="lblDebet" runat="server">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblDebetTotal" runat="server">
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KREDIT" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                                    FooterStyle-CssClass="item_grid_right ft">                                                                              
                            <ItemTemplate>                                
                                <asp:Label ID="lblAmount" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.Amount") %>'>
                                </asp:Label>
                                <asp:Label ID="lblKredit" runat="server">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblKreditTotal" runat="server">
                                </asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
            </asp:Panel>
            </label>
            </label>
            </label>
        </contenttemplate>
        </asp:UpdatePanel>
        <div class="form_button">
            <a id="btnSave" class="small button blue" onclick="btnSaveClick()">Save</a>
        </div>
    </form>
</body>
</html>
