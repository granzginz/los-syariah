﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InvoiceDataModalKerja.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.InvoiceDataModalKerja" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerFacility.ascx" TagName="ucViewCustomerFacility" 
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%--<%@ Register Src="../../../../webform.UserController/ucApplicationTabFactoring.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>--%>
<%@ Register Src="../../../../webform.UserController/ucApplicationTabModalKerja.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/UcBankAccount.ascx" TagName="UcBankAccount" TagPrefix="uc"   %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invoice Data</title>
   
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    

</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnlSupplier">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnlSupplier" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <uc1:ucViewCustomerFacility id="ucViewCustomerFacility1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h4>ENTRI INVOICE DATA</h4>
                </div>
            </div>
           <div class="form_box">
                <div class="form_left">
                    <label>Invoice No.Batch</label>
                    <asp:textbox id="txtInvoiceNoBatch" runat="server" TextCssClass="small_text" AutoPostBack="true"></asp:textbox>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtInvoiceNoBatch"
                        Enabled="true" ErrorMessage="*" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                <label  runat="server">Tanggal Jatuh Tempo </label>
                    <asp:TextBox ID="txtduedate" runat="server" CssClass="small_text" AutoPostBack="true" ></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="txtduedate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtduedate"
                        Enabled="true" ErrorMessage="Harap isi Tanggal Jatuh Tempo!" CssClass="validator_general"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Angsuran Anuitas</label>
                    <%--<asp:CheckBox ID="cbAnuitas" runat="server" AutoPostBack="true" causedvalidation="true"/>--%>
                    <asp:CheckBox ID="cbAnuitas" runat="server" AutoPostBack="true"/>
                </div>
                <div class="form_right">
                </div>
            </div>

            <asp:Panel runat="server" ID="UploadAnuitas"> 
                <div class="form_title">
                    <div class="form_single">
                        <h4>Upload Amortisasi Angsuran Anuitas</h4>
                    </div>
                </div>
        <asp:UpdatePanel runat="server" ID="up3">
        <ContentTemplate>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="Datagrid1" runat="server" Width="100%" CellSpacing="1" CellPadding="3"
                            BorderWidth="0" AutoGenerateColumns="False" DataKeyField="ApplicationID" CssClass="grid_general"  ShowFooter="True">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <FooterStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="No Aplikasi">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtApplicationID" runat="server" MaxLength="50"></asp:TextBox>
                                        <asp:Label ID="lblValidApplicationID" runat="server" Visible="False">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Cabang">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtBranchID" runat="server" MaxLength="25"></asp:TextBox>
                                        <asp:Label ID="lblValidBranchID" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Due Date">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <uc7:ucdatece id="ucDueDate" runat="server"></uc7:ucdatece>
                                        <%--<asp:TextBox ID="uscFamilyBirthDate" runat="server"></asp:TextBox>
                                        <asp:CalendarExtender ID="uscFamilyBirthDate_CalendarExtender" runat="server" Enabled="True"
                                            TargetControlID="uscFamilyBirthDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                            ControlToValidate="uscFamilyBirthDate" CssClass="validator_general"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                                            ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="uscFamilyBirthDate"
                                            SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
                                        </asp:RegularExpressionValidator>--%>
                                        <asp:Label ID="lblValidDueDate0" runat="server" Visible="False">Tanggal Lahir harus lebih kecil dari hari ini</asp:Label>
                                        <asp:Label ID="lblValidDueDate" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="Nilai Pokok">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtPrincipalAmount" runat="server" MaxLength="25" AutoPostBack="true"></asp:TextBox>
                                        <asp:Label ID="lblValidPrincipalAmount" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Nilai Bunga">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtInterestAmount" runat="server" MaxLength="25"></asp:TextBox>
                                        <asp:Label ID="lblInterestAmount" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="Rate Bunga">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtInterestRate" runat="server" MaxLength="25"></asp:TextBox>
                                        <asp:Label ID="lblInterestRate" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="No Invoice">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtInvoiceNo" runat="server" MaxLength="25"></asp:TextBox>
                                        <asp:Label ID="lblInvoiceNo" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" CausesValidation="false" CommandName="Delete" runat="server"
                                            ImageUrl="../../../../Images/icondelete.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:Button ID="btnPAdd" runat="server" Text="Add" CssClass="small buttongo blue"
                        CausesValidation="False"></asp:Button>
                </div>
            </div> 
        </ContentTemplate> 
            
    </asp:UpdatePanel>

            </asp:Panel>

            <div class="form_title">
                <div class="form_single">
                    <h4>ENTRI INVOICE DATA DETAIL</h4>
                </div>
            </div>
            <asp:HiddenField runat="server" ID="hdfTanggalPencairan" />
            <asp:HiddenField runat="server" ID="hdfApplicationID" />
            <div class="form_box">
                <div class="form_single">
                    <label>Tanggal Pencairan</label>
                    <asp:Label runat="server" ID="lblTanggalPencairan" ></asp:Label>    
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlListInvoice">
            <div class="form_button">
                <asp:Button ID="btnAdd" runat="server" Text="Add Invoice" CssClass="small button green"  CausesValidation="False" />
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h5>DAFTAR INVOICE</h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" DataKeyField="InvoiceSeqNo" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InvoiceSeqNo" HeaderText="No" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" >
                                <ItemTemplate>
                                        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../../../../Images/icondelete.gif"
                                            CommandName="Delete" OnClientClick="return DeleteConfirm()"  OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# DataBinder.eval(Container, "DataItem.ApplicationID") & "," & DataBinder.eval(Container, "DataItem.InvoiceSeqNo")  %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--<asp:TemplateColumn HeaderText="Edit" HeaderStyle-Width="30px" >
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" ImageUrl="../../../../Images/iconedit.gif"
                                            CommandName="EditInvoice" OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# DataBinder.eval(Container, "DataItem.ApplicationID") & "," & DataBinder.eval(Container, "DataItem.InvoiceSeqNo")  %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>--%>
                                <asp:BoundColumn Visible="True" DataField="InvoiceNo" HeaderText="Invoice No" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="AccountNameTo" HeaderText="Invoice To" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDate" HeaderText="Inv Date"  DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDueDate" HeaderText="Inv Due Date"  DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceAmount" HeaderText="Invoice Amount"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="TotalPembiayaan" HeaderText="Dibiayai"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="PiutangDibiayai" HeaderText="Dibiayai (%)"  DataFormatString="{0:00.00} %"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Next" CssClass="small button blue"  CausesValidation="True" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
            </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlInputInvoice">
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Invoice No</label>
                    <asp:HiddenField runat="server" ID="hdfInvoiceSeqNo" />
                    <asp:HiddenField runat="server" ID="hdfInvoiceSeqNoH" />
                    <asp:TextBox runat="server" ID="txtInvoiceNo" ></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ID="rfvInvoiceNo" ControlToValidate="txtInvoiceNo"
                        Enabled="true" ErrorMessage="*" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_single">    
                    <label class="label_req">Invoice Date</label>
                    <asp:TextBox ID="txtInvoiceDate" runat="server" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtInvoiceDate" Format="dd/MM/yyyy" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvInvoiceDate" ControlToValidate="txtInvoiceDate"
                        Enabled="true" ErrorMessage="*" CssClass="validator_general" Display="Dynamic" />
                </div>
            </div> 
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Invoice Due Date</label>
                    <asp:TextBox ID="txtInvoiceDueDate" runat="server" CssClass="small_text" OnChange="hitungUlang()" ></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtInvoiceDueDate" Format="dd/MM/yyyy" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvInvoiceDueDate" ControlToValidate="txtInvoiceDueDate"
                        Enabled="true" ErrorMessage="*" CssClass="validator_general" Display="Dynamic" />
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_single">
                    <label class="label_req">Invoice Amount</label>
                    <uc7:ucnumberformat runat="server" id="ucInvoiceAmount" OnClientChange="hitungUlang()"></uc7:ucnumberformat>
                </div>
            </div> 
            <div class="form_box_hide">
                 <div class="form_left">
                    <label class="">Retensi</label>
                     <asp:HiddenField runat="server" ID="hdfRentensiPercent" />
                     <uc7:ucnumberformat runat="server" id="ucRentensiPercent" width="50" OnClientChange="hitungUlang()"></uc7:ucnumberformat>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblRentensi"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_left">
                    <label class="">Tagihan yang dibiayai</label>
                        <uc7:ucnumberformat runat="server" id="UcTagihanDibiayai" width="50" OnClientChange="hitungUlang()"></uc7:ucnumberformat> %
                    <%--<asp:Label runat="server" ID="lblTagihanYangDibiayai" CssClass="regular_text"></asp:Label>
                        <asp:HiddenField runat="server" ID="hdfpersenbiaya" />--%>
                    <%--<div style="margin-left:-8px;display:inline">--%>
                </div>
                <div class="form_right">
                    <%--<asp:Label runat="server" ID="lbltotalYangDibiayai" CssClass="regular_text numberAlign2" >0</asp:Label>--%>
                    <uc7:ucnumberformat runat="server" id="UcTagihanDibiayaiAmount" OnClientChange="hitungUlangByRate()"></uc7:ucnumberformat>
                    <asp:HiddenField runat="server" ID="hdftotaldibiayai" />
                </div>                
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Bunga</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblBungaPercent" CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div> 
                 <div class="form_box">
                 <div class="form_left">
                    <label class="">Jangka Waktu</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblJangkaWaktu" CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div> 
            <div class="form_box">
                <div class="form_single">
                    <div class="form_title"><h4>DATA ACCOUNT</h4></div>
                    <uc:ucbankaccount id="UcBankAccount1" runat="server"></uc:ucbankaccount>
                </div>    
            </div>

            <div class="form_button">
                <asp:Button ID="btnSaveInvoice" runat="server" Text="Save" CssClass="small button blue"  CausesValidation="True" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false" />
                <asp:Button ID="btnCancelInvoice" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
            </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSaveInvoice" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cbAnuitas" EventName="CheckedChanged" />
            <%--<asp:AsyncPostBackTrigger ControlID="txtduedate" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="txtInvoiceNoBatch" EventName="TextChanged" />--%>
        </Triggers>
    </asp:UpdatePanel>
    </form>

    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('#UcBankAccount1_btnClearBankAccountChache').css('display', 'none');
            hitungUlang()
            hitungUlangByRate()
            hitungJangkaWaktu()
        });

        function hitungUlang()
        {
            
            //Get value Amount
            let AmountString = document.getElementById('ucInvoiceAmount_txtNumber').value;
            let Amount = parseInt(AmountString.replace(/\s*,\s*/g, ''));
            console.log(Amount)

            //Get value PercentAmount
            let PercentStr = document.getElementById('UcTagihanDibiayai_txtNumber').value;
            let PercentAmount = parseFloat(PercentStr.replace(/\s*,\s*/g, ''));
            console.log(PercentAmount)
            
            let totalbiaya = Amount * PercentAmount / 100;
            console.log(totalbiaya);

            $('#UcTagihanDibiayaiAmount_txtNumber').val(number_format(totalbiaya, 0));
            $('#<%=hdftotaldibiayai.ClientID %>').val(totalbiaya.toFixed(0));

            let jk = hitungJangkaWaktu();
            $('#lblJangkaWaktu').html(number_format(jk, 0));
            console.log(jk);

        }

        function hitungUlangByRate() {
            //Get value Amount
            let AmountString = document.getElementById('ucInvoiceAmount_txtNumber').value;
            let Amount = parseInt(AmountString.replace(/\s*,\s*/g, ''));
            console.log(Amount)

            //Get value rateAmount
            let getRateAmount = document.getElementById('UcTagihanDibiayaiAmount_txtNumber').value;
            let rateAmount = parseFloat(getRateAmount.replace(/\s*,\s*/g, ''));
            console.log(rateAmount)

            let Rate = (rateAmount / Amount) * 100
            console.log(Rate)
            $('#UcTagihanDibiayai_txtNumber').val(number_format(Rate, 2));

            let totalb = Amount * Rate / 100;
            let totalbiaya = parseInt(totalb.replace(/\s*,\s*/g, ''));
            console.log(totalbiaya);

            $('#UcTagihanDibiayaiAmount_txtNumber').val(number_format(totalbiaya, 0));
            $('#<%=hdftotaldibiayai.ClientID %>').val(totalbiaya.toFixed(0));

            let jk = hitungJangkaWaktu();
            $('#lblJangkaWaktu').html(number_format(jk, 0));
            console.log(jk);  
        }



        function hitungJangkaWaktu()
        {
            var _tglCair = document.getElementById('hdfTanggalPencairan').value;
            var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);
            console.log(tglCair)

            var _invoiceDueDate = document.getElementById('txtInvoiceDueDate').value;
            dateString = _invoiceDueDate.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var invoiceDueDate = new Date(dateString[3], dateString[2] - 1, dateString[1]);
            console.log(invoiceDueDate)

            var days = daysBetween(tglCair, invoiceDueDate);
            return days;
        }

        function treatAsUTC(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }

        function daysBetween(startDate, endDate) {
            var millisecondsPerDay = 24 * 60 * 60 * 1000;
            return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
        }


        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

</body>
</html>
