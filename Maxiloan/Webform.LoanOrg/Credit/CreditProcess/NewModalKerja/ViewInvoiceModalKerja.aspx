﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewInvoiceModalKerja.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewInvoiceModalKerja" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>

<%@ Register Src="../../../../webform.UserController/ucViewApplicationMK.ascx" TagName="ucViewApplicationMK"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabModalKerja.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc7" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invoice Data</title>
   
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
       
        function hitungUlang()
        {
            var bungaPercent = document.getElementById('hdfBungaPercent').value;
            var retensiPercent = document.getElementById('hdfRentensiPercent').value;
            var AmountString = document.getElementById('ucInvoiceAmount_txtNumber').value;
            var Amount = parseInt(AmountString.replace(/\s*,\s*/g, ''));
            var jk = hitungJangkaWaktu();

            var retensi = Amount * retensiPercent / 100;
            var dibiayai = Amount - retensi;
            var bunga = (dibiayai * bungaPercent / 100) * jk / 365;
            
            $('#lblJangkaWaktu').html(jk + ' days');
            $('#lblRentensi').html(number_format(retensi, 0));
            $('#lblBunga').html(number_format(bunga, 0));
            $('#lblTagihanYangDibiayai').html(number_format(dibiayai, 0));
        }

        function hitungJangkaWaktu()
        {
            var _tglCair = document.getElementById('hdfTanggalPencairan').value;
            var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);

            var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);

            var _invoiceDueDate = document.getElementById('txtInvoiceDueDate').value;
            dateString = _invoiceDueDate.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var invoiceDueDate = new Date(dateString[3], dateString[2] - 1, dateString[1]);

            var days = daysBetween(tglCair, invoiceDueDate);
            return days;
        }

        function treatAsUTC(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }

        function daysBetween(startDate, endDate) {
            var millisecondsPerDay = 24 * 60 * 60 * 1000;
            return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
        }


        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnlSupplier">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnlSupplier" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        INVOICE DATA</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc3:ucViewApplicationMK id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            
            <asp:HiddenField runat="server" ID="hdfTanggalPencairan" />
            <div class="form_box">
                <div class="form_single">
                    <label>Supplier/Dealer</label>
                    <asp:HiddenField runat="server" ID="hdfApplicationID" />
                    <asp:Label runat="server" ID="lblSupplierName" ></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>Tanggal Pencairan</label>
                    <asp:Label runat="server" ID="lblTanggalPencairan" ></asp:Label>    
                </div>
            </div>

            <asp:Panel runat="server" ID="pnlListInvoice">
            <div class="form_title">
                <div class="form_single">
                    <h5>DAFTAR INVOICE</h5>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" DataKeyField="InvoiceSeqNo" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InvoiceSeqNo" HeaderText="No" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceNo" HeaderText="Invoice No" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="AccountNameTo" HeaderText="Invoice To" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDate" HeaderText="Inv Date"  DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceDueDate" HeaderText="Inv Due Date"  DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InvoiceAmount" HeaderText="Invoice Amount"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="TotalPembiayaan" HeaderText="Dibiayai"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="PiutangDibiayai" HeaderText="Dibiayai (%)"  DataFormatString="{0:0,0}" ></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
             <div class="tab_container_form_space"><br /><br /><br /><br />
                </div>
                <div class="tab_container_button">
                    <div class="form_button">
                        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                    </div>
                </div>
            </asp:Panel>

          
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
