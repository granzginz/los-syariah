﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ApplicationModalKerja_003
    
    '''<summary>
    '''upg1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upg1 As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm
    
    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager
    
    '''<summary>
    '''jlookupContent control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents jlookupContent As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''upnl1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upnl1 As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''hdnGridGeneralSize control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnGridGeneralSize As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    
    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblKegiatanUsaha control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblKegiatanUsaha As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cboKegiatanUsaha control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboKegiatanUsaha As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Requiredfieldvalidator65 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator65 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''cboCaraBayar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboCaraBayar As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''cboAkad control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboAkad As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Requiredfieldvalidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator1 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''cbocmo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cbocmo As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''cboJenisPembiyaan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboJenisPembiyaan As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Requiredfieldvalidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Requiredfieldvalidator3 As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''ucDenda control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucDenda As Global.System.Web.UI.UserControl
    
    '''<summary>
    '''cboPayment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboPayment As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblTanggalPencairan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTanggalPencairan As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''txtTanggalPencairan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTanggalPencairan As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''CalendarExtender1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents CalendarExtender1 As Global.AjaxControlToolkit.CalendarExtender
    
    '''<summary>
    '''rfvTanggalPencairan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvTanggalPencairan As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''btnNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNext As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''pnlDataAplikasi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlDataAplikasi As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''txtAppNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAppNotes As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cboCopyAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboCopyAddress As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''hdfkabupaten control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdfkabupaten As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''txtKabupaten control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtKabupaten As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''rfvtxtKabupaten control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rfvtxtKabupaten As Global.System.Web.UI.WebControls.RequiredFieldValidator
    
    '''<summary>
    '''dtgTC control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgTC As Global.System.Web.UI.WebControls.DataGrid
    
    '''<summary>
    '''dtgTC2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgTC2 As Global.System.Web.UI.WebControls.DataGrid
    
    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnCancel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCancel As Global.System.Web.UI.WebControls.Button
End Class
