﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FinancialModalKerjaData.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.FinancialModalKerjaData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../../webform.UserController/ValidDate.ascx" %>

<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTabModalKerja.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerFacility.ascx" TagName="ucViewCustomerFacility" 
    TagPrefix="uc1" %>
<%@ Register Src="../../../../Webform.UserController/ucInsuranceBranchName.ascx"
    TagName="ucInsuranceBranchName" TagPrefix="uc4" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Financial Data</title>
   
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
       
        function hitungUlang()
        {
            var UcBiayaAdmin = document.getElementById('UcBiayaAdmin_txtNumber').value;
            var UcBiayaProvisi = document.getElementById('UcBiayaProvisi_txtNumber').value;
            var UcBiayaSurvey = document.getElementById('UcBiayaSurvey_txtNumber').value;
            var UcDiscountCharges = document.getElementById('UcDiscountCharges_txtNumber').value;
            var UcBiayaHandling = document.getElementById('UcBiayaHandling_txtNumber').value;
            var UcAsuransiKredit = document.getElementById('UcAsuransiKredit_txtNumber').value;
            var UcBiayaNotaris = document.getElementById('UcBiayaNotaris_txtNumber').value;
            var UcBiayaPolis = document.getElementById('UcBiayaPolis_txtNumber').value;

            var hdfTotalPlafond = parseFloat(document.getElementById('hdfTotalPlafond').value);
            
            var hdfTotalInvoice = parseFloat(document.getElementById('hdfTotalInvoice').value);
            //var hdfPPN = parseInt(document.getElementById('hdfPPN').value);

            var BiayaAdmin = parseFloat(UcBiayaAdmin.replace(/\s*,\s*/g, ''));
            var BiayaProvisi = (parseFloat(UcBiayaProvisi.replace(/\s*,\s*/g, '')) / 100) * hdfTotalInvoice;
            var BiayaHandling = (parseFloat(UcBiayaHandling.replace(/\s*,\s*/g, '')) / 100) * hdfTotalInvoice;
            var AsuransiKredit = (parseFloat(UcAsuransiKredit.replace(/\s*,\s*/g, '')) / 100) * hdfTotalInvoice;
            //var BiayaPolis = (parseFloat(UcBiayaPolis.replace(/\s*,\s*/g, '')) / 100);
            var BiayaPolis = (parseFloat(UcBiayaPolis.replace(/\s*,\s*/g, '')));

            var BiayaSurvey = parseFloat(UcBiayaSurvey.replace(/\s*,\s*/g, ''));
            var DiscountCharges = parseFloat(UcDiscountCharges.replace(/\s*,\s*/g, ''));
            var BiayaNotaris = parseFloat(UcBiayaNotaris.replace(/\s*,\s*/g, ''));

            var total = BiayaAdmin + BiayaNotaris + BiayaProvisi + BiayaSurvey + AsuransiKredit + BiayaHandling + BiayaPolis - DiscountCharges;
            //var ppn = (hdfPPN / 100) * (total);
            var totalbiaya = BiayaAdmin + BiayaNotaris + BiayaProvisi + BiayaSurvey + AsuransiKredit + BiayaHandling + BiayaPolis - DiscountCharges;
            //var totalcair = hdfTotalInvoice - total - ppn;

            //$('#lblPPN').html(number_format(ppn, 0));
            $('#lblTotalBiaya').html(number_format(totalbiaya, 0));
            //$('#lblJumlahPencairan').html(number_format(totalcair, 0));
            $('#lblBiayaProvisi').html(number_format(BiayaProvisi, 0));
            $('#lblBiayaHandling').html(number_format(BiayaHandling, 0));
            $('#lblAsuransiKredit').html(number_format(AsuransiKredit, 0));
        }

        function hitungJangkaWaktu()
        {
            var _tglCair = document.getElementById('hdfTanggalPencairan').value;
            var dateString = _tglCair.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);

            var tglCair = new Date(dateString[3], dateString[2] - 1, dateString[1]);

            var _invoiceDueDate = document.getElementById('txtInvoiceDueDate').value;
            dateString = _invoiceDueDate.match(/^(\d{2})\/(\d{2})\/(\d{4})$/);
            var invoiceDueDate = new Date(dateString[3], dateString[2] - 1, dateString[1]);

            var days = daysBetween(tglCair, invoiceDueDate);
            return days;
        }

        function treatAsUTC(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        }

        function daysBetween(startDate, endDate) {
            var millisecondsPerDay = 24 * 60 * 60 * 1000;
            return (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay;
        }


        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnlSupplier">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnlSupplier" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <uc1:ucViewCustomerFacility id="ucViewCustomerFacility1" runat="server" />
            <asp:HiddenField runat="server" ID="hdfApplicationID" />
            <div class="form_title">
                <div class="form_single">
                    <h4>ENTRI FINANCIAL DATA</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>Tanggal Pencairan</label>
                    <asp:Label runat="server" ID="lblTanggalPencairan" ></asp:Label>    
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Total Invoice</label>
                     <div style="margin-left:-8px;display:inline">
                         <asp:HiddenField runat="server" ID="hdfTotalPlafond" />
                         <asp:HiddenField runat="server" ID="hdfTotalInvoice" />
                        <asp:Label runat="server" ID="lblTotalInvoice"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
                <div class="form_right">
                    <label>Dari invoice sebanyak</label>
                    <asp:Label ID="lblInvoiceCount" runat="server" />
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Jumlah Pencairan</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblJumlahPencairan"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <h4>Biaya-biaya</h4>
                </div>
            </div> 
             <div class="form_box">
                <div class="form_single">
                    <label class="">
                        Perusahaan Asuransi
                    </label>
                    <asp:DropDownList ID="cboInsuranceComBranch" runat="server" AutoPostBack="true"> </asp:DropDownList>
                    <%--<uc4:ucinsurancebranchname runat="server" id="cmbInsuranceComBranch"></uc4:ucinsurancebranchname> 
                  <asp:Label ID="LblFocusMaskAssID" runat="server"></asp:Label>
                    <asp:Label ID="LblFocusInsuranceComBranchName" runat="server"></asp:Label>--%>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="">
                        Tipe Rate Asuransi
                    </label>
                    <asp:DropDownList ID="cboRateType" runat="server" AutoPostBack="true"> </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Asuransi Pembiayaan</label>
                    <uc7:ucnumberformat runat="server" id="UcAsuransiKredit" OnClientChange="hitungUlang()"></uc7:ucnumberformat>
                     <asp:Label runat="server" ID="lblAsuransiKredit"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Polis</label>
                    <uc7:ucnumberformat runat="server" id="UcBiayaPolis" OnClientChange="hitungUlang()" TextCssClass="numberAlign2"></uc7:ucnumberformat>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Admin</label>
                    <uc7:ucnumberformat runat="server" id="UcBiayaAdmin" OnClientChange="hitungUlang()" TextCssClass="numberAlign2"></uc7:ucnumberformat>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Provisi / Annual</label>
                    <uc7:ucnumberformat runat="server" id="UcBiayaProvisi" OnClientChange="hitungUlang()"></uc7:ucnumberformat>
                     <asp:Label runat="server" ID="lblBiayaProvisi"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Handling</label>
                    <uc7:ucnumberformat runat="server" id="UcBiayaHandling" OnClientChange="hitungUlang()"></uc7:ucnumberformat>
                     <asp:Label runat="server" ID="lblBiayaHandling"  CssClass="numberAlign2 regular_text" ></asp:Label>
                </div>
            </div>
           
            
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Notaris</label>
                    <uc7:ucnumberformat runat="server" id="UcBiayaNotaris" OnClientChange="hitungUlang()" TextCssClass="numberAlign2"></uc7:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Biaya Survey</label>
                    <uc7:ucnumberformat runat="server" id="UcBiayaSurvey" OnClientChange="hitungUlang()" TextCssClass="numberAlign2"></uc7:ucnumberformat>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Discount charges</label>
                    <uc7:ucnumberformat runat="server" id="UcDiscountCharges" OnClientChange="hitungUlang()" TextCssClass="numberAlign2"></uc7:ucnumberformat>
                </div>
            </div> 
            <div class="form_box">
                 <div class="form_left">
                    <label class="">Total Biaya</label>
                     <div style="margin-left:-8px;display:inline">
                        <asp:Label runat="server" ID="lblTotalBiaya"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div>
            <%--<div class="form_box">
                 <div class="form_left">
                    <label class="">PPN</label>
                     <div style="margin-left:-8px;display:inline">
                         <asp:HiddenField runat="server" ID="hdfPPN" />
                         <asp:Label runat="server" ID="lblPPN"  CssClass="numberAlign2 regular_text" ></asp:Label>
                     </div>
                </div>
            </div> --%>
            
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"  CausesValidation="True" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" />
                 <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="small button blue"  CausesValidation="True" />
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cboInsuranceComBranch" EventName="TextChanged" />
            <asp:AsyncPostBackTrigger ControlID="cboRateType" EventName="TextChanged" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
