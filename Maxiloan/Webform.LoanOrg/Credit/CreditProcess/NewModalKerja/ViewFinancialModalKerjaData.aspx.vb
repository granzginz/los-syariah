﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class ViewFinancialModalKerjaData
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplicationMK
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabModalKerja
    Protected WithEvents ucViewCustomerFacility1 As ucViewCustomerFacility
#End Region

#Region "Constanta"
    Dim style As String = "ACCACQ"
    Private oApplication As New Parameter.Application
    Private oFactoringInvoice As New Parameter.ModalKerjaInvoice
    Private m_ControllerApp As New ApplicationController
    Private mfact_controller As New FactoringInvoiceController
    Private m_controller As New ModalKerjaInvoiceController
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property InvoiceNo() As String
        Get
            Return ViewState("InvoiceNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InvoiceNo") = Value
        End Set
    End Property
    Property InvoiceSeqNo() As Integer
        Get
            Return ViewState("InvoiceSeqNo")
        End Get
        Set(ByVal Value As Integer)
            ViewState("InvoiceSeqNo") = Value
        End Set
    End Property
    Property SupplierID() As String
        Get
            Return ViewState("SupplierID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property ProductOfferingID() As String
        Get
            Return ViewState("ProductOfferingID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property
    Property Mode() As String
        Get
            Return ViewState("Mode").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Mode") = Value
        End Set
    End Property
    Property ApplicationData() As DataTable
        Get
            Return ViewState("ApplicationData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ApplicationData") = Value
        End Set
    End Property
    Property ItemData() As DataTable
        Get
            Return ViewState("ItemData")
        End Get
        Set(ByVal Value As DataTable)
            ViewState("ItemData") = Value
        End Set
    End Property
    Property FacilityNo() As String
        Get
            Return ViewState("FacilityNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("FacilityNo") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            hdfApplicationID.Value = Me.ApplicationID
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()

                .initControls("OnNewApplication")

                Me.BranchID = .BranchID
                Me.CustomerID = .CustomerID
                Me.ProductOfferingID = .ProductOfferingID
                Me.SupplierID = .SupplierID
            End With

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            Mode = Request("Page")

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial")
            ucApplicationTab1.setLink()

            Me.InvoiceSeqNo = 0
            Me.InvoiceSeqNo = Request("invseqno")
            BindDataEdit()

        End If
    End Sub
#End Region

    Private Sub BindDataEdit()
        Dim oData, fData As New DataTable
        Dim oApp = New Parameter.FactoringInvoice
        Dim AdminFee, NotaryFee, ProvisionFee, SurveyFee, sukubunga, plafond, tenor, ProvisionAmount, HandlingFeePercent, HandlingFee As Decimal
        Dim InsuranceFee, BiayaPolis, FactoringDiscountCharges As Decimal
        Dim MaskAssBranchName, InsuranceMasterHPPRateID As String
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication.ApplicationID = Me.ApplicationID
        oApplication = m_ControllerApp.GetViewApplication(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
            ApplicationData = oData
        End If
        Dim oRow As DataRow = ApplicationData.Rows(0)
        Dim totalbiaya, totalPembiayaan As Decimal
        plafond = oRow("NTF")
        sukubunga = oRow("FactoringInterest")
        totalPembiayaan = oRow("TotalPembiayaan")
        tenor = oRow("Tenor")
        'HandlingFeePercent = oRow("HandlingFeePercent")
        'UcBiayaProvisiPercent = oRow("ProvisiPercent")
        UcSukuBunga.Text = FormatNumber(sukubunga, 0)
        UcPlafond.Text = FormatNumber(plafond, 0)
        UcTtlPembiayaan.Text = FormatNumber(totalPembiayaan, 0)
        UcMasaBerlaku.Text = FormatNumber(tenor, 0)



        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Me.BranchID
        oApplication.ApplicationID = Me.ApplicationID
        oApp = mfact_controller.GetFactoringFee(oApplication)
        fData = oApp.Listdata
        If (fData.Rows.Count > 0) Then
            Dim frow As DataRow = fData.Rows(0)
            HandlingFee = frow("HandlingFee")
            NotaryFee = frow("NotaryFee")
            AdminFee = frow("AdminFee")
            'ProvisionFee = frow("ProvisiPercent")
            ProvisionFee = oRow("ProvisionFee")
            SurveyFee = frow("SurveyFee")
            BiayaPolis = frow("BiayaPolis")
            InsuranceFee = frow("InsuranceFee")
            MaskAssBranchName = frow("MaskAssBranchName")
            InsuranceMasterHPPRateID = frow("InsuranceMasterHPPRateID")
            FactoringDiscountCharges = frow("FactoringDiscountCharges")


            UcBiayaAdmin.Text = FormatNumber(AdminFee, 0)
            'UcBiayaProvisiPercent.Text = FormatNumber(ProvisionFee, 2)
            'UcBiayaProvisiAmount.Text = FormatNumber(ProvisionAmount, 0)
            UcBiayaProvisi.Text = FormatNumber(ProvisionFee, 0)
            UcBiayaNotaris.Text = FormatNumber(NotaryFee, 0)
            UcBiayaSurvey.Text = FormatNumber(SurveyFee, 0)
            'UcHandlingFeePercent.Text = FormatNumber(HandlingFeePercent, 2)
            UcHandlingFee.Text = FormatNumber(HandlingFee, 0)
            UcDiscountCharges.Text = FormatNumber(FactoringDiscountCharges, 0)
            lblAsuransiKredit.Text = FormatNumber(InsuranceFee, 0)
            lblBiayaPolis.Text = FormatNumber(BiayaPolis, 0)
            LblFocusMaskAssIDName.Text = MaskAssBranchName.ToString
            lblRateTypeInsuranceDesc.Text = InsuranceMasterHPPRateID.ToString


            'totalbiaya = NotaryFee + AdminFee + SurveyFee + (ProvisionFee * plafond / 100)
            'totalbiaya = NotaryFee + AdminFee + SurveyFee + ProvisionAmount + HandlingFee 
            totalbiaya = HandlingFee + NotaryFee + AdminFee + ProvisionFee + SurveyFee + InsuranceFee + BiayaPolis - FactoringDiscountCharges
        Else
            UcBiayaAdmin.Text = 0
            UcBiayaProvisi.Text = 0
            UcBiayaNotaris.Text = 0
            UcBiayaSurvey.Text = 0
            UcHandlingFee.Text = 0
            UcDiscountCharges.Text = 0
            lblAsuransiKredit.Text = 0
            lblBiayaPolis.Text = 0
            LblFocusMaskAssIDName.Text = ""
            lblRateTypeInsuranceDesc.Text = ""

            totalbiaya = 0
        End If

        lblTotalBiaya.Text = FormatNumber(totalbiaya, 0)

        listFasilitasType.SelectedIndex = listFasilitasType.Items.IndexOf(listFasilitasType.Items.FindByValue(oRow("FacilityType").ToString))
        lbllistFasilitasType.Text = listFasilitasType.SelectedItem.Text
        cboInstScheme.SelectedIndex = cboInstScheme.Items.IndexOf(cboInstScheme.Items.FindByValue(oRow("InstallmentScheme").ToString))
        lblInstScheme.Text = cboInstScheme.SelectedItem.Text


        Me.FacilityNo = oRow("NoFasilitas").ToString
        loadDataFasilitas()
        Dim totalPlafond As Decimal = ucViewCustomerFacility1.FasilitasInfo.FasilitasAmount
        oFactoringInvoice.strConnection = GetConnectionString()
        oFactoringInvoice.BranchId = Me.BranchID
        oFactoringInvoice.ApplicationID = Me.ApplicationID

        oFactoringInvoice = m_controller.GetInvoiceList(oFactoringInvoice)

        ItemData = oFactoringInvoice.Listdata
        lblTanggalPencairan.Text = Format(IIf(String.IsNullOrEmpty(oRow("FactoringPencairanDate").ToString), "", oRow("FactoringPencairanDate")), "dd/MM/yyyy")
        Dim totalPencairanInvoice As Object = ItemData.Compute("SUM(InvoiceAmount)", String.Empty)
        Dim totalInvoice As Object = ItemData.Rows.Count

        lblTotalInvoice.Text = FormatNumber(Convert.ToDecimal(totalPencairanInvoice), 0)
        lblJumlahPencairan.Text = FormatNumber(totalPembiayaan, 0)
        lblInvoiceCount.Text = totalInvoice.ToString
    End Sub

    Private Sub loadDataFasilitas()
        With ucViewCustomerFacility1
            .FacilityNo = Me.FacilityNo
            .CustomerID = Me.CustomerID
            .LoadCustomerFacility()
        End With
    End Sub
End Class