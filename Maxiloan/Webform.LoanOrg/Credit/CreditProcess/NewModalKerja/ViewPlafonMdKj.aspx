﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPlafonMdKj.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewPlafonMdKj" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplicationMK.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabModalKerja.ascx" TagName="ucApplicationTab"
      TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerFacility.ascx" TagName="ucViewCustomerFacility" 
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Plafon Modal Kerja</title>
    <link href="../../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="form_single">
            <h3>
                View - Plafon Modal Kerja
            </h3>
        </div>
    </div>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
        <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" /> 
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
        <uc1:ucViewCustomerFacility id="ucViewCustomerFacility1" runat="server" />
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DAFTAR KONTRAK
            </h4>
        </div>
    </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
        <asp:DataGrid ID="DtgList" AutoGenerateColumns="False" DataKeyField="ApplicationID"
            BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" BackColor="White"
            CellPadding="0" Width="100%" runat="server" CssClass="grid_general">
            <ItemStyle CssClass="item_grid"></ItemStyle>
            <HeaderStyle CssClass="th"></HeaderStyle>
            <Columns>
                <asp:TemplateColumn HeaderText="No">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblNomor" runat="server" Text='<%#Container.DataItem("Nomor")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="DATA ID">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="center" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="CONTRACT NO.">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="REALIZATION DATE">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="15%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblAgreementDate" runat="server" Text='<%#Container.DataItem("AgreementDate")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="TOTAL PEMBIAYAAN">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblTotalPembiayaan" runat="server" Text='<%#FormatNumber(Container.DataItem("TotalPembiayaan"),2)%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="OS PRINCIPAL">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblOutstandingPrincipal" runat="server" Text='<%#FormatNumber(Container.DataItem("OutstandingPrincipal"),2)%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="MODULE">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblApplicationModule" runat="server" Text='<%#Container.DataItem("ApplicationModule")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="STEP">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblApplicationStep" runat="server" Text='<%#Container.DataItem("ApplicationStep")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="STATUS">
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                        </asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
            <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                Mode="NumericPages"></PagerStyle>
        </asp:DataGrid>
                </div>
            </div>
        </div> 
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>