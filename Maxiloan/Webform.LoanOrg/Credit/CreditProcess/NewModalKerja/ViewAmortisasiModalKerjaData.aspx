﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAmortisasiModalKerjaData.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewAmortisasiModalKerjaData" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplicationMK.ascx" TagName="ucViewApplicationMK"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabModalKerja.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc8" %>
<%@ Register Src="../../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js"></script>
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <asp:HiddenField runat="server" ID="hdfApplicationID" />
                    <h4>
                        VIEW AMORTISASI DATA</h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc3:ucViewApplicationMK id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgInvoice" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                            CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn Visible="True" DataField="InsSeqNo" HeaderText="Periode"></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="DueDate" HeaderText="TGL JT" DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="PrincipalAmount" HeaderText="Pokok" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestAmount" HeaderText="Bunga" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="LateChargeAmount" HeaderText="Denda"  DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InstallmentPaidDate" HeaderText="Tgl Bayar" DataFormatString="{0:dd/MM/yyyy}" ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="PrincipalPaidAmount" HeaderText="Pokok Paid" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="InterestPaidAmount" HeaderText="Bunga Paid" DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                                <asp:BoundColumn Visible="True" DataField="LateChargePaidAmount" HeaderText="Denda Paid"  DataFormatString="{0:0,0}"  ></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
             <div class="tab_container_form_space"><br /><br /><br /><br />
                </div>
                <div class="tab_container_button">
                    <div class="form_button">
                        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>