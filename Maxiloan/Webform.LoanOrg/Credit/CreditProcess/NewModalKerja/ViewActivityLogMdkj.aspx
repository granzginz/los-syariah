﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewActivityLogMdkj.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewActivityLogMdkj" %>

<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabModalKerja.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewActivityLog</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - ACTIVITY LOG
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server" EnableViewState="False"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%" EnableViewState="False">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn Visible="False" DataField="CustomerName"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="NO">
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="ACTIVITYDATE" HeaderText="TGL AKTIVITAS" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ACTIVITYTYPE" HeaderText="JENIS AKTIVITAS"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ActivityUser" HeaderText="OLEH"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    </form>
</body>
</html>
