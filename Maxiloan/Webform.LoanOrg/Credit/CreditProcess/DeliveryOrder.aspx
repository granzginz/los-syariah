﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DeliveryOrder.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.DeliveryOrder" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpZipCode" Src="../../../webform.UserController/ucLookUpZipCode.ascx" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../webform.UserController/ucSyaratCair.ascx" TagPrefix="uc4"
    TagName="ucSyaratCair" %>
<%@ Register TagPrefix="uc1" TagName="ucAddressCity" Src="../../../Webform.UserController/ucAddressCity.ascx" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Verifikasi Pra Pencairan</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function hideMessage() {
            document.getElementById('lblMessage').style.display = 'none';
        }
    </script>
</head>
<body onload="gridGeneralSize('dtgPaging');" onresize="gridGeneralSize('dtgPaging');">
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" ToolTip="Click to close" onclick="hideMessage()"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>                                
                VERIFIKASI PRA PENCAIRAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KONTRAK</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderWidth="0" CellPadding="3" CellSpacing="1" DataKeyField="ApplicationID"
                        CssClass="grid_general">
                        <ItemStyle CssClass="item_grid"></ItemStyle>
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkAction" runat="server" CommandName="View" Text='VERIFIKASI' CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReturn" runat="server" CommandName="Return" Text='RETURN'
                                        CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO KONTRAK" SortExpression="AgreementNo">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="CABANG" SortExpression="BranchFullName">
                                <ItemTemplate>
                                    <asp:label ID="lblBranchFullName" runat="server" Text='<%#container.dataitem("BranchFullName")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA CUSTOMER" SortExpression="CustomerName">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%#container.dataitem("CustomerName")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA SUPPLIER" SortExpression="SupplierName">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynSupplierName" runat="server" Text='<%#container.dataitem("SupplierName")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KONTAK" SortExpression="ContactPersonName">
                                <ItemTemplate>
                                    <asp:Label ID="lblContactPersonName" runat="server" Text='<%#container.dataitem("ContactPersonName")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO TELP SUPPLIER" SortExpression="SupplierPhone">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierPhone" runat="server" Text='<%#container.dataitem("SupplierPhone")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA ACCOUNT OFFICER" SortExpression="EmployeeName">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynEmployeeName" runat="server" Text='<%#container.dataitem("EmployeeName")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA ASSET" SortExpression="Description">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%#container.dataitem("Description")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL PO" SortExpression="PurchaseOrderDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblPODate" runat="server" Text='<%#container.dataitem("PurchaseOrderDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO APLIKASI" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%#container.dataitem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID CUSTOMER" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%#container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID SUPPLIER" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierID" runat="server" Visible="false" Text='<%#container.dataitem("SupplierID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ID ACCOUNT OFFICER" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblAOID" runat="server" Text='<%#container.dataitem("AOID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AgreementNo" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblAgreementNo" runat="server" Text='<%#container.dataitem("AgreementNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BranchID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Text='<%#container.dataitem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" EnableViewState="False" CssClass="small buttongo blue">
                    </asp:Button>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                        MinimumValue="1" ErrorMessage="No Halaman Salah!" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI VERIFIKASI PRA PENCAIRAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_single">
                    <label>
                        Cabang
                    </label>
                    <asp:DropDownList ID="oBranch" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
                        Display="Dynamic" ErrorMessage="Harap Pilih Cabang" ControlToValidate="oBranch"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                    <asp:ListItem Value="Description">Nama Asset</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="medium_text">
                    Tanggal PO
                </label>
                <asp:TextBox runat="server" ID="txtTanggalPO"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="ceTanggalPO" TargetControlID="txtTanggalPO"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPO" runat="server">
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Customer
                    </label>
                    <asp:HyperLink ID="hplCustomerName" runat="server" NavigateUrl=""></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        No Kontrak
                    </label>
                    <asp:HyperLink ID="hplAgreement" runat="server" NavigateUrl=""></asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No. PO
                    </label>
                    <asp:Label ID="lblPONumber" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Supplier
                    </label>
                    <asp:HyperLink ID="lblSupplier" runat="server"></asp:HyperLink>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    BASTK/Tanggal Kirim
                </label>
                <asp:TextBox runat="server" ID="txtTanggalDO" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="ceTanggalDO" TargetControlID="txtTanggalDO"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:Label ID="lblErrMsgDeliveryDate" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
        <asp:Panel ID="pnlPPNBBN" runat="server">
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    PPN
                </label>
                <uc1:ucnumberformat runat="server" id="ucPPN"></uc1:ucnumberformat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    BBN
                </label>
                <uc1:ucnumberformat runat="server" id="ucBBN"></uc1:ucnumberformat>
            </div>
        </div>
        </asp:Panel>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    ASSET INFO</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        ASSET
                    </label>
                    <asp:Label ID="lblAssetDescription" runat="server"></asp:Label>
                </div>
                <div class="form_right" id="infowarna" runat="server">
                    <label>
                        Warna
                    </label>
                    <asp:Label ID="lblWarna" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <asp:Label ID="lblSerial1" runat="server" CssClass="label"></asp:Label>
                    <asp:Label ID="lblSerial1Value" runat="server"></asp:Label>
                    <%--<asp:RequiredFieldValidator ID="RFVSerial1" runat="server" Display="Dynamic" ErrorMessage="Harap isi Serial 1"
                        ControlToValidate="txtSerial1" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblErrSerial1" runat="server" Visible="False"></asp:Label>--%>
                </div>
                <div class="form_right" id="infoNopol" runat="server">
                    <label>
                        No. Polisi
                    </label>
                    <asp:Label ID="lblNopol" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box" id="infoSerial2" runat="server">
            <div>
                <div class="form_left">
                    <asp:Label ID="lblSerial2" runat="server" CssClass="label"></asp:Label>
                    <asp:Label ID="lblSerial2Value" runat="server"></asp:Label>
                    <%--<asp:RequiredFieldValidator ID="RFVSerial2" runat="server" Display="Dynamic" ErrorMessage="Harap isi Serial 2"
                        ControlToValidate="txtSerial2" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblErrSerial2" runat="server" Visible="False"></asp:Label>--%>
                </div>
            </div>
        </div>
        <div class="form_box" id="infoTahunKendaraan" runat="server">
            <div>
                <div class="form_left">
                    <label>
                        Tahun Asset
                    </label>
                    <%--<asp:TextBox ID="txtYear" runat="server" CssClass="small_text"></asp:TextBox>--%>
                    <asp:Label ID="lblYear" runat="server" CssClass="small_text"></asp:Label>
                    <%--<asp:RequiredFieldValidator ID="RFVYear" runat="server" Display="Dynamic" ErrorMessage="Harap isi tahun Produksi"
                        ControlToValidate="txtYear" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="RangeValidator3" runat="server" Display="Dynamic" ErrorMessage="Tahun harus <= Hari ini"
                        MinimumValue="0" Type="Integer" ControlToValidate="txtYear" MaximumValue="1"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                        ErrorMessage="Tahun Salah" ControlToValidate="txtYear" ValidationExpression="\d{4}"
                        CssClass="validator_general"></asp:RegularExpressionValidator>--%>
                </div>
            </div>
        </div>
        <div class="form_box_uc" id="divdtgAttribute" runat="server" visible = "false">
            <%--<div class="form_single">--%>
            <asp:DataGrid ID="dtgAttribute" runat="server" Width="100%" ShowHeader="False" AutoGenerateColumns="False"
                BorderWidth="0" BorderStyle="none" CssClass="grid_general">
                <ItemStyle CssClass="item_grid_attr" />
                <Columns>
                    <asp:TemplateColumn>
                        <ItemStyle CssClass="label_col item_grid_attr_collabel" />
                        <ItemTemplate>
                            <asp:Label ID="lblNameAttr" Text='<%#Container.DataItem("Name")%>' runat="server">
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemStyle CssClass="left_col" />
                        <ItemTemplate>
                            <asp:TextBox Text='<%#Container.DataItem("AttributeContent") %>' ID="txtAttribute"
                                runat="server" MaxLength='<%# Container.DataItem("AttributeLength") %>'>
                            </asp:TextBox>
                            <asp:Label ID="lblVAttribute" runat="server">Attribute Sudah Ada</asp:Label>
                            <asp:RegularExpressionValidator ID="RVAttribute" runat="server" Display="Dynamic"
                                Enabled="False" ControlToValidate="txtAttribute" CssClass="validator_general"></asp:RegularExpressionValidator>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="AttributeID"></asp:BoundColumn>
                    <asp:TemplateColumn Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%#Container.DataItem("Name") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn Visible="False">
                        <ItemTemplate>
                            <asp:Label ID="lblAttributeType" runat="server" Text='<%#Container.DataItem("AttributeType") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
        <%--</div>--%>
        <asp:Panel ID="infoLain" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOKUMEN ASSET</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_single">
                    <label>
                        Nama
                    </label>
                    <asp:TextBox ID="txtName" Readonly="true" runat="server" Width="240px" MaxLength="50"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_box">
            <uc1:ucaddresscity id="ucAddress" runat="server"></uc1:ucaddresscity> 
        </div>
        <%--<div class="form_box">
            <div>
                <div class="form_single">
                    <label class="label_split">
                        Alamat
                    </label>
                    <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" CssClass="multiline_textbox_uc"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        RT/RW
                    </label>
                    <asp:TextBox ID="txtRT" runat="server" MaxLength="3" CssClass="small_text"></asp:TextBox>/
                    <asp:TextBox ID="txtRW" runat="server" MaxLength="3" CssClass="small_text"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uclookupzipcode id="oZipCode" runat="server"></uc1:uclookupzipcode>
        </div>--%>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label class="label_req" runat="server" id="lblTanggalSTNK">
                        Tanggal STNK</label>
                    <asp:TextBox runat="server" ID="txtTanggalSTNK" CssClass="small_text" ></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceTanggalSTNK" TargetControlID="txtTanggalSTNK"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator runat="server" ID="rfvTanggalSTNK" ControlToValidate="txtTanggalSTNK"
                        Enabled="false" ErrorMessage="Harap isi tanggal STNK!" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        </asp:Panel>
        <div class="form_box">
            <div>
                <div class="form_single">
                    <label class="label_split">
                        Catatan</label>
                    <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox_uc"></asp:TextBox>
                </div>
            </div>
        </div>
        <uc4:ucsyaratcair id="ucSyaratCair1" runat="server" />
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOKUMEN ASSET
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgAssetDoc" runat="server" CellPadding="3" CellSpacing="1" Visible="False"
                    BorderWidth="0px" AutoGenerateColumns="False" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO" ItemStyle-CssClass="command_col">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="AssetDocName" HeaderText="DOKUMEN" ItemStyle-CssClass="name_col"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="NO DOKUMEN">                            
                            <ItemTemplate>
                                <asp:Label ID="lblIsNoRequired" runat="server"   Text='<%# DataBinder.eval(Container,"DataItem.IsNoRequired") %>'></asp:Label>
                                <asp:TextBox ID="txtNumber" Visible="false" runat="server" Text='<%#container.dataitem("DocumentNo")%>'>
                                </asp:TextBox>
                                <asp:Label ID="lblNumberNo" runat="server" Text='<%#container.dataitem("DocumentNo")%>'>
                                </asp:Label>
                                <asp:Label ID="lblVNumber" runat="server" CssClass="validator_general">No Dokumen Sudah Ada</asp:Label>
                                <asp:Label ID="lblVNumber2" runat="server" Visible='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>' CssClass="validator_general">Harap isi No Dokumen</asp:Label>
                                <asp:RequiredFieldValidator ID="RFVNumber" Enabled='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>'
                                    runat="server" ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtNumber"
                                    Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BPKB">
                            <ItemTemplate>
                                <asp:CheckBox ID="BPKBchk" Enabled="false" runat="server">
                                </asp:CheckBox>                                
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA">
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" runat="server" Checked='<%#GetValue(DataBinder.eval(Container,"DataItem.IsDocExist"))%>'>
                                </asp:CheckBox>
                                <asp:Label ID="lblVChk" runat="server" CssClass="validator_general">Harus diperiksa</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:TextBox ID="txtNotes" Text='<%#container.dataitem("Notes")%>' runat="server" Enabled="false" CssClass="desc_textbox" width="70%">
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL JANJI">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtPromiseDate2" Text='<%#Format(container.dataitem("PromiseDate"), "dd/MM/yyyy")%>' CssClass="small_text" Enabled="false" width="80px"></asp:TextBox>
                                <asp:CalendarExtender runat="server" ID="cePromiseDate2" TargetControlID="txtPromiseDate2"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                                    ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtPromiseDate2"
                                    SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
                                </asp:RegularExpressionValidator>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MandatoryForNewAsset"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="MandatoryForUsedAsset"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="AssetDocID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="IsMainDoc"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="IsValueNeeded"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="IsDocExist" Visible="False">                            
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblIsDocExist" Text='<%#container.dataitem("IsDocExist")%>' runat="server">
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <asp:DataGrid ID="DTGAssetDocNew" runat="server" CellPadding="3" CellSpacing="1"
                    Visible="False" BorderWidth="0px" AutoGenerateColumns="False" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO" ItemStyle-CssClass="command_col">
                            <ItemTemplate>
                                <asp:Label ID="lblNoNew" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="AssetDocName" HeaderText="DOKUMEN" ItemStyle-CssClass="name_col"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="NO DOKUMEN" ItemStyle-CssClass="short_col">
                            <ItemTemplate>
                                <asp:Label ID="lblIsNoRequired" runat="server"  Visible="false"  Text='<%# DataBinder.eval(Container,"DataItem.IsNoRequired") %>'></asp:Label>
                                <asp:TextBox ID="txtNumberNew" runat="server"></asp:TextBox>
                                <asp:Label ID="lblVNumberNew" runat="server" CssClass="validator_general" Visible="false">No Dokumen Sudah Ada</asp:Label>
                                <asp:Label ID="lblVNumber2New" runat="server" Visible='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>' CssClass="validator_general">Harap isi No Dokumen</asp:Label>
                                <asp:RequiredFieldValidator ID="RFVNumberNew" Enabled='<%# DataBinder.eval(Container,"DataItem.IsValueNeeded") %>'
                                    runat="server" ErrorMessage="Harap isi dengan Angka" ControlToValidate="txtNumberNew"
                                    Display="Dynamic" CssClass="validator_general">
                                </asp:RequiredFieldValidator>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BPKB">
                            <ItemTemplate>
                                <%--<asp:CheckBox ID="BPKBchk" Enabled="false" runat="server">--%>
                                <asp:CheckBox ID="BPKBchk" Enabled="True" runat="server">
                                </asp:CheckBox>                                
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA" ItemStyle-CssClass="short_col">                                                        
                            <ItemTemplate>
                                <asp:CheckBox ID="chkNew" runat="server"></asp:CheckBox>
                                <asp:Label ID="lblVChkNew" runat="server" CssClass="validator_general" Visible="false">Harus diperiksa</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <ItemTemplate>
                                <asp:TextBox ID="txtNotesNew" runat="server" CssClass="desc_textbox"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL JANJI" ItemStyle-CssClass="short_col">
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtPromiseDate2" CssClass="small_text"></asp:TextBox>
                                <asp:CalendarExtender runat="server" ID="cePromiseDate2" TargetControlID="txtPromiseDate2"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                                    ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtPromiseDate2"
                                    SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
                                </asp:RegularExpressionValidator>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MandatoryForNewAsset"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="MandatoryForUsedAsset"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="AssetDocID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="IsMainDoc"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="IsValueNeeded"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div style="display: none">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        SYARAT &amp; KONDISI</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgTC" runat="server" CellPadding="3" CellSpacing="1" Width="100%"
                        BorderWidth="0px" AutoGenerateColumns="False" PageSize="3" DataKeyField="TCName"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                <HeaderStyle Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                <HeaderStyle Width="7%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PERIKSA">
                                <HeaderStyle Width="8%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkTCChecked" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                        runat="server"></asp:CheckBox>
                                    <asp:Label ID="lblVTCChecked" runat="server">Harus diperiksa</asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="TGL JANJI">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtPromiseDate"></asp:TextBox>
                                    <asp:CalendarExtender runat="server" ID="cePromiseDate" TargetControlID="txtPromiseDate"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:Label ID="lblVPromiseDate" runat="server">Tgl Janji harus > hari ini</asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CATATAN">
                                <HeaderStyle Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                        runat="server" Width="95%">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div style="display: none">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        SYARAT &amp; KONDISI CHECK LIST</h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgTC2" runat="server" CellPadding="3" CellSpacing="1" Width="100%"
                        BorderWidth="0px" AutoGenerateColumns="False" PageSize="3" DataKeyField="TCName"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="NO">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                                <HeaderStyle Width="7%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PERIKSA">
                                <HeaderStyle Width="8%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkTCCheck2" Checked='<%# DataBinder.eval(Container, "DataItem.Checked") %>'
                                        runat="server"></asp:CheckBox>
                                    <asp:Label ID="lblVTC2Checked" runat="server">Harus diperiksa</asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                                <HeaderStyle Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="TGL JANJI" Visible="true">
                                <HeaderStyle Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtPromiseDate2"></asp:TextBox>
                                    <asp:CalendarExtender runat="server" ID="cePromiseDate2" TargetControlID="txtPromiseDate2"
                                        Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:Label ID="lblVPromiseDate2" runat="server">Tanggal Janji harus > hari ini</asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CATATAN">
                                <HeaderStyle Width="30%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.eval(Container, "DataItem.Notes") %>'
                                        Width="95%">
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlReturn" runat="server" Visible="False">
        <div class="form_box">
            <div class="form_left">
                <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
            </div>
            <div class="form_right">
                <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Alasan Return</label>
                <asp:TextBox ID="txtAlasanReturn" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    CssClass="validator_general" ControlToValidate="txtAlasanReturn"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    </form>
</body>
</html>
