﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KonfirmasiDealer.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.KonfirmasiDealer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Konfirmasi Dealer</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                KONFIRMASI DEALER
            </h3>
        </div>
    </div>
    <asp:Panel runat="server" ID="pnlList" Visible="true">
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0" CellPadding="3" CellSpacing="1" DataKeyField="ApplicationID"
                    CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAction" runat="server" CommandName="KONFIRMASI_DEALER" Text='KONFIRMASI'
                                    CausesValidation="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemStyle CssClass="command_col"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkReturn" runat="server" CommandName="Return" Text='RETURN'
                                    CausesValidation="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO KONTRAK" SortExpression="AgreementNo">
                            <ItemTemplate>
                                <asp:HyperLink ID="hypAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER" SortExpression="CustomerName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%#container.dataitem("CustomerName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA SUPPLIER" SortExpression="SupplierName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynSupplierName" runat="server" Text='<%#container.dataitem("SupplierName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KONTAK" SortExpression="ContactPersonName">
                            <ItemTemplate>
                                <asp:Label ID="lblContactPersonName" runat="server" Text='<%#container.dataitem("ContactPersonName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO TELP SUPPLIER" SortExpression="SupplierPhone">
                            <ItemTemplate>
                                <asp:Label ID="lblSupplierPhone" runat="server" Text='<%#container.dataitem("SupplierPhone")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA ACCOUNT OFFICER" SortExpression="EmployeeName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynEmployeeName" runat="server" Text='<%#container.dataitem("EmployeeName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA ASSET" SortExpression="Description">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%#container.dataitem("AssetDescription")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO APLIKASI" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationID" runat="server" Text='<%#container.dataitem("ApplicationID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID CUSTOMER" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblCustomerID" runat="server" Text='<%#container.dataitem("CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID SUPPLIER" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblSupplierID" runat="server" Text='<%#container.dataitem("SupplierID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID ACCOUNT OFFICER" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAOID" runat="server" Text='<%#container.dataitem("AOID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="AGREEMENT NO" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAgreementNo" runat="server" Text='<%#container.dataitem("AgreementNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ASSET TYPE ID " Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblAssetTypeID" runat="server" Text='<%#container.dataitem("AssetTypeID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TGL ANGSURAN I" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblTglAngsuranI" runat="server" Text='<%#container.dataitem("EffectiveDate")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="REFINANCING" Visible="False">                            
                            <ItemTemplate>
                                <asp:Label ID="lblRefinancing" runat="server" Text='<%#container.dataitem("Refinancing")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>
                    record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI VERIFIKASI PRA PENCAIRAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label class="label_medium">
                        Cari Berdasarkan
                    </label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                        <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                        <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                        <asp:ListItem Value="Description">Nama Asset</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Approval
                    </label>
                    <asp:TextBox runat="server" ID="txtTanggalApproval"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceTanggalApproval" TargetControlID="txtTanggalApproval"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlForm" Visible="false">
        <div class="form_box">
            <div class="form_left">
                <uc1:ucviewapplication id="ucViewApplication" runat="server" />
            </div>
            <div class="form_right">
                <uc1:ucviewcustomerdetail id="ucViewCustomerDetail" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Konfirmasi
                </label>
                <asp:TextBox runat="server" ID="txtTanggalKonfirmasi" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtTanggalKonfirmasi"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Isi tanggal konfirmasi dealer!"
                    Display="Dynamic" ControlToValidate="txtTanggalKonfirmasi" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <asp:Label runat="server" ID="lblSerial1">No Chasis</asp:Label>                
                </label>
                <asp:TextBox runat="server" ID="txtSerial1" CssClass="medium_text" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfSerial1" runat="server" Display="Dynamic" ControlToValidate="txtSerial1" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:Label runat="server" ID="lblErrSerial1" Visible="false" CssClass="validator_general"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <asp:Label runat="server" ID="lblSerial2">No Mesin</asp:Label>                
                </label>
                <asp:TextBox runat="server" ID="txtSerial2" CssClass="medium_text" onkeypress="return OnlyHurufAndNumber(event);"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfSerial2" runat="server" Display="Dynamic" ControlToValidate="txtSerial2"   CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:Label runat="server" ID="lblErrSerial2" Visible="false" CssClass="validator_general"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <asp:Label runat="server" ID="Label1">Warna</asp:Label>
                </label>
                <asp:TextBox runat="server" ID="txtWarna" CssClass="medium_text"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <asp:Label runat="server" ID="Label2">Nama di STNK</asp:Label>
                </label>
                <asp:TextBox runat="server" ID="txtNamaSTNK" CssClass="medium_text"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    <asp:Label runat="server" ID="Label3">Tahun Asset Yang Dibiayai</asp:Label>
                </label>
                <asp:TextBox runat="server" ID="txtTahunKendaraan" CssClass="medium_text"></asp:TextBox>
            </div>
        </div>
         <div class="form_box">
        <%--sudah tidak dipakai--%>
        <div class="form_single" runat="server" visible="false">
            <label>
                Angsuran Pertama
            </label>
            <asp:DropDownList ID="cboFirstInstallment" runat="server" AutoPostBack="true">
                <asp:ListItem Value="Select One">Select One</asp:ListItem>
                <asp:ListItem Value="AD">Advance</asp:ListItem>
                <asp:ListItem Value="AR">Arrear</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                ControlToValidate="cboFirstInstallment" ErrorMessage="Harap Pilih Angsuran Pertama"
                InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
        </div>
<%--        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Angsuran I
                </label>
                <asp:TextBox runat="server" ID="txtTanggalEfektif" CssClass="small_text"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTanggalEfektif"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                    ControlToValidate="txtTanggalEfektif" ErrorMessage="Isi tanggal efektif!" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>--%>
        <div class="form_button">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="small button blue" />
            <asp:Button runat="server" ID="btnCancel" CausesValidation="false" Text="Cancel"
                CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlReturn" runat="server" Visible="False">
        <div class="form_box">
            <div class="form_left">
                <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
            </div>
            <div class="form_right">
                <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Alasan Return</label>
                <asp:TextBox ID="txtAlasanReturn" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    CssClass="validator_general" ControlToValidate="txtAlasanReturn"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    </form>
</body>
</html>
