﻿#Region "Imports"
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class PO
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private n_controller As New MasterController
    Private m_controller As New POController
    Private m_TCcontroller As New ApplicationController
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucpengurangPO As ucNumberFormat
    Protected WithEvents UcNilaiKurs As ucNumberFormat
    Private x_controller As New DataUserControlController

    Protected WithEvents ucSyaratCair As ucSyaratCairPO

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Dim intLoop As Integer
    Dim Status As Boolean = True
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property ProductOfferingID() As String
        Get
            Return CType(ViewState("ProductOfferingID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProductOfferingID") = Value
        End Set
    End Property

    Private Property ProductID() As String
        Get
            Return CType(ViewState("ProductID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Private Property SupplierIDKaroseri() As String
        Get
            Return CType(ViewState("SupplierIDKaroseri"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierIDKaroseri") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Private Property SplitPembayaran() As Boolean
        Get
            Return CType(ViewState("SplitPembayaran"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("SplitPembayaran") = Value
        End Set
    End Property

    Private Property UsedNew() As String
        Get
            Return CType(viewstate("UsedNew"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("UsedNew") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            cboBranch()
            Me.FormID = "PO"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                txtGoPage.Text = "1"
                Me.Sort = "a.ApplicationID ASC"
                Me.CmdWhere = ""
                Me.BranchID = oBranch.SelectedValue.ToString.Trim
                lblTanggalPO.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
                ucpengurangPO.Text = FormatNumber(0, 0)
                txtTanggalKonfirmasi.Attributes.Add("readonly", "true")
                fillCboIDKurs()


                'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) = 0 And Me.IsHoBranch = True Then
                BindGridEntity(Me.CmdWhere)
                If Request.QueryString("filereport") <> "" Then
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" + Me.Session.SessionID + Me.Loginid + "rpt_PrintPO.pdf"
                    Response.Write("<script language = javascript>" & vbCrLf _
                                & "window.open('" & strFileLocation & "') " & vbCrLf _
                                & "</script>")

                End If
                'Else
                '    Response.Redirect("../../../error_notauthorized.aspx")
                'End If
            End If
        End If

        'LabelValidationFalse()
    End Sub
    Sub cboBranch()
        With oBranch
            If Me.IsHoBranch Then
                .DataSource = x_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "ALL"
                .Enabled = True
            Else
                .DataSource = x_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Enabled = False
            End If

        End With
    End Sub
    Sub fillCboIDKurs()
        'Dim Master As New Parameter.Master
        Dim oPO As New Parameter.PO
        Dim dtList As DataTable
        'With Master
        '    .WhereCond = "ALL"
        '    .CurrentPage = currentPage
        '    .PageSize = pageSize
        '    .SortBy = Me.Sort
        '.strConnection = GetConnectionString()
        'End With
        oPO.strConnection = GetConnectionString()
        oPO = m_controller.GetIDKurs(oPO)
        dtList = oPO.ListData
        cboIDKurs.DataSource = dtList
        cboIDKurs.DataTextField = "Value"
        cboIDKurs.DataValueField = "ID"
        cboIDKurs.DataBind()
        cboIDKurs.SelectedValue = "IDR"
        'cboIDKurs.Items(0).Value = "IDR-Rupiah"

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlPO.Visible = False
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oPO As New Parameter.PO
        InitialDefaultPanel()

        With oPO
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
            .BranchId = Me.BranchID
            .strConnection = GetConnectionString()
        End With
        Try
            oPO = m_controller.POPaging(oPO)
            If Not oPO Is Nothing Then
                dtEntity = oPO.ListData
                recordCount = oPO.TotalRecords
            Else
                recordCount = 0
            End If

            dtgPaging.DataSource = dtEntity.DefaultView
            dtgPaging.CurrentPageIndex = 0

            dtgPaging.DataBind()
            PagingFooter()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


        
    End Sub

#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strSupplierID & "','" & strStyle & "')"
    End Function

    Function LinkToEmployee(ByVal strBranchID As String, ByVal strAOID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinEmployee('" & strBranchID & "','" & strAOID & "','" & strStyle & "')"
    End Function

    Function LinkToAgreementNo(ByVal strAgreementNo As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinAgreementNo('" & strAgreementNo & "','" & strStyle & "')"
    End Function
#End Region

#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim hypAgreementNo As New HyperLink
        Dim hynCustomerName As New HyperLink
        Dim hynSupplierName As New HyperLink
        Dim hynEmployeeName As New HyperLink

        Dim lblSupplierID As New Label
        Dim lblCustomerID As New Label
        Dim lblEmployeeID As New Label
        Dim lblAgreementNo As New Label
        Dim lblApplicationID As New Label
        Dim lblApprovalDate As New Label
        If e.Item.ItemIndex >= 0 Then
            lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblEmployeeID = CType(e.Item.FindControl("lblEmployeeID"), Label)
            lblAgreementNo = CType(e.Item.FindControl("lblAgreementNo"), Label)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblApprovalDate = CType(e.Item.FindControl("lblApprovalDate"), Label)

            hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)
            hynSupplierName = CType(e.Item.FindControl("hynSupplierName"), HyperLink)
            hynEmployeeName = CType(e.Item.FindControl("hynEmployeeName"), HyperLink)

            hypAgreementNo.NavigateUrl = LinkToAgreementNo(lblAgreementNo.Text.Trim, "AccAcq")
            hynCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            hynSupplierName.NavigateUrl = LinkToSupplier(lblSupplierID.Text.Trim, "AccAcq")
            hynEmployeeName.NavigateUrl = LinkToEmployee(CType(e.Item.FindControl("lblBranchID"), Label).Text, lblEmployeeID.Text.Trim, "AccAcq")
        End If
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region

#Region "BindTC"
    'Sub TC()
    '    Dim oApplication As New Parameter.Application
    '    Dim oData As New DataTable
    '    oApplication.strConnection = GetConnectionString()
    '    oApplication.AppID = Me.ApplicationID
    '    oApplication.BranchId = Me.BranchID
    '    oApplication.AddEdit = "GoLive"
    '    oApplication = m_TCcontroller.GetTC(oApplication)
    '    If Not oApplication Is Nothing Then
    '        oData = oApplication.ListData
    '    End If
    '    dtgTC.DataSource = oData.DefaultView
    '    dtgTC.DataKeyField = "TCName"
    '    dtgTC.DataBind()

    '    Dim intLoop As Integer
    '    For intLoop = 0 To dtgTC.Items.Count - 1
    '        If oData.Rows(intLoop).Item(4).ToString.Trim <> "" Then
    '            CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), ValidDate).dateValue = Format(oData.Rows(intLoop).Item(4), "dd/MM/yyyy")
    '        End If
    '    Next
    'End Sub

    'Sub TC2()
    '    Dim oApplication As New Parameter.Application
    '    Dim oData As New DataTable
    '    oApplication.strConnection = GetConnectionString()
    '    oApplication.BranchId = Me.BranchID
    '    oApplication.AppID = Me.ApplicationID
    '    oApplication.AddEdit = "GoLive"
    '    oApplication = m_TCcontroller.GetTC2(oApplication)
    '    If Not oApplication Is Nothing Then
    '        oData = oApplication.ListData
    '    End If
    '    dtgTC2.DataSource = oData.DefaultView
    '    dtgTC2.DataKeyField = "TCName"
    '    dtgTC2.DataBind()
    '    Dim intLoop As Integer
    '    For intLoop = 0 To dtgTC2.Items.Count - 1
    '        If oData.Rows(intLoop).Item(5).ToString.Trim <> "" Then
    '            CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), ValidDate).dateValue = Format(oData.Rows(intLoop).Item(5), "dd/MM/yyyy")
    '        End If
    '    Next
    'End Sub
#End Region

#Region "LabelValidationFalse"
    'Sub LabelValidationFalse()
    '    Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
    '    Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
    '    Dim countDtg As Integer = CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
    '    Dim validCheck As New Label
    '    Dim Checked As Boolean
    '    Dim PriorTo As String
    '    Dim PromiseDate As TextBox
    '    Dim Mandatory As String

    '    For intLoop = 0 To countDtg
    '        If intLoop <= DtgTC1count Then
    '            PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
    '            Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
    '            PromiseDate = CType(dtgTC.Items(intLoop).FindControl("txtPromiseDate"), TextBox)
    '            Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
    '            validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
    '            CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = False
    '            If PriorTo = "PO" And Mandatory = "v" Then
    '                validCheck.Visible = True
    '            Else
    '                validCheck.Visible = False
    '            End If
    '        End If

    '        If intLoop <= DtgTC2count Then
    '            PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
    '            Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
    '            Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
    '            PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("txtPromiseDate2"), TextBox)
    '            validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
    '            CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = False
    '            If PriorTo = "PO" And Mandatory = "v" Then
    '                validCheck.Visible = True
    '            Else
    '                validCheck.Visible = False
    '            End If
    '        End If
    '    Next
    'End Sub
#End Region

#Region "dtgEntity_ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "View" Then
            If CheckFeature(Me.Loginid, Me.FormID, "PO", Me.AppId) Then
                Me.BranchID = CType(e.Item.FindControl("lblBranchID"), Label).Text

                pnlList.Visible = False
                pnlPO.Visible = True
                pnlReturn.Visible = False
                'modify set nilai kurs
                UcNilaiKurs.Text = 1
                cboIDKurs.SelectedItem.Text = "IDR - Rupiah"

                Dim oPO_PONumber As New Parameter.PO
                oPO_PONumber.BranchId = CType(e.Item.FindControl("lblBranchID"), Label).Text
                oPO_PONumber.BusinessDate = Me.BusinessDate
                oPO_PONumber.ID = "PO"
                oPO_PONumber.strConnection = GetConnectionString()

                lblPONumber.Text = m_controller.Get_PONumber(oPO_PONumber)

                Dim lblApplicationID As Label
                lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
                Me.ApplicationID = lblApplicationID.Text.Trim

                Dim lblProductOfferingID As Label
                lblProductOfferingID = CType(e.Item.FindControl("lblProductOfferingID"), Label)
                Me.ProductOfferingID = lblProductOfferingID.Text.Trim

                Dim lblProductID As Label
                lblProductID = CType(e.Item.FindControl("lblProductID"), Label)
                Me.ProductID = lblProductID.Text.Trim

                Dim lblSupplierID As Label
                lblSupplierID = CType(e.Item.FindControl("lblSupplierID"), Label)
                Me.SupplierID = lblSupplierID.Text.Trim

                Dim hynCustomerName As HyperLink
                hynCustomerName = CType(e.Item.FindControl("hynCustomerName"), HyperLink)

                Dim hypAgreementNo As New HyperLink
                hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)

                Dim lblCustomerID As Label
                lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
                hplCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
                hplCustomerName.Text = hynCustomerName.Text
                hplAgreement.Text = hypAgreementNo.Text
                hplAgreement.NavigateUrl = LinkToAgreementNo(lblApplicationID.Text.Trim, "AccAcq")

                Dim chkSplitPembayaran As CheckBox
                chkSplitPembayaran = CType(e.Item.FindControl("chkSplitPembayaran"), CheckBox)
                Me.SplitPembayaran = chkSplitPembayaran.Checked

                getAssetInfo()

                Dim oPO_Kepada As New Parameter.PO
                oPO_Kepada.SupplierID = lblSupplierID.Text.Trim
                oPO_Kepada.strConnection = GetConnectionString()

                oPO_Kepada = m_controller.PO_Kepada(oPO_Kepada)

                lblPOSupplierName.Text = oPO_Kepada.SupplierName
                lblPOSupplierAddress.Text = oPO_Kepada.SupplierAddress
                lblPOSupplierRTRW.Text = oPO_Kepada.SupplierRTRW
                lblPOSupplierKecamatan.Text = oPO_Kepada.SupplierKecamatan
                lblPOSupplierKelurahan.Text = oPO_Kepada.SupplierKelurahan
                lblPOSupplierCity.Text = oPO_Kepada.SupplierCity
                lblPOSupplierZipCode.Text = oPO_Kepada.SupplierZipCode

                Dim lblSupplierIDKaroseri As New Label
                lblSupplierIDKaroseri = CType(e.Item.FindControl("lblSupplierIDKaroseri"), Label)
                Me.SupplierIDKaroseri = lblSupplierIDKaroseri.Text.Trim

                If Me.SupplierIDKaroseri = "" Then
                    pnlKaroseri1.Visible = False
                    pnlKaroseri2.Visible = False
                    pnlKaroseri3.Visible = False
                    pnlKaroseri4.Visible = False
                    pnlKaroseri5.Visible = False
                    pnlKaroseri6.Visible = False
                    pnlKaroseri7.Visible = False

                    'rfvKaroseriAcc.Enabled = False
                Else
                    pnlKaroseri1.Visible = True
                    pnlKaroseri2.Visible = True
                    pnlKaroseri3.Visible = True
                    pnlKaroseri4.Visible = True
                    pnlKaroseri5.Visible = True
                    pnlKaroseri6.Visible = True
                    pnlKaroseri7.Visible = True

                    'rfvKaroseriAcc.Enabled = True
                    Dim oSupplierKaroseri As New Parameter.PO
                    oSupplierKaroseri.SupplierID = lblSupplierIDKaroseri.Text.Trim
                    oSupplierKaroseri.strConnection = GetConnectionString()

                    oSupplierKaroseri = m_controller.PO_Kepada(oSupplierKaroseri)

                    lblSupplierKaroseri.Text = oSupplierKaroseri.SupplierName
                    lblSupplierKaroseriAddress.Text = oSupplierKaroseri.SupplierAddress
                    lblSupplierKaroseriRTRW.Text = oSupplierKaroseri.SupplierRTRW
                    lblSupplierKaroseriKecamatan.Text = oSupplierKaroseri.SupplierKecamatan
                    lblSupplierKaroseriKelurahan.Text = oSupplierKaroseri.SupplierKelurahan
                    lblSupplierKaroseriCity.Text = oSupplierKaroseri.SupplierCity
                    lblSupplierKaroseriZipCode.Text = oSupplierKaroseri.SupplierZipCode
                End If



                Dim oPO_DikirimKe As New Parameter.PO
                oPO_DikirimKe.BranchId = CType(e.Item.FindControl("lblBranchID"), Label).Text
                oPO_DikirimKe.ApplicationID = Me.ApplicationID
                oPO_DikirimKe.strConnection = GetConnectionString()

                oPO_DikirimKe = m_controller.PO_DikirimKe(oPO_DikirimKe)

                lblPOCustomerAddress.Text = oPO_DikirimKe.CustomerAddress
                lblPOCustomerKecamatan.Text = oPO_DikirimKe.CustomerKecamatan
                lblPOCustomerKelurahan.Text = oPO_DikirimKe.CustomerKelurahan
                lblPOCustomerCity.Text = oPO_DikirimKe.CustomerCity
                lblPOCustomerZipCode.Text = oPO_DikirimKe.CustomerZipCode

                Dim oPO_ItemFee As New Parameter.PO

                oPO_ItemFee.BranchId = CType(e.Item.FindControl("lblBranchID"), Label).Text
                oPO_ItemFee.ApplicationID = Me.ApplicationID
                oPO_ItemFee.strConnection = GetConnectionString()

                oPO_ItemFee = m_controller.PO_ItemFee(oPO_ItemFee)

                lblJenisBarang.Text = oPO_ItemFee.Description
                lblTotalOTR.Text = FormatNumber(oPO_ItemFee.TotalOTR, 2)
                lblHargakaroseri.Text = FormatNumber(oPO_ItemFee.HargaKaroseri, 2)

                '  FillComboSupplierAccount(Me.SupplierID, cboSupplierAccount)
                'FillComboSupplierAccount(Me.SupplierIDKaroseri, cboSupplierKaroseriAccount)

                Me.UsedNew = oPO_ItemFee.UsedNew
                BindSyaratCair()
                'TC()
                'TC2()
                'LabelValidationFalse()
            End If
        ElseIf e.CommandName = "Return" Then
            pnlList.Visible = False
            pnlReturn.Visible = True
            pnlPO.Visible = False

            Dim lblCustomerID As New Label
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            Dim hypAgreementNo As New HyperLink
            hypAgreementNo = CType(e.Item.FindControl("hypAgreementNo"), HyperLink)
            Dim lblApplicationID As New Label
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)

            hdnApplicationID.Value = lblApplicationID.Text.Trim
            ucViewApplication1.ApplicationID = lblApplicationID.Text.Trim
            ucViewApplication1.CustomerID = lblCustomerID.Text.Trim
            ucViewApplication1.bindData()
            ucViewApplication1.initControls("OnNewApplication")
            ucViewCustomerDetail1.CustomerID = lblCustomerID.Text.Trim
            ucViewCustomerDetail1.bindCustomerDetail()
        End If        
    End Sub
#End Region

    Sub FillComboSupplierAccount(ByVal strSupplierID As String, ByVal oCombo As DropDownList)
        Dim oPO As New Parameter.PO
        Dim dtEntity As DataTable

        oPO.SupplierID = strSupplierID
        oPO.DefaultBankAccount = True
        oPO.BusinessDate = Me.BusinessDate
        oPO.strConnection = GetConnectionString()

        oPO = m_controller.Get_Combo_SupplierAccount(oPO)

        dtEntity = oPO.ListData
        oCombo.DataSource = dtEntity
        oCombo.DataValueField = "SupplierAccID"
        oCombo.DataTextField = "AccSupplierDesc"
        oCombo.DataBind()
    End Sub

#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imbReset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        Me.CmdWhere = ""
        txtSearch.Text = ""
        txtTanggalKonfirmasi.Text = ""
        BindGridEntity(Me.CmdWhere)
        txtGoPage.Text = "1"
    End Sub
#End Region

#Region "imbSearch"
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim = "" And txtTanggalKonfirmasi.Text.Trim = "" Then
            Me.CmdWhere = ""
        ElseIf txtSearch.Text.Trim = "" And txtTanggalKonfirmasi.Text.Trim <> "" Then
            Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalKonfirmasi.Text.Trim) & "'"
        ElseIf txtSearch.Text.Trim <> "" And txtTanggalKonfirmasi.Text.Trim = "" Then
            If cboSearch.SelectedItem.Value = "Name" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "b." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "AgreementNo" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "EmployeeName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "d." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "SupplierName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "c." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            End If
        ElseIf txtSearch.Text.Trim <> "" And txtTanggalKonfirmasi.Text.Trim <> "" Then

            If cboSearch.SelectedItem.Value = "Name" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalKonfirmasi.Text.Trim) & "' and b." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "AgreementNo" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalKonfirmasi.Text.Trim) & "' and a." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "EmployeeName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalKonfirmasi.Text.Trim) & "' and d." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            ElseIf cboSearch.SelectedItem.Value = "SupplierName" Then

                Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                Me.CmdWhere = "a.ApprovalDate = '" & ConvertDate2(txtTanggalKonfirmasi.Text.Trim) & "' and c." & cboSearch.SelectedItem.Value & " like '%" & tmptxtSearchBy & "%'"

            End If
        End If
        Me.BranchID = oBranch.SelectedValue.ToString.Trim
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region

#Region "imgSave"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        lblMessage.Visible = False

        'Dim oData1 As New DataTable
        'Dim oData2 As New DataTable

        Status = True

        ' If Not cekSupplierAccount() Then Exit Sub
        Dim oPO1 As New Parameter.PO

        With oPO1
            .BusinessDate = Me.BusinessDate
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .PONumber = lblPONumber.Text.Trim
            .AgreementNo = hplAgreement.Text.Trim
            .SupplierID = Me.SupplierID
            .SupplierIDKaroseri = Me.SupplierIDKaroseri
            .SplitPembayaran = Me.SplitPembayaran
            .Notes = txtCatatan.Text
            .NotesKaroseri = txtCatatanKaroseri.Text
            .TotalOTR = CDec(lblTotalOTR.Text.Trim)
            .HargaKaroseri = CDec(lblHargakaroseri.Text)
            .ProductOfferingID = Me.ProductOfferingID
            .ProductID = Me.ProductID
            '.PengurangPO = txtpengurangPO.Text
            .PengurangPO = CDec(ucpengurangPO.Text)
            .DescriptionPengurangPO = txtdeskripsiPengurangPO.Text
            .IDKurs = cboIDKurs.SelectedIndex
            .NilaiKurs = CDec(UcNilaiKurs.Text)
            .strConnection = GetConnectionString()

        End With

        Validator()
        'If Status = False Then
        '    pnlList.Visible = False
        '    pnlPO.Visible = True
        '    pnlReturn.Visible = False
        '    ShowMessage(lblMessage, "Harap isi data yang diperlukan!", True)
        '    Visible = True
        '    Exit Sub
        'End If

        Dim ErrMessage As String

        Try
            oPO1.SyaratPO = ucSyaratCair.oData
            ErrMessage = m_controller.POSave(oPO1)

            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)
                pnlList.Visible = False
                pnlPO.Visible = True
                pnlReturn.Visible = False
                Exit Sub
            Else
                Me.CmdWhere = ""
                BindGridEntity(Me.CmdWhere)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            pnlList.Visible = False
            pnlPO.Visible = True
            pnlReturn.Visible = False
            Exit Sub
        End Try
    End Sub
#End Region

#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptPO")
        CmdWhere = "Agreement.ApplicationID='" & Me.ApplicationID & "' and Agreement.BranchID ='" & Me.BranchID & "'"
        If Not cookie Is Nothing Then
            cookie.Values("cmdwhere") = CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("RptPO")
            cookieNew.Values.Add("cmdwhere", CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region "Validator"
    Sub Validator()
        If ucSyaratCair.ChkValidator() = False Then
            Status = False
        End If
        If Status = False Then
            ShowMessage(lblMessage, "Peringatan: Periksa kembali data, isi yang berlambang *", True)
            Exit Sub
        End If
    End Sub
#End Region

#Region "ItemDataBound"
    'Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
    '        CType(e.Item.FindControl("chkTCChecked"), CheckBox).Attributes.Add("OnClick", _
    '            "return CheckTC('" & Trim(CType(e.Item.FindControl("txtPromiseDate"),  _
    '            TextBox).ClientID) & "',this.checked);")
    '    End If
    'End Sub
    'Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
    '        CType(e.Item.FindControl("chkTCCheck2"), CheckBox).Attributes.Add("OnClick", _
    '     "return CheckTC('" & Trim(CType(e.Item.FindControl("txtPromiseDate2"),  _
    '     TextBox).ClientID) & "',this.checked);")
    '    End If
    'End Sub
#End Region

#Region "cek_Check"
    'Sub cek_Check()
    '    Dim CheckTC As Boolean
    '    Dim CheckTC2 As Boolean
    '    Dim counter, counter2 As Integer
    '    Dim PromiseDate As ValidDate
    '    Dim PromiseDate2 As ValidDate

    '    For counter = 0 To dtgTC.Items.Count - 1
    '        CheckTC = CType(dtgTC.Items(counter).FindControl("chkTCChecked"), CheckBox).Checked
    '        If CheckTC Then
    '            PromiseDate = CType(dtgTC.Items(counter).FindControl("txtPromiseDate"), ValidDate)
    '            PromiseDate.dateValue = ""
    '        End If
    '    Next

    '    For counter = 0 To dtgTC2.Items.Count - 1
    '        CheckTC2 = CType(dtgTC2.Items(counter).FindControl("chkTCCheck2"), CheckBox).Checked
    '        If CheckTC2 Then
    '            PromiseDate2 = CType(dtgTC2.Items(counter).FindControl("txtPromiseDate2"), ValidDate)
    '            PromiseDate2.dateValue = ""
    '        End If
    '    Next
    'End Sub
#End Region

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        InitialDefaultPanel()
    End Sub

    Protected Sub getAssetInfo()
        lblMessage.Visible = False

        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAssetData4"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If (objReader.Read) Then
                LabelSerialNo1.InnerText = objReader.Item("SerialNo1Label").ToString.Trim
                lblSerialNo1.Text = objReader.Item("SerialNo1").ToString.Trim
                LabelSerialNo2.InnerText = objReader.Item("SerialNo2Label").ToString.Trim
                lblSerialNo2.Text = objReader.Item("SerialNo2").ToString.Trim
                lblTahunProduksi.Text = objReader.Item("ManufacturingYear").ToString.Trim
                lblWarna.Text = objReader.Item("Colour").ToString.Trim
            End If

            objReader.Close()
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Protected Sub btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReturn.Click
        Dim ParDO As New Parameter._DO
        Dim m_DO As New DOController

        ParDO.strConnection = GetConnectionString()
        ParDO.BusinessDate = Me.BusinessDate
        ParDO.BranchId = Me.sesBranchId.Replace("'", "").Trim
        ParDO.ApplicationID = hdnApplicationID.Value
        ParDO.AlasanReturn = txtAlasanReturn.Text.Trim

        Try
            m_DO.ApplicationReturnUpdate(ParDO)

            BindGridEntity(Me.CmdWhere)
            pnlList.Visible = True
            pnlPO.Visible = False
            pnlReturn.Visible = False
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

    Protected Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ButtonCancel.Click
        Me.CmdWhere = ""
        BindGridEntity(Me.CmdWhere)
        pnlList.Visible = True
        pnlPO.Visible = False
        pnlReturn.Visible = False
    End Sub

#Region "BindSyaratCair"
    Sub BindSyaratCair()
        ucSyaratCair.UseNew = Me.UsedNew
        ucSyaratCair.BindData()
    End Sub
#End Region


    'Private Function cekSupplierAccount() As Boolean
    '    '(cboSupplierKaroseriAccount.SelectedIndex = -1 And Me.SupplierIDKaroseri <> "") Or
    '    If cboSupplierAccount.SelectedIndex = -1 Then
    '        ShowMessage(lblMessage, "Rekening supplier belum terdaftar!", True)
    '        Return False
    '    Else
    '        Return True
    '    End If
    'End Function
End Class