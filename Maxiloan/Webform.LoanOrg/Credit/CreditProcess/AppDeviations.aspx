﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppDeviations.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.AppDeviations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AppDeviations</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server">
    <ContentTemplate>
    <asp:Label ID="lblMessage" Visible="false" runat="server"></asp:Label>
    <input id="hdnApplicationId" type="hidden" name="hdnApplicationId" runat="server" />
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PENYIMPANGAN
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR APLIKASI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" BorderWidth="0" DataKeyField="ApplicationID" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAction" runat="server" CommandName="View" Text='CETAK' CausesValidation="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NO APLIKASI" SortExpression="ApplicationID">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER" SortExpression="CustomerName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SUPPLIER" SortExpression="SupplierName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynSupplierName" runat="server" Text='<%#container.dataitem("SupplierName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CMO" SortExpression="EmployeeName">
                            <ItemTemplate>
                                <asp:HyperLink ID="hynEmployeeName" runat="server" Text='<%#container.dataitem("EmployeeName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                                <asp:Label runat="server" Visible="false" ID="lblApprovalSchemeID" Text='<%#container.dataitem("ApprovalSchemeID")%>'></asp:Label>
                                <asp:Label runat="server" Visible="false" ID="lblAmountNTF" Text='<%#container.dataitem("AmountNTF")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="CustomerID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SupplierID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="AOID" Visible="false"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="First" ImageUrl="../../../Images/grid_navbutton01.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Prev" ImageUrl="../../../Images/grid_navbutton02.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Next" ImageUrl="../../../Images/grid_navbutton03.png"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                        CommandName="Last" ImageUrl="../../../Images/grid_navbutton04.png"></asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"
                        Display="Dynamic"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI APLIKASI KREDIT
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan
                </label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                    <asp:ListItem Value="ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="Employee">CMO</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="ButtonSearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlView" runat="server" Visible="False">
        <div class="form_box">           
            <div class="form_left">
                <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
            </div>
            <div class="form_right">
                <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_quarter">
                <h5>
                    JENIS PENYIMPANGAN</h5>
            </div>
            <div class="form_quarter">
                <h5>
                    KETENTUAN</h5>
            </div>
            <div class="form_quarter">
                <h5>
                    APLIKASI</h5>
            </div>
            <div class="form_quarter">
                <h5>
                    MENYIMPANG</h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    Harga OTR</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblHargaOTRBerlaku" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblHargaOTRAplikasi" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblHargaOTRPenyimpangan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    Down Payment</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblDownPaymentBerlaku" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblDownPaymentAplikasi" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblDownPaymentPenyimpangan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    Efektif Rate</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblEfectiveRateBerlaku" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblEfectiveRateAplikasi" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblEfectiveRatePenyimpangan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    Admin Fee</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblAdminFeeBerlaku" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblAdminFeeAplikasi" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblAdminFeePenyimpangan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    Diskon Asuransi</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblInsuranceDiscountBerlaku" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblInsuranceDiscountAplikasi" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblInsuranceDiscountPenyimpangan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    Umur Asset</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblUmurKendaraanBerlaku" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblUmurKendaraanAplikasi" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblUmurKendaraanPenyimpangan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_quarter">
                <label>
                    Rasio Angsuran Terhadap Penghasilan</label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblPenghasilanBerlaku" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblPenghasilanAplikasi" runat="server"></asp:Label>
            </div>
            <div class="form_quarter">
                <asp:Label ID="lblPenghasilanPenyimpangan" runat="server"></asp:Label>
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlGridShema">
        <div class="form_box_title">
            <div class="form_single">              
                <h4>
                    APPROVAL STEP
                </h4>
            </div>
        </div>
            <div class="form_box_title_nobg">
                <div class="form_single">
                    <label>
                        Pokok Hutang (NTF)
                    </label>
                    <asp:Label runat="server" ID="lblPokokHutang"></asp:Label>
                </div>
            </div>
        <div class="form_box_header">
        <div class="form_single">
        <div class="grid_wrapper_ns">
            <asp:DataGrid ID="dtgGridShema" runat="server" Width="100%" AutoGenerateColumns="False"
                        DataKeyField="ApprovalSchemeID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />    
                        <ItemStyle CssClass="item_grid" />
                        <Columns>                            
                            <asp:BoundColumn DataField="Description" HeaderText="SKEMA LEVEL NAME">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MaxLimit" HeaderText="MAKSIMAL LIMIT">
                            <ItemStyle CssClass="item_grid_right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="MEMBER">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAction" runat="server" CommandName="member" Text='MEMBER' CausesValidation="false"></asp:LinkButton>                                
                                <asp:Label runat="server" Visible="false" ID="lblApprovalSeqNum" Text='<%# DataBinder.eval(Container,"DataItem.ApprovalSeqNum") %>'></asp:Label>                                                             
                            </ItemTemplate>
                            </asp:TemplateColumn>                          
                        </Columns>
            </asp:DataGrid>
            </div>
            </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlGridMember">
            <div class="form_box_title">
                <div class="form_single">              
                    <h4>
                        MEMBER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
            <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgGridMember" runat="server" Width="100%" AutoGenerateColumns="False"
                            DataKeyField="ApprovalSchemeID" BorderStyle="None" BorderWidth="0"
                            CssClass="grid_general">
                            <HeaderStyle CssClass="th" />    
                            <ItemStyle CssClass="item_grid" />
                            <Columns>                            
                                <asp:BoundColumn DataField="LoginID" HeaderText="LOGIN ID">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="AccountName" HeaderText="NAMA">
                                </asp:BoundColumn>    
                                <asp:BoundColumn DataField="LimitValue" HeaderText="LIMIT">
                                </asp:BoundColumn>                                                
                            </Columns>
                </asp:DataGrid>
                </div>
                </div>
                </div>
        </asp:Panel>
        <div class="form_button">
            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>    
    </ContentTemplate>
    </asp:UpdatePanel>    
    </form>
</body>
</html>
