﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class HasilSurvey_002Capacity
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucHasilSurveyTab1 As ucHasilSurveyTab
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyTabPhone
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat

    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController
    Private m_controller As New AssetDataController
    Private m_Employee As New EmployeeController
    Private time As String

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Private Property DateEntryCondition() As Date
        Get
            Return CType(ViewState("DateEntryCondition"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryCondition") = Value
        End Set
    End Property

    Private Property NTF() As Double
        Get
            Return CType(ViewState("NTF"), Double)
        End Get
        Set(ByVal Value As Double)
            ViewState("NTF") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            Me.CustomerID = Request("id")
            'Me.ProspectAppID = "-"

            'GetCookies()
            'InitialPageLoad()
            'fillFormTesting()
            'getHasilSurvey()
            'Modify by Wira 20171013
            'Ambil data hasil survey dari mobile jika add ke agreement survey
            Me.BranchID = Request("branchID")
            Me.PageAddEdit = Request("page")
            Me.ProspectAppID = Request("prospectappid")
            InitialPageLoad()
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------
            If Me.ProspectAppID <> Nothing And Me.ProspectAppID <> "-" And Request("prospectappid") <> "" And Me.PageAddEdit = "Add" Then
                getHasilMobileSurvey()
            Else
                getHasilSurvey()
            End If

        End If
    End Sub

    'Sub GetCookies()
    '    Dim cookie As HttpCookie = Request.Cookies("HASIL_SURVEY")

    '    Me.ApplicationID = cookie.Values("ApplicationID")
    '    Me.CustomerID = cookie.Values("CustomerID")
    '    Me.BranchID = cookie.Values("BranchID")
    '    Me.SupplierID = cookie.Values("SupplierID")
    'End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub


    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        Me.ApplicationID = Request("appid")

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False                
            End If
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("Capacity")
        ucHasilSurveyTab1.setLink()


        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With
        FillCbo(cboJenisPekerjaan, "dbo.TblJobType")
        FillCbo(cboJabatan, "dbo.TblJobPosition")
    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            txtLamaUsaha.Text = oPar.LamaUsaha
            txtNameUsaha.Text = oPar.NamaUsaha
            txtAlamatUsaha.Text = oPar.AlamatUsaha1
            rboAlamatUsaha.SelectedIndex = rboAlamatUsaha.Items.IndexOf(rboAlamatUsaha.Items.FindByValue(oPar.AlamatUsaha2))
            rboFotoTempatUsaha.SelectedIndex = rboFotoTempatUsaha.Items.IndexOf(rboFotoTempatUsaha.Items.FindByValue(oPar.FotoTempatUsaha))
            txtAlasanFotoTempatUsaha.Text = .AlasanFotoTempatUsaha
            rboStockBrgDagangan.SelectedIndex = rboStockBrgDagangan.Items.IndexOf(rboStockBrgDagangan.Items.FindByValue(oPar.StockBarangDagangan))
            txtAlasanStockBarangDagangan.Text = .AlasanStockBarangDagangan
            rboBonBuktiUsaha.SelectedIndex = rboBonBuktiUsaha.Items.IndexOf(rboBonBuktiUsaha.Items.FindByValue(oPar.BonBuktiUsaha))
            txtAlasanbonBuktiUsaha.Text = .AlasanBonBuktiUsaha
            txtSKUNo.Text = .SKUNo
            'txtTglSKU.Text = .TanggalSKU.ToString("dd/MM/yyyy")
            txtTglSKU.Text = IIf(.TanggalSKU.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalSKU.ToString("dd/MM/yyyy"))
            txtSIUPNo.Text = .SIUPNo
            'txtTglSIUP.Text = .TanggalSIUP.ToString("dd/MM/yyyy")
            txtTglSIUP.Text = IIf(.TanggalSIUP.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalSIUP.ToString("dd/MM/yyyy"))
            txtTDPNo.Text = .TDPNo
            'txtTglTDP.Text = .TanggalTDPNo.ToString("dd/MM/yyyy")
            txtTglTDP.Text = IIf(.TanggalTDPNo.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalTDPNo.ToString("dd/MM/yyyy"))
            txtSKDPNo.Text = .SKDPNo
            'txtTglSKDP.Text = .TanggalSKDP.ToString("dd/MM/yyyy")
            txtTglSKDP.Text = IIf(.TanggalSKDP.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalSKDP.ToString("dd/MM/yyyy"))
            rboSlipGaji.SelectedIndex = rboSlipGaji.Items.IndexOf(rboSlipGaji.Items.FindByValue(oPar.SlipGaji))
            txtAlasanSlipGaji.Text = .AlasanSlipGaji
            rboSKpenghasilan.SelectedIndex = rboSKpenghasilan.Items.IndexOf(rboSKpenghasilan.Items.FindByValue(oPar.SKPenghasilan))
            txtAlasanSKPenghasilan.Text = .AlasanSKPenghasilan
            'txtJabatan.Text = .Jabatan
            cboJenisPekerjaan.SelectedIndex = cboJenisPekerjaan.Items.IndexOf(cboJenisPekerjaan.Items.FindByValue(oPar.JenisPekerjaan))
            cboJabatan.SelectedIndex = cboJabatan.Items.IndexOf(cboJabatan.Items.FindByValue(oPar.Jabatan))
            rboNPWPPribadi.SelectedIndex = rboNPWPPribadi.Items.IndexOf(rboNPWPPribadi.Items.FindByValue(oPar.NPWPPribadi))
            txtNPWPNo.Text = .NPWPNo
            txtUraianUsaha.Text = .UraianUsahaPekerjaan
            txtAlurUsaha.Text = .AlurUsaha
            txtKapasitasKonsumen.Text = .KapasitasKonsumen
            txtKesimpulan.Text = .AnalisaCapacity
            txtRatioAngsuran.Text = .RatioAngsuran
            txtJumlahTanggungan.Text = .JumlahTanggungan
            Me.NTF = .NTF
        End With
        validasinpwp()
    End Sub

    Private Sub validasinpwp()
        If Me.NTF >= "50000000" Then
            Regularexpressionvalidator4.Enabled = True
            RequiredFieldValidator5.Enabled = True
            NoNPWP.CssClass = "label_req"
            NoNPWP.Width = 230
        Else
            Regularexpressionvalidator4.Enabled = False
            RequiredFieldValidator5.Enabled = False
            NoNPWP.CssClass = "label_general"
            NoNPWP.Width = 235
        End If
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click        
        Response.Redirect("../CreditProcess/HasilSurvey_002Character.aspx?appid=" + Me.ApplicationID + "&page=" + Request("page") + "&id=" + Request("id") + "&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.HasilSurvey
        Dim errMsg As String = ""

        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .CustomerIDSurvey = Me.CustomerID
                .LamaUsaha = txtLamaUsaha.Text
                .NamaUsaha = txtNameUsaha.Text
                .AlamatUsaha1 = txtAlamatUsaha.Text
                .AlamatUsaha2 = rboAlamatUsaha.SelectedValue
                .FotoTempatUsaha = rboFotoTempatUsaha.SelectedValue
                .AlasanFotoTempatUsaha = txtAlasanFotoTempatUsaha.Text
                .StockBarangDagangan = rboStockBrgDagangan.SelectedValue
                .AlasanStockBarangDagangan = txtAlasanStockBarangDagangan.Text
                .BonBuktiUsaha = rboBonBuktiUsaha.SelectedValue
                .AlasanBonBuktiUsaha = txtAlasanbonBuktiUsaha.Text
                .SKUNo = txtSKUNo.Text
                .TanggalSKU = Date.ParseExact(txtTglSKU.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .SIUPNo = txtSIUPNo.Text
                .TanggalSIUP = Date.ParseExact(txtTglSIUP.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .TDPNo = txtTDPNo.Text
                .TanggalTDPNo = Date.ParseExact(txtTglTDP.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .SKDPNo = txtSKDPNo.Text
                .TanggalSKDP = Date.ParseExact(txtTglSKDP.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .SlipGaji = rboSlipGaji.SelectedValue
                .AlasanSlipGaji = txtAlasanSlipGaji.Text
                .SKPenghasilan = rboSKpenghasilan.SelectedValue
                .AlasanSKPenghasilan = txtAlasanSKPenghasilan.Text
                '.Jabatan = txtJabatan.Text
                .Jabatan = cboJabatan.SelectedValue
                .NPWPPribadi = rboNPWPPribadi.SelectedValue
                .NPWPNo = txtNPWPNo.Text
                .UraianUsahaPekerjaan = txtUraianUsaha.Text
                .AlurUsaha = txtAlurUsaha.Text
                .KapasitasKonsumen = txtKapasitasKonsumen.Text
                .AnalisaCapacity = txtKesimpulan.Text
            End With

            oController.HasilSurvey002CapacitySave(oPar)

            InitialPageLoad()
            getHasilSurvey()

            ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
            ucHasilSurveyTab1.selectedTab("Capacity")
            ucHasilSurveyTab1.setLink()

            If errMsg = String.Empty Then
                ShowMessage(lblMessage, "Data saved!", False)
            Else
                ShowMessage(lblMessage, errMsg, True)
            End If

            CekTabAplikasiSurvey()
            If Me.ProspectAppID <> "-" And Me.DateEntryCondition = "1/1/1900" Then
                Response.Redirect("HasilSurvey_002Condition.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Add&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim & "&branchID=" & Me.BranchID.Trim & "&ActivityDateStart=" & Me.ActivityDateStart)
            Else
                Response.Redirect("HasilSurvey_002Condition.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Sub getHasilMobileSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilMobileSurvey(oPar)

        With oPar
            Me.ProspectAppID = oPar.ProspectAppID
            txtLamaUsaha.Text = oPar.LamaUsaha
            txtNameUsaha.Text = oPar.NamaUsaha
            txtAlamatUsaha.Text = oPar.AlamatUsaha1
            rboAlamatUsaha.SelectedIndex = rboAlamatUsaha.Items.IndexOf(rboAlamatUsaha.Items.FindByValue(oPar.AlamatUsaha2))
            rboFotoTempatUsaha.SelectedIndex = rboFotoTempatUsaha.Items.IndexOf(rboFotoTempatUsaha.Items.FindByValue(oPar.FotoTempatUsaha))
            txtAlasanFotoTempatUsaha.Text = .AlasanFotoTempatUsaha
            rboStockBrgDagangan.SelectedIndex = rboStockBrgDagangan.Items.IndexOf(rboStockBrgDagangan.Items.FindByValue(oPar.StockBarangDagangan))
            txtAlasanStockBarangDagangan.Text = .AlasanStockBarangDagangan
            rboBonBuktiUsaha.SelectedIndex = rboBonBuktiUsaha.Items.IndexOf(rboBonBuktiUsaha.Items.FindByValue(oPar.BonBuktiUsaha))
            txtAlasanbonBuktiUsaha.Text = .AlasanBonBuktiUsaha
            txtSKUNo.Text = .SKUNo
            'txtTglSKU.Text = .TanggalSKU.ToString("dd/MM/yyyy")
            txtTglSKU.Text = IIf(.TanggalSKU.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalSKU.ToString("dd/MM/yyyy"))
            txtSIUPNo.Text = .SIUPNo
            'txtTglSIUP.Text = .TanggalSIUP.ToString("dd/MM/yyyy")
            txtTglSIUP.Text = IIf(.TanggalSIUP.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalSIUP.ToString("dd/MM/yyyy"))
            txtTDPNo.Text = .TDPNo
            'txtTglTDP.Text = .TanggalTDPNo.ToString("dd/MM/yyyy")
            txtTglTDP.Text = IIf(.TanggalTDPNo.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalTDPNo.ToString("dd/MM/yyyy"))
            txtSKDPNo.Text = .SKDPNo
            'txtTglSKDP.Text = .TanggalSKDP.ToString("dd/MM/yyyy")
            txtTglSKDP.Text = IIf(.TanggalSKDP.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalSKDP.ToString("dd/MM/yyyy"))
            rboSlipGaji.SelectedIndex = rboSlipGaji.Items.IndexOf(rboSlipGaji.Items.FindByValue(oPar.SlipGaji))
            txtAlasanSlipGaji.Text = .AlasanSlipGaji
            rboSKpenghasilan.SelectedIndex = rboSKpenghasilan.Items.IndexOf(rboSKpenghasilan.Items.FindByValue(oPar.SKPenghasilan))
            txtAlasanSKPenghasilan.Text = .AlasanSKPenghasilan
            'txtJabatan.Text = .Jabatan
            cboJenisPekerjaan.SelectedIndex = cboJenisPekerjaan.Items.IndexOf(cboJenisPekerjaan.Items.FindByValue(oPar.JenisPekerjaan))
            cboJabatan.SelectedIndex = cboJabatan.Items.IndexOf(cboJabatan.Items.FindByValue(oPar.Jabatan))
            rboNPWPPribadi.SelectedIndex = rboNPWPPribadi.Items.IndexOf(rboNPWPPribadi.Items.FindByValue(oPar.NPWPPribadi))
            txtNPWPNo.Text = .NPWPNo
            txtUraianUsaha.Text = .UraianUsahaPekerjaan
            txtAlurUsaha.Text = .AlurUsaha
            txtKapasitasKonsumen.Text = .KapasitasKonsumen
            txtKesimpulan.Text = .AnalisaCapacity
            txtRatioAngsuran.Text = .RatioAngsuran
            txtJumlahTanggungan.Text = .JumlahTanggungan
            Me.NTF = .NTF
        End With
        validasinpwp()
    End Sub

    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryCondition = .DateEntryCondition
        End With
    End Sub
End Class