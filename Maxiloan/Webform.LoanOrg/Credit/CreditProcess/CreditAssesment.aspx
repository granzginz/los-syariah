﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreditAssesment.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CreditAssesment"%>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication" TagPrefix="uc3" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="UcCustomerLookUpGlobal" Src="../../../Webform.UserController/ucLookUpCustomerGlobal.ascx" %>
<%@ Register Src="../../../webform.UserController/CreditAssessmentTab.ascx" TagName="CreditAssessmentTab" TagPrefix="uc7" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript"></script>
</head>
<body>
  <%--   <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress> --%>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />

    <div runat="server" id="jlookupContent" />
<%--
     <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate> --%>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
             <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />

             <uc7:CreditAssessmentTab id="CreditAssessmentTab1" runat="server" />
                 <div class="form_box "> 
               </div>
            <div class="form_box ">
                    <div class="form_left_uc"> 
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                 <div class="form_right_uc">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div> 
            </div>
           
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        HASIL VERIFIKASI VIA TELEPON</h3>
                </div>
            </div>

            <div class="form_box">
                <div class="form_left">
                    <label>
                        No Telepon Rumah
                    </label>
                    <asp:TextBox ID="TxtAreaPhoneHm" MaxLength="4" runat="server" Width="5%" onkeypress="return numbersonly2(event)"></asp:TextBox>-
                    <asp:TextBox ID="TxtPhoneHome" MaxLength="12" runat="server"  Width="30%" onkeypress="return numbersonly2(event)"></asp:TextBox>
               <%-- <asp:RegularExpressionValidator ID="Regularexpressionvalidator8" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,4}" ErrorMessage="Harap isi Area Telepon Rumah dengan Angka" ControlToValidate="TxtAreaPhoneHm" CssClass="validator_general" />
                <asp:RegularExpressionValidator ID="Regularexpressionvalidator9" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Telepon Rumah dengan Angka" ControlToValidate="TxtPhoneHome" CssClass="validator_general" />--%>
                </div>
                <div class="form_right">
                    <label>
                        No Telepon Kantor
                    </label>
                    <asp:TextBox ID="TxtAreaPhoneOff" runat="server" MaxLength="4"  Width="5%" onkeypress="return numbersonly2(event)"></asp:TextBox>-
                    <asp:TextBox ID="TxtPhoneOff" runat="server" MaxLength="12"  Width="30%" onkeypress="return numbersonly2(event)"></asp:TextBox>
                   <%-- <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,4}" ErrorMessage="Harap isi Area Telepon Kantor dengan Angka"
                ControlToValidate="TxtAreaPhoneOff" CssClass="validator_general" />
                <asp:RegularExpressionValidator ID="Regularexpressionvalidator2" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Telepon Kantor dengan Angka"
                ControlToValidate="TxtPhoneOff" CssClass="validator_general" />--%>
                </div>
                 </div>

             <div class="form_box">
                    <div class="form_left">
                        <label>
                            No HandPhone
                        </label>
                        <asp:TextBox ID="TxtHp" runat="server" MaxLength="15" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <%--<asp:RegularExpressionValidator ID="Regularexpressionvalidator3" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,20}" ErrorMessage="Harap isi No HP dengan Angka"
                ControlToValidate="TxtHp" CssClass="validator_general" />--%>
                    </div>
                
                 <div class="form_right">
                        <label>
                            No Telepon Emergency
                        </label>
                        <asp:TextBox ID="TxtAreaPhoneEc" runat="server" MaxLength="4" Width="5%" onkeypress="return numbersonly2(event)"></asp:TextBox>-
                        <asp:TextBox ID="TxtPhoneEC" runat="server" MaxLength="12" Width="30%" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <%--<asp:RegularExpressionValidator ID="Regularexpressionvalidator4" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,4}" ErrorMessage="Harap isi Area Telepon Emergency dengan Angka"
                ControlToValidate="TxtAreaPhoneEc" CssClass="validator_general" />
                <asp:RegularExpressionValidator ID="Regularexpressionvalidator5" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Telepon Emergency dengan Angka"
                ControlToValidate="TxtPhoneEC" CssClass="validator_general" />--%>
                    </div>
               </div>

               
             <div class="form_box">
                    <div class="form_left">
                        <label>
                            No Telepon Penjamin
                        </label>
                        <asp:TextBox ID="TxtAreaPhoneGuarantee" runat="server" MaxLength="4" Width="5%" onkeypress="return numbersonly2(event)"></asp:TextBox>-
                        <asp:TextBox ID="TxtPhoneGuarantee" runat="server" MaxLength="12"  Width="30%" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <%--<asp:RegularExpressionValidator ID="Regularexpressionvalidator6" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,4}" ErrorMessage="Harap isi Area Telepon Penjamin dengan Angka"
                ControlToValidate="TxtAreaPhoneGuarantee" CssClass="validator_general" />
                <asp:RegularExpressionValidator ID="Regularexpressionvalidator7" runat="server" Display="Dynamic"
                ValidationExpression="[0-9]{0,10}" ErrorMessage="Harap isi Telepon Penjamin dengan Angka"
                ControlToValidate="TxtPhoneGuarantee" CssClass="validator_general" />--%>
                    </div>
                
            <div class="form_right">
                <label>
                    Tanggal Telepon</label>        
                <uc1:ucdatece id="txtPhoneDate" runat="server"></uc1:ucdatece>
            </div>
            </div>
            
             <div class="form_box">
                <div class="form_single">
                    <label> Pembiayaan Analyst </label>
                    <asp:DropDownList ID="cboCreditAnalyst" runat="server"></asp:DropDownList>
            </div> </div>
             
                <div class="form_box">
                 <div class="form_single">
                    <%--<label class="label_split" runat="server" id="lblNotes">--%>
                    <label class="label_split_req" runat="server" id="Label1">
                        Kesimpulan
                    </label>
                    <asp:TextBox ID="TxtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                        ErrorMessage="Harap Kesimpulan" ControlToValidate="TxtNotes" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                </div>
             

            <div class="form_button"> 
                <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button small green" /> 
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>
<%--
       </ContentTemplate>
    </asp:UpdatePanel> --%>
    </form>
</body>
</html>

