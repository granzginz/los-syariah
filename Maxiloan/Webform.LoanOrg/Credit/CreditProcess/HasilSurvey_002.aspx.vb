﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class HasilSurvey_002
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    'Protected WithEvents ucApplicationTab1 As ucApplicationTab
    Protected WithEvents ucHasilSurveyTab1 As ucHasilSurveyTab
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyTabPhone
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat
    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController
    Private time As String

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property

    Private Property CreditScore() As Decimal
        Get
            Return CType(ViewState("CreditScore"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("CreditScore") = Value
        End Set
    End Property

    Private Property CreditScoreResult() As String
        Get
            Return CType(ViewState("CreditScoreResult"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CreditScoreResult") = Value
        End Set
    End Property

    Private Property CSResult_Temp() As String
        Get
            Return CType(ViewState("CSResult_Temp"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CSResult_Temp") = Value
        End Set
    End Property
    Private Property TotalDebet() As Decimal
        Get
            Return (CType(ViewState("TotalDebet"), Decimal))
        End Get
        Set(ByVal TotalDebet As Decimal)
            ViewState("TotalDebet") = TotalDebet
        End Set
    End Property
    Private Property TotalKredit() As Decimal
        Get
            Return (CType(ViewState("TotalKredit"), Decimal))
        End Get
        Set(ByVal TotalKredit As Decimal)
            ViewState("TotalKredit") = TotalKredit
        End Set
    End Property
    Private Property myDataTable() As DataTable
        Get
            Return CType(viewstate("myDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("myDataTable") = Value
        End Set
    End Property
    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Private Property DateEntryCharacter() As Date
        Get
            Return CType(ViewState("DateEntryCharacter"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryCharacter") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            'Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("id")
            'Me.ProspectAppID = "-"
            'Modify by Wira 20171013
            'Ambil data hasil survey dari mobile jika add ke agreement survey
            Me.PageAddEdit = Request("page")
            Me.ProspectAppID = Request("prospectappid")
            InitialPageLoad()
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------
            If Me.ProspectAppID <> Nothing And Me.ProspectAppID <> "-" And Request("prospectappid") <> "" And Me.PageAddEdit = "Add" Then
                getHasilMobileSurvey()
            Else
                getHasilSurvey()
            End If

            'GetCookies()
            'InitialPageLoad()
            'fillFormTesting()
            'getHasilSurvey()

            'pnlBalance.Visible = False
            'pnlHasilSurvey.Visible = True
            'pnlScoring.Visible = False

            'cekimage()

            'If ucViewApplication1.isProceed = True Then
            'btnSave.Visible = False
            'End If

            'If cboCaraSurvey.SelectedValue = "V" Then
            '    divCaraSurvey.Attributes.Item("style") = "display:none;"
            'End If
            'If cboCaraSurvey.SelectedValue = "T" Then
            '    divCaraSurvey.Attributes.Item("style") = "display:inline;"
            'End If

        End If
    End Sub

    'Sub GetCookies()
    '    Dim cookie As HttpCookie = Request.Cookies("HASIL_SURVEY")

    '    Me.ApplicationID = cookie.Values("ApplicationID")
    '    Me.CustomerID = cookie.Values("CustomerID")
    '    Me.BranchID = cookie.Values("BranchID")
    '    Me.SupplierID = cookie.Values("SupplierID")
    'End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        Me.ApplicationID = Request("appid")

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
            End If
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("Survey")
        ucHasilSurveyTab1.setLink()


        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With
        'modify by Nofi 7Feb2018 
        Dim WhereBranchCA As String = "BranchID = '099' "
        Dim WhereBranch As String = "BranchID = '" & Replace(Me.sesBranchId, "'", "") & "'"
        Dim WhereSup As String = "SupplierID = '" & Me.SupplierID & "'"

        'FillCboEmp("BranchEmployee", cboCreditAnalyst, WhereBranchCA + " and EmployeePosition='CA' ")
        'FillCboEmp("BranchEmployee", cboSurveyor, WhereBranch + " and (EmployeePosition='SVY' or EmployeePosition='AO') ")
        FillCboEmp("BranchEmployee", cboCreditAnalyst, WhereBranchCA + " and IsCA = 1 ")
        FillCboEmp("BranchEmployee", cboSurveyor, WhereBranch + " and IsSurveyor = 1 ")

    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            'cboSurveyor.SelectedIndex = cboSurveyor.Items.IndexOf(cboSurveyor.Items.FindByValue(oPar.SurveyorID.Trim))
            ' cboCreditAnalyst.SelectedIndex = cboCreditAnalyst.Items.IndexOf(cboCreditAnalyst.Items.FindByValue(.CreditAnalyst))
            cboCreditAnalyst.SelectedIndex = cboCreditAnalyst.Items.IndexOf(cboCreditAnalyst.Items.FindByValue(.CAID.Replace(" ", "")))
            cboSurveyor.SelectedIndex = cboSurveyor.Items.IndexOf(cboSurveyor.Items.FindByValue(.SurveyorID.Replace(" ", "")))
            txtSurveyDate.Text = .AgreementSurveyDate.ToString("dd/MM/yyyy")
            cboCaraSurvey.SelectedIndex = cboCaraSurvey.Items.IndexOf(cboCaraSurvey.Items.FindByValue(oPar.CaraSurvey.ToString))

            'If oPar.SurveyorID.Trim <> "" Then
            '    lblSurveyor.Text = oPar.SurveyorID
            'Else
            '    lblSurveyor.Text = ""
            'End If
            'If oPar.CAID.Trim <> "" Then
            '    lblCreditAnalyst.Text = oPar.CAID
            'Else
            '    lblCreditAnalyst.Text = ""
            'End If
        End With
        ucHasilSurveyTabPhone1.AreaPhoneHome = oPar.SurveyAreaPhone1
        ucHasilSurveyTabPhone1.PhoneHome = oPar.SurveyPhone1
        ucHasilSurveyTabPhone1.AreaPhoneOffice = oPar.SurveyAreaPhone2
        ucHasilSurveyTabPhone1.PhoneOffice = oPar.SurveyPhone2
        ucHasilSurveyTabPhone1.Handphone = oPar.SurveyCellphone
        ucHasilSurveyTabPhone1.EmergencyContact = oPar.EmergencyPhoneContact
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../CreditProcess/HasilSurveyList_002.aspx")
    End Sub

    Protected Sub FillCboEmp(ByVal Table As String, ByVal cboName As DropDownList, ByVal Where As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable

        oAssetData.strConnection = GetConnectionString()
        oAssetData.Where = Where
        oAssetData.Table = Table
        oAssetData = oAssetDataController.GetCboEmp(oAssetData)

        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Name"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.HasilSurvey
        If cboSurveyor.SelectedItem.Value = "Select One" Then
            ShowMessage(lblMessage, "Harap pilih Surveyor", True)
            Exit Sub
        End If

        If (ConvertDate2(txtSurveyDate.Text) > BusinessDate) Then
            ShowMessage(lblMessage, "Waktu survey kecil dari sysdate.", True)
            Exit Sub
        End If
        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .SurveyorID = cboSurveyor.SelectedValue
                .CAID = cboCreditAnalyst.SelectedValue
                '.BusinessDate = Me.BusinessDate
                'Modify by WIra 20171013
                .BusinessDate = Me.ActivityDateStart
                .CaraSurvey = cboCaraSurvey.SelectedValue
                .SurveyDt = Date.ParseExact(txtSurveyDate.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .CustomerIDSurvey = Me.CustomerID
            End With
            oPar.SurveyAreaPhone1 = ucHasilSurveyTabPhone1.AreaPhoneHome
            oPar.SurveyPhone1 = ucHasilSurveyTabPhone1.PhoneHome
            oPar.SurveyAreaPhone2 = ucHasilSurveyTabPhone1.AreaPhoneOffice
            oPar.SurveyPhone2 = ucHasilSurveyTabPhone1.PhoneOffice
            oPar.SurveyCellphone = ucHasilSurveyTabPhone1.Handphone
            oPar.EmergencyPhoneContact = ucHasilSurveyTabPhone1.EmergencyContact

            oController.HasilSurvey002Save(oPar)

            Dim errMsg As String

            ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
            ucHasilSurveyTab1.selectedTab("Survey")
            ucHasilSurveyTab1.setLink()
            InitialPageLoad()
            getHasilSurvey()

            'If cboCaraSurvey.SelectedValue = "V" Then
            '    divCaraSurvey.Attributes.Item("style") = "display:none;"
            'Else
            '    divCaraSurvey.Attributes.Item("style") = "display:inline;"
            'End If

            If errMsg = String.Empty Then
                ShowMessage(lblMessage, "Data saved!", False)
            Else
                ShowMessage(lblMessage, "Data saved! but scoring data error : " & errMsg, True)
            End If

            CekTabAplikasiSurvey()
            If Me.ProspectAppID <> "-" And Me.DateEntryCharacter = "1/1/1900" Then
                Response.Redirect("HasilSurvey_002Character.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Add&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim & "&branchID=" & Me.BranchID.Trim & "&ActivityDateStart=" & Me.ActivityDateStart)
            Else
                Response.Redirect("HasilSurvey_002Character.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    'Protected Sub hitungLamaSurvey(ByVal sender As Object, ByVal e As EventArgs) Handles txtTanggalSurvey.TextChanged
    '    lblLamaSurvey.Text = DateDiff(DateInterval.Day, ConvertDate2(ucViewApplication1.TanggalAplikasi), ConvertDate2(txtTanggalSurvey.Text)).ToString
    'End Sub

    'Private Sub dtgJournal_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgJournal.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        Dim lblNo As New Label
    '        Dim lblPost As New Label
    '        Dim lblAmount As New Label
    '        Dim lblDebet As New Label
    '        Dim lblKredit As New Label

    '        lblNo = CType(e.Item.FindControl("lblNo"), Label)
    '        lblPost = CType(e.Item.FindControl("lblPost"), Label)
    '        lblAmount = CType(e.Item.FindControl("lblAmount"), Label)
    '        lblDebet = CType(e.Item.FindControl("lblDebet"), Label)
    '        lblKredit = CType(e.Item.FindControl("lblKredit"), Label)

    '        lblNo.Text = (e.Item.ItemIndex + 1).ToString

    '        If lblPost.Text.ToUpper.Trim = "C" Then
    '            lblKredit.Text = FormatNumber(lblAmount.Text, 0)
    '            TotalKredit = TotalKredit + CDec(lblKredit.Text)
    '        Else
    '            lblDebet.Text = FormatNumber(lblAmount.Text, 0)
    '            TotalDebet = TotalDebet + CDec(lblDebet.Text)
    '        End If

    '    ElseIf e.Item.ItemType = ListItemType.Footer Then
    '        Dim lblDebetTotal As New Label
    '        Dim lblKreditTotal As New Label

    '        lblDebetTotal = CType(e.Item.FindControl("lblDebetTotal"), Label)
    '        lblKreditTotal = CType(e.Item.FindControl("lblKreditTotal"), Label)
    '        lblDebetTotal.Text = FormatNumber(TotalDebet, 0)
    '        lblKreditTotal.Text = FormatNumber(TotalKredit, 0)
    '    End If
    'End Sub

    'Private Sub btnProceed_Click(sender As Object, e As System.EventArgs) Handles btnProceed.Click
    '    Dim ocustom As New Parameter.HasilSurvey

    '    With ocustom
    '        .strConnection = Me.GetConnectionString()
    '        .ApplicationID = Me.ApplicationID
    '        .BranchId = Me.sesBranchId.Replace("'", "")
    '        .BusinessDate = Me.BusinessDate
    '        oController.Proceed(ocustom)
    '    End With

    '    ShowMessage(lblMessage, "Applikasi sudah di proses", False)
    '    'btnProceed.Visible = False
    'End Sub
    '#Region "Upload image Asset"
    '    Private Sub cekimage()
    '        If File.Exists(pathFile("Survey", Me.ApplicationID) + "Kantor.jpg") Then
    '            imgLokasiKantor.ImageUrl = ResolveClientUrl("../../../../xml/" & sesBranchId.Replace("'", "") & "/Survey/" + Me.ApplicationID + "/Kantor.jpg")
    '        Else
    '            imgLokasiKantor.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
    '        End If

    '        If File.Exists(pathFile("Survey", Me.ApplicationID) + "Rumah.jpg") Then
    '            imgLokasiRumah.ImageUrl = ResolveClientUrl("../../../../xml/" & sesBranchId.Replace("'", "") & "/Survey/" + Me.ApplicationID + "/Rumah.jpg")
    '        Else
    '            imgLokasiRumah.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
    '        End If



    '        imgLokasiKantor.Height = 200
    '        imgLokasiKantor.Width = 300
    '        imgLokasiRumah.Height = 200
    '        imgLokasiRumah.Width = 300


    '    End Sub
    '    Private Function pathFile(ByVal Group As String, ByVal ApplicationID As String) As String
    '        Dim strDirectory As String = ""
    '        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ApplicationID & "\"
    '        If Not System.IO.Directory.Exists(strDirectory) Then
    '            System.IO.Directory.CreateDirectory(strDirectory)
    '        End If
    '        Return strDirectory
    '    End Function
    '    Private Sub btnupload_Click(sender As Object, e As System.EventArgs) Handles btnupload.Click
    '        Dim FileName As String = ""
    '        Dim strExtension As String = ""

    '        If uplLokasiRumah.HasFile Then
    '            If uplLokasiRumah.PostedFile.ContentType = "image/jpeg" Then
    '                FileName = "Rumah"
    '                strExtension = Path.GetExtension(uplLokasiRumah.PostedFile.FileName)
    '                uplLokasiRumah.PostedFile.SaveAs(pathFile("Survey", Me.ApplicationID) + FileName + strExtension)
    '            End If
    '        End If
    '        If uplLokasiKantor.HasFile Then
    '            If uplLokasiKantor.PostedFile.ContentType = "image/jpeg" Then
    '                FileName = "Kantor"
    '                strExtension = Path.GetExtension(uplLokasiKantor.PostedFile.FileName)
    '                uplLokasiKantor.PostedFile.SaveAs(pathFile("Survey", Me.ApplicationID) + FileName + strExtension)
    '            End If
    '        End If

    '        cekimage()
    '    End Sub
    '#End Region

    Sub getHasilMobileSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilMobileSurvey(oPar)

        With oPar
            Me.ProspectAppID = .ProspectAppID.ToString
            'cboSurveyor.SelectedIndex = cboSurveyor.Items.IndexOf(cboSurveyor.Items.FindByValue(oPar.SurveyorID.Trim))
            ' cboCreditAnalyst.SelectedIndex = cboCreditAnalyst.Items.IndexOf(cboCreditAnalyst.Items.FindByValue(.CreditAnalyst))
            cboCreditAnalyst.SelectedIndex = cboCreditAnalyst.Items.IndexOf(cboCreditAnalyst.Items.FindByValue(.CAID.Replace(" ", "")))
            cboSurveyor.SelectedIndex = cboSurveyor.Items.IndexOf(cboSurveyor.Items.FindByValue(.SurveyorID.Replace(" ", "")))
            txtSurveyDate.Text = .AgreementSurveyDate.ToString("dd/MM/yyyy")
            cboCaraSurvey.SelectedIndex = cboCaraSurvey.Items.IndexOf(cboCaraSurvey.Items.FindByValue(oPar.CaraSurvey.ToString))

            'If oPar.SurveyorID.Trim <> "" Then
            '    lblSurveyor.Text = oPar.SurveyorID
            'Else
            '    lblSurveyor.Text = ""
            'End If
            'If oPar.CAID.Trim <> "" Then
            '    lblCreditAnalyst.Text = oPar.CAID
            'Else
            '    lblCreditAnalyst.Text = ""
            'End If
        End With
        ucHasilSurveyTabPhone1.AreaPhoneHome = oPar.SurveyAreaPhone1
        ucHasilSurveyTabPhone1.PhoneHome = oPar.SurveyPhone1
        ucHasilSurveyTabPhone1.AreaPhoneOffice = oPar.SurveyAreaPhone2
        ucHasilSurveyTabPhone1.PhoneOffice = oPar.SurveyPhone2
        ucHasilSurveyTabPhone1.Handphone = oPar.SurveyCellphone
        ucHasilSurveyTabPhone1.EmergencyContact = oPar.EmergencyPhoneContact
    End Sub

    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryCharacter = .DateEntryCharacter
        End With
    End Sub

End Class
