﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class HasilSurvey_002Collateral
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucHasilSurveyTab1 As ucHasilSurveyTab
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyTabPhone
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat

    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController
    Private m_controller As New AssetDataController
    Private m_Employee As New EmployeeController
    Private time As String

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Private Property DateEntryCatatan() As Date
        Get
            Return CType(ViewState("DateEntryCatatan"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryCatatan") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            Me.CustomerID = Request("id")
            'Me.ProspectAppID = "-"

            'InitialPageLoad()
            'getHasilSurvey()
            'Modify by Wira 20171013
            'Ambil data hasil survey dari mobile jika add ke agreement survey
            Me.BranchID = Request("branchID")
            Me.PageAddEdit = Request("page")
            Me.ProspectAppID = Request("prospectappid")
            InitialPageLoad()
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------
            If Me.ProspectAppID <> Nothing And Me.ProspectAppID <> "-" And Request("prospectappid") <> "" And Me.PageAddEdit = "Add" Then
                getHasilMobileSurvey()
            Else
                getHasilSurvey()
            End If
        End If
    End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        Me.ApplicationID = Request("appid")

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
            End If
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("Collateral")
        ucHasilSurveyTab1.setLink()


        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With
    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            txtKendaraanYangDiajukan.Text = .KendaraanYangDiajukan
            txtAlasanPembelian.Text = .AlasanPembelian
            txtLokasiPenggunaan.Text = .LokasiPenggunaan
            rboPencocokanSTNKdgnFisik.SelectedIndex = rboPencocokanSTNKdgnFisik.Items.IndexOf(rboPencocokanSTNKdgnFisik.Items.FindByValue(oPar.PencocokanSTNKdenganFisik))
            rboBPKPDikeluarkanOleh.SelectedIndex = rboBPKPDikeluarkanOleh.Items.IndexOf(rboBPKPDikeluarkanOleh.Items.FindByValue(oPar.BPKBDiKeluarkanOleh))
            rboFotoRumah.SelectedIndex = rboBPKPDikeluarkanOleh.Items.IndexOf(rboBPKPDikeluarkanOleh.Items.FindByValue(oPar.FotoRumah))
            txtJumlahAssetLain.Text = .JumlahAssetLain
            txtJenisAssetLain.Text = .JenisAssetLain
            txtStatusAssetLainLunas.Text = .StatusAssetLainLunas
            txtStatusAssetLainTidakLunas.Text = .StatusAssetLainTidakLunas
            txtBPKBNo.Text = .BPKBNo
            txtNamaBPKB.Text = .NamaBPKB
            rboBPKBDiperlihatkan.SelectedIndex = rboBPKBDiperlihatkan.Items.IndexOf(rboBPKBDiperlihatkan.Items.FindByValue(oPar.BPKBDiperlihatkan))
            txtAlasanBPKBDiperlihatkan.Text = .AlasanBPKBDiperlihatkan
            txtKesimpulan.Text = .AnalisaCollateral
            cboManufacturerCountry.SelectedIndex = cboManufacturerCountry.Items.IndexOf(cboManufacturerCountry.Items.FindByValue(oPar.manufacturercountry))
            cboTipeKendaraan.SelectedIndex = cboTipeKendaraan.Items.IndexOf(cboTipeKendaraan.Items.FindByValue(oPar.AssetType))
            txtUsiaKendaraan.Text = .UsiaKendaraan
        End With
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../CreditProcess/HasilSurvey_002Capital.aspx?appid=" + Me.ApplicationID + "&page=" + Request("page") + "&id=" + Request("id") + "&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.HasilSurvey

        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .CustomerIDSurvey = Me.CustomerID
                .KendaraanYangDiajukan = txtKendaraanYangDiajukan.Text
                .AlasanPembelian = txtAlasanPembelian.Text
                .LokasiPenggunaan = txtLokasiPenggunaan.Text
                .PencocokanSTNKdenganFisik = rboPencocokanSTNKdgnFisik.SelectedValue
                .BPKBDiKeluarkanOleh = rboBPKPDikeluarkanOleh.SelectedValue
                .JumlahAssetLain = txtJumlahAssetLain.Text
                .JenisAssetLain = txtJenisAssetLain.Text
                .StatusAssetLainLunas = txtStatusAssetLainLunas.Text
                .StatusAssetLainTidakLunas = txtStatusAssetLainTidakLunas.Text
                .BPKBNo = txtBPKBNo.Text
                .NamaBPKB = txtNamaBPKB.Text
                .BPKBDiperlihatkan = rboBPKBDiperlihatkan.SelectedValue
                .AlasanBPKBDiperlihatkan = txtAlasanBPKBDiperlihatkan.Text
                .AnalisaCollateral = txtKesimpulan.Text
            End With

            oController.HasilSurvey002CollateralSave(oPar)

            Dim errMsg As String

            ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
            ucHasilSurveyTab1.selectedTab("Collateral")
            ucHasilSurveyTab1.setLink()
            InitialPageLoad()
            getHasilSurvey()

            If errMsg = String.Empty Then
                ShowMessage(lblMessage, "Data saved!", False)
            End If

            CekTabAplikasiSurvey()
            If Me.ProspectAppID <> "-" And Me.DateEntryCatatan = "1/1/1900" Then
                Response.Redirect("HasilSurvey_002Catatan.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Add&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim & "&branchID=" & Me.BranchID.Trim & "&ActivityDateStart=" & Me.ActivityDateStart)
            Else
                Response.Redirect("HasilSurvey_002Catatan.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Sub getHasilMobileSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilMobileSurvey(oPar)

        With oPar
            Me.ProspectAppID = oPar.ProspectAppID
            txtKendaraanYangDiajukan.Text = .KendaraanYangDiajukan
            txtAlasanPembelian.Text = .AlasanPembelian
            txtLokasiPenggunaan.Text = .LokasiPenggunaan
            rboPencocokanSTNKdgnFisik.SelectedIndex = rboPencocokanSTNKdgnFisik.Items.IndexOf(rboPencocokanSTNKdgnFisik.Items.FindByValue(oPar.PencocokanSTNKdenganFisik))
            rboBPKPDikeluarkanOleh.SelectedIndex = rboBPKPDikeluarkanOleh.Items.IndexOf(rboBPKPDikeluarkanOleh.Items.FindByValue(oPar.BPKBDiKeluarkanOleh))
            rboFotoRumah.SelectedIndex = rboBPKPDikeluarkanOleh.Items.IndexOf(rboBPKPDikeluarkanOleh.Items.FindByValue(oPar.FotoRumah))
            txtJumlahAssetLain.Text = .JumlahAssetLain
            txtJenisAssetLain.Text = .JenisAssetLain
            txtStatusAssetLainLunas.Text = .StatusAssetLainLunas
            txtStatusAssetLainTidakLunas.Text = .StatusAssetLainTidakLunas
            txtBPKBNo.Text = .BPKBNo
            txtNamaBPKB.Text = .NamaBPKB
            rboBPKBDiperlihatkan.SelectedIndex = rboBPKBDiperlihatkan.Items.IndexOf(rboBPKBDiperlihatkan.Items.FindByValue(oPar.BPKBDiperlihatkan))
            txtAlasanBPKBDiperlihatkan.Text = .AlasanBPKBDiperlihatkan
            txtKesimpulan.Text = .AnalisaCollateral
            cboManufacturerCountry.SelectedIndex = cboManufacturerCountry.Items.IndexOf(cboManufacturerCountry.Items.FindByValue(oPar.ManufacturerCountry))
            cboTipeKendaraan.SelectedIndex = cboTipeKendaraan.Items.IndexOf(cboTipeKendaraan.Items.FindByValue(oPar.AssetType))
            txtUsiaKendaraan.Text = .UsiaKendaraan
        End With
    End Sub

    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryCatatan = .DateEntryCatatan
        End With
    End Sub
End Class