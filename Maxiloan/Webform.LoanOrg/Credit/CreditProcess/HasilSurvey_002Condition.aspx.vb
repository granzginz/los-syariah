﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic
Imports System.Data.SqlClient

Public Class HasilSurvey_002Condition
        Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucHasilSurveyTab1 As ucHasilSurveyTab
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyTabPhone
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat

    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController
    Private m_controller As New AssetDataController
    Private m_Employee As New EmployeeController
    Private time As String

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Private Property DateEntryCapital() As Date
        Get
            Return CType(ViewState("DateEntryCapital"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryCapital") = Value
        End Set
    End Property
#End Region

    'Protected Sub sum()
    '    var SisaSebelumTambahUnit1 = document.getElementById('txtSisaSebelumTambahUnit1').value;
    '        var IncomeDariUnit1 = document.getElementById('txtIncomeDariUnit1').value;
    '    var EstimasiAngsuran1 = document.getElementById('txtEstimasiAngsuran1').value;
    '        var result = parseInt(SisaSebelumTambahUnit1.replace(/\s*,\ s */ g, '')) + parseInt(IncomeDariUnit1.replace(/\s*,\s*/g, '')) - parseInt(EstimasiAngsuran1.replace(/\s*,\s*/g, ''));

    '    Result += '';
    '        Result = Result.replace(",", "");
    '        x = Result.split('.');
    '        x1 = x[0];
    '        x2 = x.length > 1 ? '.' + x[1] : '';
    '        var rgx = / (\ d +)(\ d{3}) /;
    '        While (rgx.test(x1)) {
    '            x1 = x1.replace(rgx, '$1' + ',' + '$2');
    '        }
    'End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            Me.CustomerID = Request("id")
            'Me.ProspectAppID = "-"

            'GetCookies()
            'InitialPageLoad()
            'fillFormTesting()
            'getHasilSurvey()

            'Modify by Wira 20171013
            'Ambil data hasil survey dari mobile jika add ke agreement survey
            Me.BranchID = Request("branchID")
            Me.PageAddEdit = Request("page")
            Me.ProspectAppID = Request("prospectappid")
            InitialPageLoad()
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------
            If Me.ProspectAppID <> Nothing And Me.ProspectAppID <> "-" And Request("prospectappid") <> "" And Me.PageAddEdit = "Add" Then
                getHasilMobileSurvey()
            Else
                getHasilSurvey()
            End If
        End If
    End Sub

    'Sub GetCookies()
    '    Dim cookie As HttpCookie = Request.Cookies("HASIL_SURVEY")

    '    Me.ApplicationID = cookie.Values("ApplicationID")
    '    Me.CustomerID = cookie.Values("CustomerID")
    '    Me.BranchID = cookie.Values("BranchID")
    '    Me.SupplierID = cookie.Values("SupplierID")
    'End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        Me.ApplicationID = Request("appid")

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
            End If
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("Condition")
        ucHasilSurveyTab1.setLink()


        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With
    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            txtTotalPenghasilan1.Text = FormatNumber(CStr(.TotalPenghasilan1), 0)
            txtTotalPenghasilan2.Text = FormatNumber(CStr(.TotalPenghasilan2), 0)
            txtBiayaUsaha1.Text = FormatNumber(CStr(.BiayaUsaha1), 0)
            txtBiayaUsaha2.Text = FormatNumber(CStr(.BiayaUsaha2), 0)
            txtBiayaHidup1.Text = FormatNumber(CStr(.BiayaHidup1), 0)
            txtBiayaHidup2.Text = FormatNumber(CStr(.BiayaHidup2), 0)
            txtBiayaCicilan1.Text = FormatNumber(CStr(.BiayaCicilan1), 0)
            txtBiayaCicilan2.Text = FormatNumber(CStr(.BiayaCicilan2), 0)
            txtSisaSebelumTambahUnit1.Text = FormatNumber(CStr(.SisaSebelumTambahUnit1), 0)
            txtSisaSebelumTambahUnit2.Text = FormatNumber(CStr(.SisaSebelumTambahUnit2), 0)
            txtIncomeDariUnit1.Text = FormatNumber(CStr(.IncomeDariUnit1), 0)
            txtIncomeDariUnit2.Text = FormatNumber(CStr(.IncomeDariUnit2), 0)
            If .EstimasiAngsuran1 = 0 Then
                getDataInsAmount1(Me.ApplicationID)
            Else
                txtEstimasiAngsuran1.Text = FormatNumber(CStr(.EstimasiAngsuran1), 0)
            End If
            If .EstimasiAngsuran2 = 0 Then
                getDataInsAmount2(Me.ApplicationID)
            Else
                txtEstimasiAngsuran2.Text = FormatNumber(CStr(.EstimasiAngsuran2), 0)
            End If

            'txtEstimasiAngsuran1.Text = FormatNumber(CStr(.EstimasiAngsuran1), 0)
            'txtEstimasiAngsuran2.Text = FormatNumber(CStr(.EstimasiAngsuran2), 0)
            txtSisaPendapatan1.Text = FormatNumber(CStr(.SisaPendapatan1), 0)
            txtSisaPendapatan2.Text = FormatNumber(CStr(.SisaPendapatan2), 0)
            rboRekeningTabunganGiro.SelectedIndex = rboRekeningTabunganGiro.Items.IndexOf(rboRekeningTabunganGiro.Items.FindByValue(oPar.RekeningTabunganGio))
            txtNamaBank.Text = .NamaBank
            rboLaporanKeuangan.SelectedIndex = rboLaporanKeuangan.Items.IndexOf(rboLaporanKeuangan.Items.FindByValue(oPar.LaporanKeuangan))
            txtAlasanLaporanKeuangan.Text = .AlasanLaporanKeuangan
            rboKreditdariBankLNKB.SelectedIndex = rboKreditdariBankLNKB.Items.IndexOf(rboKreditdariBankLNKB.Items.FindByValue(oPar.KreditdariBankLKNB))
            txtNamaInstitusi1.Text = .NamaInstitusi1
            txtAngsuran1.Text = FormatNumber(CStr(.Angsuran1), 0)
            txtSisa1.Text = FormatNumber(CStr(.Sisa1), 0)
            txtNamaInstitusi2.Text = .NamaInstitusi2
            txtAngsuran2.Text = FormatNumber(CStr(.Angsuran2), 0)
            txtSisa2.Text = FormatNumber(CStr(.Sisa2), 0)
            rboBuktiPembayaranAngsuran.SelectedIndex = rboBuktiPembayaranAngsuran.Items.IndexOf(rboBuktiPembayaranAngsuran.Items.FindByValue(oPar.BuktiPembayaranAngsuran))
            txtAlasanBuktiPembayaranAng.Text = .AlasanBuktiPembayaranAngsuran
            txtKesimpulan.Text = .AnalisaCondition
            cboKondisiKesehatan.SelectedIndex = cboKondisiKesehatan.Items.IndexOf(cboKondisiKesehatan.Items.FindByValue(.KondisiKesehatan))
            cboIndustryRisk.SelectedIndex = cboIndustryRisk.Items.IndexOf(cboIndustryRisk.Items.FindByValue(.IndustryRisk))
            cboKondisiLingkungan.SelectedIndex = cboKondisiLingkungan.Items.IndexOf(cboKondisiLingkungan.Items.FindByValue(.Hubling))
            'cboUsiaPemohon.SelectedIndex = cboUsiaPemohon.Items.IndexOf(cboKondisiKesehatan.Items.FindByValue(.UsiaPemohon))
            txtUsiaPemohon.Text = .UsiaPemohon
            cboLamaTinggal.SelectedIndex = cboLamaTinggal.Items.IndexOf(cboLamaTinggal.Items.FindByValue(.LamaTinggal))
        End With
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click        
        Response.Redirect("../CreditProcess/HasilSurvey_002Capacity.aspx?appid=" + Me.ApplicationID + "&page=" + Request("page") + "&id=" + Request("id") + "&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.HasilSurvey
        Dim errMsg As String = ""

        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .CustomerIDSurvey = Me.CustomerID
                .TotalPenghasilan1 = IIf(IsNumeric(txtTotalPenghasilan1.Text), txtTotalPenghasilan1.Text, 0)
                .TotalPenghasilan2 = IIf(IsNumeric(txtTotalPenghasilan2.Text), txtTotalPenghasilan2.Text, 0)
                .BiayaUsaha1 = IIf(IsNumeric(txtBiayaUsaha1.Text), txtBiayaUsaha1.Text, 0)
                .BiayaUsaha2 = IIf(IsNumeric(txtBiayaUsaha2.Text), txtBiayaUsaha2.Text, 0)
                .BiayaHidup1 = IIf(IsNumeric(txtBiayaHidup1.Text), txtBiayaHidup1.Text, 0)
                .BiayaHidup2 = IIf(IsNumeric(txtBiayaHidup2.Text), txtBiayaHidup2.Text, 0)
                .BiayaCicilan1 = IIf(IsNumeric(txtBiayaCicilan1.Text), txtBiayaCicilan1.Text, 0)
                .BiayaCicilan2 = IIf(IsNumeric(txtBiayaCicilan2.Text), txtBiayaCicilan2.Text, 0)
                .SisaSebelumTambahUnit1 = IIf(IsNumeric(txtSisaSebelumTambahUnit1.Text), txtSisaSebelumTambahUnit1.Text, 0)
                .SisaSebelumTambahUnit2 = IIf(IsNumeric(txtSisaSebelumTambahUnit2.Text), txtSisaSebelumTambahUnit2.Text, 0)
                .IncomeDariUnit1 = IIf(IsNumeric(txtIncomeDariUnit1.Text), txtIncomeDariUnit1.Text, 0)
                .IncomeDariUnit2 = IIf(IsNumeric(txtIncomeDariUnit2.Text), txtIncomeDariUnit2.Text, 0)
                .EstimasiAngsuran1 = IIf(IsNumeric(txtEstimasiAngsuran1.Text), txtEstimasiAngsuran1.Text, 0)
                .EstimasiAngsuran2 = IIf(IsNumeric(txtEstimasiAngsuran2.Text), txtEstimasiAngsuran2.Text, 0)
                .SisaPendapatan1 = IIf(IsNumeric(txtSisaPendapatan1.Text), txtSisaPendapatan1.Text, 0)
                .SisaPendapatan2 = IIf(IsNumeric(txtSisaPendapatan2.Text), txtSisaPendapatan2.Text, 0)
                .RekeningTabunganGio = rboRekeningTabunganGiro.SelectedValue
                .NamaBank = txtNamaBank.Text
                .LaporanKeuangan = rboLaporanKeuangan.SelectedValue
                .AlasanLaporanKeuangan = txtAlasanLaporanKeuangan.Text
                .KreditdariBankLKNB = rboKreditdariBankLNKB.SelectedValue
                .NamaInstitusi1 = txtNamaInstitusi1.Text
                .Angsuran1 = IIf(IsNumeric(txtAngsuran1.Text), txtAngsuran1.Text, 0)
                .Sisa1 = IIf(IsNumeric(txtSisa1.Text), txtSisa1.Text, 0)
                .NamaInstitusi2 = txtNamaInstitusi2.Text
                .Angsuran2 = IIf(IsNumeric(txtAngsuran2.Text), txtAngsuran2.Text, 0)
                .Sisa2 = IIf(IsNumeric(txtSisa2.Text), txtSisa2.Text, 0)
                .BuktiPembayaranAngsuran = rboBuktiPembayaranAngsuran.SelectedValue
                .AlasanBuktiPembayaranAngsuran = txtAlasanBuktiPembayaranAng.Text
                .AnalisaCondition = txtKesimpulan.Text
                .KondisiKesehatan = cboKondisiKesehatan.SelectedValue
                .IndustryRisk = cboIndustryRisk.SelectedValue
                .KondisiLingkungan = cboKondisiLingkungan.SelectedValue
                '.UsiaPemohon = cboUsiaPemohon.SelectedValue
                .UsiaPemohon = txtUsiaPemohon.Text
                .LamaTinggal = cboLamaTinggal.SelectedValue
            End With

            oController.HasilSurvey002ConditionSave(oPar)

            ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
            ucHasilSurveyTab1.selectedTab("Condition")
            ucHasilSurveyTab1.setLink()
            InitialPageLoad()
            getHasilSurvey()

            If errMsg = String.Empty Or errMsg = Nothing Then
                ShowMessage(lblMessage, "Data saved!", False)
            Else
                ShowMessage(lblMessage, errMsg, True)
            End If

            CekTabAplikasiSurvey()
            If Me.ProspectAppID <> "-" And Me.DateEntryCapital = "1/1/1900" Then
                Response.Redirect("HasilSurvey_002Capital.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Add&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim & "&branchID=" & Me.BranchID.Trim & "&ActivityDateStart=" & Me.ActivityDateStart)
            Else
                Response.Redirect("HasilSurvey_002Capital.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub getDataInsAmount1(ByVal Code As String)
        Dim dt As New DataTable()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim adapter As New SqlDataAdapter

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandText = "SELECT * FROM dbo.Agreement WHERE ApplicationID = @Id"
            objCommand.Connection = objConnection
            adapter.SelectCommand = objCommand
            objCommand.Parameters.AddWithValue("@Id", Code)
            adapter.Fill(dt)
            If dt.Rows.Count > 0 Then
                'txtEstimasiAngsuran1.Text = dt.Rows(0)("InstallmentAmount").ToString
                txtEstimasiAngsuran1.Text = FormatNumber(CStr(dt.Rows(0)("InstallmentAmount").ToString), 0)
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Private Sub getDataInsAmount2(ByVal Code As String)
        Dim dt As New DataTable()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim adapter As New SqlDataAdapter

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandText = "SELECT * FROM dbo.Agreement WHERE ApplicationID = @Id"
            objCommand.Connection = objConnection
            adapter.SelectCommand = objCommand
            objCommand.Parameters.AddWithValue("@Id", Code)
            adapter.Fill(dt)
            If dt.Rows.Count > 0 Then
                'txtEstimasiAngsuran2.Text = dt.Rows(0)("InstallmentAmount").ToString
                txtEstimasiAngsuran2.Text = FormatNumber(CStr(dt.Rows(0)("InstallmentAmount").ToString), 0)
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Sub getHasilMobileSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilMobileSurvey(oPar)

        With oPar
            Me.ProspectAppID = oPar.ProspectAppID
            txtTotalPenghasilan1.Text = FormatNumber(CStr(.TotalPenghasilan1), 0)
            txtTotalPenghasilan2.Text = FormatNumber(CStr(.TotalPenghasilan2), 0)
            txtBiayaUsaha1.Text = FormatNumber(CStr(.BiayaUsaha1), 0)
            txtBiayaUsaha2.Text = FormatNumber(CStr(.BiayaUsaha2), 0)
            txtBiayaHidup1.Text = FormatNumber(CStr(.BiayaHidup1), 0)
            txtBiayaHidup2.Text = FormatNumber(CStr(.BiayaHidup2), 0)
            txtBiayaCicilan1.Text = FormatNumber(CStr(.BiayaCicilan1), 0)
            txtBiayaCicilan2.Text = FormatNumber(CStr(.BiayaCicilan2), 0)

            If .EstimasiAngsuran1 = 0 Then
                getDataInsAmount1(Me.ApplicationID)
            Else
                txtEstimasiAngsuran1.Text = FormatNumber(CStr(.EstimasiAngsuran1), 0)
            End If

            txtSisaSebelumTambahUnit1.Text = (FormatNumber(CStr(.TotalPenghasilan1), 0) - FormatNumber(CStr(.BiayaUsaha1), 0)) - (FormatNumber(CStr(.BiayaHidup1), 0) - FormatNumber(CStr(.BiayaCicilan1), 0))
            txtSisaSebelumTambahUnit2.Text = FormatNumber(CStr(.SisaSebelumTambahUnit2), 0)
            'txtIncomeDariUnit1.Text = (FormatNumber(CStr(txtSisaSebelumTambahUnit1.Text), 0) + FormatNumber(CStr(.IncomeDariUnit1), 0)) - FormatNumber(CStr(.EstimasiAngsuran1), 0)
            txtIncomeDariUnit1.Text = FormatNumber(CStr(.IncomeDariUnit1), 0)
            txtIncomeDariUnit2.Text = FormatNumber(CStr(.IncomeDariUnit2), 0)

            If .EstimasiAngsuran2 = 0 Then
                getDataInsAmount2(Me.ApplicationID)
            Else
                txtEstimasiAngsuran2.Text = FormatNumber(CStr(.EstimasiAngsuran2), 0)
            End If

            'txtEstimasiAngsuran1.Text = FormatNumber(CStr(.EstimasiAngsuran1), 0)
            'txtEstimasiAngsuran2.Text = FormatNumber(CStr(.EstimasiAngsuran2), 0)
            txtSisaPendapatan1.Text = FormatNumber(CStr(.SisaPendapatan1), 0)
            txtSisaPendapatan2.Text = FormatNumber(CStr(.SisaPendapatan2), 0)
            rboRekeningTabunganGiro.SelectedIndex = rboRekeningTabunganGiro.Items.IndexOf(rboRekeningTabunganGiro.Items.FindByValue(oPar.RekeningTabunganGio))
            txtNamaBank.Text = .NamaBank
            rboLaporanKeuangan.SelectedIndex = rboLaporanKeuangan.Items.IndexOf(rboLaporanKeuangan.Items.FindByValue(oPar.LaporanKeuangan))
            txtAlasanLaporanKeuangan.Text = .AlasanLaporanKeuangan
            rboKreditdariBankLNKB.SelectedIndex = rboKreditdariBankLNKB.Items.IndexOf(rboKreditdariBankLNKB.Items.FindByValue(oPar.KreditdariBankLKNB))
            txtNamaInstitusi1.Text = .NamaInstitusi1
            txtAngsuran1.Text = FormatNumber(CStr(.Angsuran1), 0)
            txtSisa1.Text = FormatNumber(CStr(.Sisa1), 0)
            txtNamaInstitusi2.Text = .NamaInstitusi2
            txtAngsuran2.Text = FormatNumber(CStr(.Angsuran2), 0)
            txtSisa2.Text = FormatNumber(CStr(.Sisa2), 0)
            rboBuktiPembayaranAngsuran.SelectedIndex = rboBuktiPembayaranAngsuran.Items.IndexOf(rboBuktiPembayaranAngsuran.Items.FindByValue(oPar.BuktiPembayaranAngsuran))
            txtAlasanBuktiPembayaranAng.Text = .AlasanBuktiPembayaranAngsuran
            txtKesimpulan.Text = .AnalisaCondition
            cboKondisiKesehatan.SelectedIndex = cboKondisiKesehatan.Items.IndexOf(cboKondisiKesehatan.Items.FindByValue(.KondisiKesehatan))
            cboIndustryRisk.SelectedIndex = cboIndustryRisk.Items.IndexOf(cboIndustryRisk.Items.FindByValue(.IndustryRisk))
            cboKondisiLingkungan.SelectedIndex = cboKondisiLingkungan.Items.IndexOf(cboKondisiLingkungan.Items.FindByValue(.Hubling))
            'cboUsiaPemohon.SelectedIndex = cboUsiaPemohon.Items.IndexOf(cboKondisiKesehatan.Items.FindByValue(.UsiaPemohon))
            txtUsiaPemohon.Text = .UsiaPemohon
            cboLamaTinggal.SelectedIndex = cboLamaTinggal.Items.IndexOf(cboLamaTinggal.Items.FindByValue(.LamaTinggal))
        End With
    End Sub

    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryCapital = .DateEntryCapital
        End With
    End Sub
End Class