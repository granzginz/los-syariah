﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InvoiceDeductions.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.InvoiceDeductions" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"  TagPrefix="uc1" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../../Webform.UserController/UcBankAccount.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
       <title>Invoice Deduction</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INVOICE DEALER</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
       <div class="form_title">
            <div class="form_single"> <h4> PROSES INVOICE DEALER</h4> </div>
        </div> 

        <div class="form_box">
            <div>
                <div class="form_left">
                    <label> Nama Dealer </label>
                    <asp:HyperLink ID="hySupplierName" runat="server"></asp:HyperLink>
                </div>

                 <div class="form_right">
                    <label>Nama Konsumen</label>
                     <asp:HyperLink ID="hyNamaKonsumen" runat="server"></asp:HyperLink>      
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">   
                    <label> No Kontrak </label>
                    <asp:HyperLink ID="hyNoKontrak" runat="server"></asp:HyperLink>
                </div>
            </div>
                 <div class="form_right">
                    <label> Nilai Invoice </label>  
                     <div style='text-align: right; width:100px;display: inline-block;'>                  
                     <asp:label ID="lblNilaiInvoice" runat="server"  ></asp:label>
                      </div>
                </div>
         </div>
        
         <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Tanggal Invoice </label> 
                        <asp:label ID="lblTglInvoice" runat="server"  ></asp:label>     
                </div>
               <div class="form_right">
                    <label>Jumlah Potongan</label>     <div style='text-align: right; width:100px;display: inline-block;'>                  
                     <asp:label ID="lblJumPotongan" runat="server"  ></asp:label></div>
                </div>
            </div>
        </div>

         <div class="form_box">
            <div>
                <div class="form_left">
                    <label>No Invoice </label>
                    <asp:label ID="lblNoInvoice" runat="server"  ></asp:label>                   
                </div>
                <div class="form_right"> 
                     <label>Jumlah Pencairan</label>    
                            <div style='text-align: right; width:100px;display: inline-block;'>
                                <asp:label ID="lblJumlahPencairan" runat="server"  ></asp:label>
                            </div>                 
                </div> 
            </div>
        </div>

        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>Tanggal Rencana Cair </label>
                    <asp:label ID="lblTglRencanaCair" runat="server"  ></asp:label>                   
                </div>
               <div class="form_right">
                        <label>Refund Dealer</label> 
                        <div style='text-align: right; width:100px;display: inline-block;'>   
                            <asp:label ID="lblRefund" runat="server"  ></asp:label>
                        </div>
                    </div>            
             </div> 
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label class="label_req">Tanggal Angsuran Pertama</label>
                    <uc1:ucdatece id="txtFirstInstallmentDate" runat="server"></uc1:ucdatece>   
                </div>
                <div class="form_right" > <label>Total</label> 
                    <div style='text-align: right; width:100px;display: inline-block;'>
                            <asp:label ID="lblTotInv" runat="server"  ></asp:label>
                     </div>
                </div>
            </div>
        </div>
        
        <div class="form_title">
            <div class="form_single"> <h4> POTONGAN PENCAIRAN</h4> </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid  ID="dtgDeduction" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                     BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general" ShowHeaderWhenEmpty="True"
                    DataKeyNames="Seq"  >
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns> 
                         
                        <asp:TemplateColumn HeaderText="NO"> 
                            <ItemTemplate>
                                <asp:Label ID="lblseqE" runat="server" Text='<%#Eval("Seq") %>'></asp:Label>
                            </ItemTemplate>  
                         </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JENIS PEMOTONGAN">
                            <ItemTemplate>
                                <asp:Label ID="lblJenisPemotongan" runat="server" Text='<%#Eval("DeductionDescription") %>'></asp:Label>
                            </ItemTemplate>  
                         </asp:TemplateColumn>
                          <asp:TemplateColumn HeaderText="DATA ID">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkApplicationTo" runat="server" Text='<%#Eval("ApplicationIDTo") %>'></asp:HyperLink>
                            </ItemTemplate>  
                         </asp:TemplateColumn>
                         <asp:TemplateColumn HeaderText="JUMLAH">
                             <ItemTemplate>
                                  <div style='text-align: right; width:100px;display: inline-block;'>     
                                   <asp:Label ID="lblJumlah" runat="server" Text='<%# String.Format("{0:#,##0}",Eval("DeductionAmount")) %>'></asp:Label>
                                   </div>
                             </ItemTemplate>
                         </asp:TemplateColumn> 
                    </Columns>
                </asp:DataGrid>

                <asp:panel id='pnlAddDeduction' runat='server'>
                <div class="form_box"></div>
                     <div class="form_box">
                        <div class="form_single">
                             <label>Jenis Pemotongan</label>
                             <asp:DropDownList ID="ddlJenisPemotonganNew" runat="server" />
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                             <label>Jumlah</label>
                              <uc1:ucNumberFormat ID="txtjmlPemotonganNew" runat="server" />
                        </div>
                    </div> 
                       <div class="form_button"> 
                        <asp:Button ID="btnAddOk" runat="server" CausesValidation="True" Text="OK" CssClass="small button blue"/> 
                         <asp:Button ID="btnAddCancel" runat="server" Text="Cancel" CssClass="small button gray"  CausesValidation="False" />
                     </div> 
                </asp:panel>
                 
                 <div class="form_button" id='divAddDeduc' runat='server'> 
                    <asp:Button ID="btnAddDeduction" runat="server" CausesValidation="False" Text="Add" visible="false" CssClass="small button blue"/> 
                 </div> 
             </div>
        </div> 


          <div class="form_title">
            <div class="form_single"> <h4>REKENING BANK</h4> </div>
        </div>
         <div class="form_box_uc">
             <uc1:ucbankaccount id="UcBankAccount" runat="server"></uc1:ucbankaccount>
         </div>
        <div class="form_box_hide">
            <div class="form_single">
                 <label>Rekening Bank Pembayaran</label>
                 <asp:DropDownList ID="ddlRekBank" runat="server"  style='max-width:2500px !important;'/>
            </div>
        </div>

         <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green" />
                    <asp:Button ID="btnCancelC" runat="server" Text="Cancel" CssClass="small button gray"  CausesValidation="False" />
         </div>
    </asp:Panel>
     
    </form>
</body>
</html>
