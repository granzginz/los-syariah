﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApprovalBm.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ApprovalBm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Approval BM</title>


      <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;

        function OpenWinApplication(pApplicationID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinSupplier(pSupplierID, pStyle) {
            window.open(ServerName + App + '/Webform.SalesMarketing/ViewSupplier.aspx?style=' + pStyle + '&SupplierID=' + pSupplierID, 'Supplier', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinEmployee(pBranchID, pAOID, pStyle) {
            window.open(ServerName + App + '/Webform.Setup/Organization/EmployeeView.aspx?style=' + pStyle + '&BranchID=' + pBranchID + '&EmployeeID=' + pAOID, 'Employee', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinAgreementNo(pApplicationId, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?ApplicationId=' + pApplicationId + '&style=' + pStyle, 'AgreementNo', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
			
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <asp:Label ID="lblMessage" runat="server" />
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip"> </div>
            <div class="form_single">
                <h3> DAFTAR APLIKASI </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False" OnSortCommand="Sorting" BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <HeaderStyle Width="7%" />
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkRCA" runat="server" CausesValidation="False" CommandName="REQ">PILIH</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                            <HeaderStyle Width="20%" HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkApplicationID" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID")%>' NavigateUrl='' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                            <HeaderStyle Width="21%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkName" Text='<%# DataBinder.eval(Container,"DataItem.Name")%>' NavigateUrl='' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="AO" HeaderText="NAMA ACCOUNT OFFICER">
                            <HeaderStyle Width="18%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnkAO" Text='<%# DataBinder.eval(Container,"DataItem.AO")%>' NavigateUrl='' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn SortExpression="CA" HeaderText="NAMA PEMBIAYAAN ANALYST">
                            <HeaderStyle Width="18%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate> <asp:HyperLink runat="server" ID="lnkCA" Text='<%# DataBinder.eval(Container,"DataItem.CA")%>' NavigateUrl='' />
                            </ItemTemplate>
                        </asp:TemplateColumn> 
                        <asp:BoundColumn DataField="ScoringDate" SortExpression="ScoringDate" HeaderText="TGL SCORING" DataFormatString='{0:dd/MM/yyyy}' Visible="False">
                            <HeaderStyle Width="12%" />
                        </asp:BoundColumn> 
                        <asp:TemplateColumn HeaderText="ApplicationID" Visible="False">
                            <HeaderStyle Width="21%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblApplicationID" Text='<%# DataBinder.eval(Container,"DataItem.ApplicationID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CustomerID" Visible="False">
                            <HeaderStyle Width="21%" />
                            <ItemStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCustomerID" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="AOID" Visible="False"> 
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAOID" Text='<%# DataBinder.eval(Container,"DataItem.AOID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CAID" Visible="False"> 
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCAID" Text='<%# DataBinder.eval(Container,"DataItem.CAID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SchemeID" Visible="False"> 
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblSchemeID" Text='<%# DataBinder.eval(Container,"DataItem.SchemeID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NTF" Visible="False"> 
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblNTF" Text='<%# DataBinder.eval(Container,"DataItem.NTF")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="MODULE"> 
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblmodule" Text='<%# DataBinder.Eval(Container, "DataItem.ApplicationModule")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <uc2:ucGridNav id="GridNavigator" runat="server"/> 
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4> CARI APLIKASI</h4>
            </div>
        </div>
        <div class="form_box">
            <label class="label_medium"> Cari Berdasarkan </label>
            <asp:DropDownList ID="cboSearch" runat="server">
            <asp:ListItem Value="ApplicationID">No Aplikasi</asp:ListItem>
            <asp:ListItem Value="Name" Selected="True">Nama Customer</asp:ListItem>
            <asp:ListItem Value="AO">Nama Account Officer</asp:ListItem>
            <asp:ListItem Value="CA">Nama CA</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="txtSearch" runat="server" /> 
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" text="Find" CssClass ="small button blue" />            
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" text="Reset" CssClass="small button gray" >
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
