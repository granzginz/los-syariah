﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Invoice.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.Invoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Invoice</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
       
        function fBack() {
            history.back(-1);
            return false;
        }
        function fClose() {
            window.close();
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        function click() {
            if (event.button == 2) {
                alert('Anda tidak berhak');
            }
        }
        document.onmousedown = click
		
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INVOICE SUPPLIER</h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR SUPPLIER</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" DataKeyField="SupplierID"
                    CellSpacing="1" CellPadding="3" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                    AllowSorting="True" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                            <ItemStyle HorizontalAlign="Center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkAction" runat="server" CommandName="Invoice" Text='INVOICE'
                                    CausesValidation="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ID SUPPLIER" SortExpression="SupplierID" Visible="False">
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="hypSupplierID" runat="server" Text='<%#Container.DataItem("SupplierID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CABANG" SortExpression="BranchFullName">
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:label ID="lblCabang" runat="server" Text='<%#container.dataitem("BranchFullName")%>'>
                                </asp:label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CUSTOMER" SortExpression="CustomerName">
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:label ID="lblCustomerName" runat="server" Text='<%#container.dataitem("CustomerName")%>'>
                                </asp:label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA SUPPLIER" SortExpression="SupplierName">
                            <ItemStyle HorizontalAlign="left" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hySupplierName" runat="server" Text='<%#container.dataitem("SupplierName")%>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="KONTAK" SortExpression="ContactPersonName">
                            <ItemStyle HorizontalAlign="left" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblContactPersonName" runat="server" Text='<%#container.dataitem("ContactPersonName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TELP SUPPLIER" SortExpression="Phone">
                            <ItemStyle HorizontalAlign="left" Width="12%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPhone" runat="server" Text='<%#container.dataitem("Phone")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ALAMAT" SortExpression="Address">
                            <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%#container.dataitem("Address")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BRANCH ID" SortExpression="BranchID" Visible="False">
                            <ItemStyle HorizontalAlign="left" Width="10%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblBranchID" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI INVOICE</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cabang
                    </label>
                    <asp:DropDownList ID="oBranch" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
                        Display="Dynamic" ErrorMessage="Harap Pilih Cabang" ControlToValidate="oBranch"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_single">
                    <label>
                        Cari Berdasarkan
                    </label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                        <asp:ListItem Value="Address">Alamat</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="true" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlInvoice" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PROSES INVOICE SUPPLIER</h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Supplier
                    </label>
                    <asp:HyperLink ID="lblInvSupplierName" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Invoice
                    </label>
                    <uc1:ucdatece id="txtInvoiceDate" runat="server"></uc1:ucdatece>     
                    <asp:Label ID="lblErrMsgInvoiceDate" runat="server" Visible="False" CssClass="validator_general"></asp:Label>               
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No. Invoice
                    </label>
                    <asp:TextBox ID="txtInvInvoiceNo" runat="server" MaxLength="25"></asp:TextBox>                   
                </div>
                <div class="form_right">
                    <label> Nilai Invoice
                    </label>                    
                    <uc1:ucnumberformat id="txtInvInvoiceAmount" runat="server" />
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Rencana Cair</label>
                <uc1:ucdatece runat="server" id="txtAPDueDate"></uc1:ucdatece>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KONTRAK</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgListOfAgreement" runat="server" Width="100%" DataKeyField="AgreementNo"
                    CellSpacing="1" CellPadding="3" BorderWidth="0" OnSortCommand="Sorting" AutoGenerateColumns="False"
                    AllowSorting="True" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="imgEdit" runat="server" CausesValidation="False"  TEXT='PILIH'  NavigateUrl='' CommandName="PILIH"></asp:LinkButton>
                                </ItemTemplate>
                        </asp:TemplateColumn>


                 <%--       <asp:TemplateColumn HeaderText="PILIH">
                            <ItemStyle HorizontalAlign="left" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkAgreement" runat="server" Checked="false"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                        <asp:TemplateColumn HeaderText="NO KONTRAK">
                            <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hynAgreementNo" runat="server" Text='<%#container.dataitem("AgreementNo")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                                <asp:Label ID="lblAgreementNo" Visible="False" runat="server" Text='<%#container.dataitem("AgreementNo")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CABANG">
                            <ItemStyle HorizontalAlign="center" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblBranch" runat="server" Text='<%#container.dataitem("BranchFullName")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA CUSTOMER">
                            <ItemStyle HorizontalAlign="left" Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%#container.dataitem("CustomerName")%>'
                                    NavigateUrl=''>
                                </asp:HyperLink>
                                <asp:Label ID="lblCustomerID" Visible="False" runat="server" Text='<%#container.dataitem("CustomerID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                         <asp:TemplateColumn HeaderText="OTR">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblOTR" runat="server" Text='<%#formatnumber(container.dataitem("TotalOTR"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DP">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDownPayment" runat="server" Text='<%#formatnumber(container.dataitem("DownPayment"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="POKOK HUTANG">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPokokHutang" runat="server" Text='<%#formatnumber(container.dataitem("PokokHutang"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ADVANCE">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblFirstInstallment" runat="server" Text='<%#formatnumber(container.dataitem("FirstInstallment"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                         <asp:TemplateColumn HeaderText="B.ADMIN">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblBiayaAdmin" runat="server" Text='<%#formatnumber(container.dataitem("AdminFee"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ASURANSI">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPaidAmountByCust" runat="server" Text='<%#formatnumber(container.dataitem("PaidAmountByCust"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PELUNASAN">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblpelunasan" runat="server" Text='<%#formatnumber(container.dataitem("NilPelunasan"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH PEMBAYARAN">
                            <ItemStyle HorizontalAlign="right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAPAmount" runat="server" CssClass="bold_text" Text='<%#formatnumber(container.dataitem("APAmount"),0)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                         <asp:TemplateColumn HeaderText="ASURANSI">
                            <ItemStyle HorizontalAlign="right"  Width="30%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAsuransi" runat="server" Text='<%# container.dataitem("Description")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="DeliveryOrderDate" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblDeliveryOrderDate" runat="server" Text='<%#container.dataitem("DeliveryOrderDate")%>'>
                                </asp:Label>

                                 <asp:Label ID="lblIsGabung" runat="server" Text='<%#container.dataitem("GabungRefundSupplier")%>' />
                                 <asp:Label ID="lblRefund" runat="server" Text='<%#container.dataitem("Refund")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ApplicationID" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationID" runat="server" Text='<%#container.dataitem("ApplicationID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ApplicationStep" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblApplicationStep" runat="server" Text='<%#container.dataitem("ApplicationStep")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="APType" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblAPType" runat="server"  Text='<%#container.dataitem("APType")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BranchID" Visible="False">
                            <ItemStyle HorizontalAlign="right" Width="20%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblCabangId" runat="server" Text='<%#container.dataitem("BranchID")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_button">
          <%--  <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Next" CssClass="small button blue" />--%>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
