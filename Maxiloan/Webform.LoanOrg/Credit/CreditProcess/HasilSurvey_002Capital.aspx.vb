﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class HasilSurvey_002Capital
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucHasilSurveyTab1 As ucHasilSurveyTab
    Protected WithEvents ucHasilSurveyTabPhone1 As ucHasilSurveyTabPhone
    Protected WithEvents txtLamaSurvey As ucNumberFormat
    Protected WithEvents txtJumlahKendaraan As ucNumberFormat
    Protected WithEvents txtOrderKe As ucNumberFormat

    Private oAssetDataController As New AssetDataController
    Private oController As New HasilSurveyController
    Private oCustomerController As New CustomerController
    Private m_controller As New AssetDataController
    Private m_Employee As New EmployeeController
    Private time As String

#Region "Property"
    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Private Property CustomerID() As String
        Get
            Return (CType(ViewState("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property

    Property ProspectAppID() As String
        Get
            Return ViewState("ProspectAppID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Private Property DateEntryCollateral() As Date
        Get
            Return CType(ViewState("DateEntryCollateral"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("DateEntryCollateral") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        'TODO Reject if login is HO
        If Not Page.IsPostBack Then
            Me.FormID = "HASILSURVEY"
            Me.CustomerID = Request("id")
            'Me.ProspectAppID = "-"

            'GetCookies()
            'InitialPageLoad()
            'fillFormTesting()
            'getHasilSurvey()
            'Modify by Wira 20171013
            'Ambil data hasil survey dari mobile jika add ke agreement survey
            Me.BranchID = Request("branchID")
            Me.PageAddEdit = Request("page")
            Me.ProspectAppID = Request("prospectappid")
            InitialPageLoad()
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------
            If Me.ProspectAppID <> Nothing And Me.ProspectAppID <> "-" And Request("prospectappid") <> "" And Me.PageAddEdit = "Add" Then
                getHasilMobileSurvey()
            Else
                getHasilSurvey()
            End If
        End If
    End Sub

    'Sub GetCookies()
    '    Dim cookie As HttpCookie = Request.Cookies("HASIL_SURVEY")

    '    Me.ApplicationID = cookie.Values("ApplicationID")
    '    Me.CustomerID = cookie.Values("CustomerID")
    '    Me.BranchID = cookie.Values("BranchID")
    '    Me.SupplierID = cookie.Values("SupplierID")
    'End Sub

    Private Sub InitialPageLoad()
        lblMessage.Text = ""
        lblMessage.Visible = False

        Me.ApplicationID = Request("appid")

        With ucViewApplication1
            .ApplicationID = Me.ApplicationID
            .initControls("OnNewApplication")
            .bindData()
            Me.CustomerID = .CustomerID
            Me.BranchID = .BranchID
            Me.SupplierID = .SupplierID

            If .IsAppTimeLimitReached Then
                ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                btnSave.Visible = False
            End If
        End With

        ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
        ucHasilSurveyTab1.selectedTab("Capital")
        ucHasilSurveyTab1.setLink()


        With ucViewCustomerDetail1
            .CustomerID = Me.CustomerID
            .bindCustomerDetail()
        End With

        FillCbo(cboStatusKepemilikan, "dbo.TblHomeStatus")
    End Sub

    Sub getHasilSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            cboStatusKepemilikan.SelectedIndex = cboStatusKepemilikan.Items.IndexOf(cboStatusKepemilikan.Items.FindByValue(oPar.StatusKepemilikan.ToString.Trim))
            txtAtasNama.Text = .AtasNama
            txtPBBAJBSHMAtasNama.Text = .PBBAJBSHMatasNama
            txtTahunTagihan.Text = .TahunTagihanPBBAJBSHM
            rboPBBAJBSHM.SelectedIndex = rboPBBAJBSHM.Items.IndexOf(rboPBBAJBSHM.Items.FindByValue(oPar.PBBAJBSHM))
            txtAlasanPBBAJBSHM.Text = .AlasanPBBAJBSHM
            txtRekListrikAtasNama.Text = .RekListrikAtasNama
            txtBulanTagihan.Text = .BulanTagihanRekListrik
            rboRekListrik.SelectedIndex = rboRekListrik.Items.IndexOf(rboRekListrik.Items.FindByValue(oPar.RekListrik))
            txtAlasanrekListrik.Text = .AlasanRekListrik
            txtLuasTanah.Text = .LuasTanah
            txtLuasBangunan.Text = .LuasBangunan
            txtTaksiranNilaiJual.Text = FormatNumber(CStr(.TaksiranNilaiJual), 0)
            txtLamaTinggal.Text = .LamaTinggal
            txtAssetLain.Text = .AssetLain
            rboFotoRumah.SelectedIndex = rboFotoRumah.Items.IndexOf(rboFotoRumah.Items.FindByValue(oPar.FotoRumah))
            txtAlasanFotoRumah.Text = .AlasanFotoRumah
            'txtKondisiBangunan.Text = .KondisiBangunan
            cboKondisiBangunan.SelectedIndex = cboKondisiBangunan.Items.IndexOf(cboKondisiBangunan.Items.FindByValue(oPar.KondisiBangunan))
            cboDayaListrik.SelectedIndex = cboDayaListrik.Items.IndexOf(cboDayaListrik.Items.FindByValue(oPar.DayaListrik))
            txtNoKTPPemohon.Text = .NoKTPPemohon
            'txtTglKTPPemohon.Text = .TanggalKTPPemohon.ToString("dd/MM/yyyy")
            txtTglKTPPemohon.Text = IIf(.TanggalKTPPemohon.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalKTPPemohon.ToString("dd/MM/yyyy"))
            rboKTPPemohon.SelectedIndex = rboKTPPemohon.Items.IndexOf(rboKTPPemohon.Items.FindByValue(oPar.KTPPemohon))
            txtAlasanKTPPemohon.Text = .AlasanKTPPemohon
            txtNoKTPPasangan.Text = .NoKTPPasangan
            'txtTglKTPPasangan.Text = .TanggalNoKTPPasangan.ToString("dd/MM/yyyy")
            txtTglKTPPasangan.Text = IIf(.TanggalNoKTPPasangan.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalNoKTPPasangan.ToString("dd/MM/yyyy"))
            rboKTPPasangan.SelectedIndex = rboKTPPasangan.Items.IndexOf(rboKTPPasangan.Items.FindByValue(oPar.KTPPasangan))
            txtAlasanKTPPasangan.Text = .AlasanKTPPasangan
            txtNoKK.Text = .NoKK
            'txtTglNoKK.Text = .TanggalNoKartuKeluarga.ToString("dd/MM/yyyy")
            txtTglNoKK.Text = IIf(.TanggalNoKartuKeluarga.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalNoKartuKeluarga.ToString("dd/MM/yyyy"))
            rboBukuNikah.SelectedIndex = rboBukuNikah.Items.IndexOf(rboBukuNikah.Items.FindByValue(oPar.BukuNikah))
            txtAlasanBukuNikah.Text = .AlasanBukuNikah
            txtJumlahTanggungan.Text = .JumlahTanggungan
            txtKesimpulan.Text = .AnalisaCapital
            txtDP.text = .DP
        End With
    End Sub

    Protected Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../CreditProcess/HasilSurvey_002Condition.aspx?appid=" + Me.ApplicationID + "&page=" + Request("page") + "&id=" + Request("id") + "&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
    End Sub

    Private Sub imbSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim oPar As New Parameter.HasilSurvey

        Try
            With oPar
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                .CustomerIDSurvey = Me.CustomerID
                .StatusKepemilikan = cboStatusKepemilikan.SelectedValue
                .AtasNama = txtAtasNama.Text
                .PBBAJBSHMatasNama = txtPBBAJBSHMAtasNama.Text
                .TahunTagihanPBBAJBSHM = txtTahunTagihan.Text
                .PBBAJBSHM = rboPBBAJBSHM.SelectedValue
                .AlasanPBBAJBSHM = txtAlasanPBBAJBSHM.Text
                .RekListrikAtasNama = txtRekListrikAtasNama.Text
                .BulanTagihanRekListrik = txtBulanTagihan.Text
                .RekListrik = rboRekListrik.SelectedValue
                .AlasanRekListrik = txtAlasanrekListrik.Text
                .LuasTanah = txtLuasTanah.Text
                .LuasBangunan = txtLuasBangunan.Text
                .TaksiranNilaiJual = txtTaksiranNilaiJual.Text
                .LamaTinggal = txtLamaTinggal.Text
                .AssetLain = txtAssetLain.Text
                .FotoRumah = rboFotoRumah.SelectedValue
                .AlasanFotoRumah = txtAlasanFotoRumah.Text
                '.KondisiBangunan = txtKondisiBangunan.Text
                .KondisiBangunan = cboKondisiBangunan.SelectedValue
                .DayaListrik = cboDayaListrik.SelectedValue
                .NoKTPPemohon = txtNoKTPPemohon.Text
                .TanggalKTPPemohon = Date.ParseExact(txtTglKTPPemohon.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .KTPPemohon = rboKTPPemohon.SelectedValue
                .AlasanKTPPemohon = txtAlasanKTPPemohon.Text
                .NoKTPPasangan = txtNoKTPPasangan.Text
                .TanggalNoKTPPasangan = Date.ParseExact(txtTglKTPPasangan.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .KTPPasangan = rboKTPPasangan.SelectedValue
                .AlasanKTPPasangan = txtAlasanKTPPasangan.Text
                .NoKK = txtNoKK.Text
                .TanggalNoKartuKeluarga = Date.ParseExact(txtTglNoKK.Text.ToString, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                .BukuNikah = rboBukuNikah.SelectedValue
                .AlasanBukuNikah = txtAlasanBukuNikah.Text
                .JumlahTanggungan = txtJumlahTanggungan.Text
                .AnalisaCapital = txtKesimpulan.Text
                .DP = CDbl(txtDP.Text)
            End With

            oController.HasilSurvey002CapitalSave(oPar)

            Dim errMsg As String

            ucHasilSurveyTab1.ApplicationID = Me.ApplicationID
            ucHasilSurveyTab1.selectedTab("Capital")
            ucHasilSurveyTab1.setLink()
            InitialPageLoad()
            getHasilSurvey()

            If errMsg = String.Empty Then
                ShowMessage(lblMessage, "Data saved!", False)
            End If

            CekTabAplikasiSurvey()
            If Me.ProspectAppID <> "-" And Me.DateEntryCollateral = "1/1/1900" Then
                Response.Redirect("HasilSurvey_002Collateral.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Add&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim & "&branchID=" & Me.BranchID.Trim & "&ActivityDateStart=" & Me.ActivityDateStart)
            Else
                Response.Redirect("HasilSurvey_002Collateral.aspx?appid=" & Me.ApplicationID.ToString.Trim & "&page=Edit&id=" & Me.CustomerID.ToString.Trim & "&prospectappid=" & Me.ProspectAppID.ToString.Trim)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

    Sub getHasilMobileSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilMobileSurvey(oPar)

        With oPar
            Me.ProspectAppID = oPar.ProspectAppID
            cboStatusKepemilikan.SelectedIndex = cboStatusKepemilikan.Items.IndexOf(cboStatusKepemilikan.Items.FindByValue(oPar.StatusKepemilikan.ToString.Trim))
            txtAtasNama.Text = .AtasNama
            txtPBBAJBSHMAtasNama.Text = .PBBAJBSHMatasNama
            txtTahunTagihan.Text = .TahunTagihanPBBAJBSHM
            rboPBBAJBSHM.SelectedIndex = rboPBBAJBSHM.Items.IndexOf(rboPBBAJBSHM.Items.FindByValue(oPar.PBBAJBSHM))
            txtAlasanPBBAJBSHM.Text = .AlasanPBBAJBSHM
            txtRekListrikAtasNama.Text = .RekListrikAtasNama
            txtBulanTagihan.Text = .BulanTagihanRekListrik
            rboRekListrik.SelectedIndex = rboRekListrik.Items.IndexOf(rboRekListrik.Items.FindByValue(oPar.RekListrik))
            txtAlasanrekListrik.Text = .AlasanRekListrik
            txtLuasTanah.Text = .LuasTanah
            txtLuasBangunan.Text = .LuasBangunan
            txtTaksiranNilaiJual.Text = FormatNumber(CStr(.TaksiranNilaiJual), 0)
            txtLamaTinggal.Text = .LamaTinggal
            txtAssetLain.Text = .AssetLain
            rboFotoRumah.SelectedIndex = rboFotoRumah.Items.IndexOf(rboFotoRumah.Items.FindByValue(oPar.FotoRumah))
            txtAlasanFotoRumah.Text = .AlasanFotoRumah
            'txtKondisiBangunan.Text = .KondisiBangunan
            cboKondisiBangunan.SelectedIndex = cboKondisiBangunan.Items.IndexOf(cboKondisiBangunan.Items.FindByValue(oPar.KondisiBangunan))
            cboDayaListrik.SelectedIndex = cboDayaListrik.Items.IndexOf(cboDayaListrik.Items.FindByValue(oPar.DayaListrik))
            txtNoKTPPemohon.Text = .NoKTPPemohon
            'txtTglKTPPemohon.Text = .TanggalKTPPemohon.ToString("dd/MM/yyyy")
            txtTglKTPPemohon.Text = IIf(.TanggalKTPPemohon.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalKTPPemohon.ToString("dd/MM/yyyy"))
            rboKTPPemohon.SelectedIndex = rboKTPPemohon.Items.IndexOf(rboKTPPemohon.Items.FindByValue(oPar.KTPPemohon))
            txtAlasanKTPPemohon.Text = .AlasanKTPPemohon
            txtNoKTPPasangan.Text = .NoKTPPasangan
            'txtTglKTPPasangan.Text = .TanggalNoKTPPasangan.ToString("dd/MM/yyyy")
            txtTglKTPPasangan.Text = IIf(.TanggalNoKTPPasangan.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalNoKTPPasangan.ToString("dd/MM/yyyy"))
            rboKTPPasangan.SelectedIndex = rboKTPPasangan.Items.IndexOf(rboKTPPasangan.Items.FindByValue(oPar.KTPPasangan))
            txtAlasanKTPPasangan.Text = .AlasanKTPPasangan
            txtNoKK.Text = .NoKK
            'txtTglNoKK.Text = .TanggalNoKartuKeluarga.ToString("dd/MM/yyyy")
            txtTglNoKK.Text = IIf(.TanggalNoKartuKeluarga.ToString("dd/MM/yyyy") = "01/01/1900", Me.BusinessDate.ToString("dd/MM/yyyy"), .TanggalNoKartuKeluarga.ToString("dd/MM/yyyy"))
            rboBukuNikah.SelectedIndex = rboBukuNikah.Items.IndexOf(rboBukuNikah.Items.FindByValue(oPar.BukuNikah))
            txtAlasanBukuNikah.Text = .AlasanBukuNikah
            txtJumlahTanggungan.Text = .JumlahTanggungan
            txtKesimpulan.Text = .AnalisaCapital
            txtDP.Text = .DP
        End With
    End Sub

    Sub CekTabAplikasiSurvey()
        Dim oPar As New Parameter.HasilSurvey

        With oPar
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
        End With

        oPar = oController.getHasilSurvey(oPar)

        With oPar
            Me.DateEntryCollateral = .DateEntryCollateral
        End With
    End Sub
End Class