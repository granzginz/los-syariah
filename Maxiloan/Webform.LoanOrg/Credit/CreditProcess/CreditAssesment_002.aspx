﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreditAssesment_002.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CreditAssesment_002" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication" TagPrefix="uc3" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail" TagPrefix="uc2" %>
<%@ Register TagPrefix="uc1" TagName="UcCustomerLookUpGlobal" Src="../../../Webform.UserController/ucLookUpCustomerGlobal.ascx" %>
<%@ Register Src="../../../webform.UserController/CreditAssessmentTab.ascx" TagName="CreditAssessmentTab" TagPrefix="uc7" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
       </script>
</head>
<body>
  <%--  <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <form id="form1" runat="server">
   <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <div runat="server" id="jlookupContent" />
  <%--  <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>--%>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
             <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />
             <uc7:CreditAssessmentTab id="CreditAssessmentTab1" runat="server" />
              
              <div class="form_box "> 
               </div>
             <div class="form_box">
                    <div class="form_left_uc"> 
                        <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                    </div>
                    <div class="form_right_uc">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>  

            <div class="form_title">
                <div class="form_single">
                    <h3> HASIL VERIFIKASI VIA TELEPON</h3>
                </div>
            </div>

             <div class="form_box">
                <div class="form_left">
                    <label> Saldo Awal </label>
                    <uc1:ucnumberformat id="TxtSaldoAwal" runat="server" />  
                </div>
                <div class="form_right">
                    
                     <label> Jumlah Pemasukan </label>
                      <uc1:ucnumberformat id="TxtJumlahPemasukan" runat="server" />  
                  
                </div> 

               </div> 
             <div class="form_box">
                <div class="form_left">
                   <label> Saldo Akhir </label>
                    <uc1:ucnumberformat id="TxtSaldoAkhir" runat="server" />    
                  </div> 
                  <div class="form_right">
                      <label> Jumlah Pengeluaran </label>
                    <uc1:ucnumberformat id="TxtJumlahPengeluran" runat="server" />  
                    </div>
                 </div>
             <div class="form_box">
                <div class="form_left">
                    <label> Saldo Rata-rata </label> 
                    <uc1:ucnumberformat id="TxtSaldoRataRata" runat="server" />    
                  </div> 
                  <div class="form_right"> 
                    </div>
                 </div> 
               
              
                <div class="form_box">
                <div class="form_single">
                    <label> Jumlah Hari Transaksi </label>
                    <asp:TextBox ID="TxtJumlahhariTransaksi" runat="server" CssClass="small_text"></asp:TextBox>
                     <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="TxtJumlahhariTransaksi" Enabled="false" ErrorMessage="Harap isi tanggal Telepon" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
              </div>
                <div class="form_box">
                   <div class="form_single">
                    <%--<label> Jenis Rekening </label>--%>
                    <label class="label_req"> Jenis Rekening </label>
                    <asp:DropDownList ID="cmbAccountType" runat="server"> </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="cmbAccountType" ErrorMessage="Harap dipilih Jenis Rekening" Display="Dynamic" InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
           </div>
                <div class="form_box">
                  <div class="form_single">
                    <%--<label class="label_split" runat="server" id="Label1"> Kesimpulan </label>--%>
                      <label class="label_split_req" runat="server" id="Label1"> Kesimpulan </label>
                    <asp:TextBox ID="TxtKesimpulan" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ErrorMessage="Harap Kesimpulan" ControlToValidate="TxtKesimpulan" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
             <div class="form_box"> 
                <div class="form_left">
                        <%--<label> Akan diSetujui Oleh </label>--%>
                      <label class="label_req"> Akan diSetujui Oleh </label>
                        <asp:DropDownList ID="cboApprovedBy" runat="server"> </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Akan disetujui Oleh" Display="Dynamic" InitialValue="0" ControlToValidate="cboApprovedBy" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
<%--                <div class="form_right">
                    <label> No. Kontrak </label>
                    <asp:TextBox ID="txtAgreementNo" runat="server" MaxLength="20">-</asp:TextBox>
                    <asp:Label ID="lblMsgAgreementNo" runat="server" Visible="False"></asp:Label>
                </div> --%>
            </div>
            <div class="form_button">
             <asp:Button runat="server" ID="btnProceed" Text="Proceed" CssClass="small button blue" />
                <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button small green" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray"></asp:Button>
            </div>

    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
