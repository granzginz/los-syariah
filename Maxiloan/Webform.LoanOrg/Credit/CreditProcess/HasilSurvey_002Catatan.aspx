﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HasilSurvey_002Catatan.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.HasilSurvey_002Catatan" %>

<%@ Register Src="../../../webform.UserController/UcLookUpProductOffering.ascx"
    TagName="UcLookUpProductOffering" TagPrefix="uc1" %>

   <%-- <%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>--%>
<%@ Register Src="../../../webform.UserController/ucLookUpCustomer.ascx" TagName="ucLookUpCustomer"
    TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../webform.UserController/ucGuarantor.ascx" TagName="ucGuarantor"
    TagPrefix="uc4" %>
<%@ Register Src="../../../webform.UserController/ucReferensi.ascx" TagName="ucReferensi"
    TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucLookupKabupaten.ascx" TagName="ucLookupKabupaten"
    TagPrefix="uc6" %>
<%@ Register Src="../../../webform.UserController/ucHasilSurveyTab.ascx" TagName="ucHasilSurveyTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc8" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<%@ Register Src="../../../webform.UserController/ucRefinancing.ascx" TagName="ucRefinancing"
    TagPrefix="uc9" %>
<%@ Register Src="../../../webform.UserController/ucHasilSurveyTabPhone.ascx" TagName="ucHasilSurveyTabPhone"
    TagPrefix="uc10" %>
<%@ Register Src="../../../webform.UserController/ucCoverageTypeApk.ascx" TagName="ucCoverageTypeApk"
    TagPrefix="uc11" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc12" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../../Webform.UserController/ucAddress.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucProductOffering" Src="../../../../Webform.UserController/ucProductOffering.ascx" %>--%>

<%@ Register TagPrefix="uc1" TagName="ucProdOffering" Src="../../../Webform.UserController/UcLookUpPdctOffering.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucKabupaten" Src="../../../Webform.UserController/ucKabupaten.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../Include/Buttons.css" />
    
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript">
        function calculateAdminFeeGross() {
            var admin = $('#ucAdminFee_txtNumber').val();
            var fiducia = $('#ucFiduciaFee_txtNumber').val();
            var other = $('#ucOtherFee_txtNumber').val();
            var biayaPolis = $('#ucBiayaPolis_txtNumber').val()

            if (!admin) { admin = "0" };
            if (!fiducia) { admin = "0" };
            if (!other) { admin = "0" };
            if (!biayaPolis) { admin = "0" };

            var total = parseInt(admin.replace(/\s*,\s*/g, '')) +
                        parseInt(fiducia.replace(/\s*,\s*/g, '')) +
                        parseInt(other.replace(/\s*,\s*/g, '')) +
                        parseInt(biayaPolis.replace(/\s*,\s*/g, ''));
            $('#lblAdminFeeGross').html(number_format(total));

        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function SelectFromArray(itemArray) {
            var objAdd = 'document.forms[0].UCMailingAddress_';
            var pAddress = eval(objAdd + 'txtAddress');
            var pRT = eval(objAdd + 'txtRT');
            var pRW = eval(objAdd + 'txtRW');
            var pKelurahan = eval(objAdd + 'oLookUpKodePos_txtKelurahan'); 
            var pKecamatan = eval(objAdd + 'oLookUpKodePos_txtKecamatan');
            var pCity = eval(objAdd + 'oLookUpKodePos_txtCity');
            var pZipCode = eval(objAdd + 'oLookUpKodePos_txtZipCode'); 
            var pAreaPhone1 = eval(objAdd + 'txtAreaPhone1');
            var pPhone1 = eval(objAdd + 'txtPhone1');
            var pAreaPhone2 = eval(objAdd + 'txtAreaPhone2');
            var pPhone2 = eval(objAdd + 'txtPhone2');
            var pAreaFax = eval(objAdd + 'txtAreaFax');
            var pFax = eval(objAdd + 'txtFax');

            pAddress.value = itemArray[0][0];
            pRT.value = itemArray[0][1];
            pRW.value = itemArray[0][2];
            pKelurahan.value = itemArray[0][3];
            pKecamatan.value = itemArray[0][4];
            pCity.value = itemArray[0][5];
            pZipCode.value = itemArray[0][6];
            pAreaPhone1.value = itemArray[0][7];
            pPhone1.value = itemArray[0][8];
            pAreaPhone2.value = itemArray[0][9];
            pPhone2.value = itemArray[0][10];
            pAreaFax.value = itemArray[0][11];
            pFax.value = itemArray[0][12];
            return false;
        }

        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date);
            if (chk == true) {
                text.disabled = true;
                text.value = '';
            }
            else {
                text.disabled = false;
            }

            return true;

        }

        function InterestType() {
            var myID = 'cboInstScheme';
            var obj = eval('document.forms[0].' + myID);
            if (document.forms[0].cboInterestType.value == 'FX') {
                obj.disabled = false;
            }
            else {
                obj.disabled = true;
            }
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }

        function InstScheme() {
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;

            if (document.forms[0].cboInstScheme.value == 'ST') {
                document.forms[0].rdoSTTYpe(0).disabled = false;
                document.forms[0].rdoSTTYpe(1).disabled = false;
                document.forms[0].rdoSTTYpe(2).disabled = false;
            }
        }

        function rdoDisabled() {
            document.forms[0].rdoSTTYpe(0).checked = true;
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }
        function rboRefinancing_onchange() {
            var value = $('#rboRefinancing').find(':checked')[0].value;

            if (value === 'False') 
            {
                $('#divRefinancing').css("display", "inherit");
                return;
            }
            else 
            {
                $('#divRefinancing').css("display", "none");
                $('#ucRefinancing1_txtAgreementNo').val('');
                $('#ucRefinancing1_txtNilaiPelunasan').val('');
            }
        }
        function rboCaraSurvey_onchange() {

            var _CaraSurvey = $('#cboCaraSurvey').val();
            if (_CaraSurvey === 'False') {
                $('#divCaraSurvey').css("display", "inherit");
                return;
            }
            else {
                $('#divCaraSurvey').css("display", "none");
                $('#ucHasilSurveyTabPhone1_txtTlpRumah').val('');
                $('#ucHasilSurveyTabPhone1_txtTlpKantor').val('');
                $('#ucHasilSurveyTabPhone1_txtHandphone').val('');
                $('#ucHasilSurveyTabPhone1_txtEmergencyContact').val('');
            }
        }

        function UploadDokument(ucASPXPath, ucTitle, divLookupContent, paramData) {
            paramData = paramData || '';
            $('#dialog').remove();
            var match = new RegExp(App, 'gi');
            var tmpStrPath = AppInfo.replace(match, '');
            tmpStrPath = tmpStrPath.substring(2, tmpStrPath.length - 1);
            var arrStrPath = tmpStrPath.split('/');
            tmpStrPath = '';
            $.each(arrStrPath, function (index, value) {
                if (arrStrPath.length - 1 != index) {
                    tmpStrPath += '../'
                }
            });



            $('#' + divLookupContent).prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><img src="' + tmpStrPath + 'Images/exit_lookup00.png" class="ui-custom-close" /><iframe src="' + ucASPXPath + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
            $('#dialog')
	        .data('fnName', paramData)
	        .dialog({
	            title: ucTitle,
	            bgiframe: false,
	            width: 900,
	            height: 500,
	            resizable: false,
	            modal: true,
	            closeOnEscape: true,
	            draggable: true,
	            create: function (event, ui) {
	                $(".ui-widget-header").hide();
	            },
	            open: function () {
	                $('.ui-custom-close').bind('click', function () {
	                    $('#dialog').dialog('close');
	                })
	            }
	        });
            return false;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinZipCode(pZipCode, pKelurahan, pKecamatan, pCity, pStyle) {
            window.open(ServerName + App + '/General/LookUpZipCode.aspx?Zipcode=' + pZipCode + '&Kelurahan=' + pKelurahan + '&Kecamatan=' + pKecamatan + '&City=' + pCity + '&Style=' + pStyle, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=1, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinProductOfferingLookup(pProductOfferingID, pProductOfferingDescription, pProductID, pAssetTypeID, pStyle, pBranchID) {
            window.open(ServerName + App + '/General/LookUpProductOffering.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID + '&AssetTypeID=' + pAssetTypeID + '&branchid=' + pBranchID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinKabupaten(pProductOfferingID, pProductOfferingDescription, pProductID, pStyle) {
            window.open(ServerName + App + '/General/LookupKabupaten.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function showimagepreview(input, imgClient) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#' + imgClient).attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }

        function showOnloadAllImage() {
            showimagepreview(document.getElementById('upldenahlokasirumah1'), 'imgdenahlokasirumah1')
            showimagepreview(document.getElementById('upldenahlokasirumah2'), 'imgdenahlokasirumah2')
            showimagepreview(document.getElementById('upldenahlokasirumah3'), 'imgdenahlokasirumah3')
            showimagepreview(document.getElementById('upldenahlokasirumah4'), 'imgdenahlokasirumah4')
            showimagepreview(document.getElementById('upldenahlokasirumah5'), 'imgdenahlokasirumah5')
        }
    </script>
</head>
<body onload="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');showOnloadAllImage();"
    onresize="gridGeneralSize('dtgTC');getGridGeneralSize('hdnGridGeneralSize');">
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc7:ucHasilSurveyTab id="ucHasilSurveyTab1" runat="server" />
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        CATATAN dan KOMENTAR</h3>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Kunjungan Rumah Bertemu</label>  
                        <asp:TextBox ID="txtKunjunganRumahbertemu" width="255px" runat="server" MaxLength ="50"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>
                            Kondisi Rumah</label>
                        <asp:DropDownList ID="cboKondisiRumah" runat="server">
                            <asp:ListItem Value="A" Selected="True">SANGAT TERAWAT</asp:ListItem>
                            <asp:ListItem Value="B">TERAWAT</asp:ListItem>
                            <asp:ListItem Value="C">TIDAK TERAWAT</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <%--<div class="form_box">
                <div>
                    <div class="form_left" visible="false">
                        <label>Kondisi Kesehatan Pemohon</label>  
                        <asp:DropDownList ID="cboKondisiKesehatan" runat="server" visible="false">
                            <asp:ListItem Value="SANGAT SEHAT" Selected="True">SANGAT SEHAT</asp:ListItem>
                            <asp:ListItem Value="SEHAT">SEHAT</asp:ListItem>
                            <asp:ListItem Value="CUKUP SEHAT">CUKUP SEHAT</asp:ListItem>
                            <asp:ListItem Value="KURANG SEHAT">KURANG SEHAT</asp:ListItem>
                            <asp:ListItem Value="TIDAK SEHAT">TIDAK SEHAT</asp:ListItem>
                        </asp:DropDownList> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="cboKondisiKesehatan"></asp:RequiredFieldValidator>                  
                    </div> 
                </div>
            </div>--%>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Perabotan Rumah</label>  
                        <asp:DropDownList ID="cboPerabotanRumah" runat="server">
                            <asp:ListItem Value="M" Selected="True">MEWAH</asp:ListItem>
                            <asp:ListItem Value="S">STANDARD</asp:ListItem>
                            <asp:ListItem Value="L">LAINNYA</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Garasi Mobil</label>
                        <asp:RadioButtonList ID="rboGarasiMobil" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak Ada</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Garasi untuk berapa unit</label>  
                        <asp:TextBox ID="txtGarasiUnit" width="55px" runat="server" MaxLength ="2" onkeypress="return numbersonly2(event)"></asp:TextBox>                                      
                        <label>unit</label>
                    </div>
                    <div class="form_right">
                        <label>
                            Alasan</label>
                        <asp:TextBox ID="txtAlasanGarasiUnit" width="255px" runat="server" MaxLength ="50"></asp:TextBox>                                       
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Lokasi Rumah</label>  
                        <asp:DropDownList ID="cboLokasiRumah" runat="server">
                            <asp:ListItem Value="U" Selected="True">JALAN UTAMA</asp:ListItem>
                            <asp:ListItem Value="B">JALAN BIASA</asp:ListItem>
                            <asp:ListItem Value="G">GANG</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <%--<div class="form_right">
                        <label>
                            PARAMETER BARU</label>
                        <asp:DropDownList ID="DropDownList1" runat="server"> 
                            <asp:ListItem Value="U" Selected="True">PARAMETER BARU 1</asp:ListItem>
                            <asp:ListItem Value="B">PARAMETER BARU 2</asp:ListItem>
                            <asp:ListItem Value="G">PARAMETER BARU 3</asp:ListItem>
                        </asp:DropDownList>                                       
                    </div>--%>
                    <div class="form_left">
                        <label>Jalan masuk dilalui</label>  
                        <asp:DropDownList ID="cboJalanMasukDilalui" runat="server">
                            <asp:ListItem Value="2" Selected="True">2 MOBIL</asp:ListItem>
                            <asp:ListItem Value="1">1 MOBIL</asp:ListItem>
                            <asp:ListItem Value="0">TDK MASUK MOBIL</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Kondisi Jalan Depan Rumah</label>  
                        <asp:DropDownList ID="cboKondisiJlnDpnRumah" runat="server">
                            <asp:ListItem Value="B" Selected="True">Beton</asp:ListItem>
                            <asp:ListItem Value="A">Aspal</asp:ListItem>
                            <asp:ListItem Value="C">Conblock</asp:ListItem>
                            <asp:ListItem Value="L">Lainnya</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_left">
                        <label>Keadaan Lingkungan</label>  
                        <asp:DropDownList ID="cboKeadaanLingkungan" runat="server">
                            <asp:ListItem Value="A" Selected="True">Cluster</asp:ListItem>
                            <asp:ListItem Value="B">Perumahan</asp:ListItem>
                            <asp:ListItem Value="C">Perkampungan</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Kunjungan Kantor Bertemu</label>  
                        <asp:TextBox ID="txtKunjunganKantorBertemu" width="255px" runat="server" MaxLength ="50"></asp:TextBox>                   
                    </div>
                    <div class="form_left">
                        <label>Jumlah Karyawan</label>  
                        <asp:DropDownList ID="cboJumlahKaryawan" runat="server">
                            <asp:ListItem Value="A" Selected="True">< 20</asp:ListItem>
                            <asp:ListItem Value="B">20 < 50</asp:ListItem>
                            <asp:ListItem Value="C">50 < 100</asp:ListItem>
                            <asp:ListItem Value="D">> 100</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Lokasi tempat Usaha / Kantor</label>  
                        <asp:DropDownList ID="cboLokasiTempatUsaha" runat="server">
                            <asp:ListItem Value="U" Selected="True">JALAN UTAMA</asp:ListItem>
                            <asp:ListItem Value="B">JALAN BIASA</asp:ListItem>
                            <asp:ListItem Value="G">GANG</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_left">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanLokasiTempatUsaha" width="255px"  runat="server" CssClass="small_text" MaxLength="50"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Aktivitas Tempat Usaha</label>  
                        <asp:RadioButtonList ID="rboAktivitasTmptUsaha" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_left">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanAktivitasTempatUsaha" width="255px" runat="server" MaxLength="50"></asp:TextBox>                                       
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">TBO List</label>  
                        <asp:TextBox runat="server" ID="txtTBOList" TextMode="MultiLine" width="350px"></asp:TextBox>
                    </div>
                    <div class="form_left">
                        <label class="label_general">Usulan Lain</label>  
                        <asp:TextBox runat="server" ID="txtUsulanLain" TextMode="MultiLine" width="350px"></asp:TextBox>
                    </div>
                </div>
            </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>GAMBAR LOKASI</h4>
            </div>
        </div>              
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Denah Lokasi Rumah</label>  
                        <asp:FileUpload ID="upldenahlokasirumah1" runat="server" onchange="showimagepreview(this,'imgdenahlokasirumah1')"  />
                    </div>
                    <div class="form_right">
                        <label>Denah Lokasi Rumah</label>  
                        <asp:FileUpload ID="upldenahlokasirumah2" runat="server" onchange="showimagepreview(this,'imgdenahlokasirumah2')"  />
                    </div>
                </div>
            </div>
         <div class="form_box">
             <div class="form_left">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah1" runat="server" />
                   </div>
             </div>
             <div class="form_right">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah2" runat="server" />
                   </div>
             </div>
         </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Denah Lokasi Rumah</label>  
                        <asp:FileUpload ID="upldenahlokasirumah3" runat="server" onchange="showimagepreview(this,'imgdenahlokasirumah3')"  />
                    </div>
                    <div class="form_right">
                        <label>Denah Lokasi Rumah</label>  
                        <asp:FileUpload ID="upldenahlokasirumah4" runat="server" onchange="showimagepreview(this,'imgdenahlokasirumah4')"  />
                    </div>
                </div>
            </div>
         <div class="form_box">
             <div class="form_left">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah3" runat="server" />
                   </div>
             </div>
             <div class="form_right">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah4" runat="server" />
                   </div>
             </div>
         </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Denah Lokasi Rumah</label>  
                        <asp:FileUpload ID="upldenahlokasirumah5" runat="server" onchange="showimagepreview(this,'imgdenahlokasirumah5')"  />
                    </div>
                    <div class="form_right">
                    </div>
                </div>
            </div>
         <div class="form_box">
             <div class="form_left">
                   <div>
                       <asp:Image ID="imgdenahlokasirumah5" runat="server" />
                   </div>
             </div>
             <div class="form_right">
             </div>
         </div>
<%--         <div class="form_box">
             <div class="form_left">
                   <div>
                       <asp:Button ID="btnupload" runat="server" Text="Upload" CssClass="small button blue"  />
                   </div>
             </div>
         </div>
--%><%--                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
--%>           
        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="btnupload" EventName="Click" />--%>
            <%--<asp:AsyncPostBackTrigger ControlID="cboKegiatanUsaha" EventName="SelectedIndexChanged" />--%>
        </Triggers>
    </asp:UpdatePanel>
        <div class="form_button">
        <asp:Button runat="server" ID="btnupload" Text="Upload" CssClass="small button blue" />
        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="small button blue" />
        <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="small button gray" />
    </div>
    </form>
</body>
</html>
