﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region


Public Class GoLive_002
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents uscAgreementDate As ValidDate
    Protected WithEvents uscEffectiveDate As ValidDate
#Region "Constanta"
    Private m_controller As New ApplicationController
    Dim Status As Boolean
    Dim oRow As DataRow
    Dim intLoop As Integer
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Property SurveyDate() As String
        Get
            Return viewstate("SurveyDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("SurveyDate") = Value
        End Set
    End Property
    Property FirstInstallment() As String
        Get
            Return viewstate("FirstInstallment").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("FirstInstallment") = Value
        End Set
    End Property
    Property DeliveryOrderDate() As String
        Get
            Return viewstate("DeliveryOrderDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("DeliveryOrderDate") = Value
        End Set
    End Property
    Property AgreementDate() As String
        Get
            Return viewstate("AgreementDate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementDate") = Value
        End Set
    End Property
    Property AADate() As String
        Get
            Return viewstate("AADate").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AADate") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property FundingPledgeStatus() As String
        Get
            Return viewstate("FundingPledgeStatus").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("FundingPledgeStatus") = Value
        End Set
    End Property
#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        pnlList.Visible = True
        pnlConfirm.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Try
                imbSave.Attributes.Add("OnClick", "return checksubmit();")
                drdContract.Attributes.Add("onChange", "javascript:ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);")
                Me.AgreementNo = Request("ANo")
                Me.CustomerName = Request("Name")
                Me.ApplicationID = Request("App")
                Me.AgreementDate = Request("AgDate")
                Me.CustomerID = Request("CustomerID")
                Me.SurveyDate = Request("SurveyDate")
                Me.FirstInstallment = Request("FirstInstallment")
                Me.AADate = Request("AADate")
                Me.DeliveryOrderDate = Request("DeliveryOrderDate")
                Context.Trace.Write("ApplicationID = " + Me.ApplicationID + " -- SurveyDate = " + Me.SurveyDate + " -- DODate = " + Me.DeliveryOrderDate)
                bindComboCompany()
                Response.Write(GenerateScript(getComboContract, getComboBatchDate))
                Bind()
                uscAgreementDate.dateValue = Me.SurveyDate
                uscAgreementDate.Display = "Dynamic"
                uscAgreementDate.isCalendarPostBack = True
                uscAgreementDate.isCalendarPostBack = False
                uscAgreementDate.FillRequired = False
                uscAgreementDate.FieldRequiredMessage = "Harap isi Tanggal Kontrak"
                uscAgreementDate.ValidationErrMessage = "Harap isi Tanggal Kontrak dgn format dd/MM/yyyy"

                uscEffectiveDate.dateValue = Me.AADate
                uscEffectiveDate.Display = "Dynamic"
                uscEffectiveDate.isCalendarPostBack = True
                uscEffectiveDate.isCalendarPostBack = False
                uscEffectiveDate.FillRequired = False
                uscEffectiveDate.FieldRequiredMessage = "Harap isi dengan Tanggal Effective"
                uscEffectiveDate.ValidationErrMessage = "Harap isi dengan Tanggal Effectiv dengan dd/MM/yyyy"



            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                err.WriteLog("GoLive_002.aspx.vb", "Page Load", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                Throw New Exception(ex.Message)
            End Try

        End If


        lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('AccAcq','" & Me.ApplicationID & "')"
        lblCustName.NavigateUrl = "javascript:OpenCustomer('AccAcq','" & Me.CustomerID & "')"

        LabelValidationFalse()
    End Sub
#End Region
#Region "Bind"
    Sub Bind()
        lblCustName.Text = Me.CustomerName
        lblAgreementNo.Text = Me.AgreementNo
        uscAgreementDate.dateValue = Me.AgreementDate
        TC()
        TC2()
    End Sub
#End Region
#Region "BindTC"
    Sub TC()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        oApplication.strConnection = GetConnectionString
        oApplication.AppID = Me.ApplicationID
        oApplication.BranchId = Me.sesBranchId
        oApplication.AddEdit = "GoLive"
        oApplication = m_controller.GetTC(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        dtgTC.DataSource = oData.DefaultView
        dtgTC.DataKeyField = "TCName"
        dtgTC.DataBind()

        Dim intLoop As Integer
        For intLoop = 0 To dtgTC.Items.Count - 1
            If oData.Rows(intLoop).Item(5).ToString.Trim <> "" Then
                CType(dtgTC.Items(intLoop).FindControl("uscTCPromiseDate"), ValidDate).dateValue = Format(oData.Rows(intLoop).Item(5), "dd/MM/yyyy")
            End If
        Next
        Me.FundingPledgeStatus = oApplication.FundingPledgeStatus
        If Me.FundingPledgeStatus.Trim <> "N" Then
            lblCompany.Text = CStr(oApplication.FundingCoyId.Trim)
            lblCompany.Visible = True
            LblContract.Text = CStr(oApplication.FundingContractNo.Trim)
            LblContract.Visible = True
            LblBatchdate.Text = CStr(oApplication.FundingBatchNo.Trim)
            LblBatchdate.Visible = True
            drdCompany.Visible = False
            drdContract.Visible = False
            drdBatchDate.Visible = False
        Else
            drdCompany.Visible = True
            drdContract.Visible = True
            drdBatchDate.Visible = True
        End If
    End Sub
    Sub TC2()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        oApplication.strConnection = GetConnectionString
        oApplication.BranchId = Me.sesBranchId
        oApplication.AppID = Me.ApplicationID
        oApplication.AddEdit = "GoLive"
        oApplication = m_controller.GetTC2(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        dtgTC2.DataSource = oData.DefaultView
        dtgTC2.DataKeyField = "TCName"
        dtgTC2.DataBind()
        Dim intLoop As Integer
        For intLoop = 0 To dtgTC2.Items.Count - 1
            If oData.Rows(intLoop).Item(6).ToString.Trim <> "" Then
                'CType(dtgTC2.Items(intLoop).FindControl("uscTCPromiseDate2"), ValidDate).dateValue = Format(oData.Rows(intLoop).Item(6), "dd/MM/yyyy")
            End If
        Next
        Me.FundingPledgeStatus = oApplication.FundingPledgeStatus
        If Me.FundingPledgeStatus.Trim <> "N" Then
            lblCompany.Text = CStr(oApplication.FundingCoyId.Trim)
            lblCompany.Visible = True
            LblContract.Text = CStr(oApplication.FundingContractNo.Trim)
            LblContract.Visible = True
            LblBatchdate.Text = CStr(oApplication.FundingBatchNo.Trim)
            LblBatchdate.Visible = True
            drdCompany.Visible = False
            drdContract.Visible = False
            drdBatchDate.Visible = False
        Else
            drdCompany.Visible = True
            drdContract.Visible = True
            drdBatchDate.Visible = True
        End If
    End Sub
#End Region
#Region "LabelValidationFalse"
    Sub LabelValidationFalse()
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim countDtg As Integer = CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
        Dim validCheck As New Label
        Dim Checked As Boolean
        Dim PriorTo As String
        Dim PromiseDate As ValidDate
        Dim Mandatory As String
        For intLoop = 0 To countDtg
            If intLoop <= DtgTC1count Then
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("uscTCPromiseDate"), ValidDate)
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = False
                If PriorTo = "NAP" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
            If intLoop <= DtgTC2count Then
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                'PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("uscTCPromiseDate2"), ValidDate)
                'validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                'CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = False
                If PriorTo = "NAP" And Mandatory = "v" Then
                    validCheck.Visible = True
                Else
                    validCheck.Visible = False
                End If
            End If
        Next
    End Sub
#End Region
#Region "ItemDataBound"
    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
            CType(e.Item.FindControl("uscTCPromiseDate"), ValidDate).FillRequired = False
            CType(e.Item.FindControl("chkTCChecked"), CheckBox).Attributes.Add("OnClick", _
                "return CheckTC('" & Trim(CType(e.Item.FindControl("uscTCPromiseDate"),  _
                ValidDate).ClientID) & "',this.checked);")
        End If
    End Sub
    Private Sub dtgTC2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblTCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            '   CType(e.Item.FindControl("uscTCPromiseDate2"), ValidDate).FillRequired = False
            '   CType(e.Item.FindControl("chkTCCheck2"), CheckBox).Attributes.Add("OnClick", _
            '"return CheckTC('" & Trim(CType(e.Item.FindControl("uscTCPromiseDate2"), _
            'ValidDate).ClientID) & "',this.checked);")
        End If
    End Sub
#End Region
#Region "Save"
    Private Sub imbSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbSave.Click
        Dim strBackDated As String
        If uscEffectiveDate.dateValue.Trim <> "" Then
            strBackDated = DateAdd(DateInterval.Day, GetMaxEffectiveBackDate, ConvertDate2(Me.AADate)).ToString("dd/MM/yyyy")
            'If Me.FirstInstallment = "AD" Then
            '    'If ConvertDate2(uscEffectiveDate.dateValue.Trim) < ConvertDate2(Me.AADate) Then
            '    '    lblMessage.Text = "First Installment is Advance, Effective Date cannot smaller than BAST Date(" + Me.AADate + ")!"
            '    '    Exit Sub
            '    'End If
            'ElseIf Me.FirstInstallment = "AR" Then
            '    If ConvertDate2(uscEffectiveDate.dateValue.Trim) < ConvertDate2(strBackDated) Then
            '        lblMessage.Text = "First Installment is Arrear, Effective Date cannot smaller than BAST Date(" + strBackDated + ")!"
            '        Exit Sub
            '    End If
            'End If
        Else
            lblMessage.Text = "Harap isi Tanggal Effective"
            Exit Sub
        End If

        If uscAgreementDate.dateValue.Trim <> "" Then
            If Not (ConvertDate2(uscAgreementDate.dateValue) <= Me.BusinessDate) Then
                lblMessage.Text = "Tanggal Kontrak harus <=  " & Format(Me.BusinessDate, "dd/M/yyyy") & "!"
                LabelValidationFalse()
                Exit Sub
            End If
        Else
            lblMessage.Text = "Harap isi Tanggal Kontrak"
            Exit Sub
        End If

        If ConvertDate2(uscEffectiveDate.dateValue.Trim) > Me.BusinessDate Then
            pnlConfirm.Visible = True
            pnlList.Visible = False
            lblMessageConfirm.Text = "Apakah Yakin Tanggal Pencairan adalah Tanggal Effective ?"
        Else
            Save_Continue()
        End If

    End Sub
    Sub Save_Continue()
        Dim FundingCoyID As String
        Dim FundingContractNo As String
        Dim FundingBatchNo As String

        If Me.FundingPledgeStatus <> "N" Then
            If drdCompany.SelectedItem.Value <> "0" Then
                If tempChild.Value.Trim = "-" Or tempChild.Value.Trim = "" Then
                    lblMessage.Text = "Harap pilih No Kontrak Bank atau Tidak pilih Funding bank"
                    lblMessage.Visible = True
                    bindComboCompany()
                    Response.Write(GenerateScript(getComboContract, getComboBatchDate))
                    Bind()
                    Exit Sub
                Else
                    If tempChild2.Value.Trim = "-" Or tempChild2.Value.Trim = "" Then
                        lblMessage.Text = "Harap pilih No batch atau Tidak memmilih Funding Bank dan No Kontrak Bank"
                        lblMessage.Visible = True
                        bindComboCompany()
                        Response.Write(GenerateScript(getComboContract, getComboBatchDate))
                        Bind()
                        Exit Sub
                    End If
                End If
            End If
            FundingCoyID = drdCompany.SelectedItem.Value
            FundingContractNo = tempChild.Value.Trim
            FundingBatchNo = tempChild2.Value.Trim
        Else
            FundingCoyID = lblCompany.Text.Trim
            FundingContractNo = LblContract.Text.Trim
            FundingBatchNo = LblBatchdate.Text.Trim
        End If

        Dim oData1 As New DataTable
        Dim oData2 As New DataTable

        oData1.Columns.Add("MasterTCID", GetType(String))
        oData1.Columns.Add("PriorTo", GetType(String))
        oData1.Columns.Add("IsChecked", GetType(String))
        oData1.Columns.Add("IsMandatory", GetType(String))
        oData1.Columns.Add("PromiseDate", GetType(String))
        oData1.Columns.Add("Notes", GetType(String))

        oData2.Columns.Add("MasterTCID", GetType(String))
        oData2.Columns.Add("AGTCCLSequenceNo", GetType(String))
        oData2.Columns.Add("IsChecked", GetType(String))
        oData2.Columns.Add("PromiseDate", GetType(String))
        oData2.Columns.Add("Notes", GetType(String))

        Status = True
        Validator(oData1, oData2)
        If Status = False Then
            Exit Sub
        End If
        cek_Check()
        Dim oReturn As New Parameter.Application
        Dim oGoLive As New Parameter.Application
        oGoLive.EffectiveDate = ConvertDate2(uscEffectiveDate.dateValue)
        oGoLive.AgrDate = ConvertDate2(uscAgreementDate.dateValue)
        oGoLive.AppID = Me.AppId
        oGoLive.Notes = txtNotes.Text.Trim
        oGoLive.BranchId = Replace(Me.sesBranchId, "'", "")
        oGoLive.BusinessDate = Me.BusinessDate
        oGoLive.ApplicationID = Me.ApplicationID
        oGoLive.strConnection = GetConnectionString
        oGoLive.FundingCoyId = FundingCoyID.Trim
        oGoLive.FundingContractNo = FundingContractNo.Trim
        oGoLive.FundingBatchNo = FundingBatchNo.Trim
        oReturn = m_controller.GoLiveSave(oGoLive, oData1, oData2)
        If oReturn.Err <> "" Then
            lblMessage.Text = oReturn.Err
        Else
            Response.Redirect("GoLive.aspx")
        End If
    End Sub
    Sub Validator(ByRef oData1 As DataTable, ByRef oData2 As DataTable)
        Dim PriorTo As String
        Dim Mandatory As String
        Dim Checked As Boolean
        Dim PromiseDate As ValidDate
        Dim validCheck As New Label
        Dim MasterTCID, MasterTCID2, AGTCCLSequenceNo, Notes As String
        Dim DtgTC1count As Integer = dtgTC.Items.Count - 1
        Dim DtgTC2count As Integer = dtgTC2.Items.Count - 1
        Dim objrow As DataRow
        Dim intLoop2, intLoop3, intLoop As Integer

        For intLoop = 0 To CInt(IIf(DtgTC2count > DtgTC1count, DtgTC2count, DtgTC1count))
            If intLoop <= DtgTC1count Then
                MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
                PriorTo = dtgTC.Items(intLoop).Cells(2).Text.Trim
                Mandatory = dtgTC.Items(intLoop).Cells(4).Text.Trim
                Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
                PromiseDate = CType(dtgTC.Items(intLoop).FindControl("uscTCPromiseDate"), ValidDate)
                validCheck = CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label)
                Notes = CType(dtgTC.Items(intLoop).Cells(6).FindControl("txtTCNotes"), TextBox).Text
                If PromiseDate.dateValue <> "" Then
                    If CInt(ConvertDate(PromiseDate.dateValue)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVPromiseDate"), Label).Visible = True
                    End If
                End If
                If PriorTo = "NAP" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.dateValue = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                    validCheck.Visible = True
                End If
                objrow = oData1.NewRow
                objrow("MasterTCID") = MasterTCID
                objrow("PriorTo") = PriorTo
                objrow("IsChecked") = IIf(Checked = True, "1", "0")
                objrow("IsMandatory") = IIf(Mandatory = "v", "1", "0")
                objrow("PromiseDate") = IIf(PromiseDate.dateValue <> "", ConvertDate(PromiseDate.dateValue), "")
                objrow("Notes") = Notes
                oData1.Rows.Add(objrow)
            End If
            If intLoop <= DtgTC2count Then
                MasterTCID = dtgTC2.Items(intLoop).Cells(8).Text
                AGTCCLSequenceNo = dtgTC2.Items(intLoop).Cells(9).Text
                PriorTo = dtgTC2.Items(intLoop).Cells(3).Text.Trim
                Mandatory = dtgTC2.Items(intLoop).Cells(5).Text.Trim
                Notes = CType(dtgTC2.Items(intLoop).Cells(7).FindControl("txtTCNotes2"), TextBox).Text
                Checked = CType(dtgTC2.Items(intLoop).FindControl("chkTCCheck2"), CheckBox).Checked
                'PromiseDate = CType(dtgTC2.Items(intLoop).FindControl("uscTCPromiseDate2"), ValidDate)
                'validCheck = CType(dtgTC2.Items(intLoop).FindControl("lblVTC2Checked"), Label)
                'If PromiseDate.dateValue <> "" Then
                '    If CInt(ConvertDate(PromiseDate.dateValue)) <= CInt((Me.BusinessDate).ToString("yyyyMMdd")) Then
                '        CType(dtgTC2.Items(intLoop).FindControl("lblVPromiseDate2"), Label).Visible = True
                '    End If
                'End If
                If PriorTo = "NAP" And Mandatory = "v" Then
                    If Checked = False And PromiseDate.dateValue = "" Then
                        If Status <> False Then
                            Status = False
                        End If
                        validCheck.Visible = True
                    End If
                End If
                objrow = oData2.NewRow
                objrow("MasterTCID") = MasterTCID
                objrow("AGTCCLSequenceNo") = AGTCCLSequenceNo
                objrow("IsChecked") = IIf(Checked = True, "1", "0")
                objrow("PromiseDate") = IIf(PromiseDate.dateValue <> "", ConvertDate(PromiseDate.dateValue), "")
                objrow("Notes") = Notes
                oData2.Rows.Add(objrow)
            End If
        Next

        Dim RainCheck As Boolean = False
        Dim intLoop4, intLoop5 As Integer
        For intLoop = 0 To DtgTC1count
            MasterTCID = dtgTC.Items(intLoop).Cells(7).Text
            Checked = CType(dtgTC.Items(intLoop).FindControl("chkTCChecked"), CheckBox).Checked
            For intLoop2 = 0 To DtgTC2count
                MasterTCID2 = dtgTC2.Items(intLoop2).Cells(8).Text
                If MasterTCID = MasterTCID2 Then
                    If Checked = True Then
                        CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True

                        If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = False Then
                            CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                        Else
                            CType(dtgTC2.Items(intLoop2).FindControl("lblVTC2Checked"), Label).Visible = True
                        End If
                    Else
                        If CType(dtgTC2.Items(intLoop2).FindControl("chkTCCheck2"), CheckBox).Checked = True Then
                            For intLoop4 = 0 To intLoop2
                                If dtgTC2.Items(intLoop4).Cells(8).Text = MasterTCID Then
                                    CType(dtgTC2.Items(intLoop4).FindControl("lblVTC2Checked"), Label).Visible = True
                                End If
                            Next

                            For intLoop5 = intLoop2 To DtgTC2count
                                If dtgTC2.Items(intLoop5).Cells(8).Text = MasterTCID Then
                                    CType(dtgTC2.Items(intLoop5).FindControl("lblVTC2Checked"), Label).Visible = True
                                End If
                            Next


                            CType(dtgTC.Items(intLoop).FindControl("lblVTCChecked"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                            For intLoop3 = 0 To intLoop
                                If dtgTC.Items(intLoop3).Cells(7).Text = MasterTCID2 Then
                                    If CType(dtgTC.Items(intLoop3).FindControl("chkTCChecked"), CheckBox).Checked = False Then
                                        CType(dtgTC.Items(intLoop3).FindControl("lblVTCChecked"), Label).Visible = True
                                    End If
                                End If
                            Next
                        End If
                    End If
                End If
            Next
            RainCheck = False
        Next
    End Sub
    Public Function GetMaxEffectiveBackDate() As Integer
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        oApplication.strConnection = GetConnectionString
        oApplication.AppID = Me.ApplicationID
        oApplication.BranchId = Me.sesBranchId
        oApplication.AddEdit = "Edit"
        oApplication = m_controller.GetGoLiveBackDated(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        Dim BackDated As Integer
        If oData.Rows.Count > 0 Then
            BackDated = CInt(oData.Rows(0).Item(0))
            Return BackDated * -1
        Else
            Return 0
        End If
    End Function
#End Region
#Region "cek_Check"
    Sub cek_Check()
        Dim CheckTC As Boolean
        Dim CheckTC2 As Boolean
        Dim counter As Integer
        Dim PromiseDate As ValidDate       

        For counter = 0 To dtgTC.Items.Count - 1
            CheckTC = CType(dtgTC.Items(counter).FindControl("chkTCChecked"), CheckBox).Checked
            If CheckTC Then
                PromiseDate = CType(dtgTC.Items(counter).FindControl("uscTCPromiseDate"), ValidDate)
                PromiseDate.dateValue = ""
            End If
        Next

        For counter = 0 To dtgTC2.Items.Count - 1
            CheckTC2 = CType(dtgTC2.Items(counter).FindControl("chkTCCheck2"), CheckBox).Checked
            If CheckTC2 Then
                'PromiseDate2 = CType(dtgTC2.Items(counter).FindControl("uscTCPromiseDate2"), ValidDate)
                'PromiseDate2.dateValue = ""
            End If
        Next
    End Sub
#End Region
#Region "imgOK"
    Private Sub imgOK_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgOK.Click
        Save_Continue()
    End Sub
#End Region
#Region "imgCancel"
    Private Sub imgCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCancel.Click
        pnlList.Visible = True
        pnlConfirm.Visible = False
        Bind()
        'imgCancel.Attributes.Add("OnClick", "return fback();")

    End Sub
#End Region
#Region "imbCancel"
    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCancel.Click
        Response.Redirect("GoLive.aspx")
    End Sub
#End Region
#Region "Bind Combo Funding"
#Region "Generate Script"
    Private Function GenerateScript(ByVal DtChild As DataTable, ByVal DtChild2 As DataTable) As String
        Dim strScript As String
        Dim j As Int32

        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "function ChildVTPair (value,text,parent)" & vbCrLf
        strScript &= "{" & vbCrLf
        strScript &= "			this.value = value;" & vbCrLf
        strScript &= "					this.text = text;" & vbCrLf
        strScript &= "		this.parent = parent;" & vbCrLf
        strScript &= "				}" & vbCrLf

        strScript &= "var AOSupervisor = new Array(" & vbCrLf

        For j = 0 To DtChild.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingContractNo")).Trim & "','" & _
                            CStr(DtChild.Rows(j).Item("FundingCoyID")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var BatchDate = new Array(" & vbCrLf

        For j = 0 To DtChild2.Rows.Count - 1
            strScript &= " new ChildVTPair('" & CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingBatchNo")).Trim & "','" & _
                            CStr(DtChild2.Rows(j).Item("FundingContractNo")).Trim & "')," & vbCrLf
        Next
        strScript = Left(strScript.Trim, Len(strScript.Trim) - 1)
        strScript &= ");" & vbCrLf

        strScript &= "var tampungGrandChild = '" & tempChild.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChild2 = '" & tempChild2.ClientID & "';" & vbCrLf
        strScript &= "var tampungGrandChildName = '" & tempChildName.ClientID & "';" & vbCrLf
        strScript &= "</script>"
        Return strScript
    End Function
#End Region

#Region "GetComboContract"
    Private Function getComboContract() As DataTable
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboContractRCA"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            lblMessage.Text = e.Message
            lblMessage.Visible = True
        End Try

    End Function
#End Region
#Region "GetComboBatchDate"
    Private Function getComboBatchDate() As DataTable
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboBatchDate"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try
            dtsEntity = oContract.ListData
            Return dtsEntity

        Catch e As Exception
            lblMessage.Text = e.Message
            lblMessage.Visible = True
        End Try

    End Function
#End Region
#Region "BindComboCompany"
    Private Sub bindComboCompany()
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim cContract As New GeneralPagingController
        Dim oContract As New Parameter.GeneralPaging

        With oContract
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spFundingGetComboCompany"
        End With

        oContract = cContract.GetGeneralPaging(oContract)
        Try

            dtsEntity = oContract.ListData
            dtvEntity = dtsEntity.DefaultView


            drdCompany.DataValueField = "FundingCoyId"
            drdCompany.DataTextField = "FundingCoyName"

            drdCompany.DataSource = dtvEntity

            drdCompany.DataBind()
            drdCompany.Items.Insert(0, "All")
            drdCompany.Items(0).Value = "0"
        Catch e As Exception
            lblMessage.Text = e.Message
            lblMessage.Visible = True
        End Try

    End Sub
#End Region
    Protected Function drdCompanyChange() As String
        Return "ChangeMultiCombo(this,'" & drdContract.ClientID & "', AOSupervisor, '" & tempParent.ClientID & "');"
    End Function

    Protected Function drdContractChange() As String
        Return "ChangeMultiCombo(this,'" & drdBatchDate.ClientID & "', BatchDate, '" & tempChild.ClientID & "');setCboDetlVal(this.options[this.selectedIndex].value, this.options[this.selectedIndex].text);"
    End Function
#End Region

End Class