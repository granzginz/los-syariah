﻿#Region "Import"
Imports System.Data.SqlClient
Imports System.Data
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class POExtendDetail
    Inherits Maxiloan.Webform.WebBased

#Region "Declaration"
    Protected WithEvents lblPODate As System.Web.UI.WebControls.Label
    Protected WithEvents lblPOAmount As System.Web.UI.WebControls.Label
    Protected WithEvents lblErr As System.Web.UI.WebControls.Label
    Protected WithEvents btnSave As System.Web.UI.WebControls.ImageButton
    Protected WithEvents btnBack As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lblPONo As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblAgreement As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblSupplier As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblRemain As System.Web.UI.WebControls.Label
    Protected WithEvents LblExtend As System.Web.UI.WebControls.Label
    Protected WithEvents lblName As System.Web.UI.WebControls.HyperLink
    Protected WithEvents oDate1 As ValidDate
#End Region
    Private m_controller As New POController

#Region "Property"
    Private Property Status() As Boolean
        Get
            Return CType(viewstate("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("Status") = Value
        End Set
    End Property
    Private Property POExpiredDate() As String
        Get
            Return CType(viewstate("POExpiredDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("POExpiredDate") = Value
        End Set
    End Property
    Private Property POOriginalExpiredDate() As String
        Get
            Return CType(viewstate("POOriginalExpiredDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("POOriginalExpiredDate") = Value
        End Set
    End Property
    Private Property PODate() As String
        Get
            Return CType(viewstate("PODate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PODate") = Value
        End Set
    End Property
    Private Property POExtendCounter() As String
        Get
            Return CType(viewstate("POExtendCounter"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("POExtendCounter") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property POExtendDays() As String
        Get
            Return CType(viewstate("POExtendDays"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("POExtendDays") = Value
        End Set
    End Property
    Private Property MailTransNo() As String
        Get
            Return CType(viewstate("MailTransNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MailTransNo") = Value
        End Set
    End Property
    Private Property PONo() As String
        Get
            Return CType(viewstate("PONo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PONo") = Value
        End Set
    End Property
    Private Property Remain() As String
        Get
            Return CType(viewstate("Remain"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Remain") = Value
        End Set
    End Property
    Private Property MailTypeID() As String
        Get
            Return CType(viewstate("MailTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MailTypeID") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(viewstate("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierID") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property POTotal() As String
        Get
            Return CType(viewstate("POTotal"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("POTotal") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AccountPayableNo() As String
        Get
            Return CType(viewstate("AccountPayableNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AccountPayableNo") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Private Property SupplierName() As String
        Get
            Return CType(viewstate("SupplierName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierName") = Value
        End Set
    End Property
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("POExtend")
        Me.PONo = cookie.Values("PONo")
        Me.CustomerID = cookie.Values("CustomerID")
        Me.SupplierID = cookie.Values("SupplierID")
        Me.CustomerName = cookie.Values("CustomerName")
        Me.SupplierName = cookie.Values("SupplierName")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.POExpiredDate = cookie.Values("POExpiredDate")
        Me.POOriginalExpiredDate = cookie.Values("POOriginalExpiredDate")
        Me.POExtendCounter = cookie.Values("POExtendCounter")
        Me.POExtendDays = cookie.Values("POExtendDays")
        Me.Remain = cookie.Values("Remain")
        Me.AgreementNo = cookie.Values("AgreementNo")
        Me.PODate = cookie.Values("PODate")
        Me.POTotal = cookie.Values("POTotal")
        Me.AccountPayableNo = cookie.Values("AccountPayableNo")

        'lblPONo.NavigateUrl = LinkToPONo("AccAcq", Me.AccountPayableNo, Me.sesBranchId.Replace("'", ""))
        lblName.NavigateUrl = LinkToCustomer(Me.CustomerID, "AccAcq")
        lblSupplier.NavigateUrl = LinkToSupplier(Me.SupplierID, "AccAcq")
        lblAgreement.NavigateUrl = LinkToAgreementNo(Me.ApplicationID, "AccAcq")
    End Sub
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.Status = True
        If Not IsPostBack Then
            lblErr.Text = ""
            oDate1.isCalendarPostBack = False

            GetCookies()
            'Me.PONo = Request.QueryString("PONo")
            'Me.CustomerID = Request.QueryString("CustomerID")
            'Me.SupplierID = Request.QueryString("SupplierID")
            'Me.CustomerName = Request.QueryString("CustomerName")
            'Me.SupplierName = Request.QueryString("SupplierName")
            ''Me.AccountPayableNo = Request.QueryString("AccountPayableNo")
            'Me.ApplicationID = Request.QueryString("ApplicationID")
            'Me.POExpiredDate = Request.QueryString("POExpiredDate")
            'Me.POOriginalExpiredDate = Request.QueryString("POOriginalExpiredDate")
            'Me.POExtendCounter = Request.QueryString("POExtendCounter")
            'Me.POExtendDays = Request.QueryString("POExtendDays")
            'Me.Remain = Request.QueryString("Remain")
            'Me.AgreementNo = Request.QueryString("AgreementNo")
            'Me.PODate = Request.QueryString("PODate")
            'Me.POTotal = Request.QueryString("POTotal")
            '==============================================================================

            If CInt(Me.Remain) >= 0 Then
                lblRemain.Text = "+" & Me.Remain
            Else
                lblRemain.Text = Me.Remain
            End If

            lblPONo.Text = Me.PONo
            lblPODate.Text = Me.PODate
            lblPOAmount.Text = FormatNumber(Me.POTotal, 2)
            oDate1.dateValue = Me.POExpiredDate
            lblName.Text = Me.CustomerName
            lblAgreement.Text = Me.AgreementNo
            LblExtend.Text = Me.POExtendCounter
            lblSupplier.Text = Me.SupplierName
        End If
    End Sub
#End Region
#Region "LinkTo"
    Function LinkToPONo(ByVal strStyle As String, ByVal strAccountPayableNo As String, ByVal strBranchID As String) As String
        Return "javascript:OpenViewApSupplier('" & strStyle & "', '" & strAccountPayableNo & "','" & strBranchID & "')"
    End Function

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strSupplierID & "','" & strStyle & "')"
    End Function

    Function LinkToAgreementNo(ByVal strApplicationId As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinAgreementNo('" & strApplicationId & "','" & strStyle & "')"
    End Function

#End Region
#Region "CheckDate"
    Sub CheckDate()
        Dim NewDate As Date
        Dim ExpDate As Date
        Dim OrgExpDate As Date

        If oDate1.dateValue.Trim <> "" Then
            NewDate = ConvertDate2(oDate1.dateValue)
        End If

        If Me.POExpiredDate.Trim <> "" Then
            ExpDate = ConvertDate2(Me.POExpiredDate)
        End If

        If Me.POOriginalExpiredDate.Trim <> "" Then
            OrgExpDate = ConvertDate2(Me.POOriginalExpiredDate)
        End If

        Dim MaxDate As Date = DateAdd(DateInterval.Day, CDbl(Me.POExtendDays), OrgExpDate)

        If (NewDate < Me.BusinessDate) Then
            lblErr.Text = "<li> Date value must be within " & Format(Me.BusinessDate, "dd MMMM yyyy") & " and " & Format(MaxDate, "dd MMMM yyyy")
            Me.Status = False
        ElseIf NewDate > MaxDate Then
            lblErr.Text = "<li> Date value must be within " & Format(ExpDate, "dd MMMM yyyy") & " and " & Format(MaxDate, "dd MMMM yyyy")
            Me.Status = False
        Else
            lblErr.Text = ""
        End If
    End Sub
#End Region
#Region "btnSave"
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        CheckDate()
        If Me.Status = False Then
            Exit Sub
        End If

        Dim oPOExtend As New Parameter.PO

        oPOExtend.POExtendCounter = CInt(Me.POExtendCounter) + 1
        oPOExtend.IsExpired = False
        oPOExtend.POExpiredDate = ConvertDate2(oDate1.dateValue)
        oPOExtend.PONumber = Me.PONo
        oPOExtend.strConnection = Me.GetConnectionString
        m_controller.POExtendSaveEdit(oPOExtend)

        Response.Redirect("POExtendList.aspx?Result=OK")
    End Sub
#End Region
#Region "btnBack"
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBack.Click
        Response.Redirect("POExtendList.aspx")
    End Sub
#End Region

End Class