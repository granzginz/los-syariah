﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PO.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.PO" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../webform.UserController/ucSyaratCairPO.ascx" TagPrefix="uc4"
    TagName="ucSyaratCair" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Purchase Order</title>
    <link rel="Stylesheet" href="../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../Include/Buttons.css" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function hideMessage() {
            document.getElementById('lblMessage').style.display = 'none';
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false"  onclick="hideMessage()"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                PURCHASE ORDER
            </h3>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="1500px" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderWidth="0" CellPadding="3" CellSpacing="1" DataKeyField="ApplicationID"
                        CssClass="grid_ws">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkAction" runat="server" CommandName="View" Text='PO' CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkReturn" runat="server" CommandName="Return" Text='RETURN'
                                        CausesValidation="false"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CABANG" SortExpression="BranchFullName">
                                <ItemTemplate>
                                    <asp:label ID="lblCabang" runat="server" Text='<%#Container.DataItem("BranchFullName")%>'>
                                    </asp:label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NO KONTRAK" SortExpression="AgreementNo">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA CUSTOMER" SortExpression="CustomerName">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynCustomerName" runat="server" Text='<%#container.dataitem("CustomerName")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="NAMA SUPPLIER" SortExpression="SupplierName">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynSupplierName" runat="server" Text='<%#container.dataitem("SupplierName")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TELP SUPPLIER" SortExpression="SupplierPhone">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierPhone" runat="server" Text='<%#container.dataitem("SupplierPhone")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KAROSERI" SortExpression="SupplierKaroseri">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblSupplierKaroseri" Text='<%# Container.DataItem("SupplierKaroseri") %>'>
                                    </asp:Label>
                                    <asp:Label runat="server" ID="lblSupplierIDKaroseri" Visible="false" Text='<%# Container.DataItem("SupplierIDKaroseri") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SPLIT" SortExpression="SplitPembayaran">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkSplitPembayaran" Checked='<%# Container.DataItem("SplitPembayaran") %>'
                                        Enabled="false"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ACCOUNT OFFICER" SortExpression="EmployeeName">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hynEmployeeName" runat="server" Text='<%#container.dataitem("EmployeeName")%>'
                                        NavigateUrl=''>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL. APPROVAL" SortExpression="ApprovalDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblApprovalDate" runat="server" Text='<%#container.dataitem("ApprovalDate")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="APPLICATION ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%#container.dataitem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="CUSTOMER ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerID" runat="server" Text='<%#container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SUPPLIER ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%#container.dataitem("SupplierID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EMPLOYEE ID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmployeeID" runat="server" Text='<%#container.dataitem("EmployeeID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="AGREEMENT NO" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblAgreementNo" runat="server" Text='<%#container.dataitem("AgreementNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PRODUCTOFFERINGID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblProductOfferingID" runat="server" Text='<%#container.dataitem("ProductOfferingID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PRODUCTID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%#container.dataitem("ProductID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BRANCHID" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchID" runat="server" Text='<%#container.dataitem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last" CausesValidation="False">
                    </asp:ImageButton>
                    Page
                    <asp:TextBox ID="txtGoPage" runat="server">1</asp:TextBox>
                    <asp:Button ID="btnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtGopage"
                        Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" Display="Dynamic" ControlToValidate="txtGopage"
                        ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" Type="integer"
                        MinimumValue="1" ErrorMessage="No Halaman Salah!" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div>
                    <asp:Label ID="lblPage" runat="server"></asp:Label>of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>record(s)
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI PURCHASE ORDER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label class="medium_text">
                        Cari Berdasarkan
                    </label>
                    <asp:DropDownList ID="cboSearch" runat="server">
                        <asp:ListItem Value="Name">Nama Customer</asp:ListItem>
                        <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                        <asp:ListItem Value="EmployeeName">Nama Account Officer</asp:ListItem>
                        <asp:ListItem Value="SupplierName">Nama Supplier</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                </div>
                <div class="form_right">
                    <label>
                        Tanggal Konfirmasi
                    </label>
                    <asp:TextBox runat="server" ID="txtTanggalKonfirmasi"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="ceTanggalKonfirmasi" TargetControlID="txtTanggalKonfirmasi"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cabang
                    </label>
                    <asp:DropDownList ID="oBranch" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" InitialValue="0"
                        Display="Dynamic" ErrorMessage="Harap Pilih Cabang" ControlToValidate="oBranch"
                        CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlPO" runat="server">        
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No Kontrak
                    </label>
                    <asp:HyperLink ID="hplAgreement" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">
                    <label>
                        Nama Asset
                    </label>
                    <asp:Label ID="lblJenisBarang" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Customer
                    </label>
                    <asp:HyperLink ID="hplCustomerName" runat="server"></asp:HyperLink>
                </div>
                <div class="form_right">                
                    <label runat="server" ID="LabelSerialNo1"></label>
                    <asp:Label runat="server" ID="lblSerialNo1"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Tanggal PO
                    </label>
                    <asp:Label runat="server" ID="lblTanggalPO"></asp:Label>
                </div>
                <div class="form_right">
                    <label runat="server" ID="LabelSerialNo2"></label>
                    <asp:Label runat="server" ID="lblSerialNo2"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No. PO
                    </label>
                    <asp:Label ID="lblPONumber" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Tahun Produksi
                    </label>
                    <asp:Label runat="server" ID="lblTahunProduksi"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Dikirim ke alamat</label>
                </div>
                <div class="form_right">
                    <label>
                        Warna
                    </label>
                    <asp:Label runat="server" ID="lblWarna"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <div class="label_alamat">
                    <asp:Label ID="lblPOCustomerAddress" runat="server"></asp:Label>
                    <asp:Label ID="lblPOCustomerKelurahan" runat="server"></asp:Label>
                    <asp:Label ID="lblPOCustomerKecamatan" runat="server"></asp:Label>
                </div>
            </div>            
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Kota
                    </label>
                    <asp:Label ID="lblPOCustomerCity" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Kode Pos
                    </label>
                    <asp:Label ID="lblPOCustomerZipCode" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KEPADA
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Supplier</label>
                    <asp:Label ID="lblPOSupplierName" runat="server"></asp:Label>
                </div>
                <asp:Panel runat="server" ID="pnlKaroseri1">
                    <div class="form_right">
                        <label>
                            Nama Supplier Karoseri
                        </label>
                        <asp:Label runat="server" ID="lblSupplierKaroseri"></asp:Label>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label class="label_general">
                        Alamat
                    </label>
                    <label class="label_alamat">
                        <asp:Label ID="lblPOSupplierAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblPOSupplierRTRW" runat="server"></asp:Label>
                        <asp:Label ID="lblPOSupplierKelurahan" runat="server"></asp:Label>
                        <asp:Label ID="lblPOSupplierKecamatan" runat="server"></asp:Label>
                    </label>
                </div>
                <asp:Panel runat="server" ID="pnlKaroseri2">
                    <div class="form_right">
                        <label class="label_general">
                            Alamat
                        </label>
                        <label class="label_alamat">
                            <asp:Label ID="lblSupplierKaroseriAddress" runat="server"></asp:Label>
                            <asp:Label ID="lblSupplierKaroseriRTRW" runat="server"></asp:Label>
                            <asp:Label ID="lblSupplierKaroseriKelurahan" runat="server"></asp:Label>
                            <asp:Label ID="lblSupplierKaroseriKecamatan" runat="server"></asp:Label>
                        </label>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Kota
                    </label>
                    <asp:Label ID="lblPOSupplierCity" runat="server"></asp:Label>
                </div>
                <asp:Panel runat="server" ID="pnlKaroseri3">
                    <div class="form_right">
                        <label>
                            Kota
                        </label>
                        <asp:Label ID="lblSupplierKaroseriCity" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Kode Pos
                    </label>
                    <asp:Label ID="lblPOSupplierZipCode" runat="server"></asp:Label>
                </div>
                <asp:Panel runat="server" ID="pnlKaroseri4">
                    <div class="form_right">
                        <label>
                            Kode Pos
                        </label>
                        <asp:Label ID="lblSupplierKaroseriZipCode" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KETERANGAN
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Harga OTR</label>
                    <asp:Label ID="lblTotalOTR" runat="server"></asp:Label>
                </div>
                <asp:Panel runat="server" ID="pnlKaroseri5">
                    <div class="form_right">
                        <label>
                            Harga Karoseri</label>
                        <asp:Label ID="lblHargakaroseri" runat="server"></asp:Label>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="form_box_hide">
            <div>
                <div class="form_left">
                    <label>
                        Rekening Bank
                    </label>
                    <asp:DropDownList ID="cboSupplierAccount" runat="server">
                    </asp:DropDownList>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Rekening Bank!"
                        ControlToValidate="cboSupplierAccount" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                </div>
                <asp:Panel runat="server" ID="pnlKaroseri6">
                    <div class="form_right">
                        <label>
                            Rekening Bank
                        </label>
                        <asp:DropDownList ID="cboSupplierKaroseriAccount" runat="server">
                        </asp:DropDownList>
                        <%--<asp:RequiredFieldValidator ID="rfvKaroseriAcc" runat="server" ErrorMessage="Harap pilih Rekening Bank!"
                            ControlToValidate="cboSupplierKaroseriAccount" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Catatan
                    </label>
                    <asp:TextBox runat="server" ID="txtCatatan" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                </div>
                <asp:Panel runat="server" ID="pnlKaroseri7">
                    <div class="form_right">
                        <label>
                            Catatan
                        </label>
                        <asp:TextBox runat="server" ID="txtCatatanKaroseri" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                    </div>
                </asp:Panel>
            </div>
        </div>

        <%--modify by amri--%>
        <div class="form_box" style="display:none">
            <div>
                <div class="form_left">
                    <label>
                        Deskripsi Pengurang PO
                    </label>
                    <asp:TextBox runat="server" ID="txtdeskripsiPengurangPO" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                </div>
                <asp:Panel runat="server" ID="Panel2">
                    <div class="form_right">
                        <label>
                            Nilai
                        </label>
                         <uc1:ucnumberformat id="ucpengurangPO" runat="server" />
                    </div>
                </asp:Panel>
            </div>
        </div>

        <div class="form_box">
            <div>
                <div class="form_left">
                    <label class ="label_req">
                        ID Kurs
                    </label>
                    <asp:DropDownList ID="cboIDKurs" runat="server" CssClass="select">
                    </asp:DropDownList>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih ID Kurs"
                            ControlToValidate="cboIDKurs" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                </div>
                <asp:Panel runat="server" ID="Panel1">
                    <div class="form_right">
                        <label class ="label_req">
                            Nilai Kurs
                        </label>
                         <uc1:ucnumberformat id="UcNilaiKurs" runat="server"></uc1:ucnumberformat>
                    </div>
                </asp:Panel>
            </div>
        </div>

            <%--<div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="">
                           ID Kurs
                        </label>
                        <asp:DropDownList ID="cboIDKurs" runat="server" CssClass="select">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih ID Kurs"
                            ControlToValidate="cboIDKurs" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <label>
                           Nilai Kurs
                        </label>
                        <asp:TextBox ID="txtNilaiKurs" runat="server" CssClass="medium_text"  MaxLength="50"></asp:TextBox>
                    </div>
                    </div>
                </div>--%>

        <uc4:ucsyaratcair id="ucSyaratCair" runat="server" />
        <%--<div class="form_title">
            <div class="form_single">
                <h4>
                    SYARAT &amp; KONDISI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgTC" runat="server" CellPadding="3" CellSpacing="1" Width="100%"
                    PageSize="3" DataKeyField="TCName" BorderWidth="0px" AutoGenerateColumns="False"
                    CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                            <HeaderStyle Width="25%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                            <HeaderStyle Width="7%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="CHECKED">
                            <HeaderStyle Width="8%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTCChecked" Checked='<%# DataBinder.eval(Container, "DataItem.Checked")%>'
                                    runat="server"></asp:CheckBox>
                                <asp:Label ID="lblVTCChecked" runat="server">Harus diperiksa</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY">
                            <HeaderStyle Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="TGL JANJI">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtPromiseDate"></asp:TextBox>
                                <asp:CalendarExtender runat="server" ID="cePromiseDate" TargetControlID="txtPromiseDate"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:Label ID="lblVPromiseDate" runat="server">Tanggal Janji harus > hari ini</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <HeaderStyle Width="30%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="txtTCNotes" Text='<%# DataBinder.eval(Container, "DataItem.Notes")%>'
                                    runat="server" Width="95%">
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <h4>
                    SYARAT &amp; KONDISI CHECK LIST</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgTC2" runat="server" CellPadding="3" CellSpacing="1" Width="100%"
                    PageSize="3" DataKeyField="TCName" BorderWidth="0px" AutoGenerateColumns="False"
                    CssClass="grid_general">
                    <ItemStyle CssClass="item_grid"></ItemStyle>
                    <HeaderStyle HorizontalAlign="Center" CssClass="th"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTCNo2" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM">
                            <HeaderStyle Width="7%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="PERIKSA">
                            <HeaderStyle Width="8%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkTCCheck2" Checked='<%# DataBinder.eval(Container, "DataItem.Checked")%>'
                                    runat="server"></asp:CheckBox>
                                <asp:Label ID="lblVTC2Checked" runat="server">Harus diperiksa</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY">
                            <HeaderStyle Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="TGL JANJI" Visible="true">
                            <HeaderStyle Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtPromiseDate2"></asp:TextBox>
                                <asp:CalendarExtender runat="server" ID="cePromiseDate2" TargetControlID="txtPromiseDate2"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                                <asp:Label ID="lblVPromiseDate2" runat="server">Tanggal Janji harus > hari ini</asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CATATAN">
                            <HeaderStyle Width="30%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:TextBox ID="txtTCNotes2" runat="server" Text='<%# DataBinder.eval(Container, "DataItem.Notes")%>'
                                    Width="95%">
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>--%>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                CausesValidation="True" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                CausesValidation="False" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlReturn" runat="server" Visible="False">
        <div class="form_box">
            <div class="form_left">
                <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
            </div>
            <div class="form_right">
                <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_general">
                    Alasan Return</label>
                <asp:TextBox ID="txtAlasanReturn" runat="server" CssClass="multiline_textbox" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    CssClass="validator_general" ControlToValidate="txtAlasanReturn"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnReturn" runat="server" Text="Return" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="ButtonCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <input id="hdnApplicationID" type="hidden" name="hdnApplicationID" runat="server" />
    </form>
</body>
</html>
