﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services


Public Class ViewFinancialDataOperatingLease_003
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabOperatingLease
#End Region


#Region "Constanta"
    Private m_controller As New FinancialDataController
#End Region

#Region "Property"
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        lblMessage.Text = ""
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("CustID")
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .CustomerID = Me.CustomerID
                .bindData()
                .initControls("OnNewApplication")
            End With
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()
            BindEdit()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial")
            ucApplicationTab1.setLink()
        End If
    End Sub

#End Region

#Region "Load Financial Data"
    Sub BindEdit()
        Dim oData As New DataTable
        Dim oDataamortization As New DataTable
        Dim oFinancialData As New Parameter.FinancialData
        Dim int As Integer

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData = m_controller.GetViewFinancialData(oFinancialData)

        If Not oFinancialData Is Nothing Then
            oData = oFinancialData.Data1
            oDataamortization = oFinancialData.data2
        End If
        If oFinancialData.Output <> "" Then
            lblMessage.Text = oFinancialData.Output
            Exit Sub
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
        dtgViewInstallment.DataSource = oDataamortization.DefaultView
        dtgViewInstallment.DataBind()
        int = dtgViewInstallment.Items.Count - 1
        dtgViewInstallment.Items(0).Font.Bold = True
        dtgViewInstallment.Items(int).Font.Bold = True
    End Sub

    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)

        lblAssetInsurance3.Text = FormatNumber(oRow.Item("InsAssetCapitalized"), 0)
        lblOTR.Text = FormatNumber(oRow.Item("OTRKendaraan"), 0)
        lblKaroseri.Text = FormatNumber(oRow.Item("HargaKaroseri"), 0)
        lblPA.Text = FormatNumber(CDbl(oRow.Item("OTRKendaraan")) - CDbl(oRow.Item("RVEstimateOL")), 0)

        txtDP.Text = FormatNumber(Math.Round(CDbl(oRow.Item(4)), 0), 0)
        lblNTF.Text = FormatNumber(oRow.Item(5), 0)
        txtNumInst.Text = oRow.Item(8).ToString.Trim
        txtSukuBunga.Text = FormatNumber(CDbl(oRow.Item("FlatRate")), 7)

        txtDP.Text = FormatNumber(oRow.Item("DownPayment"), 0)
        lblRVEstimate.Text = FormatNumber(oRow.Item("RVEstimateOL"), 0)
        lblRVInterest.Text = FormatNumber(oRow.Item("RVInterestOL"), 0)
        lblInsurance.Text = FormatNumber(oRow.Item("InsuranceMonthlyOL"), 0)

        ucMaintenance.Text = FormatNumber(oRow.Item("MaintenanceFeeOL"), 0)
        ucSTNK.Text = FormatNumber(oRow.Item("STNKFeeOL"), 0)
        ucAdminFee.Text = FormatNumber(oRow.Item("AdminFeeOL"), 0)
        ucProvisi.Text = FormatNumber(oRow.Item("ProvisiOL"), 0)
        lblBasicLease.Text = FormatNumber(oRow.Item("BasicLease"), 0)
        lblExpectedInterest.Text = FormatNumber(CDbl(lblPA.Text) * (CDbl(oRow.Item("FlatRate")) / 100) * (CDbl(txtNumInst.Text) / 12), 0)

        Dim pgross, pinsnet, pselisih, ppolis, potherfee As Double

        pgross = 0
        ppolis = CDbl(IIf(IsNumeric(oRow.Item("BiayaPolis")), oRow.Item("BiayaPolis"), 0))
        potherfee = CDbl(IIf(IsNumeric(oRow.Item("OtherFee")), oRow.Item("OtherFee"), 0))
        pselisih = pgross - pinsnet


        CalculateTotalBayarPertama()
    End Sub

    Private Sub CalculateTotalBayarPertama()
         Dim ntf_ As Double = CDbl(lblOTR.Text) + CDbl(lblKaroseri.Text) - CDbl(txtDP.Text) + CDbl(lblAssetInsurance3.Text)
        lblNTF.Text = FormatNumber(ntf_, 0)
    End Sub

#End Region


End Class