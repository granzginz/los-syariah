﻿Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
Imports System.IO
Imports System.Collections.Generic

Public Class ViewApplicationOperatingLease_003
    Inherits Maxiloan.Webform.WebBased
    Private oController As New ApplicationController
    Private m_controller As New ProductController

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucMailingAddress As UcCompanyAddress
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabOperatingLease

#End Region

#Region "Constanta"
    Dim oRow As DataRow
    Dim Status As Boolean
#End Region

#Region "Properties"
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        lblMessage.Text = ""
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("CustID")
            initObjects()
            LoadingKegiatanUsaha()
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .CustomerID = Me.CustomerID
                .bindData()
                .initControls("OnNewApplication")
            End With
            BindEdit()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.setLink()
        End If
    End Sub
    Private Sub LoadingKegiatanUsaha()
        Me.KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataSource = m_controller.LoadKegiatanUsaha(GetConnectionString())
        cboKegiatanUsaha.DataValueField = "Value"
        cboKegiatanUsaha.DataTextField = "Text"
        cboKegiatanUsaha.DataBind()
        cboKegiatanUsaha.Items.Insert(0, "Select One")
        cboKegiatanUsaha.Items(0).Value = "SelectOne"
        cboKegiatanUsaha.SelectedIndex = 0
    End Sub

    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboJenisPembiyaan.DataValueField = "Value"
        cboJenisPembiyaan.DataTextField = "Text"


        If key = String.Empty Then
            cboJenisPembiyaan.DataSource = def
            cboJenisPembiyaan.DataBind()
        Else
            Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
            cboJenisPembiyaan.DataSource = k.JenisPembiayaan
        End If
        cboJenisPembiyaan.DataBind()
    End Sub


#Region "Init Objects"
    Protected Sub initObjects()
        ucViewApplication1.CustomerID = Me.CustomerID
        ucViewApplication1.bindData()
        ucViewApplication1.initControls("OnNewApplication")
        ucViewCustomerDetail1.CustomerID = Me.CustomerID
        ucViewCustomerDetail1.bindCustomerDetail()
        ucMailingAddress.ValidatorTrue()
        ucMailingAddress.showMandatoryAll()
        ucApplicationTab1.selectedTab("Application")
    End Sub
#End Region


#Region "BindEdit"
    Sub BindEdit()
        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        Dim oDataTC As New DataTable

        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "")
        oApplication.AppID = Me.ApplicationID
        oApplication = oController.GetViewApplication(oApplication)

        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
            oDataTC = oApplication.DataTC
        End If

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)

            lblProductOffering.Text = oRow("ProdOff").ToString.Trim
            cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(oRow("KegiatanUsaha").ToString))
            lblKegiatanUsaha.Text = cboKegiatanUsaha.SelectedItem.Text
            refresh_cboJenisPembiayaan(oRow("KegiatanUsaha").ToString)
            cboJenisPembiyaan.SelectedIndex = cboJenisPembiyaan.Items.IndexOf(cboJenisPembiyaan.Items.FindByValue(oRow("JenisPembiayaan").ToString))
            lblJenisPembiyaan.Text = cboJenisPembiyaan.SelectedItem.Text
            cboInterestType.SelectedIndex = cboInterestType.Items.IndexOf(cboInterestType.Items.FindByValue(oRow("InterestType").ToString))
            lblInterestType.Text = cboInterestType.SelectedItem.Text
            cboInstScheme.SelectedIndex = cboInstScheme.Items.IndexOf(cboInstScheme.Items.FindByValue(oRow("InstallmentScheme").ToString))
            lblInstScheme.Text = cboInstScheme.SelectedItem.Text
            If oRow.Item("StepUpStepDownType").ToString <> "" Then
                rdoSTTYpe.Items.FindByValue(oRow.Item("StepUpStepDownType").ToString).Selected = True
            End If

            cboWayPymt.SelectedIndex = cboWayPymt.Items.IndexOf(cboWayPymt.Items.FindByValue(oRow("WayOfPayment").ToString))
            lblWayPymt.Text = cboWayPymt.SelectedItem.Text
            cboSourceApp.SelectedIndex = cboSourceApp.Items.IndexOf(cboSourceApp.Items.FindByValue(oRow("ApplicationSource").ToString))
            lblSourceApp.Text = cboSourceApp.SelectedItem.Text

            ucAdminFee.Text = FormatNumber(oRow("AdminFee"), 0)
            ucFiduciaFee.Text = FormatNumber(oRow("FiduciaFee"), 0)
            chkFiducia.Checked = CBool(oRow("IsFiduciaCovered").ToString.Trim)
            ucOtherFee.Text = FormatNumber(oRow("OtherFee"), 0)

            txtKabupaten.Text = oRow("NamaKabKot").ToString
            chkCOP.Checked = CBool(IIf(oRow("IsCOP").ToString.Trim = "", "0", oRow("IsCOP").ToString.Trim))
            chkHakOpsi.Checked = CBool(IIf(oRow("HakOpsi").ToString.Trim = "", "0", oRow("HakOpsi").ToString.Trim))
            cboLiniBisnis.SelectedIndex = cboLiniBisnis.Items.IndexOf(cboLiniBisnis.Items.FindByValue(oRow("LiniBisnis").ToString.Trim))
            lblLiniBisnis.Text = cboLiniBisnis.SelectedItem.Text
            cboTipeLoan.SelectedIndex = cboTipeLoan.Items.IndexOf(cboTipeLoan.Items.FindByValue(oRow("TipeLoan").ToString.Trim))
            lblTipeLoan.Text = cboTipeLoan.SelectedItem.Text

            txtAppNotes.Text = oRow("Notes").ToString.Trim
            lblAdminFeeGross.Text = FormatNumber(CDbl(oRow("adminFee")) + CDbl(oRow("FiduciaFee")) + CDbl(oRow("BiayaPolis")) + CDbl(oRow("OtherFee")), 0)

            With ucMailingAddress
                .Address = oRow("MailingAddress").ToString.Trim
                .RT = oRow("MailingRT").ToString.Trim
                .RW = oRow("MailingRW").ToString.Trim
                .Kelurahan = oRow("MailingKelurahan").ToString.Trim
                .Kecamatan = oRow("MailingKecamatan").ToString.Trim
                .City = oRow("MailingCity").ToString.Trim
                .ZipCode = oRow("MailingZipCode").ToString.Trim
                .AreaPhone1 = oRow("MailingAreaPhone1").ToString.Trim
                .Phone1 = oRow("MailingPhone1").ToString.Trim
                .AreaPhone2 = oRow("MailingAreaPhone2").ToString.Trim
                .Phone2 = oRow("MailingPhone2").ToString.Trim
                .AreaFax = oRow("MailingAreaFax").ToString.Trim
                .Fax = oRow("MailingFax").ToString.Trim
                .BindAddress()
            End With

        End If
        dtgTC.DataSource = oDataTC.DefaultView
        dtgTC.DataBind()
    End Sub

    Private Sub dtgTC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTC.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyupload As HyperLink = CType(e.Item.FindControl("hyupload"), HyperLink)
            Dim MasterTCID As TextBox = CType(e.Item.FindControl("MasterTCID"), TextBox)

            If File.Exists(pathFile("Dokumen", Me.CustomerID) + MasterTCID.Text.ToString.Trim + ".jpg") Then
                hyupload.Attributes.Add("OnClick", "UploadDokument('" & ResolveClientUrl("~/webform.Utility/jLookup/UploadDokument.aspx?Upload=false&doc=" & MasterTCID.Text.Trim & "&CustomerID=" & Me.CustomerID) & "','Upload Dokument','" & jlookupContent.ClientID & "') ")
                hyupload.Visible = True
            Else
                hyupload.Visible = False
            End If


            CType(e.Item.FindControl("lblTCNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Function pathFile(ByVal Group As String, ByVal CustomerID As String) As String
        Dim strDirectory As String = ""
        strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & CustomerID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

#End Region


End Class