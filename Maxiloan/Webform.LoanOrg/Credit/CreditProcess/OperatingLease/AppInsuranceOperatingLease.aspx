﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AppInsuranceOperatingLease.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.AppInsuranceOperatingLease" %>

<%@ Register Src="../../../../webform.UserController/UcApplicationType.ascx" TagName="UcApplicationType"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTabOL.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucTPLOption.ascx" TagName="ucTPLOption"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucCoverageTypeApk.ascx" TagName="ucCoverageTypeApk"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../webform.UserController/ucCoverageTypeRate.ascx" TagName="ucCoverageTypeRate"
    TagPrefix="uc4" %>
<%@ Register Src="../../../../Webform.UserController/ucInsuranceBranchName.ascx"
    TagName="ucInsuranceBranchName" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Data Asuransi</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.min.js"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" type="text/javascript">

        function OpenWinApplication(pApplicationID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'Application', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
        function hitung(val) {
            var rateAmount = $("#txtAmountRate_txtNumber").val();
            var bpolis = $("#txtBiayaPolis_txtNumber").val();
            var bmaterai = $("#txtBiayaMaterai_txtNumber").val();
            var hasil = (parseInt(rateAmount.replace(/\s*,\s*/g, '')) + parseInt(bmaterai.replace(/\s*,\s*/g, '')) + parseInt(bpolis.replace(/\s*,\s*/g, ''))) - parseInt(val.replace(/\s*,\s*/g, ''));
            $("#<%=lblPremiDiKredit.ClientID %>").html(number_format(hasil));
        }
        function hitungdaripolis(val) {
            var rateAmount = $("#txtAmountRate_txtNumber").val();
            var bayartunai = $("#txtBayarTunai_txtNumber").val();
            var bmaterai = $("#txtBiayaMaterai_txtNumber").val();
            var hasil = (parseInt(rateAmount.replace(/\s*,\s*/g, '')) + parseInt(bmaterai.replace(/\s*,\s*/g, '')) + parseInt(val.replace(/\s*,\s*/g, ''))) - parseInt(bayartunai.replace(/\s*,\s*/g, '')) ;
            $("#<%=lblPremiDiKredit.ClientID %>").html(number_format(hasil));
        }
        function hitungdarimaterai(val) {
            var rateAmount = $("#txtAmountRate_txtNumber").val();
            var bayartunai = $("#txtBayarTunai_txtNumber").val();
            var bpolis = $("#txtBiayaPolis_txtNumber").val();
            var hasil = (parseInt(rateAmount.replace(/\s*,\s*/g, '')) + parseInt(bpolis.replace(/\s*,\s*/g, '')) + parseInt(val.replace(/\s*,\s*/g, ''))) - parseInt(bayartunai.replace(/\s*,\s*/g, ''));
            $("#<%=lblPremiDiKredit.ClientID %>").html(number_format(hasil));
        }
        function PilihPlusMinus(isi, txtDiscToCust) {
            if(isi != ""){
                $('#' + txtDiscToCust).removeAttr("disabled");
            }else{
                $('#' + txtDiscToCust).attr("disabled","disabled");
                $('#' + txtDiscToCust).val(0);
            }
            hitunganPaidCust()
        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function PilihDibayar(isi, lblClient,lblPremium) {
            if (isi == "ONLOAN") {
                $('#' + lblClient).val('0');
            }else{
                $('#' + lblClient).val($('#' + lblPremium).val());
            }

            var vartype = "";
            $('#Dgrid2InsurancePaid').find('tr').each(function (i, el) {
                if (i > 0) {
                    var $tds = $(this).find('td');
                    var type = $tds[1].getElementsByTagName('select')[0];
                    var bayarcust = $tds[4].getElementsByTagName('input')[0];
                    
                    if (vartype == "ONLOAN") {
                        type.value = "ONLOAN";
                        bayarcust.value = 0;
                    } else if (bayarcust.id == lblClient) {
                        vartype = type.value;
                    }
                }
            });

            hitunganPaidCust();
        }
        function inputDiscount(isi, MainPremiumToCust, PaidAmtByCust, total_, cbo, cboPlusDisc) {
            var PremiumToCust = $('#' + MainPremiumToCust).val();
            var PlusMinus = $('#' + cboPlusDisc).val();
            var diskon = 0;
            var penambahan = 0;
            var paidBy = $('#' + cbo).val();

            if (PlusMinus == "+") {
                diskon = parseInt(PremiumToCust.replace(/\s*,\s*/g, '')) + parseInt(isi);
                $('#' + total_).val(number_format(diskon));
            } else if (PlusMinus == "-") {
                penambahan = parseInt(PremiumToCust.replace(/\s*,\s*/g, '')) - parseInt(isi);
                $('#' + total_).val(number_format(penambahan));
            }

            if (paidBy != "ONLOAN") {
                $('#' + PaidAmtByCust).val(number_format($('#' + total_).val()));
            } else {
                $('#' + PaidAmtByCust).val('0');
            }

            hitunganPaidCust();
        }
        function hitunganPaidCust() {
            var totaldiscount = 0;
            var totalpenambahan = 0;
            var totalbayarcust = 0;
            var totalpremisetelahdiscount = 0;
            var totalpremisetelahpenambahan = 0;

            var grd = document.getElementById("Dgrid2InsurancePaid");
            var row = grd.rows.length - 1;

            $('#Dgrid2InsurancePaid').find('tr').each(function (i, el) {
                if (i > 0) {
                    var $tds = $(this).find('td');
                    var discount = $tds[2].getElementsByTagName('input')[0];
                    var plusminus = $tds[2].getElementsByTagName('select')[0];
                    var premisetelahdiscount = $tds[3].getElementsByTagName('input')[0];
                    var bayarcust = $tds[4].getElementsByTagName('input')[0];

                    var valdiscount = discount.value;
                    var valpremisetelahdiscount = premisetelahdiscount.value;
                    var valbayarcust = bayarcust.value;
                    var valplusminus = plusminus.value;

                    if (valplusminus == "-") {
                        totaldiscount = parseInt(totaldiscount) + parseInt(valdiscount.replace(/\s*,\s*/g, ''));
                    } else if (valplusminus == "+") {
                        totalpenambahan = parseInt(totalpenambahan) + parseInt(valdiscount.replace(/\s*,\s*/g, ''));
                    }

                    totalpremisetelahdiscount = parseInt(totalpremisetelahdiscount) + parseInt(valpremisetelahdiscount.replace(/\s*,\s*/g, ''));
                    totalbayarcust = parseInt(totalbayarcust) + parseInt(valbayarcust.replace(/\s*,\s*/g, ''));
                }
            });

            var bpolis = parseInt($("#txtBiayaPolis_txtNumber").val().replace(/\s*,\s*/g, ''));
            var bmaterai = parseInt($("#txtBiayaMaterai_txtNumber").val().replace(/\s*,\s*/g, ''));
            
            $('#<%=lbldiscount.ClientID %>').val(number_format(totaldiscount, 0));
            $('#<%=lblPenambahan.ClientID %>').val(number_format(totalpenambahan, 0));
            
            var premi = parseInt($('#<%=lblPremi.ClientID %>').text().replace(/\s*,\s*/g, ''));

            $('#<%=txtTotalPremi.ClientID %>').val(number_format(premi + totalpenambahan ));

            $('#<%=lblPremiDiscount.ClientID %>').val(number_format(premi + totalpenambahan - totaldiscount));

            var setelahDisCount = premi + totalpenambahan - totaldiscount;

            if (totalbayarcust > 0) {
                $('#<%=lblBayarTunai.ClientID %>').val(number_format(totalbayarcust + bpolis + bmaterai, 0));
                $('#<%=lblPremiDiKredit.ClientID %>').val(number_format(setelahDisCount - totalbayarcust , 0));
            } else {
                $('#<%=lblBayarTunai.ClientID %>').val('0');
                $('#<%=lblPremiDiKredit.ClientID %>').val(number_format(setelahDisCount - totalbayarcust + bpolis + bmaterai, 0));
            }
            
        }
    </script>
</head>
<body>
   <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="up1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="sm1">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
        
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        ENTRI DATA ASURANSI OPERATING LEASE</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Jenis Asset
                        </label>
                        <asp:Label ID="LblInsuranceType" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="lblInsuranceAssetType" runat="server">lblInsuranceAssetType</asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Penggunaan Asset
                        </label>
                        <asp:Label ID="LblAssetUsageDescr" runat="server">LblAssetUsageDescr</asp:Label>
                        <asp:Label ID="LblAssetUsageID" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="LblManufacturingYear" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Asset (New/Used)
                        </label>
                        <asp:Label ID="lblAssetNewused" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Cover Asuransi Dilakukan Oleh
                        </label>
                        <asp:Label ID="LblInsuredBy" runat="server">LblInsuredBy</asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box_hide">
                <div>
                    <%--<div class="form_left">
                        <label>
                            Premi Dibayar Oleh
                        </label>
                        <asp:Label ID="LblPaidBy" runat="server">LblPaidBy</asp:Label>
                    </div>--%>
                    <div class="form_box_hide">
                        <label>
                            Jenis Aplikasi
                        </label>
                        <asp:Label ID="LblApplicationType" runat="server"></asp:Label>
                        <uc1:ucapplicationtype id="oApplicationType" runat="server"></uc1:ucapplicationtype>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Jangka Waktu Asuransi
                        </label>
                        <asp:TextBox ID="TxtTenorCredit" runat="server" CssClass="smaller_text"></asp:TextBox><label
                            class="label_unit ">Bulan</label>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic"
                            ControlToValidate="TxtTenorCredit" ErrorMessage="Harap isi Jangka Waktu Pembiayaan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgvTenorCredit" runat="server" ControlToValidate="TxtTenorCredit"
                            ErrorMessage="RangeValidator" MaximumValue="120" MinimumValue="0" Type="Integer"
                            Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>
                    </div>
                    <div class="form_right">
                        <label>
                            Premi Dibayar Oleh
                        </label>
                        <asp:Label ID="LblPaidBy" runat="server">LblPaidBy</asp:Label>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Harga OTR
                        </label>
                        <asp:Label runat="server" ID="lblOTR" CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis Aksesoris
                        </label>
                        <asp:TextBox runat="server" ID="txtJenisAksesoris" CssClass="long_text"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nilai Aksesoris
                        </label>
                        <uc4:ucnumberformat id="txtNilaiAksesoris" runat="server" />
                    </div>
                   
                      <div class="form_right">
                        <label>
                            Wilayah Asuransi
                        </label>
                        <asp:Label runat="server" ID="lblWilayahAsuransi"/>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nilai Pertanggungan
                        </label>
                        <asp:Label ID="lblAmountCoverage" runat="server" CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                     <div class="form_right">
                        <label>
                            Tahun Asset
                        </label>
                        <asp:Label runat="server" ID="lblTahunKendaraan"/>
                    </div>
                </div>
            </div>
            <div class="form_box_hide" >
                <div>
                    <div class="form_left">
                        <div style="float:left">
                        <label>Perluasan Personal Accident</label>
                        </div>
                        <div>
                           <asp:RadioButtonList runat="server" ID="rdoPerluasanPersolanAccident" RepeatDirection="Horizontal"
                            CssClass="opt_single rboRefinancing">
                            <asp:ListItem  Value="True" Text="Ya"></asp:ListItem>
                            <asp:ListItem Value="False" Text="Tidak" Selected="True" ></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form_right">
                       
                    </div>
                </div>
            </div>
            
            <div class="form_button">
                <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="small button green"></asp:Button>
            </div>

            <asp:Panel ID="pnlTipeAsuransi" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            TIPE ASURANSI</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Perusahaan Asuransi
                        </label>
                        <uc4:ucinsurancebranchname runat="server" id="cmbInsuranceComBranch"></uc4:ucinsurancebranchname>
                        <asp:Label ID="LblFocusMaskAssID" runat="server"></asp:Label>
                        <asp:Label ID="LblFocusInsuranceComBranchName" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgTipeAsuransi" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="THN">
                                        <HeaderStyle CssClass=""></HeaderStyle>
                                        <ItemStyle CssClass=""></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="LblYearInsurance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Tahun") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JENIS COVER">
                                        <HeaderStyle CssClass=""></HeaderStyle>
                                        <ItemStyle CssClass=""></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="LblHiddenCoverageTypeDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Tipe") %>'>
                                            </asp:Label>
                                            <uc4:uccoveragetypeapk runat="server" id="DDLCoverageTypeDgrid"   />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green"></asp:Button>
                </div>
            </asp:Panel>

            <asp:Label ID="LblMinimumTenor" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblMaximumTenor" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblBranchID" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblTenorCredit" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblJmlGrid" runat="server" Visible="False"></asp:Label>
            <asp:Panel ID="PnlGrid" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            RATE ASURANSI & PERLUASAN</h4>
                    </div>
                </div>
                
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="DgridInsurance" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                                BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid"></ItemStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="THN">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="LblYearInsurance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearInsurance") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="RATE TYPE">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="LblHiddenCoverageTypeRate" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.MasterRateID") %>'>
                                            </asp:Label>
                                            <asp:Label ID="LblHiddenCoverageTypeDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CoverageType") %>'>
                                            </asp:Label>
                                            <uc4:ucCoverageTypeRate runat="server" id="DDLCoverageTypeRate"  />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TPL">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <HeaderTemplate>
                                            TPL
                                            <asp:DropDownList runat="server" ID="DDLBolTPLOptionHeader" AutoPostBack="true" OnTextChanged="DDLBolOptionHeader_OnTextChanged">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DDLBolTPLOption" runat="server">
                                                <asp:ListItem Value="0" Text="No" />
                                                 <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NILAI TPL">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <ItemTemplate>
                                            <uc4:ucnumberformat runat="server" id="txtNilaiTPL" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="FLOOD">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <HeaderTemplate>
                                            FLOOD
                                            <asp:DropDownList runat="server" ID="DDLBolFloodOptionHeader" AutoPostBack="true"
                                                OnTextChanged="DDLBolOptionHeader_OnTextChanged">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DDLBolFloodOption" runat="server">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="EQVET">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <HeaderTemplate>
                                            EQVET
                                            <asp:DropDownList runat="server" ID="DDLBolIQVETOptionHeader" AutoPostBack="true"
                                                OnTextChanged="DDLBolOptionHeader_OnTextChanged">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DDLBolIQVETOption" runat="server">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="SRCC">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <HeaderTemplate>
                                            SRCC
                                            <asp:DropDownList runat="server" ID="DDLBolSRCCOptionHeader" AutoPostBack="true"
                                                OnTextChanged="DDLBolOptionHeader_OnTextChanged">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DDLBolSRCCOption" runat="server">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TERRORISME">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <HeaderTemplate>
                                            TERRORISME
                                            <asp:DropDownList runat="server" ID="DDLBolTERROROptionHeader" AutoPostBack="true"
                                                OnTextChanged="DDLBolOptionHeader_OnTextChanged">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DDLBolTERROROption" runat="server">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <HeaderTemplate>
                                            PA
                                            <asp:DropDownList runat="server" ID="DDLBolPAPAXOptionHeader" AutoPostBack="true"
                                                OnTextChanged="DDLBolOptionHeader_OnTextChanged">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DDLBolPAPAXOption" runat="server">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NILAI PA">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <ItemTemplate>
                                            <uc4:ucnumberformat runat="server" id="txtNilaiPA" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <HeaderTemplate>
                                            PA DRIVER
                                            <asp:DropDownList runat="server" ID="DDLBolPADRIVEROptionHeader" AutoPostBack="true"
                                                OnTextChanged="DDLBolOptionHeader_OnTextChanged">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="DDLBolPADRIVEROption" runat="server">
                                                <asp:ListItem Value="0" Text="No" />
                                                <asp:ListItem Value="1" Text="Yes" />
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NILAI PA DRIVER">
                                        <HeaderStyle CssClass="auto_width"></HeaderStyle>
                                        <ItemStyle CssClass="auto_width"></ItemStyle>
                                        <ItemTemplate>
                                            <uc4:ucnumberformat runat="server" id="txtNilaiPADriver" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnPreview" Text="Proses" CssClass="small button blue" runat="server"
                        CausesValidation="true"></asp:Button>
                        <asp:Button ID="btnReset" Text="Reset" CssClass="small button gray" runat="server"
                    CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>          

            <asp:UpdatePanel ID="upSave" runat="server">
            <ContentTemplate>
                        
            <asp:Panel runat="server" ID="pnlInsSelection">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PREMI ASURANSI & PERBANDINGANNYA
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="gridInsco" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                BorderWidth="0px" CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="MaskAssBranchID"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn Visible="False" HeaderText="INSURANCE COY ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMaskAssID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="InsuranceComBranchID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInsuranceComBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssBranchID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MaskAssBranchNAME" HeaderText="CABANG ASURANSI">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PREMI UTAMA">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TPL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SrccPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING FEE">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA PASS">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA ADMIN">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridAdminFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AdminFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA MATERAI">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMeteraiFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StampdutyFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TOTAL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridGrandTotal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="gridDetailInsco" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="YearNum" CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="YEAR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDYearNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JENIS COVER">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDCoverage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI UTAMA">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TPL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING FEE">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFeeToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA PASS">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <h4>
                            BAYAR ASURANSI</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <asp:DataGrid ID="Dgrid2InsurancePaid" runat="server" AutoGenerateColumns="False"
                            BorderStyle="None" BorderWidth="0px" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="TAHUN">
                                    <HeaderStyle Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblYearNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DIBAYAR OLEH CUST">
                                    <ItemTemplate>
                                        <asp:TextBox style="display:none" ID="txtMainPremiumToCust" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust") %>' ></asp:TextBox>
                                        <asp:TextBox style="display:none" ID="txtPaidByCust" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaidByCust") %>'>
                                        </asp:TextBox>
                                        <asp:DropDownList ID="cboPaidByCust" runat="server" Width="150px">
                                            <asp:ListItem Value="PAID">PAID</asp:ListItem>
                                            <asp:ListItem Value="PDC">PDC</asp:ListItem>
                                            <asp:ListItem Value="ONLOAN">ON LOAN</asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PENAMBAHAN/DISC PREMI">
                                    <HeaderStyle Width="25%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cboPlusDisc" runat="server" Width="130px">
                                            <asp:ListItem Value="">Pilih</asp:ListItem>
                                            <asp:ListItem Value="+">Penambahan</asp:ListItem>
                                            <asp:ListItem Value="-">Pengurangan</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:textbox ID="txtDiscToCust" runat="server" Text='0' Width="150px" Enabled = "false"
                                        onblur="extractNumber(this,2,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);"
                                        onkeyup="extractNumber(this,2,true);" onkeypress="return blockNonNumbers(this, event, true, true);"
                                        onfocus="this.value=resetNumber(this.value);" CssClass="numberAlign" OnChange="javascript:void(0)" autocomplete="off">
                                        </asp:textbox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI SETELAH PENAMBAHAN/DISC">
                                    <HeaderStyle Width="25%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:textbox  Enabled="false" ID="txtTotal" runat="server" CssClass="numberAlign" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust", "{0:###,###,##0}") %>'>
                                        </asp:textbox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR CUST">
                                    <HeaderStyle Width="25%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:textbox Enabled="false" ID="lblPaidAmtByCust" runat="server" CssClass="numberAlign" Text='0'>
                                        </asp:textbox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div> 
                <asp:Panel runat="server" ID="pnlPaidHPP">
                    <div class="form_box_title">
                        <div class="form_single">
                            <h4>
                                DETAIL PREMI CUSTOMER</h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <div class="div_label">
                                    <label class="label_auto">
                                        Premi
                                    </label>
                                    <uc4:ucnumberformat id="txtRate" runat="server" visible="false" />
                                </div>
                                <uc4:ucnumberformat id="txtAmountRate" runat="server" visible="false"/>    
                                <asp:label ID="lblPremi" runat="server" CssClass="numberAlign regular_text">0</asp:label>                            
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left border_sum">
                                    <label>
                                        Penambahan
                                    </label>
                                     <label class="label_calc">
                                        +
                                    </label>

                                 <asp:textbox ID="lblPenambahan" Enabled="false" runat="server" CssClass="numberAlign regular_text">0</asp:textbox>                   
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left ">
                                <div class="div_label">
                                    <label class="label_auto">
                                        Premi setelah Penambahan
                                    </label>
                                    
                                </div>
                                    <asp:textbox ID="txtTotalPremi" Enabled="false" runat="server" CssClass="numberAlign regular_text">0</asp:textbox>                           
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left border_sum">
                                    <label>
                                        Discount
                                    </label>
                                     <label class="label_calc">
                                        -
                                    </label>

                                 <asp:textbox ID="lbldiscount" Enabled="false" runat="server" CssClass="numberAlign regular_text">0</asp:textbox>                   
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                     <div class="form_box">
                        <div>
                            <div class="form_left ">
                                <div class="div_label">
                                    <label class="label_auto">
                                        Premi setelah Penambahan/Discount
                                    </label>
                                    
                                </div>
                                    <asp:textbox ID="lblPremiDiscount" Enabled="false" runat="server" CssClass="numberAlign regular_text">0</asp:textbox>                           
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                     <div class="form_box">
                        <div>
                            <div class="form_left ">
                                <div class="div_label">
                                    <label class="label_auto">
                                        Biaya Polis
                                    </label>
                                </div>
                                    <uc4:ucnumberformat id="txtBiayaPolis" runat="server" visible="true" TextCssClass="numberAlign regular_text"  />
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left ">
                                <div class="div_label">
                                    <label class="label_auto">
                                        Biaya Materai
                                    </label>
                                </div>
                                    <uc4:ucnumberformat id="txtBiayaMaterai" runat="server" visible="true" TextCssClass="numberAlign regular_text" />
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left border_sum">
                                <label>
                                    Premi di Bayar
                                </label>
                                
                                <label class="label_calc">
                                    -
                                </label>
                                <uc4:ucnumberformat id="txtBayarTunai" runat="server" visible="false" />
                                <asp:textbox runat="server" Enabled="false" ID="lblBayarTunai" CssClass="numberAlign regular_text">0</asp:textbox>
                            </div>
                            <div class="form_right">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    Premi di Pembiayaan
                                </label>
                                <asp:textbox runat="server" Enabled="false" ID="lblPremiDiKredit" CssClass="numberAlign regular_text"></asp:textbox>
                            </div>
                            <div class="form_right">
                                <label>Ditambahkan Ke</label>
                                <asp:DropDownList ID="cboAppandTo" runat="server" Enabled ="false">
                                    <asp:ListItem Value="PH">POKOK HUTANG</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_single">
                                <label class="label_general">
                                    Catatan Asuransi
                                </label>
                                <asp:TextBox ID="TxtInsNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                <div class="form_button">
                    <asp:Button ID="btnSaveInscoSelec" Text="Save" CssClass="small button blue" runat="server">
                    </asp:Button>
                    <asp:Button ID="imbCancelInscoSelec" Text="Cancel" CssClass="small button gray" runat="server"
                        CausesValidation="False"></asp:Button>
                </div>
                </asp:Panel>
            </asp:Panel>

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSaveInscoSelec" EventName="Click" />
            </Triggers>
            </asp:UpdatePanel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
