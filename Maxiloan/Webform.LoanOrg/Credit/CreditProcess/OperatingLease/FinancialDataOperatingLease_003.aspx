﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FinancialDataOperatingLease_003.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.FinancialDataOperatingLease_003" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationTabOL.ascx" TagName="ucApplicationTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Financial Data Operating Lease</title>
    <link rel="Stylesheet" href="../../../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../../../Include/Lookup.css" type="text/css" />
    
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    
    <link href="../../../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	<script src="../../../../js/jquery-1.9.1.js" type="text/javascript"></script>
	<script src="../../../../js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    
    <script language="javascript" type="text/javascript">
      
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }

        function NumInst(Tenor, Num, txtNumInst) {
            var newtenor = Tenor / Num;
            $('#' + txtNumInst).val(newtenor);
        }

        function fInsured() {
            var cboInsured = document.forms[0].cboInsuredBy.options[document.forms[0].cboInsuredBy.selectedIndex].value
            if (cboInsured == 'CU') {
                document.forms[0].cboPaidBy.value = 'CU';
                document.forms[0].cboPaidBy.disabled = true;
            }
            else {
                document.forms[0].cboPaidBy.disabled = false;
            }
        }
        function GracePeriod(txtGracePeriod, cboGracePeriod, Value) {
            if (Value == 'AR') {
                document.forms[0].txtGracePeriod.value = '0'
                if (document.forms[0].txtGracePeriod.disabled == false) {
                    document.forms[0].txtGracePeriod.disabled = false;
                    document.forms[0].cboGracePeriod.disabled = false;
                }
            }
            else {
                document.forms[0].txtGracePeriod.value = '0'
                document.forms[0].txtGracePeriod.disabled = true;
                document.forms[0].cboGracePeriod.disabled = true;
            }
        }
     
       
        function recalculateARGross() {
            var totalbunga = $('#lblTotalBunga_txtNumber').val();
            var ntf = $('#lblNTF').html();
            var nilaikontrak = parseInt(totalbunga.replace(/\s*,\s*/g, '')) +
                                parseInt(ntf.replace(/\s*,\s*/g, ''));
            $('#lblNilaiKontrak_txtNumber').val(number_format(nilaikontrak));
        }
        
     
        
    </script>
</head>
<body>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <uc7:ucapplicationtab id="ucApplicationTab1" runat="server" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div>
                    <div class="form_left">
                        <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                    </div>
                    <div class="form_right">
                        <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        ENTRI FINANSIAL OPERATING LEASE</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req">
                            Angsuran Pertama
                        </label>
                        <asp:DropDownList ID="cboFirstInstallment" runat="server" AutoPostBack="false" Enabled="false">
                            <asp:ListItem Value="AD">Advance</asp:ListItem>
                            <asp:ListItem Value="AR" Selected="True">Arrear</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ControlToValidate="cboFirstInstallment" ErrorMessage="Harap Pilih Angsuran Pertama"
                            InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>  
                </div>
            </div>
            <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h5>
                            DATA FINANSIAL</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Asset Value
                        </label>
                        <asp:Label ID="lblOTR" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                   <div class="form_right" >
                        <label>
                            Basic Lease
                        </label>
                        <asp:Label ID="lblBasicLease" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_box_hide">
                        <label>
                            Karoseri
                        </label 
                        <asp:Label runat="server" ID="lblKaroseri" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_left">
                         <label>
                            RV Estimate
                        </label>
                        <asp:Label runat="server" ID="lblRVEstimate" CssClass="numberAlign2 regular_text"></asp:Label> 
                    </div>
                    <div class="form_right" >
                        <label>
                            Insurance
                        </label>
                        <asp:Label ID="lblInsurance" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                         <label>
                            RV Interest
                        </label>
                        <asp:Label runat="server" ID="lblRVInterest" CssClass="numberAlign2 regular_text"></asp:Label> 
                    </div>
                </div>
                <div class="form_right" >
                        <label>
                            Maintenance
                        </label>
                        <uc4:ucnumberformat id="ucMaintenance" runat="server" TextCssClass="numberAlign2" />    
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            PA
                        </label>
                        <asp:Label runat="server" ID="lblPA" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right" >
                        <label>
                            STNK
                        </label>
                        <uc4:ucnumberformat id="ucSTNK" runat="server" TextCssClass="numberAlign2" />    
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_box_hide">
                        <label class="label_auto">
                            Uang Muka
                        </label>
                    <uc4:ucnumberformat id="txtDP" runat="server" TextCssClass="numberAlign2" />    
                        <label class="label_auto numberAlign2"> %
                        </label>
                    <uc4:ucnumberformat id="txtDPPersen" runat="server" TextCssClass="numberAlign2" />
                </div>
                <div class="form_left" >
                </div>
                <div class="form_right" >
                        <label>
                            Admin Fee
                        </label>
                        <uc4:ucnumberformat id="ucAdminFee" runat="server" TextCssClass="numberAlign2" />    
                    </div>
            </div>
            <div class="form_box">
                <div class="form_left border_sum">
                    <label class="label_calc2">
                        +
                    </label>
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <asp:Label ID="lblAssetInsurance3" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                  <div class="form_right border_sum" >
                  <label class="label_calc2">
                        +
                        </label>
                        <label>
                            Provisi
                        </label>
                        <uc4:ucnumberformat id="ucProvisi" runat="server" TextCssClass="numberAlign2" />    
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_box_hide">
                        <label>
                            Pokok Hutang (NTF)
                        </label>
                        <asp:Label ID="lblNTF" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_left">
                        <label class="label_req">Suku Bunga</label>
                        <label class="label_auto numberAlign2"> %
                        </label>
                        <uc4:ucnumberformat id ="txtSukuBunga" runat="server" Width="100" TextCssClass="numberAlign2 regular_text"/> 
                    </div>
                    <div class="form_right" >
                        <label>
                            Total Monthly Lease
                        </label>
                        <asp:Label ID="lblMonthlyLease" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left" >
                        <label>
                            Expected Interest
                        </label>
                        <asp:Label ID="lblExpectedInterest" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
             </div>            
            <div class="form_box">
                <div>               
                     <div class="form_left">
                        <label class="label_req">
                        Jangka Waktu
                        </label>
                        <uc4:ucnumberformat id ="txtNumInst" runat="server" Width="200" TextCssClass="numberAlign2 smaller_text"/>
                     </div>
                </div>
            </div>
            
        <asp:Panel ID="uPanelSave" runat="server">
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Process & Save" CssClass="small button blue"
                    CausesValidation="True"></asp:Button>
                <div id="btnSaveEnable" class="small button gray" style="display:none">Save</div>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>    
        </asp:Panel>

        </ContentTemplate>
         <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="cboFirstInstallment" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>

    </form>
</body>
</html>
