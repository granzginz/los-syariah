﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewFinancialDataOperatingLease_003.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewFinancialDataOperatingLease_003" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="../../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc1" %>
<%@ Register Src="../../../../webform.UserController/ucApplicationViewTabOperatingLease.ascx" TagName="ucApplicationViewTab"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>View Financial Data 1</title>
    <link rel="Stylesheet" type="text/css" href="../../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../../Include/Buttons.css" />
    <link rel="stylesheet" type="text/css" href="../../../../include/ui-lightness/jquery-ui-1.10.3.custom.css"  />
    <script src="../../../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" class="tab_container_form">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc1:ucapplicationviewtab id="ucApplicationTab1" runat="server" />
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        VIEW APLIKASI</h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <uc1:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc1:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                <div class="title_strip"></div>
                <div class="form_single">
                    <h4>
                        DATA FINANSIAL 1</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="">
                            Angsuran Pertama
                        </label>
                        <asp:Label ID="lblFirstInstallment" runat="server"></asp:Label>
                    </div>  
                    <div class="form_right">
                        
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h5>
                            DATA FINANSIAL</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Asset Value
                        </label>
                        <asp:Label ID="lblOTR" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Basic Lease
                        </label>
                        <asp:Label runat="server" ID="lblBasicLease" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_box_hide">
                        <label>
                            Karoseri
                        </label 
                        <asp:Label runat="server" ID="lblKaroseri" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_left">
                         <label>
                            RV Estimate
                        </label>
                        <asp:Label runat="server" ID="lblRVEstimate" CssClass="numberAlign2 regular_text"></asp:Label> 
                    </div>
                    <div class="form_right" >
                        <label>
                            Insurance
                        </label>
                        <asp:Label ID="lblInsurance" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                         <label>
                            RV Interest
                        </label>
                        <asp:Label runat="server" ID="lblRVInterest" CssClass="numberAlign2 regular_text"></asp:Label> 
                    </div>
                </div>
                <div class="form_right" >
                        <label>
                            Maintenance
                        </label>
                        <asp:Label runat="server" ID="ucMaintenance" CssClass="numberAlign2 regular_text"></asp:Label> 
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            PA
                        </label>
                        <asp:Label runat="server" ID="lblPA" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right" >
                        <label>
                            STNK
                        </label>
                        <asp:Label runat="server" ID="ucSTNK" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_box_hide">
                        <label class="label_auto">
                            Uang Muka
                        </label>
                        <asp:Label runat="server" ID="txtDP" CssClass="numberAlign2 regular_text"></asp:Label>
                        <label class="label_auto numberAlign2"> %
                        </label>
                        <asp:Label runat="server" ID="txtDPPersen" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_left" >
                </div>
                <div class="form_right" >
                        <label>
                            Admin Fee
                        </label>
                        <asp:Label runat="server" ID="ucAdminFee" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
            </div>
            <div class="form_box">
                <div class="form_left border_sum">
                    <label class="label_calc2">
                        +
                    </label>
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <asp:Label ID="lblAssetInsurance3" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                  <div class="form_right border_sum" >
                  <label class="label_calc2">
                        +
                        </label>
                        <label>
                            Provisi
                        </label>
                        <asp:Label runat="server" ID="ucProvisi" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_box_hide">
                        <label>
                            Pokok Hutang (NTF)
                        </label>
                        <asp:Label ID="lblNTF" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_left">
                        <label class="label_req">Suku Bunga</label>
                        <label class="label_auto numberAlign2"> %
                        </label>
                        <asp:Label runat="server" ID="txtSukuBunga" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right" >
                        <label>
                            Total Monthly Lease
                        </label>
                        <asp:Label ID="lblMonthlyLease" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left" >
                        <label>
                            Expected Interest
                        </label>
                        <asp:Label ID="lblExpectedInterest" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
             </div>            
            <div class="form_box">
                <div>               
                     <div class="form_left">
                        <label class="label_req">
                        Jangka Waktu
                        </label>
                        <asp:Label ID="txtNumInst" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                     </div>
                </div>
            </div

                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            Amortisasi</h4>
                    </div>
                </div>
                <div class="form_box" >
                <div>
                    <div class="form_single">
                        <asp:DataGrid id="dtgViewInstallment" runat="server" CssClass="grid_general" Width="100%" AllowSorting="True"
						    AutoGenerateColumns="False" BorderWidth="0px" cellpadding="3" cellspacing="1">
						    <ItemStyle HorizontalAlign="Right" CssClass="item_grid number_align"></ItemStyle>
						    <HeaderStyle HorizontalAlign="Right" CssClass="th"></HeaderStyle>
						    <Columns>
                                <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InstallmentAmount" HeaderText="ANGSURAN" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="PrincipalAmount" HeaderText="POKOK" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InterestAmount" HeaderText="BUNGA" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OutstandingPrincipal" HeaderText="SISA POKOK" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OutstandingInterest" HeaderText="SISA BUNGA" DataFormatString="{0:###,###,##.#0;;0.00}"
                                    ItemStyle-CssClass="item_grid_right" HeaderStyle-CssClass="th_right"></asp:BoundColumn>
                            </Columns>
					    </asp:DataGrid>
                    </div>
                </div>
            </div>
    
            <div class="tab_container_form_space"><br />
            </div>
            <div class="tab_container_button">
                <div class="form_button">
                    <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                        Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    </form>
</body>
</html>
