﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ViewAssetDataOperatingLease

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''ScriptManager1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ScriptManager1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''upnlSupplier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents upnlSupplier As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtSupplierName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSupplierName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtAssetName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAssetName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtYear As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ucOTR control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucOTR As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblKondisiAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblKondisiAsset As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cboKondisiAsset control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboKondisiAsset As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtDPPersen control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDPPersen As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtDP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDP As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblAssetGrade control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAssetGrade As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cboAssetGrade control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboAssetGrade As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblTotalPembiayaan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalPembiayaan As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblUangMukaBayar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUangMukaBayar As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cboUangMukaBayar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboUangMukaBayar As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblPHJMB control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPHJMB As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblUsage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUsage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOTRPembanding control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOTRPembanding As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPencairanKe control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPencairanKe As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cboPencairanKe control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboPencairanKe As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''dtgAttribute control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgAttribute As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''lblSerial1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSerial1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtSerial1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSerial1 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSerial2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSerial2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtSerial2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSerial2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rboNamaBPKBSamaKontrak control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rboNamaBPKBSamaKontrak As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''rboBPKBPengganti control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rboBPKBPengganti As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rboBPKBanBadanUsaha control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rboBPKBanBadanUsaha As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''rboIjinTrayek control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rboIjinTrayek As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblRegAddress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegAddress As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRegRT control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegRT As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRegRW control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegRW As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRegKelurahan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegKelurahan As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRegKecamatan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegKecamatan As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRegCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegCity As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRegZip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRegZip As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtAssetNotes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAssetNotes As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''divregstnk control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divregstnk As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''lblTanggalSTNK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTanggalSTNK As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''txtTanggalSTNK control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTanggalSTNK As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInsuredBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInsuredBy As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cboInsuredBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboInsuredBy As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblPaidBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPaidBy As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cboPaidBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cboPaidBy As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblcmo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblcmo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblSalesman control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSalesman As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dtgAssetDoc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtgAssetDoc As Global.System.Web.UI.WebControls.DataGrid

    '''<summary>
    '''imgTampakDepan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgTampakDepan As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''imgTampakBelakang control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgTampakBelakang As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''imgTampakKiri control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgTampakKiri As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''imgTampakKanan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgTampakKanan As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''imgTampakDashboard control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents imgTampakDashboard As Global.System.Web.UI.WebControls.Image

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button
End Class
