﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.Drawing
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.General.CommonCacheHelper
Imports AjaxControlToolkit

Imports System.IO
Imports System.Collections.Generic


#End Region

Public Class ViewAssetDataOperatingLease
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents UCAddress As ucAddressCity
    Protected WithEvents ucApplicationTab1 As ucApplicationViewTabOperatingLease
#End Region

#Region "Constanta"
    Private m_controller As New AssetDataController
    Dim pathFolder As String = ""
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
#End Region

#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID")
            Me.CustomerID = Request("CustID")
            InitObjects()
            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .CustomerID = Me.CustomerID
                .bindData()
                .initControls("OnNewApplication")
            End With
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            Bindgrid()
            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Asset")
            ucApplicationTab1.setLink()
        End If

    End Sub
#End Region

#Region "Initial Objects"
    Private Sub InitObjects()
        FillCbo(cboInsuredBy, "tblInsuredBy")
        FillCbo(cboPaidBy, "tblPaidBy")
        FillCboGrade()
        lblMessage.Text = ""
        lblMessage.Visible = False
    End Sub

    Sub Bindgrid()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oDataAttribute As New DataTable
        Dim oDataAssetDoc As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.AppID = Me.ApplicationID
        oAssetData = m_controller.GetViewAssetData(oAssetData)

        If Not oAssetData Is Nothing Then
            oData = oAssetData.ListData
            oDataAttribute = oAssetData.DataAttribute
            oDataAssetDoc = oAssetData.DataAssetdoc
        End If
        If oAssetData.Output <> "" Then
            lblMessage.Text = oAssetData.Output
            Exit Sub
        End If
        If oData.Rows.Count > 0 Then
            SetValueToLabel(oData)
        End If
        dtgAttribute.DataSource = oDataAttribute.DefaultView
        dtgAttribute.DataBind()

        dtgAssetDoc.DataSource = oDataAssetDoc.DefaultView
        dtgAssetDoc.DataBind()
    End Sub
#End Region

    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)

        txtSupplierName.Text = oRow.Item("SupplierName").ToString
        txtAssetName.Text = oRow.Item("Description").ToString
        txtYear.Text = oRow.Item("ManufacturingYear").ToString
        ucOTR.Text = FormatNumber(oRow.Item("TotalOTR"), 0)
        cboUangMukaBayar.SelectedIndex = cboUangMukaBayar.Items.IndexOf(cboUangMukaBayar.Items.FindByValue(oRow.Item("UangMukaBayarDi").ToString))
        lblUangMukaBayar.Text = cboUangMukaBayar.SelectedItem.Text
        cboPencairanKe.SelectedIndex = cboPencairanKe.Items.IndexOf(cboPencairanKe.Items.FindByValue(oRow.Item("PencairanKe").ToString))
        lblPencairanKe.Text = cboPencairanKe.SelectedItem.Text
        lblSerial1.Text = oRow.Item("SerialNo1Label").ToString
        lblSerial2.Text = oRow.Item("SerialNo2Label").ToString
        txtSerial1.Text = oRow.Item("SerialNo1").ToString
        txtSerial2.Text = oRow.Item("SerialNo2").ToString
        rboNamaBPKBSamaKontrak.Items.FindByValue(oRow.Item("NamaBPKBSama").ToString).Selected = True
        rboBPKBPengganti.Items.FindByValue(oRow.Item("isBPKBPengganti").ToString).Selected = True
        rboIjinTrayek.Items.FindByValue(oRow.Item("IsIzinTrayek").ToString).Selected = True

        lblUsage.Text = oRow.Item("usage").ToString
        cboKondisiAsset.SelectedIndex = cboKondisiAsset.Items.IndexOf(cboKondisiAsset.Items.FindByValue(oRow.Item("KondisiAsset").ToString))
        lblKondisiAsset.Text = cboKondisiAsset.SelectedItem.Text
        If oRow.Item("TaxDate").ToString <> "" Then
            txtTanggalSTNK.Text = Format(oRow.Item("TaxDate"), "dd/MM/yyyy")
        Else
            txtTanggalSTNK.Text = ""
        End If
        txtAssetNotes.Text = oRow.Item("Notes").ToString
        cboInsuredBy.SelectedIndex = cboInsuredBy.Items.IndexOf(cboInsuredBy.Items.FindByValue(oRow.Item("InsAssetInsuredBy").ToString))
        lblInsuredBy.Text = cboInsuredBy.SelectedItem.Text
        cboPaidBy.SelectedIndex = cboPaidBy.Items.IndexOf(cboPaidBy.Items.FindByValue(oRow.Item("InsAssetPaidBy").ToString))
        lblPaidBy.Text = cboPaidBy.SelectedItem.Text
        cboAssetGrade.SelectedIndex = cboAssetGrade.Items.IndexOf(cboAssetGrade.Items.FindByValue(oRow.Item("GradeCode").ToString))
        lblAssetGrade.Text = cboAssetGrade.SelectedItem.Text

        lblRegAddress.Text = oRow.Item("OldOwnerAddress").ToString.Trim
        lblRegRT.Text = oRow.Item("OldOwnerRT").ToString.Trim
        lblRegRW.Text = oRow.Item("OldOwnerRW").ToString.Trim
        lblRegKelurahan.Text = oRow.Item("OldOwnerKelurahan").ToString.Trim
        lblRegKecamatan.Text = oRow.Item("OldOwnerKecamatan").ToString.Trim
        lblRegCity.Text = oRow.Item("OldOwnerCity").ToString.Trim
        lblRegZip.Text = oRow.Item("OldOwnerZipCode").ToString.Trim
        txtName.Text = oRow.Item("ownerasset").ToString
        lblSalesman.Text = oRow.Item("Salesman").ToString.Trim
        lblcmo.Text = oRow.Item("AO").ToString.Trim

        'Dim DownPayment As Double = CDbl(oRow.Item("DownPayment"))

        'If DownPayment > 0 Then
        '    txtDP.Text = FormatNumber(DownPayment, 0)
        '    txtDPPersen.Text = FormatNumber(DownPayment / CDbl(ucOTR.Text) * 100)
        'Else
        '    Dim totaldp As Double = CDbl(oRow.Item("TotalOTR")) * CDbl(txtDPPersen.Text) / 100
        '    txtDP.Text = FormatNumber(totaldp, 0)
        'End If
        lblTotalPembiayaan.Text = FormatNumber(CStr(CDbl(oRow.Item("TotalOTR"))), 0)

        If oRow.Item("KondisiAsset").trim = "U" Then
            Dim g As Double = CDbl(cboAssetGrade.SelectedValue)
            Dim txt As String = " &nbsp;&nbsp; (Grade " & cboAssetGrade.SelectedItem.Text & " = " & FormatNumber(g, 0) & "%" & "x" & ucOTR.Text & ")"
            lblOTRPembanding.Text = FormatNumber(CDbl(oRow.Item("TotalOTR")) * g / 100, 0) & txt
        Else
            lblOTRPembanding.Text = ""
        End If

        cekimage()

    End Sub

#Region "FillCbo"

    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub
    Sub FillCboGrade()
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData = m_controller.GetGradeAsset(oAssetData)
        oData = oAssetData.ListData
        cboAssetGrade.DataSource = oData
        cboAssetGrade.DataTextField = "GradeCode"
        cboAssetGrade.DataValueField = "GradeValue"
        cboAssetGrade.DataBind()
    End Sub
#End Region

    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Private Sub cekimage()
        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakDepan.jpg") Then
            imgTampakDepan.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakDepan.jpg"))
        Else
            imgTampakDepan.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakBelakang.jpg") Then
            imgTampakBelakang.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakBelakang.jpg"))
        Else
            imgTampakBelakang.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakKiri.jpg") Then
            imgTampakKiri.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakKiri.jpg"))
        Else
            imgTampakKiri.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakKanan.jpg") Then
            imgTampakKanan.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakKanan.jpg"))
        Else
            imgTampakKanan.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If

        If File.Exists(pathFile("Asset", Me.ApplicationID) + "TampakDashboard.jpg") Then
            imgTampakDashboard.ImageUrl = ResolveClientUrl(getDownloadPath(sesBranchId.Replace("'", "") & "/Asset/" + Me.ApplicationID + "/TampakDashboard.jpg"))
        Else
            imgTampakDashboard.ImageUrl = ResolveClientUrl("../../../../xml/NoFoto.png")
        End If


        imgTampakDepan.Height = 200
        imgTampakDepan.Width = 300
        imgTampakBelakang.Height = 200
        imgTampakBelakang.Width = 300
        imgTampakKiri.Height = 200
        imgTampakKiri.Width = 300
        imgTampakKanan.Height = 200
        imgTampakKanan.Width = 300
        imgTampakDashboard.Height = 200
        imgTampakDashboard.Width = 300

    End Sub

    Private Function pathFile(ByVal Group As String, ByVal ApplicationID As String) As String
        Dim strDirectory As String = ""
        'strDirectory = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\" & sesBranchId.Replace("'", "") & "\" & Group & "\" & ApplicationID & "\"
        strDirectory = getpathFolder() & sesBranchId.Replace("'", "") & "\" & Group & "\" & ApplicationID & "\"
        If Not System.IO.Directory.Exists(strDirectory) Then
            System.IO.Directory.CreateDirectory(strDirectory)
        End If
        Return strDirectory
    End Function

    Private Function getpathFolder() As String
        If pathFolder = "" Then
            Dim oClass As New Parameter.GeneralSetting
            Dim oCtr As New GeneralSettingController
            With oClass
                .strConnection = GetConnectionString()
                .GSID = "UploadFolder"
                .ModuleID = "GEN"
            End With
            Try
                Return oCtr.GetGeneralSettingValue(oClass).Trim
            Catch ex As Exception
                Return ""
            End Try
        Else
            Return pathFolder
        End If
    End Function
    Private Function getDownloadPath(filePath As String) As String
        Return Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd("/"c) + "/Webform.Utility/GetImage.aspx?file=" & Server.UrlEncode(getpathFolder() & filePath)
    End Function

End Class