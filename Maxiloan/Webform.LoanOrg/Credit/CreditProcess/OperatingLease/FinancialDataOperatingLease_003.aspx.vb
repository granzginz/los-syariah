﻿Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Imports System.Data
Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.Services


Public Class FinancialDataOperatingLease_003
    Inherits Maxiloan.Webform.WebBased

#Region "Controls"
    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucApplicationTab1 As ucApplicationTabOL
    Protected WithEvents txtDP As ucNumberFormat
    Protected WithEvents txtNumInst As ucNumberFormat
    Protected WithEvents txtDPPersen As ucNumberFormat
    Protected WithEvents txtSukuBunga As ucNumberFormat

    Protected WithEvents ucMaintenance As ucNumberFormat
    Protected WithEvents ucSTNK As ucNumberFormat
    Protected WithEvents ucAdminFee As ucNumberFormat
    Protected WithEvents ucProvisi As ucNumberFormat
#End Region

#Region "Constanta"
    Private Const style As String = "ACCACQ"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim RunRate As Double
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim m_controller As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController
    Private oController As New ApplicationDetailTransactionController

    Private TotalInsGrid As Double

    Private TotalBungaNett As Decimal
    Private TotalPencairan As Double
    Private TotalBungaNettAmount As Double

#End Region

#Region "Property"
    Property Status() As Boolean
        Get
            Return CType(ViewState("Status"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("Status") = Value
        End Set
    End Property
    Property Installment() As Double
        Get
            Return CDbl(ViewState("Installment"))
        End Get
        Set(ByVal Value As Double)
            ViewState("Installment") = Value
        End Set
    End Property
    Property DiffRate() As Double
        Get
            Return CDbl(ViewState("DiffRate"))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiffRate") = Value
        End Set
    End Property
    Property PrincipleAmount() As Double
        Get
            Return CDbl(ViewState("PrincipleAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("PrincipleAmount") = Value
        End Set
    End Property
    Property NTFGrossYield() As Double
        Get
            Return CDbl(ViewState("NTFGrossYield"))
        End Get
        Set(ByVal Value As Double)
            ViewState("NTFGrossYield") = Value
        End Set
    End Property
    Property RejectMinimumIncome() As Double
        Get
            Return CDbl(ViewState("RejectMinimumIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("RejectMinimumIncome") = Value
        End Set
    End Property
    Property First() As String
        Get
            Return ViewState("First").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("First") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property StepUpDownType() As String
        Get
            Return ViewState("StepUpDownType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("StepUpDownType") = Value
        End Set
    End Property
    Property Supplier() As String
        Get
            Return ViewState("Supplier").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Supplier") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustName() As String
        Get
            Return ViewState("CustName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustName") = Value
        End Set
    End Property
    Property InterestType() As String
        Get
            Return ViewState("InterestType").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InterestType") = Value
        End Set
    End Property
    Property InstallmentScheme() As String
        Get
            Return ViewState("InstallmentScheme").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InstallmentScheme") = Value
        End Set
    End Property
    Property IsIncentiveSupplier() As Boolean
        Get
            Return CType(ViewState("IsIncentiveSupplier"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("IsIncentiveSupplier") = Value
        End Set
    End Property
    Property RefundInterest As Double
        Get
            Return CType(ViewState("RefundInterest"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundInterest") = value
        End Set
    End Property
    Property AdminFee As Double
        Get
            Return CType(ViewState("AdminFee"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("AdminFee") = value
        End Set
    End Property
    Property Provisi As Double
        Get
            Return CType(ViewState("Provisi"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("Provisi") = value
        End Set
    End Property
    Private Property dtDistribusiNilaiInsentif As DataTable
        Get
            Return CType(ViewState("dtDistribusiNilaiInsentif"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("dtDistribusiNilaiInsentif") = value
        End Set
    End Property
    Private Property ApplicationDetailTransactionDT As DataTable
        Get
            Return CType(ViewState("ApplicationDetailTransactionDT"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("ApplicationDetailTransactionDT") = value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Private Property NilaiTransaksi() As Decimal
        Get
            Return CType(ViewState("NilaiTransaksi"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("NilaiTransaksi") = Value
        End Set
    End Property
    Private Property EffectiveRate() As Decimal
        Get
            Return CType(ViewState("EffectiveRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffectiveRate") = Value
        End Set
    End Property
    Private Property FlatRate() As Decimal
        Get
            Return CType(ViewState("FlatRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("FlatRate") = Value
        End Set
    End Property
    Private Property rvEffValue As String
        Get
            Return CType(ViewState("rvEffValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffValue") = value
        End Set
    End Property
    Private Property rvEffKey As String
        Get
            Return CType(ViewState("rvEffKey"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffKey") = value
        End Set
    End Property
    Private Property rvEffMinValue As String
        Get
            Return CType(ViewState("rvEffMinValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffMinValue") = value
        End Set
    End Property
    Private Property rvEffMaxValue As String
        Get
            Return CType(ViewState("rvEffMaxValue"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffMaxValue") = value
        End Set
    End Property
    Private Property rvEffErrorMsg As String
        Get
            Return CType(ViewState("rvEffErrorMsg"), String)
        End Get
        Set(ByVal value As String)
            ViewState("rvEffErrorMsg") = value
        End Set
    End Property
    Property RefundBungaAmount As Double
        Get
            Return CType(ViewState("RefundBungaAmount"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundBungaAmount") = value
        End Set
    End Property
    Property IncomeRatioPercentage() As Decimal
        Get
            Return CDec(ViewState("IncomeRatioPercentage"))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("IncomeRatioPercentage") = Value
        End Set
    End Property
    Property CustomerTotalIncome() As Double
        Get
            Return CDbl(ViewState("CustomerTotalIncome"))
        End Get
        Set(ByVal Value As Double)
            ViewState("CustomerTotalIncome") = Value
        End Set
    End Property
    Public Property OtrNtf() As String
        Get
            Return CStr(ViewState("OtrNtf"))
        End Get
        Set(value As String)
            ViewState("OtrNtf") = value
        End Set
    End Property
#End Region

    Public Property Balon() As DataTable
        Get
            Return CType(ViewState("Balon"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState("Balon") = value
        End Set
    End Property

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Session.Remove("balon")
            Me.ApplicationID = Request("ApplicationID")

            Me.EffectiveRate = 0
            Me.rvEffValue = CStr(0)

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()
                .initControls("OnNewApplication")

                Me.CustomerID = .CustomerID
                Me.CustName = .CustomerName
                Me.InterestType = .InterestType.ToString
                Me.InstallmentScheme = .InstallmentScheme.ToString

                If .IsAppTimeLimitReached Then
                    ShowMessage(lblMessage, "Aplikasi sudah pending lebih dari " & .AppTimeLimit.ToString & " hari tidak bisa diedit!", True)
                    btnSave.Visible = False
                End If
            End With

            ucApplicationTab1.ApplicationID = Me.ApplicationID
            ucApplicationTab1.selectedTab("Financial")
            ucApplicationTab1.setLink()
            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            initObjects()
            Bindgrid_002()
            
            If ucViewApplication1.isProceed = True Then
                btnSave.Visible = False
            End If

        End If
    End Sub

#Region "Load Financial Data"
    Sub Bindgrid_002()
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData

        oFinancialData.BranchId = Replace(Me.sesBranchId, "'", "")
        oFinancialData.AppID = Me.ApplicationID
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_002"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)

        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.ListData
        End If

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)
            lblAssetInsurance3.Text = FormatNumber(objrow.Item("InsAssetCapitalized"), 0)
            lblOTR.Text = FormatNumber(objrow.Item("OTRKendaraan"), 0)
            lblKaroseri.Text = FormatNumber(objrow.Item("HargaKaroseri"), 0)
            lblPA.Text = FormatNumber(CDbl(objrow.Item("OTRKendaraan")) - CDbl(objrow.Item("RVEstimateOL")), 0)

            txtDP.Text = FormatNumber(Math.Round(CDbl(objrow.Item(4)), 0), 0)
            lblNTF.Text = FormatNumber(objrow.Item(5), 0)
            txtNumInst.Text = objrow.Item(8).ToString.Trim
            txtSukuBunga.Text = FormatNumber(CDbl(objrow.Item("FlatRate")), 7)

            With txtNumInst
                .AutoPostBack = True
                .RequiredFieldValidatorEnable = True
                .TextCssClass = "numberAlign2 regular_text"
                .isReadOnly = True
                .RangeValidatorEnable = True
                .RangeValidatorMinimumValue = "1"
                .RangeValidatorMaximumValue = objrow.Item(8).ToString.Trim
            End With

            EffectiveRate = CDec(objrow.Item("EffectiveRate"))

            Me.RejectMinimumIncome = CDbl(objrow.Item(11))
            Me.NTFGrossYield = CDbl(objrow.Item(12))
            Me.RefundInterest = CDbl(objrow.Item("RefundInterest"))
            Me.AdminFee = CDbl(objrow.Item("AdminFee"))
            Me.Provisi = CDbl(objrow.Item("ProvisionFee"))
            Me.CustomerTotalIncome = CDbl(objrow.Item("CustomerTotalIncome"))
            Me.IncomeRatioPercentage = CDec(objrow.Item("IncomeInsRatioPercentage"))

            oApplication.BranchId = Replace(Me.sesBranchId, "'", "")
            oApplication.ApplicationID = Me.ApplicationID
            oApplication.strConnection = GetConnectionString()
            oApplication = m_ControllerApp.GetDataAppIDProspect(oApplication)

            If oApplication.Err = "" Then
                If Me.ApplicationID.Trim = oApplication.ApplicationID.Trim Then
                    cboFirstInstallment.SelectedIndex = cboFirstInstallment.Items.IndexOf(cboFirstInstallment.Items.FindByValue(oApplication.FirstInstallment))
                End If
            End If

            cboFirstInstallment.SelectedValue = objrow.Item("FirstInstallment").ToString
            txtDP.Text = FormatNumber(objrow.Item("DownPayment"), 0)
            lblRVEstimate.Text = FormatNumber(objrow.Item("RVEstimateOL"), 0)
            lblRVInterest.Text = FormatNumber(objrow.Item("RVInterestOL"), 0)
            lblInsurance.Text = FormatNumber(objrow.Item("InsuranceMonthlyOL"), 0)

            ucMaintenance.Text = FormatNumber(objrow.Item("MaintenanceFeeOL"), 0)
            ucSTNK.Text = FormatNumber(objrow.Item("STNKFeeOL"), 0)
            ucAdminFee.Text = FormatNumber(objrow.Item("AdminFeeOL"), 0)
            ucProvisi.Text = FormatNumber(objrow.Item("ProvisiOL"), 0)
            lblBasicLease.Text = FormatNumber(objrow.Item("BasicLease"), 0)
            lblExpectedInterest.Text = FormatNumber(CDbl(lblPA.Text) * (CDbl(objrow.Item("FlatRate")) / 100) * (CDbl(txtNumInst.Text) / 12), 0)
            
            Dim pgross, pinsnet, pselisih, ppolis, potherfee As Double

            pgross = 0
            ppolis = CDbl(IIf(IsNumeric(objrow.Item("BiayaPolis")), objrow.Item("BiayaPolis"), 0))
            potherfee = CDbl(IIf(IsNumeric(objrow.Item("OtherFee")), objrow.Item("OtherFee"), 0))
            pselisih = pgross - pinsnet

            CalculateDPPersen()
            Me.OtrNtf = objrow("OtrNtf").ToString.Trim

            If FormatNumber(CDbl(lblNTF.Text)) <> FormatNumber(objrow.Item("OldNTF")) Then
                CalculateInstallment()
            End If

            CalculateTotalPembiayaan()

        End If
    End Sub

    Sub Behaviour(ByVal textbox As TextBox, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As Button)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MinimumValue = textbox.Text.Trim
                rv.MaximumValue = "999999999999999"
                rv.ErrorMessage = "Input Harus  >= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                rv.Enabled = True
                rv.MaximumValue = textbox.Text.Trim
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input Harus <= " & textbox.Text.Trim & ""
                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "999999999999999"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Harap isi dengan Angka"
        End Select
    End Sub


    Sub BehaviourPersen(ByVal Flag As String, ByVal textbox As TextBox, ByVal value As String, ByVal rv As RangeValidator, ByVal Key As String, ByVal imb As Button)
        Select Case Key
            Case "L"
                textbox.ReadOnly = True
                imb.Visible = False
                rv.Enabled = False
            Case "N"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                rv.MinimumValue = value
                rv.MaximumValue = "100"
                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >= " & FormatNumber(value, 2) & " dan <= 100 (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "X"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                rv.MaximumValue = value
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >=0 dan <= " & value

                If Flag = "Product" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk) "
                ElseIf Flag = "ProductBranch" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Cabang) "
                ElseIf Flag = "ProductOffering" Then
                    rv.ErrorMessage = "Input harus >=0 dan <= " & FormatNumber(value, 2) & " (dari Produk Jual) "
                End If

                imb.Visible = True
            Case "D"
                textbox.ReadOnly = False
                'textbox.Text = value
                rv.Enabled = True
                imb.Visible = True
                rv.MaximumValue = "100"
                rv.MinimumValue = "0"
                rv.ErrorMessage = "Input harus >= 0 dan <= 100"
        End Select
    End Sub

#End Region

    Protected Sub CalculateDP() Handles txtDPPersen.TextChanged
        txtDP.Text = FormatNumber(CDbl(lblOTR.Text) * CDec(IIf(IsNumeric(txtDPPersen.Text), txtDPPersen.Text, 0)) / 100, 0)
        CalculateTotalPembiayaan()
    End Sub

    Protected Sub CalculateDPPersen() Handles txtDP.TextChanged
        txtDPPersen.Text = FormatNumber(CDec(IIf(IsNumeric(txtDP.Text), txtDP.Text, 0)) / CDbl(lblOTR.Text) * 100, 2)
        CalculateTotalPembiayaan()
    End Sub

#Region "Calculate Installment"

    Protected Sub CalculateInstallment() Handles txtDP.TextChanged, txtNumInst.TextChanged, txtDPPersen.TextChanged, cboFirstInstallment.SelectedIndexChanged
        Dim oEntities As New Parameter.FinancialData
        Dim Inst As Double
        lblMessage.Visible = False

        CalculateTotalPembiayaan()
    End Sub


    Private Function IsValidValidation(ByVal textbox As TextBox, ByVal CompareText As ucNumberFormat, ByVal Message As String) As Boolean
        If textbox.Text.Trim = "" Then
            ShowMessage(lblMessage, Message & " Harap diisi", True)
            Return False
        ElseIf CDbl(textbox.Text) = 0 Then
            ShowMessage(lblMessage, Message & " Harap isi > 0 ", True)
            Return False
        ElseIf CDbl(textbox.Text) > CDbl(CompareText.Text) Then
            ShowMessage(lblMessage, Message & " Harap isi < Jangka Waktu Angsuran", True)
            Return False
        End If
        Return True
    End Function

#End Region



#Region "Save"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dtEntity As New DataTable
            Dim oFinancialData As New Parameter.FinancialData
            Dim ds As New DataSet

            Me.Status = False

            If CDbl(txtSukuBunga.Text) <= 0 Then
                ShowMessage(lblMessage, "Harap isi suku bunga", True)
                Exit Sub
            End If

            oFinancialData.FlatRate = CDbl(txtSukuBunga.Text)
            oFinancialData.InstallmentAmount = 0
            oFinancialData.NumOfInstallment = CInt(txtNumInst.Text)
            oFinancialData.FloatingPeriod = ""

            Dim PA As Double = CDbl(lblOTR.Text) - CDbl(lblRVEstimate.Text)
            Dim totBunga As Double = PA * (CDbl(txtSukuBunga.Text) / 100) * (CInt(txtNumInst.Text) / 12)
            Dim totBasicLease As Double = PA + CDbl(lblRVInterest.Text) + totBunga
            Dim basicLease = Math.Round(totBasicLease / CInt(txtNumInst.Text), 0)
            Dim mnt As Double = CDbl(IIf(IsNumeric(ucMaintenance.Text), ucMaintenance.Text, 0))
            Dim stnk As Double = CDbl(IIf(IsNumeric(ucSTNK.Text), ucSTNK.Text, 0))
            Dim adminfee As Double = CDbl(IIf(IsNumeric(ucAdminFee.Text), ucAdminFee.Text, 0))
            Dim provisi As Double = CDbl(IIf(IsNumeric(ucProvisi.Text), ucProvisi.Text, 0))
            Dim insur As Double = CDbl(IIf(IsNumeric(lblInsurance.Text), lblInsurance.Text, 0))
            Dim monthlyfee As Double = basicLease + mnt + stnk + adminfee + provisi + insur

            lblBasicLease.Text = FormatNumber(basicLease, 0)
            lblMonthlyLease.Text = FormatNumber(basicLease + mnt + stnk + adminfee + provisi + insur, 0)
            lblExpectedInterest.Text = FormatNumber(totBunga, 0)

            With oFinancialData
                .BusinessDate = Me.BusinessDate
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .AppID = Me.ApplicationID
                .ApplicationID = Me.ApplicationID
                .NTF = CDbl(lblOTR.Text)
                .EffectiveRate = 0
                .RateIRR = 0
                .SupplierRate = .EffectiveRate
                .PaymentFrequency = "1"
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .Tenor = CInt(txtNumInst.Text)
                .GracePeriod = 0
                .GracePeriodType = ""
                .BusDate = Me.BusinessDate
                .DiffRate = Me.DiffRate
                .Flag = "Add"
                .strConnection = GetConnectionString()
                .AdministrationFee = Me.AdminFee
                .PolaAngsuran = "FLAT"
                .TidakAngsur = 0
                .PotongDanaCair = False
                .AngsuranBayarDealer = False

                .TotalBunga = totBunga
                .NilaiKontrak = CDbl(lblOTR.Text)
                .InstallmentAmount = 0
                .Term = m_controller.GetTerm(1)

                .InstallmentUnpaid = 0
                .DownPayment = CDbl(txtDP.Text)
                .FlatRate = CDbl(txtSukuBunga.Text)
                .EffectiveDate = CDate("01/01/1900")
                .ProvisionFee = 0 'CDbl(txtBiayaProvisiN.Text)
                .AlokasiInsentifRefundBunga = 0
                .AlokasiInsentifRefundBungaPercent = 0
                .TitipanRefundBunga = 0
                .TitipanRefundBungaPercent = 0
                .AlokasiInsentifPremi = 0
                .AlokasiInsentifPremiPercent = 0
                .AlokasiProgresifPremi = 0
                .AlokasiProgresifPremiPercent = 0
                .SubsidiBungaPremi = 0
                .SubsidiBungaPremiPercent = 0
                .PendapatanPremi = 0
                .PendapatanPremiPercent = 0
                .ProvisiPercent = 0 'CDec(txtBiayaProvisi.Text)
                .AlokasiInsentifProvisi = 0
                .AlokasiInsentifProvisiPercent = 0
                .SubsidiBungaProvisi = 0
                .SubsidiBungaProvisiPercent = 0
                .TitipanProvisi = 0
                .TitipanProvisiPercent = 0
                .AlokasiInsentifBiayaLain = 0
                .AlokasiInsentifBiayaLainPercent = 0
                .SubsidiBungaBiayaLain = 0
                .SubsidiBungaBiayaLainPercent = 0
                .TitipanBiayaLain = 0
                .TitipanBiayaLainPercent = 0
                .SubsidiBungaDealer = 0
                .SubsidiAngsuran = 0

                .BungaNettEff = 0
                .BungaNettFlat = 0

                .RefundBungaPercent = 0 ' CDec(ucRefundBunga.Text)
                .RefundBungaAmount = 0 'CDbl(txtRefundBungaN.Text)
                .RefundPremiPercent = 0 'CDec(txtRefundPremi.Text)
                .RefundPremiAmount = 0 'CDbl(txtRefundPremiN.Text)
                .RefundAdminPercent = 0 'CDec(ucRefundAdmin.Text)
                .RefundAdminAmount = 0 'CDbl(txtRefundAdminN.Text)
                .RefundProvisiPercent = 0 'CDec(txtRefundBiayaProvisi.Text)
                .RefundProvisiAmount = 0 'CDbl(txtRefundBiayaProvisiN.Text)

                .GabungRefundSupplier = False

                .AdminFeeOL = adminfee
                .MaintenanceFeeOL = mnt
                .STNKFeeOL = stnk
                .ProvisiOL = provisi
                .InsuranceMonthlyOL = insur
                .BasicLease = basicLease
            End With
            Try
                oFinancialData = m_controller.SaveFinancialDataOperatingLease(oFinancialData)
            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                err.WriteLog("FinancialData_003.aspx", "imbSave_Click", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                ShowMessage(lblMessage, ex.Message, True)
            End Try

            If oFinancialData.Output = "" Then
                ShowMessage(lblMessage, "Data saved!", False)
                ucApplicationTab1.ApplicationID = Me.ApplicationID
                ucApplicationTab1.selectedTab("Financial")
                ucApplicationTab1.setLink()
            Else
                ShowMessage(lblMessage, "Proses Simpan data Gagal " & oFinancialData.Output & "", True)
            End If

        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("FinancialDataOperatingLease_003.aspx", "imbSave_Click", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub

#End Region



#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("../ApplicationMaintenance/ApplicationMaintenance.aspx")
    End Sub
    
#End Region


#Region "Calculate Total Pembiayaan"
  
    Protected Sub CalculateTotalPembiayaan()
        Dim ntf_ As Double = CDbl(lblOTR.Text) + CDbl(lblKaroseri.Text) - CDbl(txtDP.Text) + CDbl(lblAssetInsurance3.Text)
        lblNTF.Text = FormatNumber(ntf_, 0)
    End Sub

#End Region

    Protected Sub initObjects()
        uPanelSave.Visible = True
        Entities.strConnection = GetConnectionString()
        Entities.BranchId = Replace(Me.sesBranchId, "'", "")
        Entities.AppID = Me.ApplicationID
        Me.StepUpDownType = m_controller.GetStepUpStepDownType(Entities)

        lblMessage.Text = ""
        lblMessage.Visible = False

        txtDP.RequiredFieldValidatorEnable = True
        txtDP.AutoPostBack = True
        txtDP.TextCssClass = "numberAlign2 regular_text"
        txtNumInst.RequiredFieldValidatorEnable = True
        txtNumInst.TextCssClass = "numberAlign2 regular_text"
        txtNumInst.isReadOnly = True

        txtDPPersen.RequiredFieldValidatorEnable = True
        txtDPPersen.AutoPostBack = True
        txtDPPersen.TextCssClass = "numberAlign2 smaller_text"
    End Sub


    Private Sub saveBungaNett()
        Try
            Dim oBungaNet As New Parameter.FinancialData

            With oBungaNet
                .strConnection = GetConnectionString()
                .ApplicationID = Me.ApplicationID
                .FirstInstallment = cboFirstInstallment.SelectedValue
                .EffectiveRate = TotalBungaNett / 100
                .Tenor = CInt(txtNumInst.Text)
                .BungaNettEff = TotalBungaNett
                .BungaNettFlat = cEffToFlat(oBungaNet)
            End With

            ' oBungaNet = m_controller.saveBungaNett(oBungaNet)

        Catch ex As Exception
            'skip

        End Try
    End Sub


End Class