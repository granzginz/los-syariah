﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HasilSurvey_002Condition.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.HasilSurvey_002Condition" %>

<%@ Register Src="../../../webform.UserController/UcLookUpProductOffering.ascx"
    TagName="UcLookUpProductOffering" TagPrefix="uc1" %>

   <%-- <%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>--%>
<%@ Register Src="../../../webform.UserController/ucLookUpCustomer.ascx" TagName="ucLookUpCustomer"
    TagPrefix="uc1" %>
<%@ Register Src="../../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail"
    TagPrefix="uc2" %>
<%@ Register Src="../../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication"
    TagPrefix="uc3" %>
<%@ Register Src="../../../webform.UserController/ucGuarantor.ascx" TagName="ucGuarantor"
    TagPrefix="uc4" %>
<%@ Register Src="../../../webform.UserController/ucReferensi.ascx" TagName="ucReferensi"
    TagPrefix="uc5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucLookupKabupaten.ascx" TagName="ucLookupKabupaten"
    TagPrefix="uc6" %>
<%@ Register Src="../../../webform.UserController/ucHasilSurveyTab.ascx" TagName="ucHasilSurveyTab"
    TagPrefix="uc7" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc8" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc8" %>
<%@ Register Src="../../../webform.UserController/ucRefinancing.ascx" TagName="ucRefinancing"
    TagPrefix="uc9" %>
<%@ Register Src="../../../webform.UserController/ucHasilSurveyTabPhone.ascx" TagName="ucHasilSurveyTabPhone"
    TagPrefix="uc10" %>
<%@ Register Src="../../../webform.UserController/ucCoverageTypeApk.ascx" TagName="ucCoverageTypeApk"
    TagPrefix="uc11" %>
<%@ Register Src="../../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc12" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../../Webform.UserController/ucAddress.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucProductOffering" Src="../../../../Webform.UserController/ucProductOffering.ascx" %>--%>

<%@ Register TagPrefix="uc1" TagName="ucProdOffering" Src="../../../Webform.UserController/UcLookUpPdctOffering.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucKabupaten" Src="../../../Webform.UserController/ucKabupaten.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="../../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../../Include/Buttons.css" />
    
    <script src="<%=Page.ResolveUrl("~")%>/Maxiloan.js" type="text/javascript"></script>
    <link href="<%=Page.ResolveUrl("~")%>/include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="<%=Page.ResolveUrl("~")%>/js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="<%=Page.ResolveUrl("~")%>/js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
    <script type="text/javascript">
        function calculateNPV1a(id) {
            var TotalPenghasilan1 = document.getElementById('txtTotalPenghasilan1').value;
            var BiayaUsaha1 = document.getElementById('txtBiayaUsaha1').value;
            var BiayaHidup1 = document.getElementById('txtBiayaHidup1').value;
            var BiayaCicilan1 = document.getElementById('txtBiayaCicilan1').value;
            var result = parseInt(TotalPenghasilan1.replace(/\s*,\s*/g, '')) - parseInt(BiayaUsaha1.replace(/\s*,\s*/g, '')) - parseInt(BiayaHidup1.replace(/\s*,\s*/g, '')) - parseInt(BiayaCicilan1.replace(/\s*,\s*/g, ''));

                result += '';
                result = result.replace(",", "");
                x = result.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    
                }
                //var rslt = (x1+x2);
                if (result < 0) {
                    document.getElementById('txtSisaSebelumTambahUnit1').value = 0;
                }else{
                    document.getElementById('txtSisaSebelumTambahUnit1').value = x1 + x2;
                }
        }
        function calculateNPV1b(id) {
            var SisaSebelumTambahUnit1 = document.getElementById('txtSisaSebelumTambahUnit1').value;
            var IncomeDariUnit1 = document.getElementById('txtIncomeDariUnit1').value;
            var EstimasiAngsuran1 = document.getElementById('txtEstimasiAngsuran1').value;
            var result = parseInt(SisaSebelumTambahUnit1.replace(/\s*,\s*/g, '')) + parseInt(IncomeDariUnit1.replace(/\s*,\s*/g, '')) - parseInt(EstimasiAngsuran1.replace(/\s*,\s*/g, ''));

            result += '';
            result = result.replace(",", "");
            x = result.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            //alert(result)
            var rslt = (x1 + x2);
            if (result < 0) {
                document.getElementById('txtSisaPendapatan1').value = 0;
            } else {
                document.getElementById('txtSisaPendapatan1').value = x1 + x2;
            }
            //document.getElementById('txtSisaPendapatan1').value = x1 + x2;            
        }
        function calculateNPV1c(id) {
            var TotalPenghasilan2 = document.getElementById('txtTotalPenghasilan2').value;
            var BiayaUsaha2 = document.getElementById('txtBiayaUsaha2').value;
            var BiayaHidup2 = document.getElementById('txtBiayaHidup2').value;
            var BiayaCicilan2 = document.getElementById('txtBiayaCicilan2').value;
            var result = parseInt(TotalPenghasilan2.replace(/\s*,\s*/g, '')) - parseInt(BiayaUsaha2.replace(/\s*,\s*/g, '')) - parseInt(BiayaHidup2.replace(/\s*,\s*/g, '')) - parseInt(BiayaCicilan2.replace(/\s*,\s*/g, ''));

            result += '';
            result = result.replace(",", "");
            x = result.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            document.getElementById('txtSisaSebelumTambahUnit2').value = x1 + x2;
        }
        function calculateNPV1d(id) {
            var SisaSebelumTambahUnit2 = document.getElementById('txtSisaSebelumTambahUnit2').value;
            var IncomeDariUnit2 = document.getElementById('txtIncomeDariUnit2').value;
            var EstimasiAngsuran2 = document.getElementById('txtEstimasiAngsuran2').value;
            var result = parseInt(SisaSebelumTambahUnit2.replace(/\s*,\s*/g, '')) + parseInt(IncomeDariUnit2.replace(/\s*,\s*/g, '')) - parseInt(EstimasiAngsuran2.replace(/\s*,\s*/g, ''));

            result += '';
            result = result.replace(",", "");
            x = result.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            document.getElementById('txtSisaPendapatan2').value = x1 + x2;   
        }

        function calculateSisaSebelumTambahUnit(id) {
            var TotalPendapatan = document.getElementById('txtTotalPenghasilan1').value;
            var BiayaUsaha = document.getElementById('txtBiayaUsaha1').value;
            var BiayaHidup = document.getElementById('txtBiayaHidup1').value;
            var BiayaCicilan = document.getElementById('txtBiayaCicilan1').value;
            var result = (parseInt(TotalPendapatan.replace(/\s*,\s*/g, '')) - parseInt(IncomeDariUnit2.replace(/\s*,\s*/g, ''))) - ( parseInt(BiayaHidup.replace(/\s*,\s*/g, '')) -  parseInt(BiayaHidup.replace(/\s*,\s*/g, '')) );

            result += '';
            result = result.replace(",", "");
            x = result.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            document.getElementById('txtSisaSebelumTambahUnit1').value = x1 + x2;   
        }

        function calculateAdminFeeGross() {
            var admin = $('#ucAdminFee_txtNumber').val();
            var fiducia = $('#ucFiduciaFee_txtNumber').val();
            var other = $('#ucOtherFee_txtNumber').val();
            var biayaPolis = $('#ucBiayaPolis_txtNumber').val()

            if (!admin) { admin = "0" };
            if (!fiducia) { admin = "0" };
            if (!other) { admin = "0" };
            if (!biayaPolis) { admin = "0" };

            var total = parseInt(admin.replace(/\s*,\s*/g, '')) +
                        parseInt(fiducia.replace(/\s*,\s*/g, '')) +
                        parseInt(other.replace(/\s*,\s*/g, '')) +
                        parseInt(biayaPolis.replace(/\s*,\s*/g, ''));
            $('#lblAdminFeeGross').html(number_format(total));

        }
        function number_format(number, decimals, dec_point, thousands_sep) {

            number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
            var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                .toFixed(prec);
            };
            // Fix for IE parseFloat(0.55).toFixed(0) = 0;
            s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
            if (s[0].length > 3) {
                s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
            }
            if ((s[1] || '')
            .length < prec) {
                s[1] = s[1] || '';
                s[1] += new Array(prec - s[1].length + 1)
              .join('0');
            }
            return s.join(dec);
        }
        function SelectFromArray(itemArray) {
            var objAdd = 'document.forms[0].UCMailingAddress_';
            var pAddress = eval(objAdd + 'txtAddress');
            var pRT = eval(objAdd + 'txtRT');
            var pRW = eval(objAdd + 'txtRW');
            var pKelurahan = eval(objAdd + 'oLookUpKodePos_txtKelurahan'); 
            var pKecamatan = eval(objAdd + 'oLookUpKodePos_txtKecamatan');
            var pCity = eval(objAdd + 'oLookUpKodePos_txtCity');
            var pZipCode = eval(objAdd + 'oLookUpKodePos_txtZipCode'); 
            var pAreaPhone1 = eval(objAdd + 'txtAreaPhone1');
            var pPhone1 = eval(objAdd + 'txtPhone1');
            var pAreaPhone2 = eval(objAdd + 'txtAreaPhone2');
            var pPhone2 = eval(objAdd + 'txtPhone2');
            var pAreaFax = eval(objAdd + 'txtAreaFax');
            var pFax = eval(objAdd + 'txtFax');

            pAddress.value = itemArray[0][0];
            pRT.value = itemArray[0][1];
            pRW.value = itemArray[0][2];
            pKelurahan.value = itemArray[0][3];
            pKecamatan.value = itemArray[0][4];
            pCity.value = itemArray[0][5];
            pZipCode.value = itemArray[0][6];
            pAreaPhone1.value = itemArray[0][7];
            pPhone1.value = itemArray[0][8];
            pAreaPhone2.value = itemArray[0][9];
            pPhone2.value = itemArray[0][10];
            pAreaFax.value = itemArray[0][11];
            pFax.value = itemArray[0][12];
            return false;
        }

        function CheckTC(date, chk) {
            var text = eval('document.forms[0].' + date);
            if (chk == true) {
                text.disabled = true;
                text.value = '';
            }
            else {
                text.disabled = false;
            }

            return true;

        }

        function InterestType() {
            var myID = 'cboInstScheme';
            var obj = eval('document.forms[0].' + myID);
            if (document.forms[0].cboInterestType.value == 'FX') {
                obj.disabled = false;
            }
            else {
                obj.disabled = true;
            }
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }

        function InstScheme() {
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;

            if (document.forms[0].cboInstScheme.value == 'ST') {
                document.forms[0].rdoSTTYpe(0).disabled = false;
                document.forms[0].rdoSTTYpe(1).disabled = false;
                document.forms[0].rdoSTTYpe(2).disabled = false;
            }
        }

        function rdoDisabled() {
            document.forms[0].rdoSTTYpe(0).checked = true;
            document.forms[0].rdoSTTYpe(0).disabled = true;
            document.forms[0].rdoSTTYpe(1).disabled = true;
            document.forms[0].rdoSTTYpe(2).disabled = true;
        }
        function rboRefinancing_onchange() {
            var value = $('#rboRefinancing').find(':checked')[0].value;

            if (value === 'False') 
            {
                $('#divRefinancing').css("display", "inherit");
                return;
            }
            else 
            {
                $('#divRefinancing').css("display", "none");
                $('#ucRefinancing1_txtAgreementNo').val('');
                $('#ucRefinancing1_txtNilaiPelunasan').val('');
            }
        }
        function rboCaraSurvey_onchange() {

            var _CaraSurvey = $('#cboCaraSurvey').val();
            if (_CaraSurvey === 'False') {
                $('#divCaraSurvey').css("display", "inherit");
                return;
            }
            else {
                $('#divCaraSurvey').css("display", "none");
                $('#ucHasilSurveyTabPhone1_txtTlpRumah').val('');
                $('#ucHasilSurveyTabPhone1_txtTlpKantor').val('');
                $('#ucHasilSurveyTabPhone1_txtHandphone').val('');
                $('#ucHasilSurveyTabPhone1_txtEmergencyContact').val('');
            }
        }

        function UploadDokument(ucASPXPath, ucTitle, divLookupContent, paramData) {
            paramData = paramData || '';
            $('#dialog').remove();
            var match = new RegExp(App, 'gi');
            var tmpStrPath = AppInfo.replace(match, '');
            tmpStrPath = tmpStrPath.substring(2, tmpStrPath.length - 1);
            var arrStrPath = tmpStrPath.split('/');
            tmpStrPath = '';
            $.each(arrStrPath, function (index, value) {
                if (arrStrPath.length - 1 != index) {
                    tmpStrPath += '../'
                }
            });



            $('#' + divLookupContent).prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><img src="' + tmpStrPath + 'Images/exit_lookup00.png" class="ui-custom-close" /><iframe src="' + ucASPXPath + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
            $('#dialog')
	        .data('fnName', paramData)
	        .dialog({
	            title: ucTitle,
	            bgiframe: false,
	            width: 900,
	            height: 500,
	            resizable: false,
	            modal: true,
	            closeOnEscape: true,
	            draggable: true,
	            create: function (event, ui) {
	                $(".ui-widget-header").hide();
	            },
	            open: function () {
	                $('.ui-custom-close').bind('click', function () {
	                    $('#dialog').dialog('close');
	                })
	            }
	        });
            return false;
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinZipCode(pZipCode, pKelurahan, pKecamatan, pCity, pStyle) {
            window.open(ServerName + App + '/General/LookUpZipCode.aspx?Zipcode=' + pZipCode + '&Kelurahan=' + pKelurahan + '&Kecamatan=' + pKecamatan + '&City=' + pCity + '&Style=' + pStyle, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=1, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinProductOfferingLookup(pProductOfferingID, pProductOfferingDescription, pProductID, pAssetTypeID, pStyle, pBranchID) {
            window.open(ServerName + App + '/General/LookUpProductOffering.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID + '&AssetTypeID=' + pAssetTypeID + '&branchid=' + pBranchID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinKabupaten(pProductOfferingID, pProductOfferingDescription, pProductID, pStyle) {
            window.open(ServerName + App + '/General/LookupKabupaten.aspx?style=' + pStyle + '&productofferingid=' + pProductOfferingID + '&description=' + pProductOfferingDescription + '&productid=' + pProductID, 'UserLookup', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function Comma(Num) { //function to add commas to textboxes
            Num += '';
            Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
            Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
            x = Num.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1))
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            return x1 + x2;
        }
</script>
</head>
<body onload="calculateNPV1a(document.getElementById('txtTotalPenghasilan1'));calculateNPV1b(document.getElementById('txtSisaPendapatan1'));">
    <%--getGridGeneralSize('hdnGridGeneralSize');">--%>
    <asp:UpdateProgress ID="upg1" runat="server" AssociatedUpdatePanelID="upnl1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <asp:UpdatePanel runat="server" ID="upnl1" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <input id="hdnGridGeneralSize" runat="server" type="hidden" />
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <uc7:ucHasilSurveyTab id="ucHasilSurveyTab1" runat="server" />
            <div class="form_box">
                <div class="title_strip">
                </div>
                <div class="form_left">
                    <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
                </div>
                <div class="form_right">
                    <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
                </div>
            </div>
            <div class="form_title">
                  <div>
                    <div class="form_left">
                    <h3>
                        ANALISA KEUANGAN</h3>
                    </div>
                    <div class="form_right">
                    <h3>
                        SKENARIO PENURUNAN PENDAPATAN</h3>
                    </div>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Total Penghasilan / Pendapatan</label>  
                        <asp:TextBox ID="txtTotalPenghasilan1" runat="server" OnChange="calculateNPV1a(this.id)" onkeypress="return numbersonly2(event)" 
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Total Penghasilan / Pendapatan</label>  
                        <asp:TextBox ID="txtTotalPenghasilan2" runat="server" OnChange="calculateNPV1c(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Biaya Usaha</label>  
                        <asp:TextBox ID="txtBiayaUsaha1" runat="server" OnChange="calculateNPV1a(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Biaya Usaha</label>  
                        <asp:TextBox ID="txtBiayaUsaha2" runat="server" OnChange="calculateNPV1c(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Biaya Hidup</label>  
                        <asp:TextBox ID="txtBiayaHidup1" runat="server" OnChange="calculateNPV1a(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Biaya Hidup</label>  
                        <asp:TextBox ID="txtBiayaHidup2" runat="server" OnChange="calculateNPV1c(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Biaya Cicilan</label>  
                        <asp:TextBox ID="txtBiayaCicilan1" runat="server" OnChange="calculateNPV1a(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Biaya Cicilan</label>  
                        <asp:TextBox ID="txtBiayaCicilan2" runat="server" OnChange="calculateNPV1c(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Sisa sebelum tambah unit</label>  
                        <asp:TextBox ID="txtSisaSebelumTambahUnit1" runat="server" OnChange="calculateNPV1b(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Sisa sebelum tambah unit</label>  
                        <asp:TextBox ID="txtSisaSebelumTambahUnit2" runat="server" OnChange="calculateNPV1d(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Income dari unit</label>  
                        <asp:TextBox ID="txtIncomeDariUnit1" runat="server" OnChange="calculateNPV1b(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Income dari unit</label>  
                        <asp:TextBox ID="txtIncomeDariUnit2" runat="server" OnChange="calculateNPV1d(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Estimasi Angsuran</label>  
                        <asp:TextBox ID="txtEstimasiAngsuran1" runat="server" OnChange="calculateNPV1b(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Estimasi Angsuran</label>  
                        <asp:TextBox ID="txtEstimasiAngsuran2" runat="server" OnChange="calculateNPV1d(this.id);" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Sisa Pendapatan</label>  
                        <asp:TextBox ID="txtSisaPendapatan1" runat="server" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label>Sisa Pendapatan</label>  
                        <asp:TextBox ID="txtSisaPendapatan2" runat="server" onkeypress="return numbersonly2(event)"
                            onblur="extractNumber(this,7,true);blockInvalid(this);this.value=numberFormat(this.value);this.value=blankToZero(this.value);" onfocus="this.value=resetNumber(this.value);"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_title">
                    <div class="form_single">
                    <h3>
                        BUKTI PENDUKUNG</h3>
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" >
                        <label class="label_req">Kondisi Kesehatan Pemohon</label>  
                        <asp:DropDownList ID="cboKondisiKesehatan" runat="server">
                            <asp:ListItem Value="Sangat Sehat" Selected="True">SANGAT SEHAT</asp:ListItem>
                            <asp:ListItem Value="Sehat">SEHAT</asp:ListItem>
                            <asp:ListItem Value="Cukup Sehat">CUKUP SEHAT</asp:ListItem>
                            <asp:ListItem Value="Kurang Sehat">KURANG SEHAT</asp:ListItem>
                            <asp:ListItem Value="Tidak Sehat">TIDAK SEHAT</asp:ListItem>
                        </asp:DropDownList> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="cboKondisiKesehatan"></asp:RequiredFieldValidator>                  
                    </div> 
                    <div class="form_left" >
                        <label>Industry Risk Rating</label>  
                        <asp:DropDownList ID="cboIndustryRisk" runat="server" disabled="true">
                            <asp:ListItem Value="Dark Green" Selected="True">DARK GREEN</asp:ListItem>
                            <asp:ListItem Value="Green">GREEN</asp:ListItem>
                            <asp:ListItem Value="Light Yellow">LIGHT YELLOW</asp:ListItem>
                            <asp:ListItem Value="Yellow">YELLOW</asp:ListItem>
                            <asp:ListItem Value="Red">RED</asp:ListItem>
                        </asp:DropDownList> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="cboIndustryRisk"></asp:RequiredFieldValidator>                  
                    </div> 
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" >
                        <label class="label_req">Kondisi Lingkungan</label>  
                        <asp:DropDownList ID="cboKondisiLingkungan" runat="server">
                            <asp:ListItem Value="Mewah" Selected="True">MEWAH</asp:ListItem>
                            <asp:ListItem Value="Agak Mewah">AGAK MEWAH</asp:ListItem>
                            <asp:ListItem Value="Menengah">MENENGAH</asp:ListItem>
                            <asp:ListItem Value="Sederhana">SEDERHANA</asp:ListItem>
                            <asp:ListItem Value="Kumuh">KUMUH</asp:ListItem>
                        </asp:DropDownList> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="cboKondisiLingkungan"></asp:RequiredFieldValidator>                  
                    </div> 
                    <div class="form_left" >
                        <label>Usia Pemohon</label>  
                        <asp:TextBox ID="txtUsiaPemohon" width="35px" runat="server" MaxLength="50" enabled="false"></asp:TextBox>
                        <%--<asp:DropDownList ID="cboUsiaPemohon" runat="server" disabled="true">
                            <asp:ListItem Value="5" Selected="True">>45 s/d 50 Tahun</asp:ListItem>
                            <asp:ListItem Value="4">>35 s/d 45 Tahun</asp:ListItem>
                            <asp:ListItem Value="3">>25 s/d 35 Tahun</asp:ListItem>
                            <asp:ListItem Value="2">>17 s/d 24 Tahun</asp:ListItem>
                            <asp:ListItem Value="1">>50 Tahun</asp:ListItem>
                        </asp:DropDownList> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="cboUsiaPemohon"></asp:RequiredFieldValidator>   --%>               
                    </div> 
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" >
                        <label>Lama Tinggal</label>  
                        <asp:DropDownList ID="cboLamaTinggal" runat="server" disabled="true">
                            <asp:ListItem Value="5" Selected="True">>5 Tahun</asp:ListItem>
                            <asp:ListItem Value="4">>3 s/d 5 Tahun</asp:ListItem>
                            <asp:ListItem Value="3">>2 s/d 3 Tahun</asp:ListItem>
                            <asp:ListItem Value="2">>1 s/d 2 Tahun</asp:ListItem>
                            <asp:ListItem Value="1"><1 Tahun</asp:ListItem>
                        </asp:DropDownList> 
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" CssClass="validator_general"
                        ControlToValidate="cboLamaTinggal"></asp:RequiredFieldValidator>                  
                    </div> 
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Rekening Tabungan/Giro</label>  
                        <asp:RadioButtonList ID="rboRekeningTabunganGiro" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>Nama Bank</label>  
                        <asp:TextBox ID="txtNamaBank" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Laporan Keuangan</label>  
                        <asp:RadioButtonList ID="rboLaporanKeuangan" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                        <label>Alasan</label>  
                        <asp:TextBox ID="txtAlasanLaporanKeuangan" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Pembiayaan dari Bank/LNKB</label>  
                        <asp:RadioButtonList ID="rboKreditdariBankLNKB" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama Institusi</label>  
                        <asp:TextBox ID="txtNamaInstitusi1" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label class="label_split" runat="server" id="lblTelepon1">
                            Angsuran
                        </label>
                        <asp:TextBox ID="txtAngsuran1" MaxLength="10" runat="server" CssClass="smaller_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:Label ID="lblStrip1" runat="server">Sisa</asp:Label>
                        <asp:TextBox ID="txtSisa1" MaxLength="10" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <label>bulan</label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Nama Institusi</label>  
                        <asp:TextBox ID="txtNamaInstitusi2" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                    <div class="form_right">
                        <label class="label_split" runat="server" id="Label1">
                            Angsuran
                        </label>
                        <asp:TextBox ID="txtAngsuran2" MaxLength="10" runat="server" CssClass="smaller_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:Label ID="lblStrip2" runat="server">Sisa</asp:Label>
                        <asp:TextBox ID="txtSisa2" MaxLength="10" runat="server" CssClass="small_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <label>bulan</label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_general">Bukti Pembayaran Angsuran</label>  
                        <asp:RadioButtonList ID="rboBuktiPembayaranAngsuran" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="Y">Ada</asp:ListItem>
                            <asp:ListItem Selected="True" Value="T">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form_right">
                    <label>Alasan</label>
                        <asp:TextBox ID="txtAlasanBuktiPembayaranAng" width="255px" runat="server" MaxLength="50"></asp:TextBox>                   
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_single">
                        <label class="label_general">Kesimpulan</label>  
                        <asp:TextBox runat="server" ID="txtKesimpulan" TextMode="MultiLine" Width="1000px" ></asp:TextBox>
                    </div>
                </div>
            </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
                        CausesValidation="True"></asp:Button>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
           
        </ContentTemplate>
        <Triggers>
          <%--  <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />--%>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <%--<asp:AsyncPostBackTrigger ControlID="cboKegiatanUsaha" EventName="SelectedIndexChanged" />--%>
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
