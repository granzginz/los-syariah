﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewInsuranceDetailNew.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewInsuranceDetailNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="UcInsuranceData" Src="../../Webform.UserController/ViewApplication/UcInsuranceData.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInsuranceDataAgri" Src="../../Webform.UserController/ViewApplication/UcInsuranceDataAgriculture.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAsset</title>
    <link href="../../Include/Buttons.css" type="text/css" rel="Stylesheet"/>
    <link href="../../Include/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
     <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
     <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - INSURANCE
            </h3>
        </div>
    </div>
     <div class="form_box" id="InsuranceData" runat="server">
                <div class="form_single">
                   <uc1:UcInsuranceData id="UcInsuranceData" runat="server"></uc1:UcInsuranceData>
                </div>
            </div>
    <div class="form_box" id="InsuranceDataAgri" runat="server">
                <div class="form_single">
                   <uc1:UcInsuranceDataAgri id="UcInsuranceDataAgri" runat="server"></uc1:UcInsuranceDataAgri>
                </div>
            </div>
<%--   <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
            <asp:Button ID="btnBack"  runat="server"
            Text="Back" CssClass="small button gray" CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
