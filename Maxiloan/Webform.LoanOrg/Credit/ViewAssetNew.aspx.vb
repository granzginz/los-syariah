﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region

Public Class ViewAssetNew
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"

    Protected WithEvents ucassetdata As UcAssetData
#End Region

#Region "Property"

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property AgreementNo() As String
        Get
            Return ViewState("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property


    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property


    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return ViewState("Style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property




#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "ViewAsset"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.ApplicationID = Request("ApplicationID").ToString
                Me.CustomerName = Request("CustomerName").ToString
                Me.Style = Request("Style").ToString
                Me.CustomerID = Request("CustomerID").ToString.Trim
                Me.AgreementNo = Request("AgreementNo").ToString
                loadUcAssetData()
            End If
            lblMessage.Text = ""
        End If
    End Sub
    Sub loadUcAssetData()
        ucassetdata.ApplicationID = Me.ApplicationID
        ucassetdata.hideRT()
        ucassetdata.hideRW()
        ucassetdata.HideKelurahan()
        ucassetdata.HideKecamatan()
        ucassetdata.Hidexx()
        ucassetdata.BindGrid()
    End Sub


    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    'End Sub
End Class