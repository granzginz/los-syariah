﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region
Public Class viewJourna
    Inherits Maxiloan.Webform.WebBased
#Region "Declaration"
    Protected WithEvents outError As System.Web.UI.HtmlControls.HtmlGenericControl

#End Region
#Region "Constanta"
    Private cContract As New GeneralPagingController
    Private oContract As New Parameter.GeneralPaging
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private x_controller As New DataUserControlController
#End Region
#Region "Property"
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property ApplicationReq() As String
        Get
            Return ViewState("ApplicationReq").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationReq") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then

            If Not Page.IsPostBack Then
                Me.ApplicationID = " a.ApplicationID ='" & Request("ApplicationID").ToString & "'"
                Me.ApplicationReq = Request("ApplicationID").ToString
                Bindgrid()
            End If
        End If
    End Sub
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgIndType.SortCommand
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        Bindgrid()
    End Sub
#End Region
#Region "DtgIndType_ItemCommand"
    Protected Sub DtgIndType_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgIndType.ItemCommand
        If e.CommandName = "ShowView" Then
            
            Server.Transfer("viewJournalDetail.aspx?tr_nomor=" & CStr(DtgIndType.DataKeys(e.Item.ItemIndex)) & " &ApplicationID=" & Me.ApplicationReq & "", False)
        End If
    End Sub
#End Region
#Region "Bindgrid"
    Sub Bindgrid()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView



        With oContract
            .strConnection = GetConnectionString()
            .WhereCond = Me.ApplicationID
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = ""
            .SpName = "spJournal1"
        End With
        oContract = cContract.GetGeneralPaging(oContract)

        DtUserList = oContract.ListData
        DvUserList = DtUserList.DefaultView
        recordCount = oContract.TotalRecords
        Try
            DtgIndType.DataSource = DvUserList
            DtgIndType.DataBind()
        Catch en As System.Web.HttpException
            DtgIndType.CurrentPageIndex = 0
            DtgIndType.DataBind()
        End Try
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            lblTotPage.Text = "1"
            txtGoPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotrec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid()
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles imbGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                Bindgrid()
            End If
        End If
    End Sub
#End Region

End Class