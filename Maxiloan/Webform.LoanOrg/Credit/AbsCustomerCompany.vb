﻿Imports System.IO
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Public MustInherit Class AbsCustomerCompany
    Inherits Maxiloan.Webform.WebBased
    Protected Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Protected Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property
    Protected Property Address() As String
        Get
            Return CType(ViewState("Address"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Address") = Value
        End Set
    End Property
    Protected Property RT() As String
        Get
            Return CType(ViewState("RT"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RT") = Value
        End Set
    End Property
    Protected Property RW() As String
        Get
            Return CType(ViewState("RW"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("RW") = Value
        End Set
    End Property
    Protected Property City() As String
        Get
            Return CType(ViewState("City"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("City") = Value
        End Set
    End Property
    Protected Property Kecamatan() As String
        Get
            Return CType(ViewState("Kecamatan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Kecamatan") = Value
        End Set
    End Property
    Protected Property Kelurahan() As String
        Get
            Return CType(ViewState("Kelurahan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Kelurahan") = Value
        End Set
    End Property
    Protected Property NPWP() As String
        Get
            Return CType(ViewState("NPWP"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NPWP") = Value
        End Set
    End Property
    Protected Property ZipCode() As String
        Get
            Return CType(ViewState("ZipCode"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ZipCode") = Value
        End Set
    End Property
    Protected Property AFax() As String
        Get
            Return CType(ViewState("AFax"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AFax") = Value
        End Set
    End Property
    Protected Property Fax() As String
        Get
            Return CType(ViewState("Fax"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Fax") = Value
        End Set
    End Property
    Protected Property APhone1() As String
        Get
            Return CType(ViewState("APhone1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("APhone1") = Value
        End Set
    End Property
    Protected Property Phone1() As String
        Get
            Return CType(ViewState("Phone1"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Phone1") = Value
        End Set
    End Property
    Protected Property APhone2() As String
        Get
            Return CType(ViewState("APhone2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("APhone2") = Value
        End Set
    End Property
    Protected Property Phone2() As String
        Get
            Return CType(ViewState("Phone2"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Phone2") = Value
        End Set
    End Property

    Protected Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property

    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property


    Protected Property CustomerGroupID() As String
        Get
            Return CType(ViewState("CustomerGroupID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerGroupID") = Value
        End Set
    End Property
    Protected m_controller As New CustomerController
    Protected gStrPath As String
    Protected gStrFileName As String

    Protected Sub GetXML()
        If File.Exists(gStrPath & gStrFileName) Then
            Me.Name = Get_XMLValue(gStrPath & gStrFileName, "Name")
            Me.NPWP = Get_XMLValue(gStrPath & gStrFileName, "NPWP")
            Me.Address = Get_XMLValue(gStrPath & gStrFileName, "Address")
            Me.RT = Get_XMLValue(gStrPath & gStrFileName, "RT")
            Me.RW = Get_XMLValue(gStrPath & gStrFileName, "RW")
            Me.Kelurahan = Get_XMLValue(gStrPath & gStrFileName, "Kelurahan")
            Me.Kecamatan = Get_XMLValue(gStrPath & gStrFileName, "Kecamatan")
            Me.City = Get_XMLValue(gStrPath & gStrFileName, "City")
            Me.ZipCode = Get_XMLValue(gStrPath & gStrFileName, "ZipCode")
            Me.APhone1 = Get_XMLValue(gStrPath & gStrFileName, "APhone1")
            Me.Phone1 = Get_XMLValue(gStrPath & gStrFileName, "Phone1")
            Me.APhone2 = Get_XMLValue(gStrPath & gStrFileName, "APhone2")
            Me.Phone2 = Get_XMLValue(gStrPath & gStrFileName, "Phone2")
            Me.AFax = Get_XMLValue(gStrPath & gStrFileName, "AFax")
            Me.Fax = Get_XMLValue(gStrPath & gStrFileName, "Fax")
        End If
    End Sub



    Protected Function Get_XMLValue(ByVal pStrFile As String, ByVal pStrColumn As String) As String
        Dim lObjDSCust As New DataSet
        Dim lStrName As String = ""
        Try
            lObjDSCust.ReadXml(pStrFile)
            lStrName = lObjDSCust.Tables("Cust").Rows(0)("" & pStrColumn & "").ToString
        Catch ex As Exception
            'Response.Redirect("WriteXML.aspx")
        Finally
            lObjDSCust = Nothing
        End Try
        Return lStrName
    End Function


    Protected Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean, txtcss As String)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = txtcss
        End With
    End Sub

    Protected Sub FillCommonValueCbo(ds As IList(Of Parameter.CommonValueText), cbo As DropDownList) 
        cbo.DataSource = ds
        cbo.DataTextField = "Text"
        cbo.DataValueField = "Value"
        cbo.DataBind()
        cbo.Items.Insert(0, "Select One")
        cbo.Items(0).Value = "Select One"

    End Sub

End Class
