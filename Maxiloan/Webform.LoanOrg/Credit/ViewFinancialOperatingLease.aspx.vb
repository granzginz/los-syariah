﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewFinancialOperatingLease
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucFlagTransaksi As ucFlagTransaksi

#Region "Constanta"
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim RunRate As Double
    'Dim FlatRate As Decimal
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim m_controller As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController

    Private oRefundInsentifController As New RefundInsentifController
    Private oController As New ApplicationDetailTransactionController

    Private TotalInsGrid As Double
    Private TotalProsentase As Double
    Private TotalPencairan As Double
#End Region
#Region "Property"
    Private TotalBungaNett As Decimal
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property BrchId() As String
        Get
            Return ViewState("BrchId").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("BrchId") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return ViewState("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property Style() As String
        Get
            Return ViewState("Style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property
    Private Property FlatRate() As Decimal
        Get
            Return CType(ViewState("FlatRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("FlatRate") = Value
        End Set
    End Property
    Property RefundBungaAmount As Double
        Get
            Return CType(ViewState("RefundBungaAmount"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundBungaAmount") = value
        End Set
    End Property
    Private Property EffectiveRate() As Decimal
        Get
            Return CType(ViewState("EffectiveRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffectiveRate") = Value
        End Set
    End Property
    Public Property FirstInstallment As String
        Get
            Return CStr(ViewState("FirstInstallment"))
        End Get
        Set(value As String)
            ViewState("FirstInstallment") = value
        End Set
    End Property

    Public Property Tenor As Integer
        Get
            Return CInt(ViewState("Tenor"))
        End Get
        Set(value As Integer)
            ViewState("Tenor") = value
        End Set
    End Property
    Property InstallmentAmount() As Double
        Get
            Return CDbl(ViewState("InstallmentAmount"))
        End Get
        Set(ByVal Value As Double)
            ViewState("InstallmentAmount") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID").ToString
            Me.BrchId = Request("BrchId").ToString
            Me.CustomerName = Request("CustomerName").ToString
            Me.Style = Request("Style").ToString
            Me.CustomerID = Request("CustomerID").ToString.Trim
            Me.AgreementNo = Request("AgreementNo").ToString

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()
                .initControls("OnNewApplication")
            End With

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            Bindgrid_002()


            'lblTotalBunga.Text = FormatNumber(Math.Ceiling(Math.Round((CDbl(lblNTF.Text) * CInt(lblNumInst.Text) / m_controller.GetTerm(1) * Me.FlatRate / 100), 0) / 1000) * 1000, 0)
            Dim ntf As Double
            Dim totalbunga As Double
            Dim nilaiKontrak As Double

            If IsDBNull(lblNTF.Text) Or lblNTF.Text = "" Then
                ntf = CDbl(0)
            Else
                ntf = CDbl(lblNTF.Text)
            End If

            If IsDBNull(lblTotalBunga.Text) Or lblTotalBunga.Text = "" Then
                totalbunga = CDbl(0)
            Else
                totalbunga = CDbl(lblTotalBunga.Text)
            End If
            nilaiKontrak = ntf + totalbunga

        End If



    End Sub
#Region "Load Financial Data"
    Sub Bindgrid_002()
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData

        oFinancialData.BranchId = CStr(IIf(Me.BrchId Is Nothing, Replace(Me.sesBranchId, "'", ""), Me.BrchId))
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_002"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)

        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.ListData
        End If
        SetValueToLabel(dtEntity)
    End Sub

    Sub SetValueToLabel(ByVal oData As DataTable)
        Dim oRow As DataRow
        oRow = oData.Rows(0)

        lblAssetInsurance3.Text = FormatNumber(oRow.Item("InsAssetCapitalized"), 0)
        lblOTR.Text = FormatNumber(oRow.Item("OTRKendaraan"), 0)
        lblKaroseri.Text = FormatNumber(oRow.Item("HargaKaroseri"), 0)
        lblPA.Text = FormatNumber(CDbl(oRow.Item("OTRKendaraan")) - CDbl(oRow.Item("RVEstimateOL")), 0)

        txtDP.Text = FormatNumber(Math.Round(CDbl(oRow.Item(4)), 0), 0)
        lblNTF.Text = FormatNumber(oRow.Item(5), 0)
        txtNumInst.Text = oRow.Item(8).ToString.Trim
        txtSukuBunga.Text = FormatNumber(CDbl(oRow.Item("FlatRate")), 7)

        txtDP.Text = FormatNumber(oRow.Item("DownPayment"), 0)
        lblRVEstimate.Text = FormatNumber(oRow.Item("RVEstimateOL"), 0)
        lblRVInterest.Text = FormatNumber(oRow.Item("RVInterestOL"), 0)
        lblInsurance.Text = FormatNumber(oRow.Item("InsuranceMonthlyOL"), 0)

        ucMaintenance.Text = FormatNumber(oRow.Item("MaintenanceFeeOL"), 0)
        ucSTNK.Text = FormatNumber(oRow.Item("STNKFeeOL"), 0)
        ucAdminFee.Text = FormatNumber(oRow.Item("AdminFeeOL"), 0)
        ucProvisi.Text = FormatNumber(oRow.Item("ProvisiOL"), 0)
        lblBasicLease.Text = FormatNumber(oRow.Item("BasicLease"), 0)
        lblExpectedInterest.Text = FormatNumber(CDbl(lblPA.Text) * (CDbl(oRow.Item("FlatRate")) / 100) * (CDbl(txtNumInst.Text) / 12), 0)

        Dim pgross, pinsnet, pselisih, ppolis, potherfee As Double

        pgross = 0
        ppolis = CDbl(IIf(IsNumeric(oRow.Item("BiayaPolis")), oRow.Item("BiayaPolis"), 0))
        potherfee = CDbl(IIf(IsNumeric(oRow.Item("OtherFee")), oRow.Item("OtherFee"), 0))
        pselisih = pgross - pinsnet

        lblNilaiNTFOTR.Text = oRow(oRow("OtrNtf").ToString.Trim).ToString
        lblNilaiRefundBunga.Text = oRow("NilaiRefundBunga").ToString
        lblNilaiTenor.Text = CStr(Math.Ceiling(CDbl(oRow("tenor").ToString) / 12))
        txtFlatRate.Text = FormatNumber(oRow.Item("FlatRate"), 2)
        lblTotalBunga.Text = FormatNumber(CDbl(oRow("TotalBunga")), 0)
        lblNilaiTotalBunga.Text = oRow("TotalBunga").ToString.Trim
        lblPremiAsuransiGross.Text = FormatNumber(CDbl(oRow.Item("PremiGross")), 0)

        If CInt(oRow("refundBungaPercent")) > 0 Then
            txtRefundBunga.Text = FormatNumber(oRow("refundBungaPercent"), 2) & "% &nbsp;&nbsp;&nbsp;" & FormatNumber(oRow("refundBunga"), 0)
        Else
            txtRefundBunga.Text = "0"
        End If

        lblPremiAsuransiNet.Text = FormatNumber(oRow.Item("PremiNett"), 0)

        pgross = CDbl(IIf(IsNumeric(lblPremiAsuransiGross.Text), lblPremiAsuransiGross.Text, 0))
        pinsnet = CDbl(IIf(IsNumeric(lblPremiAsuransiNet.Text), lblPremiAsuransiNet.Text, 0))
        ppolis = CDbl(IIf(IsNumeric(oRow.Item("BiayaPolis")), oRow.Item("BiayaPolis"), 0))
        potherfee = CDbl(IIf(IsNumeric(oRow.Item("OtherFee")), oRow.Item("OtherFee"), 0))
        pselisih = pgross - pinsnet
        lblSelisihPremi.Text = FormatNumber(pselisih, 0)

        txtSubsidiBungaDealer.Text = FormatNumber(oRow("SubsidiBungaDealer"), 0)

        'Refund Premi
        Dim refundPremi_ As Double = 0
        If CInt(oRow.Item("RefundPremiPercent")) > 0 Then
            txtRefundPremi.Text = FormatNumber(CDbl(oRow.Item("RefundPremiPercent")), 2) & "% &nbsp;&nbsp;&nbsp;" & FormatNumber(oRow("RefundPremi"), 0)
            refundPremi_ = CDbl(oRow("RefundPremi"))
        Else
            txtRefundPremi.Text = "0"
        End If
        lblPendapatanPremi.Text = FormatNumber(pselisih - refundPremi_, 0)
        txtSubsidiBungaATPM.Text = FormatNumber(oRow("SubsidiBungaATPM"), 0)
        txtSubsidiAngsuran.Text = FormatNumber(oRow("SubsidiAngsuran"), 0)
        txtSubsidiAngsuranATPM.Text = FormatNumber(oRow("SubsidiAngsuranATPM"), 0)
        lblOtherFee.Text = FormatNumber(oRow.Item("OtherFee"), 0)
        txtSubsidiUangMuka.Text = FormatNumber(oRow.Item("SubsidiUangMuka"), 0)
        txtSubsidiUangMukaATPM.Text = FormatNumber(oRow.Item("SubsidiUangMukaATPM"), 0)
        txtRefundLain.Text = FormatNumber(oRow.Item("RefundBiayaLainPercent"), 2) & "% &nbsp;&nbsp;&nbsp;" & FormatNumber(oRow.Item("RefundBiayaLain"), 0)
        txtDealerDiscount.Text = FormatNumber(oRow.Item("DealerDiscount"), 0)
        lblBiayaProvisi.Text = FormatNumber(oRow("ProvisionFee"), 0)
        txtBiayaMaintenance.Text = FormatNumber(oRow("BiayaMaintenance"), 0)
        txtRefundBiayaProvisi.Text = FormatNumber(oRow("RefundBiayaProvisiPercent"), 2) & "% &nbsp;&nbsp;&nbsp;" & FormatNumber(oRow.Item("RefundBiayaProvisi"), 0)
        txtSupplierName.Text = oRow.Item("SupplierNameATPM").ToString.Trim
    
        Me.FirstInstallment = oRow.Item("FirstInstallment").ToString
        Me.Tenor = CInt(oRow.Item("Tenor").ToString.Trim)
        Me.InstallmentAmount = CDbl(oRow("InstallmentAmount").ToString.Trim)
        Me.EffectiveRate = CDec(oRow.Item("EffectiveRate"))

        showPencairan()

        CalculateTotalBayarPertama()
    End Sub

    Private Sub CalculateTotalBayarPertama()
        Dim ntf_ As Double = CDbl(lblOTR.Text) + CDbl(lblKaroseri.Text) - CDbl(txtDP.Text) + CDbl(lblAssetInsurance3.Text)
        lblNTF.Text = FormatNumber(ntf_, 0)
    End Sub


    Private Function getMaxRefundBunga() As String
        Dim oClass As New Parameter.GeneralSetting
        Dim oCtr As New GeneralSettingController

        With oClass
            .strConnection = GetConnectionString()
            .GSID = "RFBNG"
            .ModuleID = "CRD"
        End With

        Try
            Return oCtr.GetGeneralSettingValue(oClass)
        Catch ex As Exception
            Return "0"
        End Try
    End Function
    
    Public Sub showPencairan()
        Dim oClass As New Parameter.ApplicationDetailTransaction

        With oClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
        End With

        Try
            oClass = oController.getBungaNettOperatingLease(oClass)


            gvBungaNet.DataSource = oClass.Tabels.Tables(0)
            gvBungaNet.DataBind()

            gvPencairan.DataSource = oClass.Tabels.Tables(1)
            gvPencairan.DataBind()

            gvNetRateStatus.DataSource = oClass.Tabels.Tables(4)
            gvNetRateStatus.DataBind()


        Catch ex As Exception

        End Try

    End Sub
    Private Sub gvPencairan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPencairan.RowDataBound




        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblNilai As Label = CType(e.Row.Cells(1).FindControl("lblNilai"), Label)

            If e.Row.Cells(3).Text = "+" Then
                TotalPencairan += CDbl(lblNilai.Text)
            Else
                TotalPencairan -= CDbl(lblNilai.Text)
            End If


        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalPencairan As Label = CType(e.Row.Cells(1).FindControl("lblTotalPencairan"), Label)

            'lblTotalPencairan.Text = FormatNumber(Math.Round(TotalPencairan / 10, 0) * 10, 0)
            lblTotalPencairan.Text = FormatNumber(TotalPencairan, 0)


        End If
    End Sub
    Function getRateDetailTransaksi(TransAmount As Double) As Decimal
        Dim xrate As Decimal
        If Me.FirstInstallment = "AD" Then
            xrate = RateConvertion.GetEffectiveRateAdv(CInt(Tenor), CDbl(Me.InstallmentAmount), TransAmount, 1)
        Else
            xrate = RateConvertion.GetEffectiveRateArr(CInt(Tenor), CDbl(Me.InstallmentAmount), TransAmount, 1)
        End If

        Return Math.Round(Math.Abs(Me.EffectiveRate - xrate), 2)
    End Function
    Private Sub gvBungaNet_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBungaNet.RowDataBound


        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblRate As Label = CType(e.Row.Cells(2).FindControl("lblRate"), Label)


            If gvBungaNet.DataKeys(e.Row.RowIndex).Item("TransID").ToString <> "EFF" Then

                Dim nilai As Double = CType(e.Row.Cells(1).Text, Double)

                lblRate.Text = getRateDetailTransaksi(CDbl(lblNTF.Text) + nilai).ToString
            End If



            If e.Row.Cells(3).Text = "+" Then
                TotalBungaNett += CDec(lblRate.Text)
            Else
                TotalBungaNett -= CDec(lblRate.Text)
            End If



        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalRate As Label = CType(e.Row.Cells(2).FindControl("lblTotalRate"), Label)
            lblTotalRate.Text = Math.Round(TotalBungaNett, 2).ToString


        End If

    End Sub
    'Private Sub gridDistribusi_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridDistribusi.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        TotalInsGrid = TotalInsGrid + CDbl(CType(e.Item.FindControl("txtInsentif"), Label).Text)
    '        'TotalProsentase = TotalProsentase + CDbl(CType(e.Item.FindControl("lblProsentase"), Label).Text)

    '        CType(e.Item.FindControl("lblNo"), Label).Text = (1 + e.Item.ItemIndex).ToString
    '    End If

    '    If e.Item.ItemType = ListItemType.Footer Then
    '        CType(e.Item.FindControl("lblTotalInsentif"), Label).Text = FormatNumber(TotalInsGrid, 0)
    '        'CType(e.Item.FindControl("lblTotalProsentase"), Label).Text = FormatNumber(TotalProsentase, 0)
    '    End If

    'End Sub
    'Private Sub gridRincianTransaksi_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridRincianTransaksi.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        e.Item.Cells(10).Text = ucFlagTransaksi.cbo.Items.FindByValue(e.Item.Cells(10).Text.Trim).Text
    '        e.Item.Cells(11).Text = ucFlagTransaksi.cbo.Items.FindByValue(e.Item.Cells(11).Text.Trim).Text

    '        Dim LblTransID As Label
    '        Dim txtPersen As Label
    '        Dim txtTransAmount As Label

    '        LblTransID = CType(e.Item.FindControl("LblTransID"), Label)
    '        txtPersen = CType(e.Item.FindControl("txtPersen"), Label)
    '        txtTransAmount = CType(e.Item.FindControl("txtTransAmount"), Label)

    '        If Not LblTransID.Text.Substring(0, 2).Trim = "TP" Then
    '            txtPersen.Visible = False
    '        Else
    '            txtPersen.Text = FormatNumber(CStr(CDbl(txtTransAmount.Text.Replace(",", "")) / CDbl(lblPremiAsuransiGross.Text.Replace(",", "")) * 100))
    '        End If
    '    End If
    'End Sub    

#End Region
    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    If Me.Style.Trim = "ViewApplication" Then
    '        Response.Redirect("CreditProcess/NewApplication/ViewApplication.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&AgreementNo=" & Me.AgreementNo & "&CustomerName=" & Me.CustomerName & "&Style=ViewApplication&CustomerID=" & Me.CustomerID)
    '    Else
    '        Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    '    End If

    'End Sub

End Class