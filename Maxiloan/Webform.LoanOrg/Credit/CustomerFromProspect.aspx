﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerFromProspect.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerFromProspect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %> 
<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCustomerLookUpGlobal" Src="../../Webform.UserController/ucLookUpCustomerGlobal.ascx" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE"  TagPrefix="uc2" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>    
<%@ Register Src="CustomerNProspectTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <title>Konsumen Dari Prospek</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server"> 
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <uct:tabs id='cTabs' runat='server' ></uct:tabs>
        <asp:Label ID="lblMessage" runat="server" Visible="false" />
        <div class="form_title">
            <div class="title_strip" />
            <div class="form_single">
                <h3>KOMSUMEN DARI PROSPEK </h3>
            </div>
        </div> 
        <div class="form_box_title">
            <div class="form_single">
                <h5>MENCARI CUSTOMER</h5>
            </div>
        </div>
    
        <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ws">
                <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnSortCommand="Sorting" DataKeyField="Name" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="CREATE">
                            <ItemStyle CssClass="command_col" />
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.png" CommandName="CREATE" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                   <%-- <asp:TemplateColumn HeaderText="">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgEditPersonal" runat="server" CausesValidation="False" ImageUrl="../../Images/idcard.png" CommandName="EditPersonal" />
                        </ItemTemplate>
                    </asp:TemplateColumn>--%>
                      <asp:BoundColumn DataField="Name"  SortExpression="Name" HeaderText="NAMA" />
                   <%-- <asp:TemplateColumn HeaderText="NAMA" SortExpression="Name">
                        <ItemStyle HorizontalAlign="Left" Width="18%" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkCustomer" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>' />
                            <asp:Label ID="lblCust" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>' />
                        </ItemTemplate>
                    </asp:TemplateColumn>--%>
                    <asp:BoundColumn DataField="CustomerType" ItemStyle-HorizontalAlign="center" SortExpression="CustomerType" HeaderText="P/C" ItemStyle-CssClass="short_col" />
                    <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT" HeaderStyle-CssClass="widthAlamat" ItemStyle-CssClass="widthAlamat">
                        <ItemStyle CssClass="address_col" />
                    </asp:BoundColumn>
                     <asp:BoundColumn DataField="IDNumber"   HeaderText="NO. IDENTITAS" />
                     <asp:BoundColumn DataField="BirthDate"   HeaderText="TGL. LAHIR" DataFormatString="{0:dd/MM/yyyy}"/>
                    <asp:BoundColumn DataField="CustomerID" Visible="False" HeaderText="CustomerID" />
                    <asp:BoundColumn DataField="BranchFullName"   HeaderText="CABANG" HeaderStyle-CssClass="widthCabang" ItemStyle-CssClass="widthCabang" />
                    </Columns>
                </asp:DataGrid>
                <uc2:ucGridNav id="GridNavigator" runat="server"/>              
            </div>
        </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label> Nama</label>
                <%--<asp:DropDownList ID="cboSearch" runat="server" style="display:none">
                    <asp:ListItem Value="Name" Selected>Nama</asp:ListItem>
                </asp:DropDownList>--%>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
            <div class="form_right">    
                <label>Tanggal Lahir</label>
                <uc2:ucdatece id="txtSrcTglLahirNew" runat="server" />
            </div>
        </div>
        <div class="form_box">
        <div class="form_left">
            <label> No Identitas</label>
            <asp:TextBox ID="srcTxtIDTypeP" runat="server" Width="30%" MaxLength="50" />
        </div> 
        <div class="form_right"> 
        </div>
        </div>  
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue" CausesValidation="False" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
        </div> 
        </asp:Panel>
    </form>
</body>
</html>
