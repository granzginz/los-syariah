﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerGroupFleetList.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerGroupFleetList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CUSTOMER GROUP</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        DAFTAR CUSTOMER DARI FLEET (GROUP CUSTOMER)
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama Customer Group
                    </label>
                    <asp:Label ID="lblNamaCustomerGroup" runat="server" CssClass="medium_text"></asp:Label>
                    <asp:TextBox ID="txtNamaCustomerGroup" runat="server" Enabled='false' CssClass="medium_text"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Plafond</label>
                    <asp:Label ID="lblPlafond" runat="server"></asp:Label>
                    <asp:TextBox ID="txtPlafond" runat="server" Enabled='false' CssClass="label_currency" ></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Outstanding</label>
                    <asp:Label ID="lblOutstanding" runat="server"></asp:Label>
                    <asp:TextBox ID="txtOutstanding" runat="server" Enabled='false' CssClass="label_currency"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jumlah Kontrak</label>
                    <asp:Label ID="lblJmlKontrak" runat="server"></asp:Label>
                    <asp:TextBox ID="txtJmlKontrak" runat="server" Enabled='false' CssClass="label_currency"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_general"  >
                        Catatan</label>
                    <asp:Label ID="lblCatatan" runat="server"></asp:Label>
                    <textarea id="txtCatatan" runat="server" cols="17" rows="5" disabled="True" class="multiline_textbox_uc"></textarea>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="CustomerID" CssClass="grid_general">
                        <ItemStyle CssClass="item_grid" />
                        <HeaderStyle CssClass="th" />
                        <Columns>
                            <asp:BoundColumn DataField="CustomerID" SortExpression="CustomerID" HeaderText="GIF"
                                ItemStyle-Width="100px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="NAMA CUSTOMER"
                                ItemStyle-CssClass="name_col"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomerType" SortExpression="CustomerType" HeaderText="JENIS"
                                ItemStyle-CssClass="command_col"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Alamat" SortExpression="Alamat" HeaderText="ALAMAT">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnBackMenu" runat="server" CausesValidation="False" text = "Back" CssClass ="small button gray" />                
            </div>
            <input id="hdnCustomerGroupID" type="hidden" name="hdnCustomerGroupID" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
