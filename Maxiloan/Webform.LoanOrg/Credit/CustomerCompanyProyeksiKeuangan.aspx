﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerCompanyProyeksiKeuangan.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerCompanyProyeksiKeuangan" %>

<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="CustomerTabsCompany.ascx" TagName="tabs" TagPrefix="uct" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Company</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
</head>

<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div runat="server" id="jlookupContent" />
        <uct:tabs id='cTabs' runat='server'></uct:tabs> 
        <asp:UpdatePanel runat="server" ID="updatePanel1">
            <ContentTemplate>
                <div class="form_title">
                    <div class="title_strip"></div>
                    <div class="form_single">
                    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                        <h4>
                            <asp:Label ID="lblTitle" runat="server"></asp:Label>
                            CORPORATE CUSTOMER
                        </h4>
                    </div>
                </div>
                <%-- GRID HISTORY KEUANGAN --%>
                <asp:panel id="pnlList" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                          HISTORI ANALISA PROYEKSI KEUANGAN
                        </h4>
                    </div>
                </div>
                <div class="form_box" >
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="SeqNo" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="10%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="SeqNo" SortExpression="SeqNo" HeaderText="SeqNo">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="TglLaporanKeuangan" SortExpression="TglLaporanKeuangan" DataFormatString="{0:dd/MM/yyyy}" HeaderText="TGL LAPORAN">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AssetIDR" SortExpression="AssetIDR" HeaderText="ASSET IDR">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Liabilitas" SortExpression="Liabilitas" HeaderText="LIABILITAS">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Ekuitas" SortExpression="Ekuitas" HeaderText="EKUITAS">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                           <uc2:ucGridNav id="GridNav" runat="server"/>
                            </div>
                        </div>
                    </div>
                    <div class="form_button">
                        <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                        </asp:Button>
                    </asp:panel>
                <%-- ********************* --%>

                <asp:panel id="pnlAdd" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            ANALISA PROYEKSI KEUANGAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Tgl Proyeksi Keuangan</label>
                        <uc1:ucDateCE id="TglProyeksi" runat="server"></uc1:ucDateCE>
                    </div>
                </div>
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Asset IDR</label>		
		                <uc1:ucNumberFormat id="AssetIDR" runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>		
                <div class="form_box_header">
                    <div class="form_single">
                        <label>
                            <b><i>
                                ASSET LANCAR
                            </i></b>                            
                        </label>
                    </div>
                </div>               
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Asset Lancar</label>		
		                <uc1:ucNumberFormat id=	"AssetLancar"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Kas dan Setara Kas (L)</label>		
		                <uc1:ucNumberFormat id=	"KasSetaraKasAsetLancar"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Piutang Usaha/Pembiayaan (L)</label>		
		                <uc1:ucNumberFormat id=	"PiutangUsahAsetLancar"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Investasi/Aset Keuangan Lainnya (L) </label>		
		                <uc1:ucNumberFormat id=	"InvestasiLainnyaAsetLancar"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Aset Lancar Lainnya</label>		
		                <uc1:ucNumberFormat id=	"AsetLancarLainnya"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>			
                <div class="form_box_header">
                    <div class="form_single">
                        <label>
                            <b><i>
                                ASSET TIDAK LANCAR
                            </i></b>                            
                        </label>
                    </div>
                </div>                
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Aset Tidak Lancar</label>		
		                <uc1:ucNumberFormat id=	"AsetTidakLancar"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Piutang Usaha/Pembiayaan (TL)</label>		
		                <uc1:ucNumberFormat id="PiutangUsahaAsetTidakLancar"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Investasi/Aset Keuangan Lainnya (TL)</label>		
		                <uc1:ucNumberFormat id="InvestasiLainnyaAsetTidakLancar"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Aset Tidak Lancar Lainnya</label>		
		                <uc1:ucNumberFormat id="AsetTidakLancarLainnya"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>		
                <div class="form_box_header">
                    <div class="form_single">
                        <label>
                            <b><i>
                                LIABILITAS
                            </i></b>                            
                        </label>
                    </div>
                </div>                  
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Liabilitas</label>		
		                <uc1:ucNumberFormat id="Liabilitas"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Liabilitas Jangka Pendek</label>		
		                <uc1:ucNumberFormat id=	"LiabilitasJangkaPendek"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Pinjaman Jangka Pendek</label>		
		                <uc1:ucNumberFormat id=	"PinjamanJangkaPendek"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Utang Usaha Jangka Pendek</label>		
		                <uc1:ucNumberFormat id=	"UtangUsahaJangkaPendek"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Liabilitas Jangka Pendek Lainnya</label>		
		                <uc1:ucNumberFormat id=	"LiabilitasJangkaPendekLainnya"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Liabilitas Jangka Panjang</label>		
		                <uc1:ucNumberFormat id=	"LiabilitasJangkaPanjang"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Pinjaman Jangka Panjang</label>		
		                <uc1:ucNumberFormat id=	"PinjamanJangkaPanjang"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Utang Usaha Jangka Panjang </label>		
		                <uc1:ucNumberFormat id=	"UtangUsahaJangkaPanjang"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Liabilitas Jangka Panjang Lainnya</label>		
		                <uc1:ucNumberFormat id=	"LiabilitasJangkaPanjangLainnya"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>	
                <div class="form_box_header">
                    <div class="form_single">
                        <label>
                            <b><i>
                                EKUITAS
                            </i></b>                            
                        </label>
                    </div>
                </div>                
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Ekuitas</label>		
		                <uc1:ucNumberFormat id=	"Ekuitas"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Pendapatan Usaha/Operasional</label>		
		                <uc1:ucNumberFormat id=	"PendapatanUsahaOpr"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Beban Pokok Pendapatan/Beban Operasional</label>		
		                <uc1:ucNumberFormat id=	"BebanPokokPendapatanOpr"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Laba/Rugi Bruto</label>		
	                    <asp:Textbox id="txtLabaRugiBruto" runat="server" onkeyup = "javascript:this.value=Comma(this.value);" CssClass="numberAlign" OnChange="javascript:void()">0</asp:Textbox>
                        <asp:RangeValidator runat="server" ID="RangeValidator1" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                            ControlToValidate="txtLabaRugiBruto" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                            Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                            ControlToValidate="txtLabaRugiBruto" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                            ControlToValidate="txtLabaRugiBruto" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-,]*$"></asp:RegularExpressionValidator>       
                    </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Pendapatan Lain-lain/Non Opr </label>		
		                <uc1:ucNumberFormat id=	"PLLNonOpr"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Beban Lain-lain/Non Operasional </label>		
		                <uc1:ucNumberFormat id=	"BebanLainLainNonOpr"	runat="server"></uc1:ucNumberFormat>
	                </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Laba/Rugi Sebelum Pajak </label>				                
	                    <asp:Textbox id="txtLabaRugiSebelumPajak" runat="server" onkeyup = "javascript:this.value=Comma(this.value);" CssClass="numberAlign" OnChange="javascript:void()">0</asp:Textbox>
                        <asp:RangeValidator runat="server" ID="rv" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                            ControlToValidate="txtLabaRugiSebelumPajak" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                            Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator runat="server" ID="rfv" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                            ControlToValidate="txtLabaRugiSebelumPajak" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                            ControlToValidate="txtLabaRugiSebelumPajak" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-,]*$"></asp:RegularExpressionValidator>
                    </div>			
                </div>				
                <div class="form_box">				
	                <div class="form_single">			
		                <label>Laba/Rugi Tahun Berjalan</label>		
	                    <asp:Textbox id="txtLabaRugiTahunBerjalan" runat="server" onkeyup = "javascript:this.value=Comma(this.value);" CssClass="numberAlign" OnChange="javascript:void()">0</asp:Textbox>
                        <asp:RangeValidator runat="server" ID="RangeValidator2" Display="Dynamic" ErrorMessage="Input hanya boleh -999999999999999 s/d 999999999999999"
                            ControlToValidate="txtLabaRugiTahunBerjalan" MaximumValue="999999999999999" MinimumValue="-999999999999999"
                            Type="Currency" Enabled="false" CssClass="validator_general"></asp:RangeValidator>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" Display="Dynamic" ErrorMessage="Input ini harus diisi!"
                            ControlToValidate="txtLabaRugiTahunBerjalan" CssClass="validator_general" Enabled="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                            ControlToValidate="txtLabaRugiTahunBerjalan" ErrorMessage="Please Enter Only Numbers" CssClass="validator_general" ValidationExpression="^[0-9-,]*$"></asp:RegularExpressionValidator>       
	                
                    </div>			
                </div>	
                 <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False" />
                </div> 
                </asp:panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    <script>
        function Comma(Num) { //function to add commas to textboxes
        Num += '';
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
        x = Num.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1))
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        return x1 + x2;
        };
    </script>
    </form>
</body>
</html>
