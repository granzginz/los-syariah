﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerPersonal.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerPersonal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../../webform.UserController/ucMonthCombo.ascx" TagName="ucMonthCombo"
    TagPrefix="uc2" %>
<%@ Register Src="../../webform.UserController/ucLookupGroupCust.ascx" TagName="ucLookupGroupCust"
    TagPrefix="uc5" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>

<%@ Register Src="CustomerTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Personal</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function OpenLookup() {
            $('#dialog').remove();

            $('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="../../webform.UserController/jLookup/ZipCode.aspx" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');

            $('#dialog').dialog({
                title: 'Daftar Kode POS',
                bgiframe: false,
                width: 1024,
                height: 550,
                resizable: false,
                modal: true,
                closeOnEscape: true,
                draggable: true
            });
        }
        function CloseDialog() {
            $('#dialog').dialog('close'); return false;
        }
        function isiData(kelurahan, kecamatan, zipcode, kota) {
            frm = document.forms[0];
            frm.UcLegalAddress_oLookUpZipCode_txtKelurahan.value = kelurahan;
            frm.UcLegalAddress_oLookUpZipCode_txtKecamatan.value = kecamatan;
            frm.UcLegalAddress_oLookUpZipCode_txtCity.value = kota;
            frm.UcLegalAddress_oLookUpZipCode_txtZipCode.value = zipcode;
            CloseDialog()
            return false;
        }
        function Nationality() {
            if (document.forms[0].cboPNationality.value == 'WNA') {
                document.forms[0].txtPWNA.disabled = false;
            }
            else {
                document.forms[0].txtPWNA.disabled = true;
                document.forms[0].txtPWNA.value = '';
            }
        }

        var Page_Validators = new Array();

        function LegalAddress() {
            ds = document.forms[0].UcLegalAddress_txtAddress.value;
            document.forms[0].UcResAddress_txtAddress.value = document.forms[0].UcLegalAddress_txtAddress.value;
            document.forms[0].UcResAddress_txtRT.value = document.forms[0].UcLegalAddress_txtRT.value;
            document.forms[0].UcResAddress_txtRW.value = document.forms[0].UcLegalAddress_txtRW.value;
            document.forms[0].UcResAddress_oLookUpZipCode_txtKelurahan.value = document.forms[0].UcLegalAddress_oLookUpZipCode_txtKelurahan.value;
            document.forms[0].UcResAddress_oLookUpZipCode_txtKecamatan.value = document.forms[0].UcLegalAddress_oLookUpZipCode_txtKecamatan.value;
            document.forms[0].UcResAddress_oLookUpZipCode_txtCity.value = document.forms[0].UcLegalAddress_oLookUpZipCode_txtCity.value;
            document.forms[0].UcResAddress_oLookUpZipCode_txtZipCode.value = document.forms[0].UcLegalAddress_oLookUpZipCode_txtZipCode.value;
            document.forms[0].UcResAddress_txtAreaPhone1.value = document.forms[0].UcLegalAddress_txtAreaPhone1.value;
            document.forms[0].UcResAddress_txtPhone1.value = document.forms[0].UcLegalAddress_txtPhone1.value;
            document.forms[0].UcResAddress_txtAreaPhone2.value = document.forms[0].UcLegalAddress_txtAreaPhone2.value;
            document.forms[0].UcResAddress_txtPhone2.value = document.forms[0].UcLegalAddress_txtPhone2.value;
            document.forms[0].UcResAddress_txtAreaFax.value = document.forms[0].UcLegalAddress_txtAreaFax.value;
            document.forms[0].UcResAddress_txtFax.value = document.forms[0].UcLegalAddress_txtFax.value;
            return false;
        }
        function cboPHomeStatus_IndexChanged(e) {
            if (e == 'KR') {
                $('#divTglSelesaiKontrak').css("visibility", "");
                $('#txtRentFinish_txtDateCE').removeAttr("style");

                if ($('#divTglSelesaiKontrak').valueOf != '') {
                    ValidatorEnable(txtRentFinish_rfvDateCE, true);
                    $('#txtRentFinish_rfvDateCE').hide();
                }
                else {
                    ValidatorEnable(txtRentFinish_rfvDateCE, true);
                    $('#txtRentFinish_rfvDateCE').show();
                }
            }
            else {
                $('#divTglSelesaiKontrak').css("visibility", "hidden");
                $('#txtRentFinish_txtDateCE').attr("style", "display : none");
                ValidatorEnable(txtRentFinish_rfvDateCE, false);
                $('#txtRentFinish_rfvDateCE').hide();
            }
        }
    </script>
    <style >
        .multiline_textbox {
            width: 60% !important;
        }
    </style>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="content">
    </div>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
      <uct:tabs id='cTabs' runat='server'></uct:tabs> 
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4> <asp:Label ID="lblTitle" runat="server"></asp:Label> IDENTITAS </h4>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlIdentitas">
                <div class="form_box">
                    <div class="form_left">
                        <label>Jenis Identitas</label>
                        <asp:Label ID="lblPIDType" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label> Nomor Identitas</label>
                        <asp:Label ID="lblPIDNumber" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Nama Konsumen</label>
                        <asp:TextBox ID="txtGelar" runat="server" placeholder="gelar depan" CssClass="smaller_text"></asp:TextBox>
                        <asp:Label ID="lblPName" runat="server" CssClass="label_auto"></asp:Label>
                        <asp:TextBox ID="txtCustomerName" Visible="false" runat="server" placeholder="nama"></asp:TextBox>
                        <asp:TextBox ID="txtGelarBelakang" runat="server" MaxLength="50" placeholder="gelar belakang"
                            CssClass="smaller_text"></asp:TextBox>
                    </div>
                    <div class="form_right">
                    <asp:UpdatePanel runat="server" ID="upGroupKonsumen">
                    <ContentTemplate> 
                        <label> Group Konsumen</label>
                        <asp:TextBox ID="txtKelompokUsaha" Enabled="false" runat="server"></asp:TextBox>
                        <asp:Button ID="btnLookupCustomer" runat="server" CausesValidation="False" Text="..."
                            CssClass="small buttongo blue" />
                        <uc5:uclookupgroupcust id="ucLookupGroupCust1" runat="server" oncatselected="CatSelectedCustomer"></uc5:uclookupgroupcust> 
                    </ContentTemplate>
                </asp:UpdatePanel>

                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label> Tempat / Tanggal Lahir</label>
                        <asp:Label ID="lblPBirthPlace" runat="server"></asp:Label>,
                        <asp:Label ID="lblPBirthDate" runat="server"></asp:Label>
                    </div>
                 
                     <div class="form_right">
                         <label class="label_general"> Nasabah Operating Lease</label>                                                   
                         <asp:CheckBox ID="cboIsOLS" runat="server"></asp:CheckBox>     
                     </div>
                </div>

                <div class="form_box">
                     <div class="form_left">
                        <label>
                            Jenis Kelamin</label>
                        <asp:Label ID="lblPGender" runat="server"></asp:Label>
                    </div>

                    <div class="form_right">
                         <label class="label_req">Jenis Asset Dibiayai&nbsp;</label>  
                         <asp:DropDownList ID="cboJenisPembiayaan" runat="server"  width='300px'/> 
                          <asp:RequiredFieldValidator ID="Requiredfieldvalidator66" runat="server"  ControlToValidate="cboJenisPembiayaan" CssClass="validator_general" ErrorMessage="Harap pilih Jenis Pembiayaan" InitialValue="SelectOne"></asp:RequiredFieldValidator> 
                     </div>

                </div>


                


                <div class="form_box_title">
                    <div class="form_box_header_l">
                        <h4>
                            ALAMAT SESUAI KTP</h4>
                    </div>
                    <div class="form_box_header_r">
                        <h4>
                            ALAMAT TEMPAT TINGGAL</h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_box_single">
                        <asp:Button ID="btnPLegalAddress" CausesValidation="false" runat="server" Text="Copy Alamat KTP"
                            CssClass="small buttongo blue label_calc"></asp:Button>
                    </div>
                </div>
                <div class="form_box_uc">
                    <div class="form_left_uc">
                        <asp:UpdatePanel runat="server" ID="upLegalAddr">
                            <ContentTemplate>
                                <uc1:ucaddress id="UcLegalAddress" runat="server"></uc1:ucaddress>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form_right_uc">
                        <asp:UpdatePanel ID="upResAddress" runat="server">
                            <ContentTemplate>
                                <uc1:ucaddress id="UcResAddress" runat="server"></uc1:ucaddress>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <asp:UpdatePanel runat="server" ID="up1">
                    <ContentTemplate>
                        <div class="form_box">
                            <div class="form_single">
                                <label class="label_req">
                                    No Handphone 1</label>
                                <asp:TextBox ID="txtPMobile" runat="server"  MaxLength="12" onkeypress="return numbersonly2(event)"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ControlToValidate="txtPMobile" ErrorMessage="Harap isi No Handphone"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                                    ControlToValidate="txtPMobile" ErrorMessage="No HandPhone Salah" ValidationExpression="\d*"
                                    CssClass="validator_general"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_single">
                                <label>
                                    No Handphone 2</label>
                                <asp:TextBox ID="txtPMobile1" runat="server"  MaxLength="12" onkeypress="return numbersonly2(event)"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                                    ControlToValidate="txtPMobile1" ErrorMessage="No HandPhone Salah" ValidationExpression="\d*"
                                    CssClass="validator_general"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form_box_title_nobg">
                            <div class="form_left">
                                <label class="label_req">
                                    Agama</label>
                                <asp:DropDownList ID="cboPReligion" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                    InitialValue="Select One" ControlToValidate="cboPReligion" ErrorMessage="Harap pilih Agama"
                                    CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="form_right">
                                <label class="label_req">
                                    Status Rumah</label>
                                <asp:DropDownList ID="cboPHomeStatus" runat="server" onchange="cboPHomeStatus_IndexChanged(this.value);">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                    InitialValue="Select One" ControlToValidate="cboPHomeStatus" ErrorMessage="Harap pilih Status Rumah"
                                    CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form_box">
                            <div class="form_left">
                                <label class="label_req">
                                    Status Perkawinan</label>
                                <asp:DropDownList ID="cboPMarital" Enabled="true" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                    InitialValue="Select One" ControlToValidate="cboPMarital" ErrorMessage="Harap pilih Status Perkawinan"
                                    CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="form_right">
                                <div runat="server" id="divTglSelesaiKontrak">
                                    <label>
                                        Tanggal Selesai Kontrak</label>
                                    <uc6:ucdatece id="txtRentFinish" runat="server"></uc6:ucdatece>
                                    <asp:Label ID="lblVRent" runat="server" Visible="False" CssClass="validator_general">Tanggal harus lebih besar dari hari ini</asp:Label>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jumlah Tanggungan</label>
                        <uc1:ucnumberformat id="txtPDependentNum" runat="server"></uc1:ucnumberformat>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Tinggal sejak Tahun</label>
                        <uc2:ucmonthcombo id="ucTinggalSejakBulan" runat="server" />
                        <asp:TextBox ID="txtPStaySince" runat="server" MaxLength="4" Columns="7"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" ControlToValidate="txtPStaySince"
                            ErrorMessage="RangeValidator" Type="Integer" MinimumValue="0">Tahun Harus lebih kecil = today!</asp:RangeValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                            Display="Dynamic" ControlToValidate="txtPStaySince" ErrorMessage="Harap isi dengan 4 Angka"
                            ValidationExpression="\d{4}" CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Kewarganegaraan</label>
                        <asp:DropDownList ID="cboPNationality" runat="server" onchange="Nationality()">
                            <asp:ListItem Value="WNI">WNI</asp:ListItem>
                            <asp:ListItem Value="WNA">WNA</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form_right">
                        <label class="label_req">
                            Kondisi Rumah</label>
                        <asp:DropDownList ID="cboPHomeLocation" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboPHomeLocation" ErrorMessage="Harap pilih Lokasi Rumah"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            WNA Negara</label>
                        <asp:TextBox ID="txtPWNA" runat="server" Width="30%" MaxLength="50"></asp:TextBox>
                        <asp:Label ID="lblVWNA" runat="server" Visible="False">Harap isi dengan negara WNA</asp:Label>
                        <div id="Div1" runat="server">
                        </div>
                    </div>
                    <div class="form_right">
                        <label>
                            Kondisi Lingkungan</label>
                        <asp:DropDownList ID="ddlKondisiRumah" runat="server">
                            <asp:ListItem Text="Sederhana" Value="Sederhana" />
                            <asp:ListItem Text="Menengah" Value="Menengah" />
                            <asp:ListItem Text="Mewah" Value="Mewah" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Berlaku Hingga</label>
                        <asp:Label runat="server" ID="lblIDBelakuSD"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Harga Rumah</label>
                        <uc1:ucnumberformat id="txtHargaRumah" runat="server"></uc1:ucnumberformat>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Nama Ibu Kandung</label>
                        <asp:TextBox ID="txtPMotherName" runat="server" MaxLength="50" Enabled="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" Display="Dynamic"
                            ControlToValidate="txtPMotherName" ErrorMessage="Harap isi dengan Nama Ibu Kandung"
                            CssClass="validator_general"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                ID="Regularexpressionvalidator8" runat="server" ControlToValidate="txtPMotherName"
                                ErrorMessage="Salah" ValidationExpression="[^0-9]{3,}" CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form_right">
                        <label class ="label_req">
                            NPWP Pribadi</label>
                        <asp:TextBox ID="txtPNPWP" runat="server" Width="30%" MaxLength="16" onkeypress="return numbersonly2(event)" ></asp:TextBox>

                       <%-- modify by amri 2017-01-18--%> <%-- modify by ario 2019-10-04--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            ControlToValidate="txtPNPWP" ErrorMessage="Harap isi No NPWP"
                            CssClass="validator_general"></asp:RequiredFieldValidator>

                        <asp:RegularExpressionValidator ID="RegtxtPNPWP" Display="Dynamic" ControlToValidate="txtPNPWP"
                            runat="server" ErrorMessage="Masukkan angka saja" ValidationExpression="\d+"
                            CssClass="validator_general"></asp:RegularExpressionValidator>

                       <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboPHomeLocation" ErrorMessage="Harap pilih Lokasi Rumah"
                            CssClass="validator_general"></asp:RequiredFieldValidator>--%>

                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            E-Mail</label>
                        <asp:TextBox ID="txtPEmail" runat="server" Width="30%" MaxLength="30"></asp:TextBox>
                       <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic" ControlToValidate="txtPEmail"
                            ErrorMessage="Email Salah" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            CssClass="validator_general"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Punya SPT Tahunan</label>
                        <asp:RadioButtonList ID="rboSPTTahunan" runat="server" RepeatDirection="Horizontal"
                            CssClass="opt_single">
                            <asp:ListItem Value="True">Ya</asp:ListItem>
                            <asp:ListItem Selected="True" Value="False">Tidak</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Pendidikan</label>
                        <asp:DropDownList ID="cboPEducation" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="Select One" ControlToValidate="cboPEducation" ErrorMessage="Harap pilih Pendidikan"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form_right">
                        <label>
                            No Kartu Keluarga</label>
                        <asp:TextBox ID="txtPNoKK" runat="server" MaxLength="20" Enabled="true" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    </div>
                </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnPSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnPCancel" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
