﻿
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Globalization


Public Class CustomerFromProspect
    Inherits Maxiloan.Webform.WebBased 
    Protected WithEvents txtSrcTglLahirNew As ucDateCE

    Private m_controller As New CustomerController
    Private currentPage As Integer = 1
    Private pageSize As Integer = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Integer = 1

    Private m_controllerApp As New ApplicationController
    Private ocustomclass As New Parameter.Application

    Dim filterBranch As String
    Dim oCustomer As New Parameter.Customer
    Protected WithEvents GridNavigator As ucGridNav
     
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property 
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        filterBranch = " ProspectAppID like '" & Replace(Me.sesBranchId, "'", "") & "%' "
        If Not Me.IsPostBack Then 
            cTabs.SetNavigateUrl(Request("page"), Request("id"))
            cTabs.RefreshAttr(Request("pnl"))
            If CheckForm(Me.Loginid, "Customer", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            If IsSingleBranch() And Me.IsHoBranch = False Then
                Me.Sort = "Name ASC"

                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = filterBranch
                End If

                BindGridEntity()
  
                txtSrcTglLahirNew.IsRequired = False
            Else 
                Dim strHTTPServer = Request.ServerVariables("PATH_INFO") 
                Dim StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & Request.ServerVariables("SERVER_NAME") & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            End If

        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True 
        lblMessage.Visible = False
    End Sub

    Sub BindGridEntity(Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.Customer

        InitialDefaultPanel()
        oCustomClass.PageSize = CType(pageSize, Int16)
        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetCustomerFromProspect(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = CType(oCustomClass.totalrecords, Integer)
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()

        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub
    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs) 
        currentPage = e.CurrentPage
        BindGridEntity(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName.ToUpper = "CREATE" Then
            If e.Item.Cells(2).Text.Trim = "P" Then
                'Response.Redirect("Customer.aspx?page=Edit&prospect=" & e.Item.Cells(6).Text.Trim & "&pnl=tabProspect")
                'Modify by Wira 20171027
                Response.Redirect("Customer.aspx?page=Edit&prospect=" & e.Item.Cells(6).Text.Trim & "&pnl=tabProspect&custtype=P")
            End If
            'Modify by Wira 20171027
            If e.Item.Cells(2).Text.Trim = "C" Then
                Response.Redirect("Customer.aspx?page=Edit&prospect=" & e.Item.Cells(6).Text.Trim & "&pnl=tabProspect&custtype=C")
            End If
        End If
    End Sub


    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        Me.Sort = String.Format("{0} {1} ", e.SortExpression, IIf(InStr(Me.Sort, "DESC") > 0, "", "DESC"))
        BindGridEntity()
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Me.CmdWhere = filterBranch

        If Not (txtSearch.Text.Trim = String.Empty) Then
            Me.CmdWhere = String.Format("{0} and name LIKE '%{1}%' ", Me.CmdWhere, Replace(txtSearch.Text.Trim, "'", "''"))
        End If

        If Not (srcTxtIDTypeP.Text.Trim = String.Empty) Then
            Me.CmdWhere = String.Format("{0} and idNumber like '%{1}%'", Me.CmdWhere, srcTxtIDTypeP.Text.Trim)
        End If

        If Not (txtSrcTglLahirNew.Text.Trim = String.Empty) Then
            Me.CmdWhere = String.Format("{0} and Birthdate ='{1}'", Me.CmdWhere, ConvertDate2(txtSrcTglLahirNew.Text.Trim).ToString("yyyyMMdd"))
        End If

        BindGridEntity()
    End Sub

#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtSearch.Text = String.Empty
        Me.CmdWhere = filterBranch
        BindGridEntity()
    End Sub
#End Region

End Class