﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAsset.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewAsset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAsset</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - ASSET
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:LinkButton ID="lblCustomerName" runat="server" EnableViewState="False"></asp:LinkButton>
            </div>
            <div class="form_right">
                <label>
                    No Aplikasi
                </label>
                <asp:Label ID="lblAppID" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Supplier
                </label>
                <asp:Label ID="lblSupplier" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <%--<div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Skema Incentive Supplier
                </label>
                <asp:Label ID="lblSSI" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>--%>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                DATA ASSET
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Asset
                </label>
                <asp:Label ID="lblAssetDesc" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Asset : Used/New
                </label>
                <asp:Label ID="lblUsedNew" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    No Polisi
                </label>
                <asp:Label ID="lblNoPolisi" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Penggunaan
                </label>
                <asp:Label ID="lblUsage" runat="server" ForeColor="Black" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Warna
                </label>
                <asp:Label ID="lblWarna" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tahun Produksi
                </label>
                <asp:Label ID="lblYear" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblS1" runat="server"></asp:Label>
                </label>
                <asp:Label ID="lblSerial1" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblS2" runat="server"></asp:Label>
                </label>
                <asp:Label ID="lblSerial2" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="LblLicPlateText" runat="server" Visible="False"></asp:Label>
                </label>
                <asp:Label ID="LblLicPlateValue" runat="server" ForeColor="Black" Visible="False"></asp:Label>
            </div>
            <div class="form_right">
                
            </div>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgAttribute" runat="server" ShowHeader="False" AutoGenerateColumns="False"
                EnableViewState="False" BorderWidth="0px" CellPadding="2" CellSpacing="1" CssClass="grid_general"
                Width="100%">
                <ItemStyle CssClass="item_grid" />
                <Columns>
                    <asp:BoundColumn DataField="Name">
                        <ItemStyle Width="25%" CssClass="tdgenap"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="AttributeContent">
                        <ItemStyle Width="75%" CssClass="tdganjil"></ItemStyle>
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                REGISTRASI ASSET
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama
                </label>
                <asp:Label ID="lblRegName" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Alamat
                </label>
                <asp:Label ID="lblRegAddress" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    RT/RW
                </label>
                <asp:Label ID="lblRegRT" runat="server" EnableViewState="False"></asp:Label>&nbsp;/
                <asp:Label ID="lblRegRW" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kelurahan
                </label>
                <asp:Label ID="lblRegKelurahan" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kecamatan
                </label>
                <asp:Label ID="lblRegKecamatan" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kota
                </label>
                <asp:Label ID="lblRegCity" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kode Pos
                </label>
                <asp:Label ID="lblRegZip" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>                    
                    Tgl Pajak STNK
                </label>
                <asp:Label ID="lblRegTaxDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Catatan
                </label>
                <asp:Label ID="lblRegNotes" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <%--<div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Asuransi Oleh
                </label>
                <asp:Label ID="lblInsInsured" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>--%>
    <%--<div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Dibayar Oleh
                </label>
                <asp:Label ID="lblInsPaid" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>--%>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DOKUMEN ASSET
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgAssetDoc" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                CellPadding="3" EnableViewState="False" CellSpacing="1" CssClass="grid_general"
                Width="100%">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="Document" HeaderText="DOKUMEN">
                        <HeaderStyle Width="20%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Number" HeaderText="NO DOKUMEN">
                        <HeaderStyle Width="30%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Checked" HeaderText="PERIKSA">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Notes" HeaderText="CATATAN">
                        <HeaderStyle Width="30%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="left"></ItemStyle>
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>    
    <div class="form_button">                
            <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server" Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray" CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
