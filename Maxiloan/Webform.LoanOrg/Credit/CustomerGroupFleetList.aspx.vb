﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
#End Region

Public Class CustomerGroupFleetList
    Inherits Maxiloan.Webform.WebBased

    Private m_controller As New CustomerGroupController    
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property CustomerGroupID() As String
        Get
            Return CType(ViewState("CustomerGroupID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerGroupID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then

            'Me.FormID = "CustGroupFleet"
            Me.FormID = "CustomerGroup"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                Me.Sort = "CustomerID ASC"
                If (CustomerGroupID Is Nothing) Then
                    If hdnCustomerGroupID.Value <> "" Then
                        CustomerGroupID = hdnCustomerGroupID.Value
                    Else
                        CustomerGroupID = Request("id")
                    End If
                End If


                If CustomerGroupID <> "" Then
                    Me.CmdWhere = " CustomerGroupID  = '" + CustomerGroupID.Trim() + "'".Trim()
                Else
                    Me.CmdWhere = "ALL"
                End If
                BindGridEntity(Me.CmdWhere)
                BindForm()
            End If
        End If

    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomerGroup As New Parameter.CustomerGroup        
        oCustomerGroup.strConnection = GetConnectionString()
        oCustomerGroup.WhereCond = cmdWhere
        oCustomerGroup.SortBy = Me.Sort
        oCustomerGroup = m_controller.GetCustomerGroupListCustomer(oCustomerGroup)

        If Not oCustomerGroup Is Nothing Then
            dtEntity = oCustomerGroup.Listdata
            recordCount = oCustomerGroup.Totalrecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.DataBind()        
    End Sub
    Sub BindForm()        
        Dim oCustomerGroup As New Parameter.CustomerGroup
        oCustomerGroup.strConnection = GetConnectionString()
        oCustomerGroup.CustomerGroupID = CustomerGroupID
        oCustomerGroup = m_controller.GetCustomerGroupList(oCustomerGroup)
        txtNamaCustomerGroup.Text = oCustomerGroup.CustomerGroupNm
        'txtPlafond.Text = oCustomerGroup.Plafond.ToString
        'txtOutstanding.Text = oCustomerGroup.Oustanding.ToString
        txtPlafond.Text = FormatNumber(oCustomerGroup.Plafond.ToString, 2)
        txtOutstanding.Text = FormatNumber(oCustomerGroup.Oustanding.ToString, 2)
        txtJmlKontrak.Text = oCustomerGroup.Kontrak
        txtCatatan.InnerText = oCustomerGroup.Catatan
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("CustomerGroup")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("CustomerGroup")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbBackMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackMenu.Click
        Response.Redirect("CustomerGroup.aspx")
    End Sub
End Class