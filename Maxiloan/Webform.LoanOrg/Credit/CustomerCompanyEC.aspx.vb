﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Public Class CustomerCompanyEC
    Inherits AbsCustomerCompany 
    Protected WithEvents UCAddressPJ As ucAddress
    Private time As String

#Region "Property"    
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            If Request("prospectappid") = "" Then Me.ProspectAppID = "-"
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
            FillCbo()

            'Modify by Wira 20171023
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------

            Me.PageAddEdit = Request("page")
            If Me.PageAddEdit = "Add" Then
                GetXML()
                lblTitle.Text = "ADD"
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                cTabs.SetNavigateUrl(Request("page"), Request("id"))
            End If

            cTabs.RefreshAttr(Request("pnl")) 
        End If


    End Sub

    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ocustomer As New Parameter.CompanyCustomerEC 
        Try
            ocustomer.CustomerID = Me.CustomerID 
            ocustomer.Name = txtNamaPJ.Text
            ocustomer.Relation = cboHubungan.SelectedValue
            ocustomer.HP = txtHP.Text
            ocustomer.Email = txtEmail.Text
            ocustomer.Alamat = New Parameter.Address With {
                .Address = UCAddressPJ.Address,
                .RT = UCAddressPJ.RT,
                .RW = UCAddressPJ.RW,
                .Kelurahan = UCAddressPJ.Kelurahan,
                .Kecamatan = UCAddressPJ.Kecamatan,
                .City = UCAddressPJ.City,
                .ZipCode = UCAddressPJ.ZipCode,
                .AreaPhone1 = UCAddressPJ.AreaPhone1,
                .Phone1 = UCAddressPJ.Phone1,
                .AreaPhone2 = UCAddressPJ.AreaPhone2,
                 .Phone2 = UCAddressPJ.Phone2,
                .AreaFax = UCAddressPJ.AreaFax,
                .Fax = UCAddressPJ.Fax
            }
            Dim Err As String = m_controller.CompanyCustomerECSaveEdit(GetConnectionString(), ocustomer)
            If Err = "" Then
                'Response.Redirect("CustomerCompanyKeuangan.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabKeuangan")
                'Modify by WIra 20171023
                If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                    Response.Redirect("CustomerCompanyKeuangan.aspx?page=Add&id=" + Me.CustomerID + "&pnl=tabKeuangan&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
                Else
                    Response.Redirect("CustomerCompanyKeuangan.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabKeuangan&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
                End If
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
    Sub BindEdit(ByVal id As String)
        Dim oCustomer As New Parameter.CompanyCustomerEC 
        oCustomer.CustomerID = id  
        oCustomer = m_controller.GetCompanyCustomerEC(GetConnectionString(), oCustomer)
        Me.CustomerID = id
        txtNamaPJ.Text = oCustomer.Name
        cboHubungan.SelectedIndex = cboHubungan.Items.IndexOf(cboHubungan.Items.FindByValue(oCustomer.Relation.Trim))
        txtHP.Text = oCustomer.HP
        txtEmail.Text = oCustomer.Email

        With UCAddressPJ
            .Address = oCustomer.Alamat.Address
            .RT = oCustomer.Alamat.RT
            .RW = oCustomer.Alamat.RW
            .Kelurahan = oCustomer.Alamat.Kelurahan
            .Kecamatan = oCustomer.Alamat.Kecamatan
            .City = oCustomer.Alamat.City
            .ZipCode = oCustomer.Alamat.ZipCode
            .AreaPhone1 = oCustomer.Alamat.AreaPhone1
            .Phone1 = oCustomer.Alamat.Phone1
            .AreaPhone2 = oCustomer.Alamat.AreaPhone2
            .Phone2 = oCustomer.Alamat.Phone2
            .AreaFax = oCustomer.Alamat.AreaFax
            .Fax = oCustomer.Alamat.Fax
            .BindAddress()
        End With
    End Sub
    Sub FillCbo()  
        Dim oCustomer = m_controller.LoadCombo(GetConnectionString(), "FAMILY")
        FillCommonValueCbo(oCustomer, cboHubungan) 
    End Sub

    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("Customer.aspx")
    End Sub
End Class