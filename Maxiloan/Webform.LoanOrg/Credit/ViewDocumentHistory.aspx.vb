﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class ViewDocumentHistory
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "ViewDoc"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.AgreementNo = Request("AgreementNo").ToString
                Me.ApplicationID = Request("ApplicationID").ToString
                Me.CustomerName = Request("CustomerName").ToString
                lblAgreementNo.Text = Me.AgreementNo
                lblCustomerName.Text = Me.CustomerName
                Me.Style = Request("Style").ToString
                Me.CustomerID = Request("CustomerID").ToString.Trim
                Bindgrid()

                Dim lb As New LinkButton
                lb = CType(Me.FindControl("lblCustomerName"), LinkButton)
                lb.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")
            End If
        End If
    End Sub

    Sub Bindgrid()
        'Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)

        'Dim adapter As New SqlDataAdapter
        Dim objReaderGrid As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewDocumentHistory"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objReaderGrid = objCommand.ExecuteReader

            'adapter.SelectCommand = objCommand
            'adapter.Fill(dtGrid)

            dtg.DataSource = objReaderGrid
            dtg.DataBind()
            objReaderGrid.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    'End Sub
End Class