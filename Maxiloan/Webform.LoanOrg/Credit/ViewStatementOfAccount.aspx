﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewStatementOfAccount.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewStatementOfAccount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewStatementOfAccount</title>
    <%--<link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />--%>
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/default/easyui.css" type="text/css">
    <%--<script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var x = screen.width;
        var y = screen.height - 100;			
    </script>--%>
    <%--<script src="../../Maxiloan.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../../js/jquery.min.js"></script>
    <script type="text/javascript" src="../../js/jquery.easyui.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
        onclick="hideMessage();"></asp:Label>
    <asp:HiddenField ID="hdAplicationID" runat="server" />
    <asp:HiddenField ID="hdCustomerName" runat="server" />
    <asp:HiddenField ID="hdCustomerID" runat="server" />
    <asp:HiddenField ID="hdAgreementNo" runat="server" />
    <asp:HiddenField ID="hdBranchID" runat="server" />
    <asp:HiddenField ID="hdApprovalNo" runat="server" />
    <asp:HiddenField ID="hdApplicationModule" runat="server" />
    <div class="form_single">
        <div id="tt" class="easyui-tabs" border="false" style="height: 630px">
       
            <div title="Customer" style="padding: 10px">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            CUSTOMER MKK</h3>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Cabang
                            </label>
                            <asp:Literal ID="lbBranch" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                No Kontrak
                            </label>
                            <b>
                                <asp:Literal ID="lbAgreementNo" runat="server" EnableViewState="False"></asp:Literal></b>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Customer
                            </label>
                            <asp:CheckBox ID="chkBaru" runat="server" Enabled="False"></asp:CheckBox>Baru
                            <asp:CheckBox ID="chkRO" runat="server" Enabled="False"></asp:CheckBox>Lama/RO
                        </div>
                        <div class="form_right">
                            <label>
                                Tanggal Scoring
                            </label>
                            <asp:Literal ID="lbScoringDate" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>

                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Default Status
                            </label>
                            <asp:Literal ID="lbDefaultStatus" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Pre Paid
                            </label>
                            <asp:Literal ID="lbPrePaid" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>

                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            A. DEBITUR
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                ID Customer
                            </label>
                            <asp:Label runat="server" ID="lblCustomerID"></asp:Label>
                            <%--<asp:HyperLink ID="lblIDCustomer" runat="server"></asp:HyperLink>--%>
                        </div>
                        <div class="form_right">

                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Nama Customer
                            </label>
                            <asp:HyperLink ID="lnkCustomerName" runat="server"></asp:HyperLink>
                        </div>
                        <div class="form_right">
                            <label>
                                Profesi
                            </label>
                            <asp:Literal ID="lbProfesi" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <%--   <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Alamat
                </label>
                <asp:Literal ID="lbAlamat" runat="server" EnableViewState="False"></asp:Literal>
            </div>
            <div class="form_right">
                <label>
                    Jenis&nbsp;Usaha
                </label>
                <asp:Literal ID="lbJenisUsaha" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
    </div>--%>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_general">
                            Alamat</label>
                        <asp:Literal ID="lbAlamat" runat="server" EnableViewState="False"></asp:Literal>
                    </div>
                    <div class="form_right">
                        <label class="label_general">
                            Jenis&nbsp;Usaha</label>
                        <asp:Literal ID="lbJenisUsaha" runat="server" EnableViewState="False"></asp:Literal>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            B. FASILITAS YANG ADA SAAT INI
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <asp:DataGrid ID="DtgFasiltas" runat="server" CellSpacing="1" CellPadding="3" BorderWidth="0px"
                            AutoGenerateColumns="False" AllowSorting="True" CssClass="grid_general" Width="100%">
                            <ItemStyle CssClass="item_grid" />
                            <HeaderStyle CssClass="th" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO KONTRAK">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="Label6" Text='<%# DataBinder.eval(Container,"DataItem.agreementNo")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="OUTSTANDING PH">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblOSPrincipal" Text='<%# formatnumber(DataBinder.eval(Container,"DataItem.OutstandingPrincipal"),2)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="POSISI ANGS KE">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblSeqNo" Text='<%# DataBinder.eval(Container,"DataItem.NextInstallmentNumber")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH OD">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblOD" Text='<%# formatnumber(DataBinder.eval(Container,"DataItem.AmountOverdueGross"),2)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="LAMA OD">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblLamaOD" Text='<%# DataBinder.eval(Container,"DataItem.DaysOverdue")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CARA BAYAR">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="Label1" Text='<%# DataBinder.eval(Container,"DataItem.WOP")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NAMA ASSET">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="Label2" Text='<%# DataBinder.eval(Container,"DataItem.DescriptionAsset")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JENIS">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="Label3" Text='<%# DataBinder.eval(Container,"DataItem.AssetType")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TAHUN">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="Label4" Text='<%# DataBinder.eval(Container,"DataItem.ManufacturingYear")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NILAI">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="Label5" Text='<%# formatnumber(DataBinder.eval(Container,"DataItem.TotalOTR"),2)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            C. FASILITAS YANG DIAJUKAN
                        </h4>
                    </div>
                </div>
                <table border="0">
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    TUJUAN
                                </label>
                                BELI
                            </div>
                            <div class="form_right">
                                <label>
                                    PH
                                </label>
                                <asp:Literal ID="lbPH" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    DP GROSS
                                </label>
                                <asp:Literal ID="lbDPGross" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                            <div class="form_right">
                                <label>
                                    DP NET
                                </label>
                                <asp:Literal ID="lbDPNet" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    ANGSURAN
                                </label>
                                <asp:Literal ID="lbAngs1" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                            <div class="form_right">
                                <label>
                                    MARGIN FLAT
                                </label>
                                <asp:Literal ID="lbBungaFlat" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    MARGIN EFEKTIF
                                </label>
                                <asp:Literal ID="lbBungaEfektif" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                            <div class="form_right">
                                <label>
                                    JANGKA WAKTU
                                </label>
                                <asp:Literal ID="lbTenor" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    BIAYA ADM
                                </label>
                                <asp:Literal ID="lbAdm" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                            <div class="form_right">
                                <label>
                                    ASURANSI
                                </label>
                                <asp:Literal ID="lbAsuransi" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="form_box">
                        <div>
                            <div class="form_left">
                                <label>
                                    BIAYA ADM LAINNYA
                                </label>
                                <asp:Literal ID="lbOtherFee" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </table>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            D. BARANG YANG DIJAMINKAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                NAMA ASSET
                            </label>
                            <asp:Literal ID="lbKendaraan" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                JENIS
                            </label>
                            <asp:Literal ID="lbType" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                TAHUN
                            </label>
                            <asp:Literal ID="lbTahun" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                NILAI
                            </label>
                            <asp:Literal ID="lbNilai" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                HARGA CEPAT LAKU
                            </label>
                            <asp:Literal ID="lbHargaLaku" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                PEMBANDING SR
                            </label>
                            <asp:Literal ID="lbSR1" runat="server" EnableViewState="False"></asp:Literal>(<asp:Literal
                                ID="lbSRNilai1" runat="server" EnableViewState="False"></asp:Literal>)
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                PEMBANDING SR
                            </label>
                            <asp:Literal ID="lbSR2" runat="server" EnableViewState="False"></asp:Literal>(<asp:Literal
                                ID="lbSrNilai2" runat="server" EnableViewState="False"></asp:Literal>)
                        </div>
                        <div class="form_right">
                            <label>
                                &nbsp;
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div>
                        <div class="form_left">
                            <h4>
                                E. TUJUAN PENGGUNAAN ASSET
                            </h4>
                            <asp:Literal ID="lbTujuan" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div>
                        <div class="form_left">
                            <h4>
                                F. LOKASI PEMAKAIAN
                            </h4>
                            <asp:Literal ID="lbLokasi" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            G. PINJAMAN BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Status Pinjaman
                            </label>
                            <asp:Literal ID="lblStatusPinjaman" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Nama Bank
                            </label>
                            <asp:Literal ID="lblNamaBank" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Tanggal Batch
                            </label>
                            <asp:Literal ID="lblTanggalBatch" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                No Fasilitas
                            </label>
                            <asp:Literal ID="lblNoFasilitas" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            H. PENGHASILAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Penghasilan Tetap
                            </label>
                            <asp:Literal ID="lbIncTetap" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Pengeluaran total
                            </label>
                            <asp:Literal ID="lbExpTotal" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Penghasilan istri/suami
                            </label>
                            <asp:Literal ID="lbIncSuamiIstri" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Angsuran
                            </label>
                            <asp:Literal ID="lbAngs" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Penghasilan tambahan
                            </label>
                            <asp:Literal ID="lbIncTambahan" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Angsuran (RO)
                            </label>
                            <asp:Literal ID="lbAngsRO" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Total penghasilan
                            </label>
                            <asp:Literal ID="lbIncTotal" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Total Pengeluaran
                            </label>
                            <asp:Literal ID="lbTotalExp" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Rasio angsuran dibagi penghasilan
                            </label>
                            <asp:Literal ID="lbRasioAngs" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Rasio angsuran dibagi sisa penghasilan
                            </label>
                            <asp:Literal ID="lblRasioSisaInc" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Sisa Penghasilan
                            </label>
                            <asp:Literal ID="lbSisaInc" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            I. DATA PRIBADI / PERUSAHAAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Pendidikan
                            </label>
                            <asp:Literal ID="lbPend" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Nama Perusahaan
                            </label>
                            <asp:Literal ID="lbNmPerusahaan" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Status Perkawinan
                            </label>
                            <asp:Literal ID="lbStsKawin" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Bidang Usaha
                            </label>
                            <asp:Literal ID="lbBidUsaha" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Tanggungan
                            </label>
                            <asp:Literal ID="lbTangungan" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Jabatan
                            </label>
                            <asp:Literal ID="lbJabatan" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Status Rumah
                            </label>
                            <asp:Literal ID="lbStsRumah" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Masa Kerja/Masa Usaha
                            </label>
                            <asp:Literal ID="lbMasaKerja" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Lama Tinggal/ Masa Sewa
                            </label>
                            <asp:Literal ID="lbMasaSewa" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                            <label>
                                Golongan Perusahaan
                            </label>
                            <asp:Literal ID="lbGolPerusahaan" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                        </div>
                        <div class="form_right">
                            <label>
                                Status Kepemilikan
                            </label>
                            <asp:Literal ID="lbStsMilikPerusahaan" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div>
                        <div class="form_left">
                            <h4>
                                J. HASIL SCORING
                            </h4>
                            <asp:Literal ID="lbScoring" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <asp:DataGrid ID="dtgApprNotes" runat="server" EnableViewState="False" BorderWidth="0px"
                            AutoGenerateColumns="False" CssClass="grid_general" Width="100%" CellSpacing="1"
                            CellPadding="3">
                            <ItemStyle CssClass="item_grid" />
                            <HeaderStyle CssClass="th" />
                            <Columns>
                                <asp:BoundColumn DataField="ApprovalNote" HeaderText="ANALISA">
                                    <HeaderStyle Width="30%"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Argumentasi" HeaderText="HAL-HAL YANG MENYIMPANG DAN ARGUMENTASI">
                                    <HeaderStyle Width="30%"></HeaderStyle>
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            APPROVAL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                CMO
                            </label>
                            <asp:Literal ID="lbCMO" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Pembiayaan Review
                            </label>
                            <asp:Literal ID="lbCreditReview" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div>
                        <div class="form_left">
                            <label>
                                Approval Process
                            </label>
                            <asp:Literal ID="lbAppIsFinal" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form_right">
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlHistory" runat="server" Visible="false">
                    <div class="form_title">
                        <div class="form_single">
                            <h4>
                                APPROVAL HISTORY
                            </h4>
                        </div>
                    </div>
                </asp:Panel>
                <div class="form_box_header">
                    <div class="form_single">
                        <asp:DataGrid ID="dtgHistory" runat="server" EnableViewState="False" BorderWidth="0px"
                            AutoGenerateColumns="False" CssClass="grid_general" Width="100%" CellSpacing="1"
                            CellPadding="3">
                            <ItemStyle CssClass="item_grid" />
                            <HeaderStyle CssClass="th" />
                            <Columns>
                                <asp:BoundColumn DataField="ApprovalSeqNum" HeaderText="NO">
                                    <HeaderStyle Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="UserApprovalName" HeaderText="NAMA">
                                    <HeaderStyle Width="20%"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="EmployeePosition" HeaderText="POSISI"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ApprovalDate" HeaderText="TANGGAL">
                                    <HeaderStyle Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div title="Asset">
                <iframe id="IframeAsset" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Insurance">
                <iframe id="IframeInsurance" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Financial">
                <iframe id="IframeFinancial" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Insentif">
                <iframe id="IframeInsentif" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Amortization">
                <iframe id="IframeAmortization" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Credit Process">
                <iframe id="IframeCreditProcess" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Hist Payment">
                <iframe id="IframeHistPayment" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Activity Log">
                <iframe id="IframeActivityLog" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="PDC">
                <iframe id="IframePDC" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Exposure">
                <iframe id="IframeExposure" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Accr Inc">
                <iframe id="IframeAccrInc" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Doc History">
                <iframe id="IframeDocHistory" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Collection Activity">
                <iframe id="IframeCollectionActivity" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Term & Condition">
                <iframe id="IframeTermCondition" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
            <div title="Journal">
                <iframe id="IframeJournal" runat="server" src="" frameborder="0" style="background-color: #FFFFFF;
                    width: 100%; height: 100%"></iframe>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="small button gray">
        </asp:Button> 
        <%--<asp:Button ID="btnPrintCustomer" runat="server" Text="Print Kartu Piutang Customer" CssClass="small button blue" />--%>
        <asp:Button ID="btnPrintInternal" runat="server" Text="Print Kartu Piutang Internal" CssClass="small button blue" />
        <%--Report baru by Wira 2016-02-29--%>
         <asp:Button ID="btnPrintEkternal" runat="server" Text="Print Kartu Piutang Eksternal" CssClass="small button blue" />
         <%--Report baru by Meisan 2017-10-11--%>
        <asp:Button ID="btnPrintProfileKonsumen" runat="server" Text="Print Profile Konsumen" CssClass="small button blue" />
    </div>
    
    <script type="text/javascript">
        $('#tt').tabs({
            border: false,
            onSelect: function (title) {

                var _AplicationID = $("#hdAplicationID").val();
                var _CustomerID = $("#hdCustomerID").val();
                var _CustomerName = $("#hdCustomerName").val();
                var _AgreementNo = $("#hdAgreementNo").val();
                var _BranchID = $("#hdBranchID").val();
                var _ApprovalNo = $("#hdApprovalNo").val();
                var _ApplicationModule = $("#hdApplicationModule").val();

                if (title == "Financial") {
                    if (_ApplicationModule=='OPLS')
                        $('#IframeFinancial').attr('src', 'ViewFinancialOperatingLease.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&BrchId=' + _BranchID + '&CustomerName=' + _CustomerName + '&Style=AccMnt&CustomerID=' + _CustomerID);
                    else
                        $('#IframeFinancial').attr('src', 'ViewFinancial.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&BrchId=' + _BranchID + '&CustomerName=' + _CustomerName + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }
                else if (title == "Insurance") {
                    $('#IframeInsurance').attr('src', 'ViewInsuranceDetailNew.aspx?ApplicationID=' + _AplicationID + '&CustomerName=' + _CustomerName + '&Style=AccMnt&AgreementNo=' + _AgreementNo + '&CustomerID=' + _CustomerID);
                }
                else if (title == "Asset") {
                    $('#IframeAsset').attr('src', 'viewAssetNew.aspx?ApplicationID=' + _AplicationID + '&CustomerName=' + _CustomerName + '&Style=AccMnt&AgreementNo=' + _AgreementNo + '&CustomerID=' + _CustomerID);
                }
                else if (title == "Insentif") {
                    $('#IframeInsentif').attr('src', 'CreditProcess/NewApplication/ViewInsentif.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerName=' + _CustomerName + '&CustomerID=' + _CustomerID +'&style=AccMnt&page=ViewStatementOfAccount');
                }
                else if (title == "Amortization") {
                    $('#IframeAmortization').attr('src', 'viewAmortization.aspx?ApplicationID=' + _AplicationID + '&CustomerName=' + _CustomerName + '&AgreementNo=' + _AgreementNo + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }

                else if (title == "Credit Process") {
                    $('#IframeCreditProcess').attr('src', 'viewCreditProcess.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerName=' + _CustomerName + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }

                else if (title == "Hist Payment") {
                    $('#IframeHistPayment').attr('src', '../../Webform.LoanMnt/View/PaymentHistory.aspx?ApplicationID=' + _AplicationID + '&CustomerName=' + _CustomerName + '&Style=AccMnt&AgreementNo=' + _AgreementNo + '&BranchID=' + _BranchID);
                }

                else if (title == "Activity Log") {
                    $('#IframeActivityLog').attr('src', 'viewActivityLog.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerName=' + _CustomerName + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }

                else if (title == "PDC") {
                    $('#IframePDC').attr('src', '../../Webform.LoanMnt/View/viewPDC.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerName=' + _CustomerName + '&Style=AccMnt&CustomerID=' + _CustomerID + '&flagfile=1');
                }

                else if (title == "Exposure") {
                    $('#IframeExposure').attr('src', 'viewExposure.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerName=' + _CustomerName + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }

                else if (title == "Accr Inc") {
                    $('#IframeAccrInc').attr('src', 'viewAccrInc.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerName=' + _CustomerName + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }

                else if (title == "Doc History") {
                    $('#IframeDocHistory').attr('src', 'viewDocumentHistory.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerName=' + _CustomerName + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }

                else if (title == "Collection Activity") {
                    //                    $('#IframeCollectionActivity').attr('src', '../../Webform.ARMgt/Inquiry/InqCollActivityResult.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerID=' + _CustomerName + '&CustomerName=' + _CustomerName + '&Style=AccMnt&Referrer=ViewAgreement');
                    $('#IframeCollectionActivity').attr('src', '../../Webform.ARMgt/Inquiry/InqCollActivityResult.aspx?ApplicationID=' + _AplicationID + '&AgreementNo=' + _AgreementNo + '&CustomerID=' + _CustomerID + '&CustomerName=' + _CustomerName + '&Style=AccMnt&Referrer=ViewAgreement');
                }

                else if (title == "Term & Condition") {
                    $('#IframeTermCondition').attr('src', 'viewTermCondition.aspx?ApplicationID=' + _AplicationID + '&CustomerName=' + _CustomerName + '&AgreementNo=' + _AgreementNo + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }

                else if (title == "Journal") {
                    $('#IframeJournal').attr('src', 'viewJournal.aspx?ApplicationID=' + _AplicationID + '&CustomerName=' + _CustomerName + '&AgreementNo=' + _AgreementNo + '&Style=AccMnt&CustomerID=' + _CustomerID);
                }
            }
        });

          function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'menubar=0, scrollbars=yes');
        }    
    </script>
    </form>
</body>
</html>
