﻿

Imports System.Web.Services
Imports System.IO

Imports Maxiloan.Webform.UserController

Public Class CompanyCustomerGuarantor
    Inherits AbsCustomerCompany

#Region "Property & Constanta"
    Protected Property CustomerGuarantors() As IList(Of Parameter.CompanyCustomerGuarantor)
        Get
            Return CType(ViewState("CustomerGuarantors"), IList(Of Parameter.CompanyCustomerGuarantor))
        End Get
        Set(ByVal Value As IList(Of Parameter.CompanyCustomerGuarantor))
            ViewState("CustomerGuarantors") = Value
        End Set
    End Property
    'Protected WithEvents txtTglLahirNew As ucDateCE
    'Protected WithEvents txtMasaBerlakuKTP As ucDateCE 
    Protected WithEvents ucAddressSI_KTP As UcCompanyAddress
    Private time As String

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        ucAddressSI_KTP.IsRequiredZipcode = True
        ucAddressSI_KTP.IsRequiredCompanyAddress = False
        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            Me.CustomerGuarantors = New List(Of Parameter.CompanyCustomerGuarantor)
            If Request("prospectappid") = "" Then Me.ProspectAppID = "-"
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
            '  FillCbo()

            'Modify by Wira 20171023
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------


            Me.PageAddEdit = Request("page")
            If Me.PageAddEdit = "Add" Then
                GetXML()
                lblTitle.Text = "ADD"
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit()
                cTabs.SetNavigateUrl(Request("page"), Request("id"))
            End If

            cTabs.RefreshAttr(Request("pnl"))
            FillGuarantorCbos()
            clearPopUp()

        End If
    End Sub

    Sub FillGuarantorCbos()
        'FillCommonValueCbo(m_controller.LoadCombo(GetConnectionString(), "EMPLOYEEPOSITION"), cboJabatan)
        FillCbo("tblJobPosition")
        FillCommonValueCbo(m_controller.LoadCombo(GetConnectionString(), "IDTYPE"), cboIDTypeP)
    End Sub
#Region "FillCBO"
    Sub FillCbo(ByVal table As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = table
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        Select Case table
            Case "tblJobPosition"
                cboJabatan.DataSource = dtEntity.DefaultView
                cboJabatan.DataTextField = "Description"
                cboJabatan.DataValueField = "ID"
                cboJabatan.DataBind()
                cboJabatan.Items.Insert(0, "Select One")
                cboJabatan.Items(0).Value = "Select One"
        End Select
    End Sub
#End Region
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim Err As String = m_controller.CompanyCustomerGuarantorSaveEdit(GetConnectionString(), Me.CustomerID, CustomerGuarantors)
            If Err = "" Then
                'Response.Redirect("CustomerCompanyEC.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabEmergencyCt")
                'Modify by WIra 20171023
                If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                    Response.Redirect("CustomerCompanyEC.aspx?page=Add&id=" + Me.CustomerID + "&pnl=tabEmergencyCt&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
                Else
                    Response.Redirect("CustomerCompanyEC.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabEmergencyCt&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
                End If
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
    Shared Function guidString() As String

        Dim gb As Byte() = Guid.NewGuid().ToByteArray()
        Return BitConverter.ToInt64(gb, 0).ToString()
    End Function
    Private Sub btnAddGua_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddGua.Click
        Dim ccG = New Parameter.CompanyCustomerGuarantor() With
                  {
                      .Id = guidString(),
                      .CustomerId = Me.CustomerID,
                      .NamaPenjamin = txtNamaPasangan.Text,
                      .JabatanId = cboJabatan.SelectedValue,
                      .JabatanDesc = cboJabatan.SelectedItem.Text,
                      .TempatLahir = txtBirthPlaceP.Text,
                      .TglLahir = ConvertDate2(txtTglLahirNew.Text),
                      .JenisDokumenId = cboIDTypeP.SelectedValue,
                      .JenisDokumenDesc = cboIDTypeP.SelectedItem.Text,
                      .NoDokumen = txtNoDokument.Text,
                      .MasaBerlakuKtp = ConvertDate2(txtMasaBerlakuKTP.Text),
        .AlamatKTP = New Parameter.Address With {
              .Address = ucAddressSI_KTP.Address,
              .RT = ucAddressSI_KTP.RT,
              .RW = ucAddressSI_KTP.RW,
              .Kelurahan = ucAddressSI_KTP.Kelurahan,
              .Kecamatan = ucAddressSI_KTP.Kecamatan,
              .City = ucAddressSI_KTP.City,
              .ZipCode = ucAddressSI_KTP.ZipCode,
              .AreaPhone1 = ucAddressSI_KTP.AreaPhone1,
              .Phone1 = ucAddressSI_KTP.Phone1,
              .AreaPhone2 = ucAddressSI_KTP.AreaPhone2,
              .Phone2 = ucAddressSI_KTP.Phone2,
              .AreaFax = ucAddressSI_KTP.AreaFax,
              .Fax = ucAddressSI_KTP.Fax,
              .MobilePhone = txtMobilePhone.Text
              }
                  }
        'ConvertDate2(IIf(CStr(StartDate.Text) = "", Format(Me.BusinessDate, "dd/MM/yyyy"), StartDate.Text))
        ' ConvertDate2(CType(txtTglLahirNew, ucDateCE).Text),
        CustomerGuarantors.Add(ccG)
        clearPopUp()
        reBindGrid()
    End Sub


    Sub clearPopUp()
        txtNamaPasangan.Text = ""
        cboJabatan.SelectedIndex = 0
        cboJabatan.SelectedIndex = 0
        txtBirthPlaceP.Text = ""
        txtTglLahirNew.Text = ""
        cboIDTypeP.SelectedIndex = 0
        cboIDTypeP.SelectedIndex = 0
        txtMasaBerlakuKTP.Text = BusinessDate.ToString("dd/MM/yyyy")
        txtNoDokument.Text = ""
        txtMobilePhone.Text = ""

        ucAddressSI_KTP.Address = ""
        ucAddressSI_KTP.RT = ""
        ucAddressSI_KTP.RW = ""
        ucAddressSI_KTP.Kelurahan = ""
        ucAddressSI_KTP.Kecamatan = ""
        ucAddressSI_KTP.City = ""
        ucAddressSI_KTP.ZipCode = ""
        ucAddressSI_KTP.AreaPhone1 = ""
        ucAddressSI_KTP.Phone1 = ""
        ucAddressSI_KTP.BindAddress()


        txtMobilePhone.Text = ""

    End Sub

    Protected Sub CommandGrid_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        If e.CommandName.ToLower = "delete" Then
            Dim key = e.CommandArgument.ToString
            remove(key)
        End If
    End Sub
    Sub remove(key As String)
        Dim k = CustomerGuarantors.Where(Function(x) x.Id = key).SingleOrDefault()
        If Not k Is Nothing Then
            CustomerGuarantors.Remove(k)
        End If
        reBindGrid()
    End Sub
    Sub reBindGrid()
        Dim i = 1
        For Each item In CustomerGuarantors
            item.Seq = i.ToString()
            i += 1
        Next
        dtgCSV.DataSource = CustomerGuarantors
        dtgCSV.DataBind()
    End Sub
    Sub BindEdit()

        Dim _CustomerGuarantors = m_controller.GetCompanyCustomerGuarantor(GetConnectionString(), Me.CustomerID)
        For Each item In _CustomerGuarantors
            CustomerGuarantors.Add(item)
        Next
        reBindGrid()
    End Sub


End Class