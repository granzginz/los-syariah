﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Customer.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.Customer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCustomerLookUpGlobal" Src="../../Webform.UserController/ucLookUpCustomerGlobal.ascx" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc2" %>
<%@ Register Src="../../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  


<%@ Register Src="CustomerNProspectTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;
            window.open('http://<%# Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }

        function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }

        function cboPMarital_IndexChanged(e) {
            if (e == 'M') {
                $('#divSuamiIstri').css("visibility", "");
                $('#txtNamaPasangan').removeAttr("style");

                ValidatorEnable(rfvNamaPasangan, true);
//                $('#rfvNamaPasangan').show();

                ValidatorEnable(revNamaPasangan, true);
                $('#revNamaPasangan').hide();
            }
            else {
                $('#divSuamiIstri').css("visibility", "hidden");
                $('#txtNamaPasangan').attr("style", "display : none");

                ValidatorEnable(rfvNamaPasangan, false);
                $('#rfvNamaPasangan').hide();

                ValidatorEnable(revNamaPasangan, false);
                $('#revNamaPasangan').hide();
            }
        }
        $(document).ready(function () {
            cboPMarital_IndexChanged($('#cboPMarital').val());
        });
    </script>
    <style type="text/css">
        .widthAlamat
        {
            width: 1500px;
        }
        .widthCabang
        {
            width: 150px;
        }
    </style>
</head>
<body onload="gridGeneralSize('dtgPaging');" onresize="gridGeneralSize('dtgPaging');">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <uct:tabs id='cTabs' runat='server'></uct:tabs>
    <div runat="server" id="jlookupContent" />
  <%--  <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>--%>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"/>
             
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR CUSTOMER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" DataKeyField="Name" BorderStyle="None" BorderWidth="0">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.png"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="" visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgEditPersonal" runat="server" CausesValidation="False" ImageUrl="../../Images/idcard.png"
                                                CommandName="EditPersonal"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="NAMA" SortExpression="Name">
                                        <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCustomer" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                            </asp:LinkButton>
                                            <asp:Label ID="lblCust" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="CustomerType" ItemStyle-HorizontalAlign="center" SortExpression="CustomerType"
                                        HeaderText="P/C" ItemStyle-CssClass="short_col"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="ALAMAT"
                                        HeaderStyle-CssClass="widthAlamat" ItemStyle-CssClass="widthAlamat">
                                        <ItemStyle CssClass="address_col"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CustomerID" Visible="False" HeaderText="CustomerID">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="CABANG" HeaderStyle-CssClass="widthCabang" ItemStyle-CssClass="widthCabang">                                        
                                        <ItemStyle HorizontalAlign="Left" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <%# DataBinder.eval(Container,"DataItem.BranchFullName")  %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                             <uc2:ucGridNav id="GridNavigator" runat="server"/>
                         
                        </div>
                    </div>
                </div>
                <div class="form_button" style="padding-top: 3px;padding-bottom: 0;">
                    <%--<asp:Button ID="btnAddPersonal" runat="server" Text="Add Personal" CssClass="small button blue"
                        CausesValidation="false" ></asp:Button>--%>
                    <asp:Button ID="btnAddCompany" runat="server" Text="Add Company" CssClass="small button blue"
                        CausesValidation="false" ></asp:Button>                    
                    <asp:HiddenField runat="server" ID="hdfKode" />
                    <asp:HiddenField runat="server" ID="hdfName" />
                    <button class="small button blue" onclick="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/CustomerGlobal.aspx?kode=" & hdfKode.ClientID & "&nama=" & hdfName.ClientID) %>','Daftar Customer','<%= jlookupContent.ClientID %>');return false;">View Global</button>                                        
                </div>
                <div class="form_box_title" style="margin-top: 3px;" >
                    <div class="form_single">
                        <h5>
                            MENCARI CUSTOMER
                        </h5>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <label> Nama</label>
                        <asp:DropDownList ID="cboSearch" runat="server" style="display:none">
                            <asp:ListItem Value="Name" Selected>Nama</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
                    </div>
                    <div class="form_right">
                          <label> tanggal Lahir</label>
                         <uc2:ucdatece id="txtSrcTglLahirNew" runat="server"></uc2:ucdatece>
                    </div>
                </div>
                 <div class="form_box">
                      <div class="form_left">
                        <label> No Identitas</label>
                         <asp:TextBox ID="srcTxtIDTypeP" runat="server" Width="30%" MaxLength="50"></asp:TextBox> 
                     </div>
                  
                    <div class="form_right">
                        <label > Nama Ibu Kandung</label>
                        <asp:TextBox ID="txtSrcPMotherName" runat="server" Width="30%" MaxLength="50"></asp:TextBox>
                   </div>
                   </div>


                   <div class="form_box_hide">
                   <div class="form_single">
                    <label> Tempat Lahir</label>
                    <asp:TextBox ID="txtSrcBirthPlaceP" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox> 
                    </div>
                    </div>



                   

                <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
                    <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
                <%--tidak perlu ditampilkan--%>
                <div class="form_box_hide">
                    <div class="form_single">
                        <h4>
                            CUSTOMER DARI PROSPECT
                        </h4>
                    </div>
                </div>
                <div class="form_box_hide">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgProspect" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            CellPadding="3" CellSpacing="1" DataKeyField="ProspectAppID" BorderStyle="None"
                            BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="LAKUKAN">
                                    <HeaderStyle CssClass="command_col" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblBranchID" runat="Server" Text='<%#Container.DataItem("BranchID")%>'
                                            Visible="false"></asp:Label>
                                        <asp:Label ID="lblProspectAppID" runat="Server" Text='<%#Container.DataItem("ProspectAppID")%>'
                                            Visible="false"></asp:Label>
                                        <asp:LinkButton ID="HypGet" runat="server" Text="AMBIL" CommandName="GET"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle CssClass="name_col" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustName" runat="Server" Text='<%#Container.DataItem("Name")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="CustomerType" HeaderText="KESAMAAN">
                                    <HeaderStyle CssClass="short_col" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="Server" Text='<%#Container.DataItem("CustomerType")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="Address" HeaderText="ALAMAT">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%#Container.DataItem("Address")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddPersonal" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            MENAMBAH - PERSONAL CUSTOMER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama</label>
                             <asp:TextBox ID="txtGelar" runat="server" placeholder="gelar depan" CssClass="smaller_text"></asp:TextBox>
                        <asp:TextBox ID="txtNameP" runat="server" MaxLength="50" CssClass="medium_text"></asp:TextBox>
                        <asp:TextBox ID="txtGelarBelakang" runat="server" MaxLength="50" placeholder="gelar belakang"
                            CssClass="smaller_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi Nama"
                            ControlToValidate="txtNameP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNameP" CssClass="validator_general"
                            ID="RegularExpressionValidator2" ValidationExpression="^[A-Za-z ]{3,50}$" runat="server"
                            ErrorMessage="Input huruf saja!!!"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_box" style="display: none">
                    <div class="form_single">
                        <label class="label_general">
                            Jenis</label>
                        <asp:RadioButtonList ID="rboTypeP" runat="server" RepeatDirection="Horizontal" CssClass="opt_single"
                            RepeatLayout="Table">
                            <asp:ListItem Value="M">Karyawan</asp:ListItem>
                            <asp:ListItem Value="M">Wiraswasta</asp:ListItem>
                            <asp:ListItem Value="O">Profesional</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Identitas</label>
                        <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Jenis Identitas"
                            ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Identitas</label>
                        <asp:TextBox ID="txtIDNumberP" runat="server" MaxLength="25" CssClass="medium_text"
                            onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegEx1" ControlToValidate="txtIDNumberP" runat="server"
                            ErrorMessage="Masukkan angka saja" ValidationExpression="\d+" CssClass="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap isi No Identitas"
                            ControlToValidate="txtIDNumberP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <label class="label_req">
                            Berlaku s/d</label>
                        <%--<asp:TextBox runat="server" ID="txtIDTglBerlaku" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtIDTglBerlaku"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Isi tanggal berlaku!"
                            ControlToValidate="txtIDTglBerlaku" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <uc2:ucdatece id="txtIDTglBerlakuNew" runat="server" Enabled= "False"/>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Kelamin</label>
                        <asp:Dropdownlist ID="cboGenderP" runat="server" CssClass="select">
                            <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                            <asp:ListItem Value="M">Laki-Laki</asp:ListItem>
                            <asp:ListItem Value="F">Perempuan</asp:ListItem>
                        </asp:Dropdownlist>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Isi Jenis Kelamin!"
                            ControlToValidate="cboGenderP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tempat / Tanggal Lahir</label>
                        <asp:TextBox ID="txtBirthPlaceP" runat="server" MaxLength="20" CssClass="medium_text"
                        onkeypress="return onlyAlphabets(event,this);"></asp:TextBox>
                        <label class="label_auto">
                            /</label>
                        <asp:RequiredFieldValidator ID="RequiredFtxtBirthPlaceP" runat="server" Display="Dynamic" ControlToValidate="txtBirthPlaceP" ErrorMessage="Harap isi Tempat lahir" CssClass="validator_general" /> 
                        <uc2:ucdatece id="txtTglLahirNew" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Ibu Kandung</label>
                        <asp:TextBox ID="txtPMotherName" runat="server" Width="30%" MaxLength="50"></asp:TextBox>
                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtPMotherName"
                            CssClass="validator_general" ID="RegularExpressionValidator1" ValidationExpression="^[A-Za-z ]{3,50}$"
                            runat="server" ErrorMessage="Input huruf saja dan minimal 3 huruf!"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="rfvPMotherName" runat="server" Display="Dynamic"
                            ControlToValidate="txtPMotherName" ErrorMessage="Harap isi dengan Nama Ibu Kandung"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Status Perkawinan</label>
                        <asp:DropDownList ID="cboPMarital" onchange="cboPMarital_IndexChanged(this.value);"  runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                            InitialValue="" ControlToValidate="cboPMarital" ErrorMessage="*" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <div runat="server" id="divSuamiIstri">
                            <label class="label_req">
                                Nama Suami/Istri</label>
                            <asp:TextBox runat="server" ID="txtNamaPasangan" CssClass="medium_text"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvNamaPasangan" runat="server" ControlToValidate="txtNamaPasangan"
                                ErrorMessage="Harap diisi nama pasangan!" CssClass="validator_general" Display="dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNamaPasangan"
                                CssClass="validator_general" ID="revNamaPasangan" ValidationExpression="^[A-Za-z ]{3,50}$"
                                runat="server" ErrorMessage="Input huruf saja dan minimal 3 huruf!"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Kartu Keluarga</label>
                        <asp:TextBox ID="txtPNoKK" runat="server" MaxLength="16" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegEx2" ControlToValidate="txtPNoKK" runat="server"
                            ErrorMessage="Masukkan angka saja" ValidationExpression="\d+" CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnNextP" runat="server" Text="Next" CssClass="small button green" />
                    <asp:Button ID="btnCancelP" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddCompany" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            MENAMBAH CORPORATE CUSTOMER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Perusahaan</label>
                        <asp:TextBox ID="txtCName" runat="server" MaxLength="50" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ErrorMessage="Harap isi Nama Perusahaan"
                            ControlToValidate="txtCName" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--edit npwp, ktp, telepon by ario--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            NPWP</label>
                        <asp:TextBox ID="txtCNPWP" runat="server" MaxLength="15" CssClass="medium_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" runat="server" ErrorMessage="Harap isi NPWP!"
                            ControlToValidate="txtCNPWP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegtxtPNPWP" Display="Dynamic" ControlToValidate="txtCNPWP"
                            runat="server" ErrorMessage="Masukkan angka saja" ValidationExpression="\d+"
                            CssClass="validator_general"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc1:uccompanyaddress id="UcCompAddress" runat="server"></uc1:uccompanyaddress>
                </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnNextc" runat="server" Text="Next" CssClass="small button green" />
                    <asp:Button ID="btnCancelC" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False" />
                </div>
            </asp:Panel>
        <%--/ContentTemplate>
    </asp:UpdatePanel>--%>
    </form>
</body>
</html>
