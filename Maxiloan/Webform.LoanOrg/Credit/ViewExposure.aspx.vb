﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class ViewExposure
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "ViewExpo"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.ApplicationID = Request("ApplicationID").ToString
                Me.AgreementNo = Request("AgreementNo").ToString
                Me.CustomerName = Request("CustomerName").ToString
                lblCustName.Text = Me.CustomerName
                Me.Style = Request("Style").ToString
                Me.CustomerID = Request("CustomerID").ToString.Trim
                Bindgrid()
                Dim lb As New LinkButton
                lb = CType(Me.FindControl("lblCustName"), LinkButton)
                lb.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")
            End If
            lblMessage.Text = ""
        End If

    End Sub


    Sub Bindgrid()


        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Dim objReader As SqlDataReader
        Dim objReader1 As SqlDataReader
        '=============================================
        'Untuk Detail Agreement Exposure
        '=============================================

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAgreementExposure"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.Char, 20).Value = Me.ApplicationID.Trim

            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                lblCustName.Text = objReader.Item("CustomerName").ToString.Trim
                lblAgreementNo.Text = objReader.Item("AgreementNo").ToString.Trim
                lblMaxOSBalance.Text = FormatNumber(objReader.Item("MaxOutstandingBalance").ToString.Trim, 0)
                lblOSBalance.Text = FormatNumber(objReader.Item("OutstandingBalanceAR").ToString.Trim, 0)
                lblOSPrincipal.Text = FormatNumber(objReader.Item("OutstandingPrincipal").ToString.Trim, 0)
                lblOSInterest.Text = FormatNumber(objReader.Item("OutstandingInterest").ToString.Trim, 0)
                lblAssetRepossed.Text = objReader.Item("NumOfAssetRepossed").ToString.Trim
                lblAssetInventoried.Text = objReader.Item("NumOfAssetInventoried").ToString.Trim
                lblRescheduling.Text = objReader.Item("NumOfRescheduling").ToString.Trim
                lblAgreementTransfer.Text = objReader.Item("NumOfAgreementTransfer").ToString.Trim
                lblAssetReplacement.Text = objReader.Item("NumOfAssetReplacement").ToString.Trim

                lblBounceCheque.Text = objReader.Item("NumOfBounceCheque").ToString.Trim

                lblBucket1text.Text = objReader.Item("Bucket1_Principle").ToString.Trim
                lblBucket2text.Text = objReader.Item("Bucket2_Principle").ToString.Trim
                lblBucket3text.Text = objReader.Item("Bucket3_Principle").ToString.Trim
                lblBucket4text.Text = objReader.Item("Bucket4_Principle").ToString.Trim
                lblBucket5text.Text = objReader.Item("Bucket5_Principle").ToString.Trim
                lblBucket6Text.Text = objReader.Item("Bucket6_Principle").ToString.Trim
                lblBucket7text.Text = objReader.Item("Bucket7_Principle").ToString.Trim
                lblBucket8text.Text = objReader.Item("Bucket8_Principle").ToString.Trim
                lblBucket9text.Text = objReader.Item("Bucket9_Principle").ToString.Trim
                lblBucket10text.Text = objReader.Item("Bucket10_Principle").ToString.Trim
                lblBucket1.Text = FormatNumber(objReader.Item("Bucket1_gross").ToString.Trim, 0)
                lblBucket2.Text = FormatNumber(objReader.Item("Bucket2_gross").ToString.Trim, 0)
                lblBucket3.Text = FormatNumber(objReader.Item("Bucket3_gross").ToString.Trim, 0)
                lblBucket4.Text = FormatNumber(objReader.Item("Bucket4_gross").ToString.Trim, 0)
                lblBucket5.Text = FormatNumber(objReader.Item("Bucket5_gross").ToString.Trim, 0)
                lblBucket6.Text = FormatNumber(objReader.Item("Bucket6_gross").ToString.Trim, 0)
                lblBucket7.Text = FormatNumber(objReader.Item("Bucket7_gross").ToString.Trim, 0)
                lblBucket8.Text = FormatNumber(objReader.Item("Bucket8_gross").ToString.Trim, 0)
                lblBucket9.Text = FormatNumber(objReader.Item("Bucket9_gross").ToString.Trim, 0)
                lblBucket10.Text = FormatNumber(objReader.Item("Bucket10_gross").ToString.Trim, 0)
                lblTotalAllBucket.Text = FormatNumber(objReader.Item("TotalAllBucket").ToString.Trim, 0)
            End If
            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
            objReader.Close()
        End Try



        '=============================================
        'Untuk Datagrid Agreement Transfer Data
        '=============================================

        'Dim dtGrid As New DataTable

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.Parameters.Clear()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAgreementTransfer"
            'objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objReader1 = objCommand.ExecuteReader()
            'adapter.SelectCommand = objCommand
            'adapter.Fill(dtGrid)

            dtg.DataSource = objReader1
            dtg.DataBind()
            objReader.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim lb As New Label
        If e.Item.ItemIndex >= 0 Then
            Me.CustomerName = e.Item.Cells(0).Text
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    'End Sub
End Class