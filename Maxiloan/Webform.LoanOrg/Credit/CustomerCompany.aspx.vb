﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region


Public Class CustomerCompany
    Inherits AbsCustomerCompany

    Protected WithEvents UCAddress As UcCompanyAddress
    Protected WithEvents ucKategoriPerusahaanCustomer As ucKategoriPerusahaan
    Protected WithEvents ucKondisiKantorCustomer As ucKondisiKantor
    Protected WithEvents ucKondisiLingkungan As ucKondisiKantor
    Protected WithEvents ucLookupGroupCust1 As ucLookupGroupCust

    ''  Private m_controller As New CustomerController
 
    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New ApplicationController
    Private ocustomclass As New Parameter.Application

    Protected WithEvents UCAddress_SIUP As UcCompanyAddress
    Private time As String

#Region "Property"
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If
        'Modify by Wira 20171023
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateStart = Me.BusinessDate + " " + time
        '----------------

        If Not Page.IsPostBack Then
            UCAddress.IsRequiredZipcode = True
            'UCAddress.IsRequiredCompanyAddress = True
            UCAddress_SIUP.IsRequiredZipcode = False
            UCAddress_SIUP.IsRequiredCompanyAddress = True
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            If Request("prospectappid") = "" Then Me.ProspectAppID = "-"
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

            FillCbo("tblCompanyType")
            FillCbo("tblcompanystatus")
            FillCbo("IndustryType")
            FillCbo("tblBankAccount")
            FillCbo("tblReference")
            FillCbo("tblJobPosition")
            FillCbo("tblIndustryHeader")
            FillJenisPembiayaanCbo()
            If Me.ProspectAppID <> "-" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)
                cboBusPlaceStatus.SelectedIndex = cboBusPlaceStatus.Items.IndexOf(cboBusPlaceStatus.Items.FindByValue(ocustomclass.txtSearch.Trim))
            End If


            Me.PageAddEdit = Request("page")
            If Me.PageAddEdit = "Add" Then
                GetXML()
                lblTitle.Text = "ADD"
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)

                Dim intTotalCustomer As Integer
                intTotalCustomer = m_controller.GetTotalCustomer(Me.CustomerID.Trim, GetConnectionString)

                If intTotalCustomer > 0 Then
                    lblName.Visible = False
                    txtCustomerName.Visible = True
                Else
                    lblName.Visible = True
                    txtCustomerName.Visible = False
                End If
                cTabs.SetNavigateUrl(Request("page"), Request("id"))

            End If
            cTabs.RefreshAttr(Request("pnl"))
            txtCustomerName.Text = Me.Name
            lblName.Text = Me.Name


            'lblNPWP.Text = Me.NPWP
            UCAddress.Address = Me.Address
            UCAddress.RT = Me.RT
            UCAddress.RW = Me.RW
            UCAddress.Kelurahan = Me.Kelurahan
            UCAddress.Kecamatan = Me.Kecamatan
            UCAddress.City = Me.City
            UCAddress.ZipCode = Me.ZipCode
            UCAddress.AreaPhone1 = Me.APhone1
            UCAddress.AreaPhone2 = Me.APhone2
            UCAddress.Phone1 = Me.Phone1
            UCAddress.Phone2 = Me.Phone2
            UCAddress.AreaFax = Me.AFax
            UCAddress.Fax = Me.Fax
            UCAddress.Style = Style
            UCAddress.showMandatoryAll()
            UCAddress.Phone1ValidatorEnabled(True)
            UCAddress.BindAddress()

            If cboBusPlaceStatus.SelectedValue = "KR" Then
                rfvRentFinish.Enabled = True
                rfvRentFinish.Visible = True
                tglBerSewadiv.Visible = True
            Else
                txtTanggalSewa.Text = ""
                rfvRentFinish.Enabled = False
                rfvRentFinish.Visible = False
                tglBerSewadiv.Visible = False
            End If

            lblVRent.Visible = False
            RangeValidator5.MaximumValue = Year(Me.BusinessDate).ToString
            RangeValidator6.MaximumValue = Year(Me.BusinessDate).ToString
            Dim intLoop As Integer

        End If
    End Sub
#Region "FillCBO"
    'Sub FillJenisPembiayaanCbo()
    '    Dim oCustomer = m_controller.LoadCombo(GetConnectionString(), "JENISPEMBIAYAAN")
    '    FillCommonValueCbo(oCustomer, cboJenisPembiayaan)
    'End Sub
    Sub FillJenisPembiayaanCbo()
        Dim m_controller As New AssetMasterPriceController


        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.AssetMasterPrice
        oCustomer.strConnection = GetConnectionString()

        dtEntity = m_controller.GetAssetTypeCombo(oCustomer)

        cboJenisPembiayaan.DataSource = dtEntity.DefaultView
        cboJenisPembiayaan.DataTextField = "description"
        cboJenisPembiayaan.DataValueField = "assettypeid"
        cboJenisPembiayaan.DataBind()
        cboJenisPembiayaan.Items.Insert(0, "Select One")
        cboJenisPembiayaan.Items(0).Value = "Select One"

    End Sub

    Sub FillCbo(ByVal table As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = table
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        Select Case table
            Case "tblCompanyType"
                cboCompType.DataSource = dtEntity.DefaultView
                cboCompType.DataTextField = "Description"
                cboCompType.DataValueField = "ID"
                cboCompType.DataBind()
                cboCompType.Items.Insert(0, "Select One")
                cboCompType.Items(0).Value = "Select One"
            Case "tblcompanystatus"
                cboBusPlaceStatus.DataSource = dtEntity.DefaultView
                cboBusPlaceStatus.DataTextField = "Description"
                cboBusPlaceStatus.DataValueField = "ID"
                cboBusPlaceStatus.DataBind()
                cboBusPlaceStatus.Items.Insert(0, "Select One")
                cboBusPlaceStatus.Items(0).Value = ""
            Case "tblIndustryHeader"
                cboIndrustriHeader.DataSource = dtEntity.DefaultView
                cboIndrustriHeader.DataTextField = "Description"
                cboIndrustriHeader.DataValueField = "ID"
                cboIndrustriHeader.DataBind()
                cboIndrustriHeader.Items.Insert(0, "Select One")
                cboIndrustriHeader.Items(0).Value = "Select One"
        End Select
    End Sub
#End Region

#Region "Edit"
    Sub BindEdit(ByVal id As String)
        Dim dtEntity As DataTable = Nothing
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = id
        oCustomer.CustomerType = "C"
        oCustomer.strConnection = GetConnectionString()

        oCustomer = m_controller.GetCustomerEdit(oCustomer, "1")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        oRow = dtEntity.Rows(0)

        Me.Name = oRow("Name").ToString.Trim
        Me.NPWP = oRow("NPWP").ToString.Trim
        Me.Address = oRow("CompanyAddress").ToString.Trim
        Me.RT = oRow("CompanyRT").ToString.Trim
        Me.RW = oRow("CompanyRW").ToString.Trim
        Me.Kelurahan = oRow("CompanyKelurahan").ToString.Trim
        Me.Kecamatan = oRow("CompanyKecamatan").ToString.Trim
        Me.City = oRow("CompanyCity").ToString.Trim
        Me.ZipCode = oRow("CompanyZipCode").ToString.Trim
        Me.APhone1 = oRow("CompanyAreaPhone1").ToString.Trim
        Me.APhone2 = oRow("CompanyAreaPhone2").ToString.Trim
        Me.Phone1 = oRow("CompanyPhone1").ToString.Trim
        Me.Phone2 = oRow("CompanyPhone2").ToString.Trim
        Me.AFax = oRow("CompanyAreaFax").ToString.Trim
        Me.Fax = oRow("CompanyFax").ToString.Trim
        Me.CustomerID = id

        Me.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim
        txtKelompokUsaha.Text = oRow("CustomerGroupNm").ToString.Trim
        ucLookupGroupCust1.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim

        cboCompType.SelectedIndex = cboCompType.Items.IndexOf(cboCompType.Items.FindByValue(oRow("CompanyType").ToString.Trim))

        txtNumEmp.Text = oRow("NumberOfEmployees").ToString.Trim
        txtYearOfEst.Text = oRow("YearOfEstablished").ToString.Trim

        cboIndrustriHeader.SelectedIndex = cboIndrustriHeader.Items.IndexOf(cboIndrustriHeader.Items.FindByValue(oRow("IndustryHeaderId").ToString.Trim))
        hdfKodeIndustriBU.Value = oRow("IndustryTypeID").ToString.Trim
        txtNamaIndustriBU.Text = oRow("NameIndustri").ToString.Trim

        If Not IsDBNull(oRow("CompanyStatus")) Then
            cboBusPlaceStatus.SelectedIndex = cboBusPlaceStatus.Items.IndexOf(cboBusPlaceStatus.Items.FindByValue(oRow("CompanyStatus").ToString.Trim))
        End If

        If oRow("CompanyStatus").ToString.Trim <> "KR" Then
            tglBerSewadiv.Visible = False
        Else
            tglBerSewadiv.Visible = True
        End If

        txtBPYear.Text = oRow("CompanyStatusSinceYear").ToString.Trim

        If Not IsDBNull(oRow("RentFinishDate")) Then
            txtTanggalSewa.Text = Day(CDate(oRow("RentFinishDate"))).ToString + "/" + Month(CDate(oRow("RentFinishDate"))).ToString + "/" + Year(CDate(oRow("RentFinishDate"))).ToString
        End If

        cboIsOLS.Checked = oRow("IsOLS").ToString.Trim
        txtKeteranganUsaha.Text = oRow("KeteranganUsaha").ToString
        cboStatusUsaha.SelectedIndex = cboStatusUsaha.Items.IndexOf(cboStatusUsaha.Items.FindByValue(oRow("StatusUsaha").ToString.Trim))
        cboIndustryRisk.SelectedIndex = cboIndustryRisk.Items.IndexOf(cboIndustryRisk.Items.FindByValue(oRow("IndustryRisk").ToString.Trim))
        ucKategoriPerusahaanCustomer.SelectedKategori = oRow("KategoriPerusahaan").ToString.Trim
        ucKondisiKantorCustomer.SelectedKondisi = oRow("KondisiKantor").ToString.Trim
        ucKondisiLingkungan.SelectedKondisi = oRow("KondisiLingkungan").ToString.Trim
        txtWebsite.Text = oRow("NamaWebsite").ToString.Trim
        cboJenisPembiayaan.SelectedIndex = cboJenisPembiayaan.Items.IndexOf(cboJenisPembiayaan.Items.FindByValue(oRow("JenisPembiayaan").ToString.Trim))
        isBUMN.Checked = oRow("IsBUMN").ToString.Trim
        With UCAddress_SIUP
            .Address = oRow("CompanyAddress_SIUP").ToString.Trim
            .RT = oRow("CompanyRT_SIUP").ToString.Trim
            .RW = oRow("CompanyRW_SIUP").ToString.Trim
            .Kelurahan = oRow("CompanyKelurahan_SIUP").ToString.Trim
            .Kecamatan = oRow("CompanyKecamatan_SIUP").ToString.Trim
            .City = oRow("CompanyCity_SIUP").ToString.Trim
            .ZipCode = oRow("CompanyZipCode_SIUP").ToString.Trim
            .AreaPhone1 = oRow("CompanyAreaPhone1_SIUP").ToString.Trim
            .Phone1 = oRow("CompanyPhone1_SIUP").ToString.Trim
            .AreaPhone2 = oRow("CompanyAreaPhone2_SIUP").ToString.Trim
            .Phone2 = oRow("CompanyPhone2_SIUP").ToString.Trim
            .AreaFax = oRow("CompanyAreaFax_SIUP").ToString.Trim
            .Fax = oRow("CompanyFax_SIUP").ToString.Trim
            .showMandatoryAll()
            .Phone1ValidatorEnabled(True)
            .BindAddress()
        End With

    End Sub

#End Region
    Function Validator() As Boolean
        If cboBusPlaceStatus.SelectedValue = "KR" Then
            rfvRentFinish.Enabled = True
            rfvRentFinish.Visible = True
            tglBerSewadiv.Visible = True
        Else
            txtTanggalSewa.Text = ""
            rfvRentFinish.Enabled = False
            rfvRentFinish.Visible = False
            tglBerSewadiv.Visible = False
        End If

        If cboBusPlaceStatus.SelectedValue.Trim = "KR" Then
            If txtTanggalSewa.Text.Trim = "" Then
                lblVRent.Visible = True
                Return False
            ElseIf txtTanggalSewa.Text.Trim <> "" Then
                If CInt(ConvertDate(txtTanggalSewa.Text.Trim)) <= CInt(Me.BusinessDate.ToString("yyyyMMdd")) Then
                    lblVRent.Visible = True
                    Return False
                End If
            End If
        End If
        Return True
    End Function
    Private Sub btnPLegalAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPLegalAddress.Click
        With UCAddress_SIUP
            .Address = UCAddress.Address
            .RT = UCAddress.RT
            .RW = UCAddress.RW
            .Kelurahan = UCAddress.Kelurahan
            .Kecamatan = UCAddress.Kecamatan
            .City = UCAddress.City
            .ZipCode = UCAddress.ZipCode
            .AreaPhone1 = UCAddress.AreaPhone1
            .AreaPhone2 = UCAddress.AreaPhone2
            .AreaFax = UCAddress.AreaFax
            .Phone1 = UCAddress.Phone1
            .Phone2 = UCAddress.Phone2
            .Fax = UCAddress.Fax
            .BindAddress()
        End With
    End Sub
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ocustomer As New Parameter.Customer
        Dim oAddress As New Parameter.Address
        Dim oWarehouseAddress As New Parameter.Address
        Dim oCompAddress As New Parameter.Address
        Dim oCompAddress_SIUP As New Parameter.Address

        Dim no As Integer = 1
        gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

        If Validator() = False Then
            Exit Sub
        End If

        Try
            ocustomer.BranchId = Me.sesBranchId
            ocustomer.Name = txtCustomerName.Text
            ocustomer.CustomerType = "C"
            ocustomer.CompanyType = cboCompType.SelectedValue
            ocustomer.PersonalNPWP = Me.NPWP
            ocustomer.NumEmployees = txtNumEmp.Text
            ocustomer.YearEstablished = txtYearOfEst.Text
            If (hdfKodeIndustriBU.Value.Trim = "") Then
                Dim err = "Silahkan pilih Bidang Usaha Detail."
                ShowMessage(lblMessage, err, True)
                Exit Sub
            End If
            ocustomer.IndustryTypeID = hdfKodeIndustriBU.Value.Trim
            ocustomer.CustomerGroupID = Me.CustomerGroupID
            ocustomer.BusinessDate = Me.BusinessDate

            oAddress.Address = UCAddress.Address
            oAddress.RT = UCAddress.RT
            oAddress.RW = UCAddress.RW
            oAddress.Kelurahan = UCAddress.Kelurahan
            oAddress.Kecamatan = UCAddress.Kecamatan
            oAddress.City = UCAddress.City
            oAddress.ZipCode = UCAddress.ZipCode
            oAddress.AreaPhone1 = UCAddress.AreaPhone1
            oAddress.Phone1 = UCAddress.Phone1
            oAddress.AreaPhone2 = UCAddress.AreaPhone2
            oAddress.Phone2 = UCAddress.Phone2
            oAddress.AreaFax = UCAddress.AreaFax
            oAddress.Fax = UCAddress.Fax

            ocustomer.OmsetBulanan = 0
            ocustomer.BiayaBulanan = 0
            ocustomer.Ratio = ""
            ocustomer.ROI = ""
            ocustomer.DER = ""

            'Pindah ke form Hasil Survey
            ocustomer.BankAccountType = Nothing
            ocustomer.AverageBalance = "0"

            ocustomer.Deposito = ""
            ocustomer.AdditionalCollateralType = ""
            ocustomer.AdditionalCollateralAmount = ""
            ocustomer.BusPlaceStatus = cboBusPlaceStatus.SelectedValue
            ocustomer.BusPlaceSinceYear = txtBPYear.Text
            ocustomer.RentFinishDate = ConvertDate(txtTanggalSewa.Text)
            ocustomer.ResikoUsaha = 0

            ocustomer.BankBranchId = 0
            ocustomer.BankID = ""
            ocustomer.BankBranch = ""
            ocustomer.AccountNo = ""
            ocustomer.AccountName = ""
            ocustomer.Reference = String.Empty   '  cboReference.SelectedValue
            ocustomer.Notes = Nothing  ' txtNotes.Text.Trim
            ocustomer.ProspectAppID = Me.ProspectAppID
            ocustomer.JumlahKendaraan = Nothing ' CInt(IIf(txtJumlahKendaraan.Text.Trim = "", 0, txtJumlahKendaraan.Text))
            ocustomer.Garasi = Nothing ' CBool(rboGarasi.SelectedValue)
            ocustomer.PertamaKredit = Nothing ' cboPertamaKredit.SelectedValue
            ocustomer.OrderKe = Nothing ' CInt(IIf(txtOrderKe.Text.Trim = "", 0, txtOrderKe.Text))

            ocustomer.IsOLS = cboIsOLS.Checked
            ocustomer.IsBUMN = isBUMN.Checked

            ocustomer.IndustryRisk = cboIndustryRisk.SelectedValue

            Dim oCompanyCustomerDPEX As New Parameter.CompanyCustomerDPEx
            With oCompanyCustomerDPEX
                .KeteranganUsaha = txtKeteranganUsaha.Text.Trim
                .StatusUsaha = cboStatusUsaha.SelectedValue
                .KategoriPerusahaan = ucKategoriPerusahaanCustomer.SelectedKategori
                .KondisiKantor = ucKondisiKantorCustomer.SelectedKondisi
                .KondisiLingkungan = ucKondisiLingkungan.SelectedKondisi
                .NamaWebsite = txtWebsite.Text.Trim
                .JenisPembiayaan = cboJenisPembiayaan.SelectedValue
            End With

            With oCompAddress_SIUP
                .Address = UCAddress_SIUP.Address
                .RT = UCAddress_SIUP.RT
                .RW = UCAddress_SIUP.RW
                .Kelurahan = UCAddress_SIUP.Kelurahan
                .Kecamatan = UCAddress_SIUP.Kecamatan
                .City = UCAddress_SIUP.City
                .ZipCode = UCAddress_SIUP.ZipCode
                .AreaPhone1 = UCAddress_SIUP.AreaPhone1
                .Phone1 = UCAddress_SIUP.Phone1
                .AreaPhone2 = UCAddress_SIUP.AreaPhone2
                .Phone2 = UCAddress_SIUP.Phone2
                .AreaFax = UCAddress_SIUP.AreaFax
                .Fax = UCAddress_SIUP.Fax
            End With

            Dim oReturn As New Parameter.Customer
            ocustomer.strConnection = GetConnectionString()

            If Me.PageAddEdit = "Add" Then
                oReturn = m_controller.CompanyDataPerusahaanSaveAdd(ocustomer, _
                                                      oAddress, _
                                                      oWarehouseAddress, _
                                                      oCompanyCustomerDPEX, _
                                                      oCompAddress_SIUP)
                If oReturn.Err = "" Then
                    Me.CustomerID = oReturn.CustomerID
                    If File.Exists(gStrPath & gStrFileName) Then
                        File.Delete(gStrPath & gStrFileName)
                    End If
                Else
                    ShowMessage(lblMessage, oReturn.Err, True)
                    Exit Sub
                End If
            Else
                Dim Err As String = ""
                ocustomer.CustomerID = Me.CustomerID
                Err = m_controller.CompanyDataPerusahaanSaveEdit(ocustomer, _
                                                      oAddress, _
                                                      oWarehouseAddress, _
                                                      oCompanyCustomerDPEX, _
                                                      oCompAddress_SIUP)
                If Err <> "" Then
                    ShowMessage(lblMessage, Err, True)
                    Exit Sub
                End If
            End If
            'Response.Redirect("CustomerCompanyLegalitas.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabLegalitas")
            'Modify by WIra 20171023
            If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                Response.Redirect("CustomerCompanyLegalitas.aspx?page=Add&id=" + Me.CustomerID + "&pnl=tabLegalitas&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
            Else
                Response.Redirect("CustomerCompanyLegalitas.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabLegalitas&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try

    End Sub
#End Region
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
#Region "LookupCustomer"
    Protected Sub btnLookupCustomer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupCustomer.Click
        ucLookupGroupCust1.CmdWhere = "All"
        ucLookupGroupCust1.Sort = "CustomerGroupID ASC"
        ucLookupGroupCust1.Popup()
    End Sub
    Public Sub CatSelectedCustomer(ByVal CustomerGroupID As String,
                                           ByVal CustomerGroupName As String)
        txtKelompokUsaha.Text = CustomerGroupName
        Me.CustomerGroupID = CustomerGroupID
    End Sub
#End Region

#Region "OnChange"
    Sub cboBusPlaceStatus_IndexChanged()
        If cboBusPlaceStatus.SelectedValue = "KR" Then
            rfvRentFinish.Enabled = True
            rfvRentFinish.Visible = True
            tglBerSewadiv.Visible = True
        Else
            txtTanggalSewa.Text = ""
            rfvRentFinish.Enabled = False
            rfvRentFinish.Visible = False
            tglBerSewadiv.Visible = False
        End If
    End Sub
#End Region
End Class