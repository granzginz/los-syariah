﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerPersonalKeluarga.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerPersonalKeluarga" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="CustomerTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Personal</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
       
        var Page_Validators = new Array();

    </script>    
</head>
<body>    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <uct:tabs id='cTabs' runat='server' />
    <%--div class="tab_container">
        <div id="tabIdentitas" runat="server">
            <asp:HyperLink ID="hyIdentitas" runat="server" Text="IDENTITAS"></asp:HyperLink>
        </div>
        <div id="tabPekerjaan" runat="server">
            <asp:HyperLink ID="hyPekerjaan" runat="server" Text="PEKERJAAN"></asp:HyperLink>
        </div>
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>
        <div id="tabPasangan" runat="server">
            <asp:HyperLink ID="hyPasangan" runat="server" Text="PASANGAN"></asp:HyperLink>
        </div>
        <div id="tabEmergency" runat="server">
            <asp:HyperLink ID="hyEmergency" runat="server" Text="EMERGENCY CONTACT"></asp:HyperLink>
        </div>
        <div id="tabKeluarga" runat="server">
            <asp:HyperLink ID="hyKeluarga" runat="server" Text="KELUARGA"></asp:HyperLink>
        </div>
    </div>--%>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"  onclick="hideMessage();"></asp:Label>
        <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                DATA KELUARGA
            </h4>
        </div>
    </div>                        
        <asp:Panel runat="server" ID="pnlKeluarga">        
        <asp:UpdatePanel runat="server" ID="up3">
        <ContentTemplate>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="Datagrid1" runat="server" Width="100%" CellSpacing="1" CellPadding="3"
                            BorderWidth="0" AutoGenerateColumns="False" DataKeyField="Name" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NAMA">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtFamilyName" runat="server" MaxLength="50"></asp:TextBox>
                                        <asp:Label ID="lblValidFName" runat="server" Visible="False">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NO IDENTITAS">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtFamilyIDNumber" runat="server" MaxLength="25"></asp:TextBox>
                                        <asp:Label ID="lblVFamilyIDNumber" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TANGGAL LAHIR">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <uc6:ucdatece id="uctgllahir" runat="server"></uc6:ucdatece>
                                        <%--<asp:TextBox ID="uscFamilyBirthDate" runat="server"></asp:TextBox>
                                        <asp:CalendarExtender ID="uscFamilyBirthDate_CalendarExtender" runat="server" Enabled="True"
                                            TargetControlID="uscFamilyBirthDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                            ControlToValidate="uscFamilyBirthDate" CssClass="validator_general"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!"
                                            ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="uscFamilyBirthDate"
                                            SetFocusOnError="true" Display="Dynamic" CssClass="validator_general"> 
                                        </asp:RegularExpressionValidator>--%>
                                        <asp:Label ID="lblVFamilyBirthDate0" runat="server" Visible="False">Tanggal Lahir harus lebih kecil dari hari ini</asp:Label>
                                        <asp:Label ID="lblVFamilyBirthDate" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="HUBUNGAN KELUARGA">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cboFamilyRelationship" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblValidFRelationship" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" CausesValidation="false" CommandName="Delete" runat="server"
                                            ImageUrl="../../Images/icondelete.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:Button ID="btnPAdd" runat="server" Text="Add" CssClass="small buttongo blue"
                        CausesValidation="False"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Panel>        
        <div class="form_button">
        <asp:Button ID="btnPSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnPCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div> 
		</ContentTemplate>
    </asp:UpdatePanel>             
    </form>
</body>
</html>
