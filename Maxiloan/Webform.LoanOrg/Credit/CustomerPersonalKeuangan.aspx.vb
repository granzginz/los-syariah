﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class CustomerPersonalKeuangan
    Inherits AbsCustomerPersonal

#Region "uc control declaration"        
    Protected WithEvents UcBankAccount1 As UcBankAccount    

    Protected WithEvents txtFDDeposito As ucNumberFormat
    Protected WithEvents txtFDLivingCost As ucNumberFormat
    'Protected WithEvents txtFDCollAmt As ucNumberFormat
    Protected WithEvents txtJDMonthlyFixedIncome As ucNumberFormat
    Protected WithEvents txtJDMonthlyVariableIncome As ucNumberFormat


    Protected WithEvents txtPenghasilanTetapPasangan As ucNumberFormat
    Protected WithEvents txtPenghasilanTambahanPasangan As ucNumberFormat
    Protected WithEvents txtPenghasilanTetapPenjamin As ucNumberFormat
    Protected WithEvents txtPenghasilanTambahanPenjamin As ucNumberFormat
    Protected WithEvents txtAngsuranLain As ucNumberFormat 
    Private time As String

#End Region
#Region "Property"
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")                        

            'Modify by Wira 20171010---
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------
            If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                    .CustomerId = Me.CustomerID
                End With
                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)
                Me.StatusPerkawinan = ocustomclass.MaritalStatus
            End If
            '--------------------------------------------------

            Dim intLoop As Integer
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

            Me.PageAddEdit = Request("page")

            If Me.PageAddEdit = "Add" Then
                UcBankAccount1.BindBankAccount()
                lblTitle.Text = "ADD"
                GetXML()
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                cTabs.SetNavigateUrl(Request("page"), Request("id"))

            End If
            cTabs.RefreshAttr(Request("pnl"))
            If Me.StatusPerkawinan.ToUpper.Trim <> "M" Then
                cTabs.EnabledLink("tabPasangan", False)
            End If

            If Me.PageAddEdit <> "Add" Then
                Dim intTotalCustomer As Integer
                intTotalCustomer = m_controller.GetTotalCustomer(Me.CustomerID.Trim, GetConnectionString)
            End If

            InitiateUCnumberFormat(txtFDDeposito, False, True)
            InitiateUCnumberFormat(txtFDLivingCost, True, True)
            'InitiateUCnumberFormat(txtFDCollAmt, False, True)
            InitiateUCnumberFormat(txtJDMonthlyFixedIncome, True, True)
            InitiateUCnumberFormat(txtJDMonthlyVariableIncome, True, True)

            InitiateUCnumberFormat(txtPenghasilanTetapPasangan, False, True)
            InitiateUCnumberFormat(txtPenghasilanTambahanPasangan, False, True)
            InitiateUCnumberFormat(txtPenghasilanTetapPenjamin, False, True)
            InitiateUCnumberFormat(txtPenghasilanTambahanPenjamin, False, True)
            InitiateUCnumberFormat(txtAngsuranLain, False, True)
            'cboRataPenghasilanPerTahun.ClearSelection()
        Else
            HitungSisaPenghasilan()
        End If
    End Sub
     
#End Region
 
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Try
            Dim ocustomer As New Parameter.Customer

            'If txtFDLivingCost.Text = "0" Or txtJDMonthlyFixedIncome.Text = "0" Then
            '    ShowMessage(lblMessage, "Penghasilan dan pengeluaran tidak boleh 0", True)
            '    Exit Sub
            'End If

            ocustomer.Deposito = txtFDDeposito.Text
            ocustomer.LivingCostAmount = txtFDLivingCost.Text
            ' ocustomer.AdditionalCollateralType = txtFDCollType.Text
            'ocustomer.AdditionalCollateralAmount = txtFDCollAmt.Text
            ocustomer.BankBranchId = CInt(IIf(UcBankAccount1.BankBranchId.Trim = "", "0", UcBankAccount1.BankBranchId.Trim))
            ocustomer.BankID = UcBankAccount1.BankID
            ocustomer.BankBranch = UcBankAccount1.BankBranch
            ocustomer.AccountNo = UcBankAccount1.AccountNo
            ocustomer.AccountName = UcBankAccount1.AccountName
            ocustomer.MonthlyFixedIncome = txtJDMonthlyFixedIncome.Text
            ocustomer.MonthlyVariableIncome = txtJDMonthlyVariableIncome.Text



            ocustomer.PenghasilanTetapPasangan = CDec(txtPenghasilanTetapPasangan.Text)
            ocustomer.PenghasilanTambahanPasangan = CDec(txtPenghasilanTambahanPasangan.Text)
            ocustomer.PenghasilanTetapPenjamin = CDec(txtPenghasilanTetapPenjamin.Text)
            ocustomer.PenghasilanTambahanPenjamin = CDec(txtPenghasilanTambahanPenjamin.Text)
            ocustomer.AngsuranLainnya = CDec(txtAngsuranLain.Text)
            ocustomer.RataRataPenghasilanPerTahun = cboRataPenghasilanPerTahun.SelectedValue
            'ocustomer.SumberPembiayaan = txtSumberPembiayaan.Text.Trim
            ocustomer.KodeSumberPenghasilan = cboKodeSumberPenghasilan.SelectedValue


            Dim Err As String = ""
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID
            Err = m_controller.PersonalCustomerKeuanganSaveEdit(ocustomer)
            If Err = "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If

            If Me.StatusPerkawinan.ToUpper.Trim <> "M" Then
                'Modify by WIra 20171011
                'Response.Redirect("CustomerPersonalEmergency.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabEmergency")
                Response.Redirect("CustomerPersonalEmergency.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabEmergency&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
            Else
                'Modify by WIra 20171011
                'Response.Redirect("CustomerPersonalPasangan.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabPasangan")
                Response.Redirect("CustomerPersonalPasangan.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabPasangan&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
            End If

        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
#End Region
#Region "imbPCancel_Click"
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
#End Region
#Region "Edit"
    Sub BindEdit(ByVal ID As String)
        Dim dtEntity As New DataTable
        Dim dtOmset As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = ID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "5")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        Me.Name = oRow("Name").ToString.Trim

        Me.IDType = oRow("IDType").ToString.Trim
        Me.IDTypeName = oRow("IDTypeName").ToString.Trim
        Me.IDNumber = oRow("IDNumber").ToString.Trim
        Me.Gender = oRow("Gender").ToString.Trim
        If IsDate(oRow("IDExpiredDate").ToString) Then Me.IDExpiredDate = Format(oRow("IDExpiredDate"), "dd/MM/yyyy")

        If Not IsDBNull(oRow("BirthDate")) Then
            Me.BirthDate = Day(CDate(oRow("BirthDate"))).ToString + "/" + Month(CDate(oRow("BirthDate"))).ToString + "/" + Year(CDate(oRow("BirthDate"))).ToString
        Else
            Me.BirthDate = ""
        End If

        Me.BirthPlace = oRow("BirthPlace").ToString.Trim
        Me.Type = oRow("PersonalCustomerType").ToString.Trim

        Me.MotherName = oRow("MotherName").ToString.Trim
        Me.NamaPasangan = oRow("SINamaSuamiIstri").ToString.Trim
        Me.KartuKeluarga = oRow("NoKK").ToString.Trim
        Me.StatusPerkawinan = oRow("MaritalStatus").ToString.Trim

        txtFDDeposito.Text = FormatNumber(oRow("Deposito"), 0)
        txtFDLivingCost.Text = FormatNumber(oRow("LivingCostAmount"), 0)
        'txtFDCollType.Text = oRow("AdditionalCollateralType").ToString.Replace("-", "").Trim
        'txtFDCollAmt.Text = FormatNumber(oRow("AdditionalCollateralAmount").ToString.Trim, 0)

        UcBankAccount1.BankBranchId = oRow("BankBranchID").ToString.Replace("-", "").Trim
        UcBankAccount1.BindBankAccount()
        UcBankAccount1.BankID = oRow("BankID").ToString.Replace("-", "").Trim

        UcBankAccount1.BankBranch = oRow("BankBranch").ToString.Replace("-", "").Trim
        UcBankAccount1.AccountNo = oRow("AccountNo").ToString.Replace("-", "").Trim
        UcBankAccount1.AccountName = oRow("AccountName").ToString.Replace("-", "")

        txtJDMonthlyFixedIncome.Text = FormatNumber(oRow("MonthlyFixedIncome"), 0)
        txtJDMonthlyVariableIncome.Text = FormatNumber(oRow("MonthlyVariableIncome"), 0)


        txtPenghasilanTetapPasangan.text = FormatNumber(oRow("PenghasilanTetapPasangan"), 0)
        txtPenghasilanTambahanPasangan.text = FormatNumber(oRow("PenghasilanTambahanPasangan"), 0)
        txtPenghasilanTetapPenjamin.text = FormatNumber(oRow("PeghasilanTetapPenjamin"), 0)
        txtPenghasilanTambahanPenjamin.text = FormatNumber(oRow("PeghasilanTambahanPenjamin"), 0)
        txtAngsuranLain.Text = FormatNumber(oRow("AngsuranLainnya"), 0)
        cboRataPenghasilanPerTahun.SelectedIndex = cboRataPenghasilanPerTahun.Items.IndexOf(cboRataPenghasilanPerTahun.Items.FindByValue(oRow("RataRataPenghasilanPerTahun").ToString.Trim))
        'txtSumberPembiayaan.Text = oRow("SumberPembiayaan").ToString.Trim
        cboKodeSumberPenghasilan.SelectedIndex = cboKodeSumberPenghasilan.Items.IndexOf(cboKodeSumberPenghasilan.Items.FindByValue(oRow("SumberPembiayaan").ToString.Trim))

        HitungSisaPenghasilan()
        'Jika status perkawinan bukan 'M', maka penghasilan tetap pasangan dan penghasilan tambahan pasanagan didisable
        'Wira 2016-01-15 
        If StatusPerkawinan = "M" Then
            txtPenghasilanTetapPasangan.Enabled = True
            txtPenghasilanTambahanPasangan.Enabled = True
        Else
            txtPenghasilanTetapPasangan.Enabled = False
            txtPenghasilanTambahanPasangan.Enabled = False
        End If
    End Sub

    Sub HitungSisaPenghasilan()
        Dim MonthlyFixedIncome As Decimal = CDec(txtJDMonthlyFixedIncome.Text)
        Dim MonthlyVariableIncome As Decimal = CDec(txtJDMonthlyVariableIncome.Text)
        Dim PenghasilanTetapPasangan As Decimal = CDec(txtPenghasilanTetapPasangan.Text)
        Dim PenghasilanTambahanPasangan As Decimal = CDec(txtPenghasilanTambahanPasangan.Text)

        Dim PeghasilanTetapPenjamin As Decimal = CDec(txtPenghasilanTetapPenjamin.Text)
        Dim PeghasilanTambahanPenjamin As Decimal = CDec(txtPenghasilanTambahanPenjamin.Text)
        Dim LivingCostAmount As Decimal = CDec(txtFDLivingCost.Text)
        Dim AngsuranLainnya As Decimal = CDec(txtAngsuranLain.Text)

        Dim income = MonthlyFixedIncome + MonthlyVariableIncome + PenghasilanTetapPasangan +
                    PenghasilanTambahanPasangan + PeghasilanTetapPenjamin + PeghasilanTambahanPenjamin

        Dim cost = LivingCostAmount + AngsuranLainnya
        lblSisaPenghasilan.Text = FormatNumber(income - cost, 0)
    End Sub

#End Region
  
End Class