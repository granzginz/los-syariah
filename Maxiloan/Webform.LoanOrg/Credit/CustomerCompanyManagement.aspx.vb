﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region

Public Class CustomerCompanyManagement
    Inherits AbsCustomerCompany

    Protected WithEvents UCAddressPJ As UcCompanyAddress
     
  
    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New ApplicationController
    Private ocustomclass As New Parameter.Application

    Protected WithEvents txtShare1 As ucNumberFormat
    Private time As String
#Region "Property"
    'Private Property ProspectAppID() As String
    '    Get
    '        Return CType(ViewState("ProspectAppID"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("ProspectAppID") = Value
    '    End Set
    'End Property
    'Property PageAddEdit() As String
    '    Get
    '        Return CType(ViewState("Page"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("Page") = Value
    '    End Set
    'End Property
    'Property CustomerID() As String
    '    Get
    '        Return CType(ViewState("CustomerID"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("CustomerID") = Value
    '    End Set
    'End Property
    'Property Name() As String
    '    Get
    '        Return CType(ViewState("Name"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("Name") = Value
    '    End Set
    'End Property
    'Property Address() As String
    '    Get
    '        Return CType(ViewState("Address"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("Address") = Value
    '    End Set
    'End Property
    'Property RT() As String
    '    Get
    '        Return CType(ViewState("RT"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("RT") = Value
    '    End Set
    'End Property
    'Property RW() As String
    '    Get
    '        Return CType(ViewState("RW"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("RW") = Value
    '    End Set
    'End Property
    'Property City() As String
    '    Get
    '        Return CType(ViewState("City"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("City") = Value
    '    End Set
    'End Property
    'Property Kecamatan() As String
    '    Get
    '        Return CType(ViewState("Kecamatan"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("Kecamatan") = Value
    '    End Set
    'End Property
    'Property Kelurahan() As String
    '    Get
    '        Return CType(ViewState("Kelurahan"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("Kelurahan") = Value
    '    End Set
    'End Property
    'Property NPWP() As String
    '    Get
    '        Return CType(ViewState("NPWP"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("NPWP") = Value
    '    End Set
    'End Property
    'Property ZipCode() As String
    '    Get
    '        Return CType(ViewState("ZipCode"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("ZipCode") = Value
    '    End Set
    'End Property
    'Property AFax() As String
    '    Get
    '        Return CType(ViewState("AFax"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("AFax") = Value
    '    End Set
    'End Property
    'Property Fax() As String
    '    Get
    '        Return CType(ViewState("Fax"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("Fax") = Value
    '    End Set
    'End Property
    'Property APhone1() As String
    '    Get
    '        Return CType(ViewState("APhone1"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("APhone1") = Value
    '    End Set
    'End Property
    'Property Phone1() As String
    '    Get
    '        Return CType(ViewState("Phone1"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("Phone1") = Value
    '    End Set
    'End Property
    'Property APhone2() As String
    '    Get
    '        Return CType(ViewState("APhone2"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("APhone2") = Value
    '    End Set
    'End Property
    'Property Phone2() As String
    '    Get
    '        Return CType(ViewState("Phone2"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("Phone2") = Value
    '    End Set
    'End Property
    'Property CustomerGroupID() As String
    '    Get
    '        Return CType(ViewState("CustomerGroupID"), String)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState("CustomerGroupID") = Value
    '    End Set
    'End Property
    Property StrValid() As String
        Get
            Return CType(ViewState("StrValid"), String)
        End Get
        Set(ByVal StrValid As String)
            ViewState("StrValid") = StrValid
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            If Request("prospectappid") = "" Then Me.ProspectAppID = "-"
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"            
            UCAddressPJ.IsRequiredZipcode = True
            FillCbo("tblCompanyType")
            FillCbo("tblcompanystatus")
            FillCbo("IndustryType")
            FillCbo("tblBankAccount")
            FillCbo("tblReference")
            FillCbo("tblJobPosition")

            'Modify by Wira 20171023
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------

            If Me.ProspectAppID <> "-" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)                
            End If
             

            Me.PageAddEdit = Request("page")
            If Me.PageAddEdit = "Add" Then
                GetXML()                
                lblTitle.Text = "ADD"
                AddRecord1()                
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                EditRecord1(Me.CustomerID)                
                cTabs.SetNavigateUrl(Request("page"), Request("id")) 
            End If
            cTabs.RefreshAttr(Request("pnl"))
            If Me.PageAddEdit <> "Add" Then
                Dim intTotalCustomer As Integer
                intTotalCustomer = m_controller.GetTotalCustomer(Me.CustomerID.Trim, GetConnectionString)                
            End If

            Dim intLoop As Integer
            If intLoop <= dtgList1.Items.Count - 1 Then
                CType(dtgList1.Items(intLoop).FindControl("lblVName1"), Label).Visible = False
                CType(dtgList1.Items(intLoop).FindControl("lblVGender"), Label).Visible = False
                CType(dtgList1.Items(intLoop).FindControl("lblVIDNumber1"), Label).Visible = False
                CType(dtgList1.Items(intLoop).FindControl("lblVPosition1"), Label).Visible = False
                CType(dtgList1.Items(intLoop).FindControl("lblVAddress1"), Label).Visible = False
                CType(dtgList1.Items(intLoop).FindControl("lblVPhone1"), Label).Visible = False
                'CType(dtgList1.Items(intLoop).FindControl("lblVShare1"), Label).Visible = False

                Dim txtShare1 As New ucNumberFormat
                txtShare1 = CType(dtgList1.Items(intLoop).FindControl("txtShare1"), ucNumberFormat)
                With txtShare1
                    .RequiredFieldValidatorEnable = True
                    .RangeValidatorEnable = True
                    .TextCssClass = "numberAlign regular_text"
                    .RangeValidatorMinimumValue = "0"
                    .RangeValidatorMaximumValue = "100"
                End With
            End If
             
        End If
    End Sub
#Region "FillCBO"
    Sub FillCbo(ByVal table As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = table
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        Select Case table           
            Case "tblJobPosition"
                cboJabatan.DataSource = dtEntity.DefaultView
                cboJabatan.DataTextField = "Description"
                cboJabatan.DataValueField = "ID"
                cboJabatan.DataBind()
                cboJabatan.Items.Insert(0, "Select One")
                cboJabatan.Items(0).Value = "Select One"
        End Select
    End Sub
#End Region
#Region "XML"
    'Sub GetXML()
    '    If File.Exists(gStrPath & gStrFileName) Then
    '        Me.Name = Get_XMLValue(gStrPath & gStrFileName, "Name")
    '        Me.NPWP = Get_XMLValue(gStrPath & gStrFileName, "NPWP")
    '        Me.Address = Get_XMLValue(gStrPath & gStrFileName, "Address")
    '        Me.RT = Get_XMLValue(gStrPath & gStrFileName, "RT")
    '        Me.RW = Get_XMLValue(gStrPath & gStrFileName, "RW")
    '        Me.Kelurahan = Get_XMLValue(gStrPath & gStrFileName, "Kelurahan")
    '        Me.Kecamatan = Get_XMLValue(gStrPath & gStrFileName, "Kecamatan")
    '        Me.City = Get_XMLValue(gStrPath & gStrFileName, "City")
    '        Me.ZipCode = Get_XMLValue(gStrPath & gStrFileName, "ZipCode")
    '        Me.APhone1 = Get_XMLValue(gStrPath & gStrFileName, "APhone1")
    '        Me.Phone1 = Get_XMLValue(gStrPath & gStrFileName, "Phone1")
    '        Me.APhone2 = Get_XMLValue(gStrPath & gStrFileName, "APhone2")
    '        Me.Phone2 = Get_XMLValue(gStrPath & gStrFileName, "Phone2")
    '        Me.AFax = Get_XMLValue(gStrPath & gStrFileName, "AFax")
    '        Me.Fax = Get_XMLValue(gStrPath & gStrFileName, "Fax")
    '    End If
    'End Sub
    'Private Function Get_XMLValue(ByVal pStrFile As String, ByVal pStrColumn As String) As String
    '    Dim lObjDSCust As New DataSet
    '    Dim lStrName As String
    '    Try
    '        lObjDSCust.ReadXml(pStrFile)
    '        lStrName = lObjDSCust.Tables("Cust").Rows(0)("" & pStrColumn & "").ToString
    '    Catch ex As Exception
    '        'Response.Redirect("WriteXML.aspx")
    '    Finally
    '        lObjDSCust = Nothing
    '    End Try
    '    Return lStrName
    'End Function
#End Region
#Region "Edit"
    Sub BindEdit(ByVal id As String)
        Dim dtEntity As DataTable = Nothing
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = id
        oCustomer.CustomerType = "C"
        oCustomer.strConnection = GetConnectionString()

        oCustomer = m_controller.GetCustomerEdit(oCustomer, "1")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        oRow = dtEntity.Rows(0)

        Me.Name = oRow("Name").ToString.Trim
        Me.NPWP = oRow("NPWP").ToString.Trim
        Me.Address = oRow("CompanyAddress").ToString.Trim
        Me.RT = oRow("CompanyRT").ToString.Trim
        Me.RW = oRow("CompanyRW").ToString.Trim
        Me.Kelurahan = oRow("CompanyKelurahan").ToString.Trim
        Me.Kecamatan = oRow("CompanyKecamatan").ToString.Trim
        Me.City = oRow("CompanyCity").ToString.Trim
        Me.ZipCode = oRow("CompanyZipCode").ToString.Trim
        Me.APhone1 = oRow("CompanyAreaPhone1").ToString.Trim
        Me.APhone2 = oRow("CompanyAreaPhone2").ToString.Trim
        Me.Phone1 = oRow("CompanyPhone1").ToString.Trim
        Me.Phone2 = oRow("CompanyPhone2").ToString.Trim
        Me.AFax = oRow("CompanyAreaFax").ToString.Trim
        Me.Fax = oRow("CompanyFax").ToString.Trim
        Me.CustomerID = id

        Me.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim        
        txtNamaPJ.Text = oRow("CCPJNama").ToString.Trim
        cboJabatan.SelectedIndex = cboJabatan.Items.IndexOf(cboJabatan.Items.FindByValue(oRow("CCPJJabatan").ToString.Trim))

        With UCAddressPJ
            .Address = oRow("CCPJAlamat").ToString.Trim
            .RT = oRow("CCPJRT").ToString.Trim
            .RW = oRow("CCPJRW").ToString.Trim
            .Kelurahan = oRow("CCPJKelurahan").ToString.Trim
            .Kecamatan = oRow("CCPJKecamatan").ToString.Trim
            .City = oRow("CCPJKota").ToString.Trim
            .ZipCode = oRow("CCPJKodePos").ToString.Trim
            .AreaPhone1 = oRow("CCPJAreaPhone1").ToString.Trim
            .Phone1 = oRow("CCPJPhone1").ToString.Trim
            .showMandatoryAll()
            .Phone1ValidatorEnabled(True)
            .BindAddress()
        End With

        txtNoHP.Text = oRow("CCPJMobilePhone").ToString.Trim
        txtEmail.Text = oRow("CCPJEmail").ToString.Trim
        txtNPWP.Text = oRow("CCPJNPWP").ToString.Trim        

    End Sub

    Private Sub EditRecord1(ByVal id As String)
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim lblNo As New Label
        Dim txtName As New TextBox
        Dim txtIDNumber As New TextBox
        Dim cboPosition As New DropDownList
        Dim txtAddress As New TextBox
        Dim txtPhone As New TextBox
        Dim oCustomer As New Parameter.Customer
        Dim cboGender As New DropDownList
        Dim chkPersetujuan As CheckBox

        oCustomer.CustomerID = id
        oCustomer.PersonalCustomerType = "c"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "2")
        objectDataTable = oCustomer.listdata
        dtgList1.DataSource = objectDataTable
        dtgList1.DataBind()
        fillCboPosition()
        For intLoopGrid = 0 To dtgList1.Items.Count - 1
            Dim txtShare1 As New ucNumberFormat
            txtShare1 = CType(dtgList1.Items(intLoopGrid).FindControl("txtShare1"), ucNumberFormat)
            With txtShare1
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .TextCssClass = "numberAlign regular_text"
                .RangeValidatorMinimumValue = "0"
                .RangeValidatorMaximumValue = "100"
            End With

            lblNo = CType(dtgList1.Items(intLoopGrid).Cells(0).FindControl("lblNo1"), Label)
            txtName = CType(dtgList1.Items(intLoopGrid).Cells(1).FindControl("txtName1"), TextBox)
            cboGender = CType(dtgList1.Items(intLoopGrid).Cells(2).FindControl("cboGender"), DropDownList)
            txtIDNumber = CType(dtgList1.Items(intLoopGrid).Cells(3).FindControl("txtIDNumber1"), TextBox)
            cboPosition = CType(dtgList1.Items(intLoopGrid).Cells(4).FindControl("cboPosition1"), DropDownList)
            txtAddress = CType(dtgList1.Items(intLoopGrid).Cells(5).FindControl("txtAddress1"), TextBox)
            txtPhone = CType(dtgList1.Items(intLoopGrid).Cells(6).FindControl("txtPhone1"), TextBox)
            chkPersetujuan = CType(dtgList1.Items(intLoopGrid).FindControl("chkPersetujuan"), CheckBox)

            lblNo.Text = objectDataTable.Rows(intLoopGrid).Item("No").ToString.Trim
            txtName.Text = objectDataTable.Rows(intLoopGrid).Item("Name").ToString.Trim
            txtIDNumber.Text = objectDataTable.Rows(intLoopGrid).Item("IDNumber").ToString.Trim
            If Not IsDBNull(objectDataTable.Rows(intLoopGrid).Item("Position")) Or objectDataTable.Rows(intLoopGrid).Item("Position").ToString.Trim <> "" Then
                cboPosition.SelectedIndex = cboPosition.Items.IndexOf(cboPosition.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item("Position").ToString.Trim))
            End If
            txtAddress.Text = objectDataTable.Rows(intLoopGrid).Item("Address").ToString.Trim
            txtPhone.Text = objectDataTable.Rows(intLoopGrid).Item("Phone").ToString.Trim
            txtShare1.Text = FormatNumber(IIf(objectDataTable.Rows(intLoopGrid).Item("Share").ToString.Trim = "", "0", objectDataTable.Rows(intLoopGrid).Item("Share").ToString.Trim), 2)
            cboGender.SelectedIndex = cboGender.Items.IndexOf(cboGender.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item("Gender").ToString.Trim))
            chkPersetujuan.Checked = CBool(objectDataTable.Rows(intLoopGrid).Item("DefaultApproval"))

        Next
        If dtgList1.Items.Count = 0 Then
            AddRecord1()
        End If
    End Sub    
#End Region
#Region "Baris DtgList1"
    Private Sub imbAdd1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd1.Click
        AddRecord1()
    End Sub
    Private Sub AddRecord1()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo As New Label
        Dim txtName As New TextBox
        Dim txtIDNumber As New TextBox
        Dim cboPosition As New DropDownList
        Dim txtAddress As New TextBox
        Dim txtPhone As New TextBox
        Dim cboGender As DropDownList
        Dim chkPersetujuan As CheckBox

        With objectDataTable
            .Columns.Add(New DataColumn("No", GetType(String)))
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("IDNumber", GetType(String)))
            .Columns.Add(New DataColumn("Position", GetType(String)))
            .Columns.Add(New DataColumn("Address", GetType(String)))
            .Columns.Add(New DataColumn("Phone", GetType(String)))
            .Columns.Add(New DataColumn("Share", GetType(String)))
            .Columns.Add(New DataColumn("Gender", GetType(String)))
            .Columns.Add(New DataColumn("Persetujuan", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgList1.Items.Count - 1
            Dim txtShare1 As New ucNumberFormat
            txtShare1 = CType(dtgList1.Items(intLoopGrid).FindControl("txtShare1"), ucNumberFormat)

            lblNo = CType(dtgList1.Items(intLoopGrid).Cells(0).FindControl("lblNo1"), Label)
            txtName = CType(dtgList1.Items(intLoopGrid).Cells(1).FindControl("txtName1"), TextBox)
            cboGender = CType(dtgList1.Items(intLoopGrid).Cells(2).FindControl("cboGender"), DropDownList)
            txtIDNumber = CType(dtgList1.Items(intLoopGrid).Cells(3).FindControl("txtIDNumber1"), TextBox)
            cboPosition = CType(dtgList1.Items(intLoopGrid).Cells(4).FindControl("cboPosition1"), DropDownList)
            txtAddress = CType(dtgList1.Items(intLoopGrid).Cells(5).FindControl("txtAddress1"), TextBox)
            txtPhone = CType(dtgList1.Items(intLoopGrid).Cells(6).FindControl("txtPhone1"), TextBox)
            chkPersetujuan = CType(dtgList1.Items(intLoopGrid).FindControl("chkPersetujuan"), CheckBox)

            oRow = objectDataTable.NewRow()
            oRow("No") = CType(lblNo.Text, String)
            oRow("Name") = CType(txtName.Text, String)
            oRow("Gender") = CType(cboGender.SelectedValue, String)
            oRow("IDNumber") = CType(txtIDNumber.Text, String)
            oRow("Position") = CType(cboPosition.SelectedValue, String)
            oRow("Address") = CType(txtAddress.Text, String)
            oRow("Phone") = CType(txtPhone.Text, String)
            oRow("Share") = CType(txtShare1.Text, String)
            oRow("Persetujuan") = CType(chkPersetujuan.Checked, String)

            objectDataTable.Rows.Add(oRow)
        Next

        oRow = objectDataTable.NewRow()
        oRow("No") = dtgList1.Items.Count + 1

        oRow("Name") = ""
        oRow("IDNumber") = ""
        oRow("Gender") = ""
        oRow("Position") = "Select One"
        oRow("Address") = ""
        oRow("Phone") = ""
        oRow("Share") = ""
        oRow("Persetujuan") = "False"

        objectDataTable.Rows.Add(oRow)
        dtgList1.DataSource = objectDataTable
        dtgList1.DataBind()
        fillCboPosition()

        For intLoopGrid = 0 To dtgList1.Items.Count - 1
            Dim txtShare1 As New ucNumberFormat
            txtShare1 = CType(dtgList1.Items(intLoopGrid).FindControl("txtShare1"), ucNumberFormat)
            With txtShare1
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .TextCssClass = "numberAlign regular_text"
                .RangeValidatorMinimumValue = "0"
                .RangeValidatorMaximumValue = "100"
            End With

            lblNo = CType(dtgList1.Items(intLoopGrid).Cells(0).FindControl("lblNo1"), Label)
            txtName = CType(dtgList1.Items(intLoopGrid).Cells(1).FindControl("txtName1"), TextBox)
            cboGender = CType(dtgList1.Items(intLoopGrid).Cells(2).FindControl("cboGender"), DropDownList)
            txtIDNumber = CType(dtgList1.Items(intLoopGrid).Cells(3).FindControl("txtIDNumber1"), TextBox)
            cboPosition = CType(dtgList1.Items(intLoopGrid).Cells(4).FindControl("cboPosition1"), DropDownList)
            txtAddress = CType(dtgList1.Items(intLoopGrid).Cells(5).FindControl("txtAddress1"), TextBox)
            txtPhone = CType(dtgList1.Items(intLoopGrid).Cells(6).FindControl("txtPhone1"), TextBox)
            chkPersetujuan = CType(dtgList1.Items(intLoopGrid).FindControl("chkPersetujuan"), CheckBox)

            lblNo.Text = objectDataTable.Rows(intLoopGrid).Item("No").ToString
            txtName.Text = objectDataTable.Rows(intLoopGrid).Item("Name").ToString

            If objectDataTable.Rows(intLoopGrid).Item("Gender").ToString <> "" Then
                cboGender.SelectedIndex = cboGender.Items.IndexOf(cboGender.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item("Gender").ToString))
            End If

            txtIDNumber.Text = objectDataTable.Rows(intLoopGrid).Item("IDNumber").ToString

            If objectDataTable.Rows(intLoopGrid).Item("Position").ToString <> "" Then
                cboPosition.SelectedIndex = cboPosition.Items.IndexOf(cboPosition.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item("Position").ToString.Trim))
            End If

            txtAddress.Text = objectDataTable.Rows(intLoopGrid).Item("Address").ToString
            txtPhone.Text = objectDataTable.Rows(intLoopGrid).Item("Phone").ToString
            txtShare1.Text = FormatNumber(IIf(objectDataTable.Rows(intLoopGrid).Item("Share").ToString.Trim = "", "0", objectDataTable.Rows(intLoopGrid).Item("Share").ToString.Trim), 2)
            chkPersetujuan.Checked = CBool(objectDataTable.Rows(intLoopGrid).Item("Persetujuan").ToString)

        Next
    End Sub
    Sub fillCboPosition()
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer
        Dim intLoop As Integer
        Dim cboFamilyRelationship As DropDownList
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblShareHolderPosition"
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        For intLoop = 0 To dtgList1.Items.Count - 1
            cboFamilyRelationship = CType(dtgList1.Items(intLoop).Cells(3).FindControl("cboPosition1"), DropDownList)
            cboFamilyRelationship.DataSource = dtEntity.DefaultView
            cboFamilyRelationship.DataTextField = "Description"
            cboFamilyRelationship.DataValueField = "ID"
            cboFamilyRelationship.DataBind()
            cboFamilyRelationship.Items.Insert(0, "Select One")
            cboFamilyRelationship.Items(0).Value = "Select One"
        Next
    End Sub
    Sub DeleteBaris1(ByVal Index As Integer)
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo As New Label
        Dim txtName As New TextBox
        Dim txtIDNumber As New TextBox
        Dim cboPosition As New DropDownList
        Dim txtAddress As New TextBox
        Dim txtPhone As New TextBox
        Dim cboGender As New DropDownList
        Dim oNewDataTable As New DataTable
        Dim txtShare1 As New ucNumberFormat
        Dim chkPersetujuan As CheckBox

        With oNewDataTable
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("IDNumber", GetType(String)))
            .Columns.Add(New DataColumn("Position", GetType(String)))
            .Columns.Add(New DataColumn("Address", GetType(String)))
            .Columns.Add(New DataColumn("Phone", GetType(String)))
            .Columns.Add(New DataColumn("Share", GetType(String)))
            .Columns.Add(New DataColumn("Gender", GetType(String)))
            .Columns.Add(New DataColumn("Persetujuan", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgList1.Items.Count - 1
            txtName = CType(dtgList1.Items(intLoopGrid).Cells(1).FindControl("txtName1"), TextBox)
            cboGender = CType(dtgList1.Items(intLoopGrid).Cells(2).FindControl("cboGender"), DropDownList)
            txtIDNumber = CType(dtgList1.Items(intLoopGrid).Cells(3).FindControl("txtIDNumber1"), TextBox)
            cboPosition = CType(dtgList1.Items(intLoopGrid).Cells(4).FindControl("cboPosition1"), DropDownList)
            txtAddress = CType(dtgList1.Items(intLoopGrid).Cells(5).FindControl("txtAddress1"), TextBox)
            txtPhone = CType(dtgList1.Items(intLoopGrid).Cells(6).FindControl("txtPhone1"), TextBox)
            txtShare1 = CType(dtgList1.Items(intLoopGrid).FindControl("txtShare1"), ucNumberFormat)
            chkPersetujuan = CType(dtgList1.Items(intLoopGrid).FindControl("chkPersetujuan"), CheckBox)

            If intLoopGrid <> Index Then
                oRow = oNewDataTable.NewRow()
                oRow("Name") = CType(txtName.Text, String)
                oRow("IDNumber") = CType(txtIDNumber.Text, String)
                oRow("Position") = CType(cboPosition.SelectedValue, String)
                oRow("Address") = CType(txtAddress.Text, String)
                oRow("Phone") = CType(txtPhone.Text, String)
                oRow("Share") = CType(txtShare1.Text, String)
                oRow("Gender") = CType(cboGender.SelectedValue, String)
                oRow("Persetujuan") = CType(chkPersetujuan.Checked, String)
                oNewDataTable.Rows.Add(oRow)
            End If
        Next

        dtgList1.DataSource = oNewDataTable
        dtgList1.DataBind()
        fillCboPosition()

        For intLoopGrid = 0 To dtgList1.Items.Count - 1
            txtShare1 = CType(dtgList1.Items(intLoopGrid).FindControl("txtShare1"), ucNumberFormat)
            With txtShare1
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .TextCssClass = "numberAlign regular_text"
                .RangeValidatorMinimumValue = "0"
                .RangeValidatorMaximumValue = "100"
            End With

            lblNo = CType(dtgList1.Items(intLoopGrid).Cells(0).FindControl("lblNo1"), Label)
            txtName = CType(dtgList1.Items(intLoopGrid).Cells(1).FindControl("txtName1"), TextBox)
            cboGender = CType(dtgList1.Items(intLoopGrid).Cells(2).FindControl("cboGender"), DropDownList)
            txtIDNumber = CType(dtgList1.Items(intLoopGrid).Cells(3).FindControl("txtIDNumber1"), TextBox)
            cboPosition = CType(dtgList1.Items(intLoopGrid).Cells(4).FindControl("cboPosition1"), DropDownList)
            txtAddress = CType(dtgList1.Items(intLoopGrid).Cells(5).FindControl("txtAddress1"), TextBox)
            txtPhone = CType(dtgList1.Items(intLoopGrid).Cells(6).FindControl("txtPhone1"), TextBox)
            chkPersetujuan = CType(dtgList1.Items(intLoopGrid).FindControl("chkPersetujuan"), CheckBox)

            lblNo.Text = (intLoopGrid + 1).ToString
            txtName.Text = oNewDataTable.Rows(intLoopGrid).Item("Name").ToString

            If oNewDataTable.Rows(intLoopGrid).Item("Gender").ToString <> "" Then
                cboGender.SelectedIndex = cboGender.Items.IndexOf(cboGender.Items.FindByValue(oNewDataTable.Rows(intLoopGrid).Item("Gender").ToString))
            End If

            txtIDNumber.Text = oNewDataTable.Rows(intLoopGrid).Item("IDNumber").ToString

            If oNewDataTable.Rows(intLoopGrid).Item("Position").ToString <> "" Then
                cboPosition.SelectedIndex = cboPosition.Items.IndexOf(cboPosition.Items.FindByValue(oNewDataTable.Rows(intLoopGrid).Item("Position").ToString.Trim))
            End If

            txtAddress.Text = oNewDataTable.Rows(intLoopGrid).Item("Address").ToString
            txtPhone.Text = oNewDataTable.Rows(intLoopGrid).Item("Phone").ToString
            txtShare1.Text = FormatNumber(IIf(oNewDataTable.Rows(intLoopGrid).Item("Share").ToString.Trim = "", "0", oNewDataTable.Rows(intLoopGrid).Item("Share").ToString.Trim), 2)
            chkPersetujuan.Checked = CBool(oNewDataTable.Rows(intLoopGrid).Item("Persetujuan").ToString)

        Next
    End Sub
#End Region    
#Region "ItemCommand"
    Private Sub dtgList1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList1.ItemCommand
        If e.CommandName = "Delete1" Then
            DeleteBaris1(e.Item.ItemIndex)
        End If
    End Sub    
#End Region    
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ocustomer As New Parameter.Customer        
        Dim intLoopGrid As Integer
        Dim LoopValidPer As Integer
        Dim oData1 As New DataTable    
        Dim oRow As DataRow       
        Dim txtName As String
        Dim txtIDNumber As String
        Dim cboPosition As String
        Dim txtAddress As String
        Dim txtPhone As String
        Dim txtShare As String
        Dim strGender As String
        Dim strPersetujuan As String
        Dim ListPersetujuan As New List(Of String)

        status = True
        Dim no As Integer = 1
        gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"


        With oData1
            .Columns.Add(New DataColumn("No", GetType(String)))
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("IDNumber", GetType(String)))
            .Columns.Add(New DataColumn("Position", GetType(String)))
            .Columns.Add(New DataColumn("Address", GetType(String)))
            .Columns.Add(New DataColumn("Phone", GetType(String)))
            .Columns.Add(New DataColumn("Share", GetType(String)))
            .Columns.Add(New DataColumn("Gender", GetType(String)))
            .Columns.Add(New DataColumn("Persetujuan", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgList1.Items.Count - 1
            txtName = CType(dtgList1.Items(intLoopGrid).Cells(1).FindControl("txtName1"), TextBox).Text
            strGender = CType(dtgList1.Items(intLoopGrid).Cells(2).FindControl("cboGender"), DropDownList).SelectedValue
            txtIDNumber = CType(dtgList1.Items(intLoopGrid).Cells(3).FindControl("txtIDNumber1"), TextBox).Text
            cboPosition = CType(dtgList1.Items(intLoopGrid).Cells(4).FindControl("cboPosition1"), DropDownList).SelectedValue
            txtAddress = CType(dtgList1.Items(intLoopGrid).Cells(5).FindControl("txtAddress1"), TextBox).Text
            txtPhone = CType(dtgList1.Items(intLoopGrid).Cells(6).FindControl("txtPhone1"), TextBox).Text
            txtShare = CType(dtgList1.Items(intLoopGrid).Cells(7).FindControl("txtShare1"), ucNumberFormat).Text
            strPersetujuan = CType(dtgList1.Items(intLoopGrid).FindControl("chkPersetujuan"), CheckBox).Checked.ToString
            If txtName.Trim <> "" And txtIDNumber.Trim <> "" And txtAddress.Trim <> "" And txtPhone.Trim <> "" And txtShare.Trim <> "" And cboPosition.Trim <> "Select One" And strGender <> "" Then
                oRow = oData1.NewRow
                oRow("No") = no
                oRow("Name") = txtName
                oRow("IDNumber") = txtIDNumber
                oRow("Position") = cboPosition
                oRow("Address") = txtAddress
                oRow("Phone") = txtPhone
                oRow("Share") = txtShare
                oRow("Gender") = strGender
                oRow("Persetujuan") = strPersetujuan
                oData1.Rows.Add(oRow)
                no += 1
            ElseIf Not (txtName.Trim = "" And txtIDNumber.Trim = "" And txtAddress.Trim = "" And txtPhone.Trim = "" And txtShare.Trim = "" And cboPosition.Trim = "Select One" And strGender = "Select One") Then
                If txtName.Trim = "" Then
                    CType(dtgList1.Items(intLoopGrid).Cells(1).FindControl("lblVName1"), Label).Visible = True
                    status = False
                Else
                    CType(dtgList1.Items(intLoopGrid).Cells(1).FindControl("lblVName1"), Label).Visible = False
                End If
                If txtIDNumber.Trim = "" Then
                    CType(dtgList1.Items(intLoopGrid).Cells(3).FindControl("lblVIDNumber1"), Label).Visible = True
                    status = False
                Else
                    CType(dtgList1.Items(intLoopGrid).Cells(3).FindControl("lblVIDNumber1"), Label).Visible = False
                End If
                If cboPosition.Trim = "Select One" Then
                    CType(dtgList1.Items(intLoopGrid).Cells(4).FindControl("lblVPosition1"), Label).Visible = True
                    status = False
                Else
                    CType(dtgList1.Items(intLoopGrid).Cells(4).FindControl("lblVPosition1"), Label).Visible = False
                End If
                If txtAddress.Trim = "" Then
                    CType(dtgList1.Items(intLoopGrid).Cells(5).FindControl("lblVAddress1"), Label).Visible = True
                    status = False
                Else
                    CType(dtgList1.Items(intLoopGrid).Cells(5).FindControl("lblVAddress1"), Label).Visible = False
                End If
                If txtPhone.Trim = "" Then
                    CType(dtgList1.Items(intLoopGrid).Cells(6).FindControl("lblVPhone1"), Label).Visible = True
                    status = False
                Else
                    CType(dtgList1.Items(intLoopGrid).Cells(6).FindControl("lblVPhone1"), Label).Visible = False
                End If
                If txtShare.Trim = "" Then
                    CType(dtgList1.Items(intLoopGrid).Cells(7).FindControl("lblVShare1"), Label).Visible = True
                    status = False
                Else
                    CType(dtgList1.Items(intLoopGrid).Cells(7).FindControl("lblVShare1"), Label).Visible = False
                End If
                If strGender.Trim = "" Then
                    CType(dtgList1.Items(intLoopGrid).Cells(2).FindControl("lblVGender"), Label).Visible = True
                    status = False
                Else
                    CType(dtgList1.Items(intLoopGrid).Cells(2).FindControl("lblVGender"), Label).Visible = False
                End If
            End If
        Next

        For LoopValidPer = 0 To dtgList1.Items.Count - 1            
            ListPersetujuan.Add(CType(dtgList1.Items(LoopValidPer).FindControl("chkPersetujuan"), CheckBox).Checked.ToString)
        Next

        If status = False Then
            ShowMessage(lblMessage, "Harus diisi!", True)
            Exit Sub
        Else
            lblMessage.Visible = False
        End If

        If ListPersetujuan.Count > 1 Then
            Dim ListFalse As New List(Of String)
            Dim ListTrue As New List(Of String)

            Me.StrValid = "False"
            ListFalse = ListPersetujuan.FindAll(AddressOf PredicateFunction)
            Me.StrValid = "True"
            ListTrue = ListPersetujuan.FindAll(AddressOf PredicateFunction)

            If ListFalse.Count > 0 And ListTrue.Count = 0 Then
                ShowMessage(lblMessage, "Persetujuan tidak harus dipilih salah satu!", True)
                Exit Sub
            End If

            If ListTrue.Count > 1 Then
                ShowMessage(lblMessage, "Persetujuan tidak boleh lebih dari satu!", True)
                Exit Sub
            End If

        Else
            lblMessage.Visible = False
        End If

        Dim oCompanyCustomerPJ As New Parameter.CompanyCustomerPJ
        With oCompanyCustomerPJ
            .Nama = txtNamaPJ.Text.Trim
            .Jabatan = cboJabatan.SelectedValue
            .NoHP = txtNoHP.Text.Trim
            .Email = txtEmail.Text.Trim
            .NPWP = txtNPWP.Text.Trim
        End With

        Dim CCPJAddress As New Parameter.Address
        With CCPJAddress
            .Address = UCAddressPJ.Address
            .RT = UCAddressPJ.RT
            '.RW = UCAddressPJ.RT
            .RW = UCAddressPJ.RW
            .Kelurahan = UCAddressPJ.Kelurahan
            .Kecamatan = UCAddressPJ.Kecamatan
            .City = UCAddressPJ.City
            .ZipCode = UCAddressPJ.ZipCode
            .AreaPhone1 = UCAddressPJ.AreaPhone1
            .Phone1 = UCAddressPJ.Phone1
        End With

        Try
            Dim Err As String = ""
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID

            Err = m_controller.CompanyManagementSaveEdit(ocustomer, oData1, oCompanyCustomerPJ, CCPJAddress)
            If Err = "" Then
                'Response.Redirect("CompanyCustomerGuarantor.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabPenjamin")
                'Modify by WIra 20171023
                If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                    Response.Redirect("CompanyCustomerGuarantor.aspx?page=Add&id=" + Me.CustomerID + "&pnl=tabPenjamin&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
                Else
                    Response.Redirect("CompanyCustomerGuarantor.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabPenjamin&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
                End If
            Else
                ShowMessage(lblMessage, Err, True)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        
    End Sub
    Public Function PredicateFunction(ByVal custom As String) As Boolean
        Return custom = Me.StrValid
    End Function
#End Region
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click        
        Response.Redirect("Customer.aspx")
    End Sub
 
End Class