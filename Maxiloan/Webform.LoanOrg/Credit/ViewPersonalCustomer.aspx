﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPersonalCustomer.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewPersonalCustomer" %>

<%@ Register TagPrefix="uc1" TagName="UcAgreementList" Src="../../webform.UserController/UcAgreementList.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewPersonalCustomer</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
     <link rel="Stylesheet" type="text/css" href="../../Include/General.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <asp:Panel ID="PnlMain" runat="server" EnableViewState="False" Width="100%">
            <div class="form_title">
                <div class="form_single">
                    <h3>
                        INFORMASI DETAIL PERSONAL CUSTOMER</h3>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DATA PERSONAL</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            ID Customer
                        </label>
                        <asp:Label ID="lblcustomerID" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                       
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nama
                        </label>
                        <asp:Label ID="lblName" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis Identitas
                        </label>
                        <asp:Label ID="lblIDType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nama Ibu Kandung
                        </label>
                        <asp:Label ID="lblNamaIbuKandung" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No. Identitas
                        </label>
                        <asp:Label ID="lblIDNumber" EnableViewState="False" runat="server"></asp:Label>                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Pendidikan
                        </label>
                        <asp:Label ID="lblEducation" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Berlaku Sampai
                        </label>
                        <asp:Label ID="lblBerlakuSampai" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Profesi
                        </label>
                        <asp:Label ID="lblProfession" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis Kelamin
                        </label>
                        <asp:Label ID="lblGender" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Kewarganegaraan
                        </label>
                        <asp:Label ID="lblNationality" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Tempat / Tanggal Lahir
                        </label>
                        <asp:Label ID="lblBirthPlaceDate" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            WNA Negara
                        </label>
                        <asp:Label ID="lblWNACountry" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No HandPhone
                        </label>
                        <asp:Label ID="lblMobilePhone" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Group
                        </label>
                        <asp:Label ID="lblGroup" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            E-Mail
                        </label>
                        <asp:Label ID="lblEmail" EnableViewState="False" runat="server"></asp:Label></font>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Status Rumah
                        </label>
                        <asp:Label ID="lblHomeStatus" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Agama
                        </label>
                        <asp:Label ID="lblReligion" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Tanggal Berakhirnya Sewa
                        </label>
                        <asp:Label ID="lblRentFinishDate" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Status Perkawinan
                        </label>
                        <asp:Label ID="lblMaritalStatus" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Kondisi Rumah
                        </label>
                        <asp:Label ID="lblHomeLocation" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Tanggungan
                        </label>
                        <asp:Label ID="lblDependentNumber" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Kondisi Lingkungan
                        </label>
                        <asp:Label ID="lblKondisiLingkungan" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            NPWP Pribadi
                        </label>
                        <asp:Label ID="lblPersonalNPWP" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">                        
                        <label>
                            Harga Rumah
                        </label>
                        <asp:Label ID="lblHomePrice" EnableViewState="False" runat="server"></asp:Label>                        
                    </div>
                    <div class="form_right">
                        <label>
                            No. Kartu Keluarga
                        </label>
                        <asp:Label ID="lblNoKartuKeluarga" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Tinggal sejak tahun
                        </label>
                        <asp:Label ID="lblStaySinceYear" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    
                </div>
            </div>
            <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h4>
                            ALAMAT SESUAI KTP
                        </h4>
                    </div>
                    <div class="form_right">
                        <h4>
                            ALAMAT TEMPAT TINGGAL
                        </h4>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Address
                        </label>
                        <asp:Label ID="lblLegalAddress" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Alamat
                        </label>
                        <asp:Label ID="lblResidenceAddress" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            RT/RW
                        </label>
                        <asp:Label ID="lblLegalRTRW" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            RT/RW
                        </label>
                        <asp:Label ID="lblResidenceRTRW" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Kelurahan
                        </label>
                        <asp:Label ID="lblLegalKelurahan" EnableViewState="False" runat="server"></asp:Label></font>
                    </div>
                    <div class="form_right">
                        <label>
                            Kelurahan
                        </label>
                        <asp:Label ID="lblResidenceKelurahan" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Kecamatan
                        </label>
                        <asp:Label ID="lblLegalKecamatan" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Kecamatan
                        </label>
                        <asp:Label ID="lblResidenceKecamatan" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Kota
                        </label>
                        <asp:Label ID="lblLegalCity" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Kota
                        </label>
                        <asp:Label ID="lblResidenceCity" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Kode Pos
                        </label>
                        <asp:Label ID="lblLegalZipCode" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Kode Pos
                        </label>
                        <asp:Label ID="lblResidenceZipCode" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Telepon-1
                        </label>
                        <asp:Label ID="lblLegalPhone1" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Telepon-1
                        </label>
                        <asp:Label ID="lblResidencePhone1" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Telepon-2
                        </label>
                        <asp:Label ID="lblLegalPhone2" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Telepon-2
                        </label>
                        <asp:Label ID="lblResidencePhone2" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            No Fax
                        </label>
                        <asp:Label ID="lblLegalFax" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            No fax
                        </label>
                        <asp:Label ID="lblResidenceFax" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="PnlEmpJobData" runat="server">            
            <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h4>
                            DATA PEKERJAAN
                        </h4>
                    </div>
                    <div class="form_right">
                        <h4>
                            DATA KEUANGAN
                        </h4>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Jenis Pekerjaan
                        </label>
                        <asp:Label ID="lblJobType" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            Penghasilan tetap Bulanan
                        </label>
                        <asp:Label ID="lblMonthlyFixedIncome" EnableViewState="False" runat="server"></asp:Label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Nama Perusahaan / Jenis Usaha
                        </label>
                        <asp:Label ID="lblJobCompanyName" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            Penghasilan Tambahan
                        </label>
                        <asp:Label ID="lblMonthlyVariableIncome" EnableViewState="False" runat="server"></asp:Label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Bidang Usaha
                    </label>
                    <asp:Label ID="lblBidangUsaha" EnableViewState="False" runat="server"></asp:Label>	
		        </div>
		        <div class="form_right">		
                    <label>
                            Biaya Hidup
                    </label>
                    <asp:Label ID="lblBiayaHidup" EnableViewState="False" runat="server"></asp:Label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Keterangan Usaha/ Jabatan
                    </label>
                    <asp:Label ID="lblKetUsaha" EnableViewState="False" runat="server"></asp:Label> 
                    <asp:Label ID="lblJobTitle" EnableViewState="False" runat="server" Visible="false"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>
                            Deposito
                    </label>
                    <asp:Label ID="lblDepositoKeuangan" EnableViewState="False" runat="server"></asp:Label>		
		        </div>
            </div>
            <div class="form_box_hide">
                <div class="form_left">
                    <label>&nbsp </label>	
                  <%--  <label>
                            Keterangan Usaha
                    </label>
                    <asp:Label ID="lblKetUsaha" EnableViewState="False" runat="server"></asp:Label>--%>
		        </div>
		        <div class="form_right">	
                    <%--<label>
                            Deposito
                    </label>
                    <asp:Label ID="lblDepositoKeuangan" EnableViewState="False" runat="server"></asp:Label>		--%>
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Kategori Perusahaan
                    </label>
                    <asp:Label ID="lblKategoriPerusahaan" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            Jenis Jaminan Tambahan
                    </label>
                    <asp:Label ID="lblJenisJaminanTambahan" EnableViewState="False" runat="server"></asp:Label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Kondisi Kantor
                    </label>
                    <asp:Label ID="lblKondisiKantor" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>
                            Nilai Jaminan Tambahan 
                    </label>
                    <asp:Label ID="lblNilaiJaminanTambahan" EnableViewState="False" runat="server"></asp:Label>		
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Alamat
                        </label>
                        <asp:Label ID="lblJobAddress" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            &nbsp
                        </label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Kota
                        </label>
                        <asp:Label ID="lblJobCity" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            &nbsp
                        </label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Telepon-1
                        </label>
                        <asp:Label ID="lblJobPhone1" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>
                            &nbsp
                        </label>		
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            No. Fax
                        </label>
                        <asp:Label ID="lblJobFax" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            &nbsp
                        </label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Mulai Kerja Sejak
                        </label>
                        <asp:Label ID="lblJobEmploymentSince" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            &nbsp
                        </label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Nama Usaha Lainnya
                        </label>
                        <asp:Label ID="lblEmpOtherBusinessName" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            &nbsp
                        </label>	
		        </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">              
                    <h4>
                        DATA PASANGAN
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Nama Pasangan
                    </label>
                    <asp:Label ID="lblNamaPasangan" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">		
                    <label>
                            Penghasilan Tetap Pasangan
                    </label>
                    <asp:Label ID="lblPenghasilanTetapPasangan" EnableViewState="False" runat="server"></asp:Label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Nama Perusahaan / Jenis Usaha
                    </label>
                    <asp:Label ID="lblNamaPerusahaanPasangan" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    	<label>
                            Penghasilan Tambahan Pasangan
                    </label>
                    <asp:Label ID="lblPenghasilanTambahanPasangan" EnableViewState="False" runat="server"></asp:Label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Bidang Usaha
                    </label>
                    <asp:Label ID="lblBidangUsahaPasangan" EnableViewState="False" runat="server"></asp:Label>	
		        </div>
		        <div class="form_right">	
                    <label>
                            &nbsp
                        </label>	
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Pekerjaan
                    </label>
                    <asp:Label ID="lblPekerjaanPasangan" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>
                            &nbsp
                        </label>			
		        </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                            Keterangan Usaha
                    </label>
                    <asp:Label ID="lblKeteranganUsahaPasangan" EnableViewState="False" runat="server"></asp:Label>
		        </div>
		        <div class="form_right">	
                    <label>
                            &nbsp
                        </label>			
		        </div>
            </div>            


           <%-- <div class="form_box">
                <div>
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        <label>
                            Posisi
                        </label>
                        <asp:Label ID="lblJobPosition" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis Industri
                        </label>
                        <asp:Label ID="lblJobIndustryType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        <label>
                            RT/RW
                        </label>
                        <asp:Label ID="lblJobRTRW" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Kelurahan
                        </label>
                        <asp:Label ID="lblJobKelurahan" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Kecamatan
                        </label>
                        <asp:Label ID="lblJobKecamatan" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        <label>
                            Kode Pos
                        </label>
                        <asp:Label ID="lblJobZipCode" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        <label>
                            Telepon-2
                        </label>
                        <asp:Label ID="lblJobPhone2" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        <label>
                            Jabatan
                        </label>
                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        
                    </div>
                    <div class="form_right">
                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Jenis Bisnis
                        </label>
                        <asp:Label ID="lblEmpOtherBusinessType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis Industri
                        </label>
                        <asp:Label ID="lblEmpOtherBusinessIndustryType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Jabatan
                        </label>
                        <asp:Label ID="lblEmpOtherBusinessJobTitle" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Bisnis Sejak tahun
                        </label>
                        <asp:Label ID="lblEmpOtherBusinessSinceYear" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>--%>
            <%--<div class="form_box_title">
                <div class="form_single">
                    <h4>
                        OMSET BULANAN
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgEmpOmset" EnableViewState="False" runat="server" CellPadding="0"
                        CssClass="grid_general" AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None"
                        BorderWidth="1px" BackColor="White" Width="100%">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="OmsetYEAR" SortExpression="OmsetYEAR" HeaderText="TAHUN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OmsetMONTH" SortExpression="OmsetMONTH" HeaderText="BULAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OMSET" SortExpression="OMSET" HeaderText="OMSET"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>--%>
        </asp:Panel>
        
        <asp:Panel ID="PnlEntJobData" runat="server" EnableViewState="False" Width="100%">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DATA PROFESIONAL
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Penghasilan Bulanan Tetap
                        </label>
                        <asp:Label ID="lblProfFixIncome" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Penghasilan Tdk Tetap Bulanan
                        </label>
                        <asp:Label ID="lblProfVarIncome" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DATA ENTERPRENUR
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nama Bisnis
                        </label>
                        <asp:Label ID="lblEntBusinessName" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis Bisnis
                        </label>
                        <asp:Label ID="lblEntBusinessType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Jenis Industri
                        </label>
                        <asp:Label ID="lblEntBusinessIndustryType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jabatan
                        </label>
                        <asp:Label ID="lblEntJobTitle" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Bisnis Sejak tahun
                        </label>
                        <asp:Label ID="lblEntSinceYear" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            &nbsp
                        </label>
                    </div>
                </div>
            </div>
            <%--<div class="form_box_title">
                <div class="form_single">
                    <label>
                        Omset Bulanan
                    </label>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgEntOmset" EnableViewState="False" runat="server" CellPadding="0"
                        CssClass="grid_general" AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None"
                        BorderWidth="1px" BackColor="White" Width="100%">
                        <ItemStyle CssClass="item_grid"></ItemStyle>
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="OMSETYEAR" SortExpression="YEAR" HeaderText="TAHUN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OMSETMONTH" SortExpression="MONTH" HeaderText="BULAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="OMSET" SortExpression="OMSET" HeaderText="OMSET"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>--%>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DATA FINANCIAL</h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Penghasilan Tetap
                        </label>
                        <asp:Label ID="lblFinFixedIncome" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Penghasilan Tdk tetap
                        </label>
                        <asp:Label ID="lblFinVariableIncome" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Penghasilan Pasangan
                        </label>
                        <asp:Label ID="lblSpouseIncome" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Saldo Rata-Rata Rekening
                        </label>
                        <asp:Label ID="lblAvgAvgBalance" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Jenis rekening Bank
                        </label>
                        <asp:Label ID="lblBankAccountType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Rata-rata Transaksi Debit
                        </label>
                        <asp:Label ID="lblAvgDebitTrans" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Rata-rata Transaksi Kredit
                        </label>
                        <asp:Label ID="lblAvgCreditTrans" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Saldo rata-rata
                        </label>
                        <asp:Label ID="lblAvgBalance" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Deposito
                        </label>
                        <asp:Label ID="lblDeposito" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Biaya Hidup
                        </label>
                        <asp:Label ID="lblLivingCostAmount" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Angsuran Pinjaman Lainnya
                        </label>
                        <asp:Label ID="lblOtherLoanInstallment" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jenis jaminan Tambahan
                        </label>
                        <asp:Label ID="lblAdditionalCollateralType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nilai Jaminan Tambahan
                        </label>
                        <asp:Label ID="lblAdditionalCollateralAmount" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Credit Card
                        </label>
                        <asp:Label ID="lblCreditCard" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Jenis Credit Card
                        </label>
                        <asp:Label ID="lblCreditCardType" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Jumlah Credit Card
                        </label>
                        <asp:Label ID="lblNumberCreditCard" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        REKENING BANK
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Nama Bank
                        </label>
                        <asp:Label ID="lblBankName" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Cabang Bank
                        </label>
                        <asp:Label ID="lblBankBranch" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            No Rekening
                        </label>
                        <asp:Label ID="lblAccountNo" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Nama Rekening
                        </label>
                        <asp:Label ID="lblAccountName" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DATA LAINNYA
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Referensi
                        </label>
                        <asp:Label ID="lblReference" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Pengajuan Pinjaman Mobil Sebelumnya?
                        </label>
                        <asp:Label ID="lblApplyBefore" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Catatan
                    </label>
                    <asp:Label ID="lblNotes" EnableViewState="False" runat="server"></asp:Label>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="PnlProfJobData" runat="server" EnableViewState="False" Width="100%">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DATA PROFESSIONAL
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Penghasilan tetap Bulanan
                        </label>
                        <asp:Label ID="Label30" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Penghasilan Tdk Tetap Bulanan
                        </label>
                        <asp:Label ID="Label29" EnableViewState="False" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="PnlOther" runat="server">
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DATA KELUARGA
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <asp:DataGrid ID="dtgFamily" EnableViewState="False" runat="server" CellPadding="0"
                        CssClass="grid_general" AutoGenerateColumns="False" BorderColor="#CCCCCC" BorderStyle="None"
                        BorderWidth="1px" BackColor="White" Width="100%">
                        <ItemStyle CssClass="item_grid"></ItemStyle>
                        <HeaderStyle CssClass="th"></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="FamilySeqNo" SortExpression="FamilySeqNo" HeaderText="NO">
                                <HeaderStyle HorizontalAlign="center" Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="NAMA">
                                <HeaderStyle HorizontalAlign="left" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IDNUMBER" SortExpression="IDNUMBER" HeaderText="NO IDENTITAS">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BIRTHDATE" SortExpression="BIRTHDATE" HeaderText="TGL LAHIR">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RELATIONSHIP" SortExpression="RELATIONSHIP" HeaderText="HUBUNGAN">
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
            <div class="form_box_title">
                <div class="form_single">
                    <h4>
                        DAFTAR KONTRAK
                    </h4>
                </div>
            </div>
            <div class="form_box_uc">
                <uc1:ucagreementlist id="oAgreementList" enableviewstate="False" runat="server"></uc1:ucagreementlist>
            </div>
            <div class="form_box_title">
                <div>
                    <div class="form_left">
                        <asp:HyperLink ID="HyCustomerExposure" EnableViewState="False" runat="server">Customer Exposure</asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <asp:HyperLink ID="HyAnalisa" EnableViewState="False" runat="server">Cetak Analisa R/O</asp:HyperLink>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnPrint" runat="server" EnableViewState="False" Text="Print" CssClass="small button blue" />
                <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
                    Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
