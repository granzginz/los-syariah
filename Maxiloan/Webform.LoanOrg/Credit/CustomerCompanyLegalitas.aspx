﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerCompanyLegalitas.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerCompanyLegalitas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register Src="../../webform.UserController/ucKategoriPerusahaan.ascx" TagName="ucKategoriPerusahaan"
    TagPrefix="uc3" %>
<%@ Register Src="../../webform.UserController/ucKondisiKantor.ascx" TagName="ucKondisiKantor"
    TagPrefix="uc4" %>
<%@ Register Src="../../webform.UserController/ucLookupGroupCust.ascx" TagName="ucLookupGroupCust"
    TagPrefix="uc5" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="CustomerTabsCompany.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Company</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
        <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Page_Validators = new Array();

        function BusinessPlaceStatus() {
            var myID = 'txtTanggalSewa';
            var objDate = eval('document.forms[0].' + myID);
            if (document.forms[0].cboBusPlaceStatus.value == 'KR') {
                objDate.disabled = false;
                ValidatorEnable(document.getElementById("rfvRentFinish"), true);
            }
            else {
                objDate.disabled = true;
                objDate.value = '';
                ValidatorEnable(document.getElementById("rfvRentFinish"), false);
            }


        }			
    </script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
     <uct:tabs id='cTabs' runat='server'></uct:tabs> 
    <%--<div class="tab_container">
        <div id="tabDataKonsumen" runat="server">
            <asp:HyperLink ID="hyDataKonsumen" runat="server" Text="DATA KONSUMEN"></asp:HyperLink>
        </div>
        <div id="tabLegalitas" runat="server">
            <asp:HyperLink ID="hyLegalitas" runat="server" Text="LEGALITAS"></asp:HyperLink>
        </div>
        <div id="tabManagement" runat="server">
            <asp:HyperLink ID="hyManagement" runat="server" Text="MANAGEMENT"></asp:HyperLink>
        </div> 
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>              
    </div>--%>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        CORPORATE CUSTOMER
                    </h4>
                </div>
            </div>            
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DOKUMEN LEGAL
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgList2" runat="server" Width="100%" CellSpacing="1" CellPadding="3"
                            BorderWidth="0" AutoGenerateColumns="False" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo2" runat="server" Width="10"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JENIS">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cboType2" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblVType2" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                        <a href="Customer_002.aspx"></a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NO DOKUMEN">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtDocNo2" runat="server" MaxLength="50" Width="95%"></asp:TextBox>
                                        <asp:Label ID="lblVDocNo2" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TANGGAL TERBIT">
                                    <ItemTemplate>
                                        <uc1:ucdatece id="txtTglDokumen" runat="server"></uc1:ucdatece>
                                        <asp:Label ID="lblVDate2" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TANGGAL MASA BERLAKU">
                                    <ItemTemplate>
                                        <uc1:ucdatece id="txtTglJT" runat="server"></uc1:ucdatece>
                                        <asp:Label ID="lblVDate3" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="CATATAN">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtNotes2" runat="server" Width="85%"></asp:TextBox>
                                        <asp:Label ID="lblVNotes2" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete2" CausesValidation="false" Width="20" runat="server"
                                            ImageUrl="../../Images/icondelete.gif" CommandName="Delete2"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAdd2" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False">
                </asp:Button>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
