﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerPersonalKeuangan.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerPersonalKeuangan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Src="CustomerTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Personal</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Page_Validators = new Array();

        
        function toCommas(yourNumber) { 
            var n = yourNumber.toString().split("."); 
            n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","); 
            return n.join(".");
        }

        function SisaPenghasilan() {

            var inc = parseFloat($('#txtJDMonthlyFixedIncome_txtNumber').val().replace(/,/g, ""));
            var inc1 = parseFloat($('#txtJDMonthlyVariableIncome_txtNumber').val().replace(/,/g, ""));
            var inc2 = parseFloat($('#txtPenghasilanTetapPasangan_txtNumber').val().replace(/,/g, ""));
            var inc3 = parseFloat($('#txtPenghasilanTambahanPasangan_txtNumber').val().replace(/,/g, ""));
            var inc4 = parseFloat($('#txtPenghasilanTetapPenjamin_txtNumber').val().replace(/,/g, ""));
            var inc5 = parseFloat($('#txtPenghasilanTambahanPenjamin_txtNumber').val().replace(/,/g, "")); 
            
            var cost = parseFloat($('#txtFDLivingCost_txtNumber').val().replace(/,/g, ""));
            var cost1 = parseFloat($('#txtAngsuranLain_txtNumber').val().replace(/,/g, ""));
            var c =  cost   +  cost1;
            var i = inc + inc1 + inc2 + inc3 + inc4 + inc5;

            
            var tot = parseFloat(i) -parseFloat( c);

            $('#lblSisaPenghasilan').text( toCommas(tot));
        };

        $(document).ready(function () {
//            $('#txtJDMonthlyFixedIncome_txtNumber').change(function (e) { SisaPenghasilan(); });
//            $('#txtJDMonthlyVariableIncome_txtNumber').change(function (e) { SisaPenghasilan(); });
//            $('#txtPenghasilanTetapPasangan_txtNumber').change(function (e) { SisaPenghasilan(); });
//            $('#txtPenghasilanTambahanPasangan_txtNumber').change(function (e) { SisaPenghasilan(); });
//            $('#txtPenghasilanTetapPenjamin_txtNumber').change(function (e) { SisaPenghasilan(); });
//            $('#txtPenghasilanTambahanPenjamin_txtNumber').change(function (e) { SisaPenghasilan(); });

//            $('#txtFDLivingCost_txtNumber').change(function (e) { SisaPenghasilan(); });
//            $('#txtAngsuranLain_txtNumber').change(function (e) { SisaPenghasilan(); });
//            SisaPenghasilan();

            $('#UcBankAccount1_btnClearBankAccountChache').css('display', 'none');
        });
          

    </script>    
</head>
<body>    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
     <uct:tabs id='cTabs' runat='server' />
    <%--<div class="tab_container">
        <div id="tabIdentitas" runat="server">
            <asp:HyperLink ID="hyIdentitas" runat="server" Text="IDENTITAS"></asp:HyperLink>
        </div>
        <div id="tabPekerjaan" runat="server">
            <asp:HyperLink ID="hyPekerjaan" runat="server" Text="PEKERJAAN"></asp:HyperLink>
        </div>
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>
        <div id="tabPasangan" runat="server">
            <asp:HyperLink ID="hyPasangan" runat="server" Text="PASANGAN"></asp:HyperLink>
        </div>
        <div id="tabEmergency" runat="server">
            <asp:HyperLink ID="hyEmergency" runat="server" Text="EMERGENCY CONTACT"></asp:HyperLink>
        </div>
        <div id="tabKeluarga" runat="server">
            <asp:HyperLink ID="hyKeluarga" runat="server" Text="KELUARGA"></asp:HyperLink>
        </div>
    </div>--%>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"  onclick="hideMessage();"></asp:Label>
        <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                DATA KEUANGAN
            </h4>
        </div>
    </div>          
        <asp:Panel runat="server" ID="pnlKeuangan">        
        
        <div class="form_box">
            <div class="form_single">
                <label>
                    Penghasilan Tetap Bulanan</label>
                <uc1:ucnumberformat id="txtJDMonthlyFixedIncome" runat="server" maxlength="15" onClientChange='SisaPenghasilan()'></uc1:ucnumberformat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Penghasilan Tambahan Bulanan</label>
                <uc1:ucnumberformat id="txtJDMonthlyVariableIncome" runat="server" maxlength="15" onClientChange='SisaPenghasilan()'></uc1:ucnumberformat>
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label>Penghasilan Tetap Pasangan</label>
                <uc1:ucnumberformat id="txtPenghasilanTetapPasangan" runat="server" maxlength="15" onClientChange='SisaPenghasilan()'></uc1:ucnumberformat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label> Penghasilan Tambahan Pasangan</label>
                <uc1:ucnumberformat id="txtPenghasilanTambahanPasangan" runat="server" maxlength="15" onClientChange='SisaPenghasilan()'></uc1:ucnumberformat>
            </div>
        </div>
         <div class="form_box">
            <div class="form_single">
                <label> Penghasilan Tetap Penjamin</label>
               <uc1:ucnumberformat id="txtPenghasilanTetapPenjamin" runat="server" maxlength="15" onClientChange='SisaPenghasilan()'></uc1:ucnumberformat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>  Penghasilan Tambahan Penjamin</label>
                <uc1:ucnumberformat id="txtPenghasilanTambahanPenjamin" runat="server" maxlength="15" onClientChange='SisaPenghasilan()'></uc1:ucnumberformat>
            </div>
        </div>
      
        <div class="form_box">
            <div class="form_single">
                <label>
                    Biaya Bulanan</label>
                <uc1:ucnumberformat id="txtFDLivingCost" runat="server" maxlength="15" onClientChange='SisaPenghasilan()'></uc1:ucnumberformat>
            </div>
        </div>
          <div class="form_box">
            <div class="form_single">
                <label> Angsuran Lainnya</label>
                <uc1:ucnumberformat id="txtAngsuranLain" runat="server" maxlength="15" onClientChange='SisaPenghasilan()'></uc1:ucnumberformat>
            </div>
        </div>
          <div class="form_box">
            <div class="form_single">
                <label> Sisa Penghasilan</label>
                 <asp:label id='lblSisaPenghasilan' runat='server' />
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label> Deposito</label>
                <uc1:ucnumberformat id="txtFDDeposito" runat="server" maxlength="15" ></uc1:ucnumberformat>
            </div>
        </div>

         <div class="form_box">
            <div class="form_single">
                <label> Rata-Rata Penghasilan Tahunan</label>
                <asp:DropDownList ID="cboRataPenghasilanPerTahun" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="< 60 juta">< 60 juta</asp:ListItem>
                    <asp:ListItem Value="60 - 120 juta">60 - 120 juta</asp:ListItem>
                    <asp:ListItem Value="> 120 juta">> 120 juta</asp:ListItem>
                </asp:DropDownList> 
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label> Sumber Pembiayaan</label>
                <%--<asp:TextBox ID="txtSumberPembiayaan" runat="server"  MaxLength="50" style="width : 18%"></asp:TextBox>--%> 
                <asp:DropDownList ID="cboKodeSumberPenghasilan" runat="server">
                    <asp:ListItem Value="1">1 - Gaji</asp:ListItem>
                    <asp:ListItem Value="2">2 - Usaha</asp:ListItem>
                    <asp:ListItem Value="3">3 - Lainnya</asp:ListItem>
                </asp:DropDownList>
             </div>
        </div> 

        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    REKENING BANK
                </h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucbankaccount id="UcBankAccount1" runat="server"></uc1:ucbankaccount>
        </div>
    </asp:Panel>                   
        <div class="form_button">
        <asp:Button ID="btnPSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnPCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div> 
		</ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnPSave" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
