﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region

Public Class ViewCompanyCustomer
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oAgreementList As UcAgreementList

#Region "Property"
    Public Property Style() As String
        Get
            Return CType(ViewState("Style"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property

    Public Property CustomerName() As String
        Get
            Return CType(ViewState("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Public Property CustomerId() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
#End Region

    Dim c_customer As New CustomerController
    'Dim customerID As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.Style = Request.QueryString("style")

            If Request.QueryString("CustomerID").Trim <> "" Then Me.CustomerId = Request.QueryString("CustomerID")

            Dim oCustomer As New Parameter.Customer

            Dim dt As New DataTable

            With oCustomer
                .strConnection = getConnectionString
                .CustomerID = Me.CustomerId
            End With

            oCustomer = c_customer.GetViewCompanyCustomer(oCustomer)
            dt = oCustomer.listdata

            With oAgreementList
                .CustomerID = Me.CustomerId
                .Style = Me.Style
                .BindAgreement()
            End With


			If dt.Rows.Count > 0 Then
				lblcustomerID.Text = CStr(dt.Rows(0).Item("CustomerID"))
				lblName.Text = CStr(dt.Rows(0).Item("CustomerName"))
				Me.CustomerName = CStr(dt.Rows(0).Item("CustomerName"))
				lblCompanyType.Text = CStr(dt.Rows(0).Item("CompanyType"))
				lblNPWP.Text = CStr(dt.Rows(0).Item("NPWP"))

				lblNumEmployee.Text = CStr(dt.Rows(0).Item("NumberOfEmployees"))
				lblYearEstablished.Text = CStr(dt.Rows(0).Item("YearOfEstablished"))
				lblIndustryType.Text = CStr(dt.Rows(0).Item("IndustryType"))
				lblGroup.Text = CStr(dt.Rows(0).Item("GroupName"))
				lblCompanyAddress.Text = CStr(dt.Rows(0).Item("CompanyAddress"))
				lblCompanyRTRW.Text = CStr(dt.Rows(0).Item("RT")) & "/" & CStr(dt.Rows(0).Item("RW"))
				lblCompanyKelurahan.Text = CStr(dt.Rows(0).Item("Kelurahan"))
				lblCompanyKecamatan.Text = CStr(dt.Rows(0).Item("Kecamatan"))
				lblCompanyCity.Text = CStr(dt.Rows(0).Item("City"))
				lblCompanyZipCode.Text = CStr(dt.Rows(0).Item("CompanyZipCode"))
				lblCompanyPhone1.Text = CStr(dt.Rows(0).Item("CompanyAreaPhone1")) & " - " & CStr(dt.Rows(0).Item("CompanyPhone1"))
				lblCompanyPhone2.Text = CStr(dt.Rows(0).Item("CompanyAreaPhone2")) & " - " & CStr(dt.Rows(0).Item("CompanyPhone2"))
				lblCompanyFax.Text = CStr(dt.Rows(0).Item("CompanyAreaFax")) & " - " & CStr(dt.Rows(0).Item("CompanyFax"))

				'Finansial Data
				lblCurrentRatio.Text = FormatNumber(CStr(dt.Rows(0).Item("CurrentRatio")), 2)
				lblROI.Text = FormatNumber(CStr(dt.Rows(0).Item("ROI")), 2)
				lblDER.Text = FormatNumber(CStr(dt.Rows(0).Item("DER")), 2)
				lblBankAccountType.Text = CStr(dt.Rows(0).Item("BankAccountType"))
				lblAvgDebitTrans.Text = FormatNumber(CStr(dt.Rows(0).Item("AverageDebitTrans")), 2)
				lblAvgCreditTrans.Text = FormatNumber(CStr(dt.Rows(0).Item("AverageCreditTrans")), 2)
				lblAvgBalance.Text = FormatNumber(CStr(dt.Rows(0).Item("AverageBalance")), 2)
				lblDeposito.Text = FormatNumber(CStr(dt.Rows(0).Item("Deposito")), 2)
				lblAdditionalCollateralType.Text = CStr(dt.Rows(0).Item("AdditionalCollateralType"))
				lblAdditionalCollateralAmount.Text = CStr(dt.Rows(0).Item("AdditionalCollateralAmount"))
				lblBusinessPlaceStatus.Text = CStr(dt.Rows(0).Item("CompanyStatus"))
				lblBusinessPlaceSinceYear.Text = CStr(dt.Rows(0).Item("CompanySinceYear"))
				lblRentFinishDate.Text = CStr(dt.Rows(0).Item("RentFinishDate"))

				'WareHouse
				lblStatus.Text = CStr(dt.Rows(0).Item("WareHouseStatus"))
				lblAddress.Text = CStr(dt.Rows(0).Item("WareHouseAddress"))
				lblRTRW.Text = CStr(dt.Rows(0).Item("WareHouseRT")) & "/" & CStr(dt.Rows(0).Item("WareHouseRW"))
				lblKelurahan.Text = CStr(dt.Rows(0).Item("WareHouseKelurahan"))
				LblKecamatan.Text = CStr(dt.Rows(0).Item("WareHouseKecamatan"))
				lblCity.Text = CStr(dt.Rows(0).Item("WareHouseCity"))
				lblZipCode.Text = CStr(dt.Rows(0).Item("WareHouseZipCode"))
				lblPhone1.Text = CStr(dt.Rows(0).Item("WareHouseAreaPhone1")) & " - " & CStr(dt.Rows(0).Item("WareHousePhone1"))
				lblPhone2.Text = CStr(dt.Rows(0).Item("WareHouseAreaPhone2")) & " - " & CStr(dt.Rows(0).Item("WareHousePhone2"))
				lblFax.Text = CStr(dt.Rows(0).Item("WareHouseAreaFax")) & " - " & CStr(dt.Rows(0).Item("WareHouseFax"))

				'BankAccount
				lblBankAccount.Text = CStr(dt.Rows(0).Item("BankAccount"))
				lblBankBranch.Text = CStr(dt.Rows(0).Item("BankBranch"))
				lblAccountNo.Text = CStr(dt.Rows(0).Item("AccountNo"))
				lblAccountName.Text = CStr(dt.Rows(0).Item("AccountName"))
				lblReference.Text = CStr(dt.Rows(0).Item("Reference"))
				lblApplyCarBefore.Text = CStr(dt.Rows(0).Item("isApplyCarLoanBefore"))
				lblNotes.Text = CStr(dt.Rows(0).Item("Notes"))
				'------------------------------
				'Data-data di grid
				'------------------------------
				Dim oCustomerGrid As New Parameter.Customer
				Dim ocompcustmanagement As New Parameter.Customer
				Dim ocompcustDocument As New Parameter.Customer

				dt = New DataTable

				With oCustomerGrid
					.strConnection = GetConnectionString()
					.CustomerID = Me.CustomerId
				End With

				ocompcustmanagement = c_customer.GetViewCompanyCustomerManagement(oCustomerGrid)

				DtgManagement.DataSource = ocompcustmanagement.listdata
				DtgManagement.DataBind()


				dt = New DataTable
				ocompcustDocument = c_customer.GetViewCompanyCustomerDocument(oCustomerGrid)
				dtgDocument.DataSource = ocompcustDocument.listdata
				dtgDocument.DataBind()
			End If
			Get_Link()
            Dim imb As New Button
            imb = CType(Me.FindControl("imbClose"), Button)
            imb.Attributes.Add("onClick", "return Close()")
        End If
    End Sub
    Private Sub Get_Link()
        HyCustomerExposure.NavigateUrl = "viewCustomerExposure.aspx?CustomerId=" & Me.CustomerId & "&CustomerName=" & Me.CustomerName & "&Style=" & Me.Style
    End Sub

    'Private Sub ImbPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ImbPrint.Click
    '    Dim cookieNew As New HttpCookie("CompanyCustomerRpt")
    '    cookieNew.Values.Add("CustomerID", Me.CustomerId)
    '    cookieNew.Values.Add("LoginID", Me.Loginid)
    '    Response.AppendCookie(cookieNew)
    '    Response.Redirect("RptCompanyCustomerViewer.aspx")
    'End Sub

End Class