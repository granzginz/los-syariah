﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerGroupPIC.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerGroupPIC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAddress" Src="../../Webform.UserController/UcCompanyAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DAFTAR PIC FLEET</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                DAFTAR PIC FLEET (GROUP CUSTOMER)
            </h3>
        </div>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" DataKeyField="ID" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid" />
                            <HeaderStyle CssClass="th" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                            CommandName="Edit"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                            CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="NamaPIC" SortExpression="NamaPIC" HeaderText="NAMA PIC">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Alamat" SortExpression="Alamat" HeaderText="ALAMAT">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Phone1" SortExpression="Phone1" HeaderText="NO TELEPON">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="NoHP" SortExpression="NoHP" HeaderText="NO HANDPHONE">
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server" cssClass="small_text">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False" />
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah"   
                                Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" Type="integer"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False" />
                    <asp:Button ID="btnPrint" runat="server" Enabled="false" Text="Print" CssClass="small button gray" />
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI PIC
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NamaPIC">NAMA PIC</asp:ListItem>
                            <asp:ListItem Value="Alamat">ALAMAT</asp:ListItem>
                            <asp:ListItem Value="Phone1">NO TELEPON</asp:ListItem>
                            <asp:ListItem Value="NoHP">NO HANDPHONE</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue" />
                    <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
                    <asp:Button ID="btnBackMenu" runat="server" CausesValidation="False" Text="Back"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            NAMA PIC
                        </label>
                        <asp:Label ID="lblNamaPIC" runat="server"></asp:Label>
                        <asp:TextBox ID="txtNamaPIC" runat="server" CssClass ="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNamaPIC" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box_uc">                    
                        <uc1:uccompanyaddress id="UcCompanyAddress" runat="server"></uc1:uccompanyaddress>                    
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            NO.HP
                        </label>
                        <asp:Label ID="lblNoHP" runat="server"></asp:Label>
                        <asp:TextBox ID="txtNoHP" runat="server"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="expNoHP" runat="server" Display="Dynamic" ValidationExpression="[0-9]{0,15}"
                            ErrorMessage="Harap isi Nomor HP dengan Angka" ControlToValidate="txtNoHP" CssClass="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="expNoHP1" runat="server" ControlToValidate="txtNoHP"
                            ErrorMessage="Harap isi Nomor HP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSave" runat="server" CausesValidation="true" text="Save" CssClass="small button blue" />                    
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="true" text="Cancel" CssClass="small button gray" />                    
                    <asp:Button ID="btnBack" runat="server" CausesValidation="true" text="Back" CssClass="small button gray"/>                    
                    <asp:Button ID="btnClose" runat="server" CausesValidation="False" text="Close" CssClass="small button gray" />                    
                </div>
            </asp:Panel>
            <input id="hdnCustomerGroupID" type="hidden" name="hdnCustomerGroupID" runat="server" />
            <input id="hdnIDEdit" type="hidden" name="hdnIDEdit" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
