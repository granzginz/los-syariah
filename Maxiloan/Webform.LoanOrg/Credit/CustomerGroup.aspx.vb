﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Webform.UserController
#End Region

Public Class CustomerGroup
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents txtPlafond As ucNumberFormat


    Private m_controller As New CustomerGroupController    
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            If IsSingleBranch() And Me.IsHoBranch = False Then
                Me.FormID = "CUSTOMERGROUP"
                If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                    txtPage.Text = "1"
                    Me.Sort = "CustomerGroupID ASC"
                    If Request("cond") <> "" Then
                        Me.CmdWhere = Request("cond")
                    Else
                        Me.CmdWhere = "ALL"

                    End If

                    BindGridEntity(Me.CmdWhere)

                    If Request("cmd") = "dtl" Then
                        If CheckFeature(Me.Loginid, Me.FormID, "View", Me.AppId) Then
                            If SessionInvalid() Then
                                Exit Sub
                            End If
                        End If
                        BindDetail(Request("id"), Request("desc"))
                    End If
                    btnClose.Attributes.Add("OnClick", "return fClose()")
                End If
            Else
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            End If
            
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomerGroup As New Parameter.CustomerGroup
        InitialDefaultPanel()
        oCustomerGroup.strConnection = GetConnectionString()
        oCustomerGroup.WhereCond = cmdWhere
        oCustomerGroup.CurrentPage = currentPage
        oCustomerGroup.PageSize = pageSize
        oCustomerGroup.SortBy = Me.Sort
        oCustomerGroup = m_controller.GetCustomerGroup(oCustomerGroup)

        If Not oCustomerGroup Is Nothing Then
            dtEntity = oCustomerGroup.Listdata
            recordCount = oCustomerGroup.Totalrecords
        Else
            recordCount = 0
        End If
       
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Sub BindDetail(ByVal ID As String, ByVal Desc As String)
        Dim oCustomerGroup As New Parameter.CustomerGroup
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If Request("cmd2") = "close" Then
            btnBack.Visible = False
            btnClose.Visible = True
        Else
            btnBack.Visible = True
            btnClose.Visible = False
        End If
        btnCancel.Visible = False
        btnSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        oCustomerGroup.CustomerGroupID = ID
        oCustomerGroup.strConnection = GetConnectionString()
        oCustomerGroup = m_controller.GetCustomerGroupList(oCustomerGroup)

    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oCustomerGroup As New Parameter.CustomerGroup
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", Me.AppID) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            btnBack.Visible = False
            btnCancel.Visible = True
            btnSave.Visible = True
            btnClose.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            oCustomerGroup.CustomerGroupID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oCustomerGroup.strConnection = GetConnectionString()
            oCustomerGroup = m_controller.GetCustomerGroupList(oCustomerGroup)

            txtNamaCustomerGroup.Text = oCustomerGroup.CustomerGroupNm
            txtPlafond.Text = FormatNumber(oCustomerGroup.Plafond, 0)
            txtNotes.Text = oCustomerGroup.Catatan.ToString()
            hdnCustomerGroupID.Value = oCustomerGroup.CustomerGroupID

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", Me.AppID) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.CustomerGroup
            With customClass
                .CustomerGroupID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.CustomerGroupDelete(customClass)
            If err <> "" Then
                showMessage(lblMessage, err, True)

            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)

            End If
            BindGridEntity(Me.CmdWhere)
            txtPage.Text = "1"
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim customClass As New Parameter.CustomerGroup
        Dim ErrMessage As String = ""


        With customClass
            .CustomerGroupNm = txtNamaCustomerGroup.Text
            .Plafond = CDec(txtPlafond.Text)
            .Catatan = txtNotes.Text.Trim
            .BranchName = Me.BranchName
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.CustomerGroupSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)

                Exit Sub
            Else
                lblMessage.Text = MessageHelper.MESSAGE_INSERT_SUCCESS

                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.CustomerGroupID = hdnCustomerGroupID.Value
            m_controller.CustomerGroupSaveEdit(customClass)
            lblMessage.Text = MessageHelper.MESSAGE_UPDATE_SUCCESS

            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppID) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        btnBack.Visible = False
        btnCancel.Visible = True
        btnSave.Visible = True
        btnClose.Visible = False
        btnCancel.CausesValidation = False
        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit

    End Sub
  
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("CustomerGroup")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("CustomerGroup")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then            
            Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
            Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + tmptxtSearchBy + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("CustomerGroup.aspx")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("CustomerGroup.aspx")
    End Sub

End Class