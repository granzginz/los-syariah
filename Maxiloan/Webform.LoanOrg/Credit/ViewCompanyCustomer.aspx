﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCompanyCustomer.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewCompanyCustomer" %>

<%@ Register TagPrefix="uc1" TagName="UcAgreementList" Src="../../webform.UserController/UcAgreementList.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewCompanyCustomer</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script> 
     <link rel="Stylesheet" type="text/css" href="../../Include/General.css" /> 

    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlMain" runat="server" >
        <div class="form_title">
            <div class="form_single">
                <h3>
                    INFORMASI DETAIL CORPORATE CUSTOMER
                </h3>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DATA CORPORATE
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Perusahaan
                    </label>
                    <asp:Label ID="lblName" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        ID Customer
                    </label>
                    <asp:Label ID="lblcustomerID" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Perusahaan
                    </label>
                    <asp:Label ID="lblCompanyType" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Alamat
                    </label>
                    <asp:Label ID="lblCompanyAddress" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        NPWP
                    </label>
                    <asp:Label ID="lblNPWP" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        RT/RW
                    </label>
                    <asp:Label ID="lblCompanyRTRW" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jumlah karyawan
                    </label>
                    <asp:Label ID="lblNumEmployee" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Kelurahan
                    </label>
                    <asp:Label ID="lblCompanyKelurahan" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Tahun Berdiri
                    </label>
                    <asp:Label ID="lblYearEstablished" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Kecamatan
                    </label>
                    <asp:Label ID="lblCompanyKecamatan" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Industri
                    </label>
                    <asp:Label ID="lblIndustryType" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Kota
                    </label>
                    <asp:Label ID="lblCompanyCity" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Group
                    </label>
                    <asp:Label ID="lblGroup" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Kode Pos
                    </label>
                    <asp:Label ID="lblCompanyZipCode" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                </div>
                <div class="form_right">
                    <label>
                        Telepon-1
                    </label>
                    <asp:Label ID="lblCompanyPhone1" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                </div>
                <div class="form_right">
                    <label>
                        Telepon-2
                    </label>
                    <asp:Label ID="lblCompanyPhone2" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                </div>
                <div class="form_right">
                    <label>
                        No Fax
                    </label>
                    <asp:Label ID="lblCompanyFax" runat="server" EnableViewState="False"></asp:Label>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlEntJobData" runat="server" >
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DATA FINANCIAL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Current Ratio
                    </label>
                    <asp:Label ID="lblCurrentRatio" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        ROI (%)
                    </label>
                    <asp:Label ID="lblROI" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        DER (%)
                    </label>
                    <asp:Label ID="lblDER" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Rekening Bank
                    </label>
                    <asp:Label ID="lblBankAccountType" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Rata-rata Transaksi Debit
                    </label>
                    <asp:Label ID="lblAvgDebitTrans" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Rata-rata Transaksi Pembiayaan
                    </label>
                    <asp:Label ID="lblAvgCreditTrans" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Saldo rata-rata
                    </label>
                    <asp:Label ID="lblAvgBalance" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Deposito
                    </label>
                    <asp:Label ID="lblDeposito" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Jenis Jaminan Tambahan
                    </label>
                    <asp:Label ID="lblAdditionalCollateralType" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nilai Jaminan Tambahan
                    </label>
                    <asp:Label ID="lblAdditionalCollateralAmount" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Status Kantor
                    </label>
                    <asp:Label ID="lblBusinessPlaceStatus" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Sejak Tahun
                    </label>
                    <asp:Label ID="lblBusinessPlaceSinceYear" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Tanggal berakhirnya Sewa
                    </label>
                    <asp:Label ID="lblRentFinishDate" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    GUDANG
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Status
                    </label>
                    <asp:Label ID="lblStatus" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Alamat
                    </label>
                    <asp:Label ID="lblAddress" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        RT/RW
                    </label>
                    <asp:Label ID="lblRTRW" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Kelurahan
                    </label>
                    <asp:Label ID="lblKelurahan" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        ---
                    </label>
                    ---
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Kecamatan
                    </label>
                    <asp:Label ID="LblKecamatan" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Kota
                    </label>
                    <asp:Label ID="lblCity" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Kode Pos
                    </label>
                    <asp:Label ID="lblZipCode" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Telepon-1
                    </label>
                    <asp:Label ID="lblPhone1" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Telepon-2
                    </label>
                    <asp:Label ID="lblPhone2" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No Fax
                    </label>
                    <asp:Label ID="lblFax" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    REKENING BANK
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Bank
                    </label>
                    <asp:Label ID="lblBankAccount" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Cabang Bank
                    </label>
                    <asp:Label ID="lblBankBranch" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No Rekening
                    </label>
                    <asp:Label ID="lblAccountNo" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Bank
                    </label>
                    <asp:Label ID="lblAccountName" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DATA LAINNYA
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Emergency Kontak
                    </label>
                    <asp:Label ID="lblReference" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Pinjaman Mobil Sebelumnya?
                    </label>
                    <asp:Label ID="lblApplyCarBefore" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Catatan
                    </label>
                    <asp:Label ID="lblNotes" runat="server" EnableViewState="False"></asp:Label>
                </div>
                <div class="form_right">
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlOther" runat="server" >
        <div class="form_title">
            <div class="form_single">
                <h4>
                    MANAGEMENT &amp; PEMEGANG SAHAM
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="DtgManagement" runat="server" CellPadding="0" CssClass="grid_general"
                    AutoGenerateColumns="False" BorderStyle="None" BorderWidth="1px" 
                    EnableViewState="False">
                    <SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
                    <ItemStyle CssClass="item_grid" />
                    <HeaderStyle CssClass="th"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="CSHSeqNo" SortExpression="CSHSeqNo" HeaderText="NO">
                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="NAMA">
                            <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="IDNUMBER" SortExpression="IDNUMBER" HeaderText="NO IDENTITAS">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="JOBPOSITION" SortExpression="JOBPOSITION" HeaderText="JABATAN">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ADDRESS" SortExpression="ADDRESS" HeaderText="ALAMAT">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="PHONE" SortExpression="PHONE" HeaderText="TELEPON"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SHAREPERCENTAGE" SortExpression="SHAREPERCENTAGE" HeaderText="SAHAM (%)">
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle Visible="False" Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOKUMEN LEGAL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgDocument" runat="server" CellPadding="0" CssClass="grid_general"
                    AutoGenerateColumns="False" EnableViewState="False" BorderStyle="None" BorderWidth="1px">
                    <SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
                    <ItemStyle CssClass="item_grid" />
                    <HeaderStyle CssClass="th"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="CLDSeqNo" SortExpression="CLDSeqNo" HeaderText="NO">
                            <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DocumentTYPE" SortExpression="DocumentTYPE" HeaderText="JENIS DOK">
                            <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DOCUMENTNO" SortExpression="DOCUMENTNO" HeaderText="NO DOKUMEN">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DocumentDATE" SortExpression="DocumentDATE" HeaderText="TANGGAL TERBIT">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="JTDate" SortExpression="JTDate" HeaderText="TANGGAL MASA BERLAKU">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="DocumentNOTES" SortExpression="DocumentNOTES" HeaderText="CATATAN">
                        </asp:BoundColumn>
                    </Columns>
                    <PagerStyle Visible="False" Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DAFTAR KONTRAK
            </h4>
        </div>
    </div>
    <div class="form_box_uc">
        <uc1:ucagreementlist id="oAgreementList" runat="server">
                        </uc1:ucagreementlist>
    </div>
    <div class="form_button">
        <a href="javascript:window.close();">
            <asp:Button ID="imbClose" runat="server" Text="Close" CssClass="small buttongo gray"
                EnableViewState="False" /></a> </asp:Button></a><asp:HyperLink ID="HyCustomerExposure"
                    runat="server" EnableViewState="False">Customer Exposure</asp:HyperLink>
    </div>
    </form>
</body>
</html>
