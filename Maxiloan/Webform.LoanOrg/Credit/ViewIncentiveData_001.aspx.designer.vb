﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ViewIncentiveData_001

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''hyApplicationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hyApplicationID As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''HyCust control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyCust As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblRefundPremi control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRefundPremi As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPremiumBase control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPremiumBase As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtRfnSupplier control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRfnSupplier As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''PnlDistribution control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlDistribution As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblPCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPCompany As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInctvAmountCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInctvAmountCompany As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPGM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPGM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFGM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFGM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInctvAmountGM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInctvAmountGM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblAccountGM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAccountGM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPBM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPBM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFBM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFBM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInctvAmountPM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInctvAmountPM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblAccountBM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAccountBM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPADH control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPADH As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFADH control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFADH As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInctvAmountADH control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInctvAmountADH As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblAccountAH control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAccountAH As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPSalesSpv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPSalesSpv As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFSalesSpv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFSalesSpv As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInctvAmountSalesSpv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInctvAmountSalesSpv As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblAccountSV control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAccountSV As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPSalesPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPSalesPerson As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFSalesPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFSalesPerson As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInctvAmountSalesPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInctvAmountSalesPerson As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblAccountSL control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAccountSL As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPSuppAdm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPSuppAdm As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblFSuppAdm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFSuppAdm As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblInctvAmountSuppAdm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblInctvAmountSuppAdm As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''LblAccountAM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents LblAccountAM As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnBack control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnBack As Global.System.Web.UI.WebControls.Button
End Class
