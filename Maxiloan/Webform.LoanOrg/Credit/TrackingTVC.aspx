﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TrackingTVC.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.TrackingTVC" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../Webform.UserController/ucLookUpCustomer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DATA TVC MONITORING BOARD</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR CUSTOMER
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" DataKeyField="ApplicationID" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid" />
                            <HeaderStyle CssClass="th" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="PILIH">
                                    <ItemStyle CssClass="command_col"></ItemStyle>
                                    <ItemTemplate>
                                        <a href='TrackingTVC.aspx?cmd=dtl&id=<%# DataBinder.eval(Container,"DataItem.ApplicationID") 
                                    %>&desc=<%# DataBinder.eval(Container,"DataItem.CustomerName") %>'>ENTRY</a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="ApplicationID" SortExpression="ApplicationID" HeaderText="NO APK">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="DateEntryApplicationData" SortExpression="DateEntryApplicationData"
                                    HeaderText="TGL APK"></asp:BoundColumn>
                                <asp:BoundColumn DataField="SurveyDate" SortExpression="SurveyDate" HeaderText="TGL SURVEY">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="AgreementNo" SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="AgreementDate" SortExpression="AgreementDate" HeaderText="TGL KONTRAK">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="CustomerName" SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASET">
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server" CssClass="small_text">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False" />
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                                CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" Type="integer"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI SUPPLIER
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Suppplier</label>
                        <asp:DropDownList ID="cboSuppplier" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" AutoPostBack="true" runat="server">
                            <asp:ListItem Value="ApplicationID">NO APK</asp:ListItem>
                            <asp:ListItem Value="DateEntryApplicationData">TGL APK</asp:ListItem>
                            <asp:ListItem Value="SurveyDate">TGL SURVEY</asp:ListItem>
                            <asp:ListItem Value="AgreementNo">NO KONTRAK</asp:ListItem>
                            <asp:ListItem Value="AgreementDate">TGL KONTRAK</asp:ListItem>
                            <asp:ListItem Value="CustomerName">NAMA CUSTOMER</asp:ListItem>
                            <asp:ListItem Value="AssetDescription">NAMA ASET</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <%--<div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>--%>
                <%--<div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Konfirmasi Dealer</label>
                        <asp:TextBox ID="TglKonfirmasiDealerDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="TglKonfirmasiDealerDate_CalendarExtender" runat="server"
                            Enabled="True" TargetControlID="TglKonfirmasiDealerDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="TglKonfirmasiDealerDate" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>--%>
                <div class="form_title">
                    <div class="form_single">
                        <h5>
                            TANDA TANGAN KONTRAK
                        </h5>
                        <%--<asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>--%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Janji</label>
                        <asp:TextBox ID="TglJanjiDate" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="TglJanjiDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="TglJanjiDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="TglJanjiDate" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Realisasi</label>
                        <asp:TextBox ID="txtTglRealisasi" runat="server"></asp:TextBox>
                        <asp:CalendarExtender ID="ceTglRealisasi" runat="server" Enabled="True" TargetControlID="txtTglRealisasi"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTglRealisasi" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general" >
                            Keterangan</label>
                        <textarea id="txtKeterangan" runat="server" cols="17" rows="5" class="multiline_textbox_uc"></textarea>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>&nbsp;
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="true" Text="Cancel"
                        CssClass="small button gray"></asp:Button>&nbsp;
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
