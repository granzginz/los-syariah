﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAmortization.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewAmortization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAmortization</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=15, top=10, width=985, height=600, menubar=0, scrollbars=yes');
        }
		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - AMORTISASI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:LinkButton ID="lblCustomerName" runat="server" EnableViewState="False"></asp:LinkButton>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Principal
                </label>
                <asp:Label ID="lblOSPrincipal" runat="server" EnableViewState="False"></asp:Label>
            </div>
            
            <div class="form_right">
                <label>
                    Outstanding Margin
                </label>
                <asp:Label ID="lblOSInterest" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Principal Undue
                </label>
                <asp:Label ID="lblOSPrincipalUndue" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Outstanding Margin Undue
                </label>
                <asp:Label ID="lblOSInterestUnDue" runat="server" Height="16px" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    O/S Angsuran Jatuh Tempo
                </label>
                <asp:Label ID="lblOSInstallmentDue" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    O/S Asuransi jatuh tempo
                </label>
                <asp:Label ID="lblOSInsuranceDue" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    O/S Ta'widh Keterlambatan Angsuran
                </label>
                <asp:Label ID="lblLCInstallment" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    O/S Koreksi Ta'widh
                </label>
                <asp:Label ID="lblOSLCInsurance" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    O/S Biaya Tagih Angsuran
                </label>
                <asp:Label ID="lblOSInstallCollectionFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    O/S Biaya tagih Asuransi
                </label>
                <asp:Label ID="lblOSInsuranceCollFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    O/S Biaya Tolakan PDC
                </label>
                <asp:Label ID="lblOSPDCBounceFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    O/S Biaya STNK
                </label>
                <asp:Label ID="lblOSSTNKFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    O/S Biaya Klaim Asuransi
                </label>
                <asp:Label ID="lblOSInsuranceClaimExpense" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    O/S Biaya Tarik
                </label>
                <asp:Label ID="lblOSCollExpense" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Jumlah Contract Prepaid
                </label>
                <asp:Label ID="lblContactPrepaidAmt" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah dibayar
                </label>
                <asp:Label ID="lblTotalAmountToBePaid" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Angsuran Berikut
                </label>
                <asp:Label ID="lblNextInstallmentNumber" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Angsuran Berikut
                </label>
                <asp:Label ID="lblNextInstallmentDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    No Angsuran Berikut JT
                </label>
                <asp:Label ID="lblNextInstallmentDueNumber" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tgl JT Angsuran Berikut
                </label>
                <asp:Label ID="lblNextInstallmentDueDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                CellPadding="3" EnableViewState="False" CellSpacing="1" CssClass="grid_general"
                Width="100%" ShowFooter="true" FooterStyle-HorizontalAlign="Right">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <FooterStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">                       
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server"  Text='<%# formatnumber(container.dataitem("InsSeqNo"),0) %>'> </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                                Total Amount
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="InsSeqNo"></asp:BoundColumn>
                    <asp:BoundColumn DataField="DUEDATE" HeaderText="TGL JT" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">  
                         </asp:BoundColumn>
                     <asp:BoundColumn DataField="PaidDate" HeaderText="TGL BAYAR" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">                        
                    </asp:BoundColumn>
                     <asp:BoundColumn Visible="False" DataField="GracePeriod" HeaderText="Grace Period" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">                        
                    </asp:BoundColumn>
                     <asp:BoundColumn DataField="OD" HeaderText="OD" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Center">                        
                    </asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="TA'WIDH" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblDenda" runat="server" Text='<%#FormatNumber(Container.DataItem("DENDA"), 0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotDenda" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <%-- <asp:BoundColumn DataField="Denda" HeaderText="DENDA" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">                        
                    </asp:BoundColumn>--%>
                    <asp:TemplateColumn HeaderText="JML BAYAR" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPaidAmount" runat="server" Text='<%# formatnumber(container.dataitem("PaidAmount"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotPaidAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="BYR POKOK" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPaidPrincipal" runat="server" Text='<%# formatnumber(container.dataitem("PaidPrincipal"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotPaidPrincipal" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="BYR MARGIN" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPaidInterest" runat="server" Text='<%# formatnumber(container.dataitem("PaidInterest"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotPaidInterest" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="BYR TA'WIDH" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPaidLateCharges" runat="server" Text='<%# formatnumber(container.dataitem("PaidLateCharges"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotPaidLateCharges" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="ANGSURAN" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblInstallmentAmount" runat="server" Text='<%# formatnumber(container.dataitem("InstallmentAmount"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>                            
                            <asp:Label ID="lblTotInstallmentAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="POKOK" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblPrincipalAmount" runat="server" Text='<%# formatnumber(container.dataitem("PrincipalAmount"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotPrincipalAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="MARGIN" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblINTERESTAMOUNT" runat="server" Text='<%# formatnumber(container.dataitem("INTERESTAMOUNT"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotINTERESTAMOUNT" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="O/S POKOK" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblDtgOSPrincipal" runat="server" Text='<%# formatnumber(container.dataitem("OutstandingPrincipal"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotOSPrincipal" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="O/S MARGIN" HeaderStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Right">                        
                        <ItemTemplate>
                            <asp:Label ID="lblDtgOSInterest" runat="server" Text='<%# formatnumber(container.dataitem("OutstandingInterest"),0) %>'>
                            </asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotOSInterest" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
   <%-- <div class="form_button">
        <asp:Button ID="btnReport" runat="server"
            Text="View Report" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
