﻿#Region "Import"

Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports System.Math
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Parameter
#End Region

Public Class ViewIncentiveData_001
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Public Property AgreementNO() As String
        Get
            Return (CType(ViewState("AgreementNO"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNO") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property RefundToSupplierAmount() As Decimal
        Get
            Return (CType(ViewState("RefundToSupplierAmount"), Decimal))
        End Get
        Set(ByVal Value As Decimal)
            ViewState("RefundToSupplierAmount") = Value
        End Set
    End Property
    Private Property BID() As String
        Get
            Return CType(ViewState("BID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BID") = Value
        End Set
    End Property
    Private Property SupplierID() As String
        Get
            Return CType(ViewState("SupplierID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("SupplierID") = Value
        End Set
    End Property
    Public Property Style() As String
        Get
            Return ViewState("Style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property
    Public Property PageSource() As String
        Get
            Return ViewState("PageSource").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("PageSource") = Value
        End Set
    End Property


#End Region
#Region "Constanta"
    Private oController As New IncentiveCardController
    Private m_Controller As New SupplierController
    Dim oSupplier As New Parameter.IncentiveCard
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            GetCookies()
            InitialPageLoad()
        End If
    End Sub
#Region "GetCookies"
    Sub GetCookies()
        Dim CustomerID As String
        Dim CustomerName As String
        If Request.QueryString("ApplicationID") <> "" Then Me.ApplicationID = Request.QueryString("ApplicationID")
        If Request.QueryString("AgreementNO") <> "" Then Me.AgreementNO = Request.QueryString("AgreementNO")
        If Request.QueryString("CustomerName") <> "" Then CustomerName = Request.QueryString("CustomerName")
        If Request.QueryString("pagesource") <> "" Then Me.PageSource = Request.QueryString("pagesource")
        If Request.QueryString("CustomerID") <> "" Then CustomerID = Request.QueryString("CustomerID")
        If Request.QueryString("Style") <> "" Then Me.Style = Request.QueryString("Style")
        hyApplicationID.Text = Me.ApplicationID.Trim
        HyCust.Text = CustomerName
        hyApplicationID.NavigateUrl = "javascript:OpenApplicationId('" & "AccAcq" & "', '" & Server.UrlEncode(hyApplicationID.Text) & "')"
        HyCust.NavigateUrl = "javascript:OpenCustomer('" & "AccAcq" & "', '" & Server.UrlEncode(CustomerID.Trim) & "')"
    End Sub
#End Region
#Region "Private Sub"
    Private Sub InitialPageLoad()
        PnlDistribution.Visible = True
        GetRefundPremiumToSupplier()
        GetIncentiveDistributionData()
        GetdataFromSupplierIncentiveDailyD()
    End Sub
    Sub GetIncentiveDistributionData()
        Dim oData As New DataTable
        Dim oData2 As New DataTable
        Dim oEntity As New Parameter.Supplier
        Dim oRow As DataRow = Nothing
        Dim oRow2 As DataRow = Nothing

        oEntity.strConnection = GetConnectionString()
        oEntity.SupplierID = Me.SupplierID
        oEntity.BranchId = Me.BID
        oEntity.WhereCond = Me.ApplicationID
        oEntity = m_Controller.GetSupplierBranchEdit(oEntity)
        oData = oEntity.ListData
        oData2 = oEntity.ListData3

        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
        End If
        If oData2.Rows.Count > 0 Then
            oRow2 = oData2.Rows(0)
        End If

        If oData.Rows.Count > 0 Then
            lblPCompany.Text = oRow(5).ToString.Trim
            lblPGM.Text = oRow(6).ToString.Trim
            lblPBM.Text = oRow(7).ToString.Trim
            lblPADH.Text = oRow(8).ToString.Trim
            lblPSalesSpv.Text = oRow(9).ToString.Trim
            lblPSalesPerson.Text = oRow(10).ToString.Trim
            lblPSuppAdm.Text = oRow(11).ToString.Trim
            lblFGM.Text = oRow(12).ToString.Trim
            lblFBM.Text = oRow(13).ToString.Trim
            lblFADH.Text = oRow(14).ToString.Trim
            lblFSalesSpv.Text = oRow(15).ToString.Trim
            lblFSalesPerson.Text = oRow(16).ToString.Trim
            lblFSuppAdm.Text = oRow(17).ToString.Trim
        End If
        If oData2.Rows.Count > 0 Then
            LblAccountAM.Text = oRow2(0).ToString.Trim
            LblAccountSL.Text = oRow2(1).ToString.Trim
            LblAccountAH.Text = oRow2(2).ToString.Trim
            LblAccountBM.Text = oRow2(3).ToString.Trim
            LblAccountGM.Text = oRow2(4).ToString.Trim
            LblAccountSV.Text = oRow2(5).ToString.Trim
        End If
    End Sub
#End Region
#Region "GetRefundPremiumToSupplier"
    Public Sub GetRefundPremiumToSupplier()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVGetRefundPremiumToSupplier"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID
            objCommand.Parameters.Add("@RefundToSupp", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@SupplierID", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@PremiumBaseforRefundSupp", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@RefundPremi", SqlDbType.Char, 10).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@RefundInterest", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            Me.RefundToSupplierAmount = CDec(IIf(IsDBNull(objCommand.Parameters("@RefundToSupp").Value), 0, objCommand.Parameters("@RefundToSupp").Value))
            Me.SupplierID = CStr(IIf(IsDBNull(objCommand.Parameters("@SupplierID").Value), "", objCommand.Parameters("@SupplierID").Value))
            Me.BID = CStr(IIf(IsDBNull(objCommand.Parameters("@BranchID").Value), "", objCommand.Parameters("@BranchID").Value))
            lblPremiumBase.Text = CStr(IIf(IsDBNull(objCommand.Parameters("@PremiumBaseforRefundSupp").Value), "", objCommand.Parameters("@PremiumBaseforRefundSupp").Value))
            lblRefundPremi.Text = CStr(IIf(IsDBNull(objCommand.Parameters("@RefundPremi").Value), "", objCommand.Parameters("@RefundPremi").Value)) + " %"
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
    Public Sub GetdataFromSupplierIncentiveDailyD()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spSupplierINCTVGetDailyDView"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID
            objCommand.Parameters.Add("@IncentiveForSupplier", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForGM", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForBM", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForADH", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForSPV", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForSL", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Parameters.Add("@IncentiveForAM", SqlDbType.Decimal).Direction = ParameterDirection.Output
            objCommand.Connection = objConnection
            objCommand.ExecuteNonQuery()
            lblInctvAmountCompany.Text = FormatNumber((IIf(IsDBNull(objCommand.Parameters("@IncentiveForSupplier").Value), "0", objCommand.Parameters("@IncentiveForSupplier").Value)), 0)
            lblInctvAmountGM.Text = FormatNumber((IIf(IsDBNull(objCommand.Parameters("@IncentiveForGM").Value), "0", objCommand.Parameters("@IncentiveForGM").Value)), 0)
            lblInctvAmountPM.Text = FormatNumber((IIf(IsDBNull(objCommand.Parameters("@IncentiveForBM").Value), "0", objCommand.Parameters("@IncentiveForBM").Value)), 0)
            lblInctvAmountADH.Text = FormatNumber((IIf(IsDBNull(objCommand.Parameters("@IncentiveForADH").Value), "0", objCommand.Parameters("@IncentiveForADH").Value)), 0)
            lblInctvAmountSalesSpv.Text = FormatNumber((IIf(IsDBNull(objCommand.Parameters("@IncentiveForSPV").Value), "0", objCommand.Parameters("@IncentiveForSPV").Value)), 0)
            lblInctvAmountSalesPerson.Text = FormatNumber((IIf(IsDBNull(objCommand.Parameters("@IncentiveForSL").Value), "0", objCommand.Parameters("@IncentiveForSL").Value)), 0)
            lblInctvAmountSuppAdm.Text = FormatNumber((IIf(IsDBNull(objCommand.Parameters("@IncentiveForAM").Value), "0", objCommand.Parameters("@IncentiveForAM").Value)), 0)
            txtRfnSupplier.Text = FormatNumber((CInt(lblInctvAmountCompany.Text) + CInt(lblInctvAmountGM.Text) + CInt(lblInctvAmountPM.Text) + CInt(lblInctvAmountADH.Text) + CInt(lblInctvAmountSalesSpv.Text) + CInt(lblInctvAmountSalesPerson.Text) + CInt(lblInctvAmountSuppAdm.Text)), 0)
            lblTotal.Text = FormatNumber((CInt(lblInctvAmountCompany.Text) + CInt(lblInctvAmountGM.Text) + CInt(lblInctvAmountPM.Text) + CInt(lblInctvAmountADH.Text) + CInt(lblInctvAmountSalesSpv.Text) + CInt(lblInctvAmountSalesPerson.Text) + CInt(lblInctvAmountSuppAdm.Text)), 0)
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
#End Region

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        If Me.PageSource = "app" Then
            Response.Redirect("CreditProcess/NewApplication/ViewApplication.aspx?Applicationid=" & Me.ApplicationID & "&style=" & Me.Style & "")
        Else
            Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNO & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
        End If
    End Sub

End Class