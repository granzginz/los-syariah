﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
#End Region

Public Class ViewFinancial
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucViewApplication1 As ucViewApplication
    Protected WithEvents ucViewCustomerDetail1 As ucViewCustomerDetail
    Protected WithEvents ucFlagTransaksi As ucFlagTransaksi

#Region "Constanta"    
    Dim objrow As DataRow
    Dim intLoop As Integer
    Dim myDataTable As DataTable
    Dim myDataColumn As DataColumn
    Dim myDataRow As DataRow
    Dim myDataSet As New DataSet
    Dim dblInterestTotal As Double
    Dim i As Integer
    Dim RunRate As Double
    'Dim FlatRate As Decimal
    Dim PokokHutang() As Double
    Dim Hutang() As Double
    Dim PokokBunga() As Double
    Dim Bunga() As Double
    Dim HitungUlangSisa As Boolean = False
    Dim HitungUlangKurang As Boolean = False
    Dim SisaPrincipal As Double
    Dim KurangPrincipal As Double
    Dim m_controller As New FinancialDataController
    Dim Entities As New Parameter.FinancialData
    Dim oApplication As New Parameter.Application
    Dim m_ControllerApp As New ApplicationController

    Private oRefundInsentifController As New RefundInsentifController
    Private oController As New ApplicationDetailTransactionController

    Private TotalInsGrid As Double
    Private TotalProsentase As Double
    Private TotalPencairan As Double
#End Region
#Region "Property"
    Private TotalBungaNett As Decimal
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Property BrchId() As String
        Get
            Return ViewState("BrchId").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("BrchId") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return ViewState("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property Style() As String
        Get
            Return ViewState("Style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property
    Private Property FlatRate() As Decimal
        Get
            Return CType(ViewState("FlatRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("FlatRate") = Value
        End Set
    End Property
    Property RefundBungaAmount As Double
        Get
            Return CType(ViewState("RefundBungaAmount"), Double)
        End Get
        Set(ByVal value As Double)
            ViewState("RefundBungaAmount") = value
        End Set
    End Property
    Private Property EffectiveRate() As Decimal
        Get
            Return CType(ViewState("EffectiveRate"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            ViewState("EffectiveRate") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID").ToString
            Me.BrchId = Request("BrchId").ToString
            Me.CustomerName = Request("CustomerName").ToString
            Me.style = Request("Style").ToString
            Me.CustomerID = Request("CustomerID").ToString.Trim
            Me.AgreementNo = Request("AgreementNo").ToString            

            With ucViewApplication1
                .ApplicationID = Me.ApplicationID
                .bindData()
                .initControls("OnNewApplication")
            End With

            ucViewCustomerDetail1.CustomerID = Me.CustomerID
            ucViewCustomerDetail1.bindCustomerDetail()

            Bindgrid_002()            
            showPencairan()                        

            'lblTotalBunga.Text = FormatNumber(Math.Ceiling(Math.Round((CDbl(lblNTF.Text) * CInt(lblNumInst.Text) / m_controller.GetTerm(1) * Me.FlatRate / 100), 0) / 1000) * 1000, 0)
            Dim ntf As Double
            Dim totalbunga As Double
            Dim nilaiKontrak As Double

            If IsDBNull(lblNTF.Text) Or lblNTF.Text = "" Then
                ntf = CDbl(0)
            Else
                ntf = CDbl(lblNTF.Text)
            End If

            If IsDBNull(lblTotalBunga.Text) Or lblTotalBunga.Text = "" Then
                totalbunga = CDbl(0)
            Else
                totalbunga = CDbl(lblTotalBunga.Text)
            End If
            nilaiKontrak = ntf + totalbunga

            lblNilaiKontrak.Text = FormatNumber(nilaiKontrak, 0)
        End If



    End Sub
#Region "Load Financial Data"
    Sub Bindgrid_002()
        Dim dtEntity As New DataTable
        Dim oFinancialData As New Parameter.FinancialData

        oFinancialData.BranchId = CStr(IIf(Me.BrchId Is Nothing, Replace(Me.sesBranchId, "'", ""), Me.BrchId))
        oFinancialData.ApplicationID = Me.ApplicationID
        oFinancialData.strConnection = GetConnectionString()
        oFinancialData.SpName = "spFinancialData_002"
        oFinancialData = m_controller.GetFinancialData_002(oFinancialData)

        If Not oFinancialData Is Nothing Then
            dtEntity = oFinancialData.ListData
        End If

        Dim strPOEffectiveRate As String
        Dim strPOEffectiveRateBehaviour As String
        Dim strPBEffectiveRate As String
        Dim strPBEffectiveRateBehaviour As String
        Dim strPEffectiveRate As String
        Dim strPEffectiveRateBehaviour As String

        If dtEntity.Rows.Count > 0 Then
            objrow = dtEntity.Rows(0)

            lblAssetInsurance3.Text = FormatNumber(objrow.Item("InsAssetCapitalized"), 0)
            lblAssetInsurance32.Text = FormatNumber(objrow.Item("PaidAmountByCust"), 0)            
            lblOTR.Text = FormatNumber(objrow.Item("OTRKendaraan"), 0)
            lblKaroseri.Text = FormatNumber(objrow.Item("HargaKaroseri"), 0)
            lblTotalOTR.Text = FormatNumber(objrow.Item("TotalOTR"), 0)
            lblOTRSupplier.Text = FormatNumber(objrow.Item(3), 2)
            lblDP.Text = FormatNumber(Math.Round(CDbl(objrow.Item(4)), 0), 0)
            lblDPKaroseri.Text = FormatNumber(objrow.Item("DPKaroseriAmount"), 0)
            'If Not IsDBNull(objrow("EffectiveDate")) Then lblTglAngsuranI.Text = Format(objrow("EffectiveDate"), "dd/MM/yyyy")
            lblNTF.Text = FormatNumber(objrow.Item(5), 0)
            lblNTFSupplier.Text = FormatNumber(objrow.Item(5), 2)
            lblTenor.Text = objrow.Item(8).ToString.Trim           
            lblNumInst.Text = objrow.Item(8).ToString.Trim            

            lblEffectiveRate.Text = FormatNumber(CDbl(objrow.Item("EffectiveRate")), 7)
            EffectiveRate = CDec(objrow.Item("EffectiveRate"))

            lblBiayaPolis.Text = FormatNumber(objrow.Item("BiayaPolisAgreement"), 0)
            lblAdminFee.Text = FormatNumber(objrow.Item("AdminFee"), 0)
            lblOtherFee.Text = FormatNumber(objrow.Item("OtherFee"), 0)
            'lblOtherFee2.Text = FormatNumber(objrow.Item("OtherFee"), 0)            
            lblIsAdminFeeCredit.Text = "(" & objrow.Item("IsAdminFeeCredit").ToString & ")"            

            ' Bagin Effective Rate dan pengecekannya
            strPOEffectiveRate = objrow.Item("POEffectiveRate").ToString.Trim
            ' strPOEffectiveRateBehaviour = objrow.Item("POEffectiveRateBehaviour").ToString.Trim

            strPBEffectiveRate = objrow.Item(13).ToString.Trim
            strPBEffectiveRateBehaviour = objrow.Item(14).ToString.Trim

            strPEffectiveRate = objrow.Item(15).ToString.Trim
            strPEffectiveRateBehaviour = objrow.Item(16).ToString.Trim

            'lblPolaAngsuran.Text = cboPolaAngsuran.Items.FindByValue(objrow.Item("PolaAngsuran").ToString.Trim).Text
            lblFirstInstallment.Text = cboFirstInstallment.Items.FindByValue(objrow.Item("FirstInstallment").ToString.Trim).Text
            'lblBayarDealer.Text = optBayarDealer.Items.FindByValue("1").Text
            lblPotongPencairanDana.Text = optPotongPencairanDana.Items.FindByValue("1").Text


            oApplication.BranchId = Replace(Me.sesBranchId, "'", "")
            oApplication.ApplicationID = Me.ApplicationID
            oApplication.strConnection = GetConnectionString()
            oApplication = m_ControllerApp.GetDataAppIDProspect(oApplication)

            If oApplication.Err = "" Then
                If Me.ApplicationID.Trim = oApplication.ApplicationID.Trim Then
                    lblInstAmt.Text = CStr(oApplication.InstallmentAmount)
                    cboFirstInstallment.SelectedIndex = cboFirstInstallment.Items.IndexOf(cboFirstInstallment.Items.FindByValue(oApplication.FirstInstallment))
                Else
                    lblInstAmt.Text = CInt(objrow.Item(9)).ToString.Trim
                End If
            End If

            cboFirstInstallment.SelectedValue = objrow.Item("FirstInstallment").ToString
            lblDP.Text = FormatNumber(objrow.Item("DownPayment"), 0)
            lblTidakAngsur.Text = FormatNumber(objrow.Item("InstallmentUnpaid"), 0)
            lblFlatRate.Text = FormatNumber(objrow.Item("FlatRate"), 2)

            If CDbl(lblFlatRate.Text.Trim) = 0 Then
                cboFirstInstallment.SelectedValue = "AD"
                Dim oClass As New Parameter.FinancialData
                With oClass
                    .Tenor = CInt(lblNumInst.Text)
                    .EffectiveRate = CDec(objrow.Item("POEffectiveRate")) / 100
                    .FirstInstallment = cboFirstInstallment.SelectedValue
                End With
                Me.FlatRate = RateConvertion.cEffToFlat(oClass)
                lblFlatRate.Text = FormatNumber(Me.FlatRate, 2)
            End If

            Dim pgross, pinsnet, pselisih, ppolis, potherfee As Double

            lblPremiAsuransiGross.Text = FormatNumber(CDbl(objrow.Item("PremiGross")) + CDbl(objrow.Item("BiayaPolis")), 0)
            lblPremiAsuransiNet.Text = FormatNumber(objrow.Item("PremiNett"), 0)

            pgross = CDbl(IIf(IsNumeric(lblPremiAsuransiGross.Text), lblPremiAsuransiGross.Text, 0))
            pinsnet = CDbl(IIf(IsNumeric(lblPremiAsuransiNet.Text), lblPremiAsuransiNet.Text, 0))
            ppolis = CDbl(IIf(IsNumeric(objrow.Item("BiayaPolis")), objrow.Item("BiayaPolis"), 0))
            potherfee = CDbl(IIf(IsNumeric(objrow.Item("OtherFee")), objrow.Item("OtherFee"), 0))

            pselisih = pgross - pinsnet
            lblSelisihPremi.Text = FormatNumber(pselisih, 0)
            
            lblBiayaFidusia.Text = FormatNumber(CDec(objrow("FiduciaFee")), 0)

            If objrow("GabungRefundSupplier").ToString = "True" Then
                lblGabungRefundSupplier.Text = "Ya"
            Else
                lblGabungRefundSupplier.Text = "Tidak"
            End If
            lblTotalBunga.Text = FormatNumber(CDbl(objrow("TotalBunga")), 0)            
            lblNilaiKontrak.Text = FormatNumber(CDbl(lblNTF.Text) + CDbl(lblTotalBunga.Text), 0)

            CalculateDPPersen()

            lblInstAmt.Text = FormatNumber(CDec(objrow("InstallmentAmount")), 0)            

            lblAngsuranPertama.Text = IIf(cboFirstInstallment.SelectedValue = "AD", lblInstAmt.Text, "0").ToString
            lblBiayaProvisi.Text = FormatNumber(objrow("ProvisionFee"), 0)

            lblRefundBunga.Text = FormatNumber(objrow("refundBungaPercent"), 2)
            If CInt(objrow("refundBunga").ToString) = 0 And CInt(lblRefundBunga.Text) > 0 Then
                calculateNPV()
            Else
                Me.RefundBungaAmount = CDbl(objrow("refundBunga"))
                lblRefundBungaN.Text = FormatNumber(objrow("refundBunga"), 0)
            End If
            lblRefundAdmin.Text = FormatNumber(objrow.Item("AdminFee"), 0)

           
            'lblTitipanRefundBungaN.Text = FormatNumber(objrow("TitipanRefundBunga"), 0)
            'lblTitipanRefundBunga.Text = FormatNumber(objrow("TitipanRefundBungaPercent"), 2)
            lblRAdminFee.Text = FormatNumber(objrow.Item("AdminFee"), 0)
            lblRefundAdminPersent.Text = FormatNumber(CInt(objrow.Item("SupplierRefundAdminPercent")), 2)
            lblSubsidiAngsuran.Text = FormatNumber(objrow("SubsidiAngsuran"), 0)
            'lblAloaskiInsentifPremiN.Text = FormatNumber(objrow("AlokasiInsentifPremi"), 0)
            'lblAloaskiInsentifPremi.Text = FormatNumber(objrow("AlokasiInsentifPremiPercent"), 2)
            'lblAloaskiProgresifPremiN.Text = FormatNumber(objrow("AlokasiProgresifPremi"), 0)
            'lblAloaskiProgresifPremi.Text = FormatNumber(objrow("AlokasiProgresifPremiPercent"), 2)
            'lblSubsidiBungaPremiN.Text = FormatNumber(objrow("SubsidiBungaPremi"), 0)
            'lblSubsidiBungaPremi.Text = FormatNumber(objrow("SubsidiBungaPremiPercent"), 2)
         
            lblRefundAdmin.Text = FormatNumber(CInt(objrow.Item("AdminFee")) * (CInt(objrow.Item("SupplierRefundAdminPercent")) / 100), 0)

            lblPendapatanPremi.Text = FormatNumber(objrow("PendapatanPremiPercent"), 2)
            lblPendapatanPremiN.Text = FormatNumber(objrow("PendapatanPremi"), 0)

            lblPendapatanPremiN.Text = FormatNumber(objrow("PendapatanPremi"), 0)
            lblPendapatanPremi.Text = FormatNumber(objrow("PendapatanPremiPercent"), 2)

            lblBiayaProvisiN.Text = FormatNumber(objrow("ProvisionFee"), 0)
            lblBiayaProvisi.Text = FormatNumber(objrow("ProvisionFee"), 2)
            'lblAlokasiInsentifProvisiN.Text = FormatNumber(objrow("AlokasiInsentifProvisi"), 0)
            'lblAlokasiInsentifProvisi.Text = FormatNumber(objrow("AlokasiInsentifProvisiPercent"), 2)
            'lblSubsidiBungaProvisiN.Text = FormatNumber(objrow("SubsidiBungaProvisi"), 0)
            'lblSubsidiBungaProvisi.Text = FormatNumber(objrow("SubsidiBungaProvisiPercent"), 2)
            'lblTitipanProvisiN.Text = FormatNumber(objrow("TitipanProvisi"), 0)
            'lblTitipanProvisi.Text = FormatNumber(objrow("TitipanProvisiPercent"), 2)
            'xlblBiayaProvisiN.Text = lblBiayaProvisiN.Text

            'lblAlokasiInsentifBiayaLainnyaN.Text = FormatNumber(objrow("AlokasiInsentifBiayaLain"), 0)
            'lblAlokasiInsentifBiayaLainnya.Text = FormatNumber(objrow("AlokasiInsentifBiayaLainPercent"), 2)
            'lblSubsidiBungaBiayaLainnyaN.Text = FormatNumber(objrow("SubsidiBungaBiayaLain"), 0)
            'lblSubsidiBungaBiayaLainnya.Text = FormatNumber(objrow("SubsidiBungaBiayaLainPercent"), 2)
            'lblTitipanLainnyaN.Text = FormatNumber(objrow("TitipanBiayaLain"), 0)
            'lblTitipanLainnya.Text = FormatNumber(objrow("TitipanBiayaLainPercent"), 2)
            lblInsAmountToNPV.Text = FormatNumber(objrow("InsAmountToNPV"), 0)

            lblSubsidiBungaDealer.Text = FormatNumber(objrow("SubsidiBungaDealer"), 0)
            lblRefundBiayaprovisiPersent.Text = FormatNumber(objrow("RefundBiayaProvisiPercent"), 0)
            lblRefundBiayaprovisi.Text = FormatNumber(objrow("RefundBiayaProvisi"), 0)
            lblRefundPremiPersent.Text = FormatNumber(objrow("RefundPremiPercent"), 0)
            lblRefundPremi.Text = FormatNumber(objrow("RefundPremi"), 0)

            'lblPendapatanPengeluaranAsuransi.Text = FormatNumber(CDbl(CStr(CDbl(lblSelisihPremi.Text.Replace(",", "")) - CDbl(lblRefundPremi.Text.Replace(",", ""))))
            'lblAlokasiRefundBunga.Text = FormatNumber(CDbl(lblAlokasiInsentifBungaN.Text) + CDbl(lblTitipanRefundBungaN.Text), 0)
            'lblAlokasiTitipanProvisi.Text = FormatNumber(CDbl(lblAlokasiInsentifProvisiN.Text) + CDbl(lblSubsidiBungaProvisiN.Text) + CDbl(lblTitipanProvisiN.Text), 0)
            'lblAlokasiBiayaLainnya.Text = FormatNumber(CDbl(lblAlokasiInsentifBiayaLainnyaN.Text) + CDbl(lblSubsidiBungaBiayaLainnyaN.Text) + CDbl(lblTitipanLainnyaN.Text), 0)
            'lblTotalPremiAsuransi.Text = FormatNumber(CDbl(lblAloaskiInsentifPremiN.Text) + CDbl(lblAloaskiProgresifPremiN.Text) + CDbl(lblSubsidiBungaPremiN.Text) + CDbl(lblPendapatanPremiN.Text), 0)


            lblRefundBunga.Text = FormatNumber(objrow("refundBungaPercent"), 2)            

            'If CDbl(pselisih) <> CDbl(lblTotalPremiAsuransi.Text.ToString) Then
            '    'lblAloaskiInsentifPremiN.Text = FormatNumber(0, 0)
            '    lblAloaskiProgresifPremiN.Text = FormatNumber(0, 0)
            '    lblSubsidiBungaPremiN.Text = FormatNumber(0, 0)
            '    lblPendapatanPremiN.Text = FormatNumber(pselisih, 0)
            '    lblPendapatanPremiN.Text = FormatNumber(pselisih, 0)

            '    lblPendapatanPremi.Text = FormatNumber(0, 2)
            '    'lblAloaskiInsentifPremi.Text = FormatNumber(0, 2)
            '    lblAloaskiProgresifPremi.Text = FormatNumber(0, 2)
            '    lblSubsidiBungaPremi.Text = FormatNumber(0, 2)
            '    lblPendapatanPremi.Text = FormatNumber(100, 2)

            '    lblTotalPremiAsuransi.Text = FormatNumber(pselisih, 0)
            'End If

            CalculateTotalBayarPertama()

            MaxRefundBungaP.Text = getMaxRefundBunga()
            MaxRefundBungaN.Text = CStr(CDbl(lblTotalBunga.Text.Replace(",", "")) * (CDbl(MaxRefundBungaP.Text.Replace(",", "")) / 100))

            lblBiayaProvisiAwal.Text = FormatNumber(objrow("ProvisiPercent"), 2)
        End If
    End Sub
    Private Function getMaxRefundBunga() As String
        Dim oClass As New Parameter.GeneralSetting
        Dim oCtr As New GeneralSettingController

        With oClass
            .strConnection = GetConnectionString()
            .GSID = "RFBNG"
            .ModuleID = "CRD"
        End With

        Try
            Return oCtr.GetGeneralSettingValue(oClass)
        Catch ex As Exception
            Return "0"
        End Try
    End Function
    Protected Sub CalculateDPPersen()
        'lblDPPersen.Text = FormatNumber(CDec(IIf(IsNumeric(lblDP.Text), lblDP.Text, 0)) / CDbl(lblTotalOTR.Text) * 100, 2)
        lblDPPersen.Text = FormatNumber(CDec(IIf(IsNumeric(lblDP.Text), lblDP.Text, 0)) / CDbl(lblOTR.Text) * 100, 2)
        lblDPPersenKaroseri.Text = FormatNumber(CDec(IIf(IsNumeric(lblDPKaroseri.Text), lblDPKaroseri.Text, 0)) / CDbl(lblKaroseri.Text) * 100, 2)
        'lblUangMuka.Text = FormatNumber(lblDP.Text, 0)
        lblUangMuka.Text = FormatNumber(CDbl(lblDP.Text) + CDbl(lblDPKaroseri.Text), 0)
    End Sub
    Private Sub calculateNPV()
        Dim ntf As Double
        Dim refundRate As Decimal
        Dim flatRate As Decimal
        Dim TenorYear As Integer

        ntf = CDbl(IIf(IsNumeric(lblNTF.Text), lblNTF.Text, 0))
        refundRate = CDec(IIf(IsNumeric(lblRefundBunga.Text), lblRefundBunga.Text, 0))
        flatRate = CDec(IIf(IsNumeric(lblFlatRate.Text), lblFlatRate.Text, 0))
        TenorYear = CInt(IIf(IsNumeric(lblNumInst.Text), CInt(lblNumInst.Text) / 12, 0))

        Me.RefundBungaAmount = CreditProcess.getNPV(ntf, refundRate, flatRate, TenorYear)
        lblRefundBungaN.Text = FormatNumber(Me.RefundBungaAmount, 0)        

    End Sub
    Private Sub CalculateTotalBayarPertama()
        Dim um, ba, bf, at, ap, ot, bp, bpr As Double

        um = CDbl(IIf(IsNumeric(lblUangMuka.Text), lblUangMuka.Text, 0))
        ba = CDbl(IIf(IsNumeric(lblAdminFee.Text), lblAdminFee.Text, 0))
        bf = CDbl(IIf(IsNumeric(lblBiayaFidusia.Text), lblBiayaFidusia.Text, 0))
        at = CDbl(IIf(IsNumeric(lblAssetInsurance32.Text), lblAssetInsurance32.Text, 0))
        ap = CDbl(IIf(IsNumeric(lblAngsuranPertama.Text), lblAngsuranPertama.Text, 0))
        ot = CDbl(IIf(IsNumeric(lblOtherFee.Text), lblOtherFee.Text, 0))
        bp = CDbl(IIf(IsNumeric(lblBiayaPolis.Text), lblBiayaPolis.Text, 0))
        bpr = CDbl(IIf(IsNumeric(lblBiayaProvisi.Text), lblBiayaProvisi.Text, 0))


        lblTotalBayarPertama.Text = FormatNumber(um + ba + bf + at + ap + ot + bp + bpr, 0)

    End Sub
    'Protected Sub getRincianTransaksi()
    '    Dim oPar As New Parameter.ApplicationDetailTransaction

    '    With oPar
    '        .strConnection = GetConnectionString()
    '        .ApplicationID = Me.ApplicationID
    '        .BranchId = Me.sesBranchId.Replace("'", "")
    '    End With

    '    Try
    '        oPar = oController.getApplicationDetailTransaction(oPar)        
    '        gridRincianTransaksi.DataSource = oPar.ApplicationDetailTransactionDT
    '        gridRincianTransaksi.DataBind()
    '    Catch ex As Exception

    '    End Try
    'End Sub
    'Private Sub LoadDataDistribusiInsentif()
    '    Dim oCustomClass As New Parameter.RefundInsentif

    '    With oCustomClass
    '        .strConnection = GetConnectionString()
    '        .BranchId = Me.sesBranchId.Replace("'", "")
    '        .ApplicationID = Me.ApplicationID
    '    End With


    '    oCustomClass = oRefundInsentifController.getDistribusiNilaiInsentif(oCustomClass)        
    '    gridDistribusi.DataSource = oCustomClass.ListData.Tables(0)
    '    gridDistribusi.DataBind()

    'End Sub
    Public Sub showPencairan()
        Dim oClass As New Parameter.ApplicationDetailTransaction

        With oClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
        End With

        Try
            oClass = oController.getBungaNett(oClass)


            gvBungaNet.DataSource = oClass.Tabels.Tables(0)
            gvBungaNet.DataBind()

            gvPencairan.DataSource = oClass.Tabels.Tables(1)
            gvPencairan.DataBind()

            gvNetRateStatus.DataSource = oClass.Tabels.Tables(4)
            gvNetRateStatus.DataBind()


        Catch ex As Exception

        End Try

    End Sub
    Private Sub gvPencairan_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvPencairan.RowDataBound




        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblNilai As Label = CType(e.Row.Cells(1).FindControl("lblNilai"), Label)

            If e.Row.Cells(3).Text = "+" Then
                TotalPencairan += CDbl(lblNilai.Text)
            Else
                TotalPencairan -= CDbl(lblNilai.Text)
            End If


        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalPencairan As Label = CType(e.Row.Cells(1).FindControl("lblTotalPencairan"), Label)

            'lblTotalPencairan.Text = FormatNumber(Math.Round(TotalPencairan / 10, 0) * 10, 0)
            lblTotalPencairan.Text = FormatNumber(TotalPencairan, 0)


        End If
    End Sub
    Function getRateDetailTransaksi(TransAmount As Double) As Decimal
        Dim xrate As Decimal
        If cboFirstInstallment.Text = "AD" Then
            xrate = RateConvertion.GetEffectiveRateAdv(CInt(lblNumInst.Text), CDbl(lblInstAmt.Text), TransAmount, 1)
        Else
            xrate = RateConvertion.GetEffectiveRateArr(CInt(lblNumInst.Text), CDbl(lblInstAmt.Text), TransAmount, 1)
        End If

        Return Math.Round(Math.Abs(Me.EffectiveRate - xrate), 2)
    End Function
    Private Sub gvBungaNet_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBungaNet.RowDataBound


        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lblRate As Label = CType(e.Row.Cells(2).FindControl("lblRate"), Label)


            If gvBungaNet.DataKeys(e.Row.RowIndex).Item("TransID").ToString <> "EFF" Then

                Dim nilai As Double = CType(e.Row.Cells(1).Text, Double)

                lblRate.Text = getRateDetailTransaksi(CDbl(lblNTF.Text) + nilai).ToString
            End If



            If e.Row.Cells(3).Text = "+" Then
                TotalBungaNett += CDec(lblRate.Text)
            Else
                TotalBungaNett -= CDec(lblRate.Text)
            End If



        ElseIf e.Row.RowType = DataControlRowType.Footer Then

            Dim lblTotalRate As Label = CType(e.Row.Cells(2).FindControl("lblTotalRate"), Label)
            lblTotalRate.Text = Math.Round(TotalBungaNett, 2).ToString


        End If

    End Sub
    'Private Sub gridDistribusi_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridDistribusi.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        TotalInsGrid = TotalInsGrid + CDbl(CType(e.Item.FindControl("txtInsentif"), Label).Text)
    '        'TotalProsentase = TotalProsentase + CDbl(CType(e.Item.FindControl("lblProsentase"), Label).Text)

    '        CType(e.Item.FindControl("lblNo"), Label).Text = (1 + e.Item.ItemIndex).ToString
    '    End If

    '    If e.Item.ItemType = ListItemType.Footer Then
    '        CType(e.Item.FindControl("lblTotalInsentif"), Label).Text = FormatNumber(TotalInsGrid, 0)
    '        'CType(e.Item.FindControl("lblTotalProsentase"), Label).Text = FormatNumber(TotalProsentase, 0)
    '    End If

    'End Sub
    'Private Sub gridRincianTransaksi_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridRincianTransaksi.ItemDataBound
    '    If e.Item.ItemIndex >= 0 Then
    '        e.Item.Cells(10).Text = ucFlagTransaksi.cbo.Items.FindByValue(e.Item.Cells(10).Text.Trim).Text
    '        e.Item.Cells(11).Text = ucFlagTransaksi.cbo.Items.FindByValue(e.Item.Cells(11).Text.Trim).Text

    '        Dim LblTransID As Label
    '        Dim txtPersen As Label
    '        Dim txtTransAmount As Label

    '        LblTransID = CType(e.Item.FindControl("LblTransID"), Label)
    '        txtPersen = CType(e.Item.FindControl("txtPersen"), Label)
    '        txtTransAmount = CType(e.Item.FindControl("txtTransAmount"), Label)

    '        If Not LblTransID.Text.Substring(0, 2).Trim = "TP" Then
    '            txtPersen.Visible = False
    '        Else
    '            txtPersen.Text = FormatNumber(CStr(CDbl(txtTransAmount.Text.Replace(",", "")) / CDbl(lblPremiAsuransiGross.Text.Replace(",", "")) * 100))
    '        End If
    '    End If
    'End Sub    

#End Region
    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    If Me.Style.Trim = "ViewApplication" Then
    '        Response.Redirect("CreditProcess/NewApplication/ViewApplication.aspx?ApplicationID=" & Me.ApplicationID.Trim & "&AgreementNo=" & Me.AgreementNo & "&CustomerName=" & Me.CustomerName & "&Style=ViewApplication&CustomerID=" & Me.CustomerID)
    '    Else
    '        Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    '    End If

    'End Sub

End Class