﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class ViewCustomerExposure
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ViewCustExpo"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.CustomerID = Request("CustomerID").ToString
                Me.CustomerName = Request("CustomerName").ToString
                lblCustName.Text = Me.CustomerName
                Me.Style = Request("Style").ToString
                Bindgrid()
                lblMessage.Text = ""
            End If
        End If
    End Sub
    Sub Bindgrid()

        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Dim objReader As SqlDataReader
        'Dim objReader1 As SqlDataReader
        '=============================================
        'Untuk Detail Customer Exposure
        '=============================================

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewCustomerExposure"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@CustomerId", SqlDbType.VarChar, 20).Value = Me.CustomerID.Trim

            objReader = objCommand.ExecuteReader()

            If objReader.Read Then
                'OutstandingAmount
                lblOSBalance.Text = FormatNumber(objReader.Item("OutstandingBalanceAR").ToString.Trim, 0)
                lblOSPrincipal.Text = FormatNumber(objReader.Item("OutstandingPrincipal").ToString.Trim, 0)
                lblOSInterest.Text = FormatNumber(objReader.Item("OutstandingInterest").ToString.Trim, 0)
                'Max Of
                LblMaxOfOSBalance.Text = FormatNumber(objReader.Item("maxOutStandingBalance").ToString.Trim, 0)
                LblMaxOfOverDueDays.Text = FormatNumber(objReader.Item("maxOverDuedays").ToString.Trim, 0)
                LblInstallmentAmount.Text = FormatNumber(objReader.Item("MaxInstallmentAmount").ToString.Trim, 0)
                LblOverdueAmount.Text = FormatNumber(objReader.Item("maxOverDueAmount").ToString.Trim, 0)
                'Number Of
                lblActiveAgreement.Text = FormatNumber(objReader.Item("NumofActiveAgreement").ToString.Trim, 0)
                lblAssetRepossessed.Text = FormatNumber(objReader.Item("NumOfAssetRepossed").ToString.Trim, 0)
                lblInProcessAgreement.Text = FormatNumber(objReader.Item("NumofInProcessAgreement").ToString.Trim, 0)
                lblAssetInventoried.Text = FormatNumber(objReader.Item("NumOfAssetInventoried").ToString.Trim, 0)
                lblAssetInFinancing.Text = FormatNumber(objReader.Item("NumOfAssetInFinancing").ToString.Trim, 0)
                lblWrittenOffAgreement.Text = FormatNumber(objReader.Item("NumOfWrittenOffAgreement").ToString.Trim, 0)
                LblRejectedAgreement.Text = FormatNumber(objReader.Item("NumOfRejectedAgreement").ToString.Trim, 0)
                LblNonAccrualAgreement.Text = FormatNumber(objReader.Item("NumOfNonAccrualAgreement").ToString.Trim, 0)
                LblCancelledAgreement.Text = FormatNumber(objReader.Item("NumOfCancelledAgreement").ToString.Trim, 0)
                LblBounceCheque.Text = FormatNumber(objReader.Item("NumOfBounceCheque").ToString.Trim, 0)
                'Bucket
                lblBucket1text.Text = objReader.Item("Bucket1_text").ToString.Trim
                lblBucket2text.Text = objReader.Item("Bucket2_text").ToString.Trim
                lblBucket3text.Text = objReader.Item("Bucket3_text").ToString.Trim
                lblBucket4text.Text = objReader.Item("Bucket4_text").ToString.Trim
                lblBucket5text.Text = objReader.Item("Bucket5_text").ToString.Trim
                lblBucket6Text.Text = objReader.Item("Bucket6_text").ToString.Trim
                lblBucket7text.Text = objReader.Item("Bucket7_text").ToString.Trim
                lblBucket8text.Text = objReader.Item("Bucket8_text").ToString.Trim
                lblBucket9text.Text = objReader.Item("Bucket9_text").ToString.Trim
                lblBucket10text.Text = objReader.Item("Bucket10_text").ToString.Trim
                lblBucket1.Text = FormatNumber(objReader.Item("Bucket1_gross").ToString.Trim, 0)
                lblBucket2.Text = FormatNumber(objReader.Item("Bucket2_gross").ToString.Trim, 0)
                lblBucket3.Text = FormatNumber(objReader.Item("Bucket3_gross").ToString.Trim, 0)
                lblBucket4.Text = FormatNumber(objReader.Item("Bucket4_gross").ToString.Trim, 0)
                lblBucket5.Text = FormatNumber(objReader.Item("Bucket5_gross").ToString.Trim, 0)
                lblBucket6.Text = FormatNumber(objReader.Item("Bucket6_gross").ToString.Trim, 0)
                lblBucket7.Text = FormatNumber(objReader.Item("Bucket7_gross").ToString.Trim, 0)
                lblBucket8.Text = FormatNumber(objReader.Item("Bucket8_gross").ToString.Trim, 0)
                lblBucket9.Text = FormatNumber(objReader.Item("Bucket9_gross").ToString.Trim, 0)
                lblBucket10.Text = FormatNumber(objReader.Item("Bucket10_gross").ToString.Trim, 0)
                lblTotalAllBucket.Text = FormatNumber(objReader.Item("TotalAllBucket").ToString.Trim, 0)
            End If
            objReader.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
            objReader.Close()
        End Try
    End Sub
    Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Response.Redirect("ViewPersonalCustomer.aspx?CustomerId=" & Me.CustomerID & "&CustomerName=" & Me.CustomerName & "&Style=" & Me.Style)
    End Sub
End Class