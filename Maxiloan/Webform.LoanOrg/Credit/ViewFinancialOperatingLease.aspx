﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewFinancialOperatingLease.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewFinancialOperatingLease" %>

<%@ Register Src="../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication" TagPrefix="uc3" %>
<%@ Register Src="../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail" TagPrefix="uc2" %>
<%@ Register Src="../../Webform.UserController/ucFlagTransaksi.ascx" TagName="ucFlagTransaksi" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>View Financial Data</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Lookup.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <style>
       .form_left, .form_right { width: 47.9% !important; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - FINANCIAL
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="title_strip">
        </div>
        <div>
            <div class="form_left">
                <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
            </div>
            <div class="form_right">
                <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
            </div>
        </div>
    </div>
   
    <div class="form_box_hide">
                <div>
                    <div class="form_left">
                        <label class="">
                            Angsuran Pertama
                        </label>
                        <asp:Label ID="lblFirstInstallment" runat="server"></asp:Label>
                    </div>  
                    <div class="form_right">
                        
                    </div>
                </div>
            </div>
            <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h5>
                            DATA FINANSIAL</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Asset Value
                        </label>
                        <asp:Label ID="lblOTR" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Basic Lease
                        </label>
                        <asp:Label runat="server" ID="lblBasicLease" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_box_hide">
                        <label>
                            Karoseri
                        </label 
                        <asp:Label runat="server" ID="lblKaroseri" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_left">
                         <label>
                            RV Estimate
                        </label>
                        <asp:Label runat="server" ID="lblRVEstimate" CssClass="numberAlign2 regular_text"></asp:Label> 
                    </div>
                    <div class="form_right" >
                        <label>
                            Insurance
                        </label>
                        <asp:Label ID="lblInsurance" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                         <label>
                            RV Interest
                        </label>
                        <asp:Label runat="server" ID="lblRVInterest" CssClass="numberAlign2 regular_text"></asp:Label> 
                    </div>
                </div>
                <div class="form_right" >
                        <label>
                            Maintenance
                        </label>
                        <asp:Label runat="server" ID="ucMaintenance" CssClass="numberAlign2 regular_text"></asp:Label> 
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            PA
                        </label>
                        <asp:Label runat="server" ID="lblPA" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right" >
                        <label>
                            STNK
                        </label>
                        <asp:Label runat="server" ID="ucSTNK" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_box_hide">
                        <label class="label_auto">
                            Uang Muka
                        </label>
                        <asp:Label runat="server" ID="txtDP" CssClass="numberAlign2 regular_text"></asp:Label>
                        <label class="label_auto numberAlign2"> %
                        </label>
                        <asp:Label runat="server" ID="txtDPPersen" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                <div class="form_left" >
                </div>
                <div class="form_right" >
                        <label>
                            Admin Fee
                        </label>
                        <asp:Label runat="server" ID="ucAdminFee" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
            </div>
            <div class="form_box">
                <div class="form_left border_sum">
                    <label class="label_calc2">
                        +
                    </label>
                    <label>
                        Asuransi Pembiayaan
                    </label>
                    <asp:Label ID="lblAssetInsurance3" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                </div>
                  <div class="form_right border_sum" >
                  <label class="label_calc2">
                        +
                        </label>
                        <label>
                            Provisi
                        </label>
                        <asp:Label runat="server" ID="ucProvisi" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_box_hide">
                        <label>
                            Pokok Hutang (NTF)
                        </label>
                        <asp:Label ID="lblNTF" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_left">
                        <label class="label_req">Suku Bunga</label>
                        <label class="label_auto numberAlign2"> %
                        </label>
                        <asp:Label runat="server" ID="txtSukuBunga" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right" >
                        <label>
                            Total Monthly Lease
                        </label>
                        <asp:Label ID="lblMonthlyLease" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left" >
                        <label>
                            Expected Interest
                        </label>
                        <asp:Label ID="lblExpectedInterest" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
             </div>            
            <div class="form_box">
                <div>               
                     <div class="form_left">
                        <label class="label_req">
                        Jangka Waktu
                        </label>
                        <asp:Label ID="txtNumInst" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                     </div>
                </div>
            </div>
    
   <div class="form_box_header">
                <div>
                    <div class="form_left">
                        <h5>
                            REFUND BUNGA</h5>
                        <div style="display:none">
                        <asp:textbox ID="lblNilaiNTFOTR" runat="server" />
                        <asp:textbox ID="lblNilaiTenor" runat="server" />
                        <asp:textbox ID="lblNilaiRefundBunga" runat="server" />
                        <asp:TextBox ID="txtFlatRate" runat="server" />
                        <asp:TextBox ID="lblNilaiTotalBunga" runat="server" />
                        </div>
                    </div>
                    <div class="form_right">
                        <h5>
                            PREMI ASURANSI</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Total Bunga
                        </label>
                        <asp:Label ID="lblTotalBunga" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Premi Asuransi
                        </label>
                        <asp:Label runat="server" ID="lblPremiAsuransiGross" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>
                            Refund Bunga
                        </label>
                        <asp:Label runat="server" ID="txtRefundBunga" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>
                            Premi Asuransi Maskapai Nett
                        </label>
                        <asp:Label runat="server" ID="lblPremiAsuransiNet" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#f9f9f9">
                        <h5>SUBSIDI DEALER</h5>
                    </div>
                    <div class="form_right">
                        <label>Diskon Premi</label>
                        <asp:Label runat="server" ID="lblSelisihPremi" CssClass="numberAlign2 regular_text" style="font-weight:bold"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Bunga Dealer</label>
                        <asp:Label runat="server" ID="txtSubsidiBungaDealer" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right"  style="background-color:#FCFCCC">
                        <label>Refund Premi</label>
                        <asp:Label runat="server" ID="txtRefundPremi" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Angsuran Dealer</label>
                        <asp:Label runat="server" ID="txtSubsidiAngsuran" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                     <label><b>Pendapatan / Pengeluaran Asuransi</b></label>
                     <asp:Label runat="server" ID="lblPendapatanPremi" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Uang Muka Dealer</label>
                        <asp:Label runat="server" ID="txtSubsidiUangMuka" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <h5>BIAYA LAINNYA</h5>
                        
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Dealer Discount</label>
                        <asp:Label runat="server" ID="txtDealerDiscount" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>

                    <div class="form_right">
                        <label>
                        Biaya Lainnya
                        </label>
                        <asp:Label ID="lblOtherFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Biaya Maintenance</label>
                        <asp:Label runat="server" ID="txtBiayaMaintenance" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Refund Biaya Lainnya</label>
                        <asp:Label runat="server" ID="txtRefundLain" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left" style="background-color:#f9f9f9">
                        <h5>SUBSIDI ATPM</h5>
                    </div>

                    <div class="form_right">
                        <h5>BIAYA PROVISI</h5>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <div style="">
                            <label class="">
                                Supplier/Dealer ATPM</label>                            
                            <asp:Label ID="txtSupplierName" runat="server" ></asp:Label>
                        </div>
                    </div>
                    <div class="form_right">
                        <label>Biaya Provisi</label>
                        <asp:Label ID="lblBiayaProvisi" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Bunga ATPM</label>
                        <asp:Label ID="txtSubsidiBungaATPM" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label>Refund Biaya Provisi</label>
                        <asp:Label ID="txtRefundBiayaProvisi" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Angsuran ATPM</label>
                        <asp:Label ID="txtSubsidiAngsuranATPM" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Uang Muka ATPM</label>
                        <asp:Label ID="txtSubsidiUangMukaATPM" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                    </div>
                </div>
            </div>

            
                <div class="form_box_title">
                    <div class="form_left">
                        <h5>
                            PERHITUNGAN BUNGA NET</h5>
                    </div>
                    <div class="form_right">
                        <h5>
                            TOTAL PENCAIRAN</h5>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_left">
                        <asp:GridView runat="server" ID="gvBungaNet" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc1" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Bunga Nett</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Nilai" DataFormatString="{0:N0}" HeaderStyle-CssClass="th_right"
                                    ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft" />
                                <asp:TemplateField  HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblRate" Text='<%# math.round(Container.DataItem("Rate"),2) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalRate"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="BungaNett" />
                            </Columns>
                        </asp:GridView>
                        <asp:GridView runat="server" ID="gvNetRateStatus" AllowPaging="false" AutoGenerateColumns="false"
                            CssClass="grid_general" ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <label>
                                            Status Rate</label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="RateTypeResult" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="form_right">
                        <asp:GridView runat="server" ID="gvPencairan" AllowPaging="false" AutoGenerateColumns="false"
                            DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                            ShowHeader="false">
                            <HeaderStyle CssClass="th" />
                            <RowStyle CssClass="item_grid" />
                            <FooterStyle CssClass="item_grid ft" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTransDesc2" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <label class="label_auto">
                                            Total Pencairan</label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                                    FooterStyle-CssClass="item_grid_right ft">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblNilai" Text='<%# formatnumber(Container.DataItem("Nilai"),0) %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="lblTotalPencairan"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FlagCustomer" Visible="false" />
                                <asp:BoundField DataField="FlagSupplier" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            
   <%-- <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
