﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ViewAmortization

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''lblCustomerName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCustomerName As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblAgreementNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAgreementNo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSPrincipal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSPrincipal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSInterest control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInterest As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSPrincipalUndue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSPrincipalUndue As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSInterestUnDue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInterestUnDue As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSInstallmentDue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInstallmentDue As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSInsuranceDue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInsuranceDue As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLCInstallment control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLCInstallment As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSLCInsurance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSLCInsurance As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSInstallCollectionFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInstallCollectionFee As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSInsuranceCollFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInsuranceCollFee As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSPDCBounceFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSPDCBounceFee As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSSTNKFee control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSSTNKFee As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSInsuranceClaimExpense control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSInsuranceClaimExpense As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblOSCollExpense control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOSCollExpense As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblContactPrepaidAmt control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblContactPrepaidAmt As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotalAmountToBePaid control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalAmountToBePaid As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNextInstallmentNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNextInstallmentNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNextInstallmentDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNextInstallmentDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNextInstallmentDueNumber control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNextInstallmentDueNumber As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblNextInstallmentDueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNextInstallmentDueDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dtg control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtg As Global.System.Web.UI.WebControls.DataGrid
End Class
