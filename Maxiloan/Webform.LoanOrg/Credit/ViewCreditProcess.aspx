﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCreditProcess.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewCreditProcess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewCreditProcess</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=15, top=10, width=985, height=600, menubar=0, scrollbars=yes');
        }
		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - PROSES PEMBIAYAAN
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:LinkButton ID="lblCustomerID" runat="server" EnableViewState="False"></asp:LinkButton>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <span id="lblAgrNumber">
                    <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
                </span>
            </div>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                PROSES PEMBIAYAAN
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tanggal Aplikasi baru
                </label>
                <asp:Label ID="lblApplicationDaTE" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Sumber Aplikasi
                </label>
                <span id="Span1">
                    <asp:Label ID="lblSourceApplication" runat="server" EnableViewState="False"></asp:Label>
                </span>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tgl Permintaan Survey
                </label>
                <asp:Label ID="lblReqSurveyDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Scoring Pembiayaan
                </label>
                <span id="lblScoring2">
                    <asp:Label ID="lblCreditBy" runat="server" EnableViewState="False"></asp:Label>
                </span>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tanggal Survey
                </label>
                <asp:Label ID="lblSurveyDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal CAR
                </label>
                <asp:Label ID="lblRCADate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tanggal Approval
                </label>
                <asp:Label ID="lblApprovalDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Approval
                </label>
                <asp:Label ID="lblApprovalNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tanggal Kontrak
                </label>
                <asp:Label ID="lblAgreementDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Purchase Order
                </label>
                <asp:Label ID="lblPODate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tanggal Verifikasi Pra Pencairan
                </label>
                <asp:Label ID="lblDODate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Invoice Supplier
                </label>
                <asp:Label ID="lblSupplierDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tanggal Loan Activation
                </label>
                <asp:Label ID="lblGoLiveDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Surveyor
                </label>
                <asp:Label ID="lblSurveyor" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    CMO
                </label>
                <asp:Label ID="lblAO" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Pembiayaan Analyst
                </label>
                <asp:Label ID="lblCA" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <%--<div class="form_box_title">
        <div class="form_single">
            <h4>
                BIAYA-BIAYA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Biaya Admin
                </label>
                <asp:Label ID="lblAdminFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Survey
                </label>
                <span id="lblCreditSrc">
                    <asp:Label ID="lblSurveyFee" runat="server" EnableViewState="False"></asp:Label>
                </span>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Biaya Fiducia
                </label>
                <asp:Label ID="lblFiduciaFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya BBN
                </label>
                <asp:Label ID="lblBBNFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Biaya Provisi
                </label>
                <asp:Label ID="lblProvisionFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Lainnya
                </label>
                <asp:Label ID="lblOtherFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Biaya Notaris
                </label>
                <asp:Label ID="lblNotaryFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>--%>
    <%--<div class="form_box_title">
        <div class="form_single">
            <h4>
                DATA LAINNYA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Ex Konversi
                </label>
                <span id="Span2">
                    <asp:Label ID="lblExConversion" runat="server" EnableViewState="False"></asp:Label>
                </span>
            </div>
            <div class="form_right">
                <label>
                    Periode Floating
                </label>
                <asp:Label ID="lblFloatingPeriod" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Penjamin
                </label>
                <span id="Span3">
                    <asp:Label ID="lblAvalist" runat="server" EnableViewState="False"></asp:Label>
                </span>
            </div>
            <div class="form_right">
                <label>
                    Tgl Review berkut Floating
                </label>
                <asp:Label ID="lblFloatingReviewDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Sindikasi
                </label>
                <span id="lblOthExConv">
                    <asp:Label ID="lblSyndication" runat="server" EnableViewState="False"></asp:Label>
                </span>
            </div>
            <div class="form_right">
                <label>
                    Adjustment Floating Terakhir
                </label>
                <asp:Label ID="lblLastFloatAdj" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Tgl Buy Back Guarantee
                </label>
                <asp:Label ID="lblBuyBackValidDate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Eksekusi Floating Terakhir
                </label>
                <asp:Label ID="lblLastFloatEx" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Transaksi Non Standard
                </label>
                <asp:Label ID="LblNST" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>--%>
    <div class="form_box">
        <div class="form_single">
            <label>
                CATATAN</label>
            <asp:Label ID="lblNotes" runat="server" EnableViewState="False"></asp:Label>
        </div>
    </div>
    <%--<div class="form_title">
        <div class="form_single">
            <h4>
                KONTRAK LAIN DIMILIKI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgCrossDefault" runat="server" EnableViewState="False" Width="95%"
                CssClass="grid_general" CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server" EnableViewState="False"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="AGREEMENTNO" HeaderText="NO KONTRAK"></asp:BoundColumn>
                    <asp:BoundColumn DataField="NAME" HeaderText="NAMA"></asp:BoundColumn>
                    <asp:BoundColumn DataField="AGREEMENTDATE" HeaderText="TGL KONTRAK"></asp:BoundColumn>
                    <asp:BoundColumn DataField="DEFAULTSTATUS" HeaderText="STATUS DEFAULT"></asp:BoundColumn>
                    <asp:BoundColumn DataField="CONTRACTSTATUS" HeaderText="STATUS KONTRAK"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>--%>
    <asp:Panel ID="pnlHistory" runat="server" Visible="false">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    APPROVAL HISTORY
                </h4>
            </div>
        </div>
    </asp:Panel>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgHistory" runat="server" EnableViewState="False" Width="100%"
                CssClass="grid_general" CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn HeaderText="NO">
                        <HeaderStyle Width="5%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <%--<asp:BoundColumn DataField="UserApproval" HeaderText="NAMA">--%>
                    <asp:BoundColumn DataField="UserApprovalName" HeaderText="NAMA">
                        <HeaderStyle Width="20%"></HeaderStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ApprovalDate" HeaderText="TGL APPROVED">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="WOA" HeaderText="JENIS">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="Duration" HeaderText="LAMA">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
<%--                    <asp:BoundColumn DataField="SecurityCode" HeaderText="SECURITY CODE">
                        <HeaderStyle Width="15%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>--%>
                    <asp:BoundColumn DataField="IsFinal" HeaderText="IS FINAL">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="ApprovalNote" HeaderText="CATATAN APPROVAL">
                        <HeaderStyle Width="30%"></HeaderStyle>
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    <%--<div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
