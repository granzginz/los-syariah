﻿#Region "Imports"
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports System.Globalization
#End Region

Public Class Customer
    Inherits Maxiloan.Webform.WebBased


    Protected WithEvents UcCompAddress As UcCompanyAddress
    'Protected WithEvents UcCustomerLookUpGlobal1 As ucLookUpCustomerGlobal
    Protected WithEvents txtIDTglBerlakuNew As ucDateCE
    Protected WithEvents txtTglLahirNew As ucDateCE
    Protected WithEvents txtSrcTglLahirNew As ucDateCE

    Private m_controller As New CustomerController
    Private currentPage As Integer = 1
    Private pageSize As Integer = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Integer = 1

    Private m_controllerApp As New ApplicationController
    Private ocustomclass As New Parameter.Application

    Dim filterBranch As String
    Dim oCustomer As New Parameter.Customer
    Dim isKTPExist As Integer
    Protected WithEvents GridNavigator As ucGridNav

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

#End Region

#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation
        filterBranch = " vwCustomer.customerId like '" & Replace(Me.sesBranchId, "'", "") & "%' "


        If Not Me.IsPostBack Then
            'txtPage.Text = "1"
            cTabs.SetNavigateUrl(Request("page"), Request("id"))
            cTabs.RefreshAttr(Request("pnl"))

            If (Not Request("prospect") Is Nothing) Then
                prospectPersonal(Request("prospect"))
                Return
            End If

            If CheckForm(Me.Loginid, "Customer", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If

            If IsSingleBranch() And Me.IsHoBranch = False Then
                Me.Sort = "Name ASC"

                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = filterBranch
                End If

                BindGridEntity(Me.CmdWhere)

                Me.SortBy = "Name ASC"

                txtIDTglBerlakuNew.IsRequired = True
                txtTglLahirNew.IsRequired = True
                UcCompAddress.IsRequiredZipcode = True
                txtSrcTglLahirNew.IsRequired = False
            Else
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            End If
        End If
    End Sub
#End Region

    Private Sub setBranchName()
        If ViewState("branchName") Is Nothing OrElse ViewState("branchName").ToString = String.Empty Then
            Dim con As New BranchController
            Dim data As New Parameter.Branch
            With data
                .strConnection = GetConnectionString()
                .BranchId = Replace(sesBranchId, "'", "")
            End With
            data = con.BranchList(data)
            branchName = data.BranchName
        Else
            branchName = ViewState("branchName").ToString

        End If

    End Sub

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddCompany.Visible = False
        pnlAddPersonal.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region "BindGridEntity"
    Sub BindGridEntity(ByVal cmdWhere As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomClass As New Parameter.Customer
        InitialDefaultPanel()


        oCustomClass.PageSize = CType(pageSize, Int16)
        oCustomClass.WhereCond = cmdWhere
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetCustomer(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = CType(oCustomClass.totalrecords, Integer)
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        'PagingFooter()


        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)

        'If txtSearch.Text.Trim <> "" Then
        '    Dim Search As String
        '    If cboSearch.SelectedIndex = 0 Then
        '        Search = Replace(txtSearch.Text.Trim, "'", "''")
        '    Else
        '        Search = txtSearch.Text.Trim
        '    End If
        '    Me.CmdWhere = cboSearch.SelectedItem.Value + " LIKE '%" + Search + "%'"
        '    Me.CmdWhere &= " and " & filterBranch
        'Else
        '    Me.CmdWhere = filterBranch 
        'End If 

        currentPage = e.CurrentPage
        BindGridEntity(Me.CmdWhere, True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub


#End Region

#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Edit" Then
            If e.Item.Cells(3).Text.Trim = "P" Then
                If CheckFeature(Me.Loginid, "Customer", "AddP", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Response.Redirect("CustomerPersonal.aspx?page=Edit&id=" & e.Item.Cells(5).Text.Trim & "")
            Else
                If CheckFeature(Me.Loginid, "Customer", "AddC", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Response.Redirect("CustomerCompany.aspx?page=Edit&id=" & e.Item.Cells(5).Text.Trim & "")
            End If
        ElseIf e.CommandName = "EditPersonal" Then
            Response.Redirect("KoreksiIdentitasCustomer.aspx?id=" & e.Item.Cells(5).Text.Trim & "")
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = filterBranch ' "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "Search"
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Me.CmdWhere = filterBranch

        If Not (txtSearch.Text.Trim = String.Empty) Then
            Me.CmdWhere = String.Format("{0} and {1} LIKE '%{2}%' ", Me.CmdWhere, cboSearch.SelectedItem.Value, Replace(txtSearch.Text.Trim, "'", "''"))
        End If
         
        If Not (srcTxtIDTypeP.Text.Trim = String.Empty) Then
            Me.CmdWhere = String.Format("{0} and vwCustomer.idNumber like '%{1}%'", Me.CmdWhere, srcTxtIDTypeP.Text.Trim)
        End If
        If Not (txtSrcPMotherName.Text.Trim = String.Empty) Then
            Me.CmdWhere = String.Format("{0} and PersonalCustomer.MotherName like '%{1}%'", Me.CmdWhere, txtSrcPMotherName.Text.Trim)
        End If
        If Not (txtSrcBirthPlaceP.Text.Trim = String.Empty) Then
            Me.CmdWhere = String.Format("{0} and PersonalCustomer.BirthPlace like '%{1}%'", Me.CmdWhere, txtSrcBirthPlaceP.Text.Trim)
        End If
        If Not (txtSrcTglLahirNew.Text.Trim = String.Empty) Then
            Me.CmdWhere = String.Format("{0} and PersonalCustomer.Birthdate ='{1}'", Me.CmdWhere, ConvertDate2(txtSrcTglLahirNew.Text.Trim).ToString("yyyyMMdd"))
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
#End Region
#Region "AddPersonal"

    Sub AddPersonal()
        If CheckFeature(Me.Loginid, "Customer", "AddP", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        pnlList.Visible = False
        pnlAddCompany.Visible = False
        pnlAddPersonal.Visible = True

        ScriptManager.RegisterStartupScript(cboPMarital, GetType(Page), cboPMarital.ClientID, String.Format("cboPMarital_IndexChanged('{0}');", cboPMarital.SelectedValue.Trim), True)

        fillCboIDType(cboIDTypeP)
        FillCbo()        
        txtNameP.Text = ""
        rboTypeP.SelectedIndex = 0
        cboIDTypeP.SelectedIndex = 0
        txtIDNumberP.Text = ""
        txtBirthPlaceP.Text = ""
        txtTglLahirNew.Text = ""
        txtGelar.Text = ""
        txtGelarBelakang.Text = ""
        txtPMotherName.Text = ""
        txtPNoKK.Text = ""

        Me.ProspectAppID = "-"

        'fillFormTesting()
    End Sub


    Sub prospectPersonal(prospectId As String)
        ViewState("PROSPECTID") = prospectId
        AddPersonal()
        Dim oRow As DataRow
         
        oCustomer = m_controller.GetCustomerEdit( New Parameter.Customer With {.CustomerID = prospectId, .strConnection = GetConnectionString(), .CustomerType = "PP"}, "-1")

        If Not oCustomer Is Nothing Then
            oRow = oCustomer.listdata.Rows(0)
        End If


        txtNameP.Text = oRow("Name").ToString.Trim
        cboIDTypeP.SelectedIndex = cboIDTypeP.Items.IndexOf(cboIDTypeP.Items.FindByValue(oRow("IDType").ToString.Trim))
        cboGenderP.SelectedIndex = cboGenderP.Items.IndexOf(cboGenderP.Items.FindByValue(oRow("Gender").ToString.Trim))
        txtIDNumberP.Text = oRow("IDNumber").ToString.Trim 
        txtBirthPlaceP.Text = oRow("BirthPlace").ToString.Trim
        txtTglLahirNew.Text = Format(oRow("BirthDate"), "dd/MM/yyyy")
        'modify nofi 20Feb2018
        'txtIDTglBerlakuNew.Text = Format(Date.Now, "dd/MM/yyyy")
        txtIDTglBerlakuNew.Text = "31/12/9999"

        'modify by amri 02/02/2018
        txtPMotherName.Text = oRow("MotherName").ToString.Trim
        cboPMarital.SelectedIndex = cboPMarital.Items.IndexOf(cboPMarital.Items.FindByValue(oRow("MaritalStatus").ToString.Trim))

        BranchID = oRow("BranchID").ToString.Trim
        ProspectAppID = oRow("ProspectAppID").ToString.Trim
    End Sub
#End Region

#Region "Bind Combo"
    Sub fillCboIDType(ddl As DropDownList)
        Dim oCustomer As New Parameter.Customer
        Dim dtList As DataTable
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.CustomerPersonalGetIDType(oCustomer)
        dtList = oCustomer.listdata
        ddl.DataSource = dtList
        ddl.DataTextField = "Description"
        ddl.DataValueField = "ID"
        ddl.DataBind()
        ddl.Items.Insert(0, "Select One")
        ddl.Items(0).Value = "Select One"
    End Sub
    Sub FillCbo()
        Dim dtEntity As DataTable
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblMaritalStatus"
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        cboPMarital.DataSource = dtEntity.DefaultView
        cboPMarital.DataTextField = "Description"
        cboPMarital.DataValueField = "ID"
        cboPMarital.DataBind()
        cboPMarital.Items.Insert(0, "Select One")
        cboPMarital.Items(0).Value = ""
    End Sub
#End Region
#Region "imbNextP_Click"
    Private Sub btnNextP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNextP.Click
        Dim gStrPath, gStrFileName As String
        Dim lBlnSuccess As Boolean = True
        Try
            Dim tglLahir As Date = ConvertDate2(txtTglLahirNew.Text)
            If tglLahir >= Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Lahir harus lebih Kecil dari hari ini", True)
                Exit Sub
            End If
            Dim age As Integer
            age = Today.Year - tglLahir.Year
            If age < 17 Then
                ShowMessage(lblMessage, "Tanggal Lahir harus usia minimal 17 tahun.", True)
                Exit Sub
            End If

            'CheckKTP()
            'If isKTPExist > 0 Then            
            'ShowMessage(lblMessage, "Customer dengan ID Nomor tersebut sudah terdaftar", True)
            'Exit Sub
            'End If

            'condition 17 tahun
            'Dim GScont As New DataUserControlController
            'Dim GSdata As New Parameter.GeneralSetting
            'GSdata.GSID = "AGEMIN"
            'Dim jmlTahun As Double = Math.Round(Me.BusinessDate.Subtract(tglLahir).TotalDays / 365, 0)
            'Dim minAge As String = GScont.GetGeneralSetting(GetConnectionString, GSdata)

            'GSdata.GSID = "AGEMAX"
            'Dim maxAge As String = GScont.GetGeneralSetting(GetConnectionString, GSdata)
            'If jmlTahun > CDbl(maxAge) Or jmlTahun < CDbl(minAge) Then
            '    ShowMessage(lblMessage, "Umur harus antara " & minAge & " dan " & maxAge, True)
            '    Exit Sub
            'End If

            If ConvertDate2(txtIDTglBerlakuNew.Text) <= Me.BusinessDate Then
                ShowMessage(lblMessage, "Identitas sudah tidak berlaku!", True)
                Exit Sub
            End If



            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

            If File.Exists(gStrPath & gStrFileName) Then
                File.Delete(gStrPath & gStrFileName)
            End If

            m_controller.Generate_XMLP(gStrPath & gStrFileName, txtGelar.Text.Trim, txtNameP.Text.Trim, txtGelarBelakang.Text.Trim, rboTypeP.SelectedValue,
            cboIDTypeP.SelectedValue, cboIDTypeP.SelectedItem.Text, txtIDNumberP.Text.Trim, txtIDTglBerlakuNew.Text.Trim, cboGenderP.SelectedValue,
            txtBirthPlaceP.Text.Trim, txtTglLahirNew.Text.Trim, lBlnSuccess,
            txtPMotherName.Text, txtPNoKK.Text, txtNamaPasangan.Text, cboPMarital.SelectedValue)

            If lBlnSuccess Then
                Response.Redirect("Customer_002.aspx?pc=p&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region

    Sub AddCompany()
        If CheckFeature(Me.Loginid, "Customer", "AddC", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If

        UcCompAddress.Style = "accacq"
        UcCompAddress.BindAddress()
        UcCompAddress.showMandatoryAll()
        pnlList.Visible = False
        pnlAddCompany.Visible = True
        pnlAddPersonal.Visible = False
        Me.ProspectAppID = "-"

        'fillFormTesting2()
    End Sub
    Private Sub btnNextC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNextc.Click
        Dim gStrPath, gStrFileName As String
        Dim lBlnSuccess As Boolean

        gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If

        m_controller.Generate_XMLC(gStrPath & gStrFileName, txtCName.Text.Trim, txtCNPWP.Text.Trim, UcCompAddress.Address, _
        UcCompAddress.RT, UcCompAddress.RW, UcCompAddress.Kelurahan, UcCompAddress.Kecamatan, _
        UcCompAddress.City, UcCompAddress.ZipCode, UcCompAddress.AreaPhone1, UcCompAddress.Phone1, _
        UcCompAddress.AreaPhone2, UcCompAddress.Phone2, UcCompAddress.AreaFax, UcCompAddress.Fax, _
        lBlnSuccess)

        If lBlnSuccess Then
            Response.Redirect("Customer_002.aspx?pc=c&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
        End If
    End Sub

    Private Sub btnCancelP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelP.Click
        If (Not ViewState("PROSPECTID") Is Nothing) Then
            Response.Redirect("CustomerFromProspect.aspx?page=Edit&id=" & CType(ViewState("PROSPECTID"), String) & "&pnl=tabProspect")
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub

    Private Sub btnCancelC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelC.Click
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            Dim imgEditPersonal As ImageButton
            lnkCustomer = CType(e.Item.FindControl("lnkCustomer"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','accacq');")
            imgEditPersonal = CType(e.Item.FindControl("imgEditPersonal"), ImageButton)

            If e.Item.Cells(3).Text.ToUpper.Trim = "C" Then
                imgEditPersonal.Visible = False
            End If

        End If
    End Sub

    'Private Sub btnAddPersonal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPersonal.Click
    '    AddPersonal()
    'End Sub

    Private Sub imbAddCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCompany.Click
        AddCompany()
    End Sub

    Public Sub fillFormTesting()
        txtNameP.Text = Now.ToString("ssmmhhddMMyyyyss")
        cboIDTypeP.SelectedIndex = 2
        txtIDNumberP.Text = Now.ToString("ssmmhhddMMyyyyss")
        txtIDTglBerlakuNew.Text = "18/01/2014"
        txtBirthPlaceP.Text = "JAKARTA"
        txtTglLahirNew.Text = "01/01/1981"
    End Sub

    Public Sub fillFormTesting2()
        txtCName.Text = "PT " & Now.ToString("ssmmhhddMMyyyyss")
        txtCNPWP.Text = Now.ToString("ssmmhhddMMyyyyss")

        With UcCompAddress
            .Address = "JAKARTA"
            .RT = "00"
            .RW = "00"
            .Kelurahan = "GROGOL UTARA"
            .Kecamatan = "GROGOL UTARA"
            .City = "JAKARTA"
            .ZipCode = "12210"
            .AreaPhone1 = "00"
            .Phone1 = "000"
            .AreaPhone2 = "1"
            .Phone2 = "2"
            .AreaFax = "3"
            .Fax = "4"
            .BindAddress()
        End With
    End Sub

    Function CheckKTP() As Integer
        Dim oCustomClass As New Parameter.Customer
        isKTPExist = 0
        With ocustomclass
            .strConnection = GetConnectionString()
            .IDNumber = txtIDNumberP.Text.Trim
        End With
        isKTPExist = m_controller.CheckPersonalCustomerDocument(oCustomClass)
        Return isKTPExist
    End Function
End Class

