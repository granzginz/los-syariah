﻿Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController


Public Class CustomerPersonalGuarantor
    Inherits AbsCustomerPersonal

    Protected WithEvents ucAddressSI As ucAddressCity
    Protected WithEvents ucAddressSI_KTP As ucAddressCity
    Protected WithEvents txtTglLahirNew As ucDateCE
    Protected WithEvents txtMasaBerlakuKTP As ucDateCE
    Protected WithEvents txtMasaKerja As ucNumberFormat




    Protected WithEvents ucAddressSI2 As ucAddressCity
    Protected WithEvents ucAddressSI_KTP2 As ucAddressCity
    Protected WithEvents txtTglLahirNew2 As ucDateCE
    Protected WithEvents txtMasaBerlakuKTP2 As ucDateCE
    Protected WithEvents txtMasaKerja2 As ucNumberFormat
    Private time As String
#Region "Constanta"
   

    Const txtHargaRumahText As String = "0"
    Const txtKeteranganUsahaText As String = ""
    Const txtOBTypeText As String = ""
    Const cboOBIndustryTypeValue As String = ""
    Const txtOBJobTitleText As String = ""
    Const txtOBSinceYearText As String = ""
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region


      
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")

            'Modify by Wira 20171010---
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------
            If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)
                Me.StatusPerkawinan = ocustomclass.MaritalStatus
            End If
            '--------------------------------------------------


            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"




            FillCbo_("tblJobType", cboJobTypeSI)
            FillCbo_("tblJobType", cboJobTypeSI2)

            'FillCbo("IndustryType")
            fillCboIDType()
            fillIndustryHeader()

            ucAddressSI.ValidatorFalse()
            ucAddressSI.Phone1ValidatorEnabled(False)
            ucAddressSI.DivHPHide()

            ucAddressSI2.ValidatorFalse()
            ucAddressSI2.DivHPHide()
            ucAddressSI2.Phone1ValidatorEnabled(False)

            ucAddressSI_KTP.SetHPValidator(False)
            ucAddressSI_KTP.ValidatorTrue()
            ucAddressSI_KTP.Phone1ValidatorEnabled(True)
            ucAddressSI_KTP.ValidatorTrue()

            ucAddressSI_KTP2.ValidatorFalse()
            ucAddressSI_KTP2.SetHPValidator(False)
            ucAddressSI_KTP2.Phone1ValidatorEnabled(False)


            lblPenjaminMessage.Visible = True
            pnlDataPasangan2.Visible = False


            Me.PageAddEdit = Request("page")
            If Me.PageAddEdit = "Add" Then
                lblTitle.Text = "ADD"
                'modify by Nofi date 6Feb2018 
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                'cTabs.SetNavigateUrl(Request("page"), Request("id"))
                GetXML()
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                cTabs.SetNavigateUrl(Request("page"), Request("id"))

            End If
            txtNamaPasangan.Text = Me.NamaPasangan

            If Me.StatusPerkawinan.ToUpper.Trim <> "M" Then
                cTabs.EnabledLink("tabPasangan", False)
                'btnCopyDataPasangan.Visible = False
                RequiredFieldValidator4.Enabled = False
                RequiredFieldValidator2.Enabled = False
                RequiredFieldValidator1.Enabled = False
            End If

            cTabs.RefreshAttr(Request("pnl"))
        End If
    End Sub

    'Private Sub btnAlamatKerja1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyDataPasangan.Click
    '    Dim dtEntity As New DataTable
    '    Dim oRow As DataRow
    '    Dim oCustomer As New Parameter.Customer

    '    oCustomer.CustomerID = Me.CustomerID
    '    oCustomer.CustomerType = "p"
    '    oCustomer.strConnection = GetConnectionString()
    '    oCustomer = m_controller.GetCustomerEdit(oCustomer, "6")

    '    If Not oCustomer Is Nothing Then
    '        dtEntity = oCustomer.listdata
    '    End If

    '    oRow = dtEntity.Rows(0)
    '    txtNamaPasangan.Text = oRow("SINamaSuamiIstri").ToString.Trim
    '    Me.NamaPasangan = oRow("SINamaSuamiIstri").ToString.Trim
    '    BindRow(oRow)
    'End Sub


#Region "imbPCancel_Click"
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
#End Region

#Region "Edit"
 

    Sub BindEdit(ByVal ID As String)
        Dim dtEntity As New DataTable 
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = ID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "65")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        Me.NamaPasangan = oRow("NamaGurantor").ToString.Trim
        txtNamaPasangan.Text = oRow("NamaGurantor").ToString.Trim
        Me.StatusPerkawinan = oRow("MaritalStatus").ToString.Trim
        BindRow(oRow)

        If (oRow("pgSeq").ToString.Trim = "0") Then
            lblPenjaminMessage.Visible = False
            pnlDataPasangan2.Visible = True
            txtTglLahirNew2.Text = "01/01/1900"
            txtMasaBerlakuKTP2.Text = "01/01/1900"
            If (dtEntity.Rows.Count = 2) Then
                'BindRow2(dtEntity.Rows(1))
            End If
        End If
    End Sub


    Sub BindRow(oRow As DataRow)

        Me.Name = oRow("Name").ToString.Trim
        Me.IDType = oRow("IDType").ToString.Trim
        Me.IDTypeName = oRow("IDTypeName").ToString.Trim
        Me.IDNumber = oRow("IDNumber").ToString.Trim
        Me.Gender = oRow("Gender").ToString.Trim

        If IsDate(oRow("IDExpiredDate").ToString) Then Me.IDExpiredDate = Format(oRow("IDExpiredDate"), "dd/MM/yyyy")

        If Not IsDBNull(oRow("BirthDate")) Then
            Me.BirthDate = Day(CDate(oRow("BirthDate"))).ToString + "/" + Month(CDate(oRow("BirthDate"))).ToString + "/" + Year(CDate(oRow("BirthDate"))).ToString
        Else
            Me.BirthDate = ""
        End If

        Me.BirthPlace = oRow("BirthPlace").ToString.Trim
        Me.Type = oRow("PersonalCustomerType").ToString.Trim

        Me.MotherName = oRow("MotherName").ToString.Trim

        Me.KartuKeluarga = oRow("NoKK").ToString.Trim
        Me.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim

        If oRow("SIJenisPekerjaanID").ToString <> "" Then
            cboJobTypeSI.SelectedIndex = cboJobTypeSI.Items.IndexOf(cboJobTypeSI.Items.FindByValue(oRow("SIJenisPekerjaanID").ToString))
        End If
        txtCompanyNameSI.Text = oRow("SINamaPers").ToString.Trim
        hdfKodeIndustriBU.Value = oRow("SIJenisIndustriID").ToString.Trim
        txtNamaIndustriBU.Text = oRow("SIJenisIndustriName").ToString.Trim
        txtKeteranganUsahaSI.Text = oRow("SIKeteranganUsaha").ToString.Trim
        With ucAddressSI
            .Address = oRow("SIAlamat").ToString.Trim
            .City = oRow("SIKota").ToString.Trim
            .ZipCode = oRow("SIKodePos").ToString.Trim
            .AreaPhone1 = oRow("SIAreaPhone1").ToString.Trim
            .Phone1 = oRow("SIPhone1").ToString.Trim
            .AreaPhone2 = oRow("SIAreaPhone2").ToString.Trim
            .Phone2 = oRow("SIPhone2").ToString.Trim
            .AreaFax = oRow("SIAreaFax").ToString.Trim
            .Fax = oRow("SIFax").ToString.Trim
            .BindAddress()
        End With


        With ucAddressSI_KTP
            .Address = oRow("SIKtpAlamat").ToString.Trim
            .City = oRow("SIKtpKota").ToString.Trim
            .ZipCode = oRow("SIKtpKodePos").ToString.Trim
            .AreaPhone1 = oRow("SIKtpAreaPhone1").ToString.Trim
            .Phone1 = oRow("SIKtpPhone1").ToString.Trim
            .AreaPhone2 = oRow("SIKtpAreaPhone2").ToString.Trim
            .Phone2 = oRow("SIKtpPhone2").ToString.Trim
            .AreaFax = oRow("SIKtpAreaFax").ToString.Trim
            .Fax = oRow("SIKtpFax").ToString.Trim
            .HP = oRow("KtpHP").ToString.Trim
            .BindAddress()
        End With
        txtBirthPlaceP.Text = oRow("TempatLahir").ToString.Trim
        Dim tgllhr = IIf(CType(oRow("TglLahir"), Date).Year < 1901, BusinessDate, oRow("TglLahir"))
        txtTglLahirNew.Text = Format(tgllhr, "dd/MM/yyyy")
        cboIDTypeP.SelectedIndex = cboIDTypeP.Items.IndexOf(cboIDTypeP.Items.FindByValue(oRow("JenisDokumen").ToString.Trim))
        txtNoDokument.Text = oRow("NoDokument").ToString.Trim
        Dim tglktp = IIf(CType(oRow("MasaBerlakuKTP"), Date).Year < 1901, BusinessDate, oRow("MasaBerlakuKTP"))
        txtMasaBerlakuKTP.Text = Format(oRow("MasaBerlakuKTP"), "dd/MM/yyyy")

        If (ConvertDate2(txtTglLahirNew.Text).Year <= 1901) Then txtTglLahirNew.Text = ""
        If (ConvertDate2(txtMasaBerlakuKTP.Text).Year <= 1901) Then txtMasaBerlakuKTP.Text = ""

        txtBerOperasiSejak.Text = oRow("BeroperasiSejak").ToString.Trim
        txtMasaKerja.Text = oRow("MasaKerja").ToString.Trim
        txtUsahaLainnya.Text = oRow("UsahaLainnya").ToString.Trim
        Try
            cboIndrustriHeader.SelectedValue = oRow("JenisIndustriHID").ToString.Trim
        Catch
        End Try

    End Sub


    'Sub BindRow2(oRow As DataRow)

    '    If oRow("SIJenisPekerjaanID").ToString <> "" Then
    '        cboJobTypeSI2.SelectedIndex = cboJobTypeSI2.Items.IndexOf(cboJobTypeSI2.Items.FindByValue(oRow("SIJenisPekerjaanID").ToString))
    '    End If
    '    txtCompanyNameSI2.Text = oRow("SINamaPers").ToString.Trim
    '    hdfKodeIndustriPS2.Value = oRow("SIJenisIndustriID").ToString.Trim
    '    txtNamaIndustriPS2.Text = oRow("SIJenisIndustriName").ToString.Trim
    '    txtKeteranganUsahaSI2.Text = oRow("SIKeteranganUsaha").ToString.Trim



    '    With ucAddressSI2
    '        .Address = oRow("SIAlamat").ToString.Trim
    '        .City = oRow("SIKota").ToString.Trim
    '        .ZipCode = oRow("SIKodePos").ToString.Trim
    '        .AreaPhone1 = oRow("SIAreaPhone1").ToString.Trim
    '        .Phone1 = oRow("SIPhone1").ToString.Trim
    '        .AreaPhone2 = oRow("SIAreaPhone2").ToString.Trim
    '        .Phone2 = oRow("SIPhone2").ToString.Trim
    '        .AreaFax = oRow("SIAreaFax").ToString.Trim
    '        .Fax = oRow("SIFax").ToString.Trim
    '        .BindAddress()
    '    End With


    '    With ucAddressSI_KTP2
    '        .Address = oRow("SIKtpAlamat").ToString.Trim
    '        .City = oRow("SIKtpKota").ToString.Trim
    '        .ZipCode = oRow("SIKtpKodePos").ToString.Trim
    '        .AreaPhone1 = oRow("SIKtpAreaPhone1").ToString.Trim
    '        .Phone1 = oRow("SIKtpPhone1").ToString.Trim
    '        .AreaPhone2 = oRow("SIKtpAreaPhone2").ToString.Trim
    '        .Phone2 = oRow("SIKtpPhone2").ToString.Trim
    '        .AreaFax = oRow("SIKtpAreaFax").ToString.Trim
    '        .Fax = oRow("SIKtpFax").ToString.Trim
    '        .HP = oRow("KtpHP").ToString.Trim
    '        .BindAddress()
    '    End With


    '    txtBirthPlaceP2.Text = oRow("TempatLahir").ToString.Trim

    '    txtTglLahirNew2.Text = Format(oRow("TglLahir"), "dd/MM/yyyy")


    '    cboIDTypeP2.SelectedIndex = cboIDTypeP2.Items.IndexOf(cboIDTypeP2.Items.FindByValue(oRow("JenisDokumen").ToString.Trim))
    '    txtNoDokument2.Text = oRow("NoDokument").ToString.Trim

    '    txtMasaBerlakuKTP2.Text = Format(oRow("MasaBerlakuKTP"), "dd/MM/yyyy")
    '    If (ConvertDate2(txtTglLahirNew2.Text).Year <= 1901) Then txtTglLahirNew2.Text = ""
    '    If (ConvertDate2(txtMasaBerlakuKTP2.Text).Year <= 1901) Then txtMasaBerlakuKTP2.Text = ""

    '    txtBerOperasiSejak2.Text = oRow("BeroperasiSejak").ToString.Trim
    '    txtMasaKerja2.Text = oRow("MasaKerja").ToString.Trim
    '    txtUsahaLainnya2.Text = oRow("UsahaLainnya").ToString.Trim
    '    cboIndrustriHeader2.SelectedValue = oRow("JenisIndustriHID").ToString.Trim
    'End Sub


#End Region


#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Try

            Dim paramPCSI As IList(Of Parameter.CustomerSI) = New List(Of Parameter.CustomerSI)
            Dim ocustomer As New Parameter.Customer
            Dim Err As String = ""

            'If (hdfKodeIndustriBU.Value.Trim = "") Then
            '    Err = "Silahkan pilih Bidang Usaha Detail."
            '    ShowMessage(lblMessage, Err, True)
            '    Exit Sub
            'End If
            paramPCSI.Add(New Parameter.CustomerSI With {
                          .Seq = 0,
                            .NamaSuamiIstri = txtNamaPasangan.Text.Trim,
                            .JenisPekerjaanID = cboJobTypeSI.SelectedValue.Trim,
                            .NamaPers = txtCompanyNameSI.Text.Trim,
                            .JenisIndustriID = hdfKodeIndustriBU.Value.Trim,
                            .KeteranganUsaha = txtKeteranganUsahaSI.Text.Trim,
                            .TempatLahirSI = txtBirthPlaceP.Text,
                            .NoDokumentSI = txtNoDokument.Text,
                            .TglLahirSI = If(txtTglLahirNew.Text.Trim = "", New Date(1900, 1, 1), ConvertDate2(txtTglLahirNew.Text)), 
                            .MasaBerlakuKTPSI = If(txtMasaBerlakuKTP.Text.Trim = "", New Date(1900, 1, 1), ConvertDate2(txtMasaBerlakuKTP.Text)),
                            .BeroperasiSejakSI = CShort(txtBerOperasiSejak.Text),
                            .MasaKerjaSI = CShort(txtMasaKerja.Text),
                            .UsahaLainnyaSI = txtUsahaLainnya.Text,
                            .JenisDokumenSI = cboIDTypeP.SelectedValue,
                            .AlamatKTP = New Parameter.Address With {
                                    .Address = ucAddressSI_KTP.Address.Trim,
                                    .RT = "-",
                                .RW = "-",
                                .Kelurahan = "-",
                                .Kecamatan = "-",
                                .City = ucAddressSI_KTP.City.Trim,
                                .ZipCode = ucAddressSI_KTP.ZipCode.Trim,
                                .AreaPhone1 = ucAddressSI_KTP.AreaPhone1.Trim,
                                .Phone1 = ucAddressSI_KTP.Phone1.Trim,
                                .AreaPhone2 = ucAddressSI_KTP.AreaPhone2.Trim,
                                .Phone2 = ucAddressSI_KTP.Phone2.Trim,
                                .AreaFax = ucAddressSI_KTP.AreaFax.Trim,
                                .Fax = ucAddressSI_KTP.Fax.Trim,
                                .MobilePhone = ucAddressSI_KTP.HP.Trim
                                },
            .PCSIAddress = New Parameter.Address With {
                .Address = ucAddressSI.Address.Trim,
                .RT = "-",
                .RW = "-",
                .Kelurahan = "-",
                .Kecamatan = "-",
                .City = ucAddressSI.City.Trim,
                .ZipCode = ucAddressSI.ZipCode.Trim,
                .AreaPhone1 = ucAddressSI.AreaPhone1.Trim,
                .Phone1 = ucAddressSI.Phone1.Trim,
                .AreaPhone2 = ucAddressSI.AreaPhone2.Trim,
                .Phone2 = ucAddressSI.Phone2.Trim,
                .AreaFax = ucAddressSI.AreaFax.Trim,
                .Fax = ucAddressSI.Fax.Trim
            }
                    })

            If (pnlDataPasangan2.Visible = True) Then
                'If (txtMasaBerlakuKTP2.Text.Trim = "") Then
                '    ShowMessage(lblMessage, "Masa Berlaku KTP Penjamin Ke-2 tidak boleh kosong", False)
                '    Return
                'End If 
                paramPCSI.Add(New Parameter.CustomerSI With {
                        .Seq = 1,
                          .NamaSuamiIstri = txtNamaPasangan2.Text.Trim,
                          .JenisPekerjaanID = cboJobTypeSI2.SelectedValue,
                          .NamaPers = txtCompanyNameSI2.Text.Trim,
                          .JenisIndustriID = hdfKodeIndustriPS2.Value.Trim,
                          .KeteranganUsaha = txtKeteranganUsahaSI2.Text.Trim,
                          .TempatLahirSI = txtBirthPlaceP2.Text,
                          .NoDokumentSI = txtNoDokument2.Text,
                            .TglLahirSI = If(txtTglLahirNew2.Text.Trim = "", New Date(1900, 1, 1), ConvertDate2(txtTglLahirNew2.Text)),
                            .MasaBerlakuKTPSI = If(txtMasaBerlakuKTP2.Text.Trim = "", New Date(1900, 1, 1), ConvertDate2(txtMasaBerlakuKTP2.Text)),
                          .BeroperasiSejakSI = CShort(txtBerOperasiSejak2.Text),
                          .MasaKerjaSI = CShort(txtMasaKerja2.Text),
                          .UsahaLainnyaSI = txtUsahaLainnya2.Text,
                          .JenisDokumenSI = cboIDTypeP2.SelectedValue,
                          .AlamatKTP = New Parameter.Address With {
                                  .Address = ucAddressSI_KTP2.Address.Trim,
                                  .RT = "0",
                              .RW = "0",
                              .Kelurahan = "-",
                              .Kecamatan = "-",
                              .City = ucAddressSI_KTP2.City.Trim,
                              .ZipCode = ucAddressSI_KTP2.ZipCode.Trim,
                              .AreaPhone1 = ucAddressSI_KTP2.AreaPhone1.Trim,
                              .Phone1 = ucAddressSI_KTP2.Phone1.Trim,
                              .AreaPhone2 = ucAddressSI_KTP2.AreaPhone2.Trim,
                              .Phone2 = ucAddressSI_KTP2.Phone2.Trim,
                              .AreaFax = ucAddressSI_KTP2.AreaFax.Trim,
                              .Fax = ucAddressSI_KTP2.Fax.Trim,
                .MobilePhone = ucAddressSI_KTP2.HP.Trim
                              },
                .PCSIAddress = New Parameter.Address With {
                    .Address = ucAddressSI2.Address.Trim,
                    .RT = "0",
                    .RW = "0",
                    .Kelurahan = "-",
                    .Kecamatan = "-",
                    .City = ucAddressSI2.City.Trim,
                    .ZipCode = ucAddressSI2.ZipCode.Trim,
                    .AreaPhone1 = ucAddressSI2.AreaPhone1.Trim,
                    .Phone1 = ucAddressSI2.Phone1.Trim,
                    .AreaPhone2 = ucAddressSI2.AreaPhone2.Trim,
                    .Phone2 = ucAddressSI2.Phone2.Trim,
                    .AreaFax = ucAddressSI2.AreaFax.Trim,
                    .Fax = ucAddressSI2.Fax.Trim
                }
                  })
            End If


            'Dim Err As String = ""
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID
            ocustomer.OPCSI = paramPCSI
            Err = m_controller.PersonalCustomerGuarantorSaveEdit(ocustomer, Nothing, Nothing)
            If Err = "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
            'Response.Redirect("CustomerPersonalEmergency.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabEmergency")
            Response.Redirect("CustomerPersonalEmergency.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabEmergency&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
#End Region 


    Sub fillIndustryHeader()
        'Dim dtEntity As DataTable
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblIndustryHeader"
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)

        cboIndrustriHeader.DataSource = oCustomer.listdata.DefaultView
        cboIndrustriHeader.DataTextField = "Description"
        cboIndrustriHeader.DataValueField = "ID"
        cboIndrustriHeader.DataBind()
        cboIndrustriHeader.Items.Insert(0, "Select One")
        cboIndrustriHeader.Items(0).Value = "Select One"



        cboIndrustriHeader2.DataSource = oCustomer.listdata.DefaultView
        cboIndrustriHeader2.DataTextField = "Description"
        cboIndrustriHeader2.DataValueField = "ID"
        cboIndrustriHeader2.DataBind()
        cboIndrustriHeader2.Items.Insert(0, "Select One")
        cboIndrustriHeader2.Items(0).Value = "Select One"

    End Sub
    Sub fillCboIDType()
        Dim oCustomer As New Parameter.Customer
        Dim dtList As DataTable
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.CustomerPersonalGetIDType(oCustomer)
        dtList = oCustomer.listdata
        cboIDTypeP.DataSource = dtList
        cboIDTypeP.DataTextField = "Description"
        cboIDTypeP.DataValueField = "ID"
        cboIDTypeP.DataBind()
        cboIDTypeP.Items.Insert(0, "Select One")
        cboIDTypeP.Items(0).Value = "Select One"


        cboIDTypeP2.DataSource = dtList
        cboIDTypeP2.DataTextField = "Description"
        cboIDTypeP2.DataValueField = "ID"
        cboIDTypeP2.DataBind()
        cboIDTypeP2.Items.Insert(0, "Select One")
        cboIDTypeP2.Items(0).Value = "Select One"
    End Sub

End Class