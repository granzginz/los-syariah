﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
#End Region
Public Class CustomerPersonalPinjamanLainnya
    Inherits AbsCustomerPersonal
#Region "Controls"
    Private oController As New ProspectController
    Private time As String
#End Region

#Region "uc control declaration"
    Protected WithEvents ucDateCE As ucDateCE
    Protected WithEvents ucNumberFormat As ucNumberFormat
#End Region

#Region "Constanta"
    Protected Property Entities() As IList(Of PinjamanLain)
        Get
            Return CType(ViewState("Entities"), IList(Of PinjamanLain))
        End Get
        Set(ByVal Value As IList(Of PinjamanLain))
            ViewState("Entities") = Value
        End Set
    End Property

    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID") 
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
            'Modify by Wira 20171011
            Me.CustomerID = Request("id")
            Me.ProspectAppID = Request("prospectappid")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '----------------------------------

            Me.PageAddEdit = Request("page")
            Entities = New List(Of PinjamanLain)
            If Me.PageAddEdit = "Add" Then
                lblTitle.Text = "ADD"
                GetXML()  
                
            Else 
                Me.CustomerID = Request("id")  
                BindEdit(Me.CustomerID) 
                cTabs.SetNavigateUrl(Request("page"), Request("id"))
            End If
            cTabs.RefreshAttr(Request("pnl")) 
        End If
    End Sub
    Sub BindEdit(ByVal ID As String) 
        Entities = m_controller.GetPersonalOL(GetConnectionString, ID)  
        Datagrid1.DataSource = Entities
        Datagrid1.DataBind()
    End Sub

#Region "Add-Delete Baris"
    Private Sub imbPAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPAdd.Click
        Dim i = 1
        If (Datagrid1.Items.Count > 0) Then
            Dim temp = New List(Of PinjamanLain)

            For Each itm As DataGridItem In Datagrid1.Items
                temp.Add(New PinjamanLain() With {
                         .No = i,
                         .SeqNo = i,
                         .Name = CType(itm.Cells(1).FindControl("txtNamaLembagaKeuangan"), TextBox).Text,
                         .JenisPinjaman = CType(itm.Cells(1).FindControl("txtJenisPinjaman"), TextBox).Text,
                         .JumlahPinjaman = CType(itm.Cells(1).FindControl("txtJumlahPinjaman"), ucNumberFormat).Text,
                         .Tenor = CType(itm.Cells(1).FindControl("txtTenor"), ucNumberFormat).Text,
                         .SisaPokok = CType(itm.Cells(1).FindControl("txtSisaPokok"), ucNumberFormat).Text,
                         .Angsuran = CType(itm.Cells(1).FindControl("txtAngsuran"), ucNumberFormat).Text})
                i = i + 1
            Next
            Entities = temp
        End If 
        Dim pin = New PinjamanLain() With {.No = i, .SeqNo = i}
        Entities.Add(pin)
        refershRecord()
    End Sub
    Private Sub refershRecord()
        Datagrid1.DataSource = Entities
        Datagrid1.DataBind()
    End Sub
     
    Function NextNo() As Integer
        Dim youngest = (From c In Entities
                Order By c.SeqNo Descending).ToList.First

        If (youngest Is Nothing) Then
            Return 1
        End If
        Return (youngest.SeqNo + 1)
    End Function
   
#End Region
#Region "imbPCancel_Click"
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
#End Region
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Try
            Dim Entities = New List(Of PinjamanLain)

            For Each itm As DataGridItem In Datagrid1.Items
                Entities.Add(New PinjamanLain() With {.Name = CType(itm.Cells(1).FindControl("txtNamaLembagaKeuangan"), TextBox).Text,
                                                      .JenisPinjaman = CType(itm.Cells(1).FindControl("txtJenisPinjaman"), TextBox).Text,
                                                        .JumlahPinjaman = CType(itm.Cells(1).FindControl("txtJumlahPinjaman"), ucNumberFormat).Text,
                                                        .Tenor = CType(itm.Cells(1).FindControl("txtTenor"), ucNumberFormat).Text,
                                                        .SisaPokok = CType(itm.Cells(1).FindControl("txtSisaPokok"), ucNumberFormat).Text,
                                                         .Angsuran = CType(itm.Cells(1).FindControl("txtAngsuran"), ucNumberFormat).Text})

            Next
            Dim _Err = m_controller.PersonalCustomerOLSaveEdit(GetConnectionString(), Me.CustomerID, Entities)
            If _Err <> "" Then
                ShowMessage(lblMessage, _Err, True)
                Exit Sub
            Else
                'Modify by WIra 20171011
                If Me.PageAddEdit = "Add" Then
                    ProspectLog()
                End If
                '----------------

                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                Response.Redirect("Customer.aspx?msg=false")
                End If
            'Response.Redirect("Customer.aspx")            
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try

    End Sub
#End Region
#Region "ItemCommand"
    Private Sub Datagrid1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid1.ItemCommand 
        Dim temp = New List(Of PinjamanLain)
        Dim i = 1
        For Each itm As DataGridItem In Datagrid1.Items
            temp.Add(New PinjamanLain() With {
                     .No = i,
                     .SeqNo = i,
                     .Name = CType(itm.Cells(1).FindControl("txtNamaLembagaKeuangan"), TextBox).Text,
                     .JenisPinjaman = CType(itm.Cells(1).FindControl("txtJenisPinjaman"), TextBox).Text,
                     .JumlahPinjaman = CType(itm.Cells(1).FindControl("txtJumlahPinjaman"), ucNumberFormat).Text,
                     .Tenor = CType(itm.Cells(1).FindControl("txtTenor"), ucNumberFormat).Text,
                     .SisaPokok = CType(itm.Cells(1).FindControl("txtSisaPokok"), ucNumberFormat).Text,
                     .Angsuran = CType(itm.Cells(1).FindControl("txtAngsuran"), ucNumberFormat).Text})
            i = i + 1
        Next
        Entities = temp

        If e.CommandName = "Delete" Then
            Dim seq = Datagrid1.Items(e.Item.ItemIndex).Cells(1).Text.Trim()
            Dim result = Entities.Where(Function(x) x.SeqNo = seq).SingleOrDefault()
            If (Not result Is Nothing) Then
                Entities.Remove(result)
                i = 1
                For Each item In Entities
                    item.No = i
                    i = i + 1
                Next
            End If
            refershRecord()
        End If
    End Sub
#End Region

    Sub ProspectLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Prospect
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication.ActivityType = "CST"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 8

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Prospect

        oReturn = oController.ProspectLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub
End Class


