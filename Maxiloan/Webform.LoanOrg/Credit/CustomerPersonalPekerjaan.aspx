﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerPersonalPekerjaan.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerPersonalPekerjaan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddressCity" Src="../../Webform.UserController/ucAddressCity.ascx" %>--%>
<%@ Register Src="../../webform.UserController/ucMonthCombo.ascx" TagName="ucMonthCombo"
    TagPrefix="uc2" %>
<%@ Register Src="../../webform.UserController/ucKategoriPerusahaan.ascx" TagName="ucKategoriPerusahaan"
    TagPrefix="uc3" %>
<%@ Register Src="../../webform.UserController/ucKondisiKantor.ascx" TagName="ucKondisiKantor"
    TagPrefix="uc4" %>

    <%@ Register Src="CustomerTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Personal</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var Page_Validators = new Array();
        $(document).ready(function () {
            $('#cboIndrustriHeader').change(function (e) {
                var sel = this.value.substr(0, 1);
                var kdind = $('#hdfKodeIndustriBU').val().substr(0, 1);
                if (sel != kdind)
                    $('#txtNamaIndustriBU').val('');

                /// you can write you code what you want to do...

            });
        });
        function copyAlamat(e) {
            //$("#UcJDAddress_txtAddress").val($("#UcLegalAddress_txtAddress").text());
            $("#UcJDAddress_txtAddress").val($("#" + e + "_txtAddress").text());
            $("#UcJDAddress_txtRT").val($("#" + e + "_txtRT").text());
            $("#UcJDAddress_txtRW").val($("#" + e + "_txtRW").text());
            $("#UcJDAddress_oLookUpZipCode_txtKelurahan").val($("#" + e + "_oLookUpZipCode_txtKelurahan").text());
            $("#UcJDAddress_oLookUpZipCode_txtKecamatan").val($("#" + e + "_oLookUpZipCode_txtKecamatan").text());
            $("#UcJDAddress_oLookUpZipCode_txtCity").val($("#" + e + "_oLookUpZipCode_txtCity").text());
            $("#UcJDAddress_oLookUpZipCode_txtZipCode").val($("#" + e + "_oLookUpZipCode_txtZipCode").text());
            $("#UcJDAddress_txtAreaPhone1").val($("#" + e + "_txtAreaPhone1").text());
            $("#UcJDAddress_txtPhone1").val($("#" + e + "_txtPhone1").text());
            $("#UcJDAddress_txtAreaPhone2").val($("#" + e + "_txtAreaPhone2").text());
            $("#UcJDAddress_txtPhone2").val($("#" + e + "_txtPhone2").text());
            $("#UcJDAddress_txtAreaFax").val($("#" + e + "_txtAreaFax").text());
            $("#UcJDAddress_txtFax").val($("#" + e + "_txtFax").text());
        }

        function copyAlamatKTPkeAlamatKerja() {
            copyAlamat("UcLegalAddress");
            return;
          
        }

        function copyAlamatDomisilikeAlamatKerja() {
            copyAlamat("UcResAddress");
            return;
           
        }

        function NamaIndustriBuClick(e,h) {
            //var cboselect = $("option:selected", $("#cboIndrustriHeader")).val();
            var cboselect = $('#cboIndrustriHeader').val();
            if (cboselect == 'Select One') return;
            var url = e + cboselect
            OpenJLookup(url, 'Daftar Industri', h); return false;
           // OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) & "&header=" &   cboIndrustriHeader.ClientID  %>', 'Daftar Industri', '<%= jlookupContent.ClientID %>'); return false;
        }


    </script>    
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
    <uct:tabs id='cTabs' runat='server'/>
   <%-- <div class="tab_container">
        <div id="tabIdentitas" runat="server">
            <asp:HyperLink ID="hyIdentitas" runat="server" Text="IDENTITAS"></asp:HyperLink>
        </div>
        <div id="tabPekerjaan" runat="server">
            <asp:HyperLink ID="hyPekerjaan" runat="server" Text="PEKERJAAN"></asp:HyperLink>
        </div>
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>
        <div id="tabPasangan" runat="server">
            <asp:HyperLink ID="hyPasangan" runat="server" Text="PASANGAN"></asp:HyperLink>
        </div>
        <div id="tabEmergency" runat="server">
            <asp:HyperLink ID="hyEmergency" runat="server" Text="EMERGENCY CONTACT"></asp:HyperLink>
        </div>
        <div id="tabKeluarga" runat="server">
            <asp:HyperLink ID="hyKeluarga" runat="server" Text="KELUARGA"></asp:HyperLink>
        </div>
    </div>--%>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        DATA PEKERJAAN
                    </h4>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlPekerjaan">
                <asp:Panel ID="pnlEmployee" runat="server">
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_general">
                                Jenis Pekerjaan</label>
                            <asp:RadioButtonList ID="rboTypeP" runat="server" RepeatDirection="Horizontal" CssClass="opt_single"
                                RepeatLayout="Table">
                                <asp:ListItem Value="M" Text="Karyawan"></asp:ListItem>
                                <asp:ListItem Value="N" Text="Wiraswasta"></asp:ListItem>
                                <asp:ListItem Value="P" Text="Profesional"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Perusahaan / Jenis Usaha</label>
                            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="long_text"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_single">                            
                            <label class="label_req"> Bidang Usaha Header</label>
                            <asp:DropDownList ID="cboIndrustriHeader" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Bidang Usaha Header"
                                ControlToValidate="cboIndrustriHeader" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
                        </div>
                    </div>
                     
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_req"> Bidang Usaha Detail</label> 
                            <asp:HiddenField runat="server" ID="hdfKodeIndustriBU" /> 
                            <asp:TextBox runat="server" ID="txtNamaIndustriBU" Enabled="false" CssClass="medium_text" text="-"/>
                            <%--<button class="small buttongo blue"  onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) & "&header=" &   cboIndrustriHeader.ClientID  %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;">...</button>                                                                            --%>
                            <button class="small buttongo blue"  onclick ="NamaIndustriBuClick('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) & "&header=" %>','<%= jlookupContent.ClientID %>');">...</button>
                            <asp:RequiredFieldValidator ID="rfvtxtKodeIndustriBU" runat="server" ErrorMessage="*" Display="Dynamic"  CssClass="validator_general" ControlToValidate="txtNamaIndustriBU" />
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Keterangan Usaha / Jabatan</label>
                            <asp:TextBox ID="txtJobTitle" runat="server" MaxLength="50" Width="30%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Industry Risk Rating</label>
                            <asp:DropDownList ID="cboIndustryRisk" runat="server">
                                <asp:ListItem Value="Dark Green" Selected="True">DARK GREEN</asp:ListItem>
                                <asp:ListItem Value="Green">GREEN</asp:ListItem>
                                <asp:ListItem Value="Light Yellow">LIGHT YELLOW</asp:ListItem>
                                <asp:ListItem Value="Yellow">YELLOW</asp:ListItem>
                                <asp:ListItem Value="Red">RED</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                 <div class="form_box">
                     <div class="form_single">
                            <label class="">
                                 Natural Of Business</label>
                            <asp:DropDownList ID="cboNaturalOfBusiness" runat="server" />
                         </div>
                     </div>
                      <div class="form_box">
                        <div class="form_single">
                            <label class="label">Occupation</label>
                            <asp:DropDownList ID="cboOccupation" runat="server" />
                        </div>
                    </div>
                     <div class="form_box">
                        <div class="form_single">
                            <label class="label_req">
                                Pekerjaan</label>
                            <asp:DropDownList ID="cboJobType" runat="server">
                            </asp:DropDownList>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Jenis Pekerjaan"
                                ControlToValidate="cboJobType" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_req">
                                Profesi</label>
                            <asp:DropDownList ID="cboPProfession" runat="server" >
                            </asp:DropDownList>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap pilih Jenis Profesi"
                                ControlToValidate="cboPProfession" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_single">                            
                            <label class="label_req">
                                Jabatan</label>
                            <asp:DropDownList ID="cboJobPosition" runat="server">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih Jenis Jabatan"
                                ControlToValidate="cboJobPosition" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
                        </div>
                    </div>
                    
                    
                   
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Kategori Perusahaan</label>
                            <uc3:uckategoriperusahaan id="ucKategoriPerusahaanCustomer" runat="server" />
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Kondisi Kantor</label>
                            <uc4:uckondisikantor id="ucKondisiKantorCustomer" runat="server" />
                        </div>
                    </div>
                    <%--edit validasi maksimum tahun by ario--%>
                     <div class="form_box">
                        <div class="form_single">
                            <label class="label_req">Beroperasi sejak</label>
                             <asp:TextBox ID="txtOperasiSejak" runat="server" MaxLength="4" Columns="7"></asp:TextBox>
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                            Display="Dynamic" ControlToValidate="txtOperasiSejak" ErrorMessage="Harap isi dengan 4 Angka"
                            ValidationExpression="\d{4}" CssClass="validator_general"></asp:RegularExpressionValidator>
                            <%--<asp:RangeValidator ID="valDateRange" runat="server"
                            Display="Dynamic" ControlToValidate="txtOperasiSejak" ErrorMessage="Harap isi Maksimal Tahun Ini"
                            MaximumValue="<% DateTime.Now.Year %>" MinimumValue="1900" CssClass="validator_general"></asp:RangeValidator>--%>
                            <asp:RangeValidator ID="valDateRange" runat="server" ErrorMessage="Harap isi Maksimal Tahun Ini"
                                        ControlToValidate="txtOperasiSejak" Display="Dynamic" MinimumValue="0" Type="Integer">Tahun Harus lebih Kecil dari tahun ini</asp:RangeValidator>
                        </div>
                    </div>
                    <%--edit npwp, ktp, telepon by ario--%>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="">NPWP Kantor/Perusahaan</label>
                             <asp:TextBox ID="txtPNPWP" runat="server"  MaxLength="15"></asp:TextBox>
                              <asp:RegularExpressionValidator ID="RegtxtPNPWP" Display="Dynamic" ControlToValidate="txtPNPWP"
                            runat="server" ErrorMessage="Masukkan angka saja" ValidationExpression="\d+"
                            CssClass="validator_general"></asp:RegularExpressionValidator>
                           <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPNPWP"
                        ErrorMessage="Harap isi Nomor NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>--%>
                        </div>
                    </div>

                    <div class="form_box">
                        <div class="form_single">
                            <asp:Button ID="btnAlamatKerja1" CausesValidation="false" runat="server" Text="Copy Alamat KTP"
                                CssClass="small buttongo blue"></asp:Button>
                            <asp:Button ID="btnAlamatKerja2" CausesValidation="false" runat="server" Text="Copy Alamat Domisili"
                                CssClass="small buttongo blue"></asp:Button>
                        </div>
                    </div>
                    <div class="form_box_uc">
                        <uc1:ucAddress id="UcJDAddress" runat="server"></uc1:ucAddress>
                    </div>
                    <asp:UpdatePanel runat="server" ID="up2">
                        <ContentTemplate>
                            <div class="form_box">
                                <div class="form_left">
                                    <label class="label_req">
                                        Mulai Kerja
                                    </label>
                                    <uc2:ucmonthcombo id="ucKaryawanSejakCustomer" runat="server" />
                                    <asp:TextBox ID="txtEmploymentYear" runat="server" MaxLength="4" Columns="7" AutoPostBack="true"
                                        OnTextChanged="txtEmploymentYear_OnTextChanged"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Harap isi Menjadi Karyawan sejak Tahun"
                                        ControlToValidate="txtEmploymentYear" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="RangeValidator"
                                        ControlToValidate="txtEmploymentYear" Display="Dynamic" MinimumValue="0" Type="Integer">Tahun Harus lebih Kecil dari hari ini</asp:RangeValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Harap isi dengan 4 Angka"
                                        ControlToValidate="txtEmploymentYear" Display="Dynamic" ValidationExpression="\d{4}"
                                        CssClass="validator_general"></asp:RegularExpressionValidator>
                                </div>
                                <div class="form_right">
                                    <label class="label_req">
                                        Total Lama Kerja</label>
                                    <asp:TextBox ID="txtTotalLamaKerja" runat="server" CssClass="small_text" Enabled="false"/>
                                    tahun
                                    <asp:RequiredFieldValidator ID="rfvTotalLamaKerja" runat="server" ErrorMessage="Isi total lama kerja"
                                        ControlToValidate="txtTotalLamaKerja" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator ID="rvTotalLamaKerja" runat="server" ErrorMessage="input tidak valid"
                                        ControlToValidate="txtTotalLamaKerja" Display="Dynamic" MinimumValue="0" MaximumValue="100"
                                        Type="Integer" CssClass="validator_general" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Usaha Lain</label>
                            <asp:TextBox ID="txtOBName" runat="server" MaxLength="50" style="width : 65%"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_general">
                                Relasi Group Perusahaan</label>                                                   
                            <asp:CheckBox ID="rboIsRelasiBNI" runat="server"></asp:CheckBox>                
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_general">
                                Karyawan Internal Perusahaan</label>                                                   
                            <asp:CheckBox ID="cboIsEmployee" runat="server"></asp:CheckBox>                
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <div class="form_button">
                <asp:Button ID="btnPSave" runat="server" Text="Save" CssClass="small button blue" >
                </asp:Button>
                <asp:Button ID="btnPCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
