﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class CustomerPersonal
    Inherits AbsCustomerPersonal

#Region "uc control declaration"
    Protected WithEvents ucLookupGroupCust1 As ucLookupGroupCust
    Protected WithEvents UcLegalAddress As UcCompanyAddress 'ucAddress
    Protected WithEvents UcResAddress As UcCompanyAddress 'ucAddress
    Protected WithEvents ucTinggalSejakBulan As ucMonthCombo
    Protected WithEvents ucDateCE As ucDateCE
    Protected WithEvents txtRentFinish As ucDateCE

    Protected WithEvents txtHargaRumah As ucNumberFormat
    Protected WithEvents txtPDependentNum As ucNumberFormat

#End Region   
     

#Region "Property"
    

    Public Property KegiatanUsahaS As IList(Of Parameter.KegiatanUsaha)
        Get
            Return CType(ViewState("KegiatanUsaha"), IList(Of Parameter.KegiatanUsaha))
        End Get
        Set(value As IList(Of Parameter.KegiatanUsaha))
            ViewState("KegiatanUsaha") = value
        End Set
    End Property
    Sub refresh_cboJenisPembiayaan(key As String)
        Dim def = New List(Of Parameter.CommonValueText)
        def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))
        cboJenisPembiayaan.DataValueField = "Value"
        cboJenisPembiayaan.DataTextField = "Text" 
        If key = String.Empty Then
            cboJenisPembiayaan.DataSource = def
            cboJenisPembiayaan.DataBind()
        Else
            Dim k = KegiatanUsahaS.Where(Function(x) x.Value = key).SingleOrDefault()
            cboJenisPembiayaan.DataSource = k.JenisPembiayaan
        End If
        cboJenisPembiayaan.DataBind()
    End Sub
    'Private Sub cboKegiatanUsaha_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboKegiatanUsaha.SelectedIndexChanged
    '    Dim cbo = CType(sender, DropDownList)
    '    Dim value = cbo.SelectedValue
    '    If cbo.SelectedIndex = 0 Then
    '        refresh_cboJenisPembiayaan("")
    '    Else
    '        refresh_cboJenisPembiayaan(value)
    '    End If
    'End Sub
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            lblVRent.Visible = False
            lblVWNA.Visible = False

            RangeValidator1.MaximumValue = Year(Me.BusinessDate).ToString

            Div1.InnerHtml = "<script language=JavaScript>Nationality()</script>"
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
            btnPLegalAddress.Attributes.Add("OnClick", "return LegalAddress();")

            UcLegalAddress.Style = Style
            UcLegalAddress.showMandatoryAll()
            UcLegalAddress.BindAddress()
            UcResAddress.Style = Style
            UcResAddress.showMandatoryAll()
            UcResAddress.BindAddress()

            FillCbo_("tblReligion", cboPReligion)
            FillCbo_("tblMaritalStatus", cboPMarital)
            FillCbo_("tblEducation", cboPEducation)
            FillCbo_("tblHomeStatus", cboPHomeStatus)
            FillCbo_("tblHomeLocation", cboPHomeLocation)
            'FillCbo_("tblBankAccount")
            'FillCbo_("tblCreditCard")
            'FillCbo_("tblReference")


            kegiatanUsahacbo()
            If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With

                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)

                txtPMobile.Text = ocustomclass.MobilePhone
                txtPEmail.Text = ocustomclass.Email
                UcLegalAddress.Address = ocustomclass.Address
                UcLegalAddress.RT = ocustomclass.Rt
                UcLegalAddress.RW = ocustomclass.Rw
                UcLegalAddress.Kelurahan = ocustomclass.Kelurahan
                UcLegalAddress.Kecamatan = ocustomclass.Kecamatan
                UcLegalAddress.City = ocustomclass.City
                UcLegalAddress.ZipCode = ocustomclass.ZipCode
                UcLegalAddress.AreaPhone1 = ocustomclass.AreaPhone1.Trim
                UcLegalAddress.Phone1 = ocustomclass.Phone1.Trim
                UcLegalAddress.BindAddress()
                ucTinggalSejakBulan.SelectedMonth = ocustomclass.StaySinceMonth
                txtPStaySince.Text = ocustomclass.StaySinceYear
                txtPDependentNum.Text = ocustomclass.NumOfDependence
                cboPEducation.SelectedIndex = cboPEducation.Items.IndexOf(cboPEducation.Items.FindByValue(ocustomclass.Education))

                cboPHomeStatus.SelectedIndex = cboPHomeStatus.Items.IndexOf(cboPHomeStatus.Items.FindByValue(ocustomclass.HomeStatus.Trim))
            End If

            UcResAddress.ValidatorTrue()
            txtPDependentNum.TextCssClass = "numberAlign smaller_text"
            txtHargaRumah.TextCssClass = "numberAlign"

            Me.PageAddEdit = Request("page")

            If Me.PageAddEdit = "Add" Then
                lblTitle.Text = "ADD"
                GetXML()
                cboPMarital.SelectedIndex = cboPMarital.Items.IndexOf(cboPMarital.Items.FindByValue(Me.StatusPerkawinan.Trim))
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                cTabs.SetNavigateUrl(Request("page"), Request("id"))
            End If

            cTabs.RefreshAttr(Request("pnl"))

            If Me.StatusPerkawinan.ToUpper.Trim <> "M" Then
                cTabs.EnabledLink("tabPasangan", False)
            End If

            lblPIDType.Text = Me.IDTypeName
            lblPIDNumber.Text = Me.IDNumber
            lblIDBelakuSD.Text = Me.IDExpiredDate

            If Me.Gender = "F" Then
                lblPGender.Text = "Perempuan"
            ElseIf Me.Gender = "M" Then
                lblPGender.Text = "Laki - Laki"
            ElseIf Me.Gender = "C" Then
                lblPGender.Text = "Conversion"
            End If

            lblPBirthDate.Text = Me.BirthDate
            lblPBirthPlace.Text = Me.BirthPlace
            txtPMotherName.Text = Me.MotherName

            txtPNoKK.Text = Me.KartuKeluarga

            If Me.PageAddEdit <> "Add" Then
                Dim intTotalCustomer As Integer
                intTotalCustomer = m_controller.GetTotalCustomer(Me.CustomerID.Trim, GetConnectionString)
                If intTotalCustomer > 0 Then
                    lblPName.Visible = False
                    txtCustomerName.Visible = True
                Else
					'lblPName.Visible = True
					'txtCustomerName.Visible = False
					lblPName.Visible = False
					txtCustomerName.Visible = True

				End If
            End If
            txtCustomerName.Text = Me.Name
            lblPName.Text = Me.Name
            txtGelar.Text = Me.GelarDepan.Trim
            txtGelarBelakang.Text = Me.GelarBelakang.Trim
            ScriptManager.RegisterStartupScript(cboPHomeStatus, GetType(Page), cboPHomeStatus.ClientID, String.Format("cboPHomeStatus_IndexChanged('{0}');", cboPHomeStatus.SelectedValue.Trim), True)
        End If

        UcLegalAddress.Phone1ValidatorEnabled(True)
        UcResAddress.Phone1ValidatorEnabled(True)
    End Sub
    'Public Sub NpwpValidator()
    '    RequiredFieldValidator7.Enabled = True

    'End Sub


#End Region


#Region "FillCbo"
    Sub kegiatanUsahacbo()
        Dim m_controller As New AssetMasterPriceController

        'Dim m_controller As New ProductController
        'KegiatanUsahaS = m_controller.LoadKegiatanUsaha(GetConnectionString())


        'cboKegiatanUsaha.DataSource = KegiatanUsahaS
        'cboKegiatanUsaha.DataValueField = "Value"
        'cboKegiatanUsaha.DataTextField = "Text"
        'cboKegiatanUsaha.DataBind()
        'cboKegiatanUsaha.Items.Insert(0, "Select One")
        'cboKegiatanUsaha.Items(0).Value = "SelectOne"
        'cboKegiatanUsaha.SelectedIndex = 0


        'Dim def = New List(Of Parameter.CommonValueText)
        'def.Add(New Parameter.CommonValueText("SelectOne", "Select One"))

        'cboJenisPembiayaan.DataValueField = "Value"
        'cboJenisPembiayaan.DataTextField = "Text"
        'cboJenisPembiayaan.DataSource = def
        'cboJenisPembiayaan.Items.Insert(0, "Select One")
        'cboJenisPembiayaan.Items(0).Value = "SelectOne"
        'cboJenisPembiayaan.DataBind()

        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.AssetMasterPrice
        oCustomer.strConnection = GetConnectionString()

        dtEntity = m_controller.GetAssetTypeCombo(oCustomer)
        
        cboJenisPembiayaan.DataSource = dtEntity.DefaultView
        cboJenisPembiayaan.DataTextField = "description"
        cboJenisPembiayaan.DataValueField = "assettypeid"
        cboJenisPembiayaan.DataBind()
        cboJenisPembiayaan.Items.Insert(0, "Select One")
        cboJenisPembiayaan.Items(0).Value = "Select One"
    End Sub
    'Sub FillCbo(ByVal table As String)
    '    Dim dtEntity As DataTable
    '    Dim oCustomer As New Parameter.Customer
    '    oCustomer.strConnection = GetConnectionString()
    '    oCustomer.Table = table
    '    oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
    '    If Not oCustomer Is Nothing Then
    '        dtEntity = oCustomer.listdata
    '    End If

    '    Dim combo As DropDownList
    '    Select Case table
    '        Case "tblReligion"
    '            combo = cboPReligion
    '        Case "tblMaritalStatus"
    '            combo = cboPMarital
    '        Case "tblEducation"
    '            combo = cboPEducation
    '        Case "tblHomeStatus"
    '            combo = cboPHomeStatus
    '        Case "tblHomeLocation"
    '            combo = cboPHomeLocation
    '    End Select
    '    If combo Is Nothing Then
    '        Return
    '    End If
    '    combo.DataSource = dtEntity.DefaultView
    '    combo.DataTextField = "Description"
    '    combo.DataValueField = "ID"
    '    combo.DataBind()
    '    combo.Items.Insert(0, "Select One")
    '    combo.Items(0).Value = "Select One"
    'End Sub
                      

#End Region
#Region "imbPCancel_Click"
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
    Private Sub btnPLegalAddress_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPLegalAddress.Click
        With UcResAddress
            .Address = UcLegalAddress.Address
            .RT = UcLegalAddress.RT
            .RW = UcLegalAddress.RW
            .Kelurahan = UcLegalAddress.Kelurahan
            .Kecamatan = UcLegalAddress.Kecamatan
            .City = UcLegalAddress.City
            .ZipCode = UcLegalAddress.ZipCode
            .AreaPhone1 = UcLegalAddress.AreaPhone1
            .AreaPhone2 = UcLegalAddress.AreaPhone2
            .AreaFax = UcLegalAddress.AreaFax
            .Phone1 = UcLegalAddress.Phone1
            .Phone2 = UcLegalAddress.Phone2
            .Fax = UcLegalAddress.Fax
            .BindAddress()
        End With
    End Sub
#End Region
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Try
            Dim ocustomer As New Parameter.Customer
            Dim oLegalAddress As New Parameter.Address
            Dim oResAddress As New Parameter.Address
            Dim oPCEX As New Parameter.CustomerEX


            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

            ocustomer.BranchId = Me.sesBranchId
            ocustomer.Name = txtCustomerName.Text.Trim
            ocustomer.CustomerType = "P"
            ocustomer.CustomerGroupID = IIf(IsNothing(Me.CustomerGroupID), "", Me.CustomerGroupID).ToString
            ocustomer.IDType = Me.IDType
            ocustomer.IDNumber = Me.IDNumber
            ocustomer.IDExpiredDate = Date.ParseExact(Me.IDExpiredDate, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            ocustomer.Gender = Me.Gender
            ocustomer.BirthPlace = Me.BirthPlace
            ocustomer.BirthDate = ConvertDate(Me.BirthDate)
            ocustomer.NamaSuamiIstri = Me.NamaPasangan
            ocustomer.MobilePhone = txtPMobile.Text.Trim
            ocustomer.MobilePhone1 = txtPMobile1.Text.Trim
            ocustomer.Email = txtPEmail.Text.Trim
            ocustomer.Religion = cboPReligion.SelectedValue.Trim
            ocustomer.MaritalStatus = cboPMarital.SelectedValue.Trim
            ocustomer.NumOfDependence = txtPDependentNum.Text.Trim
            ocustomer.PersonalNPWP = txtPNPWP.Text.Trim
            ocustomer.NoKK = txtPNoKK.Text.Trim
            ocustomer.Education = cboPEducation.SelectedValue.Trim
            ocustomer.Nationality = cboPNationality.SelectedValue.Trim
            ocustomer.WNACountry = txtPWNA.Text.Trim
            ocustomer.HomeStatus = cboPHomeStatus.SelectedValue.Trim
            ocustomer.RentFinishDate = ConvertDate(txtRentFinish.Text.Trim)
            ocustomer.HomeLocation = cboPHomeLocation.SelectedValue.Trim
            ocustomer.HomePrice = txtHargaRumah.Text.Trim
            ocustomer.StaySinceYear = txtPStaySince.Text.Trim
            ocustomer.MotherName = txtPMotherName.Text.Trim
            ocustomer.BusinessDate = Me.BusinessDate
            ocustomer.ProspectAppID = Me.ProspectAppID
            oLegalAddress.Address = UcLegalAddress.Address.Trim
            oLegalAddress.RT = UcLegalAddress.RT.Trim
            oLegalAddress.RW = UcLegalAddress.RW.Trim
            oLegalAddress.Kelurahan = UcLegalAddress.Kelurahan.Trim
            oLegalAddress.Kecamatan = UcLegalAddress.Kecamatan.Trim
            oLegalAddress.City = UcLegalAddress.City.Trim
            oLegalAddress.ZipCode = UcLegalAddress.ZipCode.Trim
            oLegalAddress.AreaPhone1 = UcLegalAddress.AreaPhone1.Trim
            oLegalAddress.Phone1 = UcLegalAddress.Phone1.Trim
            oLegalAddress.AreaPhone2 = UcLegalAddress.AreaPhone2.Trim
            oLegalAddress.Phone2 = UcLegalAddress.Phone2.Trim
            oLegalAddress.AreaFax = UcLegalAddress.AreaFax.Trim
            oLegalAddress.Fax = UcLegalAddress.Fax.Trim
            oResAddress.Address = UcResAddress.Address.Trim
            oResAddress.RT = UcResAddress.RT.Trim
            oResAddress.RW = UcResAddress.RW.Trim
            oResAddress.Kelurahan = UcResAddress.Kelurahan.Trim
            oResAddress.Kecamatan = UcResAddress.Kecamatan.Trim
            oResAddress.City = UcResAddress.City.Trim
            oResAddress.ZipCode = UcResAddress.ZipCode.Trim
            oResAddress.AreaPhone1 = UcResAddress.AreaPhone1.Trim
            oResAddress.Phone1 = UcResAddress.Phone1.Trim
            oResAddress.AreaPhone2 = UcResAddress.AreaPhone2.Trim
            oResAddress.Phone2 = UcResAddress.Phone2.Trim
            oResAddress.AreaFax = UcResAddress.AreaFax.Trim
            oResAddress.Fax = UcResAddress.Fax.Trim
            ocustomer.PersonalCustomerType = Me.Type
            ocustomer.Gelar = txtGelar.Text.Trim
            ocustomer.GelarBelakang = txtGelarBelakang.Text.Trim
            ocustomer.KondisiRumah = ddlKondisiRumah.SelectedValue.Trim

            ocustomer.KegiatanUsaha = "" 'cboKegiatanUsaha.SelectedValue.Trim
            ocustomer.JenisPembiayaan = cboJenisPembiayaan.SelectedValue.Trim
            ocustomer.IsOLS = cboIsOLS.Checked

            With oPCEX
                .SPTTahunan = CBool(rboSPTTahunan.SelectedValue.Trim)
                .TinggalSejakBulan = ucTinggalSejakBulan.SelectedMonth
                .KeteranganUsaha = ""
            End With


            Dim Err As String
            ocustomer.strConnection = GetConnectionString()

            If Me.PageAddEdit = "Add" Then
                Dim oReturn As New Parameter.Customer
                oReturn = m_controller.PersonalCustomerIdentitasAdd(ocustomer, _
                                              oLegalAddress, _
                                              oResAddress, _
                                              oPCEX)
                If oReturn.Err = "" Then
                    Me.CustomerID = oReturn.CustomerID
                    If File.Exists(gStrPath & gStrFileName) Then
                        File.Delete(gStrPath & gStrFileName)
                    End If
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_INSERT_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, ocustomer.Err, True)
                    Exit Sub
                End If
            Else
                ocustomer.CustomerID = Me.CustomerID
                Err = m_controller.PersonalCustomerIdentitasSaveEdit(ocustomer, _
                                              oLegalAddress, _
                                              oResAddress, _
                                              oPCEX)
                If Err = "" Then
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
                Else
                    ShowMessage(lblMessage, Err, True)
                    Exit Sub
                End If
            End If
            Response.Redirect("CustomerPersonalPekerjaan.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabPekerjaan")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
#End Region
#Region "Edit"
    Sub BindEdit(ByVal ID As String)
        Dim dtEntity As New DataTable
        Dim dtOmset As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = ID
        oCustomer.strConnection = GetConnectionString()
        oCustomer.CustomerType = "P"
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "3")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        Me.Name = oRow("Name").ToString.Trim
        Me.GelarDepan = oRow("Gelar").ToString.Trim

        Me.IDType = oRow("IDType").ToString.Trim
        Me.IDTypeName = oRow("IDTypeName").ToString.Trim
        Me.IDNumber = oRow("IDNumber").ToString.Trim
        Me.Gender = oRow("Gender").ToString.Trim
        If IsDate(oRow("IDExpiredDate").ToString) Then Me.IDExpiredDate = Format(oRow("IDExpiredDate"), "dd/MM/yyyy")

        If Not IsDBNull(oRow("BirthDate")) Then
            Me.BirthDate = Day(CDate(oRow("BirthDate"))).ToString + "/" + Month(CDate(oRow("BirthDate"))).ToString + "/" + Year(CDate(oRow("BirthDate"))).ToString
        Else
            Me.BirthDate = ""
        End If

        Me.BirthPlace = oRow("BirthPlace").ToString.Trim
        Me.Type = oRow("PersonalCustomerType").ToString.Trim
        Me.StatusPerkawinan = oRow("MaritalStatus").ToString.Trim()
        Me.MotherName = oRow("MotherName").ToString.Trim
        Me.NamaPasangan = oRow("SINamaSuamiIstri").ToString.Trim
        Me.KartuKeluarga = oRow("NoKK").ToString.Trim

        txtPMobile.Text = oRow("MobilePhone").ToString.Trim
        txtPMobile1.Text = oRow("MobilePhone1").ToString.Trim
        txtPEmail.Text = oRow("Email").ToString.Trim
        cboPReligion.SelectedIndex = cboPReligion.Items.IndexOf(cboPReligion.Items.FindByValue(oRow("Religion").ToString.Trim))
        cboPMarital.SelectedIndex = cboPMarital.Items.IndexOf(cboPMarital.Items.FindByValue(oRow("MaritalStatus").ToString.Trim))
        txtPDependentNum.Text = oRow("NumOfDependence").ToString.Trim
        txtPNPWP.Text = oRow("PersonalNPWP").ToString.Trim
        txtHargaRumah.Text = FormatNumber(oRow("HomePrice").ToString, 0)
        cboPEducation.SelectedIndex = cboPEducation.Items.IndexOf(cboPEducation.Items.FindByValue(oRow("Education").ToString.Trim))
        If oRow("Nationality").ToString.Trim <> "" Then
            cboPNationality.SelectedIndex = cboPNationality.Items.IndexOf(cboPNationality.Items.FindByValue(oRow("Nationality").ToString.Trim))
        End If
        If oRow("Nationality").ToString.Trim = "WNI" Then
            Div1.InnerHtml = "<script language=JavaScript>Nationality()</script>"
        End If
        txtPWNA.Text = oRow("WNACountry").ToString.Trim

        Me.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim
        txtKelompokUsaha.Text = oRow("CustomerGroupNm").ToString.Trim
        ucLookupGroupCust1.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim
        cboPHomeStatus.SelectedIndex = cboPHomeStatus.Items.IndexOf(cboPHomeStatus.Items.FindByValue(oRow("HomeStatus").ToString.Trim))

        If oRow("RentFinishDate").ToString <> "" Then
            txtRentFinish.Text = IIf((Day(CDate(oRow("RentFinishDate"))).ToString.Length = 1), "0" + Day(CDate(oRow("RentFinishDate"))).ToString, Day(CDate(oRow("RentFinishDate"))).ToString).ToString + "/" + IIf(Month(CDate(oRow("RentFinishDate"))).ToString.Length = 1, "0" + Month(CDate(oRow("RentFinishDate"))).ToString, Month(CDate(oRow("RentFinishDate"))).ToString).ToString + "/" + Year(CDate(oRow("RentFinishDate"))).ToString
        End If
        cboPHomeLocation.SelectedIndex = cboPHomeLocation.Items.IndexOf(cboPHomeLocation.Items.FindByValue(oRow("HomeLocation").ToString.Trim))
        txtPStaySince.Text = oRow("StaySinceYear").ToString.Trim
        UcLegalAddress.Address = oRow("LegalAddress").ToString.Trim
        UcLegalAddress.RT = oRow("LegalRT").ToString.Trim
        UcLegalAddress.RW = oRow("LegalRW").ToString.Trim
        UcLegalAddress.Kelurahan = oRow("LegalKelurahan").ToString.Trim
        UcLegalAddress.Kecamatan = oRow("LegalKecamatan").ToString.Trim
        UcLegalAddress.City = oRow("LegalCity").ToString.Trim
        UcLegalAddress.ZipCode = oRow("LegalZipCode").ToString.Trim
        UcLegalAddress.AreaPhone1 = oRow("LegalAreaPhone1").ToString.Trim
        UcLegalAddress.Phone1 = oRow("LegalPhone1").ToString.Trim
        UcLegalAddress.AreaPhone2 = oRow("LegalAreaPhone2").ToString.Trim
        UcLegalAddress.Phone2 = oRow("LegalPhone2").ToString.Trim
        UcLegalAddress.AreaFax = oRow("LegalAreaFax").ToString.Trim
        UcLegalAddress.Fax = oRow("LegalFax").ToString.Trim
        UcLegalAddress.BindAddress()
        UcResAddress.Address = oRow("ResidenceAddress").ToString.Trim
        UcResAddress.RT = oRow("ResidenceRT").ToString.Trim
        UcResAddress.RW = oRow("ResidenceRW").ToString.Trim
        UcResAddress.Kelurahan = oRow("ResidenceKelurahan").ToString.Trim
        UcResAddress.Kecamatan = oRow("ResidenceKecamatan").ToString.Trim
        UcResAddress.City = oRow("ResidenceCity").ToString.Trim
        UcResAddress.ZipCode = oRow("ResidenceZipCode").ToString.Trim
        UcResAddress.AreaPhone1 = oRow("ResidenceAreaPhone1").ToString.Trim
        UcResAddress.Phone1 = oRow("ResidencePhone1").ToString.Trim
        UcResAddress.AreaPhone2 = oRow("ResidenceAreaPhone2").ToString.Trim
        UcResAddress.Phone2 = oRow("ResidencePhone2").ToString.Trim
        UcResAddress.AreaFax = oRow("ResidenceAreaFax").ToString.Trim
        UcResAddress.Fax = oRow("ResidenceFax").ToString.Trim
        UcResAddress.BindAddress()

        If oRow("EXSPTTahunan").ToString.Trim <> "" Then
            rboSPTTahunan.SelectedIndex = rboSPTTahunan.Items.IndexOf(rboSPTTahunan.Items.FindByValue(oRow("EXSPTTahunan").ToString.Trim))
        End If

        cboIsOLS.Checked = oRow("IsOLS").ToString.Trim

        ucTinggalSejakBulan.SelectedMonth = CInt(oRow("EXTinggalSejakBulan"))
        Me.GelarBelakang = oRow("GelarBelakang").ToString.Trim
        ddlKondisiRumah.SelectedValue = oRow("KondisiRumah").ToString.Trim
        cboJenisPembiayaan.SelectedIndex = cboJenisPembiayaan.Items.IndexOf(cboJenisPembiayaan.Items.FindByValue(oRow("JenisPembiayaan").ToString.Trim))
        'If (oRow("KegiatanUsaha").ToString.Trim = "") Then
        '    cboKegiatanUsaha.SelectedIndex = 0
        '    refresh_cboJenisPembiayaan("")
        'Else
        '    cboKegiatanUsaha.SelectedIndex = cboKegiatanUsaha.Items.IndexOf(cboKegiatanUsaha.Items.FindByValue(oRow("KegiatanUsaha").ToString.Trim))
        '    refresh_cboJenisPembiayaan(oRow("KegiatanUsaha").ToString.Trim)
        '    cboJenisPembiayaan.SelectedIndex = cboJenisPembiayaan.Items.IndexOf(cboJenisPembiayaan.Items.FindByValue(oRow("JenisPembiayaan").ToString.Trim))
        'End If
    End Sub
#End Region
#Region "OnChange"
    
    Protected Sub btnLookupCustomer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupCustomer.Click
        ucLookupGroupCust1.CmdWhere = "All"
        ucLookupGroupCust1.Sort = "CustomerGroupID ASC"
        ucLookupGroupCust1.Popup()
    End Sub
    Public Sub CatSelectedCustomer(ByVal CustomerGroupID As String, ByVal CustomerGroupName As String)
        txtKelompokUsaha.Text = CustomerGroupName
        Me.CustomerGroupID = CustomerGroupID
    End Sub
#End Region
End Class