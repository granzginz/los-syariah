﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewTermCondition.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewTermCondition" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewTermCondition</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var x = screen.width;
        var y = screen.height - 100;			
    </script>
    <script language="javascript" type="text/javascript">
        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=15, top=10, width=985, height=600, menubar=0, scrollbars=yes');
        }
		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - SYARAT &amp; KONDISI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server" EnableViewState="False"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT &amp; KONDISI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgTC" runat="server" EnableViewState="False" CssClass="grid_general"
                AutoGenerateColumns="False" PageSize="3" DataKeyField="TCName">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <ItemTemplate>
                            <asp:Label ID="lblTCNo" runat="server" EnableViewState="False"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN"></asp:BoundColumn>
                    <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="PERIKSA">
                        <ItemTemplate>
                            <asp:Label ID="lblChecked" runat="server" EnableViewState="False" Text='<%#Container.DataItem("IsChecked")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="IsMandatory" HeaderText="MANDATORY"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="TGL JANJI">
                        <ItemTemplate>
                            <asp:Label ID="lblPromiseDate" runat="server" EnableViewState="False" Text='<%#Container.DataItem("PromiseDate")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="CATATAN">
                        <ItemTemplate>
                            <asp:Label ID="lblNotes" runat="server" EnableViewState="False" Text='<%#Container.DataItem("Notes")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    <%--<div class="form_title">
        <div class="form_single">
            <h4>
                SYARAT &amp; KONIDISI CHECK LIST
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgTC2" runat="server" EnableViewState="False" CssClass="grid_general"
                AutoGenerateColumns="False" PageSize="3" DataKeyField="TCName">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <ItemTemplate>
                            <asp:Label ID="lblTCNo2" runat="server" EnableViewState="False"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="TCName" HeaderText="DOKUMEN"></asp:BoundColumn>
                    <asp:BoundColumn DataField="CheckList" HeaderText="CHECKLIST"></asp:BoundColumn>
                    <asp:BoundColumn DataField="PriorTo" HeaderText="SEBELUM"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="PERIKSA">
                        <ItemTemplate>
                            <asp:Label ID="LblCheckedTC2" runat="server" EnableViewState="False" Text='<%#Container.DataItem("IsChecked")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="ISMandatory" HeaderText="MANDATORY"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="TGL JANJI">
                        <ItemTemplate>
                            <asp:Label ID="LblPromiseDateTC2" runat="server" EnableViewState="False" Text='<%#Container.DataItem("PromiseDate")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="CATATAN">
                        <ItemTemplate>
                            <asp:Label ID="LblNotesTC2" runat="server" EnableViewState="False" Text='<%#Container.DataItem("Notes")%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn Visible="False" DataField="MasterTCID" HeaderText="MasterTCID">
                    </asp:BoundColumn>
                    <asp:BoundColumn Visible="False" DataField="MSTCCLSequenceNo" HeaderText="MSTCCLSequenceNo">
                    </asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>--%>
   <%-- <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
