﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CustomerGroupPIC
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcCompanyAddress As UcCompanyAddress

    Private m_controller As New CustomerGroupPICController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property CustomerGroupID() As String
        Get
            Return CType(ViewState("CustomerGroupID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerGroupID") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            Me.FormID = "CustomerGroupPIC"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtPage.Text = "1"
                Me.Sort = "ID ASC"
                If (CustomerGroupID Is Nothing) Then
                    If hdnCustomerGroupID.Value <> "" Then
                        CustomerGroupID = hdnCustomerGroupID.Value
                    Else
                        CustomerGroupID = Request("id")
                    End If
                End If


                If CustomerGroupID <> "" Then
                    Me.CmdWhere = " CustomerGroupId = '" + CustomerGroupID.Trim() + "'".Trim()
                Else
                    Me.CmdWhere = "ALL"
                End If

                BindGridEntity(Me.CmdWhere)
                hdnCustomerGroupID.Value = CustomerGroupID
            End If
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomerGroupPIC As New Parameter.CustomerGroupPIC
        InitialDefaultPanel()
        oCustomerGroupPIC.strConnection = GetConnectionString()
        oCustomerGroupPIC.WhereCond = cmdWhere
        oCustomerGroupPIC.CurrentPage = currentPage
        oCustomerGroupPIC.PageSize = pageSize
        oCustomerGroupPIC.SortBy = Me.Sort
        oCustomerGroupPIC = m_controller.GetCustomerGroupPIC(oCustomerGroupPIC)

        If Not oCustomerGroupPIC Is Nothing Then
            dtEntity = oCustomerGroupPIC.Listdata
            recordCount = oCustomerGroupPIC.Totalrecords
        Else
            recordCount = 0
        End If

        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Sub BindDetail(ByVal ID As String, ByVal Desc As String)
        Dim oCustomerGroupPIC As New Parameter.CustomerGroupPIC
        Me.AddEdit = "VIEW"
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        If Request("cmd2") = "close" Then
            btnBack.Visible = False
            btnClose.Visible = True
        Else
            btnBack.Visible = True
            btnClose.Visible = False
        End If
        btnCancel.Visible = False
        btnSave.Visible = False

        lblTitleAddEdit.Text = Me.AddEdit
        oCustomerGroupPIC.CustomerGroupID = ID
        oCustomerGroupPIC.strConnection = GetConnectionString()
        oCustomerGroupPIC = m_controller.GetCustomerGroupPICListByCustGroupID(oCustomerGroupPIC)
        hdnCustomerGroupID.Value = oCustomerGroupPIC.CustomerGroupID
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub btnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oCustomerGroupPIC As New Parameter.CustomerGroupPIC
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            btnBack.Visible = False
            btnCancel.Visible = True
            btnSave.Visible = True
            btnClose.Visible = False

            lblTitleAddEdit.Text = Me.AddEdit
            oCustomerGroupPIC.ID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
            oCustomerGroupPIC.strConnection = GetConnectionString()
            oCustomerGroupPIC = m_controller.GetCustomerGroupPICList(oCustomerGroupPIC)
            btnCancel.CausesValidation = False

            hdnIDEdit.Value = oCustomerGroupPIC.ID
            txtNamaPIC.Text = oCustomerGroupPIC.NamaPIC.Trim()
            txtNoHP.Text = oCustomerGroupPIC.NoHP.Trim()
            With UcCompanyAddress
                .Address = oCustomerGroupPIC.Alamat.Trim()
                .RT = oCustomerGroupPIC.RT.Trim()
                .RW = oCustomerGroupPIC.RW.Trim()
                .Kelurahan = oCustomerGroupPIC.Kelurahan.Trim()
                .Kecamatan = oCustomerGroupPIC.Kecamatan.Trim
                .City = oCustomerGroupPIC.Kota.Trim()
                .ZipCode = oCustomerGroupPIC.KodePos.Trim()
                .AreaPhone1 = oCustomerGroupPIC.AreaPhone1.Trim()
                .AreaPhone2 = oCustomerGroupPIC.AreaPhone2.Trim()
                .Phone1 = oCustomerGroupPIC.Phone1.Trim()
                .Phone2 = oCustomerGroupPIC.Phone2.Trim()
                .AreaFax = oCustomerGroupPIC.AreaFax.Trim()
                .Fax = oCustomerGroupPIC.Fax.Trim()
                .Style = "Setting"
                .BindAddress()
            End With

        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.CustomerGroupPIC
            With customClass
                .ID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .strConnection = GetConnectionString()
            End With
            err = m_controller.CustomerGroupPICDelete(customClass)
            If err <> "" Then
                showMessage(lblMessage, err, True)

            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)

            End If
            BindGridEntity(Me.CmdWhere)
            txtPage.Text = "1"
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim customClass As New Parameter.CustomerGroupPIC
        Dim ErrMessage As String = ""

        With customClass
            .CustomerGroupID = hdnCustomerGroupID.Value
            .NamaPIC = txtNamaPIC.Text
            .Alamat = UcCompanyAddress.Address
            .RT = UcCompanyAddress.RT
            .RW = UcCompanyAddress.RW
            .Kelurahan = UcCompanyAddress.Kelurahan
            .Kecamatan = UcCompanyAddress.Kecamatan
            .Kota = UcCompanyAddress.City
            .KodePos = UcCompanyAddress.ZipCode
            .AreaPhone1 = UcCompanyAddress.AreaPhone1
            .AreaPhone2 = UcCompanyAddress.AreaPhone2
            .Phone1 = UcCompanyAddress.Phone1
            .Phone2 = UcCompanyAddress.Phone2
            .AreaFax = UcCompanyAddress.AreaFax
            .Fax = UcCompanyAddress.Fax
            .NoHP = txtNoHP.Text
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.CustomerGroupPICSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)

                Exit Sub
            Else
                lblMessage.Text = MessageHelper.MESSAGE_INSERT_SUCCESS

                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.ID = hdnIDEdit.Value
            m_controller.CustomerGroupPICSaveEdit(customClass)
            lblMessage.Text = MessageHelper.MESSAGE_UPDATE_SUCCESS

            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        btnBack.Visible = False
        btnCancel.Visible = True
        btnSave.Visible = True
        btnClose.Visible = False

        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        btnCancel.CausesValidation = False

        txtNoHP.Text = Nothing
        txtNamaPIC.Text = Nothing
        With UcCompanyAddress
            .Address = Nothing
            .RT = Nothing
            .RW = Nothing
            .Kelurahan = Nothing
            .Kecamatan = Nothing
            .City = Nothing
            .ZipCode = Nothing
            .AreaPhone1 = Nothing
            .AreaPhone2 = Nothing
            .Phone1 = Nothing
            .Phone2 = Nothing
            .AreaFax = Nothing
            .Fax = Nothing
            .Style = "Setting"
            .BindAddress()
        End With

    End Sub
    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        SendCookies()
        Response.Redirect("Report/CustomerGroupPIC.aspx")
    End Sub
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("CustomerGroupPIC")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("CustomerGroupPIC")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = " CustomerGroupId = '" + CustomerGroupID.Trim() + "'".Trim()
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text.Trim + "'"
            Me.CmdWhere += " and CustomerGroupId like '%" + CustomerGroupID.Trim() + "%'".Trim()
        Else
            Me.CmdWhere = " CustomerGroupId = '" + CustomerGroupID.Trim() + "'".Trim()
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub btnBackMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackMenu.Click
        Response.Redirect("CustomerGroup.aspx")
    End Sub
    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("CustomerGroupPIC.aspx")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("CustomerGroupPIC.aspx?cmd=dtl&id=" + hdnCustomerGroupID.Value + "&desc=" + Request("desc") + "")
    End Sub

End Class