﻿
#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class ViewCreditProcess
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property
    Property ApprovalNo() As String
        Get
            Return viewstate("ApprovalNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApprovalNo") = Value
        End Set
    End Property

#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "ViewCredit"

            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                If Not Page.IsPostBack Then
                    Me.ApplicationID = Request("ApplicationID").ToString
                    Me.AgreementNo = Request("AgreementNo").ToString
                    Me.CustomerName = Request("CustomerName").ToString
                    Me.Style = Request("Style").ToString
                    Me.CustomerID = Request("CustomerID").ToString.Trim
                    Bindgrid()
                    BindgridApprovalHistory()
                    Dim lb As New LinkButton
                    lb = CType(Me.FindControl("lblCustomerID"), LinkButton)
                    lb.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")
                End If
            End If
        End If
    End Sub
#End Region
#Region "BindGrid"
    Sub Bindgrid()
        Dim dt As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewCreditProcess"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()
            If objReader.Read Then
                lblAgreementNo.Text = objReader.Item("NewApplicationDate").ToString.Trim
                lblCustomerID.Text = Me.CustomerName
                lblApplicationDaTE.Text = objReader.Item("NewApplicationDate").ToString.Trim
                lblSourceApplication.Text = objReader.Item("ApplicationSource").ToString.Trim
                lblReqSurveyDate.Text = objReader.Item("RequestSurveyDate").ToString.Trim
                lblCreditBy.Text = objReader.Item("CreditScoringResult").ToString.Trim
                lblSurveyDate.Text = objReader.Item("SurveyDate").ToString.Trim
                lblRCADate.Text = objReader.Item("RCADate").ToString.Trim
                lblApprovalDate.Text = objReader.Item("ApprovalDate").ToString.Trim
                lblApprovalNo.Text = objReader.Item("ApprovalNo").ToString.Trim
                Me.ApprovalNo = objReader.Item("ApprovalNo").ToString.Trim
                lblAgreementDate.Text = objReader.Item("AgreementDate").ToString.Trim
                lblPODate.Text = objReader.Item("PurchaseOrderDate").ToString.Trim
                lblDODate.Text = objReader.Item("DeliveryOrderDate").ToString.Trim
                lblSupplierDate.Text = objReader.Item("SupplierInvoiceDate").ToString.Trim
                lblGoLiveDate.Text = objReader.Item("GoLiveDate").ToString.Trim
                lblSurveyor.Text = objReader.Item("SurveyorName").ToString.Trim
                lblAO.Text = objReader.Item("AOName").ToString.Trim
                lblCA.Text = objReader.Item("CAName").ToString.Trim

                'lblAdminFee.Text = FormatNumber(objReader.Item("AdminFee").ToString.Trim, 0)
                'lblSurveyFee.Text = FormatNumber(objReader.Item("SurveyFee").ToString.Trim, 0)
                'lblFiduciaFee.Text = FormatNumber(objReader.Item("FiduciaFee").ToString.Trim, 0)
                'lblBBNFee.Text = FormatNumber(objReader.Item("BBNFee").ToString.Trim, 0)
                'lblProvisionFee.Text = FormatNumber(objReader.Item("ProvisionFee").ToString.Trim, 0)
                'lblOtherFee.Text = FormatNumber(objReader.Item("OtherFee").ToString.Trim, 0)
                'lblNotaryFee.Text = FormatNumber(objReader.Item("NotaryFee").ToString.Trim, 0)

                'lblExConversion.Text = objReader.Item("isExConversion").ToString.Trim
                'lblFloatingPeriod.Text = objReader.Item("FloatingPeriod").ToString.Trim
                'lblAvalist.Text = objReader.Item("isAvalist").ToString.Trim
                'lblFloatingReviewDate.Text = objReader.Item("FloatingNextReviewDate").ToString.Trim

                'lblSyndication.Text = objReader.Item("isSyndication").ToString.Trim
                'lblLastFloatAdj.Text = objReader.Item("LastFloatingAdjustment").ToString.Trim
                'lblBuyBackValidDate.Text = objReader.Item("BuyBackGuaranteeValidDate").ToString.Trim
                'lblLastFloatEx.Text = objReader.Item("LastFloatExecution").ToString.Trim
                '' lblNADate.Text = objReader.Item("NADate").ToString.Trim
                '' lblWODate.Text = objReader.Item("WODate").ToString.Trim
                'LblNST.Text = objReader.Item("IsNst").ToString.Trim
            End If
            objReader.Close()

            '========================
            'Untuk Grid Cross Default
            '========================
            '      Dim DtGrid As New DataTable
            '       objCommand = New SqlCommand
            '     objConnection = New SqlConnection(getConnectionString)
            '        Dim adapter As New SqlDataAdapter
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.Parameters.Clear()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewCrossDefault"
            'objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.Char, 20).Value = Me.ApplicationID.Trim
            objReader = objCommand.ExecuteReader()
            'adapter.SelectCommand = objCommand
            'adapter.Fill(DtGrid)
            'dtgCrossDefault.DataSource = objReader
            'dtgCrossDefault.DataBind()
            objReader.Close()
        Catch ex As Exception
            Response.Write(ex.StackTrace)
            Dim err As New MaxiloanExceptions
            err.WriteLog("ViewCreditProcess", "BindGrid", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
    Private Sub BindgridApprovalHistory()
        Dim objCommand As New SqlCommand
        Dim objconnection As New SqlConnection(GetConnectionString)
        Dim objread As SqlDataReader
        Try
            If objconnection.State = ConnectionState.Closed Then objconnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objconnection
            objCommand.CommandText = "spApprovalHistoryList"
            objCommand.Parameters.Add("@ApprovalNo", SqlDbType.VarChar, 50).Value = Me.ApprovalNo
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objread = objCommand.ExecuteReader()

            dtgHistory.DataSource = objread
            dtgHistory.DataBind()
            objread.Close()
            If dtgHistory.Items.Count > 0 Then
                dtgHistory.Visible = True
                pnlHistory.Visible = True
            Else
                dtgHistory.Visible = False
                pnlHistory.Visible = False
            End If

        Catch ex As Exception
            Response.Write(ex.StackTrace)
            'lblMessage.Text = e.Message
        Finally
            objCommand.Parameters.Clear()
            objCommand.Dispose()
            If objconnection.State = ConnectionState.Open Then objconnection.Close()
            objconnection.Dispose()
        End Try
    End Sub
    Private Sub dtgHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgHistory.ItemDataBound
        If e.Item.ItemIndex >= 0 Then e.Item.Cells(0).Text = CStr(e.Item.ItemIndex + 1)
    End Sub
#End Region
    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    'End Sub
End Class