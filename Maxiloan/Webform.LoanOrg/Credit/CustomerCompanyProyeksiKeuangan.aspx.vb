﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class CustomerCompanyProyeksiKeuangan
	Inherits AbsCustomerCompany

	Protected WithEvents GridNav As ucGridNav
	Protected WithEvents TglProyeksi As ucDateCE
	Protected WithEvents AssetIDR _
						 , AssetLancar _
						 , KasSetaraKasAsetLancar _
						 , PiutangUsahAsetLancar _
						 , InvestasiLainnyaAsetLancar _
						 , AsetLancarLainnya _
						 , AsetTidakLancar _
						 , PiutangUsahaAsetTidakLancar _
						 , InvestasiLainnyaAsetTidakLancar _
						 , AsetTidakLancarLainnya _
						 , Liabilitas _
						 , LiabilitasJangkaPendek _
						 , PinjamanJangkaPendek _
						 , UtangUsahaJangkaPendek _
						 , LiabilitasJangkaPendekLainnya _
						 , LiabilitasJangkaPanjang _
						 , PinjamanJangkaPanjang _
						 , UtangUsahaJangkaPanjang _
						 , LiabilitasJangkaPanjangLainnya _
						 , Ekuitas _
						 , PendapatanUsahaOpr _
						 , BebanPokokPendapatanOpr _
						 , PLLNonOpr _
						 , BebanLainLainNonOpr _
						 As ucNumberFormat
	Dim status As Boolean
	Dim Style As String = "ACCACQ"
	Private m_controllerApp As New ApplicationController
	Private ocustomclass As New Parameter.Application

	Private currentPage As Int32 = 1
	Private pageSize As Int16 = 10
	Private currentPageNumber As Int16 = 1
	Private totalPages As Double = 1
	Private recordCount As Int64 = 1

	Property DataTable() As DataTable
		Get
			Return ViewState("DataTable")
		End Get
		Set(ByVal Value As DataTable)
			ViewState("DataTable") = Value
		End Set
	End Property
	Property SeqNo() As String
		Get
			Return ViewState("SeqNo")
		End Get
		Set(ByVal Value As String)
			ViewState("SeqNo") = Value
		End Set
	End Property

	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblMessage.Visible = False
		If SessionInvalid() Then
			Exit Sub
		End If

		If Not Page.IsPostBack Then

			Me.PageAddEdit = Request("page")
			If Me.PageAddEdit = "Add" Then
				lblTitle.Text = "ADD"
			Else
				lblTitle.Text = "EDIT"
				Me.CustomerID = Request("id")
				Dim SUCCES As String = Request("lblMessage")

				If SUCCES = "SUCCESSEDIT" Then
					ShowMessage(lblMessage, "Data Berhasil diEdit", False)
				ElseIf SUCCES = "SUCCESSADD" Then
					ShowMessage(lblMessage, "Data Berhasil diTambah", False)
				End If

				DoBind(Me.SearchBy, Me.SortBy)

				Dim intTotalCustomer As Integer
				intTotalCustomer = m_controller.GetTotalCustomer(Me.CustomerID.Trim, GetConnectionString)

				cTabs.SetNavigateUrl(Request("page"), Request("id"))
			End If
			cTabs.RefreshAttr(Request("pnl"))
		End If
		pnlAdd.Visible = False
	End Sub
	Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
		If e.CommandName = "Edit" Then
			Me.SeqNo = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
			Me.PageAddEdit = "Edit"
			BindEdit(Me.CustomerID, Me.SeqNo)
		End If
	End Sub
#Region "Edit"
	Sub BindEdit(ByVal CustomerId As String, ByVal SeqNo As String)
		Dim oCustomClass As New Parameter.CompanyCustomerPK
		Dim ListData As DataTable = Nothing
		Dim oRow As DataRow

		oCustomClass.CustomerId = Me.CustomerID
		oCustomClass.SeqNo = Me.SeqNo
		oCustomClass = m_controller.GetCompanyCustomerPK(GetConnectionString(), oCustomClass)

		ListData = oCustomClass.Listdata
		Me.DataTable = ListData

		If ListData.Rows.Count > 0 Then
			oRow = ListData.Rows(0)

			TglProyeksi.Text = Format(oRow("TglLaporanKeuangan"), "dd/MM/yyyy").ToString
			AssetIDR.Text = FormatNumber(oRow("AssetIDR").ToString.Trim, 0)
			AssetLancar.Text = FormatNumber(oRow("AssetLancar").ToString.Trim, 0)
			KasSetaraKasAsetLancar.Text = FormatNumber(oRow("KasSetaraKasAsetLancar").ToString.Trim, 0)
			PiutangUsahAsetLancar.Text = FormatNumber(oRow("PiutangUsahAsetLancar").ToString.Trim, 0)
			InvestasiLainnyaAsetLancar.Text = FormatNumber(oRow("InvestasiLainnyaAsetLancar").ToString.Trim, 0)
			AsetLancarLainnya.Text = FormatNumber(oRow("AsetLancarLainnya").ToString.Trim, 0)
			AsetTidakLancar.Text = FormatNumber(oRow("AsetTidakLancar").ToString.Trim, 0)
			PiutangUsahaAsetTidakLancar.Text = FormatNumber(oRow("PiutangUsahaAsetTidakLancar").ToString.Trim, 0)
			InvestasiLainnyaAsetTidakLancar.Text = FormatNumber(oRow("InvestasiLainnyaAsetTidakLancar").ToString.Trim, 0)
			AsetTidakLancarLainnya.Text = FormatNumber(oRow("AsetTidakLancarLainnya").ToString.Trim, 0)
			Liabilitas.Text = FormatNumber(oRow("Liabilitas").ToString.Trim, 0)
			LiabilitasJangkaPendek.Text = FormatNumber(oRow("LiabilitasJangkaPendek").ToString.Trim, 0)
			PinjamanJangkaPendek.Text = FormatNumber(oRow("PinjamanJangkaPendek").ToString.Trim, 0)
			UtangUsahaJangkaPendek.Text = FormatNumber(oRow("UtangUsahaJangkaPendek").ToString.Trim, 0)
			LiabilitasJangkaPendekLainnya.Text = FormatNumber(oRow("LiabilitasJangkaPendekLainnya").ToString.Trim, 0)
			LiabilitasJangkaPanjang.Text = FormatNumber(oRow("LiabilitasJangkaPanjang").ToString.Trim, 0)
			PinjamanJangkaPanjang.Text = FormatNumber(oRow("PinjamanJangkaPanjang").ToString.Trim, 0)
			UtangUsahaJangkaPanjang.Text = FormatNumber(oRow("UtangUsahaJangkaPanjang").ToString.Trim, 0)
			LiabilitasJangkaPanjangLainnya.Text = FormatNumber(oRow("LiabilitasJangkaPanjangLainnya").ToString.Trim, 0)
			Ekuitas.Text = FormatNumber(oRow("Ekuitas").ToString.Trim, 0)
			PendapatanUsahaOpr.Text = FormatNumber(oRow("PendapatanUsahaOpr").ToString.Trim, 0)
			BebanPokokPendapatanOpr.Text = FormatNumber(oRow("BebanPokokPendapatanOpr").ToString.Trim, 0)
			txtLabaRugiBruto.Text = FormatNumber(oRow("LabaRugiBruto").ToString.Trim, 0)
			PLLNonOpr.Text = FormatNumber(oRow("PLLNonOpr").ToString.Trim, 0)
			BebanLainLainNonOpr.Text = FormatNumber(oRow("BebanLainLainNonOpr").ToString.Trim, 0)
			txtLabaRugiSebelumPajak.Text = FormatNumber(oRow("LabaRugiSebelumPajak").ToString.Trim, 0)
			txtLabaRugiTahunBerjalan.Text = FormatNumber(oRow("LabaRugiTahunBerjalan").ToString.Trim, 0)
		End If
		pnlAdd.Visible = True
		pnlList.Visible = False
	End Sub
	Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
		Dim dtEntity As DataTable = Nothing
		Dim oParameter As New Maxiloan.Parameter.GeneralPaging
		Dim oController As New ImplementasiControler

		cmdWhere = " CUSTOMERID = '" & Me.CustomerID & "'"

		oParameter.strConnection = GetConnectionString()
		oParameter.WhereCond = cmdWhere
		oParameter.CurrentPage = currentPage
		oParameter.PageSize = pageSize
		oParameter.SortBy = ""
		oParameter.SpName = "spCompanyCustomerAPKPaging"
		oParameter = oController.GetGeneralPaging(oParameter)

		If Not oParameter Is Nothing Then
			dtEntity = oParameter.ListData
			recordCount = oParameter.TotalRecords
		Else
			recordCount = 0
		End If

		dtgList.DataSource = dtEntity.DefaultView
		dtgList.CurrentPageIndex = 0
		dtgList.DataBind()

		If (isFrNav = False) Then
			GridNav.Initialize(recordCount, pageSize)
		End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data Analisa Proyeksi Keuangan Costumer Belum Pernah Diinput!", True)
		End If
	End Sub

	Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
		currentPage = e.CurrentPage
		DoBind(Me.SearchBy, Me.SortBy, True)
		GridNav.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
	End Sub

#Region "Sort"
	Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
		If InStr(Me.SortBy, "DESC") > 0 Then
			Me.SortBy = e.SortExpression
		Else
			Me.SortBy = e.SortExpression + " DESC"
		End If
		DoBind(Me.SearchBy, Me.SortBy)
	End Sub
#End Region

	Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
		Dim oCustomClass As New Parameter.CompanyCustomerPK

		gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
		gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

		With oCustomClass
			.CustomerId = Me.CustomerID
			.TglLaporanKeuangan = ConvertDate2(TglProyeksi.Text)
			.AssetIDR = AssetIDR.Text
			.AssetLancar = AssetLancar.Text
			.KasSetaraKasAsetLancar = KasSetaraKasAsetLancar.Text
			.PiutangUsahAsetLancar = PiutangUsahAsetLancar.Text
			.InvestasiLainnyaAsetLancar = InvestasiLainnyaAsetLancar.Text
			.AsetLancarLainnya = AsetLancarLainnya.Text
			.AsetTidakLancar = AsetTidakLancar.Text
			.PiutangUsahaAsetTidakLancar = PiutangUsahaAsetTidakLancar.Text
			.InvestasiLainnyaAsetTidakLancar = InvestasiLainnyaAsetTidakLancar.Text
			.AsetTidakLancarLainnya = AsetTidakLancarLainnya.Text
			.Liabilitas = Liabilitas.Text
			.LiabilitasJangkaPendek = LiabilitasJangkaPendek.Text
			.PinjamanJangkaPendek = PinjamanJangkaPendek.Text
			.UtangUsahaJangkaPendek = UtangUsahaJangkaPendek.Text
			.LiabilitasJangkaPendekLainnya = LiabilitasJangkaPendekLainnya.Text
			.LiabilitasJangkaPanjang = LiabilitasJangkaPanjang.Text
			.PinjamanJangkaPanjang = PinjamanJangkaPanjang.Text
			.UtangUsahaJangkaPanjang = UtangUsahaJangkaPanjang.Text
			.LiabilitasJangkaPanjangLainnya = LiabilitasJangkaPanjangLainnya.Text
			.Ekuitas = Ekuitas.Text
			.PendapatanUsahaOpr = PendapatanUsahaOpr.Text
			.BebanPokokPendapatanOpr = BebanPokokPendapatanOpr.Text
			.LabaRugiBruto = txtLabaRugiBruto.Text
			.PLLNonOpr = PLLNonOpr.Text
			.BebanLainLainNonOpr = BebanLainLainNonOpr.Text
			.LabaRugiSebelumPajak = txtLabaRugiSebelumPajak.Text
			.LabaRugiTahunBerjalan = txtLabaRugiTahunBerjalan.Text
			.SeqNo = Me.SeqNo
			End With

			oCustomClass.strConnection = GetConnectionString()

			If Me.PageAddEdit = "Add" Then
				oCustomClass = m_controller.CompanyCustomerPKSaveAdd(oCustomClass)
				If oCustomClass.Err = "" Then
					If File.Exists(gStrPath & gStrFileName) Then
						File.Delete(gStrPath & gStrFileName)
					End If
					Response.Redirect("CustomerCompanyProyeksiKeuangan.aspx?page=Add&id=" & Me.CustomerID & "&pnl=tabAnalisaProyeksiKeuangan&lblMessage=SUCCESSADD")
				Else
					ShowMessage(lblMessage, oCustomClass.Err, True)
					Exit Sub
				End If
			ElseIf Me.PageAddEdit = "Edit" Then
				oCustomClass = m_controller.CompanyCustomerPKSaveEdit(oCustomClass)
				If oCustomClass.Err = "" Then
					If File.Exists(gStrPath & gStrFileName) Then
						File.Delete(gStrPath & gStrFileName)
					End If
					Response.Redirect("CustomerCompanyProyeksiKeuangan.aspx?page=Edit&id=" & Me.CustomerID.Trim & "&pnl=tabAnalisaProyeksiKeuangan&lblMessage=SUCCESSEDIT")
				Else
					ShowMessage(lblMessage, oCustomClass.Err, True)
					Exit Sub
				End If
				'End If
			End If
    End Sub
	Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
		pnlAdd.Visible = True
		pnlList.Visible = False
		Me.PageAddEdit = "Add"
	End Sub
	Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
		If File.Exists(gStrPath & gStrFileName) Then
			File.Delete(gStrPath & gStrFileName)
		End If
		Response.Redirect("Customer.aspx")
	End Sub
#End Region

End Class