﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CustomerTabsCompany.ascx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerTabsCompany" %>
 <div class="tab_container">
        <div id="tabDataKonsumen" runat="server">
            <asp:HyperLink ID="hyDataKonsumen" runat="server" Text="DATA KONSUMEN"></asp:HyperLink>
        </div>
        <div id="tabLegalitas" runat="server">
            <asp:HyperLink ID="hyLegalitas" runat="server" Text="LEGALITAS"></asp:HyperLink>
        </div>
        <div id="tabManagement" runat="server">
            <asp:HyperLink ID="hyManagement" runat="server" Text="MANAGEMENT"></asp:HyperLink>
        </div> 

         <div id="tabPenjamin" runat="server">
            <asp:HyperLink ID="hyPenjamin" runat="server" Text="PENJAMIN"></asp:HyperLink>
        </div> 

         <div id="tabEmergencyCt" runat="server">
            <asp:HyperLink ID="hyEmergencyCt" runat="server" Text="EMERGENCY CONTACT"></asp:HyperLink>
        </div> 
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>     
        <div id="tabAnalisaProyeksiKeuangan" runat="server">
            <asp:HyperLink ID="hyAnalisaProyeksiKeuangan" runat="server" Text="ANALISA PROYEKSI KEUANGAN"></asp:HyperLink>
        </div> 
    </div> 