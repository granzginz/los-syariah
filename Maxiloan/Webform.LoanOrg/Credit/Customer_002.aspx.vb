﻿#Region "Imports"
Imports System.IO
Imports Maxiloan
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class Customer_002
    Inherits Maxiloan.Webform.WebBased

    Private gStrPath As String
    Private gStrFileName As String
    Private m_controller As New CustomerController

#Region "Property"
    Private Property ProspectAppID() As String
        Get
            Return CType(viewstate("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ProspectAppID") = Value
        End Set
    End Property
    Property PC() As String
        Get
            Return CType(viewstate("PC"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("PC") = Value
        End Set
    End Property
    Property CName() As String
        Get
            Return CType(viewstate("CName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CName") = Value
        End Set
    End Property
    Property CNPWP() As String
        Get
            Return CType(viewstate("CNPWP"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CNPWP") = Value
        End Set
    End Property
    Property Name() As String
        Get
            Return CType(viewstate("Name"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Name") = Value
        End Set
    End Property
    Property GelarDepan() As String
        Get
            Return CType(ViewState("GelarDepan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GelarDepan") = Value
        End Set
    End Property

    Property GelarBelakang() As String
        Get
            Return CType(ViewState("GelarBelakang"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GelarBelakang") = Value
        End Set
    End Property
    Property IDType() As String
        Get
            Return CType(viewstate("IDType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("IDType") = Value
        End Set
    End Property
    Property IDTypeName() As String
        Get
            Return CType(viewstate("IDTypeName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("IDTypeName") = Value
        End Set
    End Property
    Property IDNumber() As String
        Get
            Return CType(viewstate("IDNumber"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("IDNumber") = Value
        End Set
    End Property
    Property BirthDate() As String
        Get
            Return CType(viewstate("BirthDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BirthDate") = Value
        End Set
    End Property
    Property MotherName() As String
        Get
            Return CType(ViewState("MotherName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MotherName") = Value
        End Set
    End Property
    Property NoKK() As String
        Get
            Return CType(ViewState("NoKK"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoKK") = Value
        End Set
    End Property
    Property NamaSuamiIstri() As String
        Get
            Return CType(ViewState("NamaSuamiIstri"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NamaSuamiIstri") = Value
        End Set
    End Property
    Property StatusPerkawinan() As String
        Get
            Return CType(ViewState("StatusPerkawinan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("StatusPerkawinan") = Value
        End Set
    End Property
    Property Pendidikan() As String
        Get
            Return CType(ViewState("Pendidikan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Pendidikan") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.PC = Request("pc")
            Me.branchid = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
            If Me.PC = "p" Then
                pnlPersonal.Visible = True
                pnlCompany.Visible = False
                FillCbo()
                GetXMLP()
                BindGrid1()
                BindGrid2()
            ElseIf Me.PC = "c" Then
                pnlPersonal.Visible = False
                pnlCompany.Visible = True
                GetXMLC()
                BindGridC1()
                BindGridC2()
            End If
        End If
        If Me.PC = "p" Then
            If dtgList1.Items.Count = 0 And dtgList2.Items.Count = 0 Then
                Response.Redirect("CustomerPersonal.aspx?page=Add&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
            End If
        ElseIf Me.PC = "c" Then
            If dtgCList1.Items.Count = 0 And dtgCList2.Items.Count = 0 Then
                Response.Redirect("CustomerCompany.aspx?page=Add&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
            End If
        End If
    End Sub

    Private Sub imbCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("Customer.aspx")
    End Sub
#Region "XML"
    Sub GetXMLP()
        If File.Exists(gStrPath & gStrFileName) Then
            Me.Name = Get_XMLValue(gStrPath & gStrFileName, "CustName").ToUpper
            lblName.Text = Me.Name
            Me.IDType = Get_XMLValue(gStrPath & gStrFileName, "CustIDType")
            Me.IDTypeName = Get_XMLValue(gStrPath & gStrFileName, "CustIDTypeName")
            lblIDType.Text = Me.IDTypeName
            Me.IDNumber = Get_XMLValue(gStrPath & gStrFileName, "CustNumber").ToUpper
            lblIDNumber.Text = Me.IDNumber
            Me.BirthDate = Get_XMLValue(gStrPath & gStrFileName, "CustBirthDate")
            lblBirthDate.Text = Me.BirthDate
            Me.MotherName = Get_XMLValue(gStrPath & gStrFileName, "MotherName").ToUpper
            lblNamaIbuKandung.Text = Me.MotherName
            Me.NoKK = Get_XMLValue(gStrPath & gStrFileName, "KartuKeluarga").ToUpper
            lblNoKK.Text = Me.NoKK
            Me.NamaSuamiIstri = Get_XMLValue(gStrPath & gStrFileName, "NamaPasangan").ToUpper
            lblNamaSI.Text = Me.NamaSuamiIstri
            Me.StatusPerkawinan = Get_XMLValue(gStrPath & gStrFileName, "StatusPerkawinan").ToUpper
            lblStatusPerkawinan.Text = cboPMarital.Items.FindByValue(Me.StatusPerkawinan.Trim).Text
            'Me.Pendidikan = Get_XMLValue(gStrPath & gStrFileName, "Pendidikan").ToUpper
            'lblPendidikan.Value = Me.GelarBelakang
        End If
    End Sub
    Sub GetXMLC()
        gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
        If File.Exists(gStrPath & gStrFileName) Then
            Me.CName = Get_XMLValue(gStrPath & gStrFileName, "Name")
            lblCName.Text = Me.CName
            Me.CNPWP = Get_XMLValue(gStrPath & gStrFileName, "NPWP")
            lblCNPWP.Text = Me.CNPWP
        End If
    End Sub
    Private Function Get_XMLValue(ByVal pStrFile As String, ByVal pStrColumn As String) As String
        Dim lObjDSCust As New DataSet
        Dim lStrName As String
        Try
            lObjDSCust.ReadXml(pStrFile)
            lStrName = lObjDSCust.Tables("Cust").Rows(0)("" & pStrColumn & "").ToString
        Catch ex As Exception
            Throw New Exception("Error On Get_XMLValue")
        Finally
            lObjDSCust = Nothing
        End Try
        Return lStrName
    End Function
#End Region
#Region "Bind Personal"
    Sub BindGrid1()
        Dim oCustomer As New Parameter.Customer
        Dim dtEntity As New DataTable
        oCustomer.Name = Me.Name
        oCustomer.IDType = Me.IDType
        oCustomer.IDNumber = Me.IDNumber
        oCustomer.BirthDate = ConvertDate(Me.BirthDate)
        oCustomer.strConnection = GetConnectionString()
        oCustomer.MotherName = Me.MotherName
        oCustomer.NoKK = Me.NoKK
        oCustomer.MaritalStatus = Me.StatusPerkawinan


        Dim SIdata As New Parameter.CustomerSI
        SIdata.NamaSuamiIstri = Me.NamaSuamiIstri

        oCustomer = m_controller.BindCustomer1_002_withSI(oCustomer, SIdata)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        dtgList1.DataSource = dtEntity.DefaultView
        dtgList1.CurrentPageIndex = 0
        dtgList1.DataBind()
    End Sub
    Sub BindGrid2()
        Dim oCustomer As New Parameter.Customer
        Dim dtEntity As New DataTable
        oCustomer.Name = Me.Name
        oCustomer.IDType = Me.IDType
        oCustomer.IDNumber = Me.IDNumber
        oCustomer.BirthDate = ConvertDate(Me.BirthDate)
        oCustomer.strConnection = GetConnectionString
        oCustomer = m_controller.BindCustomer2_002(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
            For Each r As DataRow In dtEntity.Rows
                If r.Item("type").ToString.Trim = "Name + Birth Date + ID Number" Then
                    btnNext.Visible = False
                End If
            Next
        End If
        dtgList2.DataSource = dtEntity.DefaultView
        dtgList2.CurrentPageIndex = 0
        dtgList2.DataBind()


    End Sub
#End Region
#Region "Bind Company"
    Sub BindGridC1()
        Dim oCustomer As New Parameter.Customer
        Dim dtEntity As New DataTable
        oCustomer.Name = Me.CName
        oCustomer.PersonalNPWP = Me.CNPWP
        oCustomer.IDNumber = Me.IDNumber
        oCustomer.BirthDate = ConvertDate(Me.BirthDate)
        oCustomer.strConnection = GetConnectionString
        oCustomer = m_controller.BindCustomerC1_002(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        dtgCList1.DataSource = dtEntity.DefaultView
        dtgCList1.CurrentPageIndex = 0
        dtgCList1.DataBind()
    End Sub
    Sub BindGridC2()
        Dim oCustomer As New Parameter.Customer
        Dim dtEntity As New DataTable
        oCustomer.Name = Me.CName
        oCustomer.PersonalNPWP = Me.CNPWP
        oCustomer.strConnection = GetConnectionString
        oCustomer = m_controller.BindCustomerC2_002(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        dtgCList2.DataSource = dtEntity.DefaultView
        dtgCList2.CurrentPageIndex = 0
        dtgCList2.DataBind()
    End Sub
#End Region
    Sub FillCbo()
        Dim dtEntity As New DataTable
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblMaritalStatus"
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        cboPMarital.DataSource = dtEntity.DefaultView
        cboPMarital.DataTextField = "Description"
        cboPMarital.DataValueField = "ID"
        cboPMarital.DataBind()
        cboPMarital.Items.Insert(0, "Select One")
        cboPMarital.Items(0).Value = ""
    End Sub
    Private Sub imbNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Response.Redirect("CustomerPersonal.aspx?page=Add&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
    End Sub

    Private Sub dtgList1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList1.ItemCommand
        Try
            If e.CommandName = "NewApp" Then
                Dim customerType As String = "P"
                'If CheckFeature(Me.Loginid, "Application", "Add", Me.AppId) Then
                '    If SessionInvalid() Then
                '        Exit Sub
                '    End If
                'End If
                Response.Redirect("CreditProcess/NewApplication/Application_003.aspx?id=" & e.CommandArgument.ToString & "&name=" & Me.Name & "&type=" & customerType & "&page=Add&flag=0")
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub dtgList1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList1.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo1"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkCustName"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','AccAcq');")
        End If
    End Sub

    Private Sub dtgList2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkCustName2"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCust2"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','AccAcq');")
        End If
    End Sub

    Private Sub imbCCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub

    Private Sub imbCNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCNext.Click
        Response.Redirect("CustomerCompany.aspx?page=Add&branchID=" & Me.BranchID & "&prospectappid=" & Me.ProspectAppID)
    End Sub

    Private Sub dtgCList1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgCList1.ItemCommand
        Try
            If e.CommandName = "NewApp" Then
                Dim customerType As String = "C"
                'If CheckFeature(Me.Loginid, "Application", "Add", Me.AppId) Then
                '    If SessionInvalid() Then
                '        Exit Sub
                '    End If
                'End If
                Response.Redirect("CreditProcess/NewApplication/Application_003.aspx?id=" & e.CommandArgument.ToString & "&name=" & Me.Name & "&type=" & customerType & "&page=Add&flag=0")
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

    Private Sub dtgCList1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCList1.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblCNo1"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkCName1"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCName1"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','AccAcq');")
        End If
    End Sub

    Private Sub dtgCList2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCList2.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblCNo2"), Label).Text = CStr(e.Item.ItemIndex + 1)
            Dim lnkCustomer As LinkButton
            Dim lblCustomer As Label
            lnkCustomer = CType(e.Item.FindControl("lnkCName2"), LinkButton)
            lblCustomer = CType(e.Item.FindControl("lblCName2"), Label)
            lnkCustomer.Attributes.Add("OnClick", "return OpenCust('" & lblCustomer.Text & "','AccAcq');")
        End If
    End Sub
End Class