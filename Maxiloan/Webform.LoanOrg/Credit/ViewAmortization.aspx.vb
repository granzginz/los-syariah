﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

#End Region

Public Class ViewAmortization
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New AssetDataController
#End Region

#Region "Property"
    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property

#End Region

    Dim tempDenda As Double = 0
    Dim tempInstallmentAmount As Double = 0
    Dim tempPaidAmount As Double = 0
    Dim tempWaivedAmount As Double = 0
    Dim tempLC As Double = 0
    Dim tempPrincipalAmount As Double = 0
    Dim tempInterestAmount As Double = 0
    Dim tempOSPrincipal As Double = 0
    Dim tempOSInterest As Double = 0

    Dim tempPaidPrincipal As Double = 0
    Dim tempPaidInterest As Double = 0
    Dim tempPaidLateCharges As Double = 0

    Dim TotDenda As Label
    Dim TotInstallmentAmount As Label
    Dim TotPaidAmount As Label
    Dim TotWaivedAmount As Label
    Dim TotLC As Label
    Dim TotPrincipalAmount As Label
    Dim TotInterestAmount As Label
    Dim TotOSPrincipal As Label
    Dim TotOSInterest As Label

    Dim totPaidPrincipal As Label
    Dim totPaidInterest As Label
    Dim totPaidLateCharges As Label



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.ApplicationID = Request("ApplicationID").ToString
            Me.AgreementNo = Request("AgreementNo").ToString
            lblAgreementNo.Text = Request("AgreementNo").ToString
            Me.CustomerName = Request("CustomerName").ToString
            lblCustomerName.Text = Me.CustomerName
            Me.Style = Request("Style").ToString
            Me.CustomerID = Request("CustomerID").ToString.Trim
            Bindgrid()

            Dim lb As New LinkButton
            lb = CType(Me.FindControl("lblCustomerName"), LinkButton)
            lb.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")
        End If

    End Sub

    Sub Bindgrid()
        Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)

        'Dim adapter As New SqlDataAdapter
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAmortization"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objReader = objCommand.ExecuteReader
            'adapter.SelectCommand = objCommand
            'adapter.Fill(dtGrid)

            dtg.DataSource = objReader
            dtg.DataBind()
            objReader.Close()
            'If objConnection.State = ConnectionState.Open Then objConnection.Close()
            'objCommand = New SqlCommand
            'If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.Parameters.Clear()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAmortization"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Parameters.Add("@BusinessDate", SqlDbType.DateTime).Value = Me.BusinessDate
            objReader = objCommand.ExecuteReader

            If objReader.Read Then
                lblOSPrincipal.Text = FormatNumber(objReader.Item("OSPrincipal"), 2)
                lblOSInterest.Text = FormatNumber(objReader.Item("OSInterest"), 2)
                lblOSPrincipalUndue.Text = FormatNumber(objReader.Item("OutStandingPrincipalUndue"), 2)
                lblOSInterestUnDue.Text = FormatNumber(objReader.Item("OutStandingInterestUndue"), 2)
                lblOSInstallmentDue.Text = FormatNumber(objReader.Item("OSInstallmentDue"), 2)
                lblOSInsuranceDue.Text = FormatNumber(objReader.Item("OSInsuranceDue"), 2)
                lblLCInstallment.Text = FormatNumber(objReader.Item("OSLCInstallment"), 2)
                lblOSLCInsurance.Text = FormatNumber(objReader.Item("OSLCInsurance"), 2)
                lblOSInstallCollectionFee.Text = FormatNumber(objReader.Item("OSInstallCollectionFee"), 2)
                lblOSInsuranceCollFee.Text = FormatNumber(objReader.Item("OSInsuranceCollectionFee"), 2)
                lblOSPDCBounceFee.Text = FormatNumber(objReader.Item("OSPDCBounceFee"), 2)
                lblOSSTNKFee.Text = FormatNumber(objReader.Item("OSSTNKFee"), 2)
                lblOSInsuranceClaimExpense.Text = FormatNumber(objReader.Item("OSInsuranceClaimExpense"), 2)
                lblOSCollExpense.Text = FormatNumber(objReader.Item("OSCollExpense"), 2)
                lblContactPrepaidAmt.Text = FormatNumber(objReader.Item("ContractPrepaidAmt"), 2)
                lblTotalAmountToBePaid.Text = FormatNumber(objReader.Item("TotalAmountToBePaid"), 2)
                lblNextInstallmentNumber.Text = CStr(objReader.Item("NextInstallmentNumber"))
                lblNextInstallmentDate.Text = CStr(objReader.Item("NextInstallmentDate"))
                lblNextInstallmentDueNumber.Text = CStr(objReader.Item("NextInstallmentDueNumber"))
                lblNextInstallmentDueDate.Text = CStr(objReader.Item("NextInstallmentDueDate"))
            End If
            objReader.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            objCommand.Dispose()
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
        End Try
    End Sub

    Private Sub dtg_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtg.ItemDataBound
        Dim lb As New Label

        If e.Item.ItemIndex >= 0 Then
            ' CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)

            'isi total 
            'lb = CType(e.Item.FindControl("lblInterestAmount"), Label)
            'lblInstallmentAmount
            lb = CType(e.Item.FindControl("lblDenda"), Label)
            tempDenda = tempDenda + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblInstallmentAmount"), Label)
            tempInstallmentAmount = tempInstallmentAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPaidAmount"), Label)
            tempPaidAmount = tempPaidAmount + CDbl(lb.Text)

            'lb = CType(e.Item.FindControl("lblWaivedAmount"), Label)
            'tempWaivedAmount = tempWaivedAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPaidPrincipal"), Label)
            tempPaidPrincipal = tempPaidPrincipal + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPaidInterest"), Label)
            tempPaidInterest = tempPaidInterest + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPaidLateCharges"), Label)
            tempPaidLateCharges = tempPaidLateCharges + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblPrincipalAmount"), Label)
            tempPrincipalAmount = tempPrincipalAmount + CDbl(lb.Text)

            lb = CType(e.Item.FindControl("lblInterestAmount"), Label)
            tempInterestAmount = tempInterestAmount + CDbl(lb.Text)

            'lb = CType(e.Item.FindControl("lblDtgOSPrincipal"), Label)
            'tempOSPrincipal = tempOSPrincipal + CDbl(lb.Text)

            'lb = CType(e.Item.FindControl("lblDtgOSInterest"), Label)
            'tempOSInterest = tempOSInterest + CDbl(lb.Text)

        End If

        If e.Item.ItemType = ListItemType.Footer Then
            TotDenda = CType(e.Item.FindControl("lblTotDenda"), Label)
            TotDenda.Text = FormatNumber(tempDenda, 2)

            TotInstallmentAmount = CType(e.Item.FindControl("lblTotInstallmentAmount"), Label)
            TotInstallmentAmount.Text = FormatNumber(tempInstallmentAmount, 2)

            TotPaidAmount = CType(e.Item.FindControl("lblTotPaidAmount"), Label)
            TotPaidAmount.Text = FormatNumber(tempPaidAmount, 2)

            'TotWaivedAmount = CType(e.Item.FindControl("lblTotWaivedAmount"), Label)
            'TotWaivedAmount.Text = FormatNumber(tempWaivedAmount, 2)

            'TotLC = CType(e.Item.FindControl("lblTotLC"), Label)
            'TotLC.Text = FormatNumber(tempLC, 2)

            TotPrincipalAmount = CType(e.Item.FindControl("lblTotPrincipalAmount"), Label)
            TotPrincipalAmount.Text = FormatNumber(tempPrincipalAmount, 2)

            TotInterestAmount = CType(e.Item.FindControl("lblTotInterestAmount"), Label)
            TotInterestAmount.Text = FormatNumber(tempInterestAmount, 2)

            totPaidPrincipal = CType(e.Item.FindControl("lblTotPaidPrincipal"), Label)
            totPaidPrincipal.Text = FormatNumber(tempPaidPrincipal, 2)

            totPaidInterest = CType(e.Item.FindControl("lblTotPaidInterest"), Label)
            totPaidInterest.Text = FormatNumber(tempPaidInterest, 2)

            totPaidLateCharges = CType(e.Item.FindControl("lblTotPaidLateCharges"), Label)
            totPaidLateCharges.Text = FormatNumber(tempPaidLateCharges, 2)

            'TotOSPrincipal = CType(e.Item.FindControl("lblTotOSPrincipal"), Label)
            'TotOSPrincipal.Text = FormatNumber(tempOSPrincipal, 2)

            'TotOSInterest = CType(e.Item.FindControl("lblTotOSInterest"), Label)
            'TotOSInterest.Text = FormatNumber(tempOSInterest, 2)

        End If
    End Sub

    'Private Sub btnReport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReport.Click
    '    'If checkfeature(Me.Loginid, Me.FormID, "VIEW", "MAXILOAN") Then
    '    Dim cookie As HttpCookie = Request.Cookies(COOKIES_AMORTIZATION)
    '    Dim cmdwhere As String
    '    Dim filterBy As String
    '    If Not cookie Is Nothing Then
    '        cookie.Values("ApplicationID") = Me.ApplicationID
    '        cookie.Values("CustomerName") = Me.CustomerName
    '        cookie.Values("AgreementNo") = Me.AgreementNo
    '        cookie.Values("CustomerID") = Me.CustomerID
    '        Response.AppendCookie(cookie)
    '    Else
    '        Dim cookieNew As New HttpCookie(COOKIES_AMORTIZATION)
    '        cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
    '        cookieNew.Values.Add("CustomerName", Me.CustomerName)
    '        cookieNew.Values.Add("AgreementNo", Me.AgreementNo)
    '        cookieNew.Values.Add("CustomerID", Me.CustomerID)
    '        Response.AppendCookie(cookieNew)
    '    End If
    '    Response.Redirect("RptAmortizationViewer.aspx")
    '    'End If
    'End Sub

    'Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
    '    Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    'End Sub
End Class