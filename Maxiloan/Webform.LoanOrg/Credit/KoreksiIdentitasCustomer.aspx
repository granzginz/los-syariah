﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="KoreksiIdentitasCustomer.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.KoreksiIdentitasCustomer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Koreksi Identitas Customer</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function cboPMarital_IndexChanged(e) {
            if (e == 'M') {
                $('#divSuamiIstri').css("visibility", "");
                $('#txtNamaPasangan').removeAttr("style");

                ValidatorEnable(rfvNamaPasangan, true);
//                $('#rfvNamaPasangan').show();

                ValidatorEnable(revNamaPasangan, true);
                $('#revNamaPasangan').hide();
            }
            else {
                $('#divSuamiIstri').css("visibility", "hidden");
                $('#txtNamaPasangan').attr("style", "display : none");

                ValidatorEnable(rfvNamaPasangan, false);
                $('#rfvNamaPasangan').hide();

                ValidatorEnable(revNamaPasangan, false);
                $('#revNamaPasangan').hide();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                KOREKSI CUSTOMER
            </h3>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <h4>
                KOREKSI - PERSONAL CUSTOMER
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Nama</label>
            <asp:TextBox ID="txtNameP" runat="server" MaxLength="50" CssClass="medium_text"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi Nama"
                ControlToValidate="txtNameP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNameP" CssClass="validator_general"
                ID="RegularExpressionValidator2" ValidationExpression="^[A-Za-z-e.f ]{3,50}$"
                runat="server" ErrorMessage="Input huruf saja dan minimal 3 huruf!"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Jenis Identitas</label>
            <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Jenis Identitas"
                ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Nomor Identitas</label>
            <asp:TextBox ID="txtIDNumberP" runat="server" MaxLength="25" CssClass="medium_text"
                onkeypress="return numbersonly2(event)"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegEx1" ControlToValidate="txtIDNumberP" runat="server"
                ErrorMessage="Masukkan angka saja" ValidationExpression="\d+" CssClass="validator_general"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap isi No Identitas"
                ControlToValidate="txtIDNumberP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
            <label class="label_auto">
                Berlaku s/d</label>
            <uc2:ucdatece id="txtIDTglBerlakuNew" runat="server"></uc2:ucdatece>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Jenis Kelamin</label>
            <asp:Dropdownlist ID="cboGenderP" runat="server" CssClass="select">
                <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                <asp:ListItem Value="M">Laki-Laki</asp:ListItem>
                <asp:ListItem Value="F">Perempuan</asp:ListItem>
            </asp:Dropdownlist>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Isi Jenis Kelamin!"
                ControlToValidate="cboGenderP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Tempat / Tanggal Lahir</label>
            <asp:TextBox ID="txtBirthPlaceP" runat="server" MaxLength="20" CssClass="medium_text"></asp:TextBox>
            <label class="label_auto">
                /</label>
            <uc2:ucdatece id="txtTglLahirNew" runat="server"></uc2:ucdatece>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Nama Ibu Kandung</label>
            <asp:TextBox ID="txtPMotherName" runat="server" Width="30%" MaxLength="50"></asp:TextBox>
            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtPMotherName"
                CssClass="validator_general" ID="RegularExpressionValidator1" ValidationExpression="^[A-Za-z ]{3,50}$"
                runat="server" ErrorMessage="Input huruf saja dan minimal 3 huruf!"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="rfvPMotherName" runat="server" Display="Dynamic"
                ControlToValidate="txtPMotherName" ErrorMessage="Harap isi dengan Nama Ibu Kandung"
                CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_req">
                Status Perkawinan</label>
            <asp:DropDownList ID="cboPMarital" onchange="cboPMarital_IndexChanged(this.value);"
                runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                InitialValue="" ControlToValidate="cboPMarital" ErrorMessage="*" CssClass="validator_general"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div runat="server" id="divSuamiIstri">
                <label class="label_req">
                    Nama Suami/Istri</label>
                <asp:TextBox runat="server" ID="txtNamaPasangan" CssClass="medium_text"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvNamaPasangan" runat="server" ControlToValidate="txtNamaPasangan"
                    ErrorMessage="Harap diisi nama pasangan!" CssClass="validator_general" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNamaPasangan"
                    CssClass="validator_general" ID="revNamaPasangan" ValidationExpression="^[A-Za-z ]{3,50}$"
                    runat="server" ErrorMessage="Input huruf saja dan minimal 3 huruf!"></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>

        <%--edit npwp, ktp, telepon by ario--%>
     <div class="form_box">
        <div class="form_single">
                        <label class="label_req">
                            NPWP Pribadi</label>
                        <asp:TextBox ID="txtPNPWP" runat="server" Width="30%" MaxLength="25"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegtxtPNPWP" Display="Dynamic" ControlToValidate="txtPNPWP"
                            runat="server" ErrorMessage="Masukkan angka saja" ValidationExpression="\d+"
                            CssClass="validator_general"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNPWP"

               </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label">
                No Kartu Keluarga</label>
            <asp:TextBox ID="txtPNoKK" runat="server" MaxLength="20" onkeypress="return numbersonly2(event)"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegEx2" ControlToValidate="txtPNoKK" runat="server"
                ErrorMessage="Masukkan angka saja" ValidationExpression="\d+" CssClass="validator_general"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class="label_general label_req">
                Keterangan Koreksi
            </label>
            <asp:TextBox ID="txtAlasanKoreksi" runat="server" TextMode="MultiLine" Rows="3" Columns="40" />
            <asp:RequiredFieldValidator ID="rfvAlasanKoreksi" runat="server" ControlToValidate="txtAlasanKoreksi"
                Display="Dynamic" ErrorMessage="Alasan harus diisi" CssClass="validator_general" />
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False" />
    </div>
    </form>
</body>
</html>
