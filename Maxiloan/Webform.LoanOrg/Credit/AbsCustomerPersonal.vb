﻿Imports System.IO
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Public MustInherit Class AbsCustomerPersonal
    Inherits Maxiloan.Webform.WebBased
    Protected gStrPath As String
    Protected gStrFileName As String
    Protected m_controller As New CustomerController


    Protected Status As Boolean
    Protected Style As String = "ACCACQ"
    Protected m_controllerApp As New ApplicationController
    Protected ocustomclass As New Parameter.Application

#Region "Property"
    Protected Property ProspectAppID() As String
        Get
            Return CType(ViewState("ProspectAppID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ProspectAppID") = Value
        End Set
    End Property
    Protected Property PageAddEdit() As String
        Get
            Return CType(ViewState("Page"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Page") = Value
        End Set
    End Property
    Protected Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Protected Property Name() As String
        Get
            Return CType(ViewState("Name"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Name") = Value
        End Set
    End Property
    Protected Property IDType() As String
        Get
            Return CType(ViewState("IDType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IDType") = Value
        End Set
    End Property
    Protected Property IDTypeName() As String
        Get
            Return CType(ViewState("IDTypeName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IDTypeName") = Value
        End Set
    End Property
    Protected Property Type() As String
        Get
            Return CType(ViewState("Type"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property
    Protected Property IDNumber() As String
        Get
            Return CType(ViewState("IDNumber"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IDNumber") = Value
        End Set
    End Property
    Protected Property IDExpiredDate() As String
        Get
            Return CType(ViewState("IDExpiredDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("IDExpiredDate") = Value
        End Set
    End Property
    Protected Property Gender() As String
        Get
            Return CType(ViewState("Gender"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Gender") = Value
        End Set
    End Property
    Protected Property BirthPlace() As String
        Get
            Return CType(ViewState("BirthPlace"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BirthPlace") = Value
        End Set
    End Property
    Protected Property BirthDate() As String
        Get
            Return CType(ViewState("BirthDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BirthDate") = Value
        End Set
    End Property

    Protected Property CustomerGroupID() As String

        Get
            Return CType(ViewState("CustomerGroupID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerGroupID") = Value
        End Set
    End Property
    Protected Property MotherName() As String
        Get
            Return CType(ViewState("MotherName"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MotherName") = Value
        End Set
    End Property
    Protected Property KartuKeluarga() As String
        Get
            Return CType(ViewState("KartuKeluarga"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("KartuKeluarga") = Value
        End Set
    End Property
    Protected Property NamaPasangan() As String
        Get
            Return CType(ViewState("NamaPasangan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NamaPasangan") = Value
        End Set
    End Property
    Protected Property StatusPerkawinan() As String
        Get
            Return CType(ViewState("StatusPerkawinan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("StatusPerkawinan") = Value
        End Set
    End Property


    Property GelarDepan() As String
        Get
            Return CType(ViewState("GelarDepan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GelarDepan") = Value
        End Set
    End Property
    Property GelarBelakang() As String
        Get
            Return CType(ViewState("GelarBelakang"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("GelarBelakang") = Value
        End Set
    End Property
    Property NamaLembagaKeuangan() As String
        Get
            Return CType(ViewState("NamaLembagaKeuangan"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NamaLembagaKeuangan") = Value
        End Set
    End Property
    Property JenisPinjaman() As String
        Get
            Return CType(ViewState("JenisPinjaman"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("JenisPinjaman") = Value
        End Set
    End Property
    Property JumlahPinjaman() As Integer
        Get
            Return CType(ViewState("JumlahPinjaman"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("JumlahPinjaman") = Value
        End Set
    End Property
    Property Tenor() As Integer
        Get
            Return CType(ViewState("Tenor"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("Tenor") = Value
        End Set
    End Property
    Property SisaPokok() As Integer
        Get
            Return CType(ViewState("SisaPokok"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("SisaPokok") = Value
        End Set
    End Property
    Property Angsuran() As Integer
        Get
            Return CType(ViewState("Angsuran"), Integer)
        End Get
        Set(ByVal Value As Integer)
            ViewState("Angsuran") = Value
        End Set
    End Property

    

#End Region



#Region "XML"
    Protected Sub GetXML()
        If File.Exists(gStrPath & gStrFileName) Then
            Me.GelarDepan = Get_XMLValue(gStrPath & gStrFileName, "GelarDepan")
            Me.Name = Get_XMLValue(gStrPath & gStrFileName, "CustName")
            Me.GelarBelakang = Get_XMLValue(gStrPath & gStrFileName, "GelarBelakang")
            Me.IDType = Get_XMLValue(gStrPath & gStrFileName, "CustIDType")
            Me.IDTypeName = Get_XMLValue(gStrPath & gStrFileName, "CustIDTypeName")
            Me.IDNumber = Get_XMLValue(gStrPath & gStrFileName, "CustNumber")
            Me.IDExpiredDate = Get_XMLValue(gStrPath & gStrFileName, "IDExpiredDate")
            Me.Gender = Get_XMLValue(gStrPath & gStrFileName, "CustGender")
            Me.BirthDate = Get_XMLValue(gStrPath & gStrFileName, "CustBirthDate")
            Me.BirthPlace = Get_XMLValue(gStrPath & gStrFileName, "CustBirthPlace")
            Me.Type = Get_XMLValue(gStrPath & gStrFileName, "CustType")
            Me.MotherName = Get_XMLValue(gStrPath & gStrFileName, "MotherName")
            Me.KartuKeluarga = Get_XMLValue(gStrPath & gStrFileName, "KartuKeluarga")
            Me.NamaPasangan = Get_XMLValue(gStrPath & gStrFileName, "NamaPasangan")
            Me.StatusPerkawinan = Get_XMLValue(gStrPath & gStrFileName, "StatusPerkawinan")
        End If
    End Sub
    Private Function Get_XMLValue(ByVal pStrFile As String, ByVal pStrColumn As String) As String
        Dim lObjDSCust As New DataSet
        Dim lStrName As String = ""

        Try
            lObjDSCust.ReadXml(pStrFile)
            lStrName = lObjDSCust.Tables("Cust").Rows(0)("" & pStrColumn & "").ToString
        Catch ex As Exception
            '  ShowMessage(lblMessage, ex.Message, True)
        Finally
            lObjDSCust = Nothing
        End Try

        Return lStrName
    End Function
#End Region

    Protected Sub FillCbo_(ByVal table As String, cboReference As DropDownList)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = table
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        cboReference.Items.Insert(0, "Select One")
        cboReference.Items(0).Value = "Select One"

    End Sub


    Protected Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
        With uc
            .RequiredFieldValidatorEnable = rfv
            .RangeValidatorEnable = rv
            .TextCssClass = "numberAlign regular_text"
        End With
    End Sub
End Class
