﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerGroup.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerGroup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../Webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CUSTOMER GROUP</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                DAFTAR FLEET(GROUP CUSTOMER)
            </h4>
        </div>
    </div>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                    OnSortCommand="Sorting" DataKeyField="CustomerGroupID" CssClass="grid_general">
                    <ItemStyle CssClass="item_grid" />
                    <HeaderStyle CssClass="th" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="EDIT">
                            <ItemStyle Width="50px" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                    CommandName="Edit"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DELETE">
                            <ItemStyle Width="50px" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                    CommandName="Delete"></asp:ImageButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PIC" SortExpression="PICID">
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <a href='CustomerGroupPIC.aspx?cmd=dtl&id=<%# DataBinder.eval(Container,"DataItem.CustomerGroupID") 
                                    %>&desc=<%# DataBinder.eval(Container,"DataItem.CustomerGroupNm") %>'>PIC</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="CUSTOMER" SortExpression="CustomerID">
                            <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                            <ItemTemplate>
                                <a href='CustomerGroupFleetList.aspx?cmd=dtl&id=<%# DataBinder.eval(Container,"DataItem.CustomerGroupID") 
                                    %>&desc=<%# DataBinder.eval(Container,"DataItem.CustomerGroupNm") %>'>CUSTOMER</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="CustomerGroupID" SortExpression="CustomerGroupID" HeaderText="GIF">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CustomerGroupNm" SortExpression="CustomerGroupNm" HeaderText="GROUP">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Plafond" SortExpression="Plafond" HeaderText="PLAFOND" DataFormatString ="{0:N0}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Oustanding" SortExpression="Oustanding" HeaderText="OUSTANDING" DataFormatString ="{0:N0}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Kontrak" SortExpression="Kontrak" HeaderText="KONTRAK">
                        </asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server" CssClass="small_text">1</asp:TextBox>
                    <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" />
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                        ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" Type="integer"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False" />
           
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI GROUP CUSTOMER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="CustomerGroupNm">Nama Group Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" CssClass="medium_text"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama Customer Group
                </label>
                <asp:Label ID="lblNamaCustomerGroup" runat="server" ></asp:Label>
                <asp:TextBox ID="txtNamaCustomerGroup" runat="server" CssClass="medium_text"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap Isi Nama Customer Group!"
                    ControlToValidate="txtNamaCustomerGroup" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Plafond</label>
                <asp:Label ID="lblPlafond" runat="server"></asp:Label>
                <%--<asp:TextBox ID="txtPlafond" runat="server"></asp:TextBox>--%>
                <uc1:ucNumberFormat ID="txtPlafond" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_general">
                    Catatan
                </label>
                <asp:Label ID="lblCatatan" runat="server"></asp:Label>
                <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" CssClass="multiline_textbox_uc">
                </asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="true" Text="Cancel" CssClass="small button gray" />
            <asp:Button ID="btnBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray" />
            <asp:Button ID="btnClose" runat="server" CausesValidation="False" Text="Close" CssClass="small button gray" />
        </div>
    </asp:Panel>
    <input id="hdnCustomerGroupID" type="hidden" name="hdnCustomerGroupID" runat="server" />
    </form>
</body>
</html>
