﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class CustomerPersonalEmergency
    Inherits AbsCustomerPersonal

#Region "uc control declaration"

    Protected WithEvents ucAddressER As UcCompanyAddress ' ucAddress

#End Region
#Region "Constanta"
     

    Const txtHargaRumahText As String = "0"
    Const txtKeteranganUsahaText As String = ""
    Const txtOBTypeText As String = ""
    Const cboOBIndustryTypeValue As String = ""
    Const txtOBJobTitleText As String = ""
    Const txtOBSinceYearText As String = ""
    Private time As String
#End Region

#Region "Property"
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
           
            'Modify by Wira 20171011
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------

            If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With

                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)
                'Modify by Wira 20171011
                Me.StatusPerkawinan = ocustomclass.MaritalStatus
            End If

            Me.PageAddEdit = Request("page")

          

            If Me.PageAddEdit = "Add" Then                
                lblTitle.Text = "ADD"
                GetXML()               
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)                              
                cTabs.SetNavigateUrl(Request("page"), Request("id"))
                
            End If
            cTabs.RefreshAttr(Request("pnl"))
            If Me.StatusPerkawinan.ToUpper.Trim <> "M" Then
                cTabs.EnabledLink("tabPasangan", False)
            End If
            ucAddressER.hideMandatoryAll()
            ucAddressER.ValidatorFalse()
            ucAddressER.ValidatorTrue()
        End If
    End Sub
#End Region
    ' #Region "FillCbo"
    '    Sub FillCbo(ByVal table As String)
    '        Dim dtEntity As DataTable
    '        Dim oCustomer As New Parameter.Customer
    '        oCustomer.strConnection = GetConnectionString()
    '        oCustomer.Table = table
    '        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
    '        If Not oCustomer Is Nothing Then
    '            dtEntity = oCustomer.listdata
    '        End If
    '        Select Case table

    '                'Case "tblReference"
    '                '    cboReference.DataSource = dtEntity.DefaultView
    '                '    cboReference.DataTextField = "Description"
    '                '    cboReference.DataValueField = "ID"
    '                '    cboReference.DataBind()
    '                '    cboReference.Items.Insert(0, "Select One")
    '                '    cboReference.Items(0).Value = "Select One"
    '        End Select
    '    End Sub    
    '#End Region
#Region "imbPCancel_Click"
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
#End Region
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Try
            Dim ocustomer As New Parameter.Customer
            Dim oPCER As New Parameter.CustomerER
            Dim oPCERAddress As New Parameter.Address
            
            With oPCER
                .Nama = txtNamaER.Text.Trim
                .Hubungan = ddlHubungan.SelectedValue
                .MobilePhone = txtHPER.Text.Trim
                .Email = txtEmailER.Text.Trim
            End With

            With oPCERAddress
                .Address = ucAddressER.Address.Trim
                .RT = ucAddressER.RT.Trim
                .RW = ucAddressER.RW.Trim
                .Kelurahan = ucAddressER.Kelurahan.Trim
                .Kecamatan = ucAddressER.Kecamatan.Trim
                .City = ucAddressER.City.Trim
                .ZipCode = ucAddressER.ZipCode.Trim
                .AreaPhone1 = ucAddressER.AreaPhone1.Trim
                .Phone1 = ucAddressER.Phone1.Trim
                .AreaPhone2 = ucAddressER.AreaPhone2.Trim
                .Phone2 = ucAddressER.Phone2.Trim
                .AreaFax = ucAddressER.AreaFax.Trim
                .Fax = ucAddressER.Fax.Trim
            End With

            Dim Err As String = ""
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID
            Err = m_controller.PersonalCustomerEmergencySaveEdit(ocustomer, _                                                
                                                oPCER, _
                                                oPCERAddress)
            If Err = "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
            'Modify by WIra 20171011
            'Response.Redirect("CustomerPersonalKeluarga.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabKeluarga")
            Response.Redirect("CustomerPersonalKeluarga.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabKeluarga&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
#End Region
#Region "Edit"
    Sub BindEdit(ByVal ID As String)
        Dim dtEntity As New DataTable
        Dim dtOmset As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = Me.CustomerID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "7")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        Me.Name = oRow("Name").ToString.Trim        

        Me.IDType = oRow("IDType").ToString.Trim
        Me.IDTypeName = oRow("IDTypeName").ToString.Trim
        Me.IDNumber = oRow("IDNumber").ToString.Trim
        Me.Gender = oRow("Gender").ToString.Trim
        If IsDate(oRow("IDExpiredDate").ToString) Then Me.IDExpiredDate = Format(oRow("IDExpiredDate"), "dd/MM/yyyy")

        If Not IsDBNull(oRow("BirthDate")) Then
            Me.BirthDate = Day(CDate(oRow("BirthDate"))).ToString + "/" + Month(CDate(oRow("BirthDate"))).ToString + "/" + Year(CDate(oRow("BirthDate"))).ToString
        Else
            Me.BirthDate = ""
        End If

        Me.BirthPlace = oRow("BirthPlace").ToString.Trim
        Me.Type = oRow("PersonalCustomerType").ToString.Trim

        Me.MotherName = oRow("MotherName").ToString.Trim
        Me.NamaPasangan = oRow("SINamaSuamiIstri").ToString.Trim
        Me.KartuKeluarga = oRow("NoKK").ToString.Trim
        Me.StatusPerkawinan = oRow("MaritalStatus").ToString.Trim

        txtNamaER.Text = oRow("ERNama").ToString.Trim                
        ddlHubungan.SelectedIndex = ddlHubungan.Items.IndexOf(ddlHubungan.Items.FindByValue(oRow("ERHubungan").ToString.Trim))
        With ucAddressER
            .Address = oRow("ERAlamat").ToString.Trim
            .RT = oRow("ERRT").ToString.Trim
            .RW = oRow("ERRW").ToString.Trim
            .Kelurahan = oRow("ERKelurahan").ToString.Trim
            .Kecamatan = oRow("ERKecamatan").ToString.Trim
            .City = oRow("ERKota").ToString.Trim
            .ZipCode = oRow("ERKodePos").ToString.Trim
            .AreaPhone1 = oRow("ERAreaPhone1").ToString.Trim
            .Phone1 = oRow("ERPhone1").ToString.Trim
            .AreaPhone2 = oRow("ERAreaPhone2").ToString.Trim
            .Phone2 = oRow("ERPhone2").ToString.Trim
            .AreaFax = oRow("ERAreaFax").ToString.Trim
            .Fax = oRow("ERFax").ToString.Trim
            .showMandatoryAll()
            .Phone1ValidatorEnabled(True)
            .BindAddress()
        End With
        txtHPER.Text = oRow("ERMobilePhone").ToString.Trim
        txtEmailER.Text = oRow("EREmail").ToString.Trim
    End Sub        
#End Region
    
End Class