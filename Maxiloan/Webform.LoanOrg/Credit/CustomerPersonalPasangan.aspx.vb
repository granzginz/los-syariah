﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class CustomerPersonalPasangan
    Inherits AbsCustomerPersonal

#Region "uc control declaration"                        
    Protected WithEvents ucAddressSI As UcAddressCity
    'Protected WithEvents ucKaryawanSejakSI As ucMonthCombo    
    Protected WithEvents ucAddressSI_KTP As ucAddressCity
    Protected WithEvents txtTglLahirNew As ucDateCE
    Protected WithEvents txtMasaBerlakuKTP As ucDateCE
    'Protected WithEvents txtPenghasilanSI As ucNumberFormat
    'Protected WithEvents txtPenghasilanTambahanSI As ucNumberFormat
    Protected WithEvents txtPDFixedInc As ucNumberFormat
    Protected WithEvents txtPDVariableInc As ucNumberFormat
    Protected WithEvents txtFDFixedInc As ucNumberFormat
    Protected WithEvents txtFDVariableInc As ucNumberFormat
    Protected WithEvents txtMasaKerja As ucNumberFormat
#End Region
#Region "Constanta"
 

    Const txtHargaRumahText As String = "0"
    Const txtKeteranganUsahaText As String = ""
    Const txtOBTypeText As String = ""
    Const cboOBIndustryTypeValue As String = ""
    Const txtOBJobTitleText As String = ""
    Const txtOBSinceYearText As String = ""
    Private time As String
#End Region
#Region "Property"
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")                        


            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

            'Modify by Wira 20171011
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------

            InitiateUCnumberFormat(txtPDFixedInc, True, True)
            InitiateUCnumberFormat(txtPDVariableInc, True, True)
            InitiateUCnumberFormat(txtFDFixedInc, True, True)
            InitiateUCnumberFormat(txtFDVariableInc, True, True)
            'InitiateUCnumberFormat(txtPenghasilanSI, True, True)
            'InitiateUCnumberFormat(txtPenghasilanTambahanSI, True, True)

            FillCbo_("tblJobType", cboJobTypeSI)
            FillCbo_("tblOccupation", cboOccupation)
            'FillCbo("IndustryType")         
            fillCboIDType()
            fillIndustryHeader()
            If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With

                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)
            End If

            ucAddressSI.ValidatorFalse()
            ucAddressSI.Phone1ValidatorEnabled(False)
            ucAddressSI.DivHPHide()

            ucAddressSI_KTP.ValidatorFalse()
            ucAddressSI_KTP.Phone1ValidatorEnabled(True)
            ucAddressSI_KTP.DivHPHide()

            Me.PageAddEdit = Request("page")

            If Me.PageAddEdit = "Add" Then               
                lblTitle.Text = "ADD"
                GetXML()                
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                cTabs.SetNavigateUrl(Request("page"), Request("id"))
               
            End If
            txtNamaPasangan.Text = Me.NamaPasangan            

            If Me.Type = "M" Then                
                pnlPEntrepreneur.Visible = False
                pnlProfessional.Visible = False
            ElseIf Me.Type = "N" Then                
                pnlPEntrepreneur.Visible = True
                pnlProfessional.Visible = False
            Else                
                pnlPEntrepreneur.Visible = False
                pnlProfessional.Visible = True
            End If
            
            cTabs.RefreshAttr(Request("pnl"))
             

        End If
    End Sub
#End Region
   
#Region "FillCbo"
    Sub fillIndustryHeader()
        'Dim dtEntity As DataTable
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblIndustryHeader"
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)

        cboIndrustriHeader.DataSource = oCustomer.listdata.DefaultView
        cboIndrustriHeader.DataTextField = "Description"
        cboIndrustriHeader.DataValueField = "ID"
        cboIndrustriHeader.DataBind()
        cboIndrustriHeader.Items.Insert(0, "Select One")
        cboIndrustriHeader.Items(0).Value = "Select One"

    End Sub
    Sub fillCboIDType()
        Dim oCustomer As New Parameter.Customer
        Dim dtList As DataTable
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.CustomerPersonalGetIDType(oCustomer)
        dtList = oCustomer.listdata
        cboIDTypeP.DataSource = dtList
        cboIDTypeP.DataTextField = "Description"
        cboIDTypeP.DataValueField = "ID"
        cboIDTypeP.DataBind()
        cboIDTypeP.Items.Insert(0, "Select One")
        cboIDTypeP.Items(0).Value = "Select One"
    End Sub
#End Region
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Try
            'modify nofi 23Feb2018
            Dim tglLahir As Date = ConvertDate2(txtTglLahirNew.Text)
            If tglLahir >= Me.BusinessDate Then
                ShowMessage(lblMessage, "Tanggal Lahir harus lebih Kecil dari hari ini", True)
                Exit Sub
            End If
            Dim age As Integer
            age = Today.Year - tglLahir.Year
            If age < 17 Then
                ShowMessage(lblMessage, "Tanggal Lahir harus usia minimal 17 tahun.", True)
                Exit Sub
            End If

            'Dim GScont As New DataUserControlController
            'Dim GSdata As New Parameter.GeneralSetting
            'GSdata.GSID = "AGEMIN"
            'Dim jmlTahun As Double = Math.Round(Me.BusinessDate.Subtract(tglLahir).TotalDays / 365, 0)
            'Dim minAge As String = GScont.GetGeneralSetting(GetConnectionString, GSdata)

            'GSdata.GSID = "AGEMAX"
            'Dim maxAge As String = GScont.GetGeneralSetting(GetConnectionString, GSdata)
            'If jmlTahun > CDbl(maxAge) Or jmlTahun < CDbl(minAge) Then
            '    ShowMessage(lblMessage, "Umur harus antara " & minAge & " dan " & maxAge, True)
            '    Exit Sub
            'End If



            Dim Err As String = ""
            Dim ocustomer As New Parameter.Customer
            Dim oPCSI As New Parameter.CustomerSI
            Dim oPCSIAddress As New Parameter.Address
            oPCSI.AlamatKTP = New Parameter.Address

            ocustomer.MonthlyFixedIncome = txtPDFixedInc.Text.Trim
            ocustomer.MonthlyVariableIncome = txtPDVariableInc.Text.Trim
            ocustomer.OtherBusinessName = txtEDName.Text.Trim

            'If (hdfKodeIndustriPS.Value.Trim = "") Then
            '    Err = "Silahkan pilih Bidang Usaha Detail."
            '    ShowMessage(lblMessage, err, True)
            '    Exit Sub
            'End If
            With oPCSI
                .NamaSuamiIstri = txtNamaPasangan.Text.Trim
                .JenisPekerjaanID = cboJobTypeSI.SelectedValue.Trim
                .NamaPers = txtCompanyNameSI.Text.Trim
                .JenisIndustriID = hdfKodeIndustriPS.Value.Trim
                .KeteranganUsaha = txtKeteranganUsahaSI.Text.Trim
                '.KaryawanSejak = CShort(ucKaryawanSejakSI.SelectedMonth)
                '.KaryawanSejakTahun = txtKaryawanSejakSI.Text.Trim
                '.PenghasilanTetap = CDbl(txtPenghasilanSI.Text)
                '.PenghasilanTambahan = CDbl(txtPenghasilanTambahanSI.Text)


                .TempatLahirSI = txtBirthPlaceP.Text
                .TglLahirSI = ConvertDate2(txtTglLahirNew.Text)
                .NoDokumentSI = txtNoDokument.Text
                .MasaBerlakuKTPSI = New Date(1900, 1, 1)
                If (txtMasaBerlakuKTP.Text.Trim <> "") Then
                    .MasaBerlakuKTPSI = ConvertDate2(txtMasaBerlakuKTP.Text)
                End If

                '.BeroperasiSejakSI = CShort(txtBerOperasiSejak.Text)
                'Modify by Wira 20180102
                .BeroperasiSejakSI = txtBerOperasiSejak.Text
                .MasaKerjaSI = CShort(txtMasaKerja.Text)
                .UsahaLainnyaSI = txtUsahaLainnya.Text
                .JenisDokumenSI = cboIDTypeP.SelectedValue
                .OccupationID = cboOccupation.SelectedValue
            End With

            With oPCSIAddress
                .Address = ucAddressSI.Address.Trim
                .RT = "-"
                .RW = "-"
                .Kelurahan = "-"
                .Kecamatan = "-"
                .City = ucAddressSI.City.Trim
                .ZipCode = ucAddressSI.ZipCode.Trim
                .AreaPhone1 = ucAddressSI.AreaPhone1.Trim
                .Phone1 = ucAddressSI.Phone1.Trim
                .AreaPhone2 = ucAddressSI.AreaPhone2.Trim
                .Phone2 = ucAddressSI.Phone2.Trim
                .AreaFax = ucAddressSI.AreaFax.Trim
                .Fax = ucAddressSI.Fax.Trim
            End With

            With oPCSI.AlamatKTP
                .Address = ucAddressSI_KTP.Address.Trim
                .RT = "-"
                .RW = "-"
                .Kelurahan = "-"
                .Kecamatan = "-"
                .City = ucAddressSI_KTP.City.Trim
                .ZipCode = ucAddressSI_KTP.ZipCode.Trim
                .AreaPhone1 = ucAddressSI_KTP.AreaPhone1.Trim
                .Phone1 = ucAddressSI_KTP.Phone1.Trim
                .AreaPhone2 = ucAddressSI_KTP.AreaPhone2.Trim
                .Phone2 = ucAddressSI_KTP.Phone2.Trim
                .AreaFax = ucAddressSI_KTP.AreaFax.Trim
                .Fax = ucAddressSI_KTP.Fax.Trim
                .MobilePhone = ucAddressSI_KTP.HP.Trim
            End With


            'Dim Err As String = ""
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID
            Err = m_controller.PersonalCustomerPasanganSaveEdit(ocustomer,
                                                oPCSI,
                                                oPCSIAddress)
            If Err = "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
            'Modify by WIra 20171011
            'Response.Redirect("CustomerPersonalGuarantor.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabGuarantor")
            Response.Redirect("CustomerPersonalGuarantor.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabGuarantor&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
#End Region
#Region "Button"
    Private Sub btnAlamatKerja1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlamatKerja1.Click
        Dim dtEntity As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = Me.CustomerID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "3")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        ucAddressSI.Address = oRow("LegalAddress").ToString.Trim & IIf(oRow("LegalRT").ToString.Trim = "", "", " RT." & oRow("LegalRT").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalRW").ToString.Trim = "", "", " RW." & oRow("LegalRW").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalKelurahan").ToString.Trim = "", "", oRow("LegalKelurahan").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalKecamatan").ToString.Trim = "", "", oRow("LegalKecamatan").ToString.Trim & "").ToString

        ucAddressSI.City = oRow("LegalCity").ToString.Trim
        ucAddressSI.ZipCode = oRow("LegalZipCode").ToString.Trim
        ucAddressSI.AreaPhone1 = oRow("LegalAreaPhone1").ToString.Trim
        ucAddressSI.Phone1 = oRow("LegalPhone1").ToString.Trim
        ucAddressSI.AreaPhone2 = oRow("LegalAreaPhone2").ToString.Trim
        ucAddressSI.Phone2 = oRow("LegalPhone2").ToString.Trim
        ucAddressSI.AreaFax = oRow("LegalAreaFax").ToString.Trim
        ucAddressSI.Fax = oRow("LegalFax").ToString.Trim
        ucAddressSI.BindAddress()
    End Sub
    Private Sub btnAlamatKerja2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlamatKerja2.Click
        Dim dtEntity As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = Me.CustomerID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "3")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)

        ucAddressSI.Address = oRow("ResidenceAddress").ToString.Trim & IIf(oRow("ResidenceRT").ToString.Trim = "", "", " RT." & oRow("ResidenceRT").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceRW").ToString.Trim = "", "", " RW." & oRow("ResidenceRW").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceKelurahan").ToString.Trim = "", "", oRow("ResidenceKelurahan").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceKecamatan").ToString.Trim = "", "", oRow("ResidenceKecamatan").ToString.Trim & "").ToString

        ucAddressSI.City = oRow("ResidenceCity").ToString.Trim
        ucAddressSI.ZipCode = oRow("ResidenceZipCode").ToString.Trim
        ucAddressSI.AreaPhone1 = oRow("ResidenceAreaPhone1").ToString.Trim
        ucAddressSI.Phone1 = oRow("ResidencePhone1").ToString.Trim
        ucAddressSI.AreaPhone2 = oRow("ResidenceAreaPhone2").ToString.Trim
        ucAddressSI.Phone2 = oRow("ResidencePhone2").ToString.Trim
        ucAddressSI.AreaFax = oRow("ResidenceAreaFax").ToString.Trim
        ucAddressSI.Fax = oRow("ResidenceFax").ToString.Trim
        ucAddressSI.BindAddress()
    End Sub
    Private Sub btnAlamatKerja1A_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlamatKerja1A.Click
        Dim dtEntity As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = Me.CustomerID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "3")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        ucAddressSI_KTP.Address = oRow("LegalAddress").ToString.Trim & IIf(oRow("LegalRT").ToString.Trim = "", "", " RT." & oRow("LegalRT").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalRW").ToString.Trim = "", "", " RW." & oRow("LegalRW").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalKelurahan").ToString.Trim = "", "", oRow("LegalKelurahan").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalKecamatan").ToString.Trim = "", "", oRow("LegalKecamatan").ToString.Trim & "").ToString

        ucAddressSI_KTP.City = oRow("LegalCity").ToString.Trim
        ucAddressSI_KTP.ZipCode = oRow("LegalZipCode").ToString.Trim
        ucAddressSI_KTP.AreaPhone1 = oRow("LegalAreaPhone1").ToString.Trim
        ucAddressSI_KTP.Phone1 = oRow("LegalPhone1").ToString.Trim
        ucAddressSI_KTP.AreaPhone2 = oRow("LegalAreaPhone2").ToString.Trim
        ucAddressSI_KTP.Phone2 = oRow("LegalPhone2").ToString.Trim
        ucAddressSI_KTP.AreaFax = oRow("LegalAreaFax").ToString.Trim
        ucAddressSI_KTP.Fax = oRow("LegalFax").ToString.Trim
        ucAddressSI_KTP.BindAddress()
    End Sub
    Private Sub btnAlamatKerja2A_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlamatKerja2A.Click
        Dim dtEntity As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = Me.CustomerID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "3")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)

        ucAddressSI_KTP.Address = oRow("ResidenceAddress").ToString.Trim & IIf(oRow("ResidenceRT").ToString.Trim = "", "", " RT." & oRow("ResidenceRT").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceRW").ToString.Trim = "", "", " RW." & oRow("ResidenceRW").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceKelurahan").ToString.Trim = "", "", oRow("ResidenceKelurahan").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceKecamatan").ToString.Trim = "", "", oRow("ResidenceKecamatan").ToString.Trim & "").ToString

        ucAddressSI_KTP.City = oRow("ResidenceCity").ToString.Trim
        ucAddressSI_KTP.ZipCode = oRow("ResidenceZipCode").ToString.Trim
        ucAddressSI_KTP.AreaPhone1 = oRow("ResidenceAreaPhone1").ToString.Trim
        ucAddressSI_KTP.Phone1 = oRow("ResidencePhone1").ToString.Trim
        ucAddressSI_KTP.AreaPhone2 = oRow("ResidenceAreaPhone2").ToString.Trim
        ucAddressSI_KTP.Phone2 = oRow("ResidencePhone2").ToString.Trim
        ucAddressSI_KTP.AreaFax = oRow("ResidenceAreaFax").ToString.Trim
        ucAddressSI_KTP.Fax = oRow("ResidenceFax").ToString.Trim
        ucAddressSI_KTP.BindAddress()
    End Sub
#End Region
#Region "imbPCancel_Click"
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
#End Region
#Region "Edit"
    Sub BindEdit(ByVal ID As String)
        Dim dtEntity As New DataTable
        Dim dtOmset As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = ID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "6")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        Me.Name = oRow("Name").ToString.Trim

        Me.IDType = oRow("IDType").ToString.Trim
        Me.IDTypeName = oRow("IDTypeName").ToString.Trim
        Me.IDNumber = oRow("IDNumber").ToString.Trim
        Me.Gender = oRow("Gender").ToString.Trim
        If IsDate(oRow("IDExpiredDate").ToString) Then Me.IDExpiredDate = Format(oRow("IDExpiredDate"), "dd/MM/yyyy")

        If Not IsDBNull(oRow("BirthDate")) Then
            Me.BirthDate = Day(CDate(oRow("BirthDate"))).ToString + "/" + Month(CDate(oRow("BirthDate"))).ToString + "/" + Year(CDate(oRow("BirthDate"))).ToString
        Else
            Me.BirthDate = ""
        End If

        Me.BirthPlace = oRow("BirthPlace").ToString.Trim
        Me.Type = oRow("PersonalCustomerType").ToString.Trim

        Me.MotherName = oRow("MotherName").ToString.Trim
        Me.NamaPasangan = oRow("SINamaSuamiIstri").ToString.Trim
        Me.KartuKeluarga = oRow("NoKK").ToString.Trim
        Me.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim

        txtPDFixedInc.Text = FormatNumber(oRow("MonthlyFixedIncome").ToString.Trim, 0)
        txtPDVariableInc.Text = FormatNumber(oRow("MonthlyVariableIncome").ToString.Trim, 0)
        txtFDFixedInc.Text = FormatNumber(oRow("MonthlyFixedIncome").ToString.Trim, 0)
        txtFDVariableInc.Text = FormatNumber(oRow("MonthlyVariableIncome").ToString.Trim, 0)
        txtEDName.Text = oRow("OtherBusinessName").ToString.Trim
        txtNamaPasangan.Text = oRow("SINamaSuamiIstri").ToString.Trim

        If oRow("SIJenisPekerjaanID").ToString <> "" Then
            cboJobTypeSI.SelectedIndex = cboJobTypeSI.Items.IndexOf(cboJobTypeSI.Items.FindByValue(oRow("SIJenisPekerjaanID").ToString))
        End If
        txtCompanyNameSI.Text = oRow("SINamaPers").ToString.Trim
        hdfKodeIndustriPS.Value = oRow("SIJenisIndustriID").ToString.Trim
        txtNamaIndustriPS.Text = oRow("SIJenisIndustriName").ToString.Trim
        txtKeteranganUsahaSI.Text = oRow("SIKeteranganUsaha").ToString.Trim
        cboOccupation.SelectedIndex = cboOccupation.Items.IndexOf(cboOccupation.Items.FindByValue(oRow("OccupationID").ToString.Trim))
        With ucAddressSI
            .Address = oRow("SIAlamat").ToString.Trim
            .City = oRow("SIKota").ToString.Trim
            .ZipCode = oRow("SIKodePos").ToString.Trim
            .AreaPhone1 = oRow("SIAreaPhone1").ToString.Trim
            .Phone1 = oRow("SIPhone1").ToString.Trim
            .AreaPhone2 = oRow("SIAreaPhone2").ToString.Trim
            .Phone2 = oRow("SIPhone2").ToString.Trim
            .AreaFax = oRow("SIAreaFax").ToString.Trim
            .Fax = oRow("SIFax").ToString.Trim
            .BindAddress()
        End With
        'ucKaryawanSejakSI.SelectedMonth = CInt(oRow("SIKaryawanSejak"))
        ' txtKaryawanSejakSI.Text = oRow("SIKaryawanSejakTahun").ToString.Trim
        'txtPenghasilanSI.Text = FormatNumber(oRow("SIPenghasilanTetap"), 0)
        'txtPenghasilanTambahanSI.Text = FormatNumber(oRow("SIPenghasilanTambahan"), 0)

        With ucAddressSI_KTP
            .Address = oRow("SIKtpAlamat").ToString.Trim
            .City = oRow("SIKtpKota").ToString.Trim
            .ZipCode = oRow("SIKtpKodePos").ToString.Trim
            .AreaPhone1 = oRow("SIKtpAreaPhone1").ToString.Trim
            .Phone1 = oRow("SIKtpPhone1").ToString.Trim
            .AreaPhone2 = oRow("SIKtpAreaPhone2").ToString.Trim
            .Phone2 = oRow("SIKtpPhone2").ToString.Trim
            .AreaFax = oRow("SIKtpAreaFax").ToString.Trim
            .Fax = oRow("SIKtpFax").ToString.Trim
            .HP = oRow("KtpHP").ToString.Trim
            .BindAddress()
        End With
        txtBirthPlaceP.Text = oRow("TempatLahir").ToString.Trim

        If (txtNamaPasangan.Text.Trim = "") Then
            txtTglLahirNew.Text = ""
        Else
            txtTglLahirNew.Text = Format(oRow("TglLahir"), "dd/MM/yyyy")
        End If
        

        cboIDTypeP.SelectedIndex = cboIDTypeP.Items.IndexOf(cboIDTypeP.Items.FindByValue(oRow("JenisDokumen").ToString.Trim))
        txtNoDokument.Text = oRow("NoDokument").ToString.Trim
        If (txtNoDokument.Text.Trim = "") Then
            txtMasaBerlakuKTP.Text = ""
        Else
            txtMasaBerlakuKTP.Text = Format(oRow("MasaBerlakuKTP"), "dd/MM/yyyy")
        End If

        txtBerOperasiSejak.Text = oRow("BeroperasiSejak").ToString.Trim
        txtMasaKerja.Text = oRow("MasaKerja").ToString.Trim
        txtUsahaLainnya.Text = oRow("UsahaLainnya").ToString.Trim

        cboIndrustriHeader.SelectedValue = oRow("JenisIndustriHID").ToString.Trim

    End Sub
#End Region

    Private Sub cboJobTypeSI_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboJobTypeSI.SelectedIndexChanged
        If cboJobTypeSI.SelectedIndex <> cboJobTypeSI.Items.IndexOf(cboJobTypeSI.Items.FindByValue("Select One")) Then
            lblBerOperasiSejak.Attributes("class") = "label_req"
            RVBeroperasiSejak.Enabled = True
            RVBeroperasiSejak.ErrorMessage = "Harap isi Beroperasi Sejak"
        Else
            lblBerOperasiSejak.Attributes("class") = "label"
            RVBeroperasiSejak.Enabled = False
        End If
    End Sub
End Class