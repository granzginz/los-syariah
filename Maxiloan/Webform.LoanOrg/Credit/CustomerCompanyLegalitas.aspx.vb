﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region

Public Class CustomerCompanyLegalitas
    Inherits AbsCustomerCompany


    
    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New ApplicationController
    Private ocustomclass As New Parameter.Application
    Private time As String

    '#Region "Property"
    '    Private Property ProspectAppID() As String
    '        Get
    '            Return CType(ViewState("ProspectAppID"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("ProspectAppID") = Value
    '        End Set
    '    End Property
    '    Property PageAddEdit() As String
    '        Get
    '            Return CType(ViewState("Page"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("Page") = Value
    '        End Set
    '    End Property
    '    Property CustomerID() As String
    '        Get
    '            Return CType(ViewState("CustomerID"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("CustomerID") = Value
    '        End Set
    '    End Property
    '    Property Name() As String
    '        Get
    '            Return CType(ViewState("Name"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("Name") = Value
    '        End Set
    '    End Property
    '    Property Address() As String
    '        Get
    '            Return CType(ViewState("Address"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("Address") = Value
    '        End Set
    '    End Property
    '    Property RT() As String
    '        Get
    '            Return CType(ViewState("RT"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("RT") = Value
    '        End Set
    '    End Property
    '    Property RW() As String
    '        Get
    '            Return CType(ViewState("RW"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("RW") = Value
    '        End Set
    '    End Property
    '    Property City() As String
    '        Get
    '            Return CType(ViewState("City"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("City") = Value
    '        End Set
    '    End Property
    '    Property Kecamatan() As String
    '        Get
    '            Return CType(ViewState("Kecamatan"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("Kecamatan") = Value
    '        End Set
    '    End Property
    '    Property Kelurahan() As String
    '        Get
    '            Return CType(ViewState("Kelurahan"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("Kelurahan") = Value
    '        End Set
    '    End Property
    '    Property NPWP() As String
    '        Get
    '            Return CType(ViewState("NPWP"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("NPWP") = Value
    '        End Set
    '    End Property
    '    Property ZipCode() As String
    '        Get
    '            Return CType(ViewState("ZipCode"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("ZipCode") = Value
    '        End Set
    '    End Property
    '    Property AFax() As String
    '        Get
    '            Return CType(ViewState("AFax"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("AFax") = Value
    '        End Set
    '    End Property
    '    Property Fax() As String
    '        Get
    '            Return CType(ViewState("Fax"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("Fax") = Value
    '        End Set
    '    End Property
    '    Property APhone1() As String
    '        Get
    '            Return CType(ViewState("APhone1"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("APhone1") = Value
    '        End Set
    '    End Property
    '    Property Phone1() As String
    '        Get
    '            Return CType(ViewState("Phone1"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("Phone1") = Value
    '        End Set
    '    End Property
    '    Property APhone2() As String
    '        Get
    '            Return CType(ViewState("APhone2"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("APhone2") = Value
    '        End Set
    '    End Property
    '    Property Phone2() As String
    '        Get
    '            Return CType(ViewState("Phone2"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("Phone2") = Value
    '        End Set
    '    End Property
    '    Property CustomerGroupID() As String
    '        Get
    '            Return CType(ViewState("CustomerGroupID"), String)
    '        End Get
    '        Set(ByVal Value As String)
    '            ViewState("CustomerGroupID") = Value
    '        End Set
    '    End Property
    '#End Region
#Region "Property"
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            If Request("prospectappid") = "" Then Me.ProspectAppID = "-"
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"            

            'Modify by Wira 20171023
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------

            If Me.ProspectAppID <> "-" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)                
            End If
             

            Me.PageAddEdit = Request("page")
            If Me.PageAddEdit = "Add" Then
                GetXML()                
                lblTitle.Text = "ADD"                
                AddRecord2()                
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)                
                EditRecord2(Me.CustomerID)
                cTabs.SetNavigateUrl(Request("page"), Request("id"))
           
            End If
            cTabs.RefreshAttr(Request("pnl"))
            If Me.PageAddEdit <> "Add" Then
                Dim intTotalCustomer As Integer
                intTotalCustomer = m_controller.GetTotalCustomer(Me.CustomerID.Trim, GetConnectionString)                
            End If

            Dim intLoop As Integer

            If intLoop <= dtgList2.Items.Count - 1 Then
                CType(dtgList2.Items(intLoop).FindControl("lblVType2"), Label).Visible = False
                CType(dtgList2.Items(intLoop).FindControl("lblVDocNo2"), Label).Visible = False
                'CType(dtgList2.Items(intLoop).FindControl("lblVDate2"), Label).Visible = False
                CType(dtgList2.Items(intLoop).FindControl("lblVNotes2"), Label).Visible = False

                Dim txtTglDokumen As New ucDateCE
                txtTglDokumen = CType(dtgList2.Items(intLoop).FindControl("txtTglDokumen"), ucDateCE)
                txtTglDokumen.IsRequired = True

                Dim txtTglJT As New ucDateCE
                txtTglJT = CType(dtgList2.Items(intLoop).FindControl("txtTglJT"), ucDateCE)
            End If
             
        End If
    End Sub 
#Region "Edit"
    Sub BindEdit(ByVal id As String)
        Dim dtEntity As DataTable = Nothing
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = id
        oCustomer.CustomerType = "C"
        oCustomer.strConnection = GetConnectionString()

        oCustomer = m_controller.GetCustomerEdit(oCustomer, "1")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        oRow = dtEntity.Rows(0)

        Me.Name = oRow("Name").ToString.Trim
        Me.NPWP = oRow("NPWP").ToString.Trim
        Me.Address = oRow("CompanyAddress").ToString.Trim
        Me.RT = oRow("CompanyRT").ToString.Trim
        Me.RW = oRow("CompanyRW").ToString.Trim
        Me.Kelurahan = oRow("CompanyKelurahan").ToString.Trim
        Me.Kecamatan = oRow("CompanyKecamatan").ToString.Trim
        Me.City = oRow("CompanyCity").ToString.Trim
        Me.ZipCode = oRow("CompanyZipCode").ToString.Trim
        Me.APhone1 = oRow("CompanyAreaPhone1").ToString.Trim
        Me.APhone2 = oRow("CompanyAreaPhone2").ToString.Trim
        Me.Phone1 = oRow("CompanyPhone1").ToString.Trim
        Me.Phone2 = oRow("CompanyPhone2").ToString.Trim
        Me.AFax = oRow("CompanyAreaFax").ToString.Trim
        Me.Fax = oRow("CompanyFax").ToString.Trim
        Me.CustomerID = id

        Me.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim
        
    End Sub
    Private Sub EditRecord2(ByVal id As String)
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim lblNo2 As New Label
        Dim cboType2 As New DropDownList
        Dim txtDocNo2 As New TextBox
        Dim txtNotes2 As New TextBox
        Dim oCustomer As New Parameter.Customer
        Dim npwpExist As Boolean = False
        Dim imbDelete2 As New ImageButton

        oCustomer.CustomerID = id
        oCustomer.PersonalCustomerType = "c"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "3")
        objectDataTable = oCustomer.listdata

        'check if npwp exist in record
        For i As Integer = 0 To objectDataTable.Rows.Count - 1
            If objectDataTable.Rows(i)(1).ToString.ToLower = "npwp" Then npwpExist = True
        Next

        If Not npwpExist Then
            'add NPWP record
            Dim oRow As DataRow
            oRow = objectDataTable.NewRow()
            oRow(0) = objectDataTable.Rows.Count + 1
            oRow(1) = "NPWP"
            oRow(2) = Me.NPWP
            oRow(3) = BusinessDate
            oRow(4) = BusinessDate
            objectDataTable.Rows.Add(oRow)
        End If


        dtgList2.DataSource = objectDataTable
        dtgList2.DataBind()
        fillCboType()

        Dim cboTypeValue As String = String.Empty
        For intLoopGrid = 0 To dtgList2.Items.Count - 1
            Dim txtTglDokumen As New ucDateCE
            txtTglDokumen = CType(dtgList2.Items(intLoopGrid).FindControl("txtTglDokumen"), ucDateCE)
            Dim txtTglJT As New ucDateCE
            txtTglJT = CType(dtgList2.Items(intLoopGrid).FindControl("txtTglJT"), ucDateCE)

            txtTglDokumen.IsRequired = True
            lblNo2 = CType(dtgList2.Items(intLoopGrid).Cells(0).FindControl("lblNo2"), Label)
            cboType2 = CType(dtgList2.Items(intLoopGrid).Cells(1).FindControl("cboType2"), DropDownList)
            txtDocNo2 = CType(dtgList2.Items(intLoopGrid).Cells(2).FindControl("txtDocNo2"), TextBox)
            txtNotes2 = CType(dtgList2.Items(intLoopGrid).Cells(5).FindControl("txtNotes2"), TextBox)
            imbDelete2 = CType(dtgList2.Items(intLoopGrid).Cells(6).FindControl("imbDelete2"), ImageButton)

            lblNo2.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim
            If Not IsDBNull(objectDataTable.Rows(intLoopGrid).Item(1)) Or objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim <> "" Then
                cboTypeValue = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
                cboType2.SelectedValue = cboTypeValue
                'If cboTypeValue.ToLower = "npwp" Then imbDelete2.Visible = False

            End If
            txtDocNo2.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            txtTglDokumen.Text = IIf(Day(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString.Length = 1, "0", "").ToString + Day(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString + "/" + IIf(Month(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString.Length = 1, "0", "").ToString + Month(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString + "/" + Year(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString
            txtTglJT.Text = IIf(Day(CDate(objectDataTable.Rows(intLoopGrid).Item(4))).ToString.Length = 1, "0", "").ToString + Day(CDate(objectDataTable.Rows(intLoopGrid).Item(4))).ToString + "/" + IIf(Month(CDate(objectDataTable.Rows(intLoopGrid).Item(4))).ToString.Length = 1, "0", "").ToString + Month(CDate(objectDataTable.Rows(intLoopGrid).Item(4))).ToString + "/" + Year(CDate(objectDataTable.Rows(intLoopGrid).Item(4))).ToString
            txtNotes2.Text = objectDataTable.Rows(intLoopGrid).Item(5).ToString.Trim



        Next
        If dtgList2.Items.Count = 0 Then
            AddRecord2()
        End If
    End Sub
#End Region
#Region "Baris DtgList2"
    Private Sub imbAdd2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd2.Click
        AddRecord2()
    End Sub
    Private Sub AddRecord2()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo2 As New Label
        Dim cboType2 As New DropDownList
        Dim txtDocNo2 As New TextBox
        Dim txtNotes2 As New TextBox
        Dim npwpExist As Boolean = False
        With objectDataTable
            .Columns.Add(New DataColumn("lblNo2", GetType(String)))
            .Columns.Add(New DataColumn("cboType2", GetType(String)))
            .Columns.Add(New DataColumn("txtDocNo2", GetType(String)))
            .Columns.Add(New DataColumn("uscDate2", GetType(String)))
            .Columns.Add(New DataColumn("txtNotes2", GetType(String)))
        End With

        'get existing data
        For intLoopGrid = 0 To dtgList2.Items.Count - 1
            lblNo2 = CType(dtgList2.Items(intLoopGrid).Cells(0).FindControl("lblNo2"), Label)
            cboType2 = CType(dtgList2.Items(intLoopGrid).Cells(1).FindControl("cboType2"), DropDownList)
            txtDocNo2 = CType(dtgList2.Items(intLoopGrid).Cells(2).FindControl("txtDocNo2"), TextBox)
            Dim txtTglDokumen As New ucDateCE
            txtTglDokumen = CType(dtgList2.Items(intLoopGrid).FindControl("txtTglDokumen"), ucDateCE)
            txtNotes2 = CType(dtgList2.Items(intLoopGrid).Cells(4).FindControl("txtNotes2"), TextBox)
            oRow = objectDataTable.NewRow()
            oRow("lblNo2") = CType(lblNo2.Text, String)
            oRow("cboType2") = CType(cboType2.SelectedValue, String)
            oRow("txtDocNo2") = CType(txtDocNo2.Text, String)
            oRow("uscDate2") = CType(txtTglDokumen.Text, String)
            oRow("txtNotes2") = CType(txtNotes2.Text, String)
            objectDataTable.Rows.Add(oRow)

            If cboType2.SelectedValue.ToLower = "npwp" Then npwpExist = True
        Next

        If Not npwpExist Then
            'add npwp row
            oRow = objectDataTable.NewRow()
            oRow("lblNo2") = dtgList2.Items.Count + 1
            oRow("cboType2") = "NPWP"
            oRow("txtDocNo2") = Me.NPWP
            oRow("uscDate2") = ""
            oRow("txtNotes2") = ""
            objectDataTable.Rows.Add(oRow)
        Else
            oRow = objectDataTable.NewRow()
            oRow("lblNo2") = dtgList2.Items.Count + 1
            oRow("cboType2") = "Select One"
            oRow("txtDocNo2") = ""
            oRow("uscDate2") = ""
            oRow("txtNotes2") = ""
            objectDataTable.Rows.Add(oRow)
        End If


        dtgList2.DataSource = objectDataTable
        dtgList2.DataBind()
        fillCboType()

        'fill value in each record
        For intLoopGrid = 0 To dtgList2.Items.Count - 1
            Dim txtTglDokumen As New ucDateCE
            txtTglDokumen = CType(dtgList2.Items(intLoopGrid).FindControl("txtTglDokumen"), ucDateCE)

            lblNo2 = CType(dtgList2.Items(intLoopGrid).Cells(0).FindControl("lblNo2"), Label)
            cboType2 = CType(dtgList2.Items(intLoopGrid).Cells(1).FindControl("cboType2"), DropDownList)
            txtDocNo2 = CType(dtgList2.Items(intLoopGrid).Cells(2).FindControl("txtDocNo2"), TextBox)
            txtTglDokumen.Attributes.Add("readOnly", "true")
            txtNotes2 = CType(dtgList2.Items(intLoopGrid).Cells(4).FindControl("txtNotes2"), TextBox)
            lblNo2.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString
            If objectDataTable.Rows(intLoopGrid).Item(1).ToString <> "" Then
                cboType2.SelectedIndex = cboType2.Items.IndexOf(cboType2.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim))
            End If
            txtDocNo2.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString
            txtTglDokumen.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString
            txtNotes2.Text = objectDataTable.Rows(intLoopGrid).Item(4).ToString
        Next

    End Sub
    Sub fillCboType()
        Dim dtEntity As DataTable
        Dim oCustomer As New Parameter.Customer
        Dim intLoop As Integer
        Dim cboType2 As DropDownList
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblLegalDocument"
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        For intLoop = 0 To dtgList2.Items.Count - 1
            cboType2 = CType(dtgList2.Items(intLoop).Cells(3).FindControl("cboType2"), DropDownList)
            cboType2.DataSource = dtEntity.DefaultView
            cboType2.DataTextField = "Description"
            cboType2.DataValueField = "ID"
            cboType2.DataBind()
            cboType2.Items.Insert(0, "Select One")
            cboType2.Items(0).Value = "Select One"
        Next
    End Sub
    Sub DeleteBaris2(ByVal Index As Integer)
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo2 As New Label
        Dim cboType2 As New DropDownList
        Dim txtDocNo2 As New TextBox
        Dim txtNotes2 As New TextBox
        Dim oNewDataTable As New DataTable
        With oNewDataTable
            .Columns.Add(New DataColumn("cboType2", GetType(String)))
            .Columns.Add(New DataColumn("txtDocNo2", GetType(String)))
            .Columns.Add(New DataColumn("uscDate2", GetType(String)))
            .Columns.Add(New DataColumn("txtNotes2", GetType(String)))
        End With

        For intLoopGrid = 0 To dtgList2.Items.Count - 1
            cboType2 = CType(dtgList2.Items(intLoopGrid).Cells(1).FindControl("cboType2"), DropDownList)
            txtDocNo2 = CType(dtgList2.Items(intLoopGrid).Cells(2).FindControl("txtDocNo2"), TextBox)
            Dim txtTglDokumen As New ucDateCE
            txtTglDokumen = CType(dtgList2.Items(intLoopGrid).FindControl("txtTglDokumen"), ucDateCE)
            txtNotes2 = CType(dtgList2.Items(intLoopGrid).Cells(4).FindControl("txtNotes2"), TextBox)
            If intLoopGrid <> Index Then
                oRow = oNewDataTable.NewRow()
                oRow("cboType2") = CType(cboType2.SelectedValue, String)
                oRow("txtDocNo2") = CType(txtDocNo2.Text, String)
                oRow("uscDate2") = CType(txtTglDokumen.Text, String)
                oRow("txtNotes2") = CType(txtNotes2.Text, String)
                oNewDataTable.Rows.Add(oRow)
            End If
        Next
        dtgList2.DataSource = oNewDataTable
        dtgList2.DataBind()
        fillCboType()
        For intLoopGrid = 0 To dtgList2.Items.Count - 1
            Dim txtTglDokumen As New ucDateCE
            txtTglDokumen = CType(dtgList2.Items(intLoopGrid).FindControl("txtTglDokumen"), ucDateCE)

            lblNo2 = CType(dtgList2.Items(intLoopGrid).Cells(0).FindControl("lblNo2"), Label)
            cboType2 = CType(dtgList2.Items(intLoopGrid).Cells(1).FindControl("cboType2"), DropDownList)
            txtDocNo2 = CType(dtgList2.Items(intLoopGrid).Cells(2).FindControl("txtDocNo2"), TextBox)
            txtTglDokumen.Attributes.Add("readOnly", "true")
            txtNotes2 = CType(dtgList2.Items(intLoopGrid).Cells(4).FindControl("txtNotes2"), TextBox)
            lblNo2.Text = (intLoopGrid + 1).ToString
            If oNewDataTable.Rows(intLoopGrid).Item("cboType2").ToString <> "" Then
                cboType2.SelectedIndex = cboType2.Items.IndexOf(cboType2.Items.FindByValue(oNewDataTable.Rows(intLoopGrid).Item("cboType2").ToString.Trim))
            End If
            txtDocNo2.Text = oNewDataTable.Rows(intLoopGrid).Item("txtDocNo2").ToString
            txtTglDokumen.Text = oNewDataTable.Rows(intLoopGrid).Item("uscDate2").ToString
            txtNotes2.Text = oNewDataTable.Rows(intLoopGrid).Item("txtNotes2").ToString
        Next
    End Sub
#End Region
#Region "ItemCommand" 
    Private Sub dtgList2_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList2.ItemCommand
        If e.CommandName = "Delete2" Then
            DeleteBaris2(e.Item.ItemIndex)
        End If
    End Sub
#End Region    
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ocustomer As New Parameter.Customer        
        Dim intLoopGrid As Integer        
        Dim oData2 As New DataTable
        Dim oRow As DataRow
        Dim cboType2 As String
        Dim txtDocNo2 As String
        Dim txtTglDokumen As String
        Dim txtTglJT As String
        Dim txtNotes2 As String               

        status = True
        Dim no As Integer = 1        

        With oData2
            .Columns.Add(New DataColumn("lblNo2", GetType(String)))
            .Columns.Add(New DataColumn("cboType2", GetType(String)))
            .Columns.Add(New DataColumn("txtDocNo2", GetType(String)))
            .Columns.Add(New DataColumn("uscDate2", GetType(String)))
            .Columns.Add(New DataColumn("uscDate3", GetType(String)))
            .Columns.Add(New DataColumn("txtNotes2", GetType(String)))
        End With

        no = 1
        For intLoopGrid = 0 To dtgList2.Items.Count - 1
            cboType2 = CType(dtgList2.Items(intLoopGrid).Cells(1).FindControl("cboType2"), DropDownList).SelectedValue
            txtDocNo2 = CType(dtgList2.Items(intLoopGrid).Cells(2).FindControl("txtDocNo2"), TextBox).Text
            txtTglDokumen = CType(dtgList2.Items(intLoopGrid).Cells(3).FindControl("txtTglDokumen"), ucDateCE).Text
            txtTglJT = CType(dtgList2.Items(intLoopGrid).Cells(4).FindControl("txtTglJT"), ucDateCE).Text
            txtNotes2 = CType(dtgList2.Items(intLoopGrid).Cells(5).FindControl("txtNotes2"), TextBox).Text
            If cboType2.Trim <> "Select One" And txtDocNo2.Trim <> "" And txtTglDokumen.Trim <> "" Then
                oRow = oData2.NewRow
                oRow("lblNo2") = no
                oRow("cboType2") = cboType2
                oRow("txtDocNo2") = txtDocNo2
                oRow("uscDate2") = ConvertDate(txtTglDokumen)
                oRow("uscDate3") = ConvertDate(txtTglJT)
                oRow("txtNotes2") = txtNotes2
                oData2.Rows.Add(oRow)
                no += 1
            Else
                If Not (txtDocNo2.Trim = "" And txtTglDokumen.Trim = "" And txtNotes2.Trim = "" And cboType2.Trim = "Select One") Then
                    If cboType2.Trim = "Select One" Then
                        CType(dtgList2.Items(intLoopGrid).Cells(1).FindControl("lblVType2"), Label).Visible = True
                        status = False
                    ElseIf txtDocNo2.Trim = "" Then
                        CType(dtgList2.Items(intLoopGrid).Cells(2).FindControl("lblVDocNo2"), Label).Visible = True
                        status = False
                    ElseIf txtTglDokumen.Trim = "" Then
                        CType(dtgList2.Items(intLoopGrid).Cells(3).FindControl("lblVDate2"), Label).Visible = True
                        status = False
                    End If
                End If
            End If

        Next
        If status = False Then
            Exit Sub
        End If

        Try
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID

            Dim Err As String = ""            
            Err = m_controller.CompanyLegalitasSaveEdit(ocustomer,oData2)
            If Err = "" Then
                'Response.Redirect("CustomerCompanyManagement.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabManagement")
                'Modify by WIra 20171023
                If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                    Response.Redirect("CustomerCompanyManagement.aspx?page=Add&id=" + Me.CustomerID + "&pnl=tabManagement&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
                Else
                    Response.Redirect("CustomerCompanyManagement.aspx?page=Edit&id=" + Me.CustomerID + "&pnl=tabManagement&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
                End If
            Else
                ShowMessage(lblMessage, Err, True)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
        
    End Sub
#End Region
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click        
        Response.Redirect("Customer.aspx")
    End Sub

End Class