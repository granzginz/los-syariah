﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CompanyCustomerGuarantor.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CompanyCustomerGuarantor" %>

<%@ Register Src="CustomerTabsCompany.ascx" TagName="tabs" TagPrefix="uct" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc2" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script src="../../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <%--   <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>--%>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("#btnAdd").on("click", function () {
                $("div.tx-overlay").css("display", "block");
                $("#divAddGuarantor").css("display", "block");
            });
        });
        function doOK(e) {
            $("div.tx-overlay").css("display", "none");
            $("#divAddGuarantor").css("display", "none");
        }
           
    </script>
    <style>
        .f_left
        {
            padding: 0px !important;
            width: 49.9%;
        }
        .f_single
        {
            border-bottom: 1px solid #e5e5e5;
        }
    </style>
</head>
<body>
    <%--<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <uct:tabs id='cTabs' runat='server'>
    </uct:tabs>
    <%--  <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>--%>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <h4>
                <asp:Label ID="lblTitle" runat="server"> </asp:Label>
                DATA PENJAMIN</h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:GridView ID="dtgCSV" runat="server" CssClass="grid_general" BorderStyle="None"
                BorderWidth="0" AutoGenerateColumns="False" AllowSorting="false" Width="100%"
                DataKeyNames="ID" ShowHeader="False">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:PlaceHolder ID="objPHOrderDetails" runat="server" Visible="true">
                                <tr>
                                    <td>
                                        <h4>
                                            Data Penjamin -
                                            <%# eval("Seq") %></h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="background-color: white;">
                                        <div class="form_box">
                                            <div class="form_left  f_left">
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Nama Penjamin</label>
                                                    <label>
                                                        <%# eval("NamaPenjamin") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Jabatan</label>
                                                    <label>
                                                        <%# eval("JabatanDesc") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Tempat/Tanggal Lahir</label>
                                                    <label>
                                                        <%# eval("TempatLahir")%>
                                                        -
                                                        <%#Convert.ToDateTime(Eval("TglLahir")).ToString("dd/MM/yyyy") %>
                                                    </label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Jenis Dokumen</label>
                                                    <label>
                                                        <%# eval("JenisDokumenDesc") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        No Dokument</label>
                                                    <label>
                                                        <%# eval("NoDokumen") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Masa Berlaku KTP</label>
                                                    <label>
                                                        <%#Convert.ToDateTime(Eval("MasaBerlakuKtp")).ToString("dd/MM/yyyy") %>
                                                    </label>
                                                </div>
                                                <div class="form_button" id="divButton" runat="server">
                                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="../../Images/buttondelete.gif"
                                                        CommandName="Delete" OnCommand="CommandGrid_Click" CausesValidation="False" CommandArgument='<%# eval("Id") %>' />
                                                </div>
                                            </div>
                                            <div class="form_right  f_left">
                                                <div class="form_box_usercontrol_header">
                                                    <h4>
                                                        ALAMAT KTP</h4>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Alamat</label>
                                                    <label>
                                                        <%# eval("AlamatKTP.Address") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        RT/RW</label>
                                                    <label>
                                                        <%# eval("AlamatKTP.RT") %>
                                                        /
                                                        <%# eval("AlamatKTP.RW") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Kelurahan</label>
                                                    <label>
                                                        <%# eval("AlamatKTP.Kelurahan") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Kecamatan</label>
                                                    <label>
                                                        <%# eval("AlamatKTP.Kecamatan") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Kota</label>
                                                    <label>
                                                        <%# eval("AlamatKTP.City") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Kode Pos</label>
                                                    <label>
                                                        <%# eval("AlamatKTP.ZipCode") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        Telepon 1</label>
                                                    <label>
                                                        (
                                                        <%# eval("AlamatKTP.AreaPhone1") %>)&nbsp;<%# eval("AlamatKTP.Phone1") %></label>
                                                </div>
                                                <div class="form_single f_single ">
                                                    <label>
                                                        No HP</label>
                                                    <label>
                                                        <%# eval("AlamatKTP.MobilePhone") %></label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <style>
        .vleft
        {
            margin-left: 236px;
        }
    </style>
    <div class="form_button">
        <input id="btnAdd" type='button' value="Add" class="small button blue" causesvalidation="False" />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue"
            CausesValidation="False" />
        <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False" />--%>
    </div>
    <div class="tx-overlay" style="opacity: 0.5; display: none; z-index: 100">
    </div>
    <div id="divAddGuarantor" class="t-widget t-window" style="top: 35px; left: 80px;
        width: 90%; display: none; z-index: 100">
        <div style="margin: 20px; height: 298px">
            <div class="af05">
                <div class="form_box_header">
                    <div class="form_single">
                        <h4>
                            DATA PENJAMIN
                        </h4>
                    </div>
                    <div runat="server" id="jlookupContent" style="z-index: 4" />
                </div>
                <div class="form_box">
                    <div class="form_left  f_left">
                        <div class="form_box_usercontrol_header"> 
                            <h4>
                                PENJAMIN</h4> 
                        </div>
                        <div class="form_single f_single ">
                            <label class="label_split_req">
                                Nama Penjamin</label>
                            <asp:TextBox runat="server" ID="txtNamaPasangan" Width="25%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Masukan nama penjamin"
                                ControlToValidate="txtNamaPasangan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_single f_single ">
                            <label class="label_split_req">
                                Jabatan</label>
                            <asp:DropDownList ID="cboJabatan" runat="server" CssClass="select" />
                            <br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Harap pilih Jabatan"
                                ControlToValidate="cboJabatan" Display="Dynamic" InitialValue="Select One" CssClass="validator_general vleft "></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_single f_single ">
                       
                            <label class="label_split_req">
                                Tempat/Tanggal Lahir</label>
                            <asp:TextBox ID="txtBirthPlaceP" runat="server" MaxLength="20" Width="25%"></asp:TextBox>&nbsp;/
                            &nbsp;
                            <%-- <uc2:ucdatece id="txtTglLahirNew" runat="server" /> --%>
                            <asp:TextBox runat="server" ID="txtTglLahirNew" CssClass="small_text"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTglLahirNew"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <br />  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Masukan tempat lahir"
                                ControlToValidate="txtBirthPlaceP" Display="Dynamic" CssClass="validator_general vleft"/>  <br /> 
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Fomat tanggal salah"
                                ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtTglLahirNew"
                                SetFocusOnError="true" Display="Dynamic" CssClass="validator_general vleft" />
                            <asp:RequiredFieldValidator ID="rfvRentFinish" runat="server" ErrorMessage="Harap isi tanggal lahir"
                                ControlToValidate="txtTglLahirNew" CssClass="validator_general vleft" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_single f_single ">
                            <label class="label_split_req">
                                Jenis Dokumen</label>
                            <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select" />
                            <br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Jenis Dokumen"
                                ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general vleft"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form_single f_single ">
                            <label>
                                No Dokument</label>
                            <asp:TextBox runat="server" ID="txtNoDokument" MaxLength="20" Width="25%" onkeypress="return numbersonly2(event)"></asp:TextBox>
                        </div>
                        <div class="form_single f_single ">
                            <label class="label_split_req">
                                Masa Berlaku KTP</label>
                            <asp:TextBox runat="server" ID="txtMasaBerlakuKTP" CssClass="small_text" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtMasaBerlakuKTP"
                                Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <br />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Format tanggal salah"
                                ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtMasaBerlakuKTP"
                                SetFocusOnError="true" Display="Dynamic" CssClass="validator_general vleft"> 
                            </asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap isi tanggal berlaku KTP"
                                ControlToValidate="txtMasaBerlakuKTP" CssClass="validator_general vleft" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form_right  f_left"> 
                        <div class="form_box_usercontrol_header"> 
                            <h4>
                                ALAMAT KTP</h4> 
                        </div>
                        <uc1:uccompanyadress id="ucAddressSI_KTP" runat="server"></uc1:uccompanyadress>
                        <div class="form_single f_single ">
                            <label>
                                No HP</label>
                            <asp:TextBox runat="server" ID="txtMobilePhone" Width="25%"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAddGua" runat="server" Text="Save" CssClass="small button blue" />
                <button type='button' runat="server" class="small button gray" causesvalidation="False"
                    onclick="doOK();">
                    <span>Cancel</span></button>
            </div>
        </div>
    </div>
    <%--  </ContentTemplate>
    </asp:UpdatePanel>
    --%>
    </form>
</body>
</html>
