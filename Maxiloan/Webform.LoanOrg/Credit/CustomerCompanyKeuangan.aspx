﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerCompanyKeuangan.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerCompanyKeuangan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register Src="../../webform.UserController/ucKategoriPerusahaan.ascx" TagName="ucKategoriPerusahaan"
    TagPrefix="uc3" %>
<%@ Register Src="../../webform.UserController/ucKondisiKantor.ascx" TagName="ucKondisiKantor"
    TagPrefix="uc4" %>
<%@ Register Src="../../webform.UserController/ucLookupGroupCust.ascx" TagName="ucLookupGroupCust"
    TagPrefix="uc5" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="CustomerTabsCompany.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Company</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
        <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Page_Validators = new Array();

        function BusinessPlaceStatus() {
            var myID = 'txtTanggalSewa';
            var objDate = eval('document.forms[0].' + myID);
            if (document.forms[0].cboBusPlaceStatus.value == 'KR') {
                objDate.disabled = false;
                ValidatorEnable(document.getElementById("rfvRentFinish"), true);
            }
            else {
                objDate.disabled = true;
                objDate.value = '';
                ValidatorEnable(document.getElementById("rfvRentFinish"), false);
            }


        }			
    </script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
             <uct:tabs id='cTabs' runat='server'></uct:tabs> 
   <%-- <div class="tab_container">
        <div id="tabDataKonsumen" runat="server">
            <asp:HyperLink ID="hyDataKonsumen" runat="server" Text="DATA KONSUMEN"></asp:HyperLink>
        </div>
        <div id="tabLegalitas" runat="server">
            <asp:HyperLink ID="hyLegalitas" runat="server" Text="LEGALITAS"></asp:HyperLink>
        </div>
        <div id="tabManagement" runat="server">
            <asp:HyperLink ID="hyManagement" runat="server" Text="MANAGEMENT"></asp:HyperLink>
        </div> 
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>              
    </div>--%>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        CORPORATE CUSTOMER
                    </h4>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DATA KEUANGAN
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Omzet / Bulan
                    </label>
                    <uc1:ucnumberformat id="txtOmsetBulanan" runat="server" maxlength="15"></uc1:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Biaya / Bulan
                    </label>
                    <uc1:ucnumberformat id="txtBiayaBulanan" runat="server" maxlength="15"></uc1:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Current Ratio (%)
                    </label>
                    <uc1:ucnumberformat id="txtRatio" runat="server" maxlength="15"></uc1:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        ROI (%)
                    </label>
                    <uc1:ucnumberformat id="txtROI" runat="server" maxlength="15"></uc1:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        DER (%)
                    </label>
                    <uc1:ucnumberformat id="txtDER" runat="server" maxlength="15"></uc1:ucnumberformat>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Deposito</label>
                    <uc1:ucnumberformat id="txtDeposito" runat="server" maxlength="15"></uc1:ucnumberformat>
                </div>
            </div>
            <%--<div class="form_box">
                <div class="form_single">
                    <label>
                        Jenis Jaminan Tambahan</label>
                    <asp:TextBox ID="txtAddCollType" runat="server" MaxLength="15" CssClass="long_text"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jumlah Jaminan Tambahan</label>
                    <uc1:ucnumberformat id="txtAddCollAmt" runat="server" maxlength="15"></uc1:ucnumberformat>
                </div>
            </div>--%>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Resiko Usaha</label>
                    <asp:DropDownList ID="cboResikoUsaha" runat="server">
                        <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                        <asp:ListItem Value="1">Rendah</asp:ListItem>
                        <asp:ListItem Value="2">Sedang</asp:ListItem>
                        <asp:ListItem Value="3">Tinggi</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                        InitialValue="" ControlToValidate="cboResikoUsaha" ErrorMessage="Harap pilih resiko usaha!"
                        CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>
             <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Resiko Lingkungan dan Sosial</label>
                    <asp:DropDownList ID="cboResikoLS" runat="server">
                        <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                        <asp:ListItem Value="1">Rendah</asp:ListItem>
                        <asp:ListItem Value="2">Sedang</asp:ListItem>
                        <asp:ListItem Value="3">Tinggi</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                        InitialValue="" ControlToValidate="cboResikoLS" ErrorMessage="Harap pilih Resiko Lingkungan dan Sosial!"
                        CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form_title">
                <div class="form_single">
                    <asp:Label ID="Label2" runat="server" Visible="false"></asp:Label>
                    <h4>
                        REKENING BANK
                    </h4>
                </div>
            </div>
            <div class="form_box_uc">
                <uc1:ucbankaccount id="UCBankAcc" runat="server">
            </uc1:ucbankaccount>
            </div>            
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
