﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

#End Region

Public Class ViewAsset
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New AssetDataController
#End Region

#Region "Property"

    Property ApplicationID() As String
        Get
            Return viewstate("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Property AgreementNo() As String
        Get
            Return viewstate("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property


    Property CustomerName() As String
        Get
            Return viewstate("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property


    Property CustomerID() As String
        Get
            Return viewstate("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return viewstate("Style").ToString
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property




#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "ViewAsset"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then

                Me.ApplicationID = Request("ApplicationID").ToString

                Me.CustomerName = Request("CustomerName").ToString
                Me.Style = Request("Style").ToString
                Me.CustomerID = Request("CustomerID").ToString.Trim
                Me.AgreementNo = Request("AgreementNo").ToString
                lblCustomerName.Text = Me.CustomerName

                Bindgrid()

                Dim lb As New LinkButton
                lb = CType(Me.FindControl("lblCustomerName"), LinkButton)
                lb.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")

            End If
            lblMessage.Text = ""
        End If
    End Sub

    Sub Bindgrid()
        Dim dt As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(getConnectionString)
        Dim objReader As SqlDataReader

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewAsset"
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objCommand.Connection = objConnection
            objReader = objCommand.ExecuteReader()

            If (objReader.Read) Then
                lblAppID.Text = objReader.Item("ApplicationID").ToString.Trim
                lblSupplier.Text = objReader.Item("SupplierName").ToString.Trim
                'lblSSI.Text = objReader.Item("Incentive").ToString.Trim
                lblAssetDesc.Text = objReader.Item("Description").ToString.Trim
                'lblOTR.Text = FormatNumber(objReader.Item("TotalOTR").ToString.Trim, 0)
                'lblDP.Text = FormatNumber(objReader.Item("DownPayment").ToString.Trim, 0)
                lblWarna.Text = objReader.Item("Warna").ToString.Trim
                lblNoPolisi.Text = objReader.Item("NoPolisi").ToString.Trim

                lblS1.Text = objReader.Item("SerialNo1Label").ToString.Trim
                lblS2.Text = objReader.Item("SerialNo2Label").ToString.Trim
                lblSerial1.Text = objReader.Item("SerialNo1").ToString.Trim
                lblSerial2.Text = objReader.Item("SerialNo2").ToString.Trim
                lblYear.Text = objReader.Item("ManufacturingYear").ToString.Trim
                lblUsedNew.Text = objReader.Item("UsedNew").ToString.Trim
                lblUsage.Text = objReader.Item("Usage").ToString.Trim
                lblRegName.Text = objReader.Item("OwnerAsset").ToString.Trim
                lblRegAddress.Text = objReader.Item("OwnerAddress").ToString.Trim
                lblRegRT.Text = objReader.Item("OwnerRT").ToString.Trim
                lblRegRW.Text = objReader.Item("OwnerRW").ToString.Trim
                lblRegKelurahan.Text = objReader.Item("OwnerKelurahan").ToString.Trim
                lblRegKecamatan.Text = objReader.Item("OwnerKecamatan").ToString.Trim
                lblRegCity.Text = objReader.Item("OwnerCity").ToString.Trim
                lblRegZip.Text = objReader.Item("OwnerZipCode").ToString.Trim
                If objReader.Item("TaxDate").ToString.Trim <> "" Then
                    lblRegTaxDate.Text = Format(CDate(objReader.Item("TaxDate")), "dd/MM/yyyy")
                End If
                lblRegNotes.Text = objReader.Item("Notes").ToString.Trim
                'lblInsInsured.Text = objReader.Item("InsuredBy").ToString.Trim
                'lblInsPaid.Text = objReader.Item("PaidBy").ToString.Trim
                LblLicPlateValue.Text = objReader.Item("LicensePlate").ToString.Trim
                If LblLicPlateValue.Text <> "-" Then
                    LblLicPlateValue.Visible = True
                    LblLicPlateText.Visible = True
                    LblLicPlateText.Text = "License Plate"
                End If
            End If
            objReader.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try


        '========================
        'Untuk Grid AssetDoc
        '========================
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        Dim oDataAttribute As New DataTable
        Dim oDataAssetDoc As New DataTable
        oAssetData.strConnection = GetConnectionString
        oAssetData.AppID = lblAppID.Text

        oAssetData = m_controller.GetViewAssetData(oAssetData)

        If Not oAssetData Is Nothing Then
            oData = oAssetData.ListData
            oDataAssetDoc = oAssetData.DataAssetdoc
        End If

        dtgAssetDoc.DataSource = oDataAssetDoc.DefaultView
        dtgAssetDoc.DataBind()

        Dim lb As New LinkButton
        lb = CType(Me.FindControl("lblCustomerName"), LinkButton)
        lb.Attributes.Add("onclick", "return OpenViewCustomer('" & Me.CustomerID & "','" & Me.Style & "')")

    End Sub

    Private Sub dtgAssetDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAssetDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            CType(e.Item.FindControl("lblNo"), Label).Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub
    Private Sub Click_btnBack(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Response.Redirect("viewStatementOfAccount.aspx?AgreementNo= " & Me.AgreementNo & "&Style=" & Me.Style & "&ApplicationID=" & Me.ApplicationID & "")
    End Sub
End Class