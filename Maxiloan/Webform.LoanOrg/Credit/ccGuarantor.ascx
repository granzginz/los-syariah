﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ccGuarantor.ascx.vb" Inherits="Maxiloan.Webform.LoanOrg.ccGuarantor" %> 
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddressCity" Src="../../Webform.UserController/ucAddressCity.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../../webform.UserController/ucMonthCombo.ascx" TagName="ucMonthCombo" TagPrefix="uc2" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>

<asp:Panel ID="pnlDataPasangan" runat="server">
    <div class="form_box">
        <div class="form_left">  
            <label> Nama Penjamin</label>
            <asp:TextBox runat="server" ID="txtNamaPasangan"   Width="25%"></asp:TextBox>
        </div>

        <div class="form_right"> 
            <label> Nama Perusahaan/Jenis Usaha</label>
            <asp:TextBox ID="txtCompanyNameSI" runat="server" Width="50%"></asp:TextBox> 
        </div>
    </div>

    <div class="form_box">
        <div class="form_left">  
            <label>Tempat/Tanggal Lahir</label>
            <asp:TextBox ID="txtBirthPlaceP" runat="server" MaxLength="20"  Width="25%"></asp:TextBox>&nbsp;/ &nbsp;
            <uc2:ucdatece id="txtTglLahirNew" runat="server" />
        </div>

        <div class="form_right">
            <label> Bidang Usaha</label>
            <asp:HiddenField runat="server" ID="hdfKodeIndustriPS" />
            <asp:TextBox runat="server" ID="txtNamaIndustriPS" Enabled="false" CssClass="medium_text"></asp:TextBox>
            <button class="small buttongo blue" onclick="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriPS.ClientID & "&nama=" & txtNamaIndustriPS.ClientID) %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;"> ...</button>        
        </div>
    </div>

    <div class="form_box">
        <div class="form_left">   
            <label>Jenis Dokumen</label>
            <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select"/> 
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Jenis Identitas" ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>                           
        </div>

        <div class="form_right">  
            <label> Pekerjaan</label>
            <asp:DropDownList ID="cboJobTypeSI" runat="server"/> 
        </div> 
    </div>
                      
    <div class="form_box">
        <div class="form_left">  
            <label>No Dokument</label>
            <asp:TextBox runat="server" ID="txtNoDokument"  Width="25%"></asp:TextBox>                            
        </div>
        <div class="form_right"> 
            <label> Keterangan Usaha</label>
            <asp:TextBox ID="txtKeteranganUsahaSI" runat="server" Width="50%"></asp:TextBox>   
        </div>  
    </div>

    <div class="form_box">
        <div class="form_left">    
        <label>Masa Berlaku KTP</label>
            <uc2:ucdatece id="txtMasaBerlakuKTP" runat="server" />  
        </div>
    </div>
</asp:Panel> 