﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CustomerTabs.ascx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerTabs" %>
 <div class="tab_container">
        <div id="tabIdentitas" runat="server">
            <asp:HyperLink ID="hyIdentitas" runat="server" Text="IDENTITAS"></asp:HyperLink>
        </div>
        <div id="tabPekerjaan" runat="server">
            <asp:HyperLink ID="hyPekerjaan" runat="server" Text="PEKERJAAN"></asp:HyperLink>
        </div>
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>
        <div id="tabPasangan" runat="server">
            <asp:HyperLink ID="hyPasangan" runat="server" Text="PASANGAN"></asp:HyperLink>
        </div>
         <div id="tabGuarantor" runat="server">
            <asp:HyperLink ID="hyGuarantor" runat="server" Text="PENJAMIN"></asp:HyperLink>
        </div>
        <div id="tabEmergency" runat="server">
            <asp:HyperLink ID="hyEmergency" runat="server" Text="EMERGENCY CONTACT"></asp:HyperLink>
        </div>
        <div id="tabKeluarga" runat="server">
            <asp:HyperLink ID="hyKeluarga" runat="server" Text="KELUARGA"></asp:HyperLink>
        </div>
        <div id="tabPinjamanLainnya" runat="server">
            <asp:HyperLink ID="hyPinjamanLainnya" runat="server" Text="PINJAMAN LAINNYA"></asp:HyperLink>
        </div>
</div>