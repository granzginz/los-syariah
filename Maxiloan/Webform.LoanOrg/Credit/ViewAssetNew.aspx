﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAssetNew.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewAssetNew" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="ucassetdata" Src="../../Webform.UserController/ViewApplication/UcAssetData.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAsset</title>
    <link href="../../Include/Buttons.css" type="text/css" rel="Stylesheet"/>
    <link href="../../Include/General.css" type="text/css" rel="stylesheet" />
    <link href="../../Include/AccMnt.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
     <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - ASSET
            </h3>
        </div>
    </div>
     <div class="form_box">
                <div class="form_single">
                   <uc1:ucassetdata id="UcAssetData" runat="server"></uc1:ucassetdata>
                </div>
            </div>
  <%-- <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
            <asp:Button ID="btnBack"  runat="server"
            Text="Back" CssClass="small button gray" CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
