﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class CustomerPersonalPekerjaan
    Inherits AbsCustomerPersonal

#Region "uc control declaration"
    
    Protected WithEvents UcJDAddress As UcCompanyAddress 'ucAddress
    Protected WithEvents ucKategoriPerusahaanCustomer As ucKategoriPerusahaan
    Protected WithEvents ucKondisiKantorCustomer As ucKondisiKantor
    Protected WithEvents ucKaryawanSejakCustomer As ucMonthCombo        
    Private time As String
#End Region
#Region "Property"
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            RangeValidator2.MaximumValue = Year(Me.BusinessDate).ToString
            valDateRange.MaximumValue = Year(Me.BusinessDate).ToString

            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
            btnAlamatKerja1.Attributes.Add("OnClick", "return copyAlamatKTPkeAlamatKerja();")
            btnAlamatKerja2.Attributes.Add("OnClick", "return copyAlamatDomisilikeAlamatKerja();")
            UcJDAddress.Style = Style
            UcJDAddress.showMandatoryAll()
            UcJDAddress.BindAddress()

            FillCbo("tblProfession", cboPProfession)
            FillCbo("tblJobType", cboJobType)
            FillCbo("tblJobPosition", cboJobPosition)
            'FillCbo("IndustryType")
            FillCbo("tblIndustryHeader", cboIndrustriHeader)

            FillCbo("tblOccupation", cboOccupation)
            FillCbo("tblNatureOfBusiness", cboNaturalOfBusiness)
            'Modify by Wira 20171010---
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then                
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------
            If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With

                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)

                cboPProfession.SelectedIndex = cboPProfession.Items.IndexOf(cboPProfession.Items.FindByValue(ocustomclass.ProfessionID.Trim))
                'cboJobType.SelectedIndex = cboJobType.Items.IndexOf(cboJobType.Items.FindByValue(ocustomclass.JobTypeID.Trim))
                'cboIndustryType.SelectedIndex = cboIndustryType.Items.IndexOf(cboIndustryType.Items.FindByValue(ocustomclass.IndustryTypeID.Trim))

                'Modify by WIra 20171010
                cboIndrustriHeader.SelectedIndex = cboIndrustriHeader.Items.IndexOf(cboIndrustriHeader.Items.FindByValue(ocustomclass.KodeIndustri.Trim))
                hdfKodeIndustriBU.Value = ocustomclass.KodeIndustriDetail.Trim
                txtNamaIndustriBU.Text = ocustomclass.NamaIndustri.Trim
                cboNaturalOfBusiness.SelectedIndex = cboNaturalOfBusiness.Items.IndexOf(cboNaturalOfBusiness.Items.FindByValue(ocustomclass.NatureOfBusinessId.Trim))
                cboIndustryRisk.SelectedIndex = cboIndustryRisk.Items.IndexOf(cboIndustryRisk.Items.FindByValue(ocustomclass.IndustryRisk.Trim))
                cboOccupation.SelectedIndex = cboOccupation.Items.IndexOf(cboOccupation.Items.FindByValue(ocustomclass.OccupationId.Trim))
                cboJobType.SelectedIndex = cboJobType.Items.IndexOf(cboJobType.Items.FindByValue(ocustomclass.ProfessionID.Trim))
                ucKaryawanSejakCustomer.SelectedMonth = ocustomclass.EmploymentSinceMonth
                txtEmploymentYear.Text = ocustomclass.EmploymentSinceYear
                Me.StatusPerkawinan = ocustomclass.MaritalStatus
                cboIndrustriHeader.Enabled = False
                txtNamaIndustriBU.Enabled = False
                cboNaturalOfBusiness.Enabled = False
                cboOccupation.Enabled = False
                cboJobType.Enabled = False
                ucKaryawanSejakCustomer.FeatureEnabled(False)
                txtEmploymentYear.Enabled = False
                txtEmploymentYear_OnTextChanged()
            End If

            'UcJDAddress.ValidatorFalse()
            Me.PageAddEdit = Request("page")
            'Dim mTabHelperModule = New TabHelperModule(New Dictionary(Of String, CustomerTab) From {
            '  {"tabIdentitas", New CustomerTab(tabIdentitas, hyIdentitas)},
            '  {"tabPekerjaan", New CustomerTab(tabPekerjaan, hyPekerjaan)},
            '  {"tabKeuangan", New CustomerTab(tabKeuangan, hyKeuangan)},
            '  {"tabPasangan", New CustomerTab(tabPasangan, hyPasangan)},
            '  {"tabEmergency", New CustomerTab(tabEmergency, hyEmergency)},
            '  {"tabKeluarga", New CustomerTab(tabKeluarga, hyKeluarga)}})


            If Me.PageAddEdit = "Add" Then
                lblTitle.Text = "ADD"
                GetXML()
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)

                cTabs.SetNavigateUrl(Request("page"), Request("id"))

            End If
            cTabs.RefreshAttr(Request("pnl"))
            If Me.StatusPerkawinan.ToUpper.Trim <> "M" Then
                cTabs.EnabledLink("tabPasangan", False)
            End If
        End If
    End Sub 

#End Region
    
#Region "FillCbo"
    Sub FillCbo(ByVal table As String, cboReference As DropDownList)
        Dim dtEntity As DataTable
        Dim oCustomer As New Parameter.Customer
        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = table
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

         
        cboReference.DataSource = dtEntity.DefaultView
        cboReference.DataTextField = "Description"
        cboReference.DataValueField = "ID"
        cboReference.DataBind()
        cboReference.Items.Insert(0, "Select One")
        cboReference.Items(0).Value = "Select One"

        'Select Case table
        '    Case "tblProfession"
        '        'cboPProfession.DataSource = dtEntity.DefaultView
        '        'cboPProfession.DataTextField = "Description"
        '        'cboPProfession.DataValueField = "ID"
        '        'cboPProfession.DataBind()
        '        'cboPProfession.Items.Insert(0, "Select One")
        '        'cboPProfession.Items(0).Value = "Select One"

        '    Case "tblJobType"
        '        'cboJobType.DataSource = dtEntity.DefaultView
        '        'cboJobType.DataTextField = "Description"
        '        'cboJobType.DataValueField = "ID"
        '        'cboJobType.DataBind()
        '        'cboJobType.Items.Insert(0, "Select One")
        '        'cboJobType.Items(0).Value = "Select One"

        '    Case "tblJobPosition"
        '        cboJobPosition.DataSource = dtEntity.DefaultView
        '        cboJobPosition.DataTextField = "Description"
        '        cboJobPosition.DataValueField = "ID"
        '        cboJobPosition.DataBind()
        '        cboJobPosition.Items.Insert(0, "Select One")
        '        cboJobPosition.Items(0).Value = "Select One"

        '    Case "tblIndustryHeader"
        '        cboIndrustriHeader.DataSource = dtEntity.DefaultView
        '        cboIndrustriHeader.DataTextField = "Description"
        '        cboIndrustriHeader.DataValueField = "ID"
        '        cboIndrustriHeader.DataBind()
        '        cboIndrustriHeader.Items.Insert(0, "Select One")
        '        cboIndrustriHeader.Items(0).Value = "Select One"
        'End Select
    End Sub
#End Region
#Region "Button"
    Private Sub btnAlamatKerja1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlamatKerja1.Click
        Dim dtEntity As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = Me.CustomerID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "3")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        UcJDAddress.Address = oRow("LegalAddress").ToString.Trim & IIf(oRow("LegalRT").ToString.Trim = "", "", " RT." & oRow("LegalRT").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalRW").ToString.Trim = "", "", " RW." & oRow("LegalRW").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalKelurahan").ToString.Trim = "", "", oRow("LegalKelurahan").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("LegalKecamatan").ToString.Trim = "", "", oRow("LegalKecamatan").ToString.Trim & "").ToString

        UcJDAddress.RT = oRow("LegalRT").ToString.Trim
        UcJDAddress.RW = oRow("LegalRW").ToString.Trim
        UcJDAddress.Kecamatan = oRow("LegalKecamatan").ToString.Trim
        UcJDAddress.Kelurahan = oRow("LegalKelurahan").ToString.Trim
        UcJDAddress.Kecamatan = oRow("LegalKecamatan").ToString.Trim
        UcJDAddress.City = oRow("LegalCity").ToString.Trim
        UcJDAddress.ZipCode = oRow("LegalZipCode").ToString.Trim
        UcJDAddress.AreaPhone1 = oRow("LegalAreaPhone1").ToString.Trim
        UcJDAddress.Phone1 = oRow("LegalPhone1").ToString.Trim
        UcJDAddress.AreaPhone2 = oRow("LegalAreaPhone2").ToString.Trim
        UcJDAddress.Phone2 = oRow("LegalPhone2").ToString.Trim
        UcJDAddress.AreaFax = oRow("LegalAreaFax").ToString.Trim
        UcJDAddress.Fax = oRow("LegalFax").ToString.Trim
        UcJDAddress.BindAddress()
    End Sub
    Private Sub btnAlamatKerja2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlamatKerja2.Click
        Dim dtEntity As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = Me.CustomerID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "3")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)

        UcJDAddress.Address = oRow("ResidenceAddress").ToString.Trim & IIf(oRow("ResidenceRT").ToString.Trim = "", "", " RT." & oRow("ResidenceRT").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceRW").ToString.Trim = "", "", " RW." & oRow("ResidenceRW").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceKelurahan").ToString.Trim = "", "", oRow("ResidenceKelurahan").ToString.Trim & " ").ToString _
                            + " " & IIf(oRow("ResidenceKecamatan").ToString.Trim = "", "", oRow("ResidenceKecamatan").ToString.Trim & "").ToString
        UcJDAddress.RT = oRow("ResidenceRT").ToString.Trim
        UcJDAddress.RW = oRow("ResidenceRW").ToString.Trim
        UcJDAddress.Kelurahan = oRow("ResidenceKelurahan").ToString.Trim
        UcJDAddress.Kecamatan = oRow("ResidenceKecamatan").ToString.Trim
        UcJDAddress.City = oRow("ResidenceCity").ToString.Trim
        UcJDAddress.ZipCode = oRow("ResidenceZipCode").ToString.Trim
        UcJDAddress.AreaPhone1 = oRow("ResidenceAreaPhone1").ToString.Trim
        UcJDAddress.Phone1 = oRow("ResidencePhone1").ToString.Trim
        UcJDAddress.AreaPhone2 = oRow("ResidenceAreaPhone2").ToString.Trim
        UcJDAddress.Phone2 = oRow("ResidencePhone2").ToString.Trim
        UcJDAddress.AreaFax = oRow("ResidenceAreaFax").ToString.Trim
        UcJDAddress.Fax = oRow("ResidenceFax").ToString.Trim
        UcJDAddress.BindAddress()
    End Sub
#End Region
#Region "imbPCancel_Click"
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
#End Region
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Try

          

            Dim ocustomer As New Parameter.Customer
            Dim oJDAddress As New Parameter.Address
            Dim oPCEX As New Parameter.CustomerEX
            Dim Err As String = ""

            ocustomer.PersonalCustomerType = rboTypeP.SelectedValue
            ocustomer.ProfessionID = cboPProfession.SelectedValue.Trim
            ocustomer.JobTypeID = cboJobType.SelectedValue.Trim
            ocustomer.JobPos = cboJobPosition.SelectedValue.Trim
            ocustomer.CompanyName = txtCompanyName.Text.Trim
            If (hdfKodeIndustriBU.Value.Trim = "") Then
                Err = "Silahkan pilih Bidang Usaha Detail."
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
            ocustomer.IndustryTypeID = hdfKodeIndustriBU.Value.Trim
            oJDAddress.Address = UcJDAddress.Address.Trim
            oJDAddress.RT = UcJDAddress.RT.Trim
            oJDAddress.RW = UcJDAddress.RW.Trim
            oJDAddress.Kelurahan = UcJDAddress.Kelurahan.Trim
            oJDAddress.Kecamatan = UcJDAddress.Kecamatan.Trim
            oJDAddress.City = UcJDAddress.City.Trim
            oJDAddress.ZipCode = UcJDAddress.ZipCode.Trim
            oJDAddress.AreaPhone1 = UcJDAddress.AreaPhone1.Trim
            oJDAddress.Phone1 = UcJDAddress.Phone1.Trim
            oJDAddress.AreaPhone2 = UcJDAddress.AreaPhone2.Trim
            oJDAddress.Phone2 = UcJDAddress.Phone2.Trim
            oJDAddress.AreaFax = UcJDAddress.AreaFax.Trim
            oJDAddress.Fax = UcJDAddress.Fax.Trim
            ocustomer.CompanyJobTitle = txtJobTitle.Text.Trim
            ocustomer.EmploymentSinceYear = txtEmploymentYear.Text.Trim
            ocustomer.OtherBusinessName = txtOBName.Text.Trim
            ocustomer.JobsKantorOperasiSejak = CInt(IIf(txtOperasiSejak.Text.Trim = "", "0", txtOperasiSejak.Text.Trim))
           
            ocustomer.JobsKantorNPWP = txtPNPWP.Text.Trim
            ocustomer.OccupationID = cboOccupation.SelectedValue
            ocustomer.NatureOfBusinessId = cboNaturalOfBusiness.SelectedValue
            ocustomer.IndustryRisk = cboIndustryRisk.SelectedValue
            ocustomer.IsRelatedBNI = rboIsRelasiBNI.Checked
            ocustomer.IsEmployee = cboIsEmployee.Checked
            With oPCEX
                .KategoryPers = ucKategoriPerusahaanCustomer.SelectedKategori
                .KondisiKantor = ucKondisiKantorCustomer.SelectedKondisi
                .KaryawanSejakBulan = ucKaryawanSejakCustomer.SelectedMonth
                .TotalLamaKerja = CInt(txtTotalLamaKerja.Text)
            End With

            'Dim Err As String = ""
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID
            Err = m_controller.PersonalCustomerPekerjaanSaveEdit(ocustomer, _
                                                oJDAddress, _
                                                oPCEX)
            If Err = "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
            'Response.Redirect("CustomerPersonalKeuangan.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabKeuangan")
            'Modify by WIra 20171010
            Response.Redirect("CustomerPersonalKeuangan.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabKeuangan&branchID=" + Me.BranchID + "&prospectappid=" + Me.ProspectAppID + "&ActivityDateStart=" & Me.ActivityDateStart & "")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
#End Region
#Region "Edit"
    Sub BindEdit(ByVal ID As String)
        Dim dtEntity As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = ID
        oCustomer.CustomerType = "P"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "4")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)

        Me.Name = oRow("Name").ToString.Trim
        Me.IDType = oRow("IDType").ToString.Trim
        Me.IDTypeName = oRow("IDTypeName").ToString.Trim
        Me.IDNumber = oRow("IDNumber").ToString.Trim
        Me.Gender = oRow("Gender").ToString.Trim

        If IsDate(oRow("IDExpiredDate").ToString) Then Me.IDExpiredDate = Format(oRow("IDExpiredDate"), "dd/MM/yyyy")

        If Not IsDBNull(oRow("BirthDate")) Then
            Me.BirthDate = Day(CDate(oRow("BirthDate"))).ToString + "/" + Month(CDate(oRow("BirthDate"))).ToString + "/" + Year(CDate(oRow("BirthDate"))).ToString
        Else
            Me.BirthDate = ""
        End If

        Me.BirthPlace = oRow("BirthPlace").ToString.Trim
        Me.Type = oRow("PersonalCustomerType").ToString.Trim

        Me.MotherName = oRow("MotherName").ToString.Trim
        Me.NamaPasangan = oRow("SINamaSuamiIstri").ToString.Trim
        Me.KartuKeluarga = oRow("NoKK").ToString.Trim
        Me.KartuKeluarga = oRow("NoKK").ToString.Trim
        Me.StatusPerkawinan = oRow("MaritalStatus").ToString.Trim

        rboTypeP.SelectedValue = oRow("PersonalCustomerType").ToString.Trim
        cboPProfession.SelectedIndex = cboPProfession.Items.IndexOf(cboPProfession.Items.FindByValue(oRow("ProfessionID").ToString.Trim))
        cboJobType.SelectedIndex = cboJobType.Items.IndexOf(cboJobType.Items.FindByValue(oRow("JobTypeID").ToString.Trim))
        cboJobPosition.SelectedIndex = cboJobPosition.Items.IndexOf(cboJobPosition.Items.FindByValue(oRow("JobPos").ToString.Trim))
        txtCompanyName.Text = oRow("CompanyName").ToString.Replace("-", "").Trim
        cboIndrustriHeader.SelectedIndex = cboIndrustriHeader.Items.IndexOf(cboIndrustriHeader.Items.FindByValue(oRow("IndustryHeaderId").ToString.Trim))
        hdfKodeIndustriBU.Value = oRow("IndustryTypeID").ToString.Trim
        txtNamaIndustriBU.Text = oRow("NameIndustri").ToString.Trim
        UcJDAddress.Address = oRow("CompanyAddress").ToString.Replace("-", "").Trim
        UcJDAddress.RT = oRow("CompanyRT").ToString.Replace("-", "").Trim
        UcJDAddress.RW = oRow("CompanyRW").ToString.Replace("-", "").Trim
        UcJDAddress.Kelurahan = oRow("CompanyKelurahan").ToString.Replace("-", "").Trim
        UcJDAddress.Kecamatan = oRow("CompanyKecamatan").ToString.Replace("-", "").Trim
        UcJDAddress.City = oRow("CompanyCity").ToString.Replace("-", "").Trim
        UcJDAddress.ZipCode = oRow("CompanyZipCode").ToString.Trim
        UcJDAddress.AreaPhone1 = oRow("CompanyAreaPhone1").ToString.Replace("-", "").Trim
        UcJDAddress.Phone1 = oRow("CompanyPhone1").ToString.Replace("-", "").Trim
        UcJDAddress.AreaPhone2 = oRow("CompanyAreaPhone2").ToString.Replace("-", "").Trim
        UcJDAddress.Phone2 = oRow("CompanyPhone2").ToString.Replace("-", "").Trim
        UcJDAddress.AreaFax = oRow("CompanyAreaFax").ToString.Replace("-", "").Trim
        UcJDAddress.Fax = oRow("CompanyFax").ToString.Replace("-", "").Trim
        UcJDAddress.BindAddress()
        txtJobTitle.Text = oRow("CompanyJobTitle").ToString.Trim
        txtEmploymentYear.Text = oRow("EmploymentSinceYear").ToString.Trim
        txtOBName.Text = oRow("OtherBusinessName").ToString.Replace("-", "").Trim
        ucKategoriPerusahaanCustomer.SelectedKategori = oRow("EXKategoryPers").ToString.Trim
        ucKondisiKantorCustomer.SelectedKondisi = oRow("EXKondisiKantor").ToString.Trim
        ucKaryawanSejakCustomer.SelectedMonth = CInt(oRow("EXKaryawanSejakBulan"))
        txtTotalLamaKerja.Text = oRow("TotalLamaKerja").ToString.Trim


        txtOperasiSejak.Text = oRow("JobsKantorOperasiSejak").ToString.Trim
        txtPNPWP.Text = oRow("JobsKantorNPWP").ToString.Trim


        cboOccupation.SelectedIndex = cboOccupation.Items.IndexOf(cboOccupation.Items.FindByValue(oRow("OccupationID").ToString.Trim))
        cboNaturalOfBusiness.SelectedIndex = cboNaturalOfBusiness.Items.IndexOf(cboNaturalOfBusiness.Items.FindByValue(oRow("NatureOfBusinessID").ToString.Trim))
        cboIndustryRisk.SelectedIndex = cboIndustryRisk.Items.IndexOf(cboIndustryRisk.Items.FindByValue(oRow("IndustryRisk").ToString.Trim))
        rboIsRelasiBNI.Checked = oRow("IsRelatedBNI").ToString.Trim
        cboIsEmployee.Checked = oRow("IsEmployee").ToString.Trim

    End Sub

#End Region
    Sub txtEmploymentYear_OnTextChanged()
      

        Dim blnInput As Integer = ucKaryawanSejakCustomer.SelectedMonth
        Dim dob As DateTime

        If (txtEmploymentYear.Text.Trim = "") Then
            Return
        End If

        If (CInt(txtEmploymentYear.Text) < 1900) Then
            Return
        End If


        dob = New DateTime(CInt(txtEmploymentYear.Text), blnInput, 1)
        Dim tday As TimeSpan = DateTime.Now.Subtract(dob)
        Dim years As Integer, months As Integer, days As Integer
        months = 12 * (DateTime.Now.Year - dob.Year) + (DateTime.Now.Month - dob.Month)

        If DateTime.Now.Day < dob.Day Then
            months -= 1
            days = DateTime.DaysInMonth(dob.Year, dob.Month) - dob.Day + DateTime.Now.Day
        Else
            days = DateTime.Now.Day - dob.Day
        End If
        years = CInt(Math.Floor(months / 12))
        months -= years * 12
        txtTotalLamaKerja.Text = CStr(CInt(IIf(months > 7, years + 1, years)))


    End Sub
    Public Sub ucKaryawanSejakCustomer_SelectedIndexChanged() Handles ucKaryawanSejakCustomer.SelectedIndexChanged
        Dim thnSekarang As Integer = Date.Now.Year
        Dim blnSekarang As Integer = Date.Now.Month
        Dim blnInput As Integer = ucKaryawanSejakCustomer.SelectedMonth
        Dim thnInput As Integer = CInt(IIf(txtEmploymentYear.Text.Trim = "", "0", txtEmploymentYear.Text.Trim))
        Dim result As Integer = 0

        result = thnSekarang - thnInput


        If blnSekarang <> blnInput Then
            result = result - 1
        End If

        txtTotalLamaKerja.Text = Math.Abs(result).ToString
    End Sub
    'Private Sub InitiateUCnumberFormat(ByVal uc As ucNumberFormat, ByVal rfv As Boolean, ByVal rv As Boolean)
    '    With uc
    '        .RequiredFieldValidatorEnable = rfv
    '        .RangeValidatorEnable = rv
    '    End With
    'End Sub
End Class