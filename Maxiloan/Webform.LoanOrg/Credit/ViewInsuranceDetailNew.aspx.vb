﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region

Public Class ViewInsuranceDetailNew
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"

    Private m_controller As New ApplicationController
    Protected WithEvents UcInsuranceData As UcInsuranceData
    Protected WithEvents UcInsuranceDataAgri As UcInsuranceDataAgriculture
    Dim pagesource As String
    Dim back As String

#End Region

#Region "Property"

    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property AgreementNo() As String
        Get
            Return ViewState("AgreementNo").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property


    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property


    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property

    Property Style() As String
        Get
            Return ViewState("Style").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Style") = Value
        End Set
    End Property




#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.BranchID = Request.QueryString("BranchID")
        Me.ApplicationID = Request("ApplicationID").ToString
        Me.Style = Request("Style").ToString
        Me.AgreementNo = Request("AgreementNo").ToString
        Me.CustomerName = Request("CustomerName").ToString

        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        'oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication = m_controller.GetViewApplication(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            If oRow("ApplicationModule").ToString.Trim = "AGRI" Then
                InsuranceDataAgri.Visible = True
                InsuranceData.Visible = False
                UcInsuranceDataAgri.ApplicationID = Me.ApplicationID
                UcInsuranceDataAgri.BranchID = oRow("BranchID").ToString.Trim
                UcInsuranceDataAgri.BindData()
            Else
                InsuranceDataAgri.Visible = False
                InsuranceData.Visible = True
                UcInsuranceData.ApplicationID = Me.ApplicationID
                UcInsuranceData.BranchID = oRow("BranchID").ToString.Trim
                UcInsuranceData.BindData()
            End If
        Else
            ShowMessage(lblMessage, "Aplikasi tidak dapat ditemukan", True)
        End If
    End Sub

End Class