﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class GesekanRangkaMesinViewer
    Inherits Maxiloan.Webform.WebBased


#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_coll As New PPKController
    Private oCustomClass As New Parameter.PPK
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
        'CreateData()
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim m_controller As New PPKController
        Dim oTandaTerimaDokumen As New Parameter.PPK

        Dim objReport As GesekanRangkaMesinPrint = New GesekanRangkaMesinPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        oTandaTerimaDokumen.strConnection = GetConnectionString
        oTandaTerimaDokumen.WhereCond = Me.CmdWhere
        oTandaTerimaDokumen = m_controller.ListReportGesekanRangkaMesin(oTandaTerimaDokumen)
        oData = oTandaTerimaDokumen.ListTandaTerimaDokumenReport

        objReport.SetDataSource(oData)
        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "GesekanRangkaMesin.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("GesekanRangkaMesin.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "GesekanRangkaMesin")

    End Sub
#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("GesekanRangkaMesin")
        Me.CmdWhere = cookie.Values("cmdwhere")
    End Sub
#End Region

End Class