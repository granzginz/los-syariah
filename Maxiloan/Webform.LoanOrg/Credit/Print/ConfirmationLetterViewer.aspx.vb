﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class ConfirmationLetterViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property BDate() As String
        Get
            Return CType(viewstate("BDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BDate") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_coll As New PPKController
    Private oCustomClass As New Parameter.PPK
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
        'CreateData()
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim m_controller As New PPKController
        Dim oConfLetter As New Parameter.PPK

        Dim objReport As ConfirmationLetterPrint = New ConfirmationLetterPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        oConfLetter.strConnection = GetConnectionString
        oConfLetter.WhereCond = Me.CmdWhere
        oConfLetter.BranchId = Me.sesBranchId.Replace("'", "")
        oConfLetter.LoginId = Me.Loginid
        oConfLetter = m_controller.ListReportConfLetter(oConfLetter)
        oData = oConfLetter.ListReport

        objReport.SetDataSource(oConfLetter.ListReport)
        'Wira 2015-07-02
        AddParamField(objReport, "CompanyName", GetCompanyName())

        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_ConfLetter.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("ConfirmationLetter.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_ConfLetter")

    End Sub

    Function GetCompanyName() As String
        Dim result As String

        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If

        Return result
    End Function

    Private Sub AddParamField(ByVal objReport As ConfirmationLetterPrint, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
#End Region

#Region "CreateData"
    Private Sub CreateData()
        Dim m_controller As New PPKController
        Dim oData As New DataSet
        Dim oConfLetter As New Parameter.PPK
        oConfLetter.strConnection = GetConnectionString
        oConfLetter.WhereCond = Me.CmdWhere
        oConfLetter = m_controller.ListReportConfLetter(oConfLetter)
        oData = oConfLetter.ListReport
        oData.WriteXmlSchema("D:\project\Maxiloan\AccAcq\Credit\Print\xmd_ConfLetter.xmd")
    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptConfLetter")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.BDate = cookie.Values("BusinessDate")
    End Sub
#End Region

End Class