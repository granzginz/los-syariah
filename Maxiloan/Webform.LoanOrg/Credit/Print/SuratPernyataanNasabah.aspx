﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuratPernyataanNasabah.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.SuratPernyataanNasabah" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>SuratPenolakan</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK SURAT PERNYATAAN NASABAH</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="Agreement.ApplicationID">No Aplikasi</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Aplikasi</label>
                <asp:TextBox runat="server" ID="txtPeriod1"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtPeriod1"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                &nbsp; S/D
                <asp:TextBox runat="server" ID="TxtPeriod2"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtPeriod2"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR APLIKASI KREDIT
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPPK" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="ApplicationID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NO APLIKASI">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:HyperLink>
                                    <asp:Label Visible="False" ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:Label>
                                    <asp:Label Visible="False" ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblCustomerName" runat="server" Text='<%#Container.dataitem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerID" runat="server" Visible="False" Text='<%#Container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="DATEENTRYAPPLICATIONDATA" SortExpression="APPLICATIONDATE"
                                HeaderText="TGL APLIKASI" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>