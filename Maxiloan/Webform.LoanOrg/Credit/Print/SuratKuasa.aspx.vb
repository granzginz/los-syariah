﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class SuratKuasa
    Inherits Maxiloan.Webform.WebBased
    Private oCustomClass As New Parameter.PPK
    Private oController As New PPKController
    Protected WithEvents oAgreementDate As ValidDate

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If sessioninvalid() Then
                Exit Sub
            End If
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=600, height=800, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "PrintSuratKuasa"

            oAgreementDate.FillRequired = False
            oAgreementDate.dateValue = ""
            oAgreementDate.ValidationErrMessage = "Harap isi Tanggal Kontrak dengan format dd/MM/yyyy"
            oAgreementDate.DataBind()


            If checkform(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    pnlsearch.Visible = True
                    pnlDtGrid.Visible = True
                    Me.SearchBy = " MailTransaction.MailTypeID='SK' and MailTransaction.MailPrintedNum = 0 and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
                    Me.SortBy = "MAILTransNo ASC"
                    DoBind(Me.SearchBy, Me.SortBy)
                Else
                    Dim strHTTPServer As String
                    Dim strHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub
#End Region
#Region "Do Bind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListAgreement(oCustomClass)

        DtUserList = oCustomClass.ListPPK
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgPPK.DataSource = DvUserList

        Try
            dtgPPK.DataBind()
        Catch
            dtgPPK.CurrentPageIndex = 0
            dtgPPK.DataBind()
        End Try
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        lblMessage.Text = ""
    End Sub
#End Region
#Region "Print"
    Private Sub imbPrint_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbPrint.Click
        If checkfeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer
            Dim chkPrint As CheckBox
            Dim lblAgreementNo As HyperLink
            Dim lblApplicationID As New Label
            Dim hasil As Integer
            Dim cmdwhere As String
            With oDataTable
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            End With
            For intloop = 0 To dtgPPK.Items.Count - 1
                chkPrint = CType(dtgPPK.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo = CType(dtgPPK.Items(intloop).Cells(1).FindControl("hyAgreementNo"), HyperLink)
                lblApplicationID = CType(dtgPPK.Items(intloop).FindControl("lblApplicationID"), Label)
                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("AgreementNo") = CType(lblAgreementNo.Text.Trim, String)
                    oRow("ApplicationID") = lblApplicationID.Text.Trim
                    oDataTable.Rows.Add(oRow)
                    If cmdwhere = "" Then
                        cmdwhere = "SK.ApplicationID='" & lblApplicationID.Text.Trim & "'"
                    Else
                        cmdwhere = cmdwhere & " or SK.ApplicationID='" & lblApplicationID.Text.Trim & "' "
                    End If
                End If
            Next
            If oDataTable.Rows.Count = 0 Then
                lblMessage.Text = "Harap Periksa Item"
                Exit Sub
            End If
            With oCustomClass
                .strConnection = GetConnectionString
                .ListPPK = oDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With
            oCustomClass = oController.SavePrintSuratKuasa(oCustomClass)
            hasil = oCustomClass.hasil
            If hasil = 0 Then
                lblMessage.Text = "Cetak gagal"
                Exit Sub
            Else
                cmdwhere = "(" & cmdwhere & ") and SK.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
                Dim cookie As HttpCookie = Request.Cookies("RptSuratKuasa")
                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = cmdwhere
                    cookie.Values("sortby") = Me.SortBy.Replace("MAILTransNo", "AgreementNo")
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("RptSuratKuasa")
                    cookieNew.Values.Add("cmdwhere", cmdwhere)
                    cookieNew.Values.Add("sortby", Me.SortBy.Replace("MAILTransNo", "AgreementNo"))
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("SuratKuasaViewer.aspx")
            End If
        End If
    End Sub
#End Region
#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        If Me.SortBy.Trim = "" Then
            Me.SortBy = "MAILTransNo ASC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Search"
    Private Sub imgsearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgsearch.Click
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.SearchBy = "MailTransaction.MailPrintedNum=0"
        Else
            Me.SearchBy = "MailTransaction.MailPrintedNum > 0"
        End If
        Me.SearchBy = Me.SearchBy & "  and MailTransaction.MailTypeID='SK' and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like'%" & txtSearchBy.Text.Trim & "%'"
        End If

        If oAgreementDate.dateValue.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and Agreement.AgreementDate='" & ConvertDate2(oAgreementDate.dateValue.Trim) & "'"
        End If
        Me.SortBy = "MAILTransNo ASC"
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Reset"
    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbReset.Click
        Me.SearchBy = " MailTransaction.MailTypeID='SK' and MailTransaction.MailPrintedNum = 0 and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
        Me.SortBy = "MAILTransNo ASC"
        DoBind(Me.SearchBy, Me.SortBy)
        txtSearchBy.Text = ""
        cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub
#End Region
#Region "dtgPPK_ItemDataBound"
    Private Sub dtgPPK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPPK.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblApplicationID As New Label
            Dim hyTemp As HyperLink

            '*** Agreement No link
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "')"
        End If
    End Sub
#End Region

End Class