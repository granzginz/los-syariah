﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
#End Region

Public Class PernyataanSetujuViewer
    Inherits Maxiloan.Webform.WebBased
    
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oController As New PernyataanSetujuController
    Private oParameter As New Parameter.PernyataanSetuju
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        bindReport()
        'createData()
    End Sub
#End Region

#Region "BindReport"
    Private Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oReport As PernyataanSetujuPrint = New PernyataanSetujuPrint

        With oParameter
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .MultiApplicationID = Me.MultiApplicationID
            .SortBy = Me.SortBy
        End With

        oParameter = oController.listPernyataanSetuju(oParameter)
        oDataSet = oParameter.listPernyataanSetuju

        oReport.SetDataSource(oDataSet)

        'Wira 2015-07-02
        AddParamField(oReport, "CompanyName", GetCompanyName())

        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "PernyataanSetuju.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("PernyataanSetuju.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "PernyataanSetuju")
    End Sub

    Function GetCompanyName() As String
        Dim result As String

        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If

        Return result
    End Function

    Private Sub AddParamField(ByVal oReport As PernyataanSetujuPrint, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = oReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
#End Region

#Region "CreateData"
    Private Sub createData()
        Dim oController As New PernyataanSetujuController
        Dim oDataSet As New DataSet
        Dim oParameter As New Parameter.PernyataanSetuju

        oParameter.strConnection = GetConnectionString()
        oParameter = oController.listPernyataanSetuju(oParameter)
        oDataSet = oParameter.listPernyataanSetuju
        oDataSet.WriteXmlSchema("c:\maxiloan.reportdef\listPernyataanSetuju.xml")
    End Sub
#End Region

#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("PernyataanSetujuCookie")

        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.MultiApplicationID = cookie.Values("ApplicationID")
        Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region

End Class