﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class SPTransfer1
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtPeriod1 As ucDateCE
    Protected WithEvents txtPeriod2 As ucDateCE

#Region "Constanta"
    Private oCustomClass As New Parameter.PPK
    Private oController As New PPKController
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If Request.QueryString("strFileLocation") <> "" Then

                Dim strFileLocation As String

                strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "spTransfer"
            Me.CmdWhere = " Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
            Me.CmdWhere = Me.CmdWhere & " and  DateEntryApplicationData >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and DateEntryApplicationData<= '" & ConvertDate2(txtPeriod2.Text.Trim) & "'"

            Me.SortBy = "ApplicationID ASC"
            DoBind(Me.CmdWhere, Me.SortBy)
        End If
    End Sub
#End Region

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        If cmdWhere.Trim = "" Then
            cmdWhere = ""
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListSuratPermohonanTransfer(oCustomClass)

        DtUserList = oCustomClass.ListTandaTerimaDokumen
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgPPK.DataSource = DvUserList

        Try
            dtgPPK.DataBind()
        Catch
            dtgPPK.CurrentPageIndex = 0
            dtgPPK.DataBind()
        End Try
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        lblMessage.Text = ""
    End Sub
#End Region

#Region "LinkToCustomer"
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function
#End Region

#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer
            Dim chkPrint As CheckBox
            Dim lblApplicationID As HyperLink
            Dim cmdwhere As String = ""
            With oDataTable
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
                '.Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            End With
            For intloop = 0 To dtgPPK.Items.Count - 1
                chkPrint = CType(dtgPPK.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblApplicationID = CType(dtgPPK.Items(intloop).Cells(1).FindControl("hyApplicationID"), HyperLink)
                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("ApplicationID") = CType(lblApplicationID.Text.Trim, String)
                    oRow("ApplicationID") = CType(dtgPPK.Items(intloop).FindControl("lblApplicationID"), Label).Text.Trim
                    oDataTable.Rows.Add(oRow)
                    If cmdwhere.Trim = "" Then
                        cmdwhere = "'" & CType(oRow("ApplicationID"), String) & "'"
                    Else
                        cmdwhere = cmdwhere & ",'" & CType(oRow("ApplicationID"), String) & "'"
                    End If
                End If
            Next
            cmdwhere = "Agreement.ApplicationID in (" & cmdwhere & ")"
            cmdwhere = cmdwhere & " and Agreement.BranchID ='" & Me.sesBranchId.Replace("'", "").Trim & "'"
            If oDataTable.Rows.Count = 0 Then
                lblMessage.Text = "Harap Periksa Item"
                Exit Sub
            End If

            Dim cookie As HttpCookie = Request.Cookies("Sptransfer")
            If Not cookie Is Nothing Then
                cookie.Values("cmdwhere") = cmdwhere
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("Sptransfer")
                cookieNew.Values.Add("cmdwhere", cmdwhere)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("SptransferViewer.aspx")
        End If
    End Sub
#End Region

#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim SearchTemp As String = ""
        If txtPeriod1.Text.Trim <> "" And txtPeriod2.Text.Trim <> "" Then
            Me.CmdWhere = " Agreement.BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'" & " and DateEntryApplicationData >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and DateEntryApplicationData<= '" & ConvertDate2(txtPeriod2.Text.Trim) & "'"

            If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
                SearchTemp = cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
                Me.CmdWhere = Me.CmdWhere & "  and " & SearchTemp
            End If

        End If
        Me.SortBy = "ApplicationID ASC"
        DoBind(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region

#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        Me.CmdWhere = " Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        Me.CmdWhere = Me.CmdWhere & " and  DateEntryApplicationData >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and DateEntryApplicationData<= '" & ConvertDate2(txtPeriod2.Text.Trim) & "'"

        Me.SortBy = "ApplicationID ASC"
        DoBind(Me.CmdWhere, Me.SortBy)
        txtSearchBy.Text = ""
        cboSearchBy.ClearSelection()
    End Sub
#End Region

#Region "dtgPPK_ItemDataBound"
    Private Sub dtgPPK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPPK.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblCustomerName As New HyperLink
            Dim lblCustomerID As New Label
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyApplicationID"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
#End Region

End Class