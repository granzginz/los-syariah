﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper

Public Class FiduciaViewer
    Inherits Maxiloan.Webform.WebBased

    Private oController As New PrintController
    Private oParameter As New Parameter.SuratKuasaFiducia

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property CmdWhereCompany() As String
        Get
            Return CType(ViewState("CmdWhereCompany"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhereCompany") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(viewstate("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MultiApplicationID") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        bindReport()
    End Sub

    Private Sub bindReport()
        GetCookies()

        Dim oReport As FiduciaPrint = New FiduciaPrint
        'Dim oReportCompany As FiduciaCompanyPrint = New FiduciaCompanyPrint
        Dim oDataSet As New DataSet
        Dim oDataSetCompany As New DataSet
        Dim strFilePribadi As String = ""
        Dim strFileCompany As String = ""

        If Me.CmdWhere.Trim <> "" Then
            With oParameter
                .strConnection = GetConnectionString()
                .WhereCond = Me.CmdWhere
                .MultiApplicationID = ""
                .SortBy = Me.SortBy
                .SPType = "P"
            End With

            oParameter = oController.getSuratKuasaFiducia(oParameter)
            oDataSet = oParameter.Dataset

            oReport.SetDataSource(oDataSet)
            CrystalReportViewer1.ReportSource = oReport
            CrystalReportViewer1.DataBind()

            With oReport.ExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
            End With

            Dim doctoprint As New System.Drawing.Printing.PrintDocument
            Dim i As Integer
            doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

            For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
                Dim rawKind As Integer
                If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Fiducia" Then
                    rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                    oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                    Exit For
                End If
            Next

            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "SuratKuasaFiduciaPribadi.pdf"
            strFilePribadi = System.Web.HttpContext.Current.Session.SessionID + Me.Loginid + "SuratKuasaFiduciaPribadi.pdf"

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
            New CrystalDecisions.Shared.DiskFileDestinationOptions

            DiskOpts.DiskFileName = strFileLocation

            With oReport
                .ExportOptions.DestinationOptions = DiskOpts
                .Export()
                .Close()
                .Dispose()
            End With
        End If

        If Me.CmdWhereCompany.Trim <> "" Then
            With oParameter
                .strConnection = GetConnectionString()
                .WhereCond = Me.CmdWhereCompany
                .MultiApplicationID = ""
                .SortBy = Me.SortBy
                .SPType = "C"
            End With

            oParameter = oController.getSuratKuasaFiducia(oParameter)
            oDataSetCompany = oParameter.Dataset

            'oReportCompany.SetDataSource(oDataSetCompany)
            oReport.SetDataSource(oDataSetCompany)
            'CrystalReportViewer1.ReportSource = oReportCompany
            CrystalReportViewer1.ReportSource = oReport
            CrystalReportViewer1.DataBind()

            'With oReportCompany.ExportOptions
            With oReport.ExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
            End With

            Dim doctoprint As New System.Drawing.Printing.PrintDocument
            Dim i As Integer
            doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

            For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
                Dim rawKind As Integer
                If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "Fiducia" Then
                    rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                    'oReportCompany.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                    oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                    Exit For
                End If
            Next

            Dim strFileLocation As String

            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "SuratKuasaFiduciaCompany.pdf"
            strFileCompany = System.Web.HttpContext.Current.Session.SessionID + Me.Loginid + "SuratKuasaFiduciaCompany.pdf"

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
            New CrystalDecisions.Shared.DiskFileDestinationOptions

            DiskOpts.DiskFileName = strFileLocation

            'With oReportCompany
            With oReport
                .ExportOptions.DestinationOptions = DiskOpts
                .Export()
                .Close()
                .Dispose()
            End With
        End If

        Response.Redirect("Fiducia.aspx?strFileLocation=" & strFilePribadi & "&strFileLocation1=" & strFileCompany & "")
    End Sub

    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_FIDUCIA)

        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.CmdWhereCompany = cookie.Values("cmdwhereCompany")
        Me.MultiApplicationID = cookie.Values("ApplicationID")
        Me.SortBy = cookie.Values("sortby")
    End Sub
End Class