﻿
#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class KwitansiViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiAgreementNo() As String
        Get
            Return CType(viewstate("MultiAgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MultiAgreementNo") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(viewstate("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("MultiApplicationID") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return CType(viewstate("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Branch_ID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_coll As New PPKController
    Private oCustomClass As New Parameter.PPK
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
    End Sub

#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim objReport As KwitansiPrint = New KwitansiPrint        

        oCustomClass.WhereCond = Me.CmdWhere
        oCustomClass.MultiAgreementNo = Me.MultiAgreementNo
        oCustomClass.MultiApplicationID = Me.MultiApplicationID
        oCustomClass.Branch_ID = Me.Branch_ID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass = m_coll.ListPrintKwitansi(oCustomClass)
        If Not oCustomClass Is Nothing Then
            oData = oCustomClass.ListAgreement
        End If
        objReport.SetDataSource(oData)

        AddParamField(objReport, "CompanyName", GetCompanyName())

        CrystalReportViewer1.ReportSource = objReport

        '========================================================
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_Kwitansi.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("Kwitansi.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_Kwitansi")

    End Sub
    Function GetCompanyName() As String
        Dim result As String

        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If

        Return result
    End Function
    Private Sub AddParamField(ByVal objReport As KwitansiPrint, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = objReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptKwitansi")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.MultiAgreementNo = cookie.Values("MultiAgreementNo")
        Me.MultiApplicationID = cookie.Values("MultiApplicationID")
        Me.Branch_ID = cookie.Values("BranchID")
    End Sub
#End Region

End Class