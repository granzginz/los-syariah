﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports System.Linq
#End Region

Public Class PPK
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtAgreementDate As ucDateCE
    Protected WithEvents txtAgreementDate1 As ucDateCE
    'Protected WithEvents txtTanggalKontrak As ucDateCE


#Region "Constanta"
    Private oCustomClass As New Parameter.PPK
    Private oController As New PPKController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 20
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property    
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property    
    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property StatusNext() As Boolean
        Get
            Return CType(ViewState("StatusNext"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            ViewState("StatusNext") = Value
        End Set
    End Property
    Private Property TglJanjiTtdKontrak() As Date
        Get
            Return CType(ViewState("TglJanjiTtdKontrak"), Date)
        End Get
        Set(ByVal Value As Date)
            ViewState("TglJanjiTtdKontrak") = Value
        End Set
    End Property
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()        
        If Not IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If            
            
            If Request.QueryString("strFileLocation") <> "" Or Request.QueryString("strFileLocation1") <> "" Then

                Dim strFileLocation As String
                strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation")

                Dim strFileLocation1 As String
                strFileLocation1 = "../../../XML/" & Request.QueryString("strFileLocation1")


                If Request.QueryString("strFileLocation") <> "" And Request.QueryString("strFileLocation1") <> "" Then
                    Response.Write("<script language = javascript>" & vbCrLf _
                           & "window.open('" & strFileLocation & "')" & vbCrLf _
                           & "window.open('" & strFileLocation1 & "')" & vbCrLf _
                           & "</script>")
                Else

                    If Request.QueryString("strFileLocation") <> "" Then
                        Response.Write("<script language = javascript>" & vbCrLf _
                           & "window.open('" & strFileLocation & "')" & vbCrLf _
                           & "</script>")
                    End If
                    If Request.QueryString("strFileLocation1") <> "" Then
                        Response.Write("<script language = javascript>" & vbCrLf _
                                   & "window.open('" & strFileLocation1 & "')" & vbCrLf _
                                   & "</script>")
                    End If

                End If



            End If

            Me.FormID = "PrintPPK"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    pnlsearch.Visible = True
                    pnlDtGrid.Visible = False
                Else
                    Dim strHTTPServer As String
                    Dim strHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub
#End Region
#Region "DoBind"
    Sub DoBind()
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView        


        If txtAgreementDate.Text = Nothing Or txtAgreementDate.Text = "" Then
            BtnCancel.Visible = False
            BtnNext.Visible = True
            BtnPrint.Visible = False
        Else
            BtnCancel.Visible = False
            BtnNext.Visible = False
            BtnPrint.Visible = True
        End If


        If StatusNext Then
            BtnCancel.Visible = False
            BtnNext.Visible = False
            BtnPrint.Visible = True
        End If


        Dim DataList As New List(Of Parameter.Application)
        Dim custom As New Parameter.Application
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.Application

                custom.ApplicationID = TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                custom.AgreementNo = TempDataTable.Rows(index).Item("AgreementNo").ToString.Trim
                custom.CustomerId = TempDataTable.Rows(index).Item("CustomerID").ToString.Trim

                DataList.Add(custom)
            Next
        End If


        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListAgreement(oCustomClass)

        DtUserList = oCustomClass.ListPPK
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy


        If Not DvUserList Is Nothing Then
            dtgPPK.DataSource = DvUserList
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        Try
            dtgPPK.DataBind()
        Catch
            dtgPPK.CurrentPageIndex = 0
            dtgPPK.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True


        For intLoopGrid = 0 To dtgPPK.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgPPK.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim AgreementNo As String = CType(dtgPPK.Items(intLoopGrid).FindControl("hyAgreementNo"), HyperLink).Text.Trim
            Dim ApplicationID As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblApplicationID"), Label).Text.Trim
            Dim CustomerID As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblCustomerID"), Label).Text.Trim


            Me.ApplicationID = ApplicationID
            Me.AgreementNo = AgreementNo
            Me.CustomerID = CustomerID

            Dim query As New Parameter.Application
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next
    End Sub
#End Region
    Public Function BindValidatorDate() As Boolean
        Dim generalSet As New Parameter.GeneralSetting
        Dim conGeneral As New GeneralSettingController
        Dim value As Integer = 0

        lblMessage.Visible = False

        generalSet.strConnection = GetConnectionString()
        generalSet.GSID = "CUTOFFSPBIAYA"

        generalSet = conGeneral.GetGeneralSettingByID(generalSet)

        If generalSet.ListData.Rows.Count > 0 Then
            value = CInt(generalSet.ListData.Rows(0).Item("GSValue").ToString.Trim)
        End If


        If txtAgreementDate.Text <> "" Then
            Dim dateTxt As Date = ConvertDate2(txtAgreementDate.Text.Trim)
            Dim dateMin As Date = CDate(Me.BusinessDate).AddMonths(-value)
            Dim dateBus As Date = CDate(Me.BusinessDate).AddMonths(1)


            If Not (dateTxt >= dateMin) Or Not (dateBus >= dateTxt) Then
                ShowMessage(lblMessage, "Pilih tanggal diantara " & dateMin.ToString("dd/MM/yyyy") & " dan " & BusinessDate.ToString("dd/MM/yyyy"), True)
                Return False
            Else
                Return True
            End If
        Else            
            Return True
        End If
    End Function
    Public Function PredicateFunction(ByVal custom As Parameter.Application) As Boolean
        Return custom.ApplicationID = Me.ApplicationID And custom.CustomerId = Me.CustomerID And custom.AgreementNo = Me.AgreementNo
    End Function
    Public Function PredicateFunctionTgl(ByVal custom As Date) As Boolean
        Return custom = Me.TglJanjiTtdKontrak
    End Function
    Sub BindDataPPK()
        Dim DataList As New List(Of Parameter.Application)
        Dim Application As New Parameter.Application

        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("CustomerID", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                .Columns.Add(New DataColumn("TglKontrak", GetType(String)))
                .Columns.Add(New DataColumn("CustomerType", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                Application = New Parameter.Application

                Application.CustomerId = TempDataTable.Rows(index).Item("CustomerID").ToString.Trim
                Application.ApplicationID = TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                Application.AgreementNo = TempDataTable.Rows(index).Item("AgreementNo").ToString.Trim

                DataList.Add(Application)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To dtgPPK.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgPPK.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim AgreementNo As String = CType(dtgPPK.Items(intLoopGrid).FindControl("hyAgreementNo"), HyperLink).Text.Trim
            Dim ApplicationID As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblApplicationID"), Label).Text.Trim
            Dim CustomerID As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblCustomerID"), Label).Text.Trim
            Dim TglKontrak As String = dtgPPK.Items(intLoopGrid).Cells(3).Text.Trim
            Dim CustomerType As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblCustomerType"), Label).Text.Trim

            Me.ApplicationID = ApplicationID
            Me.AgreementNo = AgreementNo
            Me.CustomerID = CustomerID

            Dim query As New Parameter.Application
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("CustomerID") = CustomerID
                oRow("ApplicationID") = ApplicationID
                oRow("AgreementNo") = AgreementNo
                oRow("TglKontrak") = TglKontrak
                oRow("CustomerType") = CustomerType
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClearPPK()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("CustomerID", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
            .Columns.Add(New DataColumn("TglKontrak", GetType(String)))
            .Columns.Add(New DataColumn("CustomerType", GetType(String)))
        End With
    End Sub
#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then

            If TempDataTable Is Nothing Then
                BindDataPPK()
            Else
                If TempDataTable.Rows.Count = 0 Then
                    BindDataPPK()
                End If
            End If

            Dim DataList As New List(Of Date)
            'Dim Application As New Parameter.Application
            Dim strMultiApplicationID As String = ""
            Dim strMultiApplicationIDCompany As String = ""
            Dim strMultiBranch As String = ""
            Dim CmdWherePribadi As String = ""
            Dim CmdWhereCompany As String = ""

            If TempDataTable.Rows.Count = 0 Then
                ShowMessage(lblMessage, "Harap periksa item", True)
                Exit Sub
            End If

            For index = 0 To TempDataTable.Rows.Count - 1
                'Application = New Parameter.Application

                If TempDataTable.Rows(index).Item("CustomerType").ToString.ToUpper.Trim = "C" Then
                    If strMultiApplicationIDCompany.Trim = "" Then
                        strMultiApplicationIDCompany = strMultiApplicationIDCompany + "'" + TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim + "'"
                    Else
                        strMultiApplicationIDCompany = strMultiApplicationIDCompany + ",'" + TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim + "'"
                    End If
                End If
                If TempDataTable.Rows(index).Item("CustomerType").ToString.ToUpper.Trim = "P" Then
                    If strMultiApplicationID.Trim = "" Then
                        strMultiApplicationID = strMultiApplicationID + "'" + TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim + "'"
                    Else
                        strMultiApplicationID = strMultiApplicationID + ",'" + TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim + "'"
                    End If
                End If

                If IsDate(TempDataTable.Rows(index).Item("TglKontrak").ToString) Then
                    Me.TglJanjiTtdKontrak = ConvertDate2(TempDataTable.Rows(index).Item("TglKontrak").ToString)

                    Dim query As Date
                    If DataList.Count > 0 Then
                        query = DataList.Find(AddressOf PredicateFunctionTgl)
                    Else
                        DataList.Add(Me.TglJanjiTtdKontrak)
                    End If

                    If IsNothing(query) Then
                        DataList.Add(Me.TglJanjiTtdKontrak)
                    End If
                End If

            Next

            If DataList.Count > 1 Then
                ShowMessage(lblMessage, "Tgl Kontrak tidak boleh berbeda!", True)
                Exit Sub
            End If



            With oCustomClass
                .strConnection = GetConnectionString()
                .ListPPK = TempDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                strMultiBranch = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid

                If txtTanggalKontrak.Text = Nothing Or txtTanggalKontrak.Text = "" Then
                    .TanggalKontrak = Date.ParseExact("01/01/1900", "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                Else
                    .TanggalKontrak = Date.ParseExact(txtTanggalKontrak.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                End If


            End With

            oCustomClass = oController.SavePrint(oCustomClass)

            If oCustomClass.hasil = 0 Then
                ShowMessage(lblMessage, "Cetak Gagal", True)
                Exit Sub
            Else
                If strMultiApplicationIDCompany.Trim <> "" Then
                    CmdWhereCompany = "ApplicationID in(" + strMultiApplicationIDCompany + ") and BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
                End If
                If strMultiApplicationID.Trim <> "" Then
                    CmdWherePribadi = "ApplicationID in(" + strMultiApplicationID + ") and BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
                End If

                Dim cookie As HttpCookie = Request.Cookies("RptPPK")

                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = CmdWherePribadi
                    cookie.Values("cmdwhereCompany") = CmdWhereCompany
                    cookie.Values("ApplicationID") = strMultiApplicationID
                    cookie.Values("BranchID") = strMultiBranch
                    cookie.Values("sortby") = Me.SortBy.Replace("MAILTransNo", "AgreementNo")
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("RptPPK")
                    cookieNew.Values.Add("cmdwhere", CmdWherePribadi)
                    cookieNew.Values.Add("cmdwhereCompany", CmdWhereCompany)
                    cookieNew.Values.Add("ApplicationID", strMultiApplicationID)
                    cookieNew.Values.Add("BranchID", strMultiBranch)
                    cookieNew.Values.Add("sortby", Me.SortBy.Replace("MAILTransNo", "AgreementNo"))
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("PPKViewer.aspx")
            End If
        End If
    End Sub
#End Region
#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        If Me.SortBy.Trim = "" Then
            Me.SortBy = "MAILTransNo ASC"
        End If
        BindDataPPK()
        DoBind()
    End Sub
#End Region
#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click

        'If Not BindValidatorDate() Then Exit Sub

        If cboPrinted.SelectedItem.Value = "No" Then
            Me.CmdWhere = " MailTransaction.MailPrintedNum=0 "
            'Me.CmdWhere += " and (Agreement.AgreementDate IS NULL OR Agreement.AgreementDate = '1900-01-01') "
            'Me.CmdWhere += " and Agreement.AgreementDate ='" & ConvertDate2(txtAgreementDate.Text.Trim) & "' "
        Else
            Me.CmdWhere = " MailTransaction.MailPrintedNum > 0 "

            If txtAgreementDate.Text.Trim <> "" And txtAgreementDate1.Text.Trim <> "" Then
                Me.CmdWhere += " and Agreement.AgreementDate between '" & ConvertDate2(txtAgreementDate.Text.Trim) & "' and '" & ConvertDate2(txtAgreementDate1.Text.Trim) & "' "
            End If
        End If

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.CmdWhere += " and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If

        Me.CmdWhere += "  and MailTransaction.MailTypeID='PPK' and MailTransaction.BranchID=" & Me.sesBranchId & ""
        Me.SortBy = " MAILTransNo ASC"
        DoBind()

    End Sub
#End Region
#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.CmdWhere = " MailTransaction.MailTypeID='PPK' and MailTransaction.MailPrintedNum = 0 and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
        Me.SortBy = "MAILTransNo ASC"
        BindClearPPK()
        'DoBind()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlTanggalKontrak.Visible = False
        lblMessage.Visible = False
        BtnPrint.Visible = False
        BtnNext.Visible = False
        StatusNext = False
        txtSearchBy.Text = ""
        cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub
#End Region
#Region "dtgPPK_ItemDataBound"
    Private Sub dtgPPK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPPK.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindDataPPK()
        DoBind()
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindDataPPK()
                DoBind()
            End If
        End If
    End Sub
#End Region

    Protected Sub BtnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnNext.Click
        'Dim oConInst As New InstallRcvController
        'Dim oInstal As New Parameter.InstallRcv
        'oInstal.strConnection = GetConnectionString()
        'oInstal.SPName = "spGetBranchEmployee"
        'oInstal.WhereCond = "  EmployeeID = '" + Me.Loginid.Trim + "'  "

        'If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
        '    If oConInst.GetSP(oInstal).ListData.Rows(0).Item("EmployeePositionID").ToString.Trim = "BM" Then
        '        pnlTanggalKontrak.Visible = True
        '        txtTanggalKontrak.IsRequired = True
        '    Else
        '        pnlTanggalKontrak.Visible = False
        '        txtTanggalKontrak.IsRequired = False
        '    End If

        'Else
        '    pnlTanggalKontrak.Visible = False
        '    txtTanggalKontrak.IsRequired = False
        'End If

        pnlTanggalKontrak.Visible = True
        'txtTanggalKontrak.IsRequired = True
        txtTanggalKontrak.Text = "01/01/1900"
        txtTanggalKontrak.Enabled = True


        If TempDataTable Is Nothing Then
            BindDataPPK()
        Else
            If TempDataTable.Rows.Count = 0 Then
                BindDataPPK()
            End If
        End If

        If TempDataTable.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Harap periksa item", True)
            Exit Sub
        End If

        BtnNext.Visible = False
        BtnPrint.Visible = True
        BtnCancel.Visible = False
        StatusNext = True
        DoBind()
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles BtnCancel.Click
        Response.Redirect("PPK.aspx")
    End Sub
End Class