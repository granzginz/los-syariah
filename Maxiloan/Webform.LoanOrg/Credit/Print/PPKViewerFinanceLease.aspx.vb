﻿
#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class PPKViewerFinanceLease
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property CmdWhereCompany() As String
        Get
            Return CType(ViewState("CmdWhereCompany"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhereCompany") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
    Private Property Branch_ID() As String
        Get
            Return CType(ViewState("Branch_ID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Branch_ID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_coll As New PPKController
    Private oCustomClass As New Parameter.PPK
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub
#End Region

#Region "BindReport"
    Sub BindReport()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsPPK As New DataSet
        Dim dsPPKCompany As New DataSet
        Dim strFilePribadi As String = ""
        Dim strFileCompany As String = ""
        Dim oPPK As New PPKPrintFinanceLease
        Dim oPPKPenjamin As New PPKPrintFinanceLease
        'Dim oPPKCompany As New PPKCompanyPrint

        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1

        If Me.CmdWhereCompany.Trim <> "" Then
            oCustomClass = New Parameter.PPK
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.CmdWhereCompany
                .MultiApplicationID = "" 'tidak digunakan
                .BranchId = "" 'tidak digunakan
                .SortBy = Me.SortBy
                .Status = "C"
            End With

            oCustomClass = m_coll.ListPPKReportFL(oCustomClass)
            dsPPKCompany = oCustomClass.ListAgreement

            If dsPPKCompany.Tables(0).Rows.Count > 0 Then
                oPPK.SetDataSource(dsPPKCompany)

                CrystalReportViewer1.ReportSource = oPPK
                CrystalReportViewer1.Visible = True
                CrystalReportViewer1.DataBind()
            End If

            Dim DiskOptsCompany As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            Dim strFileLocationCompany As String
            strFileLocationCompany = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocationCompany += System.Web.HttpContext.Current.Session.SessionID + Me.Loginid + "rpt_PPKFL_Company.pdf"
            strFileCompany = System.Web.HttpContext.Current.Session.SessionID + Me.Loginid + "rpt_PPKFL_Company.pdf"

            oPPK.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            oPPK.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            DiskOptsCompany.DiskFileName = strFileLocationCompany

            oPPK.ExportOptions.DestinationOptions = DiskOptsCompany
            oPPK.Export()
            oPPK.Close()
            oPPK.Dispose()
        End If

        If Me.CmdWhere.Trim <> "" Then
            oCustomClass = New Parameter.PPK
            With oCustomClass
                .strConnection = GetConnectionString()
                .WhereCond = Me.CmdWhere
                .MultiApplicationID = "" 'tidak digunakan
                .BranchId = "" 'tidak digunakan
                .SortBy = Me.SortBy
                .Status = "P"
            End With

            oCustomClass = m_coll.ListPPKReportFL(oCustomClass)
            dsPPK = oCustomClass.ListAgreement

            'If dsPPK.Tables(0).Rows.Count > 0 Then
            '    If dsPPK.Tables(0).Rows(0).Item("guarantorname").ToString.Trim <> "" Then
            '        oPPKPenjamin.SetDataSource(dsPPK)

            '        CrystalReportViewer1.ReportSource = oPPKPenjamin
            '        CrystalReportViewer1.Visible = True
            '        CrystalReportViewer1.DataBind()
            '    Else
            '        oPPK.SetDataSource(dsPPK)

            '        CrystalReportViewer1.ReportSource = oPPK
            '        CrystalReportViewer1.Visible = True
            '        CrystalReportViewer1.DataBind()
            '    End If
            'Else
            oPPK.SetDataSource(dsPPK)

            CrystalReportViewer1.ReportSource = oPPK
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()
            'End If

            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions
            Dim strFileLocation As String
            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += System.Web.HttpContext.Current.Session.SessionID + Me.Loginid + "rpt_PPKFL.pdf"
            strFilePribadi = System.Web.HttpContext.Current.Session.SessionID + Me.Loginid + "rpt_PPKFL.pdf"
            oPPK.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            oPPK.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            DiskOpts.DiskFileName = strFileLocation

            oPPK.ExportOptions.DestinationOptions = DiskOpts
            oPPK.Export()
            oPPK.Close()
            oPPK.Dispose()
        End If

        Response.Redirect("PPKFinanceLease.aspx?strFileLocation=" & strFilePribadi & "&strFileLocation1=" & strFileCompany & "")
    End Sub
#End Region

#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptPPKFL")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.CmdWhereCompany = cookie.Values("cmdwhereCompany")
        Me.MultiApplicationID = cookie.Values("ApplicationID")
        Me.Branch_ID = cookie.Values("BranchID")
        Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region

End Class