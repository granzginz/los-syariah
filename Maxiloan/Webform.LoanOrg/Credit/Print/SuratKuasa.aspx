﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuratKuasa.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.SuratKuasa" %>

<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../Webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SuratKuasa</title>
    
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var x = screen.width;
        var y = screen.height - 100;	
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinCustomer(pID) {
            window.open('../ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=AccAcq', 'UserLookup', 'left=50, top=10, width=600, height=480, menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlsearch" runat="server">
        <asp:Label ID="lblMessage" runat="server" ForeColor="#993300" font-name="Verdana"
            Font-Size="11px"></asp:Label>
        <br/>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td class="tdtopi" align="center">
                    CETAK SURAT KUASA
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" align="center"
            border="0">
            <tr>
                <td class="tdgenap" valign="middle" width="18%">
                    Cari Berdasarkan
                </td>
                <td class="tdganjil" width="82%">
                    <div align="left">
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="MailTransNo">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Nama Customer</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                        <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox></div>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="20%">
                    Tanggal Kontrak
                </td>
                <td class="tdganjil" width="40%">
                    <uc1:ValidDate id="oAgreementDate" runat="server"></uc1:ValidDate>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" width="20%">
                    Status Cetak
                </td>
                <td class="tdganjil" width="40%">
                    <asp:DropDownList ID="cboPrinted" runat="server">
                        <asp:ListItem Value="No">No</asp:ListItem>
                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td>
                    <div align="left">
                        <asp:ImageButton ID="imgsearch" runat="server" Width="100" Height="20" ImageUrl="../../../Images/ButtonSearch.gif">
                        </asp:ImageButton>&nbsp;
                        <asp:ImageButton ID="imbReset" runat="server" ImageUrl="../../../Images/ButtonReset.gif"
                            CausesValidation="False"></asp:ImageButton></div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br/>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td class="tdtopi" align="center">
                    DAFTAR KONTRAK
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" align="center"
            border="0">
            <tr>
                <td>
                    <asp:DataGrid ID="dtgPPK" runat="server" Width="100%" CellPadding="0" OnSortCommand="SortGrid"
                        BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                        DataKeyField="MailTransNo" AutoGenerateColumns="False" AllowSorting="True">
                        <SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
                        <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                        <ItemStyle CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle Height="30px" CssClass="tdjudul"></HeaderStyle>
                        <FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="MAILTransNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("MailTransNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%#Container.dataitem("Name")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AGREEMENTDATE" HeaderText="TGL KONTRAK" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MailDatePrint" HeaderText="TERAKHIR CETAK" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MailUserPrint" HeaderText="DICETAK OLEH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MailPrintedNum" HeaderText="CETAK KE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="False">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td>
                    <div align="left">
                        <asp:ImageButton ID="imbPrint" runat="server" Width="100" Height="20" ImageUrl="../../../Images/buttonprint.gif">
                        </asp:ImageButton></div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
