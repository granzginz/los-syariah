﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class SuratKuasaEksekusiViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property BDate() As String
        Get
            Return CType(ViewState("BDate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BDate") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_coll As New PPKController
    Private oCustomClass As New Parameter.PPK
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookies()
        BindReport()
        'CreateData()
    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim m_controller As New PPKController
        Dim SKEPO As New Parameter.PPK

        Dim objReport As SuratKuasaEksekusiPrint = New SuratKuasaEksekusiPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        SKEPO.strConnection = GetConnectionString()
        SKEPO.WhereCond = Me.CmdWhere
        SKEPO.BranchId = Me.sesBranchId.Replace("'", "")
        SKEPO.LoginId = Me.Loginid
        SKEPO = m_controller.ListReportSKEPO(SKEPO)
        oData = SKEPO.ListReport

        objReport.SetDataSource(SKEPO.ListReport)

        CrystalReportViewer1.ReportSource = objReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_SKEPO.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        objReport.ExportOptions.DestinationOptions = DiskOpts
        objReport.Export()
        objReport.Close()
        objReport.Dispose()
        Response.Redirect("SuratKuasaEksekusi.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_SKEPO")

    End Sub

#End Region

#Region "CreateData"
    Private Sub CreateData()
        Dim m_controller As New PPKController
        Dim oData As New DataSet
        Dim oPPKreditor As New Parameter.PPK
        oPPKreditor.strConnection = GetConnectionString()
        oPPKreditor.WhereCond = Me.CmdWhere
        oPPKreditor = m_controller.ListReportPPKreditor(oPPKreditor)
        oData = oPPKreditor.ListReport
        oData.WriteXmlSchema("D:\project\Maxiloan\AccAcq\Credit\Print\xmd_PPKreditor.xmd")
    End Sub
#End Region
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptSKEPO")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.BDate = cookie.Values("BusinessDate")
    End Sub
#End Region

End Class