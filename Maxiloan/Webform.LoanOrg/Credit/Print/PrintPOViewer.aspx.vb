﻿

#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class PrintPOViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("cmdwhere") = Value
        End Set
    End Property

    Private Property namaTTD() As String
        Get
            Return CType(ViewState("namaTTD"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("namaTTD") = Value
        End Set
    End Property

    Private Property jabatanTTD() As String
        Get
            Return CType(ViewState("jabatanTTD"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("jabatanTTD") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private m_coll As New PPKController
    Private oCustomClass As New Parameter.PPK
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        BindReport()
    End Sub


#Region "BindReport"
    Sub BindReport()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsPO As New DataSet
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = Me.CmdWhere
        End With

        oCustomClass = m_coll.ListReportPO(oCustomClass)

        dsPO = oCustomClass.ListAgreement

        Dim oReportPO As New PrintPOPrint
        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        oReportPO.SetDataSource(dsPO)

        'AddParamField(oReportPO, "NamaTTD", Me.namaTTD)
        'AddParamField(oReportPO, "JabatanTTD", Me.jabatanTTD)

        'Wira 2015-07-02
        AddParamField(oReportPO, "CompanyName", GetCompanyName())

        CrystalReportViewer1.ReportSource = oReportPO
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        '========================================================
        'Dim discrete As ParameterDiscreteValue
        'Dim ParamFields As ParameterFields
        'Dim ParamField As ParameterFieldDefinition
        'Dim CurrentValue As ParameterValues

        'ParamFields = New ParameterFields
        'ParamField = rptSPPrint.DataDefinition.ParameterFields("BusinessDate")
        'discrete = New ParameterDiscreteValue
        'discrete.Value = Me.BusinessDate
        'CurrentValue = New ParameterValues
        'CurrentValue = ParamField.DefaultValues
        'CurrentValue.Add(discrete)
        'ParamField.ApplyCurrentValues(CurrentValue)
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        oReportPO.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        oReportPO.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String
        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_PrintPO.pdf"
        DiskOpts.DiskFileName = strFileLocation
        'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
        oReportPO.ExportOptions.DestinationOptions = DiskOpts
        oReportPO.Export()
        oReportPO.Close()
        oReportPO.Dispose()
        Response.Redirect("PrintPO.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_PrintPO")

        '=====================================
        'If Request.QueryString("type") <> "1" Then
        '    strHTTPServer = Request.ServerVariables("PATH_INFO")
        '    strNameServer = Request.ServerVariables("SERVER_NAME")
        '    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        '    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" + Me.Session.SessionID + Me.Loginid + "rpt_PrintPO.pdf"
        '    'Call file PDF 

        '    Response.Write("<script language = javascript>" & vbCrLf _
        '                & "history.back(-1) " & vbCrLf _
        '                & "window.open('" & strFileLocation & "') " & vbCrLf _
        '                & "</script>")
        'Else
        '    Response.Redirect("../CreditProcess/PO.aspx?filereport=" & Me.Session.SessionID + Me.Loginid + "rpt_PrintPO.pdf")
        'End If

        'Response.Redirect("PPK.aspx")
    End Sub

    Function GetCompanyName() As String
        Dim result As String

        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If

        Return result
    End Function

#End Region

    Private Sub AddParamField(ByVal oReportPO As PrintPOPrint, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = oReportPO.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptPO")
        Me.CmdWhere = cookie.Values("cmdwhere")
        'Me.namaTTD = cookie.Values("namaTTD")
        'Me.jabatanTTD = cookie.Values("JabatanTTD")
    End Sub
#End Region

End Class