﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper

Public Class Fiducia
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents txtPeriod1 As ucDateCE
    Protected WithEvents txtPeriod2 As ucDateCE


#Region "Constanta"
    Private oCustomClass As New Parameter.SuratKuasaFiducia
    Private oController As New PrintController
#End Region

#Region "Property"
    Private Property cmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            txtPeriod1.Attributes.Add("readOnly", "true")
            txtPeriod2.Attributes.Add("readOnly", "true")
            txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If Request.QueryString("strFileLocation") <> "" Or Request.QueryString("strFileLocation1") <> "" Then

                Dim strFileLocation As String
                strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation")

                Dim strFileLocation1 As String
                strFileLocation1 = "../../../XML/" & Request.QueryString("strFileLocation1")


                If Request.QueryString("strFileLocation") <> "" And Request.QueryString("strFileLocation1") <> "" Then
                    Response.Write("<script language = javascript>" & vbCrLf _
                           & "window.open('" & strFileLocation & "')" & vbCrLf _
                           & "window.open('" & strFileLocation1 & "')" & vbCrLf _
                           & "</script>")
                Else

                    If Request.QueryString("strFileLocation") <> "" Then
                        Response.Write("<script language = javascript>" & vbCrLf _
                           & "window.open('" & strFileLocation & "')" & vbCrLf _
                           & "</script>")
                    End If
                    If Request.QueryString("strFileLocation1") <> "" Then
                        Response.Write("<script language = javascript>" & vbCrLf _
                                   & "window.open('" & strFileLocation1 & "')" & vbCrLf _
                                   & "</script>")
                    End If

                End If



            End If

            Me.FormID = "FIDUPRNT"
            Me.cmdWhere = "MailTransaction.MailPrintedNum = 0  and MailTransaction.MailTypeID='FID' and Agreement.BranchID='" & _
                Me.sesBranchId.Replace("'", "").Trim & "'"
            'Me.cmdWhere = Me.cmdWhere & " and  convert(varchar(10),Agreement.GoLiveDate,120) between '" & _
            '   ConvertDate2(txtPeriod1.Text.Trim).ToString("yyyy-MM-dd") & "'" & " and '" & _
            '   ConvertDate2(txtPeriod2.Text.Trim).ToString("yyyy-MM-dd") & "'"
            Me.SortBy = "AgreementNo ASC"

            DoBind(Me.cmdWhere, Me.SortBy)
        End If
    End Sub

    Sub DoBind(ByVal SearchBy As String, ByVal SortBy As String)
        Dim oDataTable As New DataTable
        Dim oDataView As New DataView

        If cmdWhere.Trim = "" Then cmdWhere = ""

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = SearchBy
            .SortBy = SortBy
            .SPType = "P"
        End With

        oCustomClass = oController.getSuratKuasaFiducia(oCustomClass)

        oDataTable = oCustomClass.Dataset.Tables(0)
        oDataView = oDataTable.DefaultView
        oDataView.Sort = Me.SortBy
        Datagrid1.DataSource = oDataView

        Try
            Datagrid1.DataBind()
        Catch
            Datagrid1.CurrentPageIndex = 0
            Datagrid1.DataBind()
        End Try

        pnlDtGrid.Visible = True
        pnlSearch.Visible = True
        lblMessage.Text = ""
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim iLoop As Integer
            Dim chkPrint As CheckBox
            Dim lblAgreementNo As HyperLink
            Dim lblApplicationID As New Label
            Dim lblBranchID As New Label
            Dim lblCustomerType As New Label
            Dim strMultiApplicationID As String = ""
            Dim strMultiApplicationIDCompany As String = ""
            Dim CmdWherePribadi As String = ""
            Dim CmdWhereCompany As String = ""

            With oDataTable
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            End With

            For iLoop = 0 To Datagrid1.Items.Count - 1
                chkPrint = CType(Datagrid1.Items(iLoop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo = CType(Datagrid1.Items(iLoop).Cells(1).FindControl("hyAgreementNo"), HyperLink)
                lblApplicationID = CType(Datagrid1.Items(iLoop).Cells(6).FindControl("lblApplicationId"), Label)
                lblCustomerType = CType(Datagrid1.Items(iLoop).FindControl("lblCustomerType"), Label)

                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("AgreementNo") = CType(lblAgreementNo.Text.Trim, String)
                    oRow("ApplicationID") = lblApplicationID.Text.Trim
                    oDataTable.Rows.Add(oRow)

                    If lblCustomerType.Text.ToUpper.Trim = "C" Then
                        If strMultiApplicationIDCompany.Trim = "" Then
                            strMultiApplicationIDCompany = strMultiApplicationIDCompany + "'" + lblApplicationID.Text.Trim + "'"
                        Else
                            strMultiApplicationIDCompany = strMultiApplicationIDCompany + ",'" + lblApplicationID.Text.Trim + "'"
                        End If
                    End If
                    If lblCustomerType.Text.ToUpper.Trim = "P" Then
                        If strMultiApplicationID.Trim = "" Then
                            strMultiApplicationID = strMultiApplicationID + "'" + lblApplicationID.Text.Trim + "'"
                        Else
                            strMultiApplicationID = strMultiApplicationID + ",'" + lblApplicationID.Text.Trim + "'"
                        End If
                    End If
                End If
            Next

            If oDataTable.Rows.Count = 0 Then
                'lblMessage.Text = "Harap pilih Baris yang akan dicetak"
                ShowMessage(lblMessage, "Harap pilih Baris yang akan dicetak", True)
                Exit Sub
            End If

            With oCustomClass
                .strConnection = GetConnectionString()
                .Datatable = oDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With

            oCustomClass = oController.saveSuratKuasaPrint(oCustomClass)

            If Not oCustomClass.Hasil Then
                lblMessage.Text = "Cetak gagal, Terjadi Kesalahan waktu simpan History" & vbCrLf & oCustomClass.ErrorMessage
                Exit Sub

            Else
                If strMultiApplicationIDCompany.Trim <> "" Then
                    CmdWhereCompany = " Agreement.ApplicationID in(" & strMultiApplicationIDCompany & ") and MailTransaction.MailTypeID='FID'"
                End If
                If strMultiApplicationID.Trim <> "" Then
                    CmdWherePribadi = " Agreement.ApplicationID in(" & strMultiApplicationID & ") and MailTransaction.MailTypeID='FID'"
                End If

                Dim cookie As HttpCookie = Request.Cookies(COOKIES_FIDUCIA)

                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = CmdWherePribadi
                    cookie.Values("cmdwhereCompany") = CmdWhereCompany
                    cookie.Values("ApplicationID") = strMultiApplicationID
                    cookie.Values("sortby") = Me.SortBy.Replace("MAILTransNo", "AgreementNo")

                    Response.AppendCookie(cookie)

                Else
                    Dim cookieNew As New HttpCookie(COOKIES_FIDUCIA)

                    cookieNew.Values.Add("cmdwhere", CmdWherePribadi)
                    cookieNew.Values.Add("cmdwhereCompany", CmdWhereCompany)
                    cookieNew.Values.Add("ApplicationID", strMultiApplicationID)
                    cookieNew.Values.Add("sortby", Me.SortBy.Replace("MailTransNo", "AgreementNo"))

                    Response.AppendCookie(cookieNew)
                End If

                Response.Redirect("FiduciaViewer.aspx")
            End If
        End If
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.SearchBy = " MailPrintedNum = 0 and Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "' and  MailTransaction.MailTypeID='FID'"
        Else
            Me.SearchBy = " MailPrintedNum > 0  and Agreement.BranchID='" & Me.sesBranchId.Replace("'", "") & "' and  MailTransaction.MailTypeID='FID'"
        End If

        If txtPeriod1.Text.Trim <> "" And txtPeriod2.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and Agreement.BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'" & _
            " and Agreement.DateEntryApplicationData between '" & ConvertDate2(txtPeriod1.Text.Trim).ToString("yyyy-MM-dd") & "'" & _
            " and '" & ConvertDate2(txtPeriod2.Text.Trim).ToString("yyyy-MM-dd") & "'"
        End If

        Dim SearchTemp As String

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            SearchTemp = cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Replace("%", "").Trim & "%'"
            Me.SearchBy = Me.SearchBy & " and " & SearchTemp
        End If

        Me.SortBy = "AgreementNo ASC"
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = " MailTransaction.MailTypeID='FID' and MailTransaction.MailPrintedNum = 0 and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
        Me.SortBy = "MAILTransNo ASC"
        DoBind(Me.SearchBy, Me.SortBy)
        txtSearchBy.Text = ""
        cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub

    Function LinkToSupplier(ByVal strSupplierID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinSupplier('" & strStyle & "','" & strSupplierID & "')"
    End Function

    Private Sub Datagrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Datagrid1.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblCustomerName As New HyperLink
            Dim lblCustomerID As New Label
            Dim lblApplicationId As Label

            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblCustomerName.NavigateUrl = LinkToSupplier(lblCustomerID.Text.Trim, "AccAcq")

            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

End Class