﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Kwitansi.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.Kwitansi" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Kwitansi</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script> 
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK KWITANSI</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="">Select One</asp:ListItem>
                    <asp:ListItem Value="MailTransNo">Agreement No</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Customer Name</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox>
            </div>
        </div>
       <%-- <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Kontrak</label>
                <asp:TextBox runat="server" ID="txtAgreementDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtAgreementDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </div>
        </div>--%>

          <div class="form_box">
            <div class="form_single">
                <label>
                    Tanggal Kontrak</label>
                <uc2:ucdatece id="txtAgreementDate" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Status Cetak</label>
                <asp:DropDownList ID="cboPrinted" runat="server">
                    <asp:ListItem Value="No">No</asp:ListItem>
                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR KWITANSI</h4>
            </div>
        </div>
        <%--<div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgKwitansi" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="MailTransNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="MAILTransNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("MailTransNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>','AccAcq');">
                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%#Container.dataitem("Name")%>'>
                                        </asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AGREEMENTDATE" SortExpression="AGREEMENTDATE" HeaderText="TGL KONTRAK"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MailDatePrint" SortExpression="MailDatePrint" HeaderText="TERAKHIR CETAK"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MailUserPrint" SortExpression="MailUserPrint" HeaderText="DICETAK OLEH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="MailPrintedNum" SortExpression="MailPrintedNum" HeaderText="CETAK KE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="Application Id" Visible="false">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                </div>
                </div>
            </div>--%>

        <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgKwitansi" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="SortGrid" DataKeyField="MailTransNo" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">                                
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="MAILTransNo" HeaderText="NO KONTRAK">                                
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("MailTransNo")%>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblApplicationID" runat="server" Visible="False" Text='<%#Container.DataItem("ApplicationID")%>'></asp:Label>
                                            <asp:Label ID="lblBranchID" runat="server" Visible="False" Text='<%#Container.DataItem("BranchID")%>'></asp:Label>
                                            <asp:Label ID="lblCustomerID" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerID")%>'></asp:Label>
                                            <asp:Label ID="lblCustomerType" runat="server" Visible="False" Text='<%#Container.DataItem("CustomerType")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA CUSTOMER">                                
                                        <ItemTemplate>
                                                <a href="javascript:OpenWinCustomer('<%# Container.DataItem("CustomerID")%>');">
                                                <asp:Label ID="lblCustomerName" runat="server" Text='<%#Container.dataitem("Name")%>'>
                                                </asp:Label></a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="AGREEMENTDATE" HeaderText="TGL KONTRAK" DataFormatString="{0:dd/MM/yyyy}">                                
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="MailDatePrint" HeaderText="TERAKHIR CETAK" DataFormatString="{0:dd/MM/yyyy}">                                
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="MailUserPrint" HeaderText="DICETAK OLEH">                                
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="MailPrintedNum" HeaderText="CETAK KE">                                
                                    </asp:BoundColumn>
                                </Columns>                        
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                        <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                                            CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                                            CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                                            CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                                            CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                        </asp:ImageButton>
                                        <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                        <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                            EnableViewState="False"></asp:Button>
                                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                            ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="label_gridnavigation">
                                        <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                        <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                                    </div>
                        </div>
                    </div>
                </div>

        <div class="form_button">
            <asp:Button ID="BtnPrint" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
