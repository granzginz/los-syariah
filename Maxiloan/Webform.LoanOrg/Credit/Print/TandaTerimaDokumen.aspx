﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TandaTerimaDokumen.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.TandaTerimaDokumen" %>

<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../../Webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>TandaTerima Dokumen</title>
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlsearch" runat="server">
        <asp:Label ID="lblMessage" runat="server" ForeColor="#993300" font-name="Verdana"
            Font-Size="11px"></asp:Label>
        <br/>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td class="tdtopi" align="center">
                    CETAK TANDA TERIMA DOKUMEN
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="1" cellpadding="3" width="95%" align="center"
            border="0">
            <tr>
                <td class="tdgenap" valign="middle" width="18%">
                    Cari Berdasarkan
                </td>
                <td class="tdganjil" width="82%">
                    <div align="left">
                        <asp:DropDownList ID="cboSearchBy" runat="server">
                            <asp:ListItem Value="">Select One</asp:ListItem>
                            <asp:ListItem Value="Agreement.AgreementNo">Agreement No</asp:ListItem>
                            <asp:ListItem Value="Customer.Name">Customer Name</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;
                        <asp:TextBox ID="txtSearchBy" runat="server" ></asp:TextBox></div>
                </td>
            </tr>
            <tr>
                <td class="tdgenap" valign="middle" width="18%">
                    Tanggal Loan Activation
                </td> 
                <td class="tdganjil" width="82%">
                    <uc1:ValidDate id="oPeriod1" runat="server"></uc1:ValidDate>&nbsp;S/D
                    <uc1:ValidDate id="oPeriod2" runat="server"></uc1:ValidDate>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td>
                    <div align="left">
                        <asp:ImageButton ID="imgsearch" runat="server" Width="100" Height="20" ImageUrl="../../../Images/ButtonSearch.gif">
                        </asp:ImageButton>&nbsp;
                        <asp:ImageButton ID="imbReset" runat="server" ImageUrl="../../../Images/ButtonReset.gif"
                            CausesValidation="False"></asp:ImageButton></div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br/>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td class="tdtopi" align="center">
                    DAFTAR TANDA TERIMA DOKUMEN
                </td>
            </tr>
        </table>
        <table class="tablegrid" cellspacing="0" cellpadding="0" width="95%" align="center"
            border="0">
            <tr>
                <td>
                    <asp:DataGrid ID="dtgPPK" runat="server" Width="100%" CellPadding="0" OnSortCommand="SortGrid"
                        BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#CCCCCC"
                        DataKeyField="AgreementNo" AutoGenerateColumns="False" AllowSorting="True">
                        <SelectedItemStyle CssClass="tdgenap"></SelectedItemStyle>
                        <AlternatingItemStyle CssClass="tdgenap"></AlternatingItemStyle>
                        <ItemStyle CssClass="tdganjil"></ItemStyle>
                        <HeaderStyle Height="30px" CssClass="tdjudul"></HeaderStyle>
                        <FooterStyle ForeColor="#000066" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                    <asp:Label Visible="False" ID="lblAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:Label>
                                    <asp:Label Visible="False" ID="lblApplicationID" runat="server" Text='<%#Container.DataItem("ApplicationID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblCustomerName" runat="server" Text='<%#Container.dataitem("CustomerName")%>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblCustomerID" runat="server" Visible="False" Text='<%#Container.dataitem("CustomerID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AGREEMENTDATE" SortExpression="AGREEMENTDATE" HeaderText="TGL KONTRAK"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
            <tr>
                <td>
                    <div align="left">
                        <asp:ImageButton ID="imbPrint" runat="server" Width="100" Height="20" ImageUrl="../../../Images/buttonprint.gif">
                        </asp:ImageButton></div>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </form>
</body>
</html>
