﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class BPKBInProses
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtPeriod1 As ucDateCE
    Protected WithEvents txtPeriod2 As ucDateCE

    Private Property cmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private oCustomClass As New Parameter.PernyataanSetuju
    Private oController As New PernyataanSetujuController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If Request.QueryString("strFileLocation") <> "" Then

                Dim strFileLocation As String
                strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
               & "var x = screen.width; var y = screen.height;  window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
               & "</script>")

            End If

            Me.FormID = "RPTBPKBINPROSES"
            Me.cmdWhere = "MailTransaction.MailPrintedNum = 0  and MailTransaction.MailTypeID='BPKBP' and ag.BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'"
            Me.cmdWhere = Me.cmdWhere & " and  convert(varchar(10),DateEntryApplicationData,120) between '" & ConvertDate2(txtPeriod1.Text.Trim).ToString("yyyy-MM-dd") & "' and '" & ConvertDate2(txtPeriod2.Text.Trim).ToString("yyyy-MM-dd") & "'"

            Me.SortBy = "AgreementNo ASC"
            DoBind(Me.cmdWhere, Me.SortBy)
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim oDataTable As DataTable
        Dim oDataView As DataView

        If cmdWhere.Trim = "" Then cmdWhere = ""

        With oCustomClass
            .FormID = Me.FormID
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.listPernyataanSetuju(oCustomClass)
        oDataTable = oCustomClass.listPernyataanSetuju.Tables(0)
        oDataView = oDataTable.DefaultView
        oDataView.Sort = Me.SortBy

        Datagrid1.DataSource = oDataView

        Try
            Datagrid1.DataBind()
        Catch
            Datagrid1.CurrentPageIndex = 0
            Datagrid1.DataBind()
        End Try

    End Sub

    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"

    End Function

    Private Sub Datagrid1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles Datagrid1.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblCustomerName As New HyperLink
            Dim lblCustomerID As New Label
            Dim lblApplicationId As Label

            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")

            'Agreement has no link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub


    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "PRINT", Me.AppId) Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            ' Dim iLoop As Integer
            Dim chkPrint As CheckBox
            Dim lblAgreementNo As HyperLink
            Dim lblApplicationID As New Label
            Dim lblBranchID As New Label
            Dim strMultiApplicationID As String = ""


            With oDataTable
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            End With

            'For iLoop = 0 To Datagrid1.Items.Count - 1
            'Next

            For iLoop As Integer = 0 To Datagrid1.Items.Count - 1
                chkPrint = CType(Datagrid1.Items(iLoop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo = CType(Datagrid1.Items(iLoop).Cells(0).FindControl("hyAgreementNo"), HyperLink)
                lblApplicationID = CType(Datagrid1.Items(iLoop).Cells(0).FindControl("lblApplicationId"), Label)

                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("AgreementNo") = CType(lblAgreementNo.Text.Trim, String)
                    oRow("ApplicationID") = lblApplicationID.Text.Trim
                    oDataTable.Rows.Add(oRow)

                    If strMultiApplicationID.Trim = "" Then
                        strMultiApplicationID = strMultiApplicationID & "'" & lblApplicationID.Text.Trim & "'"
                    Else
                        strMultiApplicationID = strMultiApplicationID & ",'" & lblApplicationID.Text.Trim & "'"
                    End If
                End If
            Next
            Me.cmdWhere = " ag.ApplicationID in(" & strMultiApplicationID & ") and MailTransaction.MailTypeID='BPKBP'"

            If oDataTable.Rows.Count = 0 Then
                lblMessage.Text = "Harap pilih baris yang ingin dicetak"
                Exit Sub
            End If

            With oCustomClass
                .FormID = Me.FormID
                .strConnection = GetConnectionString()
                .dtPernyataanSetuju = oDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With

            oCustomClass = oController.savePrintPernyataanSetuju(oCustomClass)

            If Not oCustomClass.Hasil Then
                lblMessage.Text = "Cetak gagal, terjadi kesalahan simpan history" & vbCrLf & oCustomClass.ErrorMessage
                Exit Sub

            Else
                Dim cookie As HttpCookie = Request.Cookies("BPKBInProsesCookie")
                'BPKBInProses
                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = Me.cmdWhere
                    cookie.Values("ApplicationID") = strMultiApplicationID
                    cookie.Values("sortby") = Me.SortBy.Replace("MAILTransNo", "AgreementNo")

                    Response.AppendCookie(cookie)

                Else
                    Dim cookieNew As New HttpCookie("BPKBInProsesCookie")

                    cookieNew.Values.Add("cmdwhere", Me.cmdWhere)
                    cookieNew.Values.Add("ApplicationID", strMultiApplicationID)
                    cookieNew.Values.Add("sortby", Me.SortBy.Replace("MAILTransNo", "AgreementNo"))

                    Response.AppendCookie(cookieNew)
                End If

                Response.Redirect("BPKBInProsesViewer.aspx")
            End If
        End If
    End Sub

    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        txtSearchBy.Text = ""
        cboSearchBy.SelectedIndex = 0
    End Sub
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.SearchBy = " MailPrintedNum = 0 and ag.BranchID='" & Me.sesBranchId.Replace("'", "") & "' and  MailTransaction.MailTypeID='BPKBP'"
        Else
            Me.SearchBy = " MailPrintedNum > 0  and ag.BranchID='" & Me.sesBranchId.Replace("'", "") & "' and  MailTransaction.MailTypeID='BPKBP'"
        End If

        If txtPeriod1.Text.Trim <> "" And txtPeriod2.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and ag.BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'" & _
            " and convert(varchar(10),ag.DateEntryApplicationData,120) between '" & ConvertDate2(txtPeriod1.Text.Trim).ToString("yyyy-MM-dd") & "'" & _
            " and '" & ConvertDate2(txtPeriod2.Text.Trim).ToString("yyyy-MM-dd") & "'"
        End If

        Dim SearchTemp As String
        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            SearchTemp = cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Replace("%", "").Trim & "%'"
            Me.SearchBy = Me.SearchBy & " and " & SearchTemp
        End If

        Me.SortBy = "AgreementNo ASC"
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
End Class