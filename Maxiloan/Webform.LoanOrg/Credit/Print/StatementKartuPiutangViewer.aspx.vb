﻿Imports Maxiloan.Controller
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.General.CommonCacheHelper
Imports System.Data.SqlClient
Imports Maxiloan.Webform.UserController

Public Class StatementKartuPiutangViewer
    Inherits Maxiloan.Webform.WebBased

    Private ocustomclass As New Parameter.GeneralPaging
    Private ocontroller As New GeneralPagingController

    Public Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Public Property Status() As String
        Get
            Return CStr(ViewState("Status"))
        End Get
        Set(ByVal Value As String)
            ViewState("Status") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub
        GetCookies()
        'If Me.Status = "C" Then
        '    BindReportCustomer()
        'Else
        '    BindReport()
        'End If
        BindReportAmortisasi()
    End Sub


    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("StatementKartuPiutang")
        Me.ApplicationID = cookie.Values("ApplicationId")
        Me.BranchID = cookie.Values("BranchID")
        Me.Status = cookie.Values("Status")
        Me.AgreementNo = cookie.Values("AgreementNo")
    End Sub

    Sub BindReportAmortisasi()
        Dim ds As New DataSet
        Dim objreport As ReportDocument

        objreport = New StatementAmortisasi

        ds = GetData()
        objreport.SetDataSource(ds)

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "StatementAmortisasi.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objreport.ExportOptions.DestinationOptions = DiskOpts
        objreport.Export()
        objreport.Close()
        objreport.Dispose()
        Response.Redirect("../ViewStatementOfAccount.aspx?ApplicationId=" & Me.ApplicationID.Trim & "&AgreementNo=" & Me.AgreementNo.Trim & "&filekwitansi=" & Me.Session.SessionID & Me.Loginid & "StatementAmortisasi")
    End Sub

    Sub BindReport()
        Dim ds As New DataSet
        Dim objreport As ReportDocument

        objreport = New StatementKartuPiutang

        ds = GetData()
        objreport.SetDataSource(ds)

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "StatementKartuPiutang.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objreport.ExportOptions.DestinationOptions = DiskOpts
        objreport.Export()
        objreport.Close()
        objreport.Dispose()
        Response.Redirect("../ViewStatementOfAccount.aspx?ApplicationId=" & Me.ApplicationID.Trim & "&AgreementNo=" & Me.AgreementNo.Trim & "&filekwitansi=" & Me.Session.SessionID & Me.Loginid & "StatementKartuPiutang")
    End Sub

    Sub BindReportCustomer()
        Dim ds As New DataSet
        Dim objreport As ReportDocument

        objreport = New StatementKartuPiutangCust

        ds = GetDataCustomer()
        objreport.SetDataSource(ds)

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "StatementKartuPiutangCustomer.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objreport.ExportOptions.DestinationOptions = DiskOpts
        objreport.Export()
        objreport.Close()
        objreport.Dispose()
        Response.Redirect("../ViewStatementOfAccount.aspx?ApplicationId=" & Me.ApplicationID.Trim & "&AgreementNo=" & Me.AgreementNo.Trim & "&filekwitansi=" & Me.Session.SessionID & Me.Loginid & "StatementKartuPiutangCustomer")
    End Sub

    Public Function GetData() As DataSet
        Dim ds As New DataSet
        Dim adapter As New SqlDataAdapter
        Dim objCommand As New SqlCommand
        Dim objcon As New SqlConnection(GetConnectionString)

        Try
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objcon
            'modify nofi SP spGetAmortisasiInternal 10092018
            objCommand.CommandText = "spGetAmortisasiInternal" 'spGetAmortisasi 'spGetKartuPiutang
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Value = Me.BranchID
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.Char, 20).Value = Me.ApplicationID
            objCommand.CommandTimeout = 60

            adapter.SelectCommand = objCommand
            adapter.Fill(ds)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try

        Return ds
    End Function
    Public Function GetDataCustomer() As DataSet
        Dim ds As New DataSet
        Dim adapter As New SqlDataAdapter
        Dim objCommand As New SqlCommand
        Dim objcon As New SqlConnection(GetConnectionString)

        Try
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Connection = objcon
            objCommand.CommandText = "spGetKartuPiutangCustomer"
            objCommand.Parameters.Add("@BranchID", SqlDbType.Char, 3).Value = Me.BranchID
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.Char, 20).Value = Me.ApplicationID
            objCommand.CommandTimeout = 60

            adapter.SelectCommand = objCommand
            adapter.Fill(ds)
        Catch ex As Exception
            Throw New Exception("Error On DataAccess.GetData")
        End Try

        Return ds
    End Function
End Class