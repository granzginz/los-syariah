﻿Imports Maxiloan.Controller
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class StatementOfAccountViewer
    Inherits Maxiloan.Webform.WebBased

    Private ocustomclass As New Parameter.GeneralPaging
    Private ocontroller As New GeneralPagingController

    Public Property ApplicationID() As String
        Get
            Return CStr(viewstate("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then Exit Sub        
        BindReport()
    End Sub


    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("StatementOfAccount")
        Me.ApplicationID = cookie.Values("ApplicationId")        
    End Sub

    Sub BindReport()
        GetCookies()

        Dim ds As New DataSet
        Dim oDCReport As New Parameter.GeneralPaging
        Dim strconn As String = GetConnectionString()        
        Dim objreport As ReportDocument

        objreport = New StatementAccountPrint
        With ocustomclass
            .strConnection = GetConnectionString
            .WhereCond = "  ins.applicationid = '" & Me.ApplicationID.Trim & "'"
            .SpName = "spStatementAccountReport"
            .BusinessDate = Me.BusinessDate
        End With


        oDCReport = ocontroller.GetReportWithTwoParameter(ocustomclass)
        ds = oDCReport.ListDataReport

        Dim numCopies As Int16 = 1
        Dim Collate As Boolean = False
        Dim startpage As Int16 = 1
        objreport.SetDataSource(ds)

        Dim discrete As ParameterDiscreteValue
        Dim ParamFields As ParameterFields
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues


        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("PrgId")
        discrete = New ParameterDiscreteValue
        discrete.Value = "RptStatementOfAccountRpt"
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("PrintedBy")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.Loginid
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)


        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("Param")
        discrete = New ParameterDiscreteValue
        discrete.Value = "Application Id : " & Me.ApplicationID.Trim
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)

        ParamFields = New ParameterFields
        ParamField = objreport.DataDefinition.ParameterFields("sdate")
        discrete = New ParameterDiscreteValue
        discrete.Value = Me.BusinessDate.ToString("dd/MM/yyyy")
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)


        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        objreport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
        objreport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID & Me.Loginid & "StatementOfAccount.pdf"
        DiskOpts.DiskFileName = strFileLocation
        objreport.ExportOptions.DestinationOptions = DiskOpts
        objreport.Export()
        objreport.Close()
        objreport.Dispose()
        Response.Redirect("../ViewStatementOfAccount.aspx?ApplicationId=" & Me.ApplicationID.Trim & "&filekwitansi=" & Me.Session.SessionID & Me.Loginid & "StatementOfAccount")
    End Sub

    'Private Sub imbBackReport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBackReport.Click
    '    Dim Cookie As HttpCookie = Request.Cookies("StatementOfAccount")
    '    Dim dt As DateTime = CDate("01/01/1950 00:00:00 AM")
    '    Dim ts As New TimeSpan(-2, 0, 0, 0)

    '    If Not Cookie Is Nothing Then
    '        Cookie.Expires = dt.Add(ts)
    '        Response.AppendCookie(Cookie)
    '    End If
    '    Response.Redirect("../ViewStatementOfAccount.aspx?ApplicationId=" & Me.ApplicationID.Trim & "&style=" & Me.Style)
    'End Sub

End Class