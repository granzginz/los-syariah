﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
#End Region

Public Class WelcomeLetterViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"
    Private oController As New PPKController
    Private oParameter As New Parameter.PPK
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If SessionInvalid() Then
            Exit Sub
        End If

        bindReport()
        'createData()
    End Sub
#End Region

#Region "BindReport"
    Private Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oDataSet2 As New DataSet
        Dim oReport As WelcomeLetterPrint = New WelcomeLetterPrint

        With oParameter
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .MultiApplicationID = Me.MultiApplicationID
            .SortBy = Me.SortBy
        End With

        oParameter = oController.ListReportWLetter(oParameter)
        oDataSet = oParameter.ListReport


        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = Me.CmdWhere
        oParameter.ApplicationID = Me.ApplicationID
        oParameter.LoginId = Me.Loginid
        oParameter = oController.ListReportWLetter(oParameter)
        oDataSet = oParameter.ListReport


        oParameter.strConnection = GetConnectionString()
        oParameter.ApplicationID = Me.ApplicationID
        oParameter = oController.ListReportWLetterSubRpt(oParameter)
        oDataSet2 = oParameter.ListReport

        oReport.SetDataSource(oDataSet)
        oReport.Subreports.Item("WelcomeLetterPrint2").SetDataSource(oDataSet2)

        'Wira 2015-07-02
        'AddParamField(oReport, "CompanyName", GetCompanyName())

        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_WLetter.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("WelcomeLetter.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_WLetter")

    End Sub



    Function GetCompanyName() As String
        Dim result As String

        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If

        Return result
    End Function

    'Private Sub AddParamField(ByVal oReport As PPK, ByVal fieldName As String, ByVal value As Object)

    '    Dim discrete As ParameterDiscreteValue
    '    Dim ParamField As ParameterFieldDefinition
    '    Dim CurrentValue As ParameterValues

    '    ParamField = oReport.DataDefinition.ParameterFields(fieldName)
    '    discrete = New ParameterDiscreteValue
    '    discrete.Value = value
    '    CurrentValue = New ParameterValues
    '    CurrentValue = ParamField.DefaultValues
    '    CurrentValue.Add(discrete)
    '    ParamField.ApplyCurrentValues(CurrentValue)
    'End Sub
#End Region

#Region "CreateData"
    Private Sub createData()
        Dim oController As New PPKController
        Dim oDataSet As New DataSet
        Dim oParameter As New Parameter.PPK

        oParameter.strConnection = GetConnectionString()
        oParameter = oController.ListReportPPKreditor(oParameter)
        oDataSet = oParameter.ListReport
        oDataSet.WriteXmlSchema("D:\project\Maxiloan\AccAcq\Credit\Print\xmd_PPKreditor.xml")
    End Sub
#End Region

#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptWLetter")

        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.MultiApplicationID = cookie.Values("ApplicationID")
        Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region

End Class