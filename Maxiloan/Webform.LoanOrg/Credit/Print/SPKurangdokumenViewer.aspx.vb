﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
#End Region


Public Class SPKurangdokumenViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oController As New PernyataanSetujuController
    Private oParameter As New Parameter.PernyataanSetuju
#End Region

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        bindReport()
    End Sub


#Region "BindReport"
    Private Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oReport As SPKurangdokumenPrint = New SPKurangdokumenPrint

        With oParameter
            .FormID = "SPKurangdokumen"
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .MultiApplicationID = Me.MultiApplicationID
            .SortBy = Me.SortBy
        End With

        oParameter = oController.listPernyataanSetuju(oParameter)
        oDataSet = oParameter.listPernyataanSetuju

        oReport.SetDataSource(oDataSet)
        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "SPKurangdokumen.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("SPKurangdokumen.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "SPKurangdokumen")
    End Sub
#End Region

#Region "CreateData"
    Private Sub createData()
        Dim oController As New PernyataanSetujuController
        Dim oDataSet As New DataSet
        Dim oParameter As New Parameter.PernyataanSetuju

        oParameter.strConnection = GetConnectionString()
        oParameter = oController.listPernyataanSetuju(oParameter)
        oDataSet = oParameter.listPernyataanSetuju
        oDataSet.WriteXmlSchema("c:\maxiloan.reportdef\listSPKurangdokumen.xml")
    End Sub
#End Region

#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("SPKurangdokumenCookie")

        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.MultiApplicationID = cookie.Values("ApplicationID")
        Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region
End Class