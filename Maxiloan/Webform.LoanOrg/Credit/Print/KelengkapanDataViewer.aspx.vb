﻿#Region "Imports"
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
#End Region

Public Class KelengkapanDataViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property CmdWherePribadi() As String
        Get
            Return CType(ViewState("CmdWherePribadi"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWherePribadi") = Value
        End Set
    End Property
    Private Property CmdWhereCompany() As String
        Get
            Return CType(ViewState("CmdWhereCompany"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhereCompany") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private m_coll As New PPKController
    Private oCustomClass As New Parameter.PPK
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        GetCookiesPribadi()
        GetCookiesCompany()
        BindReportPribadi()
        BindReportCompany()
        Response.Redirect("KelengkapanData.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_Kelengkapan_DataPribadi&strFileLocation1=" & Me.Session.SessionID & Me.Loginid & "rpt_KelengkapanData_Company")
    End Sub
#End Region
#Region "BindReportPribadi"
    Sub BindReportPribadi()
        Dim oData As New DataSet
        Dim oData1 As New DataSet
        Dim oData2 As New DataSet
        Dim oData3 As New DataSet
        Dim m_controller As New PPKController
        Dim oKelengkapanData As New Parameter.PPK

        Dim objReport As KelengkapanDataPrint = New KelengkapanDataPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions


        If Me.CmdWherePribadi.Trim <> "" Then

            oKelengkapanData.strConnection = GetConnectionString()
            oKelengkapanData.WhereCond = Me.CmdWherePribadi
            oKelengkapanData.WhereCond2 = "1"
            oKelengkapanData.Status = "P"
            oKelengkapanData = m_controller.ListReportKelengkapanData(oKelengkapanData)
            oData = oKelengkapanData.ListKelengkapanDataReport

            oKelengkapanData.strConnection = GetConnectionString()
            oKelengkapanData.WhereCond = Me.CmdWherePribadi
            oKelengkapanData.WhereCond2 = "2"
            oKelengkapanData.Status = "P"
            oKelengkapanData = m_controller.ListReportKelengkapanData(oKelengkapanData)
            oData1 = oKelengkapanData.ListKelengkapanDataReport

            oKelengkapanData.strConnection = GetConnectionString()
            oKelengkapanData.WhereCond = Me.CmdWherePribadi
            oKelengkapanData.WhereCond2 = "3"
            oKelengkapanData.Status = "P"
            oKelengkapanData = m_controller.ListReportKelengkapanData(oKelengkapanData)
            oData2 = oKelengkapanData.ListKelengkapanDataReport

            oKelengkapanData.strConnection = GetConnectionString()
            oKelengkapanData.WhereCond = Me.CmdWherePribadi
            oKelengkapanData.WhereCond2 = "4"
            oKelengkapanData.Status = "P"
            oKelengkapanData = m_controller.ListReportKelengkapanData(oKelengkapanData)
            oData3 = oKelengkapanData.ListKelengkapanDataReport



            objReport.SetDataSource(oData)
            objReport.Subreports.Item("KelengkapanDataPrint1").SetDataSource(oData1)
            objReport.Subreports.Item("LainLain").SetDataSource(oData2)
            objReport.Subreports.Item("KelengkapanDataDokumen").SetDataSource(oData3)

            CrystalReportViewer1.ReportSource = objReport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()


            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String
            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_Kelengkapan_DataPribadi.pdf"
            DiskOpts.DiskFileName = strFileLocation
            'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
            objReport.ExportOptions.DestinationOptions = DiskOpts
            objReport.Export()
            objReport.Close()
            objReport.Dispose()
        End If

    End Sub
#End Region
#Region "GetCookiesPribadi"
    Sub GetCookiesPribadi()
        Dim cookie As HttpCookie = Request.Cookies("RptKelengkapanDataPribadi")
        Me.CmdWherePribadi = cookie.Values("cmdwherePribadi")
    End Sub
#End Region
#Region "BindReportCompany"
    Sub BindReportCompany()
        Dim oData As New DataSet
        Dim oData1 As New DataSet
        Dim oData2 As New DataSet
        Dim odata3 As New DataSet
        Dim m_controller As New PPKController
        Dim oKelengkapanData As New Parameter.PPK

        Dim objReport As KelengkapanDataCompanyPrint = New KelengkapanDataCompanyPrint
        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        If Me.CmdWhereCompany.Trim <> "" Then

            oKelengkapanData.strConnection = GetConnectionString()
            oKelengkapanData.WhereCond = Me.CmdWhereCompany
            oKelengkapanData.WhereCond2 = "1"
            oKelengkapanData.Status = "C"
            oKelengkapanData = m_controller.ListReportKelengkapanData(oKelengkapanData)
            oData = oKelengkapanData.ListKelengkapanDataReport

            oKelengkapanData.strConnection = GetConnectionString()
            oKelengkapanData.WhereCond = Me.CmdWhereCompany
            oKelengkapanData.WhereCond2 = "2"
            oKelengkapanData.Status = "C"
            oKelengkapanData = m_controller.ListReportKelengkapanData(oKelengkapanData)
            oData1 = oKelengkapanData.ListKelengkapanDataReport

            oKelengkapanData.strConnection = GetConnectionString()
            oKelengkapanData.WhereCond = Me.CmdWhereCompany
            oKelengkapanData.WhereCond2 = "3"
            oKelengkapanData.Status = "C"
            oKelengkapanData = m_controller.ListReportKelengkapanData(oKelengkapanData)
            oData2 = oKelengkapanData.ListKelengkapanDataReport


            oKelengkapanData.strConnection = GetConnectionString()
            oKelengkapanData.WhereCond = Me.CmdWhereCompany
            oKelengkapanData.WhereCond2 = "4"
            oKelengkapanData.Status = "C"
            oKelengkapanData = m_controller.ListReportKelengkapanData(oKelengkapanData)
            odata3 = oKelengkapanData.ListKelengkapanDataReport

            objReport.SetDataSource(oData)
            objReport.Subreports.Item("KelengkapanDataPrint1").SetDataSource(oData1)
            objReport.Subreports.Item("LainLain").SetDataSource(oData2)
            If odata3.Tables(0).Rows.Count > 0 Then
                objReport.Subreports.Item("KelengkapanDataDokumen").SetDataSource(odata3)
            End If


            CrystalReportViewer1.ReportSource = objReport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()


            Dim strHTTPServer As String
            Dim StrHTTPApp As String
            Dim strNameServer As String
            objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String
            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_KelengkapanData_Company.pdf"
            DiskOpts.DiskFileName = strFileLocation
            'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
            objReport.ExportOptions.DestinationOptions = DiskOpts
            objReport.Export()
            objReport.Close()
        End If


    End Sub
#End Region
#Region "GetCookiesCompany"
    Sub GetCookiesCompany()
        Dim cookie As HttpCookie = Request.Cookies("RptKelengkapanDataCompany")
        Me.CmdWhereCompany = cookie.Values("cmdwhereCompany")
    End Sub
#End Region
End Class