﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region


Public Class Kwitansi
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtAgreementDate As ucDateCE


#Region "Constanta"
    Private oCustomClass As New Parameter.PPK
    Private oController As New PPKController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 20
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#End Region
    Public Function PredicateFunction(ByVal custom As Parameter.Application) As Boolean
        Return custom.ApplicationID = Me.ApplicationID And custom.CustomerId = Me.CustomerID And custom.AgreementNo = Me.AgreementNo
    End Function
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If sessioninvalid() Then
                Exit Sub
            End If
            If Request.QueryString("strFileLocation") <> "" Then
                'Dim strHTTPServer As String
                'Dim StrHTTPApp As String
                'Dim strNameServer As String
                Dim strFileLocation As String

                'strHTTPServer = Request.ServerVariables("PATH_INFO")
                'strNameServer = Request.ServerVariables("SERVER_NAME")
                'StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width;" & vbCrLf _
                & "var y = screen.height;" & vbCrLf _
                & "window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If

            txtAgreementDate.Text = ""

            Me.FormID = "PrintKwitansi"
            If checkform(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    pnlsearch.Visible = True
                    pnlDtGrid.Visible = True
                    Me.SearchBy = " MailTransaction.MailTypeID='KWT' and MailTransaction.MailPrintedNum = 0 and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
                    Me.SortBy = ""
                    DoBind(Me.SearchBy, Me.SortBy)
                Else
                    Dim strHTTPServer As String
                    Dim strHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        'With oCustomClass
        '    .strConnection = GetConnectionString()
        '    .WhereCond = cmdWhere
        '    .SortBy = SortBy
        'End With


        Dim DataList As New List(Of Parameter.Application)
        Dim custom As New Parameter.Application
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.Application

                custom.ApplicationID = TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                custom.AgreementNo = TempDataTable.Rows(index).Item("AgreementNo").ToString.Trim
                custom.CustomerId = TempDataTable.Rows(index).Item("CustomerID").ToString.Trim

                DataList.Add(custom)
            Next
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        oCustomClass = oController.ListAgreement(oCustomClass)

        DtUserList = oCustomClass.ListPPK
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy


        If Not DvUserList Is Nothing Then
            dtgKwitansi.DataSource = DvUserList
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        Try
            dtgKwitansi.DataBind()
        Catch
            dtgKwitansi.CurrentPageIndex = 0
            dtgKwitansi.DataBind()
        End Try
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True


        For intLoopGrid = 0 To dtgKwitansi.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgKwitansi.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim AgreementNo As String = CType(dtgKwitansi.Items(intLoopGrid).FindControl("hyAgreementNo"), HyperLink).Text.Trim
            Dim ApplicationID As String = CType(dtgKwitansi.Items(intLoopGrid).FindControl("lblApplicationID"), Label).Text.Trim
            Dim CustomerID As String = CType(dtgKwitansi.Items(intLoopGrid).FindControl("lblCustomerID"), Label).Text.Trim


            Me.ApplicationID = ApplicationID
            Me.AgreementNo = AgreementNo
            Me.CustomerID = CustomerID

            Dim query As New Parameter.Application
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next


    End Sub
#End Region

#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.SearchBy = "MailTransaction.MailPrintedNum=0"
        Else
            Me.SearchBy = "MailTransaction.MailPrintedNum > 0"
        End If

        Me.SearchBy = Me.SearchBy & "  and MailTransaction.MailTypeID='KWT' and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Replace("%", "").Trim & "%'"
        End If

        If txtAgreementDate.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and Agreement.AgreementDate='" & ConvertDate2(txtAgreementDate.Text.Trim) & "'"
        End If

        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = " MailTransaction.MailTypeID='KWT' and MailTransaction.MailPrintedNum = 0 and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        txtSearchBy.Text = ""
        cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        'BindDataPPK()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                'BindDataPPK()
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region

#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer
            Dim chkPrint As CheckBox
            Dim lblAgreementNo As HyperLink
            Dim hasil As Integer
            Dim cmdwhere As String
            Dim strMultiAgreementNo As String = ""
            Dim strMultiApplicationID As String = ""


            With oDataTable
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            End With
            For intloop = 0 To dtgKwitansi.Items.Count - 1
                chkPrint = CType(dtgKwitansi.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo = CType(dtgKwitansi.Items(intloop).Cells(1).FindControl("hyAgreementNo"), HyperLink)
                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("AgreementNo") = CType(lblAgreementNo.Text.Trim, String)
                    oRow("ApplicationID") = CType(dtgKwitansi.Items(intloop).FindControl("lblApplicationID"), Label).Text.Trim
                    oDataTable.Rows.Add(oRow)
                    If cmdwhere = "" Then
                        cmdwhere = "Agreement.ApplicationID='" & CType(oRow("ApplicationID"), String) & "'"
                    Else
                        cmdwhere = cmdwhere & " or Agreement.ApplicationID='" & CType(oRow("ApplicationID"), String) & "' "
                    End If

                    strMultiAgreementNo = strMultiAgreementNo + "'" + lblAgreementNo.Text.Trim + "',"
                    strMultiApplicationID = strMultiApplicationID + "'" + CType(oRow("ApplicationID"), String).Trim + "',"
                End If
            Next

            strMultiAgreementNo = Left(strMultiAgreementNo, (Len(strMultiAgreementNo) - 1))
            strMultiApplicationID = Left(strMultiApplicationID, (Len(strMultiApplicationID) - 1))

            If oDataTable.Rows.Count = 0 Then
                lblMessage.Text = "Harap Check Item"
                Exit Sub
            End If
            With oCustomClass
                .strConnection = GetConnectionString()
                .ListPPK = oDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With
            oCustomClass = oController.SavePrintKwitansi(oCustomClass)
            hasil = oCustomClass.hasil
            If hasil = 0 Then
                lblMessage.Text = "Cetak Kwitansi Gagal"
                Exit Sub
            Else
                cmdwhere = "(" & cmdwhere & ") and Agreement.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
                Dim cookie As HttpCookie = Request.Cookies("RptKwitansi")
                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = cmdwhere
                    cookie.Values("MultiAgreementNo") = strMultiAgreementNo.Trim
                    cookie.Values("MultiApplicationID") = strMultiApplicationID.Trim
                    cookie.Values("BranchID") = Me.sesBranchId.Replace("'", "")
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("RptKwitansi")
                    cookieNew.Values.Add("cmdwhere", cmdwhere)
                    cookieNew.Values.Add("MultiAgreementNo", strMultiAgreementNo.Trim)
                    cookieNew.Values.Add("MultiApplicationID", strMultiApplicationID.Trim)
                    cookieNew.Values.Add("BranchID", Me.sesBranchId.Replace("'", ""))
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("KwitansiViewer.aspx")
            End If
        End If
    End Sub
#End Region

    Private Sub dtgKwitansi_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgKwitansi.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link

            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

End Class