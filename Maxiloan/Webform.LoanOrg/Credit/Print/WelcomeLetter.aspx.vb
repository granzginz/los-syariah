﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class WelcomeLetter
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.PPK
    Private oController As New PPKController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property TempDataTable() As DataTable
        Get
            Return CType(ViewState("TempDataTable"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("TempDataTable") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(ViewState("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(ViewState("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            'txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            'TxtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            If Request.QueryString("strFileLocation") <> "" Then

                Dim strFileLocation As String

                strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "WelcomeLetter"
            Me.CmdWhere = "WLetterPrintNum=0 and BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "' and"
            'Me.CmdWhere += " GoLiveDate >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and GoLiveDate<= '" & ConvertDate2(TxtPeriod2.Text.Trim) & "'"
            Me.Sort = "AgreementNo ASC"
        End If
    End Sub
#End Region
#Region "BindGridEntity"
    Sub BindGridEntity()
        Dim dtEntity As New DataTable
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer = 0


        Dim DataList As New List(Of Parameter.Application)
        Dim custom As New Parameter.Application
        If Not TempDataTable Is Nothing Then
            For index = 0 To TempDataTable.Rows.Count - 1
                custom = New Parameter.Application

                custom.ApplicationID = TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                custom.AgreementNo = TempDataTable.Rows(index).Item("AgreementNo").ToString.Trim
                custom.CustomerId = TempDataTable.Rows(index).Item("CustomerID").ToString.Trim

                DataList.Add(custom)
            Next
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.Sort
        End With

        oCustomClass = oController.ListWLetter(oCustomClass)


        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.listdata
            recordCount = oCustomClass.TotalRecord
        Else
            recordCount = 0
        End If

        dtgPPK.DataSource = dtEntity.DefaultView
        dtgPPK.CurrentPageIndex = 0
        dtgPPK.DataBind()
        PagingFooter()

        pnlDtGrid.Visible = True
        pnlsearch.Visible = True


        For intLoopGrid = 0 To dtgPPK.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgPPK.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim AgreementNo As String = CType(dtgPPK.Items(intLoopGrid).FindControl("hyAgreementNo"), HyperLink).Text.Trim
            Dim ApplicationID As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblApplicationId"), Label).Text.Trim
            Dim CustomerID As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblCustomerID"), Label).Text.Trim


            Me.ApplicationID = ApplicationID
            Me.AgreementNo = AgreementNo
            Me.CustomerID = CustomerID

            Dim query As New Parameter.Application
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If

            If Not query Is Nothing Then
                chek.Checked = True
            End If
        Next
    End Sub
#End Region
    Public Function PredicateFunction(ByVal custom As Parameter.Application) As Boolean
        Return custom.ApplicationID = Me.ApplicationID And custom.CustomerId = Me.CustomerID And custom.AgreementNo = Me.AgreementNo
    End Function
    Sub BindDataPPK()
        Dim DataList As New List(Of Parameter.Application)
        Dim Application As New Parameter.Application

        If TempDataTable Is Nothing Then
            TempDataTable = New DataTable
            With TempDataTable
                .Columns.Add(New DataColumn("CustomerID", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
            End With
        Else
            For index = 0 To TempDataTable.Rows.Count - 1
                Application = New Parameter.Application

                Application.CustomerId = TempDataTable.Rows(index).Item("CustomerID").ToString.Trim
                Application.ApplicationID = TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                Application.AgreementNo = TempDataTable.Rows(index).Item("AgreementNo").ToString.Trim
                DataList.Add(Application)
            Next
        End If


        Dim oRow As DataRow

        For intLoopGrid = 0 To dtgPPK.Items.Count - 1
            Dim chek As CheckBox
            chek = CType(dtgPPK.Items(intLoopGrid).FindControl("chkPrint"), CheckBox)
            Dim AgreementNo As String = CType(dtgPPK.Items(intLoopGrid).FindControl("hyAgreementNo"), HyperLink).Text.Trim
            Dim ApplicationID As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblApplicationId"), Label).Text.Trim
            Dim CustomerID As String = CType(dtgPPK.Items(intLoopGrid).FindControl("lblCustomerID"), Label).Text.Trim


            Me.ApplicationID = ApplicationID
            Me.AgreementNo = AgreementNo
            Me.CustomerID = CustomerID

            Dim query As New Parameter.Application
            If DataList.Count > 0 Then
                query = DataList.Find(AddressOf PredicateFunction)
            Else
                query = Nothing
            End If


            If chek.Checked And query Is Nothing Then
                oRow = TempDataTable.NewRow()
                oRow("CustomerID") = CustomerID
                oRow("ApplicationID") = ApplicationID
                oRow("AgreementNo") = AgreementNo
                TempDataTable.Rows.Add(oRow)
            End If
        Next
    End Sub
    Sub BindClearPPK()
        TempDataTable = New DataTable
        With TempDataTable
            .Columns.Add(New DataColumn("CustomerID", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
        End With
    End Sub
#Region "LinkToCustomer"
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function
#End Region
#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer = 0
            Dim hasil As Integer = 0
            Dim cmdwhere As String = ""

            If TempDataTable Is Nothing Then
                BindDataPPK()
            Else
                If TempDataTable.Rows.Count = 0 Then
                    BindDataPPK()
                End If
            End If

            With oDataTable
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            End With


            If TempDataTable.Rows.Count = 0 Then
                ShowMessage(lblMessage, "Harap periksa item", True)
                Exit Sub
            End If

            For index = 0 To TempDataTable.Rows.Count - 1
                oRow = oDataTable.NewRow

                oRow("AgreementNo") = TempDataTable.Rows(index).Item("AgreementNo").ToString.Trim
                oRow("ApplicationID") = TempDataTable.Rows(index).Item("ApplicationID").ToString.Trim
                oDataTable.Rows.Add(oRow)
                If cmdwhere = "" Then
                    cmdwhere = "AG.ApplicationID='" & CType(oRow("ApplicationID"), String) & "'"
                Else
                    cmdwhere += " or AG.ApplicationID='" & CType(oRow("applicationID"), String) & "' "
                End If
                Me.ApplicationID = CType(oRow("ApplicationID"), String)
            Next

            With oCustomClass
                .strConnection = GetConnectionString()
                .ListWLetter = oDataTable
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
                .BranchId = Replace(Me.sesBranchId, "'", "")
            End With
            oCustomClass = oController.SavePrintWLetter(oCustomClass)

            hasil = oCustomClass.hasil

            If hasil = 0 Then
                ShowMessage(lblMessage, "Gagal", True)
                Exit Sub
            Else
                ApplicationID = Me.ApplicationID
                Dim cookie As HttpCookie = Request.Cookies("RptWLetter")
                If Not cookie Is Nothing Then
                    cookie.Values("ApplicationID") = ApplicationID
                    cookie.Values("BusinessDate") = Me.BusinessDate.ToString("dd/MM/yyyy")
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("RptWLetter")
                    cookieNew.Values.Add("ApplicationID", ApplicationID)
                    cookieNew.Values.Add("BusinessDate", Me.BusinessDate.ToString("dd/MM/yyyy"))
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("WelcomeLetterViewer.aspx")
            End If
        End If
    End Sub
#End Region
#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.CmdWhere = " WLetterPrintNum=0 and BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        Else
            Me.CmdWhere = " WLetterPrintNum > 0  and BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        End If


        Dim SearchTemp As String

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            SearchTemp = cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
            Me.CmdWhere = Me.CmdWhere & " and " & SearchTemp
        End If


        Me.Sort = "AgreementNo ASC"
        BindGridEntity()
    End Sub
#End Region
#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        'txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        'TxtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

        Me.CmdWhere = " WLetterPrintNum=0 and BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'"
        'Me.CmdWhere = Me.CmdWhere & " and  GoLiveDate >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and GoLiveDate<= '" & ConvertDate2(TxtPeriod2.Text.Trim) & "'"

        Me.Sort = "AgreementNo ASC"
        BindClearPPK()
        BindGridEntity()
        txtSearchBy.Text = ""
        cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub
#End Region
#Region "dtgPPK_ItemDataBound"
    Private Sub dtgPPK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPPK.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblCustomerName As New HyperLink
            Dim lblCustomerID As New Label
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindDataPPK()
        BindGridEntity()
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindDataPPK()
                BindGridEntity()
            End If
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPPK.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindDataPPK()
        BindGridEntity()
    End Sub
#End Region
    'Sub BindValidatorDate()
    '    Dim generalSet As New Parameter.GeneralSetting
    '    Dim conGeneral As New GeneralSettingController
    '    Dim value As Integer = 0
    '    Dim status As Boolean = True

    '    generalSet.strConnection = GetConnectionString()
    '    generalSet.GSID = "CUTOFFSPBIAYA"

    '    generalSet = conGeneral.GetGeneralSettingByID(generalSet)

    '    If generalSet.ListData.Rows.Count > 0 Then
    '        value = CInt(generalSet.ListData.Rows(0).Item("GSValue").ToString.Trim)
    '    End If


    '    If txtPeriod1.Text <> "" Or TxtPeriod2.Text <> "" Then
    '        Dim dateTxt As Date = Date.ParseExact(txtPeriod1.Text.Trim, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
    '        Dim dateTxt1 As Date = Date.ParseExact(TxtPeriod2.Text.Trim, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
    '        Dim dateMin As Date = CDate(Me.BusinessDate).AddMonths(-value)
    '        Dim dateBus As Date = CDate(Me.BusinessDate)


    '        If Not (dateTxt >= dateMin) Or Not (dateBus >= dateTxt) Then
    '            ShowMessage(lblMessage, "Pilih tanggal dari " & dateMin.ToString("dd/MM/yyyy") & " s/d " & Me.BusinessDate.ToString("dd/MM/yyyy"), True)
    '            status = False
    '        Else
    '            lblMessage.Visible = False
    '        End If

    '        If Not (dateTxt1 >= dateMin) Or Not (dateBus >= dateTxt1) Then
    '            ShowMessage(lblMessage, "Pilih tanggal dari " & dateMin.ToString("dd/MM/yyyy") & " s/d " & Me.BusinessDate.ToString("dd/MM/yyyy"), True)
    '        Else
    '            If status Then
    '                lblMessage.Visible = False
    '            End If

    '        End If
    '    Else
    '        lblMessage.Visible = False
    '    End If
    'End Sub

End Class