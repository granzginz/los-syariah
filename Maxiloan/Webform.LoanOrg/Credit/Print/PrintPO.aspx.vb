﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PrintPO
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtPoDate As ucDateCE


#Region "Constanta"
    Private oCustomClass As New Parameter.PPK
    Private oController As New PPKController
#End Region

#Region "Property"
    Private Property ModePrint() As Boolean
        Get
            Return CType(viewstate("ModePrint"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("ModePrint") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If SessionInvalid() Then
                Exit Sub
            End If

            txtPoDate.Text = ""
            'oPODate.ValidationErrMessage = "Harap isi Tanggal PO dengan format dd/MM/yyyy"
            'oPODate.DataBind()

            If Request.QueryString("strFileLocation") <> "" Then
                Dim strFileLocation As String

                strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  " & vbCrLf _
                & " window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If
            Me.FormID = "PrintPO"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                    pnlsearch.Visible = True
                    'pnlDtGrid.Visible = True
                    Me.SearchBy = " MailTransaction.MailTypeID='SPPO' and MailTransaction.MailPrintedNum = 0 and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
                    Me.SortBy = ""
                    DoBind(Me.SearchBy, Me.SortBy)
                Else
                    Dim strHTTPServer As String
                    Dim strHTTPApp As String
                    Dim strNameServer As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/error_notauthorized.aspx")
                End If
            End If
        End If
    End Sub

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .SortBy = SortBy
            .CurrentPage = 1
            .PageSize = 1000
        End With

        oCustomClass = oController.ListAgreement(oCustomClass)

        DtUserList = oCustomClass.ListPPK
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgPPK.DataSource = DvUserList

        Try
            dtgPPK.DataBind()
        Catch
            dtgPPK.CurrentPageIndex = 0
            dtgPPK.DataBind()
        End Try
        'pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        lblMessage.Text = ""
    End Sub
#End Region

#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", "MAXILOAN") Then
            Dim oDataTable As New DataTable
            Dim oRow As DataRow
            Dim intloop As Integer
            Dim chkPrint As CheckBox
            Dim lblAgreementNo As HyperLink
            Dim hasil As Integer
            Dim cmdwhere As String
            With oDataTable
                .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
                .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
            End With
            For intloop = 0 To dtgPPK.Items.Count - 1
                chkPrint = CType(dtgPPK.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
                lblAgreementNo = CType(dtgPPK.Items(intloop).Cells(1).FindControl("hyAgreementNo"), HyperLink)
                If chkPrint.Checked Then
                    oRow = oDataTable.NewRow
                    oRow("AgreementNo") = CType(lblAgreementNo.Text.Trim, String)
                    oRow("ApplicationID") = CType(dtgPPK.Items(intloop).FindControl("lblApplicationID"), Label).Text.Trim
                    oDataTable.Rows.Add(oRow)
                    If cmdwhere = "" Then
                        cmdwhere = "Agreement.ApplicationID='" & CType(oRow("ApplicationID"), String) & "'"
                    Else
                        cmdwhere = cmdwhere & " or Agreement.ApplicationID='" & CType(oRow("applicationID"), String) & "' "
                    End If
                End If
            Next
            If oDataTable.Rows.Count = 0 Then
                lblMessage.Text = "Harap periksa Item"
                Exit Sub
            End If
            With oCustomClass
                .strConnection = GetConnectionString()
                .ListPPK = oDataTable
                .BranchId = Replace(Me.sesBranchId, "'", "")
                .BusinessDate = Me.BusinessDate
                .LoginId = Me.Loginid
            End With
            oCustomClass = oController.SavePrintPO(oCustomClass)
            hasil = oCustomClass.hasil
            If hasil = 0 Then
                lblMessage.Text = "Cetak Gagal"
                Exit Sub
            Else
                cmdwhere = "(" & cmdwhere & ") and Agreement.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
                Dim cookie As HttpCookie = Request.Cookies("RptPO")
                If Not cookie Is Nothing Then
                    cookie.Values("cmdwhere") = cmdwhere
                    'cookie.Values("namaTTD") = txtnameSingnature.Text.Trim
                    'cookie.Values("JabatanTTD") = txtjabatan.Text.Trim
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("RptPO")
                    cookieNew.Values.Add("cmdwhere", cmdwhere)
                    'cookieNew.Values.Add("namaTTD", txtnameSingnature.Text.Trim)
                    'cookieNew.Values.Add("JabatanTTD", txtjabatan.Text.Trim)
                    Response.AppendCookie(cookieNew)
                End If
                Me.ModePrint = True
                'DoBind(Me.SearchBy, Me.SortBy)
               
                Response.Redirect("PrintPOViewer.aspx")
            End If
        End If
    End Sub
#End Region

#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If cboPrinted.SelectedItem.Value = "No" Then
            Me.SearchBy = "MailTransaction.MailPrintedNum=0"
        Else
            Me.SearchBy = "MailTransaction.MailPrintedNum > 0"
        End If
        Me.SearchBy = Me.SearchBy & "  and MailTransaction.MailTypeID='SPPO' and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
        End If

        If txtPoDate.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " and PODate='" & ConvertDate2(txtPoDate.Text.Trim) & "'"
        End If

        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Me.SearchBy = " MailTransaction.MailTypeID='SPPO' and MailTransaction.MailPrintedNum = 0 and MailTransaction.BranchID='" & Replace(Me.sesBranchId, "'", "") & "'"
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        txtSearchBy.Text = ""
        cboPrinted.ClearSelection()
        cboSearchBy.ClearSelection()
    End Sub
#End Region

    Private Sub dtgPPK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPPK.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        btnPrint.Visible = True
        btnNext.Visible = False
        'pnlParam.Visible = True
    End Sub
End Class