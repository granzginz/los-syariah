﻿#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
#End Region


Public Class SKPotongGajiViewer
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property MultiApplicationID() As String
        Get
            Return CType(ViewState("MultiApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("MultiApplicationID") = Value
        End Set
    End Property
#End Region
#Region "Constanta"
    Private oController As New PPKController
    Private oParameter As New Parameter.PPK
#End Region

#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        bindReport()
    End Sub
#End Region

#Region "BindReport"
    Private Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oDataSet2 As New DataSet
        Dim oReport As SKPotongGajiprint = New SKPotongGajiprint

        With oParameter
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .MultiApplicationID = Me.MultiApplicationID
            .SortBy = Me.SortBy
        End With

        oParameter = oController.ListReportSKPotongGaji(oParameter)
        oDataSet = oParameter.ListReport

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = Me.CmdWhere
        oParameter.BranchId = Me.sesBranchId.Replace("'", "")
        oParameter.LoginId = Me.Loginid
        oParameter = oController.ListReportSKPotongGaji(oParameter)
        oDataSet = oParameter.ListReport

        oReport.SetDataSource(oDataSet.Tables(0))

        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.Visible = True
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_SKPotongGaji.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions =
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("SKPotongGaji.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_SKPotongGaji")

    End Sub

    Function GetCompanyName() As String
        Dim result As String
        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If
        Return result
    End Function
#End Region

#Region "GetCookies"
    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("RptSKPotongGaji")
        Me.CmdWhere = cookie.Values("cmdwhere")
        Me.MultiApplicationID = cookie.Values("ApplicationID")
        Me.SortBy = cookie.Values("sortby")
    End Sub
#End Region

End Class