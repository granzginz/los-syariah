﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class KelengkapanData
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents txtPeriod1 As ucDateCE
    Protected WithEvents txtPeriod2 As ucDateCE
#Region "Constanta"
    Private oCustomClass As New Parameter.PPK
    Private oController As New PPKController
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property CmdWherePribadi() As String
        Get
            Return CType(ViewState("CmdWherePribadi"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWherePribadi") = Value
        End Set
    End Property
    Private Property CmdWhereCompany() As String
        Get
            Return CType(ViewState("CmdWhereCompany"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhereCompany") = Value
        End Set
    End Property
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            TxtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            GetCookiesCompany()
            GetCookiesPribadi()

            If Request.QueryString("strFileLocation") <> "" Or Request.QueryString("strFileLocation1") <> "" Then

                Dim strFileLocation As String
                strFileLocation = "../../../XML/" & Request.QueryString("strFileLocation") & ".pdf"

                Dim strFileLocation1 As String
                strFileLocation1 = "../../../XML/" & Request.QueryString("strFileLocation1") & ".pdf"


                If Me.CmdWherePribadi.Trim <> "" And Me.CmdWhereCompany <> "" Then
                    Response.Write("<script language = javascript>" & vbCrLf _
                           & "window.open('" & strFileLocation & "')" & vbCrLf _
                           & "window.open('" & strFileLocation1 & "')" & vbCrLf _
                           & "</script>")
                Else

                    If Me.CmdWhereCompany.Trim <> "" Then
                        Response.Write("<script language = javascript>" & vbCrLf _                           
                           & "window.open('" & strFileLocation1 & "')" & vbCrLf _
                           & "</script>")
                    Else
                        Response.Write("<script language = javascript>" & vbCrLf _
                                   & "window.open('" & strFileLocation & "')" & vbCrLf _                                   
                                   & "</script>")
                    End If

                End If


                
            End If

            Me.FormID = "KelengkapanData"
            Me.CmdWhere = "BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
            Me.CmdWhere = Me.CmdWhere & " and  GoLiveDate >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and GoLiveDate<= '" & ConvertDate2(TxtPeriod2.Text.Trim) & "'"

            Me.SortBy = "AgreementNo ASC"
            DoBind(Me.CmdWhere, Me.SortBy)
        End If
    End Sub
#End Region
#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView

        If cmdWhere.Trim = "" Then
            cmdWhere = ""
        End If

        With oCustomClass
            .strConnection = GetConnectionString()
            .WhereCond = Me.CmdWhere
            .SortBy = SortBy
        End With

        oCustomClass = oController.ListKelengkapanData(oCustomClass)

        DtUserList = oCustomClass.ListKelengkapanData
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgPPK.DataSource = DvUserList

        Try
            dtgPPK.DataBind()
        Catch
            dtgPPK.CurrentPageIndex = 0
            dtgPPK.DataBind()
        End Try
        pnlDtGrid.Visible = True
        pnlsearch.Visible = True
        lblMessage.Text = ""
    End Sub
#End Region
#Region "LinkToCustomer"
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenCustomer('" & strStyle & "','" & strCustomerID & "')"
    End Function
#End Region
#Region "Print"
    Private Sub BtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnprint.Click
        Dim oDataTable As New DataTable
        Dim oDataTablePribadi As Integer = 0
        Dim oDataTableCompany As Integer = 0
        Dim cmdwherePribadi As String = ""
        Dim cmdwhereCompany As String = ""

        With oDataTable
            .Columns.Add(New DataColumn("AgreementNo", GetType(String)))
            .Columns.Add(New DataColumn("ApplicationID", GetType(String)))
        End With

        SplitDataTable(oDataTable, oDataTablePribadi, oDataTableCompany, cmdwherePribadi, cmdwhereCompany)

        If oDataTable.Rows.Count = 0 Then
            lblMessage.Text = "Harap Check Item"
            Exit Sub
        End If


        If oDataTableCompany > 0 Then
            cmdwhereCompany = " AG.ApplicationID in (" & cmdwhereCompany & ")"
            cmdwhereCompany = cmdwhereCompany & " and AG.BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'"

            Dim cookie As HttpCookie = Request.Cookies("RptKelengkapanDataCompany")

            If Not cookie Is Nothing Then
                cookie.Values("cmdwhereCompany") = cmdwhereCompany
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptKelengkapanDataCompany")
                cookieNew.Values.Add("cmdwhereCompany", cmdwhereCompany)
                Response.AppendCookie(cookieNew)
            End If
        Else
            Dim cookie As HttpCookie = Request.Cookies("RptKelengkapanDataCompany")

            If Not cookie Is Nothing Then
                cookie.Values("cmdwhereCompany") = ""
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptKelengkapanDataCompany")
                cookieNew.Values.Add("cmdwhereCompany", "")
                Response.AppendCookie(cookieNew)
            End If
        End If

        If oDataTablePribadi > 0 Then
            cmdwherePribadi = " AG.ApplicationID in (" & cmdwherePribadi & ")"
            cmdwherePribadi = cmdwherePribadi & " and AG.BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'"

            Dim cookie As HttpCookie = Request.Cookies("RptKelengkapanDataPribadi")

            If Not cookie Is Nothing Then
                cookie.Values("cmdwherePribadi") = cmdwherePribadi
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptKelengkapanDataPribadi")
                cookieNew.Values.Add("cmdwherePribadi", cmdwherePribadi)
                Response.AppendCookie(cookieNew)
            End If
        Else

            Dim cookie As HttpCookie = Request.Cookies("RptKelengkapanDataPribadi")

            If Not cookie Is Nothing Then
                cookie.Values("cmdwherePribadi") = ""
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("RptKelengkapanDataPribadi")
                cookieNew.Values.Add("cmdwherePribadi", "")
                Response.AppendCookie(cookieNew)
            End If
        End If

        Response.Redirect("KelengkapanDataViewer.aspx")
    End Sub
    Sub SplitDataTable(ByVal data As DataTable, ByRef dataPribadi As Integer, ByRef dataCompany As Integer, ByRef cmdwherePribadi As String, ByRef cmdwhereCompany As String)
        Dim oRow As DataRow
        Dim intloop As Integer
        Dim chkPrint As CheckBox
        Dim lblAgreementNo As HyperLink
        Dim lblCustomerType As Label

        For intloop = 0 To dtgPPK.Items.Count - 1
            chkPrint = CType(dtgPPK.Items(intloop).Cells(0).FindControl("chkPrint"), CheckBox)
            lblAgreementNo = CType(dtgPPK.Items(intloop).Cells(1).FindControl("hyAgreementNo"), HyperLink)
            lblCustomerType = CType(dtgPPK.Items(intloop).FindControl("lblCustomerType"), Label)

            If chkPrint.Checked Then
                oRow = data.NewRow
                oRow("AgreementNo") = CType(lblAgreementNo.Text.Trim, String)
                oRow("ApplicationID") = CType(dtgPPK.Items(intloop).FindControl("lblApplicationID"), Label).Text.Trim
                data.Rows.Add(oRow)


                If lblCustomerType.Text.Trim = "C" Then
                    If cmdwhereCompany.Trim = "" Then
                        cmdwhereCompany = "'" & CType(oRow("ApplicationID"), String) & "'"
                    Else
                        cmdwhereCompany = cmdwhereCompany & ",'" & CType(oRow("ApplicationID"), String) & "'"
                    End If
                    dataCompany = dataCompany + 1
                Else
                    If cmdwherePribadi.Trim = "" Then
                        cmdwherePribadi = "'" & CType(oRow("ApplicationID"), String) & "'"
                    Else
                        cmdwherePribadi = cmdwherePribadi & ",'" & CType(oRow("ApplicationID"), String) & "'"
                    End If
                    dataPribadi = dataPribadi + 1
                End If

            End If
        Next
    End Sub
#End Region
#Region "SortGrid"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "Search"
    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        Dim SearchTemp As String
        If txtPeriod1.Text.Trim <> "" And txtPeriod2.Text.Trim <> "" Then
            Me.CmdWhere = "BranchID='" & Me.sesBranchId.Replace("'", "").Trim & "'" & " and DateEntryApplicationData BETWEEN '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " AND  '" & ConvertDate2(txtPeriod2.Text.Trim) & "'"
        End If

        If cboSearchBy.SelectedItem.Value <> "" And txtSearchBy.Text <> "" Then
            SearchTemp = cboSearchBy.SelectedItem.Value.Trim & " LIKE '%" & txtSearchBy.Text.Trim & "%'"
            Me.CmdWhere = Me.CmdWhere & "  and " & SearchTemp
        End If


        Me.SortBy = "AgreementNo ASC"
        DoBind(Me.CmdWhere, Me.SortBy)
    End Sub
#End Region
#Region "Reset"
    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnreset.Click
        txtPeriod1.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        txtPeriod2.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        Me.CmdWhere = "BranchID='" & Me.sesBranchId.Replace("'", "") & "'"
        Me.CmdWhere = Me.CmdWhere & " and  GoLiveDate >= '" & ConvertDate2(txtPeriod1.Text.Trim) & "'" & " and GoLiveDate<= '" & ConvertDate2(txtPeriod2.Text.Trim) & "'"

        Me.SortBy = "AgreementNo ASC"
        DoBind(Me.CmdWhere, Me.SortBy)
        txtSearchBy.Text = ""
        cboSearchBy.ClearSelection()
    End Sub
#End Region
#Region "dtgPPK_ItemDataBound"
    Private Sub dtgPPK_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPPK.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim hyTemp As HyperLink
            Dim lblCustomerName As New HyperLink
            Dim lblCustomerID As New Label
            Dim lblApplicationID As New Label

            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblCustomerName = CType(e.Item.FindControl("lblCustomerName"), HyperLink)
            lblApplicationID = CType(e.Item.FindControl("lblApplicationID"), Label)
            lblCustomerName.NavigateUrl = LinkToCustomer(lblCustomerID.Text.Trim, "AccAcq")
            '*** Agreement No link
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "AccAcq" & "', '" & Server.UrlEncode(lblApplicationID.Text.Trim) & "')"
        End If
    End Sub
#End Region
#Region "GetCookiesPribadi"
    Sub GetCookiesPribadi()
        Dim cookie As HttpCookie = Request.Cookies("RptKelengkapanDataPribadi")

        If cookie IsNot Nothing Then
            Me.CmdWherePribadi = cookie.Values("cmdwherePribadi")
        Else
            Me.CmdWherePribadi = ""
        End If

    End Sub
#End Region
#Region "GetCookiesCompany"
    Sub GetCookiesCompany()
        Dim cookie As HttpCookie = Request.Cookies("RptKelengkapanDataCompany")

        If cookie IsNot Nothing Then
            Me.CmdWhereCompany = cookie.Values("cmdwhereCompany")
        Else
            Me.CmdWhereCompany = ""
        End If

    End Sub
#End Region
End Class