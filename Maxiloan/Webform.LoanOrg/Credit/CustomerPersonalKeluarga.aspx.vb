﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController

#End Region
Public Class CustomerPersonalKeluarga
    Inherits AbsCustomerPersonal

#Region "uc control declaration"
    
    Protected WithEvents ucDateCE As ucDateCE
   
#End Region
#Region "Constanta"
  
    Const txtHargaRumahText As String = "0"
    Const txtKeteranganUsahaText As String = ""
    Const txtOBTypeText As String = ""
    Const cboOBIndustryTypeValue As String = ""
    Const txtOBJobTitleText As String = ""
    Const txtOBSinceYearText As String = ""

#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"

            If Me.ProspectAppID <> "-" And Request("prospectappid") <> "" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With

                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)

            End If

            Me.PageAddEdit = Request("page")

            If Me.PageAddEdit = "Add" Then
                lblTitle.Text = "ADD"
                GetXML()
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                AddRecord()
                AddRecord()
                AddRecord()
                AddRecord()
                AddRecord()
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)
                Dim oCust As New Parameter.Customer
                oCust.CustomerID = Me.CustomerID
                oCust.CustomerType = "p"
                oCust.strConnection = GetConnectionString()
                oCust = m_controller.GetCustomerEdit(oCust, "9")

                If oCust.listdata.Rows.Count = 0 Then
                    AddRecord()
                Else
                    EditRecord(Me.CustomerID)
                End If
                cTabs.SetNavigateUrl(Request("page"), Request("id"))
              
            End If
            cTabs.RefreshAttr(Request("pnl"))
            If Me.StatusPerkawinan.ToUpper.Trim <> "M" Then
                cTabs.EnabledLink("tabPasangan", False)
            End If

            If Me.PageAddEdit <> "Add" Then
                Dim intTotalCustomer As Integer
                intTotalCustomer = m_controller.GetTotalCustomer(Me.CustomerID.Trim, GetConnectionString)
               
            End If
             
        End If
    End Sub
#End Region
     
#Region "FillCbo"
    'Sub FillCbo(ByVal table As String)
    '    Dim dtEntity As DataTable
    '    Dim oCustomer As New Parameter.Customer
    '    oCustomer.strConnection = GetConnectionString()
    '    oCustomer.Table = table
    '    oCustomer = m_controller.CustomerPersonalAdd(oCustomer)
    '    If Not oCustomer Is Nothing Then
    '        dtEntity = oCustomer.listdata
    '    End If
    '    Select Case table           
    '            'Case "tblReference"
    '            '    cboReference.DataSource = dtEntity.DefaultView
    '            '    cboReference.DataTextField = "Description"
    '            '    cboReference.DataValueField = "ID"
    '            '    cboReference.DataBind()
    '            '    cboReference.Items.Insert(0, "Select One")
    '            '    cboReference.Items(0).Value = "Select One"
    '    End Select
    'End Sub
    Sub fillCboRelationship()
        Dim dtEntity As DataTable
        Dim oCustomer As New Parameter.Customer
        Dim intLoop As Integer
        Dim cboFamilyRelationship As DropDownList

        oCustomer.strConnection = GetConnectionString()
        oCustomer.Table = "tblFamily"
        oCustomer = m_controller.CustomerPersonalAdd(oCustomer)

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        For intLoop = 0 To Datagrid1.Items.Count - 1
            cboFamilyRelationship = CType(Datagrid1.Items(intLoop).Cells(4).FindControl("cboFamilyRelationship"), DropDownList)
            cboFamilyRelationship.DataSource = dtEntity.DefaultView
            cboFamilyRelationship.DataTextField = "Description"
            cboFamilyRelationship.DataValueField = "ID"
            cboFamilyRelationship.DataBind()
            cboFamilyRelationship.Items.Insert(0, "Select One")
            cboFamilyRelationship.Items(0).Value = "Select One"
        Next
    End Sub
#End Region
#Region "Add-Delete Baris"
    Private Sub imbPAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPAdd.Click
        AddRecord()
    End Sub
    Private Sub AddRecord()
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo As New Label
        Dim txtName As New TextBox
        Dim txtIDNumber As New TextBox
        'Dim uscBirthDate As TextBox
        Dim cboRelationship As New DropDownList
        Dim plus As Integer = 0
        Dim ucTglLahir As ucDateCE


        With objectDataTable
            .Columns.Add(New DataColumn("No", GetType(String)))
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("IDNumber", GetType(String)))
            .Columns.Add(New DataColumn("BirthDate", GetType(String)))
            .Columns.Add(New DataColumn("Relationship", GetType(String)))
        End With

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            lblNo = CType(Datagrid1.Items(intLoopGrid).Cells(0).FindControl("lblNo"), Label)
            txtName = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtFamilyName"), TextBox)
            txtIDNumber = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("txtFamilyIDNumber"), TextBox)
            ucTglLahir = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uctgllahir"), ucDateCE)
            'uscBirthDate = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uscFamilyBirthDate"), TextBox)
            cboRelationship = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("cboFamilyRelationship"), DropDownList)
            '----- Add row -------'
            oRow = objectDataTable.NewRow()
            oRow("No") = CType(lblNo.Text, String)
            oRow("Name") = CType(txtName.Text, String)
            oRow("IDNumber") = CType(txtIDNumber.Text, String)
            oRow("BirthDate") = CType(ucTglLahir.Text, String)
            oRow("Relationship") = CType(cboRelationship.SelectedValue, String)
            objectDataTable.Rows.Add(oRow)


        Next

        'If Datagrid1.Items.Count = 0 Then
        '    'add data pasangan
        '    oRow = objectDataTable.NewRow()
        '    oRow("No") = Datagrid1.Items.Count + 1
        '    oRow("Name") = Me.NamaPasangan
        '    oRow("IDNumber") = Me.IDNumber
        '    oRow("BirthDate") = Me.BirthDate
        '    oRow("Relationship") = "SP"
        '    objectDataTable.Rows.Add(oRow)

        '    plus += 1
        'End If

        oRow = objectDataTable.NewRow()
        oRow("No") = Datagrid1.Items.Count + 1 + plus
        oRow("Name") = ""
        oRow("IDNumber") = ""
        oRow("BirthDate") = ""
        oRow("Relationship") = ""

        objectDataTable.Rows.Add(oRow)
        Datagrid1.DataSource = objectDataTable
        Datagrid1.DataBind()

        fillCboRelationship()

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            lblNo = CType(Datagrid1.Items(intLoopGrid).Cells(0).FindControl("lblNo"), Label)
            txtName = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtFamilyName"), TextBox)
            txtIDNumber = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("txtFamilyIDNumber"), TextBox)
            'uscBirthDate = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uscFamilyBirthDate"), TextBox)
            ucTglLahir = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("ucTglLahir"), ucDateCE)
            cboRelationship = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("cboFamilyRelationship"), DropDownList)
            lblNo.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim
            txtName.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            txtIDNumber.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            'uscBirthDate.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            ucTglLahir.Text = objectDataTable.Rows(intLoopGrid).Item(3).ToString.Trim
            If objectDataTable.Rows(intLoopGrid).Item(4).ToString <> "" Then
                cboRelationship.Items.FindByValue(objectDataTable.Rows(intLoopGrid).Item(4).ToString).Selected = True
            End If
        Next
    End Sub
    Sub DeleteBaris(ByVal Index As Integer)
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo As New Label
        Dim txtName As New TextBox
        Dim txtIDNumber As New TextBox
        'Dim uscBirthDate As TextBox
        Dim uscBirthDate As ucDateCE
        Dim cboRelationship As New DropDownList
        Dim imbDelete As New ImageButton
        Dim oNewDataTable As New DataTable
        With oNewDataTable
            .Columns.Add(New DataColumn("Name", GetType(String)))
            .Columns.Add(New DataColumn("IDNumber", GetType(String)))
            .Columns.Add(New DataColumn("BirthDate", GetType(String)))
            .Columns.Add(New DataColumn("Relationship", GetType(String)))
        End With

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            txtName = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtFamilyName"), TextBox)
            txtIDNumber = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("txtFamilyIDNumber"), TextBox)
            'uscBirthDate = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uscFamilyBirthDate"), TextBox)
            uscBirthDate = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uctgllahir"), ucDateCE)
            cboRelationship = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("cboFamilyRelationship"), DropDownList)
            '----- Add row -------'
            If intLoopGrid <> Index Then
                oRow = oNewDataTable.NewRow()
                oRow("Name") = CType(txtName.Text, String)
                oRow("IDNumber") = CType(txtIDNumber.Text, String)
                oRow("BirthDate") = CType(uscBirthDate.Text, String)
                oRow("Relationship") = CType(cboRelationship.SelectedValue, String)
                oNewDataTable.Rows.Add(oRow)
            End If
        Next
        Datagrid1.DataSource = oNewDataTable
        Datagrid1.DataBind()
        fillCboRelationship()

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            lblNo = CType(Datagrid1.Items(intLoopGrid).Cells(0).FindControl("lblNo"), Label)
            txtName = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtFamilyName"), TextBox)
            txtIDNumber = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("txtFamilyIDNumber"), TextBox)
            'uscBirthDate = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uscFamilyBirthDate"), TextBox)
            uscBirthDate = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uctgllahir"), ucDateCE)
            cboRelationship = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("cboFamilyRelationship"), DropDownList)
            lblNo.Text = (intLoopGrid + 1).ToString
            txtName.Text = oNewDataTable.Rows(intLoopGrid).Item("Name").ToString.Trim
            txtIDNumber.Text = oNewDataTable.Rows(intLoopGrid).Item("IDNumber").ToString.Trim
            uscBirthDate.Text = oNewDataTable.Rows(intLoopGrid).Item("BirthDate").ToString.Trim
            If oNewDataTable.Rows(intLoopGrid).Item("Relationship").ToString <> "" Then
                cboRelationship.Items.FindByValue(oNewDataTable.Rows(intLoopGrid).Item("Relationship").ToString).Selected = True
            End If
        Next
    End Sub
#End Region
#Region "imbPCancel_Click"
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPCancel.Click
        If File.Exists(gStrPath & gStrFileName) Then
            File.Delete(gStrPath & gStrFileName)
        End If
        Response.Redirect("Customer.aspx")
    End Sub
#End Region
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPSave.Click
        Try
            Dim ocustomer As New Parameter.Customer
            Dim intLoop As Integer
            Dim oDataFamily As New DataTable
            Dim objrow As DataRow
            Dim FamilyName, FamilyIDNumber, FamilyBirthDate, FamilyRelation As String
            Dim No As Integer = 1
            Dim ocTglLahir As New ucDateCE

            oDataFamily.Columns.Add("FamilySeqNo", GetType(String))
            oDataFamily.Columns.Add("FamilyName", GetType(String))
            oDataFamily.Columns.Add("FamilyIDNumber", GetType(String))
            oDataFamily.Columns.Add("FamilyBirthDate", GetType(String))
            oDataFamily.Columns.Add("FamilyRelation", GetType(String))

            For intLoop = 0 To Datagrid1.Items.Count - 1
                FamilyName = CType(Datagrid1.Items(intLoop).Cells(1).FindControl("txtFamilyName"), TextBox).Text
                FamilyIDNumber = CType(Datagrid1.Items(intLoop).Cells(2).FindControl("txtFamilyIDNumber"), TextBox).Text
                FamilyBirthDate = CType(Datagrid1.Items(intLoop).Cells(3).FindControl("uctgllahir"), ucDateCE).Text
                FamilyRelation = CType(Datagrid1.Items(intLoop).Cells(4).FindControl("cboFamilyRelationship"), DropDownList).SelectedValue

                If FamilyName.Trim <> "" And FamilyIDNumber.Trim <> "" And FamilyBirthDate.Trim <> "" And FamilyRelation.Trim <> "Select One" Then
                    objrow = oDataFamily.NewRow
                    objrow("FamilySeqNo") = No
                    objrow("FamilyName") = FamilyName
                    objrow("FamilyIDNumber") = FamilyIDNumber
                    objrow("FamilyBirthDate") = ConvertDate(FamilyBirthDate)
                    objrow("FamilyRelation") = FamilyRelation
                    oDataFamily.Rows.Add(objrow)
                    No += 1
                    If (CInt(ConvertDate(FamilyBirthDate)) >= CInt(Format(Me.BusinessDate, "yyyyMMdd"))) Then
                        CType(Datagrid1.Items(intLoop).Cells(3).FindControl("lblVFamilyBirthDate0"), Label).Visible = True
                        If Status <> False Then
                            Status = False
                        End If
                    End If

                    If FamilyName.Trim <> "" Then
                        If FamilyIDNumber.Trim <> "" Then
                            CType(Datagrid1.Items(intLoop).Cells(2).FindControl("lblVFamilyIDNumber"), Label).Visible = False
                        End If
                        If FamilyRelation.Trim <> "Select One" Then
                            CType(Datagrid1.Items(intLoop).Cells(4).FindControl("lblValidFRelationship"), Label).Visible = False
                        End If
                        If FamilyBirthDate <> "" Then
                            CType(Datagrid1.Items(intLoop).Cells(3).FindControl("lblVFamilyBirthDate"), Label).Visible = False
                        End If
                    End If
                    Status = True
                Else

                    ''test data kosong
                    If FamilyName.Trim = "" Then
                        CType(Datagrid1.Items(intLoop).Cells(2).FindControl("lblValidFName"), Label).Visible = True
                        If Status <> False Then
                            Status = False
                        End If
                        If FamilyIDNumber.Trim = "" Then
                            CType(Datagrid1.Items(intLoop).Cells(2).FindControl("lblVFamilyIDNumber"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                        End If
                        If FamilyRelation.Trim = "Select One" Then
                            CType(Datagrid1.Items(intLoop).Cells(4).FindControl("lblValidFRelationship"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                        End If
                        If FamilyBirthDate = "" Then
                            CType(Datagrid1.Items(intLoop).Cells(3).FindControl("lblVFamilyBirthDate"), Label).Visible = True
                            If Status <> False Then
                                Status = False
                            End If
                        End If
                    End If
                End If


                If FamilyName.Trim <> "" Then
                    If FamilyIDNumber.Trim = "" Then
                        CType(Datagrid1.Items(intLoop).Cells(2).FindControl("lblVFamilyIDNumber"), Label).Visible = True
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                    If FamilyRelation.Trim = "Select One" Then
                        CType(Datagrid1.Items(intLoop).Cells(4).FindControl("lblValidFRelationship"), Label).Visible = True
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                    If FamilyBirthDate = "" Then
                        CType(Datagrid1.Items(intLoop).Cells(3).FindControl("lblVFamilyBirthDate"), Label).Visible = True
                        If Status <> False Then
                            Status = False
                        End If
                    End If
                End If



            Next
            If Me.StatusPerkawinan.ToUpper.Trim <> "M" Then
                Status = True
            End If

            If Status = False Then
                Exit Sub
            End If

            Dim Err As String = ""
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID
            Err = m_controller.PersonalCustomerKeluargaSaveEdit(ocustomer, _
                                                oDataFamily)
            If Err = "" Then
                ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If

            'Response.Redirect("Customer.aspx")
            Response.Redirect("CustomerPersonalPinjamanLainnya.aspx?page=" + Request("page") + "&id=" + Request("id") + "&pnl=tabPinjamanLainnya")
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
    End Sub
#End Region
#Region "Edit"
    Sub BindEdit(ByVal ID As String)
        Dim dtEntity As New DataTable
        Dim dtOmset As New DataTable
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = ID
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "8")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If

        oRow = dtEntity.Rows(0)
        Me.Name = oRow("Name").ToString.Trim        

        Me.IDType = oRow("IDType").ToString.Trim
        Me.IDTypeName = oRow("IDTypeName").ToString.Trim
        Me.IDNumber = oRow("IDNumber").ToString.Trim
        Me.Gender = oRow("Gender").ToString.Trim
        If IsDate(oRow("IDExpiredDate").ToString) Then Me.IDExpiredDate = Format(oRow("IDExpiredDate"), "dd/MM/yyyy")

        If Not IsDBNull(oRow("BirthDate")) Then
            Me.BirthDate = Day(CDate(oRow("BirthDate"))).ToString + "/" + Month(CDate(oRow("BirthDate"))).ToString + "/" + Year(CDate(oRow("BirthDate"))).ToString
        Else
            Me.BirthDate = ""
        End If

        Me.BirthPlace = oRow("BirthPlace").ToString.Trim
        Me.Type = oRow("PersonalCustomerType").ToString.Trim

        Me.MotherName = oRow("MotherName").ToString.Trim
        Me.NamaPasangan = oRow("SINamaSuamiIstri").ToString.Trim
        Me.IDNumber = oRow("NoDokument").ToString.Trim
        Me.BirthPlace = oRow("TempatLahir").ToString.Trim
        Me.BirthDate = oRow("TglLahir").ToString.Trim
        Me.KartuKeluarga = oRow("NoKK").ToString.Trim
        Me.StatusPerkawinan = oRow("MaritalStatus").ToString.Trim

    End Sub
    Private Sub EditRecord(ByVal id As String)
        Dim objectDataTable As New DataTable
        Dim intLoopGrid As Integer
        Dim oRow As DataRow
        Dim lblNo As New Label
        Dim txtName As New TextBox
        Dim txtIDNumber As New TextBox
        'Dim uscBirthDate As TextBox
        Dim ucTglLahir As ucDateCE
        Dim cboRelationship As New DropDownList
        Dim oCustomer As New Parameter.Customer
        Dim pasanganExist As Boolean = True
        Dim pasanganValue As String = "SP"
        Dim hubungan As String

        oCustomer.CustomerID = id
        oCustomer.CustomerType = "p"
        oCustomer.strConnection = GetConnectionString()
        oCustomer = m_controller.GetCustomerEdit(oCustomer, "9")
        objectDataTable = oCustomer.listdata


        Datagrid1.DataSource = objectDataTable
        Datagrid1.DataBind()
        fillCboRelationship()

        For intLoopGrid = 0 To Datagrid1.Items.Count - 1
            lblNo = CType(Datagrid1.Items(intLoopGrid).Cells(0).FindControl("lblNo"), Label)
            txtName = CType(Datagrid1.Items(intLoopGrid).Cells(1).FindControl("txtFamilyName"), TextBox)
            txtIDNumber = CType(Datagrid1.Items(intLoopGrid).Cells(2).FindControl("txtFamilyIDNumber"), TextBox)
            'uscBirthDate = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uscFamilyBirthDate"), TextBox)
            ucTglLahir = CType(Datagrid1.Items(intLoopGrid).Cells(3).FindControl("uctgllahir"), ucDateCE)
            cboRelationship = CType(Datagrid1.Items(intLoopGrid).Cells(4).FindControl("cboFamilyRelationship"), DropDownList)

            lblNo.Text = objectDataTable.Rows(intLoopGrid).Item(0).ToString.Trim
            txtName.Text = objectDataTable.Rows(intLoopGrid).Item(1).ToString.Trim
            txtIDNumber.Text = objectDataTable.Rows(intLoopGrid).Item(2).ToString.Trim
            'uscBirthDate.Text = Day(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString.Trim + "/" + Month(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString("D2") + "/" + Year(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString.Trim
            ucTglLahir.Text = Day(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString("D2") + "/" + Month(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString("D2") + "/" + Year(CDate(objectDataTable.Rows(intLoopGrid).Item(3))).ToString.Trim

            hubungan = objectDataTable.Rows(intLoopGrid).Item(4).ToString
            If hubungan <> "" Then
                cboRelationship.SelectedValue = hubungan

            End If
        Next

        'If Datagrid1.Items.Count <= 1 Then
        '    AddRecord()
        '    AddRecord()
        '    AddRecord()
        '    AddRecord()
        '    AddRecord()
        'End If
    End Sub
    
#End Region
#Region "ItemCommand"
    Private Sub Datagrid1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles Datagrid1.ItemCommand
        If e.CommandName = "Delete" Then
            DeleteBaris(e.Item.ItemIndex)
        End If
    End Sub
#End Region
  
End Class