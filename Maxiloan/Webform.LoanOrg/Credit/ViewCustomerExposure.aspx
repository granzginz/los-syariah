﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewCustomerExposure.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewCustomerExposure" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewCustomerExposure</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                CUSTOMER EXPOSURE
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:Label ID="lblCustName" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                OUTSTANDING AMOUNT
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Balance A/R
                </label>
                <asp:Label ID="lblOSBalance" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Principal
                </label>
                <asp:Label ID="lblOSPrincipal" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Margin
                </label>
                <asp:Label ID="lblOSInterest" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                MAXIMUM DARI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Balance
                </label>
                <asp:Label ID="LblMaxOfOSBalance" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Hari Nunggak
                </label>
                <asp:Label ID="LblMaxOfOverDueDays" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Jumlah Angsuran
                </label>
                <asp:Label ID="LblInstallmentAmount" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jumlah Nunggak
                </label>
                <asp:Label ID="LblOverdueAmount" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                JUMLAH DARI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kontrak Aktif
                </label>
                <asp:Label ID="lblActiveAgreement" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Asset Ditarik
                </label>
                <asp:Label ID="lblAssetRepossessed" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Dalam Proses Kontrak
                </label>
                <asp:Label ID="lblInProcessAgreement" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Asset Inventory
                </label>
                <asp:Label ID="lblAssetInventoried" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Asset dalam Financing
                </label>
                <asp:Label ID="lblAssetInFinancing" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Kontrak Written Off
                </label>
                <asp:Label ID="lblWrittenOffAgreement" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kontrak di Rejected
                </label>
                <asp:Label ID="LblRejectedAgreement" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Kontrak Stop Hitung Margin
                </label>
                <asp:Label ID="LblNonAccrualAgreement" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Kontrak dibatal
                </label>
                <asp:Label ID="LblCancelledAgreement" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Cheque ditolak
                </label>
                <asp:Label ID="LblBounceCheque" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    BUCKET
                </label>
                TOTAL OD
            </div>
            <div class="form_right">
                <label>
                    BUCKET
                </label>
                TOTAL OD
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket1text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket1" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket6Text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket6" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket2text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket2" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket7text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket7" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket3text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket3" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket8text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket8" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket4text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket4" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket9text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket9" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket5text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket5" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket10text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket10" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    TOTAL
                </label>
                <asp:Label ID="lblTotalAllBucket" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
