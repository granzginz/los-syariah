﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAccrInc.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewAccrInc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAccrInc</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=15, top=10, width=985, height=600, menubar=0, scrollbars=yes');
        }
		
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                ACCRUED INCOME
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:LinkButton ID="lblCustomerName" runat="server" EnableViewState="False"></asp:LinkButton>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    O/S AR
                </label>
                <asp:Label ID="lblOSAR" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    O/S Principal
                </label>
                <asp:Label ID="lblOSPrincipal" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    O/S Margin
                </label>
                <asp:Label ID="lblOSInterest" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Margin Effective
                </label>
                <asp:Label ID="lblEffectiveRate" runat="server" EnableViewState="False"></asp:Label>&nbsp;%
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DAFTAR ACCRUED INCOME
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                EnableViewState="False">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn DataField="InsSeqNo" SortExpression="InsSeqNo" HeaderText="ANGS KE">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="RecognizeDate" SortExpression="RecognizeDate" HeaderText="TGL RECOGNIZED">
                    </asp:BoundColumn>
                    <asp:TemplateColumn SortExpression="InterestAmount" HeaderText="Margin">
                        <ItemTemplate>
                            <asp:Label ID="lblInterestAmount" runat="server" Text='<%#formatnumber(Container.DataItem("InterestAmount"),2)%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="StartPeriod" SortExpression="STARTDATE" HeaderText="MULAI TGL">
                    </asp:BoundColumn>
                    <asp:BoundColumn DataField="EndPeriod" SortExpression="ENDDATE" HeaderText="SAMPAI TGL">
                    </asp:BoundColumn>
                    <asp:TemplateColumn SortExpression="RECOGNIZEDAYS" HeaderText="HARI">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" D="lblRECOGNIZEDDAYS">
												<%# container.dataitem("RECOGNIZEDAYS") %>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn SortExpression="ACCRUEDINCOME" HeaderText="ACCRUED INCOME">
                        <ItemTemplate>
                            <asp:Label ID="lblAccIncome" runat="server" Text='<%#FormatNumber(Container.DataItem("AccruedIncome"),2)%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
   <%-- <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
