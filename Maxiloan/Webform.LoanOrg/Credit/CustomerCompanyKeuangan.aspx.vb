﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller

#End Region

Public Class CustomerCompanyKeuangan
    Inherits AbsCustomerCompany

    Protected WithEvents UCBankAcc As UcBankAccount     

 

    Dim status As Boolean
    Dim Style As String = "ACCACQ"
    Private m_controllerApp As New ApplicationController
    Private ocustomclass As New Parameter.Application

    Protected WithEvents txtOmsetBulanan As ucNumberFormat
    Protected WithEvents txtBiayaBulanan As ucNumberFormat
    Protected WithEvents txtDeposito As ucNumberFormat 
    Protected WithEvents txtRatio As ucNumberFormat
    Protected WithEvents txtROI As ucNumberFormat
    Protected WithEvents txtDER As ucNumberFormat
    Private oController As New ProspectController
    Private time As String

#Region "Property"
    Property ActivityDateStart() As DateTime
        Get
            Return ViewState("ActivityDateStart").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateStart") = Value
        End Set
    End Property

    Property ActivityDateEnd() As DateTime
        Get
            Return ViewState("ActivityDateEnd").ToString
        End Get
        Set(ByVal Value As DateTime)
            ViewState("ActivityDateEnd") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.BranchID = Request("branchID")
            Me.ProspectAppID = Request("prospectappid")
            If Request("prospectappid") = "" Then Me.ProspectAppID = "-"
            gStrPath = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            gStrFileName = "CUST_" & Me.Session.SessionID & ".xml"
            BindNumberFormat()

            'Modify by Wira 20171023
            Me.CustomerID = Request("id")
            If Request("ActivityDateStart") = Nothing Then
                time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
                Me.ActivityDateStart = Me.BusinessDate + " " + time
            Else
                Me.ActivityDateStart = Request("ActivityDateStart")
            End If
            '--------------------------

            If Me.ProspectAppID <> "-" Then
                With ocustomclass
                    .BranchId = Me.BranchID
                    .strConnection = GetConnectionString()
                    .ProspectAppID = Me.ProspectAppID
                End With
                ocustomclass = m_controllerApp.GetCustomerProspect(ocustomclass)                
            End If


            Me.PageAddEdit = Request("page")
            If Me.PageAddEdit = "Add" Then
                GetXML()
                UCBankAcc.BindBankAccount()
                lblTitle.Text = "ADD"                
            Else
                lblTitle.Text = "EDIT"
                Me.CustomerID = Request("id")
                BindEdit(Me.CustomerID)                
                cTabs.SetNavigateUrl(Request("page"), Request("id")) 
            End If
            cTabs.RefreshAttr(Request("pnl"))
            'If Me.PageAddEdit <> "Add" Then
            '    Dim intTotalCustomer As Integer
            '    intTotalCustomer = m_controller.GetTotalCustomer(Me.CustomerID.Trim, GetConnectionString)                
            'End If

            InitiateUCnumberFormat(txtOmsetBulanan, True, True, "regular_text numberAlign")
            InitiateUCnumberFormat(txtBiayaBulanan, True, True, "regular_text numberAlign")
            InitiateUCnumberFormat(txtDeposito, False, True, "regular_text numberAlign")
            'InitiateUCnumberFormat(txtAddCollAmt, False, True)

        End If
        
    End Sub 
#Region "Edit"
    Sub BindEdit(ByVal id As String)
        Dim dtEntity As DataTable = Nothing
        Dim oRow As DataRow
        Dim oCustomer As New Parameter.Customer

        oCustomer.CustomerID = id
        oCustomer.CustomerType = "C"
        oCustomer.strConnection = GetConnectionString()

        oCustomer = m_controller.GetCustomerEdit(oCustomer, "1")

        If Not oCustomer Is Nothing Then
            dtEntity = oCustomer.listdata
        End If
        oRow = dtEntity.Rows(0)

        Me.Name = oRow("Name").ToString.Trim
        Me.NPWP = oRow("NPWP").ToString.Trim
        Me.Address = oRow("CompanyAddress").ToString.Trim
        Me.RT = oRow("CompanyRT").ToString.Trim
        Me.RW = oRow("CompanyRW").ToString.Trim
        Me.Kelurahan = oRow("CompanyKelurahan").ToString.Trim
        Me.Kecamatan = oRow("CompanyKecamatan").ToString.Trim
        Me.City = oRow("CompanyCity").ToString.Trim
        Me.ZipCode = oRow("CompanyZipCode").ToString.Trim
        Me.APhone1 = oRow("CompanyAreaPhone1").ToString.Trim
        Me.APhone2 = oRow("CompanyAreaPhone2").ToString.Trim
        Me.Phone1 = oRow("CompanyPhone1").ToString.Trim
        Me.Phone2 = oRow("CompanyPhone2").ToString.Trim
        Me.AFax = oRow("CompanyAreaFax").ToString.Trim
        Me.Fax = oRow("CompanyFax").ToString.Trim
        Me.CustomerID = id

        Me.CustomerGroupID = oRow("CustomerGroupID").ToString.Trim        

        txtRatio.Text = FormatNumber(IIf(oRow("CurrentRatio").ToString.Trim = "", "0", oRow("CurrentRatio").ToString.Trim), 2)
        txtROI.Text = FormatNumber(IIf(oRow("ROI").ToString.Trim = "", "0", oRow("ROI").ToString.Trim), 2)
        txtDER.Text = FormatNumber(IIf(oRow("DER").ToString.Trim = "", "0", oRow("DER").ToString.Trim), 2)
        txtDeposito.Text = FormatNumber(IIf(oRow("Deposito").ToString.Trim = "", "0", oRow("Deposito").ToString.Trim), 2)
        ' txtAddCollType.Text = oRow("AdditionalCollateralType").ToString.Trim
        'txtAddCollAmt.Text = FormatNumber(IIf(oRow("AdditionalCollateralAmount").ToString.Trim = "", "0", oRow("AdditionalCollateralAmount").ToString.Trim), 2)
        cboResikoUsaha.SelectedIndex = cboResikoUsaha.Items.IndexOf(cboResikoUsaha.Items.FindByValue(oRow("ResikoUsaha").ToString.Trim))

        cboResikoLS.SelectedIndex = cboResikoLS.Items.IndexOf(cboResikoLS.Items.FindByValue(oRow("ResikoLS").ToString.Trim))

        UCBankAcc.BankBranchId = oRow("BankBranchID").ToString.Replace("-", "").Trim
        UCBankAcc.BindBankAccount()
        UCBankAcc.BankID = oRow("BankID").ToString.Replace("-", "").Trim

        UCBankAcc.BankBranch = oRow("BankBranch").ToString.Replace("-", "").Trim
        UCBankAcc.AccountNo = oRow("AccountNo").ToString.Replace("-", "").Trim
        UCBankAcc.AccountName = oRow("AccountName").ToString.Replace("-", "")

        txtOmsetBulanan.Text = FormatNumber(IIf(oRow("OmsetBulanan").ToString.Trim = "", "0", oRow("OmsetBulanan").ToString.Trim), 0)
        txtBiayaBulanan.Text = FormatNumber(IIf(oRow("BiayaBulanan").ToString.Trim = "", "0", oRow("BiayaBulanan").ToString.Trim), 0)


    End Sub
#End Region
#Region "Save"
    Private Sub imbPSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ocustomer As New Parameter.Customer      

        Try
            ocustomer.OmsetBulanan = CDbl(txtOmsetBulanan.Text)
            ocustomer.BiayaBulanan = CDbl(txtBiayaBulanan.Text)
            ocustomer.Ratio = txtRatio.Text
            ocustomer.ROI = txtROI.Text
            ocustomer.DER = txtDER.Text
            ocustomer.Deposito = txtDeposito.Text
            'ocustomer.AdditionalCollateralType = txtAddCollType.Text
            'ocustomer.AdditionalCollateralAmount = txtAddCollAmt.Text
            ocustomer.BankBranchId = CInt(IIf(UCBankAcc.BankBranchId.Trim = "", "0", UCBankAcc.BankBranchId.Trim))
            ocustomer.BankID = UCBankAcc.BankID
            ocustomer.BankBranch = UCBankAcc.BankBranch
            ocustomer.AccountNo = UCBankAcc.AccountNo
            ocustomer.AccountName = UCBankAcc.AccountName
            ocustomer.ResikoUsaha = CInt(cboResikoUsaha.SelectedValue)
            ocustomer.ResikoLS = CInt(cboResikoLS.SelectedValue)
            Dim oReturn As New Parameter.Customer

            Dim Err As String = ""
            ocustomer.strConnection = GetConnectionString()
            ocustomer.CustomerID = Me.CustomerID
            Err = m_controller.CompanyKeuanganSaveEdit(ocustomer)

            If Err = "" Then
                'Modify by WIra 20171011
                If Me.PageAddEdit = "Add" Then
                    ProspectLog()
                End If
                '----------------

                Response.Redirect("Customer.aspx")
            Else
                ShowMessage(lblMessage, Err, True)
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            Exit Sub
        End Try
        
    End Sub
#End Region
    Private Sub imbPCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click        
        Response.Redirect("Customer.aspx")
    End Sub
    
#Region "Bind ucNumber"
    Sub BindNumberFormat()
        With txtRatio
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "medium_text numberAlign"
            .RangeValidatorMinimumValue = "0"
            .RangeValidatorMaximumValue = "100"
        End With
        With txtROI
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "medium_text numberAlign"
            .RangeValidatorMinimumValue = "0"
            .RangeValidatorMaximumValue = "100"
        End With
        With txtDER
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "medium_text numberAlign"
            .RangeValidatorMinimumValue = "0"
            .RangeValidatorMaximumValue = "100"
        End With
        With txtOmsetBulanan
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "medium_text numberAlign"
        End With
        With txtBiayaBulanan
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "medium_text numberAlign"
        End With
        With txtDeposito
            .RequiredFieldValidatorEnable = True
            .RangeValidatorEnable = True
            .TextCssClass = "medium_text numberAlign"
        End With
        'With txtAddCollAmt
        '    .RequiredFieldValidatorEnable = True
        '    .RangeValidatorEnable = True
        '    .TextCssClass = "medium_text numberAlign"
        'End With
    End Sub
#End Region

    Sub ProspectLog()
        time = Now.Hour.ToString("00") & ":" & Now.Minute.ToString("00") & ":" & Now.Second.ToString("00")
        Me.ActivityDateEnd = Me.BusinessDate + " " + time

        Dim oApplication As New Parameter.Prospect
        oApplication.strConnection = GetConnectionString()
        oApplication.BranchId = Replace(Me.sesBranchId, "'", "").ToString
        oApplication.ProspectAppID = Me.ProspectAppID
        oApplication.ActivityType = "CST"
        oApplication.ActivityDateStart = Me.ActivityDateStart
        oApplication.ActivityDateEnd = Me.ActivityDateEnd
        oApplication.ActivityUser = Me.Loginid
        oApplication.ActivitySeqNo = 8

        Dim ErrorMessage As String = ""
        Dim oReturn As New Parameter.Prospect

        oReturn = oController.ProspectLogSave(oApplication)

        If oReturn.Err <> "" Then
            ShowMessage(lblMessage, ErrorMessage, True)
            Exit Sub
        End If
    End Sub
End Class