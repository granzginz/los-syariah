﻿
#Region "Imports"
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class CustomerCases
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents ucLookUpCustomer As ucLookUpCustomer
    Private m_controller As New CustomerCasesController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property AddEdit() As String
        Get
            Return CType(ViewState("vwsAddEdt"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsAddEdt") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property DataTableCases() As DataTable
        Get
            Return CType(ViewState("DataTableCases"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DataTableCases") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then            
            Me.FormID = "CUSTCASES"
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                txtPage.Text = "1"
                Me.Sort = "CustomerID ASC"
                Me.CmdWhere = "All"
                BindComboCases()
                cboSKasus.Visible = False
            End If
            BindGridEntity(Me.CmdWhere)
        End If

    End Sub
    Private Sub InitialDefaultPanel()
        pnlList.Visible = True
        pnlAddEdit.Visible = False
    End Sub
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oCustomerCases As New Parameter.CustomerCases
        InitialDefaultPanel()
        oCustomerCases.strConnection = GetConnectionString()
        oCustomerCases.WhereCond = cmdWhere
        oCustomerCases.CurrentPage = currentPage
        oCustomerCases.PageSize = pageSize
        oCustomerCases.SortBy = Me.Sort
        oCustomerCases = m_controller.GetCustomerCases(oCustomerCases)

        If Not oCustomerCases Is Nothing Then
            dtEntity = oCustomerCases.Listdata
            recordCount = oCustomerCases.Totalrecords
        Else
            recordCount = 0
        End If
        
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
    Sub BindComboCases()
        Dim oCustomerCases As New Parameter.CustomerCases
        Dim tbl As New DataTable
        oCustomerCases.strConnection = GetConnectionString()
        tbl = m_controller.GetComboCases(oCustomerCases)

        cboKasus.DataTextField = "CaseDescription"
        cboKasus.DataValueField = "CaseID"
        cboKasus.DataSource = tbl
        cboKasus.DataBind()
        cboSKasus.DataTextField = "CaseDescription"
        cboSKasus.DataValueField = "CaseID"
        cboSKasus.DataSource = tbl
        cboSKasus.DataBind()

        DataTableCases = tbl
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
            e.Item.Cells(7).Text = CDate(e.Item.Cells(7).Text).ToString("dd/MM/yyyy").Trim()

            For i As Integer = 0 To DataTableCases.Rows.Count
                Dim dr As DataRow = DataTableCases.Rows(i)
                If dr(0).ToString.Trim = e.Item.Cells(5).Text.Trim Then
                    e.Item.Cells(5).Text = dr(1).ToString.Trim
                    Exit For
                End If
            Next
        End If
    End Sub
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imgbtnPageNumb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPageNumb.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int32)
                BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String
        Dim oCustomerCases As New Parameter.CustomerCases
        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Edit", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Me.AddEdit = "EDIT"
            pnlAddEdit.Visible = True
            pnlList.Visible = False
            ButtonBack.Visible = False
            ButtonCancel.Visible = True
            ButtonSave.Visible = True
            ButtonClose.Visible = False
            ButtonCancel.CausesValidation = False

            lblTitleAddEdit.Text = Me.AddEdit

            oCustomerCases.CustomerID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString.Trim
            oCustomerCases.CaseID = CInt(e.Item.Cells(2).Text)
            oCustomerCases.strConnection = GetConnectionString()
            oCustomerCases = m_controller.GetCustomerCasesList(oCustomerCases)

            hdnCasesID.Value = oCustomerCases.CaseID.ToString
            hdnCustomerID.Value = oCustomerCases.CustomerID.Trim
            hdnCustomerIDEdit.Value = oCustomerCases.CustomerID.Trim
            txtCatatan.InnerText = oCustomerCases.Notes.Trim
            txtNoReferensi.Text = oCustomerCases.ReferenceNo.Trim
            TglReferensiDate.Text = oCustomerCases.ReferenceDate.ToString("dd/MM/yyyy").Trim
            cboKasus.SelectedIndex = cboKasus.Items.IndexOf(cboKasus.Items.FindByValue(oCustomerCases.CaseID.ToString))

            ucLookUpCustomer.BindLookup(oCustomerCases.CustomerID.Trim)
            txtNmCustomer.Text = ucLookUpCustomer.Name
            txtJenisIdentitas.Text = ucLookUpCustomer.IDTypeDescription
            txtNoIdentitas.Text = ucLookUpCustomer.IDNumber
            txtTempatTglLahir.Text = ucLookUpCustomer.BirthPlace + ", " + ucLookUpCustomer.BirthDate.ToString("dd/MM/yyyy").Trim
        ElseIf e.CommandName = "Delete" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Del", "MAXILOAN") Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            Dim customClass As New Parameter.CustomerCases
            With customClass
                .CustomerID = dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString
                .CaseID = CInt(e.Item.Cells(2).Text)
                .strConnection = GetConnectionString()
            End With
            err = m_controller.CustomerCasesDelete(customClass)
            If err <> "" Then
                showMessage(lblMessage, err, True)

            Else
                ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)

            End If
            BindGridEntity(Me.CmdWhere)
            txtPage.Text = "1"
        End If
    End Sub
    Private Sub ButtonSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.CustomerCases
        Dim ErrMessage As String = ""

        With customClass
            .CustomerIDEdit = hdnCustomerIDEdit.Value
            .Notes = txtCatatan.InnerText
            .ReferenceNo = txtNoReferensi.Text
            .ReferenceDate = Date.ParseExact(TglReferensiDate.Text, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
            .CaseIDEdit = CInt(cboKasus.SelectedItem.Value)
            .LoginId = Me.Loginid.Trim
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
        End With

        If Me.AddEdit = "ADD" Then
            ErrMessage = m_controller.CustomerCasesSaveAdd(customClass)
            If ErrMessage <> "" Then
                ShowMessage(lblMessage, ErrMessage, True)

                Exit Sub
            Else
                lblMessage.Text = MessageHelper.MESSAGE_INSERT_SUCCESS

                BindGridEntity(Me.CmdWhere)
            End If
        ElseIf Me.AddEdit = "EDIT" Then
            customClass.CustomerID = hdnCustomerID.Value
            customClass.CaseID = CInt(hdnCasesID.Value)
            m_controller.CustomerCasesSaveEdit(customClass)
            lblMessage.Text = MessageHelper.MESSAGE_UPDATE_SUCCESS

            BindGridEntity(Me.CmdWhere)
        End If
    End Sub
    Private Sub imbAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", "MAXILOAN") Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        pnlAddEdit.Visible = True
        pnlList.Visible = False
        ButtonBack.Visible = False
        ButtonCancel.Visible = True
        ButtonSave.Visible = True
        ButtonClose.Visible = False
        ButtonCancel.CausesValidation = False
        Me.AddEdit = "ADD"
        lblTitleAddEdit.Text = Me.AddEdit
        txtCatatan.InnerText = Nothing
        txtNoReferensi.Text = Nothing
        TglReferensiDate.Text = Nothing
        cboKasus.SelectedIndex = 0


    End Sub
   
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies("CustomerCases")
        If Not cookie Is Nothing Then
            cookie.Values("where") = Me.CmdWhere
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("CustomerCases")
            cookieNew.Values.Add("where", Me.CmdWhere)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
        If cboSKasus.Visible Then
            Me.CmdWhere = "CaseID = '" + cboSKasus.SelectedItem.Value + "'"
        Else
            If txtSearch.Text.Trim <> "" Then                
                'Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
                'Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + tmptxtSearchBy + "%'"
                Me.CmdWhere = cboSearch.SelectedItem.Value + " = '" + txtSearch.Text + "'"
            End If
        End If

        BindGridEntity(Me.CmdWhere)
    End Sub
    Private Sub ButtonBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonBack.Click
        Response.Redirect("CustomerCases.aspx")
    End Sub
    Private Sub ButtonCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Response.Redirect("CustomerCases.aspx")
    End Sub
    'Lookup Customer
    Protected Sub btnLookupCustomer_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLookupCustomer.Click
        ucLookUpCustomer.CmdWhere = "All"
        ucLookUpCustomer.Sort = "CustomerID ASC"
        ucLookUpCustomer.Popup()
    End Sub
    Public Sub CatSelectedCustomer(ByVal CustomerID As String,
                                           ByVal Name As String,
                                           ByVal Address As String,
                                           ByVal BadType As String,
                                           ByVal IDType As String,
                                     ByVal IDTypeDescription As String,
                                     ByVal IDNumber As String,
                                     ByVal BirthPlace As String,
                                     ByVal BirthDate As DateTime,
                                     ByVal CustomerType As String)
        txtNmCustomer.Text = Name
        hdnCustomerIDEdit.Value = CustomerID
        txtNmCustomer.Text = Name
        txtJenisIdentitas.Text = IDTypeDescription
        txtNoIdentitas.Text = IDNumber
        txtTempatTglLahir.Text = BirthPlace + ", " + CDate(BirthDate).ToString("dd/MM/yyyy").Trim
    End Sub
    Private Sub cboSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cboSearch.SelectedIndexChanged
        If cboSearch.SelectedItem.Value = "CaseID" Then
            cboSKasus.Visible = True
            txtSearch.Visible = False
        Else
            cboSKasus.Visible = False
            txtSearch.Visible = True
        End If
    End Sub
End Class