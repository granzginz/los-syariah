﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerCases.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerCases" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpCustomer" Src="../../Webform.UserController/ucLookUpCustomer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CUSTOMER CASES</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR CUSTOMER CASES
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" DataKeyField="CustomerID" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid" />
                            <HeaderStyle CssClass="th" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../Images/iconedit.gif"
                                            CommandName="Edit"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../Images/iconDelete.gif"
                                            CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="CaseID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="NAMA CUSTOMER">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="CustomerType" SortExpression="CustomerType" HeaderText="JENIS">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="CaseID" SortExpression="CaseID" HeaderText="KASUS"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ReferenceNo" SortExpression="ReferenceNo" HeaderText="NO REFERENSI">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="ReferenceDate" SortExpression="ReferenceDate" HeaderText="TANGGAL REFERENSI">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Notes" SortExpression="Notes" HeaderText="CATATAN"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                            </asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtPage" runat="server" cssClass="small_text">1</asp:TextBox>
                            <asp:Button ID="btnPageNumb" runat="server" text="Go" cssClass="small buttongo blue" EnableViewState = "False"/>                                
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                ErrorMessage="No Halaman Salah"   Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" Type="integer"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" CssClass="validator_general"></asp:RangeValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblrecord" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" Text="Add" CssClass="small button blue"
                        CausesValidation="False" />
                  
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CARI CUSTOMER CASES
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" AutoPostBack="true" runat="server">
                            <asp:ListItem Value="Name">NAMA CUSTOMER</asp:ListItem>
                            <asp:ListItem Value="CustomerType">JENIS</asp:ListItem>
                            <asp:ListItem Value="CaseID">KASUS</asp:ListItem>
                            <asp:ListItem Value="ReferenceNo">NO REFERENSI</asp:ListItem>
                            <asp:ListItem Value="ReferenceDate">TANGGAL REFERENSI</asp:ListItem>
                            <asp:ListItem Value="Notes">CATATAN</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%" MaxLength="100"></asp:TextBox>
                        <asp:DropDownList ID="cboSKasus" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue" />
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Customer Cases
                        </label>
                        <asp:TextBox ID="txtNmCustomer" Enabled="false" runat="server"></asp:TextBox>
                        <asp:Button ID="btnLookupCustomer" runat="server" CausesValidation="False" 
                            CssClass="small buttongo blue" Text="..." />
                        <uc1:uclookupcustomer id="ucLookUpCustomer" runat="server" oncatselected="CatSelectedCustomer" />
                        </uc1:ucLookUpCustomer>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Identitas</label>
                        <asp:TextBox ID="txtJenisIdentitas" Enabled="false" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Identitas</label>
                        <asp:TextBox ID="txtNoIdentitas" Enabled="false" runat="server" 
                            CssClass="medium_text"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tempat Tanggal Lahir</label>
                        <asp:TextBox ID="txtTempatTglLahir" Enabled="false" runat="server" 
                            CssClass="medium_text"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Kasus</label>
                        <asp:DropDownList ID="cboKasus" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Referensi
                        </label>
                        <asp:TextBox ID="txtNoReferensi" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNoReferensi" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Referensi
                        </label>
                        <asp:TextBox ID="TglReferensiDate" runat="server" CssClass="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="TglReferensiDate_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="TglReferensiDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="TglReferensiDate" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_general">
                            Catatan</label>
                        <textarea id="txtCatatan" runat="server" cols="17" rows="5" 
                            class="multiline_textbox_uc"></textarea>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue" />
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="true" Text="Cancel"
                        CssClass="small button gray" />
                    <asp:Button ID="ButtonBack" runat="server" CausesValidation="true" Text="Back" CssClass="small button gray" />
                    <asp:Button ID="ButtonClose" runat="server" CausesValidation="False" Text="Close"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <input id="hdnCustomerID" type="hidden" name="hdnCustomerID" runat="server" />
            <input id="hdnCasesID" type="hidden" name="hdnCasesID" runat="server" />
            <input id="hdnCustomerIDEdit" type="hidden" name="hdnCustomerIDEdit" runat="server" />
            <input id="hdnCasesIDEdit" type="hidden" name="hdnCasesIDEdit" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
