﻿Public Class CustomerNProspectTabs
    Inherits CustomerTabsBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
        End If
    End Sub

    Protected Overrides Sub initTabs()
        _tabs = New Dictionary(Of String, CustomerTab) From {
                          {"tabCustomer", New CustomerTab(tabCustomer, hyCustomer)},
                          {"tabProspect", New CustomerTab(tabProspect, hyProspect)}}
    End Sub
    Public Overrides Sub SetNavigateUrl(page As String, id As String)
        MyBase.SetNavigateUrl(page, id)

        _tabs("tabCustomer").Link.NavigateUrl = "Customer.aspx?page=" + page + "&id=" + id + "&pnl=tabCustomer"
        _tabs("tabProspect").Link.NavigateUrl = "CustomerFromProspect.aspx?page=" + page + "&id=" + id + "&pnl=tabProspect"
    End Sub


    Public Overrides Sub RefreshAttr(pnl As String)
        MyBase.RefreshAttr(pnl)
        If pnl = "" Then
            _tabs("tabCustomer").Tab.Attributes.Remove("class")
            _tabs("tabCustomer").Tab.Attributes.Add("class", "tab_selected")
        End If
    End Sub
End Class