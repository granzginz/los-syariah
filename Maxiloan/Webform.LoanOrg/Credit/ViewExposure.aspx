﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewExposure.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewExposure" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewExposure</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;
        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=15, top=10, width=985, height=600, menubar=0, scrollbars=yes');
        }		
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" EnableViewState="False"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                DATA KONTRAK EXPOSURE
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:LinkButton ID="lblCustName" runat="server" EnableViewState="False"></asp:LinkButton>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                OUTSTANDING
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Maximum Outstanding Balance
                </label>
                <asp:Label ID="lblMaxOSBalance" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Balance
                </label>
                <asp:Label ID="lblOSBalance" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Principal
                </label>
                <asp:Label ID="lblOSPrincipal" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Outstanding Interest
                </label>
                <asp:Label ID="lblOSInterest" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box_title">
        <div class="form_single">
            <h4>
                JUMLAH
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Asset ditarik
                </label>
                <asp:Label ID="lblAssetRepossed" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Asset Inventory </td>
                </label>
                <asp:Label ID="lblAssetInventoried" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Rescheduling
                </label>
                <asp:Label ID="lblRescheduling" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Over Kontrak
                </label>
                <asp:Label ID="lblAgreementTransfer" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Penggantian Asset
                </label>
                <asp:Label ID="lblAssetReplacement" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Cheque yang ditolak
                </label>
                <asp:Label ID="lblBounceCheque" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div>
            <div class="form_left">
                <label>
                    BUCKET
                </label>
                TOTAL OD
            </div>
            <div class="form_right">
                <label>
                    BUCKET
                </label>
                TOTAL OD
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket1text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket1" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket6Text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket6" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket2text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket2" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket7text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket7" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket3text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket3" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket8text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket8" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket4text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket4" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket9text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket9" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    <asp:Label ID="lblBucket5text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket5" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    <asp:Label ID="lblBucket10text" runat="server" EnableViewState="False"></asp:Label>
                </label>
                <asp:Label ID="lblBucket10" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
            </div>
            <div class="form_right">
                <label>
                    TOTAL
                </label>
                <asp:Label ID="lblTotalAllBucket" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DATA OVER KONTRAK
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtg" runat="server" EnableViewState="False" AutoGenerateColumns="False"
                BorderWidth="0px" CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:TemplateColumn HeaderText="NO">
                        <HeaderStyle Width="10%"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server" EnableViewState="False"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="OldCustomerID" HeaderText="CUSTOMER SEBELUMNYA"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ExecutionDate" HeaderText="DATA TRANSFER"></asp:BoundColumn>
                    <asp:BoundColumn DataField="reason" HeaderText="ALASAN"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="VIEW"></asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
   <%-- <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
