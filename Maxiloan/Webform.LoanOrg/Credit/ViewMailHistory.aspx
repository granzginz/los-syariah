﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewMailHistory.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewMailHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewMailHistory</title>
     <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var x = screen.width;
        var y = screen.height - 100;	
        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=15, top=10, width=985, height=600, menubar=0, scrollbars=yes');
        }
		
    </script>
</head>
<body>
    <form id="form1" runat="server">
     <div class="form_title"> <div class="form_single"> <h3>
                      VIEW - MAIL HISTORY
						</h3>  </div>    </div>
			
			  <div class="form_box">  <div> <div class="form_left">
                        <label>
                             Nama Customer
                        </label> 
						<asp:LinkButton ID="lblCustomerName" runat="server" EnableViewState="False"></asp:LinkButton>
                    </div>
                    <div class="form_right">
                        <label>
                             No Kontrak
                        </label>
                       <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
                    </div>   </div> </div>

       <div class="form_box_header"> <div class="form_single">
				<asp:DataGrid ID="dtg" runat="server" EnableViewState="False" Width="100%" CssClass="grid_general"
                        CellSpacing="1" CellPadding="3" BorderWidth="0px" AutoGenerateColumns="False">
                        
                        <ItemStyle CssClass="item_grid"/>
                        <HeaderStyle CssClass="th"/>
                        <Columns>
                            <asp:BoundColumn Visible="False" DataField="CustomerName"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MailDateCreate" HeaderText="TANGGAL"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MAILTYPE" HeaderText="ENIS MAIL"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MailPrintedNum" HeaderText="CETAK KE"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MailDatePrint" HeaderText="TERAKHIR CETAK"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="MAIL #"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ReturnStatus" HeaderText="KEMBALI"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="CETAK"></asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="VIEW"></asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
				   </div>  </div>
          <div class="form_button"> 
			   <a href="javascript:window.close();">
                        <img src="../../Images/ButtonClose.gif" width="100" height="20" border="0" /></a>&nbsp;
                    <a href='viewStatementOfAccount.aspx?AgreementNo=<%=me.agreementNo%>&amp;Style=<%= request("style") %>&amp;ApplicationID=<%= request("ApplicationID") %>'>
                        <img height="20" src="../../Images/ButtonBack.gif" width="100" border="0" /></a>
			  </div>
			  
        
    
    </form>
</body>
</html>
