﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerPersonalPasangan.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerPersonalPasangan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddressCity" Src="../../Webform.UserController/ucAddressCity.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../../webform.UserController/ucMonthCombo.ascx" TagName="ucMonthCombo" TagPrefix="uc2" %>
<%@ Register Src="../../Webform.UserController/ucDateCE.ascx" TagName="ucDateCE" TagPrefix="uc2" %>

<%@ Register Src="CustomerTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Personal</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        var Page_Validators = new Array();

        $(document).ready(function () {
            $('#cboIndrustriHeader').change(function (e) {
                var sel = this.value.substr(0, 1);
                var kdind = $('#hdfKodeIndustriPS').val().substr(0, 1);
                if (sel != kdind)
                    $('#txtNamaIndustriPS').val('');
 
            });
        });


        function NamaIndustriBuClick(e, h) {
             
            var cboselect = $('#cboIndrustriHeader').val();
            if (cboselect == 'Select One') return;
            var url = e + cboselect
            OpenJLookup(url, 'Daftar Industri', h); return false;
             
        }

    </script>

     <style >
        .multiline_textbox {
            width: 60% !important;
        }
    </style>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
     <uct:tabs id='cTabs' runat='server'></uct:tabs>
   
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        DATA SUAMI/ISTRI
                    </h4>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlPasangan">
                <asp:Panel ID="pnlDataPasangan" runat="server">

                    <div class="form_box">
                         <div class="form_left"> 
                                <label> Nama Suami/Istri</label>
                                <asp:TextBox runat="server" ID="txtNamaPasangan" Width="60%"></asp:TextBox>                             
                        </div>
                         <div class="form_right"> 
                                    <label> Nama Perusahaan/Jenis Usaha</label>
                                    <asp:TextBox ID="txtCompanyNameSI" runat="server" Width="60%"></asp:TextBox>     
                        </div>
                    </div>

                    <div class="form_box">
                         <div class="form_left" class ="label_req">  
                                <label>Tempat/Tanggal Lahir</label>
                                <asp:TextBox ID="txtBirthPlaceP" runat="server" MaxLength="20"  Width="25%"></asp:TextBox>&nbsp;/ &nbsp;
                                <uc2:ucdatece id="txtTglLahirNew" runat="server" />
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*" ControlToValidate="txtBirthPlaceP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        </div>
                           
                        <div class="form_right">                            
                            <label class=""> Bidang Usaha Header</label>
                            <asp:DropDownList ID="cboIndrustriHeader" runat="server" />
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap pilih Bidang Usaha Header" ControlToValidate="cboIndrustriHeader" InitialValue="Select One" Display="Dynamic" CssClass="validator_general" />--%>
                        </div>
                    </div>
                         
                    
                     <div class="form_box">
                         <div class="form_left">   
                                <label class ="label_req">Jenis Identitas</label>
                                 <asp:DropDownList ID="cboIDTypeP" runat="server" CssClass="select"/> 
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Harap pilih Jenis Identitas" ControlToValidate="cboIDTypeP" Display="Dynamic" InitialValue="Select One" CssClass="validator_general"></asp:RequiredFieldValidator>                           
                            </div>

                            <div class="form_right">
                                <label> Bidang Usaha Detail</label>
                                      <asp:HiddenField runat="server" ID="hdfKodeIndustriPS" />
                                      <asp:TextBox runat="server" ID="txtNamaIndustriPS" Enabled="false" CssClass="medium_text" text="-"></asp:TextBox>
                                    <%--  <button class="small buttongo blue" onclick="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriPS.ClientID & "&nama=" & txtNamaIndustriPS.ClientID) %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;"> ...</button>        --%>
                                     <button class="small buttongo blue"  onclick ="NamaIndustriBuClick('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriPS.ClientID & "&nama=" & txtNamaIndustriPS.ClientID) & "&header=" %>','<%= jlookupContent.ClientID %>');">...</button>
                                </div>

                           
                        </div>
                       <%--modify by ario--%>
                      <div class="form_box">
                            <div class="form_left">  
                                <label class ="label_req">No Identitas</label>
                                <asp:TextBox runat="server" ID="txtNoDokument" MaxLength ="16"  Width="25%"></asp:TextBox>      
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi nomor Identitas" ControlToValidate="txtNoDokument" Display="Dynamic"  CssClass="validator_general" onkeypress="return numbersonly2(event)"></asp:RequiredFieldValidator>                      
                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" Display="Dynamic" ValidationExpression="\d+"
                                ErrorMessage="Harap isi Nomor Identitas dengan Angka" ControlToValidate="txtNoDokument"   CssClass ="validator_general"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form_right"> 
                                <label> Keterangan Usaha</label>
                                <asp:TextBox ID="txtKeteranganUsahaSI" runat="server" Width="50%"></asp:TextBox>   
                            </div>  
                       </div>

                       <div class="form_box">
                            <div class="form_left">    
                                <label>Masa Berlaku Sampai</label>
                                <uc2:ucdatece id="txtMasaBerlakuKTP" runat="server" />  
                            </div>

                             <div class="form_right">  
                                      <label> Pekerjaan</label>
                                      <%--<asp:DropDownList ID="cboJobTypeSI" runat="server"/> --%>
                                      <asp:DropDownList ID="cboJobTypeSI" runat="server" Width="150PX" AutoPostBack="true" ></asp:DropDownList>
                            </div> 
                        </div>
                        <div class="form_box">
                        <div class="form_single">
                            <label class="label">Occupation</label>
                            <asp:DropDownList ID="cboOccupation" runat="server" />
                        </div>
                    </div>

                    <div class="form_box_uc">
                        <div class="form_left_uc">
                            <asp:UpdatePanel runat="server" ID="ucAddressSIKTP">
                                <ContentTemplate>
                                    <div class="form_box_usercontrol_header">
                                        <h4>ALAMAT KTP</h4>
                                    </div>
                                    <div class="form_box_usercontrol">
                                        <asp:Button ID="btnAlamatKerja1A" CausesValidation="false" runat="server" Text="Copy Alamat KTP"
                                            CssClass="small buttongo blue label_calc"></asp:Button>
                                        <asp:Button ID="btnAlamatKerja2A" CausesValidation="false" runat="server" Text="Copy Alamat Domisili"
                                            CssClass="small buttongo blue label_calc"></asp:Button>
                                    </div>
                                    <uc1:ucaddresscity id="ucAddressSI_KTP" runat="server"></uc1:ucaddresscity>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="form_right_uc">
                            <asp:UpdatePanel runat="server" ID="upAddressSI">
                                <ContentTemplate> 
                                    <div class="form_box_usercontrol">                                        
                                        <div class="form_box_usercontrol_header">
                                            <h4>ALAMAT KANTOR</h4>
                                        </div>   
                                    </div>                    
                                    <div class="form_box_usercontrol">
                                        <asp:Button ID="btnAlamatKerja1" CausesValidation="false" runat="server" Text="Copy Alamat KTP" CssClass="small buttongo blue label_calc"></asp:Button>
                                        <asp:Button ID="btnAlamatKerja2" CausesValidation="false" runat="server" Text="Copy Alamat Domisili" CssClass="small buttongo blue label_calc"></asp:Button>
                                    </div>            
                                    <uc1:ucaddresscity id="ucAddressSI" runat="server"></uc1:ucaddresscity>
                                  <div class="form_single form_box_in_uc "> 
                                            <%--<label>  Beroperasi Sejak</label> --%>
                                            <Label ID="lblBerOperasiSejak" runat="server" >  Beroperasi Sejak</Label>
                                            <asp:TextBox ID="txtBerOperasiSejak" class="small_text" runat="server" MaxLength="4" Columns="7" onkeypress="return numbersonly2(event)"></asp:TextBox> 
                                            <asp:RequiredFieldValidator ID="RVBeroperasiSejak" runat="server" ControlToValidate="txtBerOperasiSejak"
                                            Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                    </div> 
                                    
                                        <div class="form_single form_box_in_uc "> 
                                            <label>Masa Kerja</label>
                                            <uc1:ucnumberformat id="txtMasaKerja" runat="server"></uc1:ucnumberformat> tahun
                                        </div>   
                                    
                                       <div class="form_single form_box_in_uc "> 
                                            <label>Usaha Lainnya</label>
                                            <asp:TextBox ID="txtUsahaLainnya" runat="server"  Width="50%"/>
                                            <%--<uc1:ucnumberformat id="txtPenghasilanTambahanSI" runat="server"></uc1:ucnumberformat> --%>
                                    </div> 
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>                                        
                </asp:Panel>
                <asp:Panel ID="pnlProfessional" runat="server" Width="100%">
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                DATA PROFESSIONAL
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Penghasilan Tetap Bulanan</label>
                            <uc1:ucnumberformat id="txtPDFixedInc" runat="server" maxlength="15"></uc1:ucnumberformat>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Penghasilan Tambahan Bulanan</label>
                            <uc1:ucnumberformat id="txtPDVariableInc" runat="server" maxlength="15"></uc1:ucnumberformat>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlPEntrepreneur" runat="server" Width="100%">
                    <div class="form_box_header">
                        <div class="form_single">
                            <h4>
                                DATA ENTERPRENEUR
                            </h4>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Penghasilan Tetap Bulanan</label>
                            <uc1:ucnumberformat id="txtFDFixedInc" runat="server" maxlength="15"></uc1:ucnumberformat>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Penghasilan Tambahan Bulanan</label>
                            <uc1:ucnumberformat id="txtFDVariableInc" runat="server" maxlength="15"></uc1:ucnumberformat>
                        </div>
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                            <label>
                                Nama Usaha Lain</label>
                            <asp:TextBox ID="txtEDName" runat="server" MaxLength="50" Style="width: 65%"></asp:TextBox>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <div class="form_button">
                <asp:Button ID="btnPSave" runat="server" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnPCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </ContentTemplate>
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="cboJobTypeSI" EventName="SelectedIndexChanged" />
            </Triggers>
    </asp:UpdatePanel>

    <style>
    .form_box_in_uc 
    {
        margin-left:0px !important;
        width:100% !important;
         border-left:none !important;
         border-right:none !important; 
         border-bottom: 1px solid #e5e5e5;
    }
    </style>
    </form>
</body>
</html>
