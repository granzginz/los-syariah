﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewDetailApproval.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewDetailApproval" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewTermCondition</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.ServerVariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        var x = screen.width;
        var y = screen.height - 100;			
    </script>
    <script language="javascript" type="text/javascript">
        function OpenViewCustomer(pCustomerID) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pCustomerID + '&Style=<%=request("style") %>', 'ViewCustomer', 'left=15, top=10, width=985, height=600, menubar=0, scrollbars=yes');
        }
		
    </script>
    
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - DETAIL INFORMASI APPROVAL
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server" EnableViewState="False"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                   Suku Bunga
                </label>
                <asp:Label ID="lblSukuBunga" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tenor
                </label>
                <asp:Label ID="lblTenor" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                   Admin Fee
                </label>
                <asp:Label ID="lblAdminFee" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Asuransi
                </label>
                <asp:Label ID="lblAsuransi" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                   Nilai Plafon
                </label>
                <asp:Label ID="lblPlafond" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   Nilai Pencairan
                </label>
                <asp:Label ID="lblNilaiPencairan" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                   Sisa Plafon
                </label>
                <asp:Label ID="lblAvailableAmount" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   Uang Muka OTR
                </label>
                <asp:Label ID="lblDownPayment" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                   Module
                </label>
                <asp:Label ID="lblModule" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   Total Pembiayaan
                </label>
                <asp:Label ID="lblTotalPembiayaan" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>        
        <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                   Biaya Provisi
                </label>
                <asp:Label ID="lblBiayaProvisi" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   Biaya Handling
                </label>
                <asp:Label ID="lblBiayaHandling" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>        
        <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                   Retensi Rate
                </label>
                <asp:Label ID="lblRetensiRate" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   Biaya Survey
                </label>
                <asp:Label ID="lblBiayaSurvey" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>        
        <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                   Biaya Notaris
                </label>
                <asp:Label ID="lblBiayaNotaris" runat="server" EnableViewState="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                   Total Biaya
                </label>
                <asp:Label ID="lblTotalBiaya" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
        
    <asp:Panel ID="pnldtlfact" runat="server" Visible="False">
    <div class="form_title">
        <div class="form_single">
            <h4>
                Detail Invoice Factoring
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgDtlFact" runat="server" EnableViewState="False" CssClass="grid_general"
                AutoGenerateColumns="False" PageSize="3" DataKeyField="InvoiceNo">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn DataField="InvoiceNo" HeaderText="No Invoice"></asp:BoundColumn>
                    <asp:BoundColumn DataField="InvoiceAmount" HeaderText="Nilai Invoice" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="PersentasePembiayaan" HeaderText="%"></asp:BoundColumn>
                    <asp:BoundColumn DataField="TotalPembiayaan" HeaderText="Nilai Faktur" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    </asp:Panel>
    <asp:Panel ID="pnldtlmdkj" runat="server" Visible="False">
    <div class="form_title">
        <div class="form_single">
            <h4>
                Detail Invoice Modal Kerja
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgDtlMdkj" runat="server" EnableViewState="False" CssClass="grid_general"
                AutoGenerateColumns="False" PageSize="3" DataKeyField="InvoiceNo">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn DataField="InvoiceNo" HeaderText="No Invoice"></asp:BoundColumn>
                    <asp:BoundColumn DataField="InvoiceAmount" HeaderText="Nilai Invoice" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="PersentasePembiayaan" HeaderText="%"></asp:BoundColumn>
                    <asp:BoundColumn DataField="TotalPembiayaan" HeaderText="Nilai Faktur" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    </asp:Panel>
   <asp:Panel ID="pnldtlmginv" runat="server" Visible="False">
    <div class="form_title">
        <div class="form_single">
            <h4>
                Detail
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgDtlMgInv" runat="server" EnableViewState="False" CssClass="grid_general"
                AutoGenerateColumns="False" PageSize="3" DataKeyField="InvoiceNo">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn DataField="InvoiceNo" HeaderText="No Invoice"></asp:BoundColumn>
                    <asp:BoundColumn DataField="InvoiceAmount" HeaderText="Nilai Invoice" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn>
                    <asp:BoundColumn DataField="PersentasePembiayaan" HeaderText="%"></asp:BoundColumn>
                    <asp:BoundColumn DataField="TotalPembiayaan" HeaderText="Nilai Faktur" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    </asp:Panel>
    <asp:Panel ID="PanelInfoPencairan" runat="server" Visible="true">
    <div class="form_title">
        <div class="form_single">
            <h4>
                <%--Detail Pencairan --%>
                Detail Fasilitas 
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtgpencairan" runat="server" EnableViewState="False" CssClass="grid_general"
                AutoGenerateColumns="False" PageSize="3" DataKeyField="NoFasilitas">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn DataField="NoFasilitas" HeaderText="No Fasilitas" ></asp:BoundColumn>
                    <asp:BoundColumn DataField="FasilitasAmount" HeaderText="Fasilitas Amount" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn>
                    <%--<asp:BoundColumn DataField="ApprovalValue" HeaderText="Nilai Pencairan" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn> --%>
                    <asp:BoundColumn DataField="ApprovalValue" HeaderText="Nilai Approval" DataFormatString="{0:###,###,##.#0;;0.00}"></asp:BoundColumn> 
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    </asp:Panel>
    </form>
</body>
</html>
