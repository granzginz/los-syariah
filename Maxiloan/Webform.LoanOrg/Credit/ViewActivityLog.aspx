﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewActivityLog.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewActivityLog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewActivityLog</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - ACTIVITY LOG
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="lblCustomerName" runat="server" EnableViewState="False"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="lblAgreementNo" runat="server" EnableViewState="False"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <asp:DataGrid ID="dtg" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                CellPadding="3" CellSpacing="1" CssClass="grid_general" Width="100%" EnableViewState="False">
                <ItemStyle CssClass="item_grid" />
                <HeaderStyle CssClass="th" />
                <Columns>
                    <asp:BoundColumn Visible="False" DataField="CustomerName"></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="NO">
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="ACTIVITYDATE" HeaderText="TGL AKTIVITAS"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ACTIVITYTYPE" HeaderText="JENIS AKTIVITAS"></asp:BoundColumn>
                    <asp:BoundColumn DataField="ActivityUser" HeaderText="OLEH"></asp:BoundColumn>
                </Columns>
            </asp:DataGrid>
        </div>
    </div>
    <%--<div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
