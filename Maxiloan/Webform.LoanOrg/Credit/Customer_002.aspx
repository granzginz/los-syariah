﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Customer_002.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.Customer_002" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer</title>
    <link rel="Stylesheet" type="text/css" href="../../Include/general.css" />
    <link rel="Stylesheet" type="text/css" href="../../Include/Buttons.css" />
    <script language="JavaScript" type="text/javascript">
        function OpenCust(CustomerID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width;
            var y = screen.height - 100;
            window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?Style=' + pStyle + '&CustomerID=' + CustomerID, 'UserLookup', 'left = 0, top = 0, width = ' + x + ', height = ' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <asp:Panel ID="pnlPersonal" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DATA CUSTOMER
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama
                    </label>
                    <%--modify by NOFI 6Feb2018--%> 
                     <asp:HiddenField runat="server" ID="lblPendidikan" />      
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Nomor Identitas
                    </label>
                    <asp:Label ID="lblIDNumber" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Tanggal Lahir
                    </label>
                    <asp:Label ID="lblBirthDate" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jenis Identitas
                    </label>
                    <asp:Label ID="lblIDType" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama Ibu Kandung
                    </label>
                    <asp:Label ID="lblNamaIbuKandung" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Status Perkawinan
                    </label>
                    <asp:DropDownList ID="cboPMarital" Visible="false" runat="server"></asp:DropDownList>
                    <asp:Label ID="lblStatusPerkawinan" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        No. Kartu Keluarga
                    </label>
                    <asp:Label ID="lblNoKK" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Nama Suami/Istri
                    </label>
                    <asp:Label ID="lblNamaSI" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DATA CUSTOMER YANG MIRIP
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgList1" runat="server" DataKeyField="Name" CellSpacing="1" CellPadding="3"
                    AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <ItemStyle CssClass="short_col"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblNo1" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCustName" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                </asp:LinkButton>
                                <asp:Label ID="lblCust" runat="server" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="IDType" HeaderText="JENIS ID" ItemStyle-CssClass="medium_col">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="IDNumber" HeaderText="NO IDENTITAS" ItemStyle-CssClass="medium_col">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="BirthDate" HeaderText="TGL LAHIR" DataFormatString="{0:dd/MM/yyyy}"
                            ItemStyle-CssClass="medium_col"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Type" ItemStyle-CssClass="medium_col" HeaderText="KESAMAAN">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkNewApp" runat="server" CausesValidation="false" Text="New Application"
                                    CommandName="NewApp" CommandArgument='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DAFTAR NEGATIF
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgList2" runat="server" DataKeyField="Name" CellSpacing="1" CellPadding="3"
                    AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle CssClass="short_col" />
                            <ItemStyle CssClass="short_col" />
                            <ItemTemplate>
                                <asp:Label ID="lblNo2" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCustName2" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                </asp:LinkButton>
                                <asp:Label ID="lblCust2" runat="server" Visible="False" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="IDType" HeaderText="JENIS ID" HeaderStyle-CssClass="medium_col"
                            ItemStyle-CssClass="medium_col"></asp:BoundColumn>
                        <asp:BoundColumn DataField="IDNumber" HeaderText="NO IDENTITAS" HeaderStyle-CssClass="medium_col"
                            ItemStyle-CssClass="medium_col"></asp:BoundColumn>
                        <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="BirthDate" HeaderText="TGL LAHIR"
                            DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-CssClass="medium_col" ItemStyle-CssClass="medium_col">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Type" HeaderText="KESAMAAN" HeaderStyle-CssClass="medium_col"
                            ItemStyle-CssClass="medium_col"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlCompany" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h4>
                    DATA CUSTOMER
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div>
                <div class="form_left">
                    <label>
                        Nama
                    </label>
                    <asp:Label ID="lblCName" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        NPWP
                    </label>
                    <asp:Label ID="lblCNPWP" runat="server"></asp:Label>
                </div>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DATA CUSTOMER YANG MIRIP</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCList1" runat="server" DataKeyField="Name" CellSpacing="1" CellPadding="3"
                    AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle CssClass="short_col" />
                            <ItemStyle CssClass="short_col" />
                            <ItemTemplate>
                                <asp:Label ID="lblCNo1" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCName1" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                </asp:LinkButton>
                                <asp:Label ID="lblCName1" Visible="false" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="NPWP" HeaderText="NPWP" HeaderStyle-CssClass="medium_col"
                            ItemStyle-CssClass="medium_col"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Type" HeaderText="JENIS" HeaderStyle-CssClass="medium_col"
                            ItemStyle-CssClass="medium_col"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkNewApp" runat="server" CausesValidation="false" Text="New Application"
                                    CommandName="NewApp" CommandArgument='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    DAFTAR NEGATIF</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="dtgCList2" runat="server" DataKeyField="Name" CellSpacing="1" CellPadding="3"
                    BorderWidth="0" AutoGenerateColumns="False" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="NO">
                            <HeaderStyle CssClass="short_col" />
                            <ItemStyle CssClass="short_col" />
                            <ItemTemplate>
                                <asp:Label ID="lblCNo2" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="NAMA">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCName2" runat="server" CausesValidation="false" Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'>
                                </asp:LinkButton>
                                <asp:Label ID="lblCName2" runat="server" Visible="false" Text='<%# DataBinder.eval(Container,"DataItem.CustomerID") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="NPWP" HeaderText="NPWP" HeaderStyle-CssClass="medium_col"
                            ItemStyle-CssClass="medium_col"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Type" HeaderText="JENIS" HeaderStyle-CssClass="medium_col"
                            ItemStyle-CssClass="medium_col"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnCNext" runat="server" Text="Next" CssClass="small button green" />
            <asp:Button ID="btnCCancel" runat="server" Text="Back" CssClass="small button gray" />
        </div>
    </asp:Panel>
    </form>
</body>
</html>
