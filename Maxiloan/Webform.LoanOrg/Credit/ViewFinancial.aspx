﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewFinancial.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.ViewFinancial" %>

<%@ Register Src="../../webform.UserController/ucViewApplication.ascx" TagName="ucViewApplication" TagPrefix="uc3" %>
<%@ Register Src="../../webform.UserController/ucViewCustomerDetail.ascx" TagName="ucViewCustomerDetail" TagPrefix="uc2" %>
<%@ Register Src="../../Webform.UserController/ucFlagTransaksi.ascx" TagName="ucFlagTransaksi" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>View Financial Data</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Lookup.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <style>
       .form_left, .form_right { width: 47.9% !important; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - FINANCIAL
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="title_strip">
        </div>
        <div>
            <div class="form_left">
                <uc3:ucviewapplication id="ucViewApplication1" runat="server" />
            </div>
            <div class="form_right">
                <uc2:ucviewcustomerdetail id="ucViewCustomerDetail1" runat="server" />
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                ENTRI FINANSIAL</h4>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                 <label >
                    Angsuran Pertama
                </label>
                <asp:DropDownList Visible="false" ID="cboFirstInstallment" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AD">Advance</asp:ListItem>
                    <asp:ListItem Value="AR">Arrear</asp:ListItem>
                </asp:DropDownList>
                <asp:Label runat="server" ID="lblFirstInstallment"></asp:Label>
            </div>
            <div class="form_right">
                <label >
                    Angsuran Tidak Bayar
                </label>
                <asp:Label runat="server" ID="lblTidakAngsur"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                 <label>
                    Refund Supplier digabung 
                </label>
                <asp:Label runat="server" ID="lblGabungRefundSupplier"></asp:Label>
            </div>
            <div class="form_right">
                <label class="label_general">
                    Potong Pencairan Dana
                </label>
                <asp:RadioButtonList runat="server" Visible="false" ID="optPotongPencairanDana" RepeatDirection="Horizontal"
                    CssClass="opt_single">
                    <asp:ListItem Text="Ya" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Tidak" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
                <asp:Label runat="server" ID="lblPotongPencairanDana"></asp:Label>
            </div>
        </div>
    </div>
 
    <div class="form_box_header">
        <div>
            <div class="form_left">
                <h5>
                    DATA FINANSIAL</h5>
            </div>
            <div class="form_right">
                <h5>
                    TOTAL BAYAR PERTAMA</h5>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    OTR Asset
                </label>
                <asp:Label ID="lblOTR" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Uang Muka
                </label>
                <asp:Label runat="server" ID="lblUangMuka" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
            <%--tidak perlu dtampilkan--%>
            <div class="form_right" style="display: none">
                <label>
                    Total OTR
                </label>
                <asp:Label ID="lblOTRSupplier" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Karoseri
                </label>
                <asp:Label runat="server" ID="lblKaroseri" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Administrasi
                </label>
                <asp:Label ID="lblAdminFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                <asp:Label ID="lblIsAdminFeeCredit" runat="server" Visible="false"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Total OTR
                </label>
                <asp:Label runat="server" ID="lblTotalOTR" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Fidusia
                </label>
                <asp:Label runat="server" ID="lblBiayaFidusia" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Uang Muka OTR
            </label>            
            <asp:Label runat="server" ID="lblDPPersen" CssClass="numberAlign readonly_text"></asp:Label>%&nbsp;                   
            <asp:Label runat="server" ID="lblDP" CssClass="numberAlign2 regular_text"></asp:Label>            
        </div>
        <div class="form_right">
            <label>
                Biaya Polis
            </label>
            <asp:Label ID="lblBiayaPolis" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
        </div>
    </div>

        <div class="form_box">
        <div class="form_left">
            <label>
                Uang Muka Karoseri
            </label>            
            <asp:Label runat="server" ID="lblDPPersenKaroseri" CssClass="numberAlign readonly_text"></asp:Label>%&nbsp;                   
            <asp:Label runat="server" ID="lblDPKaroseri" CssClass="numberAlign2 regular_text"></asp:Label>            
        </div>
        <div class="form_right">
        </div>
    </div>


    <div class="form_box">
        <div class="form_left border_sum">
            <label class="label_calc2">
                +
            </label>
            <label>
                Asuransi Pembiayaan
            </label>
            <asp:Label ID="lblAssetInsurance3" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Lainnya
            </label>
            <asp:Label ID="lblOtherFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Pokok Hutang (NTF)
                </label>
                <asp:Label ID="lblNTF" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Provisi
                </label>
                <asp:Label ID="lblBiayaProvisi" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
            <%--tidak perlu dtampilkan--%>
            <div class="form_right" style="display: none;">
                <label>
                    Pokok Hutang (NTF)
                </label>
                <asp:Label ID="lblNTFSupplier" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left border_sum">
                Total Margin 
                <label class="label_auto" style="padding-left: 198px;" > Flat </label>
                 <asp:Label runat="server" ID="lblFlatRate" CssClass=" smaller_text"></asp:Label>
                 <label class="label_auto  ">%&nbsp;</label>
                  <label class="label_auto  ">&nbsp;Efektif&nbsp;</label>   
                <asp:Label runat="server" ID="lblEffectiveRate" CssClass="  smaller_text readonly_text"></asp:Label>
                <label class="label_auto  ">%&nbsp;</label> 
                <asp:Label runat="server" ID="lblTotalBunga" CssClass="numberAlign2  readonly_text"></asp:Label>
            </div>
        </div>
        <div class="form_right">
            <label>
                Asuransi Tunai
            </label>
            <asp:Label ID="lblAssetInsurance32" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
        </div>
    </div>
       <div class="form_box">
                <div>
                    <div class="form_left  border_sum">
                        Penambahan Margin dari asuransi
                         <label class="label_calc2">
                             +
                         </label>
                         <asp:Label ID="lblInsAmountToNPV" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right  border_sum">
                        <label class="label_calc2">
                            +
                        </label>
                        <label>
                            Angsuran Pertama
                        </label>
                        <asp:Label ID="lblAngsuranPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                        
                    </div>
                </div>
            </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                 Jumlah A/R (Nilai Kontrak)
            </label>
            <asp:Label runat="server" ID="lblNilaiKontrak" CssClass="numberAlign2 readonly_text"></asp:Label>
        </div>
        <div class="form_right  border_sum">
            <label class="label_calc2">
                +
            </label>
           <label>
                Total
            </label>
            <asp:Label ID="lblTotalBayarPertama" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
           
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label >
                Jangka Waktu
            </label>
            <asp:Label runat="server" ID="lblNumInst" CssClass="numberAlign2 smaller_text"></asp:Label>
            <div class="form_box_hide">
                Tenor
                <asp:Label ID="lblTenor" runat="server"></asp:Label>
            </div>
        </div>
         <div class="form_right" style="background-color: #f9f9f9">
                <h5>
                    PREMI ASURANSI</h5>
            </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left box_important">
                <asp:Label ID="lblInstallment" runat="server" CssClass="label label_req">Angsuran Per bulan</asp:Label>
                <asp:Label runat="server" ID="lblInstAmt" CssClass="numberAlign2"></asp:Label>
            </div>            
            <div class="form_right" >
                <label>
                    Premi Asuransi 
                </label>
                <asp:Label runat="server" ID="lblPremiAsuransiGross" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>
        </div>       
    </div>
    <div class="form_box">
     
            <div class="form_left" style="background-color: #f9f9f9">
                 <h5>
                    Biaya Provisi</h5>
            </div>
            <div class="form_right">
               <label>
                    Premi Asuransi Maskapai Nett
                </label>
                <asp:Label runat="server" ID="lblPremiAsuransiNet" CssClass="numberAlign2 regular_text"></asp:Label>
            </div>            
        </div>

     <div class="form_box">
     
            <div class="form_left">
                <label>
                    Biaya Provisi</label>
                <asp:Label runat="server" ID="lblBiayaProvisiN" CssClass="numberAlign2 reguler_text"></asp:Label>
                <label class="label_auto numberAlign2">
                    %&nbsp;
                </label>
                <asp:Label runat="server" ID="lblBiayaProvisiAwal" CssClass="numberAlign2 smaller_text"></asp:Label>
                 </div> 
            <div class="form_right">
                <label>
                    Diskon Premi</label>
                <asp:Label runat="server" ID="lblSelisihPremi" CssClass="numberAlign2 regular_text"
                    Style="font-weight: bold"></asp:Label>
            </div>
        </div>


    <div class="form_box">
             
                    <div class="form_left" style="background-color:#FCFCCC">
                        <label>Refund Biaya Provisi</label>
                        <asp:Label runat="server" ID="lblRefundBiayaprovisiPersent" CssClass="numberAlign2 reguler_text"></asp:Label>
                <label class="label_auto numberAlign2">
                    %&nbsp;
                </label>
                <asp:Label runat="server" ID="lblRefundBiayaprovisi" CssClass="numberAlign2 smaller_text"></asp:Label>
                 </div> 
             <div class="form_right  border_sum"  style="background-color:#FCFCCC">
                        <label class="label_calc2">
                            -
                        </label>
                        <label>Refund Premi</label>
                     <asp:Label runat="server" ID="lblRefundPremiPersent" CssClass="numberAlign2 reguler_text"></asp:Label>
                <label class="label_auto numberAlign2">
                    %&nbsp;&nbsp;&nbsp;
                </label>
                <asp:Label runat="server" ID="lblRefundPremi" CssClass="numberAlign2 smaller_text"></asp:Label>
                    </div>
                </div>
          
 
    <div class="form_box">
        <div>
            <div class="form_left" style="background-color: #f9f9f9">
                <h5>
                    Refund Margin</h5>
            </div>
            <div class="form_right" style="background-color: #FCFCCC">
                <label>
                    <b>Pendapatan / Pengeluaran Asuransi</b></label>
              
               
                <asp:Label runat="server" ID="lblPendapatanPremiN" CssClass="numberAlign2 reguler_text"></asp:Label>
                <label class="label_auto numberAlign2">
                    %&nbsp;
                </label>
                <asp:Label runat="server" ID="lblPendapatanPremi" CssClass="numberAlign2 smaller_text"></asp:Label>
            </div>
        </div>
    </div>
       
    <div class="form_box">
        <div>
            <div class="form_left">
                <asp:Label ID="MaxRefundBungaP" runat="server" Style="display: none"></asp:Label>
                <asp:Label ID="MaxRefundBungaN" runat="server" Style="display: none"></asp:Label>
                <label>
                    Refund Margin</label>
                <asp:Label runat="server" ID="lblRefundBungaN" CssClass="numberAlign2 reguler_text"></asp:Label>
                <label class="label_auto numberAlign2">
                    %&nbsp;
                </label>
                <asp:Label runat="server" ID="lblRefundBunga" CssClass="numberAlign2 smaller_text"></asp:Label>
            </div>
            <div class="form_right" style="background-color: #FCFCCC">
                
               
                  <label>
                    <b>Refund Biaya Admin</b></label>
         
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left" style="background-color: #FCFCCC">
                <label>
                    Subsidi Dealer</label>
               
            </div>
            <div class="form_right">
                        <label>Biaya Admin</label>
                        <asp:Label ID="lblRAdminFee" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                        
                    </div>
        </div>
    </div>
   
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Margin</label>
                         <asp:Label ID="lblSubsidiBungaDealer" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                   
                    </div>
                    <div class="form_right"  style="background-color:#FCFCCC">
                        <label>Refund Biaya Admin</label>
                        
                         <asp:Label ID="lblRefundAdminPersent" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                         <label class="label_auto numberAlign2">
                    %&nbsp;&nbsp;
                </label>
                        <asp:Label ID="lblRefundAdmin" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                </div>
            </div>
             <div class="form_box">
                <div>
                    <div class="form_left">
                        <label>Subsidi Angsuran</label>
                           <asp:Label ID="lblSubsidiAngsuran" runat="server" CssClass="numberAlign2 regular_text"></asp:Label>
                    </div>
                    <div class="form_right">&nbsp;</div>
                </div>
            </div>

    
    <div class="form_box_header">
        <div class="form_left">
            <asp:GridView runat="server" ID="gvBungaNet" AllowPaging="false" AutoGenerateColumns="false"
                DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                ShowHeader="false">
                <HeaderStyle CssClass="th" />
                <RowStyle CssClass="item_grid" />
                <FooterStyle CssClass="item_grid ft" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblTransDesc1" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <label class="label_auto">
                                Margin Nett</label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Nilai" DataFormatString="{0:N0}" HeaderStyle-CssClass="th_right"
                        ItemStyle-CssClass="item_grid_right" FooterStyle-CssClass="item_grid_right ft" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblRate" Text='<%# math.round(Container.DataItem("Rate"),2) %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label runat="server" ID="lblTotalRate"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="BungaNett" />
                </Columns>
            </asp:GridView>
            <asp:GridView runat="server" ID="gvNetRateStatus" AllowPaging="false" AutoGenerateColumns="false"
                CssClass="grid_general" ShowHeader="false">
                <HeaderStyle CssClass="th" />
                <RowStyle CssClass="item_grid" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <label>
                                Status Rate</label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="RateTypeResult" />
                </Columns>
            </asp:GridView>
        </div>
        <div class="form_right">
            <asp:GridView runat="server" ID="gvPencairan" AllowPaging="false" AutoGenerateColumns="false"
                DataKeyNames="TransID,ApplicationID" CssClass="grid_general" ShowFooter="true"
                ShowHeader="false">
                <HeaderStyle CssClass="th" />
                <RowStyle CssClass="item_grid" />
                <FooterStyle CssClass="item_grid ft" />
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblTransDesc2" Text='<%# Container.DataItem("TransDesc") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <label class="label_auto">
                                Total Pencairan</label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right"
                        FooterStyle-CssClass="item_grid_right ft">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblNilai" Text='<%# formatnumber(Container.DataItem("Nilai"),0) %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label runat="server" ID="lblTotalPencairan"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FlagCustomer" Visible="false" />
                    <asp:BoundField DataField="FlagSupplier" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
   <%-- <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>--%>
    </form>
</body>
</html>
