﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerCompanyManagement.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerCompanyManagement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>
<%@ Register Src="../../webform.UserController/ucKategoriPerusahaan.ascx" TagName="ucKategoriPerusahaan"
    TagPrefix="uc3" %>
<%@ Register Src="../../webform.UserController/ucKondisiKantor.ascx" TagName="ucKondisiKantor"
    TagPrefix="uc4" %>
<%@ Register Src="../../webform.UserController/ucLookupGroupCust.ascx" TagName="ucLookupGroupCust"
    TagPrefix="uc5" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="CustomerTabsCompany.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Company</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
        <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Page_Validators = new Array();

        function BusinessPlaceStatus() {
            var myID = 'txtTanggalSewa';
            var objDate = eval('document.forms[0].' + myID);
            if (document.forms[0].cboBusPlaceStatus.value == 'KR') {
                objDate.disabled = false;
                ValidatorEnable(document.getElementById("rfvRentFinish"), true);
            }
            else {
                objDate.disabled = true;
                objDate.value = '';
                ValidatorEnable(document.getElementById("rfvRentFinish"), false);
            }


        }			
    </script>
</head>
<body>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div runat="server" id="jlookupContent" />
         <uct:tabs id='cTabs' runat='server'></uct:tabs> 
    <%--<div class="tab_container">
        <div id="tabDataKonsumen" runat="server">
            <asp:HyperLink ID="hyDataKonsumen" runat="server" Text="DATA KONSUMEN"></asp:HyperLink>
        </div>
        <div id="tabLegalitas" runat="server">
            <asp:HyperLink ID="hyLegalitas" runat="server" Text="LEGALITAS"></asp:HyperLink>
        </div>
        <div id="tabManagement" runat="server">
            <asp:HyperLink ID="hyManagement" runat="server" Text="MANAGEMENT"></asp:HyperLink>
        </div> 
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>              
    </div>--%>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">                    
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        CORPORATE CUSTOMER
                    </h4>
                </div>
            </div>            
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DATA PENANGGUNG JAWAB
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Nama</label>
                    <asp:TextBox runat="server" ID="txtNamaPJ" CssClass="long_text"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Harap isi nama penanggung jawab!"
                        ControlToValidate="txtNamaPJ" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jabatan</label>
                    <asp:DropDownList ID="cboJabatan" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box_uc">
                
                    <uc1:UcCompanyAdress id="UCAddressPJ" runat="server"></uc1:UcCompanyAdress>
         
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        No. Handphone</label>
                    <asp:TextBox runat="server" MaxLength="15" ID="txtNoHP" onkeypress="return numbersonly2(event)"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label >
                        Email</label>
                    <asp:TextBox runat="server" ID="txtEmail"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic"
                        ControlToValidate="txtEmail" ErrorMessage="Email Salah" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        CssClass="validator_general"></asp:RegularExpressionValidator>
                </div>
            </div>
            <%--edit npwp, ktp, telepon by ario--%>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        NPWP</label>
                    <asp:TextBox runat="server" ID="txtNPWP" MaxLength="15" CssClass="medium_text" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegtxtPNPWP" Display="Dynamic" ControlToValidate="txtNPWP"
                        runat="server" ErrorMessage="Masukkan angka saja" ValidationExpression="\d+"
                        CssClass="validator_general"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNPWP"
                        ErrorMessage="Harap isi Nomor NPWP" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
                </div>
            </div>            
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        MANAGEMENT DAN KOMPOSISI PEMEGANG SAHAM
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ws">
                        <asp:DataGrid ID="dtgList1" runat="server" Width="1200px" CellSpacing="1" CellPadding="3"
                            BorderWidth="0" AutoGenerateColumns="False" DataKeyField="Name" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo1" runat="server" Width="10"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NAMA">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtName1" runat="server" MaxLength="50" Width="200"></asp:TextBox>
                                        <asp:Label ID="lblVName1" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                        <a href="Customer_002.aspx"></a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JENIS KELAMIN">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cboGender" runat="server">
                                            <asp:ListItem Value="" Selected="True">Select One</asp:ListItem>
                                            <asp:ListItem Value="M">Laki - Laki</asp:ListItem>
                                            <asp:ListItem Value="F">Perempuan</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:Label ID="lblVGender" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="NO IDENTITAS">
                                    <ItemTemplate>
                                        <%--<asp:TextBox ID="txtIDNumber1" runat="server" MaxLength="25" onkeypress="return numbersonly2(event)"></asp:TextBox>--%>
                                        <asp:TextBox ID="txtIDNumber1" runat="server" MaxLength="25"></asp:TextBox>
                                        <asp:Label ID="lblVIDNumber1" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JABATAN">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cboPosition1" runat="server">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblVPosition1" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ALAMAT">
                                    <ItemStyle CssClass="address_col"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtAddress1" runat="server" MaxLength="100" Width="300"></asp:TextBox>
                                        <asp:Label ID="lblVAddress1" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TELEPON">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtPhone1" runat="server" MaxLength="10" Width="100"></asp:TextBox>
                                        <asp:Label ID="lblVPhone1" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                            ControlToValidate="txtPhone1" ErrorMessage="No Telepon Salah" ValidationExpression="\d*"
                                            Display="Dynamic" CssClass="validator_general"></asp:RegularExpressionValidator>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SAHAM(%)">
                                    <ItemTemplate>
                                        <uc1:ucnumberformat id="txtShare1" runat="server" maxlength="15"></uc1:ucnumberformat>
                                        <asp:Label ID="lblVShare1" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PERSETUJUAN">
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkPersetujuan"/>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete1" Width="20" runat="server" ImageUrl="../../Images/icondelete.gif"
                                            CausesValidation="False" CommandName="Delete1"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAdd1" runat="server" Text="Add" CssClass="small button blue" CausesValidation="False">
                </asp:Button>
            </div>            
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
