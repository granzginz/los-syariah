﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewIncentiveData_001.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.ViewIncentiveData_001" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewIncentiveData</title>
    <link href="../../Include/<%= request("style") %>.css" type="text/css" rel="stylesheet" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';			
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - DATA INCENTIVE
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    No Aplikasi
                </label>
                <asp:HyperLink ID="hyApplicationID" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Nama Customer
                </label>
                <asp:HyperLink ID="HyCust" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Refund Premi Supplier
                </label>
                <asp:Label ID="lblRefundPremi" runat="server" Width="144px"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Premi Dasar ke Supplier
                </label>
                <asp:Label ID="lblPremiumBase" runat="server" Width="144px"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <div class="form_box">
        <div>
            <div class="form_left">
                <label>
                    Refund Premi ke Supplier
                </label>
                <asp:Label ID="txtRfnSupplier" runat="server" Width="144px"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
    </div>
    <asp:Panel ID="PnlDistribution" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DISTRIBUSI DATA SUUPLIER</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <table cellspacing="1" cellpadding="2" width="100%" border="0">
                    <tr class="tdjudul">
                        <td width="20%">
                            PENERIMA
                        </td>
                        <td width="20%">
                            PROSENTASE
                        </td>
                        <td width="20%">
                            JUMLAH TETAP
                        </td>
                        <td width="20%">
                            JUMLAH INCENTIVE
                        </td>
                        <td width="20%">
                            REKENING BANK KARY SUPPLIER
                        </td>
                    </tr>
                    <tr class="tdganjil">
                        <td>
                            Perusahaan
                        </td>
                        <td>
                            <asp:Label ID="lblPCompany" runat="server"></asp:Label>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <asp:Label ID="lblInctvAmountCompany" Width="80%" runat="server"></asp:Label>
                        </td>
                        <td>
                            -
                        </td>
                    </tr>
                    <tr class="tdgenap">
                        <td>
                            GM
                        </td>
                        <td>
                            <asp:Label ID="lblPGM" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblFGM" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblInctvAmountGM" Width="80%" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblAccountGM" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdganjil">
                        <td>
                            BM
                        </td>
                        <td>
                            <asp:Label ID="lblPBM" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblFBM" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblInctvAmountPM" Width="80%" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblAccountBM" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdgenap">
                        <td>
                            ADH
                        </td>
                        <td>
                            <asp:Label ID="lblPADH" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblFADH" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblInctvAmountADH" Width="80%" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblAccountAH" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdganjil">
                        <td>
                            Sales Supervisor
                        </td>
                        <td>
                            <asp:Label ID="lblPSalesSpv" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblFSalesSpv" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblInctvAmountSalesSpv" Width="80%" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblAccountSV" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdgenap">
                        <td>
                            Sales Person
                        </td>
                        <td>
                            <asp:Label ID="lblPSalesPerson" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblFSalesPerson" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblInctvAmountSalesPerson" Width="80%" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblAccountSL" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdganjil">
                        <td>
                            Supplier Admin
                        </td>
                        <td>
                            <asp:Label ID="lblPSuppAdm" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblFSuppAdm" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblInctvAmountSuppAdm" Width="80%" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="LblAccountAM" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="tdjudul">
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Total :
                        </td>
                        <td>
                            <asp:Label ID="lblTotal" Width="80%" runat="server"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="btnClose" OnClientClick="javascript:window.close();" runat="server"
            Text="Close" CssClass="small button gray" CausesValidation="False"></asp:Button>
        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
