﻿#Region "Imports"
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
#End Region

Public Class ViewDetailApproval
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

    Property ApplicationModule() As String
        Get
            Return ViewState("ApplicationModule").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationModule") = Value
        End Set
    End Property

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then

            'If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            Me.ApplicationID = Request("ApplicationID").ToString
                Bindgrid()

            If Me.ApplicationModule.Trim = "FACT" Then
                pnldtlfact.Visible = True
                BindgridFact()
            End If

            If Me.ApplicationModule.Trim = "MDKJ" Then
                pnldtlmdkj.Visible = True
                BindgridMdkj()
            End If

            If Me.ApplicationModule.Trim <> "FACT" And Me.ApplicationModule.Trim <> "MDKJ" Then
                pnldtlmginv.Visible = False
            End If
            ' End If
            Bindgridpencairan()
        End If
    End Sub

    Private Sub Bindgrid()
        Dim dt As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReader As SqlDataReader
        Dim objProsCommand As New SqlCommand

        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spViewCreditProcess"

            If Not Me.ApplicationID Is Nothing Or Me.ApplicationID <> "" Then
                objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
                objCommand.Connection = objConnection
                objReader = objCommand.ExecuteReader()

                If objReader.Read Then
                    lblAgreementNo.Text = objReader.Item("AgreementNo").ToString.Trim
                    lblCustomerName.Text = objReader.Item("CustomerName").ToString.Trim
                    lblSukuBunga.Text = objReader.Item("EffectiveRate").ToString.Trim
                    lblTenor.Text = objReader.Item("Tenor").ToString.Trim
                    'lblAdminFee.Text = objReader.Item("AdminFee").ToString.Trim
                    lblAdminFee.Text = CDbl(objReader.Item("AdminFee")).ToString("#,##0")
                    'lblAsuransiThn1.Text = objReader.Item("AdminFee").ToString.Trim
                    lblAsuransi.Text = CDbl(objReader.Item("paidamountbycust")).ToString("#,##0")
                    'lblPlafond.Text = objReader.Item("FasilitasAmount").ToString.Trim
                    lblPlafond.Text = CDbl(objReader.Item("FasilitasAmount")).ToString("#,##0")
                    'lblAvailableAmount.Text = objReader.Item("FasilitasAmount").ToString.Trim
                    lblAvailableAmount.Text = CDbl(objReader.Item("AvailableAmount")).ToString("#,##0")
                    lblDownPayment.Text = CDbl(objReader.Item("DownPayment")).ToString("#,##0")
                    lblModule.Text = objReader.Item("ApplicationModule").ToString.Trim
                    lblTotalPembiayaan.Text = CDbl(objReader.Item("NTF")).ToString("#,##0")
                    lblBiayaProvisi.Text = CDbl(objReader.Item("ProvisionFee")).ToString("#,##0")
                    lblBiayaHandling.Text = CDbl(objReader.Item("HandlingFee")).ToString("#,##0")
                    lblRetensiRate.Text = CDbl(objReader.Item("RetensiRate")).ToString("#,##0")
                    lblBiayaSurvey.Text = CDbl(objReader.Item("SurveyFee")).ToString("#,##0")
                    lblBiayaNotaris.Text = CDbl(objReader.Item("NotaryFee")).ToString("#,##0")
                    lblTotalBiaya.Text = CDbl(objReader.Item("OTR")).ToString("#,##0")
                    lblNilaiPencairan.Text = CDbl(objReader.Item("NTF")).ToString("#,##0")

                    Me.ApplicationModule = objReader.Item("ApplicationModule").ToString.Trim
                End If
                objReader.Close()


            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Sub BindgridFact()
        'Dim dtGrid As New DataTable
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReaderGrid As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spFactoringInvoiceList"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objReaderGrid = objCommand.ExecuteReader

            dtgDtlFact.DataSource = objReaderGrid
            dtgDtlFact.DataBind()
            objReaderGrid.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Sub BindgridMdkj()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReaderGrid As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spModalKerjaInvoiceList"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objReaderGrid = objCommand.ExecuteReader

            dtgDtlMdkj.DataSource = objReaderGrid
            dtgDtlMdkj.DataBind()
            objReaderGrid.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub

    Sub BindgridInv()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReaderGrid As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spMGInvList"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objReaderGrid = objCommand.ExecuteReader

            dtgDtlMgInv.DataSource = objReaderGrid
            dtgDtlMgInv.DataBind()
            objReaderGrid.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
    Sub Bindgridpencairan()
        Dim objCommand As New SqlCommand
        Dim objConnection As New SqlConnection(GetConnectionString)
        Dim objReaderGrid As SqlDataReader
        Try
            If objConnection.State = ConnectionState.Closed Then objConnection.Open()

            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.CommandText = "spMGInvList2"
            objCommand.Connection = objConnection
            objCommand.Parameters.Add("@ApplicationID", SqlDbType.VarChar, 20).Value = Me.ApplicationID.Trim
            objReaderGrid = objCommand.ExecuteReader

            dtgpencairan.DataSource = objReaderGrid
            dtgpencairan.DataBind()
            objReaderGrid.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        Finally
            If objConnection.State = ConnectionState.Open Then objConnection.Close()
            objConnection.Dispose()
            objCommand.Dispose()
        End Try
    End Sub
End Class