﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerPersonalPinjamanLainnya.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerPersonalPinjamanLainnya" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc6" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Src="../../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat"
    TagPrefix="uc" %>
<%@ Register Src="CustomerTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Customer Personal</title>

    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        var Page_Validators = new Array();
    </script>    
</head>
<body>    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <uct:tabs id='cTabs' runat='server' />
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"  onclick="hideMessage();"></asp:Label>
        <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                DATA PINJAMAN LAINNYA
            </h4>
        </div>
    </div>                        
        <asp:Panel runat="server" ID="pnlKeluarga">        
        <asp:UpdatePanel runat="server" ID="up3">
        <ContentTemplate>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="Datagrid1" runat="server" Width="100%" CellSpacing="1" CellPadding="3"
                            BorderWidth="0" AutoGenerateColumns="False" DataKeyField="Name" CssClass="grid_general">
                            <ItemStyle CssClass="item_grid"></ItemStyle>
                            <HeaderStyle CssClass="th"></HeaderStyle>
                            <Columns>
                             <asp:BoundColumn DataField="No" HeaderText="NO" />
                             <asp:BoundColumn DataField="SeqNo"   visible=false />
                                <asp:TemplateColumn HeaderText="NAMA LEMBAGA KEUANGAN">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtNamaLembagaKeuangan" runat="server" MaxLength="50"   Text='<%# DataBinder.eval(Container,"DataItem.Name") %>'></asp:TextBox>
                                        <asp:Label ID="lblVNamaLembagaKeuangan" runat="server" Visible="False" > <%# DataBinder.eval(Container,"DataItem.Name") %></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JENIS PINJAMAN">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtJenisPinjaman" runat="server" MaxLength="25" Text='<%# DataBinder.eval(Container,"DataItem.JenisPinjaman") %>'></asp:TextBox>
                                        <asp:Label ID="lblVJenisPinjaman" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn HeaderText="JUMLAH PINJAMAN">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                      <uc:ucnumberformat id="txtJumlahPinjaman" runat="server" Text='<%#FormatNumber( DataBinder.eval(Container,"DataItem.JumlahPinjaman"), 0) %>'/>
                                        <asp:Label ID="lblVJumlahPinjaman" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn HeaderText="TENOR">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <uc:ucnumberformat id="txtTenor" runat="server" Text='<%# DataBinder.eval(Container,"DataItem.Tenor") %>' />
                                        <asp:Label ID="lblVTenor" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="SISA POKOK">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <uc:ucnumberformat id="txtSisaPokok" runat="server" Text='<%#FormatNumber( DataBinder.eval(Container,"DataItem.SisaPokok"), 0) %>'/>
                                        <asp:Label ID="lblVSisaPokok" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ANGSURAN">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <uc:ucnumberformat id="txtAngsuran" runat="server" Text='<%#FormatNumber( DataBinder.eval(Container,"DataItem.Angsuran"), 0) %>'/>
                                        <asp:Label ID="lblVAngsuran" runat="server" Visible="False" CssClass="validator_general">*</asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" CausesValidation="false" CommandName="Delete" runat="server"
                                            ImageUrl="../../Images/icondelete.gif"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <asp:Button ID="btnPAdd" runat="server" Text="Add" CssClass="small buttongo blue"
                        CausesValidation="False"></asp:Button>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Panel>        
        <div class="form_button">
        <asp:Button ID="btnPSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnPCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div> 
		</ContentTemplate>
    </asp:UpdatePanel>             
    </form>
</body>
</html>
