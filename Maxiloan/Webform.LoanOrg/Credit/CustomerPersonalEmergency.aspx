﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerPersonalEmergency.aspx.vb"
    Inherits="Maxiloan.Webform.LoanOrg.CustomerPersonalEmergency" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucCompanyAddress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%--<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>--%>
<%@ Register Src="CustomerTabs.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Personal</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
       
        var Page_Validators = new Array();
       
    </script>    
</head>
<body> 
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>   
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <uct:tabs id='cTabs' runat='server'/>
   <%-- <div class="tab_container">
        <div id="tabIdentitas" runat="server">
            <asp:HyperLink ID="hyIdentitas" runat="server" Text="IDENTITAS"></asp:HyperLink>
        </div>
        <div id="tabPekerjaan" runat="server">
            <asp:HyperLink ID="hyPekerjaan" runat="server" Text="PEKERJAAN"></asp:HyperLink>
        </div>
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>
        <div id="tabPasangan" runat="server">
            <asp:HyperLink ID="hyPasangan" runat="server" Text="PASANGAN"></asp:HyperLink>
        </div>
        <div id="tabEmergency" runat="server">
            <asp:HyperLink ID="hyEmergency" runat="server" Text="EMERGENCY CONTACT"></asp:HyperLink>
        </div>
        <div id="tabKeluarga" runat="server">
            <asp:HyperLink ID="hyKeluarga" runat="server" Text="KELUARGA"></asp:HyperLink>
        </div>
    </div>--%>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
        <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"  onclick="hideMessage();"></asp:Label>
        <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h4>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
                DATA EMERGENCY KONTAK
            </h4>
        </div>
    </div>                   
        <asp:Panel runat="server" ID="pnlEmergency">        
        <asp:Panel ID="pnlEmergencyContact" runat="server">       
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Nama</label>
                <asp:TextBox runat="server" ID="txtNamaER"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtNamaER"
                    ErrorMessage="Harap isi nama!" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    Hubungan</label>
                <asp:DropDownList ID="ddlHubungan" runat="server">
                    <asp:ListItem Text="-Pilih Hubungan-" Value="" />
                    <asp:ListItem Text="Orang Tua" Value="Orang Tua" />
                    <asp:ListItem Text="Anak" Value="Anak" />
                    <asp:ListItem Text="Saudara" Value="Saudara" />
                    <asp:ListItem Text="Adik" Value="Adik" />
                    <asp:ListItem Text="Kakak" Value="Kakak" />
                    <asp:ListItem Text="Paman" Value="Paman" />
                    <asp:ListItem Text="Sepupu" Value="Sepupu" />
                    <asp:ListItem Text="Keponakan" Value="Keponakan" />
                    <asp:ListItem Text="Kakek" Value="Kakek" />
                    <asp:ListItem Text="Nenek" Value="Nenek" />
                    <asp:ListItem Text="Teman" Value="Teman" />
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvHubungan" runat="server" ControlToValidate="ddlHubungan"
                    ErrorMessage="Pilih Hubungan" InitialValue="" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_uc">
            <asp:UpdatePanel runat="server" ID="upER">
                <ContentTemplate>
                    <uc1:ucCompanyAddress id="ucAddressER" runat="server"></uc1:ucCompanyAddress>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>        
        <div class="form_box">
            <div class="form_single">
                <label class="label_req">
                    No. HP</label>
                <asp:TextBox runat="server" ID="txtHPER"></asp:TextBox>
                  <asp:RequiredFieldValidator ID="reqHP" runat="server" ControlToValidate="txtHPER"
                ErrorMessage="harap isi No. HP" Display="Dynamic" CssClass="validator_general"/>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Email</label>
                <asp:TextBox runat="server" ID="txtEmailER"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server"
                    ControlToValidate="txtEmailER" ErrorMessage="Penulisan alamat email salah!" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    CssClass="validator_general"></asp:RegularExpressionValidator>
            </div>
        </div>
    </asp:Panel>
    </asp:Panel>            
        <div class="form_button">
        <asp:Button ID="btnPSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnPCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
		</ContentTemplate>
    </asp:UpdatePanel>           
    </form>
</body>
</html>
