﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerCompany.aspx.vb" Inherits="Maxiloan.Webform.LoanOrg.CustomerCompany" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucAddress" Src="../../Webform.UserController/ucAddress.ascx" %>

<%@ Register Src="../../webform.UserController/ucKategoriPerusahaan.ascx" TagName="ucKategoriPerusahaan" TagPrefix="uc3" %>
<%@ Register Src="../../webform.UserController/ucKondisiKantor.ascx" TagName="ucKondisiKantor" TagPrefix="uc4" %>
<%@ Register Src="../../webform.UserController/ucLookupGroupCust.ascx" TagName="ucLookupGroupCust" TagPrefix="uc5" %>
<%@ Register TagPrefix="uc1" TagName="ucNumberFormat" Src="../../Webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="CustomerTabsCompany.ascx" TagName="tabs" TagPrefix="uct" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Customer Company</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>


    <script type="text/javascript">
        var Page_Validators = new Array();

        function BusinessPlaceStatus() {
            var myID = 'txtTanggalSewa';
            var objDate = eval('document.forms[0].' + myID);
            if (document.forms[0].cboBusPlaceStatus.value == 'KR') {
                objDate.disabled = false;
                ValidatorEnable(document.getElementById("rfvRentFinish"), true);
            }
            else {
                objDate.disabled = true;
                objDate.value = '';
                ValidatorEnable(document.getElementById("rfvRentFinish"), false);
            }
        }
        function copyalamat() {
            $("#UCAddress_SIUP_txtAddress").val($("#UCAddress_txtAddress").val());
            $("#UCAddress_SIUP_txtRT").val($("#UCAddress_txtRT").val());
            $("#UCAddress_SIUP_txtRW").val($("#UCAddress_txtRW").val());
            $("#UCAddress_SIUP_oLookUpKodePos_txtKelurahan").val($("#UCAddress_oLookUpKodePos_txtKelurahan").val());
            $("#UCAddress_SIUP_oLookUpKodePos_txtKecamatan").val($("#UCAddress_oLookUpKodePos_txtKecamatan").val());
            $("#UCAddress_SIUP_oLookUpKodePos_txtCity").val($("#UCAddress_oLookUpKodePos_txtCity").val());
            $("#UCAddress_SIUP_oLookUpKodePos_txtZipCode").val($("#UCAddress_oLookUpKodePos_txtZipCode").val());
            $("#UCAddress_SIUP_txtAreaPhone1").val($("#UCAddress_txtAreaPhone1").val());
            $("#UCAddress_SIUP_txtPhone1").val($("#UCAddress_txtPhone1").val());
            $("#UCAddress_SIUP_txtAreaPhone1").val($("#UCAddress_txtAreaPhone1").val());
            $("#UCAddress_SIUP_txtPhone2").val($("#UCAddress_txtPhone2").val());
            $("#UCAddress_SIUP_txtAreaPhone2").val($("#UCAddress_txtAreaPhone2").val());
            $("#UCAddress_SIUP_txtPhone2").val($("#UCAddress_txtPhone2").val());
            $("#UCAddress_SIUP_txtAreaFax").val($("#UCAddress_txtAreaFax").val());
            $("#UCAddress_SIUP_txtFax").val($("#UCAddress_txtFax").val());
        }

        function NamaIndustriBuClick(e, h) {
            var cboselect = $('#cboIndrustriHeader').val();
            if (cboselect == 'Select One') return;
            var url = e + cboselect
            OpenJLookup(url, 'Daftar Industri', h); return false;
        } 
    </script>
     <style >
        .multiline_textbox {
            width: 60% !important;
        }
    </style>
</head>
<body>

    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updatePanel1">
        <ProgressTemplate>
            <div class="progress_bg">
                <label class="progress_img" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <form id="form1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div runat="server" id="jlookupContent" />
     <uct:tabs id='cTabs' runat='server'></uct:tabs> 
   <%-- <div class="tab_container">
        <div id="tabDataKonsumen" runat="server">
            <asp:HyperLink ID="hyDataKonsumen" runat="server" Text="DATA KONSUMEN"></asp:HyperLink>
        </div>
        <div id="tabLegalitas" runat="server">
            <asp:HyperLink ID="hyLegalitas" runat="server" Text="LEGALITAS"></asp:HyperLink>
        </div>
        <div id="tabManagement" runat="server">
            <asp:HyperLink ID="hyManagement" runat="server" Text="MANAGEMENT"></asp:HyperLink>
        </div> 
        <div id="tabKeuangan" runat="server">
            <asp:HyperLink ID="hyKeuangan" runat="server" Text="KEUANGAN"></asp:HyperLink>
        </div>              
    </div> --%>
 
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <%--<asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>--%>
                       <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
                    <h4>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                        CORPORATE CUSTOMER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <h4>
                        DATA PERUSAHAAN
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label> Nama Perusahaan</label>
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                    <asp:TextBox ID="txtCustomerName" Visible="true" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req"> Jenis Perusahaan</label>
                    <asp:DropDownList ID="cboCompType" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                        InitialValue="Select One" ControlToValidate="cboCompType" ErrorMessage="Harap Pilih Jenis Perusahaan"
                        CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <%--<div class="form_box">
                <div class="form_single">
                    <label>
                        NPWP</label>
                    <asp:Label ID="lblNPWP" runat="server"></asp:Label>
                </div>
            </div>--%>
            
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Jumlah Karyawan</label>
                    <asp:TextBox ID="txtNumEmp" runat="server" MaxLength="4" Columns="7"></asp:TextBox>
                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="Dynamic"
                        ControlToValidate="txtNumEmp" ErrorMessage="Harap isi dengan Angka" ValidationExpression="\d*"
                        CssClass="validator_general" SetFocusOnError="true"></asp:RegularExpressionValidator>--%>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Tahun Berdiri</label>
                    <asp:TextBox ID="txtYearOfEst" runat="server" MaxLength="4" Columns="7"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                        ControlToValidate="txtYearOfEst" ErrorMessage="Harap isi dengan Tahun" CssClass="validator_general"></asp:RequiredFieldValidator><asp:RangeValidator
                            ID="RangeValidator5" runat="server" Display="Dynamic" ControlToValidate="txtYearOfEst"
                            ErrorMessage="Tahun Harus lebih kecil dari hari ini" MinimumValue="0" Type="Integer"
                            CssClass="validator_general" SetFocusOnError="true"></asp:RangeValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" Display="Dynamic"
                        ControlToValidate="txtYearOfEst" ErrorMessage="Tahun harus 4 angka" ValidationExpression="\d{4}"
                        CssClass="validator_general" SetFocusOnError="true"></asp:RegularExpressionValidator>
                </div>
            </div>
            

             <div class="form_box">
                        <div class="form_single">                            
                            <label class="label_req"> Bidang Usaha Header</label>
                            <asp:DropDownList ID="cboIndrustriHeader" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Harap pilih Bidang Usaha Header"
                                ControlToValidate="cboIndrustriHeader" InitialValue="Select One" Display="Dynamic"
                                CssClass="validator_general" />
                        </div>
                    </div>
                     
                    <div class="form_box">
                        <div class="form_single">
                            <label class="label_req"> Bidang Usaha Detail</label> 
                            <asp:HiddenField runat="server" ID="hdfKodeIndustriBU" /> 
                            <asp:TextBox runat="server" ID="txtNamaIndustriBU" Enabled="false" CssClass="medium_text" text="-"/>
                            <%--<button class="small buttongo blue"  onclick ="OpenJLookup('<%= ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) & "&header=" & '$('#cboIndrustriHeader').val()' %>','Daftar Industri','<%= jlookupContent.ClientID %>');return false;">...</button>--%>                                                                            
                            <button class="small buttongo blue"  onclick ="NamaIndustriBuClick('<%=ResolveClientUrl("~/webform.Utility/jLookup/Industri.aspx?kode=" & hdfKodeIndustriBU.ClientID & "&nama=" & txtNamaIndustriBU.ClientID) & "&header=" %>','<%= jlookupContent.ClientID %>');">...</button>
                            <asp:RequiredFieldValidator ID="rfvtxtKodeIndustriBU" runat="server" Display="Dynamic"  CssClass="validator_general" ControlToValidate="txtNamaIndustriBU" />
                        </div>
                    </div>



            <div class="form_box">
                <div class="form_single">
                    <label>
                        Keterangan Usaha</label>
                    <asp:TextBox runat="server" ID="txtKeteranganUsaha" CssClass="long_text"></asp:TextBox>
                </div>
            </div>
            <div class="form_box">
                        <div class="form_single">
                            <label>
                                Industry Risk Rating</label>
                            <asp:DropDownList ID="cboIndustryRisk" runat="server">
                                <asp:ListItem Value="5" Selected="True">DARK GREEN</asp:ListItem>
                                <asp:ListItem Value="4">GREEN</asp:ListItem>
                                <asp:ListItem Value="3">LIGHT YELLOW</asp:ListItem>
                                <asp:ListItem Value="2">YELLOW</asp:ListItem>
                                <asp:ListItem Value="1">RED</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        status Usaha</label>
                    <asp:DropDownList runat="server" ID="cboStatusUsaha">
                        <asp:ListItem Value="PU">PUSAT</asp:ListItem>
                        <asp:ListItem Value="CA">CABANG</asp:ListItem>
                        <asp:ListItem Value="PW">PERWAKILAN</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Kategori Perusahaan</label>
                    <uc3:uckategoriperusahaan id="ucKategoriPerusahaanCustomer" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Kondisi Kantor</label>
                    <uc4:uckondisikantor id="ucKondisiKantorCustomer" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Kondisi Lingkungan Setempat</label>
                    <uc4:uckondisikantor id="ucKondisiLingkungan" runat="server" />
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Status Kantor</label>
                    <asp:DropDownList ID="cboBusPlaceStatus" AutoPostBack="true" OnSelectedIndexChanged="cboBusPlaceStatus_IndexChanged"
                        runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Harap pilih status kantor!"
                        ControlToValidate="cboBusPlaceStatus" InitialValue="" CssClass="validator_general"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Sejak Tahun</label>
                    <asp:TextBox ID="txtBPYear" runat="server" MaxLength="4" Columns="7" onkeypress="return numbersonly2(event)"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator6" runat="server" Display="Dynamic" ControlToValidate="txtBPYear"
                        ErrorMessage="Tahun harus lebih kecil dari hari ini" MinimumValue="0" Type="Integer"
                        CssClass="validator_general"></asp:RangeValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                        Display="Dynamic" ControlToValidate="txtBPYear" ErrorMessage="Harap isi dengan 4 angka"
                        ValidationExpression="\d{4}" CssClass="validator_general"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Harap isi kolom sejak tahun"
                        ControlToValidate="txtBPYear" InitialValue="" CssClass="validator_general"
                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box" id="tglBerSewadiv" runat="server">
                <div class="form_single">
                    <label class="label_req">
                        Tanggal Berakhirnya Sewa</label>
                    <asp:TextBox runat="server" ID="txtTanggalSewa" CssClass="small_text"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtTanggalSewa" Format="dd/MM/yyyy"> </asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rfvRentFinish" runat="server" ErrorMessage="Harap isi tanggal berakhir sewa!" ControlToValidate="txtTanggalSewa" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblVRent" runat="server" Visible="False">Tanggal Sewa berakhir Harus lebih besar dari hari ini</asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label> Kelompok Usaha</label>
                    <asp:TextBox ID="txtKelompokUsaha" Enabled="false" runat="server" CssClass="long_text"></asp:TextBox>
                    <asp:Button ID="btnLookupCustomer" runat="server" CausesValidation="False" Text="..." CssClass="small buttongo blue" />
                    <uc5:uclookupgroupcust id="ucLookupGroupCust1" runat="server" oncatselected="CatSelectedCustomer" />
                </div>
            </div>

            <div class="form_box">
                <div class="form_single">
                    <label class="label_req"> Jenis Pembiayaan</label>
                    <asp:DropDownList ID="cboJenisPembiayaan" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic" InitialValue="Select One" ControlToValidate="cboJenisPembiayaan" ErrorMessage="Harap Pilih Jenis Pembiayaan" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Nasabah Operating Lease</label>
                    <asp:checkbox ID="cboIsOLS" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic" InitialValue="Select One" ControlToValidate="cboJenisPembiayaan" ErrorMessage="Harap Pilih Jenis Pembiayaan" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">Nasabah BUMN</label>
                    <asp:checkbox ID="isBUMN" runat="server" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic" InitialValue="Select One" ControlToValidate="cboJenisPembiayaan" ErrorMessage="Harap Pilih Jenis Pembiayaan" CssClass="validator_general" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form_box_header">
                <div class="form_box_header_l">
                    <h4> ALAMAT DOMISILI</h4>
                </div>
                <div class="form_box_header_r">
                    <h4>
                        ALAMAT SIUP</h4>
                </div>
            </div>
                 <%--<div class="form_box">
                    <div class="form_box_single">
                       
                             <button onclick="copyalamat()" class="small buttongo blue label_calc">Copy Alamat Domisili</button>
                    </div>
                </div>--%>
             <div class="form_box">
                <div class="form_box_single">
                    <asp:Button ID="btnPLegalAddress" CausesValidation="false" runat="server" Text="Copy Alamat Domisili"
                        CssClass="small buttongo blue label_calc"></asp:Button>
                </div>
            </div>
            <div class="form_box_uc">
                <div class="form_left_uc">
                    <uc1:UcCompanyAdress id="UCAddress" runat="server"></uc1:UcCompanyAdress>
                </div>
                <div class="form_right_uc">
                    <uc1:UcCompanyAdress id="UCAddress_SIUP" runat="server"></uc1:UcCompanyAdress>
                </div>
            </div>
            <div class="form_box_uc">
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label >
                        Website</label>
                    <asp:TextBox runat="server" ID="txtWebsite" CssClass="long_text"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Format nama website salah!"
                        Display="Dynamic" ValidationExpression="([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=])?"
                        ControlToValidate="txtWebsite" CssClass="validator_general"></asp:RegularExpressionValidator>
                </div>
            </div>            
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
   <%-- </asp:Content>--%>
 </form>
</body>
</html> 
