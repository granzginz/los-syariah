﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class Application_002KPR
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcAgreementListNew As UcAgreementListNew

#Region "Constanta"
    Private m_controller As New ApplicationController
    Private ocustomclass As New Parameter.Application
#End Region

#Region "Property"
    Property Type() As String
        Get
            Return ViewState("Type").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("Type") = Value
        End Set
    End Property
    Property CustomerID() As String
        Get
            Return ViewState("CustomerID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerID") = Value
        End Set
    End Property
    Property CustomerName() As String
        Get
            Return ViewState("CustomerName").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.CustomerID = Request("id")
            Me.CustomerName = Request("name")
            Me.Type = Request("type")
            Bind1()
            Me.SortBy = "Name ASC"

            If Request("Application") <> "" Then
                Dim script As String
                script = "<script language=""JavaScript"">" & vbCrLf
                script &= "alert('Your Application Form No. is " & Request("Application").ToString.Trim & "');" & vbCrLf
                script &= "</script>"
                Response.Write(script)
            End If

            'Modify by Wira 20191125

            'end modify n
        End If
        lblCustName.Text = Me.CustomerName
    End Sub
    Sub Bind1()
        UcAgreementListNew.Style = "AccAcq"
        UcAgreementListNew.CustomerID = Me.CustomerID
        UcAgreementListNew.BindAgreement()
        If Not UcAgreementListNew.DtgRows() > 0 Then
            Response.Redirect("Application_003KPR.aspx?id=" & Me.CustomerID & "&name=" & Me.CustomerName & "&type=" & Me.Type & "&page=Add&flag=0")
        End If
    End Sub
    Private Sub imbNewApp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewApp.Click
        Response.Redirect("Application_003KPR.aspx?id=" & Me.CustomerID & "&name=" & Me.CustomerName & "&type=" & Me.Type & "&page=Add&flag=0")
    End Sub
    Private Sub imbExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("ApplicationKPR.aspx")
    End Sub



End Class