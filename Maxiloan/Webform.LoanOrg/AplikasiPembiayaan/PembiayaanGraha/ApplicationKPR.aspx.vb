﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class ApplicationKPR
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ApplicationController
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1

    Protected WithEvents Rangevalidator1 As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents rfvGo As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rbNewCust As System.Web.UI.WebControls.RadioButton
    Protected WithEvents rbROCust As System.Web.UI.WebControls.RadioButton

    Private recordCount As Int64 = 1
#End Region
#Region "Property"
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
#Region "Page_Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        lblMessage.Text = ""
        lblMessage.Visible = False
        If Not Page.IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then
                txtGoPage.Text = "1"
                If CheckForm(Me.Loginid, "Application", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                If Request("Application") <> "" Then
                    Dim script As String
                    script = "<script language=""JavaScript"">" & vbCrLf
                    script &= "alert('No Form Aplikasi adalah :  " & Request("Application").ToString.Trim & "');" & vbCrLf
                    script &= "</script>"
                    Response.Write(script)
                End If
                If Request("cond") <> "" Then
                    Me.CmdWhere = Request("cond")
                Else
                    Me.CmdWhere = "ALL"
                End If
                Me.Sort = "Name ASC"
                BindGrid(Me.CmdWhere)
                InitialDefaultPanel()
            Else
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            End If
        End If
    End Sub
#End Region
#Region "dtgPaging_ItemDataBound"
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim imgBadType As System.Web.UI.WebControls.Image
        Dim BadType As String = e.Item.Cells(4).Text
        If e.Item.ItemIndex >= 0 Then
            imgBadType = CType(e.Item.FindControl("imgBadType"), System.Web.UI.WebControls.Image)
            If BadType = "B" Then
                imgBadType.ImageUrl = "../../../images/red.gif"
            ElseIf BadType = "W" Then
                imgBadType.ImageUrl = "../../../images/yellow.gif"
            Else
                imgBadType.ImageUrl = "../../../images/green.gif"
            End If
            Dim lnkCustName As LinkButton
            lnkCustName = CType(e.Item.FindControl("lnkCustName"), LinkButton)
            lnkCustName.Attributes.Add("OnClick", "return OpenCust('" & e.Item.Cells(5).Text & "','accacq');")
        End If
    End Sub
    Sub InitialDefaultPanel()
        pnlList.Visible = True
    End Sub
#End Region
#Region "BindGrid"
    Sub BindGrid(ByVal cmdWhere As String)
        Dim dtEntity As DataTable
        Dim oCustomClass As New Parameter.Application
        pnlList.Visible = True
        oCustomClass.PageSize = pageSize

        'filter by branch
        If cmdWhere.ToLower = "all" Then
            cmdWhere = "left(vwCustomer.CustomerID,3) = " & Me.sesBranchId
        Else
            cmdWhere &= "and left(vwCustomer.CustomerID,3) = " & Me.sesBranchId
        End If

        oCustomClass.WhereCond = cmdWhere
        oCustomClass.isRO = CBool(rboCust.SelectedValue)
        oCustomClass.SortBy = Me.Sort
        oCustomClass.CurrentPage = currentPage
        oCustomClass.strConnection = GetConnectionString()
        oCustomClass = m_controller.GetApplication(oCustomClass)
        If Not oCustomClass Is Nothing Then
            dtEntity = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub
#End Region
#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan!", True)

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGrid(Me.CmdWhere)
            End If
        End If
    End Sub
#End Region
#Region "ItemCommand"
    Private Sub dtgEntity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.Item.ItemIndex >= 0 Then
            If e.CommandName = "NewApp" Then
                Dim CustId As String = e.Item.Cells(5).Text.Trim
                Dim CustName As String = CType(e.Item.Cells(1).FindControl("lnkCustName"), LinkButton).Text
                Dim Type As String = e.Item.Cells(2).Text.Trim
                If CheckFeature(Me.Loginid, "Application", "Add", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Response.Redirect("Application_002KPR.aspx?id=" & CustId & "&name=" & CustName & "&type=" & Type & "")
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region
#Region "Search - Reset"
    Private Sub imbReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        cboSearch.SelectedIndex = 0
        txtSearch.Text = ""
        Me.CmdWhere = "ALL"
        InitialDefaultPanel()
        BindGrid(Me.CmdWhere)
    End Sub
    Private Sub imbSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If txtSearch.Text.Trim <> "" Then
            Dim tmptxtSearchBy As String = txtSearch.Text.Replace("%", "")
            Me.CmdWhere = cboSearch.SelectedItem.Value + " like '%" + tmptxtSearchBy + "%'"
        Else
            Me.CmdWhere = "ALL"
        End If
        BindGrid(Me.CmdWhere)
    End Sub
#End Region

End Class