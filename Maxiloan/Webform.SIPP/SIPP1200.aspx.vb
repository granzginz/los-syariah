﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
'untuk eksport
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
'
#End Region

Public Class SIPP1200
    Inherits Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New SIPP1200Controller
    Private oCustomclass As New Parameter.SIPP1200
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNaviSIPP1200 As ucGridNav

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviSIPP1200.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP1200"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                'untuk eksport
                pnlcopybulandata.Visible = False
                '
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                FillCombo()
                'untuk eksport
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
                '
            End If
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviSIPP1200.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As New DataTable
        Dim oParameter As New Parameter.SIPP1200

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP1200List(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()


        If (isFrNav = False) Then
            GridNaviSIPP1200.Initialize(recordCount, pageSize)
        End If
        'untuk eksport
        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            'FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
        '
    End Sub
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub
    'untuk eksport
    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP1200
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub
    '
    'Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
    '    Dim oAssetData As New Parameter.SIPP1200
    '    Dim oData As New DataTable
    '    oAssetData.strConnection = GetConnectionString()
    '    oAssetData.Valuemaster = valuemaster
    '    oAssetData.Valueparent = valueparent
    '    oAssetData.Valueelement = valueelement
    '    oAssetData = oController.GetCbo(oAssetData)
    '    oData = oAssetData.ListData
    '    cboName.DataSource = oData
    '    cboName.DataTextField = "ElementLabel"
    '    cboName.DataValueField = "Element"
    '    cboName.DataBind()
    '    cboName.Items.Insert(0, "Select One")
    '    cboName.Items(0).Value = ""
    'End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP1200", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            'untuk eksport
            txtbulandata.Enabled = True
            '
            Me.Process = "ADD"
            clean()
        End If
    End Sub
#End Region

    '#Region "Cancel"
    '    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
    '        pnlList.Visible = True
    '        pnlAdd.Visible = False
    '        Me.SearchBy = ""
    '        Me.SortBy = ""

    '        'clean()
    '        DoBind(Me.SearchBy, Me.SortBy)
    '    End Sub
    '#End Region

    '#Region "Search"
    '    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
    '        If txtSearch.Text <> "" Then
    '            Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
    '        Else
    '            Me.SearchBy = ""
    '        End If
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '    End Sub
    '#End Region

    '#Region "Reset"
    '    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
    '        Me.SearchBy = ""
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '        txtSearch.Text = ""
    '        cboSearch.ClearSelection()
    '        pnlAdd.Visible = False
    '    End Sub
    '#End Region
    'Sub clean()
    '    txtid.Text = ""
    '    txtNama.Text = ""
    '    txtNoKep.Text = ""
    '    txttglkep.Text = ""
    'End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP1200
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP1200", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetSIPP1200Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH")
                txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG")
                txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL")
                txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")
                txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL")
                txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")
                txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL")
                txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")
                txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL")
                txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")
                txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL")
                txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH")
                txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG")
                txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL")
                txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH")
                txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG")
                txtPNDPTN_DMNSTRS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_DMNSTRS_TTL")
                txtPNDPTN_DMNSTRS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_DMNSTRS_NDNSN_RPH")
                txtPNDPTN_DMNSTRS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_DMNSTRS_MT_NG_SNG")
                txtPNDPTN_PRVS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_PRVS_TTL")
                txtPNDPTN_PRVS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_PRVS_NDNSN_RPH")
                txtPNDPTN_PRVS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_PRVS_MT_NG_SNG")
                txtPNDPTN_DND_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_DND_TTL")
                txtPNDPTN_DND_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_DND_NDNSN_RPH")
                txtPNDPTN_DND_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_DND_MT_NG_SNG")
                txtPNDPTN_DSKN_SRNS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_DSKN_SRNS_TTL")
                txtPNDPTN_DSKN_SRNS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_DSKN_SRNS_NDNSN_RPH")
                txtPNDPTN_DSKN_SRNS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_DSKN_SRNS_MT_NG_SNG")
                txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL")
                txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH")
                txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG")
                txtPNDPTN_PRSNL_LN_TRKT_PMBYN_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LN_TRKT_PMBYN_TTL")
                txtPNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH")
                txtPNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG")
                txtPNDPTN_DR_SW_PRS_PRTNG_LS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_DR_SW_PRS_PRTNG_LS_TTL")
                txtPNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH")
                txtPNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG")
                txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL")
                txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH")
                txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG")
                txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL")
                txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH")
                txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG")
                txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL")
                txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH")
                txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG")
                txtPNDPTN_DR_KGTN_BRBSS_F_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_DR_KGTN_BRBSS_F_TTL")
                txtPNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH")
                txtPNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG")
                txtPNDPTN_PRSNL_LNNY_LNNY_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LNNY_LNNY_TTL")
                txtPNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH")
                txtPNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG")
                txtPNDPTN_PRSNL_LNNY_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LNNY_TTL")
                txtPNDPTN_PRSNL_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LNNY_NDNSN_RPH")
                txtPNDPTN_PRSNL_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_LNNY_MT_NG_SNG")
                txtPNDPTN_PRSNL_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_TTL")
                txtPNDPTN_PRSNL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_NDNSN_RPH")
                txtPNDPTN_PRSNL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_PRSNL_MT_NG_SNG")
                txtPNDPTN_BNG_NN_PRSNL_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_NN_PRSNL_TTL")
                txtPNDPTN_BNG_NN_PRSNL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_NN_PRSNL_NDNSN_RPH")
                txtPNDPTN_BNG_NN_PRSNL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BNG_NN_PRSNL_MT_NG_SNG")
                txtPNDPTN_NN_PRSNL_LNNY_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_NN_PRSNL_LNNY_TTL")
                txtPNDPTN_NN_PRSNL_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_NN_PRSNL_LNNY_NDNSN_RPH")
                txtPNDPTN_NN_PRSNL_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_NN_PRSNL_LNNY_MT_NG_SNG")
                txtPNDPTN_NN_PRSNL_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_NN_PRSNL_TTL")
                txtPNDPTN_NN_PRSNL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_NN_PRSNL_NDNSN_RPH")
                txtPNDPTN_NN_PRSNL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_NN_PRSNL_MT_NG_SNG")
                txtPNDPTN_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_TTL")
                txtPNDPTN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_NDNSN_RPH")
                txtPNDPTN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_MT_NG_SNG")
                txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL.Text = oParameter.ListData.Rows(0)("BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL")
                txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH")
                txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG")
                txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL.Text = oParameter.ListData.Rows(0)("BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL")
                txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH")
                txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG")
                txtBBN_BNG_PRSNL_TTL.Text = oParameter.ListData.Rows(0)("BBN_BNG_PRSNL_TTL")
                txtBBN_BNG_PRSNL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_BNG_PRSNL_NDNSN_RPH")
                txtBBN_BNG_PRSNL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_BNG_PRSNL_MT_NG_SNG")
                txtBBN_PRM_TS_TRNSKS_SWP_TTL.Text = oParameter.ListData.Rows(0)("BBN_PRM_TS_TRNSKS_SWP_TTL")
                txtBBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH")
                txtBBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG")
                txtBBN_PRM_SRNS_TTL.Text = oParameter.ListData.Rows(0)("BBN_PRM_SRNS_TTL")
                txtBBN_PRM_SRNS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PRM_SRNS_NDNSN_RPH")
                txtBBN_PRM_SRNS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PRM_SRNS_MT_NG_SNG")
                txtBBN_GJ_PH_DN_TNJNGN_TTL.Text = oParameter.ListData.Rows(0)("BBN_GJ_PH_DN_TNJNGN_TTL")
                txtBBN_GJ_PH_DN_TNJNGN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_GJ_PH_DN_TNJNGN_NDNSN_RPH")
                txtBBN_GJ_PH_DN_TNJNGN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_GJ_PH_DN_TNJNGN_MT_NG_SNG")
                txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL")
                txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH")
                txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG")
                txtBBN_TNG_KRJ_LNNY_TTL.Text = oParameter.ListData.Rows(0)("BBN_TNG_KRJ_LNNY_TTL")
                txtBBN_TNG_KRJ_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_TNG_KRJ_LNNY_NDNSN_RPH")
                txtBBN_TNG_KRJ_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_TNG_KRJ_LNNY_MT_NG_SNG")
                txtBBN_TNG_KRJ_TTL.Text = oParameter.ListData.Rows(0)("BBN_TNG_KRJ_TTL")
                txtBBN_TNG_KRJ_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_TNG_KRJ_NDNSN_RPH")
                txtBBN_TNG_KRJ_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_TNG_KRJ_MT_NG_SNG")
                txtBBN_NSNTF_PHK_KTG_TTL.Text = oParameter.ListData.Rows(0)("BBN_NSNTF_PHK_KTG_TTL")
                txtBBN_NSNTF_PHK_KTG_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_NSNTF_PHK_KTG_NDNSN_RPH")
                txtBBN_NSNTF_PHK_KTG_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_NSNTF_PHK_KTG_MT_NG_SNG")
                txtBBN_PMSRN_LNNY_TTL.Text = oParameter.ListData.Rows(0)("BBN_PMSRN_LNNY_TTL")
                txtBBN_PMSRN_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PMSRN_LNNY_NDNSN_RPH")
                txtBBN_PMSRN_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PMSRN_LNNY_MT_NG_SNG")
                txtBBN_PMSRN_TTL.Text = oParameter.ListData.Rows(0)("BBN_PMSRN_TTL")
                txtBBN_PMSRN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PMSRN_NDNSN_RPH")
                txtBBN_PMSRN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PMSRN_MT_NG_SNG")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH")
                txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG")
                txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL")
                txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH")
                txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG")
                txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL")
                txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH")
                txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG")
                txtBBN_PNYSHNPNYSTN_TTL.Text = oParameter.ListData.Rows(0)("BBN_PNYSHNPNYSTN_TTL")
                txtBBN_PNYSHNPNYSTN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PNYSHNPNYSTN_NDNSN_RPH")
                txtBBN_PNYSHNPNYSTN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PNYSHNPNYSTN_MT_NG_SNG")
                txtBBN_SW_TTL.Text = oParameter.ListData.Rows(0)("BBN_SW_TTL")
                txtBBN_SW_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_SW_NDNSN_RPH")
                txtBBN_SW_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_SW_MT_NG_SNG")
                txtBBN_PMLHRN_DN_PRBKN_TTL.Text = oParameter.ListData.Rows(0)("BBN_PMLHRN_DN_PRBKN_TTL")
                txtBBN_PMLHRN_DN_PRBKN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PMLHRN_DN_PRBKN_NDNSN_RPH")
                txtBBN_PMLHRN_DN_PRBKN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PMLHRN_DN_PRBKN_MT_NG_SNG")
                txtBBN_DMNSTRS_DN_MM_TTL.Text = oParameter.ListData.Rows(0)("BBN_DMNSTRS_DN_MM_TTL")
                txtBBN_DMNSTRS_DN_MM_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_DMNSTRS_DN_MM_NDNSN_RPH")
                txtBBN_DMNSTRS_DN_MM_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_DMNSTRS_DN_MM_MT_NG_SNG")
                txtBBN_PRSNL_LNNY_TTL.Text = oParameter.ListData.Rows(0)("BBN_PRSNL_LNNY_TTL")
                txtBBN_PRSNL_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PRSNL_LNNY_NDNSN_RPH")
                txtBBN_PRSNL_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PRSNL_LNNY_MT_NG_SNG")
                txtBBN_PRSNL_TTL.Text = oParameter.ListData.Rows(0)("BBN_PRSNL_TTL")
                txtBBN_PRSNL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_PRSNL_NDNSN_RPH")
                txtBBN_PRSNL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_PRSNL_MT_NG_SNG")
                txtBBN_NN_PRSNL_TTL.Text = oParameter.ListData.Rows(0)("BBN_NN_PRSNL_TTL")
                txtBBN_NN_PRSNL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_NN_PRSNL_NDNSN_RPH")
                txtBBN_NN_PRSNL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_NN_PRSNL_MT_NG_SNG")
                txtBBN_TTL.Text = oParameter.ListData.Rows(0)("BBN_TTL")
                txtBBN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BBN_NDNSN_RPH")
                txtBBN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BBN_MT_NG_SNG")
                txtLB_RG_SBLM_PJK_TTL.Text = oParameter.ListData.Rows(0)("LB_RG_SBLM_PJK_TTL")
                txtLB_RG_SBLM_PJK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LB_RG_SBLM_PJK_NDNSN_RPH")
                txtLB_RG_SBLM_PJK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LB_RG_SBLM_PJK_MT_NG_SNG")
                txtPJK_THN_BRJLN_TTL.Text = oParameter.ListData.Rows(0)("PJK_THN_BRJLN_TTL")
                txtPJK_THN_BRJLN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PJK_THN_BRJLN_NDNSN_RPH")
                txtPJK_THN_BRJLN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PJK_THN_BRJLN_MT_NG_SNG")
                txtPNDPTN_BBN_PJK_TNGGHN_TTL.Text = oParameter.ListData.Rows(0)("PNDPTN_BBN_PJK_TNGGHN_TTL")
                txtPNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH")
                txtPNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG")
                txtLB_RG_BRSH_STLH_PJK_TTL.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_STLH_PJK_TTL")
                txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_STLH_PJK_NDNSN_RPH")
                txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_STLH_PJK_MT_NG_SNG")
                txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL")
                txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH")
                txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG")
                txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text = oParameter.ListData.Rows(0)("SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL")
                txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH")
                txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG")
                txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL")
                txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH")
                txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG")
                txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL")
                txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH")
                txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG")
                txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL")
                txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH")
                txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG")
                txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL")
                txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH")
                txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG")
                txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL")
                txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH")
                txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG")



            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP1200", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP1200
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.SIPP1200Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.SIPP1200
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                .BULANDATA = txtbulandata.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG.Text
                .PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL = txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text
                .PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL = txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text
                .PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL = txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text
                .PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL.Text
                .PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL = txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL.Text
                .PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH = txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH.Text
                .PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG = txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG.Text
                .PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL = txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL.Text
                .PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH = txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH.Text
                .PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG = txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG.Text
                .PNDPTN_DMNSTRS_TTL = txtPNDPTN_DMNSTRS_TTL.Text
                .PNDPTN_DMNSTRS_NDNSN_RPH = txtPNDPTN_DMNSTRS_NDNSN_RPH.Text
                .PNDPTN_DMNSTRS_MT_NG_SNG = txtPNDPTN_DMNSTRS_MT_NG_SNG.Text
                .PNDPTN_PRVS_TTL = txtPNDPTN_PRVS_TTL.Text
                .PNDPTN_PRVS_NDNSN_RPH = txtPNDPTN_PRVS_NDNSN_RPH.Text
                .PNDPTN_PRVS_MT_NG_SNG = txtPNDPTN_PRVS_MT_NG_SNG.Text
                .PNDPTN_DND_TTL = txtPNDPTN_DND_TTL.Text
                .PNDPTN_DND_NDNSN_RPH = txtPNDPTN_DND_NDNSN_RPH.Text
                .PNDPTN_DND_MT_NG_SNG = txtPNDPTN_DND_MT_NG_SNG.Text
                .PNDPTN_DSKN_SRNS_TTL = txtPNDPTN_DSKN_SRNS_TTL.Text
                .PNDPTN_DSKN_SRNS_NDNSN_RPH = txtPNDPTN_DSKN_SRNS_NDNSN_RPH.Text
                .PNDPTN_DSKN_SRNS_MT_NG_SNG = txtPNDPTN_DSKN_SRNS_MT_NG_SNG.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_TTL = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_TTL.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG.Text
                .PNDPTN_DR_SW_PRS_PRTNG_LS_TTL = txtPNDPTN_DR_SW_PRS_PRTNG_LS_TTL.Text
                .PNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH = txtPNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH.Text
                .PNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG = txtPNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG.Text
                .PNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL = txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL.Text
                .PNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH = txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH.Text
                .PNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG = txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG.Text
                .PNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL = txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL.Text
                .PNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH = txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH.Text
                .PNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG = txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG.Text
                .PNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL = txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL.Text
                .PNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH = txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH.Text
                .PNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG = txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG.Text
                .PNDPTN_DR_KGTN_BRBSS_F_TTL = txtPNDPTN_DR_KGTN_BRBSS_F_TTL.Text
                .PNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH = txtPNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH.Text
                .PNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG = txtPNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG.Text
                .PNDPTN_PRSNL_LNNY_LNNY_TTL = txtPNDPTN_PRSNL_LNNY_LNNY_TTL.Text
                .PNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH = txtPNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH.Text
                .PNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG = txtPNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG.Text
                .PNDPTN_PRSNL_LNNY_TTL = txtPNDPTN_PRSNL_LNNY_TTL.Text
                .PNDPTN_PRSNL_LNNY_NDNSN_RPH = txtPNDPTN_PRSNL_LNNY_NDNSN_RPH.Text
                .PNDPTN_PRSNL_LNNY_MT_NG_SNG = txtPNDPTN_PRSNL_LNNY_MT_NG_SNG.Text
                .PNDPTN_PRSNL_TTL = txtPNDPTN_PRSNL_TTL.Text
                .PNDPTN_PRSNL_NDNSN_RPH = txtPNDPTN_PRSNL_NDNSN_RPH.Text
                .PNDPTN_PRSNL_MT_NG_SNG = txtPNDPTN_PRSNL_MT_NG_SNG.Text
                .PNDPTN_BNG_NN_PRSNL_TTL = txtPNDPTN_BNG_NN_PRSNL_TTL.Text
                .PNDPTN_BNG_NN_PRSNL_NDNSN_RPH = txtPNDPTN_BNG_NN_PRSNL_NDNSN_RPH.Text
                .PNDPTN_BNG_NN_PRSNL_MT_NG_SNG = txtPNDPTN_BNG_NN_PRSNL_MT_NG_SNG.Text
                .PNDPTN_NN_PRSNL_LNNY_TTL = txtPNDPTN_NN_PRSNL_LNNY_TTL.Text
                .PNDPTN_NN_PRSNL_LNNY_NDNSN_RPH = txtPNDPTN_NN_PRSNL_LNNY_NDNSN_RPH.Text
                .PNDPTN_NN_PRSNL_LNNY_MT_NG_SNG = txtPNDPTN_NN_PRSNL_LNNY_MT_NG_SNG.Text
                .PNDPTN_NN_PRSNL_TTL = txtPNDPTN_NN_PRSNL_TTL.Text
                .PNDPTN_NN_PRSNL_NDNSN_RPH = txtPNDPTN_NN_PRSNL_NDNSN_RPH.Text
                .PNDPTN_NN_PRSNL_MT_NG_SNG = txtPNDPTN_NN_PRSNL_MT_NG_SNG.Text
                .PNDPTN_TTL = txtPNDPTN_TTL.Text
                .PNDPTN_NDNSN_RPH = txtPNDPTN_NDNSN_RPH.Text
                .PNDPTN_MT_NG_SNG = txtPNDPTN_MT_NG_SNG.Text
                .BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL = txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL.Text
                .BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH = txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH.Text
                .BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG = txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG.Text
                .BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL = txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL.Text
                .BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH = txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text
                .BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG = txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text
                .BBN_BNG_PRSNL_TTL = txtBBN_BNG_PRSNL_TTL.Text
                .BBN_BNG_PRSNL_NDNSN_RPH = txtBBN_BNG_PRSNL_NDNSN_RPH.Text
                .BBN_BNG_PRSNL_MT_NG_SNG = txtBBN_BNG_PRSNL_MT_NG_SNG.Text
                .BBN_PRM_TS_TRNSKS_SWP_TTL = txtBBN_PRM_TS_TRNSKS_SWP_TTL.Text
                .BBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH = txtBBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH.Text
                .BBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG = txtBBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG.Text
                .BBN_PRM_SRNS_TTL = txtBBN_PRM_SRNS_TTL.Text
                .BBN_PRM_SRNS_NDNSN_RPH = txtBBN_PRM_SRNS_NDNSN_RPH.Text
                .BBN_PRM_SRNS_MT_NG_SNG = txtBBN_PRM_SRNS_MT_NG_SNG.Text
                .BBN_GJ_PH_DN_TNJNGN_TTL = txtBBN_GJ_PH_DN_TNJNGN_TTL.Text
                .BBN_GJ_PH_DN_TNJNGN_NDNSN_RPH = txtBBN_GJ_PH_DN_TNJNGN_NDNSN_RPH.Text
                .BBN_GJ_PH_DN_TNJNGN_MT_NG_SNG = txtBBN_GJ_PH_DN_TNJNGN_MT_NG_SNG.Text
                .BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL = txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL.Text
                .BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH = txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH.Text
                .BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG = txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG.Text
                .BBN_TNG_KRJ_LNNY_TTL = txtBBN_TNG_KRJ_LNNY_TTL.Text
                .BBN_TNG_KRJ_LNNY_NDNSN_RPH = txtBBN_TNG_KRJ_LNNY_NDNSN_RPH.Text
                .BBN_TNG_KRJ_LNNY_MT_NG_SNG = txtBBN_TNG_KRJ_LNNY_MT_NG_SNG.Text
                .BBN_TNG_KRJ_TTL = txtBBN_TNG_KRJ_TTL.Text
                .BBN_TNG_KRJ_NDNSN_RPH = txtBBN_TNG_KRJ_NDNSN_RPH.Text
                .BBN_TNG_KRJ_MT_NG_SNG = txtBBN_TNG_KRJ_MT_NG_SNG.Text
                .BBN_NSNTF_PHK_KTG_TTL = txtBBN_NSNTF_PHK_KTG_TTL.Text
                .BBN_NSNTF_PHK_KTG_NDNSN_RPH = txtBBN_NSNTF_PHK_KTG_NDNSN_RPH.Text
                .BBN_NSNTF_PHK_KTG_MT_NG_SNG = txtBBN_NSNTF_PHK_KTG_MT_NG_SNG.Text
                .BBN_PMSRN_LNNY_TTL = txtBBN_PMSRN_LNNY_TTL.Text
                .BBN_PMSRN_LNNY_NDNSN_RPH = txtBBN_PMSRN_LNNY_NDNSN_RPH.Text
                .BBN_PMSRN_LNNY_MT_NG_SNG = txtBBN_PMSRN_LNNY_MT_NG_SNG.Text
                .BBN_PMSRN_TTL = txtBBN_PMSRN_TTL.Text
                .BBN_PMSRN_NDNSN_RPH = txtBBN_PMSRN_NDNSN_RPH.Text
                .BBN_PMSRN_MT_NG_SNG = txtBBN_PMSRN_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG.Text
                .BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL = txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL.Text
                .BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH = txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH.Text
                .BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG = txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG.Text
                .BBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL = txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text
                .BBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH = txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text
                .BBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG = txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text
                .BBN_PNYSHNPNYSTN_TTL = txtBBN_PNYSHNPNYSTN_TTL.Text
                .BBN_PNYSHNPNYSTN_NDNSN_RPH = txtBBN_PNYSHNPNYSTN_NDNSN_RPH.Text
                .BBN_PNYSHNPNYSTN_MT_NG_SNG = txtBBN_PNYSHNPNYSTN_MT_NG_SNG.Text
                .BBN_SW_TTL = txtBBN_SW_TTL.Text
                .BBN_SW_NDNSN_RPH = txtBBN_SW_NDNSN_RPH.Text
                .BBN_SW_MT_NG_SNG = txtBBN_SW_MT_NG_SNG.Text
                .BBN_PMLHRN_DN_PRBKN_TTL = txtBBN_PMLHRN_DN_PRBKN_TTL.Text
                .BBN_PMLHRN_DN_PRBKN_NDNSN_RPH = txtBBN_PMLHRN_DN_PRBKN_NDNSN_RPH.Text
                .BBN_PMLHRN_DN_PRBKN_MT_NG_SNG = txtBBN_PMLHRN_DN_PRBKN_MT_NG_SNG.Text
                .BBN_DMNSTRS_DN_MM_TTL = txtBBN_DMNSTRS_DN_MM_TTL.Text
                .BBN_DMNSTRS_DN_MM_NDNSN_RPH = txtBBN_DMNSTRS_DN_MM_NDNSN_RPH.Text
                .BBN_DMNSTRS_DN_MM_MT_NG_SNG = txtBBN_DMNSTRS_DN_MM_MT_NG_SNG.Text
                .BBN_PRSNL_LNNY_TTL = txtBBN_PRSNL_LNNY_TTL.Text
                .BBN_PRSNL_LNNY_NDNSN_RPH = txtBBN_PRSNL_LNNY_NDNSN_RPH.Text
                .BBN_PRSNL_LNNY_MT_NG_SNG = txtBBN_PRSNL_LNNY_MT_NG_SNG.Text
                .BBN_PRSNL_TTL = txtBBN_PRSNL_TTL.Text
                .BBN_PRSNL_NDNSN_RPH = txtBBN_PRSNL_NDNSN_RPH.Text
                .BBN_PRSNL_MT_NG_SNG = txtBBN_PRSNL_MT_NG_SNG.Text
                .BBN_NN_PRSNL_TTL = txtBBN_NN_PRSNL_TTL.Text
                .BBN_NN_PRSNL_NDNSN_RPH = txtBBN_NN_PRSNL_NDNSN_RPH.Text
                .BBN_NN_PRSNL_MT_NG_SNG = txtBBN_NN_PRSNL_MT_NG_SNG.Text
                .BBN_TTL = txtBBN_TTL.Text
                .BBN_NDNSN_RPH = txtBBN_NDNSN_RPH.Text
                .BBN_MT_NG_SNG = txtBBN_MT_NG_SNG.Text
                .LB_RG_SBLM_PJK_TTL = txtLB_RG_SBLM_PJK_TTL.Text
                .LB_RG_SBLM_PJK_NDNSN_RPH = txtLB_RG_SBLM_PJK_NDNSN_RPH.Text
                .LB_RG_SBLM_PJK_MT_NG_SNG = txtLB_RG_SBLM_PJK_MT_NG_SNG.Text
                .PJK_THN_BRJLN_TTL = txtPJK_THN_BRJLN_TTL.Text
                .PJK_THN_BRJLN_NDNSN_RPH = txtPJK_THN_BRJLN_NDNSN_RPH.Text
                .PJK_THN_BRJLN_MT_NG_SNG = txtPJK_THN_BRJLN_MT_NG_SNG.Text
                .PNDPTN_BBN_PJK_TNGGHN_TTL = txtPNDPTN_BBN_PJK_TNGGHN_TTL.Text
                .PNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH = txtPNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH.Text
                .PNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG = txtPNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG.Text
                .LB_RG_BRSH_STLH_PJK_TTL = txtLB_RG_BRSH_STLH_PJK_TTL.Text
                .LB_RG_BRSH_STLH_PJK_NDNSN_RPH = txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text
                .LB_RG_BRSH_STLH_PJK_MT_NG_SNG = txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text
                .KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL = txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text
                .KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH = txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text
                .KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG = txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text
                .SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL = txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text
                .SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH = txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text
                .SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG = txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text
                .KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL = txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text
                .KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH = txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text
                .KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG = txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text
                .KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL = txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text
                .KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH = txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text
                .KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG = txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text
                .KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL = txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text
                .KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH = txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text
                .KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG = txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text
                .LB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL = txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL.Text
                .LB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH = txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH.Text
                .LB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG = txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP1200SaveAdd(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter
                .BULANDATA = txtbulandata.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL = txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH = txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH.Text
                .PNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG = txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG.Text
                .PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL = txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text
                .PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .PNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL = txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text
                .PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .PNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL = txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text
                .PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .PNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL.Text
                .PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .PNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL = txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL.Text
                .PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH = txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH.Text
                .PNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG = txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG.Text
                .PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL = txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL.Text
                .PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH = txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH.Text
                .PNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG = txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG.Text
                .PNDPTN_DMNSTRS_TTL = txtPNDPTN_DMNSTRS_TTL.Text
                .PNDPTN_DMNSTRS_NDNSN_RPH = txtPNDPTN_DMNSTRS_NDNSN_RPH.Text
                .PNDPTN_DMNSTRS_MT_NG_SNG = txtPNDPTN_DMNSTRS_MT_NG_SNG.Text
                .PNDPTN_PRVS_TTL = txtPNDPTN_PRVS_TTL.Text
                .PNDPTN_PRVS_NDNSN_RPH = txtPNDPTN_PRVS_NDNSN_RPH.Text
                .PNDPTN_PRVS_MT_NG_SNG = txtPNDPTN_PRVS_MT_NG_SNG.Text
                .PNDPTN_DND_TTL = txtPNDPTN_DND_TTL.Text
                .PNDPTN_DND_NDNSN_RPH = txtPNDPTN_DND_NDNSN_RPH.Text
                .PNDPTN_DND_MT_NG_SNG = txtPNDPTN_DND_MT_NG_SNG.Text
                .PNDPTN_DSKN_SRNS_TTL = txtPNDPTN_DSKN_SRNS_TTL.Text
                .PNDPTN_DSKN_SRNS_NDNSN_RPH = txtPNDPTN_DSKN_SRNS_NDNSN_RPH.Text
                .PNDPTN_DSKN_SRNS_MT_NG_SNG = txtPNDPTN_DSKN_SRNS_MT_NG_SNG.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_TTL = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_TTL.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH.Text
                .PNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG = txtPNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG.Text
                .PNDPTN_DR_SW_PRS_PRTNG_LS_TTL = txtPNDPTN_DR_SW_PRS_PRTNG_LS_TTL.Text
                .PNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH = txtPNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH.Text
                .PNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG = txtPNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG.Text
                .PNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL = txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL.Text
                .PNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH = txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH.Text
                .PNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG = txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG.Text
                .PNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL = txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL.Text
                .PNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH = txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH.Text
                .PNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG = txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG.Text
                .PNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL = txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL.Text
                .PNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH = txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH.Text
                .PNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG = txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG.Text
                .PNDPTN_DR_KGTN_BRBSS_F_TTL = txtPNDPTN_DR_KGTN_BRBSS_F_TTL.Text
                .PNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH = txtPNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH.Text
                .PNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG = txtPNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG.Text
                .PNDPTN_PRSNL_LNNY_LNNY_TTL = txtPNDPTN_PRSNL_LNNY_LNNY_TTL.Text
                .PNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH = txtPNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH.Text
                .PNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG = txtPNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG.Text
                .PNDPTN_PRSNL_LNNY_TTL = txtPNDPTN_PRSNL_LNNY_TTL.Text
                .PNDPTN_PRSNL_LNNY_NDNSN_RPH = txtPNDPTN_PRSNL_LNNY_NDNSN_RPH.Text
                .PNDPTN_PRSNL_LNNY_MT_NG_SNG = txtPNDPTN_PRSNL_LNNY_MT_NG_SNG.Text
                .PNDPTN_PRSNL_TTL = txtPNDPTN_PRSNL_TTL.Text
                .PNDPTN_PRSNL_NDNSN_RPH = txtPNDPTN_PRSNL_NDNSN_RPH.Text
                .PNDPTN_PRSNL_MT_NG_SNG = txtPNDPTN_PRSNL_MT_NG_SNG.Text
                .PNDPTN_BNG_NN_PRSNL_TTL = txtPNDPTN_BNG_NN_PRSNL_TTL.Text
                .PNDPTN_BNG_NN_PRSNL_NDNSN_RPH = txtPNDPTN_BNG_NN_PRSNL_NDNSN_RPH.Text
                .PNDPTN_BNG_NN_PRSNL_MT_NG_SNG = txtPNDPTN_BNG_NN_PRSNL_MT_NG_SNG.Text
                .PNDPTN_NN_PRSNL_LNNY_TTL = txtPNDPTN_NN_PRSNL_LNNY_TTL.Text
                .PNDPTN_NN_PRSNL_LNNY_NDNSN_RPH = txtPNDPTN_NN_PRSNL_LNNY_NDNSN_RPH.Text
                .PNDPTN_NN_PRSNL_LNNY_MT_NG_SNG = txtPNDPTN_NN_PRSNL_LNNY_MT_NG_SNG.Text
                .PNDPTN_NN_PRSNL_TTL = txtPNDPTN_NN_PRSNL_TTL.Text
                .PNDPTN_NN_PRSNL_NDNSN_RPH = txtPNDPTN_NN_PRSNL_NDNSN_RPH.Text
                .PNDPTN_NN_PRSNL_MT_NG_SNG = txtPNDPTN_NN_PRSNL_MT_NG_SNG.Text
                .PNDPTN_TTL = txtPNDPTN_TTL.Text
                .PNDPTN_NDNSN_RPH = txtPNDPTN_NDNSN_RPH.Text
                .PNDPTN_MT_NG_SNG = txtPNDPTN_MT_NG_SNG.Text
                .BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL = txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL.Text
                .BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH = txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH.Text
                .BBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG = txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG.Text
                .BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL = txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL.Text
                .BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH = txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text
                .BBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG = txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text
                .BBN_BNG_PRSNL_TTL = txtBBN_BNG_PRSNL_TTL.Text
                .BBN_BNG_PRSNL_NDNSN_RPH = txtBBN_BNG_PRSNL_NDNSN_RPH.Text
                .BBN_BNG_PRSNL_MT_NG_SNG = txtBBN_BNG_PRSNL_MT_NG_SNG.Text
                .BBN_PRM_TS_TRNSKS_SWP_TTL = txtBBN_PRM_TS_TRNSKS_SWP_TTL.Text
                .BBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH = txtBBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH.Text
                .BBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG = txtBBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG.Text
                .BBN_PRM_SRNS_TTL = txtBBN_PRM_SRNS_TTL.Text
                .BBN_PRM_SRNS_NDNSN_RPH = txtBBN_PRM_SRNS_NDNSN_RPH.Text
                .BBN_PRM_SRNS_MT_NG_SNG = txtBBN_PRM_SRNS_MT_NG_SNG.Text
                .BBN_GJ_PH_DN_TNJNGN_TTL = txtBBN_GJ_PH_DN_TNJNGN_TTL.Text
                .BBN_GJ_PH_DN_TNJNGN_NDNSN_RPH = txtBBN_GJ_PH_DN_TNJNGN_NDNSN_RPH.Text
                .BBN_GJ_PH_DN_TNJNGN_MT_NG_SNG = txtBBN_GJ_PH_DN_TNJNGN_MT_NG_SNG.Text
                .BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL = txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL.Text
                .BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH = txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH.Text
                .BBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG = txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG.Text
                .BBN_TNG_KRJ_LNNY_TTL = txtBBN_TNG_KRJ_LNNY_TTL.Text
                .BBN_TNG_KRJ_LNNY_NDNSN_RPH = txtBBN_TNG_KRJ_LNNY_NDNSN_RPH.Text
                .BBN_TNG_KRJ_LNNY_MT_NG_SNG = txtBBN_TNG_KRJ_LNNY_MT_NG_SNG.Text
                .BBN_TNG_KRJ_TTL = txtBBN_TNG_KRJ_TTL.Text
                .BBN_TNG_KRJ_NDNSN_RPH = txtBBN_TNG_KRJ_NDNSN_RPH.Text
                .BBN_TNG_KRJ_MT_NG_SNG = txtBBN_TNG_KRJ_MT_NG_SNG.Text
                .BBN_NSNTF_PHK_KTG_TTL = txtBBN_NSNTF_PHK_KTG_TTL.Text
                .BBN_NSNTF_PHK_KTG_NDNSN_RPH = txtBBN_NSNTF_PHK_KTG_NDNSN_RPH.Text
                .BBN_NSNTF_PHK_KTG_MT_NG_SNG = txtBBN_NSNTF_PHK_KTG_MT_NG_SNG.Text
                .BBN_PMSRN_LNNY_TTL = txtBBN_PMSRN_LNNY_TTL.Text
                .BBN_PMSRN_LNNY_NDNSN_RPH = txtBBN_PMSRN_LNNY_NDNSN_RPH.Text
                .BBN_PMSRN_LNNY_MT_NG_SNG = txtBBN_PMSRN_LNNY_MT_NG_SNG.Text
                .BBN_PMSRN_TTL = txtBBN_PMSRN_TTL.Text
                .BBN_PMSRN_NDNSN_RPH = txtBBN_PMSRN_NDNSN_RPH.Text
                .BBN_PMSRN_MT_NG_SNG = txtBBN_PMSRN_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH.Text
                .BBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG = txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG.Text
                .BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL = txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL.Text
                .BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH = txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH.Text
                .BBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG = txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG.Text
                .BBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL = txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text
                .BBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH = txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text
                .BBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG = txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text
                .BBN_PNYSHNPNYSTN_TTL = txtBBN_PNYSHNPNYSTN_TTL.Text
                .BBN_PNYSHNPNYSTN_NDNSN_RPH = txtBBN_PNYSHNPNYSTN_NDNSN_RPH.Text
                .BBN_PNYSHNPNYSTN_MT_NG_SNG = txtBBN_PNYSHNPNYSTN_MT_NG_SNG.Text
                .BBN_SW_TTL = txtBBN_SW_TTL.Text
                .BBN_SW_NDNSN_RPH = txtBBN_SW_NDNSN_RPH.Text
                .BBN_SW_MT_NG_SNG = txtBBN_SW_MT_NG_SNG.Text
                .BBN_PMLHRN_DN_PRBKN_TTL = txtBBN_PMLHRN_DN_PRBKN_TTL.Text
                .BBN_PMLHRN_DN_PRBKN_NDNSN_RPH = txtBBN_PMLHRN_DN_PRBKN_NDNSN_RPH.Text
                .BBN_PMLHRN_DN_PRBKN_MT_NG_SNG = txtBBN_PMLHRN_DN_PRBKN_MT_NG_SNG.Text
                .BBN_DMNSTRS_DN_MM_TTL = txtBBN_DMNSTRS_DN_MM_TTL.Text
                .BBN_DMNSTRS_DN_MM_NDNSN_RPH = txtBBN_DMNSTRS_DN_MM_NDNSN_RPH.Text
                .BBN_DMNSTRS_DN_MM_MT_NG_SNG = txtBBN_DMNSTRS_DN_MM_MT_NG_SNG.Text
                .BBN_PRSNL_LNNY_TTL = txtBBN_PRSNL_LNNY_TTL.Text
                .BBN_PRSNL_LNNY_NDNSN_RPH = txtBBN_PRSNL_LNNY_NDNSN_RPH.Text
                .BBN_PRSNL_LNNY_MT_NG_SNG = txtBBN_PRSNL_LNNY_MT_NG_SNG.Text
                .BBN_PRSNL_TTL = txtBBN_PRSNL_TTL.Text
                .BBN_PRSNL_NDNSN_RPH = txtBBN_PRSNL_NDNSN_RPH.Text
                .BBN_PRSNL_MT_NG_SNG = txtBBN_PRSNL_MT_NG_SNG.Text
                .BBN_NN_PRSNL_TTL = txtBBN_NN_PRSNL_TTL.Text
                .BBN_NN_PRSNL_NDNSN_RPH = txtBBN_NN_PRSNL_NDNSN_RPH.Text
                .BBN_NN_PRSNL_MT_NG_SNG = txtBBN_NN_PRSNL_MT_NG_SNG.Text
                .BBN_TTL = txtBBN_TTL.Text
                .BBN_NDNSN_RPH = txtBBN_NDNSN_RPH.Text
                .BBN_MT_NG_SNG = txtBBN_MT_NG_SNG.Text
                .LB_RG_SBLM_PJK_TTL = txtLB_RG_SBLM_PJK_TTL.Text
                .LB_RG_SBLM_PJK_NDNSN_RPH = txtLB_RG_SBLM_PJK_NDNSN_RPH.Text
                .LB_RG_SBLM_PJK_MT_NG_SNG = txtLB_RG_SBLM_PJK_MT_NG_SNG.Text
                .PJK_THN_BRJLN_TTL = txtPJK_THN_BRJLN_TTL.Text
                .PJK_THN_BRJLN_NDNSN_RPH = txtPJK_THN_BRJLN_NDNSN_RPH.Text
                .PJK_THN_BRJLN_MT_NG_SNG = txtPJK_THN_BRJLN_MT_NG_SNG.Text
                .PNDPTN_BBN_PJK_TNGGHN_TTL = txtPNDPTN_BBN_PJK_TNGGHN_TTL.Text
                .PNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH = txtPNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH.Text
                .PNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG = txtPNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG.Text
                .LB_RG_BRSH_STLH_PJK_TTL = txtLB_RG_BRSH_STLH_PJK_TTL.Text
                .LB_RG_BRSH_STLH_PJK_NDNSN_RPH = txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text
                .LB_RG_BRSH_STLH_PJK_MT_NG_SNG = txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text
                .KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL = txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text
                .KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH = txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text
                .KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG = txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text
                .SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL = txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text
                .SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH = txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text
                .SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG = txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text
                .KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL = txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text
                .KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH = txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text
                .KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG = txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text
                .KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL = txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text
                .KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH = txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text
                .KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG = txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text
                .KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL = txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text
                .KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH = txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text
                .KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG = txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text
                .LB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL = txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL.Text
                .LB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH = txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH.Text
                .LB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG = txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP1200SaveEdit(oParameter)
        End If
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region
    Protected Sub txtSUM(sender As Object, e As EventArgs)
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL.Text =
 Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text) +
 Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text)

        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text =
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text) +
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text)

        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL.Text =
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text) +
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text)

        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text =
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text) +
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text)

        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL.Text =
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH.Text) +
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG.Text)

        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL.Text =
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH.Text) +
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG.Text)

        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL.Text =
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text) +
        Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text)

        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH.Text = Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text) +
                                                            Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text) +
                                                            Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text) +
                                                            Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text) +
                                                            Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH.Text) +
                                                            Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH.Text) +
                                                            Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text)

        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG.Text = 0 'Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text) +
        'Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text) +
        'Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_SNG.text) +
        'Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_SNG.text) +
        'Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_SNG.text) +
        'Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_SNG.text) +
        'Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_SNG.text)


        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL.Text = Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH.Text) + Val(txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG.Text)

    End Sub
    'untuk eksport
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_120020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP1200Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP1200
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "35_Laporan_Laba_Rugi_dan_Penghasilan_Komprehensif_Lain_TB_120020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP1200) As Parameter.SIPP1200
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            'params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            'params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP1200Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP1200
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = txtcopybulandata.Text
            '.Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    '
    Sub clean()
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG.Text = "0"
        txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text = "0"
        txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = "0"
        txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = "0"
        txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text = "0"
        txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = "0"
        txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = "0"
        txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text = "0"
        txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = "0"
        txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = "0"
        txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL.Text = "0"
        txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = "0"
        txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = "0"
        txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL.Text = "0"
        txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH.Text = "0"
        txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL.Text = "0"
        txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG.Text = "0"
        txtPNDPTN_DMNSTRS_TTL.Text = "0"
        txtPNDPTN_DMNSTRS_NDNSN_RPH.Text = "0"
        txtPNDPTN_DMNSTRS_MT_NG_SNG.Text = "0"
        txtPNDPTN_PRVS_TTL.Text = "0"
        txtPNDPTN_PRVS_NDNSN_RPH.Text = "0"
        txtPNDPTN_PRVS_MT_NG_SNG.Text = "0"
        txtPNDPTN_DND_TTL.Text = "0"
        txtPNDPTN_DND_NDNSN_RPH.Text = "0"
        txtPNDPTN_DND_MT_NG_SNG.Text = "0"
        txtPNDPTN_DSKN_SRNS_TTL.Text = "0"
        txtPNDPTN_DSKN_SRNS_NDNSN_RPH.Text = "0"
        txtPNDPTN_DSKN_SRNS_MT_NG_SNG.Text = "0"
        txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL.Text = "0"
        txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH.Text = "0"
        txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG.Text = "0"
        txtPNDPTN_PRSNL_LN_TRKT_PMBYN_TTL.Text = "0"
        txtPNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH.Text = "0"
        txtPNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG.Text = "0"
        txtPNDPTN_DR_SW_PRS_PRTNG_LS_TTL.Text = "0"
        txtPNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH.Text = "0"
        txtPNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH.Text = "0"
        txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG.Text = "0"
        txtPNDPTN_DR_KGTN_BRBSS_F_TTL.Text = "0"
        txtPNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH.Text = "0"
        txtPNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG.Text = "0"
        txtPNDPTN_PRSNL_LNNY_LNNY_TTL.Text = "0"
        txtPNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH.Text = "0"
        txtPNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG.Text = "0"
        txtPNDPTN_PRSNL_LNNY_TTL.Text = "0"
        txtPNDPTN_PRSNL_LNNY_NDNSN_RPH.Text = "0"
        txtPNDPTN_PRSNL_LNNY_MT_NG_SNG.Text = "0"
        txtPNDPTN_PRSNL_TTL.Text = "0"
        txtPNDPTN_PRSNL_NDNSN_RPH.Text = "0"
        txtPNDPTN_PRSNL_MT_NG_SNG.Text = "0"
        txtPNDPTN_BNG_NN_PRSNL_TTL.Text = "0"
        txtPNDPTN_BNG_NN_PRSNL_NDNSN_RPH.Text = "0"
        txtPNDPTN_BNG_NN_PRSNL_MT_NG_SNG.Text = "0"
        txtPNDPTN_NN_PRSNL_LNNY_TTL.Text = "0"
        txtPNDPTN_NN_PRSNL_LNNY_NDNSN_RPH.Text = "0"
        txtPNDPTN_NN_PRSNL_LNNY_MT_NG_SNG.Text = "0"
        txtPNDPTN_NN_PRSNL_TTL.Text = "0"
        txtPNDPTN_NN_PRSNL_NDNSN_RPH.Text = "0"
        txtPNDPTN_NN_PRSNL_MT_NG_SNG.Text = "0"
        txtPNDPTN_TTL.Text = "0"
        txtPNDPTN_NDNSN_RPH.Text = "0"
        txtPNDPTN_MT_NG_SNG.Text = "0"
        txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL.Text = "0"
        txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH.Text = "0"
        txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG.Text = "0"
        txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL.Text = "0"
        txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text = "0"
        txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text = "0"
        txtBBN_BNG_PRSNL_TTL.Text = "0"
        txtBBN_BNG_PRSNL_NDNSN_RPH.Text = "0"
        txtBBN_BNG_PRSNL_MT_NG_SNG.Text = "0"
        txtBBN_PRM_TS_TRNSKS_SWP_TTL.Text = "0"
        txtBBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH.Text = "0"
        txtBBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG.Text = "0"
        txtBBN_PRM_SRNS_TTL.Text = "0"
        txtBBN_PRM_SRNS_NDNSN_RPH.Text = "0"
        txtBBN_PRM_SRNS_MT_NG_SNG.Text = "0"
        txtBBN_GJ_PH_DN_TNJNGN_TTL.Text = "0"
        txtBBN_GJ_PH_DN_TNJNGN_NDNSN_RPH.Text = "0"
        txtBBN_GJ_PH_DN_TNJNGN_MT_NG_SNG.Text = "0"
        txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL.Text = "0"
        txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH.Text = "0"
        txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG.Text = "0"
        txtBBN_TNG_KRJ_LNNY_TTL.Text = "0"
        txtBBN_TNG_KRJ_LNNY_NDNSN_RPH.Text = "0"
        txtBBN_TNG_KRJ_LNNY_MT_NG_SNG.Text = "0"
        txtBBN_TNG_KRJ_TTL.Text = "0"
        txtBBN_TNG_KRJ_NDNSN_RPH.Text = "0"
        txtBBN_TNG_KRJ_MT_NG_SNG.Text = "0"
        txtBBN_NSNTF_PHK_KTG_TTL.Text = "0"
        txtBBN_NSNTF_PHK_KTG_NDNSN_RPH.Text = "0"
        txtBBN_NSNTF_PHK_KTG_MT_NG_SNG.Text = "0"
        txtBBN_PMSRN_LNNY_TTL.Text = "0"
        txtBBN_PMSRN_LNNY_NDNSN_RPH.Text = "0"
        txtBBN_PMSRN_LNNY_MT_NG_SNG.Text = "0"
        txtBBN_PMSRN_TTL.Text = "0"
        txtBBN_PMSRN_NDNSN_RPH.Text = "0"
        txtBBN_PMSRN_MT_NG_SNG.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH.Text = "0"
        txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG.Text = "0"
        txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL.Text = "0"
        txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH.Text = "0"
        txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG.Text = "0"
        txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text = "0"
        txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text = "0"
        txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text = "0"
        txtBBN_PNYSHNPNYSTN_TTL.Text = "0"
        txtBBN_PNYSHNPNYSTN_NDNSN_RPH.Text = "0"
        txtBBN_PNYSHNPNYSTN_MT_NG_SNG.Text = "0"
        txtBBN_SW_TTL.Text = "0"
        txtBBN_SW_NDNSN_RPH.Text = "0"
        txtBBN_SW_MT_NG_SNG.Text = "0"
        txtBBN_PMLHRN_DN_PRBKN_TTL.Text = "0"
        txtBBN_PMLHRN_DN_PRBKN_NDNSN_RPH.Text = "0"
        txtBBN_PMLHRN_DN_PRBKN_MT_NG_SNG.Text = "0"
        txtBBN_DMNSTRS_DN_MM_TTL.Text = "0"
        txtBBN_DMNSTRS_DN_MM_NDNSN_RPH.Text = "0"
        txtBBN_DMNSTRS_DN_MM_MT_NG_SNG.Text = "0"
        txtBBN_PRSNL_LNNY_TTL.Text = "0"
        txtBBN_PRSNL_LNNY_NDNSN_RPH.Text = "0"
        txtBBN_PRSNL_LNNY_MT_NG_SNG.Text = "0"
        txtBBN_PRSNL_TTL.Text = "0"
        txtBBN_PRSNL_NDNSN_RPH.Text = "0"
        txtBBN_PRSNL_MT_NG_SNG.Text = "0"
        txtBBN_NN_PRSNL_TTL.Text = "0"
        txtBBN_NN_PRSNL_NDNSN_RPH.Text = "0"
        txtBBN_NN_PRSNL_MT_NG_SNG.Text = "0"
        txtBBN_TTL.Text = "0"
        txtBBN_NDNSN_RPH.Text = "0"
        txtBBN_MT_NG_SNG.Text = "0"
        txtLB_RG_SBLM_PJK_TTL.Text = "0"
        txtLB_RG_SBLM_PJK_NDNSN_RPH.Text = "0"
        txtLB_RG_SBLM_PJK_MT_NG_SNG.Text = "0"
        txtPJK_THN_BRJLN_TTL.Text = "0"
        txtPJK_THN_BRJLN_NDNSN_RPH.Text = "0"
        txtPJK_THN_BRJLN_MT_NG_SNG.Text = "0"
        txtPNDPTN_BBN_PJK_TNGGHN_TTL.Text = "0"
        txtPNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH.Text = "0"
        txtPNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG.Text = "0"
        txtLB_RG_BRSH_STLH_PJK_TTL.Text = "0"
        txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text = "0"
        txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text = "0"
        txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text = "0"
        txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text = "0"
        txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text = "0"
        txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text = "0"
        txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text = "0"
        txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text = "0"
        txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text = "0"
        txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text = "0"
        txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text = "0"
        txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text = "0"
        txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text = "0"
        txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text = "0"
        txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text = "0"
        txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text = "0"
        txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text = "0"
        txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text = "0"
        txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text = "0"
        txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text = "0"
        txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL.Text = "0"
        txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH.Text = "0"
        txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG.Text = "0"
    End Sub
End Class