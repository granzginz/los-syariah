﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data.SqlClient
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class SIPP0043
    Inherits Maxiloan.Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New SIPP0043Controller
    Private oCustomclass As New Parameter.SIPP0043
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNaviSIPP0043 As ucGridNav

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviSIPP0043.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP0043"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                pnlcopybulandata.Visible = False
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)

                FillCombo()
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
            End If
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviSIPP0043.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.SIPP0043

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP0043(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        'If recordCount = 1 Then
        '    ButtonAdd.Visible = False
        'Else
        '    ButtonAdd.Visible = True
        'End If

        If (isFrNav = False) Then
            GridNaviSIPP0043.Initialize(recordCount, pageSize)
        End If

        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
    End Sub
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP0043
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP0043", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            txtbulandata.Enabled = True

            Me.Process = "ADD"
            clean()

        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    'Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
    '    If txtSearch.Text <> "" Then
    '        Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
    '    Else
    '        Me.SearchBy = ""
    '    End If
    '    Me.SortBy = ""
    '    DoBind(Me.SearchBy, Me.SortBy)
    'End Sub
#End Region

#Region "Reset"
    'Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
    '    Me.SearchBy = ""
    '    Me.SortBy = ""
    '    DoBind(Me.SearchBy, Me.SortBy)
    '    'txtSearch.Text = ""
    '    'cboSearch.ClearSelection()
    '    pnlAdd.Visible = False
    'End Sub
#End Region
    Sub clean()
        txtbulandata.Text = ""

        ''TOTAL TENAGA KERJA PER JENIS (TETAP/KONTRAK/OUTSOURCE)''
        lblTotalTTPrdks.Text = 0
        lblTotalTTPsl.Text = 0
        lblTotalKTRrdks.Text = 0
        lblTotalKTRsl.Text = 0
        lblTotalOUTrdks.Text = 0
        lblTotalOUTsl.Text = 0

        ''PEMASARAN''									
        txttngkrjttpMNJPemasaran.Text = 0
        txttngkrjttpSLPemasaran.Text = 0
        txttngkrjktrMNJPemasaran.Text = 0
        txttngkrjktrSLPemasaran.Text = 0
        txttngkrjoutMNJPemasaran.Text = 0
        txttngkrjoutSLPemasaran.Text = 0

        ''OPERASIONAL''									
        txttngkrjttpMNJOperasional.Text = 0
        txttngkrjttpSLOperasional.Text = 0
        txttngkrjktrMNJOperasional.Text = 0
        txttngkrjktrSLOperasional.Text = 0
        txttngkrjoutMNJOperasional.Text = 0
        txttngkrjoutSLOperasional.Text = 0

        ''PENAGIHAN''									
        txttngkrjttpMNJPenagihan.Text = 0
        txttngkrjttpSLPenagihan.Text = 0
        txttngkrjktrMNJPenagihan.Text = 0
        txttngkrjktrSLPenagihan.Text = 0
        txttngkrjoutMNJPenagihan.Text = 0
        txttngkrjoutSLPenagihan.Text = 0

        ''HR & GA''										
        txttngkrjttpMNJHrGa.Text = 0
        txttngkrjttpSLHrGa.Text = 0
        txttngkrjktrMNJHrGa.Text = 0
        txttngkrjktrSLHrGa.Text = 0
        txttngkrjoutMNJHrGa.Text = 0
        txttngkrjoutSLHrGa.Text = 0

        ''KEAUANGAN & AKUNTANSI''						
        txttngkrjttpMNJAkn.Text = 0
        txttngkrjttpSLAkn.Text = 0
        txttngkrjktrMNJAkn.Text = 0
        txttngkrjktrSLAkn.Text = 0
        txttngkrjoutMNJAkn.Text = 0
        txttngkrjoutSLAkn.Text = 0

        ''MANAJEMAN RISIKO''							
        txttngkrjttpMNJRsk.Text = 0
        txttngkrjttpSLRsk.Text = 0
        txttngkrjktrMNJRsk.Text = 0
        txttngkrjktrSLRsk.Text = 0
        txttngkrjoutMNJRsk.Text = 0
        txttngkrjoutSLRsk.Text = 0

        ''AUDIT INTERNAL''								
        txttngkrjttpMNJAdt.Text = 0
        txttngkrjttpSLAdt.Text = 0
        txttngkrjktrMNJAdt.Text = 0
        txttngkrjktrSLAdt.Text = 0
        txttngkrjoutMNJAdt.Text = 0
        txttngkrjoutSLAdt.Text = 0

        ''LEGAL''										
        txttngkrjttpMNJLegal.Text = 0
        txttngkrjttpSLLegal.Text = 0
        txttngkrjktrMNJLegal.Text = 0
        txttngkrjktrSLLegal.Text = 0
        txttngkrjoutMNJLegal.Text = 0
        txttngkrjoutSLLegal.Text = 0

        ''TI''											
        txttngkrjttpMNJIT.Text = 0
        txttngkrjttpSLIT.Text = 0
        txttngkrjktrMNJIT.Text = 0
        txttngkrjktrSLIT.Text = 0
        txttngkrjoutMNJIT.Text = 0
        txttngkrjoutSLIT.Text = 0

        ''FUNGSI LAINNYA''								
        txttngkrjttpMNJLn.Text = 0
        txttngkrjttpSLLn.Text = 0
        txttngkrjktrMNJLn.Text = 0
        txttngkrjktrSLLn.Text = 0
        txttngkrjoutMNJLn.Text = 0
        txttngkrjoutSLLn.Text = 0

        ''JUMLAH TENAGA KERJA KESELURUHAN"				
        lblTOTAL.Text = 0

        ''JUMLAH TENAGA KERJA PER FUNGSI''				
        lbltotalPemasaran.Text = 0
        lbltotalOperasional.Text = 0
        lblTotalPenagihan.Text = 0
        lblTotalHrGa.Text = 0
        lblTotalAkn.Text = 0
        lblTotalRsk.Text = 0
        lblTotalAudit.Text = 0
        lalTotalLegal.Text = 0
        lblTotalIT.Text = 0
        lblTotalLn.Text = 0
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP0043
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP0043", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.SIPP0043Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                ''TOTAL TENAGA KERJA PER JENIS (TETAP/KONTRAK/OUTSOURCE)''
                lblTotalTTPrdks.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                lblTotalTTPsl.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                lblTotalKTRrdks.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                lblTotalKTRsl.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                lblTotalOUTrdks.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                lblTotalOUTsl.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''PEMASARAN''
                txttngkrjttpMNJPemasaran.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLPemasaran.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJPemasaran.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLPemasaran.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJPemasaran.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLPemasaran.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''OPERASIONAL''
                txttngkrjttpMNJOperasional.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLOperasional.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJOperasional.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLOperasional.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJOperasional.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLOperasional.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''PENAGIHAN''
                txttngkrjttpMNJPenagihan.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLPenagihan.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJPenagihan.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLPenagihan.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJPenagihan.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLPenagihan.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''HR & GA''
                txttngkrjttpMNJHrGa.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLHrGa.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJHrGa.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLHrGa.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJHrGa.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLHrGa.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''KEAUANGAN & AKUNTANSI''
                txttngkrjttpMNJAkn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLAkn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJAkn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLAkn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJAkn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLAkn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''MANAJEMAN RISIKO''
                txttngkrjttpMNJRsk.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLRsk.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJRsk.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLRsk.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJRsk.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLRsk.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''AUDIT INTERNAL''
                txttngkrjttpMNJAdt.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLAdt.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJAdt.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLAdt.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJAdt.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLAdt.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''LEGAL''
                txttngkrjttpMNJLegal.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLLegal.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJLegal.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLLegal.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJLegal.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLLegal.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''TI''
                txttngkrjttpMNJIT.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLIT.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJIT.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLIT.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJIT.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLIT.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''FUNGSI LAINNYA''
                txttngkrjttpMNJLn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjttpSLLn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjktrMNJLn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjktrSLLn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY")
                txttngkrjoutMNJLn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS")
                txttngkrjoutSLLn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY")

                ''JUMLAH TENAGA KERJA KESELURUHAN (DARI TOTAL TENAGA KERJA PER FUNGSI)''
                lblTOTAL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL__")

                ''JUMLAH TENAGA KERJA PER FUNGSI''
                lbltotalPemasaran.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PMSRN_TTL_")
                lbltotalOperasional.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PRSNL_TTL_")
                lblTotalPenagihan.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_PNGHN_TTL_")
                lblTotalHrGa.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_")
                lblTotalAkn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_")
                lblTotalRsk.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_MNJMN_RSK_TTL_")
                lblTotalAudit.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_DT_NTRNL_TTL_")
                lalTotalLegal.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_LGL_TTL_")
                lblTotalIT.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_")
                lblTotalLn.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_FNGS_LNNY_TTL_")

                txtid.Text = oParameter.ListData.Rows(0)("ID")


            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP0043", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP0043
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.SIPP0043Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.SIPP0043
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                .BULANDATA = txtbulandata.Text
                ''TOTAL TENAGA KERJA PER JENIS (TETAP/KONTRAK/OUTSOURCE)''
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = HidTotalTTPrdks.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = HidTotalTTPsl.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = HidTotalKTRrdks.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = HidTotalKTRsl.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = HidTotalOUTrdks.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = HidTotalOUTsl.Value

                ''PEMASARAN''
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLPemasaran.Text

                ''OPERASIONAL''
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLOperasional.Text

                ''PENAGIHAN''
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLPenagihan.Text

                ''HR & GA''
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLHrGa.Text

                ''KEUANGAN & AKUNTANSI''
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLAkn.Text

                ''MANAJEMAN RISIKO''
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLRsk.Text

                ''AUDIT INTERNAL''
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLAdt.Text

                ''LEGAL''
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLLegal.Text

                ''TI''
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLIT.Text

                ''FUNGSI LAINNYA''
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLLn.Text

                ''JUMLAH TENAGA KERJA KESELURUHAN (DARI TOTAL TENAGA KERJA PER FUNGSI)''
                .JMLH_TNG_KRJ_TTL__ = HidTOTAL.Value

                ''JUMLAH TENAGA KERJA PER FUNGSI''
                .JMLH_TNG_KRJ_PMSRN_TTL_ = HidtotalPemasaran.Value
                .JMLH_TNG_KRJ_PRSNL_TTL_ = HidtotalOperasional.Value
                .JMLH_TNG_KRJ_PNGHN_TTL_ = HidTotalPenagihan.Value
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_ = HidTotalHrGa.Value
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_ = HidTotalAkn.Value
                .JMLH_TNG_KRJ_MNJMN_RSK_TTL_ = HidTotalRsk.Value
                .JMLH_TNG_KRJ_DT_NTRNL_TTL_ = HidTotalAudit.Value
                .JMLH_TNG_KRJ_LGL_TTL_ = HidTotalLegal.Value
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_ = HidTotalIT.Value
                .JMLH_TNG_KRJ_FNGS_LNNY_TTL_ = HidTotalLn.Value
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP0043Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter
                .ID = txtid.Text

                ''TOTAL TENAGA KERJA PER JENIS (TETAP/KONTRAK/OUTSOURCE)''
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = HidTotalTTPrdks.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = HidTotalTTPsl.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = HidTotalKTRrdks.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = HidTotalKTRsl.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = HidTotalOUTrdks.Value
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = HidTotalOUTsl.Value

                ''PEMASARAN''
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJPemasaran.Text
                .JMLH_TNG_KRJ_PMSRN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLPemasaran.Text

                ''OPERASIONAL''
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJOperasional.Text
                .JMLH_TNG_KRJ_PRSNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLOperasional.Text

                ''PENAGIHAN''
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJPenagihan.Text
                .JMLH_TNG_KRJ_PNGHN_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLPenagihan.Text

                ''HR & GA''
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJHrGa.Text
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLHrGa.Text

                ''KEUANGAN & AKUNTANSI''
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJAkn.Text
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLAkn.Text

                ''MANAJEMAN RISIKO''
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJRsk.Text
                .JMLH_TNG_KRJ_MNJMN_RSK_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLRsk.Text

                ''AUDIT INTERNAL''
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJAdt.Text
                .JMLH_TNG_KRJ_DT_NTRNL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLAdt.Text

                ''LEGAL''
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJLegal.Text
                .JMLH_TNG_KRJ_LGL_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLLegal.Text

                ''TI''
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJIT.Text
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLIT.Text

                ''FUNGSI LAINNYA''
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjttpMNJLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TTP_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjttpSLLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjktrMNJLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_KNTRK_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjktrSLLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_TNG_MNJRL_SMP_ST_LVL_D_BWH_DRKS = txttngkrjoutMNJLn.Text
                .JMLH_TNG_KRJ_FNGS_LNNY_TNG_KRJ_TSRCNG_STF_DN_TNGKT_TNG_KRJ_LNNY = txttngkrjoutSLLn.Text

                ''JUMLAH TENAGA KERJA KESELURUHAN (DARI TOTAL TENAGA KERJA PER FUNGSI)''
                .JMLH_TNG_KRJ_TTL__ = HidTOTAL.Value

                ''JUMLAH TENAGA KERJA PER FUNGSI''
                .JMLH_TNG_KRJ_PMSRN_TTL_ = HidtotalPemasaran.Value
                .JMLH_TNG_KRJ_PRSNL_TTL_ = HidtotalOperasional.Value
                .JMLH_TNG_KRJ_PNGHN_TTL_ = HidTotalPenagihan.Value
                .JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_ = HidTotalHrGa.Value
                .JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_ = HidTotalAkn.Value
                .JMLH_TNG_KRJ_MNJMN_RSK_TTL_ = HidTotalRsk.Value
                .JMLH_TNG_KRJ_DT_NTRNL_TTL_ = HidTotalAudit.Value
                .JMLH_TNG_KRJ_LGL_TTL_ = HidTotalLegal.Value
                .JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_ = HidTotalIT.Value
                .JMLH_TNG_KRJ_FNGS_LNNY_TTL_ = HidTotalLn.Value
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP0043Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region

#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_004320300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToExcel(ByVal customclass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try

            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA


            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0043Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP0043
        Dim dtViewData As New DataTable
        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToExcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "31_Rincian_Tenaga_Kerja_Berdasarkan_Fungsi_TB_004320300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP0043) As Parameter.SIPP0043
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0043Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP0043
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
End Class