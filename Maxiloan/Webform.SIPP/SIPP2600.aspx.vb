﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class SIPP2600
    Inherits Maxiloan.Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New SIPP2600Controller
    Private oCustomclass As New Parameter.SIPP2600
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNaviSIPP2600 As ucGridNav

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviSIPP2600.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP2600"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                pnlcopybulandata.Visible = False
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)

                FillCombo()
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
            End If
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviSIPP2600.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Maxiloan.Parameter.SIPP2600

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP2600(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()
        FillCombo()
        FillCbo(cbojnssurat, "13", "", "")
        FillCbo(cbojnssuku, "16", "", "")
        FillCbo(cbovaluta, "", "", 2)
        FillCbo(cboGolkreditur, "", "1419", "")
        FillCbo(cboNegara, "", "", 3)

        If (isFrNav = False) Then
            GridNaviSIPP2600.Initialize(recordCount, pageSize)
        End If

        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
    End Sub
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP2600
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP2600
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Valuemaster = valuemaster
        oAssetData.Valueparent = valueparent
        oAssetData.Valueelement = valueelement
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ElementLabel"
        cboName.DataValueField = "Element"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP2600", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False

            Me.Process = "ADD"
            clean()
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    '#Region "Search"
    '    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
    '        If txtSearch.Text <> "" Then
    '            Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
    '        Else
    '            Me.SearchBy = ""
    '        End If
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '    End Sub
    '#End Region

    '#Region "Reset"
    '    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
    '        Me.SearchBy = ""
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '        txtSearch.Text = ""
    '        cboSearch.ClearSelection()
    '        pnlAdd.Visible = False
    '    End Sub
    '#End Region
    Sub clean()
        txtbulandata.Text = ""
        txtid.Text = ""
        txtnosurat.Text = ""
        txtnilainominal.Text = ""
        txttingkatsukubunga.Text = ""
        txtsaldopinjamanvalas.Text = ""
        txtsaldopinjamanrp.Text = ""
        txtnamakreditur.Text = ""
        txttglmulai.Text = ""
        txttgljatuhtempo.Text = ""
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP2600
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP2600", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.SIPP2600Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cbojnssurat.SelectedIndex = cbojnssurat.Items.IndexOf(cbojnssurat.Items.FindByValue(oRow("JENISSURATBERHARGA").ToString.Trim))
                    cbojnssuku.SelectedIndex = cbojnssuku.Items.IndexOf(cbojnssuku.Items.FindByValue(oRow("JENISSUKUBUNGA").ToString.Trim))
                    cbovaluta.SelectedIndex = cbovaluta.Items.IndexOf(cbovaluta.Items.FindByValue(oRow("JENISVALUTA").ToString.Trim))
                    cboGolkreditur.SelectedIndex = cboGolkreditur.Items.IndexOf(cboGolkreditur.Items.FindByValue(oRow("GOLONGANKREDITUR").ToString.Trim))
                    cboNegara.SelectedIndex = cboNegara.Items.IndexOf(cboNegara.Items.FindByValue(oRow("LOKASINEGARA").ToString.Trim))
                    txttglmulai.Text = Format(oRow("TGLMULAI"), "yyyy/MM/dd")
                    txttgljatuhtempo.Text = Format(oRow("TGJATUHTEMPO"), "yyyy/MM/dd")
                End If
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtnosurat.Text = oParameter.ListData.Rows(0)("NOSURATBERHARGA")
                txtnilainominal.Text = oParameter.ListData.Rows(0)("NILAINOMINAL")
                txttingkatsukubunga.Text = oParameter.ListData.Rows(0)("TINGKATSUKUBUNGA")
                txtsaldopinjamanvalas.Text = oParameter.ListData.Rows(0)("SALDOPINJAMANASAL")
                txtsaldopinjamanrp.Text = oParameter.ListData.Rows(0)("SALDOPINJAMANRUPIAH")
                txtnamakreditur.Text = oParameter.ListData.Rows(0)("NAMAKREDITUR")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP2600", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP2600
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.SIPP2600Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.SIPP2600
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                .BULANDATA = txtbulandata.Text
                .NOSURATBERHARGA = txtnosurat.Text
                .JENISSURATBERHARGA = cbojnssurat.Text
                .TGLMULAI = txttglmulai.Text
                .TGJATUHTEMPO = txttgljatuhtempo.Text
                .JENISSUKUBUNGA = cbojnssuku.Text
                .TINGKATSUKUBUNGA = txttingkatsukubunga.Text
                .NILAINOMINAL = txtnilainominal.Text
                .JENISVALUTA = cbovaluta.Text
                .SALDOPINJAMANASAL = txtsaldopinjamanvalas.Text
                .SALDOPINJAMANRUPIAH = txtsaldopinjamanrp.Text
                .NAMAKREDITUR = txtnamakreditur.Text
                .GOLONGANKREDITUR = cboGolkreditur.Text
                .LOKASINEGARA = cboNegara.Text
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP2600Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter
                .ID = txtid.Text
                .NOSURATBERHARGA = txtnosurat.Text
                .JENISSURATBERHARGA = cbojnssurat.Text
                .TGLMULAI = txttglmulai.Text
                .TGJATUHTEMPO = txttgljatuhtempo.Text
                .JENISSUKUBUNGA = cbojnssuku.Text
                .TINGKATSUKUBUNGA = txttingkatsukubunga.Text
                .NILAINOMINAL = txtnilainominal.Text
                .JENISVALUTA = cbovaluta.Text
                .SALDOPINJAMANASAL = txtsaldopinjamanvalas.Text
                .SALDOPINJAMANRUPIAH = txtsaldopinjamanrp.Text
                .NAMAKREDITUR = txtnamakreditur.Text
                .GOLONGANKREDITUR = cboGolkreditur.Text
                .LOKASINEGARA = cboNegara.Text
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP2600Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_260020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP2600Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP2600
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "Rincian_Surat_Berharga_Yang_Diterbitkan_TB_260020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP2600) As Parameter.SIPP2600
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP2600Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP2600
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
End Class