﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class SIPP1300
    Inherits Maxiloan.Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New SIPP1300Controller
    Private oCustomclass As New Parameter.SIPP1300
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNaviSIPP1300 As ucGridNav


	Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
		lblMessage.Visible = False
		AddHandler GridNaviSIPP1300.PageChanged, AddressOf PageNavigation
		If SessionInvalid() Then
			Exit Sub
		End If
		If Not IsPostBack Then
			Me.FormID = "SIPP1300"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				Me.SearchBy = ""
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)

				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
			End If
		End If
		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub

#Region "ReGenerate"
	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region

	Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviSIPP1300.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.SIPP1300

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP1300(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        If (isFrNav = False) Then
            GridNaviSIPP1300.Initialize(recordCount, pageSize)
        End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If
	End Sub
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP1300
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
		cboName.Items.Insert(0, "All")
		cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP1300
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Valuemaster = valuemaster
        oAssetData.Valueparent = valueparent
        oAssetData.Valueelement = valueelement
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ElementLabel"
        cboName.DataValueField = "Element"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP1300", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False

            Me.Process = "ADD"
            clean()
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Sub clean()
        txtbulandata.Text = ""
        lblTOTKMPembInv.Text = 0
        txtKMPembInvRP.Text = 0
        txtKMPembInvMA.Text = 0

        lblTOTKMMdlKrj.Text = 0
        txtKMMdlKrjRP.Text = 0
        txtKMMdlKrjMA.Text = 0

        lblTOTKMMulti.Text = 0
        txtKMMultiRP.Text = 0
        txtKMMultiMA.Text = 0

        lblTOTKMPerst.Text = 0
        txtKMPerstRP.Text = 0
        txtKMPerstMA.Text = 0

        lblTOTKMSyar.Text = 0
        txtKMSyarRP.Text = 0
        txtKMSyarMA.Text = 0

        lblTOTKMFee.Text = 0
        txtKMFeeRP.Text = 0
        txtKMFeeMA.Text = 0

        lblTOTKMSewa.Text = 0
        txtKMSewaRP.Text = 0
        txtKMSewaMA.Text = 0

        lblTOTKMChan.Text = 0
        txtKMChanRP.Text = 0
        txtKMChanMA.Text = 0

        lblTOTKMSurat.Text = 0
        txtKMSuratRP.Text = 0
        txtKMSuratMA.Text = 0

        lblTOTKMLain.Text = 0
        txtKMLainRP.Text = 0
        txtKMLainMA.Text = 0

        lblTOTKMKEGOPR.Text = 0
        lblTOTKMKEGOPRRP.Text = 0
        lblTOTKMKEGOPRMA.Text = 0

        lblTOTKKPembInv.Text = 0
        txtKKPembInvRP.Text = 0
        txtKKPembInvMA.Text = 0

        lblTOTKKMdlKrj.Text = 0
        txtKKMdlKrjRP.Text = 0
        txtKKMdlKrjMA.Text = 0

        lblTOTKKMulti.Text = 0
        txtKKMultiRP.Text = 0
        txtKKMultiMA.Text = 0

        lblTOTKKPerst.Text = 0
        txtKKPerstRP.Text = 0
        txtKKPerstMA.Text = 0

        lblTOTKKSyar.Text = 0
        txtKKSyarRP.Text = 0
        txtKKSyarMA.Text = 0

        lblTOTKKFee.Text = 0
        txtKKFeeRP.Text = 0
        txtKKFeeMA.Text = 0

        lblTOTKKSewa.Text = 0
        txtKKSewaRP.Text = 0
        txtKKSewaMA.Text = 0

        lblTOTKKChan.Text = 0
        txtKKChanRP.Text = 0
        txtKKChanMA.Text = 0

        lblTOTKKSurat.Text = 0
        txtKKSuratRP.Text = 0
        txtKKSuratMA.Text = 0

        lblTOTKKLain.Text = 0
        txtKKLainRP.Text = 0
        txtKKLainMA.Text = 0

        lblTOTKKKEGOPR.Text = 0
        lblTOTKKKEGOPRRP.Text = 0
        lblTOTKKKEGOPRMA.Text = 0

        lblTOTKBKEGOPR.Text = 0
        lblTOTKBKEGOPRRP.Text = 0
        lblTOTKBKEGOPRMA.Text = 0

        lblTOTKMPel.Text = 0
        txtKMPelRP.Text = 0
        txtKMPelMA.Text = 0

        lblTOTKMPenj.Text = 0
        txtKMPenjRP.Text = 0
        txtKMPenjMA.Text = 0

        lblTOTKMPenjSurat.Text = 0
        txtKMPenjSuratRP.Text = 0
        txtKMPenjSuratMA.Text = 0

        lblTOTKMDev.Text = 0
        txtKMDevRP.Text = 0
        txtKMDevMA.Text = 0

        lblTOTKMBung.Text = 0
        txtKMBungRP.Text = 0
        txtKMBungMA.Text = 0

        lblTOTKMInv.Text = 0
        txtKMInvRP.Text = 0
        txtKMInvMA.Text = 0

        lblKMKEGINV.Text = 0
        lblKMKEGINVRP.Text = 0
        lblKMKEGINVMA.Text = 0

        lblTOTKKPer.Text = 0
        txtKKPerRP.Text = 0
        txtKKPerMA.Text = 0

        lblTOTKKPemb.Text = 0
        txtKKPembRP.Text = 0
        txtKKPembMA.Text = 0

        lblTOTKKSur.Text = 0
        txtKKSurRP.Text = 0
        txtKKSurMA.Text = 0

        lblTOTKKInvLain.Text = 0
        txtKKInvLainRP.Text = 0
        txtKKInvLainMA.Text = 0

        lblKKKEGINV.Text = 0
        lblKKKEGINVRP.Text = 0
        lblKKKEGINVMA.Text = 0

        lblKBKEGINV.Text = 0
        lblKBKEGINVRP.Text = 0
        lblKBKEGINVMA.Text = 0

        lblTOTKMTer.Text = 0
        txtKMTerRP.Text = 0
        txtKMTerMA.Text = 0

        lblTOTKMPendLain.Text = 0
        txtKMPendLainRP.Text = 0
        txtKMPendLainMA.Text = 0

        lblTOTKMSaham.Text = 0
        txtKMSahamRP.Text = 0
        txtKMSahamMA.Text = 0

        lblTOTKMKEGPEND.Text = 0
        lblTOTKMKEGPENDRP.Text = 0
        lblTOTKMKEGPENDMA.Text = 0

        lblTOTKKTerbt.Text = 0
        txtKKTerbtRP.Text = 0
        txtKKTerbtMA.Text = 0

        lblTOTKKPendLain.Text = 0
        txtKKPendLainRP.Text = 0
        txtKKPendLainMA.Text = 0

        lblTOTKKSaham.Text = 0
        txtKKSahamRP.Text = 0
        txtKKSahamMA.Text = 0

        lblTOTKKDivd.Text = 0
        txtKKDivdRP.Text = 0
        txtKKDivdMA.Text = 0

        lblTOTKKKEGPEND.Text = 0
        lblTOTKKKEGPENDRP.Text = 0
        lblTOTKKKEGPENDMA.Text = 0

        lblTOTKBKEGPEND.Text = 0
        lblTOTKBKEGPENDRP.Text = 0
        lblTOTKBKEGPENDMA.Text = 0

        lblTOTSurp.Text = 0
        txtSurpRP.Text = 0
        txtSurpMA.Text = 0

        lblTOTPenur.Text = 0
        txtPenurRP.Text = 0
        txtPenurMA.Text = 0

        lblTOTPeriod.Text = 0
        txtPeriodRP.Text = 0
        txtPeriosMA.Text = 0

        lblTOTKas.Text = 0
        txtKasRP.Text = 0
        txtKasMA.Text = 0
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP1300
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP1300", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
				pnlList.Visible = False
				txtbulandata.Enabled = False

				oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.SIPP1300Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                lblTOTKMPembInv.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_NVSTS_TTL")
                txtKMPembInvRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH")
                txtKMPembInvMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG")

                lblTOTKMMdlKrj.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL")
                txtKMMdlKrjRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH")
                txtKMMdlKrjMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG")

                lblTOTKMMulti.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_MLTGN_TTL")
                txtKMMultiRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH")
                txtKMMultiMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG")

                lblTOTKMPerst.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL")
                txtKMPerstRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH")
                txtKMPerstMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG")

                lblTOTKMSyar.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL")
                txtKMSyarRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtKMSyarMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")

                lblTOTKMFee.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_BRBSS_F_TTL")
                txtKMFeeRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH")
                txtKMFeeMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG")

                lblTOTKMSewa.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_SW_PRS_TTL")
                txtKMSewaRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH")
                txtKMSewaMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG")

                lblTOTKMChan.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL")
                txtKMChanRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH")
                txtKMChanMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG")

                lblTOTKMSurat.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL")
                txtKMSuratRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH")
                txtKMSuratMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG")

                lblTOTKMLain.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL")
                txtKMLainRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH")
                txtKMLainMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG")

                lblTOTKMKEGOPR.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PRS_TTL")
                lblTOTKMKEGOPRRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH")
                lblTOTKMKEGOPRMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG")

                lblTOTKKPembInv.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_NVSTS_TTL")
                txtKKPembInvRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH")
                txtKKPembInvMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG")

                lblTOTKKMdlKrj.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL")
                txtKKMdlKrjRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH")
                txtKKMdlKrjMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG")

                lblTOTKKMulti.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_MLTGN_TTL")
                txtKKMultiRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH")
                txtKKMultiMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG")

                lblTOTKKPerst.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL")
                txtKKPerstRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH")
                txtKKPerstMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG")

                lblTOTKKSyar.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL")
                txtKKSyarRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtKKSyarMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")

                lblTOTKKFee.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL")
                txtKKFeeRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH")
                txtKKFeeMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG")

                lblTOTKKSewa.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL")
                txtKKSewaRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH")
                txtKKSewaMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG")

                lblTOTKKChan.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL")
                txtKKChanRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH")
                txtKKChanMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG")

                lblTOTKKSurat.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL")
                txtKKSuratRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH")
                txtKKSuratMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG")

                lblTOTKKLain.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL")
                txtKKLainRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH")
                txtKKLainMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG")

                lblTOTKKKEGOPR.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_PRS_TTL")
                lblTOTKKKEGOPRRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH")
                lblTOTKKKEGOPRMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG")

                lblTOTKBKEGOPR.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_PRS_TTL")
                lblTOTKBKEGOPRRP.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH")
                lblTOTKBKEGOPRMA.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG")

                lblTOTKMPel.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL")
                txtKMPelRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH")
                txtKMPelMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG")

                lblTOTKMPenj.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL")
                txtKMPenjRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH")
                txtKMPenjMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG")

                lblTOTKMPenjSurat.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL")
                txtKMPenjSuratRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH")
                txtKMPenjSuratMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG")

                lblTOTKMDev.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_DVDN_TTL")
                txtKMDevRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_DVDN_NDNSN_RPH")
                txtKMDevMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_DVDN_MT_NG_SNG")

                lblTOTKMBung.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL")
                txtKMBungRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH")
                txtKMBungMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG")

                lblTOTKMInv.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL")
                txtKMInvRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH")
                txtKMInvMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG")

                lblKMKEGINV.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_NVSTS_TTL")
                lblKMKEGINVRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH")
                lblKMKEGINVMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG")

                lblTOTKKPer.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL")
                txtKKPerRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH")
                txtKKPerMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG")

                lblTOTKKPemb.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL")
                txtKKPembRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH")
                txtKKPembMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG")

                lblTOTKKSur.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL")
                txtKKSurRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH")
                txtKKSurMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG")

                lblTOTKKInvLain.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL")
                txtKKInvLainRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH")
                txtKKInvLainMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG")

                lblKKKEGINV.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_NVSTS_TTL")
                lblKKKEGINVRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH")
                lblKKKEGINVMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG")

                lblKBKEGINV.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_NVSTS_TTL")
                lblKBKEGINVRP.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH")
                lblKBKEGINVMA.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG")

                lblTOTKMTer.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL")
                txtKMTerRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH")
                txtKMTerMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG")

                lblTOTKMPendLain.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNDNN_LNNY_TTL")
                txtKMPendLainRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH")
                txtKMPendLainMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG")

                lblTOTKMSaham.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL")
                txtKMSahamRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH")
                txtKMSahamMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG")

                lblTOTKMKEGPEND.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PNDNN_TTL")
                lblTOTKMKEGPENDRP.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH")
                lblTOTKMKEGPENDMA.Text = oParameter.ListData.Rows(0)("RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG")

                lblTOTKKTerbt.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL")
                txtKKTerbtRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH")
                txtKKTerbtMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG")

                lblTOTKKPendLain.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PNDNN_LNNY_TTL")
                txtKKPendLainRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH")
                txtKKPendLainMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG")

                lblTOTKKSaham.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL")
                txtKKSahamRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH")
                txtKKSahamMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG")

                lblTOTKKDivd.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_DVDN_TTL")
                txtKKDivdRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH")
                txtKKDivdMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG")

                lblTOTKKKEGPEND.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_PNDNN_TTL")
                lblTOTKKKEGPENDRP.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH")
                lblTOTKKKEGPENDMA.Text = oParameter.ListData.Rows(0)("RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG")

                lblTOTKBKEGPEND.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_PNDNN_TTL")
                lblTOTKBKEGPENDRP.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH")
                lblTOTKBKEGPENDMA.Text = oParameter.ListData.Rows(0)("RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG")

                lblTOTSurp.Text = oParameter.ListData.Rows(0)("SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL")
                txtSurpRP.Text = oParameter.ListData.Rows(0)("SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH")
                txtSurpMA.Text = oParameter.ListData.Rows(0)("SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG")

                lblTOTPenur.Text = oParameter.ListData.Rows(0)("KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL")
                txtPenurRP.Text = oParameter.ListData.Rows(0)("KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH")
                txtPenurMA.Text = oParameter.ListData.Rows(0)("KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG")

                lblTOTPeriod.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_PD_WL_PRD_TTL")
                txtPeriodRP.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH")
                txtPeriosMA.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG")

                lblTOTKas.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_TTL")
                txtKasRP.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_NDNSN_RPH")
                txtKasMA.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_MT_NG_SNG")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP1300", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP1300
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.SIPP1300Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.SIPP1300
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                .Bulandata = txtbulandata.Text
                .RS_KS_MSK_DR_PMBYN_NVSTS_TTL = lblTOTKMPembInv.Text
                .RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH = txtKMPembInvRP.Text
                .RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG = txtKMPembInvMA.Text

                .RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL = lblTOTKMMdlKrj.Text
                .RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH = txtKMMdlKrjRP.Text
                .RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG = txtKMMdlKrjMA.Text

                .RS_KS_MSK_DR_PMBYN_MLTGN_TTL = lblTOTKMMulti.Text
                .RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH = txtKMMultiRP.Text
                .RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG = txtKMMultiMA.Text

                .RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = lblTOTKMPerst.Text
                .RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtKMPerstRP.Text
                .RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtKMPerstMA.Text

                .RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = lblTOTKMSyar.Text
                .RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtKMSyarRP.Text
                .RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtKMSyarMA.Text

                .RS_KS_MSK_DR_KGTN_BRBSS_F_TTL = lblTOTKMFee.Text
                .RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH = txtKMFeeRP.Text
                .RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG = txtKMFeeMA.Text

                .RS_KS_MSK_DR_KGTN_SW_PRS_TTL = lblTOTKMSewa.Text
                .RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH = txtKMSewaRP.Text
                .RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG = txtKMSewaMA.Text

                .RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL = lblTOTKMChan.Text
                .RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH = txtKMChanRP.Text
                .RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG = txtKMChanMA.Text

                .RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL = lblTOTKMSurat.Text
                .RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = txtKMSuratRP.Text
                .RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = txtKMSuratMA.Text

                .RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL = lblTOTKMLain.Text
                .RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH = txtKMLainRP.Text
                .RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG = txtKMLainMA.Text

                .RS_KS_MSK_DR_KGTN_PRS_TTL = lblTOTKMKEGOPR.Text
                .RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH = lblTOTKMKEGOPRRP.Text
                .RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG = lblTOTKMKEGOPRMA.Text

                .RS_KS_KLR_NTK_PMBYN_NVSTS_TTL = lblTOTKKPembInv.Text
                .RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH = txtKKPembInvRP.Text
                .RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG = txtKKPembInvMA.Text

                .RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL = lblTOTKKMdlKrj.Text
                .RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH = txtKKMdlKrjRP.Text
                .RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG = txtKKMdlKrjMA.Text

                .RS_KS_KLR_NTK_PMBYN_MLTGN_TTL = lblTOTKKMulti.Text
                .RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH = txtKKMultiRP.Text
                .RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG = txtKKMultiMA.Text

                .RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = lblTOTKKPerst.Text
                .RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtKKPerstRP.Text
                .RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtKKPerstMA.Text

                .RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = lblTOTKKSyar.Text
                .RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtKKSyarRP.Text
                .RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtKKSyarMA.Text

                .RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL = lblTOTKKFee.Text
                .RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH = txtKKFeeRP.Text
                .RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG = txtKKFeeMA.Text

                .RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL = lblTOTKKSewa.Text
                .RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH = txtKKSewaRP.Text
                .RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG = txtKKSewaMA.Text

                .RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL = lblTOTKKChan.Text
                .RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH = txtKKChanRP.Text
                .RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG = txtKKChanMA.Text

                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL = lblTOTKKSurat.Text
                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = txtKKSuratRP.Text
                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = txtKKSuratMA.Text

                .RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL = lblTOTKKLain.Text
                .RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH = txtKKLainRP.Text
                .RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG = txtKKLainMA.Text

                .RS_KS_KLR_NTK_KGTN_PRS_TTL = lblTOTKKKEGOPR.Text
                .RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH = lblTOTKKKEGOPRRP.Text
                .RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG = lblTOTKKKEGOPRMA.Text

                .RS_KS_BRSH_DR_KGTN_PRS_TTL = lblTOTKBKEGOPR.Text
                .RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH = lblTOTKBKEGOPRRP.Text
                .RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG = lblTOTKBKEGOPRMA.Text

                .RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL = lblTOTKMPel.Text
                .RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH = txtKMPelRP.Text
                .RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG = txtKMPelMA.Text

                .RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL = lblTOTKMPenj.Text
                .RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH = txtKMPenjRP.Text
                .RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG = txtKMPenjMA.Text

                .RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL = lblTOTKMPenjSurat.Text
                .RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = txtKMPenjSuratRP.Text
                .RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = txtKMPenjSuratMA.Text

                .RS_KS_MSK_DR_DVDN_TTL = lblTOTKMDev.Text
                .RS_KS_MSK_DR_DVDN_NDNSN_RPH = txtKMDevRP.Text
                .RS_KS_MSK_DR_DVDN_MT_NG_SNG = txtKMDevMA.Text

                .RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL = lblTOTKMBung.Text
                .RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH = txtKMBungRP.Text
                .RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG = txtKMBungMA.Text

                .RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL = lblTOTKMInv.Text
                .RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH = txtKMInvRP.Text
                .RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG = txtKMInvMA.Text

                .RS_KS_MSK_DR_KGTN_NVSTS_TTL = lblKMKEGINV.Text
                .RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH = lblKMKEGINVRP.Text
                .RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG = lblKMKEGINVMA.Text

                .RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL = lblTOTKKPer.Text
                .RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH = txtKKPerRP.Text
                .RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG = txtKKPerMA.Text

                .RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL = lblTOTKKPemb.Text
                .RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH = txtKKPembRP.Text
                .RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG = txtKKPembMA.Text

                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL = lblTOTKKSur.Text
                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = txtKKSurRP.Text
                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = txtKKSurMA.Text

                .RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL = lblTOTKKInvLain.Text
                .RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH = txtKKInvLainRP.Text
                .RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG = txtKKInvLainMA.Text

                .RS_KS_KLR_NTK_KGTN_NVSTS_TTL = lblKKKEGINV.Text
                .RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH = lblKKKEGINVRP.Text
                .RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG = lblKKKEGINVMA.Text

                .RS_KS_BRSH_DR_KGTN_NVSTS_TTL = lblKBKEGINV.Text
                .RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH = lblKBKEGINVRP.Text
                .RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG = lblKBKEGINVMA.Text

                .RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL = lblTOTKMTer.Text
                .RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH = txtKMTerRP.Text
                .RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG = txtKMTerMA.Text

                .RS_KS_MSK_DR_PNDNN_LNNY_TTL = lblTOTKMPendLain.Text
                .RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH = txtKMPendLainRP.Text
                .RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG = txtKMPendLainMA.Text

                .RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL = lblTOTKMSaham.Text
                .RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH = txtKMSahamRP.Text
                .RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG = txtKMSahamMA.Text

                .RS_KS_MSK_DR_KGTN_PNDNN_TTL = lblTOTKMKEGPEND.Text
                .RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH = lblTOTKMKEGPENDRP.Text
                .RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG = lblTOTKMKEGPENDMA.Text

                .RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL = lblTOTKKTerbt.Text
                .RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH = txtKKTerbtRP.Text
                .RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG = txtKKTerbtMA.Text

                .RS_KS_KLR_NTK_PNDNN_LNNY_TTL = lblTOTKKPendLain.Text
                .RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH = txtKKPendLainRP.Text
                .RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG = txtKKPendLainMA.Text

                .RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL = lblTOTKKSaham.Text
                .RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH = txtKKSahamRP.Text
                .RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG = txtKKSahamMA.Text

                .RS_KS_KLR_NTK_PMBYRN_DVDN_TTL = lblTOTKKDivd.Text
                .RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH = txtKKDivdRP.Text
                .RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG = txtKKDivdMA.Text

                .RS_KS_KLR_NTK_KGTN_PNDNN_TTL = lblTOTKKKEGPEND.Text
                .RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH = lblTOTKKKEGPENDRP.Text
                .RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG = lblTOTKKKEGPENDMA.Text

                .RS_KS_BRSH_DR_KGTN_PNDNN_TTL = lblTOTKBKEGPEND.Text
                .RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH = lblTOTKBKEGPENDRP.Text
                .RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG = lblTOTKBKEGPENDMA.Text

                .SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL = lblTOTSurp.Text
                .SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH = txtSurpRP.Text
                .SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG = txtSurpMA.Text

                .KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL = lblTOTPenur.Text
                .KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH = txtPenurRP.Text
                .KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG = txtPenurMA.Text

                .KS_DN_STR_KS_PD_WL_PRD_TTL = lblTOTPeriod.Text
                .KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH = txtPeriodRP.Text
                .KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG = txtPeriosMA.Text

                .KS_DN_STR_KS_TTL = lblTOTKas.Text
                .KS_DN_STR_KS_NDNSN_RPH = txtKasRP.Text
                .KS_DN_STR_KS_MT_NG_SNG = txtKasMA.Text
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP1300Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter
                .ID = txtid.Text
                '.BULANDATA
                .RS_KS_MSK_DR_PMBYN_NVSTS_TTL = lblTOTKMPembInv.Text
                .RS_KS_MSK_DR_PMBYN_NVSTS_NDNSN_RPH = txtKMPembInvRP.Text
                .RS_KS_MSK_DR_PMBYN_NVSTS_MT_NG_SNG = txtKMPembInvMA.Text

                .RS_KS_MSK_DR_PMBYN_MDL_KRJ_TTL = lblTOTKMMdlKrj.Text
                .RS_KS_MSK_DR_PMBYN_MDL_KRJ_NDNSN_RPH = txtKMMdlKrjRP.Text
                .RS_KS_MSK_DR_PMBYN_MDL_KRJ_MT_NG_SNG = txtKMMdlKrjMA.Text

                .RS_KS_MSK_DR_PMBYN_MLTGN_TTL = lblTOTKMMulti.Text
                .RS_KS_MSK_DR_PMBYN_MLTGN_NDNSN_RPH = txtKMMultiRP.Text
                .RS_KS_MSK_DR_PMBYN_MLTGN_MT_NG_SNG = txtKMMultiMA.Text

                .RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = lblTOTKMPerst.Text
                .RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtKMPerstRP.Text
                .RS_KS_MSK_DR_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtKMPerstMA.Text

                .RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = lblTOTKMSyar.Text
                .RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtKMSyarRP.Text
                .RS_KS_MSK_DR_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtKMSyarMA.Text

                .RS_KS_MSK_DR_KGTN_BRBSS_F_TTL = lblTOTKMFee.Text
                .RS_KS_MSK_DR_KGTN_BRBSS_F_NDNSN_RPH = txtKMFeeRP.Text
                .RS_KS_MSK_DR_KGTN_BRBSS_F_MT_NG_SNG = txtKMFeeMA.Text

                .RS_KS_MSK_DR_KGTN_SW_PRS_TTL = lblTOTKMSewa.Text
                .RS_KS_MSK_DR_KGTN_SW_PRS_NDNSN_RPH = txtKMSewaRP.Text
                .RS_KS_MSK_DR_KGTN_SW_PRS_MT_NG_SNG = txtKMSewaMA.Text

                .RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL = lblTOTKMChan.Text
                .RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH = txtKMChanRP.Text
                .RS_KS_MSK_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG = txtKMChanMA.Text

                .RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL = lblTOTKMSurat.Text
                .RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = txtKMSuratRP.Text
                .RS_KS_MSK_DR_PLPSN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = txtKMSuratMA.Text

                .RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_TTL = lblTOTKMLain.Text
                .RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_NDNSN_RPH = txtKMLainRP.Text
                .RS_KS_MSK_DR_PNDPTN_KGTN_PRS_LNNY_MT_NG_SNG = txtKMLainMA.Text

                .RS_KS_MSK_DR_KGTN_PRS_TTL = lblTOTKMKEGOPR.Text
                .RS_KS_MSK_DR_KGTN_PRS_NDNSN_RPH = lblTOTKMKEGOPRRP.Text
                .RS_KS_MSK_DR_KGTN_PRS_MT_NG_SNG = lblTOTKMKEGOPRMA.Text

                .RS_KS_KLR_NTK_PMBYN_NVSTS_TTL = lblTOTKKPembInv.Text
                .RS_KS_KLR_NTK_PMBYN_NVSTS_NDNSN_RPH = txtKKPembInvRP.Text
                .RS_KS_KLR_NTK_PMBYN_NVSTS_MT_NG_SNG = txtKKPembInvMA.Text

                .RS_KS_KLR_NTK_PMBYN_MDL_KRJ_TTL = lblTOTKKMdlKrj.Text
                .RS_KS_KLR_NTK_PMBYN_MDL_KRJ_NDNSN_RPH = txtKKMdlKrjRP.Text
                .RS_KS_KLR_NTK_PMBYN_MDL_KRJ_MT_NG_SNG = txtKKMdlKrjMA.Text

                .RS_KS_KLR_NTK_PMBYN_MLTGN_TTL = lblTOTKKMulti.Text
                .RS_KS_KLR_NTK_PMBYN_MLTGN_NDNSN_RPH = txtKKMultiRP.Text
                .RS_KS_KLR_NTK_PMBYN_MLTGN_MT_NG_SNG = txtKKMultiMA.Text

                .RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = lblTOTKKPerst.Text
                .RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtKKPerstRP.Text
                .RS_KS_KLR_NTK_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtKKPerstMA.Text

                .RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_TTL = lblTOTKKSyar.Text
                .RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtKKSyarRP.Text
                .RS_KS_KLR_NTK_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtKKSyarMA.Text

                .RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_TTL = lblTOTKKFee.Text
                .RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_NDNSN_RPH = txtKKFeeRP.Text
                .RS_KS_KLR_NTK_PMBYRN_BNG_PRSNL_MT_NG_SNG = txtKKFeeMA.Text

                .RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_TTL = lblTOTKKSewa.Text
                .RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_NDNSN_RPH = txtKKSewaRP.Text
                .RS_KS_KLR_NTK_PMBYRN_BBN_MM_DN_DMNSTRS_MT_NG_SNG = txtKKSewaMA.Text

                .RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_TTL = lblTOTKKChan.Text
                .RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_NDNSN_RPH = txtKKChanRP.Text
                .RS_KS_KLR_NTK_PMBYRN_PJK_PNGHSLN_MT_NG_SNG = txtKKChanMA.Text

                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_TTL = lblTOTKKSurat.Text
                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = txtKKSuratRP.Text
                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = txtKKSuratMA.Text

                .RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_TTL = lblTOTKKLain.Text
                .RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_NDNSN_RPH = txtKKLainRP.Text
                .RS_KS_KLR_NTK_PMBYRN_KGTN_PRS_LNNY_MT_NG_SNG = txtKKLainMA.Text

                .RS_KS_KLR_NTK_KGTN_PRS_TTL = lblTOTKKKEGOPR.Text
                .RS_KS_KLR_NTK_KGTN_PRS_NDNSN_RPH = lblTOTKKKEGOPRRP.Text
                .RS_KS_KLR_NTK_KGTN_PRS_MT_NG_SNG = lblTOTKKKEGOPRMA.Text

                .RS_KS_BRSH_DR_KGTN_PRS_TTL = lblTOTKBKEGOPR.Text
                .RS_KS_BRSH_DR_KGTN_PRS_NDNSN_RPH = lblTOTKBKEGOPRRP.Text
                .RS_KS_BRSH_DR_KGTN_PRS_MT_NG_SNG = lblTOTKBKEGOPRMA.Text

                .RS_KS_MSK_DR_PLPSN_NK_PRSHN_TTL = lblTOTKMPel.Text
                .RS_KS_MSK_DR_PLPSN_NK_PRSHN_NDNSN_RPH = txtKMPelRP.Text
                .RS_KS_MSK_DR_PLPSN_NK_PRSHN_MT_NG_SNG = txtKMPelMA.Text

                .RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_TTL = lblTOTKMPenj.Text
                .RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH = txtKMPenjRP.Text
                .RS_KS_MSK_DR_PNJLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG = txtKMPenjMA.Text

                .RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL = lblTOTKMPenjSurat.Text
                .RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = txtKMPenjSuratRP.Text
                .RS_KS_MSK_DR_PNJLN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = txtKMPenjSuratMA.Text

                .RS_KS_MSK_DR_DVDN_TTL = lblTOTKMDev.Text
                .RS_KS_MSK_DR_DVDN_NDNSN_RPH = txtKMDevRP.Text
                .RS_KS_MSK_DR_DVDN_MT_NG_SNG = txtKMDevMA.Text

                .RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_TTL = lblTOTKMBung.Text
                .RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_NDNSN_RPH = txtKMBungRP.Text
                .RS_KS_MSK_DR_PNRMN_BNG_KGTN_NVSTS_MT_NG_SNG = txtKMBungMA.Text

                .RS_KS_MSK_DR_KGTN_NVSTS_LNNY_TTL = lblTOTKMInv.Text
                .RS_KS_MSK_DR_KGTN_NVSTS_LNNY_NDNSN_RPH = txtKMInvRP.Text
                .RS_KS_MSK_DR_KGTN_NVSTS_LNNY_MT_NG_SNG = txtKMInvMA.Text

                .RS_KS_MSK_DR_KGTN_NVSTS_TTL = lblKMKEGINV.Text
                .RS_KS_MSK_DR_KGTN_NVSTS_NDNSN_RPH = lblKMKEGINVRP.Text
                .RS_KS_MSK_DR_KGTN_NVSTS_MT_NG_SNG = lblKMKEGINVMA.Text

                .RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_TTL = lblTOTKKPer.Text
                .RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_NDNSN_RPH = txtKKPerRP.Text
                .RS_KS_KLR_NTK_PRLHN_TS_NK_PRSHN_MT_NG_SNG = txtKKPerMA.Text

                .RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_TTL = lblTOTKKPemb.Text
                .RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_NDNSN_RPH = txtKKPembRP.Text
                .RS_KS_KLR_NTK_PMBLN_TNH_BNGNN_DN_PRLTN_MT_NG_SNG = txtKKPembMA.Text

                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_TTL = lblTOTKKSur.Text
                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_NDNSN_RPH = txtKKSurRP.Text
                .RS_KS_KLR_NTK_PRLHN_SRT_BRHRG_YNG_TDK_DTJKN_NTK_DPRJLBLKN_MT_NG_SNG = txtKKSurMA.Text

                .RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_TTL = lblTOTKKInvLain.Text
                .RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_NDNSN_RPH = txtKKInvLainRP.Text
                .RS_KS_KLR_NTK_KGTN_NVSTS_LNNY_MT_NG_SNG = txtKKInvLainMA.Text

                .RS_KS_KLR_NTK_KGTN_NVSTS_TTL = lblKKKEGINV.Text
                .RS_KS_KLR_NTK_KGTN_NVSTS_NDNSN_RPH = lblKKKEGINVRP.Text
                .RS_KS_KLR_NTK_KGTN_NVSTS_MT_NG_SNG = lblKKKEGINVMA.Text

                .RS_KS_BRSH_DR_KGTN_NVSTS_TTL = lblKBKEGINV.Text
                .RS_KS_BRSH_DR_KGTN_NVSTS_NDNSN_RPH = lblKBKEGINVRP.Text
                .RS_KS_BRSH_DR_KGTN_NVSTS_MT_NG_SNG = lblKBKEGINVMA.Text

                .RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_TTL = lblTOTKMTer.Text
                .RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_NDNSN_RPH = txtKMTerRP.Text
                .RS_KS_MSK_DR_PNJMN_DN_PNRBTN_SRT_BRHRG_MT_NG_SNG = txtKMTerMA.Text

                .RS_KS_MSK_DR_PNDNN_LNNY_TTL = lblTOTKMPendLain.Text
                .RS_KS_MSK_DR_PNDNN_LNNY_NDNSN_RPH = txtKMPendLainRP.Text
                .RS_KS_MSK_DR_PNDNN_LNNY_MT_NG_SNG = txtKMPendLainMA.Text

                .RS_KS_MSK_DR_PNRBTN_MDL_SHM_TTL = lblTOTKMSaham.Text
                .RS_KS_MSK_DR_PNRBTN_MDL_SHM_NDNSN_RPH = txtKMSahamRP.Text
                .RS_KS_MSK_DR_PNRBTN_MDL_SHM_MT_NG_SNG = txtKMSahamMA.Text

                .RS_KS_MSK_DR_KGTN_PNDNN_TTL = lblTOTKMKEGPEND.Text
                .RS_KS_MSK_DR_KGTN_PNDNN_NDNSN_RPH = lblTOTKMKEGPENDRP.Text
                .RS_KS_MSK_DR_KGTN_PNDNN_MT_NG_SNG = lblTOTKMKEGPENDMA.Text

                .RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_TTL = lblTOTKKTerbt.Text
                .RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH = txtKKTerbtRP.Text
                .RS_KS_KLR_NTK_PMBYRN_PKK_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG = txtKKTerbtMA.Text

                .RS_KS_KLR_NTK_PNDNN_LNNY_TTL = lblTOTKKPendLain.Text
                .RS_KS_KLR_NTK_PNDNN_LNNY_NDNSN_RPH = txtKKPendLainRP.Text
                .RS_KS_KLR_NTK_PNDNN_LNNY_MT_NG_SNG = txtKKPendLainMA.Text

                .RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_TTL = lblTOTKKSaham.Text
                .RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_NDNSN_RPH = txtKKSahamRP.Text
                .RS_KS_KLR_NTK_PNRKN_KMBL_MDL_PRSHN_TRSRY_STCK_MT_NG_SNG = txtKKSahamMA.Text

                .RS_KS_KLR_NTK_PMBYRN_DVDN_TTL = lblTOTKKDivd.Text
                .RS_KS_KLR_NTK_PMBYRN_DVDN_NDNSN_RPH = txtKKDivdRP.Text
                .RS_KS_KLR_NTK_PMBYRN_DVDN_MT_NG_SNG = txtKKDivdMA.Text

                .RS_KS_KLR_NTK_KGTN_PNDNN_TTL = lblTOTKKKEGPEND.Text
                .RS_KS_KLR_NTK_KGTN_PNDNN_NDNSN_RPH = lblTOTKKKEGPENDRP.Text
                .RS_KS_KLR_NTK_KGTN_PNDNN_MT_NG_SNG = lblTOTKKKEGPENDMA.Text

                .RS_KS_BRSH_DR_KGTN_PNDNN_TTL = lblTOTKBKEGPEND.Text
                .RS_KS_BRSH_DR_KGTN_PNDNN_NDNSN_RPH = lblTOTKBKEGPENDRP.Text
                .RS_KS_BRSH_DR_KGTN_PNDNN_MT_NG_SNG = lblTOTKBKEGPENDMA.Text

                .SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL = lblTOTSurp.Text
                .SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_NDNSN_RPH = txtSurpRP.Text
                .SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_MT_NG_SNG = txtSurpMA.Text

                .KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL = lblTOTPenur.Text
                .KNKN_PNRNN_BRSH_KS_DN_STR_KS_NDNSN_RPH = txtPenurRP.Text
                .KNKN_PNRNN_BRSH_KS_DN_STR_KS_MT_NG_SNG = txtPenurMA.Text

                .KS_DN_STR_KS_PD_WL_PRD_TTL = lblTOTPeriod.Text
                .KS_DN_STR_KS_PD_WL_PRD_NDNSN_RPH = txtPeriodRP.Text
                .KS_DN_STR_KS_PD_WL_PRD_MT_NG_SNG = txtPeriosMA.Text

                .KS_DN_STR_KS_TTL = lblTOTKas.Text
                .KS_DN_STR_KS_NDNSN_RPH = txtKasRP.Text
                .KS_DN_STR_KS_MT_NG_SNG = txtKasMA.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP1300Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub

    Protected Sub txtSUM(sender As Object, e As EventArgs)

        ''-------------------------------------1----------------------------------''
        lblTOTKMPembInv.Text = Val(txtKMPembInvRP.Text) + Val(txtKMPembInvMA.Text)
        lblTOTKMMdlKrj.Text = Val(txtKMMdlKrjRP.Text) + Val(txtKMMdlKrjMA.Text)
        lblTOTKMMulti.Text = Val(txtKMMultiRP.Text) + Val(txtKMMultiMA.Text)
        lblTOTKMPerst.Text = Val(txtKMPerstRP.Text) + Val(txtKMPerstMA.Text)
        lblTOTKMSyar.Text = Val(txtKMSyarRP.Text) + Val(txtKMSyarMA.Text)
        lblTOTKMFee.Text = Val(txtKMFeeRP.Text) + Val(txtKMFeeMA.Text)
        lblTOTKMSewa.Text = Val(txtKMSewaRP.Text) + Val(txtKMSewaMA.Text)
        lblTOTKMChan.Text = Val(txtKMChanRP.Text) + Val(txtKMChanMA.Text)
        lblTOTKMSurat.Text = Val(txtKMSuratRP.Text) + Val(txtKMSuratMA.Text)
        lblTOTKMLain.Text = Val(txtKMLainRP.Text) + Val(txtKMLainMA.Text)

        lblTOTKMKEGOPRRP.Text = Val(txtKMPembInvRP.Text) +
                                Val(txtKMMdlKrjRP.Text) +
                                Val(txtKMMultiRP.Text) +
                                Val(txtKMPerstRP.Text) +
                                Val(txtKMSyarRP.Text) +
                                Val(txtKMFeeRP.Text) +
                                Val(txtKMSewaRP.Text) +
                                Val(txtKMChanRP.Text) +
                                Val(txtKMSuratRP.Text) +
                                Val(txtKMLainRP.Text)

        lblTOTKMKEGOPRMA.Text = Val(txtKMPembInvMA.Text) +
                                Val(txtKMMdlKrjMA.Text) +
                                Val(txtKMMultiMA.Text) +
                                Val(txtKMPerstMA.Text) +
                                Val(txtKMSyarMA.Text) +
                                Val(txtKMFeeMA.Text) +
                                Val(txtKMSewaMA.Text) +
                                Val(txtKMChanMA.Text) +
                                Val(txtKMSuratMA.Text) +
                                Val(txtKMLainMA.Text)

        lblTOTKMKEGOPR.Text = Val(lblTOTKMKEGOPRRP.Text) + Val(lblTOTKMKEGOPRMA.Text)

        lblTOTKKPembInv.Text = Val(txtKKPembInvRP.Text) + Val(txtKKPembInvMA.Text)
        lblTOTKKMdlKrj.Text = Val(txtKKMdlKrjRP.Text) + Val(txtKKMdlKrjMA.Text)
        lblTOTKKMulti.Text = Val(txtKKMultiRP.Text) + Val(txtKKMultiMA.Text)
        lblTOTKKPerst.Text = Val(txtKKPerstRP.Text) + Val(txtKKPerstMA.Text)
        lblTOTKKSyar.Text = Val(txtKKSyarRP.Text) + Val(txtKKSyarMA.Text)
        lblTOTKKFee.Text = Val(txtKKFeeRP.Text) + Val(txtKKFeeMA.Text)
        lblTOTKKSewa.Text = Val(txtKKSewaRP.Text) + Val(txtKKSewaMA.Text)
        lblTOTKKChan.Text = Val(txtKKChanRP.Text) + Val(txtKKChanMA.Text)
        lblTOTKKSurat.Text = Val(txtKKSuratRP.Text) + Val(txtKKSuratMA.Text)
        lblTOTKKLain.Text = Val(txtKKLainRP.Text) + Val(txtKKLainMA.Text)

        lblTOTKKKEGOPRRP.Text = Val(txtKKPembInvRP.Text) +
                                Val(txtKKMdlKrjRP.Text) +
                                Val(txtKKMultiRP.Text) +
                                Val(txtKKPerstRP.Text) +
                                Val(txtKKSyarRP.Text) +
                                Val(txtKKFeeRP.Text) +
                                Val(txtKKSewaRP.Text) +
                                Val(txtKKChanRP.Text) +
                                Val(txtKKSuratRP.Text) +
                                Val(txtKKLainRP.Text)

        lblTOTKKKEGOPRMA.Text = Val(txtKKPembInvMA.Text) +
                                Val(txtKKMdlKrjMA.Text) +
                                Val(txtKKMultiMA.Text) +
                                Val(txtKKPerstMA.Text) +
                                Val(txtKKSyarMA.Text) +
                                Val(txtKKFeeMA.Text) +
                                Val(txtKKSewaMA.Text) +
                                Val(txtKKChanMA.Text) +
                                Val(txtKKSuratMA.Text) +
                                Val(txtKKLainMA.Text)

        lblTOTKKKEGOPR.Text = Val(lblTOTKKKEGOPRRP.Text) + Val(lblTOTKKKEGOPRMA.Text)


        ''-------------------------------------2----------------------------------''
        lblTOTKMPel.Text = Val(txtKMPelRP.Text) + Val(txtKMPelMA.Text)
        lblTOTKMPenj.Text = Val(txtKMPenjRP.Text) + Val(txtKMPenjMA.Text)
        lblTOTKMPenjSurat.Text = Val(txtKMPenjSuratRP.Text) + Val(txtKMPenjSuratMA.Text)
        lblTOTKMDev.Text = Val(txtKMDevRP.Text) + Val(txtKMDevMA.Text)
        lblTOTKMBung.Text = Val(txtKMBungRP.Text) + Val(txtKMBungMA.Text)
        lblTOTKMInv.Text = Val(txtKMInvRP.Text) + Val(txtKMInvMA.Text)

        lblKMKEGINVRP.Text = Val(txtKMPelRP.Text) +
                             Val(txtKMPenjRP.Text) +
                             Val(txtKMPenjSuratRP.Text) +
                             Val(txtKMDevRP.Text) +
                             Val(txtKMBungRP.Text) +
                             Val(txtKMInvRP.Text)

        lblKMKEGINVMA.Text = Val(txtKMPelMA.Text) +
                             Val(txtKMPenjMA.Text) +
                             Val(txtKMPenjSuratMA.Text) +
                             Val(txtKMDevMA.Text) +
                             Val(txtKMBungMA.Text) +
                             Val(txtKMInvMA.Text)

        lblKMKEGINV.Text = Val(lblKMKEGINVRP.Text) + Val(lblKMKEGINVMA.Text)


        lblTOTKKPer.Text = Val(txtKKPerRP.Text) + Val(txtKKPerMA.Text)
        lblTOTKKPemb.Text = Val(txtKKPembRP.Text) + Val(txtKKPembMA.Text)
        lblTOTKKSur.Text = Val(txtKKSurRP.Text) + Val(txtKKSurMA.Text)
        lblTOTKKInvLain.Text = Val(txtKKInvLainRP.Text) + Val(txtKKInvLainMA.Text)

        lblKKKEGINVRP.Text = Val(txtKKPerRP.Text) +
                             Val(txtKKPembRP.Text) +
                             Val(txtKKSurRP.Text) +
                             Val(txtKKInvLainRP.Text)

        lblKKKEGINVMA.Text = Val(txtKKPerMA.Text) +
                             Val(txtKKPembMA.Text) +
                             Val(txtKKSurMA.Text) +
                             Val(txtKKInvLainMA.Text)

        lblKKKEGINV.Text = Val(lblKKKEGINVRP.Text) + Val(lblKKKEGINVMA.Text)


        ''-------------------------------------3----------------------------------''
        lblTOTKMTer.Text = Val(txtKMTerRP.Text) + Val(txtKMTerMA.Text)
        lblTOTKMPendLain.Text = Val(txtKMPendLainRP.Text) + Val(txtKMPendLainMA.Text)
        lblTOTKMSaham.Text = Val(txtKMSahamRP.Text) + Val(txtKMSahamMA.Text)

        lblTOTKMKEGPENDRP.Text = Val(txtKMTerRP.Text) +
        Val(txtKMPendLainRP.Text) +
        Val(txtKMSahamRP.Text)

        lblTOTKMKEGPENDMA.Text = Val(txtKMTerMA.Text) +
        Val(txtKMPendLainMA.Text) +
        Val(txtKMSahamMA.Text)

        lblTOTKMKEGPEND.Text = Val(lblTOTKMKEGPENDRP.Text) + Val(lblTOTKMKEGPENDMA.Text)

        lblTOTKKTerbt.Text = Val(txtKKTerbtRP.Text) + Val(txtKKTerbtMA.Text)
        lblTOTKKPendLain.Text = Val(txtKKPendLainRP.Text) + Val(txtKKPendLainMA.Text)
        lblTOTKKSaham.Text = Val(txtKKSahamRP.Text) + Val(txtKKSahamMA.Text)

        lblTOTKKKEGPENDRP.Text = Val(txtKKTerbtRP.Text) +
        Val(txtKKPendLainRP.Text) +
        Val(txtKKSahamRP.Text)

        lblTOTKKKEGPENDMA.Text = Val(txtKKTerbtMA.Text) +
        Val(txtKKPendLainMA.Text) +
        Val(txtKKSahamMA.Text)

        lblTOTKKKEGPEND.Text = Val(lblTOTKKKEGPENDRP.Text) + Val(lblTOTKKKEGPENDMA.Text)

        ''-------------------------------------4----------------------------------''
        lblTOTSurp.Text = Val(txtSurpRP.Text) + Val(txtSurpMA.Text)

        ''-------------------------------------5----------------------------------''
        lblTOTPenur.Text = Val(txtPenurRP.Text) + Val(txtPenurMA.Text)

        ''-------------------------------------6----------------------------------''
        lblTOTPeriod.Text = Val(txtPeriodRP.Text) + Val(txtPeriosMA.Text)

        ''-------------------------------------7----------------------------------''
        lblTOTKas.Text = Val(txtKasRP.Text) + Val(txtKasMA.Text)

        ''-------------------------------------KAS BERSIH----------------------------------''

        lblTOTKBKEGOPR.Text = Val(lblTOTKMKEGOPR.Text) - Val(lblTOTKKKEGOPR.Text)
        lblTOTKBKEGOPRRP.Text = Val(lblTOTKMKEGOPRRP.Text) - Val(lblTOTKKKEGOPRRP.Text)
        lblTOTKBKEGOPRMA.Text = Val(lblTOTKMKEGOPRMA.Text) - Val(lblTOTKKKEGOPRMA.Text)

        lblKBKEGINV.Text = Val(lblKMKEGINV.Text) - Val(lblKKKEGINV.Text)
        lblKBKEGINVRP.Text = Val(lblKMKEGINVRP.Text) - Val(lblKKKEGINVRP.Text)
        lblKBKEGINVMA.Text = Val(lblKMKEGINVMA.Text) - Val(lblKKKEGINVMA.Text)

        lblTOTKBKEGPEND.Text = Val(lblTOTKMKEGPEND.Text) - Val(lblTOTKKKEGPEND.Text)
        lblTOTKBKEGPENDRP.Text = Val(lblTOTKMKEGPENDRP.Text) - Val(lblTOTKKKEGPENDRP.Text)
        lblTOTKBKEGPENDMA.Text = Val(lblTOTKMKEGPENDMA.Text) - Val(lblTOTKKKEGPENDMA.Text)
    End Sub
#End Region
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_130020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.Bulandata

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP1300Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP1300
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .Bulandata = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.Bulandata & "_" & "36_Arus_Kas_TB_130020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
		If cbobulandata.Text <> "" Then
			Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		clean()
	End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP1300) As Parameter.SIPP1300
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.Bulandata

            'Table As BULANDATA Copy
            'params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            'params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP1300Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP1300
        Dim dtViewData As New DataTable
        With oEntities
            .Bulandata = txtcopybulandata.Text
            '.Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
End Class
