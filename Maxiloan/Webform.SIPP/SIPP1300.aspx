﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP1300.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP1300" MaintainScrollPositionOnPostback="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>LAPORAN ARUS KAS</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>   
    <style type="text/css">
        .total-yellow {
            background-color: yellow
        }
        .total-green {
            background-color: greenyellow
        }
        .title-gray {
            background-color: lightgray
        }
        .title-dark-gray {
            background-color: darkgray
        }
        .rightAlign {
            text-align: right
        }
    </style>   
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                  SIPP1300 - LAPORAN ARUS KAS
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                
                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                </div>
                   <div class="form_box">
                        <div class="form_single">
                        <label>
                            Generate Bulan Data
                        </label>
                        <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                            TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                     </div>
                   </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        <div class="form_box_title" style="margin-top: 3px;" >
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="2%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    <%--</asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JUMLAH TENAGA KERJA TOTAL" SortExpression="ProductCode">
                                        <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                        <asp:Label ID="lblpengurus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JMLH_TNG_KRJ_TTL__") %>'>
                                         </asp:Label></a>
                                    </ItemTemplate>--%>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                                    </asp:BoundColumn>   
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="10%">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="RS_KS_BRSH_DR_KGTN_PRS_TTL" SortExpression="RS_KS_BRSH_DR_KGTN_PRS_TTL" HeaderText="Total Arus Kas Bersih Dari Kegiatan Operasi" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="RS_KS_BRSH_DR_KGTN_NVSTS_TTL" SortExpression="RS_KS_BRSH_DR_KGTN_NVSTS_TTL" HeaderText="Total Arus Kas Bersih Untuk Kegiatan Investasi"  ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="RS_KS_BRSH_DR_KGTN_PNDNN_TTL" SortExpression="RS_KS_BRSH_DR_KGTN_PNDNN_TTL" HeaderText="Total Arus Kas Bersih Untuk Kegiatan Pendanaan"  ItemStyle-Width="10%">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL" SortExpression="SRPLS_DFST_PD_KS_DN_STR_KS_KBT_PRBHN_KRS_TTL" HeaderText="Total Surplus (Defisit) Pada Kas dan Setara Kas Akibat Perubahan Kurs"  ItemStyle-Width="10%">
                                    </asp:BoundColumn>       
                                    <asp:BoundColumn DataField="KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL" SortExpression="KNKN_PNRNN_BRSH_KS_DN_STR_KS_TTL" HeaderText="Total Kenaikan (Penurunan) Bersih Kas dan Setara Kas"  ItemStyle-Width="10%">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="KS_DN_STR_KS_PD_WL_PRD_TTL" SortExpression="KS_DN_STR_KS_PD_WL_PRD_TTL" HeaderText="Total Kas Dan Setara Kas Pada Awal Periode"  ItemStyle-Width="10%">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="KS_DN_STR_KS_TTL" SortExpression="KS_DN_STR_KS_TTL" HeaderText="Total Kas Dan Setara Kas Pada Akhir Periode" >
                                    </asp:BoundColumn>                                     
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP1300" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                     <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                </div>

                <%--<div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI RINCIAN TENAGA KERJA BERDASARKAN FUNGSI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NM_PNGRS">Nama Pengurus</asp:ListItem>
                            <asp:ListItem Value="JBTN_KPNGRSN">Jabatan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>--%>
            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>


                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>
            
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator83" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            
                <%--  --%>
               
                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>I. ARUS KAS BERSIH DARI KEGIATAN OPERASI</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label53" runat="server" Text="1. Arus Kas Surplus (Defisit) -/-"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label41" runat="server" Text="a. Arus Kas Masuk dari Kegiatan Operasi"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label3" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label4" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Investasi
                        </label>
                         <asp:TextBox ID="txtKMPembInvRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPembInvRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Modal Kerja
                        </label>
                         <asp:TextBox ID="txtKMMdlKrjRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMMdlKrjRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Multiguna
                        </label>
                         <asp:TextBox ID="txtKMMultiRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMMultiRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Prinsip Syariah
                        </label>
                         <asp:TextBox ID="txtKMSyarRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMSyarRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Lain Berdasarkan Persetujuan OJK
                        </label>
                         <asp:TextBox ID="txtKMPerstRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPerstRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Kegiatan Berbasis Fee
                        </label>
                         <asp:TextBox ID="txtKMFeeRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMFeeRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Kegiatan Sewa Operasi
                        </label>
                         <asp:TextBox ID="txtKMSewaRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMSewaRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Kegiatan Penerusan Pembiayaan Chanelling
                        </label>
                         <asp:TextBox ID="txtKMChanRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMChanRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Surat Berharga Yang Ditunjukan Untuk Diperjualbelikan
                        </label>
                         <asp:TextBox ID="txtKMSuratRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMSuratRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pendapatan Kegiatan Operasi Lainnya
                        </label>
                         <asp:TextBox ID="txtKMLainRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMLainRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                     <div class="form_right">
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Investasi
                        </label>
                         <asp:TextBox ID="txtKMPembInvMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPembInvMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Modal Kerja
                        </label>
                         <asp:TextBox ID="txtKMMdlKrjMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMMdlKrjMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Multiguna
                        </label>
                         <asp:TextBox ID="txtKMMultiMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMMultiMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Prinsip Syariah
                        </label>
                         <asp:TextBox ID="txtKMSyarMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMSyarMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pembiayaan Lain Berdasarkan Persetujuan OJK
                        </label>
                         <asp:TextBox ID="txtKMPerstMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPerstMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Kegiatan Berbasis Fee
                        </label>
                         <asp:TextBox ID="txtKMFeeMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMFeeMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Kegiatan Sewa Operasi
                        </label>
                         <asp:TextBox ID="txtKMSewaMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMSewaMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Kegiatan Penerusan Pembiayaan Chanelling
                        </label>
                         <asp:TextBox ID="txtKMChanMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMChanMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Surat Berharga Yang Ditunjukan Untuk Diperjualbelikan
                        </label>
                         <asp:TextBox ID="txtKMSuratMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMSuratMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pendapatan Kegiatan Operasi Lainnya
                        </label>
                         <asp:TextBox ID="txtKMLainMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMLainMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
               </div>   
                 <div class="form_box">
                    <div class="form_left">
                        <b><asp:Label ID="Label1" runat="server" Text="Total Arus Kas Masuk Dari Pembiayaan Investasi :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKMPembInv" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label2" runat="server" Text="Total Arus Kas Masuk Dari Pembiayaan Modal Kerja :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMMdlKrj" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label24" runat="server" Text="Total Arus Kas Masuk Dari Pembiayaan Multiguna :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMMulti" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                        
                        <br />
                        <b><asp:Label ID="Label5" runat="server" Text="Total Arus Kas Masuk Dari Pembiayaan Prinsip Syariah :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMSyar" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label7" runat="server" Text="Total Arus Kas Masuk Dari Pembiayaan Lain Berdasarkan Persetujuan OJK :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMPerst" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                     
                        
                        <br />
                        <b><asp:Label ID="Label25" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Berbasis Fee :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKMFee" runat="server">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label29" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Sewa Operasi :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMSewa" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label35" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Penerusan Pembiayaan Chanelling :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMChan" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>    
                        
                        <br />
                        <b><asp:Label ID="Label9" runat="server" Text="Total Arus Kas Masuk Dari Surat Berharga Yang Ditunjukan Untuk Diperjualbelikan :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMSurat" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label11" runat="server" Text="Total Arus Kas Masuk Dari Pendapatan Kegiatan Operasi Lainnya :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMLain" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                      
                    </div>
                </div> 
                 <div class="form_box total-yellow">
                    <div class="form_left">
                        <b><asp:Label ID="Label6" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Operasi :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKMKEGOPR" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label10" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Operasi (RUPIAH) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMKEGOPRRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label106" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Operasi (VALAS) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMKEGOPRMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                        
                    </div>
                </div> 

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label13" runat="server" Text="b. Arus Kas Keluar dari Kegiatan Operasi"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label14" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label15" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Investasi
                        </label>
                         <asp:TextBox ID="txtKKPembInvRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPembInvRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Modal Kerja
                        </label>
                         <asp:TextBox ID="txtKKMdlKrjRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKMdlKrjRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Dari Pembiayaan Multiguna
                        </label>
                         <asp:TextBox ID="txtKKMultiRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKMultiRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Prinsip Syariah
                        </label>
                         <asp:TextBox ID="txtKKSyarRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSyarRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Lain Berdasarkan Persetujuan OJK
                        </label>
                         <asp:TextBox ID="txtKKPerstRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPerstRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembayaran Bunga
                        </label>
                         <asp:TextBox ID="txtKKFeeRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKFeeRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Beban Umum Dan Administrasi
                        </label>
                         <asp:TextBox ID="txtKKSewaRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSewaRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pajak Penghasilan
                        </label>
                         <asp:TextBox ID="txtKKChanRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKChanRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Surat Berharga Yang Ditunjukan Untuk Diperjualbelikan
                        </label>
                         <asp:TextBox ID="txtKKSuratRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSuratRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembayaran Kegiatan Operasi Lainnya
                        </label>
                         <asp:TextBox ID="txtKKLainRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKLainRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                     <div class="form_right">
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Investasi
                        </label>
                         <asp:TextBox ID="txtKKPembInvMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPembInvMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Modal Kerja
                        </label>
                         <asp:TextBox ID="txtKKMdlKrjMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKMdlKrjMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Multiguna
                        </label>
                         <asp:TextBox ID="txtKKMultiMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKMultiMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Prinsip Syariah
                        </label>
                         <asp:TextBox ID="txtKKSyarMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSyarMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembiayaan Lain Berdasarkan Persetujuan OJK
                        </label>
                         <asp:TextBox ID="txtKKPerstMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPerstMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembayaran Bunga
                        </label>
                         <asp:TextBox ID="txtKKFeeMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKFeeMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Beban Umum Dan Administrasi
                        </label>
                         <asp:TextBox ID="txtKKSewaMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSewaMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pajak Penghasilan
                        </label>
                         <asp:TextBox ID="txtKKChanMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKChanMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Surat Berharga Yang Ditunjukan Untuk Diperjualbelikan
                        </label>
                         <asp:TextBox ID="txtKKSuratMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSuratMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembayaran Kegiatan Operasi Lainnya
                        </label>
                         <asp:TextBox ID="txtKKLainMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKLainMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
               </div>   
                 <div class="form_box">
                    <div class="form_left">
                        <b><asp:Label ID="Label16" runat="server" Text="Total Arus Kas Keluar Untuk Pembiayaan Investasi :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKKPembInv" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label18" runat="server" Text="Total Arus Kas Keluar Untuk Pembiayaan Modal Kerja :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKMdlKrj" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label20" runat="server" Text="Total Arus Kas Keluar Untuk Pembiayaan Multiguna :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKMulti" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                        
                        <br />
                        <b><asp:Label ID="Label22" runat="server" Text="Total Arus Kas Keluar Untuk Pembiayaan Prinsip Syariah :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKSyar" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label26" runat="server" Text="Total Arus Kas Keluar Untuk Pembiayaan Lain Berdasarkan Persetujuan OJK :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKPerst" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                     
                        
                        <br />
                        <b><asp:Label ID="Label28" runat="server" Text="Total Arus Kas Keluar Untuk Pembayaran Bunga :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKKFee" runat="server">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label31" runat="server" Text="Total Arus Kas Keluar Untuk Beban Umum Dan Administrasi :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKSewa" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label33" runat="server" Text="Total Arus Kas Keluar Untuk Pajak Penghasilan :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKChan" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>    
                        
                        <br />
                        <b><asp:Label ID="Label36" runat="server" Text="Total Arus Kas Keluar Untuk Surat Berharga Yang Ditunjukan Untuk Diperjualbelikan :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKSurat" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label38" runat="server" Text="Total Arus Kas Keluar Untuk Pembayaran Kegiatan Operasi Lainnya :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKLain" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                      
                    </div>
                </div> 
                <div class="form_box total-yellow">
                    <div class="form_left">
                        <b><asp:Label ID="Label8" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Operasi :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKKKEGOPR" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label17" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Operasi (RUPIAH) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKKEGOPRRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label21" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Operasi (VALAS) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKKEGOPRMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                            
                     </div>
                </div> 
                <div class="form_box total-green">
                    <div class="form_left">
                        <b><asp:Label ID="Label27" runat="server" Text="Total Arus Kas Bersih Dari Kegiatan Operasi :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKBKEGOPR" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label32" runat="server" Text="Total Arus Kas Bersih Dari Kegiatan Operasi (RUPIAH) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKBKEGOPRRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label37" runat="server" Text="Total Arus Kas Bersih Dari Kegiatan Operasi (VALAS) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKBKEGOPRMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                            
                     </div>
                </div> 
            
                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>II. ARUS KAS BERSIH DARI KEGIATAN INVESTASI</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label40" runat="server" Text="1. Arus Kas Surplus (Defisit)"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label42" runat="server" Text="a. Arus Kas Masuk dari Kegiatan Investasi"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label43" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label44" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Arus Kas Masuk Dari Pelepasan Anak Perusahaan
                        </label>
                         <asp:TextBox ID="txtKMPelRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPelRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Penjualan Tanah, Bangunan, dan Peralatan
                        </label>
                         <asp:TextBox ID="txtKMPenjRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPenjRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Penjualan Surat Berharga Yang Tidak Diperjualbelikan
                        </label>
                         <asp:TextBox ID="txtKMPenjSuratRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPenjSuratRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Deviden
                        </label>
                         <asp:TextBox ID="txtKMDevRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMDevRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Bunga Dari Kegiatan Investasi
                        </label>
                         <asp:TextBox ID="txtKMBungRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMBungRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Kegiatan Investasi Lainnya
                        </label>
                         <asp:TextBox ID="txtKMInvRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator46" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMInvRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                     
                    </div>
                     <div class="form_right">
                         <label class="label_req">
                            Arus Kas Masuk Dari Pelepasan Anak Perusahaan
                        </label>
                         <asp:TextBox ID="txtKMPelMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator47" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPelMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Penjualan Tanah, Bangunan, dan Peralatan
                        </label>
                         <asp:TextBox ID="txtKMPenjMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator48" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPenjMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Penjualan Surat Berharga Yang Tidak Diperjualbelikan
                        </label>
                         <asp:TextBox ID="txtKMPenjSuratMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator49" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPenjSuratMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Deviden
                        </label>
                         <asp:TextBox ID="txtKMDevMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator50" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMDevMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Bunga Dari Kegiatan Investasi
                        </label>
                         <asp:TextBox ID="txtKMBungMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator51" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMBungMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Kegiatan Investasi Lainnya
                        </label>
                         <asp:TextBox ID="txtKMInvMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMInvMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  
                     </div>
               </div>   
               <div class="form_box">
                    <div class="form_left">
                        <b><asp:Label ID="Label45" runat="server" Text="Total Arus Kas Masuk Dari Pelepasan Anak Perusahaan :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKMPel" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label47" runat="server" Text="Total Arus Kas Masuk Dari Penjualan Tanah, Bangunan, dan Peralatan  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMPenj" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label49" runat="server" Text="Total Arus Kas Masuk Dari Penjualan Surat Berharga Yang Tidak Diperjualbelikan  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMPenjSurat" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                        
                        <br />
                        <b><asp:Label ID="Label51" runat="server" Text="Total Arus Kas Masuk Deviden :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMDev" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label54" runat="server" Text="Total Arus Kas Masuk Bunga Dari Kegiatan Investasi :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMBung" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                     
                        
                        <br />
                        <b><asp:Label ID="Label56" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Investasi Lainnya :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKMInv" runat="server">0</asp:Label></b>            
                    </div>
                </div> 
                <div class="form_box total-yellow">
                    <div class="form_left">
                        <b><asp:Label ID="Label12" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Investasi :"></asp:Label></b>                      
                        <b><asp:Label ID="lblKMKEGINV" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label23" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Investasi (RUPIAH)  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblKMKEGINVRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label34" runat="server" Text="Total Arus Kas Masuk Dari Kegiatan Investasi (VALAS)  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblKMKEGINVMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                                                         
                    </div>
                </div> 

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label66" runat="server" Text="b. Arus Kas Keluar Untuk Kegiatan Investasi"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label67" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label68" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Arus Kas Keluar Untuk Perolehan Atas Anak Perusahaan
                        </label>
                         <asp:TextBox ID="txtKKPerRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPerRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembelian Tanah, Bangunan Dan Peralatan
                        </label>
                         <asp:TextBox ID="txtKKPembRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator62" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPembRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Perolehan Surat Berharga Yang Tidak Dimaksudkan Untuk Diperjualbelikan 
                        </label>
                         <asp:TextBox ID="txtKKSurRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator63" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSurRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Kegiatan Investasi Lainnya
                        </label>
                         <asp:TextBox ID="txtKKInvLainRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator64" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKInvLainRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                  
                    </div>
                     <div class="form_right">
                     <label class="label_req">
                            Arus Kas Keluar Untuk Perolehan Atas Anak Perusahaan
                        </label>
                         <asp:TextBox ID="txtKKPerMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator53" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPerMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembelian Tanah, Bangunan Dan Peralatan
                        </label>
                         <asp:TextBox ID="txtKKPembMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator54" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPembMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Perolehan Surat Berharga Yang Tidak Dimaksudkan Untuk Diperjualbelikan 
                        </label>
                         <asp:TextBox ID="txtKKSurMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSurMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Kegiatan Investasi Lainnya
                        </label>
                         <asp:TextBox ID="txtKKInvLainMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKInvLainMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                                     
                     </div>
               </div>   
                 <div class="form_box">
                    <div class="form_left">
                        <b><asp:Label ID="Label69" runat="server" Text="Total Arus Kas Keluar Untuk Perolehan Atas Anak Perusahaan :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKKPer" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label71" runat="server" Text="Total Arus Kas Keluar Untuk Pembelian Tanah, Bangunan Dan Peralatan :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKPemb" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label73" runat="server" Text="Total Arus Kas Keluar Untuk Perolehan Surat Berharga Yang Tidak Dimaksudkan Untuk Diperjualbelikan :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKSur" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                        
                        <br />
                        <b><asp:Label ID="Label75" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Investasi Lainnya :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKInvLain" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                
                    </div>
                </div> 
                <div class="form_box total-yellow">
                    <div class="form_left">
                        <b><asp:Label ID="Label19" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Investasi :"></asp:Label></b>                      
                        <b><asp:Label ID="lblKKKEGINV" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label39" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Investasi (RUPIAH)  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblKKKEGINVRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label48" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Investasi (VALAS)  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblKKKEGINVMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                                                         
                    </div>
                </div> 
                 <div class="form_box total-green">
                    <div class="form_single">
                        <b><asp:Label ID="Label30" runat="server" Text="Total Arus Kas Bersih Untuk Kegiatan Investasi :"></asp:Label></b>                      
                        <b><asp:Label ID="lblKBKEGINV" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label50" runat="server" Text="Total Arus Kas Bersih Untuk Kegiatan Investasi (RUPIAH)  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblKBKEGINVRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label55" runat="server" Text="Total Arus Kas Bersih Untuk Kegiatan Investasi (VALAS)  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblKBKEGINVMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                                                            
                    </div>
                </div> 

                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>III. ARUS KAS BERSIH DARI KEGIATAN PENDANAAN</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label58" runat="server" Text="1. Arus Kas Surplus (Defisit)"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label59" runat="server" Text="a. Arus Kas Masuk dari Kegiatan Pendanaan"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label60" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label61" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Arus Kas Masuk Dari Pinjaman dan Penerbitan Surat Berharga
                        </label>
                         <asp:TextBox ID="txtKMTerRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMTerRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pendanaan Lainnya
                        </label>
                         <asp:TextBox ID="txtKMPendLainRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator58" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPendLainRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Penerbitan Modal Saham
                        </label>
                         <asp:TextBox ID="txtKMSahamRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMSahamRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                     
                    </div>
                     <div class="form_right">
                        <label class="label_req">
                            Arus Kas Masuk Dari Pinjaman dan Penerbitan Surat Berharga
                        </label>
                         <asp:TextBox ID="txtKMTerMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMTerMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Pendanaan Lainnya
                        </label>
                         <asp:TextBox ID="txtKMPendLainMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator65" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMPendLainMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Masuk Dari Penerbitan Modal Saham
                        </label>
                         <asp:TextBox ID="txtKMSahamMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKMSahamMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  
                     </div>
               </div>   
                 <div class="form_box">
                    <div class="form_left">
                        <b><asp:Label ID="Label62" runat="server" Text="Total Arus Kas Masuk Dari Pinjaman dan Penerbitan Surat Berharga :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKMTer" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label64" runat="server" Text="Total Arus Kas Masuk Dari Pendanaan Lainnya  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMPendLain" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label77" runat="server" Text="Total Arus Kas Masuk Dari Penerbitan Modal Saham :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMSaham" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                        
                    </div>
                </div> 
                <div class="form_box total-yellow">
                    <div class="form_left">
                        <b><asp:Label ID="Label46" runat="server" Text="Total Arus Kas Masuk dari Kegiatan Pendanaan :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKMKEGPEND" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label57" runat="server" Text="Total Arus Kas Masuk dari Kegiatan Pendanaan (RUPIAH)  :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMKEGPENDRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label65" runat="server" Text="Total Arus Kas Masuk dari Kegiatan Pendanaan (VALAS) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKMKEGPENDMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                        
                    </div>
                </div> 

                <div class="form_box title-dark-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label79" runat="server" Text="b. Arus Kas Keluar Untuk Kegiatan Pendanaan"></asp:Label>
                        </b>     
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label80" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label81" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembayaran Pokok Pinjaman dan Surat Berharga yang Diterbitkan 
                        </label>
                         <asp:TextBox ID="txtKKTerbtRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKTerbtRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pendanaan Lainnya
                        </label>
                         <asp:TextBox ID="txtKKPendLainRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator68" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPendLainRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Penarikan Kembali Modal Perusahaan 
                        </label>
                         <asp:TextBox ID="txtKKSahamRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator69" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSahamRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembayaran Dividen
                        </label>
                         <asp:TextBox ID="txtKKDivdRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator70" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKDivdRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                  
                    </div>
                     <div class="form_right">
                     <label class="label_req">
                            Arus Kas Keluar Untuk Pembayaran Pokok Pinjaman dan Surat Berharga yang Diterbitkan 
                        </label>
                         <asp:TextBox ID="txtKKTerbtMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator71" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKTerbtRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pendanaan Lainnya
                        </label>
                         <asp:TextBox ID="txtKKPendLainMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator72" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKPendLainMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Penarikan Kembali Modal Perusahaan 
                        </label>
                         <asp:TextBox ID="txtKKSahamMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator73" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKSahamMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Arus Kas Keluar Untuk Pembayaran Dividen
                        </label>
                         <asp:TextBox ID="txtKKDivdMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator74" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKKDivdMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                                     
                     </div>
               </div>   
                 <div class="form_box">
                    <div class="form_left">
                        <b><asp:Label ID="Label82" runat="server" Text="Total Arus Kas Keluar Untuk Pembayaran Pokok Pinjaman dan Surat Berharga yang Diterbitkan  :"></asp:Label></b>
                        <b><asp:Label ID="lblTOTKKTerbt" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label84" runat="server" Text="Total Arus Kas Keluar Untuk Pendanaan Lainnya :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKPendLain" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label86" runat="server" Text="Total Arus Kas Keluar Untuk Penarikan Kembali Modal Perusahaan :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKSaham" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>     
                        
                        <br />
                        <b><asp:Label ID="Label88" runat="server" Text="Total Arus Kas Keluar Untuk Pembayaran Dividen :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKDivd" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                
                    </div>
                </div> 
                <div class="form_box total-yellow">
                    <div class="form_left">
                        <b><asp:Label ID="Label52" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Pendanaan  :"></asp:Label></b> 
                        <b><asp:Label ID="lblTOTKKKEGPEND" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label72" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Pendanaan (RUPIAH) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKKEGPENDRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label76" runat="server" Text="Total Arus Kas Keluar Untuk Kegiatan Pendanaan (VALAS) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKKKEGPENDMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                   
                    </div>
                </div> 
                <div class="form_box total-green">
                    <div class="form_left">
                        <b><asp:Label ID="Label70" runat="server" Text="Total Arus Kas Bersih Untuk Kegiatan Pendanaan  :"></asp:Label></b>     
                        <b><asp:Label ID="lblTOTKBKEGPEND" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label85" runat="server" Text="Total Arus Kas Bersih Untuk Kegiatan Pendanaan (RUPIAH) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKBKEGPENDRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label89" runat="server" Text="Total Arus Kas Bersih Untuk Kegiatan Pendanaan (VALAS) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTKBKEGPENDMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                   
                    </div>
                </div> 

                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>IV. SURPLUS (DEFISIT) PADA KAS DAN SETARA KAS AKIBAT PERUBAHAN KURS</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label92" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label93" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Surplus (Defisit) Pada Kas dan Setara Kas Akibat Perubahan Kurs
                        </label>
                         <asp:TextBox ID="txtSurpRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator75" runat="server" ErrorMessage="*"
                            ControlToValidate="txtSurpRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                                       
                    </div>
                     <div class="form_right">
                        <label class="label_req">
                            Surplus (Defisit) Pada Kas dan Setara Kas Akibat Perubahan Kurs
                        </label>
                         <asp:TextBox ID="txtSurpMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator76" runat="server" ErrorMessage="*"
                            ControlToValidate="txtSurpMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     </div>
               </div>   
                 <div class="form_box total-green">
                    <div class="form_left">
                        <b><asp:Label ID="Label94" runat="server" Text="Total Surplus (Defisit) Pada Kas dan Setara Kas Akibat Perubahan Kurs :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTSurp" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                             
                    </div>
                </div> 

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>V. KENAIKAN (PENURUNAN) BERSIH KAS DAN SETARA KAS</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label90" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label91" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Kenaikan (Penurunan) Bersih Kas dan Setara Kas
                        </label>
                         <asp:TextBox ID="txtPenurRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator77" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPenurRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                         
                    </div>
                     <div class="form_right">
                        <label class="label_req">
                            Kenaikan (Penurunan) Bersih Kas dan Setara Kas
                        </label>
                         <asp:TextBox ID="txtPenurMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator78" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPenurMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     </div>
               </div>   
                 <div class="form_box total-green">
                    <div class="form_left">
                        <b><asp:Label ID="Label96" runat="server" Text="Total Kenaikan (Penurunan) Bersih Kas dan Setara Kas :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTPenur" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                             
                    </div>
                </div> 

               <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>VI. KAS DAN SETARA KAS PADA AWAL PERIODE</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label98" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label99" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Kas Dan Setara Kas Pada Awal Periode
                        </label>
                         <asp:TextBox ID="txtPeriodRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator79" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPeriodRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                         
                    </div>
                     <div class="form_right">
                        <label class="label_req">
                            Kas Dan Setara Kas Pada Awal Periode
                        </label>
                         <asp:TextBox ID="txtPeriosMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator80" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPeriosMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     </div>
               </div>   
                 <div class="form_box total-green">
                    <div class="form_left">
                        <b><asp:Label ID="Label100" runat="server" Text="Total Kas Dan Setara Kas Pada Awal Periode :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTPeriod" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                             
                    </div>
                </div> 

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>VII. KAS DAN SETARA KAS PADA AKHIR PERIODE</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label102" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label103" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Kas Dan Setara Kas Pada Akhir Periode
                        </label>
                         <asp:TextBox ID="txtKasRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator81" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKasRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                         
                    </div>
                     <div class="form_right">
                        <label class="label_req">
                            Kas Dan Setara Kas Pada Akhir Periode
                        </label>
                         <asp:TextBox ID="txtKasMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator82" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKasMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     </div>
               </div>   
                 <div class="form_box total-green">
                    <div class="form_left">
                        <b><asp:Label ID="Label104" runat="server" Text="Total Kas Dan Setara Kas Pada Akhir Periode :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTKas" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                             
                    </div>
                </div>              
            </ContentTemplate>
            </asp:UpdatePanel>
                    <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>