﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class SIPP1110
    Inherits Maxiloan.Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New SIPP1110Controller
    Private oCustomclass As New Parameter.SIPP1110
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNaviSIPP1110 As ucGridNav

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviSIPP1110.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP1110"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                pnlcopybulandata.Visible = False
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)

                FillCombo()
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
            End If
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviSIPP1110.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.SIPP1110

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP1110(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        If (isFrNav = False) Then
            GridNaviSIPP1110.Initialize(recordCount, pageSize)
        End If

        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
    End Sub

    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP1110
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP1110
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Valuemaster = valuemaster
        oAssetData.Valueparent = valueparent
        oAssetData.Valueelement = valueelement
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ElementLabel"
        cboName.DataValueField = "Element"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP1110", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False

            Me.Process = "ADD"
            clean()

        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Search"
    'Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
    '    If txtSearch.Text <> "" Then
    '        Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
    '    Else
    '        Me.SearchBy = ""
    '    End If
    '    Me.SortBy = ""
    '    DoBind(Me.SearchBy, Me.SortBy)
    'End Sub
#End Region

#Region "Reset"
    'Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
    '    Me.SearchBy = ""
    '    Me.SortBy = ""
    '    DoBind(Me.SearchBy, Me.SortBy)
    '    'txtSearch.Text = ""
    '    'cboSearch.ClearSelection()
    '    pnlAdd.Visible = False
    'End Sub
#End Region
    Sub clean()
        txtbulandata.Text = ""
        txtBankDNRP.Text = 0
        txtKeuDNRP.Text = 0
        txtLnDNRP.Text = 0

        txtBankDNMA.Text = 0
        txtKeuDNMA.Text = 0
        txtLnDNMA.Text = 0

        lblTOTDNBank.Text = 0
        lblTOTDNKeu.Text = 0
        lblTOTDNLn.Text = 0

        txtBankLNRP.Text = 0
        txtKeuLNRP.Text = 0
        txtLnLNRP.Text = 0

        txtBankLNMA.Text = 0
        txtKeuLNMA.Text = 0
        txtLnLNMA.Text = 0

        lblTOTLNBank.Text = 0
        lblTOTLNKeu.Text = 0
        lblTOTLNLn.Text = 0

        txtPembNasRP.Text = 0
        txtPembNasMA.Text = 0

        lblTOTPembNas.Text = 0

        txtPinjDNRP.Text = 0
        txtPinjLNRP.Text = 0

        txtPinjDNMA.Text = 0
        txtPinjLNMA.Text = 0

        lblTOTDNPinj.Text = 0
        lblTOTLNPinj.Text = 0

        txtChanRP.Text = 0
        txtJointRP.Text = 0

        txtChanMA.Text = 0
        txtJointMA.Text = 0

        lblTOTChan.Text = 0
        lblTOTJoint.Text = 0

        txtIntrRP.Text = 0
        txtCurRP.Text = 0
        txtCrosRP.Text = 0
        txtForwRP.Text = 0
        txtOptRP.Text = 0
        txtFutuRP.Text = 0
        txtLainRP.Text = 0

        txtIntrMA.Text = 0
        txtCurMA.Text = 0
        txtCrosMA.Text = 0
        txtForwMA.Text = 0
        txtOptMA.Text = 0
        txtFutuMA.Text = 0
        txtLainMA.Text = 0

        lblTOTIntr.Text = 0
        lblTOTCur.Text = 0
        lblTOTCros.Text = 0
        lblTOTForw.Text = 0
        lblTOTOpt.Text = 0
        lblTOTFutu.Text = 0
        lblTOTLain.Text = 0

        txtHpsBukuRP.Text = 0
        txtHpsBukuTghRP.Text = 0
        txtHpsTghRP.Text = 0

        txtHpsBukuMA.Text = 0
        txtHpsBukuTghMA.Text = 0
        txtHpsTghMA.Text = 0

        lblTOTHpsBuku.Text = 0
        lblTOTHpsBukuTgh.Text = 0
        lblTOTHpsTgh.Text = 0
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP1110
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP1110", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
				pnlList.Visible = False
				txtbulandata.Enabled = False

				oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.SIPP1110Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")

                txtLnLNMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG")
                txtBankDNRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH")
                txtKeuDNRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH")
                txtLnDNRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH")

                txtBankDNMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG")
                txtKeuDNMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG")
                txtLnDNMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG")

                lblTOTDNBank.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL")
                lblTOTDNKeu.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL")
                lblTOTDNLn.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL")

                txtBankLNRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH")
                txtKeuLNRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH")
                txtLnLNRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH")

                txtBankLNMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG")
                txtKeuLNMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG")

                lblTOTLNBank.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL")
                lblTOTLNKeu.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL")
                lblTOTLNLn.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL")

                txtPembNasRP.Text = oParameter.ListData.Rows(0)("FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH")
                txtPembNasMA.Text = oParameter.ListData.Rows(0)("FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG")

                lblTOTPembNas.Text = oParameter.ListData.Rows(0)("FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL")

                txtPinjDNRP.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH")
                txtPinjLNRP.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG")

                txtPinjDNMA.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH")
                txtPinjLNMA.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG")

                lblTOTDNPinj.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL")
                lblTOTLNPinj.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL")

                txtChanRP.Text = oParameter.ListData.Rows(0)("PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH")
                txtJointRP.Text = oParameter.ListData.Rows(0)("PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH")

                txtChanMA.Text = oParameter.ListData.Rows(0)("PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG")
                txtJointMA.Text = oParameter.ListData.Rows(0)("PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG")

                lblTOTChan.Text = oParameter.ListData.Rows(0)("PNRSN_KRDTPMBYN_CHNNLNG_TTL")
                lblTOTJoint.Text = oParameter.ListData.Rows(0)("PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL")

                txtIntrRP.Text = oParameter.ListData.Rows(0)("NMNL_NTRST_RT_SWP_NDNSN_RPH")
                txtCurRP.Text = oParameter.ListData.Rows(0)("NMNL_CRRNCY_SWP_NDNSN_RPH")
                txtCrosRP.Text = oParameter.ListData.Rows(0)("NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH")
                txtForwRP.Text = oParameter.ListData.Rows(0)("NMNL_FRWRD_NDNSN_RPH")
                txtOptRP.Text = oParameter.ListData.Rows(0)("NMNL_PTN_NDNSN_RPH")
                txtFutuRP.Text = oParameter.ListData.Rows(0)("NMNL_FTR_NDNSN_RPH")
                txtLainRP.Text = oParameter.ListData.Rows(0)("NMNL_DRVTF_LNNY_NDNSN_RPH")

                txtIntrMA.Text = oParameter.ListData.Rows(0)("NMNL_NTRST_RT_SWP_MT_NG_SNG")
                txtCurMA.Text = oParameter.ListData.Rows(0)("NMNL_CRRNCY_SWP_MT_NG_SNG")
                txtCrosMA.Text = oParameter.ListData.Rows(0)("NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG")
                txtForwMA.Text = oParameter.ListData.Rows(0)("NMNL_FRWRD_MT_NG_SNG")
                txtOptMA.Text = oParameter.ListData.Rows(0)("NMNL_PTN_MT_NG_SNG")
                txtFutuMA.Text = oParameter.ListData.Rows(0)("NMNL_FTR_MT_NG_SNG")
                txtLainMA.Text = oParameter.ListData.Rows(0)("NMNL_DRVTF_LNNY_MT_NG_SNG")

                lblTOTIntr.Text = oParameter.ListData.Rows(0)("NMNL_NTRST_RT_SWP_TTL")
                lblTOTCur.Text = oParameter.ListData.Rows(0)("NMNL_CRRNCY_SWP_TTL")
                lblTOTCros.Text = oParameter.ListData.Rows(0)("NMNL_CRSS_CRRNCY_SWP_TTL")
                lblTOTForw.Text = oParameter.ListData.Rows(0)("NMNL_FRWRD_TTL")
                lblTOTOpt.Text = oParameter.ListData.Rows(0)("NMNL_PTN_TTL")
                lblTOTFutu.Text = oParameter.ListData.Rows(0)("NMNL_FTR_TTL")
                lblTOTLain.Text = oParameter.ListData.Rows(0)("NMNL_DRVTF_LNNY_TTL")

                txtHpsBukuRP.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_BK_NDNSN_RPH")
                txtHpsBukuTghRP.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH")
                txtHpsTghRP.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_TGH_NDNSN_RPH")

                txtHpsBukuMA.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_BK_MT_NG_SNG")
                txtHpsBukuTghMA.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG")
                txtHpsTghMA.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_TGH_MT_NG_SNG")

                lblTOTHpsBuku.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_BK_TTL")
                lblTOTHpsBukuTgh.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL")
                lblTOTHpsTgh.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_HPS_TGH_TTL")

                '----------------------------------------------------------------'
                lblTOTDN.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL")
                lblTOTDNRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH")
                lblTOTDNMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG")

                lblTOTLN.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL")
                lblTOTLNRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH")
                lblTOTLNMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG")

                lblTOTBlmTarik.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_TTL")
                lblTOTBlmTarikRP.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH")
                lblTOTBlmTarikMA.Text = oParameter.ListData.Rows(0)("FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG")

                lblTOTDRVTF.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_TTL")
                lblTOTDRVTFRP.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH")
                lblTOTDRVTFMA.Text = oParameter.ListData.Rows(0)("PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG")

                lblTOTPNYLRN.Text = oParameter.ListData.Rows(0)("PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL")
                lblTOTPNYLRNRP.Text = oParameter.ListData.Rows(0)("PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH")
                lblTOTPNYLRNMA.Text = oParameter.ListData.Rows(0)("PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG")

                lblTOTDRVTF.Text = oParameter.ListData.Rows(0)("NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL")
                lblTOTDRVTFRP.Text = oParameter.ListData.Rows(0)("NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH")
                lblTOTDRVTFMA.Text = oParameter.ListData.Rows(0)("NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG")

                lblTOTLainnya.Text = oParameter.ListData.Rows(0)("RKNNG_DMNSTRTF_LNNY_TTL")
                lblTOTLainnyaRP.Text = oParameter.ListData.Rows(0)("RKNNG_DMNSTRTF_LNNY_NDNSN_RPH")
                lblTOTLainnyaMA.Text = oParameter.ListData.Rows(0)("RKNNG_DMNSTRTF_LNNY_MT_NG_SNG")

                lblTOTADM.Text = oParameter.ListData.Rows(0)("RKNNG_DMNSTRTF_TTL")
                lblTOTADMRP.Text = oParameter.ListData.Rows(0)("RKNNG_DMNSTRTF_NDNSN_RPH")
                lblTOTADMMA.Text = oParameter.ListData.Rows(0)("RKNNG_DMNSTRTF_MT_NG_SNG")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP1110", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP1110
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.SIPP1110Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.SIPP1110
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                .BULANDATA = txtbulandata.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH = txtBankDNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH = txtKeuDNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH = txtLnDNRP.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG = txtBankDNMA.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG = txtKeuDNMA.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG = txtLnDNMA.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL = lblTOTDNBank.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL = lblTOTDNKeu.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL = lblTOTDNLn.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH = txtBankLNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH = txtKeuLNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH = txtLnLNRP.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG = txtBankLNMA.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG = txtKeuLNMA.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG = txtLnLNMA.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL = lblTOTLNBank.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL = lblTOTLNKeu.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL = lblTOTLNLn.Text

                .FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH = txtPembNasRP.Text
                .FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG = txtPembNasMA.Text

                .FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL = lblTOTPembNas.Text

                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH = txtPinjDNRP.Text
                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG = txtPinjLNRP.Text

                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH = txtPinjDNMA.Text
                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG = txtPinjLNMA.Text

                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL = lblTOTDNPinj.Text
                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL = lblTOTLNPinj.Text

                .PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH = txtChanRP.Text
                .PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH = txtJointRP.Text

                .PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG = txtChanMA.Text
                .PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG = txtJointMA.Text

                .PNRSN_KRDTPMBYN_CHNNLNG_TTL = lblTOTChan.Text
                .PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL = lblTOTJoint.Text

                .NMNL_NTRST_RT_SWP_NDNSN_RPH = txtIntrRP.Text
                .NMNL_CRRNCY_SWP_NDNSN_RPH = txtCurRP.Text
                .NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH = txtCrosRP.Text
                .NMNL_FRWRD_NDNSN_RPH = txtForwRP.Text
                .NMNL_PTN_NDNSN_RPH = txtOptRP.Text
                .NMNL_FTR_NDNSN_RPH = txtFutuRP.Text
                .NMNL_DRVTF_LNNY_NDNSN_RPH = txtLainRP.Text

                .NMNL_NTRST_RT_SWP_MT_NG_SNG = txtIntrMA.Text
                .NMNL_CRRNCY_SWP_MT_NG_SNG = txtCurMA.Text
                .NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG = txtCrosMA.Text
                .NMNL_FRWRD_MT_NG_SNG = txtForwMA.Text
                .NMNL_PTN_MT_NG_SNG = txtOptMA.Text
                .NMNL_FTR_MT_NG_SNG = txtFutuMA.Text
                .NMNL_DRVTF_LNNY_MT_NG_SNG = txtLainMA.Text

                .NMNL_NTRST_RT_SWP_TTL = lblTOTIntr.Text
                .NMNL_CRRNCY_SWP_TTL = lblTOTCur.Text
                .NMNL_CRSS_CRRNCY_SWP_TTL = lblTOTCros.Text
                .NMNL_FRWRD_TTL = lblTOTForw.Text
                .NMNL_PTN_TTL = lblTOTOpt.Text
                .NMNL_FTR_TTL = lblTOTFutu.Text
                .NMNL_DRVTF_LNNY_TTL = lblTOTLain.Text

                .PTNG_PMBYN_HPS_BK_NDNSN_RPH = txtHpsBukuRP.Text
                .PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH = txtHpsBukuTghRP.Text
                .PTNG_PMBYN_HPS_TGH_NDNSN_RPH = txtHpsTghRP.Text

                .PTNG_PMBYN_HPS_BK_MT_NG_SNG = txtHpsBukuMA.Text
                .PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG = txtHpsBukuTghMA.Text
                .PTNG_PMBYN_HPS_TGH_MT_NG_SNG = txtHpsTghMA.Text

                .PTNG_PMBYN_HPS_BK_TTL = lblTOTHpsBuku.Text
                .PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL = lblTOTHpsBukuTgh.Text
                .PTNG_PMBYN_HPS_TGH_TTL = lblTOTHpsTgh.Text
                '----------------------------------------------------------------'
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL = lblTOTDN.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH = lblTOTDNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG = lblTOTDNMA.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL = lblTOTLN.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH = lblTOTLNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG = lblTOTLNMA.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_TTL = lblTOTBlmTarik.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH = lblTOTBlmTarikRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG = lblTOTBlmTarikMA.Text

                .PNRBTN_SRT_SNGGP_BYR_TTL = lblTOTDRVTF.Text
                .PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH = lblTOTDRVTFRP.Text
                .PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG = lblTOTDRVTFMA.Text

                .PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL = lblTOTPNYLRN.Text
                .PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH = lblTOTPNYLRNRP.Text
                .PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG = lblTOTPNYLRNMA.Text

                .NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL = lblTOTDRVTF.Text
                .NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH = lblTOTDRVTFRP.Text
                .NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG = lblTOTDRVTFMA.Text

                .RKNNG_DMNSTRTF_LNNY_TTL = lblTOTLainnya.Text
                .RKNNG_DMNSTRTF_LNNY_NDNSN_RPH = lblTOTLainnyaRP.Text
                .RKNNG_DMNSTRTF_LNNY_MT_NG_SNG = lblTOTLainnyaMA.Text

                .RKNNG_DMNSTRTF_TTL = lblTOTADM.Text
                .RKNNG_DMNSTRTF_NDNSN_RPH = lblTOTADMRP.Text
                .RKNNG_DMNSTRTF_MT_NG_SNG = lblTOTADMMA.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP1110Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter
                .ID = txtid.Text
                '.BULANDATA
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_NDNSN_RPH = txtBankDNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_NDNSN_RPH = txtKeuDNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_NDNSN_RPH = txtLnDNRP.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_MT_NG_SNG = txtBankDNMA.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_MT_NG_SNG = txtKeuDNMA.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_MT_NG_SNG = txtLnDNMA.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_DLM_NGR_TTL = lblTOTDNBank.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_DLM_NGR_TTL = lblTOTDNKeu.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_LNNY_TTL = lblTOTDNLn.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_NDNSN_RPH = txtBankLNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_NDNSN_RPH = txtKeuLNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_NDNSN_RPH = txtLnLNRP.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_MT_NG_SNG = txtBankLNMA.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_MT_NG_SNG = txtKeuLNMA.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_MT_NG_SNG = txtLnLNMA.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_BNK_D_LR_NGR_TTL = lblTOTLNBank.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LMBG_JS_KNGN_BKN_BNK_D_LR_NGR_TTL = lblTOTLNKeu.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_LNNY_TTL = lblTOTLNLn.Text

                .FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_NDNSN_RPH = txtPembNasRP.Text
                .FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_MT_NG_SNG = txtPembNasMA.Text

                .FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL = lblTOTPembNas.Text

                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_NDNSN_RPH = txtPinjDNRP.Text
                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_MT_NG_SNG = txtPinjLNRP.Text

                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_NDNSN_RPH = txtPinjDNMA.Text
                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_MT_NG_SNG = txtPinjLNMA.Text

                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_DLM_NGR_TTL = lblTOTDNPinj.Text
                .PNRBTN_SRT_SNGGP_BYR_NTK_PNJMN_LR_NGR_TTL = lblTOTLNPinj.Text

                .PNRSN_KRDTPMBYN_CHNNLNG_NDNSN_RPH = txtChanRP.Text
                .PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_NDNSN_RPH = txtJointRP.Text

                .PNRSN_KRDTPMBYN_CHNNLNG_MT_NG_SNG = txtChanMA.Text
                .PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_MT_NG_SNG = txtJointMA.Text

                .PNRSN_KRDTPMBYN_CHNNLNG_TTL = lblTOTChan.Text
                .PNYLRN_KRDT_DLM_RNGK_PMBYN_BRSM_JNT_FNNCNG_TTL = lblTOTJoint.Text

                .NMNL_NTRST_RT_SWP_NDNSN_RPH = txtIntrRP.Text
                .NMNL_CRRNCY_SWP_NDNSN_RPH = txtCurRP.Text
                .NMNL_CRSS_CRRNCY_SWP_NDNSN_RPH = txtCrosRP.Text
                .NMNL_FRWRD_NDNSN_RPH = txtForwRP.Text
                .NMNL_PTN_NDNSN_RPH = txtOptRP.Text
                .NMNL_FTR_NDNSN_RPH = txtFutuRP.Text
                .NMNL_DRVTF_LNNY_NDNSN_RPH = txtLainRP.Text

                .NMNL_NTRST_RT_SWP_MT_NG_SNG = txtIntrMA.Text
                .NMNL_CRRNCY_SWP_MT_NG_SNG = txtCurMA.Text
                .NMNL_CRSS_CRRNCY_SWP_MT_NG_SNG = txtCrosMA.Text
                .NMNL_FRWRD_MT_NG_SNG = txtForwMA.Text
                .NMNL_PTN_MT_NG_SNG = txtOptMA.Text
                .NMNL_FTR_MT_NG_SNG = txtFutuMA.Text
                .NMNL_DRVTF_LNNY_MT_NG_SNG = txtLainMA.Text

                .NMNL_NTRST_RT_SWP_TTL = lblTOTIntr.Text
                .NMNL_CRRNCY_SWP_TTL = lblTOTCur.Text
                .NMNL_CRSS_CRRNCY_SWP_TTL = lblTOTCros.Text
                .NMNL_FRWRD_TTL = lblTOTForw.Text
                .NMNL_PTN_TTL = lblTOTOpt.Text
                .NMNL_FTR_TTL = lblTOTFutu.Text
                .NMNL_DRVTF_LNNY_TTL = lblTOTLain.Text

                .PTNG_PMBYN_HPS_BK_NDNSN_RPH = txtHpsBukuRP.Text
                .PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_NDNSN_RPH = txtHpsBukuTghRP.Text
                .PTNG_PMBYN_HPS_TGH_NDNSN_RPH = txtHpsTghRP.Text

                .PTNG_PMBYN_HPS_BK_MT_NG_SNG = txtHpsBukuMA.Text
                .PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_MT_NG_SNG = txtHpsBukuTghMA.Text
                .PTNG_PMBYN_HPS_TGH_MT_NG_SNG = txtHpsTghMA.Text

                .PTNG_PMBYN_HPS_BK_TTL = lblTOTHpsBuku.Text
                .PTNG_PMBYN_HPS_BK_YNG_BRHSL_DTGH_TTL = lblTOTHpsBukuTgh.Text
                .PTNG_PMBYN_HPS_TGH_TTL = lblTOTHpsTgh.Text
                '----------------------------------------------------------------'
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_TTL = lblTOTDN.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_NDNSN_RPH = lblTOTDNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_DLM_NGR_MT_NG_SNG = lblTOTDNMA.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_TTL = lblTOTLN.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_NDNSN_RPH = lblTOTLNRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_DR_LR_NGR_MT_NG_SNG = lblTOTLNMA.Text

                .FSLTS_PNDNN_YNG_BLM_DTRK_TTL = lblTOTBlmTarik.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_NDNSN_RPH = lblTOTBlmTarikRP.Text
                .FSLTS_PNDNN_YNG_BLM_DTRK_MT_NG_SNG = lblTOTBlmTarikMA.Text

                .PNRBTN_SRT_SNGGP_BYR_TTL = lblTOTDRVTF.Text
                .PNRBTN_SRT_SNGGP_BYR_NDNSN_RPH = lblTOTDRVTFRP.Text
                .PNRBTN_SRT_SNGGP_BYR_MT_NG_SNG = lblTOTDRVTFMA.Text

                .PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL = lblTOTPNYLRN.Text
                .PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_NDNSN_RPH = lblTOTPNYLRNRP.Text
                .PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_MT_NG_SNG = lblTOTPNYLRNMA.Text

                .NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL = lblTOTDRVTF.Text
                .NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_NDNSN_RPH = lblTOTDRVTFRP.Text
                .NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_MT_NG_SNG = lblTOTDRVTFMA.Text

                .RKNNG_DMNSTRTF_LNNY_TTL = lblTOTLainnya.Text
                .RKNNG_DMNSTRTF_LNNY_NDNSN_RPH = lblTOTLainnyaRP.Text
                .RKNNG_DMNSTRTF_LNNY_MT_NG_SNG = lblTOTLainnyaMA.Text

                .RKNNG_DMNSTRTF_TTL = lblTOTADM.Text
                .RKNNG_DMNSTRTF_NDNSN_RPH = lblTOTADMRP.Text
                .RKNNG_DMNSTRTF_MT_NG_SNG = lblTOTADMMA.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP1110Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub

    Protected Sub txtSUM(sender As Object, e As EventArgs)
        ''1''
        lblTOTDNBank.Text = Val(txtBankDNRP.Text) + Val(txtBankDNMA.Text)
        lblTOTDNKeu.Text = Val(txtKeuDNRP.Text) + Val(txtKeuDNMA.Text)
        lblTOTDNLn.Text = Val(txtLnDNRP.Text) + Val(txtLnDNMA.Text)

        lblTOTDNRP.Text = Val(txtBankDNRP.Text) +
                          Val(txtKeuDNRP.Text) +
                          Val(txtLnDNRP.Text)

        lblTOTDNMA.Text = Val(txtBankDNMA.Text) +
                          Val(txtKeuDNMA.Text) +
                          Val(txtLnDNMA.Text)

        lblTOTDN.Text = Val(lblTOTDNRP.Text) + Val(lblTOTDNMA.Text)

        lblTOTLNBank.Text = Val(txtBankLNRP.Text) + Val(txtBankLNMA.Text)
        lblTOTLNKeu.Text = Val(txtKeuLNRP.Text) + Val(txtKeuLNMA.Text)
        lblTOTLNLn.Text = Val(txtLnLNRP.Text) + Val(txtLnLNMA.Text)

        lblTOTLNRP.Text = Val(txtBankLNRP.Text) +
                          Val(txtKeuLNRP.Text) +
                          Val(txtLnLNRP.Text)

        lblTOTLNMA.Text = Val(txtBankLNMA.Text) +
                          Val(txtKeuLNMA.Text) +
                          Val(txtLnLNMA.Text)

        lblTOTLN.Text = Val(lblTOTLNRP.Text) + Val(lblTOTLNMA.Text)

        lblTOTBlmTarikRP.Text = Val(lblTOTDNRP.Text) + Val(lblTOTLNRP.Text)

        lblTOTBlmTarikMA.Text = Val(lblTOTDNMA.Text) + Val(lblTOTLNMA.Text)

        lblTOTBlmTarik.Text = Val(lblTOTBlmTarikRP.Text) + Val(lblTOTBlmTarikMA.Text)

        ''2''
        lblTOTPembNas.Text = Val(txtPembNasRP.Text) + Val(txtPembNasMA.Text)

        ''3''
        lblTOTDNPinj.Text = Val(txtPinjDNRP.Text) + Val(txtPinjDNMA.Text)
        lblTOTLNPinj.Text = Val(txtPinjLNRP.Text) + Val(txtPinjLNMA.Text)

        lblTOTPNRBTNRP.Text = Val(txtPinjDNRP.Text) + Val(txtPinjLNRP.Text)

        lblTOTPNRBTNMA.Text = Val(txtPinjDNMA.Text) + Val(txtPinjLNMA.Text)

        lblTOTPNRBTN.Text = Val(lblTOTPNRBTNRP.Text) + Val(lblTOTPNRBTNMA.Text)

        ''4''
        lblTOTChan.Text = Val(txtChanRP.Text) + Val(txtChanMA.Text)
        lblTOTJoint.Text = Val(txtJointRP.Text) + Val(txtJointMA.Text)

        lblTOTPNYLRNRP.Text = Val(txtChanRP.Text) + Val(txtJointRP.Text)

        lblTOTPNYLRNMA.Text = Val(txtChanMA.Text) + Val(txtJointMA.Text)

        lblTOTPNYLRN.Text = Val(lblTOTPNYLRNRP.Text) + Val(lblTOTPNYLRNMA.Text)

        ''5''
        lblTOTIntr.Text = Val(txtIntrRP.Text) + Val(txtIntrMA.Text)
        lblTOTCur.Text = Val(txtCurRP.Text) + Val(txtCurMA.Text)
        lblTOTCros.Text = Val(txtCrosRP.Text) + Val(txtCrosMA.Text)
        lblTOTForw.Text = Val(txtForwRP.Text) + Val(txtForwMA.Text)
        lblTOTOpt.Text = Val(txtOptRP.Text) + Val(txtOptMA.Text)
        lblTOTFutu.Text = Val(txtFutuRP.Text) + Val(txtFutuMA.Text)
        lblTOTLain.Text = Val(txtLainRP.Text) + Val(txtLainMA.Text)

        lblTOTDRVTFRP.Text = Val(txtIntrRP.Text) +
                             Val(txtCurRP.Text) +
                             Val(txtCrosRP.Text) +
                             Val(txtForwRP.Text) +
                             Val(txtOptRP.Text) +
                             Val(txtFutuRP.Text) +
                             Val(txtLainRP.Text)

        lblTOTDRVTFMA.Text = Val(txtIntrMA.Text) +
                             Val(txtCurMA.Text) +
                             Val(txtCrosMA.Text) +
                             Val(txtForwMA.Text) +
                             Val(txtOptMA.Text) +
                             Val(txtFutuMA.Text) +
                             Val(txtLainMA.Text)

        lblTOTDRVTF.Text = Val(lblTOTDRVTFRP.Text) + Val(lblTOTDRVTFMA.Text)

        ''6''
        lblTOTHpsBuku.Text = Val(txtHpsBukuRP.Text) + Val(txtHpsBukuMA.Text)
        lblTOTHpsBukuTgh.Text = Val(txtHpsBukuTghRP.Text) + Val(txtHpsBukuTghMA.Text)
        lblTOTHpsTgh.Text = Val(txtHpsTghRP.Text) + Val(txtHpsTghMA.Text)

        lblTOTLainnyaRP.Text = Val(txtHpsBukuRP.Text) +
                               Val(txtHpsBukuTghRP.Text) +
                               Val(txtHpsTghRP.Text)

        lblTOTLainnyaMA.Text = Val(txtHpsBukuMA.Text) +
                               Val(txtHpsBukuTghMA.Text) +
                               Val(txtHpsTghMA.Text)

        lblTOTLainnya.Text = Val(lblTOTLainnyaRP.Text) + Val(lblTOTLainnyaMA.Text)

        ''Rek Administratif TOTAL''

        lblTOTADMRP.Text = Val(lblTOTLainnyaRP.Text) +
                           Val(lblTOTDRVTFRP.Text) +
                           Val(lblTOTPNYLRNRP.Text) +
                           Val(lblTOTPNRBTNRP.Text) +
                           Val(txtPembNasRP.Text) +
                           Val(lblTOTBlmTarikRP.Text)

        lblTOTADMMA.Text = Val(lblTOTLainnyaMA.Text) +
                           Val(lblTOTDRVTFMA.Text) +
                           Val(lblTOTPNYLRNMA.Text) +
                           Val(lblTOTPNRBTNMA.Text) +
                           Val(txtPembNasMA.Text) +
                           Val(lblTOTBlmTarikMA.Text)

        lblTOTADM.Text = Val(lblTOTADMRP.Text) + Val(lblTOTADMMA.Text)
    End Sub

#End Region

#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_111020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP1110Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP1110
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "34_Rekening_Administratif_TB_111020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP1110) As Parameter.SIPP1110
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP1110Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP1110
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
End Class