﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
'untuk eksport
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
'
#End Region

Public Class SIPP0041
    Inherits Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New SIPP0041Controller
    Private oCustomclass As New Parameter.SIPP0041
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNaviSIPP0041 As ucGridNav


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviSIPP0041.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP0041"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                'untuk eksport
                pnlcopybulandata.Visible = False
                '
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                'untuk eksport
                FillCombo()
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
                '
            End If
        End If
    End Sub
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub
    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviSIPP0041.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.SIPP0041

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP0041List(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()


        If (isFrNav = False) Then
            GridNaviSIPP0041.Initialize(recordCount, pageSize)
        End If

        'untuk eksport
        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
        '
    End Sub
    'untuk eksport
    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP0041
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub
    '

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP0041", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False

            Me.Process = "ADD"
            clean()
        End If
    End Sub
#End Region

    '#Region "Cancel"
    '    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
    '        pnlList.Visible = True
    '        pnlAdd.Visible = False
    '        Me.SearchBy = ""
    '        Me.SortBy = ""

    '        'clean()
    '        DoBind(Me.SearchBy, Me.SortBy)
    '    End Sub
    '#End Region

    'Sub clean()
    '    lblTOTKMPembInv.Text = 0
    '    txtKMPembInvRP.Text = 0
    '    txtKMPembInvMA.Text = 0

    '    lblTOTKMMdlKrj.Text = 0
    '    txtKMMdlKrjRP.Text = 0
    '    txtKMMdlKrjMA.Text = 0

    '    lblTOTKMMulti.Text = 0
    '    txtKMMultiRP.Text = 0
    '    txtKMMultiMA.Text = 0

    '    lblTOTKMPerst.Text = 0
    '    txtKMPerstRP.Text = 0
    '    txtKMPerstMA.Text = 0

    '    lblTOTKMSyar.Text = 0
    '    txtKMSyarRP.Text = 0
    '    txtKMSyarMA.Text = 0

    '    lblTOTKMFee.Text = 0
    '    txtKMFeeRP.Text = 0
    '    txtKMFeeMA.Text = 0

    '    lblTOTKMSewa.Text = 0
    '    txtKMSewaRP.Text = 0
    '    txtKMSewaMA.Text = 0

    '    lblTOTKMChan.Text = 0
    '    txtKMChanRP.Text = 0
    '    txtKMChanMA.Text = 0

    '    lblTOTKMSurat.Text = 0
    '    txtKMSuratRP.Text = 0
    '    txtKMSuratMA.Text = 0

    '    lblTOTKMLain.Text = 0
    '    txtKMLainRP.Text = 0
    '    txtKMLainMA.Text = 0

    '    lblTOTKMKEGOPR.Text = 0
    '    lblTOTKMKEGOPRRP.Text = 0
    '    lblTOTKMKEGOPRMA.Text = 0

    '    lblTOTKKPembInv.Text = 0
    '    txtKKPembInvRP.Text = 0
    '    txtKKPembInvMA.Text = 0

    '    lblTOTKKMdlKrj.Text = 0
    '    txtKKMdlKrjRP.Text = 0
    '    txtKKMdlKrjMA.Text = 0

    '    lblTOTKKMulti.Text = 0
    '    txtKKMultiRP.Text = 0
    '    txtKKMultiMA.Text = 0

    '    lblTOTKKPerst.Text = 0
    '    txtKKPerstRP.Text = 0
    '    txtKKPerstMA.Text = 0

    '    lblTOTKKSyar.Text = 0
    '    txtKKSyarRP.Text = 0
    '    txtKKSyarMA.Text = 0

    '    lblTOTKKFee.Text = 0
    '    txtKKFeeRP.Text = 0
    '    txtKKFeeMA.Text = 0

    '    lblTOTKKSewa.Text = 0
    '    txtKKSewaRP.Text = 0
    '    txtKKSewaMA.Text = 0

    '    lblTOTKKChan.Text = 0
    '    txtKKChanRP.Text = 0
    '    txtKKChanMA.Text = 0

    '    lblTOTKKSurat.Text = 0
    '    txtKKSuratRP.Text = 0
    '    txtKKSuratMA.Text = 0

    '    lblTOTKKLain.Text = 0
    '    txtKKLainRP.Text = 0
    '    txtKKLainMA.Text = 0

    '    lblTOTKKKEGOPR.Text = 0
    '    lblTOTKKKEGOPRRP.Text = 0
    '    lblTOTKKKEGOPRMA.Text = 0

    '    lblTOTKBKEGOPR.Text = 0
    '    lblTOTKBKEGOPRRP.Text = 0
    '    lblTOTKBKEGOPRMA.Text = 0

    '    lblTOTKMPel.Text = 0
    '    txtKMPelRP.Text = 0
    '    txtKMPelMA.Text = 0

    '    lblTOTKMPenj.Text = 0
    '    txtKMPenjRP.Text = 0
    '    txtKMPenjMA.Text = 0

    '    lblTOTKMPenjSurat.Text = 0
    '    txtKMPenjSuratRP.Text = 0
    '    txtKMPenjSuratMA.Text = 0

    '    lblTOTKMDev.Text = 0
    '    txtKMDevRP.Text = 0
    '    txtKMDevMA.Text = 0

    '    lblTOTKMBung.Text = 0
    '    txtKMBungRP.Text = 0
    '    txtKMBungMA.Text = 0

    '    lblTOTKMInv.Text = 0
    '    txtKMInvRP.Text = 0
    '    txtKMInvMA.Text = 0

    '    lblKMKEGINV.Text = 0
    '    lblKMKEGINVRP.Text = 0
    '    lblKMKEGINVMA.Text = 0

    '    lblTOTKKPer.Text = 0
    '    txtKKPerRP.Text = 0
    '    txtKKPerMA.Text = 0

    '    lblTOTKKPemb.Text = 0
    '    txtKKPembRP.Text = 0
    '    txtKKPembMA.Text = 0

    '    lblTOTKKSur.Text = 0
    '    txtKKSurRP.Text = 0
    '    txtKKSurMA.Text = 0

    '    lblTOTKKInvLain.Text = 0
    '    txtKKInvLainRP.Text = 0
    '    txtKKInvLainMA.Text = 0

    '    lblKKKEGINV.Text = 0
    '    lblKKKEGINVRP.Text = 0
    '    lblKKKEGINVMA.Text = 0

    '    lblKBKEGINV.Text = 0
    '    lblKBKEGINVRP.Text = 0
    '    lblKBKEGINVMA.Text = 0

    '    lblTOTKMTer.Text = 0
    '    txtKMTerRP.Text = 0
    '    txtKMTerMA.Text = 0

    '    lblTOTKMPendLain.Text = 0
    '    txtKMPendLainRP.Text = 0
    '    txtKMPendLainMA.Text = 0

    '    lblTOTKMSaham.Text = 0
    '    txtKMSahamRP.Text = 0
    '    txtKMSahamMA.Text = 0

    '    lblTOTKMKEGPEND.Text = 0
    '    lblTOTKMKEGPENDRP.Text = 0
    '    lblTOTKMKEGPENDMA.Text = 0

    '    lblTOTKKTerbt.Text = 0
    '    txtKKTerbtRP.Text = 0
    '    txtKKTerbtMA.Text = 0

    '    lblTOTKKPendLain.Text = 0
    '    txtKKPendLainRP.Text = 0
    '    txtKKPendLainMA.Text = 0

    '    lblTOTKKSaham.Text = 0
    '    txtKKSahamRP.Text = 0
    '    txtKKSahamMA.Text = 0

    '    lblTOTKKDivd.Text = 0
    '    txtKKDivdRP.Text = 0
    '    txtKKDivdMA.Text = 0

    '    lblTOTKKKEGPEND.Text = 0
    '    lblTOTKKKEGPENDRP.Text = 0
    '    lblTOTKKKEGPENDMA.Text = 0

    '    lblTOTKBKEGPEND.Text = 0
    '    lblTOTKBKEGPENDRP.Text = 0
    '    lblTOTKBKEGPENDMA.Text = 0

    '    lblTOTSurp.Text = 0
    '    txtSurpRP.Text = 0
    '    txtSurpMA.Text = 0

    '    lblTOTPenur.Text = 0
    '    txtPenurRP.Text = 0
    '    txtPenurMA.Text = 0

    '    lblTOTPeriod.Text = 0
    '    txtPeriodRP.Text = 0
    '    txtPeriosMA.Text = 0

    '    lblTOTKas.Text = 0
    '    txtKasRP.Text = 0
    '    txtKasMA.Text = 0
    'End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP0041
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP0041", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                'untuk eksport
                txtbulandata.Enabled = False
                '
                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetSIPP0041Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If
                'untuk eksport
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                '
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN")
                'TOTAL KESELURUHAN PEGAWAI KONTRAK CABANG
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN")
                'TOTAL KESELURUHAN PEGAWAI KONTRAK CABANG
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN")
                'TOTAL KESELURUHAN PEGAWAI OUTSOURCHING CABANG
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN LAINNYA
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN SLTA
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN DIPLOMA
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN SARJANA
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN PASCA SARJANA
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN")
                'KESELURUHAN PEGAWAI TETAP SELAIN CABANG
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN")
                'KESELURUHAN PEGAWAI KONTRAK SELAIN CABANG
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN")
                'KESELURUHAN PEGAWAI OUTSOURCHING SELAIN CABANG
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN LAINNYA
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN SLTA                                             
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN DIPLOMA                                         
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN SARJANA                                           
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN")
                'PNDDKN PASCA SARJANA                                     
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN")
                'TOTAL PEGAWAI PUSAT                                          
                txtJMLH_TNG_KRJ_KNTR_PST_TTL__.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TTL__")
                txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_")
                txtJMLH_TNG_KRJ_KNTR_PST_SLT_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SLT_TTL_")
                txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_")
                txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_")
                txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_")
                'TOTAL PEGAWAI CABANG                                           
                txtJMLH_TNG_KRJ_KNTR_CBNG_TTL__.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TTL__")
                txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_")
                txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_")
                txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_")
                txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_")
                'TOTAL PEGAWAI SELAIN CABANG                                      
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_")
                txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_")
                'TOTAL TENAGA KERJA TETAP                                            
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_")
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK")
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN")
                'TOTAL TENAGA KERJA KONTRAK                                                
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_")
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK")
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN")
                'TOTAL TENAGA KERJA OUTSOURCHING                                        
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_")
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK")
                txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN")
                'TOTAL TENAGA KERJA                                                            
                txtJMLH_TNG_KRJ_TTL__.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_TTL__")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP0041", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP0041
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.SIPP0041Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.SIPP0041
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_ = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_ = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_ = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'TOTAL KESELURUHAN PEGAWAI KONTRAK CABANG
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_ = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text
                'TOTAL KESELURUHAN PEGAWAI KONTRAK CABANG
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_ = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text
                'TOTAL KESELURUHAN PEGAWAI OUTSOURCHING CABANG
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_ = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN LAINNYA
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN SLTA
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN DIPLOMA
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN SARJANA
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN PASCA SARJANA
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'KESELURUHAN PEGAWAI TETAP SELAIN CABANG
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text
                'KESELURUHAN PEGAWAI KONTRAK SELAIN CABANG
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text
                'KESELURUHAN PEGAWAI OUTSOURCHING SELAIN CABANG
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN LAINNYA
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN SLTA                                             
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN DIPLOMA                                         
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN SARJANA                                           
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN PASCA SARJANA                                     
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'TOTAL PEGAWAI PUSAT                                          
                .JMLH_TNG_KRJ_KNTR_PST_TTL__ = txtJMLH_TNG_KRJ_KNTR_PST_TTL__.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_.Text
                'TOTAL PEGAWAI CABANG                                           
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL__ = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL__.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_.Text
                'TOTAL PEGAWAI SELAIN CABANG                                      
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_.Text
                'TOTAL TENAGA KERJA TETAP                                            
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_ = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN.Text
                'TOTAL TENAGA KERJA KONTRAK                                                
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_ = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN.Text
                'TOTAL TENAGA KERJA OUTSOURCHING                                        
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_ = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN.Text
                'TOTAL TENAGA KERJA                                                            
                .JMLH_TNG_KRJ_TTL__ = txtJMLH_TNG_KRJ_TTL__.Text
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP0041Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter
                .ID = txtid.Text
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_ = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_ = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_ = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'TOTAL KESELURUHAN PEGAWAI KONTRAK CABANG
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_ = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text
                'TOTAL KESELURUHAN PEGAWAI KONTRAK CABANG
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_ = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text
                'TOTAL KESELURUHAN PEGAWAI OUTSOURCHING CABANG
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_ = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN LAINNYA
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN SLTA
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN DIPLOMA
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN SARJANA
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN PASCA SARJANA
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'KESELURUHAN PEGAWAI TETAP SELAIN CABANG
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text
                'KESELURUHAN PEGAWAI KONTRAK SELAIN CABANG
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text
                'KESELURUHAN PEGAWAI OUTSOURCHING SELAIN CABANG
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN LAINNYA
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN SLTA                                             
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN DIPLOMA                                         
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN SARJANA                                           
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'PNDDKN PASCA SARJANA                                     
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text
                'TOTAL PEGAWAI PUSAT                                          
                .JMLH_TNG_KRJ_KNTR_PST_TTL__ = txtJMLH_TNG_KRJ_KNTR_PST_TTL__.Text
                .JMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_PST_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_.Text
                'TOTAL PEGAWAI CABANG                                           
                .JMLH_TNG_KRJ_KNTR_CBNG_TTL__ = txtJMLH_TNG_KRJ_KNTR_CBNG_TTL__.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_.Text
                'TOTAL PEGAWAI SELAIN CABANG                                      
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_ = txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_.Text
                'TOTAL TENAGA KERJA TETAP                                            
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_ = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN.Text
                'TOTAL TENAGA KERJA KONTRAK                                                
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_ = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN.Text
                'TOTAL TENAGA KERJA OUTSOURCHING                                        
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_ = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK.Text
                .JMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN = txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN.Text
                'TOTAL TENAGA KERJA                                                            
                .JMLH_TNG_KRJ_TTL__ = txtJMLH_TNG_KRJ_TTL__.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP0041AddEdit(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub

    'untuk eksport
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_004120300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0041Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP0041
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "30_Rincian_Tenaga_Kerja_Berdasarkan_Tingkat_Pendidikan_TB_004120300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP0041) As Parameter.SIPP0041
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0041Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP0041
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    '

    Protected Sub txtSUM(sender As Object, e As EventArgs)

        'Kantor Pusat Tetap
        '1
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text)
        '2
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN.Text)
        '3
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN.Text)
        '4
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '5
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '6
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text)
        '7
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '8
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN.Text)

        'Kantor Pusat Kontrak
        '9
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text)
        '10
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN.Text)
        '11
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN.Text)
        '12
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '13
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '14
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text)
        '15
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '16
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN.Text)

        'Kantor Pusat Outsourcing
        '17
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text)
        '10
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN.Text)
        '11
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text)
        '12
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '13
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '14
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text)
        '15
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '16
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN.Text)

        'Kantor Cabang Tetap
        '1
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text)
        '2
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text)
        '3
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text)
        '4
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '5
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '6
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text)
        '7
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '8
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text)

        'Kantor Cabang Kontrak
        '9
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text)
        '10
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text)
        '11
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text)
        '12
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '13
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '14
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text)
        '15
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '16
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text)

        'Kantor Cabang Outsourcing
        '17
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text)
        '10
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text)
        '11
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text)
        '12
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '13
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '14
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text)
        '15
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '16
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text)

        'Kantor Selain Cabang Tetap
        '1
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text)
        '2
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text)
        '3
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text)
        '4
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '5
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '6
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text)
        '7
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text)
        '8
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text)

        'Kantor Selain Cabang Kontrak
        '9
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text)
        '10
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text)
        '11
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text)
        '12
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '13
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '14
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text)
        '15
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text)
        '16
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text)

        'Kantor Selain Cabang Outsourcing
        '17
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text)
        '10
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text)
        '11
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text)
        '12
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '13
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '14
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text)
        '15
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text)
        '16
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text)

        'Jumlah Tenaga Kerja Kantor Pusat
        txtJMLH_TNG_KRJ_KNTR_PST_TTL__.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text)
        'Kantor Pusat Pendidikan Lainnya
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text)
        'Kantor Pusat Pendidikan SLTA
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text)
        'Kantor Pusat Pendidikan Diploma
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL.Text)
        'Kantor Pusat Pendidikan Sarjana
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL.Text)
        'Kantor Pusat Pendidikan Pasca Sarjana
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Cabang Total  
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL__.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text)
        'Jumlah Tenaga Kerja Kantor Cabang Tingkat Pendidikan Lainnya di Bawah SLTA Total 
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Cabang SLTA Total 
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Cabang Diploma Total 
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Cabang Sarjana Total 
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Cabang Pasca Sarjana Total 
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Selain Kantor Cabang Total  
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text)
        'Jumlah Tenaga Kerja Kantor Selain Kantor Cabang Tingkat Pendidikan Lainnya di Bawah SLTA Total 
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Selain Kantor Cabang SLTA Total 
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Selain Kantor Cabang Diploma Total 
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Selain Kantor Cabang Sarjana Total 
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Kantor Selain Kantor Cabang Pasca Sarjana Total 
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_.Text = Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Tetap 
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Tetap Laki-Laki
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Tetap Perempuan
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Kontrak 
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Kontrak Laki-Laki
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Kontrak Perempuan
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Outsourcing 
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Outsourcing Laki-Laki
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text)
        'Jumlah Tenaga Kerja Total Tenaga Kerja Outsourcing Perempuan
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = Val(txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text) + Val(txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text)
        'Jumlah Tenaga Kerja Total  
        txtJMLH_TNG_KRJ_TTL__.Text = Val(txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_.Text) + Val(txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_.Text) + Val(txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_.Text)
    End Sub
#End Region

    Sub clean()
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TTL__.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SLT_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TTL__.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK.Text = "0"
        txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN.Text = "0"
        txtJMLH_TNG_KRJ_TTL__.Text = "0"
        txtbulandata.Text = "0"
    End Sub
End Class
