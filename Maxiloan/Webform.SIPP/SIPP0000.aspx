﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP0000.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP0000" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Informasi Profil Perusahaan Pembiayaan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        } 
    </script>   
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SIPP0000 - INFORMASI PROFIL PERUSAHAAN PEMBIAYAAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="2%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn  HeaderText="DELETE">
                                        <ItemStyle  Width="7%" HorizontalAlign="Center" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="15%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NM_LNGKP_PRSHN" SortExpression="NM_LNGKP_PRSHN" HeaderText="NAMA PERUSAHAAN PEMBIAYAAN" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NPWP" SortExpression="NPWP" HeaderText="NPWP" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="LMT_LNGKP" SortExpression="LMT_LNGKP" HeaderText="ALAMAT" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNavi2" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                </div>

                <%--<div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI INFORMASI PROFIL PERUSAHAAN PEMBIAYAAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NM_LNGKP_PRSHN">Nama Perusahaan</asp:ListItem>
                            <asp:ListItem Value="NPWP">NPWP</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>--%>

               <%-- <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            EXPORT TO EXCEL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>
                            <asp:TextBox ID="txtGetBulanData" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtGetBulanData" Format="MMyyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                    </div>              
                </div>--%>
               
            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
            
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Lengkap Perusahaan
                        </label>
                        <asp:TextBox ID="txtnmlngkp" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnmlngkp" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Sebutan Perusahaan
                        </label>
                        <asp:TextBox ID="txtnmsbtn" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnmsbtn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            NPWP
                        </label>
                        <asp:TextBox ID="txtnpwp" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnpwp" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Status Kepemilikan Perusahaan
                        </label>
                        <%--<asp:TextBox ID="txtstatus" runat="server" CssClass="medium_text"></asp:TextBox>--%>
                        <asp:DropDownList ID="cboStatus" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="cboStatus" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Bentuk Badan Usaha
                        </label>
                        <%--<asp:TextBox ID="txtbntkbdnusaha" runat="server" CssClass="medium_text" ></asp:TextBox>--%>
                        <asp:DropDownList ID="cbobntkbdnusaha" runat="server" Width ="20%" Columns ="105" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="cbobntkbdnusaha" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kegiatan Syariah
                        </label>
                        <%--<asp:TextBox ID="txtkegsyariah" runat="server" CssClass="medium_text" ></asp:TextBox>--%>
                        <asp:DropDownList ID="cbokegsyariah" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="cbokegsyariah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Pendirian
                        </label>
                        <asp:TextBox ID="txttglpendirian" runat="server" CssClass="medium_text" Width="7%" ></asp:TextBox>
                         <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True" TargetControlID="txttglpendirian" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                            ControlToValidate="txttglpendirian" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div> 
                <div class="form_box">
                      <div class="form_single">
                          <label class="label_general">
                                Pembiayaan Investasi
                          </label>
                          <asp:Label ID="lblinvest" runat="server" Text=""></asp:Label>                                                   
                          <asp:CheckBox ID="cboinvest" runat="server"></asp:CheckBox>                
                      </div>
                 </div>
                <div class="form_box">
                      <div class="form_single">
                          <label class="label_general">
                                Pembiayaan Modal Kerja
                          </label>  
                           <asp:Label ID="lblmodal" runat="server" Text=""></asp:Label>                                                 
                          <asp:CheckBox ID="cbomodal" runat="server"></asp:CheckBox>                
                      </div>
                 </div>
                 <div class="form_box">
                      <div class="form_single">
                          <label class="label_general">
                                Pembiayaan Multi Guna
                          </label>
                           <asp:Label ID="lblmulti" runat="server" Text=""></asp:Label>                                                   
                          <asp:CheckBox ID="cbomulti" runat="server"></asp:CheckBox> 
                      </div>
                 </div>
                <div class="form_box">
                      <div class="form_single">
                          <label class="label_general">
                                Pembiayaan Lainnya
                          </label>               
                           <asp:Label ID="lbllainnya" runat="server" Text=""></asp:Label>                                    
                          <asp:CheckBox ID="cbolainnya" runat="server"></asp:CheckBox>                
                      </div>
                 </div>
                <div class="form_box">
                      <div class="form_single">
                          <label class="label_general">
                                Pembiayaan Berdasarkan Prinsip Syariah
                          </label>           
                           <asp:Label ID="lblsyariah" runat="server" Text=""></asp:Label>                                        
                          <asp:CheckBox ID="cbosyariah" runat="server"></asp:CheckBox>                
                      </div>
                 </div>
                <div class="form_box">
                      <div class="form_single">
                          <label class="label_general">
                                Pembiayaan Jual Beli
                          </label>         
                          <asp:Label ID="lbljualbeli" runat="server" Text=""></asp:Label>                                           
                          <asp:CheckBox ID="cbojualbeli" runat="server"></asp:CheckBox>                
                      </div>
                 </div>
                <div class="form_box">
                      <div class="form_single">
                          <label class="label_general">
                                Pembiayaan Jasa
                          </label>            
                          <asp:Label ID="lbljasa" runat="server" Text=""></asp:Label>                                        
                          <asp:CheckBox ID="cbojasa" runat="server"></asp:CheckBox>                
                      </div>
                 </div>

                 <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Alamat
                        </label>
                        <asp:TextBox ID="txtalmt" runat="server" CssClass="medium_text" width="40%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ControlToValidate="txtalmt" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Lokasi Dati II
                        </label>
                        <%--<asp:TextBox ID="txtdati" runat="server" CssClass="medium_text" ></asp:TextBox>--%>
                         <asp:DropDownList ID="cbodati" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                            ControlToValidate="cbodati" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kode Pos
                        </label>
                        <asp:TextBox ID="txtkdpos" runat="server" CssClass="medium_text" Width ="5%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ControlToValidate="txtkdpos" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Status Kepemilikan Gedung
                        </label>
                        <%--<asp:TextBox ID="txtsttskepemlkn" runat="server" CssClass="medium_text" ></asp:TextBox>--%>
                        <asp:DropDownList ID="cbosttskepemlkngdg" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                            ControlToValidate="cbosttskepemlkngdg" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Telepon
                        </label>
                        <asp:TextBox ID="txtnotelp" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Format No HP Salah!" ValidationExpression="[0-9]{0,15}"
                            ControlToValidate="txtnotelp" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Faksimili
                        </label>
                        <asp:TextBox ID="txtfax" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Format No Fax Salah!" ValidationExpression="[0-9]{0,15}"
                            ControlToValidate="txtfax" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jumlah Kantor Cabang
                        </label>
                        <asp:TextBox ID="txtjmlkntrcbg" runat="server" CssClass="medium_text" width="5%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                            ControlToValidate="txtjmlkntrcbg" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jumlah Kantor Selain Kantor Cabang
                        </label>
                        <asp:TextBox ID="txtjmlkntrslncbg" runat="server" CssClass="medium_text" width="5%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                            ControlToValidate="txtjmlkntrslncbg" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kantor Pusat
                        </label>
                        <asp:TextBox ID="txtjmltngkrjpst" runat="server" CssClass="medium_text" width="5%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                            ControlToValidate="txtjmltngkrjpst" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kantor Cabang
                        </label>
                        <asp:TextBox ID="txtjmltngkrjcbg" runat="server" CssClass="medium_text" width="5%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                            ControlToValidate="txtjmltngkrjcbg" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Selain Kantor Cabang
                        </label>
                        <asp:TextBox ID="txtjmltngkrjslncbg" runat="server" CssClass="medium_text" width="5%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                            ControlToValidate="txtjmltngkrjslncbg" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Lengkap Petugas Penyusun Laporan
                        </label>
                        <asp:TextBox ID="txtnmptgslap" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnmptgslap" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Bagian/Divisi
                        </label>
                        <asp:TextBox ID="txtbagian" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                            ControlToValidate="txtbagian" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No Telepon Petugas Penyusun Laporan
                        </label>
                        <asp:TextBox ID="txttelpptgslap" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                            ControlToValidate="txttelpptgslap" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No Faksimili Petugas Penyusun Laporan
                        </label>
                        <asp:TextBox ID="txtfaxptgslap" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                            ControlToValidate="txtfaxptgslap" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Lengkap Anggota Direksi Penanggung Jawab Laporan
                        </label>
                        <asp:TextBox ID="txtnmdirlap" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnmdirlap" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jabatan Anggota Direksi
                        </label>
                        <asp:TextBox ID="txtjabatan" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                            ControlToValidate="txtjabatan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No Telepon Anggota Direksi
                        </label>
                        <asp:TextBox ID="txtteldirlap" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="*"
                            ControlToValidate="txtteldirlap" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            No Faksimili Anggota Direksi
                        </label>
                        <asp:TextBox ID="txtfaxdirlap" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="*"
                            ControlToValidate="txtfaxdirlap" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>

         
    </form>
</body>
</html>