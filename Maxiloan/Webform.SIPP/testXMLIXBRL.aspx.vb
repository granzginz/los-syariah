﻿Imports System.Xml
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Web.Security
Imports System.IO
Imports System.Data
Imports System.Configuration
Imports Maxiloan.General.CommonCookiesHelper


Public Class testXMLlXBRL
    Inherits Webform.WebBased
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            ' When the page loads we get the user id from a session variable and saves this in a hidden field
            Me.FormID = "XBRL"


        End If
    End Sub

    Public Function ReplaceSymbols(ByVal Symbols As String) As String
        Symbols = Replace(Symbols, ":", "")
        Symbols = Replace(Symbols, " ", "")
        Symbols = Replace(Symbols, ",", "")
        Symbols = Replace(Symbols, "-", "")
        Return Symbols
    End Function

    'Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
    '    If txtSearch.Text <> "" Then
    '        Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
    '    Else
    '        Me.SearchBy = ""
    '    End If
    '    Me.SortBy = ""
    '    DoBind(Me.SearchBy, Me.SortBy)
    'End Sub

    'Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
    '    'Create the url for the xml-file to be saved in the database, we use a function in Tools.vb to replace
    '    ' symbols. Look in Tools.vb to see the code.

    '    Dim xbrlurl As String
    '    xbrlurl = "xbrl/" & 251230 & ReplaceSymbols(DateTime.Now.ToString()) & "001020300" & ".xml"

    '    ' Save the url for the XML-file to the XML-files table in the database so the user can see and download the XML-files 
    '    ' that the user have saved on our server.

    '    Try
    '        Dim ConnString As String = ConfigurationManager.ConnectionStrings("ConnectionString").ToString()
    '        Dim sql As String = "INSERT INTO XML-files (URL, Company, StartDate, EndDate, UserID, Date) VALUES (@URL, @Company, @StartDate, @EndDate, @UserID, @Date)"

    '        ' The Using block is used to call dispose (close) automatically even if there are an exception.
    '        Using cn As New SqlConnection(ConnString),
    '              cmd As New SqlCommand(sql, cn)
    '            cmd.Parameters.Add("@URL", Data.SqlDbType.VarChar, 80).Value = xbrlurl
    '            cmd.Parameters.Add("@Company", Data.SqlDbType.VarChar, 50).Value = txtFirma.Text
    '            cmd.Parameters.Add("@StartDate", Data.SqlDbType.SmallDateTime).Value = 2018 - 3 - 1
    '            cmd.Parameters.Add("@EndDate", Data.SqlDbType.SmallDateTime).Value = 2018 - 3 - 31
    '            cmd.Parameters.Add("@UserID", Data.SqlDbType.Int).Value = HiddenUserID.Value
    '            cmd.Parameters.Add("@Date", Data.SqlDbType.SmallDateTime).Value = DateTime.Now.ToString()

    '            cn.Open()
    '            cmd.ExecuteNonQuery()

    '        End Using
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try

    '    ' Create an XML-document with the right encoding and saves this xml-file to our 
    '    ' server according to the "xbrlurl" above.

    '    Dim textWriter As New XmlTextWriter(Server.MapPath(xbrlurl), New UTF8Encoding(False))
    '    textWriter.Formatting = System.Xml.Formatting.Indented

    '    ' IF WE DONT WANT TO SAVE THE XML-FILE ON THE SERVER WE COULD USE THIS CODE INSTEAD SO THAT
    '    ' THE USER COULD DOWNLOAD THE FILE TO HIS LOCAL COMPUTER
    '    'Dim textWriter As New XmlTextWriter(Response.OutputStream, New UTF8Encoding(False))
    '    'textWriter.Formatting = System.Xml.Formatting.Indented

    '    'Start New Document
    '    textWriter.WriteStartDocument()

    '    'Insert XBRL start row, Start Element with attributes
    '    textWriter.WriteStartElement("xbrli:xbrl xmlns:perk", "http://xbrl.ojk.go.id/taxonomy/dict/2016-06-11/perk")
    '    textWriter.WriteAttributeString("xmlns:F001020300", "http://xbrl.ojk.go.id/taxonomy/F001020300")
    '    textWriter.WriteAttributeString("xmlns:ST", "http://xbrl.ojk.go.id/taxonomy/dict/2016-06-11/dom/ST")
    '    textWriter.WriteAttributeString("xmlns:dim", "http://xbrl.ojk.go.id/taxonomy/dict/2016-06-11/dim")
    '    textWriter.WriteAttributeString("xmlns:xmlns:met", "http://xbrl.ojk.go.id/taxonomy/dict/2016-06-11/met")
    '    textWriter.WriteAttributeString("xmlnsxsi", "http://www.w3.org/2001/XMLSchema-instance")
    '    textWriter.WriteAttributeString("xmlns:xlink", "http://www.w3.org/1999/xlink")
    '    textWriter.WriteAttributeString("xmlns:xl", "http://www.xbrl.org/2003/XLink")
    '    textWriter.WriteAttributeString("xmlns:link", "http://www.xbrl.org/2003/linkbase")
    '    textWriter.WriteAttributeString("xmlns:nonnum", "http://www.xbrl.org/dtr/type/non-numeric")
    '    textWriter.WriteAttributeString("xmlns:num", "http://www.xbrl.org/dtr/type/numeric")
    '    textWriter.WriteAttributeString("xmlns:xbrldt", "http://xbrl.org/2005/xbrldt")
    '    textWriter.WriteAttributeString("xmlns:xbrli", "http://www.xbrl.org/2003/instance")
    '    textWriter.WriteAttributeString("xmlns:xbrldi", "http://xbrl.org/2006/xbrldi")
    '    textWriter.WriteAttributeString("xmlns:enum", "http://xbrl.org/2014/extensible-enumerations")
    '    textWriter.WriteAttributeString("xmlns:ref", "http://www.xbrl.org/2006/ref")
    '    textWriter.WriteAttributeString("xmlns:iso4217", "http://www.xbrl.org/2003/iso4217")

    '    'Insert XBRL link scheme
    '    If BruttoresultatRES0.Text = "" Or BruttoresultatRES0.Text Is Nothing Then
    '        textWriter.WriteElementString("link:schemaRef xlink:type=""simple"" xlink:href=""http://xbrl.ojk.go.id/taxonomy/view/pp/2016-06-11/bulanan/gabungan/001020300/001020300-2016-06-11.xsd""", "")
    '    Else
    '        textWriter.WriteElementString("link:schemaRef xlink:type=""simple"" xlink:href=""http://xbrl.ojk.go.id/taxonomy/view/pp/2016-06-11/bulanan/gabungan/001020300/001020300-2016-06-11.xsd""", "")
    '    End If

    '    'Insert Start Element for PERIOD0 with attributeString
    '    'xbrli untuk unit id
    '    textWriter.WriteStartElement("xbrli:unit", "")
    '    textWriter.WriteAttributeString("id", "I")
    '    textWriter.WriteStartElement("xbrli:Measure")
    '    textWriter.WriteAttributeString("id", "PERIOD0")


    '    'Insert entity inside PERIOD0 xbrli:context
    '    'xbrli untuk context id
    '    textWriter.WriteStartElement("xbrli:entity")
    '    textWriter.WriteStartElement("xbrli:identifier", "")
    '    textWriter.WriteAttributeString("scheme", "http://xbrl.ojk.go.id")
    '    textWriter.WriteString(251230)
    '    textWriter.WriteEndElement()
    '    textWriter.WriteEndElement()

    '    'Insert period inside PERIOD0 xbrli:context
    '    'xbrli untuk period(startdate dan enddate)
    '    textWriter.WriteStartElement("xbrli:period")
    '    textWriter.WriteStartElement("xbrli:startDate", "")
    '    textWriter.WriteString(2018 - 3 - 1)
    '    textWriter.WriteEndElement()
    '    textWriter.WriteStartElement("xbrli:endDate", "")
    '    textWriter.WriteString(2018 - 3 - 31)
    '    textWriter.WriteEndElement()
    '    textWriter.WriteEndElement()

    '    'Insert End Element for PERIOD0
    '    textWriter.WriteEndElement()

    '    'End Everything
    '    textWriter.WriteEndDocument()

    '    'Clean up
    '    textWriter.Flush()
    '    textWriter.Close()

    '    ' IF WE WANT THE USER TO BE ABLE TO DOWNLOAD THE XML-FILE directly (uncomment the code below)
    '    'Response.ContentType = "Application/xbrl"
    '    'Response.AppendHeader("Content-Disposition", "attachment; filename=Report.xbrl")
    '    'Response.Flush()
    '    'Response.Close()

    '    ' Redirect the user to a new webpage
    '    Server.Transfer("CreateXMLlXBRL.aspx")
    'End Sub



    'Protected Sub Write_XML(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles Wizard1.FinishButtonClick

    '    'Create the url for the xml-file to be saved in the database, we use a function in Tools.vb to replace
    '    ' symbols. Look in Tools.vb to see the code.

    '    Dim xbrlurl As String
    '    xbrlurl = "xbrl/" & 251230 & ReplaceSymbols(DateTime.Now.ToString()) & "001020300" & ".xml"

    '    ' Save the url for the XML-file to the XML-files table in the database so the user can see and download the XML-files 
    '    ' that the user have saved on our server.

    '    Try
    '        Dim ConnString As String = ConfigurationManager.ConnectionStrings("ConnectionString").ToString()
    '        Dim sql As String = "INSERT INTO XML-files (URL, Company, StartDate, EndDate, UserID, Date) VALUES (@URL, @Company, @StartDate, @EndDate, @UserID, @Date)"

    '        ' The Using block is used to call dispose (close) automatically even if there are an exception.
    '        Using cn As New SqlConnection(ConnString),
    '              cmd As New SqlCommand(sql, cn)
    '            cmd.Parameters.Add("@URL", Data.SqlDbType.VarChar, 80).Value = xbrlurl
    '            cmd.Parameters.Add("@Company", Data.SqlDbType.VarChar, 50).Value = txtFirma.Text
    '            cmd.Parameters.Add("@StartDate", Data.SqlDbType.SmallDateTime).Value = 2018 - 3 - 1
    '            cmd.Parameters.Add("@EndDate", Data.SqlDbType.SmallDateTime).Value = 2018 - 3 - 31
    '            cmd.Parameters.Add("@UserID", Data.SqlDbType.Int).Value = HiddenUserID.Value
    '            cmd.Parameters.Add("@Date", Data.SqlDbType.SmallDateTime).Value = DateTime.Now.ToString()

    '            cn.Open()
    '            cmd.ExecuteNonQuery()

    '        End Using
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try

    '    ' Create an XML-document with the right encoding and saves this xml-file to our 
    '    ' server according to the "xbrlurl" above.

    '    Dim textWriter As New XmlTextWriter(Server.MapPath(xbrlurl), New UTF8Encoding(False))
    '    textWriter.Formatting = System.Xml.Formatting.Indented

    '    ' IF WE DONT WANT TO SAVE THE XML-FILE ON THE SERVER WE COULD USE THIS CODE INSTEAD SO THAT
    '    ' THE USER COULD DOWNLOAD THE FILE TO HIS LOCAL COMPUTER
    '    'Dim textWriter As New XmlTextWriter(Response.OutputStream, New UTF8Encoding(False))
    '    'textWriter.Formatting = System.Xml.Formatting.Indented

    '    'Start New Document
    '    textWriter.WriteStartDocument()

    '    'Insert XBRL start row, Start Element with attributes
    '    textWriter.WriteStartElement("xbrli:xbrl xmlns:perk", "http://xbrl.ojk.go.id/taxonomy/dict/2016-06-11/perk")
    '    textWriter.WriteAttributeString("xmlns:F001020300", "http://xbrl.ojk.go.id/taxonomy/F001020300")
    '    textWriter.WriteAttributeString("xmlns:ST", "http://xbrl.ojk.go.id/taxonomy/dict/2016-06-11/dom/ST")
    '    textWriter.WriteAttributeString("xmlns:dim", "http://xbrl.ojk.go.id/taxonomy/dict/2016-06-11/dim")
    '    textWriter.WriteAttributeString("xmlns:xmlns:met", "http://xbrl.ojk.go.id/taxonomy/dict/2016-06-11/met")
    '    textWriter.WriteAttributeString("xmlnsxsi", "http://www.w3.org/2001/XMLSchema-instance")
    '    textWriter.WriteAttributeString("xmlns:xlink", "http://www.w3.org/1999/xlink")
    '    textWriter.WriteAttributeString("xmlns:xl", "http://www.xbrl.org/2003/XLink")
    '    textWriter.WriteAttributeString("xmlns:link", "http://www.xbrl.org/2003/linkbase")
    '    textWriter.WriteAttributeString("xmlns:nonnum", "http://www.xbrl.org/dtr/type/non-numeric")
    '    textWriter.WriteAttributeString("xmlns:num", "http://www.xbrl.org/dtr/type/numeric")
    '    textWriter.WriteAttributeString("xmlns:xbrldt", "http://xbrl.org/2005/xbrldt")
    '    textWriter.WriteAttributeString("xmlns:xbrli", "http://www.xbrl.org/2003/instance")
    '    textWriter.WriteAttributeString("xmlns:xbrldi", "http://xbrl.org/2006/xbrldi")
    '    textWriter.WriteAttributeString("xmlns:enum", "http://xbrl.org/2014/extensible-enumerations")
    '    textWriter.WriteAttributeString("xmlns:ref", "http://www.xbrl.org/2006/ref")
    '    textWriter.WriteAttributeString("xmlns:iso4217", "http://www.xbrl.org/2003/iso4217")

    '    'Insert XBRL link scheme
    '    If BruttoresultatRES0.Text = "" Or BruttoresultatRES0.Text Is Nothing Then
    '        textWriter.WriteElementString("link:schemaRef xlink:type=""simple"" xlink:href=""http://xbrl.ojk.go.id/taxonomy/view/pp/2016-06-11/bulanan/gabungan/001020300/001020300-2016-06-11.xsd""", "")
    '    Else
    '        textWriter.WriteElementString("link:schemaRef xlink:type=""simple"" xlink:href=""http://xbrl.ojk.go.id/taxonomy/view/pp/2016-06-11/bulanan/gabungan/001020300/001020300-2016-06-11.xsd""", "")
    '    End If

    '    'Insert Start Element for PERIOD0 with attributeString
    '    'xbrli untuk unit id
    '    textWriter.WriteStartElement("xbrli:unit", "")
    '    textWriter.WriteAttributeString("id", "I")
    '    textWriter.WriteStartElement("xbrli:Measure")
    '    textWriter.WriteAttributeString("id", "PERIOD0")


    '    'Insert entity inside PERIOD0 xbrli:context
    '    'xbrli untuk context id
    '    textWriter.WriteStartElement("xbrli:entity")
    '    textWriter.WriteStartElement("xbrli:identifier", "")
    '    textWriter.WriteAttributeString("scheme", "http://xbrl.ojk.go.id")
    '    textWriter.WriteString(251230)
    '    textWriter.WriteEndElement()
    '    textWriter.WriteEndElement()

    '    'Insert period inside PERIOD0 xbrli:context
    '    'xbrli untuk period(startdate dan enddate)
    '    textWriter.WriteStartElement("xbrli:period")
    '    textWriter.WriteStartElement("xbrli:startDate", "")
    '    textWriter.WriteString(2018 - 3 - 1)
    '    textWriter.WriteEndElement()
    '    textWriter.WriteStartElement("xbrli:endDate", "")
    '    textWriter.WriteString(2018 - 3 - 31)
    '    textWriter.WriteEndElement()
    '    textWriter.WriteEndElement()

    '    'Insert End Element for PERIOD0
    '    textWriter.WriteEndElement()

    '    'End Everything
    '    textWriter.WriteEndDocument()

    '    'Clean up
    '    textWriter.Flush()
    '    textWriter.Close()

    '    ' IF WE WANT THE USER TO BE ABLE TO DOWNLOAD THE XML-FILE directly (uncomment the code below)
    '    'Response.ContentType = "Application/xbrl"
    '    'Response.AppendHeader("Content-Disposition", "attachment; filename=Report.xbrl")
    '    'Response.Flush()
    '    'Response.Close()

    '    ' Redirect the user to a new webpage
    '    Server.Transfer("CreateXMLlXBRL.aspx")
    'End Sub
    'Protected Sub txtBeloppsformat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBeloppsformat.SelectedIndexChanged
    '    If txtBeloppsformat.SelectedValue = "NORMALFORM" Then
    '        txtDecimals.Text = "INF"
    '    Else
    '        txtDecimals.Text = "-3"
    '    End If
    'End Sub

End Class