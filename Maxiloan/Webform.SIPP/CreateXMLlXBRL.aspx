﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CreateXMLlXBRL.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.CreateXMLlXBRL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Create XML-file or XBRL-file</title>
</head>
<body>
<form id="form1" runat="server">
        <h1>Create XML-file or XBRL-file</h1>
        This example shows how you can create an XML-file or an XBRL-file from data in a form. The XML-file or XBRL-file can be saved on
        your webserver or downloaded directly by the user. When the button "Create XBRL-file" in the last step of the wizard is clicked
        there is code to create the XBRL-file.<br />
    <br />
    <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" BackColor="#F7F6F3" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" FinishCompleteButtonText="Skapa XBRL-fil" >
            <WizardSteps>
                <asp:WizardStep ID="GeneralInformation" runat="server" StepType="Start"  Title="General information">
                   <table style="width: 704px">
        <tr class="headlineRow">
            <td colspan="2" class="headlineColumn">
                General information</td>
        </tr>
        <tr>
            <td style="width: 400px">
                Firma:</td>
            <td style="width: 304px">
                <asp:TextBox ID="txtFirma" runat="server" Width="94%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="CompanyRequired" runat="server" ControlToValidate="txtFirma"
                    Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateReport" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 400px">
                Organisationsnummer:
            </td>
            <td style="width: 304px">
                <asp:TextBox ID="txtOrganisationsnummer" runat="server" Width="94%"></asp:TextBox>
                <asp:RequiredFieldValidator ID="OrgNumberRequired" runat="server" ControlToValidate="txtOrganisationsnummer"
                    Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateReport" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 400px">
                Beloppsformat:</td>
            <td style="width: 304px">
                <asp:DropDownList ID="txtBeloppsformat" runat="server" Width="95%" AutoPostBack="True">
                    <asp:ListItem Selected="True" Value="NORMALFORM">Heltal</asp:ListItem>
                    <asp:ListItem Value="TUSENTAL">Tusental</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td style="width: 400px">
                Redovisningsvaluta:</td>
            <td style="width: 304px">
                <asp:DropDownList ID="txtRedovisningsvaluta" runat="server" Width="95%">
                    <asp:ListItem Selected="True">SEK</asp:ListItem>
                    <asp:ListItem>EUR</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
       <tr>
           <td style="width: 400px">
               Decimaler:</td>
           <td style="width: 304px">
        <asp:TextBox ID="txtDecimals" runat="server" Text="INF" Width="94%" ReadOnly="True"></asp:TextBox>
           </td>
       </tr>
    </table>
    <br />
    <table style="width: 704px">
        <tr class="headlineRow">
            <td style="width: 300px" class="headlineColumn">
                Accounting year</td>
            <td style="width: 202px" class="headlineColumn">
                Startdatum</td>
            <td style="width: 202px" class="headlineColumn">
                Slutdatum</td>
        </tr>
        <tr>
            <td style="width: 300px">
                Innevarande räkenskapsår (yyyy-mm-dd):</td>
            <td style="width: 202px">
                <asp:TextBox ID="txtRakenskapsarStartDatePeriod0" runat="server" Width="94%" ></asp:TextBox>
                <asp:CompareValidator ID="DateCheckPer0" runat="server" ControlToValidate="txtRakenskapsarStartDatePeriod0"
                    Display="Dynamic" ErrorMessage="*" Operator="DataTypeCheck" Type="Date" ValueToCompare="yyyy-MM-dd" EnableTheming="True" ValidationGroup="CreateReport" SetFocusOnError="True"></asp:CompareValidator>
                <asp:RequiredFieldValidator ID="Period0Required" runat="server" ControlToValidate="txtRakenskapsarStartDatePeriod0"
                    Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateReport" SetFocusOnError="True"></asp:RequiredFieldValidator></td>

            <td style="width: 202px">
                <asp:TextBox ID="txtRakenskapsarEndDatePeriod0" runat="server" Width="94%"></asp:TextBox>
                <asp:CompareValidator ID="DateCheckPerm1" runat="server" ControlToValidate="txtRakenskapsarEndDatePeriod0"
                    Display="Dynamic" EnableTheming="True" ErrorMessage="*" Operator="DataTypeCheck"
                    Type="Date" ValidationGroup="CreateReport" ValueToCompare="yyyy-MM-dd" SetFocusOnError="True"></asp:CompareValidator>
                <asp:RequiredFieldValidator ID="Periodm1Required" runat="server" ControlToValidate="txtRakenskapsarEndDatePeriod0"
                    Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateReport" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 300px">
                Jämförelseår (yyyy-mm-dd):</td>
            <td style="width: 202px">
                <asp:TextBox ID="txtRakenskapsarStartDatePeriodm1" runat="server" Width="94%"></asp:TextBox></td>
            <td style="width: 202px">
                <asp:TextBox ID="txtRakenskapsarEndDatePeriodm1" runat="server" Width="94%"></asp:TextBox></td>
        </tr>
    </table> 
    <asp:HiddenField ID="HiddenUserID" runat="server" />
    </asp:WizardStep>
                <asp:WizardStep ID="BusinessStory" runat="server" StepType="Step" Title="Business story">
                <table style="width: 704px">
        <tr class="headlineRow">
            <td colspan="2" class="headlineColumn">
                Verksamhetsbeskrivning</td>
        </tr>
        <tr>
            <td colspan="2" style="height: 80px">
                <asp:TextBox ID="txtVerksamheten" runat="server" TextMode="MultiLine" Height="80px" Width="690px"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <table style="width: 704px">
        <tr class="headlineRow">
            <td style="width: 704px" class="headlineColumn">
                Väsentliga händelser under och efter räkenskapsåret</td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:TextBox ID="txtVasantligaHandelser" runat="server" TextMode="MultiLine" Height="80px" Width="690px"></asp:TextBox></td>
        </tr>
    </table>
    <br />
<table style="width: 704px">
        <tr class="headlineRow">
            <td style="width: 704px" class="headlineColumn">
                Framtida utveckling</td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:TextBox ID="txtFramtidaUtveckling" runat="server" TextMode="MultiLine" Height="80px" Width="690px"></asp:TextBox></td>
        </tr>
    </table>
<br />
<table style="width: 704px">
        <tr class="headlineRow">
            <td style="width: 704px" class="headlineColumn">
                Miljöpåverkan</td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:TextBox ID="txtMiljoPaverkan" runat="server" TextMode="MultiLine" Height="80px" Width="690px"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <table style="width: 704px">
        <tr class="headlineRow">
            <td colspan="2" class="headlineColumn">
                Dispositioner beträffande vinst eller förlust</td>
        </tr>
        <tr>
            <td colspan="2" style="font-style:italic">
                Medel att disponera</td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px;">
                Överkursfond</td>
            <td style="width: 304px; height: 21px;">
                <asp:TextBox ID="MedelAttDisponeraOverkursfond" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Fond för verkligt värde</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="MedelAttDisponeraFondForVerkligtVarde" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Balanserat resultat</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="MedelAttDisponeraBalanseratResultat" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Koncernbidrag</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="MedelAttDisponeraKoncernbidrag" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Erhållna aktieägartillskott</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="MedelAttDisponeraErhallnaAktieagartillskott" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Årets resultat</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="MedelAttDisponeraAretsResultat" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px; font-style:italic">
                Summa</td>
            <td style="width: 304px; height: 21px">   
                <asp:TextBox ID="MedelAttDisponeraSumma" runat="server" Width="95%" Font-Italic="True"></asp:TextBox>
             </td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                &nbsp;</td>
            <td style="width: 304px; height: 21px">
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 21px; font-style:italic">
                Förslag till disposition</td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Utdelning</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="ForslagDispositionUtdelning" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Fondemisson</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="ForslagDispositionFondemission" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Avsättning till reservfond</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="ForslagDispositionAvsattningTillReservfond" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Ianspråktagande av reservfond</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="ForslagDispositionIanspraktagandeAvReservFond" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Ianspråktagande av uppskrivningsfond</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="ForslagDispositionIanspraktagandeAvUppskrivningsfond" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px">
                Balanseras i ny räkning</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="ForslagDispositionBalanserasINyRakning" runat="server" Width="95%"></asp:TextBox></td>
        </tr>
        <tr>
            <td style="width: 500px; height: 21px; font-style:italic">
                Summa</td>
            <td style="width: 304px; height: 21px">
                <asp:TextBox ID="ForslagDispositionSumma" runat="server" Width="95%" Font-Italic="True"></asp:TextBox></td>
        </tr>
    </table>
<br />
<table style="width: 704px">
        <tr class="headlineRow">
            <td style="width: 704px" class="headlineColumn">
                Kommentar till dispositioner</td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:TextBox ID="txtKommentarDispositioner" runat="server" TextMode="MultiLine" Height="80px" Width="690px"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <table style="width: 704px">
        <tr class="headlineRow">
            <td style="width: 704px" class="headlineColumn">
                Styrelsens yttrande om vinstutdelning</td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:TextBox ID="StyrelsensYttrandeVinstutdelning" runat="server" TextMode="MultiLine" Height="80px" Width="690px" Text="Styrelsens uppfattning &#228;r att den f&#246;reslagna utdelningen ej hindrar bolaget fr&#229;n att fullg&#246;ra sina f&#246;rpliktelser p&#229; kort och l&#229;ng sikt, ej heller att fullg&#246;ra erforderliga investeringar. Den f&#246;reslagna utdelningen kan d&#228;rmed f&#246;rsvaras med h&#228;nsyn till vad som anf&#246;rs i ABL 17 kap 3 &#167; 2-3 st (f&#246;rsiktighetsregeln)."></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <table style="width: 704px">
        <tr class="headlineRow">
            <td style="width: 704px" class="headlineColumn">
                Resultat och ställning</td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:TextBox ID="ResultatOchStallning" runat="server" TextMode="MultiLine" Height="80px" Width="690px" Text="Resultatet av bolagets verksamhet samt st&#228;llningen vid r&#228;kenskaps&#229;rets utg&#229;ng framg&#229;r av efterf&#246;ljande resultatr&#228;kning och balansr&#228;kning med bokslutskommentarer."></asp:TextBox></td>
        </tr>
    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="ExpenseDetailRevenueReport" runat="server" StepType="Step" Title="Profit and loss statement">
                    <table style="width: 704px">
                        <tr>
                            <td style="width: 400px; font-weight:bold">
                                Välj typ av resultaträkning:</td>
                            <td colspan="2">
                                <asp:DropDownList ID="ChooseIncomeReport" runat="server" Width="94%" AutoPostBack="True">
                                    <asp:ListItem Selected="True" Value="KOSTNADSSLAG">Kostnadslagsindelad resultatr&#228;kning</asp:ListItem>
                                    <asp:ListItem Value="FUNKTION">Funktionsindelad resultatr&#228;kning</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr class="headlineRow">
                            <td style="width: 400px" class="headlineColumn">
                                Resultaträkning</td>
                            <td style="width: 152px" class="headlineColumn">
                                År 0</td>
                            <td style="width: 152px" class="headlineColumn">
                                År -1</td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Nettoomsättning</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="NettoomsattningRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="NettoomsattningRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="KostnadSaldaVarorRow" visible="False">
                            <td runat="server" style="width: 400px">
                                Kostnad för sålda varor </td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="KostnadSaldaVarorRES0" runat="server" Width="95%"></asp:TextBox>
                            </td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="KostnadSaldaVarorRESm1" runat="server" Width="95%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="BruttoresultatRow" visible="False">
                            <td runat="server" style="width: 400px; font-weight: bold">
                                Bruttoresultat</td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="BruttoresultatRES0" runat="server" Width="95%" Font-Bold="True"></asp:TextBox>
                            </td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="BruttoresultatRESm1" runat="server" Width="95%" Font-Bold="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="TomBruttoresultatRow" visible="False">
                            <td runat="server" style="width: 400px">
                                &nbsp;</td>
                            <td runat="server" style="width: 152px">
                            </td>
                            <td runat="server" style="width: 152px">
                            </td>
                        </tr>
                        <tr runat="server" id="ForsaljningskostnaderRow" visible="False">
                            <td runat="server" style="width: 400px">
                                Försäljningskostnader</td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="ForsaljningskostnaderRES0" runat="server" Width="95%"></asp:TextBox>
                            </td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="ForsaljningskostnaderRESm1" runat="server" Width="95%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="AdministrationskostnaderRow" visible="False">
                            <td runat="server" style="width: 400px">
                                Administrationskostnader</td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="AdministrationskostnaderRES0" AutoPostBack="True" runat="server" Width="95%"></asp:TextBox>
                            </td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="AdministrationskostnaderRESm1" runat="server" Width="95%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="ForskningUtvecklingRow" visible="False">
                            <td runat="server" style="width: 400px">
                                Forsknings- och utvecklingskostnader</td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="ForskningUtvecklingRES0" runat="server" Width="95%"></asp:TextBox>
                            </td>
                            <td runat="server" style="width: 152px">
                                <asp:TextBox ID="ForskningUtvecklingRESm1" runat="server" Width="95%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="LagerForandringRow">
                            <td style="width: 400px" runat="server">
                                Lagerförändring </td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="LagerforandringRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="LagerforandringRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="AktiveratArbeteRow">
                            <td style="width: 400px" runat="server">
                                Aktiverat arbete för egen räkning </td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="AktiveratArbeteForEgenRakningRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="AktiveratArbeteForEgenRakningRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Övriga rörelseintäkter</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaRorelseIntakterRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaRorelseIntakterRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="SummaRorelseintakterRow">
                            <td style="width: 400px; font-style:italic;" runat="server">
                                Summa rörelseintäkter </td>
                            <td style="width: 152px;" runat="server">
                                <asp:TextBox ID="RorelseintakterLagerforandringarMmRES0" runat="server" Width="95%" Font-Italic="True"></asp:TextBox></td>
                            <td style="width: 152px;" runat="server">
                                <asp:TextBox ID="RorelseintakterLagerforandringarMmRESm1" runat="server" Width="95%" Font-Italic="True"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="TomSummaRorelseintakterRow">
                            <td style="width: 400px; height: 17px;" runat="server">
                                &nbsp;</td>
                            <td style="width: 152px; height: 17px;" runat="server">
                            </td>
                            <td style="width: 152px; height: 17px;" runat="server">
                            </td>
                        </tr>
                        <tr runat="server" id="RavarorFornodenheterRow">
                            <td style="width: 400px; height: 21px;" runat="server">
                                Råvaror och förnödenheter </td>
                            <td style="width: 152px; height: 21px;" runat="server">
                                <asp:TextBox ID="RavarorOchFornodenheterResultatrakningenRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px; height: 21px;" runat="server">
                                <asp:TextBox ID="RavarorOchFornodenheterResultatrakningenRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="HandelsvarorRow">
                            <td style="width: 400px" runat="server">
                                Handelsvaror </td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="HandelsvarorRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="HandelsvarorRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="OvrigaExternaKostnaderRow">
                            <td style="width: 400px" runat="server">
                                Övriga externa kostnader </td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="OvrigaExternaKostnaderRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="OvrigaExternaKostnaderRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="PersonalkostnaderRow">
                            <td style="width: 400px; height: 21px;" runat="server">
                                Personalkostnader </td>
                            <td style="width: 152px; height: 21px;" runat="server">
                                <asp:TextBox ID="PersonalkostnaderRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px; height: 21px;" runat="server">
                                <asp:TextBox ID="PersonalkostnaderRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="AvNedskrivningarMatImmRow">
                            <td style="width: 400px" runat="server">
                                Av- och nedskrivningar av materiella och immateriella anläggningstillgångar </td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="AvskrivningarOchNedskrivningarAvMateriellaOchImmateriellaAnlaggningstillgangarRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="AvskrivningarOchNedskrivningarAvMateriellaOchImmateriellaAnlaggningstillgangarRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="NedskrivningarOmstillgangarRow">
                            <td style="width: 400px" runat="server">
                                Nedskrivningar av omsättningstillgångar utöver normala nedskrivningar </td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="NedskrivningarAvOmsattningstillgangarUtoverNormalaNedskrivningarRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="NedskrivningarAvOmsattningstillgangarUtoverNormalaNedskrivningarRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Övriga rörelsekostnader</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaRorelsekostnaderRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaRorelsekostnaderRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr runat="server" id="SummaFunktionerRow" visible="False">
                            <td style="width: 400px; font-style:italic;" runat="server">
                                Summa funktioner</td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="SummaFunktionerRES0" runat="server" Width="95%" Font-Italic="True"></asp:TextBox>
                            </td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="SummaFunktionerRESm1" runat="server" Width="95%" Font-Italic="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="SummaRorelsekostnaderRow">
                            <td style="width: 400px; font-style:italic;" runat="server">
                                Summa rörelsekostnader </td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="RorelsekostnaderRES0" runat="server" Width="95%" Font-Italic="True"></asp:TextBox></td>
                            <td style="width: 152px" runat="server">
                                <asp:TextBox ID="RorelsekostnaderRESm1" runat="server" Width="95%" Font-Italic="True"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px; font-weight:bold">
                                Rörelseresultat</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="RorelseresultatRES0" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="RorelseresultatRESm1" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Resultat från andelar i koncernföretag</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatFranAndelarIKoncernforetagRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatFranAndelarIKoncernforetagRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Resultat från andelar i intresseföretag</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatFranAndelarIIntresseforetagRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatFranAndelarIIntresseforetagRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Resultat från övriga finansiella anläggningstillgångar</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatFranOvrigaFinansiellaAnlaggningstillgangarRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatFranOvrigaFinansiellaAnlaggningstillgangarRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Övriga ränteintäkter och liknande resultatposter</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaRanteintakterOchLiknandeResultatposterRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaRanteintakterOchLiknandeResultatposterRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Nedskrivningar av finansiella anläggningstillgångar och kortfristiga placeringar</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="NedskrivningarAvFinansiellaAnlaggningstillgangarOchKortfristigaPlaceringarRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="NedskrivningarAvFinansiellaAnlaggningstillgangarOchKortfristigaPlaceringarRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Räntekostnader och liknande resultatposter</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="RantekostnaderOchLiknandeResultatposterRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="RantekostnaderOchLiknandeResultatposterRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px; font-style:italic">
                                Summa finansiella poster</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="SummaFinPosterRES0" runat="server" Width="95%" Font-Italic="True"></asp:TextBox>
                            </td>
                            <td style="width: 152px">
                                <asp:TextBox ID="SummaFinPosterRESm1" runat="server" Width="95%" Font-Italic="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px; font-weight:bold">
                                Resultat efter finansiella poster</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatEfterFinansiellaPosterRES0" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatEfterFinansiellaPosterRESm1" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Extraordinära intäkter</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ExtraordinaraIntakterRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ExtraordinaraIntakterRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Extraordinära kostnader</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ExtraordinaraKostnaderRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ExtraordinaraKostnaderRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px; font-weight:bold">
                                Resultat före bokslutsdispositioner och skatt</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatForeBokslutsdispositionerOchSkattRES0" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatForeBokslutsdispositionerOchSkattRESm1" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Förändring av periodiseringsfond</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ForandringAvPeriodiseringsfondRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ForandringAvPeriodiseringsfondRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Förändring av överavskrivningar</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ForandringAvOveravskrivningarRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ForandringAvOveravskrivningarRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Erhållna koncernbidrag</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ErhallnaKoncernbidragRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ErhallnaKoncernbidragRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Lämnade koncernbidrag</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="LamnadeKoncernbidragRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="LamnadeKoncernbidragRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Övriga bokslutsdispositioner</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaBokslutsdispositionerRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaBokslutsdispositionerRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px; font-style:italic">
                                Summa bokslutsdispositioner</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="SummaBokslutsdispositionerRES0" runat="server" Width="95%" Font-Italic="True"></asp:TextBox>
                            </td>
                            <td style="width: 152px">
                                <asp:TextBox ID="SummaBokslutsdispositionerRESm1" runat="server" Width="95%" Font-Italic="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px; font-weight:bold">
                                Resultat före skatt</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatForeSkattRES0" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="ResultatForeSkattRESm1" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Skatt på årets resultat</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="SkattPaAretsResultatRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="SkattPaAretsResultatRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                Övriga skatter</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaSkatterRES0" runat="server" Width="95%"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="OvrigaSkatterRESm1" runat="server" Width="95%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 400px">
                                &nbsp;</td>
                            <td style="width: 152px">
                            </td>
                            <td style="width: 152px">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 400px; font-weight:bold">
                                Årets resultat</td>
                            <td style="width: 152px">
                                <asp:TextBox ID="AretsResultatRES0" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                            <td style="width: 152px">
                                <asp:TextBox ID="AretsResultatRESm1" runat="server" Width="95%" Font-Bold="True"></asp:TextBox></td>
                        </tr>
                    </table>
                </asp:WizardStep>
                <asp:WizardStep ID="CreateXbrlReport" runat="server" StepType="Finish" Title="Create xbrl-file">
                    <table style="width: 704px">
                        <tr class="headlineRow">
                            <td colspan="2" class="headlineColumn">
                                Undertecknande</td>
                        </tr>
                        <tr>
                            <td style="width: 704px">
                                Ort för undertecknande:</td>
                            <td style="width: 704px">
                                <asp:TextBox ID="OrtUndertecknande" runat="server" Width="95%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredCitySign" runat="server" ControlToValidate="OrtUndertecknande"
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="Signing" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 704px">
                                Datum för undertecknande:</td>
                            <td style="width: 704px">
                                <asp:TextBox ID="DatumUndertecknande" runat="server" Width="95%"></asp:TextBox>
                                <asp:CompareValidator ID="CompareDateSign" runat="server" ControlToValidate="DatumUndertecknande"
                                Display="Dynamic" EnableTheming="True" ErrorMessage="*" Operator="DataTypeCheck"
                                Type="Date" ValidationGroup="Signing" ValueToCompare="yyyy-MM-dd" SetFocusOnError="True"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="RequiredDateSign" runat="server" ControlToValidate="DatumUndertecknande"
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="Signing" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                            You are on the last step and ready to create a XML-file.</td> 
                        </tr>
                    </table>
                </asp:WizardStep>
            </WizardSteps>
            <StepStyle BorderWidth="0px" ForeColor="Black" VerticalAlign="Top" BackColor="White" />
            <SideBarButtonStyle BorderWidth="0px" Font-Names="Arial" ForeColor="White" />
            <NavigationButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid"
                BorderWidth="1px" Font-Names="Arial" Font-Size="0.8em" ForeColor="#284775" />
            <SideBarStyle BackColor="#505C52" BorderWidth="0px" Font-Size="0.9em" VerticalAlign="Top" />
            <HeaderStyle BackColor="White" BorderStyle="Solid" Font-Bold="True" Font-Size="0.9em"
                ForeColor="White" HorizontalAlign="Left" />
            <StartNavigationTemplate>
            <div style="margin: 0 auto;">
            <asp:Button ID="StartNextButton" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                BorderStyle="Solid" BorderWidth="1px" CommandName="MoveNext" CssClass="wizardButton" Font-Names="Arial"  
                Text="Next" CausesValidation="True" ValidationGroup="CreateReport" />
            </div>
            </StartNavigationTemplate>
            <SideBarTemplate>
                <asp:DataList ID="SideBarList" runat="server" Width="160px">
                    <ItemTemplate>
                        <asp:LinkButton ID="SideBarButton" runat="server" BorderWidth="0px" Font-Names="Arial"
                        ForeColor="White" CausesValidation="true" ValidationGroup="CreateReport"></asp:LinkButton>
                    </ItemTemplate>
                    <SelectedItemStyle Font-Bold="True" />
                </asp:DataList>
            </SideBarTemplate>
        <NavigationStyle BackColor="White" />
        <StepNavigationTemplate>
        <div style="margin: 0 auto;">
            <asp:Button ID="StepPreviousButton" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" CommandName="MovePrevious"
                Font-Names="Arial" CssClass="wizardButton" Text="Previous" />
            <asp:Button ID="StepNextButton" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                BorderStyle="Solid" BorderWidth="1px" CommandName="MoveNext" Font-Names="Arial"
                CssClass="wizardButton" Text="Next" />
        </div>
        </StepNavigationTemplate>
        <FinishNavigationTemplate>
            <div style="margin: 0 auto;">
            <asp:Button ID="FinishPreviousButton" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                BorderStyle="Solid" BorderWidth="1px" CausesValidation="False" CommandName="MovePrevious"
                Font-Names="Arial" CssClass="wizardButton" Text="Previous" />
            <asp:Button ID="FinishButton" runat="server" BackColor="#FFFBFF" BorderColor="#CCCCCC"
                BorderStyle="Solid" BorderWidth="1px" CommandName="MoveComplete" Font-Names="Arial"
                CssClass="wizardButton" ValidationGroup="Signing" Text="Create XBRL-file" />
            </div>
        </FinishNavigationTemplate>
        </asp:Wizard>
        <br />
        <br />
    
</form>
</body>
</html>