﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
'untuk eksport
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
'
#End Region

Public Class SIPP2300
    Inherits Webform.WebBased
    'Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New Controller.SIPP2300Controller
    Private oCustomclass As New Parameter.SIPP2300
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNavi As ucGridNav

    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNavi.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP2300"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                'untuk eksport
                pnlcopybulandata.Visible = False
                '
                Me.SearchBy = ""
                Me.CmdWhere = "ALL"
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                'untuk eksport
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
                '
            End If
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavi.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.SIPP2300

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP2300(oParameter)


        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()
        FillCombo()
        FillCbo(cboGOL_PERUSAHAAN, "", "1419", "")
        FillCbo(cboSTATUS_KETERKAITAN, 18, "", "")
        FillCbo(cboNegara, 37, "", "")
        FillCbo(cboJNS_MATA_UANG, "", "", "2")
        FillCbo(cboKualitas, 17, "", "")

        If (isFrNav = False) Then
            GridNavi.Initialize(recordCount, pageSize)
        End If

        'untuk eksport
        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
        '

    End Sub
#End Region
    'untuk eksport
    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP2300
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub
    '
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP2300", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            'untuk eksport
            txtbulandata.Enabled = True
            '
            Me.Process = "ADD"
            txtbulandata.Text = ""
            txtNM_PERUSAHAAN.Text = ""
            txtTGL_MULAI.Text = ""
            txtPRSN_BGN_PNYRTN.Text = ""
            txtNILAI_AWL_PNYRTN.Text = ""
            txtNILAI_AWL_PNYRTN_MDL_MUA.Text = ""
            txtNILAI_PNYRTN_AWL_DLM_MUA.Text = ""
            txtNILAI_PNYRTN_MODAL.Text = ""

        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region


#Region "dtgList ItemCommand"
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP2300
        Dim dtEntity As New DataTable
        Dim err As String
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
        Try
            If e.CommandName.Trim = "Edit" Then

                If CheckFeature(Me.Loginid, "SIPP2300", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                'untuk eksport
                txtbulandata.Enabled = False
                '

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetSIPP2300Edit(oParameter)
                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboGOL_PERUSAHAAN.SelectedIndex = cboGOL_PERUSAHAAN.Items.IndexOf(cboGOL_PERUSAHAAN.Items.FindByValue(oRow("GOL_PERUSAHAAN").ToString.Trim))
                    cboSTATUS_KETERKAITAN.SelectedIndex = cboSTATUS_KETERKAITAN.Items.IndexOf(cboSTATUS_KETERKAITAN.Items.FindByValue(oRow("STATUS_KETERKAITAN").ToString.Trim))
                    cboNegara.SelectedIndex = cboNegara.Items.IndexOf(cboNegara.Items.FindByValue(oRow("NEGARA").ToString.Trim))
                    cboJNS_MATA_UANG.SelectedIndex = cboJNS_MATA_UANG.Items.IndexOf(cboJNS_MATA_UANG.Items.FindByValue(oRow("JNS_MATA_UANG").ToString.Trim))
                    cboKualitas.SelectedIndex = cboKualitas.Items.IndexOf(cboKualitas.Items.FindByValue(oRow("KUALITAS").ToString.Trim))
                End If
                txtID.Text = oParameter.ListData.Rows(0)("ID")
                'untuk eksport
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                '
                txtNM_PERUSAHAAN.Text = oParameter.ListData.Rows(0)("NM_PERUSAHAAN")
                txtTGL_MULAI.Text = oParameter.ListData.Rows(0)("TGL_MULAI")
                txtPRSN_BGN_PNYRTN.Text = oParameter.ListData.Rows(0)("PRSN_BGN_PNYRTN")
                txtNILAI_PNYRTN_AWL_DLM_MUA.Text = oParameter.ListData.Rows(0)("NILAI_PNYRTN_AWL_DLM_MUA")
                txtNILAI_AWL_PNYRTN.Text = oParameter.ListData.Rows(0)("NILAI_AWL_PNYRTN")
                txtNILAI_AWL_PNYRTN_MDL_MUA.Text = oParameter.ListData.Rows(0)("NILAI_AWL_PNYRTN_MDL_MUA")
                txtNILAI_PNYRTN_MODAL.Text = oParameter.ListData.Rows(0)("NILAI_PNYRTN_MODAL")





            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP2300", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP2300
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With
                err = oController.SIPP2300Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#Region " SAVE "
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.SIPP2300
        Dim ErrMessage As String = ""


        If Me.Process = "ADD" Then
            With customClass
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .NM_PERUSAHAAN = txtNM_PERUSAHAAN.Text
                .TGL_MULAI = txtTGL_MULAI.Text
                .PRSN_BGN_PNYRTN = txtPRSN_BGN_PNYRTN.Text
                .NILAI_PNYRTN_AWL_DLM_MUA = txtNILAI_PNYRTN_AWL_DLM_MUA.Text
                .NILAI_AWL_PNYRTN = txtNILAI_AWL_PNYRTN.Text
                .NILAI_AWL_PNYRTN_MDL_MUA = txtNILAI_AWL_PNYRTN_MDL_MUA.Text
                .NILAI_PNYRTN_MODAL = txtNILAI_PNYRTN_MODAL.Text
                .GOL_PERUSAHAAN = cboGOL_PERUSAHAAN.SelectedValue.Trim
                .STATUS_KETERKAITAN = cboSTATUS_KETERKAITAN.SelectedValue.Trim
                .NEGARA = cboNegara.SelectedValue.Trim
                .JNS_MATA_UANG = cboJNS_MATA_UANG.SelectedValue.Trim
                .KUALITAS = cboKualitas.SelectedValue.Trim

                .strConnection = GetConnectionString()
            End With
            oController.SIPP2300Add(customClass)
        ElseIf Me.Process = "EDIT" Then
            With customClass
                .ID = txtID.Text
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .NM_PERUSAHAAN = txtNM_PERUSAHAAN.Text
                .TGL_MULAI = txtTGL_MULAI.Text
                .PRSN_BGN_PNYRTN = txtPRSN_BGN_PNYRTN.Text
                .NILAI_PNYRTN_AWL_DLM_MUA = txtNILAI_PNYRTN_AWL_DLM_MUA.Text
                .NILAI_AWL_PNYRTN = txtNILAI_AWL_PNYRTN.Text
                .NILAI_AWL_PNYRTN_MDL_MUA = txtNILAI_AWL_PNYRTN_MDL_MUA.Text
                .NILAI_PNYRTN_MODAL = txtNILAI_PNYRTN_MODAL.Text
                .GOL_PERUSAHAAN = cboGOL_PERUSAHAAN.SelectedValue.Trim
                .STATUS_KETERKAITAN = cboSTATUS_KETERKAITAN.SelectedValue.Trim
                .NEGARA = cboNegara.SelectedValue.Trim
                .JNS_MATA_UANG = cboJNS_MATA_UANG.SelectedValue.Trim
                .KUALITAS = cboKualitas.SelectedValue.Trim

                .strConnection = GetConnectionString()
            End With
            oController.SIPP2300Update(customClass)
        End If
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Update data berhasil.....", False)
        End If

    End Sub
#End Region
    'untuk eksport
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_230020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP2300Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP2300
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "39_Rincian_Penyertaan_Modal_TB_230020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP2300) As Parameter.SIPP2300
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP2300Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP2300
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    '

#Region " dropdown "
    Private Sub FillCombo()
        'Dim strCombo As String
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP2300
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.valuemaster = valuemaster
        oAssetData.valueparent = valueparent
        oAssetData.valueelement = valueelement
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ElementLabel"
        cboName.DataValueField = "Element"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"


    End Sub
#End Region

#Region "Fill Data"
    Private Sub FillData(ByVal oTable As DataTable)
        Dim oRow As DataRow
        oRow = oTable.Rows(0)
    End Sub
#End Region

End Class