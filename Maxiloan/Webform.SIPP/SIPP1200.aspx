﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP1200.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP1200"  MaintainScrollPositionOnPostback="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>LAPORAN LABA/RUGI DAN PENGHASILAN KOMPREHENSIF LAIN</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>  
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        $("#tabs").tabs({ selected: 1 });
    
    </script>  
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SIPP1200 - LAPORAN POSISI KEUANGAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">
                <%--untuk eksport--%>
                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                 <div class="form_box_header">
                    </div>
                    <%--   <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>--%>
                    <div class="form_box">
                        <div class="form_single">
                        <label>
                            Pilih Bulan Generate
                        </label>
                        <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                            TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        </div>
                   </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>
                <%--end--%>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col" Width="2%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col" Width="7%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" Visible="false">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="15%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PNDPTN_TTL" SortExpression="PNDPTN_TTL " HeaderText="PENDAPATAN" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BBN_TTL" SortExpression="BBN_TTL " HeaderText="BEBAN" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="LB_RG_SBLM_PJK_TTL" SortExpression="LB_RG_SBLM_PJK_TTL" HeaderText="LABA (RUGI) SEBELUM PAJAK" ItemStyle-Width="20%">
                                    </asp:BoundColumn> 
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP1200" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <%--untuk eksport--%>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button> 
                    <%--end--%> 
                </div>

              <%--  <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI RINCIAN TENAGA KERJA ASING
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NAMA">Nama</asp:ListItem>
                            <asp:ListItem Value="KEWARGANEGARAAN">Kewarganegaraan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>--%>
            </asp:Panel>
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>
            <%--untuk eksport--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--end--%>
        <%--PENDAPATAN--%>

                <div class ="form_title">
                     <div class ="form_single">
                         <h3>
                         <b>I. PENDAPATAN</b>
                         </h3> 
                    </div> 
                         <div class="form_single">
                         <asp:Label ID="Label12" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label14" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label16" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator> --%>
                         </div> 
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>1. Pendapatan Operasional</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <asp:Label ID="Label3" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_PRSNL_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label1" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_PRSNL_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label4" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_PRSNL_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator> --%>    
                         </div>                      
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>A. Pendapatan Kegiatan Operasi</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label6" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label8" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label10" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNGBG_HSLMRGNMBL_JS_DR_KGTN_PRS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator> --%>                            
                         </div> 
                </div>   
                <div class="form_box">
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label23" runat="server" Text="1. Pendapatan Bunga dari Kegiatan Pembiayaan Konvensional"></asp:Label>
                        </b>        
                    </div>   
                    <div class="form_single">
                         <asp:Label ID="Label18" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label20" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label22" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_KNVNSNL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>                             
                         </div>
                </div> 
                <div class="form_box">
                    <div class="form_title"> 
                        <div class="form_single">
                            <b>
                                <asp:Label ID="Label24" runat="server" Text="A. Pembiayaan Investasi"></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <asp:Label ID="Label26" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label28" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label30" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>                           
                         </div>
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                          <asp:label runat="server">Sewa Pembiayaan (Finance Lease)</asp:label>
                          <br /> 
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SW_PMBYN_FNNC_LS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   
                    </div>
                    <div class="form_right">
                          <asp:label runat="server">Jual dan Sewa Balik (Sale and Leaseback</asp:label>
                          <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                          <asp:label runat="server">Anjak Piutang With Recourse</asp:label>
                          <br /> 
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   
                    </div>
                    <div class="form_right">
                          <asp:label runat="server">Pembelian dengan Pembayaran Secara Angsuran</asp:label>
                          <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                          <asp:label runat="server">Pembiayaan Proyek</asp:label>
                          <br /> 
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_PRYK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   
                    </div>
                    <div class="form_right">
                          <asp:label runat="server">Pembiayaan Infrastruktur</asp:label>
                          <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__PMBYN_NFRSTRKTR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                      <asp:label runat="server">Skema lain dengan persetujuan OJK</asp:label>  
                        <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   
                    </div>
                </div>
                <div class="form_title"> 
                        <div class="form_single">
                            <b>
                                <asp:Label ID="Label32" runat="server" Text="B. Pembiayaan Modal Kerja"></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <asp:Label ID="Label33" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label35" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label37" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div>
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                          <asp:label runat="server">Jual dan Sewa Balik (Sale and Leaseback)</asp:label>
                          <br /> 
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   --%>
                    </div>
                    <div class="form_right">
                          <asp:label runat="server">Anjak Piutang With Recourse</asp:label>
                          <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                          <asp:label runat="server">Anjak Piutang Without Recourse</asp:label>
                          <br /> 
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator46" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator47" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator48" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   
                    </div>
                    <div class="form_right">
                          <asp:label runat="server">Fasilitas Modal Usaha</asp:label>
                          <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator49" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator50" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator51" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__FSLTS_MDL_SH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                      <asp:label runat="server">Skema lain dengan persetujuan OJK</asp:label>  
                        <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator53" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator54" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   
                    </div>
                </div>
                <div class="form_title"> 
                        <div class="form_single">
                            <b>
                                <asp:Label ID="Label39" runat="server" Text="C. Pembiayaan Multiguna"></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <asp:Label ID="Label40" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label42" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label44" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div>
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                          <asp:label runat="server">Sewa Pembiayaan (Finance Lease)</asp:label>
                          <br /> 
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator58" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SW_PMBYN_FNNC_LS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   --%>
                    </div>
                    <div class="form_right">
                          <asp:label runat="server">Pembelian dengan Pembayaran Secara Angsuran</asp:label>
                          <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="15%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="15%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator62" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL" runat="server" CssClass="medium_text" Width="15%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator63" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                      <asp:label runat="server">Skema lain dengan persetujuan OJK</asp:label>  
                        <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator64" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator65" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>   
                    </div>
                </div>
                <div class="form_title"> 
                        <div class="form_single">
                            <b>
                                <asp:Label ID="Label46" runat="server" Text="D. Pembiayaan Lainnya Berdasarkan Persetujuan OJK"></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator68" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator69" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_DR_KGTN_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                              
                         </div>
                </div> 
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>2. Pembiayaan Berdasarkan Prinsip Syariah</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <asp:Label ID="Label53" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator70" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label55" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator71" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label57" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator72" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_MRGNBG_HSLMBL_JS_DR_KGTN_PMBYN_BRDSRKN_PRNSP_SYRH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>     --%>
                         </div>                      
                </div>
                <div class="form_box"> 
                    <div class="form_single">
                            <b>
                                <asp:Label ID="Label59" runat="server" Text="A. Pendapatan Bagi Hasil dari Kegiatan Pembiayaan Investasi"></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator73" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator74" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
<%--                        <asp:RequiredFieldValidator ID="RequiredFieldValidator75" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BG_HSL_DR_KGTN_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                                --%>
                    </div>
                    <div class="form_single">
                            <b>
                                <asp:Label ID="Label66" runat="server" Text="B. Pendapatan Margin dari Kegiatan Pembiayaan Jual Beli "></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator76" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator77" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator78" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MRGN_DR_KGTN_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                               
                    </div>
                    <div class="form_single">
                            <b>
                                <asp:Label ID="Label73" runat="server" Text="C. Pendapatan Imbal Jasa dari Pembiayaan Jasa "></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator79" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator80" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator81" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MBL_JS_DR_KGTN_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                                
                    </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>3. Pendapatan Dari Kegiatan Penerusan Pembiayaan (Chanelling)</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator82" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator83" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator84" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_MBL_JS_DR_KGTN_PNRSN_PMBYN_CHNNLNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                </div>  
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>B. Pendapatan Operasional Lain Terkait Pembiayaan</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label47" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator85" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label49" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator86" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label51" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator87" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div> 
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>1. Pendapatan administrasi</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_DMNSTRS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DMNSTRS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_DMNSTRS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator89" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DMNSTRS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_DMNSTRS_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator90" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DMNSTRS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>2. Pendapatan Provisi</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_PRVS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator91" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRVS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_PRVS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator92" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRVS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_PRVS_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator93" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRVS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>3. Pendapatan Denda</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_DND_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator94" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DND_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_DND_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator95" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DND_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_DND_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator96" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DND_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>4. Diskon Asuransi</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_DSKN_SRNS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator97" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DSKN_SRNS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_DSKN_SRNS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator98" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DSKN_SRNS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_DSKN_SRNS_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator99" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DSKN_SRNS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>5. Lainnya</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator100" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator101" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator102" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRSNL_LN_TRKT_PMBYN_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>C. Pendapatan Operasional Lainnya</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label60" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_PRSNL_LNNY_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator103" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label62" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_PRSNL_LNNY_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator104" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label64" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_PRSNL_LNNY_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator105" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_PRSNL_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div> 
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>1. Pendapatan dari Sewa Operasi</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator106" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DR_SW_PRS_PRTNG_LS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator107" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DR_SW_PRS_PRTNG_LS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_DR_SW_PRS_PRTNG_LS_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator108" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DR_SW_PRS_PRTNG_LS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>2. Pendapatan dari kegiatan Berbasis Fee</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator109" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DR_KGTN_BRBSS_F_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator110" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DR_KGTN_BRBSS_F_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_DR_KGTN_BRBSS_F_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator111" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_DR_KGTN_BRBSS_F_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                <div class="form_box"> 
                    <div class="form_single">
                            <b>
                                <asp:Label ID="Label67" runat="server" Text="A. Pemasaran Produk Reksadana"></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator112" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator113" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator114" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_RKSDN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                                
                    </div>
                    <div class="form_single">
                            <b>
                                <asp:Label ID="Label68" runat="server" Text="B. Pemasaran Produk Asuransi"></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator115" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator116" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator117" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_SRNS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                               
                    </div>
                    <div class="form_single">
                            <b>
                                <asp:Label ID="Label69" runat="server" Text="C. Pemasaran Produk Lainnya"></asp:Label>
                            </b> 
                        </div> 
                    <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator118" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator119" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator120" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_F_DR_PMSRN_PRDK_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                                
                    </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>3. Lainnya</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator130" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRSNL_LNNY_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator131" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRSNL_LNNY_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_PRSNL_LNNY_LNNY_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator134" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_PRSNL_LNNY_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>                      
                </div>
                </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>2. Pendapatan Non Operasional</b> </h4>  
                    </div> 
                        <div class="form_single">
                         <asp:Label ID="Label70" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_NN_PRSNL_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator135" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_NN_PRSNL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label72" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_NN_PRSNL_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator136" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_NN_PRSNL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label75" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_NN_PRSNL_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator137" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_NN_PRSNL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div>                      
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>A. Pendapatan Bunga/Jasa Giro</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_NN_PRSNL_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator138" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_NN_PRSNL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_NN_PRSNL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator139" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_NN_PRSNL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_BNG_NN_PRSNL_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator140" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_BNG_NN_PRSNL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div> 
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>B. Pendapatan Non-Operasional Lainnya</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtPNDPTN_NN_PRSNL_LNNY_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator141" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_NN_PRSNL_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtPNDPTN_NN_PRSNL_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator142" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_NN_PRSNL_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtPNDPTN_NN_PRSNL_LNNY_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator143" runat="server" ErrorMessage="*"
                        ControlToValidate="txtPNDPTN_NN_PRSNL_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div> 
                </div>
                </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>II. BEBAN</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label83" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator144" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label85" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator145" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label87" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator146" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div> 
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>1. Beban Operasional</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label77" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_PRSNL_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator147" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PRSNL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label79" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PRSNL_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator148" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PRSNL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label81" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PRSNL_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator149" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PRSNL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div> 
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>A. Bunga</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label89" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_BNG_PRSNL_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator150" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_BNG_PRSNL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label91" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_BNG_PRSNL_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator151" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_BNG_PRSNL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label93" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_BNG_PRSNL_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator152" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_BNG_PRSNL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div> 
                </div>
                <div class="form_box">
                    <div class ="form_single">
                    <h4> <b>1. Pinjaman yang diterima</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator153" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator154" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator155" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_BNG_DR_PNJMNPMBYN_YNG_DTRM_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>
                    <div class ="form_single">
                    <h4> <b>2. Surat Berharga yang diterbitkan</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator156" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator157" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator158" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_BNG_DR_SRT_BRHRG_YNG_DTRBTKN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div> 
                </div> 
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>B. Premi Swap</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator159" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PRM_TS_TRNSKS_SWP_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator160" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PRM_TS_TRNSKS_SWP_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PRM_TS_TRNSKS_SWP_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator161" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PRM_TS_TRNSKS_SWP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div> 
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>C. Premi Asuransi</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PRM_SRNS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator162" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PRM_SRNS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PRM_SRNS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator163" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PRM_SRNS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PRM_SRNS_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator164" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PRM_SRNS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div> 
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>D. Tenaga Kerja</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label95" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_TNG_KRJ_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator165" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_TNG_KRJ_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label97" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_TNG_KRJ_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator166" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_TNG_KRJ_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label99" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_TNG_KRJ_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator167" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_TNG_KRJ_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div> 
                </div>
                <div class="form_box">
                    <div class ="form_single">
                    <h4> <b>1. Gaji, Upah, dan Tunjangan</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_GJ_PH_DN_TNJNGN_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator168" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_GJ_PH_DN_TNJNGN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_GJ_PH_DN_TNJNGN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator169" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_GJ_PH_DN_TNJNGN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_GJ_PH_DN_TNJNGN_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator170" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_GJ_PH_DN_TNJNGN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>
                    <div class ="form_single">
                    <h4> <b>2. Pendidikan dan Pelatihan Tenaga Kerja</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator171" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator172" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator173" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNGMBNGN_DN_PLTHN_TNG_KRJ_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div> 
                    <div class ="form_single">
                    <h4> <b>3. Lainnya</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_TNG_KRJ_LNNY_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator174" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_TNG_KRJ_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_TNG_KRJ_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator175" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_TNG_KRJ_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_TNG_KRJ_LNNY_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator176" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_TNG_KRJ_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div> 
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>E. Pemasaran</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label101" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_PMSRN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator177" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PMSRN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label103" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PMSRN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator178" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PMSRN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label105" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PMSRN_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator179" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PMSRN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div> 
                </div>
                <div class="form_box">
                    <div class ="form_single">
                    <h4> <b>1. Insentif Pihak Ketiga</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_NSNTF_PHK_KTG_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator180" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_NSNTF_PHK_KTG_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_NSNTF_PHK_KTG_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator181" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_NSNTF_PHK_KTG_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_NSNTF_PHK_KTG_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator182" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_NSNTF_PHK_KTG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>
                    <div class ="form_single">
                    <h4> <b>2. Pemasaran Lainnya</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PMSRN_LNNY_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator183" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PMSRN_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PMSRN_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator184" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PMSRN_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PMSRN_LNNY_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator185" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PMSRN_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                         </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>F. Penyisihan/Penyusutan</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label107" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_PNYSHNPNYSTN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator186" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSHNPNYSTN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label109" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PNYSHNPNYSTN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator187" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSHNPNYSTN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label111" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PNYSHNPNYSTN_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator188" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSHNPNYSTN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div>  
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>1. Penyisihan Piutang Ragu-ragu:</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label113" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator190" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label115" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator191" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKS_DN_STR_KS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label117" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL" runat="server" Text="">0</asp:Label>
<%--                         <asp:RequiredFieldValidator ID="RequiredFieldValidator192" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                             --%>
                         </div>
                </div>
                <div class="form_box">
                    <div class ="form_single">
                    <h4> <b>A. Pembiayaan Investasi</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator193" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator194" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator195" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_NVSTS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div> 
                    <div class ="form_single">
                    <h4> <b>B. Pembiayaan Modal Kerja</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator197" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator198" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MDL_KRJ_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                    <div class ="form_single">
                    <h4> <b>C. Pembiayaan Multiguna</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator199" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator200" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator201" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_MLTGN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div> 
                    <div class ="form_single">
                    <h4> <b>D. Pembiayaan Lainnya Berdasarkan Persetujuan OJK</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator202" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator203" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator204" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_KNVNSNL_LNNY_BRDSRKN_PRSTJN_JK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                    <div class ="form_single">
                    <h4> <b>E. Pembiayaan Berdasarkan Prinsip Syariah</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator205" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator206" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator207" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_PNYSHN_PTNG_RGRG_PTNG_PMBYN_BRDSRKN_PRNSP_SYRH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>2. Penyusutan Aset Tetap Yang Di Sewa Operasikan</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label114" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator208" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label118" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator209" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label120" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator210" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSTN_ST_TTP_YNG_DSWPRSKN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>                             
                         </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>3. Penyusutan Aset Tetap dan Inventaris</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label116" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator211" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label121" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator212" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label123" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator213" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PNYSTN_ST_TTP_DN_NVNTRS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>                             
                         </div>
                </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>G. Sewa</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label108" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_SW_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator214" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_SW_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label112" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_SW_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator215" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_SW_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label122" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_SW_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator216" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_SW_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div>  
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>H. Pemeliharaan dan Perbaikan</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label110" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_PMLHRN_DN_PRBKN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator217" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PMLHRN_DN_PRBKN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label124" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PMLHRN_DN_PRBKN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator218" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PMLHRN_DN_PRBKN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label126" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PMLHRN_DN_PRBKN_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator219" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PMLHRN_DN_PRBKN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div>  
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>I. Administrasi dan Umum</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label119" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_DMNSTRS_DN_MM_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator220" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_DMNSTRS_DN_MM_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label127" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_DMNSTRS_DN_MM_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator221" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_DMNSTRS_DN_MM_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label129" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_DMNSTRS_DN_MM_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator222" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_DMNSTRS_DN_MM_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div>  
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>J. Lainnya</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label125" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtBBN_PRSNL_LNNY_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator223" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PRSNL_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label130" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PRSNL_LNNY_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator224" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PRSNL_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label132" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtBBN_PRSNL_LNNY_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator225" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBBN_PRSNL_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div>  
                </div>
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>2. Pendapatan Non Operasional</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtBBN_NN_PRSNL_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator226" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_NN_PRSNL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtBBN_NN_PRSNL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator227" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_NN_PRSNL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtBBN_NN_PRSNL_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator228" runat="server" ErrorMessage="*"
                        ControlToValidate="txtBBN_NN_PRSNL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                </div>
                </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>III. LABA (RUGI) SEBELUM PAJAK</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label78" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtLB_RG_SBLM_PJK_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator229" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_SBLM_PJK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label82" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtLB_RG_SBLM_PJK_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator230" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_SBLM_PJK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label86" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtLB_RG_SBLM_PJK_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator231" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_SBLM_PJK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div> 
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>IV. TAKSIRAN PAJAK PENGHASILAN</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label80" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="Label84" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator232" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_SBLM_PJK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label88" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="Label128" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator233" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_SBLM_PJK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label131" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="Label133" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator234" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_SBLM_PJK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div> 
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>1. Pajak Tahun Berjalan -/-</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label134" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPJK_THN_BRJLN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator235" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPJK_THN_BRJLN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label136" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPJK_THN_BRJLN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator236" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPJK_THN_BRJLN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label138" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPJK_THN_BRJLN_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator237" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPJK_THN_BRJLN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div> 
                </div>  
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>2. Pendapatan (Beban) Pajak Tangguhan</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label140" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtPNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator238" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BBN_PJK_TNGGHN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label142" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator239" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BBN_PJK_TNGGHN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label144" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtPNDPTN_BBN_PJK_TNGGHN_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator240" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDPTN_BBN_PJK_TNGGHN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div> 
                </div> 
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>V. LABA (RUGI) SETELAH PAJAK</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label135" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator241" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label139" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator242" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label143" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtLB_RG_BRSH_STLH_PJK_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator243" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_BRSH_STLH_PJK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div> 
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>VI. KEUNTUNGAN (KERUGIAN) PENDAPATAN KOMPREHENSIF LAINNYA</b> </h4>
                    </div>  
                        <div class="form_single">
                         <asp:Label ID="Label137" runat="server" Text="Dalam Rupiah :"></asp:Label>
                         <asp:Label ID="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator244" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label145" runat="server" Text="VALAS &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator245" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         <br />
                         <asp:Label ID="Label147" runat="server" Text="TOTAL &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp :"></asp:Label> 
                         <asp:Label ID="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL" runat="server" Text="">0</asp:Label>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator246" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                         </div> 
                </div>
                <div class="form_box">
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>1. Keuntungan (Kerugian) Akibat Perubahan dalam surplus Revaluasi Aset Tetap</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator247" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator248" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator249" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>2. Selisih Kurs Karena Penjabaran Laporan Keuangan dalam Mata Uang Asing</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator250" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator251" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator252" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>3. Keuntungan (Kerugian) Akibat Pengukuran Kembali Aset Keuangan Tersedia Untuk Dijual</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator253" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator254" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator255" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>4. Keuntungan (Kerugian) Akibat Bagian Efektif Instrumen Keuangan Lindung Nilai Dalam Rangka Lindung Nilai Arus Kas</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator256" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator257" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator258" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>5. Keuntungan (Kerugian) Atas Komponen Ekuitas Lainnya Sesuai Prinsip Standar Akuntansi Keuangan</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator259" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator260" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator261" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                </div>
                <div class="form_title">
                    <div class ="form_single">
                    <h4> <b>VII. LABA (RUGI) BERSIH KOMPREHENSIF PERIODE BERJALAN</b> </h4>
                    </div>  
                        <div class="form_single">
                         <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH" runat="server" CssClass="Medium_text" Width="8%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator262" runat="server" ErrorMessage="*"
                        ControlToValidate="txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="8%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator263" runat="server" ErrorMessage="*"
                        ControlToValidate="txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL" runat="server" CssClass="medium_text" Width="8%" Enabled="false" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator264" runat="server" ErrorMessage="*"
                        ControlToValidate="txtLB_RG_BRSH_KMPRHNSF_THN_BRJLN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>       
                        </div>
                </div>
                

                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                   <%-- <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>--%>
                </div>
            </asp:Panel>
    </form>
</body>
</html>
