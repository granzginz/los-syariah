﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP3020.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP3020" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Rincian Penyaluran Kerja Sama Pembiayaan Porsi Pihak Ketiga</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>   
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SIPP3020 - RINCIAN PENYALURAN KERJA SAMA PEMBIAYAAN PORSI PIHAK KETIGA
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE" >
                                        <ItemStyle CssClass="command_col" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                                    </asp:BoundColumn>    
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NOKONTRAK" SortExpression="NOKONTRAK" HeaderText="NOMOR KONTRAK">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JNSKRJSMPEMB" SortExpression="JNSKRJSMPEMB" HeaderText="JENIS KERJASAMA">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TGLMULAI" SortExpression="TGLMULAI" HeaderText="TANGGAL MULAI" DataFormatString="{0:yyyy/MM/dd}">
                                    </asp:BoundColumn>            
                                    <asp:BoundColumn DataField="TGLJATUHTEMPO" SortExpression="TGLJATUHTEMPO" HeaderText="TANGGAL JATUH TEMPO" DataFormatString="{0:yyyy/MM/dd}">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JENISVALUTA" SortExpression="JENISVALUTA" HeaderText="JENIS VALUTA">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NAMAKREDITUR" SortExpression="NAMAKREDITUR" HeaderText="NAMA KREDITUR">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NEGARAASAL" SortExpression="NEGARAASAL" HeaderText="NEGARA ASAL">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP3020" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                </div>

               <%-- <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI  RINCIAN PENYALURAN KERJA SAMA PEMBIAYAAN PORSI PIHAK KETIGA
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NAMAKREDITUR">Nama Kreditur</asp:ListItem>
                            <asp:ListItem Value="NOKONTRAK">No Kontrak</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>--%>

            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>
                
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Kontrak
                        </label>
                        <asp:TextBox ID="txtnokontrak" runat="server" CssClass="medium_text" MaxLength="25"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnokontrak" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Kerjasama Pembiayaan
                        </label>
                        <asp:DropDownList ID="cbokrjsama" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ControlToValidate="cbokrjsama" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Mulai
                        </label>
                        <asp:TextBox ID="txttglmulai" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txttglmulai" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" 
                            ControlToValidate="txttglmulai" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                  <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Jatuh Tempo
                        </label>
                        <asp:TextBox ID="txttgljatuhtempo" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True" TargetControlID="txttgljatuhtempo" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*" 
                            ControlToValidate="txttgljatuhtempo" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Valuta
                        </label>
                         <asp:DropDownList ID="cbovaluta" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="cbovaluta" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Porsi Perusahaan Pembiayaan
                        </label>
                        <asp:TextBox ID="txtporsi" runat="server" CssClass="medium_text" Width="5%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ControlToValidate="txtporsi" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                        
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Plafon Dalam Nilai Mata Uang Asal
                        </label>
                        <asp:TextBox ID="txtplafonvalas" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Masukan Nilai Plafon" ValidationExpression="[0-9]"
                            ControlToValidate="txtplafonvalas" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" 
                        ControlToValidate="txtplafonvalas" ErrorMessage="Input Harus Berupa Angka" Display="Dynamic" CssClass="validator_general"  />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Plafon Dalam Ekuivalen Rupiah
                        </label>
                        <asp:TextBox ID="txtplafonrp" runat="server" CssClass="medium_text" MaxLenght="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Masukan Nilai Plafon" ValidationExpression="[0-9]"
                            ControlToValidate="txtplafonrp" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" 
                        ControlToValidate="txtplafonrp" ErrorMessage="Input Harus Berupa Angka" Display="Dynamic" CssClass="validator_general"  />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Saldo Outstanding Dalam Nilai Mata Uang Asal
                        </label>
                        <asp:TextBox ID="txtoutstandingvalas" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Masukan Nilai Outstanding" ValidationExpression="[0-9]"
                            ControlToValidate="txtoutstandingvalas" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" 
                        ControlToValidate="txtoutstandingvalas" ErrorMessage="Input Harus Berupa Angka" Display="Dynamic" CssClass="validator_general"  />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Saldo Outstanding Dalam Ekuivalen Rupiah
                        </label>
                        <asp:TextBox ID="txtoutstandingrp" runat="server" CssClass="medium_text" MaxLenght="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Masukan Nilai Outstanding" ValidationExpression="[0-9]"
                            ControlToValidate="txtoutstandingrp" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" 
                        ControlToValidate="txtoutstandingrp" ErrorMessage="Input Harus Berupa Angka" Display="Dynamic" CssClass="validator_general"  />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Ba'i/Mu'ajjir
                        </label>
                        <asp:TextBox ID="txtnamakreditur" runat="server" CssClass="medium_text" MaxLenght="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnamakreditur" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Golongan Ba'i/Mu'ajjir
                        </label>
                        <asp:DropDownList ID="cboGolkreditur" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="cboGolkreditur" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Status Keterkaitan
                        </label>
                        <asp:DropDownList ID="cboKeterkaitan" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                            ControlToValidate="cboKeterkaitan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Negara Asal
                        </label>
                        <asp:DropDownList ID="cboNegara" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="cboNegara" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

               
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>