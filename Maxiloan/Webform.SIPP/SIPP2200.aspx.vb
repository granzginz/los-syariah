﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
'untuk eksport
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
'
#End Region

Public Class SIPP2200
    Inherits Webform.WebBased
    'Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New Controller.SIPP2200Controller
    Private oCustomclass As New Parameter.SIPP2200
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNavi As ucGridNav

    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNavi.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP2200"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                'untuk eksport
                pnlcopybulandata.Visible = False
                '
                Me.SearchBy = ""
                Me.CmdWhere = "ALL"
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                'untuk eksport
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
                '
            End If
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavi.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.SIPP2200

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP2200(oParameter)


        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()
        FillCombo()
        FillCbo(cboJNS_SRT_BRHRG, 13, "", "")
        FillCbo(cboTJN_KPMLKN, 15, "", "")
        FillCbo(cboJNS_SK_BNG, 16, "", "")
        FillCbo(cboJNS_VLT, "", "", 2)
        FillCbo(cboKUALITAS, 17, "", "")
        FillCbo(cboNGR, "", "", 3)
        FillCbo(cboGLNGN, "", 1419, "")
        FillCbo(cboSTTS_KTRKTN, 18, "", "")

        If (isFrNav = False) Then
            GridNavi.Initialize(recordCount, pageSize)
        End If

        'untuk eksport
        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
        '

    End Sub
#End Region
    'untuk eksport
    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP2200
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub
    '
#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP2200", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            txtbulandata.Enabled = True
            Me.Process = "ADD"
            txtbulandata.Text = ""
            txtID.Text = ""
            txtNMR_SRT_BRHRG.Text = ""
            txtTNGGL_PNRBTN.Text = ""
            txtTNGGL_JTH_TMP.Text = ""
            txtTNGKT_SK_BNG.Text = ""
            txtNL_MT_NG_SL.Text = ""
            txtKVLN_RPH.Text = ""
            txtNM.Text = ""

        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region


#Region "dtgList ItemCommand"
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP2200
        Dim dtEntity As New DataTable
        Dim err As String
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
        Try
            If e.CommandName.Trim = "Edit" Then

                If CheckFeature(Me.Loginid, "SIPP2200", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False

                'untuk eksport
                txtbulandata.Enabled = False
                '

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetSIPP2200Edit(oParameter)
                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboJNS_SRT_BRHRG.SelectedIndex = cboJNS_SRT_BRHRG.Items.IndexOf(cboJNS_SRT_BRHRG.Items.FindByValue(oRow("JNS_SRT_BRHRG").ToString.Trim))
                    cboTJN_KPMLKN.SelectedIndex = cboTJN_KPMLKN.Items.IndexOf(cboTJN_KPMLKN.Items.FindByValue(oRow("TJN_KPMLKN").ToString.Trim))
                    cboJNS_SK_BNG.SelectedIndex = cboJNS_SK_BNG.Items.IndexOf(cboJNS_SK_BNG.Items.FindByValue(oRow("JNS_SK_BNG").ToString.Trim))
                    cboJNS_VLT.SelectedIndex = cboJNS_VLT.Items.IndexOf(cboJNS_VLT.Items.FindByValue(oRow("JNS_VLT").ToString.Trim))
                    cboKUALITAS.SelectedIndex = cboKUALITAS.Items.IndexOf(cboKUALITAS.Items.FindByValue(oRow("KUALITAS").ToString.Trim))
                    cboNGR.SelectedIndex = cboNGR.Items.IndexOf(cboNGR.Items.FindByValue(oRow("NGR").ToString.Trim))
                    cboGLNGN.SelectedIndex = cboGLNGN.Items.IndexOf(cboGLNGN.Items.FindByValue(oRow("GLNGN").ToString.Trim))
                    cboSTTS_KTRKTN.SelectedIndex = cboSTTS_KTRKTN.Items.IndexOf(cboSTTS_KTRKTN.Items.FindByValue(oRow("STTS_KTRKTN").ToString.Trim))
                    txtTNGGL_PNRBTN.Text = Format(oRow("TNGGL_PNRBTN"), "yyyy/MM/dd").ToString
                    txtTNGGL_JTH_TMP.Text = Format(oRow("TNGGL_JTH_TMP"), "yyyy/MM/dd").ToString
                End If
                'untuk eksport
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                '
                txtID.Text = oParameter.ListData.Rows(0)("ID")
                txtNMR_SRT_BRHRG.Text = oParameter.ListData.Rows(0)("NMR_SRT_BRHRG")
                txtTNGKT_SK_BNG.Text = oParameter.ListData.Rows(0)("TNGKT_SK_BNG")
                txtNL_MT_NG_SL.Text = oParameter.ListData.Rows(0)("NL_MT_NG_SL")
                txtKVLN_RPH.Text = oParameter.ListData.Rows(0)("KVLN_RPH")
                txtNM.Text = oParameter.ListData.Rows(0)("NM")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP2200", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP2200
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With
                err = oController.SIPP2200Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#Region " SAVE "
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.SIPP2200
        Dim ErrMessage As String = ""


        If Me.Process = "ADD" Then
            With customClass
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .JNS_SRT_BRHRG = cboJNS_SRT_BRHRG.SelectedValue.Trim
                .TJN_KPMLKN = cboTJN_KPMLKN.SelectedValue.Trim()
                .JNS_SK_BNG = cboJNS_SK_BNG.SelectedValue.Trim()
                .JNS_VLT = cboJNS_VLT.SelectedValue.Trim()
                .NGR = cboNGR.SelectedValue.Trim()
                .GLNGN = cboGLNGN.SelectedValue.Trim()
                .STTS_KTRKTN = cboSTTS_KTRKTN.SelectedValue.Trim()
                .TNGGL_PNRBTN = txtTNGGL_PNRBTN.Text
                .TNGGL_JTH_TMP = txtTNGGL_JTH_TMP.Text
                .NMR_SRT_BRHRG = txtNMR_SRT_BRHRG.Text
                .TNGKT_SK_BNG = txtTNGKT_SK_BNG.Text
                .NL_MT_NG_SL = txtNL_MT_NG_SL.Text
                .KVLN_RPH = txtKVLN_RPH.Text
                .NM = txtNM.Text
                .KUALITAS = cboKUALITAS.SelectedValue.Trim

                .strConnection = GetConnectionString()
            End With
            oController.SIPP2200Add(customClass)
        ElseIf Me.Process = "EDIT" Then
            With customClass
                .ID = txtID.Text
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .JNS_SRT_BRHRG = cboJNS_SRT_BRHRG.SelectedValue.Trim
                .TJN_KPMLKN = cboTJN_KPMLKN.SelectedValue.Trim()
                .JNS_SK_BNG = cboJNS_SK_BNG.SelectedValue.Trim()
                .JNS_VLT = cboJNS_VLT.SelectedValue.Trim()
                .NGR = cboNGR.SelectedValue.Trim()
                .GLNGN = cboGLNGN.SelectedValue.Trim()
                .STTS_KTRKTN = cboSTTS_KTRKTN.SelectedValue.Trim()
                .TNGGL_PNRBTN = txtTNGGL_PNRBTN.Text
                .TNGGL_JTH_TMP = txtTNGGL_JTH_TMP.Text
                .NMR_SRT_BRHRG = txtNMR_SRT_BRHRG.Text
                .TNGKT_SK_BNG = txtTNGKT_SK_BNG.Text
                .NL_MT_NG_SL = txtNL_MT_NG_SL.Text
                .KVLN_RPH = txtKVLN_RPH.Text
                .NM = txtNM.Text
                .KUALITAS = cboKUALITAS.SelectedValue.Trim

                .strConnection = GetConnectionString()
            End With
            oController.SIPP2200Update(customClass)
        End If
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Update data berhasil.....", False)
        End If

    End Sub
#End Region
    'untuk eksport
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_220020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP2200Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP2200
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "38_Rincian_Surat_Berharga_Yang_Dimiliki_TB_220020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP2200) As Parameter.SIPP2200
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP2200Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP2200
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    '

#Region " dropdown "
    Private Sub FillCombo()
        'Dim strCombo As String
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP2200
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.valuemaster = valuemaster
        oAssetData.valueparent = valueparent
        oAssetData.valueelement = valueelement
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ElementLabel"
        cboName.DataValueField = "Element"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"


    End Sub
#End Region

#Region "Fill Data"
    Private Sub FillData(ByVal oTable As DataTable)
        Dim oRow As DataRow
        oRow = oTable.Rows(0)
    End Sub
#End Region

End Class