﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
'untuk eksport
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
' 
#End Region

Public Class SIPP1100
    Inherits Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New SIPP1100Controller
    Private oCustomclass As New Parameter.SIPP1100
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNaviSIPP1100 As ucGridNav

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNaviSIPP1100.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP1100"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                'untuk eksport
                pnlcopybulandata.Visible = False
                '
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                FillCombo()
                'untuk eksport
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
                '
            End If
        End If
    End Sub
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub
    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNaviSIPP1100.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As New DataTable
        Dim oParameter As New Parameter.SIPP1100
        Dim oData As New DataTable

        'pnlList.Visible = False
        'pnlAdd.Visible = True
        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP1100List(oParameter)



        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        If (isFrNav = False) Then
            GridNaviSIPP1100.Initialize(recordCount, pageSize)
        End If

        'untuk eksport
        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            'FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
        '
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP1100
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP1100", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                'untuk eksport
                txtbulandata.Enabled = False

                '

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.SIPP1100Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If


                'untuk eksport
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                '
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtKS_TTL.Text = oParameter.ListData.Rows(0)("KS_TTL")
                txtKS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KS_NDNSN_RPH")
                txtKS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KS_MT_NG_SNG")
                txtGR_PD_BNK_DLM_NGR_TTL.Text = oParameter.ListData.Rows(0)("GR_PD_BNK_DLM_NGR_TTL")
                txtGR_PD_BNK_DLM_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("GR_PD_BNK_DLM_NGR_NDNSN_RPH")
                txtGR_PD_BNK_DLM_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("GR_PD_BNK_DLM_NGR_MT_NG_SNG")
                txtSMPNN_LNNY_PD_BNK_DLM_NGR_TTL.Text = oParameter.ListData.Rows(0)("SMPNN_LNNY_PD_BNK_DLM_NGR_TTL")
                txtSMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH")
                txtSMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG")
                txtSMPNN_PD_BNK_DLM_NGR_TTL.Text = oParameter.ListData.Rows(0)("SMPNN_PD_BNK_DLM_NGR_TTL")
                txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SMPNN_PD_BNK_DLM_NGR_NDNSN_RPH")
                txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SMPNN_PD_BNK_DLM_NGR_MT_NG_SNG")
                txtGR_PD_BNK_LR_NGR_TTL.Text = oParameter.ListData.Rows(0)("GR_PD_BNK_LR_NGR_TTL")
                txtGR_PD_BNK_LR_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("GR_PD_BNK_LR_NGR_NDNSN_RPH")
                txtGR_PD_BNK_LR_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("GR_PD_BNK_LR_NGR_MT_NG_SNG")
                txtSMPNN_LNNY_PD_BNK_LR_NGR_TTL.Text = oParameter.ListData.Rows(0)("SMPNN_LNNY_PD_BNK_LR_NGR_TTL")
                txtSMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH")
                txtSMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG")
                txtSMPNN_PD_BNK_LR_NGR_TTL.Text = oParameter.ListData.Rows(0)("SMPNN_PD_BNK_LR_NGR_TTL")
                txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SMPNN_PD_BNK_LR_NGR_NDNSN_RPH")
                txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SMPNN_PD_BNK_LR_NGR_MT_NG_SNG")
                txtKS_DN_STR_KS_TTL.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_TTL")
                txtKS_DN_STR_KS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_NDNSN_RPH")
                txtKS_DN_STR_KS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KS_DN_STR_KS_MT_NG_SNG")
                txtST_TGHN_DRVTF_TTL.Text = oParameter.ListData.Rows(0)("ST_TGHN_DRVTF_TTL")
                txtST_TGHN_DRVTF_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("ST_TGHN_DRVTF_NDNSN_RPH")
                txtST_TGHN_DRVTF_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("ST_TGHN_DRVTF_MT_NG_SNG")
                txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL.Text = oParameter.ListData.Rows(0)("NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL")
                txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH")
                txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG")
                txtPTNG_PMBYN_NVSTS__PKK_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS__PKK_TTL")
                txtPTNG_PMBYN_NVSTS__PKK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS__PKK_NDNSN_RPH")
                txtPTNG_PMBYN_NVSTS__PKK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS__PKK_MT_NG_SNG")
                txtCDNGN_PTNG_PMBYN_NVSTS_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_NVSTS_TTL")
                txtCDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH")
                txtCDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG")
                txtPTNG_PMBYN_NVSTS__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS__NT_TTL")
                txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS__NT_NDNSN_RPH")
                txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS__NT_MT_NG_SNG")
                txtPTNG_PMBYN_MDL_KRJ__PKK_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MDL_KRJ__PKK_TTL")
                txtPTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH")
                txtPTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG")
                txtCDNGN_PTNG_MDL_KRJ_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_MDL_KRJ_TTL")
                txtCDNGN_PTNG_MDL_KRJ_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_MDL_KRJ_NDNSN_RPH")
                txtCDNGN_PTNG_MDL_KRJ_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_MDL_KRJ_MT_NG_SNG")
                txtPTNG_PMBYN_MDL_KRJ__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MDL_KRJ__NT_TTL")
                txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH")
                txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG")
                txtPTNG_PMBYN_MLTGN__PKK_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MLTGN__PKK_TTL")
                txtPTNG_PMBYN_MLTGN__PKK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MLTGN__PKK_NDNSN_RPH")
                txtPTNG_PMBYN_MLTGN__PKK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MLTGN__PKK_MT_NG_SNG")
                txtCDNGN_PTNG_PMBYN_MLTGN_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_MLTGN_TTL")
                txtCDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH")
                txtCDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG")
                txtPTNG_PMBYN_MLTGN__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MLTGN__NT_TTL")
                txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MLTGN__NT_NDNSN_RPH")
                txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_MLTGN__NT_MT_NG_SNG")
                txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL")
                txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH")
                txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG")
                txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL")
                txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH")
                txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG")
                txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL")
                txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH")
                txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG")
                txtPTNG_PMBYN_KNVNSNL__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_KNVNSNL__NT_TTL")
                txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH")
                txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG")
                txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL")
                txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH")
                txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG")
                txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL")
                txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")
                txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL")
                txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH")
                txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG")
                txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL")
                txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH")
                txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG")
                txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL")
                txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")
                txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL")
                txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH")
                txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG")
                txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL")
                txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH")
                txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG")
                txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL")
                txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH")
                txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG")
                txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL")
                txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH")
                txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG")
                txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL")
                txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH")
                txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG")
                txtPTNG_PMBYN__NT_TTL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN__NT_TTL")
                txtPTNG_PMBYN__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN__NT_NDNSN_RPH")
                txtPTNG_PMBYN__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN__NT_MT_NG_SNG")
                txtPNYRTN_MDL_PD_BNK_TTL.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_BNK_TTL")
                txtPNYRTN_MDL_PD_BNK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_BNK_NDNSN_RPH")
                txtPNYRTN_MDL_PD_BNK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_BNK_MT_NG_SNG")
                txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL")
                txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH")
                txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG")
                txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL")
                txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH")
                txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG")
                txtPNYRTN_MDL_TTL.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_TTL")
                txtPNYRTN_MDL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_NDNSN_RPH")
                txtPNYRTN_MDL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNYRTN_MDL_MT_NG_SNG")
                txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL.Text = oParameter.ListData.Rows(0)("NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL")
                txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH")
                txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG")
                txtST_YNG_DSWPRSKN_TTL.Text = oParameter.ListData.Rows(0)("ST_YNG_DSWPRSKN_TTL")
                txtST_YNG_DSWPRSKN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("ST_YNG_DSWPRSKN_NDNSN_RPH")
                txtST_YNG_DSWPRSKN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("ST_YNG_DSWPRSKN_MT_NG_SNG")
                txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL.Text = oParameter.ListData.Rows(0)("KMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL")
                txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH")
                txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG")
                txtST_YNG_DSWPRSKN__NT_TTL.Text = oParameter.ListData.Rows(0)("ST_YNG_DSWPRSKN__NT_TTL")
                txtST_YNG_DSWPRSKN__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("ST_YNG_DSWPRSKN__NT_NDNSN_RPH")
                txtST_YNG_DSWPRSKN__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("ST_YNG_DSWPRSKN__NT_MT_NG_SNG")
                txtST_TTP_DN_NVNTRS__TTL.Text = oParameter.ListData.Rows(0)("ST_TTP_DN_NVNTRS__TTL")
                txtST_TTP_DN_NVNTRS__NDNSN_RPH.Text = oParameter.ListData.Rows(0)("ST_TTP_DN_NVNTRS__NDNSN_RPH")
                txtST_TTP_DN_NVNTRS__MT_NG_SNG.Text = oParameter.ListData.Rows(0)("ST_TTP_DN_NVNTRS__MT_NG_SNG")
                txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text = oParameter.ListData.Rows(0)("KMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL")
                txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH")
                txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG")
                txtST_TTP_DN_NVNTRS__NT_TTL.Text = oParameter.ListData.Rows(0)("ST_TTP_DN_NVNTRS__NT_TTL")
                txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("ST_TTP_DN_NVNTRS__NT_NDNSN_RPH")
                txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("ST_TTP_DN_NVNTRS__NT_MT_NG_SNG")
                txtST_PJK_TNGGHN_TTL.Text = oParameter.ListData.Rows(0)("ST_PJK_TNGGHN_TTL")
                txtST_PJK_TNGGHN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("ST_PJK_TNGGHN_NDNSN_RPH")
                txtST_PJK_TNGGHN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("ST_PJK_TNGGHN_MT_NG_SNG")
                txtRPRP_ST_TTL.Text = oParameter.ListData.Rows(0)("RPRP_ST_TTL")
                txtRPRP_ST_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("RPRP_ST_NDNSN_RPH")
                txtRPRP_ST_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("RPRP_ST_MT_NG_SNG")
                txtST_TTL.Text = oParameter.ListData.Rows(0)("ST_TTL")
                txtST_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("ST_NDNSN_RPH")
                txtST_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("ST_MT_NG_SNG")
                txtLBLTS_KPD_BNK_TTL.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_BNK_TTL")
                txtLBLTS_KPD_BNK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_BNK_NDNSN_RPH")
                txtLBLTS_KPD_BNK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_BNK_MT_NG_SNG")
                txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL")
                txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH")
                txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG")
                txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL")
                txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH")
                txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG")
                txtLBLTS_SGR_LNNY_TTL.Text = oParameter.ListData.Rows(0)("LBLTS_SGR_LNNY_TTL")
                txtLBLTS_SGR_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LBLTS_SGR_LNNY_NDNSN_RPH")
                txtLBLTS_SGR_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LBLTS_SGR_LNNY_MT_NG_SNG")
                txtLBLTS_SGR_TTL.Text = oParameter.ListData.Rows(0)("LBLTS_SGR_TTL")
                txtLBLTS_SGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LBLTS_SGR_NDNSN_RPH")
                txtLBLTS_SGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LBLTS_SGR_MT_NG_SNG")
                txtLBLTS_DRVTF_TTL.Text = oParameter.ListData.Rows(0)("LBLTS_DRVTF_TTL")
                txtLBLTS_DRVTF_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LBLTS_DRVTF_NDNSN_RPH")
                txtLBLTS_DRVTF_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LBLTS_DRVTF_MT_NG_SNG")
                txtTNG_PJK_TTL.Text = oParameter.ListData.Rows(0)("TNG_PJK_TTL")
                txtTNG_PJK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("TNG_PJK_NDNSN_RPH")
                txtTNG_PJK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("TNG_PJK_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL")
                txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL")
                txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL")
                txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_DLM_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DLM_NGR_TTL")
                txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL")
                txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL")
                txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_LNNY_LR_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LNNY_LR_NGR_TTL")
                txtPNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_LR_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LR_NGR_TTL")
                txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG")
                txtPNDNN_YNG_DTRM_TTL.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_TTL")
                txtPNDNN_YNG_DTRM_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_NDNSN_RPH")
                txtPNDNN_YNG_DTRM_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNDNN_YNG_DTRM_MT_NG_SNG")
                txtSRT_BRHRG_YNG_DTRBTKN_TTL.Text = oParameter.ListData.Rows(0)("SRT_BRHRG_YNG_DTRBTKN_TTL")
                txtSRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH")
                txtSRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG")
                txtLBLTS_PJK_TNGGHN_TTL.Text = oParameter.ListData.Rows(0)("LBLTS_PJK_TNGGHN_TTL")
                txtLBLTS_PJK_TNGGHN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LBLTS_PJK_TNGGHN_NDNSN_RPH")
                txtLBLTS_PJK_TNGGHN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LBLTS_PJK_TNGGHN_MT_NG_SNG")
                txtPNJMN_SBRDNS_DLM_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_DLM_NGR_TTL")
                txtPNJMN_SBRDNS_DLM_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_DLM_NGR_NDNSN_RPH")
                txtPNJMN_SBRDNS_DLM_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_DLM_NGR_MT_NG_SNG")
                txtPNJMN_SBRDNS_LR_NGR_TTL.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_LR_NGR_TTL")
                txtPNJMN_SBRDNS_LR_NGR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_LR_NGR_NDNSN_RPH")
                txtPNJMN_SBRDNS_LR_NGR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_LR_NGR_MT_NG_SNG")
                txtPNJMN_SBRDNS_TTL.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_TTL")
                txtPNJMN_SBRDNS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_NDNSN_RPH")
                txtPNJMN_SBRDNS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("PNJMN_SBRDNS_MT_NG_SNG")
                txtRPRP_LBLTS_TTL.Text = oParameter.ListData.Rows(0)("RPRP_LBLTS_TTL")
                txtRPRP_LBLTS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("RPRP_LBLTS_NDNSN_RPH")
                txtRPRP_LBLTS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("RPRP_LBLTS_MT_NG_SNG")
                txtMDL_DSR_TTL.Text = oParameter.ListData.Rows(0)("MDL_DSR_TTL")
                txtMDL_DSR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("MDL_DSR_NDNSN_RPH")
                txtMDL_DSR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("MDL_DSR_MT_NG_SNG")
                txtMDL_YNG_BLM_DSTR_TTL.Text = oParameter.ListData.Rows(0)("MDL_YNG_BLM_DSTR_TTL")
                txtMDL_YNG_BLM_DSTR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("MDL_YNG_BLM_DSTR_NDNSN_RPH")
                txtMDL_YNG_BLM_DSTR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("MDL_YNG_BLM_DSTR_MT_NG_SNG")
                txtMDL_DSTR_TTL.Text = oParameter.ListData.Rows(0)("MDL_DSTR_TTL")
                txtMDL_DSTR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("MDL_DSTR_NDNSN_RPH")
                txtMDL_DSTR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("MDL_DSTR_MT_NG_SNG")
                txtSMPNN_PKK_TTL.Text = oParameter.ListData.Rows(0)("SMPNN_PKK_TTL")
                txtSMPNN_PKK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SMPNN_PKK_NDNSN_RPH")
                txtSMPNN_PKK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SMPNN_PKK_MT_NG_SNG")
                txtSMPNN_WJB_TTL.Text = oParameter.ListData.Rows(0)("SMPNN_WJB_TTL")
                txtSMPNN_WJB_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SMPNN_WJB_NDNSN_RPH")
                txtSMPNN_WJB_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SMPNN_WJB_MT_NG_SNG")
                txtSMPNN_PKK_DN_SMPNN_WJB_TTL.Text = oParameter.ListData.Rows(0)("SMPNN_PKK_DN_SMPNN_WJB_TTL")
                txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH")
                txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG")
                txtG_TTL.Text = oParameter.ListData.Rows(0)("G_TTL")
                txtG_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("G_NDNSN_RPH")
                txtG_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("G_MT_NG_SNG")
                txtDSG_TTL.Text = oParameter.ListData.Rows(0)("DSG_TTL")
                txtDSG_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("DSG_NDNSN_RPH")
                txtDSG_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("DSG_MT_NG_SNG")
                txtMDL_SHM_DPRLH_KMBL_TTL.Text = oParameter.ListData.Rows(0)("MDL_SHM_DPRLH_KMBL_TTL")
                txtMDL_SHM_DPRLH_KMBL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("MDL_SHM_DPRLH_KMBL_NDNSN_RPH")
                txtMDL_SHM_DPRLH_KMBL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("MDL_SHM_DPRLH_KMBL_MT_NG_SNG")
                txtBY_MS_FK_KTS_TTL.Text = oParameter.ListData.Rows(0)("BY_MS_FK_KTS_TTL")
                txtBY_MS_FK_KTS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("BY_MS_FK_KTS_NDNSN_RPH")
                txtBY_MS_FK_KTS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("BY_MS_FK_KTS_MT_NG_SNG")
                txtMDL_HBH_TTL.Text = oParameter.ListData.Rows(0)("MDL_HBH_TTL")
                txtMDL_HBH_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("MDL_HBH_NDNSN_RPH")
                txtMDL_HBH_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("MDL_HBH_MT_NG_SNG")
                txtTMBHN_MDL_DSTR_LNNY_TTL.Text = oParameter.ListData.Rows(0)("TMBHN_MDL_DSTR_LNNY_TTL")
                txtTMBHN_MDL_DSTR_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("TMBHN_MDL_DSTR_LNNY_NDNSN_RPH")
                txtTMBHN_MDL_DSTR_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("TMBHN_MDL_DSTR_LNNY_MT_NG_SNG")
                txtTMBHN_MDL_DSTR_TTL.Text = oParameter.ListData.Rows(0)("TMBHN_MDL_DSTR_TTL")
                txtTMBHN_MDL_DSTR_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("TMBHN_MDL_DSTR_NDNSN_RPH")
                txtTMBHN_MDL_DSTR_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("TMBHN_MDL_DSTR_MT_NG_SNG")
                txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL.Text = oParameter.ListData.Rows(0)("SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL")
                txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH")
                txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG")
                txtMDL_TTL.Text = oParameter.ListData.Rows(0)("MDL_TTL")
                txtMDL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("MDL_NDNSN_RPH")
                txtMDL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("MDL_MT_NG_SNG")
                txtCDNGN_MM_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_MM_TTL")
                txtCDNGN_MM_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_MM_NDNSN_RPH")
                txtCDNGN_MM_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_MM_MT_NG_SNG")
                txtCDNGN_TJN_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_TJN_TTL")
                txtCDNGN_TJN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_TJN_NDNSN_RPH")
                txtCDNGN_TJN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_TJN_MT_NG_SNG")
                txtCDNGN_MDL_TTL.Text = oParameter.ListData.Rows(0)("CDNGN_MDL_TTL")
                txtCDNGN_MDL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("CDNGN_MDL_NDNSN_RPH")
                txtCDNGN_MDL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("CDNGN_MDL_MT_NG_SNG")
                txtSLD_LB_RG_YNG_DTHN_TTL.Text = oParameter.ListData.Rows(0)("SLD_LB_RG_YNG_DTHN_TTL")
                txtSLD_LB_RG_YNG_DTHN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLD_LB_RG_YNG_DTHN_NDNSN_RPH")
                txtSLD_LB_RG_YNG_DTHN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLD_LB_RG_YNG_DTHN_MT_NG_SNG")
                txtLB_RG_BRSH_STLH_PJK_TTL.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_STLH_PJK_TTL")
                txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_STLH_PJK_NDNSN_RPH")
                txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LB_RG_BRSH_STLH_PJK_MT_NG_SNG")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH")
                txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG")
                txtSLD_KMPNN_KTS_LNNY_TTL.Text = oParameter.ListData.Rows(0)("SLD_KMPNN_KTS_LNNY_TTL")
                txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("SLD_KMPNN_KTS_LNNY_NDNSN_RPH")
                txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("SLD_KMPNN_KTS_LNNY_MT_NG_SNG")
                txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL")
                txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH")
                txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG")
                txtKMPNN_KTS_LNNY_TTL.Text = oParameter.ListData.Rows(0)("KMPNN_KTS_LNNY_TTL")
                txtKMPNN_KTS_LNNY_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("KMPNN_KTS_LNNY_NDNSN_RPH")
                txtKMPNN_KTS_LNNY_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("KMPNN_KTS_LNNY_MT_NG_SNG")
                txtLBLTS_DN_KTS_TTL.Text = oParameter.ListData.Rows(0)("LBLTS_DN_KTS_TTL")
                txtLBLTS_DN_KTS_NDNSN_RPH.Text = oParameter.ListData.Rows(0)("LBLTS_DN_KTS_NDNSN_RPH")
                txtLBLTS_DN_KTS_MT_NG_SNG.Text = oParameter.ListData.Rows(0)("LBLTS_DN_KTS_MT_NG_SNG")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP1100", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP1100
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.SIPP1100Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    'untuk eksport
    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP1100
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub
    '
#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP1100", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            Clean()
            'untuk eksport
            txtbulandata.Enabled = True
            '

            Me.Process = "ADD"
            'clean()
        End If
    End Sub
#End Region

    '#Region "Cancel"
    '    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
    '        pnlList.Visible = True
    '        pnlAdd.Visible = False
    '        Me.SearchBy = ""
    '        Me.SortBy = ""

    '        'clean()
    '        DoBind(Me.SearchBy, Me.SortBy)
    '    End Sub
    '#End Region

    '#Region "Search"
    '    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
    '        If txtSearch.Text <> "" Then
    '            Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
    '        Else
    '            Me.SearchBy = ""
    '        End If
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '    End Sub
    '#End Region

    '#Region "Reset"
    '    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
    '        Me.SearchBy = ""
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '        txtSearch.Text = ""
    '        cboSearch.ClearSelection()
    '        pnlAdd.Visible = False
    '    End Sub
    '#End Region
    'untuk eksport
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_110020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP1100) As Parameter.SIPP1100
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP1100Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP1100
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "33_Laporan_Posisi_Keuangan_TB_110020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP1100) As Parameter.SIPP1100
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            'params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            'params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP1100Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP1100
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = txtcopybulandata.Text
            '.Table = "" 'cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    '
#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.SIPP1100
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            With oParameter
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .KS_TTL = txtKS_TTL.Text
                .KS_NDNSN_RPH = txtKS_NDNSN_RPH.Text
                .KS_MT_NG_SNG = txtKS_MT_NG_SNG.Text
                .GR_PD_BNK_DLM_NGR_TTL = txtGR_PD_BNK_DLM_NGR_TTL.Text
                .GR_PD_BNK_DLM_NGR_NDNSN_RPH = txtGR_PD_BNK_DLM_NGR_NDNSN_RPH.Text
                .GR_PD_BNK_DLM_NGR_MT_NG_SNG = txtGR_PD_BNK_DLM_NGR_MT_NG_SNG.Text
                .SMPNN_LNNY_PD_BNK_DLM_NGR_TTL = txtSMPNN_LNNY_PD_BNK_DLM_NGR_TTL.Text
                .SMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH = txtSMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH.Text
                .SMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG = txtSMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG.Text
                .SMPNN_PD_BNK_DLM_NGR_TTL = txtSMPNN_PD_BNK_DLM_NGR_TTL.Text
                .SMPNN_PD_BNK_DLM_NGR_NDNSN_RPH = txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH.Text
                .SMPNN_PD_BNK_DLM_NGR_MT_NG_SNG = txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG.Text
                .GR_PD_BNK_LR_NGR_TTL = txtGR_PD_BNK_LR_NGR_TTL.Text
                .GR_PD_BNK_LR_NGR_NDNSN_RPH = txtGR_PD_BNK_LR_NGR_NDNSN_RPH.Text
                .GR_PD_BNK_LR_NGR_MT_NG_SNG = txtGR_PD_BNK_LR_NGR_MT_NG_SNG.Text
                .SMPNN_LNNY_PD_BNK_LR_NGR_TTL = txtSMPNN_LNNY_PD_BNK_LR_NGR_TTL.Text
                .SMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH = txtSMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH.Text
                .SMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG = txtSMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG.Text
                .SMPNN_PD_BNK_LR_NGR_TTL = txtSMPNN_PD_BNK_LR_NGR_TTL.Text
                .SMPNN_PD_BNK_LR_NGR_NDNSN_RPH = txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH.Text
                .SMPNN_PD_BNK_LR_NGR_MT_NG_SNG = txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG.Text
                .KS_DN_STR_KS_TTL = txtKS_DN_STR_KS_TTL.Text
                .KS_DN_STR_KS_NDNSN_RPH = txtKS_DN_STR_KS_NDNSN_RPH.Text
                .KS_DN_STR_KS_MT_NG_SNG = txtKS_DN_STR_KS_MT_NG_SNG.Text
                .ST_TGHN_DRVTF_TTL = txtST_TGHN_DRVTF_TTL.Text
                .ST_TGHN_DRVTF_NDNSN_RPH = txtST_TGHN_DRVTF_NDNSN_RPH.Text
                .ST_TGHN_DRVTF_MT_NG_SNG = txtST_TGHN_DRVTF_MT_NG_SNG.Text
                .NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL = txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL.Text
                .NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH = txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH.Text
                .NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG = txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG.Text
                .PTNG_PMBYN_NVSTS__PKK_TTL = txtPTNG_PMBYN_NVSTS__PKK_TTL.Text
                .PTNG_PMBYN_NVSTS__PKK_NDNSN_RPH = txtPTNG_PMBYN_NVSTS__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_NVSTS__PKK_MT_NG_SNG = txtPTNG_PMBYN_NVSTS__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_NVSTS_TTL = txtCDNGN_PTNG_PMBYN_NVSTS_TTL.Text
                .CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text
                .PTNG_PMBYN_NVSTS__NT_TTL = txtPTNG_PMBYN_NVSTS__NT_TTL.Text
                .PTNG_PMBYN_NVSTS__NT_NDNSN_RPH = txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_NVSTS__NT_MT_NG_SNG = txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_MDL_KRJ__PKK_TTL = txtPTNG_PMBYN_MDL_KRJ__PKK_TTL.Text
                .PTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH = txtPTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG = txtPTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_MDL_KRJ_TTL = txtCDNGN_PTNG_MDL_KRJ_TTL.Text
                .CDNGN_PTNG_MDL_KRJ_NDNSN_RPH = txtCDNGN_PTNG_MDL_KRJ_NDNSN_RPH.Text
                .CDNGN_PTNG_MDL_KRJ_MT_NG_SNG = txtCDNGN_PTNG_MDL_KRJ_MT_NG_SNG.Text
                .PTNG_PMBYN_MDL_KRJ__NT_TTL = txtPTNG_PMBYN_MDL_KRJ__NT_TTL.Text
                .PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH = txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG = txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_MLTGN__PKK_TTL = txtPTNG_PMBYN_MLTGN__PKK_TTL.Text
                .PTNG_PMBYN_MLTGN__PKK_NDNSN_RPH = txtPTNG_PMBYN_MLTGN__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_MLTGN__PKK_MT_NG_SNG = txtPTNG_PMBYN_MLTGN__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_MLTGN_TTL = txtCDNGN_PTNG_PMBYN_MLTGN_TTL.Text
                .CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text
                .PTNG_PMBYN_MLTGN__NT_TTL = txtPTNG_PMBYN_MLTGN__NT_TTL.Text
                .PTNG_PMBYN_MLTGN__NT_NDNSN_RPH = txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_MLTGN__NT_MT_NG_SNG = txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text
                .CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_KNVNSNL__NT_TTL = txtPTNG_PMBYN_KNVNSNL__NT_TTL.Text
                .PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH = txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG = txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL = txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text
                .CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL = txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text
                .CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL = txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text
                .CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL = txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL.Text
                .PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH = txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG = txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text
                .PTNG_PMBYN__NT_TTL = txtPTNG_PMBYN__NT_TTL.Text
                .PTNG_PMBYN__NT_NDNSN_RPH = txtPTNG_PMBYN__NT_NDNSN_RPH.Text
                .PTNG_PMBYN__NT_MT_NG_SNG = txtPTNG_PMBYN__NT_MT_NG_SNG.Text
                .PNYRTN_MDL_PD_BNK_TTL = txtPNYRTN_MDL_PD_BNK_TTL.Text
                .PNYRTN_MDL_PD_BNK_NDNSN_RPH = txtPNYRTN_MDL_PD_BNK_NDNSN_RPH.Text
                .PNYRTN_MDL_PD_BNK_MT_NG_SNG = txtPNYRTN_MDL_PD_BNK_MT_NG_SNG.Text
                .PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL = txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL.Text
                .PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH = txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text
                .PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG = txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text
                .PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL = txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL.Text
                .PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH = txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text
                .PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG = txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text
                .PNYRTN_MDL_TTL = txtPNYRTN_MDL_TTL.Text
                .PNYRTN_MDL_NDNSN_RPH = txtPNYRTN_MDL_NDNSN_RPH.Text
                .PNYRTN_MDL_MT_NG_SNG = txtPNYRTN_MDL_MT_NG_SNG.Text
                .NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL = txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL.Text
                .NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH = txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH.Text
                .NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG = txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG.Text
                .ST_YNG_DSWPRSKN_TTL = txtST_YNG_DSWPRSKN_TTL.Text
                .ST_YNG_DSWPRSKN_NDNSN_RPH = txtST_YNG_DSWPRSKN_NDNSN_RPH.Text
                .ST_YNG_DSWPRSKN_MT_NG_SNG = txtST_YNG_DSWPRSKN_MT_NG_SNG.Text
                .KMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL = txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL.Text
                .KMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH = txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH.Text
                .KMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG = txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG.Text
                .ST_YNG_DSWPRSKN__NT_TTL = txtST_YNG_DSWPRSKN__NT_TTL.Text
                .ST_YNG_DSWPRSKN__NT_NDNSN_RPH = txtST_YNG_DSWPRSKN__NT_NDNSN_RPH.Text
                .ST_YNG_DSWPRSKN__NT_MT_NG_SNG = txtST_YNG_DSWPRSKN__NT_MT_NG_SNG.Text
                .ST_TTP_DN_NVNTRS__TTL = txtST_TTP_DN_NVNTRS__TTL.Text
                .ST_TTP_DN_NVNTRS__NDNSN_RPH = txtST_TTP_DN_NVNTRS__NDNSN_RPH.Text
                .ST_TTP_DN_NVNTRS__MT_NG_SNG = txtST_TTP_DN_NVNTRS__MT_NG_SNG.Text
                .KMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL = txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text
                .KMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH = txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text
                .KMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG = txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text
                .ST_TTP_DN_NVNTRS__NT_TTL = txtST_TTP_DN_NVNTRS__NT_TTL.Text
                .ST_TTP_DN_NVNTRS__NT_NDNSN_RPH = txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH.Text
                .ST_TTP_DN_NVNTRS__NT_MT_NG_SNG = txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG.Text
                .ST_PJK_TNGGHN_TTL = txtST_PJK_TNGGHN_TTL.Text
                .ST_PJK_TNGGHN_NDNSN_RPH = txtST_PJK_TNGGHN_NDNSN_RPH.Text
                .ST_PJK_TNGGHN_MT_NG_SNG = txtST_PJK_TNGGHN_MT_NG_SNG.Text
                .RPRP_ST_TTL = txtRPRP_ST_TTL.Text
                .RPRP_ST_NDNSN_RPH = txtRPRP_ST_NDNSN_RPH.Text
                .RPRP_ST_MT_NG_SNG = txtRPRP_ST_MT_NG_SNG.Text
                .ST_TTL = txtST_TTL.Text
                .ST_NDNSN_RPH = txtST_NDNSN_RPH.Text
                .ST_MT_NG_SNG = txtST_MT_NG_SNG.Text
                .LBLTS_KPD_BNK_TTL = txtLBLTS_KPD_BNK_TTL.Text
                .LBLTS_KPD_BNK_NDNSN_RPH = txtLBLTS_KPD_BNK_NDNSN_RPH.Text
                .LBLTS_KPD_BNK_MT_NG_SNG = txtLBLTS_KPD_BNK_MT_NG_SNG.Text
                .LBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL = txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL.Text
                .LBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH = txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text
                .LBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG = txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text
                .LBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL = txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL.Text
                .LBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH = txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text
                .LBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG = txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text
                .LBLTS_SGR_LNNY_TTL = txtLBLTS_SGR_LNNY_TTL.Text
                .LBLTS_SGR_LNNY_NDNSN_RPH = txtLBLTS_SGR_LNNY_NDNSN_RPH.Text
                .LBLTS_SGR_LNNY_MT_NG_SNG = txtLBLTS_SGR_LNNY_MT_NG_SNG.Text
                .LBLTS_SGR_TTL = txtLBLTS_SGR_TTL.Text
                .LBLTS_SGR_NDNSN_RPH = txtLBLTS_SGR_NDNSN_RPH.Text
                .LBLTS_SGR_MT_NG_SNG = txtLBLTS_SGR_MT_NG_SNG.Text
                .LBLTS_DRVTF_TTL = txtLBLTS_DRVTF_TTL.Text
                .LBLTS_DRVTF_NDNSN_RPH = txtLBLTS_DRVTF_NDNSN_RPH.Text
                .LBLTS_DRVTF_MT_NG_SNG = txtLBLTS_DRVTF_MT_NG_SNG.Text
                .TNG_PJK_TTL = txtTNG_PJK_TTL.Text
                .TNG_PJK_NDNSN_RPH = txtTNG_PJK_NDNSN_RPH.Text
                .TNG_PJK_MT_NG_SNG = txtTNG_PJK_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL = txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL = txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL.Text
                .PNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DLM_NGR_TTL = txtPNDNN_YNG_DTRM_DLM_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL = txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_LNNY_LR_NGR_TTL = txtPNDNN_YNG_DTRM_LNNY_LR_NGR_TTL.Text
                .PNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_LR_NGR_TTL = txtPNDNN_YNG_DTRM_LR_NGR_TTL.Text
                .PNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_TTL = txtPNDNN_YNG_DTRM_TTL.Text
                .PNDNN_YNG_DTRM_NDNSN_RPH = txtPNDNN_YNG_DTRM_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_MT_NG_SNG = txtPNDNN_YNG_DTRM_MT_NG_SNG.Text
                .SRT_BRHRG_YNG_DTRBTKN_TTL = txtSRT_BRHRG_YNG_DTRBTKN_TTL.Text
                .SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH = txtSRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text
                .SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG = txtSRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text
                .LBLTS_PJK_TNGGHN_TTL = txtLBLTS_PJK_TNGGHN_TTL.Text
                .LBLTS_PJK_TNGGHN_NDNSN_RPH = txtLBLTS_PJK_TNGGHN_NDNSN_RPH.Text
                .LBLTS_PJK_TNGGHN_MT_NG_SNG = txtLBLTS_PJK_TNGGHN_MT_NG_SNG.Text
                .PNJMN_SBRDNS_DLM_NGR_TTL = txtPNJMN_SBRDNS_DLM_NGR_TTL.Text
                .PNJMN_SBRDNS_DLM_NGR_NDNSN_RPH = txtPNJMN_SBRDNS_DLM_NGR_NDNSN_RPH.Text
                .PNJMN_SBRDNS_DLM_NGR_MT_NG_SNG = txtPNJMN_SBRDNS_DLM_NGR_MT_NG_SNG.Text
                .PNJMN_SBRDNS_LR_NGR_TTL = txtPNJMN_SBRDNS_LR_NGR_TTL.Text
                .PNJMN_SBRDNS_LR_NGR_NDNSN_RPH = txtPNJMN_SBRDNS_LR_NGR_NDNSN_RPH.Text
                .PNJMN_SBRDNS_LR_NGR_MT_NG_SNG = txtPNJMN_SBRDNS_LR_NGR_MT_NG_SNG.Text
                .PNJMN_SBRDNS_TTL = txtPNJMN_SBRDNS_TTL.Text
                .PNJMN_SBRDNS_NDNSN_RPH = txtPNJMN_SBRDNS_NDNSN_RPH.Text
                .PNJMN_SBRDNS_MT_NG_SNG = txtPNJMN_SBRDNS_MT_NG_SNG.Text
                .RPRP_LBLTS_TTL = txtRPRP_LBLTS_TTL.Text
                .RPRP_LBLTS_NDNSN_RPH = txtRPRP_LBLTS_NDNSN_RPH.Text
                .RPRP_LBLTS_MT_NG_SNG = txtRPRP_LBLTS_MT_NG_SNG.Text
                .MDL_DSR_TTL = txtMDL_DSR_TTL.Text
                .MDL_DSR_NDNSN_RPH = txtMDL_DSR_NDNSN_RPH.Text
                .MDL_DSR_MT_NG_SNG = txtMDL_DSR_MT_NG_SNG.Text
                .MDL_YNG_BLM_DSTR_TTL = txtMDL_YNG_BLM_DSTR_TTL.Text
                .MDL_YNG_BLM_DSTR_NDNSN_RPH = txtMDL_YNG_BLM_DSTR_NDNSN_RPH.Text
                .MDL_YNG_BLM_DSTR_MT_NG_SNG = txtMDL_YNG_BLM_DSTR_MT_NG_SNG.Text
                .MDL_DSTR_TTL = txtMDL_DSTR_TTL.Text
                .MDL_DSTR_NDNSN_RPH = txtMDL_DSTR_NDNSN_RPH.Text
                .MDL_DSTR_MT_NG_SNG = txtMDL_DSTR_MT_NG_SNG.Text
                .SMPNN_PKK_TTL = txtSMPNN_PKK_TTL.Text
                .SMPNN_PKK_NDNSN_RPH = txtSMPNN_PKK_NDNSN_RPH.Text
                .SMPNN_PKK_MT_NG_SNG = txtSMPNN_PKK_MT_NG_SNG.Text
                .SMPNN_WJB_TTL = txtSMPNN_WJB_TTL.Text
                .SMPNN_WJB_NDNSN_RPH = txtSMPNN_WJB_NDNSN_RPH.Text
                .SMPNN_WJB_MT_NG_SNG = txtSMPNN_WJB_MT_NG_SNG.Text
                .SMPNN_PKK_DN_SMPNN_WJB_TTL = txtSMPNN_PKK_DN_SMPNN_WJB_TTL.Text
                .SMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH = txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH.Text
                .SMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG = txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG.Text
                .G_TTL = txtG_TTL.Text
                .G_NDNSN_RPH = txtG_NDNSN_RPH.Text
                .G_MT_NG_SNG = txtG_MT_NG_SNG.Text
                .DSG_TTL = txtDSG_TTL.Text
                .DSG_NDNSN_RPH = txtDSG_NDNSN_RPH.Text
                .DSG_MT_NG_SNG = txtDSG_MT_NG_SNG.Text
                .MDL_SHM_DPRLH_KMBL_TTL = txtMDL_SHM_DPRLH_KMBL_TTL.Text
                .MDL_SHM_DPRLH_KMBL_NDNSN_RPH = txtMDL_SHM_DPRLH_KMBL_NDNSN_RPH.Text
                .MDL_SHM_DPRLH_KMBL_MT_NG_SNG = txtMDL_SHM_DPRLH_KMBL_MT_NG_SNG.Text
                .BY_MS_FK_KTS_TTL = txtBY_MS_FK_KTS_TTL.Text
                .BY_MS_FK_KTS_NDNSN_RPH = txtBY_MS_FK_KTS_NDNSN_RPH.Text
                .BY_MS_FK_KTS_MT_NG_SNG = txtBY_MS_FK_KTS_MT_NG_SNG.Text
                .MDL_HBH_TTL = txtMDL_HBH_TTL.Text
                .MDL_HBH_NDNSN_RPH = txtMDL_HBH_NDNSN_RPH.Text
                .MDL_HBH_MT_NG_SNG = txtMDL_HBH_MT_NG_SNG.Text
                .TMBHN_MDL_DSTR_LNNY_TTL = txtTMBHN_MDL_DSTR_LNNY_TTL.Text
                .TMBHN_MDL_DSTR_LNNY_NDNSN_RPH = txtTMBHN_MDL_DSTR_LNNY_NDNSN_RPH.Text
                .TMBHN_MDL_DSTR_LNNY_MT_NG_SNG = txtTMBHN_MDL_DSTR_LNNY_MT_NG_SNG.Text
                .TMBHN_MDL_DSTR_TTL = txtTMBHN_MDL_DSTR_TTL.Text
                .TMBHN_MDL_DSTR_NDNSN_RPH = txtTMBHN_MDL_DSTR_NDNSN_RPH.Text
                .TMBHN_MDL_DSTR_MT_NG_SNG = txtTMBHN_MDL_DSTR_MT_NG_SNG.Text
                .SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL = txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL.Text
                .SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH = txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH.Text
                .SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG = txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG.Text
                .MDL_TTL = txtMDL_TTL.Text
                .MDL_NDNSN_RPH = txtMDL_NDNSN_RPH.Text
                .MDL_MT_NG_SNG = txtMDL_MT_NG_SNG.Text
                .CDNGN_MM_TTL = txtCDNGN_MM_TTL.Text
                .CDNGN_MM_NDNSN_RPH = txtCDNGN_MM_NDNSN_RPH.Text
                .CDNGN_MM_MT_NG_SNG = txtCDNGN_MM_MT_NG_SNG.Text
                .CDNGN_TJN_TTL = txtCDNGN_TJN_TTL.Text
                .CDNGN_TJN_NDNSN_RPH = txtCDNGN_TJN_NDNSN_RPH.Text
                .CDNGN_TJN_MT_NG_SNG = txtCDNGN_TJN_MT_NG_SNG.Text
                .CDNGN_MDL_TTL = txtCDNGN_MDL_TTL.Text
                .CDNGN_MDL_NDNSN_RPH = txtCDNGN_MDL_NDNSN_RPH.Text
                .CDNGN_MDL_MT_NG_SNG = txtCDNGN_MDL_MT_NG_SNG.Text
                .SLD_LB_RG_YNG_DTHN_TTL = txtSLD_LB_RG_YNG_DTHN_TTL.Text
                .SLD_LB_RG_YNG_DTHN_NDNSN_RPH = txtSLD_LB_RG_YNG_DTHN_NDNSN_RPH.Text
                .SLD_LB_RG_YNG_DTHN_MT_NG_SNG = txtSLD_LB_RG_YNG_DTHN_MT_NG_SNG.Text
                .LB_RG_BRSH_STLH_PJK_TTL = txtLB_RG_BRSH_STLH_PJK_TTL.Text
                .LB_RG_BRSH_STLH_PJK_NDNSN_RPH = txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text
                .LB_RG_BRSH_STLH_PJK_MT_NG_SNG = txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text
                .SLD_KMPNN_KTS_LNNY_TTL = txtSLD_KMPNN_KTS_LNNY_TTL.Text
                .SLD_KMPNN_KTS_LNNY_NDNSN_RPH = txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH.Text
                .SLD_KMPNN_KTS_LNNY_MT_NG_SNG = txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text
                .KMPNN_KTS_LNNY_TTL = txtKMPNN_KTS_LNNY_TTL.Text
                .KMPNN_KTS_LNNY_NDNSN_RPH = txtKMPNN_KTS_LNNY_NDNSN_RPH.Text
                .KMPNN_KTS_LNNY_MT_NG_SNG = txtKMPNN_KTS_LNNY_MT_NG_SNG.Text
                .LBLTS_DN_KTS_TTL = txtLBLTS_DN_KTS_TTL.Text
                .LBLTS_DN_KTS_NDNSN_RPH = txtLBLTS_DN_KTS_NDNSN_RPH.Text
                .LBLTS_DN_KTS_MT_NG_SNG = txtLBLTS_DN_KTS_MT_NG_SNG.Text

                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP1100SaveAdd(oParameter)
        ElseIf Me.Process = "EDIT" Then
            With oParameter

                .ID = txtid.Text
                .KS_TTL = txtKS_TTL.Text
                .KS_NDNSN_RPH = txtKS_NDNSN_RPH.Text
                .KS_MT_NG_SNG = txtKS_MT_NG_SNG.Text
                .GR_PD_BNK_DLM_NGR_TTL = txtGR_PD_BNK_DLM_NGR_TTL.Text
                .GR_PD_BNK_DLM_NGR_NDNSN_RPH = txtGR_PD_BNK_DLM_NGR_NDNSN_RPH.Text
                .GR_PD_BNK_DLM_NGR_MT_NG_SNG = txtGR_PD_BNK_DLM_NGR_MT_NG_SNG.Text
                .SMPNN_LNNY_PD_BNK_DLM_NGR_TTL = txtSMPNN_LNNY_PD_BNK_DLM_NGR_TTL.Text
                .SMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH = txtSMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH.Text
                .SMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG = txtSMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG.Text
                .SMPNN_PD_BNK_DLM_NGR_TTL = txtSMPNN_PD_BNK_DLM_NGR_TTL.Text
                .SMPNN_PD_BNK_DLM_NGR_NDNSN_RPH = txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH.Text
                .SMPNN_PD_BNK_DLM_NGR_MT_NG_SNG = txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG.Text
                .GR_PD_BNK_LR_NGR_TTL = txtGR_PD_BNK_LR_NGR_TTL.Text
                .GR_PD_BNK_LR_NGR_NDNSN_RPH = txtGR_PD_BNK_LR_NGR_NDNSN_RPH.Text
                .GR_PD_BNK_LR_NGR_MT_NG_SNG = txtGR_PD_BNK_LR_NGR_MT_NG_SNG.Text
                .SMPNN_LNNY_PD_BNK_LR_NGR_TTL = txtSMPNN_LNNY_PD_BNK_LR_NGR_TTL.Text
                .SMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH = txtSMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH.Text
                .SMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG = txtSMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG.Text
                .SMPNN_PD_BNK_LR_NGR_TTL = txtSMPNN_PD_BNK_LR_NGR_TTL.Text
                .SMPNN_PD_BNK_LR_NGR_NDNSN_RPH = txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH.Text
                .SMPNN_PD_BNK_LR_NGR_MT_NG_SNG = txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG.Text
                .KS_DN_STR_KS_TTL = txtKS_DN_STR_KS_TTL.Text
                .KS_DN_STR_KS_NDNSN_RPH = txtKS_DN_STR_KS_NDNSN_RPH.Text
                .KS_DN_STR_KS_MT_NG_SNG = txtKS_DN_STR_KS_MT_NG_SNG.Text
                .ST_TGHN_DRVTF_TTL = txtST_TGHN_DRVTF_TTL.Text
                .ST_TGHN_DRVTF_NDNSN_RPH = txtST_TGHN_DRVTF_NDNSN_RPH.Text
                .ST_TGHN_DRVTF_MT_NG_SNG = txtST_TGHN_DRVTF_MT_NG_SNG.Text
                .NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL = txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL.Text
                .NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH = txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH.Text
                .NVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG = txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG.Text
                .PTNG_PMBYN_NVSTS__PKK_TTL = txtPTNG_PMBYN_NVSTS__PKK_TTL.Text
                .PTNG_PMBYN_NVSTS__PKK_NDNSN_RPH = txtPTNG_PMBYN_NVSTS__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_NVSTS__PKK_MT_NG_SNG = txtPTNG_PMBYN_NVSTS__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_NVSTS_TTL = txtCDNGN_PTNG_PMBYN_NVSTS_TTL.Text
                .CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text
                .PTNG_PMBYN_NVSTS__NT_TTL = txtPTNG_PMBYN_NVSTS__NT_TTL.Text
                .PTNG_PMBYN_NVSTS__NT_NDNSN_RPH = txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_NVSTS__NT_MT_NG_SNG = txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_MDL_KRJ__PKK_TTL = txtPTNG_PMBYN_MDL_KRJ__PKK_TTL.Text
                .PTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH = txtPTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG = txtPTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_MDL_KRJ_TTL = txtCDNGN_PTNG_MDL_KRJ_TTL.Text
                .CDNGN_PTNG_MDL_KRJ_NDNSN_RPH = txtCDNGN_PTNG_MDL_KRJ_NDNSN_RPH.Text
                .CDNGN_PTNG_MDL_KRJ_MT_NG_SNG = txtCDNGN_PTNG_MDL_KRJ_MT_NG_SNG.Text
                .PTNG_PMBYN_MDL_KRJ__NT_TTL = txtPTNG_PMBYN_MDL_KRJ__NT_TTL.Text
                .PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH = txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG = txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_MLTGN__PKK_TTL = txtPTNG_PMBYN_MLTGN__PKK_TTL.Text
                .PTNG_PMBYN_MLTGN__PKK_NDNSN_RPH = txtPTNG_PMBYN_MLTGN__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_MLTGN__PKK_MT_NG_SNG = txtPTNG_PMBYN_MLTGN__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_MLTGN_TTL = txtCDNGN_PTNG_PMBYN_MLTGN_TTL.Text
                .CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text
                .PTNG_PMBYN_MLTGN__NT_TTL = txtPTNG_PMBYN_MLTGN__NT_TTL.Text
                .PTNG_PMBYN_MLTGN__NT_NDNSN_RPH = txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_MLTGN__NT_MT_NG_SNG = txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL = txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text
                .CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG = txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_KNVNSNL__NT_TTL = txtPTNG_PMBYN_KNVNSNL__NT_TTL.Text
                .PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH = txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG = txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL = txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text
                .CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG = txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL = txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text
                .CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG = txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text
                .CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL = txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text
                .CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH = txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text
                .CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG = txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG = txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text
                .PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL = txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL.Text
                .PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH = txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text
                .PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG = txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text
                .PTNG_PMBYN__NT_TTL = txtPTNG_PMBYN__NT_TTL.Text
                .PTNG_PMBYN__NT_NDNSN_RPH = txtPTNG_PMBYN__NT_NDNSN_RPH.Text
                .PTNG_PMBYN__NT_MT_NG_SNG = txtPTNG_PMBYN__NT_MT_NG_SNG.Text
                .PNYRTN_MDL_PD_BNK_TTL = txtPNYRTN_MDL_PD_BNK_TTL.Text
                .PNYRTN_MDL_PD_BNK_NDNSN_RPH = txtPNYRTN_MDL_PD_BNK_NDNSN_RPH.Text
                .PNYRTN_MDL_PD_BNK_MT_NG_SNG = txtPNYRTN_MDL_PD_BNK_MT_NG_SNG.Text
                .PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL = txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL.Text
                .PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH = txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text
                .PNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG = txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text
                .PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL = txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL.Text
                .PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH = txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text
                .PNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG = txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text
                .PNYRTN_MDL_TTL = txtPNYRTN_MDL_TTL.Text
                .PNYRTN_MDL_NDNSN_RPH = txtPNYRTN_MDL_NDNSN_RPH.Text
                .PNYRTN_MDL_MT_NG_SNG = txtPNYRTN_MDL_MT_NG_SNG.Text
                .NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL = txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL.Text
                .NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH = txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH.Text
                .NVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG = txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG.Text
                .ST_YNG_DSWPRSKN_TTL = txtST_YNG_DSWPRSKN_TTL.Text
                .ST_YNG_DSWPRSKN_NDNSN_RPH = txtST_YNG_DSWPRSKN_NDNSN_RPH.Text
                .ST_YNG_DSWPRSKN_MT_NG_SNG = txtST_YNG_DSWPRSKN_MT_NG_SNG.Text
                .KMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL = txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL.Text
                .KMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH = txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH.Text
                .KMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG = txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG.Text
                .ST_YNG_DSWPRSKN__NT_TTL = txtST_YNG_DSWPRSKN__NT_TTL.Text
                .ST_YNG_DSWPRSKN__NT_NDNSN_RPH = txtST_YNG_DSWPRSKN__NT_NDNSN_RPH.Text
                .ST_YNG_DSWPRSKN__NT_MT_NG_SNG = txtST_YNG_DSWPRSKN__NT_MT_NG_SNG.Text
                .ST_TTP_DN_NVNTRS__TTL = txtST_TTP_DN_NVNTRS__TTL.Text
                .ST_TTP_DN_NVNTRS__NDNSN_RPH = txtST_TTP_DN_NVNTRS__NDNSN_RPH.Text
                .ST_TTP_DN_NVNTRS__MT_NG_SNG = txtST_TTP_DN_NVNTRS__MT_NG_SNG.Text
                .KMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL = txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text
                .KMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH = txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text
                .KMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG = txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text
                .ST_TTP_DN_NVNTRS__NT_TTL = txtST_TTP_DN_NVNTRS__NT_TTL.Text
                .ST_TTP_DN_NVNTRS__NT_NDNSN_RPH = txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH.Text
                .ST_TTP_DN_NVNTRS__NT_MT_NG_SNG = txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG.Text
                .ST_PJK_TNGGHN_TTL = txtST_PJK_TNGGHN_TTL.Text
                .ST_PJK_TNGGHN_NDNSN_RPH = txtST_PJK_TNGGHN_NDNSN_RPH.Text
                .ST_PJK_TNGGHN_MT_NG_SNG = txtST_PJK_TNGGHN_MT_NG_SNG.Text
                .RPRP_ST_TTL = txtRPRP_ST_TTL.Text
                .RPRP_ST_NDNSN_RPH = txtRPRP_ST_NDNSN_RPH.Text
                .RPRP_ST_MT_NG_SNG = txtRPRP_ST_MT_NG_SNG.Text
                .ST_TTL = txtST_TTL.Text
                .ST_NDNSN_RPH = txtST_NDNSN_RPH.Text
                .ST_MT_NG_SNG = txtST_MT_NG_SNG.Text
                .LBLTS_KPD_BNK_TTL = txtLBLTS_KPD_BNK_TTL.Text
                .LBLTS_KPD_BNK_NDNSN_RPH = txtLBLTS_KPD_BNK_NDNSN_RPH.Text
                .LBLTS_KPD_BNK_MT_NG_SNG = txtLBLTS_KPD_BNK_MT_NG_SNG.Text
                .LBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL = txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL.Text
                .LBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH = txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text
                .LBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG = txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text
                .LBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL = txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL.Text
                .LBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH = txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text
                .LBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG = txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text
                .LBLTS_SGR_LNNY_TTL = txtLBLTS_SGR_LNNY_TTL.Text
                .LBLTS_SGR_LNNY_NDNSN_RPH = txtLBLTS_SGR_LNNY_NDNSN_RPH.Text
                .LBLTS_SGR_LNNY_MT_NG_SNG = txtLBLTS_SGR_LNNY_MT_NG_SNG.Text
                .LBLTS_SGR_TTL = txtLBLTS_SGR_TTL.Text
                .LBLTS_SGR_NDNSN_RPH = txtLBLTS_SGR_NDNSN_RPH.Text
                .LBLTS_SGR_MT_NG_SNG = txtLBLTS_SGR_MT_NG_SNG.Text
                .LBLTS_DRVTF_TTL = txtLBLTS_DRVTF_TTL.Text
                .LBLTS_DRVTF_NDNSN_RPH = txtLBLTS_DRVTF_NDNSN_RPH.Text
                .LBLTS_DRVTF_MT_NG_SNG = txtLBLTS_DRVTF_MT_NG_SNG.Text
                .TNG_PJK_TTL = txtTNG_PJK_TTL.Text
                .TNG_PJK_NDNSN_RPH = txtTNG_PJK_NDNSN_RPH.Text
                .TNG_PJK_MT_NG_SNG = txtTNG_PJK_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL = txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL = txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL.Text
                .PNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DLM_NGR_TTL = txtPNDNN_YNG_DTRM_DLM_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL = txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_LNNY_LR_NGR_TTL = txtPNDNN_YNG_DTRM_LNNY_LR_NGR_TTL.Text
                .PNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_LR_NGR_TTL = txtPNDNN_YNG_DTRM_LR_NGR_TTL.Text
                .PNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH = txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG = txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG.Text
                .PNDNN_YNG_DTRM_TTL = txtPNDNN_YNG_DTRM_TTL.Text
                .PNDNN_YNG_DTRM_NDNSN_RPH = txtPNDNN_YNG_DTRM_NDNSN_RPH.Text
                .PNDNN_YNG_DTRM_MT_NG_SNG = txtPNDNN_YNG_DTRM_MT_NG_SNG.Text
                .SRT_BRHRG_YNG_DTRBTKN_TTL = txtSRT_BRHRG_YNG_DTRBTKN_TTL.Text
                .SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH = txtSRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text
                .SRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG = txtSRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text
                .LBLTS_PJK_TNGGHN_TTL = txtLBLTS_PJK_TNGGHN_TTL.Text
                .LBLTS_PJK_TNGGHN_NDNSN_RPH = txtLBLTS_PJK_TNGGHN_NDNSN_RPH.Text
                .LBLTS_PJK_TNGGHN_MT_NG_SNG = txtLBLTS_PJK_TNGGHN_MT_NG_SNG.Text
                .PNJMN_SBRDNS_DLM_NGR_TTL = txtPNJMN_SBRDNS_DLM_NGR_TTL.Text
                .PNJMN_SBRDNS_DLM_NGR_NDNSN_RPH = txtPNJMN_SBRDNS_DLM_NGR_NDNSN_RPH.Text
                .PNJMN_SBRDNS_DLM_NGR_MT_NG_SNG = txtPNJMN_SBRDNS_DLM_NGR_MT_NG_SNG.Text
                .PNJMN_SBRDNS_LR_NGR_TTL = txtPNJMN_SBRDNS_LR_NGR_TTL.Text
                .PNJMN_SBRDNS_LR_NGR_NDNSN_RPH = txtPNJMN_SBRDNS_LR_NGR_NDNSN_RPH.Text
                .PNJMN_SBRDNS_LR_NGR_MT_NG_SNG = txtPNJMN_SBRDNS_LR_NGR_MT_NG_SNG.Text
                .PNJMN_SBRDNS_TTL = txtPNJMN_SBRDNS_TTL.Text
                .PNJMN_SBRDNS_NDNSN_RPH = txtPNJMN_SBRDNS_NDNSN_RPH.Text
                .PNJMN_SBRDNS_MT_NG_SNG = txtPNJMN_SBRDNS_MT_NG_SNG.Text
                .RPRP_LBLTS_TTL = txtRPRP_LBLTS_TTL.Text
                .RPRP_LBLTS_NDNSN_RPH = txtRPRP_LBLTS_NDNSN_RPH.Text
                .RPRP_LBLTS_MT_NG_SNG = txtRPRP_LBLTS_MT_NG_SNG.Text
                .MDL_DSR_TTL = txtMDL_DSR_TTL.Text
                .MDL_DSR_NDNSN_RPH = txtMDL_DSR_NDNSN_RPH.Text
                .MDL_DSR_MT_NG_SNG = txtMDL_DSR_MT_NG_SNG.Text
                .MDL_YNG_BLM_DSTR_TTL = txtMDL_YNG_BLM_DSTR_TTL.Text
                .MDL_YNG_BLM_DSTR_NDNSN_RPH = txtMDL_YNG_BLM_DSTR_NDNSN_RPH.Text
                .MDL_YNG_BLM_DSTR_MT_NG_SNG = txtMDL_YNG_BLM_DSTR_MT_NG_SNG.Text
                .MDL_DSTR_TTL = txtMDL_DSTR_TTL.Text
                .MDL_DSTR_NDNSN_RPH = txtMDL_DSTR_NDNSN_RPH.Text
                .MDL_DSTR_MT_NG_SNG = txtMDL_DSTR_MT_NG_SNG.Text
                .SMPNN_PKK_TTL = txtSMPNN_PKK_TTL.Text
                .SMPNN_PKK_NDNSN_RPH = txtSMPNN_PKK_NDNSN_RPH.Text
                .SMPNN_PKK_MT_NG_SNG = txtSMPNN_PKK_MT_NG_SNG.Text
                .SMPNN_WJB_TTL = txtSMPNN_WJB_TTL.Text
                .SMPNN_WJB_NDNSN_RPH = txtSMPNN_WJB_NDNSN_RPH.Text
                .SMPNN_WJB_MT_NG_SNG = txtSMPNN_WJB_MT_NG_SNG.Text
                .SMPNN_PKK_DN_SMPNN_WJB_TTL = txtSMPNN_PKK_DN_SMPNN_WJB_TTL.Text
                .SMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH = txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH.Text
                .SMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG = txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG.Text
                .G_TTL = txtG_TTL.Text
                .G_NDNSN_RPH = txtG_NDNSN_RPH.Text
                .G_MT_NG_SNG = txtG_MT_NG_SNG.Text
                .DSG_TTL = txtDSG_TTL.Text
                .DSG_NDNSN_RPH = txtDSG_NDNSN_RPH.Text
                .DSG_MT_NG_SNG = txtDSG_MT_NG_SNG.Text
                .MDL_SHM_DPRLH_KMBL_TTL = txtMDL_SHM_DPRLH_KMBL_TTL.Text
                .MDL_SHM_DPRLH_KMBL_NDNSN_RPH = txtMDL_SHM_DPRLH_KMBL_NDNSN_RPH.Text
                .MDL_SHM_DPRLH_KMBL_MT_NG_SNG = txtMDL_SHM_DPRLH_KMBL_MT_NG_SNG.Text
                .BY_MS_FK_KTS_TTL = txtBY_MS_FK_KTS_TTL.Text
                .BY_MS_FK_KTS_NDNSN_RPH = txtBY_MS_FK_KTS_NDNSN_RPH.Text
                .BY_MS_FK_KTS_MT_NG_SNG = txtBY_MS_FK_KTS_MT_NG_SNG.Text
                .MDL_HBH_TTL = txtMDL_HBH_TTL.Text
                .MDL_HBH_NDNSN_RPH = txtMDL_HBH_NDNSN_RPH.Text
                .MDL_HBH_MT_NG_SNG = txtMDL_HBH_MT_NG_SNG.Text
                .TMBHN_MDL_DSTR_LNNY_TTL = txtTMBHN_MDL_DSTR_LNNY_TTL.Text
                .TMBHN_MDL_DSTR_LNNY_NDNSN_RPH = txtTMBHN_MDL_DSTR_LNNY_NDNSN_RPH.Text
                .TMBHN_MDL_DSTR_LNNY_MT_NG_SNG = txtTMBHN_MDL_DSTR_LNNY_MT_NG_SNG.Text
                .TMBHN_MDL_DSTR_TTL = txtTMBHN_MDL_DSTR_TTL.Text
                .TMBHN_MDL_DSTR_NDNSN_RPH = txtTMBHN_MDL_DSTR_NDNSN_RPH.Text
                .TMBHN_MDL_DSTR_MT_NG_SNG = txtTMBHN_MDL_DSTR_MT_NG_SNG.Text
                .SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL = txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL.Text
                .SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH = txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH.Text
                .SLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG = txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG.Text
                .MDL_TTL = txtMDL_TTL.Text
                .MDL_NDNSN_RPH = txtMDL_NDNSN_RPH.Text
                .MDL_MT_NG_SNG = txtMDL_MT_NG_SNG.Text
                .CDNGN_MM_TTL = txtCDNGN_MM_TTL.Text
                .CDNGN_MM_NDNSN_RPH = txtCDNGN_MM_NDNSN_RPH.Text
                .CDNGN_MM_MT_NG_SNG = txtCDNGN_MM_MT_NG_SNG.Text
                .CDNGN_TJN_TTL = txtCDNGN_TJN_TTL.Text
                .CDNGN_TJN_NDNSN_RPH = txtCDNGN_TJN_NDNSN_RPH.Text
                .CDNGN_TJN_MT_NG_SNG = txtCDNGN_TJN_MT_NG_SNG.Text
                .CDNGN_MDL_TTL = txtCDNGN_MDL_TTL.Text
                .CDNGN_MDL_NDNSN_RPH = txtCDNGN_MDL_NDNSN_RPH.Text
                .CDNGN_MDL_MT_NG_SNG = txtCDNGN_MDL_MT_NG_SNG.Text
                .SLD_LB_RG_YNG_DTHN_TTL = txtSLD_LB_RG_YNG_DTHN_TTL.Text
                .SLD_LB_RG_YNG_DTHN_NDNSN_RPH = txtSLD_LB_RG_YNG_DTHN_NDNSN_RPH.Text
                .SLD_LB_RG_YNG_DTHN_MT_NG_SNG = txtSLD_LB_RG_YNG_DTHN_MT_NG_SNG.Text
                .LB_RG_BRSH_STLH_PJK_TTL = txtLB_RG_BRSH_STLH_PJK_TTL.Text
                .LB_RG_BRSH_STLH_PJK_NDNSN_RPH = txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text
                .LB_RG_BRSH_STLH_PJK_MT_NG_SNG = txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text
                .SLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG = txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text
                .SLD_KMPNN_KTS_LNNY_TTL = txtSLD_KMPNN_KTS_LNNY_TTL.Text
                .SLD_KMPNN_KTS_LNNY_NDNSN_RPH = txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH.Text
                .SLD_KMPNN_KTS_LNNY_MT_NG_SNG = txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text
                .KNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG = txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text
                .KMPNN_KTS_LNNY_TTL = txtKMPNN_KTS_LNNY_TTL.Text
                .KMPNN_KTS_LNNY_NDNSN_RPH = txtKMPNN_KTS_LNNY_NDNSN_RPH.Text
                .KMPNN_KTS_LNNY_MT_NG_SNG = txtKMPNN_KTS_LNNY_MT_NG_SNG.Text
                .LBLTS_DN_KTS_TTL = txtLBLTS_DN_KTS_TTL.Text
                .LBLTS_DN_KTS_NDNSN_RPH = txtLBLTS_DN_KTS_NDNSN_RPH.Text
                .LBLTS_DN_KTS_MT_NG_SNG = txtLBLTS_DN_KTS_MT_NG_SNG.Text
            End With
            oParameter = oController.SIPP1100SaveEdit(oParameter)
        End If
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region

    Protected Sub txtSUM(sender As Object, e As EventArgs)
        txtGR_PD_BNK_DLM_NGR_TTL.Text = Val(txtGR_PD_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtGR_PD_BNK_DLM_NGR_MT_NG_SNG.Text)
        txtSMPNN_LNNY_PD_BNK_DLM_NGR_TTL.Text = Val(txtSMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtSMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG.Text)
        txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH.Text = Val(txtGR_PD_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtSMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH.Text)
        txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG.Text = Val(txtGR_PD_BNK_DLM_NGR_MT_NG_SNG.Text) + Val(txtSMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG.Text)
        txtSMPNN_PD_BNK_DLM_NGR_TTL.Text = Val(txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG.Text)

        txtGR_PD_BNK_LR_NGR_TTL.Text = Val(txtGR_PD_BNK_LR_NGR_NDNSN_RPH.Text) + Val(txtGR_PD_BNK_LR_NGR_MT_NG_SNG.Text)
        txtSMPNN_LNNY_PD_BNK_LR_NGR_TTL.Text = Val(txtSMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH.Text) + Val(txtSMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG.Text)
        txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH.Text = Val(txtGR_PD_BNK_LR_NGR_NDNSN_RPH.Text) + Val(txtSMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH.Text)
        txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG.Text = Val(txtGR_PD_BNK_LR_NGR_MT_NG_SNG.Text) + Val(txtSMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG.Text)
        txtSMPNN_PD_BNK_LR_NGR_TTL.Text = Val(txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH.Text) + Val(txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG.Text)

        txtKS_TTL.Text = Val(txtKS_NDNSN_RPH.Text) + Val(txtKS_MT_NG_SNG.Text)

        txtKS_DN_STR_KS_TTL.Text = Val(txtKS_DN_STR_KS_NDNSN_RPH.Text) + Val(txtKS_DN_STR_KS_MT_NG_SNG.Text)
        txtKS_DN_STR_KS_NDNSN_RPH.Text = Val(txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH.Text)
        txtKS_DN_STR_KS_MT_NG_SNG.Text = Val(txtKS_MT_NG_SNG.Text) + Val(txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG.Text) + Val(txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG.Text)



        '2. Tagihan Derivatif
        txtST_TGHN_DRVTF_TTL.Text = Val(txtST_TGHN_DRVTF_NDNSN_RPH.Text) + Val(txtST_TGHN_DRVTF_MT_NG_SNG.Text)

        '3. Investasi Jangka Pendek Dalam Surat Berharga
        txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL.Text = Val(txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH.Text) + Val(txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG.Text)

        '4. Piutang Pembiayaan - Neto 
        txtPTNG_PMBYN_NVSTS__PKK_TTL.Text = Val(txtPTNG_PMBYN_NVSTS__PKK_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_NVSTS__PKK_MT_NG_SNG.Text)
        txtCDNGN_PTNG_PMBYN_NVSTS_TTL.Text = Val(txtCDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text)
        txtPTNG_PMBYN_NVSTS__NT_TTL.Text = Val(txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_NVSTS__PKK_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text)
        txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_NVSTS__PKK_MT_NG_SNG.Text) + Val(txtCDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text)

        txtPTNG_PMBYN_MDL_KRJ__PKK_TTL.Text = Val(txtPTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG.Text)
        txtCDNGN_PTNG_MDL_KRJ_TTL.Text = Val(txtCDNGN_PTNG_MDL_KRJ_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_MDL_KRJ_MT_NG_SNG.Text)
        txtPTNG_PMBYN_MDL_KRJ__NT_TTL.Text = Val(txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_MDL_KRJ_NDNSN_RPH.Text)
        txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG.Text) + Val(txtCDNGN_PTNG_MDL_KRJ_MT_NG_SNG.Text)

        txtPTNG_PMBYN_MLTGN__PKK_TTL.Text = Val(txtPTNG_PMBYN_MLTGN__PKK_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_MLTGN__PKK_MT_NG_SNG.Text)
        txtCDNGN_PTNG_PMBYN_MLTGN_TTL.Text = Val(txtCDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text)
        txtPTNG_PMBYN_MLTGN__NT_TTL.Text = Val(txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_MLTGN__PKK_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text)
        txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_MLTGN__PKK_MT_NG_SNG.Text) + Val(txtCDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text)

        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL.Text = Val(txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG.Text)
        txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text = Val(txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text)
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL.Text = Val(txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text)
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG.Text) + Val(txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text)

        txtPTNG_PMBYN_KNVNSNL__NT_TTL.Text = Val(txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH.Text)
        txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG.Text) + Val(txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG.Text) + Val(txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG.Text) + Val(txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG.Text)

        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = Val(txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text)
        txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text = Val(txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text)
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = Val(txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text)
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text) + Val(txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text)

        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = Val(txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text)
        txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text = Val(txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text)
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = Val(txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text)
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text) + Val(txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text)

        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = Val(txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text)
        txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text = Val(txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text)
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = Val(txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text) + Val(txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text)
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text) + Val(txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text)

        txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = Val(txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text)
        txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text) + Val(txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text) + Val(txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text)

        txtPTNG_PMBYN__NT_TTL.Text = Val(txtPTNG_PMBYN__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN__NT_MT_NG_SNG.Text)
        txtPTNG_PMBYN__NT_NDNSN_RPH.Text = Val(txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text)
        txtPTNG_PMBYN__NT_MT_NG_SNG.Text = Val(txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG.Text) + Val(txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text)

        '5. Penyertaan Modal
        txtPNYRTN_MDL_PD_BNK_TTL.Text = Val(txtPNYRTN_MDL_PD_BNK_NDNSN_RPH.Text) + Val(txtPNYRTN_MDL_PD_BNK_MT_NG_SNG.Text)
        txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL.Text = Val(txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text) + Val(txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text)
        txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL.Text = Val(txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text) + Val(txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text)
        txtPNYRTN_MDL_TTL.Text = Val(txtPNYRTN_MDL_NDNSN_RPH.Text) + Val(txtPNYRTN_MDL_MT_NG_SNG.Text)
        txtPNYRTN_MDL_NDNSN_RPH.Text = Val(txtPNYRTN_MDL_PD_BNK_NDNSN_RPH.Text) + Val(txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text) + Val(txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text)
        txtPNYRTN_MDL_MT_NG_SNG.Text = Val(txtPNYRTN_MDL_PD_BNK_MT_NG_SNG.Text) + Val(txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text) + Val(txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text)

        '6. Investasi Jangka Panjang Dalam Surat Berharga 
        txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL.Text = Val(txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH.Text) + Val(txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG.Text)

        '7. Aset yang Disewa operasikan – Neto 
        txtST_YNG_DSWPRSKN_TTL.Text = Val(txtST_YNG_DSWPRSKN_NDNSN_RPH.Text) + Val(txtST_YNG_DSWPRSKN_MT_NG_SNG.Text)
        txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL.Text = Val(txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH.Text) + Val(txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG.Text)
        txtST_YNG_DSWPRSKN__NT_TTL.Text = Val(txtST_YNG_DSWPRSKN__NT_NDNSN_RPH.Text) + Val(txtST_YNG_DSWPRSKN__NT_MT_NG_SNG.Text)
        txtST_YNG_DSWPRSKN__NT_NDNSN_RPH.Text = Val(txtST_YNG_DSWPRSKN_NDNSN_RPH.Text) + Val(txtST_YNG_DSWPRSKN_NDNSN_RPH.Text)
        txtST_YNG_DSWPRSKN__NT_MT_NG_SNG.Text = Val(txtST_YNG_DSWPRSKN_MT_NG_SNG.Text) + Val(txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG.Text)

        '8. Aset Tetap dan Inventaris – Neto
        txtST_TTP_DN_NVNTRS__TTL.Text = Val(txtST_TTP_DN_NVNTRS__NDNSN_RPH.Text) + Val(txtST_TTP_DN_NVNTRS__MT_NG_SNG.Text)
        txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text = Val(txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text) + Val(txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text)
        txtST_TTP_DN_NVNTRS__NT_TTL.Text = Val(txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH.Text) + Val(txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG.Text)
        txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH.Text = Val(txtST_TTP_DN_NVNTRS__NDNSN_RPH.Text) + Val(txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text)
        txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG.Text = Val(txtST_TTP_DN_NVNTRS__MT_NG_SNG.Text) + Val(txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text)

        '9. Aset Pajak Tangguhan 
        txtST_PJK_TNGGHN_TTL.Text = Val(txtST_PJK_TNGGHN_NDNSN_RPH.Text) + Val(txtST_PJK_TNGGHN_MT_NG_SNG.Text)

        '10. Rupa-rupa Aset
        txtRPRP_ST_TTL.Text = Val(txtRPRP_ST_NDNSN_RPH.Text) + Val(txtRPRP_ST_MT_NG_SNG.Text)

        'TOTAL ASET
        txtST_TTL.Text = Val(txtST_NDNSN_RPH.Text) + Val(txtST_MT_NG_SNG.Text)
        txtST_NDNSN_RPH.Text = Val(txtKS_DN_STR_KS_NDNSN_RPH.Text) + Val(txtST_TGHN_DRVTF_NDNSN_RPH.Text) + Val(txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH.Text) + Val(txtPTNG_PMBYN__NT_NDNSN_RPH.Text) + Val(txtPNYRTN_MDL_NDNSN_RPH.Text) + Val(txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH.Text) + Val(txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text) + Val(txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH.Text) + Val(txtRPRP_ST_NDNSN_RPH.Text) + Val(txtST_NDNSN_RPH.Text)
        txtST_MT_NG_SNG.Text = Val(txtKS_DN_STR_KS_MT_NG_SNG.Text) + Val(txtST_TGHN_DRVTF_MT_NG_SNG.Text) + Val(txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG.Text) + Val(txtPTNG_PMBYN__NT_MT_NG_SNG.Text) + Val(txtPNYRTN_MDL_MT_NG_SNG.Text) + Val(txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG.Text) + Val(txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text) + Val(txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG.Text) + Val(txtRPRP_ST_MT_NG_SNG.Text) + Val(txtST_MT_NG_SNG.Text)


        'LIABILITAS DAN EKUITAS   

        'Liabilitas Segera 
        txtLBLTS_KPD_BNK_TTL.Text = Val(txtLBLTS_KPD_BNK_NDNSN_RPH.Text) + Val(txtLBLTS_KPD_BNK_MT_NG_SNG.Text)
        txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL.Text = Val(txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text) + Val(txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text)
        txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL.Text = Val(txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text) + Val(txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text)
        txtLBLTS_SGR_LNNY_TTL.Text = Val(txtLBLTS_SGR_LNNY_NDNSN_RPH.Text) + Val(txtLBLTS_SGR_LNNY_MT_NG_SNG.Text)
        txtLBLTS_SGR_TTL.Text = Val(txtLBLTS_SGR_NDNSN_RPH.Text) + Val(txtLBLTS_SGR_MT_NG_SNG.Text)
        txtLBLTS_SGR_NDNSN_RPH.Text = Val(txtLBLTS_KPD_BNK_NDNSN_RPH.Text) + Val(txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text) + Val(txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text) + Val(txtLBLTS_SGR_LNNY_NDNSN_RPH.Text)
        txtLBLTS_SGR_MT_NG_SNG.Text = Val(txtLBLTS_KPD_BNK_MT_NG_SNG.Text) + Val(txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text) + Val(txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text) + Val(txtLBLTS_SGR_LNNY_MT_NG_SNG.Text)

        'LIABILITAS Derivatif
        txtLBLTS_DRVTF_TTL.Text = Val(txtLBLTS_DRVTF_NDNSN_RPH.Text) + Val(txtLBLTS_DRVTF_MT_NG_SNG.Text)

        ' Utang Pajak 
        txtTNG_PJK_TTL.Text = Val(txtTNG_PJK_NDNSN_RPH.Text) + Val(txtTNG_PJK_MT_NG_SNG.Text)

        'Pinjaman Yang Diterima
        txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL.Text = Val(txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL.Text = Val(txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL.Text = Val(txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_DLM_NGR_TTL.Text = Val(txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH.Text = Val(txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH.Text)
        txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG.Text = Val(txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG.Text) + Val(txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG.Text) + Val(txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG.Text)

        txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL.Text = Val(txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL.Text = Val(txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_LNNY_LR_NGR_TTL.Text = Val(txtPNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_LR_NGR_TTL.Text = Val(txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH.Text = Val(txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH.Text)
        txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG.Text = Val(txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG.Text) + Val(txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG.Text) + Val(txtPNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG.Text)

        txtPNDNN_YNG_DTRM_TTL.Text = Val(txtPNDNN_YNG_DTRM_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_MT_NG_SNG.Text)
        txtPNDNN_YNG_DTRM_NDNSN_RPH.Text = Val(txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH.Text)
        txtPNDNN_YNG_DTRM_MT_NG_SNG.Text = Val(txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG.Text) + Val(txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG.Text)

        'Surat Berharga Yang Diterbitkan 
        txtSRT_BRHRG_YNG_DTRBTKN_TTL.Text = Val(txtSRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text) + Val(txtSRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text)

        'Liabilitas Pajak Tangguhan 
        txtLBLTS_PJK_TNGGHN_TTL.Text = Val(txtLBLTS_PJK_TNGGHN_NDNSN_RPH.Text) + Val(txtLBLTS_PJK_TNGGHN_MT_NG_SNG.Text)

        'Pinjaman Subordinasi 
        txtPNJMN_SBRDNS_DLM_NGR_TTL.Text = Val(txtPNJMN_SBRDNS_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNJMN_SBRDNS_DLM_NGR_MT_NG_SNG.Text)
        txtPNJMN_SBRDNS_LR_NGR_TTL.Text = Val(txtPNJMN_SBRDNS_LR_NGR_NDNSN_RPH.Text) + Val(txtPNJMN_SBRDNS_LR_NGR_MT_NG_SNG.Text)
        txtPNJMN_SBRDNS_TTL.Text = Val(txtPNJMN_SBRDNS_NDNSN_RPH.Text) + Val(txtPNJMN_SBRDNS_MT_NG_SNG.Text)
        txtPNJMN_SBRDNS_NDNSN_RPH.Text = Val(txtPNJMN_SBRDNS_DLM_NGR_NDNSN_RPH.Text) + Val(txtPNJMN_SBRDNS_LR_NGR_NDNSN_RPH.Text)
        txtPNJMN_SBRDNS_MT_NG_SNG.Text = Val(txtPNJMN_SBRDNS_DLM_NGR_MT_NG_SNG.Text) + Val(txtPNJMN_SBRDNS_LR_NGR_MT_NG_SNG.Text)

        'Rupa-rupa Liabilitas
        txtRPRP_LBLTS_TTL.Text = Val(txtRPRP_LBLTS_NDNSN_RPH.Text) + Val(txtRPRP_LBLTS_MT_NG_SNG.Text)

        ' Modal
        txtMDL_DSR_TTL.Text = Val(txtMDL_DSR_NDNSN_RPH.Text) + Val(txtMDL_DSR_MT_NG_SNG.Text)
        txtMDL_YNG_BLM_DSTR_TTL.Text = Val(txtMDL_YNG_BLM_DSTR_NDNSN_RPH.Text) + Val(txtMDL_YNG_BLM_DSTR_MT_NG_SNG.Text)
        txtMDL_DSTR_TTL.Text = Val(txtMDL_DSTR_NDNSN_RPH.Text) + Val(txtMDL_DSTR_MT_NG_SNG.Text)
        txtMDL_DSTR_NDNSN_RPH.Text = Val(txtMDL_DSR_NDNSN_RPH.Text) + Val(txtMDL_YNG_BLM_DSTR_NDNSN_RPH.Text)
        txtMDL_DSTR_MT_NG_SNG.Text = Val(txtMDL_DSR_MT_NG_SNG.Text) + Val(txtMDL_YNG_BLM_DSTR_MT_NG_SNG.Text)

        txtSMPNN_PKK_TTL.Text = Val(txtSMPNN_PKK_NDNSN_RPH.Text) + Val(txtSMPNN_PKK_MT_NG_SNG.Text)
        txtSMPNN_WJB_TTL.Text = Val(txtSMPNN_WJB_NDNSN_RPH.Text) + Val(txtSMPNN_WJB_MT_NG_SNG.Text)
        txtSMPNN_PKK_DN_SMPNN_WJB_TTL.Text = Val(txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH.Text) + Val(txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG.Text)
        txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH.Text = Val(txtSMPNN_PKK_NDNSN_RPH.Text) + Val(txtSMPNN_WJB_NDNSN_RPH.Text)
        txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG.Text = Val(txtSMPNN_PKK_MT_NG_SNG.Text) + Val(txtSMPNN_WJB_MT_NG_SNG.Text)

        txtG_TTL.Text = Val(txtG_NDNSN_RPH.Text) + Val(txtG_MT_NG_SNG.Text)
        txtDSG_TTL.Text = Val(txtDSG_NDNSN_RPH.Text) + Val(txtDSG_MT_NG_SNG.Text)
        txtMDL_SHM_DPRLH_KMBL_TTL.Text = Val(txtMDL_SHM_DPRLH_KMBL_NDNSN_RPH.Text) + Val(txtMDL_SHM_DPRLH_KMBL_MT_NG_SNG.Text)
        txtBY_MS_FK_KTS_TTL.Text = Val(txtBY_MS_FK_KTS_NDNSN_RPH.Text) + Val(txtBY_MS_FK_KTS_MT_NG_SNG.Text)
        txtMDL_HBH_TTL.Text = Val(txtMDL_HBH_NDNSN_RPH.Text) + Val(txtMDL_HBH_MT_NG_SNG.Text)
        txtTMBHN_MDL_DSTR_LNNY_TTL.Text = Val(txtTMBHN_MDL_DSTR_LNNY_NDNSN_RPH.Text) + Val(txtTMBHN_MDL_DSTR_LNNY_MT_NG_SNG.Text)
        txtTMBHN_MDL_DSTR_TTL.Text = Val(txtTMBHN_MDL_DSTR_NDNSN_RPH.Text) + Val(txtTMBHN_MDL_DSTR_MT_NG_SNG.Text)
        txtTMBHN_MDL_DSTR_NDNSN_RPH.Text = Val(txtG_NDNSN_RPH.Text) + Val(txtDSG_NDNSN_RPH.Text) + Val(txtMDL_SHM_DPRLH_KMBL_NDNSN_RPH.Text) + Val(txtBY_MS_FK_KTS_NDNSN_RPH.Text) + Val(txtMDL_HBH_NDNSN_RPH.Text) + Val(txtTMBHN_MDL_DSTR_LNNY_NDNSN_RPH.Text)
        txtTMBHN_MDL_DSTR_MT_NG_SNG.Text = Val(txtG_MT_NG_SNG.Text) + Val(txtDSG_MT_NG_SNG.Text) + Val(txtMDL_SHM_DPRLH_KMBL_MT_NG_SNG.Text) + Val(txtBY_MS_FK_KTS_MT_NG_SNG.Text) + Val(txtMDL_HBH_MT_NG_SNG.Text) + Val(txtTMBHN_MDL_DSTR_LNNY_MT_NG_SNG.Text)

        txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL.Text = Val(txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH.Text) + Val(txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG.Text)

        txtMDL_TTL.Text = Val(txtMDL_NDNSN_RPH.Text) + Val(txtMDL_MT_NG_SNG.Text)
        txtMDL_NDNSN_RPH.Text = Val(txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH.Text) + Val(txtTMBHN_MDL_DSTR_NDNSN_RPH.Text) + Val(txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH.Text) + Val(txtMDL_DSTR_NDNSN_RPH.Text)
        txtMDL_MT_NG_SNG.Text = Val(txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG.Text) + Val(txtTMBHN_MDL_DSTR_MT_NG_SNG.Text) + Val(txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG.Text) + Val(txtMDL_DSTR_MT_NG_SNG.Text)

        'Cadangan
        txtCDNGN_MM_TTL.Text = Val(txtCDNGN_MM_NDNSN_RPH.Text) + Val(txtCDNGN_MM_MT_NG_SNG.Text)
        txtCDNGN_TJN_TTL.Text = Val(txtCDNGN_TJN_NDNSN_RPH.Text) + Val(txtCDNGN_TJN_MT_NG_SNG.Text)
        txtCDNGN_MDL_TTL.Text = Val(txtCDNGN_MDL_NDNSN_RPH.Text) + Val(txtCDNGN_MDL_MT_NG_SNG.Text)
        txtCDNGN_MDL_NDNSN_RPH.Text = Val(txtCDNGN_MM_NDNSN_RPH.Text) + Val(txtCDNGN_TJN_NDNSN_RPH.Text)
        txtCDNGN_MDL_MT_NG_SNG.Text = Val(txtCDNGN_MM_MT_NG_SNG.Text) + Val(txtCDNGN_TJN_MT_NG_SNG.Text)

        'Saldo Laba(Rugi) Yang Ditahan 
        txtSLD_LB_RG_YNG_DTHN_TTL.Text = Val(txtSLD_LB_RG_YNG_DTHN_NDNSN_RPH.Text) + Val(txtSLD_LB_RG_YNG_DTHN_MT_NG_SNG.Text)

        'Laba(Rugi) Bersih Setelah Pajak 
        txtLB_RG_BRSH_STLH_PJK_TTL.Text = Val(txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text) + Val(txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text)

        'Komponen Ekuitas Lainnya
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text = Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text)
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text = Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text)
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text = Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text)
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text = Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text)
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text = Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text)
        txtSLD_KMPNN_KTS_LNNY_TTL.Text = Val(txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH.Text) + Val(txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG.Text)
        txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH.Text = Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text)
        txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG.Text = Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text) + Val(txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text)

        txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text = Val(txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text) + Val(txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text)

        txtKMPNN_KTS_LNNY_TTL.Text = Val(txtKMPNN_KTS_LNNY_NDNSN_RPH.Text) + Val(txtKMPNN_KTS_LNNY_MT_NG_SNG.Text)
        txtKMPNN_KTS_LNNY_NDNSN_RPH.Text = Val(txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text) + Val(txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH.Text)
        txtKMPNN_KTS_LNNY_MT_NG_SNG.Text = Val(txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text) + Val(txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG.Text)


        txtLBLTS_DN_KTS_TTL.Text = Val(txtLBLTS_DN_KTS_NDNSN_RPH.Text) + Val(txtLBLTS_DN_KTS_MT_NG_SNG.Text)
        txtLBLTS_DN_KTS_NDNSN_RPH.Text = Val(txtLBLTS_SGR_NDNSN_RPH.Text) + Val(txtLBLTS_DRVTF_NDNSN_RPH.Text) + Val(txtTNG_PJK_NDNSN_RPH.Text) + Val(txtPNDNN_YNG_DTRM_NDNSN_RPH.Text) + Val(txtLBLTS_PJK_TNGGHN_NDNSN_RPH.Text) + Val(txtPNJMN_SBRDNS_NDNSN_RPH.Text) + Val(txtRPRP_LBLTS_NDNSN_RPH.Text) + Val(txtMDL_NDNSN_RPH.Text) + Val(txtCDNGN_MDL_NDNSN_RPH.Text) + Val(txtSLD_LB_RG_YNG_DTHN_NDNSN_RPH.Text) + Val(txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text) + Val(txtKMPNN_KTS_LNNY_NDNSN_RPH.Text)
        txtLBLTS_DN_KTS_MT_NG_SNG.Text = Val(txtLBLTS_SGR_MT_NG_SNG.Text) + Val(txtLBLTS_DRVTF_MT_NG_SNG.Text) + Val(txtTNG_PJK_MT_NG_SNG.Text) + Val(txtPNDNN_YNG_DTRM_MT_NG_SNG.Text) + Val(txtLBLTS_PJK_TNGGHN_MT_NG_SNG.Text) + Val(txtPNJMN_SBRDNS_MT_NG_SNG.Text) + Val(txtRPRP_LBLTS_MT_NG_SNG.Text) + Val(txtMDL_MT_NG_SNG.Text) + Val(txtCDNGN_MDL_MT_NG_SNG.Text) + Val(txtSLD_LB_RG_YNG_DTHN_MT_NG_SNG.Text) + Val(txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text) + Val(txtKMPNN_KTS_LNNY_MT_NG_SNG.Text)

    End Sub



    Sub Clean()
        txtKS_TTL.Text = "0"
        txtKS_NDNSN_RPH.Text = "0"
        txtKS_MT_NG_SNG.Text = "0"
        txtGR_PD_BNK_DLM_NGR_TTL.Text = "0"
        txtGR_PD_BNK_DLM_NGR_NDNSN_RPH.Text = "0"
        txtGR_PD_BNK_DLM_NGR_MT_NG_SNG.Text = "0"
        txtSMPNN_LNNY_PD_BNK_DLM_NGR_TTL.Text = "0"
        txtSMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH.Text = "0"
        txtSMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG.Text = "0"
        txtSMPNN_PD_BNK_DLM_NGR_TTL.Text = "0"
        txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH.Text = "0"
        txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG.Text = "0"
        txtGR_PD_BNK_LR_NGR_TTL.Text = "0"
        txtGR_PD_BNK_LR_NGR_NDNSN_RPH.Text = "0"
        txtGR_PD_BNK_LR_NGR_MT_NG_SNG.Text = "0"
        txtSMPNN_LNNY_PD_BNK_LR_NGR_TTL.Text = "0"
        txtSMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH.Text = "0"
        txtSMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG.Text = "0"
        txtSMPNN_PD_BNK_LR_NGR_TTL.Text = "0"
        txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH.Text = "0"
        txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG.Text = "0"
        txtKS_DN_STR_KS_TTL.Text = "0"
        txtKS_DN_STR_KS_NDNSN_RPH.Text = "0"
        txtKS_DN_STR_KS_MT_NG_SNG.Text = "0"
        txtST_TGHN_DRVTF_TTL.Text = "0"
        txtST_TGHN_DRVTF_NDNSN_RPH.Text = "0"
        txtST_TGHN_DRVTF_MT_NG_SNG.Text = "0"
        txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL.Text = "0"
        txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH.Text = "0"
        txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_NVSTS__PKK_TTL.Text = "0"
        txtPTNG_PMBYN_NVSTS__PKK_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_NVSTS__PKK_MT_NG_SNG.Text = "0"
        txtCDNGN_PTNG_PMBYN_NVSTS_TTL.Text = "0"
        txtCDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH.Text = "0"
        txtCDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_NVSTS__NT_TTL.Text = "0"
        txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_MDL_KRJ__PKK_TTL.Text = "0"
        txtPTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG.Text = "0"
        txtCDNGN_PTNG_MDL_KRJ_TTL.Text = "0"
        txtCDNGN_PTNG_MDL_KRJ_NDNSN_RPH.Text = "0"
        txtCDNGN_PTNG_MDL_KRJ_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_MDL_KRJ__NT_TTL.Text = "0"
        txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_MLTGN__PKK_TTL.Text = "0"
        txtPTNG_PMBYN_MLTGN__PKK_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_MLTGN__PKK_MT_NG_SNG.Text = "0"
        txtCDNGN_PTNG_PMBYN_MLTGN_TTL.Text = "0"
        txtCDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH.Text = "0"
        txtCDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_MLTGN__NT_TTL.Text = "0"
        txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL.Text = "0"
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG.Text = "0"
        txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL.Text = "0"
        txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH.Text = "0"
        txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL.Text = "0"
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_KNVNSNL__NT_TTL.Text = "0"
        txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = "0"
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text = "0"
        txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL.Text = "0"
        txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = "0"
        txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = "0"
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = "0"
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text = "0"
        txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL.Text = "0"
        txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = "0"
        txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = "0"
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL.Text = "0"
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG.Text = "0"
        txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL.Text = "0"
        txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH.Text = "0"
        txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = "0"
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL.Text = "0"
        txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG.Text = "0"
        txtPTNG_PMBYN__NT_TTL.Text = "0"
        txtPTNG_PMBYN__NT_NDNSN_RPH.Text = "0"
        txtPTNG_PMBYN__NT_MT_NG_SNG.Text = "0"
        txtPNYRTN_MDL_PD_BNK_TTL.Text = "0"
        txtPNYRTN_MDL_PD_BNK_NDNSN_RPH.Text = "0"
        txtPNYRTN_MDL_PD_BNK_MT_NG_SNG.Text = "0"
        txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL.Text = "0"
        txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text = "0"
        txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text = "0"
        txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL.Text = "0"
        txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text = "0"
        txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text = "0"
        txtPNYRTN_MDL_TTL.Text = "0"
        txtPNYRTN_MDL_NDNSN_RPH.Text = "0"
        txtPNYRTN_MDL_MT_NG_SNG.Text = "0"
        txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL.Text = "0"
        txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH.Text = "0"
        txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG.Text = "0"
        txtST_YNG_DSWPRSKN_TTL.Text = "0"
        txtST_YNG_DSWPRSKN_NDNSN_RPH.Text = "0"
        txtST_YNG_DSWPRSKN_MT_NG_SNG.Text = "0"
        txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL.Text = "0"
        txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH.Text = "0"
        txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG.Text = "0"
        txtST_YNG_DSWPRSKN__NT_TTL.Text = "0"
        txtST_YNG_DSWPRSKN__NT_NDNSN_RPH.Text = "0"
        txtST_YNG_DSWPRSKN__NT_MT_NG_SNG.Text = "0"
        txtST_TTP_DN_NVNTRS__TTL.Text = "0"
        txtST_TTP_DN_NVNTRS__NDNSN_RPH.Text = "0"
        txtST_TTP_DN_NVNTRS__MT_NG_SNG.Text = "0"
        txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL.Text = "0"
        txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH.Text = "0"
        txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG.Text = "0"
        txtST_TTP_DN_NVNTRS__NT_TTL.Text = "0"
        txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH.Text = "0"
        txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG.Text = "0"
        txtST_PJK_TNGGHN_TTL.Text = "0"
        txtST_PJK_TNGGHN_NDNSN_RPH.Text = "0"
        txtST_PJK_TNGGHN_MT_NG_SNG.Text = "0"
        txtRPRP_ST_TTL.Text = "0"
        txtRPRP_ST_NDNSN_RPH.Text = "0"
        txtRPRP_ST_MT_NG_SNG.Text = "0"
        txtST_TTL.Text = "0"
        txtST_NDNSN_RPH.Text = "0"
        txtST_MT_NG_SNG.Text = "0"
        txtLBLTS_KPD_BNK_TTL.Text = "0"
        txtLBLTS_KPD_BNK_NDNSN_RPH.Text = "0"
        txtLBLTS_KPD_BNK_MT_NG_SNG.Text = "0"
        txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL.Text = "0"
        txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH.Text = "0"
        txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG.Text = "0"
        txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL.Text = "0"
        txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH.Text = "0"
        txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG.Text = "0"
        txtLBLTS_SGR_LNNY_TTL.Text = "0"
        txtLBLTS_SGR_LNNY_NDNSN_RPH.Text = "0"
        txtLBLTS_SGR_LNNY_MT_NG_SNG.Text = "0"
        txtLBLTS_SGR_TTL.Text = "0"
        txtLBLTS_SGR_NDNSN_RPH.Text = "0"
        txtLBLTS_SGR_MT_NG_SNG.Text = "0"
        txtLBLTS_DRVTF_TTL.Text = "0"
        txtLBLTS_DRVTF_NDNSN_RPH.Text = "0"
        txtLBLTS_DRVTF_MT_NG_SNG.Text = "0"
        txtTNG_PJK_TTL.Text = "0"
        txtTNG_PJK_NDNSN_RPH.Text = "0"
        txtTNG_PJK_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_DLM_NGR_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_LNNY_LR_NGR_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_LR_NGR_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG.Text = "0"
        txtPNDNN_YNG_DTRM_TTL.Text = "0"
        txtPNDNN_YNG_DTRM_NDNSN_RPH.Text = "0"
        txtPNDNN_YNG_DTRM_MT_NG_SNG.Text = "0"
        txtSRT_BRHRG_YNG_DTRBTKN_TTL.Text = "0"
        txtSRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH.Text = "0"
        txtSRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG.Text = "0"
        txtLBLTS_PJK_TNGGHN_TTL.Text = "0"
        txtLBLTS_PJK_TNGGHN_NDNSN_RPH.Text = "0"
        txtLBLTS_PJK_TNGGHN_MT_NG_SNG.Text = "0"
        txtPNJMN_SBRDNS_DLM_NGR_TTL.Text = "0"
        txtPNJMN_SBRDNS_DLM_NGR_NDNSN_RPH.Text = "0"
        txtPNJMN_SBRDNS_DLM_NGR_MT_NG_SNG.Text = "0"
        txtPNJMN_SBRDNS_LR_NGR_TTL.Text = "0"
        txtPNJMN_SBRDNS_LR_NGR_NDNSN_RPH.Text = "0"
        txtPNJMN_SBRDNS_LR_NGR_MT_NG_SNG.Text = "0"
        txtPNJMN_SBRDNS_TTL.Text = "0"
        txtPNJMN_SBRDNS_NDNSN_RPH.Text = "0"
        txtPNJMN_SBRDNS_MT_NG_SNG.Text = "0"
        txtRPRP_LBLTS_TTL.Text = "0"
        txtRPRP_LBLTS_NDNSN_RPH.Text = "0"
        txtRPRP_LBLTS_MT_NG_SNG.Text = "0"
        txtMDL_DSR_TTL.Text = "0"
        txtMDL_DSR_NDNSN_RPH.Text = "0"
        txtMDL_DSR_MT_NG_SNG.Text = "0"
        txtMDL_YNG_BLM_DSTR_TTL.Text = "0"
        txtMDL_YNG_BLM_DSTR_NDNSN_RPH.Text = "0"
        txtMDL_YNG_BLM_DSTR_MT_NG_SNG.Text = "0"
        txtMDL_DSTR_TTL.Text = "0"
        txtMDL_DSTR_NDNSN_RPH.Text = "0"
        txtMDL_DSTR_MT_NG_SNG.Text = "0"
        txtSMPNN_PKK_TTL.Text = "0"
        txtSMPNN_PKK_NDNSN_RPH.Text = "0"
        txtSMPNN_PKK_MT_NG_SNG.Text = "0"
        txtSMPNN_WJB_TTL.Text = "0"
        txtSMPNN_WJB_NDNSN_RPH.Text = "0"
        txtSMPNN_WJB_MT_NG_SNG.Text = "0"
        txtSMPNN_PKK_DN_SMPNN_WJB_TTL.Text = "0"
        txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH.Text = "0"
        txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG.Text = "0"
        txtG_TTL.Text = "0"
        txtG_NDNSN_RPH.Text = "0"
        txtG_MT_NG_SNG.Text = "0"
        txtDSG_TTL.Text = "0"
        txtDSG_NDNSN_RPH.Text = "0"
        txtDSG_MT_NG_SNG.Text = "0"
        txtMDL_SHM_DPRLH_KMBL_TTL.Text = "0"
        txtMDL_SHM_DPRLH_KMBL_NDNSN_RPH.Text = "0"
        txtMDL_SHM_DPRLH_KMBL_MT_NG_SNG.Text = "0"
        txtBY_MS_FK_KTS_TTL.Text = "0"
        txtBY_MS_FK_KTS_NDNSN_RPH.Text = "0"
        txtBY_MS_FK_KTS_MT_NG_SNG.Text = "0"
        txtMDL_HBH_TTL.Text = "0"
        txtMDL_HBH_NDNSN_RPH.Text = "0"
        txtMDL_HBH_MT_NG_SNG.Text = "0"
        txtTMBHN_MDL_DSTR_LNNY_TTL.Text = "0"
        txtTMBHN_MDL_DSTR_LNNY_NDNSN_RPH.Text = "0"
        txtTMBHN_MDL_DSTR_LNNY_MT_NG_SNG.Text = "0"
        txtTMBHN_MDL_DSTR_TTL.Text = "0"
        txtTMBHN_MDL_DSTR_NDNSN_RPH.Text = "0"
        txtTMBHN_MDL_DSTR_MT_NG_SNG.Text = "0"
        txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL.Text = "0"
        txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH.Text = "0"
        txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG.Text = "0"
        txtMDL_TTL.Text = "0"
        txtMDL_NDNSN_RPH.Text = "0"
        txtMDL_MT_NG_SNG.Text = "0"
        txtCDNGN_MM_TTL.Text = "0"
        txtCDNGN_MM_NDNSN_RPH.Text = "0"
        txtCDNGN_MM_MT_NG_SNG.Text = "0"
        txtCDNGN_TJN_TTL.Text = "0"
        txtCDNGN_TJN_NDNSN_RPH.Text = "0"
        txtCDNGN_TJN_MT_NG_SNG.Text = "0"
        txtCDNGN_MDL_TTL.Text = "0"
        txtCDNGN_MDL_NDNSN_RPH.Text = "0"
        txtCDNGN_MDL_MT_NG_SNG.Text = "0"
        txtSLD_LB_RG_YNG_DTHN_TTL.Text = "0"
        txtSLD_LB_RG_YNG_DTHN_NDNSN_RPH.Text = "0"
        txtSLD_LB_RG_YNG_DTHN_MT_NG_SNG.Text = "0"
        txtLB_RG_BRSH_STLH_PJK_TTL.Text = "0"
        txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH.Text = "0"
        txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH.Text = "0"
        txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG.Text = "0"
        txtSLD_KMPNN_KTS_LNNY_TTL.Text = "0"
        txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH.Text = "0"
        txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG.Text = "0"
        txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL.Text = "0"
        txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH.Text = "0"
        txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG.Text = "0"
        txtKMPNN_KTS_LNNY_TTL.Text = "0"
        txtKMPNN_KTS_LNNY_NDNSN_RPH.Text = "0"
        txtKMPNN_KTS_LNNY_MT_NG_SNG.Text = "0"
        txtLBLTS_DN_KTS_TTL.Text = "0"
        txtLBLTS_DN_KTS_NDNSN_RPH.Text = "0"
        txtLBLTS_DN_KTS_MT_NG_SNG.Text = "0"
    End Sub
End Class