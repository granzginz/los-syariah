﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP5310.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP5310"  MaintainScrollPositionOnPostback="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>LAPORAN ANALISIS KESESUAIAN ASET DAN LIABILITAS</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>  
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        $("#tabs").tabs({ selected: 1 });
    
    </script>  
    <style type="text/css">
        .form_x
        {
            text-align: left;
            width: 25%;
            float: left;
            display: block;
            padding-top: 3px;
            padding-bottom: 3px;
            padding-left: 3px;
            padding-right: 3px;
            min-height: 28px;
            border-right: 1px solid #e5e5e5;
        }
        .form_y
        {
            text-align: center;
            width: 10%;
            float: left;
            display: block;
            padding-top: 3px;
            padding-bottom: 3px;
            padding-left: 3px;
            padding-right: 3px;
            min-height: 28px;
            border-right: 1px solid #e5e5e5;
        }
        .form_z
        {
            text-align: center;
            width: 10%;
            float: left;
            display: block;
            padding-top: 3px;
            padding-bottom: 3px;
            padding-left: 3px;
            padding-right: 3px;
            min-height: 28px;
        }
        .bg_color_a
        {
            background-color: #FEFAAB;
            
        }
        .bg_color_b
        {
            background-color: #C6FEB1;
            
        }
        .bg_color_c
        {
            background-color: #A9E9F8;
            
        }
    </style>
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                   SIPP5310 - LAPORAN ANALISIS KESESUAIAN ASET DAN LIABILITAS
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">
                <%--untuk eksport--%>
                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <%--<div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>--%>
                <div class="form_box">
                        <div class="form_single">
                        <label>
                            Pilih Bulan Generate
                        </label>
                        <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                            TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>
                <%--end--%>
 <div class="form_box_title" style="margin-top: 3px;" >
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="5%" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" Visible="false">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="18%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ST_TTL_" SortExpression="ST_TTL_" HeaderText="Total Aset" >
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="LBLTS_TTL_" SortExpression="LBLTS_TTL_ " HeaderText="Total Liabilitas" >
                                    </asp:BoundColumn> 
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP5310" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <%--untuk eksport--%>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button> 
                    <%--end--%>
                </div>

                
            </asp:Panel>
        <asp:Panel ID="pnladd" runat="server">
        <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                            <b>
                                BulanData
                            </b>                               
                        </label>
                        <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                        ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

        <div class ="form_box_title">
            <div class="form_x">
                    <b>
                             
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="Label23" Text="<= 3 Bulan"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>                           
                        <asp:Label runat="server" ID="Label24" Text="< 3 Bulan & >= 6 Bulan"></asp:Label>
                   </b>
            </div>
            <div class="form_y">
                    <b> 
                        <asp:Label runat="server" ID="Label25" Text="> 6 Bulan & <= 1 Tahun"></asp:Label>
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="Label26" Text="> 1 Tahun & <= 5 Tahun"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="Label27" Text="> 5 Tahun & <= 10 Tahun"></asp:Label>  
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="Label28" Text="> 10 Tahun"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="Label29" Text="TOTAL"></asp:Label>
                    </b>
            </div>
        </div>                   

        <div class ="form_box_header">
            <div class="form_x">                   
                <div class="form_single">
                    <b>
                        <asp:Label runat="server" ID="Label22" Text="I. ASSET"></asp:Label>
                    </b> 
                </div>                                                         
            </div>
            <div class="form_y">
          
            </div>
            <div class="form_y">

            </div>
            <div class="form_y">

            </div>
            <div class="form_y">

            </div>
            <div class="form_y">

            </div>
            <div class="form_y">

            </div>
            <div class="form_z">

            </div>
        </div>

        <div class="form_box bg_color_a">
            <div class="form_x">
                    <b>                             
                        &emsp; <asp:Label runat="server" ID="Label21" Text="Piutang Pembiayaan Konvensional - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0" ></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>

        <div class="form_box bg_color_b">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; <asp:Label runat="server" ID="Label30" Text="Piutang Pembiayaan Investasi - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label31" Text="Sewa Pembiayaan (Finance Lease) - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                        <asp:TextBox ID="PTNG_PMBYN_NVSTS__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox>           
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_NVSTS__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_NVSTS__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_NVSTS__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_NVSTS__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                  <asp:TextBox ID="PTNG_PMBYN_NVSTS__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>               
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label14" Text="Jual dan Sewa Balik (Sale and Leaseback) - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox>          
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label15" Text="Anjak Piutang With Recourse - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label16" Text="Pembelian dgn Pembayaran Scr Angsuran - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label17" Text="Pembiayaan Proyek - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_PRYK__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_PRYK__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_PRYK__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_PRYK__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_PRYK__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_PRYK__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__PMBYN_PRYK__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label18" Text="Pembiayaan Infrastruktur - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_NFRSTRKTR__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_NFRSTRKTR__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_NFRSTRKTR__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_NFRSTRKTR__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_NFRSTRKTR__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__PMBYN_NFRSTRKTR__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__PMBYN_NFRSTRKTR__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label19" Text="Skema lain dengan persetujuan OJK - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label20" Text="Cadangan Piutang Pembiayaan Investasi"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2 " Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="CDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <%-- mdkj --%>

        <div class="form_box bg_color_b">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; <asp:Label runat="server" ID="Label32" Text="Piutang Pembiayaan Modal Kerja - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0" ></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>        

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label33" Text="Jual dan Sewa Balik (Sale and Leaseback) - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                        <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox>           
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                    <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__JL_DN_SW_BLK_SL_ND_LSBCK__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>               
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label35" Text="Anjak Piutang With Recourse - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox>          
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTH_RCRS__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label37" Text="Anjak Piutang Without Recourse - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__NJK_PTNG_WTHT_RCRS__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label39" Text="Fasilitas Modal Usaha - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__FSLTS_MDL_SH__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__FSLTS_MDL_SH__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__FSLTS_MDL_SH__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__FSLTS_MDL_SH__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__FSLTS_MDL_SH__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__FSLTS_MDL_SH__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__FSLTS_MDL_SH__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label41" Text="Skema lain dengan persetujuan OJK - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_MDL_KRJ__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label43" Text="Cadangan Piutang Modal Kerja"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_MDL_KRJ_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%" OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox>
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_MDL_KRJ_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_MDL_KRJ_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_MDL_KRJ_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_MDL_KRJ_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_MDL_KRJ_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="CDNGN_PTNG_MDL_KRJ_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <%-- multiguna --%>

        <div class="form_box bg_color_b">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; <asp:Label runat="server" ID="Label34" Text="Piutang Pembiayaan Multiguna - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label47" Text="Sewa Pembiayaan (Finance Lease) - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                        <asp:TextBox ID="PTNG_PMBYN_MLTGN__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox>           
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_MLTGN__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_MLTGN__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_MLTGN__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PTNG_PMBYN_MLTGN__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                  <asp:TextBox ID="PTNG_PMBYN_MLTGN__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__SW_PMBYN_FNNC_LS__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>               
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label49" Text="Pembelian dgn Pembayaran Scr Angsuran - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__PMBLN_DNGN_PMBYRN_SCR_NGSRN__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label51" Text="Skema lain dengan persetujuan OJK - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_MLTGN__SKM_LN_DNGN_PRSTJN_JK__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label53" Text="Cadangan Piutang Multiguna"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="CDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <%-- Piutang Pembiayaan Lainnya Berdasarkan Persetujuan OJK --%>

        <div class="form_box bg_color_b">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; <asp:Label runat="server" ID="Label55" Text="Piutang Pembyn Lain Brdsrkn Persetujuan OJK - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label63" Text="Piutang Pembyn Lain Brdsrkn Persetujuan OJK - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label65" Text="Cadangan Piutang Pembyn Lain Brdsrkn Perstjn OJK"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="CDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <%-- Piutang Pembiayaan Berdasarkan Prinsip Syariah --%>

        <div class="form_box bg_color_a">
            <div class="form_x">
                    <b>                             
                        &emsp; <asp:Label runat="server" ID="Label67" Text="Piutang Pembiayaan Berdasarkan Prinsip Syariah - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>

        <div class="form_box bg_color_b">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; <asp:Label runat="server" ID="Label75" Text="Piutang Pembyn Jual Beli Brdsrkn Prinsip Syariah - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label83" Text="Piutang Pembyn Jual Beli Syariah - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label85" Text="Cadngn Piutang Pembyrn Jual Beli Syariah"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="CDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box bg_color_b">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; <asp:Label runat="server" ID="Label87" Text="Piutang Pembyn Invstasi Brdsrkn Prinsip Syariah - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label95" Text="Piutang Pembyn Invstasi Syariah - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label97" Text="Cadngn Piutang Pembyn Invstasi Syariah"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="CDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box bg_color_b">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; <asp:Label runat="server" ID="Label99" Text="Piutang Pembyn Jasa Brdsrkn Prinsip Syariah - Neto"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label107" Text="Piutang Pembyn Jasa Syariah - Pokok"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box">
            <div class="form_x">
                    <b>
                       &emsp; &emsp; &emsp; <asp:Label runat="server" ID="Label109" Text="Cadngn Piutang Pembyn Jasa Syariah"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="CDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box bg_color_a">
            <div class="form_x">
                    <b>
                       &emsp; <asp:Label runat="server" ID="Label126" Text="Aset Non Pembiayaan"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="ST_NN_PMBYN_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="ST_NN_PMBYN_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="ST_NN_PMBYN_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="ST_NN_PMBYN_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="ST_NN_PMBYN_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="ST_NN_PMBYN_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="ST_NN_PMBYN_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>
        
        <div class="form_box bg_color_c">
            <div class="form_x">
                    <b>                             
                        &emsp; <asp:Label runat="server" ID="Label128" Text="Aset"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="ST_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="ST_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="ST_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="ST_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="ST_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="ST_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="ST_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>                

        <%-- LIABILITAS --%>

        <div class="form_box">
            <div class="form_single"></div>
        </div>

        <div class ="form_box_header">
            <div class="form_x">                   
                <div class="form_single">
                    <b>
                        <asp:Label runat="server" ID="Label111" Text="II. LIABILITAS"></asp:Label>
                    </b> 
                </div>                                                         
            </div>
            <div class="form_y">
          
            </div>
            <div class="form_y">

            </div>
            <div class="form_y">

            </div>
            <div class="form_y">

            </div>
            <div class="form_y">

            </div>
            <div class="form_y">

            </div>
            <div class="form_z">

            </div>
        </div>

        <div class="form_box bg_color_a">
            <div class="form_x">
                    <b>
                      &emsp; <asp:Label runat="server" ID="Label112" Text="Pendanaan Yang Diterima"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                        <asp:TextBox ID="PNDNN_YNG_DTRM_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox>           
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PNDNN_YNG_DTRM_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PNDNN_YNG_DTRM_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PNDNN_YNG_DTRM_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                       <asp:TextBox ID="PNDNN_YNG_DTRM_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                  <asp:TextBox ID="PNDNN_YNG_DTRM_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="PNDNN_YNG_DTRM_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>               
            </div>
        </div>

        <div class="form_box bg_color_a">
            <div class="form_x">
                    <b>
                       &emsp; <asp:Label runat="server" ID="Label114" Text="Surat Berharga yang Diterbitkan"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box bg_color_a">
            <div class="form_x">
                    <b>
                       &emsp; <asp:Label runat="server" ID="Label116" Text="Liabilitas Selain Pnjman dan Surat Brhrga yang Ditrbtkan"></asp:Label>
                    </b>                           
            </div>
            <div class="form_y">
                             <asp:TextBox ID="LBLTS_SLN_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="LBLTS_SLN_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="LBLTS_SLN_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="LBLTS_SLN_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="LBLTS_SLN_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_y">
                             <asp:TextBox ID="LBLTS_SLN_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_LBH_DR_10_THN" runat="server" CssClass="numberAlign2" Width="80%"    OnTextChanged="SUM" AutoPostBack="True" >0</asp:TextBox> 
            </div>
            <div class="form_z">
                    <b>
                             <asp:Label runat="server" ID="LBLTS_SLN_PNJMN_DN_SRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH_TTL" Text="0"></asp:Label>
                    </b>              
            </div>
        </div>

        <div class="form_box bg_color_c">
            <div class="form_x">
                    <b>                             
                        &emsp; <asp:Label runat="server" ID="Label118" Text="Liabilitas"></asp:Label>
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="LBLTS_NDNSN_RPH_KRNG_DR_T_SM_DNGN_3_BLN" Text="0"></asp:Label> 
                    </b>           
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="LBLTS_NDNSN_RPH_LBH_DR_3_BLN_DN_KRNG_DR_T_SM_DNGN_6_BLN" Text="0"></asp:Label> 
                   </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="LBLTS_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="LBLTS_NDNSN_RPH_LBH_DR_1_THN_DN_KRNG_DR_T_SM_DNGN_5_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="LBLTS_NDNSN_RPH_LBH_DR_5_THN_DN_KRNG_DR_T_SM_DNGN_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_y">
                    <b>
                        <asp:Label runat="server" ID="LBLTS_NDNSN_RPH_LBH_DR_10_THN" Text="0"></asp:Label> 
                    </b>
            </div>
            <div class="form_z">
                    <b>
                        <asp:Label runat="server" ID="LBLTS_NDNSN_RPH_TTL" Text="0"></asp:Label> 
                    </b>
            </div>
        </div>
        
        <div class="form_button" style="margin-bottom:3%;">
                <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                </asp:Button>
        </div>
        
        </ContentTemplate> 
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonSave" EventName="Click" />
            </Triggers>      
        </asp:UpdatePanel> 
        </asp:Panel> 
    </form>
</body>
</html>
