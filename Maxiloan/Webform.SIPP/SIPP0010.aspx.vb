﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class SIPP0010
    Inherits Webform.WebBased
    'Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New Controller.SIPP0010Controller
    Private oCustomclass As New Parameter.SIPP0010
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNavi As ucGridNav

    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNavi.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP0010"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                pnlcopybulandata.Visible = False
                Me.SearchBy = ""
                Me.CmdWhere = "ALL"
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                FillCombo()
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
            End If
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavi.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.SIPP0010

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP0010(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()
        FillCombo()
        FillCbo(cboJNS_PRZNN, 6, "", "")

        If (isFrNav = False) Then
            GridNavi.Initialize(recordCount, pageSize)
        End If

        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If

    End Sub
#End Region

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP0010", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False

            Me.Process = "ADD"
            txtNMR_ZN_SH.Enabled = True
            txtNMR_ZN_SH.Text = ""
            txtTNGGL_ZN_SH.Text = ""
            txtKeterangan.Text = ""
            txtbulandata.Text = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        txtNMR_ZN_SH.Text = ""
        txtTNGGL_ZN_SH.Text = ""
        txtKeterangan.Text = ""
        txtbulandata.Text = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    '#Region "Search"
    '    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
    '        If txtSearch.Text <> "" Then
    '            Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
    '        Else
    '            Me.SearchBy = ""
    '        End If
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '    End Sub
    '#End Region

    '#Region "Reset"
    '    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
    '        Me.SearchBy = ""
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '        txtSearch.Text = ""
    '        cboSearch.ClearSelection()
    '        pnlAdd.Visible = False
    '    End Sub
    '#End Region

#Region "dtgList ItemCommand"
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP0010
        Dim dtEntity As New DataTable
        Dim err As String
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
        Try
            If e.CommandName.Trim = "Edit" Then

                If CheckFeature(Me.Loginid, "SIPP0010", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False
                'ButtonSearch.Visible = False
                'ButtonReset.Visible = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetSIPP0010Edit(oParameter)
                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboJNS_PRZNN.SelectedIndex = cboJNS_PRZNN.Items.IndexOf(cboJNS_PRZNN.Items.FindByValue(oRow("JNS_PRZNN").ToString.Trim))
                    txtTNGGL_ZN_SH.Text = Format(oRow("TNGGL_ZN_SH"), "yyyy/MM/dd").ToString
                End If
                txtID.Text = oParameter.ListData.Rows(0)("ID")
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                txtNMR_ZN_SH.Text = oParameter.ListData.Rows(0)("NMR_ZN_SH")
                txtKeterangan.Text = oParameter.ListData.Rows(0)("KTRNGN")





            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP0010", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP0010
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With
                err = oController.SIPP0010Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#Region " SAVE "
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.SIPP0010
        Dim ErrMessage As String = ""


        If Me.Process = "ADD" Then
            With customClass
                .BULANDATA = txtbulandata.Text
                .KTRNGN = txtKeterangan.Text
                .NMR_ZN_SH = txtNMR_ZN_SH.Text
                .JNS_PRZNN = cboJNS_PRZNN.SelectedValue.Trim
                .TNGGL_ZN_SH = txtTNGGL_ZN_SH.Text

                .strConnection = GetConnectionString()
            End With
            oController.SIPP0010Add(customClass)
        ElseIf Me.Process = "EDIT" Then
            With customClass
                .ID = txtID.Text
                .BULANDATA = txtbulandata.Text
                .KTRNGN = txtKeterangan.Text
                .NMR_ZN_SH = txtNMR_ZN_SH.Text
                .JNS_PRZNN = cboJNS_PRZNN.SelectedValue.Trim
                .TNGGL_ZN_SH = txtTNGGL_ZN_SH.Text

                .strConnection = GetConnectionString()
            End With
            oController.SIPP0010Update(customClass)
        End If
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Update data berhasil.....", False)
        End If

    End Sub
#End Region


#Region " dropdown "
    Private Sub FillCombo()
        'Dim strCombo As String
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP0010
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.valuemaster = valuemaster
        oAssetData.valueparent = valueparent
        oAssetData.valueelement = valueelement
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        With cboJNS_PRZNN
            .DataSource = oData
            .DataTextField = "ElementLabel"
            .DataValueField = "Element"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
            'If oData.Rows.Count = 1 Then
            '    .Items.FindByValue(Me.ID).Selected = True
            'End If
        End With
    End Sub
#End Region

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP0010
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_001020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0010Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP0010
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "24_Informasi_Profil_Perusahaan_Pembiayaan_TB_001020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP0010) As Parameter.SIPP0010
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0010Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP0010
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

End Class
