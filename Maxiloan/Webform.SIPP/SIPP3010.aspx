﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP3010.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP3010" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>SIPP3010</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>    
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                   SIPP3010 - RINCIAN INSTRUMEN DERIVATIF UNTUK LINDUNG NILAI
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">
                <%--untuk eksport--%>
                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>
                <%--end--%>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="10%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/iconDelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" Visible="false"></asp:BoundColumn> 
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="15%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NMR_KNTRK" SortExpression="NMR_KNTRK" HeaderText="Nomor Kontrak">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NMNL" SortExpression="NMNL" HeaderText="Nominal">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NMR_KNTRK_LNDNG_NL" SortExpression="NMR_KNTRK_LNDNG_NL" HeaderText="Nomor Kontrak Lindung Nilai">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TNGGL_TMP" SortExpression="TNGGL_TMP" HeaderText="Tanggal Jatuh Tempo" DataFormatString="{0:yyyy/MM/dd}">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNavi" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <%--untuk eksport--%>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button> 
                    <%--end--%>
                </div>

                
            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
            
<%--        <div class ="form_title">
            <div class ="form_single">
                <h4>
                    SIPP3010 - RINCIAN INSTRUMEN DERIVATIF UNTUK LINDUNG NILAI
                </h4>
            </div>
        </div>--%>
                <div class="form_box"> 
                        <asp:TextBox ID="txtID" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>  
                </div>
                <%--untuk eksport--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--end--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Kontrak
                        </label>
                        <asp:TextBox ID="txtNMR_KNTRK" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNMR_KNTRK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nominal
                        </label>
                        <asp:TextBox ID="txtNMNL" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNMNL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Kontrak Lindung Nilai
                        </label>
                        <asp:TextBox ID="txtNMR_KNTRK_LNDNG_NL" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNMR_KNTRK_LNDNG_NL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis
                        </label>
                        <asp:DropDownList ID="cboJNS" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="cboJNS" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Mulai
                        </label>
                        <asp:TextBox ID="txtTNGGL_ML" runat="server" CssClass="medium_text" Width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True"
                            TargetControlID="txtTNGGL_ML" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTNGGL_ML" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Jatuh Tempo
                        </label>
                        <asp:TextBox ID="txtTNGGL_TMP" runat="server" CssClass="medium_text" Width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                            TargetControlID="txtTNGGL_TMP" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTNGGL_TMP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nominal Dalam Nilai Mata Uang Asal
                        </label>
                        <asp:TextBox ID="txtNL_MT_NG_SL" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNL_MT_NG_SL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nominal Dalam Ekuivalen Rupiah
                        </label>
                        <asp:TextBox ID="txtNL_KVLN_RPH" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNL_KVLN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Rincian Counter Party
                        </label>
                        <asp:TextBox ID="txtNM" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNM" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Golongan Rincian Counter Party
                        </label>
                        <asp:DropDownList ID="cboGLNGN" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                            ControlToValidate="cboGLNGN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Asal Negara Counter Party
                        </label>
                        <asp:DropDownList ID="cboSL_NGR" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ControlToValidate="cboSL_NGR" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>
