﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP0035.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP0035" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Rincian Kepengurusan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        } 
    </script>   
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                   SIPP0035 - RINCIAN KEPENGURUSAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

             <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="2%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULANDATA" ItemStyle-Width="20%">
                                    </asp:BoundColumn> 
                                    <asp:TemplateColumn HeaderText="NAMA PENGURUS" SortExpression="ProductCode">
                                        <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                        <asp:Label ID="lblpengurus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NM_PNGRS") %>'>
                                         </asp:Label></a>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                                    </asp:BoundColumn>    
                                    <%--<asp:BoundColumn DataField="NM_PNGRS" SortExpression="NM_PNGRS" HeaderText="NAMA PENGURUS">
                                    </asp:BoundColumn>--%>                                    
                                    <asp:BoundColumn DataField="JBTN_KPNGRSN" SortExpression="JBTN_KPNGRSN" HeaderText="JABATAN" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TNGGL_ML" SortExpression="TNGGL_ML" HeaderText="TANGGAL MULAI MENJABAT" ItemStyle-Width="20%" DataFormatString="{0:yyyy/MM/dd}">
                                    </asp:BoundColumn>            
                                    
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP0035" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>                               
                </div>

               <%-- <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI RINCIAN KEPENGURUSAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NM_PNGRS">Nama Pengurus</asp:ListItem>
                            <asp:ListItem Value="JBTN_KPNGRSN">Jabatan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>

                 <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            EXPORT TO EXCEL
                        </h4>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>
                            <asp:TextBox ID="txtGetBulanData" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtGetBulanData" Format="MMyyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                    </div>              
                </div>
                <div class="form_button">
                     <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                </div>--%>
            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>  
                
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                             
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama
                        </label>
                        <asp:TextBox ID="txtnama" runat="server" CssClass="medium_text" MaxLength="35"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnama" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kewarganegaraan
                        </label>
                         <asp:DropDownList ID="cbokwg" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ControlToValidate="cbokwg" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                  <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jabatan
                        </label>
                        <asp:DropDownList ID="cboJabatan" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                            ControlToValidate="cboJabatan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Lokasi Dati II
                        </label>
                         <asp:DropDownList ID="cbodati" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                            ControlToValidate="cbodati" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
               <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Mulai Menjabat
                        </label>
                        <asp:TextBox ID="txttglmulai" runat="server" CssClass="medium_text" Width="7%" ></asp:TextBox>
                         <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True" TargetControlID="txttglmulai" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                            ControlToValidate="txttglmulai" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Surat Keputusan
                        </label>
                        <asp:TextBox ID="txtnosurat" runat="server" CssClass="medium_text" MaxLenght="15"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnosurat" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Surat Keputusan
                        </label>
                        <asp:TextBox ID="txttglkeputusan" runat="server" CssClass="medium_text" Width="7%" ></asp:TextBox>
                         <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txttglkeputusan" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txttglkeputusan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div> 

               
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>
