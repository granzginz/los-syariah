﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP2600.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP2600" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Rincian Surat Berharga Yang Diterbitkan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>   
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SIPP2600 - RINCIAN SURAT BERHARGA YANG DITERBITKAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                
                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle CssClass="command_col" Width="2%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE" >
                                        <ItemStyle CssClass="command_col" Width="5%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="10%">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="NOSURATBERHARGA" SortExpression="NOSURATBERHARGA" HeaderText="NOMOR SURAT BERHARGA" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JENISSURATBERHARGA" SortExpression="JENISSURATBERHARGA" HeaderText="JENIS SURAT BERHARGA" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TGLMULAI" SortExpression="TGLMULAI" HeaderText="TANGGAL MULAI" DataFormatString="{0:yyyy/MM/dd}" ItemStyle-Width="10%">
                                    </asp:BoundColumn>            
                                    <asp:BoundColumn DataField="TGJATUHTEMPO" SortExpression="TGJATUHTEMPO" HeaderText="TANGGAL JATUH TEMPO" DataFormatString="{0:yyyy/MM/dd}" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JENISSUKUBUNGA" SortExpression="JENISSUKUBUNGA" HeaderText="JENIS SUKU BUNGA" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TINGKATSUKUBUNGA" SortExpression="TINGKATSUKUBUNGA" HeaderText="TINGKAT SUKU BUNGA" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SALDOPINJAMANRUPIAH" SortExpression="SALDOPINJAMANRUPIAH" HeaderText="SALDO PINJAMAN (Rp)" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NAMAKREDITUR" SortExpression="NAMAKREDITUR" HeaderText="Nama Ba'i/Mu'ajjir" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP2600" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                     <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                </div>

<%--                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI RINCIAN SURAT BERHARGA YANG DITERBITKAN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NAMAKREDITUR">Nama Kreditur</asp:ListItem>
                            <asp:ListItem Value="SALDOPINJAMANRUPIAH">Saldo Pinjaman</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>--%>
            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>
            
                    
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Surat Berharga
                        </label>
                        <asp:TextBox ID="txtnosurat" runat="server" CssClass="medium_text" MaxLength="35"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnosurat" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Surat Berharga
                        </label>
                        <asp:DropDownList ID="cbojnssurat" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ControlToValidate="cbojnssurat" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Mulai
                        </label>
                        <asp:TextBox ID="txttglmulai" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txttglmulai" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*" 
                            ControlToValidate="txttglmulai" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                  <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Jatuh Tempo
                        </label>
                        <asp:TextBox ID="txttgljatuhtempo" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalenderExtender" runat="server" Enabled="True" TargetControlID="txttgljatuhtempo" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*" 
                            ControlToValidate="txttgljatuhtempo" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Suku Bunga
                        </label>
                         <asp:DropDownList ID="cbojnssuku" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                            ControlToValidate="cbojnssuku" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tingkat Suku Bunga
                        </label>
                        <asp:TextBox ID="txttingkatsukubunga" runat="server" CssClass="medium_text" Width="5%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ControlToValidate="txttingkatsukubunga" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>                        
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nilai Nominal
                        </label>
                        <asp:TextBox ID="txtnilainominal" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Masukan Nilai Nominal" ValidationExpression="[0-9]"
                            ControlToValidate="txtnilainominal" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" 
                        ControlToValidate="txtnilainominal" ErrorMessage="Input Harus Berupa Angka" Display="Dynamic" CssClass="validator_general"  />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Valuta
                        </label>
                         <asp:DropDownList ID="cbovaluta" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="cbovaluta" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Saldo Pinjaman Dalam Nilai Mata Uang Asal
                        </label>
                        <asp:TextBox ID="txtsaldopinjamanvalas" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Masukan Saldo" ValidationExpression="[0-9]"
                            ControlToValidate="txtsaldopinjamanvalas" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" 
                        ControlToValidate="txtsaldopinjamanvalas" ErrorMessage="Input Harus Berupa Angka" Display="Dynamic" CssClass="validator_general"  />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Saldo Pinjaman Dalam Ekuivalen Rupiah
                        </label>
                        <asp:TextBox ID="txtsaldopinjamanrp" runat="server" CssClass="medium_text" MaxLenght="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Masukan Saldo" ValidationExpression="[0-9]"
                            ControlToValidate="txtsaldopinjamanrp" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Integer" 
                        ControlToValidate="txtsaldopinjamanrp" ErrorMessage="Input Harus Berupa Angka" Display="Dynamic" CssClass="validator_general"  />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Ba'i/Mu'ajjir
                        </label>
                        <asp:TextBox ID="txtnamakreditur" runat="server" CssClass="medium_text" MaxLenght="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtnamakreditur" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Golongan Ba'i/Mu'ajjir
                        </label>
                        <asp:DropDownList ID="cboGolkreditur" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="cboGolkreditur" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Lokasi Negara
                        </label>
                        <asp:DropDownList ID="cboNegara" runat="server" Width ="20%" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="cboNegara" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

               
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>