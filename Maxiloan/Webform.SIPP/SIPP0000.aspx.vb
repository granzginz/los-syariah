﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
#End Region

Public Class SIPP0000
    Inherits Maxiloan.Webform.WebBased
    Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New SIPP0000Controller
    Private oCustomclass As New Parameter.SIPP0000
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNavi2 As ucGridNav

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNavi2.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP0000"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                pnlcopybulandata.Visible = False
                Me.SearchBy = ""
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)

                FillCombo()
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
            End If
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavi2.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Maxiloan.Parameter.SIPP0000

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP0000(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()

        FillCombo()
        FillCbo(cboStatus, 1, "", "")
        FillCbo(cbobntkbdnusaha, 2, "", "")
        FillCbo(cbokegsyariah, 4, "", "")
        FillCbo(cbodati, "", "", 1)
        FillCbo(cbosttskepemlkngdg, 5, "", "")



        If (isFrNav = False) Then
            GridNavi2.Initialize(recordCount, pageSize)
        End If

        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
    End Sub
    Private Sub FillCombo()
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub

    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP0000
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP0000
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Valuemaster = valuemaster
        oAssetData.Valueparent = valueparent
        oAssetData.Valueelement = valueelement
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ElementLabel"
        cboName.DataValueField = "Element"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP0000", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            txtbulandata.Enabled = True

            Me.Process = "ADD"
            clean()
            'DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    '#Region "Search"
    '    Private Sub ButtonSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSearch.Click
    '        If txtSearch.Text <> "" Then
    '            Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & txtSearch.Text.Trim & "%'"
    '        Else
    '            Me.SearchBy = ""
    '        End If
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '    End Sub
    '#End Region

    '#Region "Reset"
    '    Private Sub imbReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonReset.Click
    '        Me.SearchBy = ""
    '        Me.SortBy = ""
    '        DoBind(Me.SearchBy, Me.SortBy)
    '        txtSearch.Text = ""
    '        cboSearch.ClearSelection()
    '        pnlAdd.Visible = False
    '    End Sub
    '#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP0000
        Dim dtEntity As New DataTable
        Dim err As String

        Try
            If e.CommandName = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP0000", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                txtbulandata.Enabled = False

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.SIPP0000Edit(oParameter)

                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If


                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cbosttskepemlkngdg.SelectedIndex = cbosttskepemlkngdg.Items.IndexOf(cbosttskepemlkngdg.Items.FindByValue(oRow("STTS_KPMLKN_GDNG").ToString.Trim))
                    cboStatus.SelectedIndex = cboStatus.Items.IndexOf(cboStatus.Items.FindByValue(oRow("STTS_KPMLKN_PRSHN").ToString.Trim))
                    cbobntkbdnusaha.SelectedIndex = cbobntkbdnusaha.Items.IndexOf(cbobntkbdnusaha.Items.FindByValue(oRow("BNTK_BDN_SH_PRSHN_PMBYN").ToString.Trim))
                    cbokegsyariah.SelectedIndex = cbokegsyariah.Items.IndexOf(cbokegsyariah.Items.FindByValue(oRow("STTS_KGTN_SH_SYRH").ToString.Trim))
                    cbodati.SelectedIndex = cbodati.Items.IndexOf(cbodati.Items.FindByValue(oRow("LKS_DT__KNTR").ToString.Trim))
                    txttglpendirian.Text = Format(oRow("TNGGL_PNDRN"), "yyyy/MM/dd")
                End If

                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                txtid.Text = oParameter.ListData.Rows(0)("ID")
                txtnmlngkp.Text = oParameter.ListData.Rows(0)("NM_LNGKP_PRSHN")
                txtnmsbtn.Text = oParameter.ListData.Rows(0)("NM_SBTN_PRSHN")
                txtnpwp.Text = oParameter.ListData.Rows(0)("NPWP")
                txtalmt.Text = oParameter.ListData.Rows(0)("LMT_LNGKP")
                txtkdpos.Text = oParameter.ListData.Rows(0)("KD_PS")
                txtnotelp.Text = oParameter.ListData.Rows(0)("NMR_TLPN")
                txtfax.Text = oParameter.ListData.Rows(0)("NMR_FKSML")
                txtjmlkntrcbg.Text = oParameter.ListData.Rows(0)("JMLH_KNTR_CBNG")
                txtjmlkntrslncbg.Text = oParameter.ListData.Rows(0)("JMLH_JRNGN_KNTR_SLN_KNTR_CBNG")
                txtjmltngkrjpst.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_PST")
                txtjmltngkrjcbg.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_CBNG")
                txtjmltngkrjslncbg.Text = oParameter.ListData.Rows(0)("JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG")
                txtnmptgslap.Text = oParameter.ListData.Rows(0)("NM_PTGS_PNYSN_LPRN")
                txtbagian.Text = oParameter.ListData.Rows(0)("BGNDVS_PTGS_PNYSN_LPRN")
                txttelpptgslap.Text = oParameter.ListData.Rows(0)("NMR_TLPN_PTGS_PNYSN_LPRN")
                txtfaxptgslap.Text = oParameter.ListData.Rows(0)("NMR_FKSML_PTGS_PNYSN_LPRN")
                txtnmdirlap.Text = oParameter.ListData.Rows(0)("NM_PNNGGNG_JWB_LPRN")
                txtjabatan.Text = oParameter.ListData.Rows(0)("BGNDVS_PNNGGNG_JWB_LPRN")
                txtteldirlap.Text = oParameter.ListData.Rows(0)("NMR_TLPN_PNNGGNG_JWB_LPRN")
                txtfaxdirlap.Text = oParameter.ListData.Rows(0)("NMR_FKSML_PNNGGNG_JWB_LPRN")



                lblinvest.Text = oParameter.ListData.Rows(0)("BDNG_SH_PRSHN_PMBYN")
                lblmodal.Text = oParameter.ListData.Rows(0)("BDNG_SH_PRSHN_PMBYN_1")
                lblmulti.Text = oParameter.ListData.Rows(0)("BDNG_SH_PRSHN_PMBYN_2")
                lbllainnya.Text = oParameter.ListData.Rows(0)("BDNG_SH_PRSHN_PMBYN_3")
                lblsyariah.Text = oParameter.ListData.Rows(0)("BDNG_SH_PRSHN_PMBYN_4")
                lbljualbeli.Text = oParameter.ListData.Rows(0)("BDNG_SH_PRSHN_PMBYN_5")
                lbljasa.Text = oParameter.ListData.Rows(0)("BDNG_SH_PRSHN_PMBYN_6")
                getcbo()

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP0000", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP0000
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With

                err = oController.SIPP0000Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
    Sub clean()
        txtbulandata.Text = ""
        txtid.Text = ""
        txtnmlngkp.Text = ""
        txtnmsbtn.Text = ""
        txtnpwp.Text = ""
        txtalmt.Text = ""
        txtkdpos.Text = ""
        txtnotelp.Text = ""
        txtfax.Text = ""
        txtjmlkntrcbg.Text = ""
        txtjmlkntrslncbg.Text = ""
        txtjmltngkrjpst.Text = ""
        txtjmltngkrjcbg.Text = ""
        txtjmltngkrjslncbg.Text = ""
        txtnmptgslap.Text = ""
        txtbagian.Text = ""
        txttelpptgslap.Text = ""
        txtfaxptgslap.Text = ""
        txtnmdirlap.Text = ""
        txtjabatan.Text = ""
        txtteldirlap.Text = ""
        txtfaxdirlap.Text = ""
        txttglpendirian.Text = ""

        lblinvest.Text = ""
        lbljasa.Text = ""
        lbljualbeli.Text = ""
        lbllainnya.Text = ""
        lblmodal.Text = ""
        lblmulti.Text = ""
        lblsyariah.Text = ""
        getcbo()
    End Sub
    Sub lblhide()
        lblinvest.Visible = False
        lbljasa.Visible = False
        lbljualbeli.Visible = False
        lbllainnya.Visible = False
        lblmodal.Visible = False
        lblmulti.Visible = False
        lblsyariah.Visible = False
    End Sub
    Sub getcbo()
        If lblinvest.Text = "e1" Then
            cboinvest.Checked = True
        Else
            cboinvest.Checked = False
        End If
        If lblmodal.Text = "e2" Then
            cbomodal.Checked = True
        Else
            cbomodal.Checked = False
        End If
        If lblmulti.Text = "e3" Then
            cbomulti.Checked = True
        Else
            cbomulti.Checked = False
        End If
        If lbllainnya.Text = "e4" Then
            cbolainnya.Checked = True
        Else
            cbolainnya.Checked = False
        End If
        If lblsyariah.Text = "e5" Then
            cbosyariah.Checked = True
        Else
            cbosyariah.Checked = False
        End If
        If lbljualbeli.Text = "e6" Then
            cbojualbeli.Checked = True
        Else
            cbojualbeli.Checked = False
        End If
        If lbljasa.Text = "e7" Then
            cbojasa.Checked = True
        Else
            cbojasa.Checked = False
        End If

        lblhide()
    End Sub

    Sub isicbo()
        If cboinvest.Checked = True Then
            lblinvest.Text = "e1"
        Else
            lblinvest.Text = ""
        End If

        If cbomodal.Checked = True Then
            lblmodal.Text = "e2"
        Else
            lblmodal.Text = ""
        End If

        If cbomulti.Checked = True Then
            lblmulti.Text = "e3"
        Else
            lblmulti.Text = ""
        End If

        If cbolainnya.Checked = True Then
            lbllainnya.Text = "e4"
        Else
            lbllainnya.Text = ""
        End If

        If cbosyariah.Checked = True Then
            lblsyariah.Text = "e5"
        Else
            lblsyariah.Text = ""
        End If

        If cbojualbeli.Checked = True Then
            lbljualbeli.Text = "e6"
        Else
            lbljualbeli.Text = ""
        End If

        If cbojasa.Checked = True Then
            lbljasa.Text = "e7"
        Else
            lbljasa.Text = ""
        End If

        lblhide()
    End Sub

#Region "Save"
    Private Sub ButtonSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim oParameter As New Parameter.SIPP0000
        Dim errMessage As String = ""

        If Me.Process = "ADD" Then
            isicbo()
            With oParameter
                .BULANDATA = txtbulandata.Text
                .NM_LNGKP_PRSHN = txtnmlngkp.Text
                .NM_SBTN_PRSHN = txtnmsbtn.Text
                .NPWP = txtnpwp.Text
                .STTS_KPMLKN_PRSHN = cboStatus.Text
                .BNTK_BDN_SH_PRSHN_PMBYN = cbobntkbdnusaha.Text
                .STTS_KGTN_SH_SYRH = cbokegsyariah.Text
                .TNGGL_PNDRN = txttglpendirian.Text
                .BDNG_SH_PRSHN_PMBYN = lblinvest.Text
                .BDNG_SH_PRSHN_PMBYN_1 = lblmodal.Text
                .BDNG_SH_PRSHN_PMBYN_2 = lblmulti.Text
                .BDNG_SH_PRSHN_PMBYN_3 = lbllainnya.Text
                .BDNG_SH_PRSHN_PMBYN_4 = lblsyariah.Text
                .BDNG_SH_PRSHN_PMBYN_5 = lbljualbeli.Text
                .BDNG_SH_PRSHN_PMBYN_6 = lbljasa.Text
                .LMT_LNGKP = txtalmt.Text
                .LKS_DT__KNTR = cbodati.Text
                .KD_PS = txtkdpos.Text
                .STTS_KPMLKN_GDNG = cbosttskepemlkngdg.Text
                .NMR_TLPN = txtnotelp.Text
                .NMR_FKSML = txtfax.Text
                .JMLH_KNTR_CBNG = txtjmlkntrcbg.Text
                .JMLH_JRNGN_KNTR_SLN_KNTR_CBNG = txtjmlkntrslncbg.Text
                .JMLH_TNG_KRJ_KNTR_PST = txtjmltngkrjpst.Text
                .JMLH_TNG_KRJ_KNTR_CBNG = txtjmltngkrjcbg.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG = txtjmltngkrjslncbg.Text
                .NM_PTGS_PNYSN_LPRN = txtnmptgslap.Text
                .BGNDVS_PTGS_PNYSN_LPRN = txtbagian.Text
                .NMR_TLPN_PTGS_PNYSN_LPRN = txttelpptgslap.Text
                .NMR_FKSML_PTGS_PNYSN_LPRN = txtfaxptgslap.Text
                .NM_PNNGGNG_JWB_LPRN = txtnmdirlap.Text
                .BGNDVS_PNNGGNG_JWB_LPRN = txtjabatan.Text
                .NMR_TLPN_PNNGGNG_JWB_LPRN = txtteldirlap.Text
                .NMR_FKSML_PNNGGNG_JWB_LPRN = txtfaxdirlap.Text
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP0000Add(oParameter)
        ElseIf Me.Process = "EDIT" Then
            isicbo()
            With oParameter
                .ID = txtid.Text
                .BULANDATA = txtbulandata.Text
                .NM_LNGKP_PRSHN = txtnmlngkp.Text
                .NM_SBTN_PRSHN = txtnmsbtn.Text
                .NPWP = txtnpwp.Text
                .STTS_KPMLKN_PRSHN = cboStatus.Text
                .BNTK_BDN_SH_PRSHN_PMBYN = cbobntkbdnusaha.Text
                .STTS_KGTN_SH_SYRH = cbokegsyariah.Text
                .TNGGL_PNDRN = txttglpendirian.Text
                .BDNG_SH_PRSHN_PMBYN = lblinvest.Text
                .BDNG_SH_PRSHN_PMBYN_1 = lblmodal.Text
                .BDNG_SH_PRSHN_PMBYN_2 = lblmulti.Text
                .BDNG_SH_PRSHN_PMBYN_3 = lbllainnya.Text
                .BDNG_SH_PRSHN_PMBYN_4 = lblsyariah.Text
                .BDNG_SH_PRSHN_PMBYN_5 = lbljualbeli.Text
                .BDNG_SH_PRSHN_PMBYN_6 = lbljasa.Text
                .LMT_LNGKP = txtalmt.Text
                .LKS_DT__KNTR = cbodati.Text
                .KD_PS = txtkdpos.Text
                .STTS_KPMLKN_GDNG = cbosttskepemlkngdg.Text
                .NMR_TLPN = txtnotelp.Text
                .NMR_FKSML = txtfax.Text
                .JMLH_KNTR_CBNG = txtjmlkntrcbg.Text
                .JMLH_JRNGN_KNTR_SLN_KNTR_CBNG = txtjmlkntrslncbg.Text
                .JMLH_TNG_KRJ_KNTR_PST = txtjmltngkrjpst.Text
                .JMLH_TNG_KRJ_KNTR_CBNG = txtjmltngkrjcbg.Text
                .JMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG = txtjmltngkrjslncbg.Text
                .NM_PTGS_PNYSN_LPRN = txtnmptgslap.Text
                .BGNDVS_PTGS_PNYSN_LPRN = txtbagian.Text
                .NMR_TLPN_PTGS_PNYSN_LPRN = txttelpptgslap.Text
                .NMR_FKSML_PTGS_PNYSN_LPRN = txtfaxptgslap.Text
                .NM_PNNGGNG_JWB_LPRN = txtnmdirlap.Text
                .BGNDVS_PNNGGNG_JWB_LPRN = txtjabatan.Text
                .NMR_TLPN_PNNGGNG_JWB_LPRN = txtteldirlap.Text
                .NMR_FKSML_PNNGGNG_JWB_LPRN = txtfaxdirlap.Text
                .strConnection = GetConnectionString()
            End With
            oParameter = oController.SIPP0000Save(oParameter)
        End If

        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Updated data berhasil .....", False)
        End If
    End Sub
#End Region

#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()


            Dim ws = wb.Worksheets.Add(dtdata, "TB_000020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using

    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0000Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP0000
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "24_Informasi_Profil_Perusahaan_Pembiayaan_TB_000020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP0000) As Parameter.SIPP0000
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0000Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP0000
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
End Class