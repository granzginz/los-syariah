﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP0043.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP0043" MaintainScrollPositionOnPostback="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Rincian Tenaga Kerja Berdasarkan Fungsi</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        } 
    </script> 
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                   SIPP0043 - RINCIAN TENAGA KERJA BERDASARKAN FUNGSI
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="2%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="2%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:TemplateColumn HeaderText="JUMLAH TENAGA KERJA TOTAL" SortExpression="ProductCode">
                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                        <asp:Label ID="lblpengurus" runat="server" Text='<%= DataBinder.Eval(Container, "DataItem.JMLH_TNG_KRJ_TTL__") %>'>
                                         </asp:Label></a>
                                    </ItemTemplate>
                                    </asp:TemplateColumn>--%>
                                     <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULANDATA" ItemStyle-Width="5%">
                                    </asp:BoundColumn>  
                                     <asp:BoundColumn DataField="JMLH_TNG_KRJ_TTL__" SortExpression="JMLH_TNG_KRJ_TTL__" HeaderText="JUMLAH TENAGA KERJA TOTAL" ItemStyle-Width="5%">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_PMSRN_TTL_" SortExpression="JMLH_TNG_KRJ_PMSRN_TTL_" HeaderText="PEMASARAN" ItemStyle-Width="5%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_PRSNL_TTL_" SortExpression="JMLH_TNG_KRJ_PRSNL_TTL_" HeaderText="OPERASIONAL" ItemStyle-Width="5%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_PNGHN_TTL_" SortExpression="JMLH_TNG_KRJ_PNGHN_TTL_" HeaderText="PENAGIHAN" ItemStyle-Width="5%">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_" SortExpression="JMLH_TNG_KRJ_HMN_RSRC_HR_DN_GNRL_FFR_G_TTL_" HeaderText="HR & GA" ItemStyle-Width="5%">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_" SortExpression="JMLH_TNG_KRJ_KNGN_DN_KNTNS_TTL_" HeaderText="KEUANGAN DAN AKUNTANSI" ItemStyle-Width="5%">
                                    </asp:BoundColumn>    
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_MNJMN_RSK_TTL_" SortExpression="JMLH_TNG_KRJ_MNJMN_RSK_TTL_" HeaderText="MANAJEMEN RISIKO" ItemStyle-Width="5%">
                                    </asp:BoundColumn>      
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_DT_NTRNL_TTL_" SortExpression="JMLH_TNG_KRJ_DT_NTRNL_TTL_" HeaderText="AUDIT INTERNAL" ItemStyle-Width="5%">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_LGL_TTL_" SortExpression="JMLH_TNG_KRJ_LGL_TTL_" HeaderText="LEGAL" ItemStyle-Width="5%">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_" SortExpression="JMLH_TNG_KRJ_TKNLG_NFRMS_T_TTL_" HeaderText="IT" ItemStyle-Width="5%">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_FNGS_LNNY_TTL_" SortExpression="JMLH_TNG_KRJ_FNGS_LNNY_TTL_" HeaderText="SATUAN KERJA LAINNYA" ItemStyle-Width="5%">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                                    </asp:BoundColumn>                     
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP0043" runat="server"/>
                        </div>
                    </div>
                </div>
                
                
                    <div class="form_button">
                        <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue"></asp:Button>
                        <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                    </div>
               <%-- 
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            EXPORT TO EXCEL
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>
                            <asp:TextBox ID="txtGetBulanData" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtGetBulanData" Format="MMyyyy" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                    </div>              
                </div>
                <div class="form_button">
                     <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                </div>--%>
                

                <%--<div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI RINCIAN TENAGA KERJA BERDASARKAN FUNGSI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NM_PNGRS">Nama Pengurus</asp:ListItem>
                            <asp:ListItem Value="JBTN_KPNGRSN">Jabatan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>--%>

            </asp:Panel>
            



            <asp:Panel ID="pnlAdd" runat="server">
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>

                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>PEMASARAN</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label1" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label2" runat="server" Text="Staf dan Lainnya"></asp:Label>                           
                        </b> 
                    </div>
                </div>


      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJPemasaran" runat="server" CssClass="clsPemasaran clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"   >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJPemasaran" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    
                        <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJPemasaran" runat="server" CssClass="clsPemasaran clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"   >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJPemasaran" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    
                        <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJPemasaran" runat="server" CssClass="clsPemasaran clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"   >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJPemasaran" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>


                    <div class="form_right">
                        <label class="label_req">

                            Jumlah Tenaga Kerja Tetap Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLPemasaran" runat="server" CssClass="clsPemasaran clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"   >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLPemasaran" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        
                         <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLPemasaran" runat="server" CssClass="clsPemasaran clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"   >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLPemasaran" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        
                         <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLPemasaran" runat="server" CssClass="clsPemasaran clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"   >0</asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLPemasaran" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                     
                </div>

                 <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                            <b>Total Tenaga Kerja Pemasaran  :</b>
                        </label>
                        <b><asp:Label ID="lbltotalPemasaran" runat="server" Text="0"></asp:Label></b> 
                        <asp:HiddenField ID="HidtotalPemasaran" runat="server" Value="0"/>                       
                    </div>
                </div>


                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>OPERASIONAL</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label3" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label4" runat="server" Text="Staf dan Lainnya"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJOperasional" runat="server" CssClass="clsOperasional clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJOperasional" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJOperasional" runat="server" CssClass="clsOperasional  clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJOperasional" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJOperasional" runat="server" CssClass="clsOperasional clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJOperasional" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>

                     <div class="form_right">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLOperasional" runat="server" CssClass="clsOperasional clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"   >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLOperasional" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLOperasional" runat="server" CssClass="clsOperasional clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLOperasional" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Pemasaran
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLOperasional" runat="server" CssClass="clsOperasional  clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLOperasional" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                            <b>Total Tenaga Kerja Operasional :</b>
                        </label>
                        <b><asp:Label ID="lbltotalOperasional" runat="server" Text="">0</asp:Label></b> 
                        <asp:HiddenField ID="HidtotalOperasional" runat="server" Value="0"/>
                    </div>
                </div>        

                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>PENAGIHAN</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label8" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label9" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Penagihan
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJPenagihan" runat="server" CssClass="clsPenagihan clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJPenagihan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Penagihan
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJPenagihan" runat="server" CssClass="clsPenagihan clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJPenagihan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Penagihan
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJPenagihan" runat="server" CssClass="clsPenagihan clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJPenagihan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>

                     <div class="form_right">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Penagihan
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLPenagihan" runat="server" CssClass="clsPenagihan clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLPenagihan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Penagihan
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLPenagihan" runat="server" CssClass="clsPenagihan clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLPenagihan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Penagihan
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLPenagihan" runat="server" CssClass="clsPenagihan clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLPenagihan" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                            <b>Total Tenaga Kerja Penagihan  :</b>
                        </label>
                        <b><asp:Label ID="lblTotalPenagihan" runat="server" Text="">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalPenagihan" runat="server" Value="0"/>
                    </div>
                </div>

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>HUMAN RESOURCE (HR) dan GENERAL AFFAIR (GA)</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label5" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label6" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap HR dan GA
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJHrGa" runat="server" CssClass="clsHR clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJHrGa" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak HR dan GA
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJHrGa" runat="server" CssClass="clsHR clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJHrGa" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing HR dan GA
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJHrGa" runat="server" CssClass="clsHR clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJHrGa" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>

                     <div class="form_right">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap HR dan GA
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLHrGa" runat="server" CssClass="clsHR clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLHrGa" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak HR dan GA
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLHrGa" runat="server" CssClass="clsHR clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLHrGa" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing HR dan GA
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLHrGa" runat="server" CssClass="clsHR clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLHrGa" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                            <b>Total Tenaga Kerja HR dan GA  :</b>
                        </label>
                        <b><asp:Label ID="lblTotalHrGa" runat="server" Text="">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalHrGa" runat="server" Value="0"/>
                    </div>
                </div>

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>KEUANGAN dan AKUNTANSI</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label7" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label10" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Keuangan dan Akuntansi
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJAkn" runat="server" CssClass="clsAkuntansi clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJAkn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Keuangan dan Akuntansi
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJAkn" runat="server" CssClass="clsAkuntansi clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJAkn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Keuangan dan Akuntansi
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJAkn" runat="server" CssClass="clsAkuntansi clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJAkn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>

                     <div class="form_right">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Keuangan dan Akuntansi
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLAkn" runat="server" CssClass="clsAkuntansi clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLAkn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Keuangan dan Akuntansi
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLAkn" runat="server" CssClass="clsAkuntansi clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLAkn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Keuangan dan Akuntansi
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLAkn" runat="server" CssClass="clsAkuntansi clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLAkn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <%--<label class="medium_text">
                            <b>Total Tenaga Kerja Keuangan dan Akuntansi  :</b>
                        </label>--%>
                        <b><asp:Label ID="Label23" runat="server" Text="Total Tenaga Kerja Keuangan dan Akuntansi  :"></asp:Label></b>
                        <b><asp:Label ID="lblTotalAkn" runat="server" Text="">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalAkn" runat="server" Value="0"/>
                    </div>
                </div>

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>MANAJEMEN RISIKO</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label11" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label12" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Manajeman Risiko
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJRsk" runat="server" CssClass="clsMnjRisiko clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJRsk" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Manajeman Risiko
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJRsk" runat="server" CssClass="clsMnjRisiko clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJRsk" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Manajeman Risiko
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJRsk" runat="server" CssClass="clsMnjRisiko clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJRsk" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>

                     <div class="form_right">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Manajeman Risiko
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLRsk" runat="server" CssClass="clsMnjRisiko clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLRsk" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Manajeman Risiko
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLRsk" runat="server" CssClass="clsMnjRisiko clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLRsk" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Manajeman Risiko
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLRsk" runat="server" CssClass="clsMnjRisiko clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLRsk" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                       <%-- <label class="medium_text">
                            <b>Total Tenaga Kerja Manajeman Risiko  :</b>
                        </label>--%>
                        <b><asp:Label ID="Label24" runat="server" Text="Total Tenaga Kerja Manajeman Risiko  :"></asp:Label></b>
                        <b><asp:Label ID="lblTotalRsk" runat="server" Text="">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalRsk" runat="server" Value="0"/>
                    </div>
                </div>

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>AUDIT INTERNAL</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label13" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label14" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Audit Internal
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJAdt" runat="server" CssClass="clsAudit clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJAdt" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Audit Internal
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJAdt" runat="server" CssClass="clsAudit clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJAdt" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Audit Internal
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJAdt" runat="server" CssClass="clsAudit clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator46" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJAdt" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>

                     <div class="form_right">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Audit Internal
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLAdt" runat="server" CssClass="clsAudit clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator47" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLAdt" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Audit Internal
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLAdt" runat="server" CssClass="clsAudit clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator48" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLAdt" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Audit Internal
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLAdt" runat="server" CssClass="clsAudit clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator49" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLAdt" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                            <b>Total Tenaga Kerja Audit Internal  :</b>
                        </label>
                        <b><asp:Label ID="lblTotalAudit" runat="server" Text="">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalAudit" runat="server" Value="0"/>
                    </div>
                </div>

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>LEGAL</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label16" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label17" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Legal
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJLegal" runat="server" CssClass="clsLegal clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator50" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJLegal" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Legal
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJLegal" runat="server" CssClass="clsLegal clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator51" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJLegal" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Legal
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJLegal" runat="server" CssClass="clsLegal clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJLegal" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>

                     <div class="form_right">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Legal
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLLegal" runat="server" CssClass="clsLegal clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator53" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLLegal" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Legal
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLLegal" runat="server" CssClass="clsLegal clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator54" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLLegal" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Legal
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLLegal" runat="server" CssClass="clsLegal clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLLegal" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                            <b>Total Tenaga Kerja Legal  :</b>
                        </label>
                        <b><asp:Label ID="lalTotalLegal" runat="server" Text="">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalLegal" runat="server" Value="0"/>
                    </div>
                </div>


                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>TEKNOLOGI INFORMASI (IT)</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label18" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label19" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap IT
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJIT" runat="server" CssClass="clsTI clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJIT" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak IT
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJIT" runat="server" CssClass="clsTI clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJIT" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing IT
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJIT" runat="server" CssClass="clsTI clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator58" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJIT" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
            
                     <div class="form_right">
                         <label class="label_req">
                            Jumlah Tenaga Kerja Tetap IT
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLIT" runat="server" CssClass="clsTI clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLIT" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                 
                        <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak IT
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLIT" runat="server" CssClass="clsTI clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"   >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLIT" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing IT
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLIT" runat="server" CssClass="clsTI clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLIT" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                            <b>Total Tenaga Kerja IT  :</b>
                        </label>
                        <b><asp:Label ID="lblTotalIT" runat="server" Text="">0</asp:Label></b> <br />
                        <asp:HiddenField ID="HidTotalIT" runat="server" Value="0"/>
                    </div>
                </div>
               

                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>SATUAN KERJA LAINNYA</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label20" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label21" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Satuan Kerja Lainnya
                        </label>
                         <asp:TextBox ID="txttngkrjttpMNJLn" runat="server" CssClass="clsLainnya clsTetapDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator62" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpMNJLn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Satuan Kerja Lainnya
                        </label>
                         <asp:TextBox ID="txttngkrjktrMNJLn" runat="server" CssClass="clsLainnya clsKontrakDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator63" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrMNJLn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Satuan Kerja Lainnya
                        </label>
                         <asp:TextBox ID="txttngkrjoutMNJLn" runat="server" CssClass="clsLainnya clsOutsourceDir clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator64" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutMNJLn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
               
                     <div class="form_right">
                        <label class="label_req">
                            Jumlah Tenaga Kerja Tetap Satuan Kerja Lainnya
                        </label>
                         <asp:TextBox ID="txttngkrjttpSLLn" runat="server" CssClass="clsLainnya clsTetapLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator65" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjttpSLLn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Kontrak Satuan Kerja Lainnya
                        </label>
                         <asp:TextBox ID="txttngkrjktrSLLn" runat="server" CssClass="clsLainnya clsKontrakLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjktrSLLn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jumlah Tenaga Kerja Outsourcing Satuan Kerja Lainnya
                        </label>
                         <asp:TextBox ID="txttngkrjoutSLLn" runat="server" CssClass="clsLainnya clsOutsourceLain clsTOTAL" Width="5%" AutoPostBack="false"  >0</asp:TextBox>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ErrorMessage="*"
                            ControlToValidate="txttngkrjoutSLLn" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <%--<label class="medium_text">
                            <b>Total Tenaga Kerja Satuan Kerja Lainnya  :</b>
                        </label>--%>
                        <b><asp:Label ID="Label25" runat="server" Text="Total Tenaga Kerja Satuan Kerja Lainnya  :"></asp:Label></b>
                        <b><asp:Label ID="lblTotalLn" runat="server" Text="">0</asp:Label></b> <br />
                        <asp:HiddenField ID="HidTotalLn" runat="server" Value="0"/>
                    </div>
                </div>

                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>TOTAL TENAGA KERJA</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label15" runat="server" Text="Tenaga Manajerial Sampai Satu Level di bawah Anggota Direksi"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                             <b>
                                <asp:Label ID="Label22" runat="server" Text="Staf dan Lainnya"></asp:Label>
                            </b> 
                    </div>
                </div>
                    
                 <div class="form_box">
                    <div class="form_left">
                        <label class="medium_text" style="width:auto">
                            <b>Total Tenaga Kerja Tetap </b>
                        </label>
                        <label style="width:10px">
                            <b>:</b>
                        </label>
                        <b><asp:Label ID="lblTotalTTPrdks" runat="server" AutoPostBack="false" >0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalTTPrdks" runat="server" Value="0"/>
                    <br />
                        <label class="medium_text" style="width:auto">
                            <b>Total Tenaga Kerja Kontrak</b>
                        </label>
                        <label style="width:10px">
                            <b>:</b>
                        </label>
                        <b><asp:Label ID="lblTotalKTRrdks" runat="server" AutoPostBack="false" >0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalKTRrdks" runat="server" Value="0"/>
                    <br />
                        <label class="medium_text" style="width:auto">
                            <b>Total Tenaga Kerja Outsourcing</b>
                        </label>
                        <label style="width:10px">
                            <b>:</b>
                        </label>
                        <b><asp:Label ID="lblTotalOUTrdks" runat="server" AutoPostBack="false" >0</asp:Label></b>
                        <asp:HiddenField ID="HidTotalOUTrdks" runat="server" Value="0"/> 
                    </div>

                    <div class="form_right">
                        <label class="medium_text" style="width:auto">
                            <b>Total Tenaga Kerja Tetap</b>
                        </label>
                        <label style="width:10px">
                            <b>:</b>
                        </label>
                        <b><asp:Label ID="lblTotalTTPsl" runat="server" AutoPostBack="false">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalTTPsl" runat="server" Value="0"/> 
                    <br />
                        <label class="medium_text" style="width:auto">
                            <b>Total Tenaga Kerja Kontrak</b>
                        </label>
                        <label style="width:10px">
                            <b>:</b>
                        </label>
                        <b><asp:Label ID="lblTotalKTRsl" runat="server" AutoPostBack="false">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalKTRsl" runat="server" Value="0"/> 
                    <br />
                        <label class="medium_text" style="width:auto">
                            <b>Total Tenaga Kerja Outsourcing</b>
                        </label>
                        <label style="width:10px">
                            <b>:</b>
                        </label>
                        <b><asp:Label ID="lblTotalOUTsl" runat="server" AutoPostBack="false"  CssClass="clsTotal">0</asp:Label></b> 
                        <asp:HiddenField ID="HidTotalOUTsl" runat="server" Value="0" /> 
                    </div>
                </div>
                <div class="form_box">
                     <div class="form_single">
                        <label class="medium_text" style="width:auto">
                            <b>Total Tenaga Kerja Keseluruhan</b>
                        </label>
                         <label style="width:10px">
                            <b>:</b>
                        </label>
                          <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="false"></asp:TextBox>--%>
                        <b><asp:Label ID="lblTOTAL" runat="server" AutoPostBack="True"  >0</asp:Label ></b> 
                         <asp:HiddenField ID="HidTOTAL" runat="server" Value="0" /> 
                    </div>
                </div>                 
                
                
                    <div class="form_button">
                        <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                        </asp:Button>
                        <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                        </asp:Button>
                    </div>
                
            </asp:Panel>
    </form>

        <script type="text/javascript">
        $(document).ready(function () {
            //Pemasaran
            $(".clsPemasaran").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsPemasaran").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });                
                    $('#<%=lbltotalPemasaran.ClientID %>').text(total.toFixed(0)); 
                    $('#<%=HidtotalPemasaran.ClientID %>').val(total.toFixed(0));
                    });
                  });

             //Operasional
            $(".clsOperasional").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsOperasional").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lbltotalOperasional.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidtotalOperasional.ClientID %>').val(total.toFixed(0));
                    });
                  });

            //Penagihan
            $(".clsPenagihan").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsPenagihan").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalPenagihan.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalPenagihan.ClientID %>').val(total.toFixed(0));
                    });
                  });

            //HR
            $(".clsHR").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsHR").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalHrGa.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalHrGa.ClientID %>').val(total.toFixed(0));
                    });
                  });

             //Keuangan Akuntansi
            $(".clsAkuntansi").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsAkuntansi").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalAkn.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalAkn.ClientID %>').val(total.toFixed(0));
                    });
                  });

            //Manajemen Risiko
            $(".clsMnjRisiko").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsMnjRisiko").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalRsk.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalRsk.ClientID %>').val(total.toFixed(0));
                    });
                 });

             //Audit Internal
            $(".clsAudit").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsAudit").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalAudit.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalAudit.ClientID %>').val(total.toFixed(0));
                    });
                  });

            //Legal
            $(".clsLegal").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsLegal").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lalTotalLegal.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalLegal.ClientID %>').val(total.toFixed(0));
                    });
                  });

            //TI
            $(".clsTI").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsTI").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalIT.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalIT.ClientID %>').val(total.toFixed(0));
                    });
                  });

            //Lainnya
            $(".clsLainnya").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsLainnya").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalLn.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalLn.ClientID %>').val(total.toFixed(0));
                    });
                });
             });


        $(document).ready(function () {
            //Tetap Dibawah Direksi
            $(".clsTetapDir").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsTetapDir").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalTTPrdks.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalTTPrdks.ClientID %>').val(total.toFixed(0));
                });
            });

             //Kontrak Dibawah Direksi
             $(".clsKontrakDir").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsKontrakDir").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalKTRrdks.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalKTRrdks.ClientID %>').val(total.toFixed(0));
                });
             });

             //Outsource Dibawah Direksi
             $(".clsOutsourceDir").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsOutsourceDir").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalOUTrdks.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalOUTrdks.ClientID %>').val(total.toFixed(0));
                    });
            });

            //Tetap lainnya
            $(".clsTetapLain").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsTetapLain").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalTTPsl.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalTTPsl.ClientID %>').val(total.toFixed(0));
                    });
                });

             //Kontrak lainnya
             $(".clsKontrakLain").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsKontrakLain").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalKTRsl.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalKTRsl.ClientID %>').val(total.toFixed(0));
                });
             });

             //Outsource lainnya
             $(".clsOutsourceLain").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsOutsourceLain").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTotalOUTsl.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTotalOUTsl.ClientID %>').val(total.toFixed(0));
                });
             });
            //TOTAL
             $(".clsTOTAL").each(function () {
                $(this).keyup(function () {
                    var total = 0;
                    $(".clsTOTAL").each(function () {
                        if (!isNaN(this.value) && this.value.length != 0) {
                            total += parseFloat(this.value);
                        }
                    });
                    $('#<%=lblTOTAL.ClientID %>').text(total.toFixed(0));
                    $('#<%=HidTOTAL.ClientID %>').val(total.toFixed(0));
                });
             });
            $('.clsTOTAL').keyup();
        });
    </script>     
</body>
</html>