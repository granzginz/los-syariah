﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
'untuk eksport
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
'
#End Region

Public Class SIPP0030
    Inherits Webform.WebBased
    'Dim dtCSV, dtSQL As New DataTable

#Region "Constanta"
    Private oController As New Controller.SIPP0030Controller
    Private oCustomclass As New Parameter.SIPP0030
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNavi As ucGridNav

    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNavi.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "SIPP0030"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                pnlList.Visible = True
                pnlAdd.Visible = False
                'untuk eksport
                pnlcopybulandata.Visible = False
                '
                Me.SearchBy = ""
                Me.CmdWhere = "ALL"
                Me.SortBy = ""
                DoBind(Me.SearchBy, Me.SortBy)
                'untuk eksport
                FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
                '
            End If
        End If
    End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavi.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Parameter.SIPP0030

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP0030(oParameter)

        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()
        FillCombo()
        'FillCbo(cboGLNGN_PHK_LWN, "", 1419, "")
        FillCbo(cboGLNGN_PHK_LWN, "", "", 5)
        FillCbo(cboLKS_NGR_PHK_LWN, 37, "", "")
        FillCbo(cboBNTK_HKM, 7, "", "")
        FillCbo(cboSTTS_PMGNG_SHM, 8, "", "")
        FillCbo(cboJBTN_KPNGRSN, 9, "", "")
        FillCbo(cboKWRGNGRN, "", "", 3)
        'FillCbo(cboGLNGN_KLMPK_PMGNG_SHM_DRJT_KD, "", 1418, "")
        FillCbo(cboGLNGN_KLMPK_PMGNG_SHM_DRJT_KD, "", "", 6)
        FillCbo(cboLKS_NGR_PMGNG_SHM_DRJT_KD, "", "", 3)

        If (isFrNav = False) Then
            GridNavi.Initialize(recordCount, pageSize)
        End If

        'untuk eksport
        If recordCount = 0 Then
            ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
            pnlcopybulandata.Visible = True
            FillCombo()
            FillCboBulanData(cbocopybulandata, "TblBulanDataSIPP")
        Else
            pnlcopybulandata.Visible = False
        End If
        '


    End Sub
#End Region
    'untuk eksport
    Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.SIPP0030
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = oController.GetCboBulandataSIPP(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ID"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""
    End Sub
    '

#Region "Sort"
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP0030", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
            pnlList.Visible = False
            'untuk eksport
            txtbulandata.Enabled = True
            '
            Me.Process = "ADD"
            'DoBind(Me.SearchBy, Me.SortBy)
            clean()
        End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""

        'txtNMR_ZN_SH.Text = ""
        'txtTNGGL_ZN_SH.Text = ""
        'cboJNS_PRZNN.Text = "Select One"
        'txtKeterangan.Text = ""
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region



#Region "dtgList ItemCommand"
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP0030
        Dim dtEntity As New DataTable
        Dim err As String
        Try
            If e.CommandName.Trim = "Edit" Then
                If CheckFeature(Me.Loginid, "SIPP0030", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False
                'untuk eksport
                txtbulandata.Enabled = False
                '

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetSIPP0030Edit(oParameter)
                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboGLNGN_PHK_LWN.SelectedIndex = cboGLNGN_PHK_LWN.Items.IndexOf(cboGLNGN_PHK_LWN.Items.FindByValue(oRow("GLNGN_PHK_LWN").ToString.Trim))
                    cboLKS_NGR_PHK_LWN.SelectedIndex = cboLKS_NGR_PHK_LWN.Items.IndexOf(cboLKS_NGR_PHK_LWN.Items.FindByValue(oRow("LKS_NGR_PHK_LWN").ToString.Trim))
                    cboBNTK_HKM.SelectedIndex = cboBNTK_HKM.Items.IndexOf(cboBNTK_HKM.Items.FindByValue(oRow("BNTK_HKM").ToString.Trim))
                    cboSTTS_PMGNG_SHM.SelectedIndex = cboSTTS_PMGNG_SHM.Items.IndexOf(cboSTTS_PMGNG_SHM.Items.FindByValue(oRow("STTS_PMGNG_SHM").ToString.Trim))
                    cboJBTN_KPNGRSN.SelectedIndex = cboJBTN_KPNGRSN.Items.IndexOf(cboJBTN_KPNGRSN.Items.FindByValue(oRow("JBTN_KPNGRSN").ToString.Trim))
                    cboKWRGNGRN.SelectedIndex = cboKWRGNGRN.Items.IndexOf(cboKWRGNGRN.Items.FindByValue(oRow("KWRGNGRN").ToString.Trim))
                    cboGLNGN_KLMPK_PMGNG_SHM_DRJT_KD.SelectedIndex = cboGLNGN_KLMPK_PMGNG_SHM_DRJT_KD.Items.IndexOf(cboGLNGN_KLMPK_PMGNG_SHM_DRJT_KD.Items.FindByValue(oRow("GLNGN_KLMPK_PMGNG_SHM_DRJT_KD").ToString.Trim))
                    cboLKS_NGR_PMGNG_SHM_DRJT_KD.SelectedIndex = cboLKS_NGR_PMGNG_SHM_DRJT_KD.Items.IndexOf(cboLKS_NGR_PMGNG_SHM_DRJT_KD.Items.FindByValue(oRow("LKS_NGR_PMGNG_SHM_DRJT_KD").ToString.Trim))
                End If
                'untuk eksport
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                '
                txtID.Text = oParameter.ListData.Rows(0)("ID")
                txtNM_PMGNG_SHM.Text = oParameter.ListData.Rows(0)("NM_PMGNG_SHM")
                txtKTS_PMGNG_SHM.Text = oParameter.ListData.Rows(0)("KTS_PMGNG_SHM")
                txtNL_KPMLKN_SHM.Text = oParameter.ListData.Rows(0)("NL_KPMLKN_SHM")
                txtPRSNTS_KPMLKN_SHM.Text = oParameter.ListData.Rows(0)("PRSNTS_KPMLKN_SHM")
                txtNM_PNGRS_DRJT_KD.Text = oParameter.ListData.Rows(0)("NM_PNGRS_DRJT_KD")
                txtNM_PMGNG_SHM_DRJT_KD.Text = oParameter.ListData.Rows(0)("NM_PMGNG_SHM_DRJT_KD")
                txtNL_KPMLKN_SHM_DRJT_KD.Text = oParameter.ListData.Rows(0)("NL_KPMLKN_SHM_DRJT_KD")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP0030", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP0030
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString
                End With
                Err = oController.SIPP0030Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#End Region

#Region " SAVE "
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.SIPP0030
        Dim ErrMessage As String = ""


        If Me.Process = "ADD" Then
            With customClass
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .NM_PMGNG_SHM = txtNM_PMGNG_SHM.Text
                .GLNGN_PHK_LWN = cboGLNGN_PHK_LWN.SelectedValue.Trim
                .LKS_NGR_PHK_LWN = cboLKS_NGR_PHK_LWN.SelectedValue.Trim
                .BNTK_HKM = cboBNTK_HKM.SelectedValue.Trim
                .STTS_PMGNG_SHM = cboSTTS_PMGNG_SHM.SelectedValue.Trim
                .KTS_PMGNG_SHM = txtKTS_PMGNG_SHM.Text
                .NL_KPMLKN_SHM = txtNL_KPMLKN_SHM.Text
                .PRSNTS_KPMLKN_SHM = txtPRSNTS_KPMLKN_SHM.Text
                .NM_PNGRS_DRJT_KD = txtNM_PNGRS_DRJT_KD.Text
                .JBTN_KPNGRSN = cboJBTN_KPNGRSN.SelectedValue.Trim
                .KWRGNGRN = cboKWRGNGRN.SelectedValue.Trim
                .NM_PMGNG_SHM_DRJT_KD = txtNM_PMGNG_SHM_DRJT_KD.Text
                .GLNGN_KLMPK_PMGNG_SHM_DRJT_KD = cboGLNGN_KLMPK_PMGNG_SHM_DRJT_KD.SelectedValue.Trim
                .LKS_NGR_PMGNG_SHM_DRJT_KD = cboLKS_NGR_PMGNG_SHM_DRJT_KD.SelectedValue.Trim
                .NL_KPMLKN_SHM_DRJT_KD = txtNL_KPMLKN_SHM_DRJT_KD.Text

                .strConnection = GetConnectionString()
            End With
            oController.SIPP0030Add(customClass)
        ElseIf Me.Process = "EDIT" Then
            With customClass
                .ID = txtID.Text
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .NM_PMGNG_SHM = txtNM_PMGNG_SHM.Text
                .GLNGN_PHK_LWN = cboGLNGN_PHK_LWN.SelectedValue.Trim
                .LKS_NGR_PHK_LWN = cboLKS_NGR_PHK_LWN.SelectedValue.Trim
                .BNTK_HKM = cboBNTK_HKM.SelectedValue.Trim
                .STTS_PMGNG_SHM = cboSTTS_PMGNG_SHM.SelectedValue.Trim
                .KTS_PMGNG_SHM = txtKTS_PMGNG_SHM.Text
                .NL_KPMLKN_SHM = txtNL_KPMLKN_SHM.Text
                .PRSNTS_KPMLKN_SHM = txtPRSNTS_KPMLKN_SHM.Text
                .NM_PNGRS_DRJT_KD = txtNM_PNGRS_DRJT_KD.Text
                .JBTN_KPNGRSN = cboJBTN_KPNGRSN.SelectedValue.Trim
                .KWRGNGRN = cboKWRGNGRN.SelectedValue.Trim
                .NM_PMGNG_SHM_DRJT_KD = txtNM_PMGNG_SHM_DRJT_KD.Text
                .GLNGN_KLMPK_PMGNG_SHM_DRJT_KD = cboGLNGN_KLMPK_PMGNG_SHM_DRJT_KD.SelectedValue.Trim
                .LKS_NGR_PMGNG_SHM_DRJT_KD = cboLKS_NGR_PMGNG_SHM_DRJT_KD.SelectedValue.Trim
                .NL_KPMLKN_SHM_DRJT_KD = txtNL_KPMLKN_SHM_DRJT_KD.Text

                .strConnection = GetConnectionString()
            End With
            oController.SIPP0030Update(customClass)
        End If
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Update data berhasil.....", False)
        End If

    End Sub
#End Region

    'untuk eksport
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_003020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0030Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP0030
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "28_Rincian_Pemegang_Saham_dan_Pemegang_Saham_Derajat_Kedua_TB_003020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
    End Sub
#End Region

#Region "Copy Data"
    Public Function GetCopy(ByVal customclass As Parameter.SIPP0030) As Parameter.SIPP0030
        Dim params() As SqlParameter = New SqlParameter(1) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP0030Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP0030
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = cbobulandata.Text
            .Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region " dropdown "
    Private Sub FillCombo()
        'Dim strCombo As String
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP0030
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.valuemaster = valuemaster
        oAssetData.valueparent = valueparent
        oAssetData.valueelement = valueelement
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ElementLabel"
        cboName.DataValueField = "Element"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = ""

    End Sub
#End Region

    Sub clean()
        txtNM_PMGNG_SHM.Text = ""
        txtKTS_PMGNG_SHM.Text = ""
        txtNL_KPMLKN_SHM.Text = ""
        txtPRSNTS_KPMLKN_SHM.Text = ""
        txtNM_PNGRS_DRJT_KD.Text = ""
        txtNM_PMGNG_SHM_DRJT_KD.Text = ""
        txtNL_KPMLKN_SHM_DRJT_KD.Text = ""

    End Sub

End Class