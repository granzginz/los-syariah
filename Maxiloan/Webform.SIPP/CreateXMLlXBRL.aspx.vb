﻿Imports System.Xml
Imports System.Web.UI.WebControls
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Web.Security


Public Class CreateXMLlXBRL
    Inherits Webform.WebBased
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            ' When the page loads we get the user id from a session variable and saves this in a hidden field
            Me.FormID = "XBRL"
            HiddenUserID.Value = Convert.ToString(Session("UserID"))

        End If
    End Sub

    Public Function ReplaceSymbols(ByVal Symbols As String) As String
        Symbols = Replace(Symbols, ":", "")
        Symbols = Replace(Symbols, " ", "")
        Symbols = Replace(Symbols, ",", "")
        Symbols = Replace(Symbols, "-", "")
        Return Symbols
    End Function

    Protected Sub Write_XML(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.WizardNavigationEventArgs) Handles Wizard1.FinishButtonClick

        'Create the url for the xml-file to be saved in the database, we use a function in Tools.vb to replace
        ' symbols. Look in Tools.vb to see the code.

        Dim xbrlurl As String
        xbrlurl = "xbrl/" & ReplaceSymbols(txtFirma.Text) & ReplaceSymbols(DateTime.Now.ToString()) & ".xml"

        ' Save the url for the XML-file to the XML-files table in the database so the user can see and download the XML-files 
        ' that the user have saved on our server.

        Try
            Dim ConnString As String = ConfigurationManager.ConnectionStrings("ConnectionString").ToString()
            Dim sql As String = "INSERT INTO XML-files (URL, Company, StartDate, EndDate, UserID, Date) VALUES (@URL, @Company, @StartDate, @EndDate, @UserID, @Date)"

            ' The Using block is used to call dispose (close) automatically even if there are an exception.
            Using cn As New SqlConnection(ConnString),
                  cmd As New SqlCommand(sql, cn)
                cmd.Parameters.Add("@URL", Data.SqlDbType.VarChar, 80).Value = xbrlurl
                cmd.Parameters.Add("@Company", Data.SqlDbType.VarChar, 50).Value = txtFirma.Text
                cmd.Parameters.Add("@StartDate", Data.SqlDbType.SmallDateTime).Value = txtRakenskapsarStartDatePeriod0.Text
                cmd.Parameters.Add("@EndDate", Data.SqlDbType.SmallDateTime).Value = txtRakenskapsarEndDatePeriod0.Text
                cmd.Parameters.Add("@UserID", Data.SqlDbType.Int).Value = HiddenUserID.Value
                cmd.Parameters.Add("@Date", Data.SqlDbType.SmallDateTime).Value = DateTime.Now.ToString()

                cn.Open()
                cmd.ExecuteNonQuery()

            End Using
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        ' Create an XML-document with the right encoding and saves this xml-file to our 
        ' server according to the "xbrlurl" above.

        Dim textWriter As New XmlTextWriter(Server.MapPath(xbrlurl), New UTF8Encoding(False))
        textWriter.Formatting = System.Xml.Formatting.Indented

        ' IF WE DONT WANT TO SAVE THE XML-FILE ON THE SERVER WE COULD USE THIS CODE INSTEAD SO THAT
        ' THE USER COULD DOWNLOAD THE FILE TO HIS LOCAL COMPUTER
        'Dim textWriter As New XmlTextWriter(Response.OutputStream, New UTF8Encoding(False))
        'textWriter.Formatting = System.Xml.Formatting.Indented

        'Start New Document
        textWriter.WriteStartDocument()

        'Insert XBRL start row, Start Element with attributes
        textWriter.WriteStartElement("xbrli:xbrl")
        textWriter.WriteAttributeString("xmlns:se-cd-base", "http://www.xbrl.se/se/fr/cd-base/2007-05-25")
        textWriter.WriteAttributeString("xmlns:link", "http://www.xbrl.org/2003/linkbase")
        textWriter.WriteAttributeString("xmlns:se-gen-base", "http://www.xbrl.se/se/fr/gen-base/2007-05-25")
        textWriter.WriteAttributeString("xmlns:bve", "http://www.bolagsverket.se/xbrl/types/2006-05-25")
        textWriter.WriteAttributeString("xmlns:iso4217", "http://www.xbrl.org/2003/iso4217")
        textWriter.WriteAttributeString("xmlns:xlink", "http://www.w3.org/1999/xlink")
        textWriter.WriteAttributeString("xmlns:xbrli", "http://www.xbrl.org/2003/instance")

        'Insert XBRL link scheme
        If BruttoresultatRES0.Text = "" Or BruttoresultatRES0.Text Is Nothing Then
            textWriter.WriteElementString("link:schemaRef xlink:type=""simple"" xlink:href=""http://taxonomi.xbrl.se/se/fr/sme/rbn/2008-09-30/se-sme-rbn-2008-09-30.xsd""", "")
        Else
            textWriter.WriteElementString("link:schemaRef xlink:type=""simple"" xlink:href=""http://taxonomi.xbrl.se/se/fr/sme/rbf/2008-09-30/se-sme-rbf-2008-09-30.xsd""", "")
        End If

        'Insert Start Element for PERIOD0 with attributeString
        textWriter.WriteStartElement("xbrli:context")
        textWriter.WriteAttributeString("id", "PERIOD0")

        'Insert entity inside PERIOD0 xbrli:context
        textWriter.WriteStartElement("xbrli:entity")
        textWriter.WriteStartElement("xbrli:identifier", "")
        textWriter.WriteAttributeString("scheme", "http://www.bolagsverket.se")
        textWriter.WriteString(txtOrganisationsnummer.Text)
        textWriter.WriteEndElement()
        textWriter.WriteEndElement()

        'Insert period inside PERIOD0 xbrli:context
        textWriter.WriteStartElement("xbrli:period")
        textWriter.WriteStartElement("xbrli:startDate", "")
        textWriter.WriteString(txtRakenskapsarStartDatePeriod0.Text)
        textWriter.WriteEndElement()
        textWriter.WriteStartElement("xbrli:endDate", "")
        textWriter.WriteString(txtRakenskapsarEndDatePeriod0.Text)
        textWriter.WriteEndElement()
        textWriter.WriteEndElement()

        'Insert End Element for PERIOD0
        textWriter.WriteEndElement()

        If Not txtRakenskapsarStartDatePeriodm1.Text = "" Then
            'Insert Start Element for PERIOD-1 with attributeString
            textWriter.WriteStartElement("xbrli:context")
            textWriter.WriteAttributeString("id", "PERIOD-1")
            'Insert entity inside PERIOD-1 xbrli:context
            textWriter.WriteStartElement("xbrli:entity")
            textWriter.WriteStartElement("xbrli:identifier", "")
            textWriter.WriteAttributeString("scheme", "http://www.bolagsverket.se")
            textWriter.WriteString(txtOrganisationsnummer.Text)
            textWriter.WriteEndElement()
            textWriter.WriteEndElement()
            'Insert period inside PERIOD-1 xbrli:context
            textWriter.WriteStartElement("xbrli:period")
            textWriter.WriteStartElement("xbrli:startDate", "")
            textWriter.WriteString(txtRakenskapsarStartDatePeriodm1.Text)
            textWriter.WriteEndElement()
            textWriter.WriteStartElement("xbrli:endDate", "")
            textWriter.WriteString(txtRakenskapsarEndDatePeriodm1.Text)
            textWriter.WriteEndElement()
            textWriter.WriteEndElement()
            'Insert End Element for PERIOD-1
            textWriter.WriteEndElement()
        End If

        'Insert Start Element for BALANS0 with attributeString
        textWriter.WriteStartElement("xbrli:context")
        textWriter.WriteAttributeString("id", "BALANS0")
        'Insert entity inside BALANS0 xbrli:context
        textWriter.WriteStartElement("xbrli:entity")
        textWriter.WriteStartElement("xbrli:identifier", "")
        textWriter.WriteAttributeString("scheme", "http://www.bolagsverket.se")
        textWriter.WriteString(txtOrganisationsnummer.Text)
        textWriter.WriteEndElement()
        textWriter.WriteEndElement()
        'Insert period inside BALANS0 xbrli:context
        textWriter.WriteStartElement("xbrli:period")
        textWriter.WriteStartElement("xbrli:instant", "")
        textWriter.WriteString(txtRakenskapsarEndDatePeriod0.Text)
        textWriter.WriteEndElement()
        textWriter.WriteEndElement()
        'Insert End Element for BALANS0
        textWriter.WriteEndElement()

        'Insert Start Element for BALANS-1 with attributeString
        If Not txtRakenskapsarStartDatePeriodm1.Text = "" Then
            textWriter.WriteStartElement("xbrli:context")
            textWriter.WriteAttributeString("id", "BALANS-1")
            'Insert entity inside BALANS-1 xbrli:context
            textWriter.WriteStartElement("xbrli:entity")
            textWriter.WriteStartElement("xbrli:identifier", "")
            textWriter.WriteAttributeString("scheme", "http://www.bolagsverket.se")
            textWriter.WriteString(txtOrganisationsnummer.Text)
            textWriter.WriteEndElement()
            textWriter.WriteEndElement()
            'Insert period inside BALANS-1 xbrli:context
            textWriter.WriteStartElement("xbrli:period")
            textWriter.WriteStartElement("xbrli:instant", "")
            textWriter.WriteString(txtRakenskapsarEndDatePeriodm1.Text)
            textWriter.WriteEndElement()
            textWriter.WriteEndElement()
            'Insert End Element for BALANS-1
            textWriter.WriteEndElement()
        End If

        'Insert Start Element for Valuta
        textWriter.WriteStartElement("xbrli:unit")
        textWriter.WriteAttributeString("id", "valuta")
        'Insert measure inside Valuta
        textWriter.WriteStartElement("xbrli:measure")
        textWriter.WriteString("iso4217:" & txtRedovisningsvaluta.SelectedValue)
        textWriter.WriteEndElement()
        'Insert End Element for Valuta
        textWriter.WriteEndElement()

        'Insert Start Element for Andelar
        textWriter.WriteStartElement("xbrli:unit")
        textWriter.WriteAttributeString("id", "andelar")
        'Insert measure inside Andelar
        textWriter.WriteStartElement("xbrli:measure")
        textWriter.WriteString("xbrli:shares")
        textWriter.WriteEndElement()
        'Insert End Element for Andelar
        textWriter.WriteEndElement()

        'Insert Start Element for Procent
        textWriter.WriteStartElement("xbrli:unit")
        textWriter.WriteAttributeString("id", "procent")
        'Insert measure inside Procent
        textWriter.WriteStartElement("xbrli:measure")
        textWriter.WriteString("xbrli:pure")
        textWriter.WriteEndElement()
        'Insert End Element for Procent
        textWriter.WriteEndElement()

        'Insert Start Element for Antal
        textWriter.WriteStartElement("xbrli:unit")
        textWriter.WriteAttributeString("id", "antal")
        'Insert measure inside Antal
        textWriter.WriteStartElement("xbrli:measure")
        textWriter.WriteString("bve:antal")
        textWriter.WriteEndElement()
        'Insert End Element for Antal
        textWriter.WriteEndElement()

        'Insert Elements for Firma
        textWriter.WriteStartElement("se-cd-base:Firma")
        textWriter.WriteAttributeString("contextRef", "PERIOD0")
        textWriter.WriteString(txtFirma.Text)
        textWriter.WriteEndElement()

        'Insert Elements for Organisationsnummer
        textWriter.WriteStartElement("se-cd-base:Organisationsnummer")
        textWriter.WriteAttributeString("contextRef", "PERIOD0")
        textWriter.WriteString(txtOrganisationsnummer.Text)
        textWriter.WriteEndElement()

        'Insert Elements for Sprak
        textWriter.WriteStartElement("se-cd-base:Sprak")
        textWriter.WriteAttributeString("contextRef", "PERIOD0")
        textWriter.WriteString("sv")
        textWriter.WriteEndElement()

        'Insert Elements for Land
        textWriter.WriteStartElement("se-cd-base:Land")
        textWriter.WriteAttributeString("contextRef", "PERIOD0")
        textWriter.WriteString("SE")
        textWriter.WriteEndElement()

        'Insert Elements for Beloppsformat
        textWriter.WriteStartElement("se-cd-base:Beloppsformat")
        textWriter.WriteAttributeString("contextRef", "PERIOD0")
        textWriter.WriteString(txtBeloppsformat.SelectedValue)
        textWriter.WriteEndElement()

        'Insert Elements for Redovisningsvaluta
        textWriter.WriteStartElement("se-cd-base:Redovisningsvaluta")
        textWriter.WriteAttributeString("contextRef", "PERIOD0")
        textWriter.WriteString(txtRedovisningsvaluta.SelectedValue)
        textWriter.WriteEndElement()

        'Insert Elements for Verksamhetsbeskrivning i Forvaltningsberattelsen
        If Not txtVerksamheten.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Verksamheten")
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteString(txtVerksamheten.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Vasentliga Handelser i Forvaltningsberattelsen
        If Not txtVasantligaHandelser.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:VasentligaHandelserUnderOchEfterRakenskapsaretsSlut")
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteString(txtVasantligaHandelser.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Framtida Utveckling i Forvaltningsberattelsen
        If Not txtFramtidaUtveckling.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:FramtidaUtveckling")
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteString(txtFramtidaUtveckling.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for MiljoPaverkan i Forvaltningsberattelsen
        If Not txtMiljoPaverkan.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Miljopaverkan")
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteString(txtMiljoPaverkan.Text)
            textWriter.WriteEndElement()
        End If

        '///////////////////////////// Medel att disponera //////////////////////////////////

        'Insert Elements for Medel att disponera Overkursfond
        If Not MedelAttDisponeraOverkursfond.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:MedelAttDisponeraOverkursfond")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(MedelAttDisponeraOverkursfond.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Medel att disponera Fond for verkligt varde
        If Not MedelAttDisponeraFondForVerkligtVarde.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:MedelAttDisponeraFondForVerkligtVarde")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(MedelAttDisponeraFondForVerkligtVarde.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Medel att disponera Balanserat resultat
        If Not MedelAttDisponeraBalanseratResultat.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:MedelAttDisponeraBalanseratResultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(MedelAttDisponeraBalanseratResultat.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Medel att disponera Koncernbidrag
        If Not MedelAttDisponeraKoncernbidrag.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:MedelAttDisponeraKoncernbidrag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(MedelAttDisponeraKoncernbidrag.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Medel att disponera Erhallna aktieagartillskott
        If Not MedelAttDisponeraErhallnaAktieagartillskott.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:MedelAttDisponeraErhallnaAktieagartillskott")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(MedelAttDisponeraErhallnaAktieagartillskott.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Medel att disponera Arets resultat
        If Not MedelAttDisponeraAretsResultat.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:MedelAttDisponeraAretsResultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(MedelAttDisponeraAretsResultat.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Medel att disponera Summa
        If Not MedelAttDisponeraSumma.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:MedelAttDisponera")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(MedelAttDisponeraSumma.Text)
            textWriter.WriteEndElement()
        End If

        '///////////////////////////// Forslag till disposition //////////////////////////////////

        'Insert Elements for Forslag till disposition Utdelning
        If Not ForslagDispositionUtdelning.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForslagDispositionUtdelning")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForslagDispositionUtdelning.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Forslag till disposition Fondemission
        If Not ForslagDispositionFondemission.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForslagDispositionFondemission")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForslagDispositionFondemission.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Forslag till disposition Avsattning till reservfond
        If Not ForslagDispositionAvsattningTillReservfond.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForslagDispositionAvsattningReservfond")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForslagDispositionAvsattningTillReservfond.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Forslag till disposition Ianspraktagande av reservfond
        If Not ForslagDispositionIanspraktagandeAvReservFond.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForslagDispositionIanspraktagandeAvReservfond")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForslagDispositionIanspraktagandeAvReservFond.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Forslag till disposition Ianspraktagande av uppskrivningsfond
        If Not ForslagDispositionIanspraktagandeAvUppskrivningsfond.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForslagDispositionIanspraktagandeAvUppskrivningsfond")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForslagDispositionIanspraktagandeAvUppskrivningsfond.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Forslag till disposition Balanseras i ny rakning
        If Not ForslagDispositionBalanserasINyRakning.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForslagDispositionBalanserasINyRakning")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForslagDispositionBalanserasINyRakning.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Forslag till disposition Summa
        If Not ForslagDispositionSumma.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForslagDispositionSumma")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForslagDispositionSumma.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Kommentar till dispositioner
        If Not txtKommentarDispositioner.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:DispositionerVinstEllerForlustKommentar")
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteString(txtKommentarDispositioner.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Styrelsens yttrande om vinstutdelning
        If Not StyrelsensYttrandeVinstutdelning.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:StyrelsensYttrandeVinstutdelning")
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteString(StyrelsensYttrandeVinstutdelning.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultat och stallning
        If Not ResultatOchStallning.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatOchStallning")
            textWriter.WriteAttributeString("contextRef", "BALANS0")
            textWriter.WriteString(ResultatOchStallning.Text)
            textWriter.WriteEndElement()
        End If

        '///////////////////////////// Resultatrakning //////////////////////////////////

        'Insert Elements for Resultatrakning Nettoomsattning Period 0
        If Not NettoomsattningRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Nettoomsattning")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(NettoomsattningRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Nettoomsattning Period -1
        If Not NettoomsattningRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Nettoomsattning")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(NettoomsattningRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Kostnad Salda Varor Period 0
        If Not KostnadSaldaVarorRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:KostnadSaldaVaror")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(KostnadSaldaVarorRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Kostnad Salda Varor Period -1
        If Not KostnadSaldaVarorRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:KostnadSaldaVaror")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(KostnadSaldaVarorRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Bruttoresultat Period 0
        If Not BruttoresultatRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Bruttoresultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(BruttoresultatRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Bruttoresultat Period -1
        If Not BruttoresultatRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Bruttoresultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(BruttoresultatRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Forsaljningskostnader Period 0
        If Not ForsaljningskostnaderRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Forsaljningskostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForsaljningskostnaderRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Forsaljningskostnader Period -1
        If Not ForsaljningskostnaderRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Forsaljningskostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForsaljningskostnaderRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Administrationskostnader Period 0
        If Not AdministrationskostnaderRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Administrationskostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(AdministrationskostnaderRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Administrationskostnader Period -1
        If Not AdministrationskostnaderRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Administrationskostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(AdministrationskostnaderRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Forskning utveckling Period 0
        If Not ForskningUtvecklingRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForskningsUtvecklingskostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForskningUtvecklingRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Forskning utveckling Period -1
        If Not ForskningUtvecklingRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForskningsUtvecklingskostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForskningUtvecklingRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Lagerforandring Period 0
        If Not LagerforandringRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForandringAvLagerAvProdukterIArbeteFardigaVarorOchPagaendeArbetenForAnnansRakning")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(LagerforandringRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Lagerforandring Period -1
        If Not LagerforandringRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForandringAvLagerAvProdukterIArbeteFardigaVarorOchPagaendeArbetenForAnnansRakning")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(LagerforandringRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Aktiverat arbete for egen rakning Period 0
        If Not AktiveratArbeteForEgenRakningRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:AktiveratArbeteForEgenRakning")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(AktiveratArbeteForEgenRakningRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Aktiverat arbete for egen rakning Period -1
        If Not AktiveratArbeteForEgenRakningRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:AktiveratArbeteForEgenRakning")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(AktiveratArbeteForEgenRakningRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga rorelseintakter Period 0
        If Not OvrigaRorelseIntakterRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaRorelseintakter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaRorelseIntakterRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga rorelseintakter Period -1
        If Not OvrigaRorelseIntakterRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaRorelseintakter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaRorelseIntakterRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa rorelseintakter Period 0
        If Not RorelseintakterLagerforandringarMmRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:RorelseintakterLagerforandringarMm")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RorelseintakterLagerforandringarMmRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa rorelseintakter Period -1
        If Not RorelseintakterLagerforandringarMmRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:RorelseintakterLagerforandringarMm")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RorelseintakterLagerforandringarMmRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ravaror och Fornodenheter Period 0
        If Not RavarorOchFornodenheterResultatrakningenRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:RavarorFornodenheterResultatrakningen")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RavarorOchFornodenheterResultatrakningenRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ravaror och Fornodenheter Period -1
        If Not RavarorOchFornodenheterResultatrakningenRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:RavarorFornodenheterResultatrakningen")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RavarorOchFornodenheterResultatrakningenRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Handelsvaror Period 0
        If Not HandelsvarorRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Handelsvaror")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(HandelsvarorRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Handelsvaror Period -1
        If Not HandelsvarorRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Handelsvaror")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(HandelsvarorRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga externa kostnader Period 0
        If Not OvrigaExternaKostnaderRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaExternaKostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaExternaKostnaderRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga externa kostnader Period -1
        If Not OvrigaExternaKostnaderRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaExternaKostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaExternaKostnaderRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Personalkostnader Period 0
        If Not PersonalkostnaderRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Personalkostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(PersonalkostnaderRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Personalkostnader Period -1
        If Not PersonalkostnaderRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Personalkostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(PersonalkostnaderRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Av- och nedskrivningar materiella och immateriella Period 0
        If Not AvskrivningarOchNedskrivningarAvMateriellaOchImmateriellaAnlaggningstillgangarRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:AvskrivningarOchNedskrivningarMateriellaOchImmateriellaAnlaggningstillgangar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(AvskrivningarOchNedskrivningarAvMateriellaOchImmateriellaAnlaggningstillgangarRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Av- och nedskrivningar materiella och immateriella Period -1
        If Not AvskrivningarOchNedskrivningarAvMateriellaOchImmateriellaAnlaggningstillgangarRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:AvskrivningarOchNedskrivningarMateriellaOchImmateriellaAnlaggningstillgangar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(AvskrivningarOchNedskrivningarAvMateriellaOchImmateriellaAnlaggningstillgangarRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Nedskrivningar av omsattningstillgangar Period 0
        If Not NedskrivningarAvOmsattningstillgangarUtoverNormalaNedskrivningarRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:NedskrivningarOmsattningstillgangarUtoverNormalaNedskrivningar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(NedskrivningarAvOmsattningstillgangarUtoverNormalaNedskrivningarRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Nedskrivningar av omsattningstillgangar Period -1
        If Not NedskrivningarAvOmsattningstillgangarUtoverNormalaNedskrivningarRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:NedskrivningarOmsattningstillgangarUtoverNormalaNedskrivningar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(NedskrivningarAvOmsattningstillgangarUtoverNormalaNedskrivningarRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga rorelsekostnader Period 0
        If Not OvrigaRorelsekostnaderRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaRorelsekostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaRorelsekostnaderRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga rorelsekostnader Period -1
        If Not OvrigaRorelsekostnaderRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaRorelsekostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaRorelsekostnaderRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa funktioner Period 0
        If Not SummaFunktionerRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:FunktionerMmSumma")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(SummaFunktionerRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa funktioner Period -1
        If Not SummaFunktionerRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:FunktionerMmSumma")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(SummaFunktionerRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa rorelsekostnader Period 0
        If Not RorelsekostnaderRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Rorelsekostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RorelsekostnaderRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa rorelsekostnader Period -1
        If Not RorelsekostnaderRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Rorelsekostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RorelsekostnaderRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Rorelseresultat Period 0
        If Not RorelseresultatRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Rorelseresultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RorelseresultatRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Rorelseresultat Period -1
        If Not RorelseresultatRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:Rorelseresultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RorelseresultatRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fran andelar i koncernforetag Period 0
        If Not ResultatFranAndelarIKoncernforetagRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatAndelarKoncernforetag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatFranAndelarIKoncernforetagRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fran andelar i koncernforetag Period -1
        If Not ResultatFranAndelarIKoncernforetagRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatAndelarKoncernforetag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatFranAndelarIKoncernforetagRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fran andelar i intresseforetag Period 0
        If Not ResultatFranAndelarIIntresseforetagRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatAndelarIntresseforetag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatFranAndelarIIntresseforetagRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fran andelar i intresseforetag Period -1
        If Not ResultatFranAndelarIIntresseforetagRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatAndelarIntresseforetag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatFranAndelarIIntresseforetagRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fran ovriga finansiella anlaggningstillgangar Period 0
        If Not ResultatFranOvrigaFinansiellaAnlaggningstillgangarRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatOvrigaFinansiellaAnlaggningstillgangar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatFranOvrigaFinansiellaAnlaggningstillgangarRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fran ovriga finansiella anlaggningstillgangar Period -1
        If Not ResultatFranOvrigaFinansiellaAnlaggningstillgangarRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatOvrigaFinansiellaAnlaggningstillgangar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatFranOvrigaFinansiellaAnlaggningstillgangarRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga ranteintakter och liknande resultatposter Period 0
        If Not OvrigaRanteintakterOchLiknandeResultatposterRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaRanteintakterOchLiknandeResultatposter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaRanteintakterOchLiknandeResultatposterRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga ranteintakter och liknande resultatposter Period -1
        If Not OvrigaRanteintakterOchLiknandeResultatposterRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaRanteintakterOchLiknandeResultatposter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaRanteintakterOchLiknandeResultatposterRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Nedskrivningar av finansiella anlaggningstillgangar Period 0
        If Not NedskrivningarAvFinansiellaAnlaggningstillgangarOchKortfristigaPlaceringarRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:NedskrivningarFinansiellaAnlaggningstillgangarOchKortfristigaPlaceringar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(NedskrivningarAvFinansiellaAnlaggningstillgangarOchKortfristigaPlaceringarRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Nedskrivningar av finansiella anlaggningstillgangar Period -1
        If Not NedskrivningarAvFinansiellaAnlaggningstillgangarOchKortfristigaPlaceringarRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:NedskrivningarFinansiellaAnlaggningstillgangarOchKortfristigaPlaceringar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(NedskrivningarAvFinansiellaAnlaggningstillgangarOchKortfristigaPlaceringarRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Rantekostnader och liknande resultatposter Period 0
        If Not RantekostnaderOchLiknandeResultatposterRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:RantekostnaderOchLiknandeResultatposter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RantekostnaderOchLiknandeResultatposterRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Rantekostnader och liknande resultatposter Period -1
        If Not RantekostnaderOchLiknandeResultatposterRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:RantekostnaderOchLiknandeResultatposter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(RantekostnaderOchLiknandeResultatposterRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat efter finansiella poster Period 0
        If Not ResultatEfterFinansiellaPosterRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatEfterFinansiellaPoster")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatEfterFinansiellaPosterRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat efter finansiella poster Period -1
        If Not ResultatEfterFinansiellaPosterRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatEfterFinansiellaPoster")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatEfterFinansiellaPosterRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa finansiella poster Period 0
        If Not SummaFinPosterRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:FinansiellaPosterSumma")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(SummaFinPosterRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa finansiella poster Period -1
        If Not SummaFinPosterRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:FinansiellaPosterSumma")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(SummaFinPosterRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Extraordinara intakter Period 0
        If Not ExtraordinaraIntakterRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ExtraordinaraIntakter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ExtraordinaraIntakterRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Extraordinara intakter Period -1
        If Not ExtraordinaraIntakterRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ExtraordinaraIntakter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ExtraordinaraIntakterRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Extraordinara kostnader Period 0
        If Not ExtraordinaraKostnaderRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ExtraordinaraKostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ExtraordinaraKostnaderRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Extraordinara kostnader Period -1
        If Not ExtraordinaraKostnaderRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ExtraordinaraKostnader")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ExtraordinaraKostnaderRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fore bokslutsdispositioner och skatt Period 0
        If Not ResultatForeBokslutsdispositionerOchSkattRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatForeBokslutsdispositionerOchSkatt")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatForeBokslutsdispositionerOchSkattRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fore bokslutsdispositioner och skatt Period -1
        If Not ResultatForeBokslutsdispositionerOchSkattRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatForeBokslutsdispositionerOchSkatt")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatForeBokslutsdispositionerOchSkattRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Forandring av periodiseringsfond Period 0
        If Not ForandringAvPeriodiseringsfondRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForandringPeriodiseringsfond")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForandringAvPeriodiseringsfondRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Forandring av periodiseringsfond Period -1
        If Not ForandringAvPeriodiseringsfondRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForandringPeriodiseringsfond")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForandringAvPeriodiseringsfondRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Forandring av overavskrivningar Period 0
        If Not ForandringAvOveravskrivningarRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForandringOveravskrivningar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForandringAvOveravskrivningarRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Forandring av overavskrivningar Period -1
        If Not ForandringAvOveravskrivningarRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ForandringOveravskrivningar")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ForandringAvOveravskrivningarRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Erhallna koncernbidrag Period 0
        If Not ErhallnaKoncernbidragRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ErhallnaKoncernbidrag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ErhallnaKoncernbidragRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Erhallna koncernbidrag Period -1
        If Not ErhallnaKoncernbidragRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ErhallnaKoncernbidrag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ErhallnaKoncernbidragRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Lamnade koncernbidrag Period 0
        If Not LamnadeKoncernbidragRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:LamnadeKoncernbidrag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(LamnadeKoncernbidragRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Lamnade koncernbidrag Period -1
        If Not LamnadeKoncernbidragRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:LamnadeKoncernbidrag")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(LamnadeKoncernbidragRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga bokslutsdispositioner Period 0
        If Not OvrigaBokslutsdispositionerRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaBokslutsdispositioner")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaBokslutsdispositionerRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga bokslutsdispositioner Period -1
        If Not OvrigaBokslutsdispositionerRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaBokslutsdispositioner")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaBokslutsdispositionerRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa bokslutsdispositioner Period 0
        If Not SummaBokslutsdispositionerRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:BokslutsdispositionerSumma")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(SummaBokslutsdispositionerRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Summa bokslutsdispositioner Period -1
        If Not SummaBokslutsdispositionerRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:BokslutsdispositionerSumma")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(SummaBokslutsdispositionerRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fore skatt Period 0
        If Not ResultatForeSkattRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatForeSkatt")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatForeSkattRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Resultat fore skatt Period -1
        If Not ResultatForeSkattRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:ResultatForeSkatt")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(ResultatForeSkattRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Skatt pa arets resultat Period 0
        If Not SkattPaAretsResultatRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:SkattAretsResultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(SkattPaAretsResultatRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Skatt pa arets resultat Period -1
        If Not SkattPaAretsResultatRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:SkattAretsResultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(SkattPaAretsResultatRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga skatter Period 0
        If Not OvrigaSkatterRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaSkatter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaSkatterRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Ovriga skatter Period -1
        If Not OvrigaSkatterRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:OvrigaSkatter")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(OvrigaSkatterRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Arets resultat Period 0
        If Not AretsResultatRES0.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:AretsResultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(AretsResultatRES0.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Resultatrakning Arets resultat Period -1
        If Not AretsResultatRESm1.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:AretsResultat")
            textWriter.WriteAttributeString("decimals", txtDecimals.Text)
            textWriter.WriteAttributeString("contextRef", "PERIOD-1")
            textWriter.WriteAttributeString("unitRef", "valuta")
            textWriter.WriteString(AretsResultatRESm1.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Ort for untertecknande
        If Not OrtUndertecknande.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:UndertecknandeAvArsredovisningOrt")
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteString(OrtUndertecknande.Text)
            textWriter.WriteEndElement()
        End If

        'Insert Elements for Datum for untertecknande
        If Not DatumUndertecknande.Text = "" Then
            textWriter.WriteStartElement("se-gen-base:UndertecknandeAvArsredovisningDatum")
            textWriter.WriteAttributeString("contextRef", "PERIOD0")
            textWriter.WriteString(DatumUndertecknande.Text)
            textWriter.WriteEndElement()
        End If

        'End Everything
        textWriter.WriteEndDocument()

        'Clean up
        textWriter.Flush()
        textWriter.Close()

        ' IF WE WANT THE USER TO BE ABLE TO DOWNLOAD THE XML-FILE directly (uncomment the code below)
        'Response.ContentType = "Application/xbrl"
        'Response.AppendHeader("Content-Disposition", "attachment; filename=Report.xbrl")
        'Response.Flush()
        'Response.Close()

        ' Redirect the user to a new webpage
        Server.Transfer("CreateXMLlXBRL.aspx")
    End Sub
    Protected Sub txtBeloppsformat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBeloppsformat.SelectedIndexChanged
        If txtBeloppsformat.SelectedValue = "NORMALFORM" Then
            txtDecimals.Text = "INF"
        Else
            txtDecimals.Text = "-3"
        End If
    End Sub

End Class