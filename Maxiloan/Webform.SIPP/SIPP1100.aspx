﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP1100.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP1100"  MaintainScrollPositionOnPostback="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>LAPORAN KEUANGAN BULANAN PERUSAHAAN PEMBIAYAAN</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>  
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        $("#tabs").tabs({ selected: 1 });
    
    </script>  
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                   SIPP1100 - LAPORAN KEUANGAN BULANAN PERUSAHAAN PEMBIAYAAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">
                <%--untuk eksport--%>
                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                  <div class="form_box_header">
                    </div>
                     <%-- <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>--%>
                    <div class="form_box">
                        <div class="form_single">
                        <label>
                            Pilih Bulan Generate
                        </label>
                        <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                            TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        </div>
                   </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>
                <%--end--%>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle  Width="2%" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle CssClass="command_col" Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" Visible="false">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA " HeaderText="BULANDATA" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="ST_TTL" SortExpression="ST_TTL " HeaderText="Total Aset" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="LBLTS_DN_KTS_TTL" SortExpression="LBLTS_DN_KTS_TTL " HeaderText="Total Liabilitas dan Ekuitas" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                     
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP1100" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <%--untuk eksport--%>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button> 
                    <%--end--%>
                </div>

            </asp:Panel>
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>
            
        <%--ASET--%>

                <div class ="form_title">
                     <div class ="form_single">
                         <h3>
                         <b>ASET</b>
                         </h3> 
                    </div>
                </div>
                <%--untuk eksport--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator136" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--end--%>
                <div class="form_box">
                <div class="form_title">
                    <h4> <b>1. Kas dan Setara Kas</b> </h4>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <asp:Label ID="Label85" runat="server" Text="Dalam Rupiah"></asp:Label>&nbsp &nbsp &nbsp&nbsp 
                        <asp:TextBox ID="txtKS_DN_STR_KS_NDNSN_RPH" runat="server" CssClass="" Width="20%"  enabled="false" AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKS_DN_STR_KS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <asp:Label ID="Label86" runat="server" Text="Valas"></asp:Label> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 
                        <asp:TextBox ID="txtKS_DN_STR_KS_MT_NG_SNG" runat="server" CssClass="" Width="20%"  enabled="false" AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                        ControlToValidate="txtKS_DN_STR_KS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <asp:Label ID="Label87" runat="server" Text="Total"></asp:Label> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp&nbsp &nbsp &nbsp&nbsp 
                        <asp:TextBox ID="txtKS_DN_STR_KS_TTL" runat="server" CssClass="" Width="20%"  enabled="false" AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                        ControlToValidate="txtST_NDNSN_RPH_LBH_DR_6_BLN_DN_KRNG_DR_T_SM_DNGN_1_THN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label23" runat="server" Text="Kas"></asp:Label>
                        </b>        
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtKS_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtKS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtKS_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label24" runat="server" Text="Simpanan Bank Dalam Negeri"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PD_BNK_DLM_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PD_BNK_DLM_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSMPNN_PD_BNK_DLM_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PD_BNK_DLM_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                          <label>Giro Pada Bank Dalam Negeri</label>
                          <br /><br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtGR_PD_BNK_DLM_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                        ControlToValidate="txtGR_PD_BNK_DLM_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtGR_PD_BNK_DLM_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                        ControlToValidate="txtGR_PD_BNK_DLM_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtGR_PD_BNK_DLM_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                        ControlToValidate="txtGR_PD_BNK_DLM_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                        
                    </div>
                    <div class="form_right">
                          <label>Simpanan Lainnya Pada Bank Luar Negeri</label>
                          <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtSMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSMPNN_LNNY_PD_BNK_DLM_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtSMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSMPNN_LNNY_PD_BNK_DLM_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtSMPNN_LNNY_PD_BNK_DLM_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSMPNN_LNNY_PD_BNK_DLM_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label1" runat="server" Text="Simpanan Bank Luar Negeri"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PD_BNK_LR_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PD_BNK_LR_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSMPNN_PD_BNK_LR_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PD_BNK_LR_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left"> 
                          <label>Giro Pada Bank Luar Negeri</label>
                          <br /><br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtGR_PD_BNK_LR_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                        ControlToValidate="txtGR_PD_BNK_LR_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtGR_PD_BNK_LR_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                        ControlToValidate="txtGR_PD_BNK_LR_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtGR_PD_BNK_LR_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                        ControlToValidate="txtGR_PD_BNK_LR_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                        
                    </div>
                    <div class="form_right">
                          <label>Simpanan Lainnya Pada Bank Luar Negeri</label>
                          <br />
                        <label>Dalam Rupiah</label>
                        <asp:TextBox ID="txtSMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSMPNN_LNNY_PD_BNK_LR_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br /> 
                        <label>Valas</label>
                        <asp:TextBox ID="txtSMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSMPNN_LNNY_PD_BNK_LR_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtSMPNN_LNNY_PD_BNK_LR_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                        ControlToValidate="txtSMPNN_LNNY_PD_BNK_LR_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div> 
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label2" runat="server" Text="2. Tagihan Derivatif"></asp:Label>
                        </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtST_TGHN_DRVTF_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TGHN_DRVTF_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtST_TGHN_DRVTF_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TGHN_DRVTF_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtST_TGHN_DRVTF_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TGHN_DRVTF_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div> 
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label3" runat="server" Text="3. Investasi Jangka Pendek Dalam Surat Berharga"></asp:Label>
                        </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                         ControlToValidate="txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                         ControlToValidate="txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtNVSTS_JNGK_PNDK_DLM_SRT_BRHRG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label4" runat="server" Text="4. Piutang Pembiayaan - Neto"></asp:Label>
                        </b>        
                    </div>
                </div>  
                    <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPTNG_PMBYN__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPTNG_PMBYN__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPTNG_PMBYN__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label81" runat="server" Text="Piutang Pembiayaan Konvensional - Neto"></asp:Label>
                        </b>        
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_KNVNSNL__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_KNVNSNL__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_KNVNSNL__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_KNVNSNL__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label6" runat="server" Text="A. Piutang Pembiayaan Konvensional - Investasi Neto"></asp:Label>
                        </b>        
                    </div>
                </div> 
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_NVSTS__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_NVSTS__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_NVSTS__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_NVSTS__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>
                <div class="form_box"> 
                    <div class="form_left">  
                        <div class="form_single">
                            <asp:Label ID="Label7" runat="server" Text="Piutang Pembiayaan Investasi - Pokok"></asp:Label>
                            <br />
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS__PKK_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS__PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS__PKK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS__PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS__PKK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS__PKK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div> 
                    <div class="form_right"> 
                            <div class="form_single">
                                <asp:Label ID="Label8" runat="server" Text="Cadangan Piutang Pembiayaan Investasi"></asp:Label>
                                <br />
                                <label>Dalam Rupiah</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_NVSTS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br /> 
                                <label>Valas</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_NVSTS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br />
                                <label class="label_req">Total</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_NVSTS_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_NVSTS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                                </div>
                            </div> 
                    </div>  
                <div class="form_box"> &nbsp &nbsp &nbsp 
                <b><asp:Label ID="Label5" runat="server" Text="  Piutang Pembiayaan Konvensional - Modal Kerja Neto"></asp:Label></b>
                <br /> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_MDL_KRJ__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_MDL_KRJ__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_MDL_KRJ__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_MDL_KRJ__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>
                <div class="form_box"> 
                    <div class="form_left">  
                        <div class="form_single">
                            <asp:Label ID="Label9" runat="server" Text="Piutang Pembiayaan Modal Kerja - Pokok"></asp:Label>
                            <br />
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_MDL_KRJ__PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_MDL_KRJ__PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_MDL_KRJ__PKK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_MDL_KRJ__PKK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div> 
                    <div class="form_right"> 
                            <div class="form_single">
                                <asp:Label ID="Label10" runat="server" Text="Cadangan Piutang Modal Kerja"></asp:Label>
                                <br />
                                <label>Dalam Rupiah</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_MDL_KRJ_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_MDL_KRJ_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br /> 
                                <label>Valas</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_MDL_KRJ_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_MDL_KRJ_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br />
                                <label class="label_req">Total</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_MDL_KRJ_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_MDL_KRJ_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                                </div>
                            </div> 
                    </div>
                <div class="form_box"> &nbsp &nbsp &nbsp
                <b><asp:Label ID="Label11" runat="server" Text="  Piutang Pembiayaan Konvelsional - Multiguna Neto"></asp:Label></b>
                <br /> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_MLTGN__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_MLTGN__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_MLTGN__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_MLTGN__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>
                <div class="form_box"> 
                    <div class="form_left">  
                        <div class="form_single">
                            <asp:Label ID="Label12" runat="server" Text="Piutang Pembiayaan Multiguna - Pokok"></asp:Label>
                            <br />
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_MLTGN__PKK_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_MLTGN__PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_MLTGN__PKK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_MLTGN__PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_MLTGN__PKK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_MLTGN__PKK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div> 
                    <div class="form_right"> 
                            <div class="form_single">
                                <asp:Label ID="Label13" runat="server" Text="Cadangan Piutang Pembiayaan Multiguna"></asp:Label>
                                <br />
                                <label>Dalam Rupiah</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_MLTGN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br /> 
                                <label>Valas</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_MLTGN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br />
                                <label class="label_req">Total</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_MLTGN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_MLTGN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                                </div>
                            </div> 
                    </div>  
                <div class="form_box"> &nbsp &nbsp &nbsp
                <b><asp:Label ID="Label14" runat="server" Text="  Piutang Pembiayaan Konvensional - Lainnya Berdasarkan Persetujuan OJK Neto"></asp:Label></b>
                <br /> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>
                <div class="form_box"> 
                    <div class="form_left">  
                        <div class="form_single">
                            <asp:Label ID="Label15" runat="server" Text="Piutang Pembiayaan Lainnya – Pokok"></asp:Label>
                            <br />
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator46" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK__PKK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div> 
                    <div class="form_right"> 
                            <div class="form_single">
                                <asp:Label ID="Label16" runat="server" Text="Cadangan Piutang Pembiayaan Lainnya"></asp:Label>
                                <br />
                                <label>Dalam Rupiah</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator47" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br /> 
                                <label>Valas</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator48" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br />
                                <label class="label_req">Total</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_LNNY_BRDSRKN_PRSTJN_JK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                                </div>
                            </div> 
                    </div>
                <div class="form_box"> &nbsp &nbsp 
                <b><asp:Label ID="Label84" runat="server" Text="Piutang Pembiayaan Berdasarkan Prinsip Syariah : "></asp:Label></b>
                <br /> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator49" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator50" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_BRDSRKN_PRNSP_SYRH__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>  
                <div class="form_box"> &nbsp &nbsp 
                <b><asp:Label ID="Label17" runat="server" Text="B. Piutang Pembiayaan Syariah - Jual Beli Berdasarkan Prinsip Syariah Neto"></asp:Label></b>
                <br /> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator51" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                </div>
                <div class="form_box"> 
                    <div class="form_left">  
                        <div class="form_single">
                            <asp:Label ID="Label18" runat="server" Text="Piutang Pembiayaan Jual Beli Berdasarkan Prinsip Syariah - Pokok"></asp:Label>
                            <br />
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator53" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator54" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH__PKK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div> 
                    <div class="form_right"> 
                            <div class="form_single">
                                <asp:Label ID="Label19" runat="server" Text="Cadangan Piutang Pembiayaan Jual Beli Berdasarkan Prinsip Syariah"></asp:Label>
                                <br />
                                <label>Dalam Rupiah</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br /> 
                                <label>Valas</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br />
                                <label class="label_req">Total</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_JL_BL_BRDSRKN_PRNSP_SYRH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                                </div>
                            </div> 
                    </div> 
                <div class="form_box"> &nbsp &nbsp &nbsp
                <b><asp:Label ID="Label20" runat="server" Text="  Piutang Pembiayaan Syariah - Investasi Berdasarkan Prinsip Syariah - Neto"></asp:Label></b>
                <br /> 
                </div>
                    <div class="form_box">
                    <div class="form_left"> 
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator58" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div>
                <div class="form_box"> 
                    <div class="form_left">  
                        <div class="form_single">
                            <asp:Label ID="Label21" runat="server" Text="Piutang Pembiayaan Investasi Berdasarkan Prinsip Syariah – Pokok"></asp:Label>
                            <br />
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH__PKK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div> 
                    <div class="form_right"> 
                            <div class="form_single">
                                <asp:Label ID="Label22" runat="server" Text="Cadangan Piutang Pembiayaan Investasi Berdasarkan Prinsip Syariah"></asp:Label>
                                <br />
                                <label>Dalam Rupiah</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br /> 
                                <label>Valas</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator62" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br />
                                <label class="label_req">Total</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_NVSTS_BRDSRKN_PRNSP_SYRH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                                </div>
                            </div> 
                    </div> 
                <div class="form_box"> &nbsp &nbsp &nbsp
                <b><asp:Label ID="Label25" runat="server" Text="  Piutang Pembiayaan Syariah - Jasa Berdasarkan Prinsip Syariah Neto"></asp:Label></b>
                <br /> 
                </div>
                    <div class="form_box">  
                        <div class="form_left"> 
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator63" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator64" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div> 
                <div class="form_box"> 
                    <div class="form_left">  
                        <div class="form_single">
                            <asp:Label ID="Label26" runat="server" Text="Piutang Pembiayaan Jasa Berdasarkan Prinsip Syariah - Pokok"></asp:Label>
                            <br />
                            <label>Dalam Rupiah</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator65" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br /> 
                            <label>Valas</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            <br />
                            <label class="label_req">Total</label>
                            <asp:TextBox ID="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH__PKK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                            </div>
                        </div> 
                    <div class="form_right"> 
                            <div class="form_single">
                                <asp:Label ID="Label27" runat="server" Text="Cadangan Piutang Pembiayaan Jasa Berdasarkan Prinsip Syariah"></asp:Label>
                                <br />
                                <label>Dalam Rupiah</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" runat="server" CssClass="" Width="20%"   AutoPostBack="true" OnTextChanged="txtSUM"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br /> 
                                <label>Valas</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%"  AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator68" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                                <br />
                                <label class="label_req">Total</label>
                                <asp:TextBox ID="txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                                ControlToValidate="txtCDNGN_PTNG_PMBYN_JS_BRDSRKN_PRNSP_SYRH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                                </div>
                            </div> 
                    </div>  
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label28" runat="server" Text="5. Penyertaan Modal"></asp:Label>
                        </b>        
                    </div>
                </div>
                     <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator69" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator70" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                         </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label29" runat="server" Text="Penyertaan Modal Pada Bank"></asp:Label>
                        </b>        
                    </div> 
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label30" runat="server" Text="Penyertaan Modal Pada Perusahaan Jasa Keuangan Lainnya"></asp:Label>
                        </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">  
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_BNK_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator71" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_BNK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_BNK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator72" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_BNK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_BNK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_BNK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>  
                    </div>
                    <div class="form_right">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator73" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator74" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_PRSHN_JS_KNGN_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label31" runat="server" Text="Penyertaan Modal Pada Perusahaan Bukan Jasa Keuangan"></asp:Label>
                        </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator75" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator76" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNYRTN_MDL_PD_PRSHN_BKN_JS_KNGN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label32" runat="server" Text="6. Investasi Jangka Panjang Dalam Surat Berharga"></asp:Label>
                        </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator77" runat="server" ErrorMessage="*"
                         ControlToValidate="txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator78" runat="server" ErrorMessage="*"
                         ControlToValidate="txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtNVSTS_JNGK_PNJNG_DLM_SRT_BRHRG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                </div> 
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label33" runat="server" Text="7. Aset yang Disewa operasikan – Neto"></asp:Label>
                        </b>        
                    </div>
                </div>
                    <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtST_YNG_DSWPRSKN__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator79" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_YNG_DSWPRSKN__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtST_YNG_DSWPRSKN__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator80" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_YNG_DSWPRSKN__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtST_YNG_DSWPRSKN__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_YNG_DSWPRSKN__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                        </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label34" runat="server" Text="Aset yang Disewa operasikan"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label35" runat="server" Text="Akumulasi penyusutan Aset yang Disewa operasikan - / -"></asp:Label>
                        </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtST_YNG_DSWPRSKN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator81" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_YNG_DSWPRSKN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtST_YNG_DSWPRSKN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator82" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_YNG_DSWPRSKN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtST_YNG_DSWPRSKN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_YNG_DSWPRSKN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator83" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator84" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMLS_PNYSTN_ST_YNG_DSWPRSKN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label36" runat="server" Text="8. Aset Tetap dan Inventaris – Neto"></asp:Label>
                        </b>        
                    </div>
                </div>
                    <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator85" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TTP_DN_NVNTRS__NT_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator86" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TTP_DN_NVNTRS__NT_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtST_TTP_DN_NVNTRS__NT_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TTP_DN_NVNTRS__NT_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label37" runat="server" Text="Aset tetap dan inventaris"></asp:Label>
                        </b>        
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label38" runat="server" Text="Akumulasi penyusutan Aset tetap dan Inventaris - / -"></asp:Label>
                        </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtST_TTP_DN_NVNTRS__NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator87" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TTP_DN_NVNTRS__NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtST_TTP_DN_NVNTRS__MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TTP_DN_NVNTRS__MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtST_TTP_DN_NVNTRS__TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TTP_DN_NVNTRS__TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator89" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator90" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMLS_PNYSTN_ST_TTP_DN_NVNTRS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label39" runat="server" Text="9. Aset Pajak Tangguhan"></asp:Label>
                        </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtST_PJK_TNGGHN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator91" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_PJK_TNGGHN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtST_PJK_TNGGHN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator92" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_PJK_TNGGHN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtST_PJK_TNGGHN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_PJK_TNGGHN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                </div> 
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label40" runat="server" Text="10. Rupa-rupa Aset"></asp:Label>
                        </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtRPRP_ST_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator93" runat="server" ErrorMessage="*"
                         ControlToValidate="txtRPRP_ST_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtRPRP_ST_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator94" runat="server" ErrorMessage="*"
                         ControlToValidate="txtRPRP_ST_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtRPRP_ST_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtRPRP_ST_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                </div> 
                <div class="form_title">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label41" runat="server" Text="TOTAL ASET = "></asp:Label>
                        </b>        
                    </div>
                </div>
                    <div class="form_box">
                    <div class="form_left">   
                        <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtST_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator95" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtST_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator96" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtST_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtST_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>   
                    </div> 
                </div>
                </div>
                
                 <div class ="form_title">
                     <div class ="form_single">
                         <h3>
                         <b>LIABILITAS DAN EKUITAS</b>
                         </h3> 
                    </div>
                </div>
                <div class="form_box">
                <div class="form_title">
                    <h4> <b>1. Liabilitas Segera</b> </h4>
                </div>
                    <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLBLTS_SGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator97" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_SGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLBLTS_SGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator98" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_SGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLBLTS_SGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_SGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                        </div>
                <div class="form_box">
                    <div class="form_left">
                        <b> <asp:Label ID="Label42" runat="server" Text="Liabilitas Kepada Bank"></asp:Label> </b>        
                    </div>
                    <div class="form_right">
                        <b> <asp:Label ID="Label43" runat="server" Text="Liabilitas Kepada Perusahaan Jasa Keuangan Lainnya"></asp:Label> </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLBLTS_KPD_BNK_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator99" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_BNK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLBLTS_KPD_BNK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator100" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_BNK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLBLTS_KPD_BNK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_BNK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator101" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator102" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_PRSHN_JS_KNGN_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                    <div class="form_box">
                    <div class="form_left">
                        <b> <asp:Label ID="Label44" runat="server" Text="Liabilitas Kepada Perusahaan Bukan Jasa Keuangan"></asp:Label> </b>        
                    </div>
                    <div class="form_right">
                        <b> <asp:Label ID="Label45" runat="server" Text="Liabilitas Segera Lainnya"></asp:Label> </b>        
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator103" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator104" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_KPD_PRSHN_BKN_JS_KNGN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLBLTS_SGR_LNNY_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator105" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_SGR_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLBLTS_SGR_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator106" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_SGR_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLBLTS_SGR_LNNY_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_SGR_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_title">
                <h4> <b>2. Liabilitas derivatif</b> </h4>
                </div> 
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLBLTS_DRVTF_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator107" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_DRVTF_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLBLTS_DRVTF_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator108" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_DRVTF_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLBLTS_DRVTF_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_DRVTF_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>3. Utang Pajak</b> </h4>
                </div> 
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtTNG_PJK_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator109" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTNG_PJK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtTNG_PJK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator110" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTNG_PJK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtTNG_PJK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTNG_PJK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>4. Pinjaman Yang Diterima</b> </h4>
                </div> 
                     <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator111" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator112" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                         </div>
                <div class="form_box">
                    <div class="form_single">
                        <b> <asp:Label ID="Label46" runat="server" Text="A. Pinjaman Yang Diterima Dalam Negeri"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator113" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DLM_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator114" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DLM_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DLM_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DLM_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                         </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label47" runat="server" Text="Pinjaman Yang Diterima Dari Bank Dalam Negeri"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator115" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator116" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_BNK_DLM_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label48" runat="server" Text="Pinjaman Yang Diterima Dari Lembaga Jasa Keuangan Non Bank Dalam Negeri"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator117" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator118" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_DLM_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                    <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label49" runat="server" Text="Pinjaman Yang Diterima Lainnya Dalam Negeri"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator119" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator120" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LNNY_DLM_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                    <div class="form_box">
                    <div class="form_single">
                        <b> <asp:Label ID="Label50" runat="server" Text="B. Pinjaman Yang Diterima Dari Luar Negeri"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator121" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LR_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator122" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LR_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LR_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LR_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label51" runat="server" Text="Pinjaman Yang Diterima Dari Bank Luar Negeri"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator123" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator124" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_BNK_LR_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label52" runat="server" Text="Pinjaman Yang Diterima Dari Lembaga Jasa Keuangan Non Bank Luar Negeri"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator125" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator126" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_DR_LMBG_JS_KNGN_BKN_BNK_LR_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                    <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label53" runat="server" Text="Pinjaman Yang Diterima Lainnya Luar Negeri"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator127" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LNNY_LR_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator128" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LNNY_LR_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNDNN_YNG_DTRM_LNNY_LR_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNDNN_YNG_DTRM_LNNY_LR_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>5. Surat Berharga Yang Diterbitkan</b> </h4>
                </div> 
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator129" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSRT_BRHRG_YNG_DTRBTKN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator130" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSRT_BRHRG_YNG_DTRBTKN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSRT_BRHRG_YNG_DTRBTKN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSRT_BRHRG_YNG_DTRBTKN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>6. Liabilitas Pajak Tangguhan</b> </h4>
                </div> 
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLBLTS_PJK_TNGGHN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator131" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_PJK_TNGGHN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLBLTS_PJK_TNGGHN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator132" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_PJK_TNGGHN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLBLTS_PJK_TNGGHN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_PJK_TNGGHN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>7. Pinjaman Subordinasi</b> </h4>
                </div> 
                    <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator133" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator134" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                        </div>
                <div class="form_box">
                    <div class="form_left">
                        <b> <asp:Label ID="Label54" runat="server" Text="Pinjaman Subordinasi Dalam Negeri"></asp:Label> </b>        
                    </div>
                    <div class="form_right">
                        <b> <asp:Label ID="Label55" runat="server" Text="Pinjaman Subordinasi Luar Negeri"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_DLM_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator135" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_DLM_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_DLM_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidato136" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_DLM_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_DLM_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_DLM_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                    <div class="form_right">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_LR_NGR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator137" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_LR_NGR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_LR_NGR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator138" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_LR_NGR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtPNJMN_SBRDNS_LR_NGR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtPNJMN_SBRDNS_LR_NGR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>8. Rupa-rupa Liabilitas</b> </h4>
                </div> 
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtRPRP_LBLTS_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator139" runat="server" ErrorMessage="*"
                         ControlToValidate="txtRPRP_LBLTS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtRPRP_LBLTS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator140" runat="server" ErrorMessage="*"
                         ControlToValidate="txtRPRP_LBLTS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtRPRP_LBLTS_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtRPRP_LBLTS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>9. Modal</b> </h4>
                </div> 
                    <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtMDL_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator141" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtMDL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator142" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtMDL_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    </div>
                <div class="form_box">
                    <div class="form_single">
                        <b> <asp:Label ID="Label56" runat="server" Text="A. Modal Disetor"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtMDL_DSTR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator143" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_DSTR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtMDL_DSTR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator144" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_DSTR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtMDL_DSTR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_DSTR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label57" runat="server" Text="Modal Dasar"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtMDL_DSR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator145" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_DSR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtMDL_DSR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator146" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_DSR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtMDL_DSR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_DSR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label58" runat="server" Text="Modal Yang Belum Disetor -/-"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtMDL_YNG_BLM_DSTR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator147" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_YNG_BLM_DSTR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtMDL_YNG_BLM_DSTR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator148" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_YNG_BLM_DSTR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtMDL_YNG_BLM_DSTR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_YNG_BLM_DSTR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b> <asp:Label ID="Label59" runat="server" Text="B. Simpanan Pokok dan Simpanan Wajib"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator149" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PKK_DN_SMPNN_WJB_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator150" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PKK_DN_SMPNN_WJB_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSMPNN_PKK_DN_SMPNN_WJB_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label60" runat="server" Text="Simpanan Pokok"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSMPNN_PKK_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator151" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PKK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSMPNN_PKK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator152" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PKK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSMPNN_PKK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_PKK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label61" runat="server" Text="Simpanan Wajib"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSMPNN_WJB_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator153" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_WJB_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSMPNN_WJB_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator154" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_WJB_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSMPNN_WJB_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSMPNN_WJB_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b> <asp:Label ID="Label62" runat="server" Text="C. Tambahan Modal Disetor"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtTMBHN_MDL_DSTR_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator155" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTMBHN_MDL_DSTR_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtTMBHN_MDL_DSTR_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator156" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTMBHN_MDL_DSTR_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtTMBHN_MDL_DSTR_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTMBHN_MDL_DSTR_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                        </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label63" runat="server" Text="Agio"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtG_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator157" runat="server" ErrorMessage="*"
                         ControlToValidate="txtG_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtG_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator158" runat="server" ErrorMessage="*"
                         ControlToValidate="txtG_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtG_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label64" runat="server" Text="Disagio -/-"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtDSG_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator159" runat="server" ErrorMessage="*"
                         ControlToValidate="txtDSG_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtDSG_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator160" runat="server" ErrorMessage="*"
                         ControlToValidate="txtDSG_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtDSG_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtDSG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label65" runat="server" Text="Modal Saham Diperoleh Kembali -/-"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtMDL_SHM_DPRLH_KMBL_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator161" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_SHM_DPRLH_KMBL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtMDL_SHM_DPRLH_KMBL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator162" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_SHM_DPRLH_KMBL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtMDL_SHM_DPRLH_KMBL_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_SHM_DPRLH_KMBL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label66" runat="server" Text="Biaya Emisi Efek Ekuitas"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtBY_MS_FK_KTS_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator163" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBY_MS_FK_KTS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtBY_MS_FK_KTS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator164" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBY_MS_FK_KTS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtBY_MS_FK_KTS_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtBY_MS_FK_KTS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label67" runat="server" Text="Modal Hibah"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtMDL_HBH_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator165" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_HBH_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtMDL_HBH_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator166" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_HBH_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtMDL_HBH_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtMDL_HBH_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label68" runat="server" Text="Lainnya"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtTMBHN_MDL_DSTR_LNNY_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator167" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTMBHN_MDL_DSTR_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtTMBHN_MDL_DSTR_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator168" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTMBHN_MDL_DSTR_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtTMBHN_MDL_DSTR_LNNY_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtTMBHN_MDL_DSTR_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b> <asp:Label ID="Label69" runat="server" Text="D. Selisih Nilai Transaksi Restrukturisasi Entitas Sepengendali"></asp:Label> </b>        
                    </div> 
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label70" runat="server" Text="Simpanan Pokok"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator169" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator170" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLSH_NL_TRNSKS_RSTRKTRSS_NTTS_SPNGNDL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    </div>
                <div class="form_title">
                <h4> <b>10. Cadangan</b> </h4>
                </div> 
                    <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtCDNGN_MDL_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator171" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_MDL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtCDNGN_MDL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator172" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_MDL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtCDNGN_MDL_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_MDL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                        </div>
                <div class="form_box">
                    <div class="form_left">
                        <b> <asp:Label ID="Label71" runat="server" Text="Cadangan Umum"></asp:Label> </b>        
                    </div> 
                    <div class="form_right">
                        <b> <asp:Label ID="Label72" runat="server" Text="Cadangan Tujuan"></asp:Label> </b>        
                    </div> 
                </div>
      
                <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtCDNGN_MM_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator173" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_MM_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtCDNGN_MM_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator174" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_MM_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtCDNGN_MM_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_MM_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtCDNGN_TJN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator175" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_TJN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtCDNGN_TJN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator176" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_TJN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtCDNGN_TJN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtCDNGN_TJN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_title">
                <h4> <b>11. Saldo Laba (Rugi) Yang Ditahan</b> </h4>
                </div> 
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSLD_LB_RG_YNG_DTHN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator177" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_LB_RG_YNG_DTHN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSLD_LB_RG_YNG_DTHN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator178" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_LB_RG_YNG_DTHN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSLD_LB_RG_YNG_DTHN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_LB_RG_YNG_DTHN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>12. Laba (Rugi) Bersih Setelah Pajak</b> </h4>
                </div> 
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator179" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_BRSH_STLH_PJK_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator180" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_BRSH_STLH_PJK_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLB_RG_BRSH_STLH_PJK_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLB_RG_BRSH_STLH_PJK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_title">
                <h4> <b>13. Komponen Ekuitas Lainnya</b> </h4>
                </div> 
                     <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label83" runat="server" Text="Saldo Keuntungan (Kerugian) Akibat Perubahan Dalam Surplus Revaluasi Aset Tetap"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtKMPNN_KTS_LNNY_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator181" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMPNN_KTS_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtKMPNN_KTS_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator182" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMPNN_KTS_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtKMPNN_KTS_LNNY_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKMPNN_KTS_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                        </div>
                <div class="form_box">
                    <div class="form_single">
                        <b> <asp:Label ID="Label73" runat="server" Text="A. Saldo Komponen Ekuitas Lainnya"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label82" runat="server" Text="Saldo Keuntungan (Kerugian) Akibat Perubahan Dalam Surplus Revaluasi Aset Tetap"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator183" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_KMPNN_KTS_LNNY_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator184" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_KMPNN_KTS_LNNY_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSLD_KMPNN_KTS_LNNY_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_KMPNN_KTS_LNNY_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                        </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label74" runat="server" Text="Saldo Keuntungan (Kerugian) Akibat Perubahan Dalam Surplus Revaluasi Aset Tetap"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator185" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator186" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PRBHN_DLM_SRPLS_RVLS_ST_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label75" runat="server" Text="Saldo Keuntungan (Kerugian) Akibat Selisih Kurs Karena Penjabaran Laporan Keuangan Dalam Mata Uang Asing"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator187" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator188" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_SLSH_KRS_KRN_PNJBRN_LPRN_KNGN_DLM_MT_NG_SNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label76" runat="server" Text="Saldo Keuntungan (Kerugian) Akibat Pengukuran Kembali Aset Keuangan Tersedia Untuk Dijual"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator189" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator190" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_PNGKRN_KMBL_ST_KNGN_TRSD_NTK_DJL_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                    <div class="form_right">
                         <asp:Label ID="Label77" runat="server" Text="Saldo Keuntungan (Kerugian) Akibat Bagian Efektif Instrumen Keuangan Lindung Nilai Dalam Rangka Lindung Nilai Arus Kas"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator191" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator192" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_KBT_BGN_FKTF_NSTRMN_KNGN_LNDNG_NL_DLM_RNGK_LNDNG_NL_RS_KS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <asp:Label ID="Label78" runat="server" Text="Saldo Keuntungan (Kerugian) Atas Komponen Ekuitas Lainnya Sesuai Prinsip Standar Akuntansi Keuangan"></asp:Label><br />
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator193" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator194" runat="server" ErrorMessage="*"
                         ControlToValidate="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtSLD_PRD_SBLMNY_KNTNGN_KRGN_TS_KMPNN_KTS_LNNY_SS_PRNSP_STNDR_KNTNS_KNGN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b> <asp:Label ID="Label79" runat="server" Text="B. Keuntungan (Kerugian) Komperehensif Lainnya Periode Berjalan"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left"> 
                         <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator195" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator196" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtKNTNGN_KRGN_PNDPTN_KMPRHNSF_LNNY_THN_BRJLN_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%> 
                    </div> 
                </div>
                    <div class="form_title">
                    <div class="form_single">
                        <b> <asp:Label ID="Label80" runat="server" Text="Total Liabilitas dan Ekuitas"></asp:Label> </b>        
                    </div> 
                </div>
                <div class="form_box">
                    <div class="form_left">   
                        <label class="label_req">Dalam Rupiah</label>
                         <asp:TextBox ID="txtLBLTS_DN_KTS_NDNSN_RPH" runat="server" CssClass="" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator197" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_DN_KTS_NDNSN_RPH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Valas</label>
                         <asp:TextBox ID="txtLBLTS_DN_KTS_MT_NG_SNG" runat="server" CssClass="medium_text" Width="20%" AutoPostBack="true" OnTextChanged="txtSUM" ></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator198" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_DN_KTS_MT_NG_SNG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtLBLTS_DN_KTS_TTL" runat="server" CssClass="medium_text" Width="20%" Enabled="false" ></asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtLBLTS_DN_KTS_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>   
                    </div> 
                </div>

                    </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                   <%-- <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>--%>
                </div>
            </asp:Panel>
    </form>
</body>
</html>
