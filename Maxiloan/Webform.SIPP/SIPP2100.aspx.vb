﻿#Region "Imports"
Imports System.IO
Imports Maxiloan.cbse
Imports Maxiloan.Exceptions
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports System.Data.SqlClient
Imports Maxiloan.General
Imports Maxiloan.General.CommonCookiesHelper
Imports ClosedXML.Excel
Imports Maxiloan.Framework.SQLEngine
'
#End Region

Public Class SIPP2100
	Inherits Webform.WebBased

#Region "Constanta"
	Private oController As New Controller.SIPP2100Controller
    Private oCustomclass As New Parameter.SIPP2100
    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property"
    Private Property Process() As String
        Get
            Return CType(ViewState("Process"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Process") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
#End Region

    Protected WithEvents GridNavi As ucGridNav

    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        AddHandler GridNavi.PageChanged, AddressOf PageNavigation
        If SessionInvalid() Then
            Exit Sub
        End If
		If Not IsPostBack Then
			Me.FormID = "SIPP2100"
			If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
				pnlList.Visible = True
				pnlAdd.Visible = False
				pnlcopybulandata.Visible = False
				'
				Me.SearchBy = ""
				Me.CmdWhere = "ALL"
				Me.SortBy = ""
				DoBind(Me.SearchBy, Me.SortBy)
				FillCombo()
				FillCboBulanData(cbobulandata, "TblBulanDataSIPP")
			End If
		End If
		pnlcopybulandata.Visible = False
		btnGenerateUlang.Visible = False
	End Sub

    Private Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        DoBind(Me.SearchBy, Me.SortBy, True)
        GridNavi.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

#Region "DoBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String, Optional isFrNav As Boolean = False)
        Dim dtEntity As DataTable = Nothing
        Dim oParameter As New Maxiloan.Parameter.SIPP2100

        oParameter.strConnection = GetConnectionString()
        oParameter.WhereCond = cmdWhere
        oParameter.CurrentPage = currentPage
        oParameter.PageSize = pageSize
        oParameter.SortBy = SortBy
        oParameter = oController.GetSIPP2100(oParameter)


        If Not oParameter Is Nothing Then
            dtEntity = oParameter.ListData
            recordCount = oParameter.TotalRecord
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            'btnGenerate.Enabled = False
        Else
            'btnGenerate.Enabled = True
        End If
        dtgList.DataSource = dtEntity.DefaultView
        dtgList.CurrentPageIndex = 0
        dtgList.DataBind()
        FillCombo()
        FillCbo(cboJNS_PMBYN, 19, "", "")
        FillCbo(cboSKM_PMBYN, 26, "", "")
        FillCbo(cboTJN_PMBYN, 35, "", "")
		FillCbo(cboJNS_BRNG_DN_JS, "", "", 8)
		FillCbo(cboJNS_BNGMRGNBG_HSLMBL_JS, 16, "", "")
        FillCbo(cboSMBR_PMBYN, 36, "", "")
        FillCbo(cboKLTS, 17, "", "")
        FillCbo(cboJNS_MT_NG, "", "", 2)
        FillCbo(cboKTGR_SH_DBTR, 20, "", "")
        FillCbo(cboGLNGN_PHK_LWN, "", "", 7)
        FillCbo(cboSTTS_KTRKTN, 18, "", "")
        FillCbo(cboSKTR_KNM_LPNGN_SH, "", "", "4")
        FillCbo(cboLKS_DT__PRYK, "", "", 1)
		FillCbo(cboJNS_GNN, "", "", 8)

		If (isFrNav = False) Then
            GridNavi.Initialize(recordCount, pageSize)
        End If

		If recordCount = 0 Then
			ShowMessage(lblMessage, "Data tidak ditemukan .....", True)
			If txtSearch.Text <> "" Or cbobulandata.SelectedIndex = 0 Then
				pnlcopybulandata.Visible = False
			Else
				pnlcopybulandata.Visible = True
				FillCombo()
			End If
		ElseIf cbobulandata.SelectedIndex <> 0 Then
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = True
		Else
			pnlcopybulandata.Visible = False
			btnGenerateUlang.Visible = False
		End If

	End Sub
#End Region
	Sub FillCboBulanData(ByVal cboName As DropDownList, ByVal Table As String)
		Dim oAssetData As New Parameter.SIPP2100
		Dim oData As New DataTable
		oAssetData.strConnection = GetConnectionString()
		oAssetData.Table = Table
		oAssetData = oController.GetCboBulandataSIPP(oAssetData)
		oData = oAssetData.ListData
		cboName.DataSource = oData
		cboName.DataTextField = "ID"
		cboName.DataValueField = "ID"
		cboName.DataBind()
		cboName.Items.Insert(0, "All")
		cboName.Items(0).Value = ""
	End Sub
#Region "Sort"
	Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Add"
    Private Sub imbAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonAdd.Click
        If CheckFeature(Me.Loginid, "SIPP2100", "ADD", "MAXILOAN") Then
            pnlAdd.Visible = True
			pnlList.Visible = False
			txtbulandata.Enabled = True
			Me.Process = "ADD"
			clean()
		End If
    End Sub
#End Region

#Region "Cancel"
    Private Sub imbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        pnlList.Visible = True
        pnlAdd.Visible = False
        Me.SearchBy = ""
        Me.SortBy = ""
        clean()
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgList.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
    End Sub

#Region "dtgList ItemCommand"
    Private Sub dtgList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgList.ItemCommand
        Dim oParameter As New Parameter.SIPP2100
        Dim dtEntity As New DataTable
        Dim err As String
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteConfirm()")
        End If
        Try
            If e.CommandName.Trim = "Edit" Then

                If CheckFeature(Me.Loginid, "SIPP2100", "EDIT", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Me.Process = "EDIT"
                pnlAdd.Visible = True
                pnlList.Visible = False

                'untuk eksport
                txtbulandata.Enabled = False
                '

                oParameter.ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                oParameter.strConnection = GetConnectionString()
                oParameter = oController.GetSIPP2100Edit(oParameter)
                If Not oParameter Is Nothing Then
                    dtEntity = oParameter.ListData
                End If

                If dtEntity.Rows.Count > 0 Then
                    oRow = dtEntity.Rows(0)
                    cboJNS_PMBYN.SelectedIndex = cboJNS_PMBYN.Items.IndexOf(cboJNS_PMBYN.Items.FindByValue(oRow("JNS_PMBYN").ToString.Trim))
                    cboSKM_PMBYN.SelectedIndex = cboSKM_PMBYN.Items.IndexOf(cboSKM_PMBYN.Items.FindByValue(oRow("SKM_PMBYN").ToString.Trim))
                    cboTJN_PMBYN.SelectedIndex = cboTJN_PMBYN.Items.IndexOf(cboTJN_PMBYN.Items.FindByValue(oRow("TJN_PMBYN").ToString.Trim))
                    cboJNS_BNGMRGNBG_HSLMBL_JS.SelectedIndex = cboJNS_BNGMRGNBG_HSLMBL_JS.Items.IndexOf(cboJNS_BNGMRGNBG_HSLMBL_JS.Items.FindByValue(oRow("JNS_BNGMRGNBG_HSLMBL_JS").ToString.Trim))
                    cboSMBR_PMBYN.SelectedIndex = cboSMBR_PMBYN.Items.IndexOf(cboSMBR_PMBYN.Items.FindByValue(oRow("SMBR_PMBYN").ToString.Trim))
                    cboKLTS.SelectedIndex = cboKLTS.Items.IndexOf(cboKLTS.Items.FindByValue(oRow("KLTS").ToString.Trim))
                    cboJNS_MT_NG.SelectedIndex = cboJNS_MT_NG.Items.IndexOf(cboJNS_MT_NG.Items.FindByValue(oRow("JNS_MT_NG").ToString.Trim))
                    cboKTGR_SH_DBTR.SelectedIndex = cboKTGR_SH_DBTR.Items.IndexOf(cboKTGR_SH_DBTR.Items.FindByValue(oRow("KTGR_SH_DBTR").ToString.Trim))
                    cboGLNGN_PHK_LWN.SelectedIndex = cboGLNGN_PHK_LWN.Items.IndexOf(cboGLNGN_PHK_LWN.Items.FindByValue(oRow("GLNGN_PHK_LWN").ToString.Trim))
                    cboSTTS_KTRKTN.SelectedIndex = cboSTTS_KTRKTN.Items.IndexOf(cboSTTS_KTRKTN.Items.FindByValue(oRow("STTS_KTRKTN").ToString.Trim))
                    cboSKTR_KNM_LPNGN_SH.SelectedIndex = cboSKTR_KNM_LPNGN_SH.Items.IndexOf(cboSKTR_KNM_LPNGN_SH.Items.FindByValue(oRow("SKTR_KNM_LPNGN_SH").ToString.Trim))
                    cboLKS_DT__PRYK.SelectedIndex = cboLKS_DT__PRYK.Items.IndexOf(cboLKS_DT__PRYK.Items.FindByValue(oRow("LKS_DT__PRYK").ToString.Trim))
                    cboJNS_GNN.SelectedIndex = cboJNS_GNN.Items.IndexOf(cboJNS_GNN.Items.FindByValue(oRow("JNS_GNN").ToString.Trim))
                    cboJNS_BRNG_DN_JS.SelectedIndex = cboJNS_BRNG_DN_JS.Items.IndexOf(cboJNS_BRNG_DN_JS.Items.FindByValue(oRow("JNS_BRNG_DN_JS").ToString.Trim))
                    txtTNGGL_ML.Text = Format(oRow("TNGGL_ML"), "yyyy/MM/dd").ToString
                    txtTNGGL_JTH_TMP.Text = Format(oRow("TNGGL_JTH_TMP"), "yyyy/MM/dd").ToString
                End If
                'untuk eksport
                txtbulandata.Text = oParameter.ListData.Rows(0)("BULANDATA")
                '
                txtID.Text = oParameter.ListData.Rows(0)("ID")
                txtNMR_KNTRK.Text = oParameter.ListData.Rows(0)("NMR_KNTRK")
                'cboJNS_BRNG_DN_JS.Text = oParameter.ListData.Rows(0)("JNS_BRNG_DN_JS")
                txtNL_BRNGJS_YNG_DBY.Text = oParameter.ListData.Rows(0)("NL_BRNGJS_YNG_DBY")
                txtNL_MRGNMBL_JS.Text = oParameter.ListData.Rows(0)("NL_MRGNMBL_JS")
                txtTNGKT_BNGBG_HSL.Text = oParameter.ListData.Rows(0)("TNGKT_BNGBG_HSL")
                txtNL_TRCTT.Text = oParameter.ListData.Rows(0)("NL_TRCTT")
                txtPRS_PRSHN_PD_PMBYN_BRSM.Text = oParameter.ListData.Rows(0)("PRS_PRSHN_PD_PMBYN_BRSM")
                txtNG_MK.Text = oParameter.ListData.Rows(0)("NG_MK")
                txtTGHN_PMBYN_DLM_MT_NG_SL.Text = oParameter.ListData.Rows(0)("TGHN_PMBYN_DLM_MT_NG_SL")
                txtTGHN_PMBYN.Text = oParameter.ListData.Rows(0)("TGHN_PMBYN")
                txtBNGMRGN_DTNGGHKN_DLM_MT_NG_SL.Text = oParameter.ListData.Rows(0)("BNGMRGN_DTNGGHKN_DLM_MT_NG_SL")
                txtBNGMRGN_DTNGGHKN.Text = oParameter.ListData.Rows(0)("BNGMRGN_DTNGGHKN")
                txtPTNG_PMBYN_PKK_DLM_MT_NG_SL.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN_PKK_DLM_MT_NG_SL")
                txtPTNG_PMBYN__PKK.Text = oParameter.ListData.Rows(0)("PTNG_PMBYN__PKK")
                txtPRPRS_PNJMNN_KRDT_T_SRNS_KRDT.Text = oParameter.ListData.Rows(0)("PRPRS_PNJMNN_KRDT_T_SRNS_KRDT")
                txtNMR_DBTR.Text = oParameter.ListData.Rows(0)("NMR_DBTR")
                txtNM_DBTR.Text = oParameter.ListData.Rows(0)("NM_DBTR")
                txtGRP_PHK_LWN.Text = oParameter.ListData.Rows(0)("GRP_PHK_LWN")
                txtNMR_GNN.Text = oParameter.ListData.Rows(0)("NMR_GNN")
                txtNL_GNNJMNN_YNG_DPT_DPRHTNGKN.Text = oParameter.ListData.Rows(0)("NL_GNNJMNN_YNG_DPT_DPRHTNGKN")

            ElseIf e.CommandName = "DEL" Then
                If CheckFeature(Me.Loginid, "SIPP2100", "DEL", "MAXILOAN") Then
                    If SessionInvalid() Then
                        Exit Sub
                    End If
                End If
                Dim customClass As New Parameter.SIPP2100
                With customClass
                    .ID = dtgList.DataKeys.Item(e.Item.ItemIndex).ToString
                    .strConnection = GetConnectionString()
                End With
                err = oController.SIPP2100Delete(customClass)
                If err <> "" Then
                    ShowMessage(lblMessage, err, True)
                Else
                    ShowMessage(lblMessage, MessageHelper.MESSAGE_DELETE_SUCCESS, False)
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try

    End Sub
#End Region

#Region " SAVE "
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSave.Click
        Dim customClass As New Parameter.SIPP2100
        Dim ErrMessage As String = ""


        If Me.Process = "ADD" Then
            With customClass
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .NMR_KNTRK = txtNMR_KNTRK.Text
                .JNS_BRNG_DN_JS = cboJNS_BRNG_DN_JS.SelectedValue.Trim
                .NL_BRNGJS_YNG_DBY = txtNL_BRNGJS_YNG_DBY.Text
                .NL_MRGNMBL_JS = txtNL_MRGNMBL_JS.Text
                .TNGKT_BNGBG_HSL = txtTNGKT_BNGBG_HSL.Text
                .NL_TRCTT = txtNL_TRCTT.Text
                .PRS_PRSHN_PD_PMBYN_BRSM = txtPRS_PRSHN_PD_PMBYN_BRSM.Text
                .NG_MK = txtNG_MK.Text
                .TGHN_PMBYN_DLM_MT_NG_SL = txtTGHN_PMBYN_DLM_MT_NG_SL.Text
                .TGHN_PMBYN = txtTGHN_PMBYN.Text
                .BNGMRGN_DTNGGHKN_DLM_MT_NG_SL = txtBNGMRGN_DTNGGHKN_DLM_MT_NG_SL.Text
                .BNGMRGN_DTNGGHKN = txtBNGMRGN_DTNGGHKN.Text
                .PTNG_PMBYN_PKK_DLM_MT_NG_SL = txtPTNG_PMBYN_PKK_DLM_MT_NG_SL.Text
                .PTNG_PMBYN__PKK = txtPTNG_PMBYN__PKK.Text
                .PRPRS_PNJMNN_KRDT_T_SRNS_KRDT = txtPRPRS_PNJMNN_KRDT_T_SRNS_KRDT.Text
                .NMR_DBTR = txtNMR_DBTR.Text
                .NM_DBTR = txtNM_DBTR.Text
                .GRP_PHK_LWN = txtGRP_PHK_LWN.Text
                .NMR_GNN = txtNMR_GNN.Text
                .NL_GNNJMNN_YNG_DPT_DPRHTNGKN = txtNL_GNNJMNN_YNG_DPT_DPRHTNGKN.Text
                .JNS_PMBYN = cboJNS_PMBYN.SelectedValue.Trim
                .SKM_PMBYN = cboSKM_PMBYN.SelectedValue.Trim
                .TJN_PMBYN = cboTJN_PMBYN.SelectedValue.Trim
                .JNS_BNGMRGNBG_HSLMBL_JS = cboJNS_BNGMRGNBG_HSLMBL_JS.SelectedValue.Trim
                .SMBR_PMBYN = cboSMBR_PMBYN.SelectedValue.Trim
                .KLTS = cboKLTS.SelectedValue.Trim
                .JNS_MT_NG = cboJNS_MT_NG.SelectedValue.Trim
                .KTGR_SH_DBTR = cboKTGR_SH_DBTR.SelectedValue.Trim
                .GLNGN_PHK_LWN = cboGLNGN_PHK_LWN.SelectedValue.Trim
                .STTS_KTRKTN = cboSTTS_KTRKTN.SelectedValue.Trim
                .SKTR_KNM_LPNGN_SH = cboSKTR_KNM_LPNGN_SH.SelectedValue.Trim
                .LKS_DT__PRYK = cboLKS_DT__PRYK.SelectedValue.Trim
                .JNS_GNN = cboJNS_GNN.SelectedValue.Trim
                .TNGGL_ML = txtTNGGL_ML.Text
                .TNGGL_JTH_TMP = txtTNGGL_JTH_TMP.Text

                .strConnection = GetConnectionString()
            End With
            oController.SIPP2100Add(customClass)
        ElseIf Me.Process = "EDIT" Then
            With customClass
                'untuk eksport
                .BULANDATA = txtbulandata.Text
                '
                .ID = txtID.Text
                .NMR_KNTRK = txtNMR_KNTRK.Text
                .JNS_BRNG_DN_JS = cboJNS_BRNG_DN_JS.SelectedValue.Trim
                .NL_BRNGJS_YNG_DBY = txtNL_BRNGJS_YNG_DBY.Text
                .NL_MRGNMBL_JS = txtNL_MRGNMBL_JS.Text
                .TNGKT_BNGBG_HSL = txtTNGKT_BNGBG_HSL.Text
                .NL_TRCTT = txtNL_TRCTT.Text
                .PRS_PRSHN_PD_PMBYN_BRSM = txtPRS_PRSHN_PD_PMBYN_BRSM.Text
                .NG_MK = txtNG_MK.Text
                .TGHN_PMBYN_DLM_MT_NG_SL = txtTGHN_PMBYN_DLM_MT_NG_SL.Text
                .TGHN_PMBYN = txtTGHN_PMBYN.Text
                .BNGMRGN_DTNGGHKN_DLM_MT_NG_SL = txtBNGMRGN_DTNGGHKN_DLM_MT_NG_SL.Text
                .BNGMRGN_DTNGGHKN = txtBNGMRGN_DTNGGHKN.Text
                .PTNG_PMBYN_PKK_DLM_MT_NG_SL = txtPTNG_PMBYN_PKK_DLM_MT_NG_SL.Text
                .PTNG_PMBYN__PKK = txtPTNG_PMBYN__PKK.Text
                .PRPRS_PNJMNN_KRDT_T_SRNS_KRDT = txtPRPRS_PNJMNN_KRDT_T_SRNS_KRDT.Text
                .NMR_DBTR = txtNMR_DBTR.Text
                .NM_DBTR = txtNM_DBTR.Text
                .GRP_PHK_LWN = txtGRP_PHK_LWN.Text
                .NMR_GNN = txtNMR_GNN.Text
                .NL_GNNJMNN_YNG_DPT_DPRHTNGKN = txtNL_GNNJMNN_YNG_DPT_DPRHTNGKN.Text
                .JNS_PMBYN = cboJNS_PMBYN.SelectedValue.Trim
                .SKM_PMBYN = cboSKM_PMBYN.SelectedValue.Trim
                .TJN_PMBYN = cboTJN_PMBYN.SelectedValue.Trim
                .JNS_BNGMRGNBG_HSLMBL_JS = cboJNS_BNGMRGNBG_HSLMBL_JS.SelectedValue.Trim
                .SMBR_PMBYN = cboSMBR_PMBYN.SelectedValue.Trim
                .KLTS = cboKLTS.SelectedValue.Trim
                .JNS_MT_NG = cboJNS_MT_NG.SelectedValue.Trim
                .KTGR_SH_DBTR = cboKTGR_SH_DBTR.SelectedValue.Trim
                .GLNGN_PHK_LWN = cboGLNGN_PHK_LWN.SelectedValue.Trim
                .STTS_KTRKTN = cboSTTS_KTRKTN.SelectedValue.Trim
                .SKTR_KNM_LPNGN_SH = cboSKTR_KNM_LPNGN_SH.SelectedValue.Trim
                .LKS_DT__PRYK = cboLKS_DT__PRYK.SelectedValue.Trim
                .JNS_GNN = cboJNS_GNN.SelectedValue.Trim
                .TNGGL_ML = txtTNGGL_ML.Text
                .TNGGL_JTH_TMP = txtTNGGL_JTH_TMP.Text

                .strConnection = GetConnectionString()
            End With
            oController.SIPP2100Update(customClass)
        End If
        Me.SearchBy = ""
        Me.SortBy = ""
        DoBind(Me.SearchBy, Me.SortBy)
        pnlAdd.Visible = False
        pnlList.Visible = True

        If Me.Process = "ADD" Then
            ShowMessage(lblMessage, "Simpan data berhasil.....", False)
        Else
            ShowMessage(lblMessage, "Update data berhasil.....", False)
        End If

    End Sub
#End Region


#Region " dropdown "
    Private Sub FillCombo()
        'Dim strCombo As String
        Dim strConn As String
        Dim dtCombo As New DataTable

        strConn = GetConnectionString()
    End Sub
    Sub FillCbo(ByVal cboName As DropDownList, ByVal valuemaster As String, ByVal valueparent As String, ByVal valueelement As String)
        Dim oAssetData As New Parameter.SIPP2100
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.valuemaster = valuemaster
        oAssetData.valueparent = valueparent
        oAssetData.valueelement = valueelement.ToString
        oAssetData = oController.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "ElementLabel"
        cboName.DataValueField = "Element"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"


    End Sub
#End Region

#Region "Fill Data"
    Private Sub FillData(ByVal oTable As DataTable)
        Dim oRow As DataRow
        oRow = oTable.Rows(0)
    End Sub
#End Region

    'untuk eksport
#Region "EXPORT"
    Public Sub ExportTableData(ByVal dtdata As DataTable, ByVal nm As String)
        Using wb As New XLWorkbook()

            Dim ws = wb.Worksheets.Add(dtdata, "TB_210020300")
            ws.Tables.FirstOrDefault().ShowAutoFilter = False

            Response.Clear()
            Response.Buffer = True
            Response.Charset = ""
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            Response.AddHeader("content-disposition", "attachment;filename=" & nm & ".xlsx")
            Using MyMemoryStream As New MemoryStream()
                wb.SaveAs(MyMemoryStream)
                MyMemoryStream.WriteTo(Response.OutputStream)
                Response.Flush()
                Response.End()
            End Using
        End Using


    End Sub

    Public Function GetToexcel(ByVal customclass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP2100Excel", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
    End Function

    Private Sub BtnExportToExcel_Click(sender As Object, e As System.EventArgs) Handles BtnExportToExcel.Click
        Dim oEntities As New Parameter.SIPP2100
        Dim dtViewData As New DataTable

        If cbobulandata.Text <> "" Then
            With oEntities
                .BULANDATA = cbobulandata.Text
                .strConnection = GetConnectionString()
            End With
            oEntities = GetToexcel(oEntities)
            dtViewData = oEntities.ListData
            ExportTableData(dtViewData, oEntities.BULANDATA & "_" & "37_Rincian_Pembiayaan_yang_Diberikan_TB_210020300")
        Else
            ShowMessage(lblMessage, "Harap Pilih BulanData .....", True)
        End If
    End Sub

    Private Sub btnfind_Click(sender As Object, e As EventArgs) Handles btnfind.Click
        If cbobulandata.Text <> "" Then
            Me.SearchBy = "BULANDATA" & " like '%" & cbobulandata.Text.Trim & "%'"

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        Else
            Me.SearchBy = ""

            Me.SortBy = ""
            DoBind(Me.SearchBy, Me.SortBy)
        End If
		If cbobulandata.Text <> "" Then

			Dim dateString, format As String
			Dim result As Date
			Dim provider As Globalization.CultureInfo = Globalization.CultureInfo.InvariantCulture

			' Untuk Parsing EOMONTH ke textbox generate
			dateString = (cbobulandata.SelectedValue)
			format = "yyyyMM"
			result = Date.ParseExact(dateString, format, provider)

			txtcopybulandata.Text = result.AddMonths(1).AddDays(-1).ToString("yyy-MM-dd")
		End If
		clean()
	End Sub
#End Region
#Region "Search"
	Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

		If cboSearch.Text <> "" Then
			Me.SearchBy = cboSearch.SelectedItem.Value & " like '%" & Replace(txtSearch.Text.Trim, "'", "''") & "%'"

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		Else
			Me.SearchBy = ""

			Me.SortBy = ""
			DoBind(Me.SearchBy, Me.SortBy)
		End If
	End Sub
#End Region

#Region "Reset"
	Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
		cboSearch.SelectedIndex = 0
		cbobulandata.SelectedIndex = 0
		txtSearch.Text = ""
		Me.SearchBy = ""
		DoBind(Me.SearchBy, Me.SortBy)
		clean()
	End Sub
#End Region

#Region "ReGenerate"
	Private Sub btnGenerateUlang_Click(sender As Object, e As EventArgs) Handles btnGenerateUlang.Click
		btnGenerateUlang.Visible = False
		pnlcopybulandata.Visible = True
	End Sub
#End Region

#Region "Copy Data"
	Public Function GetCopy(ByVal customclass As Parameter.SIPP2100) As Parameter.SIPP2100
        Dim params() As SqlParameter = New SqlParameter(0) {}
        Try
            params(0) = New SqlParameter("@BULANDATA", SqlDbType.VarChar, 20)
            params(0).Value = customclass.BULANDATA

            'Table As BULANDATA Copy
            'params(1) = New SqlParameter("@TABLE", SqlDbType.VarChar, 20)
            'params(1).Value = customclass.Table

            customclass.ListData = SqlHelper.ExecuteDataset(customclass.strConnection, CommandType.StoredProcedure, "spSIPP2100Copy", params).Tables(0)
            Return customclass
        Catch exp As Exception
        End Try
        ShowMessage(lblMessage, "Copy Data Berhasil......", False)
    End Function

    Private Sub btncopybulandata_Click(sender As Object, e As EventArgs) Handles btncopybulandata.Click
        Dim oEntities As New Parameter.SIPP2100
        Dim dtViewData As New DataTable
        With oEntities
            .BULANDATA = txtcopybulandata.Text
            '.Table = cbocopybulandata.Text
            .strConnection = GetConnectionString()
        End With
        GetCopy(oEntities)
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

	Sub clean()
        txtbulandata.Text = ""
        txtNMR_KNTRK.Text = ""

        txtNL_BRNGJS_YNG_DBY.Text = ""
        txtTNGGL_ML.Text = ""
        txtTNGGL_JTH_TMP.Text = ""

        txtNL_MRGNMBL_JS.Text = ""
        txtTNGKT_BNGBG_HSL.Text = ""
        txtNL_TRCTT.Text = ""

        txtPRS_PRSHN_PD_PMBYN_BRSM.Text = ""
        txtNG_MK.Text = ""

        txtTGHN_PMBYN_DLM_MT_NG_SL.Text = ""
        txtTGHN_PMBYN.Text = ""
        txtBNGMRGN_DTNGGHKN_DLM_MT_NG_SL.Text = ""
        txtBNGMRGN_DTNGGHKN.Text = ""
        txtPTNG_PMBYN_PKK_DLM_MT_NG_SL.Text = ""
        txtPTNG_PMBYN__PKK.Text = ""
        txtPRPRS_PNJMNN_KRDT_T_SRNS_KRDT.Text = ""
        txtNMR_DBTR.Text = ""
        txtNM_DBTR.Text = ""
        txtGRP_PHK_LWN.Text = ""

		txtNMR_GNN.Text = ""
		txtNL_GNNJMNN_YNG_DPT_DPRHTNGKN.Text = ""
    End Sub



End Class