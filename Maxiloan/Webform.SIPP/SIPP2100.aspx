﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP2100.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP2100" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register TagPrefix="uc1" TagName="ucDateCE" Src="../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>SIPP2100</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>   
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        } 
    </script>    
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SIPP2100 - RINCIAN PEMBIAYAAN YANG DIBERIKAN
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnGenerateUlang" runat="server" CausesValidation="False" Text="Generate Ulang" CssClass="small button green">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                        <div class="form_single">
                        <label>
                            Generate Bulan Data
                        </label>
                        <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                            TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        </div>
                   </div>
                    <div class="form_button">                  
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        <div class="form_box_title" style="margin-top: 3px;" >
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle  Width="2%" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../Images/iconDelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" Visible="false"></asp:BoundColumn> 
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="7%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NMR_DBTR" SortExpression="NMR_DBTR" HeaderText="Nomor Debitur" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NM_DBTR" SortExpression="NM_DBTR" HeaderText="Nama Debitur" ItemStyle-Width="15%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NMR_KNTRK" SortExpression="NMR_KNTRK" HeaderText="Nomor Kontrak" ItemStyle-Width="12%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JNS_PMBYN" SortExpression="JNS_PMBYN" HeaderText="Jenis Pembiayaan" ItemStyle-Width="11%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SKM_PMBYN" SortExpression="SKM_PMBYN" HeaderText="Skema Pembiayaan" ItemStyle-Width="15%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TNGGL_ML" SortExpression="TNGGL_ML" HeaderText="TGL Mulai" DataFormatString="{0:yyyy/MM/dd}" ItemStyle-Width="7%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TNGGL_JTH_TMP" SortExpression="TNGGL_JTH_TMP" HeaderText="TGL Jt Tempo" DataFormatString="{0:yyyy/MM/dd}" ItemStyle-Width="7%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JNS_BRNG_DN_JS" SortExpression="JNS_BRNG_DN_JS" HeaderText="JNS Barang" ItemStyle-Width="10%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NL_BRNGJS_YNG_DBY" SortExpression="NL_BRNGJS_YNG_DBY" HeaderText="Nilai Dibiayai">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNavi" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button> 
                </div>

        <div class="form_box_title" style="margin-top: 3px;" >
            <div class="form_single">
                <h5>
                    CARI DEBITUR
                </h5>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearch" runat="server">
                    <asp:ListItem Value="NM_DBTR" Selected="True">Nama Debitur</asp:ListItem>
                    <asp:ListItem Value="NMR_DBTR">CIF Debitur</asp:ListItem>
                    <asp:ListItem Value="NMR_KNTRK">Nomor Kontrak</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearch" runat="server" Width="20%"></asp:TextBox>
            </div>
        </div>
        <div class="form_button" style="padding-bottom: 0; padding-top: 3px;"> 
             <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
             <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
        </div>
                
            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
                <div class="form_box"> 
                        <asp:TextBox ID="txtID" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>  
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Kontrak
                        </label>
                        <asp:TextBox ID="txtNMR_KNTRK" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNMR_KNTRK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Pembiayaan
                        </label>
                        <asp:DropDownList ID="cboJNS_PMBYN" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="cboJNS_PMBYN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Skema Pembiayaan
                        </label>
                        <asp:DropDownList ID="cboSKM_PMBYN" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="cboSKM_PMBYN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tujuan Pembiayaan
                        </label>
                        <asp:DropDownList ID="cboTJN_PMBYN" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="cboTJN_PMBYN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Barang Dan Jasa yang diBiayai
                        </label>
                        <asp:DropDownList ID="cboJNS_BRNG_DN_JS" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="cboJNS_BRNG_DN_JS" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nilai Barang dan Jasa yang diBiayai
                        </label>
                        <asp:TextBox ID="txtNL_BRNGJS_YNG_DBY" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNL_BRNGJS_YNG_DBY" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Mulai
                        </label>
                        <asp:TextBox ID="txtTNGGL_ML" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                            TargetControlID="txtTNGGL_ML" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTNGGL_ML" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tanggal Jatuh Tempo
                        </label>
                        <asp:TextBox ID="txtTNGGL_JTH_TMP" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True"
                            TargetControlID="txtTNGGL_JTH_TMP" Format="yyyy/MM/dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTNGGL_JTH_TMP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Bunga/Margin
                        </label>
                        <asp:DropDownList ID="cboJNS_BNGMRGNBG_HSLMBL_JS" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ControlToValidate="cboJNS_BNGMRGNBG_HSLMBL_JS" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nilai Margin
                        </label>
                        <asp:TextBox ID="txtNL_MRGNMBL_JS" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNL_MRGNMBL_JS" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tingkat Bunga
                        </label>
                        <asp:TextBox ID="txtTNGKT_BNGBG_HSL" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTNGKT_BNGBG_HSL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nilai Tercatat
                        </label>
                        <asp:TextBox ID="txtNL_TRCTT" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNL_TRCTT" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Sumber Pembiayaan
                        </label>
                        <asp:DropDownList ID="cboSMBR_PMBYN" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                            ControlToValidate="cboSMBR_PMBYN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Porsi Perusahaan pada Pembiayaan Bersama
                        </label>
                        <asp:TextBox ID="txtPRS_PRSHN_PD_PMBYN_BRSM" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPRS_PRSHN_PD_PMBYN_BRSM" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Uang Muka
                        </label>
                        <asp:TextBox ID="txtNG_MK" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNG_MK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kualitas
                        </label>
                        <asp:DropDownList ID="cboKLTS" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                            ControlToValidate="cboKLTS" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Mata Uang
                        </label>
                        <asp:DropDownList ID="cboJNS_MT_NG" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                            ControlToValidate="cboJNS_MT_NG" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tagihan Pembiayaan dalam Mata Uang Asal
                        </label>
                        <asp:TextBox ID="txtTGHN_PMBYN_DLM_MT_NG_SL" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTGHN_PMBYN_DLM_MT_NG_SL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Tagihan Pembiayaan
                        </label>
                        <asp:TextBox ID="txtTGHN_PMBYN" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTGHN_PMBYN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Bunga/Margin Ditangguhkan Dalam Mata Uang Asal
                        </label>
                        <asp:TextBox ID="txtBNGMRGN_DTNGGHKN_DLM_MT_NG_SL" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBNGMRGN_DTNGGHKN_DLM_MT_NG_SL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Bunga/Margin Ditangguhkan
                        </label>
                        <asp:TextBox ID="txtBNGMRGN_DTNGGHKN" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBNGMRGN_DTNGGHKN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Piutang Pembiayaan Pokok dalam Mata Uang Asli
                        </label>
                        <asp:TextBox ID="txtPTNG_PMBYN_PKK_DLM_MT_NG_SL" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN_PKK_DLM_MT_NG_SL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Piutang Pembiayaan Pokok
                        </label>
                        <asp:TextBox ID="txtPTNG_PMBYN__PKK" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPTNG_PMBYN__PKK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Proporsi Penjaminan Pembiayaan atau Asuransi Pembiayaan
                        </label>
                        <asp:TextBox ID="txtPRPRS_PNJMNN_KRDT_T_SRNS_KRDT" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPRPRS_PNJMNN_KRDT_T_SRNS_KRDT" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Debitur
                        </label>
                        <asp:TextBox ID="txtNMR_DBTR" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNMR_DBTR" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Debitur
                        </label>
                        <asp:TextBox ID="txtNM_DBTR" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNM_DBTR" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nama Kelompok Debitur
                        </label>
                        <asp:TextBox ID="txtGRP_PHK_LWN" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="*"
                            ControlToValidate="txtGRP_PHK_LWN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Kategori Usaha Debitur
                        </label>
                        <asp:DropDownList ID="cboKTGR_SH_DBTR" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="*"
                            ControlToValidate="cboKTGR_SH_DBTR" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Golongan Debitur
                        </label>
                        <asp:DropDownList ID="cboGLNGN_PHK_LWN" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="*"
                            ControlToValidate="cboGLNGN_PHK_LWN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Status Keterkaitan
                        </label>
                        <asp:DropDownList ID="cboSTTS_KTRKTN" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="*"
                            ControlToValidate="cboSTTS_KTRKTN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Sektor Ekonomi Lapangan Usaha
                        </label>
                        <asp:DropDownList ID="cboSKTR_KNM_LPNGN_SH" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="*"
                            ControlToValidate="cboSKTR_KNM_LPNGN_SH" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Lokasi Proyek
                        </label>
                        <asp:DropDownList ID="cboLKS_DT__PRYK" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="*"
                            ControlToValidate="cboLKS_DT__PRYK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nomor Agunan
                        </label>
                        <asp:TextBox ID="txtNMR_GNN" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNMR_GNN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Agunan
                        </label>
                        <asp:DropDownList ID="cboJNS_GNN" runat="server" CssClass="medium_text"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="*"
                            ControlToValidate="cboJNS_GNN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nilai Agunan/Jaminan yang dapat diperhitungkan
                        </label>
                        <asp:TextBox ID="txtNL_GNNJMNN_YNG_DPT_DPRHTNGKN" runat="server" CssClass="medium_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNL_GNNJMNN_YNG_DPT_DPRHTNGKN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>
