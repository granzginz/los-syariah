﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP1110.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP1110" MaintainScrollPositionOnPostback="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>REKENING ADMINISTRATIF</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> 
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <style type="text/css">
        .total-yellow {
            background-color: yellow
        }
        .total-green {
            background-color: greenyellow
        }
        .title-gray {
            background-color: lightgray
        }
        .rightAlign {
            text-align: right
        }
    </style>   
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SIPP1110 - REKENING ADMINISTRATIF
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

        <div class="form_box_title" style="margin-top: 3px;" >
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="2%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    <%--</asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JUMLAH TENAGA KERJA TOTAL" SortExpression="ProductCode">
                                        <ItemStyle Width="20%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                        <asp:Label ID="lblpengurus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.JMLH_TNG_KRJ_TTL__") %>'>
                                         </asp:Label></a>
                                    </ItemTemplate>--%>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" visible="false">
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="10%">
                                    </asp:BoundColumn>   
                                    <asp:BoundColumn DataField="FSLTS_PNDNN_YNG_BLM_DTRK_TTL" SortExpression="FSLTS_PNDNN_YNG_BLM_DTRK_TTL" HeaderText="Total Fasilitas Pendanaan yang Belum Ditarik" ItemStyle-Width="10%" HeaderStyle-Font-Size ="Small">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL" SortExpression="FSLTS_PMBYN_KPD_NSBH_YNG_BLM_DTRK_TTL" HeaderText="Total Fasilitas Pembiayaan Kepada Nasabah Yang Belum Ditarik" ItemStyle-Width="10%" HeaderStyle-Font-Size ="Small">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PNRBTN_SRT_SNGGP_BYR_TTL" SortExpression="PNRBTN_SRT_SNGGP_BYR_TTL" HeaderText="Total Penerbitan Surat Sanggup Bayar" ItemStyle-Width="10%" HeaderStyle-Font-Size ="Small">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL" SortExpression="PNYLRN_PMBYN_BRSM_PRS_PHK_KTG_TTL" HeaderText="Total Penyaluran pembiayaan Bersama Porsi Pihak Ketiga" ItemStyle-Width="10%" HeaderStyle-Font-Size ="Small">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL" SortExpression="NMNL_NSTRMN_DRVTF_NTK_LNDNG_NL_TTL" HeaderText="Total Nominal Instrumen Derivatif Untuk Lindung Nilai" ItemStyle-Width="10%" HeaderStyle-Font-Size ="Small">
                                    </asp:BoundColumn>    
                                    <asp:BoundColumn DataField="RKNNG_DMNSTRTF_LNNY_TTL" SortExpression="RKNNG_DMNSTRTF_LNNY_TTL" HeaderText="Total Rekening Administratif Lainnya" ItemStyle-Width="10%" HeaderStyle-Font-Size ="Small">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="RKNNG_DMNSTRTF_TTL" SortExpression="RKNNG_DMNSTRTF_TTL" HeaderText="Total Rekening Administratif" ItemStyle-Width="10%" HeaderStyle-Font-Size ="Small" >
                                    </asp:BoundColumn>     
                                    
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP1110" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                </div>

                <%--<div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI RINCIAN TENAGA KERJA BERDASARKAN FUNGSI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="NM_PNGRS">Nama Pengurus</asp:ListItem>
                            <asp:ListItem Value="JBTN_KPNGRSN">Jabatan</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>--%>
            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>
            
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
               
                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>1. FASILITAS PINJAMAN YANG BELUM DITARIK</b>
                         </h4>
                    </div>
                </div>

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label3" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label4" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Bank (Dalam Negeri)
                        </label>
                         <asp:TextBox ID="txtBankDNRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBankDNRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jasa Keuangan Non Bank (Dalam Negeri)
                        </label>
                         <asp:TextBox ID="txtKeuDNRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKeuDNRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Lainnya (Dalam Negeri)
                        </label>
                         <asp:TextBox ID="txtLnDNRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                            ControlToValidate="txtLnDNRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>

                     <div class="form_right">
                        <label class="label_req">
                            Bank (Dalam Negeri)
                        </label>
                         <asp:TextBox ID="txtBankDNMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBankDNMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jasa Keuangan Non Bank (Dalam Negeri)
                        </label>
                         <asp:TextBox ID="txtKeuDNMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKeuDNMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Lainnya (Dalam Negeri)
                        </label>
                         <asp:TextBox ID="txtLnDNMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtLnDNMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box total">
                    <div class="form_left">
                        <b><asp:Label ID="Label26" runat="server" Text="Total Fasilitas Yang Belum Ditarik Bank (Dalam Negeri) :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTDNBank" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label28" runat="server" Text="Total Fasilitas Yang Belum Ditarik Jasa Keuangan Non Bank (Dalam Negeri) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTDNKeu" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label30" runat="server" Text="Total Fasilitas Yang Belum Ditarik Lainnya (Dalam Negeri) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTDNLn" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                       
                    </div>
                    <div class="form_right">
                        <b><asp:Label ID="Label8" runat="server" Text="Total Fasilitas Yang Belum Ditarik Dari Dalam Negeri :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTDN" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label14" runat="server" Text="Total Fasilitas Yang Belum Ditarik Dari Dalam Negeri (Rupiah) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTDNRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label20" runat="server" Text="Total Fasilitas Yang Belum Ditarik Dari Dalam Negeri (Valas) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTDNMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                       
                    </div>
                </div>        

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label32" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label33" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>

                 <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Bank (Luar Negeri)
                        </label>
                         <asp:TextBox ID="txtBankLNRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBankLNRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jasa Keuangan Non Bank (Luar Negeri)
                        </label>
                         <asp:TextBox ID="txtKeuLNRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKeuLNRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Lainnya (Luar Negeri)
                        </label>
                         <asp:TextBox ID="txtLnLNRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                            ControlToValidate="txtLnLNRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     </div>                
                     <div class="form_right">
                        <label class="label_req">
                            Bank (Luar Negeri)
                        </label>
                         <asp:TextBox ID="txtBankLNMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                            ControlToValidate="txtBankLNMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Jasa Keuangan Non Bank (Luar Negeri)
                        </label>
                         <asp:TextBox ID="txtKeuLNMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                            ControlToValidate="txtKeuLNMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Lainnya (Luar Negeri)
                        </label>
                         <asp:TextBox ID="txtLnLNMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                            ControlToValidate="txtLnLNMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_left">
                        <b><asp:Label ID="Label1" runat="server" Text="Total Fasilitas Yang Belum Ditarik Bank (Luar Negeri) :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTLNBank" runat="server" Text="" CssClass="numberAlign2 regular_text">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label2" runat="server" Text="Total Fasilitas Yang Belum Ditarik Jasa Keuangan Non Bank (Luar Negeri) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTLNKeu" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label24" runat="server" Text="Total Fasilitas Yang Belum Ditarik Lainnya (Luar Negeri) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTLNLn" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                       
                    </div>
                    <div class="form_right">
                        <b><asp:Label ID="Label25" runat="server" Text="Total Fasilitas Yang Belum Ditarik Dari Luar Negeri :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTLN" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label29" runat="server" Text="Total Fasilitas Yang Belum Ditarik Dari Luar Negeri (Rupiah) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTLNRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label35" runat="server" Text="Total Fasilitas Yang Belum Ditarik Dari Luar Negeri (Valas) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTLNMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                       
                    </div>
                </div> 
                <div class="form_box total-yellow">
                    <div class="form_left">
                        <b><asp:Label ID="Label39" runat="server" Text="Total Keseluruhan Fasilitas Yang Belum Ditarik :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTBlmTarik" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label43" runat="server" Text="Total Keseluruhan Fasilitas Yang Belum Ditarik (Rupiah) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTBlmTarikRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 

                        <br />
                        <b><asp:Label ID="Label47" runat="server" Text="Total Keseluruhan Fasilitas Yang Belum Ditarik (Valas) :"></asp:Label></b>                       
                        <b><asp:Label ID="lblTOTBlmTarikMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                 
                    </div>
                </div>


                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>2. FASILITAS PEMBIAYAAN KEPADA NASABAH YANG BELUM DITARIK</b>
                         </h4>
                    </div>
                </div>   

                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label5" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label6" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>

                 <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Jumlah yang Belum Ditarik
                        </label>
                         <asp:TextBox ID="txtPembNasRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPembNasRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                    </div> 
                    <div class="form_right">
                        <label class="label_req">
                            Jumlah yang Belum Ditarik
                        </label>
                         <asp:TextBox ID="txtPembNasMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPembNasMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                      </div>
                   </div>      
                
                <div class="form_box total-yellow">
                    <div class="form_left">
                        <b><asp:Label ID="Label7" runat="server" Text="Total Fasilitas Pembiayaan Kepada Nasabah Yang Belum Ditarik :"></asp:Label></b>                      
                        <b><asp:Label ID="lblTOTPembNas" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                    </div>
                </div>        

                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>3. PENERBITAN SURAT SANGGUP BAYAR</b>
                         </h4>
                    </div>
                </div>
                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label9" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label10" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Pinjaman Dalam Negeri
                        </label>
                         <asp:TextBox ID="txtPinjDNRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPinjDNRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Pinjaman Luar Negeri
                        </label>
                         <asp:TextBox ID="txtPinjLNRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPinjLNRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div> 
                    <div class="form_right">
                        <label class="label_req">
                            Pinjaman Dalam Negeri
                        </label>
                         <asp:TextBox ID="txtPinjDNMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPinjDNMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Pinjaman Luar Negeri
                        </label>
                         <asp:TextBox ID="txtPinjLNMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                            ControlToValidate="txtPinjLNMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                      </div>
                   </div>  
                <div class="form_box">
                    <div class="form_left">
                            <b><asp:Label ID="Label11" runat="server" Text="Total Pinjaman Dalam Negeri :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTDNPinj" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label13" runat="server" Text="Total Pinjaman Luar Negeri :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTLNPinj" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                        
                    </div>
                </div> 
                <div class="form_box total-yellow">
                    <div class="form_left">
                            <b><asp:Label ID="Label12" runat="server" Text="Total Pinjaman :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTPNRBTN" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label23" runat="server" Text="Total Pinjaman (Rupiah) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTPNRBTNRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>         
                            <br />
                            <b><asp:Label ID="Label31" runat="server" Text="Total Pinjaman (Valas) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTPNRBTNMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                
                    </div>
                </div>                      

                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>4. PENYALURAN PEMBIAYAAN BERSAMA PORSI PIHAK KETIGA</b>
                         </h4>
                    </div>
                </div>
                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label15" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label16" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
                 <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Kegiatan Chanelling
                        </label>
                         <asp:TextBox ID="txtChanRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                            ControlToValidate="txtChanRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Kegiatan Joint Financing
                        </label>
                         <asp:TextBox ID="txtJointRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                            ControlToValidate="txtJointRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div> 
                    <div class="form_right">
                        <label class="label_req">
                            Kegiatan Chanelling
                        </label>
                         <asp:TextBox ID="txtChanMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                            ControlToValidate="txtChanMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Kegiatan Joint Financing
                        </label>
                         <asp:TextBox ID="txtJointMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                            ControlToValidate="txtJointMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                      </div>
                   </div>  
                <div class="form_box">
                    <div class="form_left">
                            <b><asp:Label ID="Label17" runat="server" Text="Total Kegiatan Chanelling :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTChan" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label19" runat="server" Text="Total Kegiatan Joint Financing :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTJoint" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                        
                    </div>
                </div>
                 <div class="form_box total-yellow">
                    <div class="form_left">
                            <b><asp:Label ID="Label18" runat="server" Text="Total Penyaluran Pembiayaan Bersama Porsi Pihak Ketiga :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTPNYLRN" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label37" runat="server" Text="Total Penyaluran Pembiayaan Bersama Porsi Pihak Ketiga (Rupiah) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTPNYLRNRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>  
                            <br />
                            <b><asp:Label ID="Label45" runat="server" Text="Total Penyaluran Pembiayaan Bersama Porsi Pihak Ketiga (Valas) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTPNYLRNMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>                        
                    </div>
                </div>     
                
                 <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>5. INSTRUMEN  DERIVATIF UNTUK LINDUNGI NILAI</b>
                         </h4>
                    </div>
                </div>
                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label21" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label22" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>        
                 <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Interest Rate Swap
                        </label>
                         <asp:TextBox ID="txtIntrRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                            ControlToValidate="txtIntrRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Currency Swap
                        </label>
                         <asp:TextBox ID="txtCurRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                            ControlToValidate="txtCurRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Cross Currency Swap
                        </label>
                         <asp:TextBox ID="txtCrosRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                            ControlToValidate="txtCrosRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Forward
                        </label>
                         <asp:TextBox ID="txtForwRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="*"
                            ControlToValidate="txtForwRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Option
                        </label>
                         <asp:TextBox ID="txtOptRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="*"
                            ControlToValidate="txtOptRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Future
                        </label>
                         <asp:TextBox ID="txtFutuRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="*"
                            ControlToValidate="txtFutuRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Lainnya
                        </label>
                         <asp:TextBox ID="txtLainRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="*"
                            ControlToValidate="txtLainRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                      </div>
                    <div class="form_right">
                        <label class="label_req">
                            Interest Rate Swap
                        </label>
                         <asp:TextBox ID="txtIntrMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="*"
                            ControlToValidate="txtIntrMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Currency Swap
                        </label>
                         <asp:TextBox ID="txtCurMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="*"
                            ControlToValidate="txtCurMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Cross Currency Swap
                        </label>
                         <asp:TextBox ID="txtCrosMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="*"
                            ControlToValidate="txtCrosMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Forward
                        </label>
                         <asp:TextBox ID="txtForwMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="*"
                            ControlToValidate="txtForwMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Option
                        </label>
                         <asp:TextBox ID="txtOptMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="*"
                            ControlToValidate="txtOptMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Future
                        </label>
                         <asp:TextBox ID="txtFutuMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="*"
                            ControlToValidate="txtFutuMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Lainnya
                        </label>
                         <asp:TextBox ID="txtLainMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="*"
                            ControlToValidate="txtLainMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                      </div>
                   </div> 
                <div class="form_box">
                    <div class="form_left">
                            <b><asp:Label ID="Label34" runat="server" Text="Total Interest Rate Swap :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTIntr" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label36" runat="server" Text="Total Currency Swap :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTCur" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>      
                            <br />
                            <b><asp:Label ID="Label38" runat="server" Text="Total Cross Currency Swap :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTCros" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>    
                            <br />
                            <b><asp:Label ID="Label40" runat="server" Text="Total Forward :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTForw" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>   
                            <br />
                            <b><asp:Label ID="Label42" runat="server" Text="Total Option :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTOpt" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>   
                            <br />
                            <b><asp:Label ID="Label44" runat="server" Text="Total Future:"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTFutu" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label46" runat="server" Text="Total Lainnya:"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTLain" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>            
                    </div>
                </div>
                 <div class="form_box total-yellow">
                    <div class="form_left">
                            <b><asp:Label ID="Label27" runat="server" Text="Total Instrumen Derivatif Untuk Lindungi Nilai :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTDRVTF" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label51" runat="server" Text="Total Instrumen Derivatif Untuk Lindungi Nilai (Rupiah) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTDRVTFRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>      
                            <br />
                            <b><asp:Label ID="Label55" runat="server" Text="Total Instrumen Derivatif Untuk Lindungi Nilai (Valas) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTDRVTFMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>    
                    </div>
                </div>        
                
                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>6. LAINNYA</b>
                         </h4>
                    </div>
                </div>
                <div class="form_box title-gray">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label48" runat="server" Text="RUPIAH"></asp:Label>
                        </b>     
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label49" runat="server" Text="VALAS"></asp:Label>
                        </b> 
                    </div>
                </div>
                              
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Piutang Pembiayaan hapus buku
                        </label>                           
                                <asp:TextBox ID="txtHpsBukuRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="*"
                            ControlToValidate="txtHpsBukuRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Piutang Pembiayaan hapus buku yang berhasil ditagih
                        </label>
                         <asp:TextBox ID="txtHpsBukuTghRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ErrorMessage="*"
                            ControlToValidate="txtHpsBukuTghRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Piutang Pembiayaan Hapus Tagih
                        </label>
                         <asp:TextBox ID="txtHpsTghRP" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ErrorMessage="*"
                            ControlToValidate="txtHpsTghRP" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div> 
                    <div class="form_right">
                        <label class="label_req">
                            Piutang Pembiayaan hapus buku
                        </label>
                         <asp:TextBox ID="txtHpsBukuMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ErrorMessage="*"
                            ControlToValidate="txtHpsBukuMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    <br />
                        <label class="label_req">
                            Piutang Pembiayaan hapus buku yang berhasil ditagih
                        </label>
                         <asp:TextBox ID="txtHpsBukuTghMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ErrorMessage="*"
                            ControlToValidate="txtHpsBukuTghMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                     <br />
                        <label class="label_req">
                            Piutang Pembiayaan Hapus Tagih
                        </label>
                         <asp:TextBox ID="txtHpsTghMA" runat="server" CssClass="medium_text rightAlign" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ErrorMessage="*"
                            ControlToValidate="txtHpsTghMA" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                      </div>
                   </div> 
                  <div class="form_box">
                    <div class="form_left">
                            <b><asp:Label ID="Label50" runat="server" Text="Total Piutang Pembiayaan hapus buku :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTHpsBuku" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label52" runat="server" Text="Total Piutang Pembiayaan hapus buku yang berhasil ditagih :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTHpsBukuTgh" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>      
                            <br />
                            <b><asp:Label ID="Label54" runat="server" Text="Total Piutang Pembiayaan Hapus Tagih :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTHpsTgh" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>              
                    </div>
                </div>      
                <div class="form_box total-yellow">
                    <div class="form_left">
                            <b><asp:Label ID="Label41" runat="server" Text="Total Rekening Administratif Lainnya :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTLainnya" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label56" runat="server" Text="Total Rekening Administratif Lainnya (RUPIAH) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTLainnyaRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>      
                            <br />
                            <b><asp:Label ID="Label58" runat="server" Text="Total Rekening Administratif Lainnya (VALAS) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTLainnyaMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>              
                    </div>
                </div>         
                <div class ="form_title">
                     <div class ="form_single">
                         <h4>
                         <b>TOTAL REKENING ADMINISTRATIF</b>
                         </h4>
                    </div>
                </div> 
                 <div class="form_box total-green">
                    <div class="form_left">
                            <b><asp:Label ID="Label53" runat="server" Text="Total Rekening Administratif :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTADM" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b> 
                            <br />
                            <b><asp:Label ID="Label59" runat="server" Text="Total Rekening Administratif (RUPIAH) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTADMRP" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>      
                            <br />
                            <b><asp:Label ID="Label61" runat="server" Text="Total Rekening Administratif (VALAS) :"></asp:Label></b>                      
                            <b><asp:Label ID="lblTOTADMMA" runat="server" CssClass="numberAlign2 regular_text" Text="">0</asp:Label></b>              
                    </div>
                </div>                       
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            
            </asp:Panel>
    </form>
</body>
</html>