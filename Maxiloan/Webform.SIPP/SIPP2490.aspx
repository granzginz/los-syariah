﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP2490.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP2490" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>Rincian Rupa-Rupa Aset</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>  
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
       
    
    </script>  
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                   SIPP2490 - RINCIAN RUPA-RUPA ASET
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">

                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <%--<div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>--%>
                <div class="form_box">
                        <div class="form_single">
                        <label>
                            Pilih Bulan Generate
                        </label>
                        <asp:TextBox ID="txtcopybulandata" runat="server" CssClass="medium_text" width="6%"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True"
                            TargetControlID="txtcopybulandata" Format="yyyy-MM-dd" PopupPosition="BottomRight"></asp:CalendarExtender>
                        </div>
                   </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>

                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle width="2%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="5%" HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" Visible="false">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JNS_RPRP_ST_PRSHN_PMBYN" SortExpression="JNS_RPRP_ST_PRSHN_PMBYN" HeaderText="JENIS" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JNS_MT_NG" SortExpression="JNS_MT_NG" HeaderText="JENIS VALUTA" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="RPRP_ST" SortExpression="RPRP_ST" HeaderText="NOMINAL" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    
                                    
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNavi2490" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button>               
                </div>

                <%--<div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI RINCIAN RUPA-RUPA ASET
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="JNS_RPRP_ST_PRSHN_PMBYN">Jenis</asp:ListItem>
                            <asp:ListItem Value="RPRP_ST">Nominal</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" CssClass="small button blue" Text="Search"></asp:Button>
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
                </div>--%>
            </asp:Panel>
            
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false"></asp:TextBox>
            
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                        <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                            ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis
                        </label>
                        <%--<asp:TextBox ID="txtJenis" runat="server" CssClass="medium_text"></asp:TextBox>--%>
                         <asp:DropDownList ID="cboJenis" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="cboJenis" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Jenis Valuta
                        </label>
                        
                         <asp:DropDownList ID="cboValuta" runat="server" Width ="20%" MaxLength ="100" Columns ="105" CssClass="medium_text"/>
                       <%-- <asp:TextBox ID="txtJenisValuta" runat="server" CssClass="medium_text"></asp:TextBox>--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="cboValuta" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="label_req">
                            Nominal
                        </label>
                        <asp:TextBox ID="txtNominal" runat="server" CssClass="medium_text" ></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtNominal" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
    </form>
</body>
</html>

