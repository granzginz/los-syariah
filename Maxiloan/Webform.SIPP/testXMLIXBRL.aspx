﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="testXMLIXBRL.aspx.vb" Inherits="Maxiloan.Webform.Implementasi.testXMLlXBRL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>001020350 - Form Profil Rincian Izin Usaha PP Bulanan Gabungan</title>
</head>
<body>
<form id="form1" runat="server">
     <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    001020350 - Form Profil Rincian Izin Usaha PP Bulanan Gabungan
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="NO"></asp:BoundColumn> 
                                    <asp:BoundColumn DataField="NMR_ZN_SH" SortExpression="NMR_ZN_SH" HeaderText="Nomor Izin Usaha">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="TNGGL_ZN_SH" SortExpression="TNGGL_ZN_SH" HeaderText="Tanggal Izin Usaha">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JNS_PRZNN" SortExpression="JNS_PRZNN" HeaderText="Jenis Perizinan">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="KTRNGN" SortExpression="KTRNGN" HeaderText="Keterangan">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                
            </asp:Panel>
    <div class="form_button">
    <asp:Button ID="btnGenerate" runat="server" Text="Create file"  CssClass="small button green"></asp:Button> 
        <asp:Button ID="Buttonsearch" runat="server" Text="Search" CssClass="small button blue"></asp:Button>
    </div>
    
</form>
</body>
</html>