﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SIPP0041.aspx.vb" Inherits="Maxiloan.Webform.SIPP.SIPP0041"  MaintainScrollPositionOnPostback="true"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title>SIPP0041 - Rincian Tenaga Kerja Berdasarkan Tingkat Pendidikan</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <link href="../Include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>  
    <script language="JavaScript" type="text/javascript">
        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
       
    
    </script>  
</head>

<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
        <div class="form_title">
            <div class="title_strip"></div>
            <div class="form_single">
                <h3>
                    SIPP0041 - Rincian Tenaga Kerja Berdasarkan Tingkat Pendidikan
                </h3>
            </div>
        </div>
            <asp:Panel ID="pnlList" runat="server">
                <%--untuk eksport--%>
                <div class="form_box_header">
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Pilih Bulan Data
                        </label>                           
                            <asp:DropDownList ID="cbobulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                </div>
                <div class="form_button">
                    <asp:Button ID="btnfind" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                    </asp:Button>
                </div>

                <asp:Panel ID="pnlcopybulandata" runat="server">
                    <div class="form_box_header">
                    </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                Copy Dari Bulan Data
                        </label>
                            <asp:DropDownList ID="cbocopybulandata" runat="server" Width ="7%" CssClass="medium_text"/>       
                    </div>              
                    </div>
                <div class="form_button">
                    <asp:Button ID="btncopybulandata" runat="server" CausesValidation="False" Text="Generate" CssClass="small button green">
                    </asp:Button>
                </div>
                </asp:Panel>
                <%--end--%>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgList" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                DataKeyField="ID" BorderStyle="None" BorderWidth="0" OnSortCommand="SortGrid"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle Width="2%"  CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle Width="7%" CssClass="command_col"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../Images/icondelete.gif"
                                                CommandName="DEL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn> 
                                    <asp:BoundColumn DataField="ID" SortExpression="ID" HeaderText="ID" Visible="false">
                                    </asp:BoundColumn>  
                                    <asp:BoundColumn DataField="BULANDATA" SortExpression="BULANDATA" HeaderText="BULAN DATA" ItemStyle-Width="15%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_" SortExpression="JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_ " HeaderText="Tenaga Kerja Tetap" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_" SortExpression="JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_ " HeaderText="Tenaga Kerja Kontrak" ItemStyle-Width="20%">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_" SortExpression="JMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_" HeaderText="Tenaga Kerja Oursourching" ItemStyle-Width="20%">
                                    </asp:BoundColumn> 
                                </Columns>
                            </asp:DataGrid>
                               <uc2:ucGridNav id="GridNaviSIPP0041" runat="server"/>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <%--untuk eksport--%>
                    <asp:Button ID="BtnExportToExcel" runat="server"  Text="Export To Excel" CssClass="small button green"></asp:Button> 
                    <%--end--%> 
                </div>

                
            </asp:Panel>
            <asp:Panel ID="pnlAdd" runat="server">
     
                        <asp:TextBox ID="txtid" runat="server" CssClass="medium_text" Visible="false">0</asp:TextBox>
            <%--untuk eksport--%>
                <div class="form_box">
                    <div class="form_single">
                        <label class="medium_text">
                                BulanData
                        </label>
                <asp:TextBox ID="txtbulandata" runat="server" CssClass="medium_text" Width="6%" ></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtbulandata" Format="yyyyMM" PopupPosition="BottomRight" DefaultView="Months"></asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                    ControlToValidate="txtbulandata" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <%--end--%>
        <%--KANTOR PUSAT--%>

                <div class ="form_title">
                     <div class ="form_single">
                         <h3>
                         <b>Tenaga Kerja Kantor Pusat</b>
                         </h3>
                         <hr />
                        <h4>
                            <b>Tenaga Kerja Tetap</b>
                        </h4>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label23" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label25" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL" runat="server" CssClass="medium_text" Width="5%"  Enabled="false">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator> 
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%"   AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label24" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label2" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label26" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label33" runat="server" Text="Total Keseluruhan Pegawai Tetap"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator70" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator71" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator72" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK" runat="server"  CssClass="" Width="5%" Enabled="false"  >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator85" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN" runat="server"  CssClass="medium_text" Width="5%" Enabled="false"  >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator86" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Pegawai Tetap</label>
                        <asp:label ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_" runat="server"   CssClass="medium_text" Width="5%" Enabled="false" >0</asp:label>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator87" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TTP_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label27" runat="server" Text="Tenaga Kerja Kontrak"></asp:Label>
                        </b>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label28" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label29" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator68" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator69" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator73" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator74" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator75" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label30" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label31" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator76" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator77" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator78" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator79" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator80" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator81" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label32" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label34" runat="server" Text="Total Keseluruhan Pegawai Kontrak"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator82" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator83" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator84" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK" runat="server"    CssClass="" Width="5%" Enabled="false" >0</asp:TextBox>
                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator88" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN" runat="server"    CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator89" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Pegawai Kontrak</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_" runat="server"   CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator90" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_KNTRK_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
               <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label35" runat="server" Text="Tenaga Kerja Outsourching"></asp:Label>
                        </b>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label36" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label37" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator91" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator92" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL" runat="server"   CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator93" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator94" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator95" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator96" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label38" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label39" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator97" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator98" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator99" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator100" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator101" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL" runat="server"   CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator102" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label40" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label41" runat="server" Text="Total Keseluruhan Pegawai Outsourching"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator103" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator104" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator105" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK" runat="server"  CssClass="" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator106" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator107" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Pegawai Outsourching</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator108" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_PST_TTL_TNG_KRJ_TSRCNG_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>

                <%--CABANG--%>
                           <div class ="form_title">
                     <div class ="form_single">
                         <h3>
                         <b>Tenaga Kerja Kantor Cabang</b>
                         </h3>
                         <hr />
                        <h4>
                            <b>Tenaga Kerja Tetap</b>
                        </h4>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label1" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label3" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL"  runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label4" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label5" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label6" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label7" runat="server" Text="Total Keseluruhan Pegawai Tetap"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" Enabled="false"  >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" Enabled="false"  >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Pegawai Tetap</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TTP_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label8" runat="server" Text="Tenaga Kerja Kontrak"></asp:Label>
                        </b>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label9" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label10" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM"   >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator35" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label11" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label12" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM"  >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator40" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator42" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator43" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label13" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label14" runat="server" Text="Total Keseluruhan Pegawai Kontrak"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator44" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator45" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator46" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%" Enabled="false"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator47" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%" Enabled="false"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator48" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total Pegawai Kontrak</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator49" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
               <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label15" runat="server" Text="Tenaga Kerja Outsourching"></asp:Label>
                        </b>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label16" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label17" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator50" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator51" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator52" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator53" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator54" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator55" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label18" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label19" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator56" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator57" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator58" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator59" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator60" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator61" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label20" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label21" runat="server" Text="Total Keseluruhan Pegawai Outsourching"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator62" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator63" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator64" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator65" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator66" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Pegawai Outsourching</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator67" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>

                <%--Kantor Selain Kantor Cabang--%>
                <div class ="form_title">
                     <div class ="form_single">
                         <h3>
                         <b>Tenaga Kerja Kantor Selain Kantor Cabang</b>
                         </h3>
                         <hr />
                        <h4>
                            <b>Tenaga Kerja Tetap</b>
                        </h4>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label22" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label42" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator109" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator110" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator111" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator112" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator113" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator114" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label43" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label44" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator115" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator116" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator117" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator118" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator119" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator120" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label45" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label46" runat="server" Text="Total Keseluruhan Pegawai Tetap"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator121" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator122" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator123" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TTP_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK" runat="server" CssClass="" Width="5%" Enabled="false"  >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator124" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" Enabled="false"  >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator125" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Pegawai Tetap</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator126" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TTP_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label47" runat="server" Text="Tenaga Kerja Kontrak"></asp:Label>
                        </b>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label48" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label49" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator127" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator128" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator129" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator130" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator131" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator132" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label50" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label51" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator133" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator134" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator135" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator136" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator137" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator138" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label52" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label53" runat="server" Text="Total Keseluruhan Pegawai Kontrak"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator139" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator140" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator141" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_KNTRK_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK" runat="server"   CssClass="" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator142" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN" runat="server"   CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator143" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Pegawai Kontrak</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator144" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_KNTRK_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
               <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="Label54" runat="server" Text="Tenaga Kerja Outsourching"></asp:Label>
                        </b>        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label55" runat="server" Text="Lainnya"></asp:Label>
                        </b>        
                    </div>

                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label56" runat="server" Text="SLTA"></asp:Label>
                        </b> 
                    </div>
                </div>
      
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator145" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator146" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator147" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                            
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator148" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator149" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator150" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label57" runat="server" Text="Diploma"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label58" runat="server" Text="Sarjana"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                         <label class="label_req">Laki-Laki</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator151" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Perempuan</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator152" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                         <br />
                         <label class="label_req">Total</label>
                         <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL" runat="server"   CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator153" runat="server" ErrorMessage="*"
                         ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator154" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator155" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator156" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label59" runat="server" Text="Pasca Sarjana"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label60" runat="server" Text="Total Keseluruhan Pegawai Outsourching"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator157" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%"  AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator158" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <br />
                        <label class="label_req">Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator159" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TNG_KRJ_TSRCNG_TTL" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Total Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK" runat="server"   CssClass="" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator160" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_LKLK" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator161" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_PRMPN" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <br />
                        <label class="label_req">Total Pegawai Outsourching</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_" runat="server"  CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator162" runat="server" ErrorMessage="*"
                        ControlToValidate="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL_TNG_KRJ_TSRCNG_" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>  --%>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="Label61" runat="server" Text="Total Tenaga Kerja Kantor Pusat"></asp:Label>
                        </b> 
                    </div>
                    <div class="form_right">
                        <b>
                            <asp:Label ID="Label62" runat="server" Text="Total Tenaga Kerja Cabang"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Pusat Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TTL__" runat="server" CssClass="" Width="5%" Enabled="false"  >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Pusat Tingkat Pendidikan Lainnya di Bawah</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_" runat="server" CssClass="medium_text" Width="5%"  Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Pusat Tingkat SLTA</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SLT_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Pusat Tingkat Diploma</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_DPLM_TTL_" runat="server" CssClass="" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Pusat Tingkat Sarjana</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_SRJN_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Pusat Tingkat Pasca Sarjana</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_PST_PSC_SRJN_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Cabang Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TTL__" runat="server" CssClass="" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Cabang Tingkat Pendidikan Lainnya di Bawah</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Cabang Tingkat SLTA</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SLT_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Cabang Tingkat Diploma</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_DPLM_TTL_" runat="server" CssClass="" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Cabang Tingkat Sarjana</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_SRJN_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Kantor Cabang Tingkat Pasca Sarjana</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_CBNG_PSC_SRJN_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <b>
                            <asp:Label ID="txtLabel63" runat="server" Text="Total Tenaga Kerja Selain Kantor Cabang"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Jumlah Tenaga Kerja Selain Kantor Cabang Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TTL__" runat="server" CssClass="" Width="5%"  Enabled="false"  >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Selain Kantor Cabang Tingkat Pendidikan Lainnya di Bawah</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_TNGKT_PNDDKN_LNNY_D_BWH_SLT_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Selain Kantor Cabang Tingkat SLTA</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SLT_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false"  >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Selain Kantor Cabang Tingkat Diploma</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_DPLM_TTL_" runat="server" CssClass="" Width="5%"  Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Selain Kantor Cabang Tingkat Sarjana</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_SRJN_TTL_" runat="server" CssClass="medium_text" Width="5%"  Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Selain Kantor Cabang Tingkat Pasca Sarjana</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_KNTR_SLN_KNTR_CBNG_PSC_SRJN_TTL_" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <b>
                            <asp:Label ID="txtLabel64" runat="server" Text="Total Tenaga Kerja"></asp:Label>
                        </b> 
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">Total Tenaga Kerja Tetap</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_" runat="server" CssClass="" Width="5%"  Enabled="false"  AutoPostBack="true" OnTextChanged="txtSUM" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Total Tenaga Kerja Tetap Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_LKLK" runat="server" CssClass="medium_text" Width="5%"  Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Total Tenaga Kerja Tetap Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TTP_PRMPN" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Total Tenaga Kerja Kontrak</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_" runat="server" CssClass="" Width="5%"  Enabled="false" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Total Tenaga Kerja Kontrak Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_LKLK" runat="server" CssClass="medium_text" Width="5%"  Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Total Tenaga Kerja Kontrak Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_KNTRK_PRMPN" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                    </div>
                    <div class="form_right">
                        <label class="label_req">Jumlah Tenaga Kerja Total Tenaga Kerja Outsourcing</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_" runat="server" CssClass="" Width="5%"  Enabled="false" AutoPostBack="true" OnTextChanged="txtSUM">0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Total Tenaga Kerja Outsourcing Laki-Laki</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_LKLK" runat="server" CssClass="medium_text" Width="5%"  Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Total Tenaga Kerja Outsourcing Perempuan</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL_TNG_KRJ_TSRCNG_PRMPN" runat="server" CssClass="medium_text" Width="5%" Enabled="false" >0</asp:TextBox>
                        <br />
                        <label class="label_req">Jumlah Tenaga Kerja Total</label>
                        <asp:TextBox ID="txtJMLH_TNG_KRJ_TTL__" runat="server" CssClass="medium_text" Width="5%" Enabled="false"  >0</asp:TextBox>
                    </div> 
                </div>
                
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                   <%-- <asp:Button ID="ButtonCancel" runat="server" CausesValidation="false" Text="Cancel" CssClass="small button gray">
                    </asp:Button>--%>
                </div>
            </asp:Panel>
    </form>
</body>
</html>
