#Region "Revision History"

'-----------------------------------------------------------
#End Region

#Region "CacheHelper"
Public Class CacheHelper

#Region "Document"

    Public Const DOCUMENT_TYPE_LIST As String = "DOCUMENTTYPELIST"
    Public Const DOCUMENT_ELEMENT_LIST As String = "DOCUMENTELEMENTLIST"
    Public Const DOCUMENT_DETAIL As String = "DOCUMENTDETAIL"
    Public Const DOCUMENT_DETAIL_FIELD As String = "DOCUMENTDETAILFIELD"

#End Region


    Public Const USER_PROFILE As String = "USERPROFILE"
    Public Const GROUP_SCREEN As String = "GROUPSCREEN"
    Public Const USER_GROUP As String = "USERGROUP"

    Public Const LAYOUT_LIST As String = "LAYOUTLIST_"
    Public Const DOCUMENT_TEMPLATE_LIST As String = "DOCUMENTTEMPLATELIST"
    Public Const DOCUMENT_TEMPLATE_PREFIX As String = "DOCUMENTTEMPLATE_"
    Public Const ATTRIBUTE_PREFIX As String = "ATTRIBUTE_"
    Public Const LAYOUT_PREFIX As String = "LAYOUT_"
    Public Const ATTRIBUTE_LIST As String = "ATTRIBUTELIST"
    Public Const CASE_TEMPLATE_LIST As String = "CASETEMPLATELIST"
    Public Const CASE_TEMPLATE_ATTRIBUTE_LIST As String = "CASETEMPLATEATTRIBUTELIST"
    Public Const ADVANCE_SEARCH As String = "ADVANCESEARCH"
    Public Const ORGANIZATION_LIST As String = "ORGANIZATION"
    Public Const LOOK_UP_ATTRIBUTE_LIST As String = "LOOKUPATTRIBUTE"
    Public Const DATA_TYPE As String = "DATATYPE"
    Public Const HELP_SCREEN_LIST As String = "HELPSCREENLIST"
    Public Const LOOK_UP_LIST As String = "LOOKUP"
    Public Const USER_ENTITY As String = "USERENTITY"
    Public Const UNIT_LIST As String = "UNITLIST"
    Public Const GROUP As String = "GROUP"
    Public Const MENU As String = "MENU"
    Public Const HIERARCHY_ORG As String = "HIERARCHYORG"
    Public Const MODEL_LIST As String = "MODELLIST"
    Public Const MEMBER_GROUP As String = "MEMBERGROUP"
    Public Const OFFICE_LIST As String = "OFFICELIST"
    Public Const MEMBER_GROUP_USER_TERDAFTAR As String = "GROUPUSERTERDAFTAR"
    Public Const MEMBER_GROUP_USER_BELUM_TERDAFTAR As String = "GROUPUSERBELUMTERDAFTAR"
    Public Const MEMBER_GROUP_USER_BY_NAME As String = "GETUSERBYNAME"
    Public Const MEMBER_GROUP_USER_BY_ID As String = "GETUSERBYID"
    Public Const ROLE_LIST As String = "ROLELIST"
    Public Const MODEL_STRUCTURE_LIST As String = "MODELSTRUCTURELIST"
    Public Const MENU_GROUP As String = "MENUGROUP"
    Public Const MENU_GROUP_TERDAFTAR As String = "MENUGROUPTERDAFTAR"
    Public Const MENU_GROUP_BELUM_TERDAFTAR As String = "MENUGROUPBELUMTERDAFTAR"
    Public Const GROUP_MENU_BY_NAME As String = "GROUPMENUBYNAME"
    Public Const GROUP_MENU_BY_ID As String = "GROUPMENUBYID"
    Public Const KPP_KARIKPA As String = "KPPKARIKPA"
    Public Const UNIT_STRUCTURE_LIST As String = "UNITSTRUCTURELIST"
    Public Const ORG_MODEL_LIST As String = "ORGMODELLIST"
    Public Const CASE_TEMPLATE_ATTRIBUTE_FIND As String = "CASETEMPLATEATTRIBUTEFIND"
    Public Const MEMBER_GROUP_NAME As String = "MEMBERGROUPNAME"
    Public Const CALENDAR_MANAGEMENT As String = "CALENDARMANAGEMENT"
    Public Const DATA_CALENDAR As String = "DATACALENDAR"
    Public Const DATA_HOLIDAY As String = "DATAHOLIDAY"
    Public Const DATA_DIFFERENT_TIME As String = "DATADIFFERENTTIME"
    Public Const MS_WORKFLOW As String = "MSWORKFLOW"
    Public Const DISTRIBUTION_RULE_LIST As String = "DISTRIBUTIONRULE"
    Public Const WORKFLOW_ACTIVITY As String = "WORKFLOWACTIVITY"
    Public Const VALUE_USER_MAP As String = "VALUEUSERMAP"
    Public Const ACTIVITY_ATTRIBUTES As String = "ACTIVITYATTRIBUTES"
    Public Const ACTIVITY_ARRANGEMENT As String = "ACTIVITYARRANGEMENT"
    Public Const ACTIVITY_SUBCASE As String = "ACTIVITYSUBCASE"
    Public Const ACTIVITY_DOCUMENT As String = "ACTIVITYDOCUMENT"
    Public Const ACTIVITY_CHECKLIST As String = "ACTIVITYCHECKLIST"
    Public Const PERCENTAGE_SETTING As String = "PERCENTAGESETTING"
    Public Const GET_ORG As String = "GETORG"
    Public Const GET_UNIT As String = "GETUNIT"
    Public Const GET_ROLE As String = "GETROLE"
    Public Const USER_SEQUENCE As String = "USERSEQUENCE"
    Public Const SEARCH_GENERATOR As String = "SEARCHGENERATOR"
    Public Const SYSLIST As String = "SYSLIST"
    Public Const CREATE_TYPE_NUMID As String = "CREATETYPENUMID"
    Public Const GROUP_BY_USER As String = "GROUPBYUSER"
    Public Const SCREEN_GROUP As String = "SCREENGROUP"
    Public Const CASE_WORKFLOW_LIST As String = "CASEWORKFLOW"

End Class
#End Region

Public Class UrlHelper

    Public Const URL_IMAGES_TREE As String = "../images/ftv2lastnode.gif"

End Class

Public Class MessageHelper

#Region "Document"

    Public Const MESSAGE_DOCUMENT_EVENT_INVALID As String = "Pengisian Even Dokument '{0}' Salah, dicek lagi"
    Public Const MESSAGE_DOCUMENT_FORMULA_GET_PARAM As String = "Pengisian Formula Salah, dicek lagi"
    Public Const MESSAGE_DOCUMENT_SOURCE_DOC_ALREADY_EXIST As String = "Source Document sudah terdaftar"
    Public Const MESSAGE_DOCUMENT_SOURCE_FIELD_NOT_SET As String = "Data tidak bisa disimpan sebelum Field Document di set"
    Public Const MESSAGE_DATA_NOT_FOUND As String = "Data tidak ketemu"

    Public Const MESSAGE_FIELD_DISPLAY_TYPE_INVALID As String = "Tampilan harus '{0}' jika Tipe Field adalah '{1}'"
    Public Const MESSAGE_FIELD_FORMULA_TYPE_INVALID As String = "Formula harus '{0}' jika Tipe Field adalah '{1}'"
    Public Const MESSAGE_FIELD_FORMAT_TYPE_INVALID As String = "Format harus '{0}' jika Tipe Field adalah '{1}'"
    Public Const MESSAGE_FIELD_VALIDATOR_TYPE_INVALID As String = "Validasi harus '{0}' jika Tipe Field adalah '{1}'"
    Public Const MESSAGE_FIELD_DATA_TYPE_INVALID As String = "Tipe Data harus '{0}' jika Tampilan adalah '{1}'"
    Public Const MESSAGE_FIELD_FORMULA_TYPE_DISPLAY As String = "Formula harus '{0}' jika Tampilan adalah '{1}'"
    Public Const MESSAGE_VALIDATION_INVALID As String = "Nilai Validasi {0} harus diisi"
    Public Const MESSAGE_FORMAT_NOT_FILL As String = "Format harus diisi"
    Public Const MESSAGE_FORMAT_INVALID As String = "Tipe Format tidak sama dengan Format"
    Public Const MESSAGE_FORMULA_TYPE_INVALID As String = "Pilihan Tipe Formula tidak sama dengan di layar, cek lagi"
    Public Const MESSAGE_DETAIL_FIELD_FORMULA_INVALID As String = "Formula harus '{0}' jika Tipe Detil adalah '{1}'"
    Public Const MESSAGE_DETAIL_FIELD_TYPE_INVALID As String = "Tipe Detil Field harus '{0}' jika Tipe Detil adalah '{1}'"
    Public Const MESSAGE_FORMULA_FILL_INVALID As String = "Pengisian Formula salah, cek lagi"
    Public Const MESSAGE_FORMULA_MAPPING_INVALID As String = "Pengisian Formula hanya bisa Formula atau MappingField"

#End Region

    Public Const MESSAGES_NO_ACCESS As String = " Anda tidak mendapatkan hak akses halaman ini "
    Public Const MESSAGE_INSERT_FAILED As String = " A record already exists with the same primary key! "
    Public Const MESSAGE_INSERT_SUCCESS As String = " Record is added successfully! "
    Public Const MESSAGE_DELETE_SUCCESS As String = " Record is deleted successfully! "
    Public Const MESSAGE_DELETE_FAILED As String = " Unable to delete the selected record. Record already used! "
    Public Const MESSAGE_UPDATE_SUCCESS As String = " Record Is Update Successfully "
    Public Const MESSAGE_UPDATE_FAILED As String = " Record Is Update Failed "
    Public Const MESSAGE_GENERATE As String = " Case Template sudah tergenerate ..."
    Public Const MESSAGE_UNGENERATE As String = " Case Template belum tergenerate, tekan Bangun ..."
    Public Const MESSAGES_DOC_TARGET_CHANGED As String = " Dokumen Target tidak sama "
    Public Const MESSAGES_DATA_INCOMPLETE As String = " Data tidak lengkap "
    Public Const MESSAGES_FIELD_ALREADY_EXISTS As String = " Field sudah ada "
    Public Const MESSAGE_USER_FAILED As String = " Maaf, nama user salah "
    Public Const MESSAGE_PASS_FAILED As String = " Maaf, password salah "
    Public Const MESSAGE_IS_DEFAULT_EXIST As String = " Default=1 sudah dipunyai oleh DocumentID yang bersangkutan "
    Public Const MESSAGE_SCREEN_ID_DO_NOT_FILLED As String = " Screen ID jangan diisi "
    Public Const MESSAGE_DATA_DETAIL_NOT_FOUND As String = "Detail Data tidak diketemukan !"
    Public Const MESSAGE_DATA_DETAIL_NOT_FOUND_ASSETUSAGE As String = "Check Data Asset Usage "

End Class

Public Class StateHelper

#Region "Document"

    Public Const DOC_FORMATNO_ELEMENT As String = "ELEMENT"
    Public Const FIELD_RELATION As String = "FIELD"
    Public Const DOC_EVENT As String = "DOCUMENT_EVENT"
    Public Const DOC_FORMULA_PARAMETER As String = "FORMULA_PARAMETER"

#End Region

    Public Const USER_PROFILE As String = "USERPROFILE"
    Public Const GROUP_SCREEN As String = "GROUPSCREEN"
    Public Const USER_GROUP As String = "USERGROUP"

    Public Const LAYOUT As String = "LAYOUT"
    Public Const ATTRIBUTE_LAYOUT As String = "ATTRIBUTELAYOUT"
    Public Const DOCUMENT_TEMPLATE As String = "DOCUMENTTEMPLATE"
    Public Const DOCUMENT_ATTRIBUTES As String = "DOCUMENTATTRIBUTES"
    Public Const ATTRIBUTE_COLLECTION As String = "ATTRIBUTECOLLECTION"
    Public Const DOCUMENT_DATA As String = "DOCUMENTDATA"
    Public Const ATTRIBUTE_LIST As String = "ATTRIBUTELIST"
    Public Const CASE_TEMPLATE_LIST As String = "CASETEMPLATELIST"
    Public Const CASE_TEMPLATE_ATTRIBUTE_LIST As String = "CASETEMPLATEATTRIBUTELIST"
    Public Const ADVANCE_SEARCH As String = "ADVANCESEARCH"
    Public Const ORGANIZATION_LIST As String = "ORGANIZATION"
    Public Const LOOK_UP_ATTRIBUTE_LIST As String = "LOOKUPATTRIBUTE"
    Public Const DATA_TYPE As String = "DATATYPE"
    Public Const HELP_SCREEN_LIST As String = "HELPSCREENLIST"
    Public Const LOOK_UP_LIST As String = "LOOKUP"
    Public Const USER_ENTITY As String = "USERENTITY"
    Public Const UNIT_LIST As String = "UNITLIST"
    Public Const GROUP As String = "GROUP"
    Public Const MENU As String = "MENU"
    Public Const HIERARCHY_ORG As String = "HIERARCHYORG"
    Public Const MODEL_LIST As String = "MODELLIST"
    Public Const SCREEN_GROUP As String = "SCREENGROUP"
    Public Const MEMBER_GROUP As String = "MEMBERGROUP"
    Public Const OFFICE_LIST As String = "OFFICELIST"
    Public Const MEMBER_GROUP_USER_TERDAFTAR As String = "GROUPUSERTERDAFTAR"
    Public Const MEMBER_GROUP_USER_BELUM_TERDAFTAR As String = "GROUPUSERBELUMTERDAFTAR"
    Public Const MEMBER_GROUP_USER_BY_NAME As String = "GETUSERBYNAME"
    Public Const ROLE_LIST As String = "ROLELIST"
    Public Const MODEL_STRUCTURE_LIST As String = "MODELSTRUCTURELIST"
    Public Const MENU_GROUP As String = "MENUGROUP"
    Public Const MENU_GROUP_TERDAFTAR As String = "MENUGROUPTERDAFTAR"
    Public Const MENU_GROUP_BELUM_TERDAFTAR As String = "MENUGROUPBELUMTERDAFTAR"
    Public Const GROUP_MENU_BY_NAME As String = "GROUPMENUBYNAME"
    Public Const MEMBER_GROUP_USER_BY_ID As String = "GETUSERBYID"
    Public Const GROUP_MENU_BY_ID As String = "GROUPMENUBYID"
    Public Const KPP_KARIKPA As String = "KPPKARIKPA"
    Public Const UNIT_STRUCTURE_LIST As String = "MODELSTRUCTURELIST"
    Public Const ORG_MODEL_LIST As String = "ORGMODELLIST"
    Public Const CASE_TEMPLATE_ATTRIBUTE_FIND As String = "CASETEMPLATEATTRIBUTEFIND"
    Public Const MEMBER_GROUP_NAME As String = "MEMBERGROUPNAME"
    Public Const CALENDAR_MANAGEMENT As String = "CALENDARMANAGEMENT"
    Public Const DATA_CALENDAR As String = "DATACALENDAR"
    Public Const DATA_HOLIDAY As String = "DATAHOLIDAY"
    Public Const DATA_DIFFERENT_TIME As String = "DATADIFFERENTTIME"
    Public Const MS_WORKFLOW As String = "MSWORKFLOW"
    Public Const DISTRIBUTION_RULE_LIST As String = "DISTRIBUTIONRULE"
    Public Const WORKFLOW_ACTIVITY As String = "WORKFLOWACTIVITY"
    Public Const VALUE_USER_MAP As String = "VALUEUSERMAP"
    Public Const ACTIVITY_ATTRIBUTES As String = "ACTIVITYATTRIBUTES"
    Public Const ACTIVITY_ARRANGEMENT As String = "ACTIVITYARRANGEMENT"
    Public Const ACTIVITY_SUBCASE As String = "ACTIVITYSUBCASE"
    Public Const ACTIVITY_DOCUMENT As String = "ACTIVITYDOCUMENT"
    Public Const ACTIVITY_CHECKLIST As String = "ACTIVITYCHECKLIST"
    Public Const PERCENTAGE_SETTING As String = "PERCENTAGESETTING"
    Public Const GET_ORG As String = "GETORG"
    Public Const GET_UNIT As String = "GETUNIT"
    Public Const GET_ROLE As String = "GETROLE"
    Public Const USER_SEQUENCE As String = "USERSEQUENCE"
    Public Const SEARCH_GENERATOR As String = "SEARCHGENERATOR"
    Public Const SYSLIST As String = "SYSLIST"
    Public Const CREATE_TYPE_NUMID As String = "CREATETYPENUMID"
    Public Const GROUP_BY_USER As String = "GROUPBYUSER"
    Public Const CASE_WORKFLOW_LIST As String = "CASEWORKFLOW"

    Public Const PAGE_TRANSFER_GROUP_ID As String = "GroupID"
End Class

Public Class SettingTextHelper

    '================================= General =======================================
    Public Const ADD_TEXT As String = "TAMBAH "
    Public Const EDIT_DETAIL_TEXT As String = "EDIT / DETAIL "
    Public Const SETTING_TEXT As String = "PENGATURAN "

    '================================= Header Text =======================================
    Public Const ACTIVITY_ADDINFO As String = "AKTIFITAS ADD INFO"
    Public Const ACTIVITY_ARRANGEMENT As String = "AKTIFITAS ARRANGEMENT"
    Public Const ACTIVITY_SUBCASE As String = "SUB KASUS AKTIFITAS"
    Public Const ACTIVITY_DOCUMENT As String = "DOKUMEN AKTIFITAS"
    Public Const ACTIVITY_CHECKLIST As String = "CHECKLIST AKTIFITAS"
    Public Const ATTRIBUTE As String = "ATRIBUT"
    Public Const CASEENTRY_BY_ROLE As String = "MEMASUKKAN KASUS SESUAI JABATAN"
    Public Const CASEFILTER_BY_ROLE As String = "MENGURUTKAN KASUS SESUAI JABATAN"
    Public Const CASEID_DOCUMENT As String = "DOKUMEN KODE KASUS"
    Public Const CASE_TEMPLATE As String = "TEMPLATE KASUS"
    Public Const CASE_TEMPLATE_ATTRIBUTE As String = "ATRIBUT TEMPLATE KASUS"
    Public Const CASE_VARIANT As String = "VARIAN KASUS"
    Public Const CASE_WORKFLOW As String = "WORKFLOW KASUS"
    Public Const CONDITION_USER As String = "KODISI PENGGUNA"
    Public Const DISTRIBUTION_ROLE As String = "DISTRIBUSI JABATAN"
    Public Const DOC_LAYOUT_USAGE As String = "USAGE LAYOUT DOKUMEN"
    Public Const DOC_TEMPLATE_USAGE As String = "USAGE TEMPLATE DOKUMEN"
    Public Const HIERARCHY_ORG As String = "HIRARKI ORGANISASI"
    Public Const KPP_KARIKPA As String = "KPPKARIKPA"
    Public Const MODEL As String = "MODEL"
    Public Const MODEL_STRUCTURE As String = "STRUKTUR MODEL ORGANISASI"
    Public Const MSWORKFLOW As String = "MASTER WORKFLOW"
    Public Const OFFICE As String = "KANTOR"
    Public Const ORGANIZATION As String = "ORGANISASI"
    Public Const ORG_MODEL As String = "MODEL ORGANISASI"
    Public Const PERCENTAGE_SETTING As String = "PERSENTASI SETTING"
    Public Const ROLE As String = "JABATAN"
    Public Const UNIT As String = "UNIT"
    Public Const UNIT_STRUCTURE As String = "STRUKTUR UNIT"
    Public Const USER_SEQUENCE As String = "URUTAN PENGGUNA"
    Public Const VALUE_USER_MAPPING As String = "NILAI MAPPING PENGGUNA"
    Public Const WORKFLOW_ACTIVITY As String = "AKTIFITAS WORKFLOW"

End Class