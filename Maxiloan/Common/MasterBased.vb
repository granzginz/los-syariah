﻿Imports System.Web
Imports System.Configuration
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class MasterBased : Inherits System.Web.UI.MasterPage

    Protected Property AppId() As String
        Get
            Return (CType(Session("AppId"), String))
        End Get
        Set(ByVal Value As String)
            Session("AppId") = Value
        End Set
    End Property

    Protected Property Loginid() As String
        Get
            Return (CType(Session("LoginId"), String))
        End Get
        Set(ByVal Value As String)
            Session("LoginId") = Value
        End Set
    End Property

    Protected Property GroubDbID() As String
        Get
            Return (CType(Session("groupdbid"), String))
        End Get
        Set(ByVal Value As String)
            Session("groupdbid") = Value
        End Set
    End Property

    Protected Property Password() As String
        Get
            Return (CType(Session("Password"), String))
        End Get
        Set(ByVal Value As String)
            Session("Password") = Value
        End Set
    End Property
End Class
