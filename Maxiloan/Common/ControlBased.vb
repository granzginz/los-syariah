Imports Maxiloan.SQLEngine.DataAccessBase

Public Class ControlBased
    Inherits System.Web.UI.UserControl

#Region "Property"

    Protected Property IsHoBranch() As Boolean
        Get
            Return (CType(Session("IsHoBranch"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            Session("IsHoBranch") = Value
        End Set
    End Property

    Protected Property BusinessDate() As DateTime
        Get
            Return (CType(Session("sesBusinessDate"), DateTime))
        End Get
        Set(ByVal Value As DateTime)
            Session("sesBusinessDate") = Value
        End Set
    End Property

    Protected Property GroubDbID() As String
        Get
            Return (CType(Session("groupdbid"), String))
        End Get
        Set(ByVal Value As String)
            Session("groupdbid") = Value
        End Set
    End Property

    Protected Property sesBranchId() As String
        Get
            Return (CType(Session("sesBranchID"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesBranchId") = Value
        End Set
    End Property

    Protected Property SortBy() As String
        Get
            Return CType(Viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property

    Protected Property Loginid() As String
        Get
            Return (CType(Session("LoginId"), String))
        End Get
        Set(ByVal Value As String)
            Session("LoginId") = Value
        End Set
    End Property

    Protected Property Password() As String
        Get
            Return (CType(Session("Password"), String))
        End Get
        Set(ByVal Value As String)
            Session("Password") = Value
        End Set
    End Property

    Protected Property AppId() As String
        Get
            Return (CType(Session("AppId"), String))
        End Get
        Set(ByVal Value As String)
            Session("AppId") = Value
        End Set
    End Property

    Protected Property ServerName() As String
        Get
            Return (CType(Session("ServerName"), String))
        End Get
        Set(ByVal Value As String)
            Session("ServerName") = Value
        End Set
    End Property

    Protected Property BaseCurrency() As String
        Get
            Return (CType(Session("BaseCurrency"), String))
        End Get
        Set(ByVal Value As String)
            Session("BaseCurrency") = Value
        End Set
    End Property

    Protected Property LoginNo() As String
        Get
            Return (CType(Session("LoginNo"), String))
        End Get
        Set(ByVal Value As String)
            Session("LoginNo") = Value
        End Set
    End Property

    Protected Property DataBaseName() As String
        Get
            Return (CType(Session("DataBaseName"), String))
        End Get
        Set(ByVal Value As String)
            Session("DataBaseName") = Value
        End Set
    End Property

    Protected Property UserID() As String
        Get
            Return (CType(Session("UserID"), String))
        End Get
        Set(ByVal Value As String)
            Session("UserID") = Value
        End Set
    End Property

    Protected Property PassKey() As String
        Get
            Return (CType(Session("PassKey"), String))
        End Get
        Set(ByVal Value As String)
            Session("PassKey") = Value
        End Set
    End Property

    Protected Property SearchBy() As String
        Get
            Return (CType(viewstate("SearchBy"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("SearchBy") = Value
        End Set
    End Property

    Protected Property sesBranchName() As String
        Get
            Return (CType(Session("BranchName"), String))
        End Get
        Set(ByVal Value As String)
            Session("BranchName") = Value
        End Set
    End Property


    Protected Property ServerNameRS() As String
        Get
            Return (CType(Session("sesServerNameRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesServerNameRS") = Value
        End Set
    End Property

    Protected Property DatabaseNameRS() As String
        Get
            Return (CType(Session("sesDatabaseNameRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesDatabaseNameRS") = Value
        End Set
    End Property

    Protected Property VirtualDirectoryRS() As String
        Get
            Return (CType(Session("sesVirtualDirectoryRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesVirtualDirectoryRS") = Value
        End Set
    End Property

    Protected Property ReportManagerRS() As String
        Get
            Return (CType(Session("sesReportManagerRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesReportManagerRS") = Value
        End Set
    End Property

    Protected Property UIDRS() As String
        Get
            Return (CType(Session("sesUIDRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesUIDRS") = Value
        End Set
    End Property

    Protected Property PassRS() As String
        Get
            Return (CType(Session("sesPassRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesPassRS") = Value
        End Set
    End Property

#End Region

    Protected Function GetConnectionString() As String
        Dim Conn As String
        Conn = "Server=" & Me.ServerName & ";Database=" & Me.DataBaseName & ";UID=" & Me.UserID & ";Pwd=" & Me.PassKey
        Return Conn
    End Function

    Protected Shared Function ConvertDate(ByVal pStrValue As String) As String
        Dim lStrValue As String = Trim(pStrValue)
        Dim lArrValue As Array
        lArrValue = CType(lStrValue.Split(CChar("/")), Array)
        If UBound(lArrValue) = 2 Then
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(0).ToString), 2), 0)
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(1).ToString), 2), 1)
            If Len(lArrValue.GetValue(2)) = 2 Then
                lArrValue.SetValue(Left(CStr(Year(Now())), 2) + lArrValue.GetValue(2).ToString, 2)
            End If
            lStrValue = lArrValue.GetValue(2).ToString & lArrValue.GetValue(1).ToString & lArrValue.GetValue(0).ToString
        End If
        Return lStrValue
    End Function

    Protected Shared Function ConvertDate2(ByVal pStrValue As String) As Date
        Dim lStrValue As String = Trim(CStr(pStrValue))
        Dim lArrValue As Array
        lArrValue = CType(lStrValue.Split(CChar("/")), Array)
        If UBound(lArrValue) = 2 Then
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(0).ToString), 2), 0)
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(1).ToString), 2), 1)
            If Len(lArrValue.GetValue(2)) = 2 Then
                lArrValue.SetValue(Left(CStr(Year(Now())), 2) + lArrValue.GetValue(2).ToString, 2)
            End If
            lStrValue = lArrValue.GetValue(1).ToString & "/" & lArrValue.GetValue(0).ToString & "/" & lArrValue.GetValue(2).ToString
        End If
        Return CDate(lStrValue)
    End Function

    Protected Function IsSingleBranch() As Boolean
        Dim strBranch() As String
        strBranch = Split(Me.sesBranchId, ",")
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        If UBound(strBranch) > 0 Then
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strNameServer = Request.ServerVariables("SERVER_NAME")
            Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            Return False
            Exit Function
        Else
            Return True
        End If
    End Function

    Protected Function GetConnectionStringRS() As String
        Dim Conn As String
        If Me.ServerNameRS = "" Or Me.DatabaseNameRS = "" Or Me.UIDRS = "" Or Me.PassRS = "" Then
            Return ""
        Else
            Conn = "Data Source=" & Me.ServerNameRS & ";Initial Catalog=" & Me.DatabaseNameRS & ";Persist Security Info=True;User ID=" & Me.UIDRS & ";Password=" & Me.PassRS
            Return Conn
        End If
    End Function


    Protected Function ConnectionStringAM() As String    
        Return GetReferenceDataConnectionString()
    End Function
End Class
