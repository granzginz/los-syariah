﻿

Imports System.Xml
Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class ReadXMLFile
    Inherits WebBased

    Private oController As New LoginController
    Private oCustomClass As New Maxiloan.Parameter.Login
    Private dtWriteXML As New DataTable

#Region "Read XML"
    Public Function ReadDoc(ByVal doc As String) As String
        Dim xmlReader As New XmlTextReader(doc)
        Dim output As String = ""
        Try
            output = WriteXML(xmlReader)
        Catch e As Exception
            output = output & (" Occured While Reading " + doc + " " + e.ToString()) & vbCrLf
        Finally
            If Not IsDBNull(xmlReader) Then
                xmlReader.Close()
            End If
        End Try
        Return output
    End Function

    Private Function WriteXML(ByVal xmlReader As XmlTextReader) As String
        Dim output As String = ""
        While xmlReader.Read()
            If xmlReader.Name <> "xml" Then
                If xmlReader.NodeType = XmlNodeType.Element Then
                    If xmlReader.Name = "MsMenu" Then
                        output = output & "foldersTree = gFld(""<b>Menu</b>"", null);" & vbCrLf
                    ElseIf xmlReader.Name = "MsMenuItem" And xmlReader.GetAttribute(2) = "1" Then
                        output = output & "aux1 = insFld(foldersTree, gFld('" & xmlReader.GetAttribute(0) & "', null));" & vbCrLf
                    ElseIf xmlReader.GetAttribute(3) = "FM" Then
                        output = output & "insDoc(aux" & CInt(xmlReader.GetAttribute(2)) - 1 & ", gLnk(2, """ & xmlReader.GetAttribute(0) & """, """ & xmlReader.GetAttribute(1) & """));" & vbCrLf
                    ElseIf xmlReader.GetAttribute(3) = "CM" Then
                        output = output & "insDoc(aux" & CInt(xmlReader.GetAttribute(2)) - 1 & ", gLnk(2, """ & xmlReader.GetAttribute(0) & """, """ & xmlReader.GetAttribute(5) & """));" & vbCrLf
                    ElseIf xmlReader.GetAttribute(3) = "SM" Then
                        output = output & "aux" & xmlReader.GetAttribute(2) & " = insFld(aux" & CInt(xmlReader.GetAttribute(2)) - 1 & ", gFld(""" & xmlReader.GetAttribute(0) & """, null));" & vbCrLf
                    End If
                End If
            End If
        End While
        Return output
    End Function
#End Region

    Public Sub WriteXMLToFile(ByVal XMLFileName As String, ByVal LstrAppID As String)
        Dim fs As FileStream = New FileStream(XMLFileName, FileMode.CreateNew, FileAccess.Write)
        Dim w As StreamWriter = New StreamWriter(fs)  '  create a Char writer
        Dim LObjPrimary(2) As DataColumn

        Dim LstrString As String

        oCustomClass.LoginId = Me.Loginid
        oCustomClass.Applicationid = Me.AppId
        oCustomClass.GroupDBID = Me.GroubDbID
        dtWriteXML = oController.WriteFileXML(oCustomClass)

        LObjPrimary(0) = dtWriteXML.Columns("menuid")
        dtWriteXML.PrimaryKey = LObjPrimary

        LstrString = createMenuXml(LstrAppID)
        w.WriteLine(LstrString)
        w.Flush()
        w.Close()
    End Sub

    Private Function createMenuXml(ByVal applicationId As String) As String
        Dim LStrXml As String
        Dim LObjDataMenuFounds As DataRow()

        LObjDataMenuFounds = (dtWriteXML.Select(" parentmenuid='root' ", "order_"))
        LStrXml = "<?xml version='1.0'?>" & vbCrLf & _
                "<MsMenu>" & vbCrLf
        createMenuXmlItem(LStrXml, LObjDataMenuFounds, 0, LObjDataMenuFounds.Length - 1, applicationId)
        LStrXml = LStrXml & "</MsMenu>"
        createMenuXml = LStrXml
    End Function

    Private Sub createMenuXmlItem(ByRef xmlString As String, ByVal DataFounds As DataRow(), ByRef starting As Integer, ByVal ending As Integer, ByVal applicationId As String)
        Dim LObjDataMenuFounds As DataRow()        
        Dim LStrMenuID As String

        While starting <= ending
            LStrMenuID = CStr(DataFounds(starting)("menuid"))

            xmlString = xmlString & "<MsMenuItem title='" & CStr(DataFounds(starting)("prompt")) & _
                        "'  url='" & CStr(IIf((CStr(DataFounds(starting)("formfilename")) = ""), "am_construction.aspx", DataFounds(starting)("formfilename"))).Trim & _
                        "' no='" & CStr(DataFounds(starting)("level_")) & _
                        "' result='" & CStr(DataFounds(starting)("result")) & _
                        "'  menuid='" & LStrMenuID & "' command='" & CStr(DataFounds(starting)("Command")).Trim & _
                        "' >"
            LObjDataMenuFounds = (dtWriteXML.Select(" parentmenuid='" + LStrMenuID + "'"))
            If LObjDataMenuFounds.Length > 0 Then
                createMenuXmlItem(xmlString, LObjDataMenuFounds, 0, LObjDataMenuFounds.Length - 1, applicationId)
            End If
            xmlString = xmlString & "</MsMenuItem>" & vbCrLf
            starting = starting + 1
        End While
    End Sub
End Class
