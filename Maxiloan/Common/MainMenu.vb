﻿Imports System.Xml
Imports System.IO
Imports Maxiloan.Controller
Imports Maxiloan.Parameter

Public Class MainMenu
    Inherits WebBased

    Private oController As New LoginController
    Private oCustomClass As New Maxiloan.Parameter.Login
    Private dtWriteXML As New DataTable

    Public Sub WriteXMLToFile(ByVal XMLFileName As String, ByVal LstrAppID As String)
        Dim fs As FileStream = New FileStream(XMLFileName, FileMode.CreateNew, _
        FileAccess.Write)
        Dim w As StreamWriter = New StreamWriter(fs)
        Dim LObjPrimary(2) As DataColumn

        Dim LstrString As String

        oCustomClass.LoginId = Me.Loginid
        oCustomClass.Applicationid = Me.AppId
        oCustomClass.GroupDBID = Me.GroubDbID
        dtWriteXML = oController.WriteFileXML(oCustomClass)

        LObjPrimary(0) = dtWriteXML.Columns("menuid")
        dtWriteXML.PrimaryKey = LObjPrimary

        LstrString = createMenuXmlx(LstrAppID)
        w.WriteLine(LstrString)
        w.Flush()
        w.Close()
    End Sub

    Private Function createMenuXmlx(ByVal applicationId As String) As String
        Dim LStrXml As String
        Dim LObjDataMenuFounds As DataRow()

        LObjDataMenuFounds = (dtWriteXML.Select(" parentmenuid='root' ", "order_"))
        LStrXml = "<?xml version='1.0'?>" & vbCrLf & _
                "<menu>" & vbCrLf

        createMenuXmlItemx(LStrXml, LObjDataMenuFounds, 0, LObjDataMenuFounds.Length - 1, applicationId)
        LStrXml = LStrXml & "</menu>"

        createMenuXmlx = LStrXml
    End Function

    Private Sub createMenuXmlItemx(ByRef xmlString As String, ByVal DataFounds As DataRow(), _
        ByRef starting As Integer, ByVal ending As Integer, ByVal applicationId As String)
        Dim LObjDataMenuFounds As DataRow()
        Dim LStrMenuID As String

        While starting <= ending
            LStrMenuID = CStr(DataFounds(starting)("menuid"))

            'xmlString = xmlString & "<item id='" & CStr(DataFounds(starting)("menuid")) & "' text='" & CStr(DataFounds(starting)("prompt")) & "'>" & _
            '            CStr(IIf(CStr(DataFounds(starting)("result")) = "SM", "", _
            '            "<href target='content'><![CDATA[" & CStr(IIf((CStr(DataFounds(starting)("formfilename")) = ""), _
            '                                                          IIf(CStr(DataFounds(starting)("command")) = "-" Or CStr(DataFounds(starting)("command")) = "", _
            '                                                              "am_construction.aspx", _
            '                                                              Replace(CStr(DataFounds(starting)("command")).Trim, "&amp;", "&")), _
            '                                                          DataFounds(starting)("formfilename"))).Trim & "]]></href>"))



            Dim strContent As String = ""
            Dim strContentLink As String

            If DataFounds(starting)("result").ToString = "SM" Then
                strContent = ""
                strContentLink = ""
            ElseIf DataFounds(starting)("result").ToString = "CM" Then
                If DataFounds(starting)("command").ToString = "-" Or DataFounds(starting)("command").ToString = "" Then
                    strContent = "am_construction.aspx"
                Else
                    strContent = Replace(CStr(DataFounds(starting)("command")).Trim, "&amp;", "&")
                End If
                strContentLink = "<href target='content'><![CDATA[" & strContent & "]]></href>"
            ElseIf DataFounds(starting)("result").ToString = "FM" Then
                If DataFounds(starting)("formfilename").ToString = "-" Or DataFounds(starting)("formfilename").ToString = "" Then
                    strContent = "am_construction.aspx"
                Else
                    strContent = Replace(CStr(DataFounds(starting)("formfilename")).Trim, "&amp;", "&")
                End If
                strContentLink = "<href target='content'><![CDATA[" & strContent & "]]></href>"
            End If


            xmlString = xmlString & "<item id='" & CStr(DataFounds(starting)("menuid")) & "' text='" & CStr(DataFounds(starting)("prompt")) & "'>" & strContentLink




            'Dim xmlstr As New StringBuilder

            'xmlstr.Append(xmlString)



            LObjDataMenuFounds = (dtWriteXML.Select(" parentmenuid='" + LStrMenuID + "'"))

            If LObjDataMenuFounds.Length > 0 Then
                createMenuXmlItemx(xmlString, LObjDataMenuFounds, 0, LObjDataMenuFounds.Length - 1, applicationId)
            End If

            xmlString = xmlString & "</item>" & vbCrLf
            starting = starting + 1
        End While
    End Sub

    Public Function WriteXmlforUser(ByVal LstrAppID As String, ByVal UserID As String, ByVal XMLFileName As String) As String
        Dim fs As FileStream = New FileStream(XMLFileName, FileMode.CreateNew, FileAccess.Write)
        Dim fileWrite As StreamWriter = New StreamWriter(fs)
        Dim LstrString As String
        Dim LObjPrimary(2) As DataColumn


        oCustomClass.LoginId = Me.Loginid
        oCustomClass.Applicationid = Me.AppId
        oCustomClass.GroupDBID = Me.GroubDbID
        dtWriteXML = oController.WriteXMLForUser(oCustomClass)

        LObjPrimary(0) = dtWriteXML.Columns("menuid")
        dtWriteXML.PrimaryKey = LObjPrimary

        LstrString = createMenuXmlx(LstrAppID)
        fileWrite.WriteLine(LstrString)
        fileWrite.Flush()
        fileWrite.Close()
    End Function


End Class
