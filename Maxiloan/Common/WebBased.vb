﻿Imports System.Web
Imports System.Configuration
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

Public Class WebBased : Inherits System.Web.UI.Page

#Region "Property"
    Protected Property FullName() As String
        Get
            Return (CType(Session("FullName"), String))
        End Get
        Set(ByVal Value As String)
            Session("FullName") = Value
        End Set
    End Property

    Protected Property EmpPos() As String
        Get
            Return (CType(Session("EmpPos"), String))
        End Get
        Set(ByVal Value As String)
            Session("EmpPos") = Value
        End Set
    End Property

    Protected Property IsHoBranch() As Boolean
        Get
            Return (CType(Session("IsHoBranch"), Boolean))
        End Get
        Set(ByVal Value As Boolean)
            Session("IsHoBranch") = Value
        End Set
    End Property

    Protected Property GroubDbID() As String
        Get
            Return (CType(Session("groupdbid"), String))
        End Get
        Set(ByVal Value As String)
            Session("groupdbid") = Value
        End Set
    End Property

    Protected Property BusinessDate() As DateTime
        Get
            Return (CType(Session("sesBusinessDate"), DateTime))
        End Get
        Set(ByVal Value As DateTime)
            Session("sesBusinessDate") = Value
        End Set

    End Property

    Protected Property NextBusinessDate() As DateTime
        Get
            Return (CType(Session("sesNextBusinessDate"), DateTime))
        End Get
        Set(ByVal Value As DateTime)
            Session("sesNextBusinessDate") = Value
        End Set
    End Property

    Protected Property sesBranchId() As String
        Get
            Return (CType(Session("sesBranchID"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesBranchId") = Value
        End Set
    End Property

    Protected Property sesCompanyName() As String
        Get
            Return (CType(Session("sesCompanyName"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesCompanyName") = Value
        End Set
    End Property

    Protected Property SortBy() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property

    Protected Property Loginid() As String
        Get
            Return (CType(Session("LoginId"), String))
        End Get
        Set(ByVal Value As String)
            Session("LoginId") = Value
        End Set
    End Property

    Protected Property Password() As String
        Get
            Return (CType(Session("Password"), String))
        End Get
        Set(ByVal Value As String)
            Session("Password") = Value
        End Set
    End Property

    Protected Property AppId() As String
        Get
            Return (CType(Session("AppId"), String))
        End Get
        Set(ByVal Value As String)
            Session("AppId") = Value
        End Set
    End Property

    Protected Property ServerName() As String
        Get
            Return (CType(Session("ServerName"), String))
        End Get
        Set(ByVal Value As String)
            Session("ServerName") = Value
        End Set
    End Property

    Protected Property BaseCurrency() As String
        Get
            Return (CType(Session("BaseCurrency"), String))
        End Get
        Set(ByVal Value As String)
            Session("BaseCurrency") = Value
        End Set
    End Property

    Protected Property LoginNo() As String
        Get
            Return (CType(Session("LoginNo"), String))
        End Get
        Set(ByVal Value As String)
            Session("LoginNo") = Value
        End Set
    End Property

    Protected Property BranchName() As String
        Get
            Return (CType(Session("BranchName"), String))
        End Get
        Set(ByVal Value As String)
            Session("BranchName") = Value
        End Set
    End Property

    Protected Property DataBaseName() As String
        Get
            Return (CType(Session("DataBaseName"), String))
        End Get
        Set(ByVal Value As String)
            Session("DataBaseName") = Value
        End Set
    End Property

    Protected Property UserID() As String
        Get
            Return (CType(Session("UserID"), String))
        End Get
        Set(ByVal Value As String)
            Session("UserID") = Value
        End Set
    End Property

    Protected Property PassKey() As String
        Get
            Return (CType(Session("PassKey"), String))
        End Get
        Set(ByVal Value As String)
            Session("PassKey") = Value
        End Set
    End Property

    Protected Property SearchBy() As String
        Get
            Return (CType(ViewState("SearchBy"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SearchBy") = Value
        End Set
    End Property

    Protected Property FormID() As String
        Get
            Return CType(ViewState("FormID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("FormID") = Value
        End Set
    End Property

    Protected Property BranchID() As String
        Get
            Return CType(ViewState("BranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("BranchID") = Value
        End Set
    End Property

    Protected Property SesCompanyID() As String
        Get
            Return CType(Session("SesCompanyID"), String)
        End Get
        Set(ByVal Value As String)
            Session("SesCompanyID") = Value
        End Set
    End Property

    Protected Property ServerNameRS() As String
        Get
            Return (CType(Session("sesServerNameRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesServerNameRS") = Value
        End Set
    End Property

    Protected Property DatabaseNameRS() As String
        Get
            Return (CType(Session("sesDatabaseNameRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesDatabaseNameRS") = Value
        End Set
    End Property

    Protected Property VirtualDirectoryRS() As String
        Get
            Return (CType(Session("sesVirtualDirectoryRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesVirtualDirectoryRS") = Value
        End Set
    End Property

    Protected Property ReportManagerRS() As String
        Get
            Return (CType(Session("sesReportManagerRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesReportManagerRS") = Value
        End Set
    End Property

    Protected Property BranchCity() As String
        Get
            Return (CType(Session("sesBranchCity"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesBranchCity") = Value
        End Set
    End Property

    Protected Property UIDRS() As String
        Get
            Return (CType(Session("sesUIDRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesUIDRS") = Value
        End Set
    End Property

    Protected Property PassRS() As String
        Get
            Return (CType(Session("sesPassRS"), String))
        End Get
        Set(ByVal Value As String)
            Session("sesPassRS") = Value
        End Set
    End Property

    Protected Property AppMgrDB() As String
        Get
            Return (CType(Session("AppMgrDB"), String))
        End Get
        Set(ByVal Value As String)
            Session("AppMgrDB") = Value
        End Set
    End Property

    Protected Property PoolSize() As String
        Get
            Return (CType(Session("PoolSize"), String))
        End Get
        Set(ByVal Value As String)
            Session("PoolSize") = Value
        End Set
    End Property
#End Region

    Protected Function GetConnectionString() As String
        Dim Conn As String
        If Me.ServerName = "" Or Me.DataBaseName = "" Or Me.UserID = "" Or Me.PassKey = "" Then
            Return ""
        Else
            Conn = "Server=" & Me.ServerName & ";Database=" & Me.DataBaseName & ";UID=" & Me.UserID & ";Pwd=" & Me.PassKey & ";Max Pool Size=" & Me.PoolSize & ";Pooling=true;Connection Lifetime=120"
            Return Conn
        End If
    End Function

    Protected Function GetConnectionStringRS() As String
        Dim Conn As String
        If Me.ServerNameRS = "" Or Me.DatabaseNameRS = "" Or Me.UIDRS = "" Or Me.PassRS = "" Then
            Return ""
        Else
            Conn = "Data Source=" & Me.ServerNameRS & ";Initial Catalog=" & Me.DatabaseNameRS & ";Persist Security Info=True;User ID=" & Me.UIDRS & ";Password=" & Me.PassRS
            Return Conn
        End If
    End Function

    Protected Shared Function ConvertDate(ByVal pStrValue As String) As String
        Dim lStrValue As String = Trim(pStrValue)
        Dim lArrValue As Array

        lArrValue = CType(lStrValue.Split(CChar("/")), Array)

        If UBound(lArrValue) = 2 Then
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(0).ToString), 2), 0)
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(1).ToString), 2), 1)
            If Len(lArrValue.GetValue(2)) = 2 Then
                lArrValue.SetValue(Left(CStr(Year(Now())), 2) + lArrValue.GetValue(2).ToString, 2)
            End If
            lStrValue = lArrValue.GetValue(2).ToString & lArrValue.GetValue(1).ToString & lArrValue.GetValue(0).ToString
        End If
        Return lStrValue
    End Function

    Protected Shared Function ConvertDate2(ByVal pStrValue As String) As Date
        Dim lStrValue As String = Trim(CStr(pStrValue))
        Dim lArrValue As Array
        lArrValue = CType(lStrValue.Split(CChar("/")), Array)
        If UBound(lArrValue) = 2 Then
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(0).ToString), 2), 0)
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(1).ToString), 2), 1)
            If Len(lArrValue.GetValue(2)) = 2 Then
                lArrValue.SetValue(Left(CStr(Year(Now())), 2) + lArrValue.GetValue(2).ToString, 2)
            End If
            lStrValue = lArrValue.GetValue(1).ToString & "/" & lArrValue.GetValue(0).ToString & "/" & lArrValue.GetValue(2).ToString
        End If
        Return CDate(lStrValue)
    End Function

    Protected Shared Function ConvertDateVB(ByVal pStrValue As String) As String

        Dim lStrValue As String = Trim(pStrValue)
        Dim lArrValue As Array
        lArrValue = CType(lStrValue.Split(CChar("/")), Array)
        If UBound(lArrValue) = 2 Then
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(0).ToString), 2), 0)
            lArrValue.SetValue(Right(CStr("0" + lArrValue.GetValue(1).ToString), 2), 1)
            If Len(lArrValue.GetValue(2)) = 2 Then
                lArrValue.SetValue(Left(CStr(Year(Now())), 2) + lArrValue.GetValue(2).ToString, 2)
            End If
            lStrValue = lArrValue.GetValue(1).ToString & "/" & lArrValue.GetValue(0).ToString & "/" & lArrValue.GetValue(2).ToString
        End If
        Return lStrValue
    End Function

    Protected Shared Function ConvertDateSql(ByVal pStrValue As String) As String
        Dim dtDate As Date
        Dim strYear As String
        Dim strMonth As String
        Dim strDay As String
        Dim strDate As New System.Text.StringBuilder
        dtDate = ConvertDate2(pStrValue)
        strMonth = "0" & dtDate.Month.ToString
        strYear = dtDate.Year.ToString
        strDay = "0" & dtDate.Day.ToString
        strDate.Append(strYear)
        strDate.Append(Right(strMonth, 2))
        strDate.Append(Right(strDay, 2))
        strDate.ToString()
        Return strDate.ToString
    End Function

    Protected Function SessionInvalid() As Boolean
        Dim ceksession As Boolean
        ceksession = False
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        If Me.Loginid = "" Or Me.Password = "" Or Me.AppId = "" Then
            ceksession = True
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            'Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/am_sessionend.aspx")
            Response.Redirect("~/am_sessionend.aspx")
        End If
        Return CBool(ceksession)
    End Function

    Protected Function IsFeatureAvailable(ByVal strLoginID As String, ByVal strFormID As String,
                                      ByVal strFeatureId As String, ByVal strApplicationID As String) As Boolean
        Dim oCustomClass As New Maxiloan.Parameter.Common
        Dim oController As New AuthorizationController
        Dim isAuthorize As Boolean

        With oCustomClass
            .strConnection = GetConnectionString()
            .LoginId = strLoginID
            .FeatureID = strFeatureId
            .FormID = strFormID
            .AppID = strApplicationID
        End With

        isAuthorize = oController.CheckFeature(oCustomClass)

        Return isAuthorize
    End Function

    Protected Function CheckForm(ByVal strLoginID As String, ByVal strFormId As String, ByVal strApplicationID As String) As Boolean
        Dim oCustomClass As New Maxiloan.Parameter.Common
        Dim oController As New AuthorizationController
        Dim isAuthorize As Boolean
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String

        With oCustomClass
            .strConnection = GetConnectionString()
            .LoginId = strLoginID
            .FormID = strFormId
            .AppID = strApplicationID
        End With

        isAuthorize = oController.CheckForm(oCustomClass)

        If Not isAuthorize Then
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            Return False
        Else
            Return True
        End If
    End Function

    Protected Function CheckFeature(ByVal strLoginID As String, ByVal strFormID As String, ByVal strFeatureId As String, ByVal strApplicationID As String) As Boolean
        Dim oCustomClass As New Maxiloan.Parameter.Common
        Dim oController As New AuthorizationController
        Dim isAuthorize As Boolean
        With oCustomClass
            .strConnection = GetConnectionString()
            .LoginId = strLoginID
            .FeatureID = strFeatureId
            .FormID = strFormID
            .AppID = strApplicationID
        End With
        isAuthorize = oController.CheckFeature(oCustomClass)
        Dim strHTTPServer As String
        Dim strNameServer As String
        Dim StrHTTPApp As String
        If Not isAuthorize Then
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strNameServer = Request.ServerVariables("SERVER_NAME")
            Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            Return False
        Else
            Return True
        End If
    End Function

    Protected Function IsSingleBranch() As Boolean
        Dim strBranch() As String
        strBranch = Split(Me.sesBranchId, ",")
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        If UBound(strBranch) > 0 Then
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            strNameServer = Request.ServerVariables("SERVER_NAME")
            Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")
            Return False
        Else
            Return True
        End If
    End Function

    Public Sub KickOutForMultipleBranch()

        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
        Response.Redirect("http://" & strNameServer & "/" & StrHTTPApp & "/error_notauthorized.aspx")

    End Sub

    Public Function GetCompanyAddress(ByVal strCompanyID As String) As String
        Dim oController As New LoginController
        Return oController.GetCompanyAddress(GetConnectionString, strCompanyID)
    End Function

#Region "Approval"
    Public Function Get_UserApproval(ByVal SchemeID As String,
                                     ByVal BranchID As String,
                                     Optional ByVal Amount As Double = 0) As DataTable
        Dim oController As New ApprovalController
        Dim oCustomClass As New Maxiloan.Parameter.Approval
        With oCustomClass
            .strConnection = GetConnectionString()
            .SchemeID = SchemeID
            .BranchId = BranchID
            .AmountApproval = Amount
        End With
        Return oController.GetUserApproval(oCustomClass)
    End Function
#End Region

#Region "Message"
    Public Sub ShowMessage(ByVal lblMessage As System.Web.UI.WebControls.Label, ByVal strMessage As String, ByVal IsError As Boolean)
        'lblMessage.Visible = True
        'lblMessage.Text = strMessage
        'lblMessage.Attributes.Remove("class")
        'Dim div As New HtmlGenericControl("div")

        'Dim Container As New HtmlGenericControl("div")
        'Dim Span As New HtmlGenericControl("span")

        'Span.Attributes.Add("innerHtml", strMessage)

        'Container.Controls.Add(Span)
        'Container.Attributes.Add("style", "visibility: visible;")
        'lblMessage.Controls.Add(Container)

        'If IsError Then
        '    lblMessage.Attributes.Add("Data-layout", "topRight")
        '    lblMessage.Attributes.Add("Data-Type", "error")
        '    'lblMessage.Attributes.Add("class", "alert alert-warning alert-styled-left")
        'Else
        '    lblMessage.Attributes.Add("Data-layout", "topRight")
        '    lblMessage.Attributes.Add("Data-Type", "succes")
        '    'lblMessage.Attributes.Add("class", "alert alert-warning alert-styled-left")
        'End If

        Dim strHeader As String
        Dim strNode As String
        Dim strFooter As String
        '1. visible
        lblMessage.Visible = True

        '2. Create element

        'Dim ErrorBtnTrig As New Button
        ''Error
        ''<button type = "button" Class="btn btn-danger btn-sm" id="pnotify-solid-danger">Launch <i Class="icon-play3 position-right"></i></button>
        'ErrorBtnTrig.Attributes.Add("style", "visibility:hidden;")
        'ErrorBtnTrig.Attributes.Add("class", "btn btn-danger btn-sm")

        'Dim SuccessBtnTrig As New Button
        ''Success
        ''<button type = "button" Class="btn btn-success btn-sm" id="pnotify-solid-success">Launch <i Class="icon-play3 position-right"></i></button>
        'ErrorBtnTrig.Attributes.Add("style", "visibility:hidden;")
        'ErrorBtnTrig.Attributes.Add("class", "btn btn-success btn-sm")

        Dim control As Control = GetControlThatCausedPostBack(Page)



        If IsError Then
            strHeader = "<script>" & vbCrLf
            strNode = "" &
            "setTimeout(function() {" &
            " var notice = new PNotify({ " &
            "title: 'Danger notice', " &
            "text: '" & strMessage & "', " &
            "addclass: 'bg-danger', " &
            "hide :  false, " &
            "buttons: { " &
            "    closer: false, " &
            "    sticker: false " &
            "} " &
            "});" &
            "notice.get().click(function() { " &
            "notice.remove(); " &
            "});  " &
                    "}, 500);"
            strFooter = vbCrLf & "</script>"


            If strMessage.Length > 300 Then

                strMessage = "Page error, contact your administrator!"

                strNode = "" &
            "setTimeout(function() {" &
            " var notice = new PNotify({ " &
            "title: 'Danger notice', " &
            "text: '" & strMessage & "', " &
            "addclass: 'bg-danger', " &
            "hide :  false, " &
            "buttons: { " &
            "    closer: false, " &
            "    sticker: false " &
            "} " &
            "});" &
            "notice.get().click(function() { " &
            "notice.remove(); " &
            "});  " &
            " console.log(" + strMessage + ") " &
                    "}, 500);"
            End If


            ' Define the name and type of the client scripts on the page. 
            Dim csname1 As [String] = "PopupScript"
            Dim cstype As Type = Me.[GetType]()
            'Dim cstype2 As Type = UpdatePanel

            ' Get a ClientScriptManager reference from the Page class. 
            Dim cs As ClientScriptManager = Page.ClientScript
            'Dim aa As ScriptManager = Page.



            If ScriptManager.GetCurrent(CType(HttpContext.Current.Handler, Page)) Is Nothing Then


                ' Check to see if the startup script is already registered. 
                If Not cs.IsClientScriptBlockRegistered(cstype, csname1) Then
                    Dim cstext1 As New StringBuilder()
                    cstext1.Append(strHeader & strNode & strFooter)

                    cs.RegisterClientScriptBlock(cstype, csname1, cstext1.ToString())
                    'cs.RegisterClientScriptInclude(cstype, csname1, cstext1.ToString())
                End If


            Else
                ScriptManager.ScriptResourceMapping.Clear()

                Dim cstext1 As New StringBuilder()
                cstext1.Append(strHeader & strNode & strFooter)
                ScriptManager.RegisterClientScriptBlock(Me.Page, cstype, csname1, cstext1.ToString(), False)


            End If
            'lblMessage.Controls.Add(ErrorBtnTrig)
        Else
            strHeader = "<script>" & vbCrLf
            strNode = "" &
            "setTimeout(function() {" &
            " var notice = new PNotify({ " &
            "title: 'Success notice', " &
            "text: '" & strMessage & "', " &
            "addclass: 'bg-success', " &
            "hide :  false, " &
            "buttons: { " &
            "    closer: false, " &
            "    sticker: false " &
            "} " &
            "});" &
            "notice.get().click(function() { " &
            "notice.remove(); " &
            "});  " &
                    "}, 500);"
            strFooter = vbCrLf & "</script>"

            ' Define the name and type of the client scripts on the page. 
            Dim csname1 As [String] = "PopupScript"
            Dim cstype As Type = Me.[GetType]()

            ' Get a ClientScriptManager reference from the Page class. 
            Dim cs As ClientScriptManager = Page.ClientScript
            'Dim sc As ScriptManager

            If ScriptManager.GetCurrent(CType(HttpContext.Current.Handler, Page)) Is Nothing Then


                ' Check to see if the startup script is already registered. 
                If Not cs.IsClientScriptBlockRegistered(cstype, csname1) Then
                    Dim cstext1 As New StringBuilder()
                    cstext1.Append(strHeader & strNode & strFooter)

                    cs.RegisterClientScriptBlock(cstype, csname1, cstext1.ToString())
                    'cs.RegisterClientScriptInclude(cstype, csname1, cstext1.ToString())
                End If


            Else

                ScriptManager.ScriptResourceMapping.Clear()

                Dim cstext1 As New StringBuilder()
                cstext1.Append(strHeader & strNode & strFooter)
                ScriptManager.RegisterClientScriptBlock(Me.Page, cstype, csname1, cstext1.ToString(), False)


            End If
        End If

        '3.add event



        'lblMessage.
        'Disable handle if hardcoded already !!
        lblMessage.Text = ""
        'lblMessage.Attributes.Remove("class")

    End Sub

    Function GetControlThatCausedPostBack(ByVal page As Page) As Control

        'initialize a control And set it to null
        Dim ctrl As Control = Nothing

        'get the event target name And find the control
        Dim ctrlName As String = page.Request.Params.Get("__EVENTTARGET")
        If (Not String.IsNullOrEmpty(ctrlName)) Then
            ctrl = page.FindControl(ctrlName)
        End If
        'return the control to the calling method
        Return ctrl
    End Function

    Public Sub ShowMessage2(ByVal lblMessage As System.Web.UI.WebControls.Label, ByVal strMessage As String, ByVal IsError As Boolean)
        lblMessage.Visible = True
        lblMessage.Text = strMessage
        lblMessage.Attributes.Remove("class")
        If IsError Then
            lblMessage.Attributes.Add("class", "alert alert-warning alert-styled-left")
        Else
            lblMessage.Attributes.Add("class", "alert alert-warning alert-styled-left")
        End If
    End Sub

    Public Function cNum(ByVal str As String) As String
        Return IIf(IsNumeric(str), str, "0")
    End Function
#End Region
End Class
