
Imports Maxiloan.Controller

Public Class AccMntWebBased : Inherits maxiloan.webform.WebBased
#Region "Property"

#Region "Agreement"
    Protected Property ApplicationID() As String
        Get
            Return CType(Viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Protected Property AgreementNo() As String
        Get
            Return CType(Viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Protected Property BranchAgreement() As String
        Get
            Return CType(Viewstate("BranchAgreement"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("BranchAgreement") = Value
        End Set
    End Property

#End Region

#Region "Customer"
    Protected Property ReceivedFrom() As Double
        Get
            Return CType(viewstate("ReceivedFrom"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("ReceivedFrom") = Value
        End Set
    End Property

    Protected Property CustomerType() As String
        Get
            Return CType(Viewstate("CustomerType"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("CustomerType") = Value
        End Set
    End Property

    Protected Property CustomerID() As String
        Get
            Return CType(Viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("CustomerID") = Value
        End Set
    End Property

    Protected Property CustomerName() As String
        Get
            Return CType(Viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            Viewstate("CustomerName") = Value
        End Set
    End Property
#End Region

    Protected Property BankAccount() As String
        Get
            Return CType(Viewstate("BankAccount"), String)

        End Get
        Set(ByVal Value As String)
            viewstate("BankAccount") = Value
        End Set
    End Property

    Protected Property NextInstallmentDate() As Date
        Get
            Return CType(Viewstate("NextInstallmentDate"), Date)
        End Get
        Set(ByVal Value As Date)
            Viewstate("NextInstallmentDate") = Value
        End Set
    End Property

    Protected Property TotalInstallmentAmount() As Double
        Get
            Return CType(viewstate("TotalInstallmentAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalInstallmentAmount") = Value
        End Set
    End Property

    Protected Property WayOfPayment() As String
        Get
            Return CType(viewstate("WayOfPayment"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("WayOfPayment") = Value
        End Set
    End Property

    Protected Property AmountToBePaid() As Double
        Get
            Return CType(viewstate("AmountToBePaid"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("AmountToBePaid") = Value
        End Set
    End Property

    Protected Property ValueDate() As Date
        Get
            Return CType(Viewstate("ValueDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("ValueDate") = Value
        End Set
    End Property

#Region "Payment Info"
    Protected Property InstallmentDue() As Double
        Get
            Return CType(Viewstate("InstallmentDue"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InstallmentDue") = Value
        End Set
    End Property

    Protected Property InsuranceDue() As Double
        Get
            Return CType(Viewstate("InsuranceDue"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InsuranceDue") = Value
        End Set
    End Property

    Protected Property InstallLC() As Double
        Get
            Return CType(Viewstate("InstallLC"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InstallLC") = Value
        End Set
    End Property

    Protected Property InsuranceLC() As Double
        Get
            Return CType(Viewstate("InsuranceLC"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InsuranceLC") = Value
        End Set
    End Property

    Protected Property InstallCollFee() As Double
        Get
            Return CType(Viewstate("InstallCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InstallCollFee") = Value
        End Set
    End Property

    Protected Property InsuranceCollFee() As Double
        Get
            Return CType(Viewstate("InsuranceCollFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InsuranceCollFee") = Value
        End Set
    End Property

    Protected Property PDCBounceFee() As Double
        Get
            Return CType(Viewstate("PDCBounceFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("PDCBounceFee") = Value
        End Set
    End Property

    Protected Property STNKRenewalFee() As Double
        Get
            Return CType(Viewstate("STNKRenewalFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("STNKRenewalFee") = Value
        End Set
    End Property

    Protected Property InsuranceClaim() As Double
        Get
            Return CType(Viewstate("InsuranceClaim"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("InsuranceClaim") = Value
        End Set
    End Property

    Protected Property ReposessionFee() As Double
        Get
            Return CType(Viewstate("ReposessionFee"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("ReposessionFee") = Value
        End Set
    End Property

    Protected Property PrepaidBalance() As Double
        Get
            Return CType(Viewstate("PrepaidBalance"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("PrepaidBalance") = Value
        End Set
    End Property

    Protected Property TotalOSOverDue() As Double
        Get
            Return CType(Viewstate("TotalOSOverDue"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("TotalOSOverDue") = Value
        End Set
    End Property
#End Region

#End Region

    Protected Function CheckCashier(ByVal LoginID As String) As Boolean
        Dim lBlnOpen As Boolean
        Dim oController As New CheckCashierController

        If oController.CheckCashier(GetConnectionString, LoginID, Me.sesBranchId.Replace("'", ""), Me.BusinessDate) Then
            lBlnOpen = True
        Else
            lBlnOpen = False
        End If
        Return lBlnOpen
    End Function

    Protected Function IsMaxBacDated(ByVal valuedate As Date) As Boolean
        Dim oController As New InstallRcvController
        Return oController.IsMaxBackDate(GetConnectionString, valuedate, Me.BusinessDate)
    End Function

    Protected Function IsValidLastPayment(ByVal ApplicationID As String, ByVal valuedate As Date) As Boolean
        Dim oController As New InstallRcvController
        Return oController.IsValidLastPayment(GetConnectionString, ApplicationID, valuedate)
    End Function
End Class
