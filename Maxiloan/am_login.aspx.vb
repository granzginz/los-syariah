﻿Imports Maxiloan.Parameter
Imports System.Web.Security
Imports Maxiloan.Controller

Public Class am_login
    Inherits Maxiloan.Webform.WebBased
    Private oLoginController As New LoginController
    Private ocustomclass As New Parameter.Login
#Region "Property"
    Private Property PasswordSQL() As String
        Get
            Return (CType(Session("sqlpassword"), String))
        End Get
        Set(ByVal Value As String)
            Session("sqlpassword") = Value
        End Set
    End Property
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lDteDate As Date
        Dim dtApplication As New DataTable

        lblMessage.Visible = False
        SlcGroupDBID.Visible = True
        'rfvGroupDB.Visible = True
        pnlgroupdb.Visible = False

        LogoutClear()

        'Try
        If Not IsPostBack Then
            Dim oCustomClass As New Maxiloan.Parameter.Login

            'Dim oDongle As New RajawaliSecureDongle

            'If oDongle.isExist Then                
            '    If oDongle.openDongle Then                    
            '        If oDongle.readUID("&H61736961") Then
            '            ShowMessage(lblMessage, "Dongle matched!", False)                       
            '        Else
            '            ShowMessage(lblMessage, "Wrong dongle!", True)
            '            Exit Sub
            '        End If
            '    Else
            '        ShowMessage(lblMessage, "Cannot open dongle!", True)
            '        Exit Sub
            '    End If
            'Else
            '    ShowMessage(lblMessage, "No dongle found!", True)
            '    Exit Sub
            'End If

            'TOKEN Failed!
            'If Now.Date > CDate("08/30/2015") Then
            '    'Me.BusinessDate <> CDate("03/30/2015") Then
            '    lblMessage.Text = "Trial expired!" & "<br/>"
            '    lblMessage.Visible = True
            '    Exit Sub
            'End If

            oCustomClass = oLoginController.GetLoginData
            dtApplication = CType(Me.Cache.Item("APPLICATIONLOGIN"), DataTable)

            If dtApplication Is Nothing Then
                Dim dtApplicationCache As New DataTable
                dtApplicationCache = oCustomClass.LoginApplication
                Me.Cache.Insert("APPLICATIONLOGIN", dtApplicationCache, Nothing, DateTime.Now.AddHours(10), TimeSpan.Zero)
                dtApplication = CType(Me.Cache.Item("APPLICATIONLOGIN"), DataTable)
            End If

            cboApplication.DataSource = dtApplication.DefaultView
            cboApplication.DataTextField = "applicationname"
            cboApplication.DataValueField = "applicationid"
            cboApplication.DataBind()
            'cboApplication.Items.Insert(0, "Select Application")
            'cboApplication.Items(0).Value = ""
            cboApplication.SelectedIndex = 0

            lDteDate = Now()
            lblDate.Text = WeekdayName(lDteDate.DayOfWeek + 1) & ", " & Format(lDteDate, "dd MMMM yyyy")
        End If
        'Catch ex As Exception
        '    lblMessage.Text &= "Sub Page Load :" & ex.Message & "<br/>"
        '    lblMessage.Visible = True
        'End Try       
    End Sub

    Protected Sub Set_Session(ByVal pStrApplicationID As String, _
                            ByVal pStrGroupDBID As String, _
                            ByVal pStrLoginID As String)
        Dim lDteNow As DateTime = Now
        Try
            Dim oCustomClass As New Maxiloan.Parameter.Login
            oCustomClass.LoginId = pStrLoginID.Trim
            oCustomClass.Applicationid = pStrApplicationID.Trim
            oCustomClass.GroupDBID = pStrGroupDBID.Trim
            oCustomClass = oLoginController.SetSession(oCustomClass)

            Me.UserID = pStrLoginID.Trim
            Me.LoginNo = Me.UserID & Format(lDteNow, "yyyy") & Format(lDteNow, "MM") & Format(lDteNow, "dd") & Format(lDteNow, "hh") & Format(lDteNow, "mm") & Format(lDteNow, "ss")

            With oCustomClass
                Me.ServerName = .ServerName
                Me.DataBaseName = .DBName
                Me.FullName = .FullName
                Me.EmpPos = .EmpPos
                Me.PassKey = .Password & .PassKeyPrefix
                Me.PasswordSQL = Me.PassKey
                Me.SesCompanyID = .CompanyID
                Me.sesCompanyName = .CompanyName
                Me.ServerNameRS = .ServerNameRS
                Me.DatabaseNameRS = .DatabaseNameRS
                Me.VirtualDirectoryRS = .VirtualDirectoryRS
                Me.ReportManagerRS = .ReportManagerRS
                Me.UIDRS = .UIDRS
                Me.PassRS = .PassRS
                Me.AppMgrDB = .ApplicationManagerDB
                Me.BranchCity = .BranchCity
            End With

        Catch exp As Exception
            lblMessage.Text = exp.Message
            ShowMessage(lblMessage, lblMessage.Text, True)
            'lblMessage.Visible = True
        End Try
    End Sub

    Private Sub btnLogin1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin1.Click
        Dim objCom As New am_com_001
        Dim LNumValid As Int16 = 0
        Dim lBlnValid As Boolean
        Dim oCustomClass As New Maxiloan.Parameter.Login
        Dim bolIsPasswordExpired As Boolean = False
        Dim intCountFailed As Int16
        Dim intMaxPwdFailed As Integer
        lblMessage.Text = ""
        lblMessage.Visible = False

        If cboApplication.SelectedValue.Trim <> "AM" And txtUsrNm.Text.Trim.ToLower = "admin" Then
            lblMessage.Text = "Admin hanya boleh login ke application manager!"
            ShowMessage(lblMessage, lblMessage.Text, True)
            'lblMessage.Visible = True
            Exit Sub
        End If

        Try
            Me.AppId = cboApplication.SelectedItem.Value
            Me.Loginid = Trim(txtUsrNm.Text)
            Me.Password = Trim(txtPwd.Text)
            Me.PasswordSQL = ""

            LNumValid = objCom.getUser(txtUsrNm.Text.Trim, txtPwd.Text, cboApplication.SelectedItem.Value)

            Select Case LNumValid
                Case -3
                    lblMessage.Text = "This User is Not Active! Contact Your Administrator ."
                    ShowMessage(lblMessage, lblMessage.Text, True)
                    'lblMessage.Visible = True
                    lBlnValid = False
                Case -1
                    lblMessage.Text = "Invalid Password."
                    ShowMessage(lblMessage, lblMessage.Text, True)
                    'lblMessage.Visible = True
                    lBlnValid = False
                    With oCustomClass
                        .LoginId = Me.Loginid
                        .IsReset = False
                    End With
                    intCountFailed = oLoginController.CountWrongPwd(oCustomClass)
                    oCustomClass = oLoginController.PasswordSetting
                    intMaxPwdFailed = oCustomClass.PwdFailed

                    If (intMaxPwdFailed - intCountFailed) = 1 Then
                        lblMessage.Text = "Invalid Password.  Warning ! You Will Be Lock  if  Failed This Time!"
                        ShowMessage(lblMessage, lblMessage.Text, True)
                        'lblMessage.Visible = True
                    ElseIf (intMaxPwdFailed - intCountFailed) = 0 Then
                        lblMessage.Text = "Invalid Password.  You Have Been Locked! Contact Your Administrator!"
                        ShowMessage(lblMessage, lblMessage.Text, True)
                        'lblMessage.Visible = True
                    End If

                Case -2
                    lblMessage.Text = "Invalid User Name."
                    ShowMessage(lblMessage, lblMessage.Text, True)
                    'lblMessage.Visible = True
                    lBlnValid = False
                Case 1
                    With oCustomClass
                        .LoginId = Me.Loginid
                        .IsReset = True
                    End With
                    intCountFailed = oLoginController.CountWrongPwd(oCustomClass)

                    With oCustomClass
                        .LoginId = Me.Loginid
                        .Applicationid = Me.AppId
                    End With
                    oCustomClass = oLoginController.CheckMultiBranch(oCustomClass)
                    Me.GroubDbID = oCustomClass.BranchId
                    If oCustomClass.IsMultiBranch Then
                        pnlgroupdb.Visible = True
                        pnllogininfo.Visible = False
                        SlcGroupDBID.DataSource = oCustomClass.ListMultiBranch
                        SlcGroupDBID.DataTextField = "groupdbname"
                        SlcGroupDBID.DataValueField = "groupdbid"
                        SlcGroupDBID.DataBind()
                    Else
                        Set_Session(Me.AppId, Me.GroubDbID, Me.Loginid)
                        If cboApplication.SelectedItem.Value.Trim.ToUpper = "MAXILOAN" Then
                            oCustomClass.strConnection = GetConnectionString()
                            oCustomClass.BranchId = Me.GroubDbID
                            oCustomClass = oLoginController.CekBranch(oCustomClass)
                            Me.BranchName = oCustomClass.BranchName
                            Me.sesBranchId = oCustomClass.BranchId
                            If Me.sesBranchId = "" Then Me.sesBranchId = "0"
                            Me.BusinessDate = oCustomClass.BusinessDate
                            Me.IsHoBranch = oCustomClass.IsHoBranch
                            If oCustomClass.IsClosed Then
                                lblMessage.Text = "Branch has been closed."
                                ShowMessage(lblMessage, lblMessage.Text, True)
                                'lblMessage.Visible = True
                                lBlnValid = False
                            ElseIf oCustomClass.IsEod Then
                                lblMessage.Text = "Day is not opened yet."
                                ShowMessage(lblMessage, lblMessage.Text, True)
                                'lblMessage.Visible = True
                                lBlnValid = False
                            Else
                                lBlnValid = True
                            End If
                        Else
                            lBlnValid = True
                        End If
                        If lBlnValid Then
                            FormsAuthentication.RedirectFromLoginPage(txtUsrNm.Text, False)
                            Response.Redirect("am_main.aspx")
                        End If
                    End If

                Case 2
                    With oCustomClass
                        .LoginId = Me.Loginid
                        .IsReset = True
                    End With
                    intCountFailed = oLoginController.CountWrongPwd(oCustomClass)

                    With oCustomClass
                        .Applicationid = Me.AppId
                        .LoginId = Me.Loginid
                    End With
                    oCustomClass = oLoginController.CheckMultiBranch(oCustomClass)
                    Me.GroubDbID = oCustomClass.BranchId
                    If oCustomClass.IsMultiBranch Then
                        pnlgroupdb.Visible = True
                        pnllogininfo.Visible = False
                        SlcGroupDBID.DataSource = oCustomClass.ListMultiBranch
                        SlcGroupDBID.DataTextField = "groupdbname"
                        SlcGroupDBID.DataValueField = "groupdbid"
                        SlcGroupDBID.DataBind()
                    Else
                        Set_Session(Me.AppId, Me.GroubDbID, Me.Loginid)
                        If cboApplication.SelectedItem.Value.Trim.ToUpper = "MAXILOAN" Then
                            oCustomClass.strConnection = GetConnectionString()
                            oCustomClass.BranchId = Me.GroubDbID
                            oCustomClass = oLoginController.CekBranch(oCustomClass)
                            Me.sesBranchId = oCustomClass.BranchId
                            If Me.sesBranchId = "" Then Me.sesBranchId = "0"
                            Me.BusinessDate = oCustomClass.BusinessDate
                            Me.BranchName = oCustomClass.BranchName
                            Me.IsHoBranch = oCustomClass.IsHoBranch

                            Try
                                If oCustomClass.IsClosed Then
                                    lblMessage.Text = "Branch has been closed."
                                    ShowMessage(lblMessage, lblMessage.Text, True)
                                    'lblMessage.Visible = True
                                    lBlnValid = False
                                ElseIf oCustomClass.IsEod Then
                                    lblMessage.Text = "Day is not opened yet."
                                    ShowMessage(lblMessage, lblMessage.Text, True)
                                    'lblMessage.Visible = True
                                    lBlnValid = False
                                Else
                                    lBlnValid = True
                                End If
                            Catch exp As Exception
                                lblMessage.Text = "Branch Not Exists"
                                ShowMessage(lblMessage, lblMessage.Text, True)
                                'lblMessage.Visible = True
                                Exit Sub
                            End Try

                        Else
                            lBlnValid = True
                        End If
                    End If
                Case Else
                    lblMessage.Text = "Access Denied."
                    'lblMessage.Visible = True
                    ShowMessage(lblMessage, lblMessage.Text, True)
                    lBlnValid = False
            End Select
        Catch exp As Exception
            lblMessage.Text &= exp.Message
            ShowMessage(lblMessage, lblMessage.Text, True)
            'lblMessage.Visible = True
        End Try

        If lBlnValid Then
            With oCustomClass
                .LoginId = txtUsrNm.Text.Trim
                .BusinessDate = Me.BusinessDate
            End With

            Try
                bolIsPasswordExpired = oLoginController.IsPasswordExpired(oCustomClass)
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try


            If bolIsPasswordExpired = True Then
                Session("Expired") = "T"
                Response.Redirect("Webform.AppMgt/Users/am_user_002.aspx")
            Else
                FormsAuthentication.RedirectFromLoginPage(txtUsrNm.Text, False)
                Response.Redirect("am_main.aspx")
            End If
        End If
    End Sub

    Private Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        lblMessage.Text = ""
        lblMessage.Visible = False

        Dim LNumValid As Int16 = 0
        Dim lBlnValid As Boolean
        Dim oCustomClass As New Maxiloan.Parameter.Login
        Dim bolIsPasswordExpired As Boolean = False
        Me.GroubDbID = SlcGroupDBID.SelectedValue.Trim

        Me.BranchName = SlcGroupDBID.SelectedItem.Text.Trim
        Set_Session(Me.AppId, Me.GroubDbID, Me.Loginid)

        If cboApplication.SelectedItem.Value.Trim.ToUpper = "MAXILOAN" Then
            oCustomClass.strConnection = GetConnectionString()
            oCustomClass.BranchId = SlcGroupDBID.SelectedValue.Trim
            oCustomClass = oLoginController.CekBranch(oCustomClass)
            Me.sesBranchId = oCustomClass.BranchId
            If Me.sesBranchId = "" Then Me.sesBranchId = "0"
            Me.BusinessDate = oCustomClass.BusinessDate
            Me.IsHoBranch = oCustomClass.IsHoBranch

            Try
                If oCustomClass.IsClosed Then
                    lblMessage.Text = "Branch has been closed."
                    ShowMessage(lblMessage, lblMessage.Text, True)
                    'lblMessage.Visible = True
                    lBlnValid = False
                ElseIf oCustomClass.IsEod Then
                    lblMessage.Text = "Day is not opened yet."
                    ShowMessage(lblMessage, lblMessage.Text, True)
                    'lblMessage.Visible = True
                    lBlnValid = False
                Else
                    lBlnValid = True
                End If
            Catch exp As Exception
                lblMessage.Text = "Branch Not Exists"
                ShowMessage(lblMessage, lblMessage.Text, True)
                'lblMessage.Visible = True
                Exit Sub
            End Try
        Else
            lBlnValid = True
        End If

        If lBlnValid Then
            With oCustomClass
                .LoginId = txtUsrNm.Text.Trim
                .BusinessDate = Me.BusinessDate
            End With

            Try
                bolIsPasswordExpired = oLoginController.IsPasswordExpired(oCustomClass)

                'TOKEN(Failed!)
                'If Me.BusinessDate > CDate("08/30/2015") Then
                '    lblMessage.Text = "Trial expired!" & "<br/>"
                '    lblMessage.Visible = True
                '    Exit Sub
                'End If

            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
            End Try


            If bolIsPasswordExpired = True Then
                Session("Expired") = "T"
                Response.Redirect("Webform.AppMgt/Users/am_user_002.aspx")
            Else
                Try
                    oCustomClass.BranchId = Me.sesBranchId.Replace("'", "")
                    oCustomClass.activityid = 0
                    oCustomClass.FormID = "LOGIN"
                    oLoginController.setloghistory(oCustomClass)
                Catch ex As Exception

                End Try
                FormsAuthentication.RedirectFromLoginPage(txtUsrNm.Text, False)
                Response.Redirect("am_main.aspx")
            End If


        End If
    End Sub

    Private Sub LogoutClear()
        Try
            ocustomclass.strConnection = Me.GetConnectionString
            ocustomclass.BranchId = Me.sesBranchId.Replace("'", "")
            ocustomclass.LoginId = Me.Loginid
            ocustomclass.activityid = 2
            ocustomclass.FormID = "LOGOUT"
            oLoginController.setloghistory(ocustomclass)
        Catch ex As Exception

        End Try

        Session("loginid") = ""
        Session("appid") = ""
        Session("DBID") = ""
        Session("password") = ""
        Session("sqlpassword") = ""
        Session("ServerName") = ""
        Session("DatabaseName") = ""
        Session("UserID") = ""
        Session("PassKey") = ""
        Session("fullname") = ""
        Session("sesBusinessDate") = ""
        Session("sesBranchID") = ""
        Session("BranchName") = ""
        Session("Expired") = ""
        Dim lstrstring As String = ""
        Dim strHTTPServer As String
        Dim StrHTTPApp As String
        Dim strNameServer As String
        strHTTPServer = Request.ServerVariables("PATH_INFO")
        strNameServer = Request.ServerVariables("SERVER_NAME")
        If strHTTPServer.IndexOf("/", 1) = -1 Then
            lstrstring = "<a href='am_login.aspx'>Login</a> atau <a href='javascript:fclose()'> Tutup </a>"
        Else
            StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            lstrstring = "<a href='http://" & strNameServer & "/" & StrHTTPApp & "/am_login.aspx'>Login</a> or <a href=""javascript:fclose()""> Tutup </a>"
        End If
        'outtext.InnerHtml = lstrstring
        FormsAuthentication.SignOut()
    End Sub
End Class