﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Webform.UserController
#End Region

Public Class SPPAViewer
    Inherits Maxiloan.Webform.WebBased
    Public Property SPPANo() As String
        Get
            Return (CType(viewstate("SPPANo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("SPPANo") = Value
        End Set
    End Property

    Public Property SPPACPNo() As String
        Get
            Return (CType(ViewState("SPPACPNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SPPACPNo") = Value
        End Set
    End Property
    Public Property SPPAJKNo() As String
        Get
            Return (CType(ViewState("SPPAJKNo"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("SPPAJKNo") = Value
        End Set
    End Property
    Public Property ProductAsuransi() As String
        Get
            Return (CType(ViewState("ProductAsuransi"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ProductAsuransi") = Value
        End Set
    End Property



#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If sessioninvalid() Then
            Exit Sub
        End If

        GetCookies()
        If ProductAsuransi.Trim = "KB" Then
            BindReport()
        Else
            BindReportCPJK()
        End If


    End Sub

#End Region
#Region "Constanta"
    Private Const SP_CREATE_SPPA_REPORT As String = "spCreateSPPAReport"
    Private Const THIS_REPORT As String = "GeneralSettingReport"
    Private Const FILE_NAME_REDIRECT As String = "SPPA.aspx"

#End Region
#Region "Get Cookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(General.CommonCookiesHelper.COOKIES_SPPA_REPORT)
        Me.SearchBy = cookie.Values("SearchBy")
        Me.SortBy = cookie.Values("SortBy")
        SPPANo = cookie.Values("SPPANO")
        SPPACPNo = cookie.Values("SPPACPNo")
        SPPAJKNo = cookie.Values("SPPAJKNo")
        ProductAsuransi = cookie.Values("ProductAsuransi")


    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        Dim oData As New DataSet
        Dim oData2 As New DataSet
        Dim m_controller As New GeneralPagingController
        Dim oCustomClass As New Parameter.GeneralPaging
        Dim oCustomClass2 As New Parameter.GeneralPaging
        Dim oReport As SPPAPrint =  New SPPAPrint
        Dim wherecound_ As String = Me.SearchBy

        Dim sppaCon As New SPPAController
        oData = sppaCon.GetSPPAReport(New Parameter.SPPA With {.strConnection = GetConnectionString(), .WhereCond = wherecound_, .SPPANo = SPPANo})

        oCustomClass2.SpName = "spInsCoByApplication"
        oCustomClass2.strConnection = GetConnectionString()
        oCustomClass2.WhereCond = String.Format(" and  dbo.insuranceAsset.SPPANo = '{0}'", SPPANo.Trim)
        oCustomClass2.SortBy = wherecound_

        oCustomClass2 = m_controller.GetReportWithParameterWhereCond(oCustomClass2)

        If Not oCustomClass2 Is Nothing Then
            oData2 = oCustomClass2.ListDataReport
        End If

        oReport.SetDataSource(oData)
        oReport.Subreports.Item("SPPADetailInsurance").SetDataSource(oData2)

        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "SPPAPrintViewer" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With
        If Request.QueryString("caller") <> "" Then
            Response.Redirect(Request.QueryString("caller") & "?file=" & Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf")
        Else
            Response.Redirect("SPPA.aspx?file=" & Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf")
        End If

    End Sub


    Sub BindReportCPJK()
        Dim oData As New DataSet
        Dim oData2 As New DataSet
        Dim m_controller As New GeneralPagingController
        Dim oCustomClass As New Parameter.GeneralPaging
        Dim oCustomClass2 As New Parameter.GeneralPaging
        Dim oReport As SPPAPrintCPJK = New SPPAPrintCPJK
        Dim wherecound_ As String = Me.SearchBy

        Dim sppaCon As New SPPAController

        If ProductAsuransi.Trim = "CP" Then
            oData = sppaCon.GetSPPAReportCP(New Parameter.SPPA With {.strConnection = GetConnectionString(), .WhereCond = wherecound_, .SPPANo = SPPACPNo})
        ElseIf ProductAsuransi.Trim = "JK" Then
            oData = sppaCon.GetSPPAReportJK(New Parameter.SPPA With {.strConnection = GetConnectionString(), .WhereCond = wherecound_, .SPPANo = SPPAJKNo})
        End If

        'oData = sppaCon.GetSPPAReport(New Parameter.SPPA With {.strConnection = GetConnectionString(), .WhereCond = wherecound_, .SPPANo = SPPANo})

        oCustomClass2.SpName = "spInsCoByApplication"
        oCustomClass2.strConnection = GetConnectionString()
        If ProductAsuransi.Trim = "CP" Then
            oCustomClass2.WhereCond = String.Format(" and  dbo.insuranceAsset.SPPACPNo = '{0}'", SPPACPNo.Trim)
        ElseIf ProductAsuransi.Trim = "JK" Then
            oCustomClass2.WhereCond = String.Format(" and  dbo.insuranceAsset.SPPAJKNo = '{0}'", SPPAJKNo.Trim)
        End If

        oCustomClass2.SortBy = wherecound_

        oCustomClass2 = m_controller.GetReportWithParameterWhereCond(oCustomClass2)

        If Not oCustomClass2 Is Nothing Then
            oData2 = oCustomClass2.ListDataReport
        End If

        oReport.SetDataSource(oData)
        'oReport.Subreports.Item("SPPADetailInsurance").SetDataSource(oData2)

        CrystalReportViewer1.ReportSource = oReport
        CrystalReportViewer1.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "SPPAPrintViewer" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With
        If Request.QueryString("caller") <> "" Then
            Response.Redirect(Request.QueryString("caller") & "?file=" & Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf")
        Else
            Response.Redirect("SPPA.aspx?file=" & Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf")
        End If

    End Sub

    'Sub BindReportJK()
    '    Dim oData As New DataSet
    '    Dim oData2 As New DataSet
    '    Dim m_controller As New GeneralPagingController
    '    Dim oCustomClass As New Parameter.GeneralPaging
    '    Dim oCustomClass2 As New Parameter.GeneralPaging
    '    Dim oReport As SPPAPrintCPJK = New SPPAPrintCPJK
    '    Dim wherecound_ As String = Me.SearchBy

    '    Dim sppaCon As New SPPAController
    '    oData = sppaCon.GetSPPAReport(New Parameter.SPPA With {.strConnection = GetConnectionString(), .WhereCond = wherecound_, .SPPANo = SPPAJKNo})

    '    oCustomClass2.SpName = "spInsCoByApplication"
    '    oCustomClass2.strConnection = GetConnectionString()
    '    oCustomClass2.WhereCond = String.Format(" and  dbo.insuranceAsset.SPPANo = '{0}'", SPPAJKNo.Trim)
    '    oCustomClass2.SortBy = wherecound_

    '    oCustomClass2 = m_controller.GetReportWithParameterWhereCond(oCustomClass2)

    '    If Not oCustomClass2 Is Nothing Then
    '        oData2 = oCustomClass2.ListDataReport
    '    End If

    '    oReport.SetDataSource(oData)
    '    oReport.Subreports.Item("SPPADetailInsurance").SetDataSource(oData2)

    '    CrystalReportViewer1.ReportSource = oReport
    '    CrystalReportViewer1.DataBind()

    '    With oReport.ExportOptions
    '        .ExportDestinationType = ExportDestinationType.DiskFile
    '        .ExportFormatType = ExportFormatType.PortableDocFormat
    '    End With

    '    Dim doctoprint As New System.Drawing.Printing.PrintDocument
    '    Dim i As Integer

    '    For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
    '        Dim rawKind As Integer
    '        If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "SPPAPrintViewer" Then
    '            rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
    '            oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
    '            Exit For
    '        End If
    '    Next

    '    Dim strFileLocation As String

    '    strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
    '    strFileLocation += Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf"

    '    Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
    '    New CrystalDecisions.Shared.DiskFileDestinationOptions

    '    DiskOpts.DiskFileName = strFileLocation

    '    With oReport
    '        .ExportOptions.DestinationOptions = DiskOpts
    '        .Export()
    '        .Close()
    '        .Dispose()
    '    End With
    '    If Request.QueryString("caller") <> "" Then
    '        Response.Redirect(Request.QueryString("caller") & "?file=" & Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf")
    '    Else
    '        Response.Redirect("SPPA.aspx?file=" & Me.Session.SessionID + Me.Loginid + "SPPAPrintViewer.pdf")
    '    End If

    'End Sub

#End Region


#Region "Back "
    Private Sub imbBackReport_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBackReport.Click
        Response.Redirect(FILE_NAME_REDIRECT)
    End Sub
#End Region

End Class