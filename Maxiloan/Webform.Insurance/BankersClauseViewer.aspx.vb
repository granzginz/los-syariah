﻿Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports Maxiloan.General.CommonCookiesHelper

Public Class BankersClauseViewer
    Inherits Maxiloan.Webform.WebBased

    Private oController As New InsPolicyNumberControler
    Private oParameter As New Parameter.BankersClause

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property NoClause() As String
        Get
            Return CType(ViewState("NoClause"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("NoClause") = Value
        End Set
    End Property
    Private Property InsuranceComBranchID() As String
        Get
            Return CType(ViewState("InsuranceComBranchID"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsuranceComBranchID") = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        bindReport()
    End Sub

    Private Sub bindReport()
        GetCookies()

        Dim oDataSet As New DataSet
        Dim oReport As BankersClausePrint = New BankersClausePrint

        With oParameter
            .strConnection = GetConnectionString()
            .NoClause = Me.NoClause
            .InsuranceComBranchID = Me.InsuranceComBranchID
        End With

        oDataSet = oController.GetBankersClausePrint(oParameter)

        oReport.SetDataSource(oDataSet)
        'Wira 2015-07-02
        AddParamField(oReport, "CompanyName", GetCompanyName())

        CrystalReportViewer.ReportSource = oReport
        CrystalReportViewer.DataBind()

        With oReport.ExportOptions
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        Dim doctoprint As New System.Drawing.Printing.PrintDocument
        Dim i As Integer
        'doctoprint.PrinterSettings.PrinterName = "Maxiloan Printer Server"

        For i = 0 To doctoprint.PrinterSettings.PaperSizes.Count - 1
            Dim rawKind As Integer
            If doctoprint.PrinterSettings.PaperSizes(i).PaperName = "BankersClause" Then
                rawKind = CInt(doctoprint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(doctoprint.PrinterSettings.PaperSizes(i)))
                oReport.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
                Exit For
            End If
        Next

        Dim strFileLocation As String

        strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "xml\"
        strFileLocation += Me.Session.SessionID + Me.Loginid + "BankersClause.pdf"

        Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
        New CrystalDecisions.Shared.DiskFileDestinationOptions

        DiskOpts.DiskFileName = strFileLocation

        With oReport
            .ExportOptions.DestinationOptions = DiskOpts
            .Export()
            .Close()
            .Dispose()
        End With

        Response.Redirect("BankersClauseView.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "BankersClause")
    End Sub

    Private Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_BANKERS_CLAUSE)
        Me.NoClause = cookie.Values("NoClause")
        Me.InsuranceComBranchID = cookie.Values("InsuranceComBranchID")
    End Sub

    Function GetCompanyName() As String
        Dim result As String

        Dim oConInst As New InstallRcvController
        Dim oInstal As New Parameter.InstallRcv
        oInstal.strConnection = GetConnectionString()
        oInstal.SPName = "spGetCompanyFullName"
        oInstal.WhereCond = ""
        If oConInst.GetSP(oInstal).ListData.Rows.Count > 0 Then
            result = oConInst.GetSP(oInstal).ListData.Rows(0).Item("CompanyFullName").ToString.Trim
        End If

        Return result
    End Function

    Private Sub AddParamField(ByVal oReport As BankersClausePrint, ByVal fieldName As String, ByVal value As Object)

        Dim discrete As ParameterDiscreteValue
        Dim ParamField As ParameterFieldDefinition
        Dim CurrentValue As ParameterValues

        ParamField = oReport.DataDefinition.ParameterFields(fieldName)
        discrete = New ParameterDiscreteValue
        discrete.Value = value
        CurrentValue = New ParameterValues
        CurrentValue = ParamField.DefaultValues
        CurrentValue.Add(discrete)
        ParamField.ApplyCurrentValues(CurrentValue)
    End Sub
End Class