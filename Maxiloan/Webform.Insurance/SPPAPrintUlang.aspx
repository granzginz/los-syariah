﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SPPAPrintUlang.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.SPPAPrintUlang" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInsuranceBranchName" Src="../Webform.UserController/UcInsuranceBranchName.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SPPAPrintUlang</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />   
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/';

        var hdnDetail;
        var hdndetailvalue;
        var cboBankAccount = null;
        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null

            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 1;
            };
            eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One', '0');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);

                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            }
        };

        function cboChildonChange(selIndex, l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
            eval('document.forms[0].hdnBankAccount').value = selIndex;
        }
        function RestoreInsuranceCoIndex(BankAccountIndex) {
            cboChild = eval('document.forms[0].cboChild');
            if (cboChild != null)
                if (eval('document.forms[0].hdnBankAccount').value != null) {
                    if (BankAccountIndex != null)
                        cboChild.selectedIndex = BankAccountIndex;
                }
        }
    </script>     
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
    <input id="hdnBankAccount" runat="server" type="hidden" name="hdSP" />

    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="LblErrorMessages" runat="server"></asp:Label>
    <asp:Panel ID="PnlSPPAListing" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK ULANG SPPA
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DGridSPPA" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:ButtonColumn Text="CETAK" CommandName="PRINT"></asp:ButtonColumn>
                            <asp:BoundColumn DataField="SPPADate" HeaderText="TGL SPPA" DataFormatString="{0:dd/MM/yyyy}">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SPPANo" HeaderText="NO SPPA"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CustomerName" HeaderText="NAMA KONSUMEN"></asp:BoundColumn>
                            <asp:BoundColumn DataField="AgreementNo" HeaderText="AGREEMENT NO"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                CARI SPPA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang
            </label>
<%--            <uc1:ucbranchall id="oBranch" runat="server">
                </uc1:ucbranchall>--%>
                <asp:DropDownList ID="cboParent" runat="server" onchange="<%#BranchIDChange()%>">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Harap Pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
            <label>
                Perusahaan Asuransi
            </label>
<%--            <uc1:ucinsurancebranchname id="oInsuranceBranchName" runat="server">
                </uc1:ucinsurancebranchname>--%>
                <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                    ErrorMessage="Harap Pilih Perusahaan Asuransi" ControlToValidate="cboChild" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis
            </label>
            <asp:DropDownList ID="cboType" runat="server">
                <asp:ListItem Value="N">Kontrak Baru</asp:ListItem>
                <asp:ListItem Value="R">Perpanjangan</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form_right">
            <label>
                Tanggal Pilih Asuransi
            </label>
            <asp:TextBox runat="server" ID="txtStartSelectionDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtStartSelectionDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            <label>
                &nbsp;S/D&nbsp;
            </label>
            <asp:TextBox runat="server" ID="txtEndSelectionDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtEndSelectionDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
