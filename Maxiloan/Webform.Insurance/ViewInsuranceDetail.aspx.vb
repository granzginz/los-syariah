﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

#End Region

Public Class ViewInsuranceDetail
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private m_controller As New ApplicationController
    Protected WithEvents UcInsuranceData As UcInsuranceData
    Protected WithEvents UcInsuranceDataAgri As UcInsuranceDataAgriculture
    Dim pagesource As String
    Dim back As String

#End Region

#Region "Property"

    Private Property ApplicationID() As String
        Get
            Return (CType(ViewState("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        Me.BranchID = Request.QueryString("BranchID")
        Me.ApplicationID = Request("ApplicationID").ToString

        Dim oApplication As New Parameter.Application
        Dim oData As New DataTable
        Dim oRow As DataRow

        oApplication.strConnection = GetConnectionString()
        'oApplication.BranchId = Me.BranchID
        oApplication.AppID = Me.ApplicationID
        oApplication = m_controller.GetViewApplication(oApplication)
        If Not oApplication Is Nothing Then
            oData = oApplication.ListData
        End If
        If oData.Rows.Count > 0 Then
            oRow = oData.Rows(0)
            If oRow("ApplicationModule").ToString.Trim = "AGRI" Then
                InsuranceDataAgri.Visible = True
                InsuranceData.Visible = False
                UcInsuranceDataAgri.ApplicationID = Me.ApplicationID
                UcInsuranceDataAgri.BranchID = oRow("BranchID").ToString.Trim
                UcInsuranceDataAgri.BindData()
            Else
                InsuranceDataAgri.Visible = False
                InsuranceData.Visible = True
                UcInsuranceData.ApplicationID = Me.ApplicationID
                UcInsuranceData.BranchID = oRow("BranchID").ToString.Trim
                UcInsuranceData.BindData()
            End If
        Else
            ShowMessage(lblMessage, "Aplikasi tidak dapat ditemukan", True)
        End If
        Dim imb As New Button
        imb = CType(Me.FindControl("btnClose"), Button)
        imb.Attributes.Add("onclick", "window.close()")

    End Sub

    

    'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '    If pagesource = "app" Then
    '        'Response.Redirect("../AccAcq/Credit/CreditProcess/NewApplication/ViewApplication.aspx")
    '        Response.Redirect("../Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?Applicationid=" & Me.ApplicationID & "&style=" & Request("style") & "")
    '    ElseIf pagesource = "covering" Then
    '        Response.Redirect("Inquiry/InsuranceCoveringInq.aspx")
    '    ElseIf pagesource = "appsta" Then
    '        Response.Redirect("../Webform.LoanOrg/Credit/viewStatementOfAccount.aspx?Applicationid=" & Me.ApplicationID & "&style=" & Request("style") & "")
    '        'ElseIf pagesource = "covering" Then
    '        '    Response.Redirect("Inquiry/InsuranceCoveringInq.aspx")
    '        'ElseIf pagesource = "InquiryAgreement" Then
    '        '    Response.Redirect("../Collection/Inquiry/InqAgreementList.aspx")
    '        'ElseIf pagesource = "appsta" Then
    '        '    Response.Redirect("../AccAcq/Credit/ViewStatementOfAccount.aspx?ApplicationID=" & Me.ApplicationID & "&Style=AccMnt ")
    '    ElseIf pagesource = "InsuranceDue" Then
    '        Response.Redirect("../Webform.Insurance/inquiry/InsuranceDue.aspx")
    '    ElseIf pagesource = "Endor" Then
    '        Response.Redirect("../Webform.Insurance/Endorsment.aspx")
    '    ElseIf pagesource = "Terminate" Then
    '        Response.Redirect("../Webform.Insurance/InsTerminate.aspx")
    '        'ElseIf pagesource = "Claim" Then
    '        '    Response.Redirect("../Webform.Insurance/ClaimInqDet.aspx")
    '    Else
    '        Response.Redirect("PolicyReceive.aspx")
    '    End If

    '    ' Response.Redirect("../Webform.Insurance/PolicyReceive.aspx?")
    'End Sub

End Class