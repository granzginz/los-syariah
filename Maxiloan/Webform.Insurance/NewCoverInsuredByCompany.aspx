﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewCoverInsuredByCompany.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.NewCoverInsuredByCompany" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcApplicationType" Src="../webform.UserController/UcApplicationType.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NewCoverInsuredByCompany</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function OpenWinApplication(pApplicationID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewApplication.aspx?style=' + pStyle + '&ApplicationID=' + pApplicationID, 'Application', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script language="javascript" type="text/javascript">

		<!--
        function DisplayNilaiTotalPremiumByCust() {
            var totPremiumByCust;
            totPremiumByCust = 0;
            var numericDiscountPremium;
            numericDiscountPremium = 0;
            var numericPremiumToCust;
            numericPremiumToCust = 0;
            var TempPremiumByCust;
            TempPremiumByCust = 0

            numericDiscountPremium = parseFloat(document.InsNewCover.TxtDiscountPremium.value);
            TempPremiumByCust = parseFloat(document.all["LblPremiumToCust"].innerHTML) - parseFloat(numericDiscountPremium);
            document.all["LblTotalPremiumByCust"].innerHTML = TempPremiumByCust;
            document.all["LblPremiumToCustFormat"].innerHTML = formatNumber((document.all["LblPremiumToCust"].innerHTML));
            document.all["LbTotalPremiumByCustformat"].innerHTML = formatNumber(TempPremiumByCust);

        }

        function DisplayNilayAmountCapitalized() {
            var PaidAmountByCust;
            PaidAmountByCust = 0;
            var TotPremiumByCust;
            TotPremiumByCust = 0;
            var AmountCapitalized;
            AmountCapitalized = 0;
            var AdditionalInsurancePremium;
            AdditionalInsurancePremium = 0;

            AdditionalInsurancePremium = parseFloat(document.Form1.TxtAdditionalInsurancePremium.value);
            PaidAmountByCust = parseFloat(document.Form1.TxtPaidAmountByCustomer.value);
            AmountCapitalized = (parseFloat(document.all["LblTotalPremiumByCust"].innerHTML) - parseFloat(PaidAmountByCust)) + parseFloat(AdditionalInsurancePremium);
            document.all["LblAmountCapitalized"].innerHTML = AmountCapitalized;
            document.all["LblAmountCapitalizedFormat"].innerHTML = formatNumber(AmountCapitalized);

        }

        function formatNumber(pValue) {
            var lArrPre = pValue.toString().split('.')
            var lStrValue = '';
            var lBytLength = 0;

            lBytLength = lArrPre[0].length - 3;
            while (lBytLength > 0) {
                lStrValue = ',' + lArrPre[0].substr(lBytLength, 3) + lStrValue;
                lBytLength -= 3;
            }
            lBytLength += 3;
            lStrValue = lArrPre[0].substr(0, lBytLength) + lStrValue;

            if (lArrPre.length == 2) {
                lStrValue += '.' + lArrPre[1];
            } else {
                lStrValue += '.00';
            }
            return lStrValue;
        }
	
			
// -->
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <asp:Label ID="LblErrorMessages" runat="server"></asp:Label>
    <form id="form1" runat="server">
    <asp:Panel ID="pnl1" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    COVER BARU - DIASURANSI OLEH PERUSAHAAN
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Aplikasi
                </label>
                <asp:Label ID="LblApplicationID" runat="server" Visible="False"></asp:Label>
                <asp:Label ID="LblInsuranceType" runat="server" Visible="False"></asp:Label>
                <asp:HyperLink ID="HplinkApplicationID" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:Label ID="lblCustomerName" runat="server" Visible="false">lblCustomerName</asp:Label>
                <asp:HyperLink ID="HpLinkCustName" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Asset</label>
                <asp:Label ID="LblAssetDescr" runat="server">LblAssetDescr</asp:Label>
                <asp:Label ID="LblManufacturingYear" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Asset Asuransi
                </label>
                <asp:Label ID="lblInsuranceAssetType" runat="server">lblInsuranceAssetType</asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Penggunaan Asset
                </label>
                <asp:Label ID="LblAssetUsageDescr" runat="server">LblAssetUsageDescr</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Asset : Baru / Bekas
                </label>
                <asp:Label ID="LblAssetNewused" runat="server">LblAssetNewused</asp:Label>
            </div>
            <div class="form_right">
                <label>
                    ID Penggunaan
                </label>
                <asp:Label ID="LblAssetUsageID" runat="server" Visible="False"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Di Asuransi Oleh
                </label>
                <asp:Label ID="LblInsuredBy" runat="server">LblInsuredBy</asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Di Bayar Oleh
                </label>
                <asp:Label ID="LblPaidBy" runat="server">LblPaidBy</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Perusahaan Asuransi</label>
                <asp:DropDownList ID="cmbInsuranceComBranch" runat="server">
                </asp:DropDownList>
                <asp:Label ID="LblFocusMaskAssID" runat="server"></asp:Label>
                <asp:Label ID="LblFocusInsuranceComBranchName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <div class="mandatory_container">
                    <label>
                        Nilai pertanggungan
                    </label>
                    <label class="mandatory">
                        *</label></div>
                <%--<asp:TextBox ID="TxtAmountCoverage" runat="server" Width="115px" ></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="harap isi nilai pertanggungan " ControlToValidate="TxtAmountCoverage"
                    ValidationExpression="\d*"></asp:RegularExpressionValidator>--%>
                <uc1:ucnumberformat id="TxtAmountCoverage" runat="server" /></uc1:ucNumberFormat>
            </div>
            <div class="form_right">
                <label>
                    Jenis Aplikasi
                </label>
                <asp:Label ID="LblApplicationType" runat="server"></asp:Label>&nbsp;
                <uc1:ucapplicationtype id="oApplicationType" runat="server"></uc1:ucapplicationtype>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
        <asp:Label ID="LblMinimumTenor" runat="server" Font-Names="Verdana" Font-Size="8pt"
            Visible="False"></asp:Label>
        <asp:Label ID="LblMaximumTenor" runat="server" Font-Names="Verdana" Font-Size="8pt"
            Visible="False"></asp:Label>
        <asp:Label ID="LblBranchID" runat="server" Font-Names="Verdana" Font-Size="8pt" Visible="False"></asp:Label>
        <asp:Label ID="LblTenorCredit" runat="server" Font-Names="Verdana" Font-Size="8pt"
            Visible="False"></asp:Label>
        <asp:Label ID="LblJmlGrid" runat="server" Font-Names="Verdana" Font-Size="8pt" Visible="False"></asp:Label>
        <asp:Panel ID="PnlGrid" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        JENIS COVER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DgridInsurance" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                            BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="TAHUN">
                                    <ItemTemplate>
                                        <asp:Label ID="LblYearInsurance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearInsurance") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="COVER">
                                    <ItemTemplate>
                                        <asp:Label ID="LblHiddenCoverageTypeDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CoverageType") %>'>
                                        </asp:Label>
                                        <asp:DropDownList ID="DDLCoverageTypeDgrid" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI">
                                    <ItemTemplate>
                                        <asp:Label ID="LblPremiumDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Premium", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI SRCC">
                                    <ItemTemplate>
                                        <asp:Label ID="LblHiddenSRCCDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.SRCC") %>'>
                                        </asp:Label>
                                        <asp:DropDownList ID="DDLBolSRCCPilihan" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH SRCC">
                                    <ItemTemplate>
                                        <asp:Label ID="LblSRCCAmountDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCAmount","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH TPL">
                                    <ItemTemplate>
                                        <asp:Label ID="LblHiddenTPLDgrid" runat="server" Visible="False"></asp:Label>
                                        <asp:DropDownList ID="DDLTPLPilihan" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI TPL">
                                    <ItemTemplate>
                                        <asp:Label ID="LblTPLAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmount","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                    <ItemTemplate>
                                        <asp:Label ID="LblHiddenFloodDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.Flood") %>'>
                                        </asp:Label>
                                        <asp:DropDownList ID="DDLFloodPilihan" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH FLOOD">
                                    <ItemTemplate>
                                        <asp:Label ID="LblFloodAmountDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLOODAmount","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TOTAL">
                                    <ItemTemplate>
                                        <asp:Label ID="LblTotalDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DIBAYAR CUSTOMER">
                                    <ItemTemplate>
                                        <asp:Label ID="LblHiddenPaidByCust" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.PaidByCust") %>'>
                                        </asp:Label>
                                        <asp:DropDownList ID="DDLPaidByCustDGrid" runat="server" Visible="True">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnCalculate" runat="server" CausesValidation="False" Text="Calculate"
                    CssClass="small button blue"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="PnlDGrid2Insurance" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        JENIS COVER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="Dgrid2Insurance" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                            BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="TAHUN">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="COVER">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGrid2Coverage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CoverageType") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGrid2Premium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI SRCC">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridSRCC" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BolSRCC") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH SRCC">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGrid2SRCCAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI TPL">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGrid2TPL" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH TPL">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGrid2TPLAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLPremiumToCust","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGrid2Flood" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BolFlood") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH FLOOD">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGrid2FloodAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLOODPremiumToCust","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TOTAL">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="LblGrid2TotalPremiumTOCustAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PremiumToCustAmount","{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DIBAYAR OLEH CUSTOMER">
                                    <ItemTemplate>
                                        <asp:Label ID="LbGrid2lPaidByCust" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaidByCust") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="PnlEntry" runat="server">
            <asp:Label ID="LblErrorMessages2" runat="server" ForeColor="Red"></asp:Label>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Premi SRCC
                    </label>
                    <asp:Label ID="LblTotalSRCCPremium" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Premi TPL
                    </label>
                    <asp:Label ID="LblTotalTPLPremium" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Premi Flood
                    </label>
                    <asp:Label ID="LblTotalFloodPremium" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Biaya Admin
                    </label>
                    <%--<asp:TextBox ID="txtInsAdminFee" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtInsAdminFee"
                        Display="Dynamic" ErrorMessage="Harap isi Biaya Admin" ValidationExpression="\d*"
                        CssClass="validator_general"></asp:RegularExpressionValidator>--%>
                        <uc1:ucnumberformat id="txtInsAdminFee" runat="server" /></uc1:ucNumberFormat>

                    <asp:Label ID="LblInsAdminFeeBehaviour" runat="server" Visible="False"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtInsAdminFee"
                        Display="Dynamic" ErrorMessage="Harap isi Biaya Admin" CssClass="validator_general"></asp:RequiredFieldValidator>
                </div>
                <div class="form_right">
                    <label>
                        Biaya Materai
                    </label>
                    <%--<asp:TextBox ID="TxtInsStampDutyFee" runat="server"></asp:TextBox>--%>
                    <asp:Label ID="LblInsStampDutyBehavior" runat="server" Visible="False"></asp:Label>
                   <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TxtInsStampDutyFee"
                        Display="Dynamic" ErrorMessage="Harap isi Biaya Materai" ValidationExpression="\d*"
                        CssClass="validator_general"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TxtInsStampDutyFee"
                        Display="Dynamic" ErrorMessage="Harap isi Biaya Materai" CssClass="validator_general"></asp:RequiredFieldValidator>--%>
                        <uc1:ucnumberformat id="TxtInsStampDutyFee" runat="server" /></uc1:ucNumberFormat>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total Premi Standard
                    </label>
                    <asp:Label ID="LblTotalStandardPremium" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Premi Customer
                    </label>
                    <asp:Label ID="LblPremiumToCust" runat="server" ForeColor="White"></asp:Label>
                    <asp:Label ID="LblPremiumToCustFormat" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Discount Premi
                    </label>
                    <input id="txtdiscountpremium" runat="server" class="inptype" maxlength="15" name="txtdiscountpremium" 
                        onkeyup="displaynilaitotalpremiumbycust()" size="17" style="width: 136px; height: 17px"
                        type="text" />
                    <asp:regularexpressionvalidator id="regamountreceive" runat="server" controltovalidate="txtdiscountpremium"
                        display="dynamic" enabled="true" errormessage="harap isi dengan angka" validationexpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                        visible="true" cssclass="validator_general"></asp:regularexpressionvalidator>
                    <asp:requiredfieldvalidator id="requiredfieldvalidator1" runat="server" controltovalidate="txtdiscountpremium"
                        display="dynamic" errormessage="harap isi discount premi" cssclass="validator_general"></asp:requiredfieldvalidator>
                          
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Total Premi Customer
                    </label>
                    <asp:Label ID="LblTotalPremiumByCust" runat="server" ForeColor="#ffffff"></asp:Label>
                    <asp:Label ID="LbTotalPremiumByCustformat" runat="server"></asp:Label>
                </div>
                <div class="form_right">
                    <label>
                        Jumlah Prepaid
                    </label>
                    <asp:Label ID="lblPrepaid" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_left">
                    <label>
                        Catatan Accessories
                    </label>
                    <asp:TextBox ID="TxtAccNotes" runat="server" Height="80px" MaxLength="500" TextMode="MultiLine"
                        Width="167px"></asp:TextBox>
                </div>
                <div class="form_right">
                    <label>
                        Catatan Asuransi
                    </label>
                    <asp:TextBox ID="TxtInsNotes" runat="server" Height="82px" MaxLength="500" TextMode="MultiLine"
                        Width="160px"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnCancelBawah" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
