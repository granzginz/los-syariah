﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class Endorsment
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = General.CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Private pageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Private currentPageNumber As Int16 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Private totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Private recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT

    Private oCustomClass As New Parameter.InsEndorsment
    Private oController As New InsEndorsmentController
#End Region
#Region "Property "
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
#End Region
#Region "InitialDefaultPanel"
    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
    End Sub

#End Region
#Region "Page Load "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            fillcbo()
        End If
        Me.FormID = "InsEndors"

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            InitialDefaultPanel()
        Else

            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If

    End Sub
#End Region
#Region "Bindgrid"
    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        Dim intloop As Integer
        Dim hypID As HyperLink

        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort
        End With


        oCustomClass = oController.InsEndorsmentList(oCustomClass)

        If Not oCustomClass Is Nothing Then
            DtUserList = oCustomClass.ListData
            recordCount = oCustomClass.TotalRecords
        Else
            recordCount = 0
        End If

        DtUserList = oCustomClass.ListData
        DvUserList = DtUserList.DefaultView

        dtgPaging.DataSource = DvUserList

        If Not DvUserList Is Nothing Then
            dtgPaging.DataBind()
        End If
        With oCustomClass
            lblrecord.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With
        PagingFooter()
        '        

    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

#End Region
    'description : sub ini untuk mengisi data ke combo box branch
    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With ddlBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub
#Region "Search !"
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        LblErrMessages.Text = ""

        Me.SearchBy = ""
        Me.SortBy = ""

        If ddlBranch.SelectedValue <> "" And ddlBranch.SelectedValue <> "ALL" Then
            Me.SearchBy = "BranchID='" & ddlBranch.SelectedValue & "'"
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
        PnlGrid.Visible = True
    End Sub
#End Region

#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        '

        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Paging Footer"
    Private Sub PagingFooter()
        PnlGrid.Visible = True
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblrecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

#End Region
#Region "Go Page"
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                Bindgrid(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy.Trim, Me.SortBy)
    End Sub
#End Region
#Region "Reset !"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        LblErrMessages.Text = ""

        ddlBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub
#End Region
#Region "dtgPaging_ItemCommand"
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        If e.CommandName = "Endors" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Endor", Me.AppId) Then
                Dim strCustomerName As String
                Dim strApplicationID As String
                Dim strAgreementNo As String
                Dim strAsset As String
                Dim strBranchId As String
                Dim strAssetSeqNo As String
                Dim strInsSeqNo As String

                strBranchId = CType(e.Item.FindControl("lblBranchID"), Label).Text.Trim
                strAssetSeqNo = CType(e.Item.FindControl("lblAssetSeqNo"), Label).Text.Trim
                strInsSeqNo = CType(e.Item.FindControl("lblInsSeqNo"), Label).Text.Trim
                strCustomerName = CType(e.Item.FindControl("hyCustomerName"), HyperLink).Text
                strAgreementNo = CType(e.Item.FindControl("hyAgreementNo"), HyperLink).Text.Trim
                strApplicationID = CType(e.Item.FindControl("lblApplicationId"), Label).Text.Trim
                strAsset = CType(e.Item.FindControl("lblAsset"), Label).Text.Trim

                Dim cookie As HttpCookie = Request.Cookies("Endors")

                If Not cookie Is Nothing Then
                    cookie.Values("CustomerName") = strCustomerName
                    cookie.Values("ApplicationID") = strApplicationID
                    cookie.Values("AgreementNo") = strAgreementNo
                    cookie.Values("Asset") = strAsset
                    cookie.Values("BranchID") = strBranchId
                    cookie.Values("AssetSeqNo") = strAssetSeqNo
                    cookie.Values("InsSeqNo") = strInsSeqNo
                    Response.AppendCookie(cookie)
                Else
                    Dim cookieNew As New HttpCookie("Endors")
                    cookieNew.Values.Add("CustomerName", strCustomerName)
                    cookieNew.Values.Add("ApplicationID", strApplicationID)
                    cookieNew.Values.Add("AgreementNo", strAgreementNo)
                    cookieNew.Values.Add("Asset", strAsset)
                    cookieNew.Values.Add("BranchID", strBranchId)
                    cookieNew.Values.Add("AssetSeqNo", strAssetSeqNo)
                    cookieNew.Values.Add("InsSeqNo", strInsSeqNo)
                    Response.AppendCookie(cookieNew)
                End If
                Response.Redirect("EndorsmentDetail.aspx")
            End If
        End If
    End Sub
#End Region

End Class