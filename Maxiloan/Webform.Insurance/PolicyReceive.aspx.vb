﻿#Region "Imports"
Option Strict On

Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class PolicyReceive
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents oBranch As ucBranchAll
    Protected WithEvents oInsuranceBranchName As UcInsuranceBranchName

    Public Function GenerateLinkTo(ByVal flagins As String, ByVal flagdoc As String) As String
        'If flagins.Trim <> "E" And flagdoc.Trim = "E" Then
        '    Return "InsuranceEndorsValid.aspx" 'Endorsment Policy Receive
        '    'Ini untuk polis yang pernah diterima tapi dikembalikkan
        '    'lagi ke insco karena terdapat kesalahan
        '    ' hanya yang flaginsactivationnya=E dan flag DocStatus=N
        '    'Nanti apabila setelah dicek semua dan datanya benar, maka
        '    ' flag insactivation=A dan flagDocStatus=O
        '    ' Bila tidak ganti flagdocStatusnya menjadi = E
        'Else
        '    Return "PolicyBillings.aspx"  'pertama kali'
        '    'ini untuk polis yang masuk pertana kali, status awalnya
        '    ' flaginsactivation=N dan flagDocStatus=N
        '    'Nanti apabila setelah dicek semua dan datanya benar, maka
        '    ' flag insactivation=A dan flagDocStatus=O
        '    ' Bila tidak ganti flagdocStatusnya menjadi = E
        'End If
        If cboProductAsuransi.SelectedItem.Value = "KB" Then
            Return "PolicyBillings.aspx"
        ElseIf cboProductAsuransi.SelectedItem.Value = "JK" Then
            Return "PolicyBillingsJK.aspx"
        ElseIf cboProductAsuransi.SelectedItem.Value = "CP" Then
            Return "PolicyBillingsCP.aspx"
        End If
    End Function

#Region "Property "

    Private Property StrHelperJS() As String
        Get
            Return CType(ViewState("helperReplaceCombobox"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("helperReplaceCombobox") = Value
        End Set
    End Property

    Private Property StrHelperJSDataTable() As DataTable
        Get
            Return CType(ViewState("dataTableHandle"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("dataTableHandle") = Value
        End Set
    End Property

    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Property InsuranceComBranchID() As String
        Get
            Return ViewState("InsuranceComBranchID").ToString
        End Get
        Set(ByVal Value As String)
            ViewState("InsuranceComBranchID") = Value
        End Set
    End Property
#End Region

#Region "Constanta"


    Private m_controller As New InsurancePolicyReceiveController
    Private o_controller As New AssetDataController
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    Dim BranchIDIns As String
    Protected WithEvents oSearchBy As UcSearchBy
    Private m_controller2 As New DataUserControlController
    Private oControllerChild As New GeneralPagingController
    Dim oEntitesChild As New Parameter.InsCoAllocationDetailList
#End Region

#Region "InitialDefaultPanel"

    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
        FillCbo(cboProductAsuransi, "dbo.tblinsuranceproduct")
    End Sub


#End Region

#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'LblBranchName.Text = Me.BranchName

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            'oBranch.IsAll = False
            'oInsuranceBranchName.loadInsBranch(False)
            Dim dtbranch As New DataTable
            Dim dtInsCo As New DataTable

            Me.sesBranchId = Replace(Me.sesBranchId, "'", "")

            If Me.IsHoBranch Then
                dtbranch = m_controller2.GetBranchName(GetConnectionString, "All")
            Else
                dtbranch = m_controller2.GetBranchName(GetConnectionString, Me.sesBranchId)
            End If

            With cboParent
                .DataTextField = "Name"
                .DataValueField = "ID"
                .DataSource = dtbranch
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
            End With

            getchildcombo()
            'oInsuranceBranchName.FillRequired = True

            'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 110 Then
            'If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) = 0 And Me.IsHoBranch = True Then
            '    oSearchBy.ListData = "dbo.Agreement.AgreementNo, No Kontrak-dbo.Customer.Name , Nama Konsumen"
            '    oSearchBy.BindData()
            'End If
            oSearchBy.ListData = "dbo.Agreement.AgreementNo, No Kontrak-dbo.Customer.Name, Nama Konsumen"
            oSearchBy.BindData()

            InitialDefaultPanel()

            If Request.QueryString("back") = "no" And Request.QueryString("print") = "yes" Then
                Response.Redirect("Report/PrintPDFEndorsLetterViewer.aspx?InsuranceComBranchID=" + Request.QueryString("InsuranceComBranchID") + "")
            End If
            If Request.QueryString("InsuranceComBranchID") <> "" Then
                Me.InsuranceComBranchID = Request.QueryString("InsuranceComBranchID")
                'oInsuranceBranchName.SelectedValue = Me.InsuranceComBranchID
                cboParent.SelectedValue = Me.InsuranceComBranchID
                bindGrid(False)
            End If

            If Request.QueryString("strFileLocation") <> "" Then

                Dim strFileLocation As String

                strFileLocation = "../XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "var x = screen.width; var y = screen.height;  window.open('" & strFileLocation & "','accacq', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes'); " & vbCrLf _
                & "</script>")

            End If
        End If
    End Sub

#End Region

#Region "Search"

    'Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    bindGrid(True)
    'End Sub
    'Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.SearchBy = ""
        bindGrid(True)
        cboParent.SelectedIndex = 0 '
    End Sub


    Sub bindGrid(ByVal bindSearch As Boolean)
        If bindSearch Then
            PnlGrid.Visible = True
            Me.Sort = " Insuranceasset.BranchID "
            'Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & oBranch.BranchID & "' "
            Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & cboParent.SelectedItem.Value.Trim & "' "

            'modify nofi 05122018 karena CP / JK tidak bisa pake MaskAssBranchID
            'If oInsuranceBranchName.SelectedValue <> "" Then
            'If cboParent.SelectedValue <> "" Then

            '    'Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID = '" & Replace(oInsuranceBranchName.SelectedValue, "'", "") & "' "
            '    'Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' "
            '    Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' "
            'End If

            If cboProductAsuransi.SelectedValue <> "" Then
                If cboProductAsuransi.SelectedValue = "KB" Then
                    Me.SearchBy = Me.SearchBy & " and ( isnull(InsuranceAssetDetail.PolicyNumber,'') like '' or  isnull(InsuranceAssetDetail.PolicyNumber,'-') like '-' ) AND isnull(dbo.InsuranceAssetDetail.NoNotaAsuransi,'') = '' And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' and  InsuranceAsset.KendaraanMotor='1'"
                    'Me.SearchBy = Me.SearchBy & " and dbo.InsuranceAsset.PolicyNumber='-' AND isnull(dbo.InsuranceAsset.NoNotaAsuransi,'') = '' And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' "
                    'Me.SearchBy = Me.SearchBy & " and dbo.InsuranceAsset.PolicyNumber='-' AND isnull(dbo.InsuranceAsset.NoNotaAsuransi,'') = '' And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' and InsuranceAsset.CreditProtection <> '1' and InsuranceAsset.JaminanCredit <> '1' "
                ElseIf cboProductAsuransi.SelectedValue = "CP" Then
                    'Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchIDCreditProtection = '" & hdnChildValue.Value.Trim & "' And dbo.InsuranceAsset.CreditProtection = '1' AND dbo.InsuranceAsset.SPPACPNo <> '-'"
                    Me.SearchBy = Me.SearchBy & "  and (isnull(dbo.InsuranceAsset.PolicyNumberCP, '')='' or isnull(dbo.InsuranceAsset.PolicyNumberCP, '-')='-')   And dbo.InsuranceAsset.MaskAssBranchIDCreditProtection = '" & hdnChildValue.Value.Trim & "' And dbo.InsuranceAsset.CreditProtection = '1' "
                ElseIf cboProductAsuransi.SelectedValue = "JK" Then
                    'Me.SearchBy = Me.SearchBy & " and isnull(dbo.InsuranceAsset.PolicyNumberJK, '')=''  And dbo.InsuranceAsset.MaskAssBranchIDJaminanCredit = '" & hdnChildValue.Value.Trim & "' And dbo.InsuranceAsset.JaminanCredit = '1' AND dbo.InsuranceAsset.SPPAJKNo <> '-'"
                    Me.SearchBy = Me.SearchBy & " and ( isnull(dbo.InsuranceAsset.PolicyNumberJK, '')='' or isnull(dbo.InsuranceAsset.PolicyNumberJK, '-')='-') And dbo.InsuranceAsset.MaskAssBranchIDJaminanCredit = '" & hdnChildValue.Value.Trim & "' And dbo.InsuranceAsset.JaminanCredit = '1'  "
                End If

            End If

            If oSearchBy.Text.Trim <> "" Then
                Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
            End If


        Else
            PnlGrid.Visible = True
            Me.Sort = " Insuranceasset.BranchID "
            Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & Me.sesBranchId.Replace("'", "") & "' "

            'If oInsuranceBranchName.SelectedValue <> "" Then
            If cboParent.SelectedValue <> "" Then
                'Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID = '" & oInsuranceBranchName.SelectedValue & "' "
                Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.MaskAssBranchID = '" & hdnChildValue.Value.Trim & "' "
            End If

        End If
        BindGridEntity(Me.SearchBy)
    End Sub
#End Region

#Region "BindGrid"

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As New DataTable
        Dim oInsStdRate As New Parameter.InsuranceStandardPremium
        Me.SearchBy = cmdWhere

        If Me.Sort = "" Then
            Me.Sort = " InsuranceAsset.ApplicationID "
        End If

        With oInsStdRate
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = Me.Sort
            .strConnection = GetConnectionString()
            .ProductInsurance = cboProductAsuransi.SelectedValue
        End With

        oInsStdRate = m_controller.InsurancePolicyReceive(oInsStdRate)

        If Not oInsStdRate Is Nothing Then
            dtEntity = oInsStdRate.ListData
            recordCount = oInsStdRate.TotalRecords
        Else
            recordCount = 0
        End If

        DtgPaging.DataSource = dtEntity.DefaultView
        DtgPaging.CurrentPageIndex = 0
        DtgPaging.DataBind()
        Response.Write(StrHelperJS)
        PagingFooter()
    End Sub
    Private Sub DtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgPaging.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data tidak ditemukan!", true)
            lblTotPage.Text = "1"
            ' rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            '   rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        'BindGridEntity(Me.CmdWhere)
        BindGridEntity(Me.SearchBy)

    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                'BindGridEntity(Me.CmdWhere)
                BindGridEntity(Me.SearchBy)
            End If
        End If
    End Sub
#End Region

#Region "Sorting"

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles DtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy)
    End Sub

#End Region

#Region "Reset "

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        'oInsuranceBranchName.SelectedValue = ""
        cboParent.SelectedItem.Value = ""
        'Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & Replace(Me.sesBranchId, "'", "") & "' "
        Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & cboParent.SelectedItem.Value.Trim() & "' "
        oSearchBy.Text = ""
        oSearchBy.BindData()
        BindGridEntity(Me.SearchBy)

    End Sub

#End Region

#Region "Div Table in code"

    Private Sub GenDivTable()
        'Dim a As New HtmlGenericControl("div")
        'a.Attributes.Add("class", "form_box")
        Dim a As HtmlGenericControl = genDiv("form_box")
        Dim b As HtmlGenericControl = genDiv("form_single")
        'Dim b As New HtmlGenericControl("div")
        'b.Attributes.Add("class", "form_single")
        b.InnerHtml = "<label>test</label> uyeeeaaaa"
        a.Controls.Add(b)

        pnlResult.Controls.Add(a)

        a = genDiv("form_box")
        b = genDiv("form_left")
        Dim c As HtmlGenericControl = genLabel("custName")


        Dim chk As New CheckBox
        chk.ID = "chk"
        chk.Text = "coba nih"
        Dim d As HtmlGenericControl = genCustom(chk)

        insertDiv(b, c, d)
        insertDiv(a, b)

        b = genDiv("form_right")
        c = genLabel("nomor KK")
        d = genLabel(" 23123 ")
        insertDiv(b, c, d)
        insertDiv(a, b)

        pnlResult.Controls.Add(a)

        'table
        Dim lObjTblRow As HtmlTableRow
        Dim lObjTblCell As HtmlTableCell
        Dim lObjChkBox As HtmlInputCheckBox
        lObjTblRow = New HtmlTableRow
        lObjTblCell = New HtmlTableCell
        lObjChkBox = New HtmlInputCheckBox
        lObjChkBox.ID = "chkNewName"

        lObjTblCell = New HtmlTableCell
        With lObjTblCell
            .Controls.Add(lObjChkBox)

            .Attributes.Add("width", "25%")
            .Attributes.Add("bgcolor", "#ffffff")
        End With
        lObjTblRow.Cells.Add(lObjTblCell)
    End Sub

    Private Sub insertDiv(ByVal a As HtmlGenericControl, ByVal b As HtmlGenericControl)
        a.Controls.Add(b)
    End Sub

    Private Sub insertDiv(ByVal a As HtmlGenericControl, ByVal b As HtmlGenericControl, ByVal c As HtmlGenericControl)
        a.Controls.Add(b)
        a.Controls.Add(c)
    End Sub
    Private Function genDiv(ByVal classname As String) As HtmlGenericControl
        Dim a As New HtmlGenericControl("div")
        a.Attributes.Add("class", classname)
        Return a
    End Function

    Private Function genLabel(ByVal value As String) As HtmlGenericControl
        Dim a As New HtmlGenericControl("label")
        a.InnerHtml = value
        Return a
    End Function

    Private Function genCustom(ByVal value As Control) As HtmlGenericControl
        Dim a As New HtmlGenericControl()
        a.Controls.Add(value)
        Return a
    End Function
#End Region
    'by Wira 20160224
    Protected Function BranchIDChange() As String

        'Dim listdata = "ListData[this.selectedIndex]"
        Return "ParentChange('" & Trim(cboParent.ClientID) & "','" & Trim(cboChild.ClientID) & "','" & Trim(hdnChildValue.ClientID) & "','" & Trim(hdnChildName.ClientID) & "',((this.selectedIndex == -1) ? null : ListData[this.selectedIndex]));"
    End Function

    Protected Sub getchildcombo()

        Dim dtInsCo As New DataTable

        dtInsCo = getchildbranchCombotDT()
        Response.Write(GenerateScript(dtInsCo))

    End Sub

    Private Function getchildbranchCombotDT() As DataTable
        With oEntitesChild
            .strConnection = GetConnectionString()
            If Me.IsHoBranch Then
                .BranchId = "All"
            Else
                .BranchId = Me.sesBranchId
            End If
        End With

        Dim dtInsCo As New DataTable

        oEntitesChild = oControllerChild.GetInsuranceBranchCombo(oEntitesChild)
        dtInsCo = oEntitesChild.ListData
        Return dtInsCo
    End Function

    Private Function GenerateScript(ByVal DtTable As DataTable) As String
        Dim strScript As String
        Dim strScript1 As String
        Dim DataRow As DataRow()
        Dim strType As String
        Dim i As Int32
        Dim j As Int32
        strScript = "<script language=""JavaScript"">" & vbCrLf
        strScript &= "ListData = new Array(" & vbCrLf
        strType = ""
        For j = 0 To cboParent.Items.Count - 1
            DataRow = DtTable.Select(" BranchID = '" & cboParent.Items(j).Value & "'")
            If DataRow.Length > 0 Then
                For i = 0 To DataRow.Length - 1
                    If strType <> CStr(DataRow(i)("BranchID")).Trim Then
                        strType = CStr(DataRow(i)("BranchID")).Trim
                        strScript &= "new Array(" & vbCrLf
                        strScript1 = ""
                    End If


                    If strScript1 = "" Then
                        strScript1 = " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildId")), "null", DataRow(i)("ChildId"))).Trim & "') "
                    Else
                        strScript1 &= "," & vbCrLf & " new Array('" & CStr(DataRow(i)("name")).Trim & "','" & CStr(IIf(IsDBNull(DataRow(i)("ChildId")), "null", DataRow(i)("ChildId"))).Trim & "') "
                    End If
                Next
                strScript &= strScript1 & ")," & vbCrLf
            Else
                strScript &= " null," & vbCrLf
            End If
        Next

        If Right(strScript.Trim, 5) = "null," Then
            strScript = Left(strScript.Trim, strScript.Trim.Length - 1)
        Else
            strScript = Left(strScript.Trim, strScript.Trim.Length - 2)
        End If

        If Right(strScript.Trim, 4) = "null" Then
            strScript &= vbCrLf & ");" & vbCrLf
        Else
            strScript &= vbCrLf & "));" & vbCrLf
        End If
        'strScript &= vbCrLf & "));" & vbCrLf

        strScript &= "</script>"

        StrHelperJS = strScript
        Return strScript
    End Function

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        Dim DtChild As DataTable = getchildbranchCombotDT()
        Dim i As Integer

        For i = 0 To DtChild.Rows.Count - 1
            Page.ClientScript.RegisterForEventValidation(cboChild.UniqueID, CStr(DtChild.Rows(i).Item("ChildId")).Trim)
        Next

        Page.ClientScript.RegisterForEventValidation(cboChild.UniqueID, "0")


        MyBase.Render(writer)
    End Sub

    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = o_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub

End Class