﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewAPInsurance.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.ViewAPInsurance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewAPInsurance</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinMain(BranchID, Voucherno) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.LoanMNT/View/CashBankVoucher.aspx?BranchID=' + BranchID + '&Voucherno=' + Voucherno, null, 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                DETAIL A/P ASURANSI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No A/P
            </label>
            <asp:Label ID="lblAPNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Aplikasi
            </label>
            <asp:HyperLink ID="HYApplicationID" runat="server"></asp:HyperLink>
            <asp:Label ID="lblBranchID" runat="server" Visible="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="HyAgreementNo" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="HYCustomerName" runat="server"></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama Asset</label>
            <asp:Label ID="LblAssetDescription" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                ASURANSI ASSET
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Perusahaan Asuransi
            </label>
            <asp:HyperLink ID="HyInsco" runat="server"></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nilai Pertanggungan
            </label>
            <asp:Label ID="LblSumInsured" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                DiAsuransi Oleh
            </label>
            <asp:Label ID="LblInsuredByDescr" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                DiBayar Oleh
            </label>
            <asp:Label ID="LblPaidByDescr" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jangka Waktu
            </label>
            <asp:Label ID="lblCoverPeriod" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Periode Asuransi
            </label>
            <asp:Label ID="lblsdate" runat="server"></asp:Label>
            &nbsp;S/D&nbsp;
            <asp:Label ID="lblEDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Polis
            </label>
            <asp:Label ID="lblPolicyNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Terima Polis
            </label>
            <asp:Label ID="lblPolicyDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Catatan Accessories
            </label>
            <asp:Label ID="lblAccNotes" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Catatan Asuransi
            </label>
            <asp:Label ID="lblInsuranceNotes" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi Ke Customer
            </label>
            <asp:Label ID="lblPRemiToCust" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                PREMI KE PERUSAHAAN ASURANSI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi Utama
            </label>
            <asp:Label ID="lblMainPremium" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Loading Fee
            </label>
            <asp:Label ID="lblLoadingFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi SRCC
            </label>
            <asp:Label ID="lblSRCCPremium" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi Flood
            </label>
            <asp:Label ID="lblFloodPremium" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi TPL
            </label>
            <asp:Label ID="lblTPLPremium" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Admin
            </label>
            <asp:Label ID="lblAdminFee" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Meterai
            </label>
            <asp:Label ID="lblMeteraiFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Total Premi
            </label>
            <asp:Label ID="lblTotalPremium" runat="server" Font-Bold="true"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DtgList" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TAHUN">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="8%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="8%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblYearnum" runat="server" Text='<%#Container.DataItem("YearNum")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JENIS COVER">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="15%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="15%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblCoverageType" runat="server" Text='<%#Container.DataItem("CoverageTypeDescr")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="SRCC">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblSRCC" runat="server" Text='<%#formatnumber(Container.DataItem("SRCCPremiumToCust"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TPL">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="18%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblTPL" runat="server" Text='<%#formatnumber(Container.DataItem("TPLPremiumToCust"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="FlOOD">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="5%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="center" Width="5%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblFlOOD" runat="server" Text='<%#Container.DataItem("FLOODPremiumToCust")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="LOADING FEE">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="18%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right" Width="18%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblLoading" runat="server" Text='<%#formatnumber(Container.DataItem("FLOODPremiumToCust"),2)%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="DIBAYAR CUSTOMER">
                            <HeaderStyle HorizontalAlign="Center" Height="30px" Width="25%"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Left" Width="25%"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblPaidByCust" runat="server" Text='<%#Container.DataItem("PaidByCust")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                    <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                        Mode="NumericPages"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnExit" runat="server" Text="Exit" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
