﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class EndorsmentDetail
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private currentPage As Int32 = General.CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Private pageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Private currentPageNumber As Int16 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Private totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Private recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    Private cboCoverageNew As New DropDownList
    Private cboTPLAmountNew As New DropDownList
    Private cboSRCCNew As New DropDownList
    Private cboFloodNew As New DropDownList
    Private oCustomClass As New Parameter.InsEndorsment
    Private oController As New InsEndorsmentController

#End Region
#Region "Property "
    Private Property TotalPrevPremitocust() As Decimal
        Get
            Return CType(viewstate("TotalPrevPremitocust"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("TotalPrevPremitocust") = Value
        End Set
    End Property
    Private Property ActiveEndorsDate() As Date
        Get
            Return CType(viewstate("ActiveEndorsDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("ActiveEndorsDate") = Value
        End Set
    End Property
    Private Property ChangedEndorsDate() As Date
        Get
            Return CType(viewstate("ChangedEndorsDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("ChangedEndorsDate") = Value
        End Set
    End Property
    Private Property OldSRCC() As Boolean
        Get
            Return CType(viewstate("OldSRCC"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("OldSRCC") = Value
        End Set
    End Property
    Private Property NewSRCC() As Boolean
        Get
            Return CType(viewstate("NewSRCC"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("NewSRCC") = Value
        End Set
    End Property
    Private Property OldFlood() As Boolean
        Get
            Return CType(viewstate("OldFlood"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("OldFlood") = Value
        End Set
    End Property
    Private Property NewFlood() As Boolean
        Get
            Return CType(viewstate("NewFlood"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("NewFlood") = Value
        End Set
    End Property
    Private Property NewSumInsured() As Decimal
        Get
            Return CType(viewstate("NewSumInsured"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("NewSumInsured") = Value
        End Set
    End Property
    Private Property Counter() As Integer
        Get
            Return CType(viewstate("Counter"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("Counter") = Value
        End Set
    End Property
    Private Property AssetSeqNo() As String
        Get
            Return CType(viewstate("AssetSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Private Property EndorsmentLetter() As String
        Get
            Return CType(viewstate("EndorsmentLetter"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EndorsmentLetter") = Value
        End Set
    End Property
    Private Property EndorsmentDate() As String
        Get
            Return CType(viewstate("EndorsmentDate"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("EndorsmentDate") = Value
        End Set
    End Property
    Private Property InsSeqNo() As String
        Get
            Return CType(viewstate("InsSeqNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsSeqNo") = Value
        End Set
    End Property
    Private Property NewRateToCust() As Decimal
        Get
            Return CType(viewstate("NewRateToCust"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("NewRateToCust") = Value
        End Set
    End Property
    Private Property Discount() As Decimal
        Get
            Return CType(viewstate("Discount"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("Discount") = Value
        End Set
    End Property
    Private Property SumAdditionalCust() As Decimal
        Get
            Return CType(viewstate("SumAdditionalCust"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumAdditionalCust") = Value
        End Set
    End Property
    Private Property SumAdditionalInsCo() As Decimal
        Get
            Return CType(viewstate("SumAdditionalInsCo"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumAdditionalInsCo") = Value
        End Set
    End Property
    Private Property SumMain() As Decimal
        Get
            Return CType(viewstate("SumMain"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumMain") = Value
        End Set
    End Property
    Private Property SumOldSRCC() As Decimal
        Get
            Return CType(viewstate("SumOldSRCC"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumOldSRCC") = Value
        End Set
    End Property
    Private Property SumOldFlood() As Decimal
        Get
            Return CType(viewstate("SumOldFlood"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumOldFlood") = Value
        End Set
    End Property
    Private Property SumOldTPL() As Decimal
        Get
            Return CType(viewstate("SumOldTPL"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumOldTPL") = Value
        End Set
    End Property
    Private Property SumNewMain() As Decimal
        Get
            Return CType(viewstate("SumNewMain"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumNewMain") = Value
        End Set
    End Property

    Private Property TotalPremToCustomerRefund() As Decimal
        Get
            Return CType(viewstate("TotalPremToCustomerRefund"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("TotalPremToCustomerRefund") = Value
        End Set
    End Property
    Private Property SumAllRefund() As Decimal
        Get
            Return CType(viewstate("SumAllRefund"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumAllRefund") = Value
        End Set
    End Property
    Private Property SumAllRefundInsCo() As Decimal
        Get
            Return CType(viewstate("SumAllRefundInsCo"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumAllRefundInsCo") = Value
        End Set
    End Property
    Private Property SumAllNewPremiInsCo() As Decimal
        Get
            Return CType(viewstate("SumAllNewPremiInsCo"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumAllNewPremiInsCo") = Value
        End Set
    End Property
    Private Property SumAllNewPremi() As Decimal
        Get
            Return CType(viewstate("SumAllNewPremi"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumAllNewPremi") = Value
        End Set
    End Property
    Private Property SumAllAdditional() As Decimal
        Get
            Return CType(viewstate("SumAllAdditional"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumAllAdditional") = Value
        End Set
    End Property
    Private Property SumSRCC() As Decimal
        Get
            Return CType(viewstate("SumSRCC"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumSRCC") = Value
        End Set
    End Property
    Private Property SumTPL() As Decimal
        Get
            Return CType(viewstate("SumTPL"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumTPL") = Value
        End Set
    End Property
    Private Property SumFlood() As Decimal
        Get
            Return CType(viewstate("SumFlood"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumFlood") = Value
        End Set
    End Property
    Private Property SumAdminFee() As Decimal
        Get
            Return CType(viewstate("SumAdminFee"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumAdminFee") = Value
        End Set
    End Property
    Private Property SumMeteraiFee() As Decimal
        Get
            Return CType(viewstate("SumMeteraiFee"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumMeteraiFee") = Value
        End Set
    End Property
    Private Property SumTotal() As Decimal
        Get
            Return CType(viewstate("SumTotal"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumTotal") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Private Property Asset() As String
        Get
            Return CType(viewstate("Asset"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Asset") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return CType(viewstate("CustomerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property YearActive() As String
        Get
            Return CType(viewstate("YearActive"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("YearActive") = Value
        End Set
    End Property
    Private Property EndDate_ActiveYear() As Date
        Get
            Return CType(viewstate("Enddate_ActiveYear"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("EndDate_ActiveYear") = Value
        End Set
    End Property
    Private Property StartDate_ActiveYear() As Date
        Get
            Return CType(viewstate("StartDate_ActiveYear"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("StartDate_ActiveYear") = Value
        End Set
    End Property
    Private Property NewTPLPremium() As Decimal
        Get
            Return CType(viewstate("NewTPLPremium"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("NewTPLPremium") = Value
        End Set
    End Property
    Private Property InsCo_BasicNew() As Decimal
        Get
            Return CType(viewstate("InsCo_BasicNew"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_BasicNew") = Value
        End Set
    End Property
    Private Property InsCo_BasicRefund() As Decimal
        Get
            Return CType(viewstate("InsCo_BasicRefund"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_BasicRefund") = Value
        End Set
    End Property
    Private Property InsCo_BasicAdditional() As Decimal
        Get
            Return CType(viewstate("InsCo_BasicAdditional"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_BasicAdditional") = Value
        End Set
    End Property
    Private Property InsCo_SRCCNew() As Decimal
        Get
            Return CType(viewstate("InsCo_SRCCNew"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_SRCCNew") = Value
        End Set
    End Property
    Private Property InsCo_SRCCRefund() As Decimal
        Get
            Return CType(viewstate("InsCo_SRCCRefund"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_SRCCRefund") = Value
        End Set
    End Property
    Private Property InsCo_SRCCAdditional() As Decimal
        Get
            Return CType(viewstate("InsCo_SRCCAdditional"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_SRCCAdditional") = Value
        End Set
    End Property
    Private Property SumMainInsco() As Decimal
        Get
            Return CType(viewstate("SumMainInsco"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumMainInsco") = Value
        End Set
    End Property
    Private Property SumNewMainInsco() As Decimal
        Get
            Return CType(viewstate("SumNewMainInsco"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumNewMainInsco") = Value
        End Set
    End Property
    Private Property InsCo_FloodNew() As Decimal
        Get
            Return CType(viewstate("InsCo_FloodNew"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_FloodNew") = Value
        End Set
    End Property
    Private Property InsCo_FloodRefund() As Decimal
        Get
            Return CType(viewstate("InsCo_FloodRefund"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_FloodRefund") = Value
        End Set
    End Property
    Private Property InsCo_FloodAdditional() As Decimal
        Get
            Return CType(viewstate("InsCo_FloodAdditional"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("InsCo_FloodAdditional") = Value
        End Set
    End Property
    Private Property FlagFirstTime() As Boolean
        Get
            Return CType(viewstate("FlagFirstTime"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("FlagFirstTime") = Value
        End Set
    End Property
    Private Property InsuranceType() As String
        Get
            Return CType(viewstate("InsuranceType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceType") = Value
        End Set
    End Property
    Private Property ApplicationType() As String
        Get
            Return CType(viewstate("ApplicationType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationType") = Value
        End Set
    End Property
    Private Property NewUsed() As String
        Get
            Return CType(viewstate("AssetUsage"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetUsage") = Value
        End Set
    End Property
    Private Property UsageID() As String
        Get
            Return CType(viewstate("UsageID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("UsageID") = Value
        End Set
    End Property
    Private Property oData2() As DataTable
        Get
            Return CType(viewstate("oData2"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("oData2") = Value
        End Set
    End Property
    Private Property Result1() As DataTable
        Get
            Return CType(viewstate("Result1"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("Result1") = Value
        End Set
    End Property
    Private Property SumOldLoadingFee() As Decimal
        Get
            Return CType(viewstate("SumOldLoadingFee"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumOldLoadingFee") = Value
        End Set
    End Property
    Private Property SumLoadingFeeNew() As Decimal
        Get
            Return CType(viewstate("SumLoadingFeeNew"), Decimal)
        End Get
        Set(ByVal Value As Decimal)
            viewstate("SumLoadingFeeNew") = Value
        End Set
    End Property

#End Region

#Region "Page Load "
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.Discount = 0
            txtsuminsurednew.Text = "0"
            Me.Counter = 0
            Me.FlagFirstTime = True
            txtEndorsmentDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            txtDiscToCustNew.Text = "0"
            pnlChoice.Visible = True
            PnlRecalculateResult.Visible = False
            btnSave.Visible = False
            btnPrint.Visible = False
            GetCookies()
            GetData()
        End If
    End Sub
#End Region



#Region "Sub and Function"
    'sub ini untuk mengambil cookie
#Region "GetCookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("Endors")
        Me.CustomerName = cookie.Values("CustomerName")
        Me.ApplicationID = cookie.Values("ApplicationID")
        Me.AgreementNo = cookie.Values("AgreementNo")
        Me.Asset = cookie.Values("Asset")
        Me.BranchID = cookie.Values("BranchID")
        Me.AssetSeqNo = cookie.Values("AssetSeqNo")
        Me.InsSeqNo = cookie.Values("InsSeqNo")
    End Sub
#End Region
    'sub ini untuk menampilkan data detil cus ke form sekaligus data asuransi cust di datagrid
#Region "Get Data"
    Sub GetData()
        Dim oEndorsCN As New Parameter.InsEndorsment
        Dim oEndorsDetail As New Parameter.InsEndorsment
        Dim ocontroller As New InsEndorsmentController

        With oEndorsCN
            .AppID = Me.ApplicationID
            .BranchId = Me.BranchID
            .AssetSeqNo = CType(Me.AssetSeqNo, Integer)
            .InsSeqNo = CType(Me.InsSeqNo, Integer)
            .strConnection = GetConnectionString
        End With
        oEndorsCN = ocontroller.GetData(oEndorsCN)

        hynAgreementNo.Text = oEndorsCN.AgreementNo
        hynAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
        hynCustomerName.Text = oEndorsCN.Name
        Me.CustomerID = oEndorsCN.CustomerId
        hynCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(Me.CustomerID.Trim) & "')"
        lblAsset.Text = oEndorsCN.Description
        lblInsuranceCo.Text = oEndorsCN.MaskAssBranchID
        lblPolicyNo.Text = oEndorsCN.PolicyNumber
        lblInsuranceAssetType.Text = oEndorsCN.InsuranceType
        Me.InsuranceType = oEndorsCN.InsuranceType
        Me.ApplicationType = oEndorsCN.ApplicationType
        Me.UsageID = oEndorsCN.AssetUsage
        lblAssetUsage.Text = oEndorsCN.Usage
        lblAssetUsedNew.Text = oEndorsCN.UsedNew
        If oEndorsCN.UsedNew = "New" Then
            Me.NewUsed = "N"

        Else
            Me.NewUsed = "U"
        End If

        lblYear.Text = oEndorsCN.ManufacturingYear
        lblTenor.Text = oEndorsCN.Tenor

        lblSerialNo1.Text = oEndorsCN.SerialNo1
        lblSerialNo2.Text = oEndorsCN.SerialNo2
        lblInsuranceLength.Text = oEndorsCN.InsLength

        If oEndorsCN.StartDate.ToString = "" Then
            lblPeriod1.Text = ""
        Else
            lblPeriod1.Text = oEndorsCN.StartDate
        End If

        If oEndorsCN.EndDate.ToString = "" Then
            lblPeriod2.Text = ""
        Else
            lblPeriod2.Text = oEndorsCN.EndDate
        End If

        txtInsNotes.Text = oEndorsCN.InsNotes
        txtAccNotes.Text = oEndorsCN.AccNotes
        txtEndorsmentDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
        lblPrepaidAmount.Text = oEndorsCN.ContractPrepaidAmount.ToString
        lblMainPremiumByCust_C.Text = FormatNumber(CStr(oEndorsCN.MainPremiumToCust), 2)
        lblMainPremiumToInsCo_C.Text = FormatNumber(oEndorsCN.MainPremiumToInsCo, 2)
        lblPremiumAmountByCust_C.Text = FormatNumber(oEndorsCN.PremiumAmountByCust, 2)
        lblPremiumAmountToInsCo_C.Text = FormatNumber(oEndorsCN.PremiumAmountToInsCo, 2)
        lblMainPremiumByCust_N.Text = FormatNumber(CStr(oEndorsCN.MainPremiumToCust), 2)
        lblMainPremiumToInsCo_N.Text = FormatNumber(oEndorsCN.MainPremiumToInsCo, 2)
        lblPremiumAmountByCust_N.Text = FormatNumber(oEndorsCN.PremiumAmountByCust, 2)
        lblPremiumAmountToInsCo_N.Text = FormatNumber(oEndorsCN.PremiumAmountToInsCo, 2)
        lblOldSum.Text = FormatNumber(oEndorsCN.SumInsured, 2)
        txtsuminsurednew.Text = CStr(oEndorsCN.SumInsured)
        'Me.NewRateToCust = oEndorsCN.NewRateToCust
        Me.YearActive = oEndorsCN.YearActive

        '============ Detail =================================================
        With oEndorsDetail
            .AppID = Me.ApplicationID
            .BranchId = Me.BranchID
            .AssetSeqNo = CType(Me.AssetSeqNo, Integer)
            .InsSeqNo = CType(Me.InsSeqNo, Integer)
            .BusinessDate = Me.BusinessDate
            .BDEndorsDate = ConvertDate2(txtEndorsmentDate.Text)
            .strConnection = GetConnectionString
            context.Trace.Write("Me.BusinessDate = " & Me.BusinessDate)
            Context.Trace.Write("txtEndorsmentDate.text = " & txtEndorsmentDate.Text)
            Context.Trace.Write("Convertdate2(txtEndorsmentDate.text) = " & ConvertDate2(txtEndorsmentDate.Text))
        End With

        Try
            oEndorsDetail = ocontroller.GetDataInsDetail(oEndorsDetail)
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)
            LblErrMessages.Text = ex.Message
        End Try

        Dim DtInsDetail As New DataTable

        If Not oEndorsDetail Is Nothing Then
            DtInsDetail = oEndorsDetail.ListData
        Else
            Dim errmsg As String = "Data For this ApplicationID = '" & Me.ApplicationID & "' is not availabe !"
            Response.Write(errmsg)
        End If

        Me.ActiveEndorsDate = ConvertDate2(txtEndorsmentDate.Text)
        dtgInsDetail.DataSource = DtInsDetail.DefaultView
        dtgInsDetail.CurrentPageIndex = 0
        dtgInsDetail.DataBind()

        Dim i As Integer
        Dim oEndorsCoverage As New Parameter.InsEndorsment
        Dim oEndorsTPL As New Parameter.InsEndorsment
        Dim dtEntityCoverage As New DataTable
        Dim dtEntityTPL As New DataTable
        Dim lblcoverage As Label
        Dim lblTPL As Label
        Dim lblSRCC As Label
        For i = 0 To dtgInsDetail.Items.Count - 1
            cboCoverageNew = CType(dtgInsDetail.Items(i).FindControl("cboCoverageNew"), DropDownList)
            cboTPLAmountNew = CType(dtgInsDetail.Items(i).FindControl("cboTPLAmountNew"), DropDownList)

            'handle coverage
            oEndorsCoverage.strConnection = GetConnectionString()

            oEndorsCoverage = ocontroller.GetDataCbCoverage(oEndorsCoverage)

            dtEntityCoverage = oEndorsCoverage.ListData
            cboCoverageNew.DataSource = dtEntityCoverage
            cboCoverageNew.DataValueField = "ID"
            cboCoverageNew.DataTextField = "Description"
            cboCoverageNew.DataBind()

            'handle tpl
            oEndorsTPL.strConnection = GetConnectionString()
            oEndorsTPL.BranchId = Me.BranchID

            oEndorsTPL = ocontroller.GetDataCbTPL(oEndorsTPL)

            dtEntityTPL = oEndorsTPL.ListData
            cboTPLAmountNew.DataSource = dtEntityTPL
            cboTPLAmountNew.DataValueField = "TPLAmount"
            cboTPLAmountNew.DataTextField = "TPLAmount"
            cboTPLAmountNew.DataBind()
        Next

    End Sub
    'sub ini untuk menyimpan semua perubahan data yang dilakukan,melakukan perhitungan dan menyimpannya ke datatable
#End Region

#Region "GetInsured Coverage"
    Private Sub GetInsured_Coverage()
        PnlRecalculateResult.Visible = True

        Dim oEndorsGetInsured_Coverage As New Parameter.InsEndorsment
        Dim oData1 As New DataTable
        Dim i As Integer
      
        Dim objrow As DataRow

        oData1.Columns.Add("CoverageType", GetType(String))
        oData1.Columns.Add("OldCoverageType", GetType(String))
        oData1.Columns.Add("Year", GetType(Integer))

        oData1.Columns.Add("NewTPL", GetType(Decimal))
        oData1.Columns.Add("OldTPL", GetType(Decimal))
        oData1.Columns.Add("NewTPLAmount", GetType(Decimal))

        oData1.Columns.Add("NewSRCC", GetType(Boolean))
        oData1.Columns.Add("OldSRCC", GetType(String))
        oData1.Columns.Add("NewFlood", GetType(Boolean))
        oData1.Columns.Add("OldFlood", GetType(String))

        oData1.Columns.Add("NewSumInsured", GetType(Decimal))
        oData1.Columns.Add("EndorsmentDate", GetType(Date))
        oData1.Columns.Add("YearNumRate", GetType(Integer))

        Dim strCoverageNewTemp As String = ""
        Dim j As Integer

        j = 0

        For i = 0 To dtgInsDetail.Items.Count - 1
            cboCoverageNew = CType(dtgInsDetail.Items(i).FindControl("cboCoverageNew"), DropDownList)
            cboTPLAmountNew = CType(dtgInsDetail.Items(i).FindControl("cboTPLAmountNew"), DropDownList)
            cboSRCCNew = CType(dtgInsDetail.Items(i).FindControl("cboSRCCNew"), DropDownList)
            cboFloodNew = CType(dtgInsDetail.Items(i).FindControl("cboFloodNew"), DropDownList)

            If cboCoverageNew.Enabled = True And cboTPLAmountNew.Enabled = True And cboSRCCNew.Enabled = True And cboFloodNew.Enabled = True Then
                objrow = oData1.NewRow

                'Get CoverageType
                objrow("CoverageType") = CType(dtgInsDetail.Items(i).FindControl("cboCoverageNew"), DropDownList).SelectedItem.Value.Trim
                objrow("OldCoverageType") = CType(dtgInsDetail.Items(i).FindControl("lblCoverageCurrent"), Label).Text.Trim
                objrow("Year") = CType(dtgInsDetail.Items(i).FindControl("lblInsYear"), Label).Text.Trim

                If objrow("CoverageType").ToString = strCoverageNewTemp Then
                    j += 1
                Else
                    strCoverageNewTemp = objrow("CoverageType").ToString
                    j = 1
                End If

                objrow("YearNumRate") = j

                'Get TPL Premium
                cboTPLAmountNew = CType(dtgInsDetail.Items(i).FindControl("cboTPLAmountNew"), DropDownList)

                Me.NewTPLPremium = 0
                Me.NewTPLPremium = Me.NewTPLPremium + CDec(cboTPLAmountNew.SelectedItem.Value.Trim)
                objrow("NewTPL") = Me.NewTPLPremium 'ini ngambil amount di value combo
                objrow("OldTPL") = CDec(CType(dtgInsDetail.Items(i).FindControl("lblTPLAmountCurr"), Label).Text.Trim)
                objrow("NewTPLAmount") = CDec(cboTPLAmountNew.SelectedItem.Text.Trim) 'ini ngambil amountnya

                'Get SRCC
                objrow("NewSRCC") = CType(CType(dtgInsDetail.Items(i).FindControl("cboSRCCNew"), DropDownList).SelectedItem.Value.Trim, Boolean)
                objrow("OldSRCC") = CType(dtgInsDetail.Items(i).FindControl("lblSRCCCurrent"), Label).Text.Trim

                'Get Flood
                objrow("NewFlood") = CType(CType(dtgInsDetail.Items(i).FindControl("cboFloodNew"), DropDownList).SelectedItem.Value.Trim, Boolean)
                objrow("OldFlood") = CType(dtgInsDetail.Items(i).FindControl("lblFloodCurrent"), Label).Text.Trim
                objrow("NewSumInsured") = 0
                objrow("EndorsmentDate") = Me.BusinessDate
                oData1.Rows.Add(objrow)

            End If
        Next

        For i = 0 To oData1.Rows.Count - 1
            oData1.Rows(i).Item("NewSumInsured") = CDec(txtsuminsurednew.Text.Trim)
            oData1.Rows(i).Item("EndorsmentDate") = ConvertDate2(txtEndorsmentDate.Text)
        Next

        Me.NewSumInsured = CType(txtsuminsurednew.Text.Trim, Decimal)
        Me.EndorsmentDate = txtEndorsmentDate.Text.ToString.Trim
        txtEndorsmentDate.ReadOnly = True
        Me.oData2 = oData1

        With oCustomClass
            .strConnection = GetConnectionString
            .ApplicationType = Me.ApplicationType
            .InsuranceType = Me.InsuranceType
            .BranchId = Me.BranchID
            .AppID = Me.ApplicationID
            .AssetSeqNo = CType(Me.AssetSeqNo, Integer)
            .InsSeqNo = CType(Me.InsSeqNo, Integer)
            .AssetUsage = Me.UsageID
            .UsedNew = Me.NewUsed
        End With

        Dim result As New DataTable

        Try
            If (oData1.Rows.Count > 0) Then
                oCustomClass = oController.GetInsured_Coverage(oCustomClass, oData1)
                result = oCustomClass.ListData
            End If 
        Catch ex As Exception
            Response.Write(ex.Message + ex.StackTrace)
            btnSave.Visible = False
            btnPrint.Visible = False
            Exit Sub

        End Try
 

        Me.SumAdminFee = 0
        Me.SumFlood = 0
        Me.SumMain = 0
        Me.SumMeteraiFee = 0
        Me.SumSRCC = 0
        Me.SumTotal = 0
        Me.SumTPL = 0
        Me.SumNewMain = 0
        Me.SumNewMainInsco = 0
        Me.SumMainInsco = 0
        Me.SumOldFlood = 0
        Me.SumOldSRCC = 0
        Me.SumOldTPL = 0
        Me.SumAllRefundInsCo = 0
        Me.SumOldLoadingFee = 0
        Me.SumLoadingFeeNew = 0

        For i = 0 To result.Rows.Count - 1

            result.Rows(i).Item("AddPremiCust") = CType(result.Rows(i).Item("NewPremiCust"), Decimal) - CType(result.Rows(i).Item("OldPremiCust"), Decimal)
            result.Rows(i).Item("AddPremiInsCo") = CType(result.Rows(i).Item("NewPremiInsco"), Decimal) - CType(result.Rows(i).Item("OldPremiInsco"), Decimal)
            result.Rows(i).Item("NewSumInsured") = CType(Me.NewSumInsured, Decimal)

            Me.Counter = Me.Counter + 1


            If CType(result.Rows(i).Item("Year"), Integer) = CType(result.Rows(i).Item("ActiveYearNum"), Integer) Then
                Me.TotalPrevPremitocust = CType(result.Rows(i).Item("PrevMainToCust"), Decimal) + CType(result.Rows(i).Item("SRCCPremiumToCust"), Decimal) + CType(result.Rows(i).Item("FloodPremiumToCust"), Decimal) + CType(result.Rows(i).Item("TPLPremiumToCust"), Decimal)
                If CDec(Me.TotalPrevPremitocust) - (CType(result.Rows(i).Item("DiscToCust"), Decimal)) > (CType(result.Rows(i).Item("PaidAmtByCust"), Decimal)) Then
                    btnSave.Visible = False
                    LblErrMessages.Text = ""
                    LblErrMessages.Text = "Premi untuk Tahun Belum dibayar, harap input Pembayaran terlebih dahulu"
                    Exit Sub
                End If
            End If

            If CType(result.Rows(i).Item("ispaid"), String) = "True" Or CType(result.Rows(i).Item("Year"), Integer) <= CType(result.Rows(i).Item("ActiveYearNum"), Integer) Then
                Me.SumMain = Me.SumMain + CType(result.Rows(i).Item("OldPremiCust"), Decimal) ''total refund premi
                Me.SumNewMain = Me.SumNewMain + CType(result.Rows(i).Item("NewPremiCust"), Decimal)
                Me.SumFlood = Me.SumFlood + CType(result.Rows(i).Item("NewFloodCust"), Decimal)
                Me.SumTPL = Me.SumTPL + CType(result.Rows(i).Item("NewTPLCust"), Decimal)
                Me.SumSRCC = Me.SumSRCC + CType(result.Rows(i).Item("NewSRCCCust"), Decimal)
                Me.SumOldSRCC = Me.SumOldSRCC + CType(result.Rows(i).Item("OldSRCCCust"), Decimal)
                Me.SumOldFlood = Me.SumOldFlood + CType(result.Rows(i).Item("OldFloodCust"), Decimal)
                Me.SumOldTPL = Me.SumOldTPL + CType(result.Rows(i).Item("OldTPLCust"), Decimal)
                Me.SumOldLoadingFee = Me.SumOldLoadingFee + CType(result.Rows(i).Item("OldLoadingFeeToCust"), Decimal)
                Me.SumLoadingFeeNew = Me.SumLoadingFeeNew + CType(result.Rows(i).Item("NewLoadingFeeToInsco"), Decimal)
            End If

            Me.SumMainInsco = Me.SumMainInsco + CType(result.Rows(i).Item("OldPremiInsco"), Decimal)
            Me.SumNewMainInsco = Me.SumNewMainInsco + CType(result.Rows(i).Item("NewPremiInsco"), Decimal)
            Me.SumAllNewPremiInsCo = Me.SumAllNewPremiInsCo + CType(result.Rows(i).Item("NewPremiInsco"), Decimal) + CType(result.Rows(i).Item("NewSRCCInsco"), Decimal) + CType(result.Rows(i).Item("NewFloodInsco"), Decimal) + CType(result.Rows(i).Item("NewTPLInsCo"), Decimal) + +CType(result.Rows(i).Item("NewLoadingFeeToInsco"), Decimal)
            Me.SumAllRefundInsCo = Me.SumAllRefundInsCo + CType(result.Rows(i).Item("OldPremiInsco"), Decimal) + CType(result.Rows(i).Item("OldSRCCInsco"), Decimal) + CType(result.Rows(i).Item("OldFloodInsco"), Decimal) + CType(result.Rows(i).Item("OldTPLInsco"), Decimal) + CType(result.Rows(i).Item("OldLoadingFeeToInsco"), Decimal)

        Next

        Me.SumAdditionalCust = (Me.SumNewMain + Me.SumSRCC + Me.SumTPL + Me.SumFlood) - (Me.SumMain + Me.SumOldSRCC + Me.SumOldTPL + Me.SumOldFlood)
        Me.SumAdditionalInsCo = Me.SumAllNewPremiInsCo - Me.SumAllRefundInsCo

        Dim oEndorsRecalculate As New Parameter.InsEndorsment

        Me.Result1 = result

        dtgResult.Visible = True
        dtgResult.DataSource = Me.Result1.DefaultView
        dtgResult.DataBind()


        DgridResultPremToCust.DataSource = Me.Result1.DefaultView
        DgridResultPremToCust.DataBind()

        DgridResultPremToInsco.DataSource = Me.Result1.DefaultView
        DgridResultPremToInsco.DataBind()

        DgridResultNewPremToInsco.DataSource = Me.Result1.DefaultView
        DgridResultNewPremToInsco.DataBind()

        DgridResultNewPremToCust.DataSource = Me.Result1.DefaultView
        DgridResultNewPremToCust.DataBind()

        'sekrng ambil meteraifee dan adminfee
        With oEndorsRecalculate
            .BranchId = Me.BranchID
            .AppID = Me.ApplicationID
            .strConnection = GetConnectionString
        End With

        oEndorsRecalculate = oController.GetEndorsCalculationResult(oEndorsRecalculate)

        With oEndorsRecalculate.ListData.Rows(0)
            Me.SumAdminFee = Me.SumAdminFee + CType(.Item("AdminFee"), Decimal)
            Me.SumMeteraiFee = Me.SumMeteraiFee + CType(.Item("MeteraiFee"), Decimal)
        End With
    End Sub
#End Region

#Region "Display"
    'sub ini untuk menampilkan semua data yang dihitung dr datatable ke layar form dan menghitung premi baru setelah dikurangi discount
    Private Sub Display()
        btnSave.Visible = True
        btnPrint.Visible = True
        Me.Discount = CDec((txtDiscToCustNew.Text))
        Me.SumAllRefund = 0
        Me.SumAllNewPremi = 0
        lblBasicNew.Text = FormatNumber(Me.SumNewMain, 2)

        lblBasicRefund.Text = FormatNumber(Me.SumMain, 2)
        lblBasicAdditional.Text = FormatNumber((Me.SumNewMain - Me.SumMain), 2)

        lblSRCCNew.Text = FormatNumber(Me.SumSRCC, 2)
        lblSRCCRefund.Text = FormatNumber(Me.SumOldSRCC, 2)
        lblSRCCAdditional.Text = FormatNumber((Me.SumSRCC - Me.SumOldSRCC), 2)

        lblFloodNew.Text = FormatNumber(Me.SumFlood, 2)
        lblFloodRefund.Text = FormatNumber(Me.SumOldFlood, 2)
        lblFloodAdditional.Text = FormatNumber((Me.SumFlood - Me.SumOldFlood), 2)

        lblTPLNew.Text = FormatNumber(Me.SumTPL, 2)
        lblTPLRefund.Text = FormatNumber(Me.SumOldTPL, 2)
        lblTPLAdditional.Text = FormatNumber((Me.SumTPL - Me.SumOldTPL), 2)

        lblLoadingFeeNew.Text = FormatNumber(Me.SumLoadingFeeNew, 2)
        lblRefundLoadingFee.Text = FormatNumber(Me.SumOldLoadingFee, 2)
        lblLoadingFeeAdditional.Text = FormatNumber(Me.SumLoadingFeeNew - Me.SumOldLoadingFee, 2)

        lblAdminFeeNew.Text = FormatNumber(Me.SumAdminFee, 2)
        lblAdminFeeRefund.Text = "0"
        lblAdminFeeAdditional.Text = lblAdminFeeNew.Text.Trim

        lblMeteraiFeeNew.Text = FormatNumber(Me.SumMeteraiFee, 2)
        lblMeteraiFeeRefund.Text = "0"
        lblMeteraiFeeAdditional.Text = lblMeteraiFeeNew.Text.Trim

        lblTotalPremiumRefund.Text = FormatNumber((CDec(lblBasicRefund.Text.Trim) + CDec(lblSRCCRefund.Text.Trim) + CDec(lblFloodRefund.Text.Trim) + CDec(lblTPLRefund.Text.Trim) + CDec(lblAdminFeeRefund.Text.Trim) + CDec(lblMeteraiFeeRefund.Text.Trim) + CDec(lblRefundLoadingFee.Text.Trim)).ToString, 2)
        lblTotalPremiumNew.Text = FormatNumber((CDec(lblBasicNew.Text.Trim) + CDec(lblSRCCNew.Text.Trim) + CDec(lblFloodNew.Text.Trim) + CDec(lblTPLNew.Text.Trim) + CDec(lblAdminFeeNew.Text.Trim) + CDec(lblMeteraiFeeNew.Text.Trim) + CDec(lblLoadingFeeNew.Text.Trim)).ToString, 2)
        lblTotalPremiumAdditional.Text = FormatNumber((CDec(lblBasicAdditional.Text.Trim) + CDec(lblSRCCAdditional.Text.Trim) + CDec(lblFloodAdditional.Text.Trim) + CDec(lblTPLAdditional.Text.Trim) + CDec(lblAdminFeeAdditional.Text.Trim) + CDec(lblMeteraiFeeAdditional.Text.Trim) + CDec(lblLoadingFeeAdditional.Text.Trim)).ToString, 2)

        Me.SumTotal = CDec(lblTotalPremiumNew.Text.Trim)

        If CDbl((txtDiscToCustNew.Text)) > CDbl(lblTotalPremiumNew.Text.Trim) Then
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Discount Harus lebih kecil dari total Premi Baru"
            Exit Sub
        End If

        lblTotalPremiToCustRefund.Text = FormatNumber(lblTotalPremiumRefund.Text.Trim, 2)
        lblTotalPremiToCustNew.Text = FormatNumber((CDec(lblTotalPremiumNew.Text.Trim) - CDec(txtDiscToCustNew.Text.Trim)).ToString, 2)
        lblTotalPremiToCustAdditional.Text = FormatNumber((CDec(lblTotalPremiToCustNew.Text.Trim) - CDec(lblTotalPremiToCustRefund.Text.Trim)), 2)

        lblTotalPremiToInsCoRefund.Text = FormatNumber(Me.SumAllRefundInsCo, 2)
        lblTotalPremiToInsCONew.Text = FormatNumber(Me.SumAllNewPremiInsCo, 2)
        lblTotalPremiToInsCoAdditional.Text = FormatNumber(Me.SumAllNewPremiInsCo - Me.SumAllRefundInsCo, 2)

        Me.TotalPremToCustomerRefund = CDec(lblTotalPremiToCustRefund.Text)
    End Sub
#End Region

#Region "GetStatus"

    'fungsi ini untuk menentukan status dari srcc dan flood dari cust setelah adanya perubahan
    Protected Function GetStatus(ByVal status As Boolean) As String
        If CType(status, String) = "True" Then
            Return "Yes"
        Else
            Return "No"
        End If
    End Function
#End Region

#End Region

#Region "Event Handlers"
    Private Sub DgridResultPremToCust_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DgridResultPremToCust.ItemDataBound

    End Sub
    Private Sub dtgInsDetail_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInsDetail.ItemDataBound
 
        Dim lblStartDate As Label
        Dim lblEndDate As Label
        If e.Item.ItemIndex >= 0 Then
            cboCoverageNew = CType(e.Item.FindControl("cboCoverageNew"), DropDownList)
            cboTPLAmountNew = CType(e.Item.FindControl("cboTPLAmountNew"), DropDownList)
            cboSRCCNew = CType(e.Item.FindControl("cboSRCCNew"), DropDownList)
            cboFloodNew = CType(e.Item.FindControl("cboFloodNew"), DropDownList)
            lblStartDate = CType(e.Item.FindControl("lblStartDate"), Label)
            lblEndDate = CType(e.Item.FindControl("lblEndDate"), Label)

            If ConvertDate2(lblEndDate.Text.Trim) < ConvertDate2(txtEndorsmentDate.Text) Then
                cboCoverageNew.Enabled = False
                cboTPLAmountNew.Enabled = False
                cboSRCCNew.Enabled = False
                cboFloodNew.Enabled = False
            End If
        End If
    End Sub
    ' KETIKA TOMBOL CANCEL DICLICK !!
#Region "Ketika Tombol Cancel di click !"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.FlagFirstTime = True
        Response.Redirect("Endorsment.aspx")

    End Sub
#End Region
    ' KETIKA TOMBOL RECALCULATE DI CLICK ... PROSES ADA DI SINI !!
#Region "Ketika Tombol Recalculate di click !"

    Private Sub btnRecalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecalculate.Click
        If ConvertDate2(txtEndorsmentDate.Text) < (Me.BusinessDate) Then
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Tanggal Endorsment harus >= Hari ini"
            Exit Sub
        End If
        If ConvertDate2(txtEndorsmentDate.Text) <> Me.ActiveEndorsDate Then
            Me.ChangedEndorsDate = ConvertDate2(txtEndorsmentDate.Text)
            GetData()
            txtEndorsmentDate.Text = Me.ChangedEndorsDate.ToString("dd/MM/yyyy")
            Me.ActiveEndorsDate = Me.ChangedEndorsDate
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Tanggal Endorsment Sudah Berubah, Harap pilih Jenis Cover srcc,flood dan TPL Lagi"
            Exit Sub
        End If

        LblErrMessages.Text = ""

        If Me.FlagFirstTime = True Then
            Me.Discount = 0
            GetInsured_Coverage()
            Display()
            PnlRecalculateResult.Visible = True
            pnlChoice.Visible = False
            txtsuminsurednew.Enabled = False
        Else
            txtsuminsurednew.Enabled = False
            Display()
            PnlRecalculateResult.Visible = True
            pnlChoice.Visible = False
        End If

        Me.FlagFirstTime = False
    End Sub
#End Region
    ' KETIKA TOMBOL PRINT DI CLICK !!!
#Region "Ketika Tombol Print Di Click !"

    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim cookie As HttpCookie = Request.Cookies("EndorsPrint")
        If CDec(lblTotalPremiToCustAdditional.Text.Trim) < 0 Then
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Premi Tambahan Untuk Endorsment Harus >= 0 "
            LblErrMessages.Visible = True
            Exit Sub
        Else
            Me.SumAllRefund = CDec(lblTotalPremiToCustRefund.Text)
            Me.SumAllNewPremi = CDec(lblTotalPremiToCustNew.Text)
            Me.SumAllAdditional = CDec(lblTotalPremiToCustAdditional.Text)
        End If
        Me.EndorsmentDate = txtEndorsmentDate.Text.ToString.Trim
        If Not cookie Is Nothing Then

            cookie.Values("ApplicationID") = Me.ApplicationID
            cookie.Values("RefundPremium") = CStr(Me.SumAllRefund)
            cookie.Values("NewPremium") = CStr(Me.SumAllNewPremi)
            cookie.Values("AdditionalPremium") = CStr(Me.SumAllAdditional)
            cookie.Values("BranchID") = Me.BranchID
            cookie.Values("AssetSeqNo") = Me.AssetSeqNo
            cookie.Values("InsSeqNo") = Me.InsSeqNo
            cookie.Values("EndorsmentDate") = Me.EndorsmentDate
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("EndorsPrint")

            cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
            cookieNew.Values.Add("RefundPremium", CStr(Me.SumAllRefund))
            cookieNew.Values.Add("NewPremium", CStr(Me.SumAllNewPremi))
            cookieNew.Values.Add("AdditionalPremium", CStr(Me.SumAllAdditional))
            cookieNew.Values.Add("BranchID", Me.BranchID)
            cookieNew.Values.Add("AssetSeqNo", Me.AssetSeqNo)
            cookieNew.Values.Add("InsSeqNo", Me.InsSeqNo)
            cookieNew.Values.Add("EndorsmentDate", Me.EndorsmentDate)
            Response.AppendCookie(cookieNew)
        End If
        Me.FlagFirstTime = True
        Response.Redirect("EndorsmentPrintViewer.aspx")
    End Sub
#End Region
    ' KETIKA TOMBOL SAVE DI CLICK !!!
#Region "Ketika Tombol Save diclick !"


    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        pnlChoice.Visible = False
        PnlRecalculateResult.Visible = True

        If ConvertDate2(txtEndorsmentDate.Text) <> (Me.BusinessDate) Then
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Tgl Endorsment Harus Sama Tgl Hari ini"
            Exit Sub
        End If
        If Me.Discount <> CDec((txtDiscToCustNew.Text)) Then
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Jumlah Discount Berubah, Harap Klik Tombol recalculate"
            Exit Sub
        End If
        If ConvertDate2(txtEndorsmentDate.Text) < (Me.BusinessDate) Then
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Tanggal Endorsment Harus >= Tgl Hari ini"
            Exit Sub
        End If
        LblErrMessages.Text = ""
        Display()
        If CDec(lblTotalPremiToInsCoAdditional.Text) < 0 Then
            btnSave.Visible = False
            btnPrint.Visible = False
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Tambahan Premi Asuransi untuk Endorsment harus >= 0, Harap Periksa Rate Premi lama"
            Exit Sub
        End If
        If CDec(lblTotalPremiToCustAdditional.Text.Trim) < 0 Then
            btnSave.Visible = False
            btnPrint.Visible = False
            LblErrMessages.Text = ""
            LblErrMessages.Text = "Tambahan Premi untuk Endorsment Harus >= 0 "
            Exit Sub
        Else
            Me.SumAllRefund = CDec(lblTotalPremiToCustRefund.Text)
            Me.SumAllNewPremi = CDec(lblTotalPremiToCustNew.Text)
            Me.SumAllAdditional = CDec(lblTotalPremiToCustAdditional.Text)
            btnSave.Visible = True
            btnPrint.Visible = True
        End If

        Me.SumAdditionalCust = CDec(lblTotalPremiToCustAdditional.Text)

        With oCustomClass
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .AppID = Me.ApplicationID
            .InsSeqNo = CType(Me.InsSeqNo, Integer)
            .AssetSeqNo = CType(Me.AssetSeqNo, Integer)
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid

        End With
        Try

            'insert ke tabel insuranceassethistory, insuranceassetdetailhistory dan mailtransaction
            'update tabel insuranceasset dan insuranceassetdetail
            With oCustomClass
                .LoginId = Me.Loginid
                .strConnection = GetConnectionString()
                .ApplicationType = Me.ApplicationType
                .InsuranceType = Me.InsuranceType
                .BranchId = Me.BranchID
                .AppID = Me.ApplicationID
                .AssetSeqNo = CType(Me.AssetSeqNo, Integer)
                .InsSeqNo = CType(Me.InsSeqNo, Integer)
                .AssetUsage = Me.UsageID
                .UsedNew = Me.NewUsed
                .Discount = CDec(txtDiscToCustNew.Text.Trim)

                If CDbl(txtDiscToCustNew.Text.Trim) >= 0 Then
                    oCustomClass.spName = "spProcessInsuranceDetailAllocationDiscountYearNum"
                ElseIf CDbl(txtDiscToCustNew.Text.Trim) < 0 Then
                    oCustomClass.spName = "spProcessInsuranceDetailAllocationDiscountNegativeYearNum"
                End If

                .AdditionalCust = Me.SumAdditionalCust
                .AdditionalInsco = Me.SumAdditionalInsCo
                .BusinessDate = Me.BusinessDate
                .InsNotes = txtInsNotes.Text.Trim
                .AccNotes = txtAccNotes.Text.Trim
                .CoyID = Me.SesCompanyID
                .RefundToCust = Me.TotalPremToCustomerRefund  'RefundToCust = all Refund
                .RefundToInsCo = Me.SumAllRefundInsCo
            End With

            oCustomClass = oController.SaveData(oCustomClass, Me.Result1)

            Me.EndorsmentLetter = oCustomClass.EndorsmentLetter
            Me.EndorsmentDate = txtEndorsmentDate.Text.ToString.Trim

            If Me.EndorsmentLetter = "" Then
                LblErrMessages.Text = "Proses Endorsment Gagal"
                LblErrMessages.Visible = True
                Exit Sub

            End If

            Dim cookie As HttpCookie = Request.Cookies("Endors")

            If Not cookie Is Nothing Then
                cookie.Values("ApplicationID") = Me.ApplicationID
                cookie.Values("RefundPremium") = CStr(Me.SumAllRefund)
                cookie.Values("NewPremium") = CStr(Me.SumAllNewPremi)
                cookie.Values("AdditionalPremium") = CStr(Me.SumAllAdditional)
                cookie.Values("BranchID") = Me.BranchID
                cookie.Values("AssetSeqNo") = Me.AssetSeqNo
                cookie.Values("InsSeqNo") = Me.InsSeqNo
                cookie.Values("EndorsmentDate") = Me.EndorsmentDate
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("Endors")
                cookieNew.Values.Add("ApplicationID", Me.ApplicationID)
                cookieNew.Values.Add("RefundPremium", CStr(Me.SumAllRefund))
                cookieNew.Values.Add("NewPremium", CStr(Me.SumAllNewPremi))
                cookieNew.Values.Add("AdditionalPremium", CStr(Me.SumAllAdditional))
                cookieNew.Values.Add("BranchID", Me.BranchID)
                cookieNew.Values.Add("AssetSeqNo", Me.AssetSeqNo)
                cookieNew.Values.Add("InsSeqNo", Me.InsSeqNo)
                cookieNew.Values.Add("EndorsmentDate", Me.EndorsmentDate)
                Response.AppendCookie(cookieNew)
            End If

            Me.FlagFirstTime = True
            Response.Redirect("EndorsmentCalculateViewer.aspx")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
#End Region

#End Region


End Class