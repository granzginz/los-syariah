﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.Controller
#End Region

Public Class ViewAPInsurance
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property AccountPayableNo() As String
        Get
            Return (CType(Viewstate("AccountPayableNo"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("AccountPayableNo") = Value
        End Set
    End Property

    Public Property Style() As String
        Get
            Return CType(viewstate("Style"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Style") = Value
        End Set
    End Property
    Public Property Customerid() As String
        Get
            Return CType(viewstate("Customerid"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Customerid") = Value
        End Set
    End Property

    Public Property InsuranceID() As String
        Get
            Return CType(viewstate("InsuranceID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceID") = Value
        End Set
    End Property

    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
#End Region

    Private oController As New InsuranceAssetDetailController
    Private oCustomClass As New Parameter.InsuranceAssetDetail


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FormID = "VIEWAPINSUR"
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.ApplicationID = Request.QueryString("ApplicationID")
                Me.BranchID = Request.QueryString("Branchid")
                Me.Style = Request.QueryString("Style")
                DoBind()
            End If
        End If
    End Sub

    Sub DoBind()
        Dim dtEntity As New DataTable
        Dim hypID As HyperLink
        oCustomClass.ApplicationID = Me.ApplicationID
        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.WhereCond = " Where Dbo.Agreement.ApplicationID = '" & Me.ApplicationID & "' and dbo.Agreement.BranchID = '" & Me.BranchID & "'"
        oCustomClass.PageSource = Maxiloan.General.CommonVariableHelper.PAGE_SOURCE_INSURANCE_DETAIL_INFORMATION

        oCustomClass = oController.GetViewInsuranceDetail(oCustomClass)
        With oCustomClass
            lblAPNo.Text = .AccountPayableNo
            HYApplicationID.Text = .ApplicationID
            Me.ApplicationID = .ApplicationID
            HyAgreementNo.Text = .AgreementNo
            HYCustomerName.Text = .CustomerName
            Me.Customerid = .CustomerID
            LblAssetDescription.Text = .AssetMasterDescription
            '===========================================================================
        End With

        oCustomClass.strConnection = GetConnectionString
        oCustomClass.BranchId = Me.BranchID
        oCustomClass.WhereCond = " Where dbo.InsuranceAsset.BranchId = '" & Me.BranchID & "' And dbo.InsuranceAsset.ApplicationID = '" & Me.ApplicationID & "' "
        oCustomClass.PageSource = Maxiloan.General.CommonVariableHelper.PAGE_SOURCE_ASSET_INSURANCE_DETAIL_INFORMATION


        Dim oCont As New InsuranceAssetDetailController
        oCustomClass = oCont.GetViewInsuranceDetail(oCustomClass)
        If Not oCustomClass Is Nothing Then
            With oCustomClass
                HyInsco.Text = .InsuranceComBranchName
                Me.InsuranceID = .InsuranceComBranchID
                LblSumInsured.Text = FormatNumber(.SumInsured, 2)
                LblInsuredByDescr.Text = .InsuredByDescr
                LblPaidByDescr.Text = .PaidByDescr
                If .Coverperiod = "FT" Then
                    lblCoverPeriod.Text = "Full Tenor"
                ElseIf .Coverperiod = "AN" Then
                    lblCoverPeriod.Text = "Annually"
                End If
                lblsdate.Text = .StartDate.ToString("dd/MM/yyyy")
                lblEDate.Text = .EndDate.ToString("dd/MM/yyyy")
                lblPolicyNo.Text = .PolicyNumber
                lblPolicyDate.Text = .PolicyReceiveDate.ToString("dd/MM/yyyy")
                lblAccNotes.Text = .AccNotes
                lblInsuranceNotes.Text = .InsNotes
                lblPRemiToCust.Text = FormatNumber(.PremiumAmountByCust, 2)
                lblMainPremium.Text = FormatNumber(.MainPremiumToInsco, 2)
                lblLoadingFee.Text = FormatNumber(.LoadingFeeToInsco, 2)
                lblSRCCPremium.Text = FormatNumber(.RSCCToInsco, 2)
                lblFloodPremium.Text = FormatNumber(.FloodToInsco, 2)
                lblTPLPremium.Text = FormatNumber(.TPLToInsco, 2)
                lblAdminFee.Text = FormatNumber(.AdminFee, 2)
                lblMeteraiFee.Text = FormatNumber(.MeteraiFee, 2)
                lblTotalPremium.Text = FormatNumber(.TotalPremiumToInsco, 2)
            End With

        End If

        '=================================================================================        
        Dim ocustomclassgrid As New Parameter.SPPA
        Dim ocontrollergrd As New SPPAController
        Dim CmdWhere As String = " Where dbo.InsuranceAssetDetail.BranchId ='" & Me.BranchID & "' And dbo.InsuranceAssetDetail.ApplicationID= '" & Me.ApplicationID & "' "
        With ocustomclassgrid
            .WhereCond = CmdWhere
            .PageSource = General.CommonVariableHelper.PAGE_SOURCE_GRID_TENOR_VIEW_INSURANCE_ASSET_DETAIL
            .strConnection = GetConnectionString
        End With
        Try
            ocustomclassgrid = ocontrollergrd.GetListCreateSPPA(ocustomclassgrid)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        If Not ocustomclassgrid Is Nothing Then
            dtEntity = ocustomclassgrid.ListData
        End If

        DtgList.DataSource = dtEntity
        DtgList.DataBind()
    End Sub

End Class