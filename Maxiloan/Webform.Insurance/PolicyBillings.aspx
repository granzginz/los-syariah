﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PolicyBillings.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.PolicyBillings" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagName="UcNotaAsuransi" TagPrefix="uc1" Src="../webform.UserController/UcNotaAsuransi.ascx" %>
<%@ Register  TagName="ucDateCE" TagPrefix="uc1"Src="../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>policybillings</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
      <script src="../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
       // var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
      //  var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function OpenWinAgreementNo(pApplicationId, pStyle) {

            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App +'/Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?ApplicationId=' + pApplicationId + '&style=' + pStyle, 'AgreementNo', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinCustomer(pID, pStyle) {

            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App +'/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinAsset(pID, pStyle) {

            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App +'/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewAssetData.aspx?ApplicationID=' + pID + '&style=' + pStyle, 'Customer', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
    <asp:Panel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        TERIMA POLIS
                    </h3>
                </div>
            </div>
            <asp:Panel ID="Panel1" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="hplAgreement" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="hplCustomerName" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkName" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nama Asset</label>
                        <asp:HyperLink ID="lnkAsset" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkAsset" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Penggunaan Asset</label>
                        <asp:Label ID="lblUsage" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkUsage" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Tahun Produksi</label>
                        <asp:Label ID="lblYear" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkYear" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Alamat</label>
                        <asp:Label ID="LblAddress" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="ChkAddress" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgAttribute" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" Width="100%">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn DataField="AssetName" HeaderText="ASSET ATRIBUT"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="AttributeContent"></asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkAttribute" Checked="true" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Periode Asuransi</label>
                        <asp:Label ID="lblPeriode" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkPeriode" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Jangka Waktu</label>
                        <asp:Label ID="lblLamaAsuransi" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkLamaAsuransi" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Accessories</label>
                        <asp:Label ID="lblAccessories" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkAccessories" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgAsset" runat="server" AutoGenerateColumns="False" CssClass="grid_general"
                                BorderStyle="None" BorderWidth="0" Width="100%">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:BoundColumn Visible="false"  DataField="YearNum" HeaderText="TAHUN KE"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="false"  DataField="CoverageType" HeaderText="JENIS COVER"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="false"  DataField="TPLAmountToCust" HeaderText="TPL"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="false"  DataField="RSCC" HeaderText="SRCC"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="false"  DataField="Flood" HeaderText="FLOOD"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="false"  DataField="Loading" HeaderText="LOADING FEE"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="YEAR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDYearNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JENIS COVER">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDCoverage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI UTAMA">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToInsCo", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TPL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING FEE">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFeeToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA PASS">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkGridSelect" Checked="true" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label>
                            Tanggal SPPA</label>
                        <asp:Label ID="lblSPPADate" runat="server"></asp:Label>
		            </div>
		            <div class="form_right">			
                        <asp:CheckBox ID="chkSPPADate" Checked="true" runat="server" />
		            </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label class="label_req">
                            No Polis</label>
                        <asp:TextBox ID="txtNoPolis" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNoPolis"
                            ErrorMessage="Harap diisi No Polis" CssClass="validator_general"></asp:RequiredFieldValidator>
		            </div>
		            <div class="form_right">	
                        
		            </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label>
                            Tanggal Terima Polis</label>
                        <uc1:ucdatece id="txtValidDate" runat="server" />                        
		            </div>
		            <div class="form_right">	
                        	
		            </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label>
                            Nilai Pertanggungan</label>                        
                        <asp:Label ID="lblSumInsured" runat="server"></asp:Label>
		            </div>
		            <div class="form_right">		
                        <asp:CheckBox ID="chkSumInsured" Checked="true" runat="server" />			
		            </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label>
                            No Rangka</label>                        
                        <asp:Label ID="lblChasis" runat="server"></asp:Label>
		            </div>
		            <div class="form_right">
                        <asp:CheckBox ID="chkChasis" Checked="true" runat="server" />					
		            </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label>
                            No Mesin</label>                        
                        <asp:Label ID="lblMachine" runat="server"></asp:Label>
		            </div>
		            <div class="form_right">	
                        <asp:CheckBox ID="chkMachine" Checked="true" runat="server" />				
		            </div>
                </div>
                <div class="form_box_hide">                    
                    <div class="form_left">
                        <label>
                            Premi Ke Pers Asuransi</label>                                                
                        <asp:Label ID="lblTagihan" runat="server"></asp:Label>
		            </div>
		            <div class="form_right">	
                        <asp:CheckBox ID="chkTagihan" Checked="true" runat="server" />				
		            </div>
                </div>                
                <div class="form_button">
                    <asp:Button ID="ButtonValidate" runat="server" CausesValidation="False" Text="Next"
                        CssClass="small button blue" />
                    <asp:Button ID="ButtonExitPalingAtas" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlNotaAsuransi" Visible="false">
                <uc1:ucnotaasuransi id="UcNotaAsuransi1" runat="server" />
                <div class="form_button">
                    <asp:Button ID="ButtonSaveNota" runat="server" Text="Save" CssClass="small button blue" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false"/>
                    <asp:Button ID="ButtonCancelNota" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlValid" runat="server">
               
               
                <asp:Panel ID="pnlValidationDiv" runat="server">
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="PnlValid2" runat="server" Visible="False">
              
                <asp:Panel ID="pnlValidationDiv2" runat="server">
                    <div class="form_box_title">
                        <div class="form_eighten">
                            <h5>
                                Tahun Ke</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                Jenis Cover</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                TPL</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                SRCC</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                Flood</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                Loading Fee</h5>
                        </div>
                        <div class="form_eighten">
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlBilling" runat="server" Visible="False">
 
                <asp:Panel ID="pnlValidationDiv3" runat="server">
                    <div class="form_box_title">
                        <div class="form_left">
                            <h5>
                                Data Dari Perusahaan Asuransi</h5>
                        </div>
                        <div class="form_right">
                            <h5>
                                Data Benar</h5>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="PnlBtn" runat="server" Visible="False">
                <div class="form_button">
                    <asp:Button ID="ButtonEndors" runat="server" Text="Next" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                    <asp:Button ID="ButtonEdit" runat="server" Text="Edit" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                    <asp:Button ID="ButtonExitBawah" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    <%--</asp:UpdatePanel>--%>
    </asp:Panel>
    </form>
</body>
</html>
