﻿#Region "Imports"
Option Strict On
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class RenewalPrint
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
    Private oController As New GeneralPagingController
    Private oPrintMailController As New MailTransactionController
    Private Const FILE_NAME_VIEWER As String = "Report/PrintPDFRenewalLetterViewer.aspx"
    Private Const COOKIES_RENEWAL_PRINT As String = CommonCookiesHelper.COOKIES_RENEWAL_PRINT
    Protected WithEvents oBranch As ucBranchAll
#End Region
#Region "Property"
    Private Property StrAggrementChecked() As String
        Get
            Return (CType(ViewState("StrAggrementChecked"), String))
        End Get
        Set(ByVal Value As String)
            ViewState("StrAggrementChecked") = Value
        End Set
    End Property
#End Region
#Region "InitialDefaultPanel"

    Public Sub InitialDefaultPanel()
        PnlSearch.Visible = True
        PnlGrid.Visible = False
    End Sub


#End Region
#Region "Page Load"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        'LblBranchName.Text = Me.BranchName
        If Not IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Insurance', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If
            LblErrorMessages.Text = ""
            ' CInt(Replace(Me.sesBranchId, "'", "")) >= 110 And
            'If IsSingleBranch() And Me.IsHoBranch = False Then
            'Remark by Wira 2016-02-24, agar HO bisa setak surat perpanjangan
            If IsSingleBranch() Then
                InitialDefaultPanel()
                ' oLetterDate.dateValue = CStr(Me.BusinessDate)
                oSearchBy.ListData = "dbo.Agreement.AgreementNo, No Kontrak-dbo.Customer.Name , Nama Konsumen-dbo.AssetMaster.Description , Nama Asset"
                oSearchBy.BindData()
                If Me.SortBy = "" Then Me.SortBy = " AgreementNo Desc "
            Else
                KickOutForMultipleBranch()
            End If
        End If

    End Sub

#End Region
#Region "Search"

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        PnlGrid.Visible = True
        'Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & Replace(Me.sesBranchId, "'", "") & "' "
        Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & oBranch.BranchID.Trim() & "' "

        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
        End If

        If txtCreateDate.Text <> "" Then
            Me.SearchBy = Me.SearchBy & " And dbo.InsuranceAsset.RenewalDocDate = Convert(DateTime,'" & txtCreateDate.Text & "',103) "
        End If
        If Me.SortBy = "" Then
            Me.SortBy = " dbo.InsuranceAsset.RenewalDocDate "
        End If
        DoBind(Me.SearchBy, Me.SortBy)

    End Sub

#End Region
#Region "CheckStatus"

    Public Sub ChkStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim loopitem As Int16
        Dim ChkStatusAgreementNo As CheckBox

        For loopitem = 0 To CType(DGrid.Items.Count - 1, Int16)
            ChkStatusAgreementNo = New CheckBox
            ChkStatusAgreementNo = CType(DGrid.Items(loopitem).FindControl("ChkStatusAgreementNo"), CheckBox)

            If ChkStatusAgreementNo.Enabled Then
                If CType(sender, CheckBox).Checked = True Then
                    ChkStatusAgreementNo.Checked = True
                Else
                    ChkStatusAgreementNo.Checked = False
                End If
            Else
                ChkStatusAgreementNo.Checked = False
            End If

        Next

    End Sub
#End Region
#Region "DoBind"
    Sub DoBind(ByVal cmdWhere As String, ByVal cmdSort As String)
        Dim oEntities As New Parameter.GeneralPaging
        With oEntities
            .WhereCond = cmdWhere
            .SortBy = cmdSort
            .SpName = "spInsListPrintRenewalLetter"
            .strConnection = GetConnectionString()
        End With

        Try
            oEntities = oController.GetReportWithParameterWhereAndSort(oEntities)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim dtEntity As New DataSet

        If Not oEntities Is Nothing Then
            dtEntity = oEntities.ListDataReport

        End If

        DGrid.DataSource = dtEntity
        DGrid.DataBind()

    End Sub
    Private Sub DGrid_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGrid.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationIDHidden"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub
#End Region
#Region "SortGrid"

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Print "

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

        'Mark dulu untuk sementara
        'If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_PRINT_RENEWAL_LETTER, CommonVariableHelper.FORM_FEATURE_PRINT, CommonVariableHelper.APPLICATION_NAME) Then
        '    If sessioninvalid() Then
        '        Exit Sub
        '    End If
        'End If

        Dim LoopPrint As Int32
        Dim TempAgreementStatusPrint As String

        For LoopPrint = 0 To DGrid.Items.Count - 1

            Dim CurrentCheckBox As New CheckBox
            Dim strAgreementNo As New Label
            Dim strApplicationID As New Label
            Dim strBranchID As Label
            Dim strRenewalDocNo As Label
            Dim koma As String

            CurrentCheckBox = CType(DGrid.Items(LoopPrint).FindControl("ChkStatusAgreementNo"), CheckBox)
            strAgreementNo = CType(DGrid.Items(LoopPrint).FindControl("LblAgreementNo"), Label)
            strApplicationID = CType(DGrid.Items(LoopPrint).FindControl("lblApplicationIDHidden"), Label)
            strBranchID = CType(DGrid.Items(LoopPrint).FindControl("LblGridBranchIDHidden"), Label)
            strRenewalDocNo = CType(DGrid.Items(LoopPrint).FindControl("LblRenewalDocNoHidden"), Label)

            If CurrentCheckBox.Checked = True Then
                If TempAgreementStatusPrint = "" Then koma = "" Else koma = ","
                TempAgreementStatusPrint = TempAgreementStatusPrint & koma & strAgreementNo.Text.Trim
                Dim oEntities As New Parameter.MailTransaction
                With oEntities
                    .BranchId = strBranchID.Text.Trim
                    .ApplicationID = strApplicationID.Text.Trim
                    .MailTypeID = "RENEWAL"
                    .MailTransNo = strRenewalDocNo.Text.Trim
                    .MailDateCreate = Me.BusinessDate
                    .MailUserCreate = Me.Loginid
                    .MailDatePrint = Me.BusinessDate
                    .MailPrintedNum = 0 ' Di Spnya sudah dibuatkan counternya...
                    .MailUserPrint = Me.Loginid.Trim
                    .strConnection = GetConnectionString()
                    Context.Trace.Write(" .BranchId  = " & .BranchId)
                    Context.Trace.Write(" .ApplicationID  = " & .ApplicationID)
                    Context.Trace.Write(" .MailTypeID  = " & .MailTypeID)
                    Context.Trace.Write(" .MailTransNo  = " & .MailTransNo)
                    Context.Trace.Write(" .MailDateCreate  = " & .MailDateCreate)
                    Context.Trace.Write(" .MailUserCreate  = " & .MailUserCreate)
                    Context.Trace.Write(" .MailDatePrint  = " & .MailDatePrint)
                    Context.Trace.Write(" .MailPrintedNum  = " & .MailPrintedNum)
                    Context.Trace.Write(" .MailUserPrint  = " & .MailUserPrint)

                End With
                'Insert ke tabel Mail Transaction
                Try
                    oPrintMailController.PrintMailTransaction(oEntities)
                Catch ex As Exception
                    Response.Write(ex.Message + ex.StackTrace)
                    Exit Sub
                End Try

            End If
        Next

        If TempAgreementStatusPrint = "" Then
            'Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & Replace(Me.sesBranchId, "'", "") & "' "
            Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & oBranch.BranchID.Trim & "' "
            DoBind(Me.SearchBy, Me.SortBy)
        End If

        AgreementToBePrinted(TempAgreementStatusPrint)

    End Sub

#End Region
#Region "AgreementToBePrinted"


    Public Sub AgreementToBePrinted(ByVal aggrementtobeprinted As String)
        Dim AggrementChecked As String

        If aggrementtobeprinted = "" Then
            Exit Sub
        End If

        AggrementChecked = Replace(aggrementtobeprinted, ",", "','").Trim
        AggrementChecked = " And AgreementNo in ('" & AggrementChecked & "')".Trim

        Me.StrAggrementChecked = AggrementChecked

        SendCookies()
        Context.Trace.Write("cookies(SearchBy) = " & Me.SearchBy & Me.StrAggrementChecked)
        Response.Redirect(FILE_NAME_VIEWER)

    End Sub

#End Region
#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_RENEWAL_PRINT)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = Me.SearchBy & Me.StrAggrementChecked
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(COOKIES_RENEWAL_PRINT)
            cookieNew.Values("SearchBy") = Me.SearchBy & Me.StrAggrementChecked
            Response.AppendCookie(cookieNew)
        End If
    End Sub

#End Region
#Region "Reset"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        'Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & Replace(Me.sesBranchId, "'", "") & "' "
        Me.SearchBy = " And dbo.InsuranceAsset.BranchId = '" & oBranch.BranchID.Trim & "' "
        DoBind(Me.SearchBy, Me.SortBy)
    End Sub
#End Region


End Class