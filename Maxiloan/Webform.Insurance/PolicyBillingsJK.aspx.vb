﻿#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports System.Math
Imports System.Text
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class PolicyBillingsJK
    Inherits Maxiloan.Webform.WebBased
    Private m_Doc As New DocReceiveController
    Protected WithEvents UcNotaAsuransi1 As UcNotaAsuransi
    Protected WithEvents txtValidDate As ucDateCE


#Region "Property"
    Private Property SPPADate() As Date
        Get
            Return (CType(ViewState("SPPADate"), Date))
        End Get
        Set(ByVal Value As Date)
            ViewState("SPPADate") = Value
        End Set
    End Property
    Property ApplicationID() As String
        Get
            Return ViewState("ApplicationID").ToString
        End Get
        Set(ByVal ApplicationID As String)
            ViewState("ApplicationID") = ApplicationID
        End Set
    End Property
    Property assetseqno() As String
        Get
            Return ViewState("assetseqno").ToString
        End Get
        Set(ByVal assetseqno As String)
            ViewState("assetseqno") = assetseqno
        End Set
    End Property
    Property produciIns() As String
        Get
            Return ViewState("produciIns").ToString
        End Get
        Set(ByVal inssequenceno As String)
            ViewState("produciIns") = inssequenceno
        End Set
    End Property
    Property inssequenceno() As String
        Get
            Return ViewState("inssequenceno").ToString
        End Get
        Set(ByVal inssequenceno As String)
            ViewState("inssequenceno") = inssequenceno
        End Set
    End Property
    Property total() As Decimal
        Get
            Return CDec(ViewState("total").ToString)
        End Get
        Set(ByVal total As Decimal)
            ViewState("total") = total
        End Set
    End Property
    Property MaskAssBranchID() As String
        Get
            Return ViewState("MaskAssBranchID").ToString
        End Get
        Set(ByVal MaskAssBranchID As String)
            ViewState("MaskAssBranchID") = MaskAssBranchID
        End Set
    End Property
    Property Next1() As String
        Get
            Return ViewState("Next1").ToString
        End Get
        Set(ByVal Next1 As String)
            ViewState("Next1") = Next1
        End Set
    End Property
    Property Status() As String
        Get
            Return ViewState("Status").ToString
        End Get
        Set(ByVal Status As String)
            ViewState("Status") = Status
        End Set
    End Property
#End Region
#Region "LinkToAgreementNo"

    Function LinkToAgreementNo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "','" & strApplicationID & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function
    Function LinkToAsset(ByVal strApplicationID As String) As String
        Return "javascript:OpenWinAsset('" & strApplicationID & "','Insurance')"
    End Function

#End Region
#Region "Declare Variable"
    Dim strConnect As String
    Dim dsSearch As DataSet
    Dim conSearch As SqlConnection
    Dim cmdSearch As SqlCommand
    Dim gStrSQL As String
    Dim gBytPageSize As Byte
    Dim agreementno As String
    Dim customerID As String
    Dim custid As String
    Dim desc As String
    Dim customertype As String
    Dim checkbutton As String
    Dim c As Integer
    Dim d As Integer
    Dim branch As String
    Dim x As Integer
    Dim y As Integer
#End Region
#Region "Page Load"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            If Request.QueryString("strFileLocation") <> "" Then
                Dim strHTTPServer As String
                Dim StrHTTPApp As String
                Dim strNameServer As String
                Dim strFileLocation As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"
                '& "history.back(-1) " & vbCrLf _
                'Call file PDF 
                Response.Write("<script language = javascript>" & vbCrLf _
                & "window.open('" & strFileLocation & "','Insurance', 'left=0, top=0, width=600, height=480, menubar=0, scrollbars=yes') " & vbCrLf _
                & "</script>")

            End If

            PnlValid.Visible = False
            txtValidDate.IsRequired = True
            txtValidDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")

            Try
                Bindgrid()
            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                err.WriteLog("PolicyBillingsCPJK.aspx.vb", "PageLoad", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                Response.Write(ex.Message)
            End Try
        End If
    End Sub

#End Region
#Region "Connection"
    Sub Connection()
        strConnect = GetConnectionString()
        Try
            conSearch = New SqlConnection(strConnect)
        Catch objError As Exception
            Exit Sub
        End Try
    End Sub


#End Region
#Region "BindGrid"

    Sub Bindgrid()

        ApplicationID = Replace(Request.QueryString("applicationid"), "'", "").Trim
        BranchID = Replace(Request.QueryString("BranchId"), "'", "").Trim
        agreementno = Replace(Request.QueryString("agreementno"), "'", "").Trim
        customerID = Replace(Request.QueryString("customerID"), "'", "").Trim
        assetseqno = Request.QueryString("assetseqno")
        inssequenceno = Request.QueryString("inssequenceno")
        produciIns = Request.QueryString("produciIns")

        custid = Replace(Request.QueryString("customerID"), "'", "")
        desc = Request.QueryString("desc")
        customertype = Request.QueryString("customertype")
        checkbutton = Request.QueryString("checkbutton")

        Context.Trace.Write("AgreementNo = " & agreementno)
        Context.Trace.Write("customerID = " & customerID)
        Context.Trace.Write("applicationid = " & ApplicationID)
        Context.Trace.Write("custid = " & custid)
        Context.Trace.Write("customertype = " & customertype)

        'BindDataNotaAsuransi()

        If IsNothing(conSearch) Then Connection()

        Dim oSqlText As New StringBuilder

        oSqlText.Append(" SELECT InsuranceAsset.Applicationid, InsuranceAsset.AssetSeqno, ")
        oSqlText.Append(" InsuranceAsset.InsSequenceNo, ")
        oSqlText.Append(" InsuranceAsset.CoverPeriod, ")
        oSqlText.Append(" InsuranceAsset.SPPANo, InsuranceAsset.AssetTypeID as iassetid, ")
        oSqlText.Append(" isnull(InsuranceAsset.SPPADAte,'') as SPPADate ,  ")
        oSqlText.Append(" InsuranceAsset.AccNotes as accnotes, InsuranceAsset.PremiumAmountToInsCo,InsuranceAsset.PaidAmountToInsco,  ")
        oSqlText.Append(" InsuranceAsset.SRCCToInsco, InsuranceAsset.TPLToInsco, InsuranceAsset.AdminFee, InsuranceAsset.MeteraiFee,  ")
        oSqlText.Append(" Customer.Name as CustomerName, AgreementAsset.OwnerAsset,  ")
        oSqlText.Append(" Agreement.AgreementNo, Agreement.Tenor, AssetMaster.Description AS assetDescription, ")
        oSqlText.Append(" TblAssetUsage.Description as UsageId, ")
        oSqlText.Append(" isnull(AgreementAsset.SerialNo1,'') as SerialNo1 , isnull(AgreementAsset.SerialNo2,'') as SerialNo2, AgreementAsset.ManufacturingYear as myear,  ")
        oSqlText.Append(" AssetAttribute.Name as assetname,  ")
        oSqlText.Append(" isnull(AssetAttributeContent.AttributeID,'') as AttributeID, isNull(AssetAttributeContent.AttributeContent,'') as AttributeContent , ")
        oSqlText.Append(" convert(char(10),InsuranceAsset.StartDate,103)as startdate, convert(char(10),InsuranceAsset.EndDate ,103) as endDate,  ")
        oSqlText.Append(" DATEDIFF(month, InsuranceAsset.StartDate, InsuranceAsset.EndDate) AS periodeAsuransi, InsuranceAsset.SumInsured, Customer.Name as cname , ")
        oSqlText.Append(" Agreement.MailingAddress as Address ,")
        oSqlText.Append(" isnull(InsuranceAsset.Suminsured,0) as Suminsured,")
        oSqlText.Append(" isnull(AgreementAsset.SerialNo1,'') as SerialNo1,")
        oSqlText.Append(" isnull(AgreementAsset.SerialNo2,'') as SerialNo2")
        oSqlText.Append(" FROM AgreementAsset INNER JOIN  ")
        oSqlText.Append(" InsuranceAsset ON AgreementAsset.BranchID = InsuranceAsset.BranchID and ")
        oSqlText.Append(" AgreementAsset.ApplicationID = InsuranceAsset.ApplicationID AND   ")
        oSqlText.Append(" AgreementAsset.AssetSeqNo = InsuranceAsset.AssetSeqNo inner join ")
        oSqlText.Append(" TblAssetUsage on InsuranceAsset.UsageId=TblAssetUsage.Id ")
        oSqlText.Append(" INNER JOIN  ")
        'oSqlText.Append(" AssetMaster ON AgreementAsset.AssetCode = AssetMaster.AssetCode INNER JOIN  ")
        oSqlText.Append(" AssetMaster ON AgreementAsset.AssetCode = AssetMaster.AssetCode LEFT JOIN  ")
        oSqlText.Append(" AssetAttribute ON AssetAttribute.AssetTypeID = InsuranceAsset.AssetTypeID LEFT OUTER JOIN  ")
        oSqlText.Append(" AssetAttributeContent ON AssetAttributeContent.AttributeID = AssetAttribute.AttributeID AND   ")
        oSqlText.Append(" InsuranceAsset.ApplicationID = AssetAttributeContent.ApplicationID AND InsuranceAsset.AssetSeqNo = AssetAttributeContent.AssetSeqNo INNER JOIN  ")
        oSqlText.Append(" Agreement ON InsuranceAsset.BranchID = Agreement.BranchID and ")
        oSqlText.Append(" InsuranceAsset.ApplicationID = Agreement.ApplicationID INNER JOIN  ")
        oSqlText.Append(" Customer ON Agreement.CustomerID = Customer.CustomerID  ")
        oSqlText.Append(" WHERE InsuranceAsset.BranchId='" & BranchID & "' and InsuranceAsset.ApplicationID ='" & ApplicationID & "'  ")
        oSqlText.Append(" AND InsuranceAsset.AssetSeqNo ='" & assetseqno & "' ")
        oSqlText.Append(" AND InsuranceAsset.InsSequenceNo ='" & inssequenceno & "' ")

        '--select Max(InsSequenceNo)
        Context.Trace.Write("sql = " & oSqlText.ToString)

        Dim objAdapter As New SqlDataAdapter(oSqlText.ToString, conSearch)

        dsSearch = New DataSet
        objAdapter.Fill(dsSearch, "InsuranceAsset")
        Dim strCoverPeriod As String

        Context.Trace.Write("Count InsuranceAsset tbls  = " & dsSearch.Tables("InsuranceAsset").Rows.Count)

        If dsSearch.Tables("InsuranceAsset").Rows.Count > 0 Then
            Dim objRow As DataRowView = dsSearch.Tables("InsuranceAsset").DefaultView(0)

            strCoverPeriod = CType(objRow("CoverPeriod"), String)

            hplAgreement.Text = agreementno
            hplAgreement.NavigateUrl = LinkToAgreementNo(Me.ApplicationID, "AccAcq")

            hplCustomerName.NavigateUrl = LinkToCustomer(customerID, "AccAcq")
            hplCustomerName.Text = CType(objRow("cname"), String)


            lnkAsset.Text = CType(objRow("AssetDescription"), String)
            lnkAsset.NavigateUrl = LinkToAsset(Me.ApplicationID)


            'lblUsage.Text = CType(objRow("Usageid"), String)
            'lblYear.Text = CType(objRow("myear"), String)
            'lblPeriode.Text = CType(objRow("startdate"), String) & " s/d " & CType(objRow("enddate"), String)
            'LblAddress.Text = CType(objRow("address"), String)


            'lblLamaAsuransi.Text = CType(objRow("periodeAsuransi"), String)


            Me.SPPADate = CType(objRow("SPPADate"), Date)
            lblSPPADate.Text = Me.SPPADate.ToString("dd/MM/yyyy")
            lblSumInsured.Text = FormatNumber(CType(objRow("SumInsured"), String), 0, , , )

            'lblChasis.Text = CType(objRow("SerialNo1"), String)
            'lblMachine.Text = CType(objRow("SerialNo2"), String)
            'lblTagihan.Text = FormatNumber(CType(objRow("PaidAmountToInsco"), String), 0, , , )

            Me.SPPADate = CType(objRow("SPPADate"), Date)

        Else
            Response.Write("<p><font color='red' face='Tahoma' size='2'>Record Not Found</font></p>")
        End If

        '========================View Asset Attribute============================

        Dim LObjAdpt2 As New SqlDataAdapter
        Dim SqlAssetAttribute As New StringBuilder

        SqlAssetAttribute.Append("")
        SqlAssetAttribute.Append(" SELECT AssetAttribute.Name As assetname, ")
        SqlAssetAttribute.Append(" IsNull(AssetAttributeContent.AttributeContent,'') as AttributeContent ")
        SqlAssetAttribute.Append(" FROM InsuranceAsset INNER JOIN ")
        SqlAssetAttribute.Append(" AssetAttribute ON AssetAttribute.AssetTypeID = InsuranceAsset.AssetTypeID LEFT OUTER JOIN ")
        SqlAssetAttribute.Append(" AssetAttributeContent ON AssetAttributeContent.AttributeID = AssetAttribute.AttributeID ")
        SqlAssetAttribute.Append(" AND AssetAttributeContent.ApplicationID = InsuranceAsset.ApplicationID  ")
        SqlAssetAttribute.Append(" AND AssetAttributeContent.AssetSeqNo = InsuranceAsset.AssetSeqNo  ")
        SqlAssetAttribute.Append(" AND AssetAttributeContent.AssetTypeID = InsuranceAsset.AssetTypeID  ")
        SqlAssetAttribute.Append(" WHERE InsuranceAsset.BranchId='" & BranchID & "'  and ")
        SqlAssetAttribute.Append(" InsuranceAsset.ApplicationID ='" & ApplicationID & "'  ")
        SqlAssetAttribute.Append(" AND InsuranceAsset.AssetSeqNo ='" & assetseqno & "' ")
        SqlAssetAttribute.Append(" AND InsuranceAsset.InsSequenceNo ='" & inssequenceno & "'")

        LObjAdpt2 = New SqlDataAdapter(SqlAssetAttribute.ToString, conSearch)
        LObjAdpt2.Fill(dsSearch, "AssetAttribute")

        'Dim dvAttribute As DataView
        'dvAttribute = New DataView(dsSearch.Tables("AssetAttribute"))
        'dtgAttribute.DataSource = dvAttribute
        'dtgAttribute.DataBind()

        '==========================End View========================================

        '========================View DataGrid detail============================

        Dim LObjAdpt1 As New SqlDataAdapter

        Dim SqlAsset As New StringBuilder
        SqlAsset.Append("")




        Dim oController As New SPPAController
        Dim cmdwherebuilder As New StringBuilder
        cmdwherebuilder.Append("Where dbo.InsuranceAssetDetail.BranchId ='" & Me.BranchID & "' And dbo.InsuranceAssetDetail.ApplicationID= '" & Replace(Me.ApplicationID, "'", "") & "' ")
        cmdwherebuilder.Append(" AND dbo.InsuranceAssetDetail.AssetSeqNo = (select dbo.AmbilAssetSequencenoInsuranceAsset('" & Replace(Me.BranchID, "'", "") & "','" & Replace(Me.ApplicationID, "'", "") & "'))  ")
        cmdwherebuilder.Append(" And dbo.InsuranceAssetDetail.InsSequenceNo = (select dbo.AmbilInsSequencenoInsuranceAsset('" & Replace(Me.BranchID, "'", "") & "','" & Replace(Me.ApplicationID, "'", "") & "')) ")
        Dim oEntities As New Parameter.SPPA
        With oEntities

            .WhereCond = cmdwherebuilder.ToString
            .PageSource = General.CommonVariableHelper.PAGE_SOURCE_GRID_TENOR_VIEW_INSURANCE_ASSET_DETAIL
            .strConnection = GetConnectionString()
        End With
        oEntities = oController.GetListCreateSPPA(oEntities)

        'DtgAsset.DataSource = oEntities.ListData
        'DtgAsset.DataBind()
    End Sub

#End Region
#Region "ValidateData"

    Private Function ValidateData(ByVal txtText As String, ByVal txtJudul As String) As Boolean
        Try
            If txtText <> "" Then
                lblMessage.Visible = False
            Else
                lblMessage.Visible = True
                ShowMessage(lblMessage, " " & txtJudul & " is required!", True)
                Return False
            End If
        Catch
            lblMessage.Visible = True
            ShowMessage(lblMessage, " " & txtJudul & " is required!", True)
            Return False
        End Try
        Return True
    End Function
#End Region
#Region "ValidateNum"

    Private Function ValidateNum(ByVal txtText As String, ByVal txtJudul As String) As Boolean
        Try
            If IsNumeric(txtText) Then
                lblMessage.Visible = False
            Else
                lblMessage.Visible = True
                ShowMessage(lblMessage, " " & txtJudul & " Harap diisi dengan Angka", True)
                Return False
            End If
        Catch
            lblMessage.Visible = True
            ShowMessage(lblMessage, " " & txtJudul & " Harap diisi dengan Angka", True)
            Return False
        End Try
        Return True
    End Function

#End Region
#Region "ValidateDate"

    Private Function ValidateDate(ByVal txtText As String, ByVal txtJudul As String) As Boolean
        Dim arrdate() As String
        Dim tempdate As String
        If InStr(1, txtText, "/") >= 2 Then
            arrdate = Split(txtText, "/")
            tempdate = arrdate(1) & "/" & arrdate(0) & "/" & arrdate(2)
            Try
                If CDate(tempdate) <= Me.BusinessDate Then
                    lblMessage.Visible = False
                Else
                    lblMessage.Visible = True
                    ShowMessage(lblMessage, " " & txtJudul & " Tanggal harus  <= Hari ini", True)
                    Return False
                End If
            Catch
                lblMessage.Visible = True
                ShowMessage(lblMessage, " " & txtJudul & " Tanggal harus  <= Hari ini", True)
                Return False
            End Try
        Else
            lblMessage.Visible = True
            ShowMessage(lblMessage, " " & txtJudul & " Tanggal Salah", True)
            Return False
        End If
        Return True
    End Function
#End Region
#Region "ValidateDate1"

    Private Function ValidateDate1(ByVal txtText As String, ByVal txtJudul As String) As Boolean
        Dim arrdate() As String
        Dim tempdate As String
        If InStr(1, txtText, "/") >= 2 Then
            arrdate = Split(txtText, "/")
            tempdate = arrdate(1) & "/" & arrdate(0) & "/" & arrdate(2)
            Try
                If CDate(tempdate) >= Me.SPPADate Then
                    lblMessage.Visible = False
                Else
                    lblMessage.Visible = True
                    ShowMessage(lblMessage, " " & txtJudul & " Tanggal Harus  >= Tanggal SPPA", True)
                    Return False
                End If
            Catch
                lblMessage.Visible = True
                ShowMessage(lblMessage, " " & txtJudul & " Tanggal Harus  >= Tanggal SPPA", True)
                Return False
            End Try
        Else
            lblMessage.Visible = True
            ShowMessage(lblMessage, " " & txtJudul & " Tanggal Salah", True)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Div Table in code"

    Private Sub insertDiv(ByVal a As HtmlGenericControl, ByVal b As HtmlGenericControl)
        a.Controls.Add(b)
    End Sub

    Private Sub insertDiv(ByVal a As HtmlGenericControl, ByVal b As HtmlGenericControl, ByVal c As HtmlGenericControl)
        a.Controls.Add(b)
        a.Controls.Add(c)
    End Sub
    Private Function genDiv(ByVal classname As String) As HtmlGenericControl
        Dim a As New HtmlGenericControl("div")
        a.Attributes.Add("class", classname)
        Return a
    End Function

    Private Function genLabel(ByVal value As String) As HtmlGenericControl
        Dim a As New HtmlGenericControl("label")
        a.InnerHtml = value
        Return a
    End Function

    Private Function genCustom(ByVal value As Control) As HtmlGenericControl
        Dim a As New HtmlGenericControl()
        a.Controls.Add(value)
        Return a
    End Function

    Private Sub genTemplateDiv(ByVal labelLeft As String, ByVal valueLeft As String, ByVal ctrlRight As Control, ByVal valueRight As String, ByVal pnlTarget As Panel)
        genTemplateDiv(labelLeft, valueLeft, New Label, ctrlRight, valueRight, pnlTarget)
    End Sub

    Private Sub genTemplateDiv(ByVal value1 As String, ByVal ctrl1 As Control, ByVal value2 As String, ByVal ctrl2 As Control, ByVal value3 As String, ByVal ctrl3 As Control, ByVal pnlTarget As Panel)
        Dim row As HtmlGenericControl
        Dim innerRow As HtmlGenericControl
        Dim leftside As HtmlGenericControl

        Dim leftSide_ctrl As HtmlGenericControl


        row = genDiv("form_box")
        innerRow = genDiv("form_left")

        leftside = genLabel(value1)
        leftSide_ctrl = genCustom(ctrl1)
        insertDiv(leftside, leftSide_ctrl)
        insertDiv(innerRow, leftside)

        leftside = genLabel(value2)
        leftSide_ctrl = genCustom(ctrl2)
        insertDiv(leftside, leftSide_ctrl)
        insertDiv(innerRow, leftside)

        insertDiv(row, innerRow)

        innerRow = genDiv("form_right")
        leftside = genLabel(value3)
        leftSide_ctrl = genCustom(ctrl3)
        insertDiv(leftside, leftSide_ctrl)
        insertDiv(innerRow, leftside)

        insertDiv(row, innerRow)

        pnlTarget.Controls.Add(row)
    End Sub

    Private Sub genTemplateDivAdd7(ByVal labelLeft As String, ByVal valueLeft As String, ByVal ctrlLeft As Control, ByVal ctrlRight As Control, ByVal valueRight As String, ByVal pnlTarget As Panel, ByVal formClass As String)

    End Sub
    Private Sub genTemplateDiv(ByVal labelLeft As String, ByVal valueLeft As String, ByVal ctrlLeft As Control, ByVal ctrlRight As Control, ByVal valueRight As String, ByVal pnlTarget As Panel)
        Dim row As HtmlGenericControl
        Dim innerRow As HtmlGenericControl
        Dim leftside As HtmlGenericControl
        Dim rightside As HtmlGenericControl
        Dim item1 As HtmlGenericControl


        row = genDiv("form_box")
        innerRow = genDiv("form_left")
        leftside = genLabel(labelLeft)

        rightside = genLabel(valueLeft)
        item1 = genCustom(ctrlLeft)
        insertDiv(rightside, item1)

        insertDiv(innerRow, leftside, rightside)
        insertDiv(row, innerRow)

        innerRow = genDiv("form_right")
        leftside = genLabel(valueRight)
        item1 = genCustom(ctrlRight)
        insertDiv(leftside, item1)

        insertDiv(innerRow, leftside)
        insertDiv(row, innerRow)

        pnlTarget.Controls.Add(row)
    End Sub

#End Region

#Region "Generate Table "


    'Private Sub Gen_Table(ByVal pblnIsFill As Boolean, ByRef counter As Integer, ByRef c As Integer, ByRef d As Integer)

    '    Dim lObjChkBox As HtmlInputCheckBox
    '    Dim lObjTxtBox As HtmlInputText
    '    Dim lObjLbl As HtmlInputHidden
    '    Dim lObjLbl1 As HtmlInputHidden

    '    Dim row As HtmlGenericControl
    '    Dim innerRow As HtmlGenericControl
    '    'Dim leftside As HtmlGenericControl
    '    'Dim rightside As HtmlGenericControl

    '    Dim i As Integer
    '    Dim chkdtg As CheckBox
    '    Dim valValid1 As Boolean
    '    Dim valValid As Boolean
    '    Dim valasset As Boolean
    '    Dim valyear As Boolean
    '    Dim koreksi As Boolean
    '    koreksi = False

    '    agreementno = Request.QueryString("agreementno")
    '    assetseqno = Request.QueryString("assetseqno")
    '    inssequenceno = Request.QueryString("inssequenceno")
    '    ApplicationID = Request.QueryString("applicationid")
    '    valValid = True
    '    valValid1 = True
    '    valasset = True
    '    valyear = True
    '    counter = 0

    '    For i = 0 To dtgAttribute.Items.Count - 1
    '        chkdtg = New CheckBox
    '        chkdtg = CType(dtgAttribute.Items(i).FindControl("chkAttribute"), CheckBox)
    '        If chkdtg.Checked = False Then
    '            valasset = False
    '        End If
    '    Next

    '    For i = 0 To DtgAsset.Items.Count - 1
    '        chkdtg = New CheckBox
    '        chkdtg = CType(DtgAsset.Items(i).FindControl("chkGridSelect"), CheckBox)
    '        If chkdtg.Checked = False Then
    '            valyear = False
    '        End If
    '    Next

    '    If (chkName.Checked = False) Or (ChkAddress.Checked = False) Or (chkAsset.Checked = False) Or (chkUsage.Checked = False) Or (chkYear.Checked = False) _
    '        Or (chkPeriode.Checked = False) Or (chkLamaAsuransi.Checked = False) Or (chkAccessories.Checked = False) Or (valasset = False) Or (valyear = False) _
    '        Or (chkSPPADate.Checked = False) Or (chkSumInsured.Checked = False) Or (chkChasis.Checked = False) Or (chkMachine.Checked = False) _
    '        Or (chkTagihan.Checked = False) Then
    '        ' Bila CheckNamenya tidak di check !!
    '        '=============== ADD
    '        If chkSPPADate.Checked = False Then
    '            counter = counter + 1
    '            lObjChkBox = New HtmlInputCheckBox
    '            lObjChkBox.ID = "chkNewSPPADate"
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If

    '            genTemplateDiv("Tanggal SPPA", lblSPPADate.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '        End If
    '        If chkSumInsured.Checked = False Then
    '            counter = counter + 1
    '            lObjChkBox = New HtmlInputCheckBox
    '            lObjChkBox.ID = "chkNewSumInsured"
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If

    '            genTemplateDiv("Nilai Pertanggungan", lblSumInsured.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '        End If
    '        If chkChasis.Checked = False Then
    '            counter = counter + 1
    '            lObjChkBox = New HtmlInputCheckBox
    '            lObjChkBox.ID = "chkNewChasis"
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If

    '            genTemplateDiv("No Rangka", lblChasis.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '        End If
    '        If chkMachine.Checked = False Then
    '            counter = counter + 1
    '            lObjChkBox = New HtmlInputCheckBox
    '            lObjChkBox.ID = "chkNewMachine"
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If

    '            genTemplateDiv("No Mesin", lblMachine.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '        End If
    '        If chkTagihan.Checked = False Then
    '            counter = counter + 1
    '            lObjChkBox = New HtmlInputCheckBox
    '            lObjChkBox.ID = "chkNewTagihan"
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If

    '            genTemplateDiv("Premi Ke Pers Asuransi", lblTagihan.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '        End If
    '        '========================================

    '        If chkName.Checked = False Then
    '            counter = counter + 1

    '            lObjChkBox = New HtmlInputCheckBox
    '            lObjChkBox.ID = "chkNewName"
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If


    '            genTemplateDiv("Customer Name", hplCustomerName.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '        End If

    '        ' Bila ChkAddress tidak di check !!

    '        If ChkAddress.Checked = False Then
    '            counter = counter + 1

    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If


    '            genTemplateDiv("Address", LblAddress.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '        End If



    '        ' Bila CheckAssetnya tidak di check !!

    '        If chkAsset.Checked = False Then
    '            counter = counter + 1


    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If
    '            lObjChkBox.ID = "chkNewAsset"

    '            genTemplateDiv("Asset Description", lnkAsset.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '        End If

    '        ' Bila CheckUsagenya tidak di check !!

    '        If chkUsage.Checked = False Then
    '            counter = counter + 1

    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If
    '            lObjChkBox.ID = "chkNewUsage"

    '            genTemplateDiv("Usage", lblUsage.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '        End If
    '        ' Bila CheckYearnya tidak di check !!

    '        If chkYear.Checked = False Then
    '            counter = counter + 1


    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If
    '            lObjChkBox.ID = "chkNewYear"

    '            genTemplateDiv("Year", lblYear.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '        End If

    '        c = 0
    '        y = dtgAttribute.Items.Count
    '        Dim assetname(y) As String
    '        Dim assetattribute(y) As String

    '        For i = 0 To dtgAttribute.Items.Count - 1
    '            chkdtg = New CheckBox
    '            chkdtg = CType(dtgAttribute.Items(i).FindControl("chkAttribute"), CheckBox)
    '            If chkdtg.Checked = False Then
    '                counter = counter + 1
    '                assetname(c) = dtgAttribute.Items(i).Cells(0).Text
    '                assetattribute(c) = dtgAttribute.Items(i).Cells(1).Text
    '                If assetattribute(c) = "&nbsp;" Then assetattribute(c) = ""

    '                'lObjTblRow = New HtmlTableRow

    '                lObjLbl = New HtmlInputHidden
    '                lObjLbl.Visible = False
    '                lObjLbl.ID = "lblNewAssetName" & c
    '                lObjLbl.Value = assetname(c)


    '                lObjLbl1 = New HtmlInputHidden
    '                lObjLbl1.Visible = False
    '                lObjLbl1.ID = "lblNewAssetAttribute" & c
    '                lObjLbl1.Value = assetattribute(c)



    '                lObjChkBox = New HtmlInputCheckBox
    '                If pblnIsFill Then
    '                    lObjChkBox.Disabled = False
    '                Else
    '                    lObjChkBox.Disabled = True
    '                End If
    '                lObjChkBox.ID = "chkNewAttribute" & c


    '                genTemplateDiv(assetname(c), lObjLbl, assetattribute(c), lObjLbl1, String.Empty, lObjChkBox, pnlValidationDiv)
    '                c = c + 1
    '            End If
    '        Next

    '        ' Bila Checkperiodeenya tidak di check !!
    '        If chkPeriode.Checked = False Then
    '            counter = counter + 1


    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If
    '            lObjChkBox.ID = "chkNewPeriode"

    '            genTemplateDiv("Insurance Period", lblPeriode.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '        End If

    '        ' Bila chkLamaAsuransi tidak di check !!
    '        If chkLamaAsuransi.Checked = False Then
    '            counter = counter + 1


    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If
    '            lObjChkBox.ID = "chkNewLamaAsuransi"


    '            genTemplateDiv("Insurance Length", lblLamaAsuransi.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '        End If

    '        ' Bila chkAccessories tidak di check !!

    '        If chkAccessories.Checked = False Then
    '            counter = counter + 1

    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If

    '            lObjChkBox.ID = "chkNewAccessories"


    '            genTemplateDiv("Accessories", lblAccessories.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '        End If

    '        '=============================================================


    '        d = 0
    '        x = DtgAsset.Items.Count
    '        Dim yearnum(x) As String
    '        Dim coverage(x) As String
    '        Dim tpl(x) As String
    '        Dim rscc(x) As String
    '        Dim flood(x) As String
    '        Dim loading(x) As String

    '        For i = 0 To DtgAsset.Items.Count - 1
    '            chkdtg = New CheckBox
    '            chkdtg = CType(DtgAsset.Items(i).FindControl("chkGridSelect"), CheckBox)

    '            If chkdtg.Checked = False Then
    '                counter = counter + 1
    '                PnlValid2.Visible = True

    '                yearnum(d) = DtgAsset.Items(i).Cells(0).Text
    '                coverage(d) = DtgAsset.Items(i).Cells(1).Text
    '                tpl(d) = DtgAsset.Items(i).Cells(2).Text
    '                rscc(d) = DtgAsset.Items(i).Cells(3).Text
    '                flood(d) = DtgAsset.Items(i).Cells(4).Text
    '                loading(d) = DtgAsset.Items(i).Cells(5).Text

    '                'lObjTblRow = New HtmlTableRow
    '                row = genDiv("form_box")

    '                lObjLbl = New HtmlInputHidden
    '                lObjLbl.Visible = False
    '                lObjLbl.ID = "lblNewYearNum" & d
    '                lObjLbl.Value = yearnum(d)


    '                innerRow = genDiv("form_eighten")
    '                insertDiv(innerRow, genLabel(yearnum(d)), genCustom(lObjLbl))
    '                insertDiv(row, innerRow)


    '                innerRow = genDiv("form_eighten")
    '                insertDiv(innerRow, genLabel(coverage(d)))
    '                insertDiv(row, innerRow)

    '                innerRow = genDiv("form_eighten")
    '                insertDiv(innerRow, genLabel(tpl(d)))
    '                insertDiv(row, innerRow)

    '                innerRow = genDiv("form_eighten")
    '                insertDiv(innerRow, genLabel(rscc(d)))
    '                insertDiv(row, innerRow)


    '                innerRow = genDiv("form_eighten")
    '                insertDiv(innerRow, genLabel(flood(d)))
    '                insertDiv(row, innerRow)

    '                innerRow = genDiv("form_eighten")
    '                insertDiv(innerRow, genLabel(loading(d)))
    '                insertDiv(row, innerRow)

    '                lObjChkBox = New HtmlInputCheckBox
    '                If pblnIsFill Then
    '                    lObjChkBox.Disabled = False
    '                Else
    '                    lObjChkBox.Disabled = True
    '                End If
    '                lObjChkBox.ID = "chkNewAsset" & d

    '                innerRow = genDiv("form_eighten")
    '                insertDiv(innerRow, genCustom(lObjChkBox))
    '                insertDiv(row, innerRow)

    '                pnlValidationDiv2.Controls.Add(row)

    '                d = d + 1
    '            End If
    '        Next

    '        PnlValid.Visible = True
    '        PnlBtn.Visible = True
    '        Panel1.Visible = False
    '        valValid = False
    '    End If
    '    '======================================================================
    '    'Check If data billing sama dgn database atau tidak
    '    Dim lSumInsured As String
    '    Dim tSumInsured As String
    '    Dim dbllSumInsured As Double
    '    Dim dbltSumInsured As Double
    '    Dim lChasis As String
    '    Dim tChasis As String
    '    Dim lMachine As String
    '    Dim tMachine As String
    '    Dim lTagihan As String
    '    Dim tTagihan As String

    '    lSumInsured = lblSumInsured.Text.Trim
    '    tSumInsured = lblSumInsured.Text.Trim
    '    dbllSumInsured = CType(lSumInsured, Double)
    '    dbltSumInsured = CType(tSumInsured, Double)

    '    lChasis = lblChasis.Text
    '    tChasis = lblChasis.Text.Trim
    '    lMachine = lblMachine.Text
    '    tMachine = lblMachine.Text.Trim
    '    lTagihan = lblTagihan.Text
    '    tTagihan = lblTagihan.Text.Trim
    '    valValid1 = True
    '    If (dbllSumInsured <> dbltSumInsured) Or (lChasis <> tChasis) Or (lMachine <> tMachine) Or (lTagihan <> tTagihan) Then

    '        If dbllSumInsured <> dbltSumInsured Then
    '            counter = counter + 1


    '            lObjTxtBox = New HtmlInputText

    '            With lObjTxtBox
    '                .ID = "txtNewSumInsured"
    '                If pblnIsFill Then
    '                    .Disabled = False
    '                Else
    '                    .Disabled = True
    '                End If
    '                .Value = tSumInsured
    '            End With

    '            genTemplateDiv("Nilai Pertanggungan", New Label, String.Empty, lObjTxtBox, lSumInsured, New Label, pnlValidationDiv3)
    '            valValid1 = False
    '        End If

    '        If lChasis <> tChasis Then
    '            counter = counter + 1

    '            lObjTxtBox = New HtmlInputText

    '            With lObjTxtBox
    '                .ID = "txtNewChasis"
    '                If pblnIsFill Then
    '                    .Disabled = False
    '                Else
    '                    .Disabled = True
    '                End If
    '                .Value = tChasis
    '            End With

    '            genTemplateDiv("Chasis Number", New Label, String.Empty, lObjTxtBox, lChasis, New Label, pnlValidationDiv3)
    '            valValid1 = False
    '        End If

    '        If lMachine <> tMachine Then
    '            counter = counter + 1


    '            lObjTxtBox = New HtmlInputText
    '            With lObjTxtBox
    '                .ID = "txtNewMachine"
    '                If pblnIsFill Then
    '                    .Disabled = False
    '                Else
    '                    .Disabled = True
    '                End If
    '                .Value = tMachine
    '            End With

    '            genTemplateDiv("Machine Number", New Label, String.Empty, lObjTxtBox, lMachine, New Label, pnlValidationDiv3)
    '            valValid1 = False
    '        End If




    '    End If

    '    pnlBilling.Visible = True
    '    PnlBtn.Visible = True
    '    Panel1.Visible = False

    '    'Bila datanya Ok, Tidak ada salah-salah lagi
    '    If valValid And valValid1 Then

    '        Connection()
    '        Dim sqlupdateendors As New StringBuilder
    '        sqlupdateendors.Append("UPDATE InsuranceAsset SET PolicyReceiveDate = @PolicyReceiveDate, ")
    '        sqlupdateendors.Append(" FlagDocStatus = @FlagDocStatus, ")
    '        sqlupdateendors.Append(" FlagInsStatus = @FlagInsStatus, ")
    '        sqlupdateendors.Append(" Policyreceiveby = @Policyreceiveby, ")
    '        sqlupdateendors.Append(" PolicyNumber = @PolicyNumber ")
    '        sqlupdateendors.Append("WHERE ApplicationID = @ApplicationID AND AssetSeqNo = @AssetSeqNo ")
    '        sqlupdateendors.Append("AND InsSequenceNo = @InsSequenceNo")

    '        cmdSearch = New SqlCommand(sqlupdateendors.ToString, conSearch)

    '        Dim arrDate() As String
    '        Dim tempDate As String

    '        With cmdSearch
    '            .Parameters.Add(New SqlParameter("@PolicyReceiveDate", SqlDbType.DateTime, 8))
    '            .Parameters.Add(New SqlParameter("@FlagDocStatus", SqlDbType.Char, 1))
    '            .Parameters.Add(New SqlParameter("@FlagInsStatus", SqlDbType.Char, 1))
    '            .Parameters.Add(New SqlParameter("@Policyreceiveby", SqlDbType.Char, 30))
    '            .Parameters.Add(New SqlParameter("@PolicyNumber", SqlDbType.VarChar, 25))
    '            .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
    '            .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
    '            .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))

    '            'arrDate = Split(txtValidDate.Text, "/")
    '            tempDate = arrDate(1) & "/" & arrDate(0) & "/" & arrDate(2)

    '            .Parameters("@PolicyReceiveDate").Value = tempDate
    '            .Parameters("@FlagDocStatus").Value = "O"
    '            .Parameters("@FlagInsStatus").Value = "A"
    '            .Parameters("@Policyreceiveby").Value = Me.Loginid
    '            .Parameters("@PolicyNumber").Value = txtNoPolis.Text
    '            .Parameters("@ApplicationID").Value = ApplicationID
    '            .Parameters("@AssetSeqNo").Value = assetseqno
    '            .Parameters("@InsSequenceNo").Value = inssequenceno

    '            .Connection.Open()
    '            .ExecuteNonQuery()
    '            .Connection.Close()
    '        End With

    '        Context.Trace.Write("PolicyReceiveDate = " & tempDate)
    '        Context.Trace.Write("PolicyNumber = " & txtNoPolis.Text)
    '        Context.Trace.Write("ApplicationID = " & ApplicationID)
    '    End If
    'End Sub
#End Region
#Region "Ketika Tombol Exit di click ! Paling Atas "


    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("PolicyReceive.aspx")
    End Sub
#End Region
#Region "LinkTo"


    Function LinkTo(ByVal pStrType As String, ByVal strCustType As String, ByVal strCustID As String, ByVal strAgreeNo As String, ByVal strAppID As String, ByVal strAsq As String, Optional ByVal strIsq As String = "", Optional ByVal strInsc As String = "", Optional ByVal strInsBr As String = "", Optional ByVal pCust As String = "", Optional ByVal assetcode As String = "", Optional ByVal checkbutton As String = "") As String
        Return "javascript:OpenWinMain('" & strCustType & pStrType & "','" & strCustID.Trim & "','" & strAgreeNo.Trim & "', '" & strAppID.Trim & "','" & strAsq.Trim & "', '" & strIsq.Trim & "','" & strInsc.Trim & "','" & strInsBr.Trim & "','" & pCust.Trim & "','" & assetcode.Trim & "','" & checkbutton.Trim & "')"
    End Function
#End Region
#Region "Ketika Tombol Keluar diclick ! Paling Bawah "
    Private Sub ButtonExitBawah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonExitBawah.Click
        'Response.Redirect("PolicyReceive.aspx?back=yes&MaskAssBranchID=" + Me.MaskAssBranchID + "")
        Response.Redirect("PolicyReceive.aspx?back=yes")
    End Sub
#End Region
#Region "Tombol Exit Paling Atas "


    Private Sub ButtonExitPalingAtas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonExitPalingAtas.Click
        'Response.Redirect("PolicyReceive.aspx?back=yes&MaskAssBranchID=" + Me.MaskAssBranchID + "")
        Response.Redirect("PolicyReceive.aspx?back=yes")
    End Sub
#End Region
#Region "Ketika Tombol Edit di click !"

    Private Sub ButtonEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEdit.Click

        Panel1.Visible = True
        PnlValid.Visible = False
        PnlValid2.Visible = False
        pnlBilling.Visible = False
        PnlBtn.Visible = False

    End Sub


#End Region
#Region "Ketika Tombol Validate di Click "


    Private Sub ButtonValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonValidate.Click


        Page.Validate()
        If ValidateData(txtNoPolis.Text.Trim, "Policy Number") = False Then
            Exit Sub
        End If

        If ValidateData(txtValidDate.Text, "Received Date") = False Then
            Exit Sub
        End If

        If ValidateDate(txtValidDate.Text, "Received Date") = False Then
            Exit Sub
        End If

        If ValidateDate1(txtValidDate.Text, "Received Date") = False Then
            Exit Sub
        End If

        'If Page.IsValid And StepChek() Then
        stepNotaAsuransi()
        'Else
        ''ShowMessage(lblMessage, "Proses tidak bisa dilanjutkan karena ceklist tidak lengkap!", True)
        'End If


    End Sub
#End Region
#Region "Button"
    Private Sub ButtonEndors_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEndors.Click
        Next1 = "True"
        'stepNotaAsuransi()
    End Sub
    Private Sub ButtonSaveNota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveNota.Click
        Dim m_controller As New InsurancePolicyReceiveController
        Dim oInsurance As New Parameter.InsuranceStandardPremium
        Dim Status As Boolean = True
        Dim totalInput As Decimal = 0
        Dim result As Decimal = 0
        Dim generalSet As New Parameter.GeneralSetting
        Dim conGeneral As New GeneralSettingController
        Dim value As Decimal = 0

        generalSet.strConnection = GetConnectionString()
        generalSet.GSID = "INSINVTOLERANCE"

        generalSet = conGeneral.GetGeneralSettingByID(generalSet)

        If generalSet.ListData.Rows.Count > 0 Then
            value = CDec(generalSet.ListData.Rows(0).Item("GSValue").ToString.Trim)
        End If


        totalInput = (CDec(UcNotaAsuransi1.PremiNettxt) - (CDec(UcNotaAsuransi1.PPNtxt) - CDec(UcNotaAsuransi1.PPHtxt))) + CDec(UcNotaAsuransi1.BiayaPolistxt) + CDec(UcNotaAsuransi1.BiayaMateraitxt)
        result = Me.total - totalInput


        If totalInput = 0 Then
            Status = False
        Else
            If Math.Abs(result) < value Then
                Status = True
            Else
                Status = False
            End If

        End If

        With oInsurance
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetSeqNo = Me.assetseqno
            .InsSequenceNo = Me.inssequenceno
            .NoNotaAsuransi = UcNotaAsuransi1.NoNotatxt
            .TglNotaAsuransi = ConvertDate2(UcNotaAsuransi1.TglNota)
            .PolicyNumber = txtNoPolis.Text.Trim
            .PolicyReceiveDate = ConvertDate2(txtValidDate.Text)
            .PremiGross = CDec(UcNotaAsuransi1.PremiGrosstxt)
            .Komisi = CDec(UcNotaAsuransi1.Komisitxt)
            .PremiNet = CDec(UcNotaAsuransi1.PremiNettxt)
            .PPN = CDec(UcNotaAsuransi1.PPNtxt)
            .PPH = CDec(UcNotaAsuransi1.PPHtxt)
            .AdminFee = CDec(UcNotaAsuransi1.BiayaPolistxt)
            .MeteraiFee = CDec(UcNotaAsuransi1.BiayaMateraitxt)
            .PayAmount = CDec(UcNotaAsuransi1.TotalTagihanlbl)
            .BillAmount = CDec(UcNotaAsuransi1.TotalTagihantxt)
            .UserID = Me.UserID
            .ProductInsurance = "JK"

            'Modify by WIra 20190327
            .BeforeBalance = CDec(UcNotaAsuransi1.AdjSelisihInsPayablelbl)
            .JournalCode = UcNotaAsuransi1.cboAdjSelsihInsPayableID
            'End Modify
        End With

        If Status Then
            Try


                m_controller.NotaAsuransiSave(oInsurance)
                SendCookies()
                'Response.Redirect("PolicyReceive.aspx?back=no&MaskAssBranchID=" + Me.MaskAssBranchID + "&print=no")
                Response.Redirect("PolicyReceive.aspx?back=no&print=no")
            Catch ex As Exception
                ShowMessage(lblMessage, ex.Message, True)
                Exit Sub
            End Try
        Else
            ShowMessage(lblMessage, "Jumlah selisih " + FormatNumber(Math.Abs(result), 0) + " lebih dari " + FormatNumber(value, 0) + " ", True)
        End If
    End Sub
    Private Sub ButtonCancelNota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelNota.Click
        'Response.Redirect("PolicyReceive.aspx?back=yes&MaskAssBranchID=" + Me.MaskAssBranchID + "")
        Response.Redirect("PolicyReceive.aspx?back=yes")
    End Sub
#End Region
    Sub BindDataNotaAsuransi()
        Dim oData As New DataTable
        Dim oDoc As New DocRec

        oDoc.strConnection = GetConnectionString()
        oDoc.WhereCond = " InsuranceAsset.ApplicationId = '" + Me.ApplicationID.Trim + "'"
        oDoc.SpName = "spGetNotaAsuransiJK"
        oDoc = m_Doc.GetSPReport(oDoc)
        oData = oDoc.ListDataReport.Tables(0)

        If oData.Rows.Count > 0 Then
            Me.MaskAssBranchID = oData.Rows(0).Item("MaskAssBranchID").ToString.Trim

            UcNotaAsuransi1.Asuransilbl = oData.Rows(0).Item("MaskAssBranchName").ToString
            UcNotaAsuransi1.NoKontraklbl = oData.Rows(0).Item("AgreementNo").ToString
            UcNotaAsuransi1.NamaKonsumenlbl = oData.Rows(0).Item("Name").ToString
            UcNotaAsuransi1.Tenorlbl = oData.Rows(0).Item("Tenor").ToString
            UcNotaAsuransi1.Periodelbl = IIf(CDate(oData.Rows(0).Item("StartDate").ToString).ToString("dd/MM/yyyy").Contains("1900"), "", CDate(oData.Rows(0).Item("StartDate").ToString).ToString("dd/MM/yyyy")).ToString + " s/d" _
                                        + " " + IIf(CDate(oData.Rows(0).Item("EndDate").ToString).ToString("dd/MM/yyyy").Contains("1900"), "", CDate(oData.Rows(0).Item("EndDate").ToString).ToString("dd/MM/yyyy")).ToString

            UcNotaAsuransi1.PremiGrosslbl = FormatNumber(oData.Rows(0).Item("PremiGross").ToString, 2)
            UcNotaAsuransi1.Komisilbl = FormatNumber(oData.Rows(0).Item("Komisi").ToString, 2)
            UcNotaAsuransi1.PremiNetlbl = FormatNumber(oData.Rows(0).Item("PremiNett").ToString, 2)
            UcNotaAsuransi1.PPNlbl = FormatNumber(oData.Rows(0).Item("PPn").ToString, 2)
            UcNotaAsuransi1.PPHlbl = FormatNumber(oData.Rows(0).Item("PPH").ToString, 2)
            UcNotaAsuransi1.BiayaPolislbl = FormatNumber(oData.Rows(0).Item("BiayaPolis").ToString, 2)
            UcNotaAsuransi1.BiayaMaterailbl = FormatNumber(oData.Rows(0).Item("BiayaMaterai").ToString, 2)

            UcNotaAsuransi1.PremiGrosstxt = FormatNumber(oData.Rows(0).Item("PremiGross").ToString, 2)
            UcNotaAsuransi1.Komisitxt = FormatNumber(oData.Rows(0).Item("Komisi").ToString, 2)
            UcNotaAsuransi1.PremiNettxt = FormatNumber(oData.Rows(0).Item("PremiNett").ToString, 2)
            UcNotaAsuransi1.PPNtxt = FormatNumber(oData.Rows(0).Item("PPn").ToString, 2)
            UcNotaAsuransi1.PPHtxt = FormatNumber(oData.Rows(0).Item("PPH").ToString, 2)
            UcNotaAsuransi1.BiayaPolistxt = FormatNumber(oData.Rows(0).Item("BiayaPolis").ToString, 2)
            UcNotaAsuransi1.BiayaMateraitxt = FormatNumber(oData.Rows(0).Item("BiayaMaterai").ToString, 2)

            Me.total = (CDec(UcNotaAsuransi1.PremiNetlbl) - (CDec(UcNotaAsuransi1.PPNlbl) - CDec(UcNotaAsuransi1.PPHlbl))) + CDec(UcNotaAsuransi1.BiayaPolislbl) + CDec(UcNotaAsuransi1.BiayaMaterailbl)

            UcNotaAsuransi1.TotalTagihanlbl = FormatNumber(total, 2)
            UcNotaAsuransi1.TotalTagihantxt = FormatNumber(total, 2)
            AutoChangesText()
        End If

    End Sub
    Sub txtPremiGross_load() Handles UcNotaAsuransi1.TextChanged_txtPremiGross
        UcNotaAsuransi1.txtPremiGross.Text = FormatNumber(CDbl(UcNotaAsuransi1.txtPremiGross.Text), 2)
        AutoChangesText()
    End Sub
    Sub TextChanged_txtKomisi_load() Handles UcNotaAsuransi1.TextChanged_txtKomisi
        UcNotaAsuransi1.txtKomisi.Text = FormatNumber(CDbl(UcNotaAsuransi1.txtKomisi.Text), 2)
        AutoChangesText()
    End Sub
    Sub TextChanged_txtPremiNet_load() Handles UcNotaAsuransi1.TextChanged_txtPremiNet
        UcNotaAsuransi1.txtPremiNet.Text = FormatNumber(CDbl(UcNotaAsuransi1.txtPremiNet.Text), 2)
        AutoChangesText()
    End Sub
    Sub TextChanged_txtPPN_load() Handles UcNotaAsuransi1.TextChanged_txtPPN
        UcNotaAsuransi1.txtPPN.Text = FormatNumber(CDbl(UcNotaAsuransi1.txtPPN.Text), 2)
        AutoChangesText()
    End Sub
    Sub TextChanged_txtPPH_load() Handles UcNotaAsuransi1.TextChanged_txtPPH
        UcNotaAsuransi1.txtPPH.Text = FormatNumber(CDbl(UcNotaAsuransi1.txtPPH.Text), 2)
        AutoChangesText()
    End Sub
    Sub TextChanged_txtBiayaPolis_load() Handles UcNotaAsuransi1.TextChanged_txtBiayaPolis
        UcNotaAsuransi1.txtBiayaPolis.Text = FormatNumber(CDbl(UcNotaAsuransi1.txtBiayaPolis.Text), 2)
        AutoChangesText()
    End Sub
    Sub TextChanged_txtBiayaMaterai_load() Handles UcNotaAsuransi1.TextChanged_txtBiayaMaterai
        UcNotaAsuransi1.txtBiayaPolis.Text = FormatNumber(CDbl(UcNotaAsuransi1.txtBiayaPolis.Text), 2)
        AutoChangesText()
    End Sub
    Sub TextChanged_txtTotalTagihan_load() Handles UcNotaAsuransi1.TextChanged_txtTotalTagihan
        AutoChangesText()
    End Sub
    Sub AutoChangesText()
        UcNotaAsuransi1.PremiNettxt = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.PremiGrosstxt = "", 0, UcNotaAsuransi1.PremiGrosstxt)) - CDec(IIf(UcNotaAsuransi1.Komisitxt = "", 0, UcNotaAsuransi1.Komisitxt))).ToString, 2)
        UcNotaAsuransi1.TotalTagihantxt = FormatNumber(((CDec(IIf(UcNotaAsuransi1.PremiGrosstxt = "", 0, UcNotaAsuransi1.PremiGrosstxt)) - CDec(IIf(UcNotaAsuransi1.Komisitxt = "", 0, UcNotaAsuransi1.Komisitxt))) - (CDec(IIf(UcNotaAsuransi1.PPNtxt = "", 0, UcNotaAsuransi1.PPNtxt)) - CDec(IIf(UcNotaAsuransi1.PPHtxt = "", 0, UcNotaAsuransi1.PPHtxt)))) + CDec(IIf(UcNotaAsuransi1.BiayaPolistxt = "", 0, UcNotaAsuransi1.BiayaPolistxt)) + CDec(IIf(UcNotaAsuransi1.BiayaMateraitxt = "", 0, UcNotaAsuransi1.BiayaMateraitxt)).ToString, 2)

        UcNotaAsuransi1.PremisGrossSelisihlbl = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.PremiGrosslbl = "", 0, UcNotaAsuransi1.PremiGrosslbl)) - CDec(IIf(UcNotaAsuransi1.PremiGrosstxt = "", 0, UcNotaAsuransi1.PremiGrosstxt))).ToString, 2)
        UcNotaAsuransi1.KomisiSelisihlbl = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.Komisilbl = "", 0, UcNotaAsuransi1.Komisilbl)) - CDec(IIf(UcNotaAsuransi1.Komisitxt = "", 0, UcNotaAsuransi1.Komisitxt))).ToString, 2)
        UcNotaAsuransi1.PremiNetSelisihlbl = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.PremiNetlbl = "", 0, UcNotaAsuransi1.PremiNetlbl)) - CDec(IIf(UcNotaAsuransi1.PremiNettxt = "", 0, UcNotaAsuransi1.PremiNettxt))).ToString, 2)
        UcNotaAsuransi1.PPNSelisihlbl = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.PPNlbl = "", 0, UcNotaAsuransi1.PPNlbl)) - CDec(IIf(UcNotaAsuransi1.PPNtxt = "", 0, UcNotaAsuransi1.PPNtxt))).ToString, 2)
        UcNotaAsuransi1.PPHSelisihlbl = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.PPHlbl = "", 0, UcNotaAsuransi1.PPHlbl)) - CDec(IIf(UcNotaAsuransi1.PPHtxt = "", 0, UcNotaAsuransi1.PPHtxt))).ToString, 2)
        UcNotaAsuransi1.BiayaPolisSelisihlbl = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.BiayaPolislbl = "", 0, UcNotaAsuransi1.BiayaPolislbl)) - CDec(IIf(UcNotaAsuransi1.BiayaPolistxt = "", 0, UcNotaAsuransi1.BiayaPolistxt))).ToString, 2)
        UcNotaAsuransi1.BiayaMateraiSelisihlbl = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.BiayaMaterailbl = "", 0, UcNotaAsuransi1.BiayaMaterailbl)) - CDec(IIf(UcNotaAsuransi1.BiayaMateraitxt = "", 0, UcNotaAsuransi1.BiayaMateraitxt))).ToString, 2)
        UcNotaAsuransi1.TotalTagihanSelisihlbl = FormatNumber(Abs(CDec(IIf(UcNotaAsuransi1.TotalTagihanlbl = "", 0, UcNotaAsuransi1.TotalTagihanlbl)) - CDec(IIf(UcNotaAsuransi1.TotalTagihantxt = "", 0, UcNotaAsuransi1.TotalTagihantxt))).ToString, 2)
        'Modify by WIra 20190327
        UcNotaAsuransi1.AdjSelisihInsPayablelbl = UcNotaAsuransi1.PremisGrossSelisihlbl
        If UcNotaAsuransi1.AdjSelisihInsPayablelbl > 0 Then
            UcNotaAsuransi1.cboAdjSelsihInsPayablereq(1)
        Else
            UcNotaAsuransi1.cboAdjSelsihInsPayablereq(0)
        End If
        'End Modify
    End Sub
    Sub stepNotaAsuransi()
        Panel1.Visible = False
        ButtonEndors.Visible = False
        ButtonEdit.Visible = False
        pnlNotaAsuransi.Visible = True
        pnlBilling.Visible = False
        PnlBtn.Visible = False
        PnlValid.Visible = False
        PnlValid2.Visible = False
        pnlValidationDiv.Visible = False
        pnlValidationDiv2.Visible = False
        pnlValidationDiv3.Visible = False
        BindDataNotaAsuransi()
    End Sub
    Sub StepNext1()
        Dim counter As Integer

        If Page.IsValid Then
            'Gen_Table(False, counter, c, d)
        End If
    End Sub
    'Sub StepNext2(ByVal pblnIsFill As Boolean, ByRef counter As Integer, ByRef c As Integer, ByRef d As Integer)
    '    'Dim lObjTblRow As HtmlTableRow
    '    'Dim lObjTblCell As HtmlTableCell
    '    Dim lObjChkBox As HtmlInputCheckBox
    '    Dim lObjTxtBox As HtmlInputText
    '    Dim lObjLbl As HtmlInputHidden
    '    Dim lObjLbl1 As HtmlInputHidden

    '    Dim row As HtmlGenericControl
    '    Dim innerRow As HtmlGenericControl


    '    Dim i As Integer
    '    Dim chkdtg As CheckBox
    '    Dim valValid1 As Boolean
    '    Dim valValid As Boolean
    '    Dim valasset As Boolean
    '    Dim valyear As Boolean
    '    Dim koreksi As Boolean
    '    koreksi = False

    '    agreementno = Request.QueryString("agreementno")
    '    assetseqno = Request.QueryString("assetseqno")
    '    inssequenceno = Request.QueryString("inssequenceno")
    '    ApplicationID = Request.QueryString("applicationid")
    '    valValid = True
    '    valValid1 = True
    '    valasset = True
    '    valyear = True
    '    counter = 0

    '    'For i = 0 To dtgAttribute.Items.Count - 1
    '    '    chkdtg = New CheckBox
    '    '    chkdtg = CType(dtgAttribute.Items(i).FindControl("chkAttribute"), CheckBox)
    '    '    If chkdtg.Checked = False Then
    '    '        valasset = False
    '    '    End If
    '    'Next

    '    'For i = 0 To DtgAsset.Items.Count - 1
    '    '    chkdtg = New CheckBox
    '    '    chkdtg = CType(DtgAsset.Items(i).FindControl("chkGridSelect"), CheckBox)
    '    '    If chkdtg.Checked = False Then
    '    '        valyear = False
    '    '    End If
    '    'Next

    '    'If (chkName.Checked = False) Or (ChkAddress.Checked = False) Or (chkAsset.Checked = False) Or (chkUsage.Checked = False) Or (chkYear.Checked = False) _
    '    '    Or (chkPeriode.Checked = False) Or (chkLamaAsuransi.Checked = False) Or (chkAccessories.Checked = False) Or (valasset = False) Or (valyear = False) _
    '    '    Or (chkSPPADate.Checked = False) Or (chkSumInsured.Checked = False) Or (chkChasis.Checked = False) Or (chkMachine.Checked = False) _
    '    '    Or (chkTagihan.Checked = False) Then

    '    ' Bila CheckNamenya tidak di check !!
    '    '=============== ADD
    '    If chkSPPADate.Checked = False Then
    '        counter = counter + 1
    '        lObjChkBox = New HtmlInputCheckBox
    '        lObjChkBox.ID = "chkNewSPPADate"
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If

    '        genTemplateDiv("Tanggal SPPA", lblSPPADate.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '    End If

    '    If chkSumInsured.Checked = False Then
    '        counter = counter + 1
    '        lObjChkBox = New HtmlInputCheckBox
    '        lObjChkBox.ID = "chkNewSumInsured"
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If

    '        genTemplateDiv("Nilai Pertanggungan", lblSumInsured.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '    End If
    '    If chkChasis.Checked = False Then
    '        counter = counter + 1
    '        lObjChkBox = New HtmlInputCheckBox
    '        lObjChkBox.ID = "chkNewChasis"
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If

    '        genTemplateDiv("No Rangka", lblChasis.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '    End If
    '    If chkMachine.Checked = False Then
    '        counter = counter + 1
    '        lObjChkBox = New HtmlInputCheckBox
    '        lObjChkBox.ID = "chkNewMachine"
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If

    '        genTemplateDiv("No Mesin", lblMachine.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '    End If
    '    If chkTagihan.Checked = False Then
    '        counter = counter + 1
    '        lObjChkBox = New HtmlInputCheckBox
    '        lObjChkBox.ID = "chkNewTagihan"
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If

    '        genTemplateDiv("Premi Ke Pers Asuransi", lblTagihan.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '    End If

    '    '========================================

    '    If chkName.Checked = False Then
    '        counter = counter + 1

    '        lObjChkBox = New HtmlInputCheckBox
    '        lObjChkBox.ID = "chkNewName"
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If

    '        genTemplateDiv("Customer Name", hplCustomerName.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '    End If

    '    ' Bila ChkAddress tidak di check !!

    '    If ChkAddress.Checked = False Then
    '        counter = counter + 1

    '        lObjChkBox = New HtmlInputCheckBox
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If


    '        genTemplateDiv("Address", LblAddress.Text, lObjChkBox, String.Empty, pnlValidationDiv)

    '    End If



    '    ' Bila CheckAssetnya tidak di check !!

    '    If chkAsset.Checked = False Then
    '        counter = counter + 1

    '        lObjChkBox = New HtmlInputCheckBox
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If
    '        lObjChkBox.ID = "chkNewAsset"

    '        genTemplateDiv("Asset Description", lnkAsset.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '    End If

    '    ' Bila CheckUsagenya tidak di check !!

    '    If chkUsage.Checked = False Then
    '        counter = counter + 1

    '        lObjChkBox = New HtmlInputCheckBox
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If
    '        lObjChkBox.ID = "chkNewUsage"

    '        genTemplateDiv("Usage", lblUsage.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '    End If
    '    ' Bila CheckYearnya tidak di check !!

    '    If chkYear.Checked = False Then
    '        counter = counter + 1
    '        lObjChkBox = New HtmlInputCheckBox
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If
    '        lObjChkBox.ID = "chkNewYear"

    '        genTemplateDiv("Year", lblYear.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '    End If

    '    c = 0
    '    y = dtgAttribute.Items.Count
    '    Dim assetname(y) As String
    '    Dim assetattribute(y) As String

    '    For i = 0 To dtgAttribute.Items.Count - 1
    '        chkdtg = New CheckBox
    '        chkdtg = CType(dtgAttribute.Items(i).FindControl("chkAttribute"), CheckBox)
    '        If chkdtg.Checked = False Then
    '            counter = counter + 1
    '            assetname(c) = dtgAttribute.Items(i).Cells(0).Text
    '            assetattribute(c) = dtgAttribute.Items(i).Cells(1).Text
    '            If assetattribute(c) = "&nbsp;" Then assetattribute(c) = ""

    '            'lObjTblRow = New HtmlTableRow

    '            lObjLbl = New HtmlInputHidden
    '            lObjLbl.Visible = False
    '            lObjLbl.ID = "lblNewAssetName" & c
    '            lObjLbl.Value = assetname(c)



    '            lObjLbl1 = New HtmlInputHidden
    '            lObjLbl1.Visible = False
    '            lObjLbl1.ID = "lblNewAssetAttribute" & c
    '            lObjLbl1.Value = assetattribute(c)

    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If
    '            lObjChkBox.ID = "chkNewAttribute" & c

    '            genTemplateDiv(assetname(c), lObjLbl, assetattribute(c), lObjLbl1, String.Empty, lObjChkBox, pnlValidationDiv)
    '            c = c + 1
    '        End If
    '    Next

    '    ' Bila Checkperiodeenya tidak di check !!
    '    If chkPeriode.Checked = False Then
    '        counter = counter + 1

    '        lObjChkBox = New HtmlInputCheckBox
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If
    '        lObjChkBox.ID = "chkNewPeriode"

    '        genTemplateDiv("Insurance Period", lblPeriode.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '    End If

    '    ' Bila chkLamaAsuransi tidak di check !!
    '    If chkLamaAsuransi.Checked = False Then
    '        counter = counter + 1

    '        lObjChkBox = New HtmlInputCheckBox
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If
    '        lObjChkBox.ID = "chkNewLamaAsuransi"

    '        genTemplateDiv("Insurance Length", lblLamaAsuransi.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '    End If

    '    ' Bila chkAccessories tidak di check !!

    '    If chkAccessories.Checked = False Then
    '        counter = counter + 1

    '        lObjChkBox = New HtmlInputCheckBox
    '        If pblnIsFill Then
    '            lObjChkBox.Disabled = False
    '        Else
    '            lObjChkBox.Disabled = True
    '        End If

    '        lObjChkBox.ID = "chkNewAccessories"

    '        genTemplateDiv("Accessories", lblAccessories.Text, lObjChkBox, String.Empty, pnlValidationDiv)
    '    End If

    '    '=============================================================


    '    d = 0
    '    x = DtgAsset.Items.Count
    '    Dim yearnum(x) As String
    '    Dim coverage(x) As String
    '    Dim tpl(x) As String
    '    Dim rscc(x) As String
    '    Dim flood(x) As String
    '    Dim loading(x) As String

    '    For i = 0 To DtgAsset.Items.Count - 1
    '        chkdtg = New CheckBox
    '        chkdtg = CType(DtgAsset.Items(i).FindControl("chkGridSelect"), CheckBox)

    '        If chkdtg.Checked = False Then
    '            counter = counter + 1
    '            PnlValid2.Visible = True

    '            yearnum(d) = DtgAsset.Items(i).Cells(0).Text
    '            coverage(d) = DtgAsset.Items(i).Cells(1).Text
    '            tpl(d) = DtgAsset.Items(i).Cells(2).Text
    '            rscc(d) = DtgAsset.Items(i).Cells(3).Text
    '            flood(d) = DtgAsset.Items(i).Cells(4).Text
    '            loading(d) = DtgAsset.Items(i).Cells(5).Text

    '            'lObjTblRow = New HtmlTableRow
    '            row = genDiv("form_box")

    '            lObjLbl = New HtmlInputHidden
    '            lObjLbl.Visible = False
    '            lObjLbl.ID = "lblNewYearNum" & d
    '            lObjLbl.Value = yearnum(d)

    '            innerRow = genDiv("form_eighten")
    '            insertDiv(innerRow, genLabel(yearnum(d)), genCustom(lObjLbl))
    '            insertDiv(row, innerRow)

    '            innerRow = genDiv("form_eighten")
    '            insertDiv(innerRow, genLabel(coverage(d)))
    '            insertDiv(row, innerRow)


    '            innerRow = genDiv("form_eighten")
    '            insertDiv(innerRow, genLabel(tpl(d)))
    '            insertDiv(row, innerRow)

    '            innerRow = genDiv("form_eighten")
    '            insertDiv(innerRow, genLabel(rscc(d)))
    '            insertDiv(row, innerRow)

    '            innerRow = genDiv("form_eighten")
    '            insertDiv(innerRow, genLabel(flood(d)))
    '            insertDiv(row, innerRow)

    '            innerRow = genDiv("form_eighten")
    '            insertDiv(innerRow, genLabel(loading(d)))
    '            insertDiv(row, innerRow)

    '            lObjChkBox = New HtmlInputCheckBox
    '            If pblnIsFill Then
    '                lObjChkBox.Disabled = False
    '            Else
    '                lObjChkBox.Disabled = True
    '            End If
    '            lObjChkBox.ID = "chkNewAsset" & d


    '            innerRow = genDiv("form_eighten")
    '            insertDiv(innerRow, genCustom(lObjChkBox))
    '            insertDiv(row, innerRow)

    '            pnlValidationDiv2.Controls.Add(row)

    '            'tblValidation2.Rows.Add(lObjTblRow)
    '            d = d + 1
    '        End If
    '    Next

    '    PnlValid.Visible = True
    '    PnlBtn.Visible = True
    '    Panel1.Visible = False
    '    valValid = False
    '    valValid1 = False
    '    Else
    '    valValid1 = True
    '    End If

    '    '======================================================================
    '    'Check If data billing sama dgn database atau tidak
    '    Dim lSumInsured As String
    '    Dim tSumInsured As String
    '    Dim dbllSumInsured As Double
    '    Dim dbltSumInsured As Double
    '    Dim lChasis As String
    '    Dim tChasis As String
    '    Dim lMachine As String
    '    Dim tMachine As String
    '    Dim lTagihan As String
    '    Dim tTagihan As String

    '    lSumInsured = lblSumInsured.Text.Trim
    '    tSumInsured = lblSumInsured.Text.Trim
    '    dbllSumInsured = CType(lSumInsured, Double)
    '    dbltSumInsured = CType(tSumInsured, Double)

    '    lChasis = lblChasis.Text
    '    tChasis = lblChasis.Text.Trim
    '    lMachine = lblMachine.Text
    '    tMachine = lblMachine.Text.Trim
    '    lTagihan = lblTagihan.Text
    '    tTagihan = lblTagihan.Text.Trim
    '    'valValid1 = True
    '    If (dbllSumInsured <> dbltSumInsured) Or (lChasis <> tChasis) Or (lMachine <> tMachine) Or (lTagihan <> tTagihan) Then

    '        If dbllSumInsured <> dbltSumInsured Then
    '            counter = counter + 1
    '            lObjTxtBox = New HtmlInputText

    '            With lObjTxtBox
    '                .ID = "txtNewSumInsured"
    '                If pblnIsFill Then
    '                    .Disabled = False
    '                Else
    '                    .Disabled = True
    '                End If
    '                .Value = tSumInsured
    '            End With

    '            genTemplateDiv("Nilai Pertanggungan", New Label, String.Empty, lObjTxtBox, lSumInsured, New Label, pnlValidationDiv3)
    '            valValid1 = False
    '        End If

    '        If lChasis <> tChasis Then
    '            counter = counter + 1
    '            lObjTxtBox = New HtmlInputText

    '            With lObjTxtBox
    '                .ID = "txtNewChasis"
    '                If pblnIsFill Then
    '                    .Disabled = False
    '                Else
    '                    .Disabled = True
    '                End If
    '                .Value = tChasis
    '            End With
    '            genTemplateDiv("Chasis Number", New Label, String.Empty, lObjTxtBox, lChasis, New Label, pnlValidationDiv3)
    '            valValid1 = False
    '        End If

    '        'Status = valValid1.ToString

    '        If lMachine <> tMachine Then
    '            counter = counter + 1
    '            lObjTxtBox = New HtmlInputText
    '            With lObjTxtBox
    '                .ID = "txtNewMachine"
    '                If pblnIsFill Then
    '                    .Disabled = False
    '                Else
    '                    .Disabled = True
    '                End If
    '                .Value = tMachine
    '            End With
    '            genTemplateDiv("Machine Number", New Label, String.Empty, lObjTxtBox, lMachine, New Label, pnlValidationDiv3)
    '            valValid1 = False
    '        End If



    '        valValid1 = False
    '    Else
    '        valValid1 = True
    '    End If
    '    Status = valValid1.ToString

    '    pnlBilling.Visible = True
    '    PnlBtn.Visible = True
    '    Panel1.Visible = False
    'End Sub
    Sub StepNext3()
        Try


            assetseqno = Request.QueryString("assetseqno")
            inssequenceno = Request.QueryString("inssequenceno")
            ApplicationID = Request.QueryString("applicationid")
            Connection()

            Dim OUpdateInsAsset As New StringBuilder
            OUpdateInsAsset.Append(" UPDATE InsuranceAsset ")
            OUpdateInsAsset.Append(" SET PolicyReceiveDate = @PolicyReceiveDate, ")
            OUpdateInsAsset.Append(" FlagDocStatus = @FlagDocStatus, PolicyNumber = @PolicyNumber , ")
            OUpdateInsAsset.Append(" FlagInsActivation = @FlagInsActivation , ")
            OUpdateInsAsset.Append(" PolicyReceiveBy = @PolicyReceiveBy ")
            OUpdateInsAsset.Append(" WHERE ApplicationID = @ApplicationID ")
            OUpdateInsAsset.Append(" AND AssetSeqNo = @AssetSeqNo AND InsSequenceNo = @InsSequenceNo ")


            cmdSearch = New SqlCommand(OUpdateInsAsset.ToString, conSearch)

            With cmdSearch
                .Parameters.Add(New SqlParameter("@PolicyReceiveDate", SqlDbType.DateTime))
                .Parameters.Add(New SqlParameter("@FlagDocStatus", SqlDbType.Char, 1))
                .Parameters.Add(New SqlParameter("@PolicyNumber", SqlDbType.VarChar, 25))
                .Parameters.Add(New SqlParameter("@FlagInsActivation", SqlDbType.Char, 4))
                .Parameters.Add(New SqlParameter("@PolicyReceiveBy", SqlDbType.Char, 30))
                .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))

                .Parameters("@PolicyReceiveDate").Value = Me.BusinessDate
                .Parameters("@FlagDocStatus").Value = "E"
                .Parameters("@PolicyNumber").Value = txtNoPolis.Text
                .Parameters("@FlagInsActivation").Value = "N"
                .Parameters("@PolicyReceiveBy").Value = Me.Loginid
                .Parameters("@ApplicationID").Value = ApplicationID
                .Parameters("@AssetSeqNo").Value = assetseqno
                .Parameters("@InsSequenceNo").Value = inssequenceno

                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
            End With


        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("PolicyBillings.aspx.vb", "Endors Proses 1", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try

        '====================Create data to EndorsmentHistory=================
        'Get Endorske from EndorsmentHistory

        Dim strSqlEndorsmentHistory As New StringBuilder
        strSqlEndorsmentHistory.Append(" SELECT isnull(Max(Endorske),'0') as endorske FROM EndorsmentHistory ")
        strSqlEndorsmentHistory.Append(" WHERE ApplicationID ='" & Replace(ApplicationID.Trim, "'", "") & "' ")
        strSqlEndorsmentHistory.Append(" AND AssetSeqNo ='" & Replace(assetseqno.Trim, "'", "") & "' ")
        strSqlEndorsmentHistory.Append(" AND InsSequenceNo ='" & Replace(inssequenceno.Trim, "'", "") & "' ")


        Dim objAdapter As New SqlDataAdapter(strSqlEndorsmentHistory.ToString, conSearch)

        dsSearch = New DataSet
        objAdapter.Fill(dsSearch, "EndorsmentHistory")

        Dim endorske As Integer
        Dim objRow1 As DataRowView
        If dsSearch.Tables("EndorsmentHistory").Rows.Count > 0 Then
            objRow1 = dsSearch.Tables("EndorsmentHistory").DefaultView(0)
            endorske = CType(objRow1("Endorske"), Integer)
        Else
            endorske = 0
        End If

        'Generate Nomor Transaksi
        Dim notransaksi As String
        ' ======================== Generate Nomor Transaksi ==========================
        Try

            Dim cmd As SqlCommand
            Dim objparam As SqlParameter
            'Declare @no as varchar(100)
            'exec (spGetNoTransaction) '900', '7/1/2003','ENDORS', @no output
            'print @no

            'Dim sqlgenerate As String = "select dbo.spGenerateNoTransaction ('" & Me.BranchID & "', '" & Me.BusinessDate & "','ENDORS') as NoTransaksi"
            'Dim sqlgenerate As String = "exec spGetNoTransaction  '" & Me.BranchID & "' , '" & Me.BusinessDate & " ','ENDORS, @No OutPut "
            cmd = New SqlCommand("spGetNoTransaction", conSearch)
            cmd.CommandType = CommandType.StoredProcedure

            With cmd
                .Connection.Open()
                .Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3))
                .Parameters("@BranchID").Value = Me.BranchID
                .Parameters.Add(New SqlParameter("@BusinessDate", SqlDbType.SmallDateTime, 4))
                .Parameters("@BusinessDate").Value = Me.BusinessDate
                .Parameters.Add(New SqlParameter("@ID", SqlDbType.Char, 10))
                .Parameters("@ID").Value = "ENDORS"
                objparam = .Parameters.Add(New SqlParameter("@sequenceNo", SqlDbType.VarChar, 20))
                objparam.Direction = ParameterDirection.Output
                .ExecuteReader()
                .Connection.Close()
                notransaksi = CType(objparam.Value, String)
            End With

            Context.Trace.Write("No Transaksi = " & notransaksi)

        Catch ex As Exception
            Dim errtrans As New MaxiloanExceptions
            errtrans.WriteLog("InsuranceEndorsValid.aspx.vb", "Generate Nomor Transaksi EndorsDocNo ", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Context.Trace.Write("Error Di Generate No Transaction ")
        End Try

        'Insert data to EndorsmentHistory
        Dim oUpdateEHistory As New StringBuilder

        oUpdateEHistory.Append("INSERT INTO EndorsmentHistory ")
        oUpdateEHistory.Append(" (BranchID, ApplicationID, AssetSeqNo, InsSequenceNo, ")
        oUpdateEHistory.Append(" Endorske, EndorsDate , EndorsDocNo ) ")
        oUpdateEHistory.Append("   VALUES   ")
        oUpdateEHistory.Append("  ( @BranchID, @ApplicationID, @AssetSeqNo, @InsSequenceNo, ")
        oUpdateEHistory.Append(" @Endorske, @EndorsDate, @EndorsDocNo) ")

        cmdSearch = New SqlCommand(oUpdateEHistory.ToString, conSearch)

        With cmdSearch

            .Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 20))
            .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
            .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
            .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))
            .Parameters.Add(New SqlParameter("@Endorske", SqlDbType.SmallInt, 2))
            .Parameters.Add(New SqlParameter("@EndorsDate", SqlDbType.SmallDateTime, 4))
            .Parameters.Add(New SqlParameter("@EndorsDocNo", SqlDbType.Char, 30))

            .Parameters("@BranchID").Value = BranchID
            .Parameters("@ApplicationID").Value = ApplicationID
            .Parameters("@AssetSeqNo").Value = assetseqno
            .Parameters("@InsSequenceNo").Value = inssequenceno
            .Parameters("@Endorske").Value = endorske + 1
            .Parameters("@EndorsDate").Value = Me.BusinessDate
            .Parameters("@EndorsDocNo").Value = notransaksi

            .Connection.Open()
            .ExecuteNonQuery()
            .Connection.Close()
        End With

        '============= Update EndorsDocNo di tbl InsuranceAsset

        Dim oUpdateEndorsDocNo As New StringBuilder
        oUpdateEndorsDocNo.Append("  UPDATE InsuranceAsset  ")
        oUpdateEndorsDocNo.Append(" SET EndorsDocNo = @EndorsDocNo ")
        oUpdateEndorsDocNo.Append(" WHERE ApplicationID = @ApplicationID ")
        oUpdateEndorsDocNo.Append(" AND AssetSeqNo = @AssetSeqNo ")
        oUpdateEndorsDocNo.Append(" AND InsSequenceNo = @InsSequenceNo ")


        cmdSearch = New SqlCommand(oUpdateEndorsDocNo.ToString, conSearch)
        With cmdSearch
            .Parameters.Add(New SqlParameter("@EndorsDocNo", SqlDbType.Char, 30))
            .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
            .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
            .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))
            .Parameters("@EndorsDocNo").Value = notransaksi
            .Parameters("@ApplicationID").Value = ApplicationID
            .Parameters("@AssetSeqNo").Value = assetseqno
            .Parameters("@InsSequenceNo").Value = inssequenceno

            .Connection.Open()
            .ExecuteNonQuery()
            .Connection.Close()
        End With





        '===== End Create ==================================
        '==========Create data to EndorsmentHistoryDetail==========

        Dim counter As Integer
        'Gen_Table(False, counter, c, d)
        Dim arrdata(counter) As String
        Dim arrisidata(counter) As String
        Dim arrdatabenar(counter) As String
        Dim i As Integer
        i = 0

        Dim chk As HtmlInputCheckBox
        Dim txt As HtmlInputText
        Dim lblAsset As HtmlInputHidden
        Dim lblAssetAtt As HtmlInputHidden
        Dim lblThn As HtmlInputHidden


        'chk = CType(tblValidation.FindControl("chkNewName"), HtmlInputCheckBox)
        'chk = CType(pnlValidationDiv.FindControl("chkNewName"), HtmlInputCheckBox)
        'If Not IsNothing(chk) Then
        '    arrdata(i) = "Customer Name"
        '    arrisidata(i) = ""
        '    arrdatabenar(i) = hplCustomerName.Text
        '    i = i + 1
        'End If

        ''chk = CType(tblValidation.FindControl("chkNewAddress"), HtmlInputCheckBox)
        'chk = CType(pnlValidationDiv.FindControl("chkNewAddress"), HtmlInputCheckBox)
        'If Not IsNothing(chk) Then
        '    arrdata(i) = "Address"
        '    arrisidata(i) = ""
        '    arrdatabenar(i) = LblAddress.Text
        '    i = i + 1
        'End If

        ''chk = CType(tblValidation.FindControl("chkNewAsset"), HtmlInputCheckBox)
        'chk = CType(pnlValidationDiv.FindControl("chkNewAsset"), HtmlInputCheckBox)
        'If Not IsNothing(chk) Then
        '    arrdata(i) = "Asset Description"
        '    arrisidata(i) = ""
        '    arrdatabenar(i) = lnkAsset.Text
        '    i = i + 1
        'End If

        ''chk = CType(tblValidation.FindControl("chkNewUsage"), HtmlInputCheckBox)
        'chk = CType(pnlValidationDiv.FindControl("chkNewUsage"), HtmlInputCheckBox)
        'If Not IsNothing(chk) Then
        '    arrdata(i) = "Usage"
        '    arrisidata(i) = ""
        '    arrdatabenar(i) = lblUsage.Text
        '    i = i + 1
        'End If

        ''chk = CType(tblValidation.FindControl("chkNewYear"), HtmlInputCheckBox)
        'chk = CType(pnlValidationDiv.FindControl("chkNewYear"), HtmlInputCheckBox)
        'If Not IsNothing(chk) Then
        '    arrdata(i) = "Year"
        '    arrisidata(i) = ""
        '    arrdatabenar(i) = lblYear.Text
        '    i = i + 1
        'End If

        ''check chkNewAttributec
        'Dim assetname(c) As String
        'Dim assetattribute(c) As String

        'Dim r As Integer
        'For r = 0 To c - 1
        '    'chk = CType(tblValidation.FindControl("chkNewAttribute" & r), HtmlInputCheckBox)
        '    chk = CType(pnlValidationDiv.FindControl("chkNewAttribute" & r), HtmlInputCheckBox)
        '    'lblAsset = CType(tblValidation.FindControl("lblNewAssetName" & r), HtmlInputHidden)
        '    lblAsset = CType(pnlValidationDiv.FindControl("lblNewAssetName" & r), HtmlInputHidden)
        '    'lblAssetAtt = CType(tblValidation.FindControl("lblNewAssetAttribute" & r), HtmlInputHidden)
        '    lblAssetAtt = CType(pnlValidationDiv.FindControl("lblNewAssetAttribute" & r), HtmlInputHidden)
        '    If Not IsNothing(chk) Then
        '        arrdata(i) = lblAsset.Value
        '        arrisidata(i) = ""
        '        arrdatabenar(i) = lblAssetAtt.Value
        '        i = i + 1
        '    End If
        'Next
        ''end check

        ''chk = CType(tblValidation.FindControl("chkNewPeriode"), HtmlInputCheckBox)
        'chk = CType(pnlValidationDiv.FindControl("chkNewPeriode"), HtmlInputCheckBox)
        'If Not IsNothing(chk) Then
        '    arrdata(i) = "Insurance Period"
        '    arrisidata(i) = ""
        '    arrdatabenar(i) = lblPeriode.Text
        '    i = i + 1
        'End If

        ''chk = CType(tblValidation.FindControl("chkNewLamaAsuransi"), HtmlInputCheckBox)
        'chk = CType(pnlValidationDiv.FindControl("chkNewLamaAsuransi"), HtmlInputCheckBox)
        'If Not IsNothing(chk) Then
        '    arrdata(i) = "Insurance Length"
        '    arrisidata(i) = ""
        '    arrdatabenar(i) = lblLamaAsuransi.Text
        '    i = i + 1
        'End If

        ''chk = CType(tblValidation.FindControl("chkNewAccessories"), HtmlInputCheckBox)
        'chk = CType(pnlValidationDiv.FindControl("chkNewAccessories"), HtmlInputCheckBox)
        'If Not IsNothing(chk) Then
        '    arrdata(i) = "Accessories"
        '    arrisidata(i) = ""
        '    arrdatabenar(i) = lblAccessories.Text
        '    i = i + 1
        'End If


        ''Check Assetd
        'Dim yearnum(d) As Integer
        'Dim coverage(d) As Integer

        'Dim s As Integer
        'For s = 0 To d - 1
        '    'chk = CType(tblValidation.FindControl("chkNewAsset" & s), HtmlInputCheckBox)
        '    chk = CType(pnlValidationDiv.FindControl("chkNewAsset" & s), HtmlInputCheckBox)
        '    'lblThn = CType(tblValidation.FindControl("lblNewYearNum" & s), HtmlInputHidden)
        '    lblThn = CType(pnlValidationDiv.FindControl("lblNewYearNum" & s), HtmlInputHidden)

        '    If Not IsNothing(chk) Then
        '        arrdata(i) = "Thn " & lblThn.Value & " Coverage/TPL/RSCC/Flood/Loading"
        '        arrisidata(i) = "-"
        '        arrdatabenar(i) = DtgAsset.Items(s).Cells(1).Text & "/" & DtgAsset.Items(s).Cells(2).Text & "/" & DtgAsset.Items(s).Cells(3).Text & "/" & DtgAsset.Items(s).Cells(4).Text & "/" & DtgAsset.Items(s).Cells(5).Text
        '        i = i + 1
        '    End If
        'Next

        'txt = CType(pnlValidationDiv.FindControl("txtNewSumInsured"), HtmlInputText)
        'If Not IsNothing(txt) Then
        '    arrdata(i) = "Sum Insured"
        '    If lblSumInsured.Text = "" Then arrisidata(i) = "-" Else arrisidata(i) = lblSumInsured.Text
        '    'arrisidata(i) = lblSumInsured.Text
        '    arrdatabenar(i) = lblSumInsured.Text
        '    i = i + 1
        'End If

        'txt = CType(pnlValidationDiv.FindControl("txtNewTagihan"), HtmlInputText)
        'If Not IsNothing(txt) Then
        '    arrdata(i) = "Premium"
        '    If lblTagihan.Text = "" Then arrisidata(i) = "-" Else arrisidata(i) = lblTagihan.Text
        '    arrdatabenar(i) = lblTagihan.Text
        '    i = i + 1
        'End If

        'Insert Data to EndorsmentHistoryDetail
        Dim x As Integer

        For x = 0 To i - 1

            Dim oUpdateDetail As New StringBuilder
            oUpdateDetail.Append(" INSERT INTO EndorsmentHistoryDetail ")
            oUpdateDetail.Append(" (BranchID, ApplicationID, AssetSeqNo, InsSequenceNo, Endorske, EndorsItem, DataSalahDesc, DataBenarDesc) ")
            oUpdateDetail.Append(" VALUES ")
            oUpdateDetail.Append(" (@BranchId, @ApplicationID, @AssetSeqNo, @InsSequenceNo, @Endorske, @EndorsItem, @DataSalahDesc, @DataBenarDesc) ")

            cmdSearch = New SqlCommand(oUpdateDetail.ToString, conSearch)
            Context.Trace.Write("sql = " & oUpdateDetail.ToString)

            With cmdSearch

                .Parameters.Add(New SqlParameter("@BranchId", SqlDbType.Char, 3))
                .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@AssetSeqNo", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@InsSequenceNo", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@Endorske", SqlDbType.SmallInt, 2))
                .Parameters.Add(New SqlParameter("@EndorsItem", SqlDbType.VarChar, 30))
                .Parameters.Add(New SqlParameter("@DataSalahDesc", SqlDbType.VarChar, 50))
                .Parameters.Add(New SqlParameter("@DataBenarDesc", SqlDbType.VarChar, 50))

                .Parameters("@BranchId").Value = BranchID
                .Parameters("@ApplicationID").Value = ApplicationID
                .Parameters("@AssetSeqNo").Value = assetseqno
                .Parameters("@InsSequenceNo").Value = inssequenceno
                .Parameters("@Endorske").Value = endorske + 1
                .Parameters("@EndorsItem").Value = arrdata(x)
                .Parameters("@DataSalahDesc").Value = arrisidata(x)
                .Parameters("@DataBenarDesc").Value = arrdatabenar(x)

                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()
                Me.assetseqno = assetseqno
                Me.inssequenceno = inssequenceno
            End With
        Next
        'cetak surat...ambil counter cetak surat ke..berapa ?

        Dim cmdctksurat As SqlCommand
        Dim drdctksurat As SqlDataReader
        Dim strSqlcetaksurat As New StringBuilder
        Dim countercetaksurat As Integer
        strSqlcetaksurat.Append(" SELECT isnull(mailprintednum,'0') as mailprintednum  FROM mailtransaction ")
        strSqlcetaksurat.Append(" WHERE ApplicationID ='" & Replace(ApplicationID.Trim, "'", "") & "' ")
        strSqlcetaksurat.Append(" AND branchid ='" & Replace(BranchID.Trim, "'", "") & "' ")
        strSqlcetaksurat.Append(" AND MailTypeID = 'ENDORS' ")
        conSearch.Open()
        cmdSearch = New SqlCommand(strSqlcetaksurat.ToString, conSearch)
        drdctksurat = cmdSearch.ExecuteReader
        If drdctksurat.Read Then
            countercetaksurat = CType(drdctksurat("mailprintednum"), Integer)
        End If
        conSearch.Close()

        'Insert data ke mail transaction

        Try

            Dim oInsertMail As New StringBuilder
            oInsertMail.Append(" INSERT INTO MAILTRANSACTION ")
            oInsertMail.Append(" (BranchID, MailTypeID , MailTransNo, ")
            oInsertMail.Append(" applicationid, MailDateCreate, MailUserCreate, ")
            oInsertMail.Append(" MailDatePrint, MailPrintedNum ) ")
            oInsertMail.Append(" VALUES ")
            oInsertMail.Append(" (@BranchID, @MailTypeID, @MailTransNo ,  ")
            oInsertMail.Append(" @ApplicationID, @MailDateCreate, @MailUserCreate , ")
            oInsertMail.Append(" @MailDatePrint, @MailPrintedNum )")

            cmdSearch = New SqlCommand(oInsertMail.ToString, conSearch)
            Context.Trace.Write("sql = " & oInsertMail.ToString)
            With cmdSearch
                .Parameters.Add(New SqlParameter("@BranchID", SqlDbType.Char, 3))
                .Parameters.Add(New SqlParameter("@MailTypeID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@MailTransNo", SqlDbType.Char, 30))
                .Parameters.Add(New SqlParameter("@ApplicationID", SqlDbType.Char, 20))
                .Parameters.Add(New SqlParameter("@MailDateCreate", SqlDbType.DateTime))
                .Parameters.Add(New SqlParameter("@MailUserCreate", SqlDbType.Char, 50))
                .Parameters.Add(New SqlParameter("@MailDatePrint", SqlDbType.DateTime))
                .Parameters.Add(New SqlParameter("@MailPrintedNum", SqlDbType.Int))

                .Parameters("@BranchID").Value = BranchID
                .Parameters("@MailTypeID").Value = "ENDORS"
                .Parameters("@MailTransNo").Value = notransaksi
                .Parameters("@ApplicationID").Value = ApplicationID
                .Parameters("@MailDateCreate").Value = Me.BusinessDate
                .Parameters("@MailUserCreate").Value = Me.Loginid
                .Parameters("@MailDatePrint").Value = Me.BusinessDate
                .Parameters("@MailPrintedNum").Value = countercetaksurat + 1

                .Connection.Open()
                .ExecuteNonQuery()
                .Connection.Close()

                Me.ApplicationID = ApplicationID

            End With


        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("PolicyBillings.aspx.vb", "Endors - Cetaik Mail", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Throw New Exception(ex.Message)
        End Try
    End Sub
    Private Function StepChek() As Boolean
        Dim result As Boolean
        Dim valasset As Boolean
        Dim valyear As Boolean
        Dim chkdtg As CheckBox

        agreementno = Request.QueryString("agreementno")
        assetseqno = Request.QueryString("assetseqno")
        inssequenceno = Request.QueryString("inssequenceno")
        ApplicationID = Request.QueryString("applicationid")

        valasset = True
        valyear = True

        'For i = 0 To dtgAttribute.Items.Count - 1
        '    chkdtg = New CheckBox
        '    chkdtg = CType(dtgAttribute.Items(i).FindControl("chkAttribute"), CheckBox)
        '    If chkdtg.Checked = False Then
        '        valasset = False
        '    End If
        'Next

        'For i = 0 To DtgAsset.Items.Count - 1
        '    chkdtg = New CheckBox
        '    chkdtg = CType(DtgAsset.Items(i).FindControl("chkGridSelect"), CheckBox)
        '    If chkdtg.Checked = False Then
        '        valyear = False
        '    End If
        'Next

        'If (chkName.Checked = False) Or (ChkAddress.Checked = False) Or (chkAsset.Checked = False) Or (chkUsage.Checked = False) Or (chkYear.Checked = False) _
        '    Or (chkPeriode.Checked = False) Or (chkLamaAsuransi.Checked = False) Or (chkAccessories.Checked = False) Or (valasset = False) Or (valyear = False) _
        '    Or (chkSPPADate.Checked = False) Or (chkSumInsured.Checked = False) Or (chkChasis.Checked = False) Or (chkMachine.Checked = False) _
        '    Or (chkTagihan.Checked = False) Then
        '    result = False
        'Else
        '    result = True
        'End If

        Return result
    End Function
#Region "SendCookies"
    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_ENDORS_LETTER_PRINT)
        Dim strWhere As New StringBuilder
        strWhere.Append(" And InsuranceAsset.BranchID='" & Replace(Me.BranchID, "'", "") & "' ")
        strWhere.Append(" And InsuranceAsset.ApplicationID='" & Replace(Me.ApplicationID, "'", "") & "' ")
        strWhere.Append(" And InsuranceAsset.AssetSeqNo='" & Replace(Me.assetseqno, "'", "") & "' ")
        strWhere.Append(" And InsuranceAsset.InsSequenceNo='" & Replace(Me.inssequenceno, "'", "") & "' ")
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = strWhere.ToString
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_ENDORS_LETTER_PRINT)
            cookieNew.Values("SearchBy") = strWhere.ToString
            Response.AppendCookie(cookieNew)
        End If
    End Sub

#End Region
End Class