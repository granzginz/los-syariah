﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
#End Region

Public Class PopUpEndorsHistory
    Inherits Maxiloan.Webform.WebBased

#Region "Property"


    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return (CType(viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

#End Region

#Region "Private Constanta"

    Private oController As New PolicyDetailController
    Private mController As New SPPAController

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Me.ApplicationID = Replace(Request.QueryString("ApplicationID"), "'", "")
            Me.AgreementNo = Replace(Request.QueryString("AgreementNo"), "'", "")
            Me.BranchID = Request.QueryString("BranchID")
            Dim CmdWhere As String = "  Where dbo.OldInsuranceAssetDetail.ApplicationId = '" & Me.ApplicationID & "' "
            '=================================== ENDORSMENT HISTORY =======================
            Dim oEntitiesEndors As New Parameter.InsCoAllocationDetailList

            With oEntitiesEndors
                .WhereCond = CmdWhere
                .strConnection = GetConnectionString
            End With

            Try
                oEntitiesEndors = oController.GetPopUpEndorsmentHistory(oEntitiesEndors)
            Catch ex As Exception
            End Try

            With oEntitiesEndors
                LblAgreementNo.Text = Me.AgreementNo
                LblPolicyNo.Text = .PolicyNumber
                LblEndorsPolicyNo.Text = .BDEndorsPolicyNo
                LblEndorsDate.Text = CType(.BDEndorsDate, String)
                LblEndorsDocNo.Text = .BDEndorsDocNo
            End With


            '===================================== GRID ENDORSMENT DETAIL ====================
            Dim oEntities As New Maxiloan.Parameter.SPPA

            With oEntities
                .WhereCond = CmdWhere
                .PageSource = General.CommonVariableHelper.PAGE_SOURCE_GET_ENDORSMENT_HISTORY_DETAIL_POPUP
                .strConnection = GetConnectionString
            End With


            Try
                oEntities = mController.GetListCreateSPPA(oEntities)
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try


            Dim dtEntity As New DataTable

            If Not oEntities Is Nothing Then
                dtEntity = oEntities.ListData
            End If
            DGridEndorsDetail.DataSource = dtEntity
            DGridEndorsDetail.DataBind()


        End If

    End Sub

    
End Class