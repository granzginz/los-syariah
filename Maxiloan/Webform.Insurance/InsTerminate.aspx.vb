﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsTerminate
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oReason As UCReason
    Protected WithEvents txtDeductAmount As ucNumberFormat
    Protected WithEvents txtRefundAmountToCust As ucNumberFormat
    'Protected WithEvents GridNavigator As ucGridNav
#Region "Constanta"
    Dim m_InsTerminate As New InsTerminateController
    Private oInsTerminate As New Parameter.InsTerminate
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private currentPage As Integer = 1
#End Region
#Region "properties"
    Public Property flag() As Boolean
        Get
            Return CType(viewstate("flag"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("flag") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Public Property NilaiAwal() As String
        Get
            Return CType(viewstate("nilaiawal"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("nilaiawal") = Value
        End Set
    End Property
    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property
    Public Property YearNum() As String
        Get
            Return CType(viewstate("YearNum"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("YearNum") = Value
        End Set
    End Property
    Public Property customerID() As String
        Get
            Return CType(viewstate("customerID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("customerID") = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return CType(viewstate("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return CType(viewstate("AssetSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSequenceNo") = Value
        End Set
    End Property
    Public Property RefundAmountToCust() As Double
        Get
            Return CType(viewstate("RefundAmountToCust"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("RefundAmountToCust") = Value
        End Set
    End Property
    Public Property DeductAmount() As Double
        Get
            Return CType(viewstate("DeductAmount"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("DeductAmount") = Value
        End Set
    End Property

    Public Property PremiumToInsCo() As Double
        Get
            Return CType(viewstate("PremiumToInsCo"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("PremiumToInsCo") = Value
        End Set
    End Property
    Public Property PremiumToCust() As Double
        Get
            Return CType(viewstate("PremiumToCust"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("PremiumToCust") = Value
        End Set
    End Property

    Public Property RefundAmountFromInsCo() As Double
        Get
            Return CType(viewstate("RefundAmountFromInsCo"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("RefundAmountFromInsCo") = Value
        End Set
    End Property

    Public Property RefundToSupplier() As Double
        Get
            Return CType(viewstate(" RefundToSupplier"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate(" RefundToSupplier") = Value
        End Set
    End Property
    Public Property TotalRefund() As Double
        Get
            Return CType(viewstate(" TotalRefund"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate(" TotalRefund") = Value
        End Set
    End Property

    Public Property NilaiAkhir() As String
        Get
            Return CType(viewstate("nilaiakhir"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("nilaiakhir") = Value
        End Set
    End Property
    Public Property Notes() As String
        Get
            Return CType(viewstate("Notes"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Notes") = Value
        End Set
    End Property
    Public Property ReasonId() As String
        Get
            Return CType(viewstate("ReasonId"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ReasonId") = Value
        End Set
    End Property

    Public Property DeductPercent() As Double
        Get
            Return CType(viewstate("DeductPercent"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("DeductPercent") = Value
        End Set
    End Property
    Public Property FirstDate() As Date
        Get
            Return CType(viewstate("FirstDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("FirstDate") = Value
        End Set
    End Property
    Public Property StartDate() As Date
        Get
            Return CType(viewstate("StartDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("StartDate") = Value
        End Set
    End Property
    Public Property EndDate() As Date
        Get
            Return CType(viewstate("EndDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("EndDate") = Value
        End Set
    End Property
    Public Property TerminateDate() As Date
        Get
            Return CType(viewstate("TerminateDate"), Date)
        End Get
        Set(ByVal Value As Date)
            viewstate("TerminateDate") = Value
        End Set
    End Property
#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'AddHandler GridNavigator.PageChanged, AddressOf PageNavigation

        If Not Page.IsPostBack Then
            fillcbo()
            Me.flag = False
            Me.FirstDate = Me.BusinessDate

            Me.FormID = "InsTerminate"


            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                oInsTerminate.TerminationDate = Me.BusinessDate

                If Request("strFileLocation") <> "" Then
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String

                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation")

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; " & vbCrLf _
                    & "var y = screen.height; " & vbCrLf _
                    & "window.open('" & strFileLocation & "','Insurance', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                    & "</script>")
                End If 

            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String

                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If

        Me.Notes = ""
        Me.ReasonId = ""


    End Sub

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub btnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub
    'Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
    '    currentPage = e.CurrentPage
    '    Bindgrid(Me.SearchBy, Me.SortBy, True)
    '    GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    'End Sub
    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
#Region "Sub and Functions"
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlView.Visible = False
        pnlCalculate.Visible = False


    End Sub
    'description : sub ini untuk mengisi data ke combo box branch
    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With ddlBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub
    'description : sub ini untuk bind data customer ke datagrid

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String, Optional isFrNav As Boolean = False)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oInsTerminate
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort
        End With

        oInsTerminate = m_InsTerminate.InsTerminateList(oInsTerminate)

        With oInsTerminate
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oInsTerminate.ListData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgClaimRequestList.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgClaimRequestList.DataBind()
        End If

        'If (isFrNav = False) Then
        '    GridNavigator.Initialize(recordCount, pageSize)
        'End If
        PagingFooter()
        pnlDtGrid.Visible = True
        pnlCalculate.Visible = False
        pnlView.Visible = False
    End Sub
    Sub clear()
        lblAgreementNo.Text = ""
        lblAsset.Text = ""
        lblCustomerName.Text = ""
        lblEngineNo.Text = ""
        lblInsCo.Text = ""
        lblContractStatus.Text = ""
        lblTenor.Text = ""
        lblChassisNo.Text = ""
        lblPolicyNo.Text = ""
        lblInsuranceLength.Text = ""
        lblStartDate.Text = ""
        lblEndDate.Text = ""
        lblMainPremiumToCust.Text = ""
        lblPremiumToCust.Text = ""
        lblMainPremiumToInsCo.Text = ""
        lblPremiumToInsCo.Text = ""
        lblisrefund.Text = ""
        txtTerminateDate.Text = ""
        txtNotes.Text = ""

    End Sub
    'description : sub ini untuk menyajikan detil data customer yang mengajukan refund jika klik terminate
    Private Sub viewReportDetail(ByVal BranchID As String, ByVal ApplicationID As String, ByVal AssetSeqNo As Integer, ByVal InsSeqNo As Integer)
        clear()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlView.Visible = True
        dtgMainPremium.Visible = True
        pnlCalculate.Visible = False
        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView

        Dim dt As New DataTable

        With oInsTerminate
            .strConnection = GetConnectionString()
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AssetSequenceNo = Me.AssetSequenceNo
            .InsSequenceNo = Me.InsSequenceNo
            .TerminationDate = Me.TerminateDate
        End With

        oInsTerminate = m_InsTerminate.InsTerminateDetail(oInsTerminate)
        dt = oInsTerminate.ListData
        btnCalculate.Visible = True
        If dt.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            btnCalculate.Visible = False
            btnBack.Visible = False
            Exit Sub
        End If
        With dt.Rows(0)
            lblAgreementNo.Text = CStr(IIf(IsDBNull(.Item("AgreementNo")), "", .Item("AgreementNo")))
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
            lblCustomerName.Text = CStr(IIf(IsDBNull(.Item("Name")), "", .Item("Name")))
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(Me.customerID.Trim) & "')"
            lblAsset.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
            lblInsCo.Text = CStr(IIf(IsDBNull(.Item("InsCo")), "", .Item("InsCo")))
            lblPolicyNo.Text = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
            lblContractStatus.Text = CStr(IIf(IsDBNull(.Item("ContractStatus")), "", .Item("ContractStatus")))
            lblTenor.Text = CStr(IIf(IsDBNull(.Item("Tenor")), "", .Item("Tenor")))
            lblChassisNo.Text = CStr(IIf(IsDBNull(.Item("ChassisNo")), "", .Item("ChassisNo")))
            lblEngineNo.Text = CStr(IIf(IsDBNull(.Item("EngineNo")), "", .Item("EngineNo")))
            lblisrefund.Text = CStr(IIf(IsDBNull(.Item("IsRefundable")), "", .Item("IsRefundable")))
            lblInsuranceLength.Text = CStr(IIf(IsDBNull(.Item("InsuranceLength")), "", .Item("InsuranceLength")))
            lblStartDate.Text = CDate(CStr(IIf(IsDBNull(.Item("StartDate")), "", .Item("StartDate")))).ToString("dd/MM/yyyy")
            lblEndDate.Text = CDate(CStr(IIf(IsDBNull(.Item("EndDate")), "", .Item("EndDate")))).ToString("dd/MM/yyyy")
            If lblisrefund.Text = "1" Then
                lblisrefund.Text = "Yes"
            Else
                lblisrefund.Text = "No"
            End If
            lblMainPremiumToCust.Text = FormatNumber(IIf(IsDBNull(.Item("MainPremiumToCustomer")), "", .Item("MainPremiumToCustomer")), 2)
            lblPremiumToCust.Text = FormatNumber(IIf(IsDBNull(.Item("PremiumAmountToCustomer")), "", .Item("PremiumAmountToCustomer")), 2)
            lblMainPremiumToInsCo.Text = FormatNumber(IIf(IsDBNull(.Item("MainPremiumToInsCo")), "", .Item("MainPremiumToInsCo")), 2)
            lblPremiumToInsCo.Text = FormatNumber(IIf(IsDBNull(.Item("PremiumAmountToInsCo")), "", .Item("PremiumAmountToInsCo")), 2)
            lblCoverage.Text = CStr(IIf(IsDBNull(.Item("MainCoverage")), "", .Item("MainCoverage")))
            Me.StartDate = CDate(IIf(IsDBNull(.Item("StartDate")), "", .Item("StartDate")))
            Me.EndDate = CDate(IIf(IsDBNull(.Item("EndDate")), "", .Item("EndDate")))
        End With
        dtgMainPremium.DataSource = dt.DefaultView
        dtgMainPremium.DataBind()
        '==============================================================
        oReason.ReasonTypeID = "TERMI"
        oReason.BindReason()

        txtTerminateDate.Text = Me.FirstDate.ToString("dd/MM/yyyy")
        txtNotes.Text = Me.Notes
        oReason.ReasonID = Me.ReasonId


    End Sub
    'description : sub ini untuk menampilkan data perhitungan refund customer jika btn calculate diklik
    Private Sub viewCalculate(ByVal BranchID As String, ByVal ApplicationID As String, ByVal AssetSeqNo As Integer, ByVal InsSeqNo As Integer)

     
        Me.FirstDate = ConvertDate2(txtTerminateDate.Text)
        Me.TerminateDate = ConvertDate2(txtTerminateDate.Text)
        Me.Notes = txtNotes.Text
        Me.ReasonId = oReason.ReasonID
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlView.Visible = True
        pnlCalculate.Visible = False
        If Me.TerminateDate >= Me.StartDate And Me.TerminateDate <= Me.EndDate Then
            With oInsTerminate
                .strConnection = GetConnectionString()
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .AssetSequenceNo = Me.AssetSequenceNo
                .InsSequenceNo = Me.InsSequenceNo
                .TerminationDate = Me.TerminateDate
            End With
            viewReportDetail(Me.BranchID, Me.ApplicationID, Me.AssetSequenceNo, Me.InsSequenceNo)
            Dim dtsEntity As New DataTable
            Dim dtvEntity As New DataView
            pnlCalculate.Visible = True
            Dim dt As New DataTable


            oInsTerminate = m_InsTerminate.InsCalculate(oInsTerminate)
            dt = oInsTerminate.ListData
            If dt.Rows.Count = 0 Then
                ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
                btnCalculate.Visible = False

                Exit Sub
            End If
            With dt.Rows(0)
                lblRestInsToInsCo.Text = CStr(IIf(IsDBNull(.Item("RestOfPeriodToInsCo")), "", .Item("RestOfPeriodToInsCo")))
                lblRefundAmountFromInsCo.Text = FormatNumber(IIf(IsDBNull(.Item("RefundFromInsCo")), "", .Item("RefundFromInsCo")), 0)


                'lblRefundToSupplier.Text = FormatNumber(IIf(IsDBNull(.Item("RefundToSupplier")), "", .Item("RefundToSupplier")), 2) 
                'lblDeductPercent.Text = CStr(IIf(IsDBNull(.Item("DeductPercentage")), "", .Item("DeductPercentage"))) 
                '  lblRefundAmountToCust.Text = FormatNumber(IIf(IsDBNull(.Item("RefundAmountToCust")), "", .Item("RefundAmountToCust")), 2)
                Me.PremiumToInsCo = CDbl(IIf(IsDBNull(.Item("PremiumToInsCo")), "", .Item("PremiumToInsCo")))
                Me.PremiumToCust = CDbl(IIf(IsDBNull(.Item("PremiumToCust")), "", .Item("PremiumToCust")))
                Me.RefundAmountToCust = CDbl(IIf(IsDBNull(.Item("RefundAmountToCust")), "", .Item("RefundAmountToCust")))
                Me.RefundAmountFromInsCo = CDbl(IIf(IsDBNull(.Item("RefundFromInsCo")), "", .Item("RefundFromInsCo")))
                Me.RefundToSupplier = CDbl(IIf(IsDBNull(.Item("RefundToSupplier")), "", .Item("RefundToSupplier")))
                Me.DeductPercent = CDbl(IIf(IsDBNull(.Item("DeductPercentage")), "", .Item("DeductPercentage")))
            End With
            txtRefundAmountToCust.Text = "0"
            txtDeductAmount.Text = "0"
            lblTtl.Text = "0.00"
            lblGain.Text = FormatNumber(RefundAmountFromInsCo, 0)


            'Me.NilaiAkhir = Me.NilaiAwalV  
            'jika deduction di ubah tidak sesuai dengan deduction precent
            'If Me.DeductPercent <> DeductionToPersen() Then
            '    Me.DeductAmount = CDbl(txtDeductAmount.Text)
            'Else
            '    Me.DeductAmount = Me.RefundAmountToCust * Me.DeductPercent / 100
            'End If

            'txtDeductAmount.Text = Me.DeductAmount

            'lblTtl.Text = FormatNumber((Me.RefundAmountToCust - Me.DeductAmount), 2)
            'Me.TotalRefund = Me.RefundAmountToCust - Me.DeductAmount

            'lblGain.Text = FormatNumber(((Me.PremiumToCust - Me.PremiumToInsCo) - Me.RefundToSupplier + (Me.RefundAmountFromInsCo - Me.TotalRefund)), 2)
            'Me.flag = True
        Else
            ShowMessage(lblMessage, "Tanggal Penutupan harus <= Tanggal Selesai", True)
        End If

    End Sub

    'Function DeductionToPersen() As Decimal
    '    If IsNumeric(txtDeductAmount.Text) Then
    '        If Me.RefundAmountToCust = 0 Then
    '            Return 0
    '        Else
    '            Return CDbl(txtDeductAmount.Text) / Me.RefundAmountToCust * 100
    '        End If
    '    Else
    '        Return Me.DeductPercent
    '    End If
    'End Function


  
#End Region
   
#Region " Event Handlers "
    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""

        If ddlBranch.SelectedValue <> "" And ddlBranch.SelectedValue <> "ALL" Then
            Me.SearchBy = "BranchID='" & ddlBranch.SelectedValue & "'"
        End If
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then
            If Me.SearchBy = "" Then
                Me.SearchBy = cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            Else
                Me.SearchBy = Me.SearchBy & " and " & cboSearchBy.SelectedItem.Value.Trim & " like '%" & txtSearchBy.Text.Trim & "%'"
            End If
        End If

        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
    Private Sub dtgClaimRequestList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgClaimRequestList.ItemCommand

        Select Case e.CommandName
            Case "Terminate"
                Me.BranchID = e.Item.Cells(4).Text.Trim
                Me.ApplicationID = e.Item.Cells(5).Text.Trim
                Me.customerID = e.Item.Cells(6).Text.Trim
                Me.AgreementNo = e.Item.Cells(7).Text.Trim
                Me.CustomerName = e.Item.Cells(8).Text.Trim
                Me.AssetSequenceNo = CInt(e.Item.Cells(14).Text.Trim)
                Me.InsSequenceNo = CInt(e.Item.Cells(15).Text.Trim)
                Dim dtsPrintRefund As New DataSet
                If checkFeature(Me.Loginid, Me.FormID, "Termi", Me.AppId) Then
                    If sessioninvalid() Then
                        Dim strHTTPServer As String
                        Dim strHTTPApp As String
                        Dim strNameServer As String
                        strHTTPServer = Request.ServerVariables("PATH_INFO")
                        strNameServer = Request.ServerVariables("SERVER_NAME")
                        strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                        Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
                    Else
                        Me.TerminateDate = BusinessDate
                        viewReportDetail(Me.BranchID, Me.ApplicationID, Me.AssetSequenceNo, Me.InsSequenceNo)
                    End If
                End If
            Case "Print"
                Me.BranchID = e.Item.Cells(4).Text.Trim
                Me.ApplicationID = e.Item.Cells(5).Text.Trim
                Me.customerID = e.Item.Cells(6).Text.Trim
                Me.AgreementNo = e.Item.Cells(7).Text.Trim
                Me.CustomerName = e.Item.Cells(8).Text.Trim
                Me.AssetSequenceNo = CInt(e.Item.Cells(14).Text.Trim)
                Me.InsSequenceNo = CInt(e.Item.Cells(15).Text.Trim)

                Dim dtsPrintRefund As New DataSet
                Dim strwrite As String
                With oInsTerminate
                    .strConnection = GetConnectionString
                    .ApplicationID = Me.ApplicationID
                    .BranchId = Me.BranchID
                    .AssetSequenceNo = Me.AssetSequenceNo
                    .InsSequenceNo = Me.InsSequenceNo
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                End With
                m_InsTerminate.InsInsertMail(oInsTerminate)
                dtsPrintRefund = m_InsTerminate.InsrequestPrint(oInsTerminate)

                If dtsPrintRefund.Tables(0).Rows.Count = 0 Then
                    strwrite = "Data Tidak ditemukan ....."

                    ShowMessage(lblMessage, strwrite, True)
                Else 
                    Response.Redirect("PrintRefundInscoForm.aspx?BranchID=" & Me.BranchID & "&style=Insurance&ApplicationID=" & Me.ApplicationID & "&AssSeqNo=" & Me.AssetSequenceNo & "&InsSeqNo=" & Me.InsSequenceNo)
                End If
            Case "PrintRefund"
                Me.BranchID = e.Item.Cells(4).Text.Trim
                Me.ApplicationID = e.Item.Cells(5).Text.Trim
                Me.customerID = e.Item.Cells(6).Text.Trim
                Me.AgreementNo = e.Item.Cells(7).Text.Trim
                Me.CustomerName = e.Item.Cells(8).Text.Trim
                'Me.AssetSequenceNo = CInt(e.Item.Cells(13).Text.Trim)
                'Me.InsSequenceNo = CInt(e.Item.Cells(14).Text.Trim)

                Dim dtsPrintRefund As New DataSet
                Dim strwrite As String
                With oInsTerminate
                    .strConnection = GetConnectionString()
                    .ApplicationID = Me.ApplicationID
                    .BranchId = Me.BranchID
                    '.AssetSequenceNo = Me.AssetSequenceNo
                    '.InsSequenceNo = Me.InsSequenceNo
                    .LoginId = Me.Loginid
                    .BusinessDate = Me.BusinessDate
                End With
                m_InsTerminate.InsInsertMailRefund(oInsTerminate)
                dtsPrintRefund = m_InsTerminate.InsrequestPrintRefund(oInsTerminate)

                If dtsPrintRefund.Tables(0).Rows.Count = 0 Then
                    strwrite = "Data Tidak ditemukan ....."

                    ShowMessage(LblMessage, strwrite, True)
                Else
                    Response.Redirect("PrintRefundAsuransiForm.aspx?BranchID=" & Me.BranchID & "&style=Insurance&ApplicationID=" & Me.ApplicationID)
                End If
        End Select
    End Sub

    Private Sub dtgClaimRequestList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgClaimRequestList.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lnkAgreementNo As HyperLink
            Dim lnkCustomerName As HyperLink
            Dim lnkInsurance As HyperLink
            Dim strCustomerID As String
            Dim strApplicationID As String


            lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            'strApplicationID = e.Item.Cells(4).Text.Trim
            strApplicationID = e.Item.Cells(5).Text.Trim
            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(strApplicationID.Trim) & "')"
            End If
            strCustomerID = e.Item.Cells(6).Text.Trim
            lnkCustomerName = CType(e.Item.FindControl("lnkCustomerName"), HyperLink)

            If strCustomerID.Trim.Length > 0 Then
                lnkCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(strCustomerID.Trim) & "')"
            End If

            lnkInsurance = CType(e.Item.FindControl("hyInsurance"), HyperLink)
            'lnkInsurance.NavigateUrl = "javascript:OpenWinInsurance('" & "Terminate" & "','" & Server.UrlEncode(strApplicationID.Trim) & "','" & Server.UrlEncode(e.Item.Cells(3).Text.Trim) & "')"
            lnkInsurance.NavigateUrl = "javascript:OpenWinInsurance('" & "Terminate" & "','" & Server.UrlEncode(strApplicationID.Trim) & "','" & Server.UrlEncode(e.Item.Cells(4).Text.Trim) & "')"
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        ddlBranch.ClearSelection()
        cboSearchBy.ClearSelection()
        txtSearchBy.Text = ""
    End Sub
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        pnlView.Visible = False
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlCalculate.Visible = False
    End Sub

    Private Sub btnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculate.Click

        pnlCalculate.Visible = True
        pnlView.Visible = True
        pnlsearch.Visible = False
        pnlDtGrid.Visible = False
        viewCalculate(Me.BranchID, Me.ApplicationID, Me.AssetSequenceNo, Me.InsSequenceNo)
    End Sub
    Private Sub btnSave2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave2.Click        

        Me.TerminateDate = ConvertDate2(txtTerminateDate.Text)
        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlView.Visible = True
        pnlCalculate.Visible = True

        If Me.FirstDate <> Me.TerminateDate Then
            ShowMessage(lblMessage, "Tanggal Penutupan berbeda dengan Kalkulasi Terakhir. Harap Klik tombol Calculate", True)
            pnlDtGrid.Visible = False
            pnlsearch.Visible = False
            pnlView.Visible = True
            pnlCalculate.Visible = True
            Exit Sub
        Else
            
            If Me.TerminateDate >= Me.StartDate And Me.TerminateDate <= Me.EndDate Then
            
                With oInsTerminate
                    .strConnection = GetConnectionString()
                    .AgreementNo = Me.AgreementNo
                    .BranchId = Me.BranchID
                    .ApplicationID = Me.ApplicationID
                    .AssetSequenceNo = Me.AssetSequenceNo
                    .InsSequenceNo = Me.InsSequenceNo
                    .CustomerName = Me.CustomerName
                    .TerminationDate = Me.TerminateDate
                    .ClaimAmount = CDec(txtRefundAmountToCust.Text) ' CType(Me.RefundAmountFromInsCo, Decimal)
                    .RefundDeduct = CDec(txtDeductAmount.Text) ' CType(Me.DeductAmount, Decimal)
                    .TotalRefundTocust = CDec(txtRefundAmountToCust.Text) - CDec(txtDeductAmount.Text)  'CType(Me.TotalRefund, Decimal)
                    .ReasonID = oReason.ReasonID
                    .Notes = txtNotes.Text
                End With
                Try
                    If m_InsTerminate.InsSaveCalculate(oInsTerminate) Then
                        btnsearch_Click(Nothing, Nothing)
                        ShowMessage(LblMessage, "Simpan Data Berhasil", False)
                        'Dim cookie As HttpCookie = Request.Cookies("REFUND_COOKIES")
                        'If Not cookie Is Nothing Then
                        '    cookie.Values("ApplicationId") = Me.ApplicationID
                        '    cookie.Values("BranchID") = Me.BranchID
                        '    cookie.Values("AssSequenceNo") = CStr(Me.AssetSequenceNo)
                        '    cookie.Values("InsSequenceNo") = CStr(Me.InsSequenceNo)
                        '    Response.AppendCookie(cookie)
                        'Else
                        '    Dim cookieNew As New HttpCookie("REFUND_COOKIES")
                        '    cookieNew.Values.Add("ApplicationId", Me.ApplicationID)
                        '    cookieNew.Values.Add("BranchID", Me.BranchID)
                        '    cookieNew.Values.Add("AssSequenceNo", CStr(Me.AssetSequenceNo))
                        '    cookieNew.Values.Add("InsSequenceNo", CStr(Me.InsSequenceNo))
                        '    Response.AppendCookie(cookieNew)
                        'End If
                        ' Response.Redirect("InsTerminate.aspx")
                    Else
                        ShowMessage(LblMessage, "Simpan Data Gagal", True)
                    End If
                Catch exp As Exception
                    ShowMessage(lblMessage, exp.Message, True)
                End Try

            Else
                ShowMessage(lblMessage, "Tanggal Penutupan harus <= Tanggal Selesai", True)
            End If
        End If

    End Sub

    Private Sub btnCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel2.Click
        pnlView.Visible = False
        pnlsearch.Visible = True
        pnlDtGrid.Visible = True
        pnlCalculate.Visible = False
    End Sub
#End Region

End Class