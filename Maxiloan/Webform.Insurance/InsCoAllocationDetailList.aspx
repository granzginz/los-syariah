﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsCoAllocationDetailList.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsCoAllocationDetailList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcInsuranceBranchName" Src="../webform.UserController/UcInsuranceBranchName.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsCoAllocationDetailList</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="LblErrorMessages" runat="server"></asp:Label>
    <div align="center">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    PEMILIHAN PERUSAHAAN ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="LblAgreementNo" runat="server" ForeColor="Blue" Visible="False"></asp:Label><asp:HyperLink
                    ID="HplinkAgreementNo" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:Label ID="LblCustomerName" runat="server" ForeColor="Blue" Visible="False"></asp:Label> 
                <asp:HyperLink ID="HpLinkCustName" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Asset Asuransi
                </label>
                <asp:Label ID="LblInsuranceAssetType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Penggunaan
                </label>
                <asp:Label ID="LblAssetUsage" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Asset Baru / Bekas
                </label>
                <asp:Label ID="LblAssetNewUSed" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Loan ACT
                </label>
                <asp:Label ID="LblGoLiveDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nilai Pertanggungan
                </label>
                <asp:Label ID="LblAmountCoverage" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Total Premi Standard
                </label>
                <asp:Label ID="LblTotalStandardPremium" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Total Premi Customer
                </label>
                <asp:Label ID="LblTotalPremiumByCust" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Loading Fee
                </label>
                <asp:Label ID="LblLoadingFee" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Materai
                </label>
                <asp:Label ID="LblStampDutyFee" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Administrasi
                </label>
                <asp:Label ID="LblAdminFee" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    DiAsuransi Oleh
                </label>
                <asp:Label ID="LblInsuredByDescr" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    DiBayar Oleh
                </label>
                <asp:Label ID="LblPaidBy" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jangka Waktu
                </label>
                <asp:Label ID="lblTenor" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tahun Asset
                </label>
                <asp:Label ID="lblAssetYear" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Periode Cover
                </label>
                <asp:DropDownList ID="DDLCoverPeriod" runat="server">
                    <asp:ListItem Value="FT" Selected="True">Full Tenor</asp:ListItem>
                    <asp:ListItem Value="AN">Annualy</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="LBlDataExist" runat="server" Visible="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Mulai Tanggal
                </label>
                <asp:TextBox runat="server" ID="txtodate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtodate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:Label ID="LblMessageForDate" runat="server" ForeColor="Red" DESIGNTIMEDRAGDROP="852"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnOkAtas" runat="server" Text="OK" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelAtas" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
            <asp:Label ID="LblBranchIDHidden" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblFlagInsActivationHidden" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblApplicationIDHidden" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblMaturityDate" runat="server" Visible="False"></asp:Label>
        </div>
    </div>
    <asp:Panel ID="PnlGrid" runat="server">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DGridTenor" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="yearnum" HeaderText="TAHUN KE"></asp:BoundColumn>
                            <asp:BoundColumn DataField="coveragetypedescr" HeaderText="JENIS COVER"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PREMI SRCC">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SrccPremiumToCust", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH TPL">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tplamounttocust", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI TPL">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tplpremiumtocust", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLOOD">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLoodPremiumToCust", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMIUM">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.premiumtocustamount", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="paidbycust" HeaderText="DIBAYAR CUSTOMER"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PREMI PERUSAHAAN ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DGridInsurance" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                        DataKeyField="MaskAssBranchID" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" HeaderText="INSURANCE COY ID">
                                <ItemTemplate>
                                    <asp:Label ID="LblGridMaskAssID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="InsuranceComBranchID">
                                <ItemTemplate>
                                    <asp:Label ID="LblGridInsuranceComBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssBranchID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="MaskAssBranchNAME" HeaderText="CABANG ASURANSI">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="PREMI UTAMA">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGridMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremium", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI SRCC">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGridSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SrccPremium", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI TPL">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGridTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLPremium", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGridFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremium", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LOADING FEE">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGridLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFee", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BIAYA ADMIN">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGridAdminFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AdminFee", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BIAYA MATERAI">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGridMeteraiFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StampdutyFee", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TOTAL">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblGridGrandTotal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STATISTIK ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DgridQuota" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="CABANG ASURANSI">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%#Container.DataItem("InsuranceComBranchname")%>'
                                        ID="LblInsuranceComBranchname">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH KUOTA">
                                <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaAmount", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI TERALOKASI">
                                <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PremiumAllocated", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="(%) TERALOKASI">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AllocatedPercentage", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="KONTRAK TERALOKASI">
                                <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Text='<%#formatnumber(Container.DataItem("AllocatedAgreement"),0)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="(%) KUOTA">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaPercentage", "{0:###,###,##0.00}") %>'
                                        ID="lblQuotaPercentage">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="(%) KUOTA DICAPAI">
                                <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaPercentageAchieved", "{0:###,###,##0.00}") %>'
                                        ID="lblQuotaPercentageAchieved">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Sampai Tanggal</label>
                <asp:TextBox runat="server" ID="txtEndDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtEndDate"
                    Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:Label ID="LblMessageEndDate" runat="server" ForeColor="Red"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Perusahaan Asuransi</label>
                <uc1:ucinsurancebranchname id="oInsuranceBranchName" runat="server"></uc1:ucinsurancebranchname>
                <asp:Label ID="LblPremiumInsCo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
