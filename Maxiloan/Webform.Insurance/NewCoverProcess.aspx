﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewCoverProcess.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.NewCoverProcess" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NewCoverProcess</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
		<!--
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinHistory(pCGID, pExID, pExName) {
            var x = screen.width; var y = screen.height - 100; window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Setting/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function currencyFormatku(fld, e) {
            var key = '';
            var i = j = 0;
            var strCheck = '0123456789';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            document.onkeypress = window.event;
            if (whichCode == 13) return true;  // Enter
            key = String.fromCharCode(whichCode);  // Get key value from key code
            if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
        }
     //-->
    </script>
</head>
<body>
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    COVER BARU UNTUK APLIKASI SUDAH ADA
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:HyperLink ID="lblAgreementNo" runat="server"></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:Label ID="lblName" runat="server"></asp:Label>
            </div>
            <div class="form_left">
                <label>
                    Di Asuransi Oleh
                </label>
                <asp:DropDownList ID="cboInsuredBy" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Di Bayar Oleh
                </label>
                <asp:DropDownList ID="cboPaidBy" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="small button green">
            </asp:Button>
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
