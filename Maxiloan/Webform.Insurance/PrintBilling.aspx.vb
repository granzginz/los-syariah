﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
#End Region

Public Class PrintBilling
    Inherits Maxiloan.Webform.WebBased

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.FormID = "InsPrintBilling"

        If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
            If Not Page.IsPostBack Then
                fillcbo()
            End If
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub
    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With ddlbranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"
                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub
    Private Sub btnViewReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewReport.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            If SessionInvalid() Then
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            Else
                Response.Cookies("PrintBilling")("InvoiceNo") = txtInvoiceNo.Text.Trim
                Response.Cookies("PrintBilling")("BranchID") = ddlbranch.SelectedValue
                Response.Cookies("PrintBilling")("BranchName") = ddlbranch.SelectedItem.Text.Trim
                Response.Redirect("PrintInsuranceBillingViewer.aspx")
            End If
        End If

    End Sub
End Class