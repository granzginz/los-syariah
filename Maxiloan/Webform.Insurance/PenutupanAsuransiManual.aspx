﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PenutupanAsuransiManual.aspx.vb" Inherits="Maxiloan.Webform.Insurance.PenutupanAsuransiManual" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../webform.UserController/UcSearchBy.ascx" %>
<%@ Register Src="../webform.UserController/ucNumberFormat.ascx" TagName="ucNumberFormat" TagPrefix="uc4" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtstartDate').change(function (e) {
                var sdate = $('#txtstartDate').val();
                var n = sdate.toString().split("/");
                var nyear = parseFloat(n[2]) + 1;
                $('#lblEndDate').text(n[0] + '/' + n[1] + '/' + nyear);
                }
            );
        });
     </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="sm1"/>
    <asp:Label ID="lblMessage" Visible="false" runat="server" />

    <div class="form_title">
        <div class="title_strip"></div>
        <div class="form_single">
            <h3> DAFTAR KONTRAK PENUTUPAN ASURANSI </h3>
        </div>
    </div>

    <asp:Panel ID="pnlDatagrid" runat="server" Visible="false">        
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgAsuransi" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="Applicationid" BorderStyle="None"
                        BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="Asuransi">                                
                                <ItemTemplate>
                                    <asp:LinkButton ID="HypSelect" runat="server" Text='PILIH'  CommandName="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                           <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">                                
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                              <asp:BoundColumn DataField="InsuredBy"   HeaderText="DIASURANSI" /> 
                              <asp:BoundColumn DataField="InsurancePaidBy"  HeaderText="DITANGGUNG" /> 
                              <asp:BoundColumn DataField="dateEntryInsuranceData"  HeaderText="TANGGAL APK" DataFormatString="{0:dd/MM/yyyy}" /> 


                             <asp:TemplateColumn Visible="False">                                
                                <ItemTemplate>
                                    <asp:Label id="hdnBranch"   runat="server"   Text='<%#Container.DataItem("Branchid")%>'/>
                                    <asp:Label id="hdnAppId"    runat="server"   Text='<%#Container.DataItem("ApplicationID")%>'/>
                                    <asp:Label ID="hdnCustomerId" runat="server" Text='<%#Container.DataItem("Customerid")%>' /> 
                                    <asp:Label ID="hdnMaskAssBranchID" runat="server" Text='<%#Container.DataItem("MaskAssBranchID")%>' /> 
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>                        
                    </asp:DataGrid>
                    <uc2:ucGridNav id="GridNavigator" runat="server"/> 
                </div>
            </div>
        </div>
    </asp:Panel>
         
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_box_title">
            <div class="form_single">
                <h4>
                    PENUTUPAN ASURANSI MANUAL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req"> Cabang</label>
                <asp:DropDownList ID="cbobranch" runat="server" /> 
                <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" InitialValue="0" ErrorMessage="Harap pilih Cabang" ControlToValidate="cbobranch" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>            
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue" /> 
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray" /> 
        </div>
    </asp:Panel>

   

    <asp:Panel ID="pnlPenutupan" runat="server" visible="false">
    
    
            <div class="form_title">
                <div class="form_single">
                    <h3> ENTRI DATA ASURANSI</h3>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label> Jenis Asset </label>
                        <asp:Label ID="LblInsuranceType" runat="server" Visible="False" />
                        <asp:Label ID="lblInsuranceAssetType" runat="server">lblInsuranceAssetType</asp:Label>
                    </div>
                    <div class="form_right">
                        <label> Penggunaan Asset </label>
                        <asp:Label ID="LblAssetUsageDescr" runat="server">LblAssetUsageDescr</asp:Label>
                        <asp:Label ID="LblAssetUsageID" runat="server" Visible="False"></asp:Label>
                        <asp:Label ID="LblManufacturingYear" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label> Asset (New/Used) </label>
                        <asp:Label ID="lblAssetNewused" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label> Cover Asuransi Dilakukan Oleh </label>
                        <asp:Label ID="LblInsuredBy" runat="server">LblInsuredBy</asp:Label>
                    </div>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label class="label_req"> Jangka Waktu Asuransi </label>
                        <asp:TextBox ID="TxtTenorCredit" runat="server" CssClass="smaller_text"></asp:TextBox><label class="label_unit ">Bulan</label>
                       <%-- <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic" ControlToValidate="TxtTenorCredit" ErrorMessage="Harap isi Jangka Waktu Kredit" CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rgvTenorCredit" runat="server" ControlToValidate="TxtTenorCredit" ErrorMessage="RangeValidator" MaximumValue="120" MinimumValue="0" Type="Integer" Display="Dynamic" CssClass="validator_general"></asp:RangeValidator>--%>
                    </div>
                    <div class="form_right">
                        <label> Premi Dibayar Oleh </label>
                        <asp:Label ID="LblPaidBy" runat="server" />
                    </div>
                </div>
            </div>
            
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label> Harga OTR </label>
                        <asp:Label runat="server" ID="lblOTR" CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                    <div class="form_right">
                        <label> Jenis Aksesoris </label>
                        <asp:TextBox runat="server" ID="txtJenisAksesoris" CssClass="long_text"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label> Nilai Aksesoris </label>
                        <uc4:ucnumberformat id="txtNilaiAksesoris" runat="server" />
                    </div>
                   
                      <div class="form_right">
                        <label> Wilayah Asuransi </label>
                        <asp:Label runat="server" ID="lblWilayahAsuransi"/>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <label> Nilai Pertanggungan </label>
                        <asp:Label ID="lblAmountCoverage" runat="server" CssClass="numberAlign regular_text"></asp:Label>
                    </div>
                     <div class="form_right">
                        <label> Tahun Kendaraan </label>
                        <asp:Label runat="server" ID="lblTahunKendaraan"/>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div>
                    <div class="form_left">
                        <div style="float:left">
                        <label>Perluasan Personal Accident</label>
                        </div>
                        <div>
                           <asp:RadioButtonList runat="server" ID="rdoPerluasanPersolanAccident" RepeatDirection="Horizontal" CssClass="opt_single rboRefinancing">
                            <asp:ListItem  Value="True" Text="Ya"></asp:ListItem>
                            <asp:ListItem Value="False" Text="Tidak" Selected="True" ></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form_right">
                       
                    </div>
                </div>
            </div>
    
    
 
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PREMI ASURANSI & PERBANDINGANNYA
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="gridInsco" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                BorderWidth="0px" CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="MaskAssBranchID"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn Visible="False" HeaderText="INSURANCE COY ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMaskAssID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="InsuranceComBranchID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInsuranceComBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssBranchID") %>' /> 
                                            <asp:Label ID="lblMaskapaiAsuransiDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskapaiAsuransi") %>' /> 
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MaskAssBranchNAME" HeaderText="CABANG ASURANSI">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PREMI UTAMA">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TPL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SrccPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING FEE">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA PASS">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA ADMIN">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridAdminFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AdminFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA MATERAI">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMeteraiFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StampdutyFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TOTAL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridGrandTotal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="gridDetailInsco" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="YearNum" CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="YEAR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDYearNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JENIS COVER">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDCoverage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI UTAMA">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TPL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING FEE">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFeeToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA PASS">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>

                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PENUTUPAN ASURANSI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label >Cover Period</label>
                    <asp:DropDownList ID="cboCoverPeriod" runat="server" >
                        <asp:ListItem Value="FT">Full Tenor</asp:ListItem>
                        <asp:ListItem Value="AN">Annualy</asp:ListItem>
                    </asp:DropDownList> 
                </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Tanggal </label>
                        <asp:TextBox runat="server" ID="txtstartDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtstartDate" Format="dd/MM/yyyy" />  
                        <asp:RequiredFieldValidator runat="server" ID="rfvDateCE" ControlToValidate="txtstartDate" Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general" SetFocusOnError="true" Enabled="false" />  
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!" ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtstartDate" SetFocusOnError="true" Display="Dynamic" CssClass="validator_general" />  
                        
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label >Sampai</label>
                         <asp:Label ID="lblEndDate" runat="server" />
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label >Maskapai Asuransi</label>
                         <asp:Label ID="lblMaskapaiAsuransi" runat="server" />
                    </div>
                </div>

               <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green"></asp:Button>
                <asp:Button ID="btnCancel" Text="Cancel" CssClass="small button gray" runat="server" CausesValidation="False"></asp:Button>
            </div>
    </asp:Panel>
 
    </form>
</body>
</html>
