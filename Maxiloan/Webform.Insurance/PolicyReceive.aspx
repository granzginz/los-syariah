﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PolicyReceive.aspx.vb" Inherits="Maxiloan.Webform.Insurance.PolicyReceive" %>

<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../webform.UserController/UcBranchAll.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../Webform.UserController/UcSearchBy.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcInsuranceBranchName" Src="../Webform.UserController/UcInsuranceBranchName.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PolicyReceive</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/';

        var hdnDetail;
        var hdndetailvalue;
        var cboBankAccount = null;
        function ParentChange(pCmbOfPayment, pBankAccount, pHdnDetail, pHdnDetailValue, itemArray) {
            hdnDetail = eval('document.forms[0].' + pHdnDetail);
            HdnDetailValue = eval('document.forms[0].' + pHdnDetailValue);            
            var i, j;
            for (i = eval('document.forms[0].' + pBankAccount).options.length; i >= 0; i--) {
                eval('document.forms[0].' + pBankAccount).options[i] = null
            };
            if (itemArray == null) {
                j = 0;
            }
            else {
                j = 1;
            };
            //alert(itemArray);
            eval('document.forms[0].' + pBankAccount).options[0] = new Option('Select One', '0');
            if (itemArray != null) {
                for (i = 0; i < itemArray.length; i++) {
                    eval('document.forms[0].' + pBankAccount).options[j++] = new Option(itemArray[i][0], itemArray[i][1]);
                };
                eval('document.forms[0].' + pBankAccount).selected = true;
            }
        };

        function cboChildonChange(selIndex, l, j) {
            hdnDetail.value = l;
            HdnDetailValue.value = j;
            eval('document.forms[0].hdnBankAccount').value = selIndex;
        }
        function RestoreInsuranceCoIndex(BankAccountIndex) {
            cboChild = eval('document.forms[0].cboChild');
            if (cboChild != null)
                if (eval('document.forms[0].hdnBankAccount').value != null) {
                    if (BankAccountIndex != null)
                        cboChild.selectedIndex = BankAccountIndex;
                }
        }
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server" onsubmit="<%#BranchIDChange()%>">
        <input id="hdnChildValue" type="hidden" name="hdnSP" runat="server" />
        <input id="hdnChildName" type="hidden" name="hdnSP" runat="server" />
        <input id="hdnBankAccount" runat="server" type="hidden" name="hdSP" />

        <asp:Label ID="LblErrorMessages" runat="server" ForeColor="Red"></asp:Label>

        <asp:ScriptManager runat="server" ID="sm1">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="up1" runat="server">
            <ContentTemplate>
                <div class="form_title">
                    <div class="form_single">
                        <h3>CARI PENERIMAAN POLIS
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <%--            <label>
                Cabang</label>
            <asp:Label ID="LblBranchName" runat="server" Visible="False"></asp:Label>
            <uc1:ucbranchall id="oBranch" runat="server">
                </uc1:ucbranchall>--%>
                        <label class="label_req">
                            Cabang
                        </label>
                        <asp:DropDownList ID="cboParent" runat="server" onchange="<%#BranchIDChange()%>">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Harap Pilih Cabang" ControlToValidate="cboParent" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <%--            <label>
                Perusahaan Asuransi</label>
            <uc1:ucinsurancebranchname id="oInsuranceBranchName" runat="server">
                </uc1:ucinsurancebranchname>--%>
                        <label class="label_req">
                            Perusahaan Asuransi
                        </label>
                        <asp:DropDownList ID="cboChild" runat="server" onchange="cboChildonChange(this.selectedIndex,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="0"
                            ErrorMessage="Harap Pilih Perusahaan Asuransi" ControlToValidate="cboChild" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label class="label_req">
                            Product Asuransi
                        </label>
                        <asp:DropDownList ID="cboProductAsuransi" runat="server"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" InitialValue="Select One" ErrorMessage="Harap Pilih Product Asuransi" ControlToValidate="cboProductAsuransi" CssClass="validator_general"></asp:RequiredFieldValidator>
                <%--  <asp:ListItem Value="CP">Credit Protection</asp:ListItem>
                <asp:ListItem Value="JK">Jaminan Kredit</asp:ListItem>
                <asp:ListItem Value="KB">Kendaraan Bermotor</asp:ListItem>--%>
                        
                    </div>
                </div>

                <div class="form_box">
                    <uc1:UcSearchByWithNoTable ID="oSearchBy" runat="server"></uc1:UcSearchByWithNoTable>
                </div>

                <triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
            </triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="pnlResult" runat="server" />

        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"></asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray"></asp:Button>
        </div>
        <asp:Panel ID="PnlGrid" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h3>DAFTAR KONTRAK UNTUK PENERIMAAN POLIS
                    </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="Sorting" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                            CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="POLIS">
                                    <ItemStyle HorizontalAlign="center" Width="5%"></ItemStyle>
                                    <ItemTemplate>
                                        <a href='<%# GenerateLinkTo(DataBinder.eval(Container,"DataItem.FlagInsActivation"),DataBinder.eval(Container,"DataItem.FlagDocStatus")) %>?ApplicationID=<%# DataBinder.eval(Container,"DataItem.ApplicationID") %>&BranchId=<%# DataBinder.eval(Container,"DataItem.BranchID") %>&agreementno=<%# DataBinder.eval(Container,"DataItem.agreementno") %>&CustomerID=<%# DataBinder.eval(Container,"DataItem.CustomerID") %>&assetseqno=<%# DataBinder.eval(Container,"DataItem.assetseqno") %>&inssequenceno=<%# DataBinder.eval(Container,"DataItem.inssequenceno") %>'>TERIMA
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DTL">
                                    <ItemStyle HorizontalAlign="center" Width="3%"></ItemStyle>
                                    <ItemTemplate>
                                        <a href='ViewInsuranceDetail.aspx?ApplicationID=<%# DataBinder.eval(Container,"DataItem.ApplicationID") %>&BranchId=<%# DataBinder.eval(Container,"DataItem.BranchID") %>'>
                                            <asp:Image ID="imgRate" ImageUrl="../images/iconinsurance.gif" runat="server"></asp:Image>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                    <HeaderStyle HorizontalAlign="Left" Width="9%"></HeaderStyle>
                                    <ItemStyle></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA CUSTOMER">
                                    <HeaderStyle HorizontalAlign="Left" Width="22%"></HeaderStyle>
                                    <ItemStyle Width="18%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="AssetMasterDescr" SortExpression="AssetMasterDescr" ItemStyle-Width="28%" HeaderText="NAMA ASSET"></asp:BoundColumn>
                                <asp:BoundColumn DataField="SerialNo1" SortExpression="SerialNo1" ItemStyle-Width="14%" HeaderText="NO RANGKA"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InsuranceCompany" SortExpression="InsuranceCompany" ItemStyle-Width="12%" HeaderText="ASURANSI"></asp:BoundColumn>
                                <asp:BoundColumn DataField="InsuredByDescr" SortExpression="InsuredByDescr" ItemStyle-Width="6%" HeaderText="ASS"></asp:BoundColumn>
                                <asp:BoundColumn DataField="PaidByDescr" SortExpression="PaidByDescr" ItemStyle-Width="6%" HeaderText="BAYAR"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ContractStatus" SortExpression="ContractStatus" ItemStyle-Width="4%" HeaderText="STS"></asp:BoundColumn>
                                <asp:BoundColumn DataField="ProductAsuransi" SortExpression="ProductAsuransi" ItemStyle-Width="4%" HeaderText="Product Asuransi"></asp:BoundColumn>
                                <asp:TemplateColumn Visible="False" SortExpression="" HeaderText="CUSTOMER ID">
                                    <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                    <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                            Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                                MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGopage"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                                ErrorMessage="No Halaman Salah" ControlToValidate="txtGopage" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

    </form>
</body>
</html>
