﻿#Region "Imports"
Imports System.IO
Imports System.Text
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonSimpleRuleHelper
#End Region

Imports System.Math
Imports System.Data
Imports System.Data.SqlClient
Public Class InsCoAllocationDetailEdit
    Inherits Maxiloan.Webform.WebBased

#Region "Property"

    Private Property FlagInsActivation() As String
        Get
            Return (CType(viewstate("FlagInsActivation"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("FlagInsActivation") = Value
        End Set
    End Property

    Private Property MaxYearNum() As Int16
        Get
            Return (CType(viewstate("MaxYearNum"), Int16))
        End Get
        Set(ByVal Value As Int16)
            viewstate("MaxYearNum") = Value
        End Set
    End Property


    Private Property MaturityDate() As String
        Get
            Return (CType(viewstate("MaturityDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("MaturityDate") = Value
        End Set
    End Property

    Private Property StartDate() As String
        Get
            Return (CType(viewstate("StartDate"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartDate") = Value
        End Set
    End Property
    Private Property EndDateEntry() As String
        Get
            Return (CType(viewstate("EndDateEntry"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("EndDateEntry") = Value
        End Set
    End Property

    Private Property StartDateRenewal() As String
        Get
            Return (CType(viewstate("StartDateRenewal"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StartDateRenewal") = Value
        End Set
    End Property

    Private Property MaskAssID() As String
        Get
            Return (CType(viewstate("MaskAssID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("MaskAssID") = Value
        End Set
    End Property

    Private Property InsuranceComBranchID() As String
        Get
            Return (CType(viewstate("InsuranceComBranchID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceComBranchID") = Value
        End Set
    End Property


    Private Property AgreementNo() As String
        Get
            Return (CType(viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property

    Private Property CoverPeriod() As String
        Get
            Return (CType(viewstate("CoverPeriod"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CoverPeriod") = Value
        End Set
    End Property






    Private Property MaskAssComponent As MaskAssCalcEng
        Get
            Return (CType(ViewState("MaskAssCalcEng"), MaskAssCalcEng))
        End Get
        Set(value As MaskAssCalcEng)
            ViewState("MaskAssCalcEng") = value
        End Set
    End Property

    Private Property DiscountMin() As Double
        Get
            Return (CType(ViewState("DiscountMin"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiscountMin") = Value
        End Set
    End Property
    Private Property DiscountMax() As Double
        Get
            Return (CType(ViewState("DiscountMax"), Double))
        End Get
        Set(ByVal Value As Double)
            ViewState("DiscountMax") = Value
        End Set
    End Property
    Private Property CoverageTypeMs() As DataTable
        Get
            Return CType(ViewState("CoverageTypeMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("CoverageTypeMs") = Value
        End Set
    End Property
#End Region




#Region "Constanta"

    Private oController As New InsCoAllocationDetailListController
    Protected WithEvents oInsuranceBranchName As UcInsuranceBranchName 


#End Region
#Region "InitialDEfaultPanel"

    Public Sub InitialDefaulPanel()
        'PnlGrid.Visible = False
        pnlResultGrid.Visible = False
        oInsuranceBranchName.FillRequired = True
        oInsuranceBranchName.loadInsBranch(True)
    End Sub

#End Region

    Function LinkToAgreement(ByVal strStyle As String, ByVal strApplicationId As String) As String
        Return "javascript:OpenAgreementNo('" & strStyle & "', '" & strApplicationId & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then

            Dim DataTdkLengkap As Boolean = False
            InitialDefaulPanel()
            Me.AgreementNo = Request.QueryString("AgreementNo")
            Me.ApplicationID = Request.QueryString("ApplicationId")
            Me.BranchID = Request.QueryString("BranchId")
            LblAgreementNo.Text = Me.AgreementNo

            Dim oInsCoAllocationDetailList As New Parameter.InsCoAllocationDetailList

            With oInsCoAllocationDetailList
                .AgreementNo = Me.AgreementNo
                .ApplicationID = Me.ApplicationID
                .BranchId = Me.BranchID
                HplinkAgreementNo.Text = Me.AgreementNo
                HplinkAgreementNo.NavigateUrl = LinkToAgreement("Insurance", Me.ApplicationID)
                .strConnection = GetConnectionString()

            End With
            txtstartDate.Text = Me.BusinessDate.ToString("dd/MM/yyyy")
            txtEndDate.Text = Me.BusinessDate.AddYears(1).ToString("dd/MM/yyyy")
            Try
                oInsCoAllocationDetailList = oController.GetListByApplicationId(oInsCoAllocationDetailList)


                With oInsCoAllocationDetailList
                    lblTenor.Text = CType(.Tenor, String)
                    lblAssetYear.Text = .AssetYear
                    LblCustomerName.Text = .CustomerName
                    HpLinkCustName.Text = .CustomerName
                    HpLinkCustName.NavigateUrl = LinkToCustomer(.CustomerID, "Insurance")
                    LblInsuranceAssetType.Text = .AssetTypeDescr
                    LblAssetNewUSed.Text = .UsedNew
                    LblAssetUsage.Text = .AssetUsageDescr
                    LblTotalStandardPremium.Text = FormatNumber(CType(.TotalStdPremium, String), 2, , , )
                    ' LblGoLiveDate.Text = Format(CDate(.GoLiveDate), "dd/MM/yyyy")
                    LblGoLiveDate.Text = .GoLiveDate.ToString("dd/MM/yyyy")
                    Me.StartDateRenewal = .StartDateRenewal.ToString("dd/MM/yyyy")
                    'Format(CDate(oRow("NextInstallmentDate")), "dd/MM/yyyy")
                    LblAdminFee.Text = FormatNumber(CType(.AdminFeeToCust, String), 2, , , )
                    LblStampDutyFee.Text = FormatNumber(CType(.MeteraiFeeToCust, String), 2, , , )
                    LblAmountCoverage.Text = FormatNumber(CType(.AmountCoverage, String), 2, , , )

                    LblTotalPremiumByCust.Text = FormatNumber(.PremiumAmountByCust, 2, , , )

                    LblLoadingFee.Text = FormatNumber(CType(.LoadingFeeToCust, String), 2, , , )
                    LblApplicationIDHidden.Text = .ApplicationID
                    LblBranchIDHidden.Text = .BranchId
                    LBlDataExist.Text = CType(.DataExist, String)
                    LblInsuredByDescr.Text = .InsuredByDescr
                    LblPaidBy.Text = .PaidByDescr
                    LblFlagInsActivationHidden.Text = .FlagInsActivation

                    Me.MaturityDate = .MaturityDate.ToString("dd/MM/yyyy")
                    Me.MaxYearNum = .MaxYearNum
                    LblMaturityDate.Text = Me.MaturityDate

                    Me.FlagInsActivation = .FlagInsActivation
                    Me.ApplicationID = LblApplicationIDHidden.Text
                    Me.BranchID = LblBranchIDHidden.Text
                    Me.InsuranceComBranchID = .InsuranceComBranchID
                      
                End With

                'btnCancelatas.Visible = True

                DisplayGridInsco()
                MaskAssComponent = New MaskAssCalcEng
                With MaskAssComponent
                    .ApplicationId = Me.ApplicationID
                    .BranchId = Me.sesBranchId.Replace("'", "").Trim
                    .strConnection = GetConnectionString()
                End With

                oController.InitMaskAssComponent(MaskAssComponent)

                MaskAssComponent.ApplicationType = oInsCoAllocationDetailList.ApplicationType
                MaskAssComponent.UsageID = oInsCoAllocationDetailList.UsageID
                MaskAssComponent.NewUsed = oInsCoAllocationDetailList.UsedNew
                MaskAssComponent.AmountCoverage = oInsCoAllocationDetailList.AmountCoverage 
                MaskAssComponent.InsuranceType = oInsCoAllocationDetailList.InsuranceType 
                MaskAssComponent.IsPageSourceCompanyCustomer = True 
                MaskAssComponent.DiscountToCust = 0
                MaskAssComponent.PaidAmountByCust = 0


                oInsuranceBranchName.RemoveById(InsuranceComBranchID.Trim)

                 
                Me.CoverageTypeMs = New DataTable("CoverageTypeMs")
                Dim objAdapter As New SqlDataAdapter(CommonVariableHelper.SQL_QUERY_COVERAGE_TYPEMS, GetConnectionString)
                objAdapter.Fill(Me.CoverageTypeMs)

                DgridInsuranceProcess()



            Catch ex As Exception
                ' ex.WriteLog(ex.Message, "MAXILOAN", "ModuleInsurance")
                Dim err As New MaxiloanExceptions
                err.WriteLog("InsCoAllocationDetailEdit", "Page_Load", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                ShowMessage(lblMessage, ex.Source + ex.TargetSite.Name + ex.Message + ex.StackTrace, True)
                'LblErrorMessages.Text = MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND
                'LblErrorMessages.Text = ex.Source + ex.TargetSite.Name + ex.Message + ex.StackTrace
                'btnOkatas.Visible = False
            End Try


        End If


    End Sub

    Private Sub DisplayGridInsco()

        Dim oInsCoAllocationInsCO As New Parameter.InsCoAllocationDetailList
        Dim dtEntityInsCO As New DataTable
        Dim dtEntityInsCOD As New DataTable
        With oInsCoAllocationInsCO
            .ApplicationID = Me.ApplicationID 
            .BranchId = Me.sesBranchId.Replace("'", "").Trim
            .strConnection = GetConnectionString()
        End With
         
        oInsCoAllocationInsCO = oController.GetGridInsCoPremiumView(oInsCoAllocationInsCO)

        If Not oInsCoAllocationInsCO Is Nothing Then
            dtEntityInsCOD = oInsCoAllocationInsCO.ListData2
            dtEntityInsCO = oInsCoAllocationInsCO.ListData
        End If

        Dim foundRows = dtEntityInsCO.Select(String.Format(" MaskAssBranchID LIKE '{0}%'", Me.InsuranceComBranchID.Trim))
        If Not foundRows Is Nothing Then
            lblAsuransi.Text = foundRows(0)("MaskapaiAsuransi")
        End If  
        gridDetailInsco.DataSource = dtEntityInsCOD
        gridDetailInsco.DataBind()
         

    End Sub
#End Region

    Public Sub DgridInsuranceProcess()    
        DGridInsurance.DataSource = MaskAssComponent.InsuranceCoverType
        DGridInsurance.DataBind()
          
    End Sub


    Public Sub DDLBolOptionHeader_OnTextChanged(ByVal sender As Object, ByVal e As EventArgs)

        Dim DDLOptionHeader As DropDownList, DDLOptionHeaderName As String
        DDLOptionHeader = CType(sender, DropDownList)
        DDLOptionHeaderName = DDLOptionHeader.ID

        For Each dt As DataGridItem In DgridInsurance.Items
            CType(dt.FindControl(DDLOptionHeaderName.Substring(0, DDLOptionHeaderName.Length - 6)), DropDownList).SelectedValue = DDLOptionHeader.SelectedValue
        Next

    End Sub

    Private Sub DgridInsurance_DataBinding(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGridInsurance.ItemDataBound
        'Dim CboCoverageTypeDT As New DataTable("CoverageTypeMs") 
        Dim DDLCoverageTypePilihan As ucCoverageTypeApk 
        Dim LblHiddenCoverageTypeDGrid As Label 
        Dim LblPremiumDGrid As Label


        If e.Item.ItemIndex = -1 Then 
            'Dim strsqlQuery As String
            'strsqlQuery = CommonVariableHelper.SQL_QUERY_COVERAGE_TYPEMS
            'Dim objAdapter As New SqlDataAdapter(strsqlQuery, GetConnectionString)
            'objAdapter.Fill(CboCoverageTypeDT)
            'Me.CoverageTypeMs = CboCoverageTypeDT
        Else

            '============= Handle CoverageType =============

            DDLCoverageTypePilihan = CType(e.Item.FindControl("DDLCoverageTypeDgrid"), ucCoverageTypeApk)
            LblHiddenCoverageTypeDGrid = CType(e.Item.FindControl("LblHiddenCoverageTypeDGrid"), Label)
            'CboCoverageTypeDT = Me.CoverageTypeMs
            Dim cbodvCoverageType As DataView
            cbodvCoverageType = CoverageTypeMs.DefaultView


            With DDLCoverageTypePilihan
                .DataSource = cbodvCoverageType
                .DataTextField = "Description"
                .DataValueField = "ID"
                .DataBind()
                .ClearSelection()
                .SelectedValue = Trim(LblHiddenCoverageTypeDGrid.Text)
                .GridIndex = e.Item.ItemIndex
            End With
            LblPremiumDGrid = CType(e.Item.FindControl("LblPremiumDGrid"), Label)
 
            '================= Handles =======================

            Dim txtNilaiTPL As ucNumberFormat
            Dim txtNilaiPA As ucNumberFormat
            Dim txtNilaiPADriver As ucNumberFormat
            Dim _tTPLAmount = CType(e.Item.FindControl("LblTPLAmount"), Label).Text
            txtNilaiTPL = CType(e.Item.FindControl("txtNilaiTPL"), ucNumberFormat)
            txtNilaiTPL.Text = FormatNumber(0, 0) ' FormatNumber(CDec(_tTPLAmount).ToString, 0)
            txtNilaiTPL.TextCssClass = "small_text numberAlign"

            Dim _tPAmount = CType(e.Item.FindControl("LblPAmount"), Label).Text
            txtNilaiPA = CType(e.Item.FindControl("txtNilaiPA"), ucNumberFormat)
            txtNilaiPA.Text = FormatNumber(0, 0) 'FormatNumber(CDec(_tPAmount).ToString, 0)
            txtNilaiPA.TextCssClass = "small_text numberAlign"

            Dim _tPADrivermount = CType(e.Item.FindControl("LblPADriverAmount"), Label).Text
            txtNilaiPADriver = CType(e.Item.FindControl("txtNilaiPADriver"), ucNumberFormat)
            txtNilaiPADriver.Text = FormatNumber(0, 0) 'FormatNumber(CDec(_tPADrivermount).ToString, 0)
            txtNilaiPADriver.TextCssClass = "small_text numberAlign"
 
            Dim DDLBolPADRIVEROption As DropDownList

            DDLBolPADRIVEROption = CType(e.Item.FindControl("DDLBolPADRIVEROption"), DropDownList)
            If DDLCoverageTypePilihan.SelectedValue = "ARK" Then DDLBolPADRIVEROption.SelectedValue = "1" Else DDLBolPADRIVEROption.SelectedValue = "0"

        End If

    End Sub
   
    Private Sub imgCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        With MaskAssComponent
            .MaskAssBranchIDChoosed = oInsuranceBranchName.SelectedValue 
        End With

        For Each item In DGridInsurance.Items
            Dim LblYearInsurance = CType(item.FindControl("LblYearInsurance"), Label).Text 
            Dim CoverageTpye = CType(item.FindControl("DDLCoverageTypeDgrid"), ucCoverageTypeApk).SelectedValue.Trim
            ' lblYearNumRate = CType(item.FindControl("lblYearNumRate"), Label)
            Dim SRCC = CBool(CType(item.FindControl("DDLBolSRCCOption"), DropDownList).SelectedItem.Value.Trim)

            Dim Flood = CBool(CType(item.FindControl("DDLBolFLOODOption"), DropDownList).SelectedItem.Value.Trim)
            Dim PA = CBool(CType(item.FindControl("DDLBolPAPAXOption"), DropDownList).SelectedItem.Value.Trim)
            Dim PADriver = CBool(CType(item.FindControl("DDLBolPADRIVEROption"), DropDownList).SelectedItem.Value.Trim)

            Dim EQVET = CBool(CType(item.FindControl("DDLBolIQVETOption"), DropDownList).SelectedItem.Value.Trim)
            Dim Terror = CBool(CType(item.FindControl("DDLBolTERROROption"), DropDownList).SelectedItem.Value.Trim)


            Dim TPLOption = CBool(CType(item.FindControl("DDLBolTPLOption"), DropDownList).SelectedItem.Value.Trim)
            Dim TPLAmount = CType(CType(item.FindControl("txtNilaiTPL"), ucNumberFormat).Text, Double) 
 
            Dim cverType = MaskAssComponent.InsuranceCoverType.Where(Function(it) it.YEARInsurance = CInt(LblYearInsurance)).SingleOrDefault()
            If (Not (cverType Is Nothing)) Then
                With cverType
                    .COVERAGETYPE = CoverageTpye
                    .SRCC = SRCC
                    .FLOOD = Flood
                    .PA = PA
                    .PADriver = PADriver
                    .EQVET = EQVET
                    .TERROR = Terror
                    .TPL = TPLOption
                    .TPLAmount = TPLAmount
                End With
            End If
        Next
        Dim result = oController.CalcKoreksiMaskapaiAss(MaskAssComponent)

        pnlResultGrid.Visible = True
        gridInsco.DataSource = result.Tables(0)
        gridInsco.DataBind()

        gridDetailInsco1.DataSource = result.Tables(1)
        gridDetailInsco1.DataBind()
    End Sub

    Private Sub gridInsco_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gridInsco.ItemDataBound
        If e.Item.ItemIndex > -1 Then
            Dim i As Integer = e.Item.ItemIndex
            Dim lblGridAdminFee As Label
            Dim lblGridGrandTotal As Label
            Dim lblGridMeteraiFee As Label
            If oInsuranceBranchName.SelectedValue.Trim = CType(e.Item.FindControl("lblGridInsuranceComBranchID"), Label).Text.Trim Then 
                e.Item.CssClass = "item_grid item_gridselected" 
                lblGridGrandTotal = CType(e.Item.FindControl("lblGridGrandTotal"), Label)
                lblGridAdminFee = CType(e.Item.FindControl("lblGridAdminFee"), Label)
                lblGridMeteraiFee = CType(e.Item.FindControl("lblGridMeteraiFee"), Label)

                'txtAmountRate.Text = FormatNumber(CDbl(lblGridGrandTotal.Text) - CDbl(lblGridAdminFee.Text) - CDbl(lblGridMeteraiFee.Text), 0)
                'calculatePremiGross()

                ' lblMaskapaiAsuransi.Text = CType(e.Item.FindControl("lblMaskapaiAsuransiDesc"), Label).Text
            End If
        End If
    End Sub


    Private Sub btnOkatas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        MaskAssComponent.CoverPeriodKoreksi = cboCoverPeriod.SelectedValue().Trim
        MaskAssComponent.TglDariKoreksi = ConvertDate2(txtstartDate.Text)
        MaskAssComponent.TglSampaiKoreksi = ConvertDate2(txtEndDate.Text)

        If (MaskAssComponent.TglSampaiKoreksi.Ticks - MaskAssComponent.TglDariKoreksi.Ticks < 0) Then
            ShowMessage(lblMessage, "Tanggal Penutupan Tidak Boleh Kecil dari Busines Date", True)
            Exit Sub
        End If 
        Try
            Dim result = oController.KoreksiMaskapaiAss(MaskAssComponent)
            If result.Trim <> "OK" Then
                ShowMessage(lblMessage, result, True)
                Exit Sub
            Else
                ShowMessage(lblMessage, "Sukses", False)
                 Response.Redirect("InsCoAllocationEdit.aspx")
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
    End Sub
#Region "Cancel dibagian paling bawah"

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("InsCoAllocationEdit.aspx")

    End Sub

#End Region

#Region "remark"

    'Private Sub btnOkatas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOkatas.Click
    '    PnlGrid.Visible = True
    '    Context.Trace.Write("Ketika Tombol Ok Dipijit !")
    '    Context.Trace.Write("Me.StartDate = " & Me.StartDate)
    '    Me.StartDate = txtodate.Text
    '    Context.Trace.Write("Check Apakah Me.StartDate Kosong ?")
    '    If Me.StartDate = "" Then
    '        Context.Trace.Write("Kalau Kosong, tampil messages")
    '        LblMessageForDate.Text = "Harap isi Tanggal"
    '        Exit Sub
    '    End If


    '    Context.Trace.Write("Me.StartDate = " & Me.StartDate)
    '    Context.Trace.Write("Ambil Tenor")
    '    '============== Ambil Tenor ========================
    '    Dim oInsCoAllocationDetailList As New Parameter.InsCoAllocationDetailList
    '    With oInsCoAllocationDetailList
    '        .ApplicationID = Me.ApplicationID
    '        .strConnection = GetConnectionString()
    '    End With

    '    Try
    '        oInsCoAllocationDetailList = oController.GetGridTenor(oInsCoAllocationDetailList)
    '    Catch ex As Exception
    '        Dim err As New MaxiloanExceptions
    '        err.WriteLog("InscoAllocatationDetailList.aspx", "ImbOk_Click-getgridtenor", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
    '        Response.Write(ex.StackTrace)
    '    End Try

    '    Dim dtEntity As New DataTable

    '    If Not oInsCoAllocationDetailList Is Nothing Then
    '        dtEntity = oInsCoAllocationDetailList.ListData
    '    End If

    '    DGridTenor.DataSource = dtEntity
    '    DGridTenor.DataBind()

    '    Context.Trace.Write("Ambil GRID INSCO PREMIUM ")
    '    '===== Ambil GRID INSCO PREMIUM =====
    '    Me.CoverPeriod = DDLCoverPeriod.SelectedItem.Value

    '    Dim oInsCoAllocationInsCO As New Parameter.InsCoAllocationDetailList

    '    With oInsCoAllocationInsCO
    '        .ApplicationID = Me.ApplicationID
    '        .CoverPeriodSelection = Me.CoverPeriod
    '        .BranchId = Me.BranchID
    '        .strConnection = GetConnectionString()
    '    End With

    '    Try
    '        oInsCoAllocationInsCO = oController.GetGridInsCoPremium(oInsCoAllocationInsCO)
    '    Catch ex As Exception
    '        Dim err As New MaxiloanExceptions
    '        err.WriteLog("InscoAllocatationDetailList.aspx", "ImbOk_Click-getgridinscopermium", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
    '        Response.Write(ex.StackTrace)

    '    End Try

    '    Dim dtEntityInsCO As New DataTable

    '    If Not oInsCoAllocationInsCO Is Nothing Then
    '        dtEntityInsCO = oInsCoAllocationInsCO.ListData
    '    End If

    '    DGridInsurance.DataSource = dtEntityInsCO
    '    DGridInsurance.DataBind()

    '    '=============== AMBIL INSURANCE STATISTIC QUOTA =============
    '    Context.Trace.Write("Ambil INSURANCE STATISTIC QUOTA ")
    '    Dim oInsquota As New Parameter.InsCoAllocationDetailList

    '    Dim bulan As Integer = CType(Month(Me.BusinessDate), Integer)
    '    Dim tahun As Integer = CType(Year(Me.BusinessDate), Integer)
    '    With oInsquota
    '        .StatMonth = CType(bulan, Int16)
    '        .StatYear = CType(tahun, Int16)
    '        .strConnection = GetConnectionString()
    '        .BranchId = Me.sesBranchId.Replace("'", "")
    '    End With
    '    Try
    '        oInsquota = oController.GetGridInsuranceStatistic(oInsquota)
    '    Catch ex As Exception
    '        Dim err As New MaxiloanExceptions
    '        err.WriteLog("InscoAllocatationDetailList.aspx", "ImbOk_Click-GetGridInsuranceStatistic", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
    '        Response.Write(ex.StackTrace)
    '    End Try

    '    Dim dtEntityInsuranceStatistic As New DataTable

    '    If Not oInsquota Is Nothing Then
    '        dtEntityInsuranceStatistic = oInsquota.ListData
    '    End If

    '    DgridQuota.DataSource = dtEntityInsuranceStatistic
    '    DgridQuota.DataBind()

    '    Dim ProcessDefaultEndDate As Date
    '    Dim StartDateMMDDYYY As String
    '    Dim Proc As New CommonSimpleRuleHelper
    '    StartDateMMDDYYY = Proc.ConvertDateReturnAsString(Me.StartDate)

    '    Context.Trace.Write("Me.MaxYearNum = " & Me.MaxYearNum)
    '    Context.Trace.Write("StartDateMMDDYYY = " & StartDateMMDDYYY)

    '    ProcessDefaultEndDate = DateAdd(DateInterval.Year, Me.MaxYearNum, CType(StartDateMMDDYYY, Date))
    '    Context.Trace.Write(" ProcessDefaultEndDate = " & ProcessDefaultEndDate)

    '    txtEndDate.Text = CType(ProcessDefaultEndDate, String)
    '    Context.Trace.Write("txtEndDate.text = " & txtEndDate.Text)

    '    Dim MaturityDateMMDDYYYY As String
    '    MaturityDateMMDDYYYY = Proc.ConvertDateReturnAsString(Me.MaturityDate)

    '    Context.Trace.Write("MaturityDateMMDDYYYY = " & MaturityDateMMDDYYYY)
    '    Context.Trace.Write("ProcessDefaultEndDate= " & ProcessDefaultEndDate)
    '    Context.Trace.Write("Apakah Maturity Date > dari txtendDate.text ?")
    '    Context.Trace.Write("Apakah " & MaturityDateMMDDYYYY & " > dari " & txtEndDate.Text & " ?")

    '    If CType(MaturityDateMMDDYYYY, Date) > CType(ProcessDefaultEndDate, Date) Then
    '        txtEndDate.Text = CType(Me.MaturityDate, String)
    '        Context.Trace.Write("Iya ....Maturity Date > dari txtendDate.text")
    '    Else

    '        Context.Trace.Write("Tidak !! ....Maturity Date < dari txtendDate.text")
    '        Context.Trace.Write("CType(ProcessDefaultEndDate, String) = " & CType(ProcessDefaultEndDate, String))
    '        txtEndDate.Text = Proc.ConvertDateReturnAsString(CType(ProcessDefaultEndDate, String))

    '    End If
    '    Context.Trace.Write("CType(Me.MaturityDate, String) = " & CType(Me.MaturityDate, String))
    '    Context.Trace.Write("Proc.ConvertDateMMDDYYY(CType(ProcessDefaultEndDate, String)) =" & Proc.ConvertDateReturnAsString(CType(ProcessDefaultEndDate, String)))
    '    Context.Trace.Write("txtEndDate.text =" & txtEndDate.Text)

    '    oInsuranceBranchName.loadInsBranch(False)
    '    oInsuranceBranchName.FillRequired = True

    '    Me.EndDateEntry = txtEndDate.Text
    '    Context.Trace.Write("Me.EndDateEntry = " & Me.EndDateEntry)

    'End Sub 

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

    '    If SessionInvalid() Then
    '        Exit Sub
    '    End If

    '    Context.Trace.Write("Ketika Tombol Save Dipijit !")
    '    Me.InsuranceComBranchID = oInsuranceBranchName.SelectedValue

    '    If Me.EndDateEntry = "" Then
    '        LblMessageEndDate.Text = "Harap isi Tanggal Berlaku Sampai"
    '        Exit Sub
    '    End If


    '    '==================== Grid Tenor ==================================

    '    '========================================= Grid Insco Premium ===========================
    '    Context.Trace.Write("Grid Insco Premium  !")
    '    Dim LoopGridInsco As Int16

    '    For LoopGridInsco = 0 To CType(DGridInsurance.Items.Count - 1, Int16)
    '        Context.Trace.Write("Awal LoopGridInsco =" & LoopGridInsco)

    '        Dim LblInsuranceComBranchID As Label
    '        LblInsuranceComBranchID = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridInsuranceComBranchID"), Label)
    '        Dim LblGrandTotal As Label
    '        LblGrandTotal = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridGrandTotal"), Label)

    '        If Me.InsuranceComBranchID.Trim.ToLower = CType(LblInsuranceComBranchID.Text, String).Trim.ToLower Then
    '            Dim LblMainPremiumSave As Label
    '            LblMainPremiumSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridMainPremium"), Label)
    '            Dim LblSRCCPremiumSave As Label
    '            LblSRCCPremiumSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridSRCCPremium"), Label)
    '            Dim LblTPLPremiumSave As Label
    '            LblTPLPremiumSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridTPLPremium"), Label)

    '            Dim LblFloodPremiumSave As Label
    '            LblFloodPremiumSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridFloodPremium"), Label)

    '            Dim LblLoadingFeeSave As Label
    '            LblLoadingFeeSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridLoadingFee"), Label)

    '            Dim LblAdminFeeSave As Label
    '            LblAdminFeeSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridAdminFee"), Label)

    '            Dim LblMeteraiFeeSave As Label
    '            LblMeteraiFeeSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridMeteraiFee"), Label)

    '            Dim LblGrandTotalSave As Label
    '            LblGrandTotalSave = CType(DGridInsurance.Items(LoopGridInsco).FindControl("LblGridGrandTotal"), Label)

    '            LblPremiumInsCo.Text = CType(LblGrandTotal.Text, String)


    '            '====Proses Save dilakukan !!!! ==================
    '            Dim oSaveInsuranceCompanySelection As New Parameter.InsCoAllocationDetailList
    '            Dim oController As New InsCoAllocationDetailListController

    '            'Me.EndDateEntry di-update karena ada kemungkinan user merubah default EndDate
    '            Me.EndDateEntry = txtEndDate.Text

    '            With oSaveInsuranceCompanySelection
    '                .BranchId = Me.BranchID
    '                .ApplicationID = Me.ApplicationID
    '                .StartDate = ConvertDate2(Me.StartDate)
    '                .EndDate = ConvertDate2(Me.EndDateEntry)
    '                .InsuranceComBranchID = Me.InsuranceComBranchID
    '                .MainPremiumToInsco = CType(LblMainPremiumSave.Text, Double)
    '                .RSCCToInsco = CType(LblSRCCPremiumSave.Text, Double)
    '                .TPLToInsco = CType(LblTPLPremiumSave.Text, Double)
    '                .FloodToInsco = CType(LblFloodPremiumSave.Text, Double)
    '                .LoadingFeeToInsco = CType(LblLoadingFeeSave.Text, Double)
    '                .TotalPremiumToInsco = CType(LblGrandTotalSave.Text, Double)
    '                .AdminFee = CType(LblAdminFeeSave.Text, Double)
    '                .MeteraiFee = CType(LblMeteraiFeeSave.Text, Double)
    '                .StatMonth = CType(Me.BusinessDate.Month, Int16)
    '                .StatYear = CType(Me.BusinessDate.Year, Int16)
    '                .BusinessDate = Me.BusinessDate
    '                .CoyID = Me.SesCompanyID
    '                .CoverPeriodSelection = Me.CoverPeriod
    '                .strConnection = GetConnectionString()
    '                .spName = "SpInscoSelectionUpdateAssetDetailEDIT"

    '                Context.Trace.Write("BranchId = " & .BranchId)
    '                Context.Trace.Write("ApplicationID = " & .ApplicationID)
    '                Context.Trace.Write("StartDate = " & .StartDate)
    '                Context.Trace.Write("EndDate = " & .EndDate)
    '                Context.Trace.Write("InsuranceComBranchID = " & .InsuranceComBranchID)
    '                Context.Trace.Write("MainPremiumToInsco = " & .MainPremiumToInsco)
    '                Context.Trace.Write("RSCCToInsco = " & .RSCCToInsco)
    '                Context.Trace.Write("TPLToInsco = " & .TPLToInsco)
    '                Context.Trace.Write("FloodToInsco = " & .FloodToInsco)
    '                Context.Trace.Write("LoadingFeeToInsco = " & .LoadingFeeToInsco)
    '                Context.Trace.Write("TotalPremiumToInsco = " & .TotalPremiumToInsco)
    '                Context.Trace.Write("AdminFee = " & .AdminFee)
    '                Context.Trace.Write("MeteraiFee = " & .MeteraiFee)
    '                Context.Trace.Write("StatMonth = " & .StatMonth)
    '                Context.Trace.Write("StatYear = " & .StatYear)
    '                Context.Trace.Write("BusinessDate = " & .BusinessDate)
    '                Context.Trace.Write("CoyID = " & .CoyID)
    '                Context.Trace.Write("CoverPeriodSelection = " & .CoverPeriodSelection)

    '            End With
    '            Context.Trace.Write("Akhir LoopGridInsco =" & LoopGridInsco)
    '            Dim errMessages As String = ""
    '            'sementara ini diremark dulu !
    '            errMessages = oController.SaveInsuranceCompanySelectionLastProcess(oSaveInsuranceCompanySelection)
    '            Response.Redirect("InsCoAllocationEdit.aspx?errMessages=" & errMessages & "")

    '        End If
    '    Next

    'End Sub 

    'Private Sub btnCancelatas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Response.Redirect("InsCoAllocationEdit.aspx")
    'End Sub

#End Region

End Class