﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class NewCoverProcess
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Dim m_InsNewCover As New InsNewCoverController
    Private oInsNewCover As New Parameter.InsNewCover

#End Region
#Region "properties"
    Public Property Asset() As String
        Get
            Return CType(viewstate("Asset"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Asset") = Value
        End Set
    End Property
    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property CustomerName() As String
        Get
            Return CType(viewstate("CustomerName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property

    Public Property AgreementNo() As String
        Get
            Return CType(viewstate("AgreementNo"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property







#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.FormID = "InsNewCover"

        If Not Page.IsPostBack Then


            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                Me.AgreementNo = Request.QueryString("AgreementNo")
                Me.BranchID = Request.QueryString("BranchID")
                Me.Asset = Request.QueryString("Asset")
                Me.CustomerName = Request.QueryString("Name")
                Me.ApplicationID = Request.QueryString("ApplicationID")
                fillcbo()

            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If


    End Sub
    Sub fillcbo()
        Dim m_controller As New InsNewCoverController
        Dim oGetData As New Parameter.InsNewCover
        Dim row As DataRow
        With oInsNewCover
            .strConnection = GetConnectionString()
        End With
        oGetData = m_controller.InsNewCoverGetInsured(oInsNewCover)
        With cboInsuredBy
            .DataSource = oGetData.ListData.DefaultView
            .DataValueField = "id"
            .DataTextField = "description"
            .DataBind()


        End With
        oGetData = m_controller.InsNewCoverGetPaid(oInsNewCover)
        With cboPaidBy
            .DataSource = oGetData.ListData.DefaultView
            .DataValueField = "id"
            .DataTextField = "description"
            .DataBind()
            .Items.FindByValue("CU").Selected = True

        End With
        lblAgreementNo.Text = CStr(Me.AgreementNo)
        lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(Me.ApplicationID.Trim) & "')"
        lblName.Text = CStr(Me.CustomerName)
    End Sub

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("NewCoverForExistingApp.aspx")
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click

        If cboInsuredBy.SelectedItem.Value = "CU" Then
            If cboPaidBy.SelectedItem.Value = "CU" Then
                Response.Redirect("NewCoverInsuredByCust.aspx?ApplicationID=" & Me.ApplicationID & "&Name=" & Me.CustomerName & "&Insured=" & cboInsuredBy.SelectedValue & "&Paid=" & cboPaidBy.SelectedValue)
            Else
                ShowMessage(lblmessage, "Pemilihan Dibayar Oleh Harus Customer", True)
                Exit Sub
            End If
        Else
            Response.Redirect("NewCoverInsuredByCompany.aspx?AgreementNo=" & Me.AgreementNo & "&BranchID=" & Me.BranchID & "&ApplicationID=" & Me.ApplicationID & "&Name=" & Me.CustomerName & "&Insured=" & cboInsuredBy.SelectedItem.Text & "&Paid=" & cboPaidBy.SelectedItem.Text)
        End If

    End Sub

End Class