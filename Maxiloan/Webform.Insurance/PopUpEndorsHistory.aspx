﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PopUpEndorsHistory.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.PopUpEndorsHistory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PopUpEndorsHistory</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                ENDORSEMENT HISTORY
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Kontrak</label>
            <asp:Label ID="LblAgreementNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Polis</label>
            <asp:Label ID="LblPolicyNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Endors No Polis</label>
            <asp:Label ID="LblEndorsPolicyNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Endors No Dokumen</label>
            <asp:Label ID="LblEndorsDocNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Endors No Dokumen</label>
            <asp:Label ID="LblEndorsDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DGridEndorsDetail" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="YearNum" HeaderText="TAHUN KE"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CoverageType" HeaderText="JENIS COVER"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Srcc" HeaderText="PREMI SRCC"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TPLAmount" HeaderText="PREMI TPL"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Flood" HeaderText="PREMI FLOOD"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" CausesValidation="False" runat="server" Text="Find" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
