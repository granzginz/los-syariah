﻿#Region "Imports"
Imports System.Text
Imports System.Threading
Imports System.Data.SqlClient
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsuranceEditPolicy
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oSearchBY As UcSearchBy
    Protected WithEvents oBranch As ucBranchAll
#Region "Constanta"
    'Private m_controller As New PremiumToCustomerController
    Private oInsPolicyNumber As New Parameter.InsPolicyNumber
    Private m_controller As New InsPolicyNumberControler
    Private currentPage As Int32 = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int16 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region

#Region "Property "
    Private Property AssetSequenceNo() As Integer
        Get
            Return CInt(ViewState("AssetSequenceNo"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("AssetSequenceNo") = Value
        End Set
    End Property
    Private Property InsSequenceNo() As Integer
        Get
            Return CInt(ViewState("InsSequenceNo"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("InsSequenceNo") = Value
        End Set
    End Property

    Private Property PolisNumber() As String
        Get
            Return CStr(ViewState("PolisNumber"))
        End Get
        Set(ByVal Value As String)
            ViewState("PolisNumber") = Value
        End Set
    End Property

    Private Property CustomerName() As String
        Get
            Return CStr(ViewState("CustomerName"))
        End Get
        Set(ByVal Value As String)
            ViewState("CustomerName") = Value
        End Set
    End Property

    Private Property AgreementNo() As String
        Get
            Return CStr(ViewState("AgreementNo"))
        End Get
        Set(ByVal Value As String)
            ViewState("AgreementNo") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CStr(ViewState("ApplicationID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ApplicationID") = Value
        End Set
    End Property
    Private Property OldPolicyNumber() As String
        Get
            Return CType(ViewState("OldPolicyNumber"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("OldPolicyNumber") = Value
        End Set
    End Property
    Private Property YearNum() As String
        Get
            Return CType(ViewState("YearNum"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("YearNum") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            oBranch.IsAll = False
            txtPage.Text = "1"
            oSearchBY.ListData = "AGREEMENTNO,No Kontrak-ApplicationID, No Aplikasi-Name,Nama Konsumen-MaskAssBranchId,Maskapai Asuransi-POLICYNUMBER, No Polis"
            If CheckForm(Me.Loginid, "InsRateToCustTPL", "MAXILOAN") Then
                Me.SearchBy = ""
                Me.SortBy = ""
                InitialDefaultPanel()
            End If
        End If
    End Sub

#Region "InitialDefaultPanel"
    Private Sub InitialDefaultPanel()
        PnlGrid.Visible = False
        pnlAddEdit.Visible = False
        pnlSearch.Visible = True

    End Sub

#End Region
#Region "Navigation "
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity()
    End Sub
#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
#End Region
#Region "Go Page"
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                BindGridEntity()
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity()
    End Sub

#End Region
#Region "Reseh..eh Reset !"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Trim.Replace("'", "") & "' "
        Me.SortBy = ""
        BindGridEntity()
    End Sub
#End Region
    Sub BindGridEntity()
        With oInsPolicyNumber
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString()
        End With
        oInsPolicyNumber = m_controller.GetInsPolicyNumber(oInsPolicyNumber)
        recordCount = oInsPolicyNumber.TotalRecords
        dtgPaging.DataSource = oInsPolicyNumber.ListData
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        pnlSearch.Visible = True
        PnlGrid.Visible = True
        pnlAddEdit.Visible = False
        PagingFooter()
    End Sub

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand

        If e.CommandName = "Edit" Then
            If CheckFeature(Me.Loginid, "InsRateToCustTPL", "Edit", "MAXILOAN") Then

                If SessionInvalid() Then
                    Exit Sub
                End If
            End If
            pnlAddEdit.Visible = True
            PnlGrid.Visible = False
            pnlSearch.Visible = False
            txtNewPolicy.Text = ""


            Dim HypAgree As HyperLink
            Dim HypCustname As HyperLink
            Dim HypPolNo As Label
            Dim LblInsCompany As Label
            Dim lblCustomerID As Label
            Dim lblApplicationId As Label
            Dim lblInsSequenceNo As Label
            Dim lblAssetSequence As Label
            Dim lblYearNum As Label


            HypAgree = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            HypCustname = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            HypPolNo = CType(e.Item.FindControl("hyPOLICYNO"), Label)

            LblInsCompany = CType(e.Item.FindControl("lblInsCompany"), Label)
            lblCustomerID = CType(e.Item.FindControl("lblCustomerID"), Label)
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            lblInsSequenceNo = CType(e.Item.FindControl("lblInsSequenceNo"), Label)
            lblAssetSequence = CType(e.Item.FindControl("lblAssetSequence"), Label)
            lblYearNum = CType(e.Item.FindControl("lblYearNum"), Label)

            Me.ApplicationID = lblApplicationId.Text.Trim
            Me.InsSequenceNo = CInt(lblInsSequenceNo.Text)
            Me.AssetSequenceNo = CInt(lblAssetSequence.Text)
            Me.OldPolicyNumber = HypPolNo.Text
            Me.YearNum = lblYearNum.Text


            hypAgreement.Text = HypAgree.Text
            hypCustomerName.Text = HypCustname.Text
            ltlInsCo.Text = LblInsCompany.Text

            ltlPolisNumber.Text = HypPolNo.Text
            txtPolicyNumber.Text = HypPolNo.Text

            lbltahunasuransi.Text = lblYearNum.Text

            hypCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblCustomerID.Text.Trim) & "')"
            '*** Agreement No link
            hypAgreement.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblTemp As Label
            Dim hyTemp As HyperLink
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        With oInsPolicyNumber
            .strConnection = GetConnectionString()
            .BranchId = oBranch.BranchID.Trim
            .ApplicationId = Me.ApplicationID
            .AssetSeqId = Me.AssetSequenceNo
            .InsSeqNo = Me.InsSequenceNo
            .PolicyNumber = txtNewPolicy.Text
            .BusinessDate = Me.BusinessDate
            .LoginId = Me.Loginid
            .OldPolicyNumber = Me.OldPolicyNumber
            .Yearnum = Me.YearNum
        End With
        Try
            m_controller.InsurancePolicyNumberSaveEdit(oInsPolicyNumber)
            'ShowMessage(lblMessage, MessageHelper.MESSAGE_UPDATE_SUCCESS, False)
            BindGridEntity()
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.SearchBy = " Agreement.BranchID = '" & Me.sesBranchId.Trim.Replace("'", "") & "' "
        Me.SortBy = ""
        BindGridEntity()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pnlSearch.Visible = True
        PnlGrid.Visible = True
        pnlAddEdit.Visible = False
        Dim strSearch As New StringBuilder
        strSearch.Append(" Agreement.BranchID = '" & oBranch.BranchID.Trim & "' ")

        If oSearchBY.Text <> "" Then
            strSearch.Append(" and ")

            strSearch.Append(oSearchBY.ValueID)
            strSearch.Append(" like '%")
            strSearch.Append(oSearchBY.Text)
            strSearch.Append("%'")
   
        End If
        Me.SearchBy = strSearch.ToString
        BindGridEntity()
    End Sub

End Class