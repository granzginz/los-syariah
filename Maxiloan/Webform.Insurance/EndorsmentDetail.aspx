﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EndorsmentDetail.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.EndorsmentDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EndorsmentDetail</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="LblErrMessages" runat="server"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                PERMINTAAN ENDORSMENT
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:HyperLink ID="hynAgreementNo" runat="server" NavigateUrl=""></asp:HyperLink>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:HyperLink ID="hynCustomerName" runat="server" NavigateUrl=""></asp:HyperLink>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset
            </label>
            <asp:Label ID="lblAsset" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Asuransi
            </label>
            <asp:Label ID="lblInsuranceCo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Polis
            </label>
            <asp:Label ID="lblPolicyNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Asset
            </label>
            <asp:Label ID="lblInsuranceAssetType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Penggunaan Asset
            </label>
            <asp:Label ID="lblAssetUsage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Asset : Baru / Bekas
            </label>
            <asp:Label ID="lblAssetUsedNew" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jangka Waktu
            </label>
            <asp:Label ID="lblTenor" runat="server"></asp:Label>&nbsp;month
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tahun Produksi
            </label>
            <asp:Label ID="lblYear" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Polis
            </label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Rangka
            </label>
            <asp:Label ID="lblSerialNo1" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Mesin
            </label>
            <asp:Label ID="lblSerialNo2" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jangka Waktu
            </label>
            <asp:Label ID="lblInsuranceLength" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Periode Asuransi
            </label>
            <asp:Label ID="lblPeriod1" runat="server"></asp:Label>&nbsp; S/D &nbsp;
            <asp:Label ID="lblPeriod2" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Catatan Accessories
            </label>
            <asp:TextBox ID="txtAccNotes" runat="server" TextMode="MultiLine" 
                Width="192px" Height="72px"></asp:TextBox>
        </div>
        <div class="form_right">
            <label>
                Catatan Asuransi
            </label>
            <asp:TextBox ID="txtInsNotes" runat="server" TextMode="MultiLine" 
                Width="200px" Height="73px"></asp:TextBox>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Tanggal Endorsment
            </label>
            <asp:TextBox runat="server" ID="txtEndorsmentDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtEndorsmentDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
        <div class="form_right">
            <label>
                Jumlah Prepaid
            </label>
            <asp:Label ID="lblPrepaidAmount" runat="server"></asp:Label>
        </div>
    </div>
    <table cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr class="trtopi">
            <td class="tdjudul" width="40%" height="20">&nbsp;
                
            </td>
            <td class="tdjudul" align="center" width="30%">
                SAAT INI
            </td>
            <td class="tdjudul" align="center" width="30%">
                BARU
            </td>
        </tr>
    </table>
    <table class="tblCurrentNew" cellspacing="1" cellpadding="2" width="95%" align="center"
        border="0">
        <tr class="tdganjil">
            <td class="tdgenap" style="height: 36px" width="40%">
                Nilai Pertanggungan
            </td>
            <td class="tdgenap" style="height: 36px" align="right" width="30%">
                <asp:Label ID="lblOldSum" runat="server"></asp:Label>
            </td>
            <td class="tdgenap" style="height: 36px" align="right" width="30%">
                <asp:TextBox ID="txtsuminsurednew" runat="server"  Width="185px"></asp:TextBox><asp:RequiredFieldValidator
                    ID="rgvSumInsured" runat="server" ControlToValidate="txtSumInsuredNew" Display="Dynamic"
                    ErrorMessage="Harap diisi dengan Angka">Harap Diisi</asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="regSumInsured" runat="server" Display="Dynamic" Enabled="True" ValidationExpression="^\d{1,}$|^\d{1,}[.{1}]\d{1,}$"
                        Visible="True" ErrorMessage="Harap diisi dengan Angka" ControlToValidate="txtSumInsuredNew"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr class="tdganjil">
            <td class="tdgenap" width="40%">
                Premi Utama Oleh Customer
            </td>
            <td class="tdgenap" align="right" width="30%">
                <asp:Label ID="lblMainPremiumByCust_C" runat="server"></asp:Label>
            </td>
            <td class="tdgenap" align="right" width="30%">
                <asp:Label ID="lblMainPremiumByCust_N" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="tdganjil">
            <td class="tdganjil" width="40%">
                Premi Utama Ke Asuransi
            </td>
            <td class="tdganjil" align="right" width="30%">
                <asp:Label ID="lblMainPremiumToInsCo_C" runat="server"></asp:Label>
            </td>
            <td class="tdganjil" align="right" width="30%">
                <asp:Label ID="lblMainPremiumToInsCo_N" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="tdganjil">
            <td class="tdgenap" width="40%">
                Jumlah Premi Oleh Customer
            </td>
            <td class="tdgenap" align="right" width="30%">
                <asp:Label ID="lblPremiumAmountByCust_C" runat="server"></asp:Label>
            </td>
            <td class="tdgenap" align="right" width="30%">
                <asp:Label ID="lblPremiumAmountByCust_N" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="tdganjil">
            <td class="tdganjil" width="40%">
                Jumlah Premi Ke Asuransi
            </td>
            <td class="tdganjil" align="right" width="30%">
                <asp:Label ID="lblPremiumAmountToInsCo_C" runat="server"></asp:Label>
            </td>
            <td class="tdganjil" align="right" width="30%">
                <asp:Label ID="lblPremiumAmountToInsCo_N" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlChoice" runat="server" Width="100%" Visible="TRUE">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInsDetail" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                        AllowSorting="True" DataKeyField="Year" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblInsYear" runat="server" Text='<%#Container.DataItem("Year")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COVER SAAT INI">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCoverageCurrent" runat="server" Text='<%#Container.DataItem("CoverageType")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COVER BARU">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboCoverageNew" runat="server"  Enabled="True">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH TPL SAAT INI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTPLAmountCurr" runat="server" Visible="true" Text='<%# formatnumber(Container.DataItem("TPLAmountToCust"),2)%>'>
                                    </asp:Label>
                                    <asp:Label ID="lblTPLPremiumCurr" runat="server" Visible="false" Text='<%#Container.DataItem("TPLPremiumToCust")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH TPL BARU">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboTPLAmountNew" runat="server"  Enabled="True">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SRCC SAAT INI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblSRCCCurrent" runat="server" Text='<%#Container.DataItem("SRCCPremiumToCust")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SRCC BARU">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboSRCCNew" runat="server"  Enabled="True">
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLOOD SAAT INI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblFloodCurrent" runat="server" Text='<%#Container.DataItem("FloodPremiumToCust")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLOOD BARU">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:DropDownList ID="cboFloodNew" runat="server"  Enabled="True">
                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                        <asp:ListItem Value="0" Selected="True">No</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="StartDate">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# FORMAT(Container.DataItem("StartDate"),"dd/MM/yyyy")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="EndDate">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# format(Container.DataItem("EndDate"),"dd/MM/yyyy")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlRecalculateResult" runat="server" Visible="TRUE">
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgResult" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                        DataKeyField="Year" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultYear" runat="server" Text='<%#Container.DataItem("Year")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COVER SAAT INI">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultCoverage" runat="server" Text='<%# Container.DataItem("OldCoverage") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COVER BARU">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultNewCoverage" runat="server" Text='<%# Container.DataItem("NewCoverage") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH TPL SAAT INI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultTPL" runat="server" Text='<%# formatnumber(Container.DataItem("OldTPL"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH TPL BARU">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultNewTPL" runat="server" Text='<%# formatnumber(Container.DataItem("NewTPLAmount"),2)%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SRCC SAAT INI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultSRCC" runat="server" Text='<%#Container.DataItem("OldSRCC")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SRCC BARU">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultNewSRCC" runat="server" Text='<%# GetStatus(Container.DataItem("NewSRCC"))%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLOOD SAAT INI">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultFlood" runat="server" Text='<%#Container.DataItem("OldFlood")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLOOD BARU">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultNewFlood" runat="server" Text='<%# GetStatus(Container.DataItem("NewFlood"))%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DgridResultPremToCust" runat="server" AutoGenerateColumns="False"
                        AllowSorting="True" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblYearNew" runat="server" Text='<%#Container.DataItem("Year")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI UTAMA KE CUST LAMA">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblOldMainPremium" runat="server" Text='<%# FormatNumber(Container.DataItem("PrevMainToCust"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI SRCC KE CUST LAMA">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblOldSRCCPremium" runat="server" Text='<%# FormatNumber(Container.DataItem("SRCCPremiumToCust"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI FLOOD KE CUST LAMA">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblOldFloodPremium" runat="server" Text='<%# FormatNumber(Container.DataItem("FloodPremiumToCust"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI TPL KE CUST LAMA">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblOldTPLPremium" runat="server" Text='<%# FormatNumber(Container.DataItem("TPLPremiumToCust"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LOADING FEE KE CUST LAMA">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblOldLoadingFeeToCust" runat="server" Text='<%# FormatNumber(Container.DataItem("OldLoadingFeeToCust"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DISC KE CUST">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("DiscToCust"),2) %>'
                                        ID="LblDiscToCust">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DIBAYAR OLEH CUSTOMER">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("PaidAmtByCust"),2) %>'
                                        ID="LblPaidAmtByCust"> 
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DgridResultNewPremToCust" runat="server" AutoGenerateColumns="False"
                        AllowSorting="True" DataKeyField="Year" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblYearToCust" runat="server" Text='<%#Container.DataItem("Year")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="HARI REFUND">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblRefundDay" runat="server" Text='<%#Container.DataItem("RefundDays")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND PREMI KE CUST">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("OldPremiCust"),2) %>'
                                        ID="LblRefundPremiCust">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND SRCC KE CUST">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("OldSRCCCust"),2) %>'
                                        ID="LblRefundSRCCPremiumToCustAmount">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND FLOOD KE CUST">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("OldFloodCust"),2) %>'
                                        ID="LblrefundFloodPremiumToCustAmount">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND TPL KE CUST">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("OldTPLCust"),2) %>'
                                        ID="LblRefundTPLPremiumToCust">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND LOADING FEE KE CUST">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("oldLoadingFeeToCust"),2) %>'
                                        ID="lbloldLoadingFeeToCust">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI KE CUST BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewPremiCust"),2) %>'
                                        ID="LblNewPremiCust">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SRCC KE CUST BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewSRCCCust"),2) %>'
                                        ID="LblSRCCPremiumToCustAmount">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLOOD KE CUST BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewFloodCust"),2) %>'
                                        ID="LblFloodPremiumToCustAmount">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TPL KE CUST BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewTPLCust"),2) %>'
                                        ID="LblTPLPremiumToCust">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LOADING FEE KE CUST BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewLoadingFeeToCustomer"),2) %>'
                                        ID="lblNewLoadingFeeToCustomer">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DgridResultPremToInsco" runat="server" AutoGenerateColumns="False"
                        AllowSorting="True" DataKeyField="Year" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblResultYearPremToCust" runat="server" Text='<%#Container.DataItem("Year")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI DARI ASR LAMA">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# FormatNumber(Container.DataItem("PrevMainToInsCo"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI SRCC ASR LAMA">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# FormatNumber(Container.DataItem("SRCCPremiumToInsCo"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI FLOOD ASR LAMA">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("FloodPremiumToInsco"),2) %>'
                                        ID="Label2">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI TPL ASR LAMA">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# FormatNumber(Container.DataItem("TPLPremiumToInsCo"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LOADING FEE ASR LAMA">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("oldLoadingFeeToInsco"),2) %>'
                                        ID="lbloldLoadingFeeToInsco">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH DIBAYAR ASR">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# FormatNumber(Container.DataItem("PaidAmtToInsCo"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DgridResultNewPremToInsco" runat="server" AutoGenerateColumns="False"
                        AllowSorting="True" DataKeyField="Year" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblYearInsco" runat="server" Text='<%#Container.DataItem("Year")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND PREMI DARI ASR">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("OldPremiInsco"),2) %>'
                                        ID="lblrefundinsco">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND SRCC DARI ASR">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# FormatNumber(Container.DataItem("OldSRCCInsco"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND FLOOD DARI ASR">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("OldFloodInsco"),2) %>'
                                        ID="lblrefundfloodinsco">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND TPL DARI ASR">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# FormatNumber(Container.DataItem("OldTPLInsco"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="REFUND LOADING FEE DARI ASR">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("oldLoadingFeeToInsco"),2) %>'
                                        ID="lbloldLoadingFeeToInsco">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI DARI ASR BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewPremiInsco"),2) %>'
                                        ID="LblNewPremiInsco">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SRCC DARI ASR BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewSRCCInsco"),2) %>'
                                        ID="lblnewsrccinsco">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FLOOD DARI ASR BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# FormatNumber(Container.DataItem("NewFloodInsco"),2) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TPL DARI ASR BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewTPLInsCo"),2) %>'
                                        ID="LblNewtplInsco">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LOADING FEE DARI ASR BARU">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# FormatNumber(Container.DataItem("NewLoadingFeeToInsco"),2) %>'
                                        ID="lblNewLoadingFeeToInsco">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                </label>
                <label>
                    REFUND PREMI
                </label>
            </div>
            <div class="form_right">
                <label>
                    PREMI BARU
                </label>
                <label>
                    TAMBAHAN PREM
                </label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Premi Utama
                </label>
                <asp:Label ID="lblBasicRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblBasicNew" runat="server"></asp:Label>
                <asp:Label ID="lblBasicAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Premi SRCC
                </label>
                <asp:Label ID="lblSRCCRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblSRCCNew" runat="server"></asp:Label>
                <asp:Label ID="lblSRCCAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Premi TPL
                </label>
                <asp:Label ID="lblTPLRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblTPLNew" runat="server"></asp:Label>
                <asp:Label ID="lblTPLAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Premi Flood
                </label>
                <asp:Label ID="lblFloodRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblFloodNew" runat="server"></asp:Label>
                <asp:Label ID="lblFloodAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Loading Fee
                </label>
                <asp:Label ID="lblRefundLoadingFee" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblLoadingFeeNew" runat="server"></asp:Label>
                <asp:Label ID="lblLoadingFeeAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Admin
                </label>
                <asp:Label ID="lblAdminFeeRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblAdminFeeNew" runat="server"></asp:Label>
                <asp:Label ID="lblAdminFeeAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Meterai
                </label>
                <asp:Label ID="lblMeteraiFeeRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblMeteraiFeeNew" runat="server"></asp:Label>
                <asp:Label ID="lblMeteraiFeeAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Total Premi
                </label>
                <asp:Label ID="lblTotalPremiumRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblTotalPremiumNew" runat="server"></asp:Label>
                <asp:Label ID="lblTotalPremiumAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label class ="label_req">
                    Discount Customer
                </label>
            </div>
            <div class="form_right">
                <asp:TextBox ID="txtDiscToCustNew" runat="server" Width="185px" ></asp:TextBox>
                <asp:RangeValidator ID="rv1" runat="server" ErrorMessage="Harap diisi" ControlToValidate="txtDiscToCustNew"
                    Type="Double" MaximumValue="999999999999" MinimumValue="-999999999999"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="rfvDiscount" runat="server" ErrorMessage="Harap Diisi"
                    ControlToValidate="txtDiscToCustNew">Harap Diisi</asp:RequiredFieldValidator>
                <asp:Label ID="lblDiscToCustAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    <b>Total Premi Ke Customer</b>
                </label>
                <asp:Label ID="lblTotalPremiToCustRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblTotalPremiToCustNew" runat="server"></asp:Label>
                <asp:Label ID="lblTotalPremiToCustAdditional" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    <b>Total Premium To Asuransi</b>
                </label>
                <asp:Label ID="lblTotalPremiToInsCoRefund" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <asp:Label ID="lblTotalPremiToInsCONew" runat="server"></asp:Label>
                <asp:Label ID="lblTotalPremiToInsCoAdditional" runat="server"></asp:Label>
            </div>
        </div>
    </asp:Panel>
    <div class="form_button">
        <asp:Button ID="btnPrint" runat="server" CausesValidation="true" Text="Print" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
        <asp:Button ID="btnRecalculate" runat="server" CausesValidation="true" Text="ReCalculate"
            CssClass="small button green"></asp:Button>
    </div>
    </form>
</body>
</html>
