﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ViewPolicyDetail.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.ViewPolicyDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ViewPolicyDetail</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
<!--        function windowClose() {
            window.close();
        }
        function OpenWinEndorsDetail(ApplicationID, BranchID, Ass, Ins, BDEndors) {
            var x = screen.width; var y = screen.height - 100; window.open('Inquiry/ViewEndorsInqDetail.aspx?ApplicationID=' + ApplicationID + '&BranchID=' + BranchID + '&Ass=' + Ass + '&Ins=' + Ins + '&BDEndors=' + BDEndors + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
//-->
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                VIEW - DETAIL POLIS
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:Label ID="LblAgreementNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:Label ID="LblCustomerName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama Asset</label>
            <asp:Label ID="LblAssetDescription" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Asset Asuransi
            </label>
            <asp:Label ID="LblInsuranceAssetType" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Penggunaan
            </label>
            <asp:Label ID="LblAssetUsage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Asset : Baru/Bekas
            </label>
            <asp:Label ID="LblAssetNewUsed" runat="server"></asp:Label>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Catatan Accessories
            </label>
            <asp:Label ID="LblAccNotes" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Catatan Asuransi
            </label>
            <asp:Label ID="LblInsNotes" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Perusahaan Asuransi</label>
            <asp:Label ID="LblInsuranceCompany" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No SPPA
            </label>
            <asp:Label ID="LblSPPANo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal SPPA
            </label>
            <asp:Label ID="LblSPPADate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Polis
            </label>
            <asp:Label ID="LblPolicyNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Polis
            </label>
            <asp:Label ID="LblPolicyDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Periode Asuransi
            </label>
            <asp:Label ID="LblInsuranceStartDate" runat="server"></asp:Label>&nbsp;S/D&nbsp;
            <asp:Label ID="LblInsuranceEndDate" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nilai Pertanggungan
            </label>
            <asp:Label ID="LblSumInsured" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi Ke Customer
            </label>
            <asp:Label ID="LblPremiumAmountByCust" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah Dibayar Customer
            </label>
            <asp:Label ID="LblPaidAmountByCust" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi Ke Pers Asuransi
            </label>
            <asp:Label ID="LblPremiumAmountToInsCo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Jumlah DiBayar ke Pers Asuransi
            </label>
            <asp:Label ID="LblPaidAmountToInsco" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                DETAIL INFORMASI ASURANSI
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DGridInsDetail" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="YearNum" HeaderText="TAHUN"></asp:BoundColumn>
                        <asp:BoundColumn DataField="SumInsuredAmount" HeaderText="NILAI PERTANGGUNGAN"></asp:BoundColumn>
                        <asp:BoundColumn DataField="CoverageTypeDescr" HeaderText="JENIS COVER"></asp:BoundColumn>
                        <asp:BoundColumn DataField="StartDate" HeaderText="TGL MULAI" DataFormatString="{0:dd/MM/yyyy}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="EndDate" HeaderText="TGL SELESAI" DataFormatString="{0:dd/MM/yyyy}">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="SrccPremiumToCust" HeaderText="PREMI SRCC"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TplAmountToCust" HeaderText="TPL"></asp:BoundColumn>
                        <asp:BoundColumn DataField="TplPremiumToCust" HeaderText="PREMI TPL"></asp:BoundColumn>
                        <asp:BoundColumn DataField="FloodPremiumToCust" HeaderText="PREMI FLOOD"></asp:BoundColumn>
                        <asp:BoundColumn DataField="LoadingFeeToCust" HeaderText="LOADING FEE"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PaidByCust" HeaderText="DIBAYAR CUSTOMER"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                KLAIM HISTORY
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DGridClaimHistory" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="ClaimDocNo" HeaderText="NO DOK KLAIM"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ClaimDate" HeaderText="TGL KLAIM"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ClaimTypeDescr" HeaderText="JENIS KLAIM"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ReportedBy" HeaderText="DILAPORKAN OLEH"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ReportedAs" HeaderText="LAPOR SEBAGAI"></asp:BoundColumn>
                        <asp:BoundColumn DataField="ClaimStatus" HeaderText="STATUS KLAIM"></asp:BoundColumn>
                        <asp:BoundColumn DataField="PaidAmountClaim" HeaderText="JUMLAH DIBAYAR ASR"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                ENDORSEMENT HISTORY
            </h4>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DGridEndorsment" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:BoundColumn DataField="BDEndorsSeqNo" Visible="False" HeaderText="NO URUT ENDORS">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="BDEndorsDocNo" HeaderText="NO DOK ENDORS"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="TGL ENDORS">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEndorsDate" runat="server" Text='<%#format(Container.DataItem("BDEndorsDate"),"dd/MM/yyyy")%>'>>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="PolicyNumber" HeaderText="ENDORS NO POLIS"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="DETAIL">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkDetil" runat="server" Text="DETAIL"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnClose" runat="server" CausesValidation="False" Text="Exit" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
