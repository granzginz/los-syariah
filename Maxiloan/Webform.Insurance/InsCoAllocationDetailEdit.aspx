﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsCoAllocationDetailEdit.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsCoAllocationDetailEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcInsuranceBranchName" Src="../webform.UserController/UcInsuranceBranchName.ascx" %>
<%@ Register TagPrefix="uc4" TagName="ucCoverageTypeApk" Src="../webform.UserController/ucCoverageTypeApk.ascx" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc4" Src="../webform.UserController/ucNumberFormat.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsCoAllocationDetailEdit</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
   
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
  <asp:Label ID="lblMessage" Visible="false" runat="server" />

    <div align="center">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    EDIT PEMILIHAN PERUSAHAAN ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Kontrak
                </label>
                <asp:Label ID="LblAgreementNo" runat="server" ForeColor="Blue" Visible="False"></asp:Label><asp:HyperLink
                    ID="HplinkAgreementNo" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
            <div class="form_right">
                <label>
                    Nama Customer
                </label>
                <asp:Label ID="LblCustomerName" runat="server" ForeColor="Blue" Visible="False"></asp:Label>
                <asp:HyperLink ID="HpLinkCustName" runat="server" NavigateUrl=""></asp:HyperLink>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Asset Asuransi
                </label>
                <asp:Label ID="LblInsuranceAssetType" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Penggunaan
                </label>
                <asp:Label ID="LblAssetUsage" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Asset Baru / Bekas
                </label>
                <asp:Label ID="LblAssetNewUSed" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tanggal Loan ACT
                </label>
                <asp:Label ID="LblGoLiveDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Nilai Pertanggungan
                </label>
                <asp:Label ID="LblAmountCoverage" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Total Premi Standard
                </label>
                <asp:Label ID="LblTotalStandardPremium" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Total Premi Customer
                </label>
                <asp:Label ID="LblTotalPremiumByCust" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Loading Fee
                </label>
                <asp:Label ID="LblLoadingFee" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Biaya Materai
                </label>
                <asp:Label ID="LblStampDutyFee" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Biaya Admin
                </label>
                <asp:Label ID="LblAdminFee" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Di Asuransi Oleh
                </label>
                <asp:Label ID="LblInsuredByDescr" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    DiBayar Oleh
                </label>
                <asp:Label ID="LblPaidBy" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jangka Waktu
                </label>
                <asp:Label ID="lblTenor" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Tahun Asset
                </label>
                <asp:Label ID="lblAssetYear" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_hide"  >
            <div class="form_left">
                <label>
                    Periode Cover
                </label>
                <asp:DropDownList ID="DDLCoverPeriod" runat="server">
                    <asp:ListItem Value="FT" Selected="True">Full Tenor</asp:ListItem>
                    <asp:ListItem Value="AN">Annualy</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="LBlDataExist" runat="server" Visible="False"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Berlaku Dari Tanggal
                </label>
                <asp:TextBox runat="server" ID="txtodate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtodate" Format="dd/MM/yyyy" />
                <asp:Label ID="LblMessageForDate" runat="server" ForeColor="Red" DESIGNTIMEDRAGDROP="852"></asp:Label>
            </div>
        </div>

          <div class="form_box_title">
            <div class="form_single">
                <h5> Asuransi  <asp:Label   style="margin-left: 75px;" ID="lblAsuransi" runat="server" /></h5>
            </div>
        </div>


         <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="gridDetailInsco" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="YearNum" CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="YEAR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDYearNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JENIS COVER">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDCoverage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI CUTOMER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI MASKAPAI">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoMask" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToInsCo", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    
                                    <asp:TemplateColumn HeaderText="PREMI TPL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING FEE">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFeeToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA PASS">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>

        <div class="form_box_title">
            <div class="form_single">
                <h4> TIPE ASURANSI & PERLUASAN</h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label> Perusahaan Asuransi</label>
                <uc1:ucinsurancebranchname id="oInsuranceBranchName" runat="server"></uc1:ucinsurancebranchname>
                <asp:Label ID="LblPremiumInsCo" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ws">
                    <asp:DataGrid ID="DGridInsurance" runat="server" AutoGenerateColumns="False" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="THN">
                                <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                                <ItemTemplate> <asp:Label ID="LblYearInsurance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearInsurance") %>' /> </ItemTemplate>
                            </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="JENIS COVER">
                                 <HeaderStyle CssClass="auto_width"/>  <ItemStyle CssClass="auto_width" />
                                 <ItemTemplate>
                                 <asp:Label ID="LblHiddenCoverageTypeDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.CoverageType") %>' />
                                 <uc4:uccoveragetypeapk runat="server" id="DDLCoverageTypeDgrid"  />
                                 </ItemTemplate>
                             </asp:TemplateColumn>
                             
                             <asp:TemplateColumn HeaderText="TPL">
                                 <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                                 <HeaderTemplate>TPL <asp:DropDownList runat="server" ID="DDLBolTPLOptionHeader" AutoPostBack="true" OnTextChanged="DDLBolOptionHeader_OnTextChanged"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                                 </HeaderTemplate>
                                 <ItemTemplate>
                                    <asp:DropDownList ID="DDLBolTPLOption" runat="server"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                                 </ItemTemplate>
                             </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="NILAI TPL">
                             <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                             <ItemTemplate>
                                <asp:Label ID="LblTPLAmount" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmount") %>' />
                                <uc4:ucnumberformat runat="server" id="txtNilaiTPL"  />
                             </ItemTemplate>
                             </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="FLOOD">
                                 <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                                 <HeaderTemplate> FLOOD
                                    <asp:DropDownList runat="server" ID="DDLBolFloodOptionHeader" AutoPostBack="true" OnTextChanged="DDLBolOptionHeader_OnTextChanged"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                                 </HeaderTemplate>
                                 <ItemTemplate>
                                    <asp:DropDownList ID="DDLBolFloodOption" runat="server"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                                 </ItemTemplate>
                             </asp:TemplateColumn>
                             
                             <asp:TemplateColumn HeaderText="EQVET">
                             <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                             <HeaderTemplate> EQVET
                             <asp:DropDownList runat="server" ID="DDLBolIQVETOptionHeader" AutoPostBack="true" OnTextChanged="DDLBolOptionHeader_OnTextChanged"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </HeaderTemplate>
                             <ItemTemplate>
                                <asp:DropDownList ID="DDLBolIQVETOption" runat="server"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </ItemTemplate>
                             </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="SRCC">
                             <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                             <HeaderTemplate> SRCC
                                <asp:DropDownList runat="server" ID="DDLBolSRCCOptionHeader" AutoPostBack="true" OnTextChanged="DDLBolOptionHeader_OnTextChanged"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </HeaderTemplate>
                             <ItemTemplate>
                                <asp:DropDownList ID="DDLBolSRCCOption" runat="server"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </ItemTemplate>
                             </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="TERRORISME">
                             <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                             <HeaderTemplate> TERRORISME
                                <asp:DropDownList runat="server" ID="DDLBolTERROROptionHeader" AutoPostBack="true" OnTextChanged="DDLBolOptionHeader_OnTextChanged"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </HeaderTemplate>
                             <ItemTemplate>
                                <asp:DropDownList ID="DDLBolTERROROption" runat="server"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </ItemTemplate>
                             </asp:TemplateColumn>
                             
                             <asp:TemplateColumn HeaderText="PA">
                             <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                             <HeaderTemplate> PA
                             <asp:DropDownList runat="server" ID="DDLBolPAPAXOptionHeader" AutoPostBack="true" OnTextChanged="DDLBolOptionHeader_OnTextChanged"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </HeaderTemplate>
                             <ItemTemplate>
                                <asp:DropDownList ID="DDLBolPAPAXOption" runat="server"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </ItemTemplate>
                             </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="NILAI PA">
                             <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                             <ItemTemplate>
                               <asp:Label ID="LblPAmount" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAAmount") %>' />
                             <uc4:ucnumberformat runat="server" id="txtNilaiPA"  />
                             </ItemTemplate>
                             </asp:TemplateColumn>

                             <asp:TemplateColumn HeaderText="PA DRIVER">
                             <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" />
                             <HeaderTemplate>PA DRIVER
                             <asp:DropDownList runat="server" ID="DDLBolPADRIVEROptionHeader" AutoPostBack="true" OnTextChanged="DDLBolOptionHeader_OnTextChanged"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </HeaderTemplate>
                             <ItemTemplate>
                                <asp:DropDownList ID="DDLBolPADRIVEROption" runat="server"> <asp:ListItem Value="0" Text="No" /> <asp:ListItem Value="1" Text="Yes" /> </asp:DropDownList>
                             </ItemTemplate>
                             </asp:TemplateColumn>
                             
                             <asp:TemplateColumn HeaderText="NILAI PA DRIVER">
                             <HeaderStyle CssClass="auto_width" /> <ItemStyle CssClass="auto_width" /> 
                             <ItemTemplate> 
                                <asp:Label ID="LblPADriverAmount" Visible="False" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverAmount") %>' />
                                <uc4:ucnumberformat runat="server" id="txtNilaiPADriver" />
                             </ItemTemplate>
                             </asp:TemplateColumn>

                             </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button"> <asp:Button ID="btnCalculate" Text="Proses" CssClass="small button blue" runat="server" CausesValidation="true" /> </div>

        <asp:panel id='pnlResultGrid' runat='server' >
        
        
        <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            PREMI ASURANSI & PERBANDINGANNYA
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="gridInsco" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                BorderWidth="0px" CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="MaskAssBranchID"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn Visible="False" HeaderText="INSURANCE COY ID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMaskAssID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="InsuranceComBranchID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInsuranceComBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssBranchID") %>' /> 
                                            <asp:Label ID="lblMaskapaiAsuransiDesc" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskapaiAsuransi") %>' /> 
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MaskAssBranchNAME" HeaderText="CABANG ASURANSI">
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="PREMI CUSTOMER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TPL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SrccPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING FEE">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA PASS">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverPremium", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA ADMIN">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridAdminFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AdminFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA MATERAI">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMeteraiFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StampdutyFee", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TOTAL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridGrandTotal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ws">
                            <asp:DataGrid ID="gridDetailInsco1" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                                CellPadding="3" CellSpacing="1" BorderStyle="None" DataKeyField="YearNum" CssClass="grid_general">
                                <HeaderStyle CssClass="th"></HeaderStyle>
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="YEAR">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDYearNum" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JENIS COVER">
                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDCoverage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Description") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI CUSTOMER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                     <asp:TemplateColumn HeaderText="PREMI MASKAPAI">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridMainMask" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToInsCo", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TPL">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI EQVET">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDEQVETPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EQVETPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI SRCC">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI TERORISME">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDTerrorismPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERRORISMPremiumToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING FEE">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFeeToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA PASS">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPAPass" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PAAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PA DRIVER">
                                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridInscoDPADriver" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PADriverAmountToCust", "{0:###,###,##0}") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                 
            <div class="form_box_title">
                    <div class="form_single">
                        <h4> PENUTUPAN ASURANSI </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label >Cover Period</label>
                        <asp:DropDownList ID="cboCoverPeriod" runat="server" >
                        <asp:ListItem Value="FT">Full Tenor</asp:ListItem>
                        <asp:ListItem Value="AN">Annualy</asp:ListItem>
                    </asp:DropDownList> 
                </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>Tanggal </label>
                        <asp:TextBox runat="server" ID="txtstartDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtstartDate" Format="dd/MM/yyyy" />  
                        <asp:RequiredFieldValidator runat="server" ID="rfvDateCE" ControlToValidate="txtstartDate" Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general" SetFocusOnError="true" Enabled="false" />  
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!" ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtstartDate" SetFocusOnError="true" Display="Dynamic" CssClass="validator_general" />   
                    </div>
                </div>

                <div class="form_box">
                    <div class="form_single">
                        <label >Sampai</label>
                        <asp:TextBox runat="server" ID="txtEndDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender12" TargetControlID="txtEndDate" Format="dd/MM/yyyy" />  
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtEndDate" Display="Dynamic" ErrorMessage="Tanggal harus diisi!" CssClass="validator_general" SetFocusOnError="true" Enabled="false" />  
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Penulisan tanggal salah format, input dengan format dd/MM/yyyy!" ValidationExpression="(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/\d{4}" ControlToValidate="txtstartDate" SetFocusOnError="true" Display="Dynamic" CssClass="validator_general" />   
                    </div>
                </div>
                 
               <div class="form_button">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button green"></asp:Button>
                <asp:Button ID="btnCancel" Text="Cancel" CssClass="small button gray" runat="server" CausesValidation="False"></asp:Button>
                    
            <asp:Label ID="LblBranchIDHidden" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblFlagInsActivationHidden" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblApplicationIDHidden" runat="server" Visible="False"></asp:Label>
            <asp:Label ID="LblMaturityDate" runat="server" Visible="False"></asp:Label>
            </div>

        </asp:panel>



<%--
        <div class="form_button">
            <asp:Button ID="btnOkatas" runat="server" Text="OK" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelatas" runat="server" CausesValidation="False" Text="Cancel"
           
        </div>--%>
      <%--  <asp:Panel ID="PnlGrid" runat="server">
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DGridTenor" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                            BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:BoundColumn DataField="yearnum" HeaderText="TAHUN KE"></asp:BoundColumn>
                                <asp:BoundColumn DataField="coveragetypedescr" HeaderText="JENIS COVER"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="PREMI SRCC">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SrccPremiumToCust", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH TPL">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tplamounttocust", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI TPL">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.tplpremiumtocust", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="FLOOD">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FLoodPremiumToCust", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.premiumtocustamount", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="paidbycust" HeaderText="DIBAYAR CUSTOMER"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        PREMI PERUSAHAAN ASURANSI
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DGridInsurance_" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                            DataKeyField="InsuranceComBranchID" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn Visible="False" HeaderText="INSURANCE COY ID">
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridMaskAssID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="False" HeaderText="InsuranceComBranchID">
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridInsuranceComBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.InsuranceComBranchID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="InsuranceComBranchNAME" HeaderText="CABANG ASURANSI">
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                </asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="PREMI UTAMA">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridMainPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremium", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI SRCC">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridSRCCPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SrccPremium", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI TPL">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridTPLPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLPremium", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI FLOOD">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridFloodPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremium", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="LOADING FEE">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LoadingFee", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="BIAYA ADMIN">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridAdminFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AdminFee", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="BIAYA MATERAI">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridMeteraiFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StampdutyFee", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TOTAL">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="LblGridGrandTotal" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Total", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        STATISTIK ASURANSI
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="DgridQuota" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                            BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="CABANG ASURANSI">
                                    <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%#Container.DataItem("InsuranceComBranchname")%>'
                                            ID="LblInsuranceComBranchname">
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="JUMLAH KUOTA">
                                    <HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaAmount", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="PREMI TERALOKASI">
                                    <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PremiumAllocated", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="(%) TERALOKASI">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AllocatedPercentage", "{0:###,###,##0.00}") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="KONTRAK TERALOKASI">
                                    <HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Label9" runat="server" Text='<%#formatnumber(Container.DataItem("AllocatedAgreement"),0)%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="(%) KUOTA">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaPercentage", "{0:###,###,##0.00}") %>'
                                            ID="lblQuotaPercentage">
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="(%) KUOTA TERCAPAI">
                                    <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaPercentageAchieved", "{0:###,###,##0.00}") %>'
                                            ID="lblQuotaPercentageAchieved">
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Sampai Tanggal</label>
                    <asp:TextBox runat="server" ID="txtEndDate"></asp:TextBox>
                    <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtEndDate"
                        Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                    <asp:Label ID="LblMessageEndDate" runat="server" ForeColor="Red"></asp:Label>
                </div>
            </div>
           
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue" />
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
            </div>
        </asp:Panel>--%>
    </div>
    </form>
</body>
</html>
