﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsCoAllocationEdit
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranch As UcBranch
    Protected WithEvents oSearchBy As UcSearchByWithNoTable
#Region "Constanta"
    Private currentPage As Int32 = General.CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Private pageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Private currentPageNumber As Int16 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Private totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Private recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT

    Private oCustomClass As New Parameter.InsCoAllocation
    Private oCustomClassGeneralPaging As New Parameter.GeneralPaging
    Private oController As New GeneralPagingController
#End Region
#Region "Property "

    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region
#Region "InitialDefaultPanel"
    Public Sub InitialDefaultPanel()
        PnlGrid.Visible = False
    End Sub

#End Region
#Region "Page Load "

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If SessionInvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then

                Dim ParamErrMessages As String = Request.QueryString("errMessages")
                If ParamErrMessages <> "" Then
                    LblErrMessages.Text = ParamErrMessages
                Else
                    LblErrMessages.Text = ""
                End If

                InitialDefaultPanel()
                oSearchBy.ListData = "dbo.Agreement.AgreementNo, Agreement NO-dbo.Customer.Name, Customer Name"
                oSearchBy.BindData()

                If Me.SortBy = "" Then Me.SortBy = " GoLiveDate "
            Else

                KickOutForMultipleBranch()

            End If

        End If
    End Sub

#End Region
#Region "DataBind"

    Sub DoBind(ByVal cmdWhere As String, ByVal SortBy As String)
        Dim DtUserList As New DataTable
        Dim DvUserList As New DataView
        'Dim intloop As Integer
        'Dim hypID As HyperLink

        With oCustomClassGeneralPaging
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
            .SpName = "SpInsCompanySelectionSelectEdit"
        End With

        oCustomClassGeneralPaging = oController.GetGeneralPaging(oCustomClassGeneralPaging)

        If Not oCustomClassGeneralPaging Is Nothing Then
            DtUserList = oCustomClassGeneralPaging.ListData
            recordCount = oCustomClassGeneralPaging.TotalRecords

        Else
            recordCount = 0
        End If


        DtUserList = oCustomClassGeneralPaging.ListData
        DvUserList = DtUserList.DefaultView
        DvUserList.Sort = Me.SortBy
        dtgPaging.DataSource = DvUserList

        Try
            dtgPaging.DataBind()
        Catch
            dtgPaging.CurrentPageIndex = 0
            dtgPaging.DataBind()

        End Try
        PagingFooter()
        '        

    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim m As Int32
        If e.Item.ItemIndex >= 0 Then
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
        End If
    End Sub

#End Region
#Region "Search !"


    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        LblErrMessages.Text = ""
        Me.BranchID = oBranch.BranchID
        Me.SearchBy = ""

        If oBranch.BranchID <> "0" Then
            Me.SearchBy = Me.SearchBy & " And dbo.Agreement.BranchID = '" & Me.BranchID & "' "
        End If

        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
        End If

        PnlGrid.Visible = True

        If Me.SortBy = "" Then Me.SortBy = " GoLiveDate "

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "Navigation "


    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        If Me.SortBy Is Nothing Then
            Me.SortBy = ""
        End If
        '

        DoBind(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "Paging Footer"

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ' ShowMessage(lblMessage, "Data tidak ditemukan!", true)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = CType(totalPages, String)
            'rgvGo.MaximumValue = CType(totalPages, String)
        End If
        lblRecord.Text = CType(recordCount, String)

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

#End Region
#Region "Go Page"

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtPage.Text, Int16)
                If Me.SortBy Is Nothing Then
                    Me.SortBy = ""
                End If
                DoBind(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        DoBind(Me.SearchBy.Trim, Me.SortBy)
    End Sub

#End Region
#Region "??"

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        LblErrMessages.Text = ""

        Me.SearchBy = " And dbo.Agreement.BranchID = '" & Me.BranchID & "' "
        oSearchBy.Text = ""
        oSearchBy.BindData()
        If Me.SortBy = "" Then Me.SortBy = " GoLiveDate DESC"
        DoBind(Me.SearchBy, Me.SortBy)

    End Sub
#End Region


End Class