﻿#Region "Imports"
Imports System.Threading
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller

#End Region

Public Class ViewPolicyDetail
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property AssetSeqNo() As Integer
        Get
            Return (CType(viewstate("AssetSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSeqNo") = Value
        End Set
    End Property
    Private Property Back() As String
        Get
            Return (CType(viewstate("Back"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Back") = Value
        End Set
    End Property
    Private Property InsSeqNo() As Integer
        Get
            Return (CType(viewstate("InsSeqNo"), Integer))
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSeqNo") = Value
        End Set
    End Property
    Private Property AgreementNo() As String
        Get
            Return (CType(viewstate("AgreementNo"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AgreementNo") = Value
        End Set
    End Property

    Private Property ApplicationID() As String
        Get
            Return (CType(viewstate("ApplicationID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property


#End Region
#Region "Private Constanta"

    Private oController As New PolicyDetailController
    Private mController As New SPPAController

#End Region
#Region "PageLoad"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub

        End If

        If Not IsPostBack Then
            Me.Back = Request.QueryString("Back")
            '============================VIEW POLICY DETAIL====================================================
            Dim ThreadViewPolicyDetail As New Thread(AddressOf ViewPolicyDetail)
            ThreadViewPolicyDetail.Priority = ThreadPriority.AboveNormal
            ViewPolicyDetail()
            '===============================INSURANCE DETAIL INFORMATION ======================================
            Dim ThreadInsuranceDetailInformation As New Thread(AddressOf InsuranceDetailInformation)
            ThreadViewPolicyDetail.Priority = ThreadPriority.AboveNormal
            InsuranceDetailInformation()
            '===================================CLAIM HISTORY =================================================
            Dim ThreadClaimHistory As New Thread(AddressOf ClaimHistory)
            ThreadClaimHistory.Priority = ThreadPriority.AboveNormal
            ClaimHistory()
            '=================================VIEW ENDORSMENT HISTORY==========================================
            Dim ThreadViewEndorsmentHistory As New Thread(AddressOf ViewEndorsmentHistory)
            ThreadViewEndorsmentHistory.Priority = ThreadPriority.AboveNormal
            ViewEndorsmentHistory()

            'ThreadViewPolicyDetail.Start()
            'ThreadInsuranceDetailInformation.Start()
            'ThreadClaimHistory.Start()
            'ThreadViewEndorsmentHistory.Start()
            If Me.Back <> "" Then
                btnClose.Attributes.Add("onclick", "windowClose()")

            End If

        End If


    End Sub
#End Region
#Region "ViewPolicyDetail"
    Sub ViewPolicyDetail()
        Try
            Me.ApplicationID = Request.QueryString("ApplicationID")
            Me.AssetSeqNo = CInt(Request.QueryString("AssetSeqNo"))
            Me.InsSeqNo = CInt(Request.QueryString("InsSeqNo"))
            Me.AgreementNo = Replace(Request.QueryString("AgreementNo"), "'", "")
            Me.BranchID = Replace(Request.QueryString("BranchID"), "'", "")
            LblAgreementNo.Text = Me.AgreementNo
            Dim oEntities As New Parameter.InsCoAllocationDetailList
            Dim AppAggrement As String = " (Select Dbo.AmbilApplicationID('" & Me.AgreementNo & "')) "

            'AND	AssetSeqNo = (select dbo.AmbilAssetSequencenoInsuranceAsset(InsuranceAsset.BranchID,@ApplicationID)) 
            'And 	InsSequenceNo = (select dbo.AmbilInsSequencenoInsuranceAsset(InsuranceAsset.branchID,@ApplicationID))

            Dim WhereCond1Builder As New StringBuilder
            WhereCond1Builder.Append(" WHERE dbo.Agreement.AgreementNo = '" & Me.AgreementNo & "' And dbo.Agreement.BranchID ='" & Me.BranchID & "'")
            'WhereCond1Builder.Append(" And dbo.insuranceasset.AssetSeqNo = (Select dbo.AmbilAssetSequencenoInsuranceAsset('" & Me.BranchID & "'," & AppAggrement & ")) ")
            'WhereCond1Builder.Append(" And dbo.insuranceasset.InsSequenceNo = (select dbo.AmbilInsSequencenoInsuranceAsset('" & Me.BranchID & "'," & AppAggrement & ")) ")
            WhereCond1Builder.Append(" And dbo.insuranceasset.ApplicationID  = '" & Me.ApplicationID & "' ")
            WhereCond1Builder.Append(" And dbo.insuranceasset.AssetSeqNo  = '" & Me.AssetSeqNo & "' ")
            WhereCond1Builder.Append(" And dbo.insuranceasset.InsSequenceNo  = '" & Me.InsSeqNo & "' ")


            With oEntities
                .WhereCond = WhereCond1Builder.ToString
                .strConnection = GetConnectionString
                Context.Trace.Write(" WhreCond = " & .WhereCond)
            End With


            oEntities = oController.GetPolicyDetail(oEntities)

            With oEntities
                Me.ApplicationID = .ApplicationID.Trim
                LblCustomerName.Text = .CustomerName
                LblAssetDescription.Text = .AssetMasterDescr
                LblAssetUsage.Text = .AssetUsageDescr
                LblInsuranceAssetType.Text = .InsuranceTypeDescr
                LblAssetNewUsed.Text = .UsedNew
                LblInsNotes.Text = .InsNotes
                LblAccNotes.Text = .AccNotes
                LblInsuranceCompany.Text = .InsuranceComBranchName
                LblSPPANo.Text = .SPPANo
                If CType(.SPPADate, String) = "1/1/1900" Then
                    LblSPPADate.Text = "-"
                Else
                    LblSPPADate.Text = .SPPADate.ToString("dd/MM/yyyy")

                End If

                LblPolicyNo.Text = .PolicyNumber

                If CType(.PolicyReceiveDate, String) = "1/1/1900" Then
                    LblPolicyDate.Text = "-"
                Else
                    LblPolicyDate.Text = .PolicyReceiveDate.ToString("dd/MM/yyyy")
                End If
                LblInsuranceStartDate.Text = .StartDate.ToString("dd/MM/yyyy")
                LblInsuranceEndDate.Text = .EndDate.ToString("dd/MM/yyyy")
                LblPremiumAmountByCust.Text = FormatNumber(CType(.PremiumAmountByCust, String), 2, , , )
                LblPaidAmountByCust.Text = FormatNumber(CType(.PaidAmountByCust, String), 2, , , )
                LblSumInsured.Text = FormatNumber(CType(.SumInsured, String), 2, , , )
                LblPremiumAmountToInsCo.Text = FormatNumber(CType(.PremiumAmountToInsco, String), 2, , , )
                LblPaidAmountToInsco.Text = FormatNumber(CType(.PaidAmountToInsco, String), 2, , , )

            End With
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("", "", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Response.Write(err.Message)
        End Try


    End Sub

#End Region
#Region "InsuranceDetailInformation"
    Sub InsuranceDetailInformation()
        Dim ParamApplicationId As String = Me.ApplicationID
        Dim ParamBranchID As String = Me.BranchID

        Dim CmdWhere As String = " Where dbo.InsuranceAssetDetail.BranchId ='" & Replace(ParamBranchID, "'", "") & "' And dbo.InsuranceAssetDetail.ApplicationID= '" & Replace(ParamApplicationId, "'", "") & "' "
        Dim CmdWhereBuilder As New StringBuilder
        CmdWhereBuilder.Append(" Where dbo.InsuranceAssetDetail.BranchId ='" & Replace(ParamBranchID, "'", "") & "' And dbo.InsuranceAssetDetail.ApplicationID= '" & Replace(ParamApplicationId, "'", "") & "' ")
        CmdWhereBuilder.Append("  And dbo.insuranceAssetDetail.AssetSeqNo = (Select dbo.AmbilAssetSequencenoInsuranceAsset('" & Me.BranchID & "','" & ParamApplicationId & "')) ")
        CmdWhereBuilder.Append("  And dbo.insuranceAssetDetail.InsSequenceNo = (select dbo.AmbilInsSequencenoInsuranceAsset('" & Me.BranchID & "','" & ParamApplicationId & "')) ")

        Dim oEntitiesGrid As New Parameter.SPPA
        With oEntitiesGrid
            .WhereCond = CmdWhereBuilder.ToString
            .PageSource = General.CommonVariableHelper.PAGE_SOURCE_GRID_TENOR_VIEW_INSURANCE_ASSET_DETAIL
            .strConnection = GetConnectionString
        End With

        Try
            oEntitiesGrid = mController.GetListCreateSPPA(oEntitiesGrid)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim dtEntity As New DataTable

        If Not oEntitiesGrid Is Nothing Then
            dtEntity = oEntitiesGrid.ListData
        End If



        DGridInsDetail.DataSource = dtEntity
        DGridInsDetail.DataBind()


    End Sub

#End Region
#Region "ClaimHistory"
    Sub ClaimHistory()

        Dim oEntitiesClaimHistory As New Parameter.SPPA
        Dim ParamApplicationId As String = Me.ApplicationID
        Dim ParamBranchID As String = Me.BranchID
        Dim ParamAssetSeqno As String = CStr(Me.AssetSeqNo)
        Dim ParamInsSeqNo As String = CStr(Me.InsSeqNo)
        Dim CmdWhere As String = " Where dbo.InsuranceClaim.BranchId ='" & Replace(ParamBranchID, "'", "") & "' And dbo.InsuranceClaim.ApplicationID= '" & Replace(ParamApplicationId, "'", "") & "' And dbo.InsuranceClaim.AssetSeqNo= '" & Replace(ParamAssetSeqno, "'", "") & "' And dbo.InsuranceClaim.InsSequenceNo= '" & Replace(ParamInsSeqNo, "'", "") & "' "
        With oEntitiesClaimHistory
            .WhereCond = CmdWhere
            .PageSource = CommonVariableHelper.PAGE_SOURCE_GET_CLAIM_HISTORY
            .strConnection = GetConnectionString
        End With

        Try
            oEntitiesClaimHistory = mController.GetListCreateSPPA(oEntitiesClaimHistory)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim dtEntityClaimHistory As New DataTable

        If Not oEntitiesClaimHistory Is Nothing Then
            dtEntityClaimHistory = oEntitiesClaimHistory.ListData
        End If


        DGridClaimHistory.DataSource = dtEntityClaimHistory
        DGridClaimHistory.DataBind()


    End Sub
#End Region
#Region "ViewEndorsmentHistory"
    Sub ViewEndorsmentHistory()
        '=================================VIEW ENDORSMENT HISTORY===============================================================
        Dim oEntitiesEndorsmentHistory As New Parameter.SPPA
        Dim ParamApplicationId As String = Me.ApplicationID
        Dim ParamBranchID As String = Me.BranchID
        Dim ParamAssetSeqNo As Integer
        Dim ParamInsSeqNo As Integer
        ParamAssetSeqNo = Me.AssetSeqNo
        ParamInsSeqNo = Me.InsSeqNo
        Dim CmdWhere As String = " Where dbo.InsuranceAssetHistory.BranchId ='" & Replace(ParamBranchID, "'", "") & "' And dbo.InsuranceAssetHistory.ApplicationID= '" & Replace(ParamApplicationId, "'", "") & "' And dbo.InsuranceAssetHistory.InsSequenceNo= " & ParamInsSeqNo & " And dbo.InsuranceAssetHistory.AssetSeqNo = " & ParamAssetSeqNo
        With oEntitiesEndorsmentHistory
            .WhereCond = CmdWhere
            .PageSource = CommonVariableHelper.PAGE_SOURCE_GET_ENDORSMENT_HISTORY
            .strConnection = GetConnectionString
        End With
        Try
            oEntitiesEndorsmentHistory = mController.GetListCreateSPPA(oEntitiesEndorsmentHistory)

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim dtEndorsmentHistory As New DataTable
        If Not oEntitiesEndorsmentHistory Is Nothing Then
            dtEndorsmentHistory = oEntitiesEndorsmentHistory.ListData
        End If

        DGridEndorsment.DataSource = dtEndorsmentHistory
        DGridEndorsment.DataBind()

    End Sub

#End Region



    Private Sub DGridEndorsment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGridEndorsment.ItemDataBound
        Dim lnkDetail As HyperLink
        Dim strBranchid As String
        Dim strapplicationid As String
        Dim strAss As String
        Dim strIns As String
        Dim strbdendors As String

        If e.Item.ItemIndex >= 0 Then
            lnkDetail = CType(e.Item.FindControl("lnkDetil"), HyperLink)
            strapplicationid = Me.ApplicationID

            strAss = CStr(Me.AssetSeqNo)
            strIns = CStr(Me.InsSeqNo)
            strBranchid = Me.BranchID
            strbdendors = e.Item.Cells(0).Text.Trim


            lnkDetail.NavigateUrl = "javascript:OpenWinEndorsDetail('" & Server.UrlEncode(strapplicationid.Trim) & "','" & strBranchid & "','" & strAss & "','" & strIns & "','" & strbdendors & "')"
        End If

    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click

        Response.Redirect("Inquiry/EndorsmentInq.aspx")

    End Sub

End Class