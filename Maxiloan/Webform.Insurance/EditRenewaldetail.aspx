﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditRenewaldetail.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.EditRenewaldetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EditRenewaldetail</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="LblErrorMessages" runat="server" ForeColor="Red"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                EDIT COVER UNTUK PERPANJANGAN
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Kontrak
            </label>
            <asp:Label ID="LblAgreementNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Polis
            </label>
            <asp:Label ID="LblPolicyNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nama Asset</label>
            <asp:Label ID="LblAssetDescr" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Nama Customer
            </label>
            <asp:Label ID="LblCustomerName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Asuransi
            </label>
            <asp:Label ID="LblInsuranceCo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Periode Asuransi
            </label>
            <asp:Label ID="LblStartDateInsPeriod" runat="server"></asp:Label>&nbsp;to
            <asp:Label ID="LblEndDateInsPeriod" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Asset Asuransi
            </label>
            <asp:Label ID="LblInsuranceTypeDescr" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Penggunaan Asset
            </label>
            <asp:Label ID="LblUsage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Asset : Baru / Bekas
            </label>
            <asp:Label ID="LblAssetNewUsed" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tahun Produksi
            </label>
            <asp:Label ID="LblAssetYear" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Jenis Aplikasi
            </label>
            <asp:Label ID="LblApplicationType" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                No Rangka
            </label>
            <asp:Label ID="LblChasisNo" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                No Mesin
            </label>
            <asp:Label ID="LblEngineNo" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                COVER SEBELUMNYA
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Nilai Pertanggungan
            </label>
            <asp:Label ID="LblSumInsured" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi Utama
            </label>
            <asp:Label ID="LblMainPremium" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi SRCC
            </label>
            <asp:Label ID="LblSRCCPremium" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi TPL
            </label>
            <asp:Label ID="LblTPLPremium" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi Flood
            </label>
            <asp:Label ID="LblFloodPremium" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Loading Fee
            </label>
            <asp:Label ID="LblLoadingFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Admin
            </label>
            <asp:Label ID="LblAdminFee" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Meterai
            </label>
            <asp:Label ID="LblMeteraiFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Discount Customer
            </label>
            <asp:Label ID="LblDiscAmountToCust" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi Customer
            </label>
            <asp:Label ID="LblTotPremiumByCust" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Catatan Accessories
            </label>
            <asp:Label ID="LblACCNotes" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Catatan Asuransi
            </label>
            <asp:Label ID="LblInsNotes" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_title">
        <div class="form_single">
            <h4>
                COVER BARU
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
           <label class ="label_req">
                Nilai Pertanggungan
            </label>
            <asp:TextBox ID="TxtNewSumInsured" runat="server" ></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\d*"
                ErrorMessage="Harap isi dengan Angka" ControlToValidate="TxtNewSumInsured" Display="Dynamic"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi dengan Angka"
                    ControlToValidate="TxtNewSumInsured" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
            <label>
                Premi Utama
            </label>
            <asp:Label ID="LblMainPremiumNewCoverage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi SRCC
            </label>
            <asp:Label ID="LblSRCCPremiumNewCoverage" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Premi TPL
            </label>
            <asp:Label ID="LblTPLPremiumNewCoverage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi Flood
            </label>
            <asp:Label ID="LblFloodPremiumNewCoverage" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Loading Fee
            </label>
            <asp:Label ID="LblLoadingFeeNewCoverage" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Biaya Admin
            </label>
            <asp:Label ID="LblNewAdminFee" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Biaya Meterai
            </label>
            <asp:Label ID="LblNewMeteraiFee" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Premi Customer Baru
            </label>
            <asp:Label ID="LblNewPremiumToCust" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Tanggal Berakhir
            </label>
            <asp:Label ID="LblExpiredDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Catatan Accessories
            </label>
            <asp:TextBox ID="TxtNewAccNotes" runat="server"  TextMode="MultiLine"></asp:TextBox>
        </div>
        <div class="form_right">
            <label>
                Catatan Asuransi
            </label>
            <asp:TextBox ID="TxtNewInsNotes" runat="server"  TextMode="MultiLine"></asp:TextBox>
        </div>
    </div>
    <div class="form_box_header">
        <div class="form_single">
            <div class="grid_wrapper_ns">
                <asp:DataGrid ID="DgridInsurance" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    BorderWidth="0" CssClass="grid_general">
                    <HeaderStyle CssClass="th" />
                    <ItemStyle CssClass="item_grid" />
                    <Columns>
                        <asp:TemplateColumn HeaderText="TAHUN">
                            <ItemTemplate>
                                <asp:Label ID="LblYearInsurance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.YearNum") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JENI COVER">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenCoverageTypeDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CoverageType") %>'
                                    Visible="False">
                                </asp:Label>
                                <asp:DropDownList ID="DDLCoverageTypeDgrid" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PREMI">
                            <ItemTemplate>
                                <asp:Label ID="LblPremiumDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MainPremiumToCust", "{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PREMI SRCC">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenSRCCDGrid" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.BolSRCCToCust") %>'>
                                </asp:Label>
                                <asp:DropDownList ID="DDLBolSRCCPilihan" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH SRCC">
                            <ItemTemplate>
                                <asp:Label ID="LblSRCCAmountDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SRCCPremiumToCust","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH TPL">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenTPLDgrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLAmountToCust") %>'
                                    Visible="false">
                                </asp:Label>
                                <asp:DropDownList ID="DDLTPLPilihan" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PREMI TPL">
                            <ItemTemplate>
                                <asp:Label ID="LblTPLAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TPLPremiumToCust","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="PREMI FLOOD">
                            <ItemTemplate>
                                <asp:Label ID="LblHiddenFloodDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BolFloodToCust") %>'
                                    Visible="False">
                                </asp:Label>
                                <asp:DropDownList ID="DDLFloodPilihan" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="JUMLAH FLOOD">
                            <ItemTemplate>
                                <asp:Label ID="LblFloodAmountDGrid" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FloodPremiumToCust","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="LOADING FEE">
                            <ItemTemplate>
                                <asp:Label ID="LblPremiLoadingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.loadingfeetocust","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TOTAL">
                            <ItemTemplate>
                                <asp:Label ID="LblNewTotalPremium" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PremiumToCustAmount","{0:###,###,##0.00}") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnCalculate" runat="server" Text="Calculate" CssClass="css small button green">
        </asp:Button>
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </form>
</body>
</html>
