﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsTerminateApproval.aspx.vb" Inherits="Maxiloan.Webform.Insurance.InsTerminateApproval" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UCReason" Src="../webform.UserController/UCReason.ascx" %>
<%@ Register Src="../webform.UserController/ucGridNav.ascx" TagName="ucGridNav" TagPrefix="uc2" %> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>Insurance Termination Approval</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:ScriptManager runat="server" ID="ScriptManager1" />
       <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();" />
       
       <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
          <div class="title_strip"> </div>
            <div class="form_single">
                <h4> APPROVAL PENUTUPAN ASURANSI </h4>
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req"> Cabang</label>
                <asp:DropDownList ID="ddlBranch" runat="server" />
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic" ControlToValidate="ddlBranch" ErrorMessage="Harap pilih Cabang" InitialValue="0" CssClass="validator_general" />
            </div>
        </div>

        <div class="form_box">
            <div class="form_single">
                <label>Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server">
                    <asp:ListItem Value="Select One">Select One</asp:ListItem>
                    <asp:ListItem Value="AgreementNo">No Kontrak</asp:ListItem>
                    <asp:ListItem Value="Customer.Name">Nama Konsumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchBy" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue" />
            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray" CausesValidation="False" />
        </div>
    </asp:Panel>


      <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip"> </div>
            <div class="form_single">
                <h4>PENUTUPAN ASURANSI</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgClaimRequestList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="AgreementNo" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH">
                                <ItemStyle HorizontalAlign="Center" width='3%' VerticalAlign="Middle"></ItemStyle>
                                <ItemTemplate>
                                     <asp:LinkButton  ID="imgTerminate"  CommandName="Terminate" runat="server" Text='APPROVE' CausesValidation='false'  Visible='<%#iif(Container.dataitem("ClaimType")="TMN" and Container.dataitem("ClaimStatus")="R" , "True", "False")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn> 
                            <asp:BoundColumn Visible="False" DataField="BranchID" />
                            <asp:BoundColumn Visible="False" DataField="ApplicationID" />
                            <asp:BoundColumn Visible="False" DataField="CustomerID" />
                            <asp:BoundColumn Visible="False" DataField="AgreementNo" />
                            <asp:BoundColumn Visible="False" DataField="CustomerName" />
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="10%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="CustomerName" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Height="30px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="24%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkCustomerName" runat="server" Text='<%#Container.DataItem("CustomerName")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn SortExpression="description" DataField="description" HeaderText="NAMA ASSET">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="30%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn SortExpression="PolicyNumber" DataField="PolicyNumber" HeaderText="NO POLIS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn SortExpression="Contractstatus" DataField="Contractstatus" HeaderText="STS">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="3%"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="AssetSeqNo" HeaderText="AssetSeqNo" />
                            <asp:BoundColumn Visible="False" DataField="InsSequenceNo" HeaderText="InsSeqNo" />
                        </Columns>
                         
                    </asp:DataGrid>
                       <uc2:ucGridNav id="GridNavigator" runat="server"/>   
                </div>
            </div>
        </div>
    </asp:Panel>
     



     
    <asp:Panel ID="pnlView" runat="server">
     <div class="form_title">
          <div class="title_strip"> </div>
        <div class="form_single">
            <h4> PENUTUPAN ASURANSI </h4>
        </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>No Kontrak</label>
                <asp:HyperLink ID="lblAgreementNo" runat="server" />
            </div>
            <div class="form_right">
                <label> Nama Customer </label>
                <asp:HyperLink ID="lblCustomerName" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Nama Asset</label>
                <asp:Label ID="lblAsset" runat="server" />
            </div>
            <div class="form_right">
                <label>Jenis Cover</label>
                <asp:Label ID="lblCoverage" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Perusahaan Asuransi</label>
                <asp:Label ID="lblInsCo" runat="server" />
            </div>
            <div class="form_right">
                <label> No Polis </label>
                <asp:Label ID="lblPolicyNo" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Status Kontrak</label>
                <asp:Label ID="lblContractStatus" runat="server" />
            </div>
            <div class="form_right">
                <label>Jangka Waktu</label>
                <asp:Label ID="lblTenor" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>No Rangka</label>
                <asp:Label ID="lblChassisNo" runat="server" />
            </div>
            <div class="form_right">
                <label>No Mesin</label>
                <asp:Label ID="lblEngineNo" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Jangka Waktu </label>
                <asp:Label ID="lblInsuranceLength" runat="server"></asp:Label>&nbsp;month
            </div>
            <div class="form_right">
                <label>Periode Asuransi </label>
                <asp:Label ID="lblStartDate" runat="server"></asp:Label>&nbsp;S/D&nbsp;
                <asp:Label ID="lblEndDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Premi Utama Customer</label>
                <asp:Label ID="lblMainPremiumToCust" runat="server" />
            </div>
            <div class="form_right">
                <label>Premi Ke Customer</label>
                <asp:Label ID="lblPremiumToCust" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Premi Utama ke Asuransi</label>
                <asp:Label ID="lblMainPremiumToInsCo" runat="server" />
            </div>
            <div class="form_right">
                <label>Premi Utama Ke Asuransi</label>
                <asp:Label ID="lblPremiumToInsCo" runat="server" />
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>Apakah bisa Refund</label>
                <asp:Label ID="lblisrefund" runat="server" />
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Tanggal Tutup</label>
                 <asp:Label ID="lblTerminateDate" runat="server" />
               <%-- <asp:TextBox runat="server" ID="txtTerminateDate"></asp:TextBox>
                <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtTerminateDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Alasan Tutup</label>
                <asp:Label ID="lbloReason" runat="server" />
                <%--<uc1:ucreason id="oReason" runat="server"></uc1:ucreason>--%>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>Catatan</label>
                <asp:TextBox ID="txtNotes" runat="server" Width="70%" TextMode="MultiLine" enabled='false'></asp:TextBox>
            </div>
        </div>
      <%--  <div class="form_button">
            <asp:Button ID="btnCalculate" runat="server" Text="Calculate" CssClass="small button green" />
            <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray" /> 
        </div>--%>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgMainPremium" runat="server" AutoGenerateColumns="False" AllowSorting="True" OnSortCommand="Sorting" DataKeyField="Year" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="TAHUN">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblYear" runat="server" Text='<%#Container.DataItem("Year")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PREMI UTAMA KE CUST">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="right" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblMainPremToCust" runat="server" Text='<%#formatnumber(Container.DataItem("MainPremiumToCust"),2)%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DIBAYAR OLEH CUST">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="right" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblPaidAmtByCust" runat="server" Text='<%#formatnumber(Container.DataItem("PaidAmtByCust"),2)%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL MULAI">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDateCust" runat="server" Text='<%#Format(Container.DataItem("StartDateCust"), "dd/MM/yyyy")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TGL SELESAI">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDateCust" runat="server" Text='<%#Format(Container.DataItem("EndDateCust"), "dd/MM/yyyy")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="SISA PERIODE OLEH CUST">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" Width="14%" />
                                <ItemTemplate>
                                    <asp:Label ID="RestPeriodCust" runat="server" Text='<%#Container.DataItem("RestOfPeriodByCust")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns> 
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>



     <asp:Panel ID="pnlCalculate" runat="server">
       <div class="form_box_header">
        <div class="form_single">
            <h4>
                PERHITUNGAN REFUND
            </h4>
        </div>
        </div>
        <div class="form_box"> 
            <div class="form_single"> 
                <label> Sisa Periode  </label>
                <asp:Label ID="lblRestInsToInsCo" runat="server"></asp:Label>&nbsp;days
            </div>
        </div>


        <div class="form_box"> 
            <div class="form_single"> 
                <label>Total Refund</label>
               <asp:Label ID="lblRefundAmountFromInsCo" runat="server"> </asp:Label>
            </div>
        </div>
         
          <div class="form_box"> 
            <div class="form_single"> 
                <label>Refund Ke Customer</label> 
                <asp:Label ID="lblRefundAmountToCust" runat="server"></asp:Label>    
            </div>
        </div>

        <div class="form_box"> 
            <div class="form_single"> 
                 <label > Jumlah Pengurangan </label> 
                 <asp:Label ID="lblDeductAmount" runat="server"></asp:Label>   
            </div>
        </div>

        <div class="form_box"> 
            <div class="form_single"> 
                <label> Total Refund Ke Customer </label>
                <asp:Label ID="lblTtl" runat="server"></asp:Label>
            </div>
        </div>
      <div class="form_box"> 
            <div class="form_single"> 
                <label> Pendapatan Asuransi</label>
                <asp:Label ID="lblGain" runat="server"></asp:Label>
            </div>
        </div>
           <div class="form_box">
            <div class="form_single">
                <label>Catatan</label>
                <asp:TextBox ID="txtApprNotes" runat="server" CssClass="multiline_textbox" TextMode="MultiLine" />
            </div>
        </div>
          <div class="form_box">
            <div class="form_single">
                <label class="label_req">Approval</label>
                <asp:DropDownList ID="ddlApprove" runat="server">
                    <asp:ListItem Value="0">Select One</asp:ListItem>
                    <asp:ListItem Value="N">Approve</asp:ListItem>
                    <asp:ListItem Value="J">Reject</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="reqFloatingPeriod" runat="server"   Display="Dynamic" ErrorMessage="Harap pilih Approval" ControlToValidate="ddlApprove" CssClass="validator_general" InitialValue="0" />
            </div>
        </div>

        <div class="form_button">
            <asp:Button ID="btnSave2" runat="server" Text="Save" CssClass="small button blue" />
            <asp:Button ID="btnCancel2" runat="server" CausesValidation="False" Text="Cancel" CssClass="small button gray" />
        </div>
    </asp:Panel>


    </form>
</body>
</html>
