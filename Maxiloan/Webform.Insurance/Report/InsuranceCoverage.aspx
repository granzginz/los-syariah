﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceCoverage.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceCoverage" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../../webform.UserController/ucDateCE.ascx" TagName="ucDateCE"
    TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>InsuranceCoverage</title>
    <link rel="Stylesheet" href="../../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../../Include/Buttons.css" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';

        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                INSURANCE COVERAGE
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Branch</label>
            <asp:DropDownList ID="cboBranch" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvcbobranchid" runat="server" ControlToValidate="cboBranch"
                ErrorMessage="Please Select Branch" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Loan Act.&nbsp;Date</label>
            <uc2:ucdatece id="sDate" runat="server" />
            &nbsp; S/D
            <uc2:ucdatece id="eDate" runat="server" />
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="ButtonViewReport" runat="server" CausesValidation="False" Text="View Report"
            CssClass="small button blue"></asp:Button>
    </div>
    </form>
</body>
</html>
