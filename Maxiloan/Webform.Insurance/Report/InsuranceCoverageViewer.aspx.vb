﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Web
Imports CrystalDecisions.ReportSource
Imports Maxiloan.Controller
Imports Maxiloan.Parameter
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller.DataUserControlController
#End Region

Public Class InsuranceCoverageViewer
    Inherits Maxiloan.webform.WebBased

    Private m_controllerUC As New DataUserControlController
    Private m_controller As New ClaimOutstandingController
    Private oCustomClass As New Parameter.ClaimOutstanding

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imbBack As System.Web.UI.WebControls.ImageButton
    Protected WithEvents CrystalReportViewer1 As CrystalDecisions.Web.CrystalReportViewer

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Property"
    Property WhereCond() As String
        Get
            Return CType(viewstate("WhereCond"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("WhereCond") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        BindReport()
        'createData()
    End Sub

    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies("InsuranceCoverageCookie")
        Me.WhereCond = cookie.Values("whereCond")
    End Sub

    Private Sub imbBack_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbBack.Click
        Response.Redirect("InsuranceCoverage.aspx")
    End Sub

    Private Sub createData()
        Dim oController As New ClaimOutstandingController
        Dim oParameter As New Parameter.ClaimOutstanding
        Dim oDataSet As New DataSet
        With oParameter
            .strConnection = GetConnectionString
            .WhereCond = "Where agreement.branchID IN (select branchID from branch)"
        End With
        oParameter = oController.getInsuranceCoverageList(oParameter)
        oDataSet = oParameter.ListData
        oDataSet.WriteXmlSchema("c:\maxiloan.reportdef\InsuranceCoverage.xml")
    End Sub

    Sub BindReport()
        GetCookies()
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable
        Dim dsSPAsset As New DataSet
        Dim objReport As InsuranceCoverageReport = New InsuranceCoverageReport
        Try
            With oCustomClass
                .strConnection = GetConnectionString
                .WhereCond = Me.WhereCond
            End With
            oCustomClass = m_controller.getInsuranceCoverageList(oCustomClass)
            dsSPAsset = oCustomClass.ListData
            objReport.SetDataSource(dsSPAsset)
            With CrystalReportViewer1
                .ReportSource = objReport
                .Visible = True
                .DataBind()
            End With
            With objReport.ExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.ExcelRecord
            End With
            Dim strFileLocation As String
            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "InsuranceCoverage.xls"
            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = _
            New CrystalDecisions.Shared.DiskFileDestinationOptions
            DiskOpts.DiskFileName = strFileLocation
            With objReport
                .ExportOptions.DestinationOptions = DiskOpts
                .Export()
                .Close()
                .Dispose()
            End With
            Response.Redirect("InsuranceCoverage.aspx?strFileLocation=" & Me.Session.SessionID & Me.Loginid & "InsuranceCoverage")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
