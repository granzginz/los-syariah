﻿Imports Maxiloan.Webform.UserController
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper

Public Class InsuranceCoverage
    Inherits Maxiloan.Webform.WebBased
    Private m_controller As New DataUserControlController

    Protected WithEvents sDate As ucDateCE
    Protected WithEvents eDate As ucDateCE

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "INSCOVER"
        If CheckForm(Me.Loginid, Me.FormID, "Eloan") Then
            If Not Page.IsPostBack Then
                If Request.QueryString("strFileLocation") <> "" Then
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String
                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".xls"
                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "window.open('" & strFileLocation & "','accacq', 'menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                    & "</script>")
                End If


                Dim dtbranch As New DataTable
                dtbranch = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                With cboBranch
                    If Me.IsHoBranch Then
                        .DataSource = m_controller.GetBranchAll(GetConnectionString)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                        .Items.Insert(1, "ALL")
                        .Items(1).Value = "ALL"
                    Else
                        .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                        .DataValueField = "ID"
                        .DataTextField = "Name"
                        .DataBind()
                        .Items.Insert(0, "Select One")
                        .Items(0).Value = "0"
                    End If
                End With
            End If
        End If
    End Sub

    Private Sub ButtonViewReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonViewReport.Click
        Dim strWhere As String
        strWhere = IIf(cboBranch.SelectedItem.Value = "ALL", " where agreement.branchID in (select branchID from branch) ", " where agreement.branchID = '" & cboBranch.SelectedItem.Value & "'").ToString
        strWhere = strWhere & " and convert(char(10),agreement.GoLiveDate,112) between '" & _
            ConvertDate2(sDate.Text).ToString("yyyyMMdd") & "' and '" & _
            ConvertDate2(eDate.Text).ToString("yyyyMMdd") & "' and agreement.ContractStatus ='LIV'"
        Dim cookie As HttpCookie = Request.Cookies("InsuranceCoverageCookie")
        If CheckFeature(Me.Loginid, Me.FormID, "print", Me.AppId) Then
            If Not cookie Is Nothing Then
                cookie.Values("whereCond") = strWhere
                Response.AppendCookie(cookie)
            Else
                Dim cookieNew As New HttpCookie("InsuranceCoverageCookie")
                cookieNew.Values.Add("whereCond", strWhere)
                Response.AppendCookie(cookieNew)
            End If
            Response.Redirect("InsuranceCoverageViewer.aspx")
        End If
    End Sub
End Class
