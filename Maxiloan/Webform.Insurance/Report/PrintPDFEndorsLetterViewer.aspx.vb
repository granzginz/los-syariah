﻿#Region "Imports"
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General.CommonCookiesHelper
Imports Maxiloan.Webform.UserController
#End Region

Public Class PrintPDFEndorsLetterViewer
    Inherits Maxiloan.webform.WebBased

#Region "Constanta"
    Private Const SP_PRINT_PDF_ENDORS_LETTER_MAIN As String = "spRptEndors"
    Private Const SP_PRINT_PDF_ENDORS_LETTER_SUB As String = "spRptInsuranceAssetDetail"
    
#End Region
#Region "Property"
    Private Property FROM_FILE() As String
        Get
            Return (CType(viewstate("FROM_FILE"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("FROM_FILE") = Value
        End Set
    End Property
#End Region
#Region "PageLoad"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load        
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            Me.FROM_FILE = Request.QueryString("File")
        End If
        GetCookies()
        BindReport()
    End Sub
#End Region
#Region "Get Cookies"
    Sub GetCookies()
        Dim cookie As HttpCookie = Request.Cookies(General.CommonCookiesHelper.COOKIES_ENDORS_LETTER_PRINT)
        Me.SearchBy = cookie.Values("SearchBy")
        Me.SortBy = cookie.Values("SortBy")

    End Sub
#End Region
#Region "BindReport"
    Sub BindReport()
        Try

            Dim dsMain As New DataSet
            Dim dsMainSub1 As New DataSet
            Dim m_controller As New GeneralPagingController
            Dim oCustomClass As New Parameter.GeneralPaging
            Dim oCustomClassSub As New Parameter.GeneralPaging            
            Dim objReport As New EndorsLetter

            With oCustomClass
                .SpName = SP_PRINT_PDF_ENDORS_LETTER_MAIN
                .strConnection = GetConnectionString
                .WhereCond = Me.SearchBy
                .SortBy = Me.SortBy
                Context.Trace.Write("Panggil SP =" & SP_PRINT_PDF_ENDORS_LETTER_MAIN)
            End With

            oCustomClass = m_controller.GetReportWithParameterWhereCond(oCustomClass)

            With oCustomClassSub
                .SpName = SP_PRINT_PDF_ENDORS_LETTER_SUB
                .strConnection = GetConnectionString
                .WhereCond = Me.SearchBy
                .SortBy = Me.SortBy
                Context.Trace.Write("Panggil SP =" & SP_PRINT_PDF_ENDORS_LETTER_SUB)
            End With

            oCustomClassSub = m_controller.GetReportWithParameterWhereCond(oCustomClassSub)

            If Not oCustomClass Is Nothing Then
                dsMain = oCustomClass.ListDataReport
            End If

            If Not oCustomClassSub Is Nothing Then
                dsMainSub1 = oCustomClassSub.ListDataReport
            End If

            objReport.SetDataSource(dsMain)
            objReport.Subreports.Item("InsuranceAssetDetail").SetDataSource(dsMainSub1)

            CrystalReportViewer1.ReportSource = objReport
            CrystalReportViewer1.Visible = True
            CrystalReportViewer1.DataBind()
 
            Dim DiskOpts As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

            objReport.ExportOptions.ExportDestinationType = CrystalDecisions.[Shared].ExportDestinationType.DiskFile
            objReport.ExportOptions.ExportFormatType = CrystalDecisions.[Shared].ExportFormatType.PortableDocFormat

            Dim strFileLocation As String
            strFileLocation = Request.ServerVariables("APPL_PHYSICAL_PATH") & "XML\"
            strFileLocation += Me.Session.SessionID + Me.Loginid + "rpt_PrintPDFEndorsLetter.pdf"
            DiskOpts.DiskFileName = strFileLocation
            'oPPK.PrintOptions.PaperSize = PaperSize.PaperA4
            objReport.ExportOptions.DestinationOptions = DiskOpts
            objReport.Export()
            objReport.Close()
            objReport.Dispose()
            Response.Redirect("../PolicyReceive.aspx?InsuranceComBranchID=" + Request.QueryString("InsuranceComBranchID") + "&strFileLocation=" & Me.Session.SessionID & Me.Loginid & "rpt_PrintPDFEndorsLetter")
        Catch ex As Exception
            Dim err As New MaxiloanExceptions
            err.WriteLog("PrintPDFEndorsLetterViewer", "BindReport", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
            Response.Write(ex.StackTrace)

        End Try

    End Sub

#End Region
End Class