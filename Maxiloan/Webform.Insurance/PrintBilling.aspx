﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PrintBilling.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.PrintBilling" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PrintBilling</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="form_single">
            <h3>
                CETAK TAGIHAN ASURANSI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
           <label class ="label_req">
                Cabang</label>
            <asp:DropDownList ID="ddlbranch" runat="server">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="rfvCG" runat="server" Display="Dynamic" ErrorMessage="Harap Pilih Cabang"
                ControlToValidate="ddlbranch" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label class ="label_req">
                No Invoice</label>
            <asp:TextBox ID="txtInvoiceNo" runat="server"  Width="128px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtInvoiceNo"
                ErrorMessage="Harap diisi No Invoice" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnViewReport" runat="server" Text="Print" CssClass="small button blue">
        </asp:Button>
    </div>
    </form>
</body>
</html>
