﻿Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper

Public Class BankersClauseView
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.BankersClause
    Private oController As New InsPolicyNumberControler
#End Region

#Region "Property"
    Private Property cmdWhere() As String
        Get
            Return CType(ViewState("cmdwhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("cmdwhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'pnlDtGrid.Visible = False
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If
        Me.FormID = "INSBANKERSCLPRINT"

        If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
            If Not IsPostBack Then
                If Request.QueryString("strFileLocation") <> "" Then
                    Dim strHTTPServer As String
                    Dim StrHTTPApp As String
                    Dim strNameServer As String
                    Dim strFileLocation As String

                    strHTTPServer = Request.ServerVariables("PATH_INFO")
                    strNameServer = Request.ServerVariables("SERVER_NAME")
                    StrHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                    strFileLocation = "http://" & strNameServer & "/" & StrHTTPApp & "/XML/" & Request.QueryString("strFileLocation") & ".pdf"

                    Response.Write("<script language = javascript>" & vbCrLf _
                    & "var x = screen.width; " & vbCrLf _
                    & "var y = screen.height; " & vbCrLf _
                    & "window.open('" & strFileLocation & "','Bankers', 'left=0, top=0, width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes, resizable=1') " & vbCrLf _
                    & "</script>")

                End If

                Me.Sort = "NoClause ASC"
                Me.cmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim()

                'DoBind(Me.cmdWhere)
                BindComboInsuranceBranch()
            End If
        End If
    End Sub

    Sub DoBind(ByVal cmdWhere As String)
        Dim dtEntity As DataTable = Nothing
        Dim oBankersClause As New Parameter.BankersClause

        oBankersClause.strConnection = GetConnectionString()
        oBankersClause.WhereCond = cmdWhere
        oBankersClause.CurrentPage = 1
        oBankersClause.PageSize = 10
        oBankersClause.SortBy = Me.Sort
        oBankersClause = oController.GetBankersClauseViewPrint(oBankersClause)

        If Not oBankersClause Is Nothing Then
            dtEntity = oBankersClause.Listdata                    
        End If
        dtgPaging.DataSource = dtEntity.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
    End Sub

    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        DoBind(Me.cmdWhere)        
    End Sub

    Sub Print(ByVal NoClause As String, ByVal InsuranceComBranchID As String)
        Dim cookie As HttpCookie = Request.Cookies(COOKIES_BANKERS_CLAUSE)
        If Not cookie Is Nothing Then
            cookie.Values("NoClause") = NoClause.Trim
            cookie.Values("InsuranceComBranchID") = InsuranceComBranchID.Trim
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(COOKIES_BANKERS_CLAUSE)
            cookieNew.Values.Add("NoClause", NoClause.Trim)
            cookieNew.Values.Add("InsuranceComBranchID", InsuranceComBranchID.Trim)
            Response.AppendCookie(cookieNew)
        End If
        Response.Redirect("BankersClauseViewer.aspx")
    End Sub

    Private Sub Btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnsearch.Click
        If cboInsuranceBranch.SelectedValue <> "" Or cboInsuranceBranch.SelectedValue <> "0" Then
            Me.cmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim() + " and MaskAssBranchID = '" + cboInsuranceBranch.SelectedValue.Trim + "'"
        Else
            Me.cmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim()
        End If
        DoBind(Me.cmdWhere)
    End Sub

    Private Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        cboInsuranceBranch.SelectedIndex = 0
        Me.cmdWhere = " BranchID = '" + Me.sesBranchId.Replace("'", "").Trim() + "'".Trim()
        DoBind(Me.cmdWhere)
    End Sub
    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Dim err As String = ""
        Dim oGroupSupplierAccount As New Parameter.BankersClause
        If e.CommandName = "Print" Then
            If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
                If SessionInvalid() Then
                    Exit Sub
                End If
            End If            
            Print(dtgPaging.DataKeys.Item(e.Item.ItemIndex).ToString.Trim, dtgPaging.Items(e.Item.ItemIndex).Cells(6).Text.Trim)
        End If
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblNo As Label        
        If e.Item.ItemIndex >= 0 Then            
            lblNo = CType(e.Item.FindControl("lblNo"), Label)


            If e.Item.Cells(1).Text.Trim <> "" And e.Item.Cells(1).Text.Trim <> "&nbsp" Then
                Dim TglDate As DateTime = CDate(e.Item.Cells(1).Text.Trim)
                e.Item.Cells(1).Text = TglDate.ToString("dd-MM-yyyyy")
            End If
            lblNo.Text = CStr(e.Item.ItemIndex + 1)
        End If
    End Sub

    Sub BindComboInsuranceBranch()
        Dim DtInsuranceBranchName As New DataTable
        Dim m_controller As New DataUserControlController

        DtInsuranceBranchName = m_controller.GetInsuranceBranchName(GetConnectionString, Me.sesBranchId, Me.Loginid)

        cboInsuranceBranch.DataValueField = "ID"
        cboInsuranceBranch.DataTextField = "Name"
        cboInsuranceBranch.DataSource = DtInsuranceBranchName
        cboInsuranceBranch.DataBind()
        cboInsuranceBranch.Items.Insert(0, "Select One")
        If ReqValField.Enabled = True Then
            cboInsuranceBranch.Items(0).Value = ""
        Else
            cboInsuranceBranch.Items(0).Value = "0"
        End If
        pnlDtGrid.Visible = True

    End Sub
End Class