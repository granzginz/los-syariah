﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsCoAllocationEdit.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsCoAllocationEdit" %>

<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../webform.UserController/UcSearchByWithNoTable.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../webform.UserController/UcBranch.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsCoAllocationEdit</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = 'http://<%= Request.servervariables("SERVER_NAME")%>/';
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="LblErrMessages" runat="server"></asp:Label>
    <asp:Panel ID="PnlGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR KONTRAK UNTUK PERUBAHAN PERUSAHAAN ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" DataKeyField="Applicationid" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="ASURANSI">
                                <ItemTemplate>
                                    <asp:Label ID="LblHyperLinkSelect" runat="server">
									<a href='InsCoAllocationDetailEdit.aspx?AgreementNo=<%# DataBinder.Eval(Container, "DataItem.AgreementNo") %>&ApplicationId=<%# DataBinder.Eval(Container, "DataItem.ApplicationId") %>&BranchId=<%# DataBinder.Eval(Container, "DataItem.BranchId") %>'> EDIT</a>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="AssetDescription" SortExpression="AssetDescription" HeaderText="NAMA ASSET" />
                            <asp:BoundColumn DataField="MaskAssBranchName" SortExpression="MaskAssBranchName" HeaderText="CABANG ASURANSI" />
                            <asp:BoundColumn DataField="ApplicationStep" SortExpression="ApplicationStep" HeaderText="STEP APLIKASI" />
                            <asp:TemplateColumn Visible="False" HeaderText="CUSTOMER ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CommandName="First" OnCommand="NavigationLink_Click"
                            CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CommandName="Prev" OnCommand="NavigationLink_Click"
                            CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CommandName="Next" OnCommand="NavigationLink_Click"
                            CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CommandName="Last" OnCommand="NavigationLink_Click"
                            CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                            ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"  CssClass="validator_general" ></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                            ErrorMessage="No Halaman Salah"  CssClass="validator_general" 
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                EDIT PERUSAHAAN ASURANSI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang</label>
            <uc1:ucbranch id="oBranch" runat="server">
                </uc1:ucbranch>
        </div>
        <div class="form_right">
            <uc1:ucsearchbywithnotable id="oSearchBy" runat="server">
                </uc1:ucsearchbywithnotable>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnReset" CausesValidation="False" runat="server" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
