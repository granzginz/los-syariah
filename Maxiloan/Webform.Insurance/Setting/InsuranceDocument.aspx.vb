﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Parameter.InsDocSelectionList
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsuranceDocument
    Inherits Maxiloan.Webform.WebBased

#Region " Private Const "
    Private m_InsDoc As New InsDocSelectionController
    Private m_dtvEntity As DataView
    Private m_dtsEntity As DataTable
    Private m_oInsDoc As Parameter.InsDocSelectionList
    Private m_imbDelete As ImageButton

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private Const STR_INS_DOC_ID As String = "InsDocId"
    Private Const STR_INS_DOC_DESC As String = "InsDocDescription"
    Private Const STR_APP_ID As String = "AppId"
    Private Const STR_FILTER_BY As String = "FilterBy"
    Private Const STR_PRINTED_DATE As String = "BusinessDate"
    Private Const STR_PRINTED_BY As String = "PrintedBy"
    Private Const STR_ORDER_BY As String = "OrderBy"
    Private Const STR_COOKIE_NAME As String = "InsDoc"
    Private Const STR_COMPANY_ID As String = "CompanyId"

    Private Const STR_RPT_FOLDER As String = "Report"
    Private Const STR_RPT_VIEWER As String = "RptInsDocumentViewer.aspx"
#End Region



#Region " Property "
    Private Property InsDocID() As String
        Get
            Return CStr(viewstate(STR_INS_DOC_ID))
        End Get
        Set(ByVal Value As String)
            viewstate(STR_INS_DOC_ID) = Value
        End Set
    End Property

    Private Property InsDocDescription() As String
        Get
            Return CStr(viewstate(STR_INS_DOC_DESC))
        End Get
        Set(ByVal Value As String)
            viewstate(STR_INS_DOC_DESC) = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            InitialDefaultPanel()
            Me.FormID = "InsDoc"
            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                'Tentukan panjang property
                txtDocumentCodeAdd.MaxLength = 10
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                Me.SortBy = ""

                BindGridEntity(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAdd.Visible = False
        PnlEdit.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region

#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
        pnlSearch.Visible = True
    End Sub
#End Region

#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        'Dim dtvEntity As DataView
        'Dim dtsEntity As DataTable
        'Dim oInsDoc As New Parameter.eInsuranceRateCardInsDocSelectionList

        With m_oInsDoc
            .strConnection = GetConnectionString
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort
        End With

        m_oInsDoc = m_InsDoc.GetInsDocList(m_oInsDoc)

        If m_oInsDoc Is Nothing Then Exit Sub

        With m_oInsDoc
            lblTotRec.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        m_dtsEntity = m_oInsDoc.ListData
        m_dtvEntity = m_dtsEntity.DefaultView
        m_dtvEntity.Sort = Me.SortBy
        dtgInsDoc.DataSource = m_dtvEntity
        Try
            dtgInsDoc.DataBind()
        Catch
            dtgInsDoc.CurrentPageIndex = 0
            dtgInsDoc.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region

    Private Sub BtSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtSearch.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + " like '%" + StrSearchByValue + "%'"
        End If
        PanelAllFalse()

        Try
            BindGridEntity(Me.SearchBy, Me.SortBy)
        Catch ex As Exception
            DisplayError(ex.Message)
        End Try

        pnlList.Visible = True
    End Sub

    Private Sub DisplayError(ByRef strErrMsg As String)
        ShowMessage(lblMessage, "Data Sudah Ada", True)
        lblMessage.Visible = True
    End Sub

    Private Sub ClearAddForm()
        txtDocumentCodeAdd.Text = ""
        txtDescriptionAdd.Text = ""
    End Sub

    Private Sub BtnAdd1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd1.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            pnlSearch.Visible = False
            ClearAddForm()
            PanelAllFalse()
            pnlAdd.Visible = True
        End If
    End Sub

    Private Sub BtReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtReset.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        PanelAllFalse()
        pnlList.Visible = False
    End Sub

    Public Sub New()
        m_oInsDoc = New Parameter.InsDocSelectionList
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAdd.Click
        InitialDefaultPanel()
        pnlSearch.Visible = True
        pnlList.Visible = True
        pnlAdd.Visible = False
        PnlEdit.Visible = False
    End Sub

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAdd.Click
        Try
            If m_oInsDoc Is Nothing Then
                m_oInsDoc = New Parameter.InsDocSelectionList
            End If

            With m_oInsDoc
                .strConnection = GetConnectionString()
                .InsDocId = txtDocumentCodeAdd.Text
                .InsDocDescription = txtDescriptionAdd.Text
            End With
            m_InsDoc.AddInsDoc(m_oInsDoc)

            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            pnlAdd.Visible = False

        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dtgInsDoc_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtgInsDoc.SelectedIndexChanged

    End Sub

    Private Sub dtgInsDoc_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgInsDoc.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                If checkFeature(Me.Loginid, Me.FormID, "Edit", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    'Me.InsDocID = e.Item.Cells(0).Text
                    lblInsDocIdEdit.Text = e.Item.Cells(2).Text
                    txtInsDocDescEdit.Text = e.Item.Cells(3).Text
                    pnlSearch.Visible = False
                    pnlList.Visible = False
                    pnlAdd.Visible = False
                    PnlEdit.Visible = True
                    'viewbyID(Me.InsCoID)
                End If

            Case "Delete"
                If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                    If sessioninvalid() Then
                        Exit Sub
                    End If

                    Me.InsDocID = e.Item.Cells(2).Text
                    Dim customClass As New Parameter.InsDocSelectionList
                    With customClass
                        .strConnection = getConnectionString
                        .InsDocId = Me.InsDocID
                    End With

                    Try
                        m_InsDoc.DeleteInsDoc(customClass)
                    Catch ex As Exception
                        DisplayError(ex.Message)
                    Finally
                        'BindGridEntity("ALL", "")
                        BindGridEntity(Me.SearchBy, Me.SortBy)
                        pnlList.Visible = True
                    End Try
                End If
        End Select
    End Sub

    Private Sub BtSaveEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtSaveEdit.Click
        Try
            If m_oInsDoc Is Nothing Then
                m_oInsDoc = New Parameter.InsDocSelectionList
            End If

            With m_oInsDoc
                .strConnection = GetConnectionString()
                .InsDocId = lblInsDocIdEdit.Text
                .InsDocDescription = txtInsDocDescEdit.Text
            End With
            m_InsDoc.UpdateInsDoc(m_oInsDoc)

            BindGridEntity(Me.SearchBy, Me.SortBy)
            pnlList.Visible = True
            pnlAdd.Visible = False

        Catch ex As Exception
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dtgInsDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInsDoc.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            m_imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            If m_imbDelete Is Nothing Then Exit Sub
            m_imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub imbNextPage_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbNextPage.Click

    End Sub

    Private Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(STR_COOKIE_NAME)

        Dim a As String

        a = Me.SesCompanyID
        If Not cookie Is Nothing Then
            With cookie
                '.Values(STR_APP_ID) = Me.AppId
                .Values(STR_FILTER_BY) = Me.SearchBy
                '.Values(STR_PRINTED_BY) = Me.Loginid
                '.Values(STR_PRINTED_DATE) = Me.BusinessDate.ToShortDateString() 'Me.BusinessDate.ToString()
                .Values(STR_ORDER_BY) = Me.SortBy
                '.Values(STR_COMPANY_ID) = Me.SesCompanyID
            End With
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(Me.FormID)
            With cookieNew
                '.Values.Add(STR_APP_ID, Me.AppId)
                .Values.Add(STR_FILTER_BY, Me.SearchBy)
                '.Values.Add(STR_PRINTED_BY, Me.Loginid)
                '.Values.Add(STR_PRINTED_DATE, Me.BusinessDate.ToShortDateString())
                .Values.Add(STR_ORDER_BY, Me.SortBy)
                '.Values.Add(STR_COMPANY_ID, Me.SesCompanyID)
            End With
            Response.AppendCookie(cookieNew)
        End If
    End Sub

    Private Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Print", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If
        SendCookies()
        Response.Redirect(STR_RPT_FOLDER + "/" + STR_RPT_VIEWER)
    End Sub

    Private Sub BtCancelEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtCancelEdit.Click
        pnlSearch.Visible = True
        pnlList.Visible = True
        pnlAdd.Visible = False
        PnlEdit.Visible = False
    End Sub

End Class