﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsuranceQuota
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Private oCustomClass As New Parameter.InsQuotaStatistic
    Private m_controller As New InsQuotaStatisticController
    Protected WithEvents oSearchBy As UcSearchBy
    Protected WithEvents oBranch As UcBranch
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
#End Region
#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property

    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property

#End Region
#Region "Page Load"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            ' InitialDefaultPanel()
            PnlDgrid.Visible = True
            Me.BranchID = Me.sesBranchId
            Me.SearchBy = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
            ' Me.SearchBy = " BranchID = '501'"
            Me.SortBy = ""
            BindGridEntity(Me.SearchBy, Me.SortBy)
            Me.FormID = "INSQUOTA"

            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                oSearchBy.ListData = "MaskAssID,Insurance Coy Id-INSURANCECOYNAME, InsuranceCoyName-InsuranceComBranchID, Insurance Coy BranchID"
                oSearchBy.BindData()
                Me.SearchBy = ""
                Me.SortBy = ""
            End If
        End If

    End Sub
#End Region


    Private Sub dtgInsQuota_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInsQuota.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim lblMaskAssID As Label
            lblMaskAssID = CType(e.Item.FindControl("lblMaskAssID"), Label)
            Dim hp As HyperLink
            hp = CType(e.Item.FindControl("hpInsCoBranchID"), HyperLink)
            hp.NavigateUrl = "javascript:OpenViewInsCoBranch('" & lblMaskAssID.Text.Trim & "','" & hp.Text.Trim & "','Insurance')"
            Context.Trace.Write("lblMaskAssID" & lblMaskAssID.Text.Trim)
            Context.Trace.Write("hpInscoBranch" & hp.Text.Trim)
        End If


    End Sub

#Region "BindGrid"
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal sort As String)
        PnlDgrid.Visible = True
        Dim dtEntity As New DataTable
        Dim oInsQuotaStatistic As New Parameter.InsQuotaStatistic
        If sort = "" Then sort = "MaskAssID"
        Me.CmdWhere = cmdWhere
        With oInsQuotaStatistic
            .Page = General.CommonVariableHelper.INS_QUOTA_LISTING
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = sort
            .strConnection = GetConnectionString
        End With
        oInsQuotaStatistic = m_controller.getInsuranceQuotaListing(oInsQuotaStatistic)

        If Not oInsQuotaStatistic Is Nothing Then
            dtEntity = oInsQuotaStatistic.ListData
            recordCount = oInsQuotaStatistic.TotalRecords
        Else
            recordCount = 0
        End If

        dtgInsQuota.DataSource = dtEntity.DefaultView
        dtgInsQuota.CurrentPageIndex = 0
        dtgInsQuota.DataBind()

        PagingFooter()
    End Sub
#End Region
#Region "Search By"

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        PnlDgrid.Visible = True
        Me.BranchID = oBranch.BranchID
        Me.SearchBy = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
        If oSearchBy.Text.Trim <> "" Then
            Me.SearchBy = Me.SearchBy & " And " & oSearchBy.ValueID.Replace("'", "''") & " like '%" & oSearchBy.Text.Trim.Replace("'", "''") & "%' "
        End If
        ' pnlList.Visible = True
        ' pnlDtGrid.Visible = True
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub
#End Region

#Region "Reset"
    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        '       Me.CmdWhere = ""
        oSearchBy.Text = ""
        oSearchBy.BindData()
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region "Go Page"


    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                'currentPage = CType(txtGoPage.Text, Int32)
                ' BindGridEntity(Me.CmdWhere)
            End If
        End If
    End Sub

#End Region
#Region "Sorting"

    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgInsQuota.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.CmdWhere, Me.Sort)
    End Sub

#End Region
#Region "Paging Footer"



    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(PageSize, Integer)), Double))
        If totalPages = 0 Then
            'ShowMessage(lblMessage, "Data tidak ditemukan!", true)
            lblTotPage.Text = "1"
            'rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            'rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

#End Region
#Region "Navigation Link Click"


    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        'BindGridEntity(Me.CmdWhere)
    End Sub

#End Region

End Class