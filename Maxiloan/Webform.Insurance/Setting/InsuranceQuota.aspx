﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceQuota.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceQuota" %>

<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../Webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchBy" Src="../../Webform.UserController/UcSearchBy.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsuranceQuota</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
            var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%=Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&style=INSURANCE&InsCoBranchID=' + pInsCoBranchID, 'InsuranceCo', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlDgrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR KUOTA ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInsQuota" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="QUOTA">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <a href='InsQuotaDetail.aspx?BranchID=<%# DataBinder.eval(Container,"DataItem.BranchID") %>&amp;MaskAssID=<%# DataBinder.eval(Container,"DataItem.MaskAssID")%>&amp;InsuranceComBranchID=<%# DataBinder.eval(Container,"DataItem.BranchID") %>&amp;InsuranceComBranchName=<%# DataBinder.eval(Container,"DataItem.MaskAssBranchName") %>'>
                                        <asp:Image ID="imgQuota" runat="server" ImageUrl="../../images/iconquota.gif"></asp:Image></a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="PERUSAHAAN ASURANSI" SortExpression="MaskAssID">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblMaskAssID" Text='<%# DataBinder.Eval(Container, "DataItem.MaskAssID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsuranceComBranchId" HeaderText="CABANG PERS ASURANSI">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hpInsCoBranchID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BranchID") %>'
                                        Visible="True" Enabled="True">
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="MaskAssBranchName" SortExpression="MaskAssBranchName"
                                HeaderText="NAMA CABANG PERS ASURANSI"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" MinimumValue="1"
                        ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"  CssClass="validator_general" ></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                        ErrorMessage="No Halaman Salah"  CssClass="validator_general" 
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="form_title">
        <div class="form_single">
            <h4>
                CARI KUOTA ASURANSI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cabang</label>
            <uc1:ucbranch id="oBranch" runat="server">
                </uc1:ucbranch>
        </div>
    </div>
    <div class="form_box">
            <uc1:ucsearchby id="oSearchBy" runat="server"></uc1:ucsearchby>
    </div>
    <div class="form_button">
        <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    </form>
</body>
</html>
