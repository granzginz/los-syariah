﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsQuotaDetail.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsQuotaDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsQuotaDetail</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                ALOKASI KUOTA ASURANSI
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Asuransi
            </label>
            <asp:Label ID="LblMaskAssID" runat="server">LblMaskAssID</asp:Label>
        </div>
        <div class="form_right">
            <label>
                Cabang Asuransi
            </label>
            <asp:Label ID="LblInsuranceComBranchName" runat="server">LblInsuranceComBranchName</asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
             <label class ="label_req">
                Kuota Tahun
            </label>
            <asp:TextBox ID="TxtYear" runat="server" MaxLength="4" Width="65px" ></asp:TextBox>(yyyy)
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Harap isi Kuota dengan Angka"
                ControlToValidate="TxtYear" ValidationExpression="\d*" Display="Dynamic" CssClass="validator_general" ></asp:RegularExpressionValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TxtYear"
                ErrorMessage="Tahun Kuota harus 4 Angka" ValueToCompare="2000" Operator="GreaterThanEqual"
                Type="Double" CssClass="validator_general" ></asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TxtYear"
                ErrorMessage="Harap isi Tahun Kuota" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
        </div>
        <div class="form_right">
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnViewListingQuota" runat="server" Text="View" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnsearch" runat="server" Text="Edit" 
            CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnBack" runat="server" CausesValidation="false" Text="Back" CssClass="small button gray">
        </asp:Button>
        <asp:Label ID="LblInsuranceComBranchID" runat="server" Visible="False">LblInsuranceComBranchID</asp:Label><asp:Label
            ID="LblBranchID" runat="server" Visible="False">LblBranchID</asp:Label>
    </div>
    <asp:Panel ID="PnlEntryQuota" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    KUOTA ASURANSI -&nbsp;
                    <asp:Label ID="LblYearQuota" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInsQuota" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn Visible="False" HeaderText="StatisticMonth">
                                <ItemTemplate>
                                    <asp:Label ID="LblStatisticMonth" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StatisticMonth") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BULAN">
                                <HeaderStyle Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StrMonth") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StrMonth") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="Year">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StatisticYear") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StatisticYear") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH KUOTA">
                                <HeaderStyle Width="40%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaAmount") %>'>
                                    </asp:Label>
                                    <asp:TextBox ID="TxtQuotaAmount" runat="server" Width="114px" MaxLength="15" 
                                        Text='<%# container.dataitem("QuotaAmount")%>' Enabled='<%# container.dataitem("IsAllowUpdate") %>'>
                                    </asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="\d*"
                                        ControlToValidate="TxtQuotaAmount" ErrorMessage="Harap isi Kuota dengan Angka"></asp:RegularExpressionValidator>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="IsAllowUpdate" HeaderText="IsAllowUpdate">
                            </asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="(%) KUOTA">
                                <HeaderStyle Width="35%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblQuotaPercentage" runat="server" Visible="False" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaPercentage") %>'>
                                    </asp:Label>
                                    <asp:TextBox ID="txtQuotaPercentage" runat="server" Width="114px" MaxLength="15"
                                         Text='<%# container.dataitem("QuotaPercentage")%>' Enabled='<%# container.dataitem("IsAllowUpdate") %>'>
                                    </asp:TextBox>
                                    <asp:CompareValidator ID="CVQuotaPercentage" runat="server" ErrorMessage="Harap isi dengan Angka antara 0 s/d 100"
                                        Type="Double" ControlToValidate="txtQuotaPercentage" Display="Dynamic" Operator="LessThanEqual"
                                        ValueToCompare="100"></asp:CompareValidator>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlDisplayQuota" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    VIEW - KUOTA ASURANSI TAHUN -&nbsp;
                    <asp:Label ID="LblYearQuotaListing" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DgridViewListingQuota" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="BULAN">
                                <HeaderStyle Width="20%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StrMonth") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StrMonth") %>'>
                                    </asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="JUMLAH KUOTA">
                                <HeaderStyle Width="40%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblViewQuotaAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaAmount", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="(%) KUOTA">
                                <HeaderStyle Width="35%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblViewQuotaPercentage" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.QuotaPercentage", "{0:###,###,##0.00}") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnPrint2" runat="server" CausesValidation="False" Text="Print" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBack2" runat="server" CausesValidation="false" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
