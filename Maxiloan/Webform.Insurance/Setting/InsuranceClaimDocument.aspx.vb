﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Controller
Imports Maxiloan.General
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsuranceClaimDocument
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"
    Dim m_ClaimAct As New InsClaimActController
    Dim oClaimAct As New Parameter.InsClaimAct

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRecord.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If

    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtPage.Text = "" Then
            txtPage.Text = "0"
        Else
            If IsNumeric(txtPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtPage.Text, Int32)
                    Bindgrid(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False

        Me.FormID = "INSDOCCLAIM"
        If Not Me.IsPostBack Then
            If CheckForm(Me.Loginid, Me.FormID, "MAXILOAN") Then
                fillcbo()
                InitialDefaultPanel()
                Me.SearchBy = ""
                Me.SortBy = ""
                Bindgrid(Me.SearchBy, Me.SortBy)
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If

        

    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = True
        pnlAdd.Visible = False
        pnlsearch.Visible = True

    End Sub
    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With ddlBranch
            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
            End If
        End With


        With oClaimAct
            .strConnection = GetConnectionString()
        End With

        With ddlInsAssetType
            .DataSource = m_ClaimAct.GetInsRateCategory(oClaimAct)
            .DataValueField = "InsRateCategoryID"
            .DataTextField = "Description"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
        With ddlAssetType
            .DataSource = m_ClaimAct.GetInsRateCategory(oClaimAct)
            .DataValueField = "InsRateCategoryID"
            .DataTextField = "Description"
            .DataBind()
        End With
        Dim oClaimReq As New Parameter.ClaimRequest
        Dim m_Claimreq As New ClaimReqController

        oClaimReq.strConnection = GetConnectionString
        oClaimReq = m_Claimreq.ClaimrequestGetClaimType(oClaimReq)
        With ddlClaimType
            .DataSource = oClaimReq.ListData
            .DataTextField = "Description"
            .DataValueField = "ClaimType"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = ""
        End With
        With dllClaimTypeAdd
            .DataSource = oClaimReq.ListData
            .DataTextField = "Description"
            .DataValueField = "ClaimType"
            .DataBind()
        End With
        With ddlInsDocID
            .DataSource = m_ClaimAct.GetDocMaster(oClaimAct)
            .DataTextField = "InsDocdescription"
            .DataValueField = "InsDocid"
            .DataBind()
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        With oClaimAct
            .strConnection = GetConnectionString()
            .InsDocID = ddlInsDocID.SelectedValue
            .BranchId = ddlBranch.SelectedValue
            .InsAssetType = ddlAssetType.SelectedValue
            .ClaimType = dllClaimTypeAdd.SelectedValue
        End With

        If m_ClaimAct.SaveClaimDoc(oClaimAct) Then
            Bindgrid(Me.SearchBy, Me.SortBy)
            ShowMessage(lblMessage, "Simpan Data Berhasil", False)
        Else
            ShowMessage(lblMessage, "Simpan Data Gagal", True)
        End If
        InitialDefaultPanel()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        InitialDefaultPanel()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Me.SearchBy = ""
        Me.SortBy = ""

        If ddlBranch.SelectedValue <> "" Then
            Me.SearchBy = "BranchID='" & ddlBranch.SelectedValue & "'"
        End If
        If ddlInsAssetType.SelectedValue <> "" Then
            If Me.SearchBy.Trim = "" Then
                Me.SearchBy += " InsuranceType = '" & ddlInsAssetType.SelectedValue & "'"
            Else
                Me.SearchBy += " and InsuranceType = '" & ddlInsAssetType.SelectedValue & "'"
            End If

        End If
        If ddlClaimType.SelectedValue <> "" Then
            If Me.SearchBy.Trim = "" Then
                Me.SearchBy += " InsClaimDocument.ClaimType = '" & ddlClaimType.SelectedValue & "'"
            Else
                Me.SearchBy += " and  InsClaimDocument.ClaimType = '" & ddlClaimType.SelectedValue & "'"
            End If

        End If
        Bindgrid(Me.SearchBy, Me.SortBy)

    End Sub

    Sub Bindgrid(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oClaimAct
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oClaimAct = m_ClaimAct.DocclaimList(oClaimAct)

        With oClaimAct
            lblRecord.Text = CType(.TotalRecord, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecord
        End With

        dtsEntity = oClaimAct.ListData
        dtsEntity.TableName = "Table"

        dtvEntity = dtsEntity.DefaultView

        dtgDocClaimList.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgDocClaimList.DataBind()
        End If

        PagingFooter()
        pnlDtGrid.Visible = True
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            pnlDtGrid.Visible = False
            pnlsearch.Visible = False
            pnlAdd.Visible = True

            lblBranch.Text = ddlBranch.SelectedItem.Text
        Else
            Dim strHTTPServer As String
            Dim strHTTPApp As String
            Dim strNameServer As String
            strHTTPServer = Request.ServerVariables("PATH_INFO")
            strNameServer = Request.ServerVariables("SERVER_NAME")
            strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
            Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
        End If
    End Sub

    Private Sub dtgDocClaimList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgDocClaimList.ItemCommand
        If e.CommandName = "Del" Then
            If checkFeature(Me.Loginid, Me.FormID, "Del", Me.AppId) Then
                With oClaimAct
                    .strConnection = GetConnectionString()
                    .WhereCond = " branchId='" & e.Item.Cells(2).Text.Trim & "' and InsuranceType='" & e.Item.Cells(3).Text.Trim & "' and ClaimType='" & e.Item.Cells(4).Text.Trim & "' and InsDocID='" & e.Item.Cells(1).Text.Trim & "'"
                End With
                If m_ClaimAct.DeleteClaimDoc(oClaimAct) Then
                    ShowMessage(lblMessage, "Hapus Data Berhasil", False)
                Else
                    ShowMessage(lblMessage, "Hapus Data Gagal, Dokumen Asuransi Sudah dipakai", True)
                End If
                Bindgrid(Me.SearchBy, Me.SortBy)
                InitialDefaultPanel()
            Else
                Dim strHTTPServer As String
                Dim strHTTPApp As String
                Dim strNameServer As String
                strHTTPServer = Request.ServerVariables("PATH_INFO")
                strNameServer = Request.ServerVariables("SERVER_NAME")
                strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If

        End If

    End Sub

    Private Sub dtgDocClaimList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgDocClaimList.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            CType(e.Item.FindControl("imbDelete"), ImageButton).Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    'Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click

    '    Dim cookie As HttpCookie = Request.Cookies("INSDOCCLAIM")

    '    If Not cookie Is Nothing Then
    '        cookie.Values("WhereCond") = Me.SearchBy
    '        cookie.Values("BranchName") = ddlBranch.SelectedItem.Text.Trim
    '        cookie.Values("BranchID") = ddlBranch.SelectedValue
    '        Response.AppendCookie(cookie)
    '    Else
    '        Dim cookieNew As New HttpCookie("INSDOCCLAIM")
    '        cookieNew.Values.Add("WhereCond", Me.SearchBy)
    '        cookieNew.Values.Add("BranchName", ddlBranch.SelectedItem.Text.Trim)
    '        cookieNew.Values.Add("BranchID", ddlBranch.SelectedValue)
    '        Response.AppendCookie(cookieNew)
    '    End If
    '    Response.Redirect("InsuranceClaimDocumentViewer.aspx")

    'End Sub

    Private Sub btnReset_Click(sender As Object, e As System.EventArgs) Handles btnReset.Click
        Bindgrid(Me.SearchBy, Me.SortBy)
    End Sub
End Class