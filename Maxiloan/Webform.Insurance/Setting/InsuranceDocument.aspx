﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceDocument.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceDocument" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsuranceDocument</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR DOKUMEN ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInsDoc" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" ImageUrl="../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="InsDocId" SortExpression="InsDocId" HeaderText="ID DOKUMEN">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InsDocDescription" SortExpression="InsDocDescription"
                                HeaderText="NAMA DOKUMEN">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" CommandName="First"
                        OnCommand="NavigationLink_Click" ImageUrl="../../Images/grid_navbutton01.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" CommandName="Prev"
                        OnCommand="NavigationLink_Click" ImageUrl="../../Images/grid_navbutton02.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" CommandName="Next"
                        OnCommand="NavigationLink_Click" ImageUrl="../../Images/grid_navbutton03.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" CommandName="Last"
                        OnCommand="NavigationLink_Click" ImageUrl="../../Images/grid_navbutton04.png">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtGoPage" MinimumValue="1" ErrorMessage="No Halaman Salah"
                        MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtGoPage" ErrorMessage="No Halaman Salah"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd1" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnPrint" runat="server" CausesValidation="False" Text="Print" CssClass="small button green">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI DOKUMEN ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                    <asp:ListItem Value="Select One" Selected="True">Select One</asp:ListItem>
                    <asp:ListItem Value="InsDocId">ID Dokumen</asp:ListItem>
                    <asp:ListItem Value="InsDocDescription">Nama Dokumen</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px" ></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtSearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtReset" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlEdit" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOKUMEN ASURANSI - EDIT
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Dokumen</label>
                <asp:Label ID="lblInsDocIdEdit" runat="server" Width="583px"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Dokumen</label>
                <asp:TextBox ID="txtInsDocDescEdit" runat="server" Width="291px" 
                    MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Rfv2" runat="server" Width="184px" ControlToValidate="txtInsDocDescEdit"
                    ErrorMessage="Harap isi Nama Dokumen" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtSaveEdit" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtCancelEdit" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOKUMEN ASURANSI - ADD
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Dokumen</label>
                <asp:TextBox ID="txtDocumentCodeAdd" runat="server" Width="100px" 
                    MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                    ControlToValidate="txtDocumentCodeAdd" ErrorMessage="Harap isi ID Dokumen" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Dokumen</label>
                <asp:TextBox ID="txtDescriptionAdd" runat="server" Width="291px" 
                    MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" ControlToValidate="txtDescriptionAdd"
                    ErrorMessage="Harap isi Nama Dokumen" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveAdd" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelAdd" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
