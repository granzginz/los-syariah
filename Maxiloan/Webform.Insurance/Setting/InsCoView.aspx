﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsCoView.aspx.vb" Inherits="Maxiloan.Webform.Insurance.InsCoView" %>

<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../webform.UserController/UcContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../Webform.UserController/ucCompanyAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsCoView</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function windowClose() {
            window.close();
        }
        function fback() {
            history.go(-1);
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="PnlView" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    VIEW - CABANG PERUSAHAAN ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    ID Asuransi
                </label>
                <asp:Label ID="lblInsCoID" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Asuransi
                </label>
                <asp:Label ID="lblInsCoName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    ID Cabang Asuransi
                </label>
                <asp:Label ID="lblInsCoBranchID" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Nama Cabang Asuransi
                </label>
                <asp:Label ID="lblInsCoBranchName" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Alamat
                </label>
                <asp:Label ID="lblAddress" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    RT
                </label>
                <asp:Label ID="lblRT" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    RW
                </label>
                <asp:Label ID="lblRW" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Kelurahan
                </label>
                <asp:Label ID="lblKelurahan" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Kecamatan
                </label>
                <asp:Label ID="lblkecamatan" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Kota
                </label>
                <asp:Label ID="lblCity" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    kode Pos
                </label>
                <asp:Label ID="lblZipCode" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Telepon-1
                </label>
                <asp:Label ID="lblPhone1" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    No Telepon-2
                </label>
                <asp:Label ID="lblPhone2" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No Fax
                </label>
                <asp:Label ID="lblFax" runat="server"></asp:Label>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Kontak
                </label>
                <asp:Label ID="lblCPName" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    Jabatan
                </label>
                <asp:Label ID="lblCPTitle" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    No HandPhone
                </label>
                <asp:Label ID="lblMobilePhone" runat="server"></asp:Label>
            </div>
            <div class="form_right">
                <label>
                    E-mail
                </label>
                <asp:Label ID="lblEmail" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnClose1" runat="server" OnClientClick="windowClose();" CausesValidation="False" Text="Close" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
