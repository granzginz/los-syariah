﻿#Region "Imports"
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
Imports Maxiloan.Parameter.InsCo
#End Region

Public Class InsCoView
    Inherits Maxiloan.Webform.WebBased
    Dim m_InsCo As New InsCoSelectionController

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Page.IsPostBack Then
            Me.FormID = "InsCoBranch"
            'If CheckFeature(Me.Loginid, Me.FormID, "View", "MAXILOAN") Then
            lblInsCoID.Text = Request("InsCoID")
            lblInsCoBranchID.Text = Request("InsCoBranchID")
            viewbyID(lblInsCoID.Text, lblInsCoBranchID.Text)
            PnlView.Visible = True
            BtnClose1.Attributes.Add("onclick", "windowClose()")
            'End If
        End If
    End Sub

    Private Sub viewbyID(ByVal inscoid As String, ByVal inscoBranchid As String)
        Dim customClass As New Parameter.InsCoSelectionList
        Dim inscobranch As New Parameter.InsCoBranch
        Dim dtInsCo As New DataTable

        With inscobranch
            .strConnection = getConnectionString
            .InsCoID = inscoid
            .InsCoBranchID = inscoBranchid
        End With

        Try

            customClass = m_InsCo.GetInsCompanyBranchByID(inscobranch)
            dtInsCo = customClass.ListData

            'Isi semua field Edit Action

            lblInsCoName.Text = CStr(dtInsCo.Rows(0).Item("InsCoName"))
            lblInsCoBranchName.Text = CStr(dtInsCo.Rows(0).Item("InsCoBranchName"))

            lblAddress.Text = CStr(dtInsCo.Rows(0).Item("Address"))
            lblRT.Text = CStr(dtInsCo.Rows(0).Item("RT"))
            lblRW.Text = CStr(dtInsCo.Rows(0).Item("RW"))
            lblKelurahan.Text = CStr(dtInsCo.Rows(0).Item("Kelurahan"))
            lblkecamatan.Text = CStr(dtInsCo.Rows(0).Item("Kecamatan"))
            lblCity.Text = CStr(dtInsCo.Rows(0).Item("City"))
            lblZipCode.Text = CStr(dtInsCo.Rows(0).Item("ZipCode"))
            lblPhone1.Text = CStr(dtInsCo.Rows(0).Item("AreaPhone1")) + "-" + CStr(dtInsCo.Rows(0).Item("Phone1"))
            lblPhone2.Text = CStr(dtInsCo.Rows(0).Item("AreaPhone2")) + "-" + CStr(dtInsCo.Rows(0).Item("Phone2"))
            lblFax.Text = CStr(dtInsCo.Rows(0).Item("AreaFax")) + "-" + CStr(dtInsCo.Rows(0).Item("Fax"))

            lblCPName.Text = CStr(dtInsCo.Rows(0).Item("ContactPersonName"))
            lblCPTitle.Text = CStr(dtInsCo.Rows(0).Item("ContactPersonTitle"))
            lblMobilePhone.Text = CStr(dtInsCo.Rows(0).Item("MobilePhone"))
            lblEmail.Text = CStr(dtInsCo.Rows(0).Item("Email"))


        Catch ex As Exception

        End Try

    End Sub

End Class