﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceClaimDocument.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceClaimDocument"   %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>InsuranceClaimDocument</title>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }

        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function OpenWinHistory(pCGID, pExID, pExName) {
            var x = screen.width; var y = screen.height - 100; window.open('../ExecutorHistory.aspx?CGID=' + pCGID + '&ExecutorID=' + pExID + '&ExecutorName=' + pExName + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCollector(pCollID, pCGID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../Webform.Setup/CollectorView.aspx?CGID=' + pCGID + '&CollectorID=' + pCollID + '', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }

        function OpenWinCustomer(pID) {
            var x = screen.width; var y = screen.height - 100; window.open('../../../Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=Collection', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinPrintClaimForm(BranchID, ApplicationID) {
            var x = screen.width; var y = screen.height - 100; window.open('PrintClaimForm.aspx?Branch=' + BranchID + '&style=Insurance&ApplicationID=' + ApplicationID, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function currencyFormatku(fld, e) {
            var key = '';
            var i = j = 0;
            var strCheck = '0123456789';
            var whichCode = (window.Event) ? e.which : e.keyCode;
            document.onkeypress = window.event;
            if (whichCode == 13) return true;  // Enter
            key = String.fromCharCode(whichCode);  // Get key value from key code
            if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR DOKUMEN KLAIM ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgDocClaimList" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" ImageUrl="../../Images/icondelete.gif" runat="server" CausesValidation="false" 
                                        CommandName="Del"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="INSDOCID"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="BranchId"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="ASSETTYPE"></asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CLAIMTYPE"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RateCategoryDesc" HeaderText="JENIS ASSET ASR"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ClaimTypeDesc" HeaderText="JENIS KLAIM"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DOCUMENT" HeaderText="DOKUMEN"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Visible="False" HorizontalAlign="Left" ForeColor="#000066" BackColor="White"
                            Mode="NumericPages"></PagerStyle>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo"  CssClass="validator_general" 
                        runat="server" Type="Integer" MaximumValue="999999999" ErrorMessage="No Halaman Salah"
                        MinimumValue="1" ControlToValidate="txtpage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo"  CssClass="validator_general" 
                        runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtpage" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblRecord" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
<%--            <asp:Button ID="btnPrint" runat="server" Enabled="true" Text="Print" CssClass="small button green">
            </asp:Button>--%>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlsearch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI DOKUMEN KLAIM ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Cabang</label>
                <asp:DropDownList ID="ddlBranch" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form_right">
                <label>
                    Jenis Asset Asuransi
                </label>
                <asp:DropDownList ID="ddlInsAssetType" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_left">
                <label>
                    Jenis Klaim
                </label>
                <asp:DropDownList ID="ddlClaimType" runat="server">
                </asp:DropDownList>
            </div>
            <div class="form_right">
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DOKUMEN KLAIM ASURANSI - ADD
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblBranch" runat="server" ForeColor="Black">Label</asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Asset Asuransi</label>
                <asp:DropDownList ID="ddlAssetType" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Klaim</label>
                <asp:DropDownList ID="dllClaimTypeAdd" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Dokumen</label>
                <asp:DropDownList ID="ddlInsDocID" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
