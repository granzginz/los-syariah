﻿#Region "Imports"
Imports System.Text
Imports Maxiloan.Exceptions
Imports Maxiloan.Parameter
Imports Maxiloan.General
Imports Maxiloan.BusinessProcess
Imports Maxiloan.Controller
#End Region

Public Class InsQuotaDetail
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"

    Private oCustomClass As New Parameter.InsQuotaStatistic
    Private m_controller As New InsQuotaStatisticController
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = 12
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    'Private THIS_PAGE As String = "INSQUOTA"
    Private SORT_BY_FIXED As String = " StatisticMonth ASC "

#End Region
#Region "Property"
    Private Property PageNumber() As String
        Get
            Return (CType(Viewstate("page"), String))
        End Get
        Set(ByVal Value As String)
            Viewstate("page") = Value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property


    Private Property Sort() As String
        Get
            Return CType(viewstate("Sort"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Sort") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property

    Public Property MaskAssID() As String
        Get
            Return CType(viewstate("vwMaskAssID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwMaskAssID") = Value
        End Set
    End Property

    Public Property InsuranceComBranchID() As String
        Get
            Return CType(viewstate("vwInsuranceComBranchID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwInsuranceComBranchID") = Value
        End Set
    End Property

    Public Property InsuranceComBranchName() As String
        Get
            Return CType(viewstate("vwInsuranceComBranchName"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwInsuranceComBranchName") = Value
        End Set
    End Property


    Public Property StatisticYear() As String
        Get
            Return CType(viewstate("vwStatisticYear"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwStatisticYear") = Value
        End Set
    End Property

#End Region
#Region "Page Load"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sessioninvalid() Then
            Exit Sub
        End If
        If Not IsPostBack Then
            LblBranchID.Text = Request.QueryString("BranchId")
            LblMaskAssID.Text = Request.QueryString("MaskAssID")
            LblInsuranceComBranchID.Text = Request.QueryString("InsuranceComBranchID")
            LblInsuranceComBranchName.Text = Request.QueryString("InsuranceComBranchName")
            TxtYear.Text = CType(Year(Me.BusinessDate), String)
            InitialDefaultPanel()
            Me.SortBy = SORT_BY_FIXED
        End If


    End Sub

#End Region
#Region " InitialDefaultPanel "

    Public Sub InitialDefaultPanel()
        PnlEntryQuota.Visible = False
        PnlDisplayQuota.Visible = False


    End Sub

#End Region
#Region "Bind Grid "

    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtEntity As New DataTable
        Dim oInsQuotaStatistic As New Parameter.InsQuotaStatistic

        PnlEntryQuota.Visible = True
        With oInsQuotaStatistic
            .Page = General.CommonVariableHelper.INS_QUOTA_LISTING_DETAIL_MONTH_YEAR
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = " StatisticMonth ASC "
            .strConnection = GetConnectionString
        End With

        oInsQuotaStatistic = m_controller.getInsuranceQuotaListing(oInsQuotaStatistic)

        If Not oInsQuotaStatistic Is Nothing Then
            dtEntity = oInsQuotaStatistic.ListData
            recordCount = oInsQuotaStatistic.TotalRecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            ' imbPrint.Enabled = False
        Else
            'imbPrint.Enabled = True
        End If

        dtgInsQuota.DataSource = dtEntity.DefaultView
        dtgInsQuota.CurrentPageIndex = 0
        dtgInsQuota.DataBind()

    End Sub
#End Region
#Region "Setting Variabel Search By"

    Public Function setSearhBy(ByVal isreport As Int16) As String

        Dim statYear As String = TxtYear.Text.Trim
        Dim strBranchid As String = LblBranchID.Text.Trim.Replace("'", "")
        Dim strMaskAssID As String = LblMaskAssID.Text.Trim.Replace("'", "")
        Dim strInsuranceComBranchID As String = LblInsuranceComBranchID.Text.Trim
        Dim strReportQuery As String
        If isreport = 1 Then
            strReportQuery = "InsuranceStatistic."
        Else
            strReportQuery = ""
        End If
        Dim searchBuilder As New StringBuilder
        searchBuilder.Append(strReportQuery & "BranchID = '" & strBranchid & "' ")
        searchBuilder.Append(" AND ")
        searchBuilder.Append(strReportQuery & "MaskAssID = '" & strMaskAssID & "' ")
        searchBuilder.Append(" AND ")
        searchBuilder.Append(strReportQuery & "MaskAssBranchID = '" & strInsuranceComBranchID & "' ")
        searchBuilder.Append(" AND ")
        searchBuilder.Append(" StatisticYear = '" & statYear & "' ")

        Return searchBuilder.ToString

    End Function

#End Region
#Region "Search"

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click

        'If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_INSURANCE_QUOTA, CommonVariableHelper.FORM_FEATURE_YEAR, CommonVariableHelper.APPLICATION_NAME) Then
        '    If sessioninvalid() Then
        '        Exit Sub
        '    End If
        'End If

        PnlEntryQuota.Visible = True
        PnlDisplayQuota.Visible = False

        Me.SearchBy = setSearhBy(0)
        Me.BranchID = LblBranchID.Text
        Me.MaskAssID = LblMaskAssID.Text.Trim.Replace("'", "").Trim
        Me.InsuranceComBranchID = LblInsuranceComBranchID.Text.Trim
        Me.InsuranceComBranchName = LblInsuranceComBranchName.Text.Trim
        Me.StatisticYear = TxtYear.Text.Trim
        LblYearQuota.Text = TxtYear.Text.Trim

        Dim oProcessQuotaYear As New Parameter.InsQuotaStatistic

        With oProcessQuotaYear
            .BranchId = Me.BranchID
            .MaskAssID = Me.MaskAssID
            .InsuranceComBranchID = Me.InsuranceComBranchID
            .InsuranceComBranchName = Me.InsuranceComBranchName
            .StatisticYear = CType(Me.StatisticYear, Int16)
            .strConnection = GetConnectionString()
        End With
        Try
            m_controller.ProcessInsuranceQuotaYear(oProcessQuotaYear)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        BindGridEntity(Me.SearchBy)

    End Sub
#End Region
#Region "Cancel"


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        InitialDefaultPanel()
        TxtYear.Text = CType(Year(Me.BusinessDate), String)

    End Sub

#End Region
#Region "Save"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click


        If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_INSURANCE_QUOTA, CommonVariableHelper.FORM_FEATURE_ENTRY, CommonVariableHelper.APPLICATION_NAME) Then
            If SessionInvalid() Then
                Exit Sub
            End If
        End If


        PnlEntryQuota.Visible = True

        Dim oProcessQuotaYear As New Parameter.InsQuotaStatistic

        Dim LoopQuota As Int16
        Dim StrStatisticMonth As New Label
        Dim TxtQuotaAmount As New TextBox
        Dim txtQuotaPercentage As New TextBox

        For LoopQuota = 0 To CType(dtgInsQuota.Items.Count - 1, Int16)
            StrStatisticMonth = CType(dtgInsQuota.Items(LoopQuota).Cells(1).FindControl("LblStatisticMonth"), Label)
            TxtQuotaAmount = CType(dtgInsQuota.Items(LoopQuota).Cells(4).FindControl("TxtQuotaAmount"), TextBox)
            txtQuotaPercentage = CType(dtgInsQuota.Items(LoopQuota).Cells(5).FindControl("txtQuotaPercentage"), TextBox)

            If Trim(TxtQuotaAmount.Text) = "" Then
                TxtQuotaAmount.Text = "0"
            End If
            If Trim(txtQuotaPercentage.Text) = "" Then
                txtQuotaPercentage.Text = "0"
            End If

            With oProcessQuotaYear
                .BranchId = Me.BranchID
                .MaskAssID = Me.MaskAssID
                .InsuranceComBranchID = Me.InsuranceComBranchID
                .InsuranceComBranchName = Me.InsuranceComBranchName
                .QuotaAmont = CType(TxtQuotaAmount.Text, Decimal)
                .QuotaPercentage = CType(txtQuotaPercentage.Text, Decimal)
                .StatisticMonth = CType(StrStatisticMonth.Text, Int16)
                .StatisticYear = CType(TxtYear.Text, Int16)
                .strConnection = GetConnectionString()
            End With
            Try
                m_controller.UpdateInsuranceQuotaYear(oProcessQuotaYear)
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try


        Next

        Response.Redirect("InsuranceQuota.aspx")
        '   BindGridEntity(Me.SearchBy)

    End Sub

#End Region
#Region "View Listing Quota Button Click"


    Private Sub btnViewListingQuota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewListingQuota.Click

        'If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_INSURANCE_QUOTA, CommonVariableHelper.FORM_FEATURE_VIEW, CommonVariableHelper.APPLICATION_NAME) Then
        '    If sessioninvalid() Then
        '        Exit Sub
        '    End If
        'End If

        PnlDisplayQuota.Visible = True
        PnlEntryQuota.Visible = False
        LblYearQuotaListing.Text = TxtYear.Text.Trim
        Dim dtEntity As New DataTable
        Dim oInsQuotaStatistic As New Parameter.InsQuotaStatistic

        PnlEntryQuota.Visible = False
        PnlDisplayQuota.Visible = True

        Me.SearchBy = setSearhBy(0)
        Me.SortBy = "StatisticMonth ASC"
        With oInsQuotaStatistic
            .Page = General.CommonVariableHelper.INS_QUOTA_LISTING_DETAIL_MONTH_YEAR
            .WhereCond = Me.SearchBy
            .CurrentPage = currentPage
            .PageSize = PageSize
            .SortBy = Me.SortBy
            .strConnection = GetConnectionString()
        End With

        oInsQuotaStatistic = m_controller.getInsuranceQuotaListing(oInsQuotaStatistic)

        If Not oInsQuotaStatistic Is Nothing Then
            dtEntity = oInsQuotaStatistic.ListData
            recordCount = oInsQuotaStatistic.TotalRecords
        Else
            recordCount = 0
        End If
        If recordCount = 0 Then
            ' imbPrint.Enabled = False
        Else
            'imbPrint.Enabled = True
        End If

        DgridViewListingQuota.DataSource = dtEntity.DefaultView
        DgridViewListingQuota.CurrentPageIndex = 0
        DgridViewListingQuota.DataBind()

    End Sub


#End Region
#Region "Back Button"


    Private Sub btnBack2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack2.Click
        InitialDefaultPanel()
        TxtYear.Text = ""

    End Sub

#End Region

    Public Sub Print()

        If CheckFeature(Me.Loginid, CommonVariableHelper.FORM_NAME_INSURANCE_QUOTA, CommonVariableHelper.FORM_FEATURE_PRINT, CommonVariableHelper.APPLICATION_NAME) Then
            If sessioninvalid() Then
                Exit Sub
            End If
        End If

        SendCookies()
        'Dim strvarquerystringBranchId As String = "?BranchID='" & Me.BranchID & "'"
        'Dim strvarQueryStringMaskAssID As String = "&MaskAssID='" & Me.MaskAssID & "'"
        'Dim strvarQueryStringInsuranceComBranchID As String = "&InsuranceComBranchId='" & Me.InsuranceComBranchID & "'"
        'Dim strvarQueryStringInsuranceComBranchName As String = "&InsuranceComBranchName='" & Me.InsuranceComBranchName & "'"
        'Dim strvarquerystringFull As String = strvarquerystringBranchId & strvarQueryStringMaskAssID & strvarQueryStringInsuranceComBranchID & strvarQueryStringInsuranceComBranchName
        Dim filename As String = "Report/InsuranceQuotaViewer.aspx"
        Response.Redirect(filename)


    End Sub

#Region "Print dihalaman save"

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Print()
    End Sub

#End Region

#Region "Send Cookies "


    Sub SendCookies()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_INSURANCE_QUOTA)
        If Not cookie Is Nothing Then
            cookie.Values("SearchBy") = setSearhBy(1)
            cookie.Values("SortBy") = Me.SortBy
            cookie.Values("BranchId") = Me.BranchID
            cookie.Values("InsCoID") = Me.MaskAssID
            cookie.Values("InsCoBranchID") = Me.InsuranceComBranchID
            cookie.Values("BranchName") = Me.InsuranceComBranchName
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_INSURANCE_QUOTA)
            cookieNew.Values.Add("SearchBy", setSearhBy(1))
            cookieNew.Values.Add("SortBy", Me.SortBy)
            cookieNew.Values.Add("BranchId", Me.BranchID)
            cookieNew.Values.Add("InsCoID", Me.MaskAssID)
            cookieNew.Values.Add("InsCoBranchID", Me.InsuranceComBranchID)
            cookieNew.Values.Add("BranchName", Me.InsuranceComBranchName)
            Response.AppendCookie(cookieNew)
        End If
    End Sub
#End Region

#Region "Print di halaman view "

    Private Sub btnPrint2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint2.Click

        Print()

    End Sub
#End Region

    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("InsuranceQuota.aspx")
    End Sub


End Class