﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class WFStdCoverageOthers
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private oCustomClass As New Parameter.GeneralPaging
    Private oController As New GeneralPagingController
    Private oCustomClassSave As New Parameter.eInsuranceRateCard
    Private oControllerSave As New cInsuranceRateCard
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Command() As String
        Get
            Return CType(viewstate("Command"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Command") = Value
        End Set
    End Property
#End Region

#Region "Panel"
    Sub PanelHeader()
        pnlrate.Visible = True
        pnlAddEditrate.Visible = False
    End Sub
    Sub PanelDetail()
        pnlrate.Visible = False
        pnlAddEditrate.Visible = True
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim BranchID As String

        If sessioninvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            PanelHeader()
            BranchID = Request("BranchID")
            lblBranchIDH.Text = BranchID.Trim
            BindGridEntity()
            fillcbo()
        End If
    End Sub
    Sub BindGridEntity()
        Dim dtRate As DataTable
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = "BranchID = '" & lblBranchIDH.Text.Trim & "'"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowStdOtherCoverageRatePaging"
        End With
        oCustomClass = oController.GetGeneralPaging(oCustomClass)

        If Not oCustomClass Is Nothing Then
            dtRate = oCustomClass.ListData
        End If

        dtgRate.DataSource = dtRate.DefaultView
        dtgRate.CurrentPageIndex = 0
        dtgRate.DataBind()
    End Sub
    Private Sub fillcbo()
        Dim dtInsuranceCoverage As DataTable
        With oCustomClass
            .strConnection = GetConnectionString
            .WhereCond = "isactive = 1"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowFillDDOtherCoverageRateToCust"
        End With
        oCustomClass = oController.GetGeneralPaging(oCustomClass)
        dtInsuranceCoverage = oCustomClass.ListData

        With cboCovType
            .DataSource = dtInsuranceCoverage.DefaultView
            .DataValueField = "CoverageID"
            .DataTextField = "Description"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            '.Items.Insert(1, "ALL")
            '.Items(1).Value = "ALL"
        End With
    End Sub
    Private Sub dtgRate_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRate.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                PanelDetail()
                Me.Command = "E1"
                lblTitleAddEdit.Text = "EDIT"
                lblBranchIDD.Text = lblBranchIDH.Text
                cboCovType.Enabled = False
                cboCovType.SelectedValue = dtgRate.Items(e.Item.ItemIndex).Cells(0).Text
                txtrate.Text = CDec(dtgRate.Items(e.Item.ItemIndex).Cells(2).Text).ToString
            Case "Delete"
                Me.Command = "D1"
                With oCustomClassSave
                    .strConnection = GetConnectionString
                    .CardID = lblBranchIDH.Text.Trim
                    .OtherCoverage = dtgRate.Items(e.Item.ItemIndex).Cells(0).Text
                    .CoverageRate = CDbl(dtgRate.Items(e.Item.ItemIndex).Cells(2).Text)
                    .Command = Me.Command
                End With
                oCustomClassSave = oControllerSave.GetOtherCoverage(oCustomClassSave)

                If oCustomClassSave.output = "" Then
                    ShowMessage(lblMessage, "Hapus Data Berhasil", False)
                    PanelHeader()
                    BindGridEntity()
                Else
                    ShowMessage(lblMessage, oCustomClassSave.output, True)
                End If
        End Select
    End Sub
    Private Sub dtgRate_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgRate.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete2 As ImageButton
            imbDelete2 = CType(e.Item.FindControl("imbdeleterate"), ImageButton)
            imbDelete2.Attributes.Add("Onclick", "return DeleteRateConfirm()")
        End If
    End Sub
    Private Sub btnAdd1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd1.Click
        Me.Command = "A1"
        PanelDetail()
        cboCovType.Enabled = True
        cboCovType.ClearSelection()
        txtrate.Text = ""
        lblTitleAddEdit.Text = "ADD"
        lblBranchIDD.Text = lblBranchIDH.Text
    End Sub
    Private Sub btnBackPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackPage.Click
        Response.Redirect("WFNewInsuranceStandardPremium.aspx")
    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        PanelHeader()
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        With oCustomClassSave
            .strConnection = GetConnectionString()
            .CardID = lblBranchIDD.Text.Trim
            .OtherCoverage = cboCovType.SelectedValue
            .CoverageRate = CDbl(txtrate.Text.Trim)
            .Command = Me.Command
        End With

        oCustomClassSave = oControllerSave.GetOtherCoverage(oCustomClassSave)

        If oCustomClassSave.output = "" Then
            If Me.Command = "A1" Then
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
            ElseIf Me.Command = "E1" Then
                ShowMessage(lblMessage, "Update data Berhasil", False)
            End If
            PanelHeader()
            BindGridEntity()
        Else
            ShowMessage(lblMessage, oCustomClassSave.output, True)
        End If
    End Sub

End Class