﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class WFInsuranceCompanyProduct
    Inherits Maxiloan.Webform.WebBased

#Region " Property "

    Private Property InsCoID() As String
        Get
            Return CStr(ViewState("InsCoID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoID") = Value
        End Set
    End Property

    Private Property InsCoName() As String
        Get
            Return CStr(ViewState("InsCoName"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoName") = Value
        End Set
    End Property

#End Region


#Region " SETTINGS "
    '#########################################################################
    '########### METHOD Settings
    '### - PANEL
    '### - DEFAULT PAGING

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private controller As New InsCoInsuranceProduct
    Private m_controller As New AssetDataController

#Region " PANEL SECTION "
    Private Sub PanelAllFalse()

        PnlProd.Visible = False
        PnlProductAdd.Visible = False
        lblMessage.Visible = False

    End Sub

#Region " PANEL ADD "
    '######################################################
    'S                                                   S#
    'From PANEL ADD
    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAdd.Click
        PanelAllFalse()
        PnlProd.Visible = True
    End Sub
    'E                                                   E#
    '######################################################


    '######################################################
    'S                                                   S#
    'From PANEL sAVE
    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAdd.Click
        SaveInsCoBranch() 'saved
        PanelAllFalse()
        lblMessage.Visible = True
        BindGridProd(Me.SearchBy, Me.SortBy)
        PnlProd.Visible = True
    End Sub
    'E                                                   E#
    '######################################################
#End Region

#End Region


    '                                                                      ###
    '                                                              ###########
    '#########################################################################

#End Region


#Region "******* LOAD PAGE CODE!!! *******"
    '#########################################################################
    '### Code page master with add and back include default code search method
    '### FIRST EXECUTE LOAD
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False

        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            InitialDefaultPanel()
            Me.InsCoID = Request.QueryString("InsCoID")
            Me.InsCoName = Request.QueryString("InsCoName")


            LblInsCoHOID.Text = Me.InsCoID.Trim
            LblInsCoHOName.Text = Me.InsCoName.Trim


            'Me.SearchBy = "MaskAssID=" + Me.InsCoID.Trim
            Me.SearchBy = "MaskAssID =  '" + Me.InsCoID.Trim + "'"
            Me.SortBy = "MaskAssID"

            BindGridProd(Me.SearchBy, Me.SortBy)
        End If
    End Sub



#Region " InitialDefaultPanel "
    '######################################################
    'S                                                   S#
    '    INIT PANEL
    '    SHOW & HIDE
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        PnlProd.Visible = True
        PnlProductAdd.Visible = False
        FillCbo(cboProduct, "dbo.tblinsuranceproduct")
    End Sub
    'E                                                   E#
    '######################################################
#End Region



#Region "Button Click EVENT"
    '######################################################
    'S                                                   S#

    'PAGE LOAD ADD
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        PanelAllFalse()
        PnlProductAdd.Visible = True
    End Sub

    'PAGE LOAD BACK
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("WFInsuranceCompanySetting.aspx")
    End Sub

    'E                                                   E#
    '######################################################
#End Region

    Sub FillCbo(ByVal cboName As DropDownList, ByVal Table As String)
        Dim oAssetData As New Parameter.AssetData
        Dim oData As New DataTable
        oAssetData.strConnection = GetConnectionString()
        oAssetData.Table = Table
        oAssetData = m_controller.GetCbo(oAssetData)
        oData = oAssetData.ListData
        cboName.DataSource = oData
        cboName.DataTextField = "Description"
        cboName.DataValueField = "ID"
        cboName.DataBind()
        cboName.Items.Insert(0, "Select One")
        cboName.Items(0).Value = "Select One"
    End Sub


#End Region


#Region " BindGridBranch "
    Sub BindGridProd(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable

        Dim oCustomClassInsCoProd As New Parameter.InsCoProduct

        With oCustomClassInsCoProd
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort.Trim
        End With

        oCustomClassInsCoProd = controller.GetInsCoProductList(oCustomClassInsCoProd)

        dtsEntity = oCustomClassInsCoProd.ListProduct
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        DtgInsCoProd.DataSource = dtvEntity

        Try
            DtgInsCoProd.DataBind()
            DtgInsCoProd.Visible = True
        Catch
            DtgInsCoProd.CurrentPageIndex = 0
            DtgInsCoProd.DataBind()
            DtgInsCoProd.Visible = True
        End Try

        PagingFooter()
    End Sub
#End Region

#Region "DtgInsCoProd_ItemCommand"

    Private Sub DtgInsCoProd_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgInsCoProd.ItemCommand

        'Me.FormID = "InsCoBranch"

        Select Case e.CommandName

            Case "Delete"

                lblMessage.Visible = False

                Dim customClass As New Parameter.InsCoProduct
                With customClass
                    .strConnection = GetConnectionString()
                    .MaskAssID = Me.InsCoID
                    .IDProd = e.Item.Cells(1).Text
                End With

                Try
                    Dim msg As String
                    msg = controller.InsCoProductDelete(customClass)
                    ShowMessage(lblMessage, msg, False)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    lblMessage.Visible = True

                Finally

                    Me.SearchBy = "MaskAssID= '" + Me.InsCoID.Trim + "' "
                    Me.SortBy = "MaskAssID"

                    BindGridProd(Me.SearchBy, Me.SortBy)
                End Try


        End Select

    End Sub

#End Region



#Region "SaveInsCoBranch"


    Private Sub SaveInsCoBranch()

        Dim msg As String = ""
        Dim customClass As New Parameter.InsCoProduct
        With customClass
            .strConnection = GetConnectionString()
            .MaskAssID = Me.InsCoID
            .IDProd = cboProduct.SelectedValue
            .ProdName = cboProduct.SelectedItem.Text
            .IsActive = 1 ' True
        End With

        Try
            msg = controller.InsCoProductSaveAdd(customClass)
            ShowMessage(lblMessage, msg, False)
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
            lblMessage.Visible = True

        Finally

            Me.SearchBy = "MaskAssID='" + Me.InsCoID.Trim + "'"
            Me.SortBy = "MaskAssID"

            BindGridProd(Me.SearchBy, Me.SortBy)
        End Try

    End Sub


#End Region





#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub



    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridProd(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridProd(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridProd(Me.SearchBy, Me.SortBy)
    End Sub

#End Region


End Class