﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WFInsuranceMasterRate.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.WFInsuranceMasterRate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WFInsuranceMasterRate</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function CopyPremiumRateConfirm() {
            if (document.forms[0].cboCopyFrom.value == '0') {
                alert("Harap Pilih Rate Card Asuransi Terlebih Dahulu")
                return false;
            }
            else {
                if (confirm("Apakah yakin mau copy rate dari Rate Asuransi ini ?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        function DeleteRateConfirm() {
            if (confirm("Apakah yakin mau hapus rate dari Rate Asuransi ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function SetFocus() {
            document.forms[0].txtSearchByH.focus();
        }
				
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlHeader" runat="server">
        <asp:Panel ID="pnlListH" runat="server" Width="100%">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        DAFTAR MASTER RATE ASURANSI
                    </h3>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            OnSortCommand="SortGrid" DataKeyField="InsuranceMasterHPPRateID" BorderStyle="None"
                            BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="RATE">
                                    <HeaderStyle Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbRate" runat="server" ImageUrl="../../../images/IconRate.gif"
                                            CausesValidation="False" CommandName="Rate"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <HeaderStyle Width="5%"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../../../images/IconEdit.gif"
                                            CausesValidation="False" CommandName="EditH"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Imagebutton2" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                            CommandName="DeleteH"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InsuranceMasterHPPRateID" HeaderText="ID">
                                    <HeaderStyle HorizontalAlign="Center" Height="25px"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblID" runat="server" Text='<%# Container.DataItem("InsuranceMasterHPPRateID") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="InsuranceMasterHPPRateDesc" HeaderText="NAMA MASTER RATE">
                                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesc" runat="server" Text='<%# Container.DataItem("InsuranceMasterHPPRateDesc") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn SortExpression="IsActive" HeaderText="AKTIF">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsActiveH" runat="server" Text='<%# Container.DataItem("IsActive") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="First" ImageUrl="../../../Images/grid_navbutton01.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Prev" ImageUrl="../../../Images/grid_navbutton02.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Next" ImageUrl="../../../Images/grid_navbutton03.png"></asp:ImageButton>
                        <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" OnCommand="NavigationLink_Click"
                            CommandName="Last" ImageUrl="../../../Images/grid_navbutton04.png"></asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGo" runat="server" MinimumValue="1" MaximumValue="999999999"
                            CssClass="validator_general"  Type="Integer" ControlToValidate="txtGoPage"
                            ErrorMessage="No Halaman Salah"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGo" runat="server"  CssClass="validator_general"  ControlToValidate="txtGoPage" ErrorMessage="No Halaman Salah"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlSearchH" runat="server" Width="100%">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        CARI MASTER RATE ASURANSI
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Cari Berdasarkan</label>
                    <asp:DropDownList ID="cboSearchByH" runat="server" onchange="Cases();">
                        <asp:ListItem Value="InsuranceMasterHPPRateID">ID</asp:ListItem>
                        <asp:ListItem Value="InsuranceMasterHPPRateDesc">Nama</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="txtSearchByH" runat="server" Width="213px"  MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddEditH" runat="server" Width="100%">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        MASTER RATE ASURANSI -&nbsp;
                        <asp:Label ID="lblAddEditH" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                     <label class ="label_req">
                        ID</label>
                    <asp:TextBox ID="txtIdH" runat="server" Width="213px"  MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvIDHeader" runat="server" ControlToValidate="txtIdH"
                        ErrorMessage="Harap isi ID Master Rate" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class ="label_req">
                        Nama Master Rate</label>
                    <asp:TextBox ID="txtDescH" runat="server" Width="100%"  MaxLength="100"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDescriptionHeader" runat="server" ControlToValidate="txtDescH"
                        ErrorMessage="Harap isi Nama master Rate" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Aktif</label>
                    <asp:CheckBox ID="chkIsActiveH" runat="server" Checked="True" Text="True"></asp:CheckBox>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSaveH" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnCancelH" runat="server" CausesValidation="False" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlDetail" runat="server">
        <asp:Panel ID="pnlSearchD" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DETAIL MASTER RATE ASURANSI
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        ID</label>
                    <asp:Label ID="lblID1" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Nama Master Rate Asuransi</label>
                    <asp:Label ID="lblDesc1" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Copy Rate Dari</label>
                    <asp:DropDownList ID="cboCopyFrom" runat="server">
                    </asp:DropDownList>
                    <asp:ImageButton ID="imbCopyD" runat="server" CausesValidation="False" ImageUrl="../../../Images/ButtonCopy.gif">
                    </asp:ImageButton>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR DETAIL MASTER RATE ASURANSI
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgPagingD" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                            CommandName="EditD"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                            CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="YearNum" HeaderText="TAHUN KE">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="InsuranceRate" HeaderText="RATE ASURANSI">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="SumInsured" HeaderText="PROSENTASE COVER">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>
<%--                                <asp:BoundColumn DataField="OJKSumInsuredFrom" HeaderText="PROSENTASE BAWAH OJK" DataFormatString="{0:#,##0}">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="OJKSumInsuredTo" HeaderText="PROSENTASE ATAS OJK" DataFormatString="{0:#,##0}">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>--%>
                                <asp:BoundColumn DataField="OJKSumInsuredFrom" HeaderText="PROSENTASE BAWAH OJK" DataFormatString="{0:#,##0.##}">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="OJKSumInsuredTo" HeaderText="PROSENTASE ATAS OJK" DataFormatString="{0:#,##0.##}">
                                    <ItemStyle HorizontalAlign="Right"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="InsuranceAreaName" HeaderText="WILAYAH PTG">
                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="RateBottom" Visible="false" />
                                <asp:BoundColumn DataField="RateTop" Visible="false" />
                                <asp:BoundColumn DataField="InsuranceAreaID" Visible="false" />
                                <asp:BoundColumn DataField="SeqNo" Visible="false" />
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAddD" runat="server" Text="Add" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnBackD" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                </asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddEditD" runat="server">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DETAIL MASTER RATE ASURANSI -&nbsp;
                        <asp:Label ID="lblAddEditD" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                     <label class ="label_req">
                        Tahun Ke</label>
                    <asp:TextBox ID="txtYearNum" runat="server"  MaxLength="3" Columns="5"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="txtYearNum"
                        ErrorMessage="Harap isi Tahun Ke" CssClass="validator_general" ></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtYearNum"
                        ErrorMessage="Harap isi dengan Angka" Display="Dynamic" ValidationExpression="\d*" CssClass="validator_general" ></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Rate Bottom</label>
                    <asp:TextBox ID="txtRateButtom" runat="server"  MaxLength="9" Columns="10" AutoPostBack="true" ></asp:TextBox>%
                    <asp:CompareValidator ID="CompareValidator6" runat="server" Type="Double" ControlToValidate="txtRateButtom"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Operator="LessThanEqual"
                        ValueToCompare="100"></asp:CompareValidator>&nbsp;
                    <asp:CompareValidator ID="Comparevalidator9" runat="server" Type="Double" ControlToValidate="txtRateButtom"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Display="Dynamic" Operator="GreaterThanEqual"
                        ValueToCompare="0"></asp:CompareValidator>&nbsp;
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Rate Top</label>
                    <asp:TextBox ID="txtRateTop" runat="server"  MaxLength="9" Columns="10"  AutoPostBack="true"></asp:TextBox>%
                    <asp:CompareValidator ID="CompareValidator10" runat="server" Type="Double" ControlToValidate="txtRateTop"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Operator="LessThanEqual"
                        ValueToCompare="100"></asp:CompareValidator>&nbsp;
                    <asp:CompareValidator ID="Comparevalidator11" runat="server" Type="Double" ControlToValidate="txtRateTop"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Display="Dynamic" Operator="GreaterThanEqual"
                        ValueToCompare="0"></asp:CompareValidator>&nbsp;
                </div>
            </div>

            <div class="form_box">
                <div class="form_single">
                    <label>
                        Rate Asuransi</label>
                    <asp:TextBox ID="txtInsuranceRate" runat="server"  MaxLength="9"
                        Columns="10"></asp:TextBox>%
                    <asp:CompareValidator ID="CompareValidator1" runat="server" Type="Double" ControlToValidate="txtInsuranceRate"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Display="Dynamic" Operator="LessThanEqual"
                        ValueToCompare="100"></asp:CompareValidator>&nbsp;
                    <asp:CompareValidator ID="Comparevalidator8" runat="server" Type="Double" ControlToValidate="txtInsuranceRate"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Display="Dynamic" Operator="GreaterThanEqual"
                        ValueToCompare="0"></asp:CompareValidator>&nbsp;
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        Prosentase Cover</label>
                    <asp:TextBox ID="txtSumIns" runat="server"  MaxLength="9" Columns="10"></asp:TextBox>%
                    <asp:CompareValidator ID="CompareValidator7" runat="server" Type="Double" ControlToValidate="txtSumIns"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Operator="LessThanEqual"
                        ValueToCompare="100"></asp:CompareValidator>&nbsp;
                    <asp:CompareValidator ID="Comparevalidator14" runat="server" Type="Double" ControlToValidate="txtSumIns"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Display="Dynamic" Operator="GreaterThanEqual"
                        ValueToCompare="0"></asp:CompareValidator>&nbsp;
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class ="label_req">
                        Batas Bawah OJK</label>
                    <asp:TextBox ID="txtOJKSumInsFrom" runat="server"  MaxLength="10" Columns="10"></asp:TextBox>
<%--                    <asp:CompareValidator ID="CompareValidator2" runat="server" Type="Double" ControlToValidate="txtOJKSumInsFrom"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Operator="LessThanEqual"
                        ValueToCompare="100"></asp:CompareValidator>&nbsp;
                    <asp:CompareValidator ID="Comparevalidator3" runat="server" Type="Double" ControlToValidate="txtOJKSumInsFrom"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 100" Display="Dynamic" Operator="GreaterThanEqual"
                        ValueToCompare="0"></asp:CompareValidator>&nbsp;--%>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" Type="Double" ControlToValidate="txtOJKSumInsFrom"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 9999999999" Operator="LessThanEqual"
                        ValueToCompare="9999999999"></asp:CompareValidator>&nbsp;
                    <asp:CompareValidator ID="Comparevalidator3" runat="server" Type="Double" ControlToValidate="txtOJKSumInsFrom"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 9999999999" Display="Dynamic" Operator="GreaterThanEqual"
                        ValueToCompare="0"></asp:CompareValidator>&nbsp;
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class ="label_req">
                        Batas Atas OJK</label>
                    <asp:TextBox ID="txtOJKSumInsTo" runat="server"  MaxLength="10" Columns="10"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" Type="Double" ControlToValidate="txtOJKSumInsTo"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 9999999999" Operator="LessThanEqual"
                        ValueToCompare="9999999999"></asp:CompareValidator>&nbsp;
                    <asp:CompareValidator ID="Comparevalidator5" runat="server" Type="Double" ControlToValidate="txtOJKSumInsTo"
                        ErrorMessage="Harap isi dengan Angka Antara  0 s/d 9999999999" Display="Dynamic" Operator="GreaterThanEqual"
                        ValueToCompare="0"></asp:CompareValidator>&nbsp;
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class="label_req">
                        Wilayah Pertanggungan Asuransi</label>
                    <asp:DropDownList runat="server" ID="cboWilayah">
                        <asp:ListItem Value="1" Text="Wilayah 1" />
                        <asp:ListItem Value="2" Text="Wilayah 2" />
                        <asp:ListItem Value="3" Text="Wilayah 3" Selected="True" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSaveD" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnCancelD" runat="server" Text="Cancel" CssClass="small button gray"
                    CausesValidation="False"></asp:Button>
            </div>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>

