﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WFInsuranceCompanyBranch.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.WFInsuranceCompanyBranch" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcBranch" Src="../../../webform.UserController/UcBranch.ascx" %>
<%@ Register TagPrefix="uc2" TagName="UcContactPerson" Src="../../../webform.UserController/UcContactPerson.ascx" %>
<%@ Register TagPrefix="uc2" TagName="BankAccount" Src="../../../webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc2" TagName="CompanyAdress" Src="../../../Webform.UserController/ucCompanyAddress.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WFInsuranceCompanyBranch</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
     <link href="../../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }

        function CopyRateConfirm() {
            if (document.forms[0].cboInsCoBranchCopy.value == '0') {
                alert("Harap pilih Cabang Perusahaan Asuransi terlebih dahulu")
                return false;
            }
            else {
                if (window.confirm("Apakah yakin mau hapus semua rate dan copy rate dari Cabang Asuransi dipilih ?"))
                    return true;
                else
                    return false;
            }
        }

        function fback() {
            history.go(-1);
            return false;
        }
        function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&style=INSURANCE&InsCoBranchID=' + pInsCoBranchID, 'InsuranceCo', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
              <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="PnlBranch" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            CABANG PERUSAHAAN ASURANSI
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Perusahaan Asuransi</label>
                        <asp:Label ID="LblInsCoHOID" runat="server" Width="504px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Perusahaan Asuransi</label>
                        <asp:Label ID="LblInsCoHOName" runat="server" Width="504px"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgInsCoBranch" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="RATE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbBranchRate" runat="server" ImageUrl="../../../Images/IconRate.gif"
                                                CommandName="Rate"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TPL">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbBranchTPL" runat="server" ImageUrl="../../../Images/IconRate.gif"
                                                CommandName="TPL"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LAIN">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbBranchCoverage" runat="server" ImageUrl="../../../Images/IconRate.gif"
                                                CommandName="COVERAGE"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbBranchEdit" runat="server" CommandName="Edit" ImageUrl="../../../Images/iconedit.gif">
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="DELETE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../../Images/icondelete.gif"
                                                CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="LOADING">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkLoading" runat="server" ImageUrl="../../../Images/IconRate.gif"
                                                CommandName="Loading"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="CP">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkCP" runat="server" ImageUrl="../../../Images/IconRate.gif"
                                                CommandName="CP"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="JK">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="lnkJK" runat="server" ImageUrl="../../../Images/IconRate.gif"
                                                CommandName="JK"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="MaskAssBranchID" HeaderText="ID CABANG ASR">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="hpInsCoBranchID" Text='<%# container.dataitem("MaskAssBranchID") %>'
                                                Visible="True" Enabled="True">
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MaskAssBranchName" SortExpression="MaskAssBranchName"
                                        HeaderText="NAMA CABANG ASR"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ContactPersonName" SortExpression="ContactPersonName"
                                        HeaderText="KONTAK"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="TELEPON"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BranchName" SortExpression="BranchName" HeaderText="CABANG">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="MaskAssID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="MaskAssBranchID" SortExpression="MaskAssBranchID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="BranchID" SortExpression="BranchID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="BankName" HeaderText="BANK NAME"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="AKTIF">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="LblStateBranch" runat="server" Text='<%# container.dataitem("IsActive") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblBranchID" runat="server" Visible="false" Text='<%# container.dataitem("BranchID") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblMaskAssBranchName" runat="server" Visible="false" Text='<%# container.dataitem("MaskAssBranchName") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblBranchName" runat="server" Visible="false" Text='<%# container.dataitem("BranchName") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblBankName" runat="server" Visible="false" Text='<%# container.dataitem("BankName") %>'>
                                            </asp:Label>
                                            <asp:Label ID="lblMaskAssID" runat="server" Visible="false" Text='<%# container.dataitem("MaskAssID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" ControlToValidate="txtGopage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" Type="Integer" MaximumValue="999999999"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                                ControlToValidate="txtGopage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small
        button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlBranchAdd" runat="server">
                <asp:Label ID="Label4" runat="server" ForeColor="Red"></asp:Label>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            CABANG PERUSAHAAN ASURANSI -&nbsp;
                            <asp:Label ID="lblAddEditBranch" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Perusahaan Asuransi</label>
                        <asp:Label ID="lblInsCoIDAdd" runat="server" Width="544px"></asp:Label>
                        <asp:HyperLink ID="HpInsCoIDAdd" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Perusahaan Asuransi</label>
                        <asp:Label ID="lblInsCoNameAdd" runat="server" Width="544px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Cabang</label>
                        <uc1:ucbranch id="UcBranchIDAdd" runat="server">
                        </uc1:ucbranch>
                        <asp:Label ID="lblBranchNameAdd" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Cabang Perusahaan Asuransi</label>
                        <asp:TextBox ID="TxtInsCoBranchIDAdd" runat="server" Width="100px" MaxLength="10"></asp:TextBox>
                        <div style="width: 16px; display: inline; height: 15px; color: red" ms_positioning="FlowLayout">
                        </div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Width="280px"
                            ControlToValidate="TxtInsCoBranchIDAdd" ErrorMessage="Harap isi ID Cabang Perusahaan Asuransi"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Cabang Perusahaan Asuransi</label>
                        <asp:TextBox ID="TxtInsCoBranchNameAdd" runat="server" Width="536px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Width="280px"
                            ControlToValidate="TxtInsCoBranchNameAdd" ErrorMessage="Harap isi Nama Cabang Perusahaan Asuransi"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            ALAMAT
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <uc2:companyadress id="UcCompanyAddressAdd" runat="server"></uc2:companyadress>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            KONTAK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <uc2:uccontactperson id="UcContactPersonAdd" runat="server"></uc2:uccontactperson>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            REKENING BANK
                        </h4>
                    </div>
                </div>
                <div class="form_box_uc">
                    <uc2:bankaccount id="UcBankBranchAccountAdd" runat="server"></uc2:bankaccount>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Termin Pembayaran</label>
                        <asp:TextBox ID="TxtTOPAdd" runat="server" Width="56px" MaxLength="4">0</asp:TextBox>&nbsp;days
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="TxtTOPAdd"
                            ErrorMessage="Harap isi Termin Pembayaran" ValidationExpression="[0-9]{0,4}"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Biaya Admin</label>
                        <%-- <asp:TextBox ID="TxtAdminFeeAdd" runat="server" MaxLength="15">0</asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Width="320px"
                            ControlToValidate="TxtAdminFeeAdd" ErrorMessage="Harap isi dengan Angka" ValidationExpression="[0-9]{0,12}"></asp:RegularExpressionValidator>--%>
                        <uc1:ucnumberformat id="TxtAdminFeeAdd" runat="server" />
                        </uc1:ucNumberFormat>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Biaya Materai</label>
                        <%-- <asp:TextBox ID="TxtSDFAdd" runat="server" MaxLength="15">0</asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TxtSDFAdd"
                            ErrorMessage="Harap isi Biaya Materai dengan Angka" ValidationExpression="[0-9]{0,12}"></asp:RegularExpressionValidator>--%>
                        <uc1:ucnumberformat id="TxtSDFAdd" runat="server" />
                        </uc1:ucNumberFormat>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Biaya Polis Pembiayaan Protection</label>
                        <%-- <asp:TextBox ID="TxtFeeCp" runat="server" MaxLength="15">0</asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TxtFeeCp"
                            ErrorMessage="Harap isi Biaya Materai dengan Angka" ValidationExpression="[0-9]{0,12}"></asp:RegularExpressionValidator>--%>
                        <uc1:ucnumberformat id="TxtFeeCp" runat="server" />
                        </uc1:ucNumberFormat>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Biaya Polis Jaminan Pembiayaan</label>
                        <%-- <asp:TextBox ID="TxtFeeJk" runat="server" MaxLength="15">0</asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TxtFeeJk"
                            ErrorMessage="Harap isi Biaya Materai dengan Angka" ValidationExpression="[0-9]{0,12}"></asp:RegularExpressionValidator>--%>
                        <uc1:ucnumberformat id="TxtFeeJk" runat="server" />
                        </uc1:ucNumberFormat>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Diskon (%)</label>
                        <uc1:ucnumberformat id="txtDist" runat="server" />
                        </uc1:ucNumberFormat>%
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Komisi (%)</label>
                        <uc1:ucnumberformat id="txtKomisi" runat="server" />
                        </uc1:ucNumberFormat>%
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Imbalan Jasa (%)</label>
                        <uc1:ucnumberformat id="txtFee" runat="server" />
                        </uc1:ucNumberFormat>%
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            PPh 23 (%)</label>
                        <uc1:ucnumberformat id="txtPPh23" runat="server" />
                        </uc1:ucNumberFormat>
                        <asp:CheckBox Text="Ditanggung Asuransi" runat="server" ID="chkPPH23" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            PPN (%)</label>
                        <uc1:ucnumberformat id="txtPPN" runat="server" />
                        </uc1:ucNumberFormat>
                        <asp:CheckBox Text="Ditanggung Asuransi" runat="server" ID="chkPPN" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Biaya Opex (%)</label>
                        <uc1:ucnumberformat id="txtBiayaOpex" runat="server" />
                        </uc1:ucNumberFormat>%
                    </div>
                </div>
                    <div class="form_box">
                    <div class="form_single">
                        <label>
                            NoClaimBonus (%)</label>
                        <uc1:ucnumberformat id="txtNoClaimBonus" runat="server" />
                        </uc1:ucNumberFormat>%
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <h5>
                            Usia Asset
                        </h5>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Maksimum Usia Komprehensif</label>
                        <asp:TextBox ID="txtLoadingFeeAllRisk" runat="server" Width="56px" MaxLength="2">0</asp:TextBox>&nbsp;Thn
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtLoadingFeeAllRisk"
                            ErrorMessage="Harap isi dengan Angka" ValidationExpression="[0-9]{0,12}"></asp:RegularExpressionValidator>
                        <asp:DropDownList runat="server" ID="cboAllRiskOptions">
                            <asp:ListItem Value="1" Text="Termasuk Tenor" />
                            <asp:ListItem Value="2" Text="Saat Penutupan" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Maksimum Usia TLO</label>
                        <asp:TextBox ID="txtLoadingFeeTLO" runat="server" Width="56px" MaxLength="2">0</asp:TextBox>&nbsp;Thn
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtLoadingFeeTLO"
                            ErrorMessage="Harap isi dengan Angka" ValidationExpression="[0-9]{0,12}"></asp:RegularExpressionValidator>
                        <asp:DropDownList runat="server" ID="cboTLOOptions">
                            <asp:ListItem Value="1" Text="Termasuk Tenor" />
                            <asp:ListItem Value="2" Text="Saat Penutupan" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSaveAdd" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancelAdd" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlRate" runat="server" Width="100%">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            RATE ASURANSI CABANG
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Perusahaan Asuransi</label>
                        <asp:Label ID="LblInsCoNameRate" runat="server" Width="512px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Cabang Asuransi</label>
                        <asp:HyperLink ID="hpInsCoBranchIDRate" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Cabang Asuransi</label>
                        <asp:Label ID="LblInsCoBranchNameRate" runat="server" Width="512px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label ID="LblTFSBranchRate" runat="server" Width="512px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                            <asp:ListItem Value="irc.description" Selected="True">Insurance Asset Type</asp:ListItem>
                            <asp:ListItem Value="au.Description">Asset Ussage</asp:ListItem>
                            <asp:ListItem Value="n.NewUsed">NewUsed</asp:ListItem>
                            <asp:ListItem Value="cov.Description">Coverage Type</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Copy Rate Dari Cabang</label>
                        <asp:DropDownList ID="cboInsCoBranchCopy" runat="server">
                        </asp:DropDownList>
                        &nbsp;
                        <%--<asp:ImageButton ID="ImgCopyButton" runat="server" CausesValidation="False" ImageUrl="../../../images/buttoncopy.gif">
                        </asp:ImageButton>--%>
                        <asp:Button runat="server" ID="btnCopy" CausesValidation="false" CssClass="small buttongo blue"
                            Text="Copy" />
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray">
                    </asp:Button>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            RATE CABANG ASURANSI
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgRate" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="SortingRate" DataKeyField="CoverageType" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgEdit" runat="server" CommandName="Edit" ImageUrl="../../../Images/iconedit.gif">
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imbRateDelete" runat="server" ImageUrl="../../../Images/icondelete.gif"
                                                CommandName="Delete" Enabled="True" CausesValidation="False"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="InsuranceType" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="InsuranceTypeDesc" SortExpression="InsuranceTypeDesc"
                                        HeaderText="INS.ASSET TYPE"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="AssetUssage" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="AssetUssageDesc" SortExpression="AssetUssageDesc" HeaderText="ASSET USSAGE">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NewUsed" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="NewUsedDesc" SortExpression="NewUsedDesc" HeaderText="NEW/USED">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CoverageType" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CoverageTypeDesc" SortExpression="CoverageTypeDesc" HeaderText="COVERAGE TYPE">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="InsuranceMasterHPPRateID" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="InsuranceMasterHPPRateDesc" SortExpression="InsuranceMasterHPPRateDesc"
                                        HeaderText="RATE CARD TYPE"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="SumInsuredFrom" SortExpression="SumInsuredFrom" HeaderText="SUM INSURED FROM"
                                        DataFormatString="{0:N2}" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="SumInsuredTo" SortExpression="SumInsuredTo" HeaderText="SUM INSURED TO"
                                        DataFormatString="{0:N2}" HeaderStyle-CssClass="th_right" ItemStyle-CssClass="item_grid_right">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbRateFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLinkRate_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbRatePrevPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLinkRate_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbRateNextPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLinkRate_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbRateLastPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLinkRate_Click"></asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtRateGoPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="BtnRateGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvRateGo" runat="server" CssClass="validator_general" ControlToValidate="txtRateGoPage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" Type="Integer" MaximumValue="999999999"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvRateGo" runat="server" CssClass="validator_general"
                                ControlToValidate="txtRateGoPage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblRatePage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblRateTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblRateTotRec" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnRateAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnRateBack" runat="server" CausesValidation="False" Text="Back"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlRateAdd" runat="server">
                <asp:Label ID="Label3" runat="server" ForeColor="Red"></asp:Label>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            RATE CABANG PERUSAHAAN ASURANSI -&nbsp;
                            <asp:Label ID="lblAddEditRate" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Cabang Asuransi</label>
                        <asp:Label ID="LblInsCoBranchIDRateAdd" runat="server" Width="544px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Cabang Asuransi</label>
                        <asp:Label ID="LblInsCoBranchNameRateAdd" runat="server" Width="544px"></asp:Label>
                        </td>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label ID="lblTFSBranchRateAdd" runat="server" Width="544px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Asset Asuransi</label>
                        <asp:DropDownList ID="CboInsAssetTypeAdd" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Penggunaan Asset</label>
                        <asp:DropDownList ID="CboAssetUssageAdd" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Barus / Bekas</label>
                        <asp:DropDownList ID="CboNewUsedAdd" runat="server">
                            <asp:ListItem Value="N" Selected="True">New</asp:ListItem>
                            <asp:ListItem Value="U">Used</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Cover</label>
                        <asp:DropDownList ID="CboCoverageTypeAdd" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Rate Card</label>
                        <asp:DropDownList ID="cboRateCardType" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nilai Pertanggungan Dari</label>
                        <%--<asp:TextBox ID="txtSumInsuredFrom" runat="server">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                            ControlToValidate="txtSumInsuredFrom" ErrorMessage="Harap isi Nilai Pertanggungan Dari"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtSumInsuredFrom"
                            ErrorMessage="Harap isi dengan Angka" Type="Double" MaximumValue="999999999999"
                            MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>--%>
                        <uc1:ucnumberformat id="txtSumInsuredFrom" runat="server" />
                        </uc1:ucNumberFormat>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nilai Pertanggungan Sampai</label>
                        <%--<asp:TextBox ID="txtSumInsuredTo" runat="server">0</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            ControlToValidate="txtSumInsuredTo" ErrorMessage="Harap isi Nilai Pertanggungan Sampai"
                            CssClass="validator_general"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtSumInsuredTo"
                            ErrorMessage="Harap isi dengan Angka" Type="Double" MaximumValue="999999999999"
                            MinimumValue="0" CssClass="validator_general"></asp:RangeValidator>--%>
                        <uc1:ucnumberformat id="ucSumInsuredTo" runat="server" />
                        </uc1:ucNumberFormat>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSaveRate" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancelRate" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlTPL" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            TPL (THIRD PARTY LIABILITIES)
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asuransi</label>
                        <asp:Label ID="LblInsCoNameTPL" runat="server" Width="496px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Cabang Asuransi</label>
                        <asp:Label ID="LblInsCoBranchIDTPL" runat="server" Width="496px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Cabang Asuransi</label>
                        <asp:Label ID="LblInsCoBranchNameTPL" runat="server" Width="496px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label ID="lblTFSBranchTPL" runat="server" Width="496px"></asp:Label>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            TPL (THIRD PARTY LIABILITIES)</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgTPL" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="SortingTPL" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgBranchEdit" runat="server" ImageUrl="../../../Images/iconedit.gif"
                                                CommandName="Edit"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbTPLDelete" runat="server" ImageUrl="../../../Images/icondelete.gif"
                                                CausesValidation="False" CommandName="Delete" Enabled="True"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="tplAmountFrom" SortExpression="tplAmountFrom" HeaderText="TPL AMOUNT FROM"
                                        DataFormatString="{0:N2}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tplAmountTo" SortExpression="tplAmountTo" HeaderText="TPL AMOUNT TO"
                                        DataFormatString="{0:N2}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tplRate" SortExpression="tplRate" HeaderText="TPL RATE"
                                        DataFormatString="{0:N2}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="InsuranceRateCategoryName" SortExpression="InsuranceRateCategoryName"
                                        HeaderText="INS RATE CATEGORY"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="InsuranceRateCategory" SortExpression="InsuranceRateCategory"
                                        Visible="false"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbTPLFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLinkTPL_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbTPLPrevPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLinkTPL_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbTPLNextPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLinkTPL_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbTPLLastPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLinkTPL_Click"></asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtTPLGoPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="BtnTPLGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvTPLGo" runat="server" CssClass="validator_general" ControlToValidate="txtTPLGoPage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" Type="Integer" MaximumValue="999999999"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvTPLGo" runat="server" CssClass="validator_general"
                                ControlToValidate="txtTPLGoPage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblTPLPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTPLTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblTPLTotRec" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnAddTPL" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnBackTPL" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlTPLAdd" runat="server">
                <asp:Label ID="Label5" runat="server" ForeColor="Red"></asp:Label>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            TPL -
                            <asp:Label ID="lblAddEditTPL" runat="server"></asp:Label>
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Perusahaan Asuransi</label>
                        <asp:Label ID="LblInsCoIdTPLAdd" runat="server" Width="544px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Cabang Asuransi</label>
                        <asp:Label ID="LblInsCOBranchIdTPLAdd" runat="server" Width="544px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Cabang Asuransi</label>
                        <asp:Label ID="LblInsCoBranchNameTPLAdd" runat="server" Width="544px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label ID="LblTFSBranchTPLAdd" runat="server" Width="544px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            TPL Amount From</label>
                        <uc1:ucNumberFormat ID="TxtTPLAmountFromAdd" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            TPL Amount To</label>
                        <uc1:ucNumberFormat ID="TxtTPLAmountToAdd" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rate</label>
                        <uc1:ucNumberFormat ID="txtTPLRate" runat="server" />

                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Jenis Asset Asuransi</label>
                        <asp:DropDownList ID="cboTPLJenisAssetAsuransi" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnTPLSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnTPLCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCoverage" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            RATE COVER LAIN
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asuransi</label>
                        <asp:Label ID="lblCoverageInsCo" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Cabang Asuransi</label>
                        <asp:Label ID="lblCoverageInsCoBranchID" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Cabang Asuransi</label>
                        <asp:Label ID="lblCoverageInsCoBranchName" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang</label>
                        <asp:Label ID="lblCoverageTFSBranch" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR RATE COVER LAIN
                        </h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgCoverage" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="SortingTPL" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn Visible="False">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text='<%# container.dataitem("ID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="COVER">
                                        <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Height="20px"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCoverage" runat="server" Text='<%# container.dataitem("Coverage") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="COVERAGE TYPE">
                                        <HeaderStyle CssClass="th_center" />
                                        <ItemStyle CssClass="th_center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCoverageType" runat="server" Text='<%# container.dataitem("CoverageTypeName") %>' />
                                            <asp:Label ID="lblCoverageTypeId" runat="server" Text='<%# container.dataitem("CoverageType") %>'
                                                Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="WILAYAH PTG">
                                        <HeaderStyle CssClass="th_center" />
                                        <ItemStyle CssClass="th_center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblWilayah" runat="server" Text='<%# container.dataitem("InsuranceAreaName") %>' />
                                            <asp:Label ID="lblWilayahId" runat="server" Text='<%# container.dataitem("InsuranceAreaId") %>'
                                                Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="RATE">
                                        <ItemStyle CssClass="th_right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="rfvCoverageRate" runat="server" ControlToValidate="txtCoverageRate"
                                                ErrorMessage="Harap isi Rate Cover" CssClass="validator_general"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="rvCoverageRate" runat="server" ControlToValidate="txtCoverageRate"
                                                ErrorMessage="Harap isi dengan Angka" MinimumValue="0" MaximumValue="999999999999"
                                                Display="Dynamic" Type="Double" CssClass="validator_general"></asp:RangeValidator>
                                            <asp:Label ID="lblCoverageRate" runat="server" Text='<%# container.dataitem("CoverageRate") %>'>
                                            </asp:Label>
                                            <asp:TextBox ID="txtCoverageRate" runat="server" Text='<%# container.dataitem("CoverageRate") %>'>
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="RATE BOTTOM">
                                        <HeaderStyle CssClass="th-center"></HeaderStyle>
                                        <ItemStyle CssClass="th-right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="rfvRateBottom" runat="server" ControlToValidate="txtRateBottom"
                                                ErrorMessage="Harap isi Rate Bottom" CssClass="validator_general"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="rvRateBottom" runat="server" ControlToValidate="txtRateBottom"
                                                ErrorMessage="Harap isi dengan Angka" MinimumValue="0" MaximumValue="999999999999"
                                                Display="Dynamic" Type="Double" CssClass="validator_general"></asp:RangeValidator>
                                            <asp:Label ID="lblRateBottom" runat="server" Text='<%# FormatNumber(container.dataitem("RateBottom"),0) %>'>
                                            </asp:Label>
                                            <asp:TextBox ID="txtRateBottom" runat="server" Text='<%# container.dataitem("RateBottom") %>'>
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="RATE TOP">
                                        <HeaderStyle CssClass="th-center"></HeaderStyle>
                                        <ItemStyle CssClass="th-right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="rfvRateTop" runat="server" ControlToValidate="txtRateTop"
                                                ErrorMessage="Harap isi Rate Top" CssClass="validator_general"></asp:RequiredFieldValidator>
                                            <asp:RangeValidator ID="rvRateTop" runat="server" ControlToValidate="txtRateTop"
                                                ErrorMessage="Harap isi dengan Angka" MinimumValue="0" MaximumValue="999999999999"
                                                Display="Dynamic" Type="Double" CssClass="validator_general"></asp:RangeValidator>
                                            <asp:Label ID="lblRateTop" runat="server" Text='<%# FormatNumber(container.dataitem("RateTop"),0) %>'>
                                            </asp:Label>
                                            <asp:TextBox ID="txtRateTop" runat="server" Text='<%# container.dataitem("RateTop") %>'>
                                            </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="DELETE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Imagebutton2" runat="server" ImageUrl="../../../Images/icondelete.gif"
                                                CausesValidation="False" CommandName="Delete" Enabled="True"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnCoverageSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCoverageCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                    <asp:Button ID="btnCoverageEdit" runat="server" Text="Edit" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCoverageBack" runat="server" Text="Back" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlBranchEdit" runat="server">
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            EDIT - CABANG ASURANSI
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            ID Asuransi</label>
                        <asp:Label ID="LblInsCoHOIDEdit" runat="server" Width="504px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Nama Asuransi</label>
                        <asp:Label ID="LblInsCoHONameEdit" runat="server" Width="504px"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgInsCoBranchEdit" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn Visible="False" HeaderText="BranchID">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBranchID" runat="server" Text='<%# container.dataitem("BranchID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="MaskAssBranchID" HeaderText="CABANG PERS ASR">
                                        <ItemTemplate>
                                            <asp:Label ID="HpInsCoyID" runat="server" Visible="True" Enabled="True" Text='<%# container.dataitem("MaskAssBranchID") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="MaskAssBranchName" SortExpression="MaskAssBranchName"
                                        HeaderText="NAMA CABANG PERS ASR"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="ContactPersonName" SortExpression="ContactPersonName"
                                        HeaderText="KONTAK"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="TELEPON"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="BranchName" SortExpression="BranchName" HeaderText="BRANCH">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="AKTIF">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckSelect" runat="server" Checked='<%# container.dataitem("IsActive") %>'>
                                            </asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn Visible="False" DataField="MaskAssID"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="MaskAssBranchID" SortExpression="MaskAssBranchID">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="BankName" HeaderText="BANK NAME"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnSaveEdit" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnBackEdit" runat="server" Text="Back" CssClass="small button gray">
                    </asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
