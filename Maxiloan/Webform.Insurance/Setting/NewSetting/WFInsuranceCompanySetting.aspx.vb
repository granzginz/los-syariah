﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class WFInsuranceCompanySetting
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents UcCompanyAddress As UcCompanyAddress
    Protected WithEvents UcContactPerson As UcContactPerson
    Protected WithEvents oTrans As ucLookUpTransaction

#Region " Private Const "
    Dim m_InsCo As New cInsuranceCompany

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private oCustomClassInsCo As New Parameter.GeneralPaging
    Private oControllerInsCo As New GeneralPagingController
    Private oCustomClassInsCoByID As New Parameter.GeneralPaging
    Private oControllerInsCoByID As New GeneralPagingController

#End Region

#Region " Property "
    Private Property InsCoID() As String
        Get
            Return CStr(ViewState("InsCoID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoID") = Value
        End Set
    End Property

    Private Property InsCoName() As String
        Get
            Return CStr(ViewState("InsCoName"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoName") = Value
        End Set
    End Property

    Private Property InsCoBranchID() As String
        Get
            Return CStr(ViewState("InsCoBranchID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchID") = Value
        End Set
    End Property

    Private Property Command() As String
        Get
            Return CStr(ViewState("Command"))
        End Get
        Set(ByVal Value As String)
            ViewState("Command") = Value
        End Set
    End Property

#End Region

#Region "Bind"
    Private Sub Bind()
        If Me.IsHoBranch Then
            With oTrans
                .ProcessID = "INSAP"
                .IsAgreement = "1"
                .IsPettyCash = "0"
                .IsHOTransaction = "1"
                .IsPaymentReceive = "0"
                .Style = "AccMnt"
                .BindData()
            End With
        Else
            With oTrans
                .ProcessID = "INSAP"
                .IsAgreement = "1"
                .IsPettyCash = "0"
                .IsHOTransaction = "0"
                .IsPaymentReceive = "0"
                .Style = "AccMnt"
                .BindData()
            End With
        End If
    End Sub
    Sub BindDisableAmount()
        oTrans.VisibleAmount = False
        oTrans.VisibleDescription = False
        oTrans.BindData()
        ' oTrans.Transaction = ""
    End Sub
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If
        If Not Me.IsPostBack Then
            Me.FormID = "NEWINSCOMPANY"
            InitialDefaultPanel()
            'createxmdlist()
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                Me.SearchBy = ""
                Me.SortBy = ""
                'Tentukan panjang property
                txtInsCoIDAdd.MaxLength = 10
                If Request("cond") <> "" Then
                    Me.SearchBy = Request("cond")
                Else
                    Me.SearchBy = "ALL"
                End If
                BindDisableAmount()
                Bind()

                Me.SortBy = ""
                BindGridEntity(Me.SearchBy, Me.SortBy)

            End If
        End If
    End Sub

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()
        pnlList.Visible = False
        pnlAdd.Visible = False
        lblMessage.Visible = False
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click

        If txtGoPage.Text = "" Then
            txtGoPage.Text = "0"
        Else
            If IsNumeric(txtGoPage.Text) Then
                If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                    currentPage = CType(txtGoPage.Text, Int32)
                    BindGridEntity(Me.SearchBy, Me.SortBy)
                End If
            End If
        End If


    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridEntity(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        pnlList.Visible = True
    End Sub
#End Region
#Region " BindGridEntity "
    Sub BindGridEntity(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable

        With oCustomClassInsCo
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort.Trim
            .SpName = "spInsCoList"
        End With
        oCustomClassInsCo = oControllerInsCo.GetGeneralPaging(oCustomClassInsCo)


        With oCustomClassInsCo
            lblTotRec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oCustomClassInsCo.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        dtgInsCo.DataSource = dtvEntity
        Try
            dtgInsCo.DataBind()
        Catch
            dtgInsCo.CurrentPageIndex = 0
            dtgInsCo.DataBind()
        End Try

        PagingFooter()
    End Sub
#End Region
#Region "ClearAddForm"
    Private Sub ClearAddForm()

        txtInsCoIDAdd.Enabled = True
        txtInsCoIDAdd.Text = ""
        txtInsCoNameAdd.Text = ""

        oTrans.Transaction = ""

        With UcCompanyAddress
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Style = "Insurance"
            .BindAddress()
        End With

        With UcContactPerson
            .ContactPerson = ""
            .ContactPersonTitle = ""
            .MobilePhone = ""
            .Email = ""
            .BindContacPerson()
        End With

    End Sub
#End Region
#Region "BtnAdd_Click"
    Private Sub BtnAddAsr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAsr.Click
        If CheckFeature(Me.Loginid, Me.FormID, "Add", Me.AppId) Then
            If SessionInvalid() Then
                Exit Sub
            End If
            chkRekanan.Checked = False
            lblMessage.Visible = False
            ClearAddForm()
            PanelAllFalse()
            pnlAdd.Visible = True
            lblEditAdd.Text = "ADD"
            Me.Command = "A"
            ' BindDisableAmount()
        End If
    End Sub
#End Region
#Region "imbSaveAdd_Click"

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAdd.Click
        Dim customClass As New Parameter.eInsuranceCompany
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal

        With customClass
            .strConnection = GetConnectionString()
            .InsCoID = txtInsCoIDAdd.Text
            .InsCoName = txtInsCoNameAdd.Text
            .DtmUpd = Now()
            .Command = Me.Command.Trim

            ' .PolisFeeCp = 

            .IsUtama = chkRekanan.Checked
            .AccountAP = oTrans.TransactionID
        End With

        With oClassAddress
            .Address = UcCompanyAddress.Address
            .RT = UcCompanyAddress.RT
            .RW = UcCompanyAddress.RW
            .Kelurahan = UcCompanyAddress.Kelurahan
            .Kecamatan = UcCompanyAddress.Kecamatan
            .City = UcCompanyAddress.City
            .ZipCode = UcCompanyAddress.ZipCode
            .AreaPhone1 = UcCompanyAddress.AreaPhone1.Trim
            .Phone1 = UcCompanyAddress.Phone1.Trim
            .AreaPhone2 = UcCompanyAddress.AreaPhone2.Trim
            .Phone2 = UcCompanyAddress.Phone2.Trim
            .AreaFax = UcCompanyAddress.AreaFax.Trim
            .Fax = UcCompanyAddress.Fax.Trim
        End With

        With oClassPersonal
            .PersonName = UcContactPerson.ContactPerson
            .PersonTitle = UcContactPerson.ContactPersonTitle
            .MobilePhone = UcContactPerson.MobilePhone
            .Email = UcContactPerson.Email
        End With

        customClass = m_InsCo.InsCoSave(customClass, oClassAddress, oClassPersonal)
        If customClass.output = "" Then
            If Me.Command = "A" Then

                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
                InitialDefaultPanel()
            Else

                ShowMessage(lblMessage, "Update data Berhasil", False)
                InitialDefaultPanel()
            End If
        Else
            ShowMessage(lblMessage, customClass.output, True)
            lblMessage.Visible = True
            Exit Sub
        End If

        Me.SearchBy = "all"
        Me.SortBy = ""
        BindGridEntity(Me.SearchBy, Me.SortBy)

        PanelAllFalse()

        pnlList.Visible = True
        lblMessage.Visible = True
    End Sub
#End Region

    Private Sub BtnCariAsr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCariAsr.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.SearchBy = "all"
            Me.SortBy = ""
        Else
            Me.SearchBy = StrSearchBy + " like '%" + StrSearchByValue + "%'"
        End If
        PanelAllFalse()
        BindGridEntity(Me.SearchBy, Me.SortBy)
        pnlList.Visible = True
    End Sub

    Private Sub viewbyID(ByVal inscoid As String)
        Dim customClass As New Parameter.eInsuranceCompany
        Dim dtInsCo As New DataTable

        Try
            With oCustomClassInsCoByID
                .strConnection = GetConnectionString()
                .WhereCond = "MaskAssID = '" & inscoid.Trim & "'"
                .CurrentPage = currentPage
                .PageSize = pageSize
                .SortBy = ""
                .SpName = "spInsCoListEdit"
            End With
            oCustomClassInsCoByID = oControllerInsCoByID.GetGeneralPaging(oCustomClassInsCoByID)

            dtInsCo = oCustomClassInsCoByID.ListData

            'Isi semua field Edit Action
            txtInsCoIDAdd.Text = inscoid
            txtInsCoIDAdd.Enabled = False
            txtInsCoNameAdd.Text = CStr(dtInsCo.Rows(0).Item("InsuranceComDescription"))

            chkRekanan.Checked = dtInsCo.Rows(0).Item("IsUtama")

            With oTrans
                .TransactionID = dtInsCo.Rows(0).Item("AccountAP").ToString
                .Transaction = dtInsCo.Rows(0).Item("DescAccountAP").ToString
            End With

            With (UcCompanyAddress)
                .Address = CStr(dtInsCo.Rows(0).Item("Address")).Trim
                .RT = CStr(dtInsCo.Rows(0).Item("RT")).Trim
                .RW = CStr(dtInsCo.Rows(0).Item("RW")).Trim
                .Kelurahan = CStr(dtInsCo.Rows(0).Item("Kelurahan")).Trim
                .Kecamatan = CStr(dtInsCo.Rows(0).Item("Kecamatan")).Trim
                .City = CStr(dtInsCo.Rows(0).Item("City")).Trim
                .ZipCode = CStr(dtInsCo.Rows(0).Item("ZipCode")).Trim
                .AreaPhone1 = CStr(dtInsCo.Rows(0).Item("AreaPhone1")).Trim
                .Phone1 = CStr(dtInsCo.Rows(0).Item("Phone1")).Trim
                .AreaPhone2 = CStr(dtInsCo.Rows(0).Item("AreaPhone2")).Trim
                .Phone2 = CStr(dtInsCo.Rows(0).Item("Phone2")).Trim
                .AreaFax = CStr(dtInsCo.Rows(0).Item("AreaFax")).Trim
                .Fax = CStr(dtInsCo.Rows(0).Item("Fax")).Trim
                .Style = "Insurance"
                .BindAddress()
            End With

            With UcContactPerson
                .ContactPerson = CStr(dtInsCo.Rows(0).Item("ContactPersonName")).Trim
                .ContactPersonTitle = CStr(dtInsCo.Rows(0).Item("ContactPersonTitle")).Trim
                .MobilePhone = CStr(dtInsCo.Rows(0).Item("MobilePhone")).Trim
                .Email = CStr(dtInsCo.Rows(0).Item("Email")).Trim
                .BindContacPerson()
            End With

        Catch ex As Exception

        End Try

    End Sub

    Private Sub dtgInsCo_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgInsCo.ItemCommand
        Select Case e.CommandName
            Case "Edit"

                'Me.InsCoID = e.Item.Cells(3).Text
                Me.InsCoID = e.Item.Cells(4).Text
                pnlList.Visible = False
                lblMessage.Visible = False
                pnlAdd.Visible = True
                lblEditAdd.Text = "EDIT"
                Me.Command = "E"
                ' BindDisableAmount()
                viewbyID(Me.InsCoID)

            Case "Delete"
                'Me.InsCoID = e.Item.Cells(3).Text
                Me.InsCoID = e.Item.Cells(4).Text
                lblMessage.Visible = False
                Dim customClass As New Parameter.eInsuranceCompany
                With customClass
                    .strConnection = GetConnectionString()
                    .InsCoID = Me.InsCoID
                End With

                Try
                    m_InsCo.InsCoDelete(customClass)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    lblMessage.Visible = True

                Finally
                    BindGridEntity("ALL", "")
                End Try

            Case "ViewBranch"
                'Me.InsCoID = e.Item.Cells(3).Text
                Me.InsCoID = e.Item.Cells(4).Text
                'Me.InsCoName = e.Item.Cells(4).Text
                Me.InsCoName = e.Item.Cells(5).Text
                Response.Redirect("WFInsuranceCompanyBranch.aspx?InsCoID=" & Me.InsCoID & "&InsCoName=" & Me.InsCoName)

            Case "TC"
                'Me.InsCoID = e.Item.Cells(3).Text
                Me.InsCoID = e.Item.Cells(4).Text
                'Me.InsCoName = e.Item.Cells(4).Text
                Me.InsCoName = e.Item.Cells(5).Text
                Response.Redirect("../../Webform.Insurance/Setting/WFInsCoAllocationTC.aspx?InsCoID=" & Me.InsCoID & "&InsCoName=" & Me.InsCoName)

            Case "ViewProduct"
                Me.InsCoID = e.Item.Cells(4).Text
                Me.InsCoName = e.Item.Cells(5).Text
                Response.Redirect("WFInsuranceCompanyProduct.aspx?InsCoID=" & Me.InsCoID & "&InsCoName=" & Me.InsCoName)

        End Select
    End Sub

    Private Sub BtnResetAsr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnResetAsr.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        PanelAllFalse()
        BindGridEntity("ALL", "")
        pnlList.Visible = True
    End Sub

    Private Sub dtgInsCo_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgInsCo.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub btnCancelAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAdd.Click
        cboSearchBy.SelectedIndex = 0
        TxtSearchByValue.Text = ""
        PanelAllFalse()

        pnlList.Visible = True
    End Sub

End Class