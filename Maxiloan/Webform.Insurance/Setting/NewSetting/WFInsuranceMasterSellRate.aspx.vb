﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class WFInsuranceMasterSellRate
    Inherits Maxiloan.Webform.WebBased

#Region "Property"
    Private Property Command() As String
        Get
            Return CType(viewstate("Command"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Command") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere") = Value
        End Set
    End Property
    Private Property SearchByH() As String
        Get
            Return CType(viewstate("SearchByH"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SearchByH") = Value
        End Set
    End Property
    Private Property SearchByD() As String
        Get
            Return CType(viewstate("SearchByD"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SearchByD") = Value
        End Set
    End Property
    Private Property SortD() As String
        Get
            Return CType(viewstate("SortD"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SortD") = Value
        End Set
    End Property
    Private Property Flag() As String
        Get
            Return CType(viewstate("Flag"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Flag") = Value
        End Set
    End Property

#End Region

#Region "Constanta"

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow

    Private currentPageD As Integer = 1
    Private pageSizeD As Int16 = 10
    Private currentPageNumberD As Int32 = 1
    Private totalPagesD As Double = 1
    Private recordCountD As Int64 = 1
    Dim oRowD As DataRow

    Private oCustomClassH As New Parameter.GeneralPaging
    Private oControllerH As New GeneralPagingController
    Private oCustomClassD As New Parameter.GeneralPaging
    Private oControllerD As New GeneralPagingController

    Private oControllerHeader As New cInsuranceRateCard
    Private oCustomClassHeader As New Parameter.eInsuranceRateCard
    Private oControllerDetail As New cInsuranceRateCard
    Private oCustomClassDetail As New Parameter.eInsuranceRateCard


#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridH(Me.SortBy, Me.SearchByH)
    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridH(Me.SortBy, Me.SearchByH)
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridH(Me.SortBy, Me.SearchByH)
    End Sub

#End Region

#Region "Page Load"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If sessioninvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            pnlSearchH.Visible = True
            pnlListH.Visible = True
            pnlAddEditH.Visible = False
            pnlSearchD.Visible = False
            pnlAddEditD.Visible = False
            txtSearchByH.Text = ""
            txtIdH.Enabled = True
            chkIsActiveH.Checked = True
            Me.SortBy = ""
            Me.SearchByH = ""
            BindGridH(Me.SortBy, Me.SearchByH)
        End If
    End Sub

#End Region

#Region "Initial Panel"

    Sub InitialPanelH()
        pnlSearchH.Visible = True
        pnlListH.Visible = False
        pnlAddEditH.Visible = False
        pnlSearchD.Visible = False
        pnlAddEditD.Visible = False
        txtSearchByH.Text = ""
        txtIdH.Enabled = True
        chkIsActiveH.Checked = True
    End Sub
    Sub InitialPanelD()
        pnlSearchH.Visible = False
        pnlListH.Visible = False
        pnlAddEditH.Visible = False
        pnlSearchD.Visible = True
        pnlAddEditD.Visible = False
        Me.Flag = "N"

    End Sub

#End Region

#Region "Click Header"

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pnlSearchH.Visible = True
        pnlListH.Visible = True

        txtGoPage.Text = ""
        Me.SortBy = ""
        If txtSearchByH.Text = "" Then
            Me.SearchByH = ""
        Else
            Me.SearchByH = cboSearchByH.SelectedValue.Trim + " like '%" & txtSearchByH.Text.Trim & "%'"
        End If
        BindGridH(Me.SortBy, Me.SearchByH)
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        pnlSearchH.Visible = False
        pnlListH.Visible = False
        pnlAddEditH.Visible = True
        chkIsActiveH.Checked = True
        txtIdH.Enabled = True
        lblAddEditH.Text = "ADD"
        Me.Command = "A1"

        txtIdH.Text = ""
        txtDescH.Text = ""

    End Sub
    Private Sub btnSaveH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveH.Click

        With oCustomClassHeader
            .strConnection = GetConnectionString()
            .CardID = txtIdH.Text.Trim
            .CardDesc = txtDescH.Text.Trim
            .IsActive = CBool(chkIsActiveH.Checked)
            .Command = Me.Command.Trim
        End With
        oCustomClassHeader = oControllerHeader.GetInsuranceRateCardHSave(oCustomClassHeader)

        If oCustomClassHeader.output = "" Then
            If Me.Command = "A1" Then
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
            Else
                ShowMessage(lblMessage, "Update data Berhasil", False)
            End If
            pnlSearchH.Visible = True
            pnlListH.Visible = True
            pnlAddEditH.Visible = False
            pnlSearchD.Visible = False
            pnlAddEditD.Visible = False
            txtSearchByH.Text = ""
            Me.SortBy = ""
            Me.SearchByH = ""
            BindGridH(Me.SortBy, Me.SearchByH)
        Else
            ShowMessage(lblMessage, oCustomClassHeader.output, True)
        End If
    End Sub
    Private Sub btnCancelH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelH.Click
        pnlSearchH.Visible = True
        pnlListH.Visible = True
        pnlAddEditH.Visible = False
        pnlSearchD.Visible = False
        pnlAddEditD.Visible = False
        txtSearchByH.Text = ""
        txtIdH.Enabled = True
    End Sub

#End Region

#Region "Bind Header"

    Sub BindGridH(ByVal SortBy As String, ByVal SearchByH As String)
        Dim dtModel As DataTable

        If SearchByH = "" Then
            Me.CmdWhere = ""
        Else
            Me.CmdWhere = SearchByH
        End If
        With oCustomClassH
            .strConnection = GetConnectionString
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy.Trim
            .SpName = "spShadowInsuranceMasterSellRatePaging"
        End With
        oCustomClassH = oControllerH.GetGeneralPaging(oCustomClassH)

        If Not oCustomClassH Is Nothing Then
            dtModel = oCustomClassH.ListData
            recordCount = oCustomClassH.TotalRecords
            lblTotRec.Text = recordCount.ToString
        Else
            recordCount = 0
            lblTotRec.Text = recordCount.ToString
        End If
        dtgPaging.DataSource = dtModel.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Select Case e.CommandName
            Case "EditH"

                pnlSearchH.Visible = False
                pnlListH.Visible = False
                pnlAddEditH.Visible = True
                lblAddEditH.Text = "EDIT"
                Me.Command = "E1"

                txtIdH.Enabled = False
                txtIdH.Text = CType(e.Item.FindControl("lblID"), Label).Text
                txtDescH.Text = CType(e.Item.FindControl("lblDesc"), Label).Text

                chkIsActiveH.Checked = CBool(CType(e.Item.FindControl("lblIsActiveH"), Label).Text)
            Case "Rate"
                InitialPanelD()
                lblID1.Text = CType(e.Item.FindControl("lblID"), Label).Text
                lblDesc1.Text = CType(e.Item.FindControl("lblDesc"), Label).Text
                Me.SearchByD = "InsuranceMasterRateID = '" & lblID1.Text.Trim & "'"
                Me.SortD = ""
                BindGridD(Me.SortD, Me.SearchByD)
                fillcbo()
                imbCopyD.Attributes.Add("OnClick", "return CopyPremiumRateConfirm()")
        End Select
    End Sub

#End Region


#Region " Navigation Detail"
    Private Sub PagingFooterD()
        lblPageD.Text = currentPageD.ToString()
        totalPagesD = Math.Ceiling(CType((recordCountD / CType(pageSizeD, Integer)), Double))
        If totalPagesD = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            lblTotPageD.Text = "1"
            rgvGoD.MaximumValue = "1"
        Else
            lblTotPageD.Text = (System.Math.Ceiling(totalPagesD)).ToString()
            rgvGoD.MaximumValue = (System.Math.Ceiling(totalPagesD)).ToString()
        End If
        lblTotRecD.Text = recordCountD.ToString

        If currentPageD = 1 Then
            imbPrevPageD.Enabled = False
            imbFirstPageD.Enabled = False
            If totalPagesD > 1 Then
                imbNextPageD.Enabled = True
                imbLastPageD.Enabled = True
            Else
                imbPrevPageD.Enabled = False
                imbNextPageD.Enabled = False
                imbLastPageD.Enabled = False
                imbFirstPageD.Enabled = False
            End If
        Else
            imbPrevPageD.Enabled = True
            imbFirstPageD.Enabled = True
            If currentPageD = totalPagesD Then
                imbNextPageD.Enabled = False
                imbLastPageD.Enabled = False
            Else
                imbLastPageD.Enabled = True
                imbNextPageD.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLinkD_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPageD = 1
            Case "Last" : currentPageD = Int32.Parse(lblTotPageD.Text)
            Case "Next" : currentPageD = Int32.Parse(lblPageD.Text) + 1
            Case "Prev" : currentPageD = Int32.Parse(lblPageD.Text) - 1
        End Select
        BindGridD(Me.SortD, Me.SearchByD)
    End Sub
    Private Sub BtnGoPageD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPageD.Click
        If IsNumeric(txtGoPageD.Text) Then
            If CType(lblTotPageD.Text, Integer) > 1 And CType(txtGoPageD.Text, Integer) <= CType(lblTotPageD.Text, Integer) Then
                currentPageD = CType(txtGoPageD.Text, Int32)
                BindGridD(Me.SortD, Me.SearchByD)
            End If
        End If
    End Sub
    Public Sub SortGridD(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortD = e.SortExpression
        Else
            Me.SortD = e.SortExpression + " DESC"
        End If
        BindGridD(Me.SortD, Me.SearchByD)
    End Sub
    Sub BindGridD(ByVal SortBy As String, ByVal SearchByD As String)
        Dim dtRate As DataTable

        With oCustomClassD
            .strConnection = GetConnectionString
            .WhereCond = SearchByD
            .CurrentPage = currentPageD
            .PageSize = pageSizeD
            .SortBy = SortBy.Trim
            .SpName = "spShadowInsuranceMasterSellRateDPaging"
        End With
        oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)

        If Not oCustomClassD Is Nothing Then
            dtRate = oCustomClassD.ListData
            recordCountD = oCustomClassD.TotalRecords
            lblTotRecD.Text = recordCountD.ToString
        Else
            recordCountD = 0
            lblTotRecD.Text = recordCountD.ToString
        End If
        If recordCountD = 0 Then
            cboCopyFrom.Enabled = True
            imbCopyD.Visible = True
        Else
            cboCopyFrom.Enabled = False
            imbCopyD.Visible = False
        End If
        dtgPagingD.DataSource = dtRate.DefaultView
        dtgPagingD.CurrentPageIndex = 0
        dtgPagingD.DataBind()
        PagingFooterD()
    End Sub

#End Region

#Region "Bind Detail"

    Private Sub fillcbo()
        Dim dtCopyFrom As DataTable
        With oCustomClassD
            .strConnection = GetConnectionString
            .WhereCond = "d.InsuranceMasterRateID <> '" & lblID1.Text.Trim & "'"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowInsuranceMasterSellRateDSource"
        End With
        oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)
        dtCopyFrom = oCustomClassD.ListData

        With cboCopyFrom
            .DataSource = dtCopyFrom.DefaultView
            .DataValueField = "InsuranceMasterRateID"
            .DataTextField = "InsuranceMasterRateDesc"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
            '.Items.Insert(1, "ALL")
            '.Items(1).Value = "ALL"
        End With

        'Dim dtInsuranceType As DataTable
        'With oCustomClassD
        '    .strConnection = GetConnectionString
        '    .WhereCond = ""
        '    .CurrentPage = 1
        '    .PageSize = 1000
        '    .SortBy = ""
        '    .SpName = "spShadowInsuranceRateCategory"
        'End With
        'oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)
        'dtInsuranceType = oCustomClassD.ListData

        'With cboInsType
        '    .DataSource = dtInsuranceType.DefaultView
        '    .DataValueField = "InsRateCategoryID"
        '    .DataTextField = "Description"
        '    .DataBind()
        '    .Items.Insert(0, "Select One")
        '    .Items(0).Value = "0"
        '    '.Items.Insert(1, "ALL")
        '    '.Items(1).Value = "ALL"
        'End With

    End Sub

    Private Sub dtgPagingD_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPagingD.ItemCommand
        Select Case e.CommandName
            Case "EditD"

                pnlSearchD.Visible = False
                pnlAddEditD.Visible = True
                Me.Command = "E"

                lblAddEditD.Text = "EDIT"

                'cboInsType.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(12).Text
                'LblInsTypeDescription.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(0).Text

                'cboUsage.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(13).Text
                'cboNewUsed.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(14).Text
                'cboCoverageType.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(15).Text

                'cboInsType.Items.FindByText(dtgPagingD.Items(e.Item.ItemIndex).Cells(0).Text).Selected = True
                txtYearNum.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(2).Text
                txtRateForGroup.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(3).Text
                txtRateForNonGroup.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(4).Text
                txtRateRO.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(5).Text
                'txtSRCCRate.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(8).Text
                'txtFloodRate.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(9).Text
                txtRateLoadingPenggunaan.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(6).Text
                txtSumIns.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(7).Text



                'cboInsType.Enabled = False
                'cboUsage.Enabled = False
                'cboNewUsed.Enabled = False
                'cboCoverageType.Enabled = False
                txtYearNum.Enabled = False

            Case "Delete"
                Me.Command = "D"
                Dim oReturnValue As New Parameter.eInsuranceRateCard

                With oCustomClassDetail
                    .CardID = lblID1.Text.Trim
                    .YearNum = CInt(dtgPagingD.Items(e.Item.ItemIndex).Cells(2).Text)
                    .strConnection = GetConnectionString
                    .Command = Me.Command
                End With

                Dim strError As String
                strError = oControllerDetail.GetInsuranceMasterRateDDelete(oCustomClassDetail)

                If strError = "" Then
                    ShowMessage(lblMessage, "Data Sudah diHapus", True)
                    Me.SortD = ""
                    Me.SearchBy = ""
                    BindGridD(Me.SortD, Me.SearchByD)
                Else
                    ShowMessage(lblMessage, strError, True)
                End If

        End Select
    End Sub
    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPagingD.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete1 As ImageButton
            imbDelete1 = CType(e.Item.FindControl("imbdelete"), ImageButton)
            imbDelete1.Attributes.Add("Onclick", "return DeleteRateConfirm()")
        End If
    End Sub

#End Region

#Region "Click Detail"
    Private Sub btnAddD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddD.Click

        pnlSearchD.Visible = False
        pnlAddEditD.Visible = True
        Me.Command = "A"

        lblAddEditD.Text = "ADD"
        txtYearNum.Text = ""
        txtRateForGroup.Text = ""
        txtRateForNonGroup.Text = ""
        txtRateRO.Text = ""
        txtRateLoadingPenggunaan.Text = ""
        txtSumIns.Text = ""

        txtYearNum.Enabled = True
    End Sub
    Private Sub btnSaveD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveD.Click

        With oCustomClassDetail
            .CardID = lblID1.Text.Trim
            .YearNum = CInt(txtYearNum.Text)
            .RateGroup = CDbl(IIf(txtRateForGroup.Text.Trim = "", "0", txtRateForGroup.Text.Trim))
            .RateNonGroup = CDbl(IIf(txtRateForNonGroup.Text.Trim = "", "0", txtRateForNonGroup.Text.Trim))
            .RateRepeatOrder = CDbl(IIf(txtRateRO.Text.Trim = "", "0", txtRateRO.Text.Trim))
            .RateLoadingPenggunaan = CDbl(IIf(txtRateLoadingPenggunaan.Text.Trim = "", "0", txtRateLoadingPenggunaan.Text.Trim))
            .SumIns = CDbl(IIf(txtSumIns.Text.Trim = "", "0", txtSumIns.Text.Trim))
            .Command = Me.Command.Trim
            .strConnection = GetConnectionString()
        End With

        oCustomClassDetail = oControllerDetail.GetInsuranceMasterRateDSave(oCustomClassDetail)

        If oCustomClassDetail.output = "" Then
            If Me.Command = "A" Then
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
            Else
                ShowMessage(lblMessage, "Update data Berhasil", False)
            End If
            InitialPanelD()
            Me.SortD = ""
            Me.SearchByD = "InsuranceMasterRateID = '" & lblID1.Text.Trim & "'"
            BindGridD(Me.SortD, Me.SearchByD)
        Else
            ShowMessage(lblMessage, oCustomClassDetail.output, True)
        End If

    End Sub
    Private Sub btnBackD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackD.Click
        pnlSearchH.Visible = True
        pnlListH.Visible = True
        pnlAddEditH.Visible = False
        pnlSearchD.Visible = False
        pnlAddEditD.Visible = False
        txtSearchByH.Text = ""
    End Sub

    Private Sub btnCancelD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelD.Click
        InitialPanelD()
    End Sub
    Private Sub imbCopyD_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCopyD.Click
        'If cboCopyFrom.SelectedValue = "0" Then
        '    ShowMessage(lblMessage, "Please Select Insurance Rate Card to Copy"
        '    Exit Sub
        'End If

        'imbCopyD.Attributes.Add("OnClick", "return CopyPremiumRateConfirm()")
        Me.Command = "C1"
        Dim oReturnValue As New Parameter.eInsuranceRateCard
        With oCustomClassDetail
            .CardID = lblID1.Text.Trim
            .CardIDSource = cboCopyFrom.SelectedValue.Trim
            .strConnection = GetConnectionString
            .Command = Me.Command.Trim
        End With

        Dim strError As String
        strError = oControllerDetail.GetInsuranceRateCardDCopy(oCustomClassDetail)

        If strError = "" Then
            ShowMessage(lblMessage, "Copy Data Berhasil", False)
            Me.SortD = ""
            Me.SearchByD = "InsuranceMasterRateID = '" & lblID1.Text.Trim & "'"
            BindGridD(Me.SortD, Me.SearchByD)
        Else
            ShowMessage(lblMessage, strError, True)
        End If
    End Sub
#End Region

End Class