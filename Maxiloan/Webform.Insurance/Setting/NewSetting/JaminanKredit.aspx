﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JaminanKredit.aspx.vb" Inherits="Maxiloan.Webform.Insurance.JaminanKredit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>WFInsuranceCompanyBranch</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }

        function CopyRateConfirm() {
            if (document.forms[0].cboInsCoBranchCopy.value == '0') {
                alert("Harap pilih Cabang Perusahaan Asuransi terlebih dahulu")
                return false;
            }
            else {
                if (window.confirm("Apakah yakin mau hapus semua rate dan copy rate dari Cabang Asuransi dipilih ?"))
                    return true;
                else
                    return false;
            }
        }

        function fback() {
            history.go(-1);
            return false;
        }
        function OpenViewInsCoBranch(pInsCoID, pInsCoBranchID, pStyle) {
            var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
            var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
            var x = screen.width; var y = screen.height - 100; window.open('http://<%#Request.servervariables("SERVER_NAME")%>/' + App + '/Webform.Insurance/Setting/InsCoView.aspx?InsCoID=' + pInsCoID + '&style=INSURANCE&InsCoBranchID=' + pInsCoBranchID, 'InsuranceCo', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
              <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
            <asp:Panel ID="PnlMain" runat="server">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            INSURANCE LOADING RATE
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Maskapai Asuransi</label>
                        <asp:Label ID="LblMaskapaiAsr" runat="server" Width="504px"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cabang Asuransi</label>
                        <asp:Label ID="lblCabangAsuransi" runat="server" Width="504px"></asp:Label>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DtgInsCoCP" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="EDIT">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbBranchEdit" runat="server" CommandName="Edit" ImageUrl="../../../Images/iconedit.gif"
                                                CausesValidation="False">
                                            </asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DELETE">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../../Images/icondelete.gif"
                                                CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:BoundColumn DataField="CategoryType" SortExpression="CategoryType"
                                        Visible="false"></asp:BoundColumn>--%>
                                    <asp:BoundColumn DataField="Tenor" SortExpression="Tenor"
                                        HeaderText="TENOR"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Rate" SortExpression="Rate"
                                        HeaderText="RATE"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                        <div class="button_gridnavigation">
                            <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                                CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                                CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                                CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                                CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                            Page&nbsp;
                            <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                            <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                                EnableViewState="False"></asp:Button>
                            <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" ControlToValidate="txtGopage"
                                MinimumValue="1" ErrorMessage="No Halaman Salah" Type="Integer" MaximumValue="999999999"></asp:RangeValidator>
                            <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                                ControlToValidate="txtGopage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="label_gridnavigation">
                            <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                            <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                            <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="small button blue"></asp:Button>
                    <asp:Button ID="btnBack" runat="server" CausesValidation="False" Text="Back" CssClass="small
        button gray"></asp:Button>
                </div>
            </asp:Panel> 

             <asp:Panel ID="PnlAdd" runat="server">
                <asp:Label ID="Label4" runat="server" ForeColor="Red"></asp:Label>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            JAMINAN KREDIT -&nbsp;ADD
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tenor</label>
                        <asp:TextBox ID="txtTenor" runat="server"  MaxLength="9"
                    Columns="10"></asp:TextBox>&nbsp;Tahun
                    </div>
                </div>

                  <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rate</label>
                        <asp:TextBox ID="txtRate" runat="server"  MaxLength="9"
                    Columns="10"></asp:TextBox>%
                    </div>
                </div>

                <div class="form_button">
                    <asp:Button ID="btnSaveAdd" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancelAdd" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
           
           
             <asp:Panel ID="PnlEdit" runat="server">
                <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                <div class="form_title">
                    <div class="form_single">
                        <h4>
                            JAMINAN KREDIT -&nbsp;EDIT
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tenor</label>
                        <asp:TextBox ID="TxtTenorEdit" runat="server"  MaxLength="9"
                    Columns="10"></asp:TextBox>&nbsp;Tahun
                    </div>
                </div>

                  <div class="form_box">
                    <div class="form_single">
                        <label>
                            Rate</label>
                        <asp:TextBox ID="TxtRateEdit" runat="server"  MaxLength="9"
                    Columns="10"></asp:TextBox>%
                    </div>
                </div>

                <div class="form_button">
                    <asp:Button ID="BtnSaveEdit" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="BtnCancelEdit" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
           


        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
