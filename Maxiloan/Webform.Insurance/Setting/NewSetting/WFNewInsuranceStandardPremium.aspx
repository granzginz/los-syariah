﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WFNewInsuranceStandardPremium.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.WFNewInsuranceStandardPremium" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../../../webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WFNewInsuranceStandardPremium</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function CopyPremiumRateConfirm() {
            if (document.forms[0].cboCopyFrom.value == '0') {
                alert("Harap pilih Rate Card Asuransi Terlebih Dahulu")
                return false;
            }
            else {
                if (confirm("Apakah yakin mau copy rate dari Master Rate ?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        function DeleteRateConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ? ")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="PnlList" runat="server" Width="100%" HorizontalAlign="center">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR STANDARD RATE ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="RATE KE CUSTOMER">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbRate" runat="server" ImageUrl="../../../images/IconRate.gif"
                                        CausesValidation="False" CommandName="Rate"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TPL">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgTPL" ImageUrl="../../../images/iconrate.gif" runat="server"
                                        CausesValidation="False" CommandName="TPL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COVER LAIN">
                                <HeaderStyle Width="5%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <a href='WFStdCoverageOthers.aspx?BranchID=<%# DataBinder.eval(Container,"DataItem.BranchID") %>'>
                                        <asp:Image ID="Image1" ImageUrl="../../../images/iconrate.gif" runat="server"></asp:Image>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="BranchID" SortExpression="BranchID" HeaderText="ID CABANG">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="BranchFullName" SortExpression="BranchFullName" HeaderText="NAMA CABANG">
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlBranch" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI STANDARD RATE ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <uc1:ucbranchall id="oBranchAll" runat="server" />
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearchD" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR DETAIL RATE CARD ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPagingD" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="EditD"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="InsuranceType2" SortExpression="InsuranceType"
                                HeaderText="JENIS ASSET ASR">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="UsageId2" SortExpression="UsageId" HeaderText="PENGGUNAAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="NewUsed2" SortExpression="NewUsed" HeaderText="BARU / BEKAS">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CoverageType2" SortExpression="CoverageType"
                                HeaderText="JENIS COVER"></asp:BoundColumn>
                            <asp:BoundColumn DataField="InsuranceType" SortExpression="InsuranceType" HeaderText="JENIS ASSET ASR">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="UsageId" SortExpression="UsageId" HeaderText="PENGGUNAAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NewUsed" SortExpression="NewUsed" HeaderText="BARU / BEKAS">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CoverageType" SortExpression="CoverageType" HeaderText="JENIS COVER">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SumInsuredFrom" SortExpression="SumInsuredFrom" HeaderText="NILAI COVER DARI"
                                DataFormatString="{0:N2}">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SumInsuredTo" SortExpression="SumInsuredTo" HeaderText="NILAI COVER SAMPAI"
                                DataFormatString="{0:N2}">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InsuranceMasterRateDesc" SortExpression="InsuranceMasterRateDesc"
                                HeaderText="MASTER ASURANSI">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="InsuranceMasterRateID" SortExpression="InsuranceMasterRateID"
                                HeaderText="MASTER ASURANSI"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <div class="button_gridnavigation">
                        <asp:ImageButton ID="imbFirstPageD" runat="server" ImageUrl="../../../Images/grid_navbutton01.png"
                            CausesValidation="False" OnCommand="NavigationLinkD_Click" CommandName="First">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbPrevPageD" runat="server" ImageUrl="../../../Images/grid_navbutton02.png"
                            CausesValidation="False" OnCommand="NavigationLinkD_Click" CommandName="Prev">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbNextPageD" runat="server" ImageUrl="../../../Images/grid_navbutton03.png"
                            CausesValidation="False" OnCommand="NavigationLinkD_Click" CommandName="Next">
                        </asp:ImageButton>
                        <asp:ImageButton ID="imbLastPageD" runat="server" ImageUrl="../../../Images/grid_navbutton04.png"
                            CausesValidation="False" OnCommand="NavigationLinkD_Click" CommandName="Last">
                        </asp:ImageButton>
                        Page&nbsp;
                        <asp:TextBox ID="txtGoPageD" runat="server" Width="34px" >1</asp:TextBox>
                        <asp:Button ID="BtnGoPageD" runat="server" Text="Go" CssClass="small buttongo blue"
                            EnableViewState="False"></asp:Button>
                        <asp:RangeValidator ID="rgvGoD" runat="server" MinimumValue="1" MaximumValue="999999999"
                            Type="Integer"  ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPageD"
                             CssClass="validator_general" ></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rfvGoD" runat="server"   ErrorMessage="No Halaman Salah"
                            ControlToValidate="txtGoPageD"  CssClass="validator_general"  Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="label_gridnavigation">
                        <asp:Label ID="lblPageD" runat="server"></asp:Label>&nbsp;of
                        <asp:Label ID="lblTotPageD" runat="server"></asp:Label>, Total&nbsp;
                        <asp:Label ID="lblTotRecD" runat="server"></asp:Label>&nbsp;record(s)
                    </div>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddD" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBackD" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI DETAIL STANDARD RATE ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID</label>
                <asp:Label ID="lblID1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cabang</label>
                <asp:Label ID="lblDesc1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchByD" runat="server">
                    <asp:ListItem Value="InsuranceType2">Jenis Asset Asuransi</asp:ListItem>
                    <asp:ListItem Value="UsageId2">Penggunaan Asset</asp:ListItem>
                    <asp:ListItem Value="NewUsed2">Baru / Bekas</asp:ListItem>
                    <asp:ListItem Value="CoverageType2">Jenis Cover</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchByD" runat="server" Width="200px"  MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Copy Rate Dari</label>
                <asp:DropDownList ID="cboCopyFrom" runat="server">
                </asp:DropDownList>
                <asp:ImageButton ID="imbCopyD" runat="server" ImageUrl="../../../Images/ButtonCopy.gif"
                    CausesValidation="False"></asp:ImageButton>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearchD" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEditD" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STANDARD RATE ASURANSI -&nbsp;
                    <asp:Label ID="lblAddEditD" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Asset Asuransi</label>
                <asp:DropDownList ID="cboInsType" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic"
                    InitialValue="0" ControlToValidate="cboInsType" ErrorMessage="Harap pilih Jenis Asuransi" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Penggunaan Asset</label>
                <asp:DropDownList ID="cboUsage" runat="server">
                    <asp:ListItem Value="N" Selected="True">Non-Commercial</asp:ListItem>
                    <asp:ListItem Value="C">Commercial</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Asset : Baru / Bekas</label>
                <asp:DropDownList ID="cboNewUsed" runat="server">
                    <asp:ListItem Value="N">New</asp:ListItem>
                    <asp:ListItem Value="U">Used</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Cover</label>
                <asp:DropDownList ID="cboCoverageType" runat="server">
                    <asp:ListItem Value="ARK">All Risk</asp:ListItem>
                    <asp:ListItem Value="TLO">Total Lost Only</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Master Rate</label>
                <asp:DropDownList ID="cbomasterrate" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic"
                    InitialValue="0" ControlToValidate="cbomasterrate" ErrorMessage="Harap Pilih Master Rate" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nilai Pertanggungan Dari</label>
               <%-- <asp:TextBox ID="txtSumFrom" runat="server" Width="129px" Columns="10" MaxLength="20"
                    ></asp:TextBox>
                <asp:RequiredFieldValidator ID="requiredfieldvalidator1" runat="server" ErrorMessage="Harap isi Nilai Pertanggungan Dari"
                    ControlToValidate="txtSumFrom"  CssClass="validator_general"  Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtSumFrom"
                    ErrorMessage="Nilai Pertanggungan Salah" MaximumValue="999999999999" Type="Double"
                    MinimumValue="0" CssClass="validator_general" ></asp:RangeValidator>--%>
                     <uc1:ucnumberformat id="txtSumFrom" runat="server" /></uc1:ucNumberFormat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nilai Pertanggungan Sampai</label>
     <%--           <asp:TextBox ID="txtSumTo" runat="server" Width="129px" Columns="10" MaxLength="20"
                    ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi Nilai Pertanggungan Sampai"
                    ControlToValidate="txtSumTo"   Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="txtSumTo"
                    ErrorMessage="Nilai Pertanggungan Salah" MaximumValue="999999999999" Type="Double"
                    MinimumValue="0" CssClass="validator_general" ></asp:RangeValidator>--%>
                     <uc1:ucnumberformat id="txtSumTo" runat="server" /></uc1:ucNumberFormat>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveD" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelD" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnltpl" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TPL
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Cabang</label>
                <asp:Label ID="lblBranchIDH" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TPL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgTPL" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="TPLAmount" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbedittpl" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="EditTPL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbdeletetpl" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="DeleteTPL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="TPLAmount" SortExpression="TPLAmount" HeaderText="JUMLAH TPL"
                                DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TPLPremium" SortExpression="TPLPremium" HeaderText="PREMI TPL"
                                DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddTPL1" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBackPage" runat="server" CausesValidation="False" Text="Back"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdittpl" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TPL -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Cabang</label>
                <asp:Label ID="LblBranchIDD" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah TPL</label>
               <%-- <asp:TextBox ID="txtTplAmount" runat="server" Width="154px"  MaxLength="12"
                    Columns="14"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="txtTplAmount"
                    ErrorMessage="Harap isi data Jumlah TPL" CssClass="validator_general" ></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator4" runat="server" ControlToValidate="txtTplAmount"
                    ErrorMessage="Jumlah TPL Salah" MaximumValue="999999999999" Type="Double" MinimumValue="0" CssClass="validator_general" ></asp:RangeValidator>--%>
                     <uc1:ucnumberformat id="txtTplAmount" runat="server" /></uc1:ucNumberFormat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Premi TPL</label>
                <%--<asp:TextBox ID="txtTplPremium" runat="server" Width="154px"  MaxLength="12"
                    Columns="14"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="txtTplPremium"
                    ErrorMessage="Harap isi Premi TPL" CssClass="validator_general" ></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="txtTplPremium"
                    ErrorMessage="Premi TPL Salah" MaximumValue="999999999999" Type="Double" MinimumValue="0" CssClass="validator_general" ></asp:RangeValidator>--%>       
             <uc1:ucnumberformat id="txtTplPremium" runat="server" /></uc1:ucNumberFormat>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveTPL" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelTPL" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
