﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class CreditProtection
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents txtUsiaAsset As ucNumberFormat
    Protected WithEvents txtLoadingRate As ucNumberFormat
#Region " Private Const "
    Dim m_InsCo As New Maxiloan.Controller.cInsuranceCompany

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Dim oController As New InsCoRateAdditionalController


#End Region

#Region "Property"

    Private Property InsCoID() As String
        Get
            Return CStr(ViewState("InsCoID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoID") = Value
        End Set
    End Property

    Private Property ProductID() As String
        Get
            Return CStr(ViewState("ProductID"))
        End Get
        Set(ByVal Value As String)
            ViewState("ProductID") = Value
        End Set
    End Property

    Private Property InsCoBranchID() As String
        Get
            Return CStr(ViewState("InsCoBranchID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchID") = Value
        End Set
    End Property

    Private Property InsCoBranchName() As String
        Get
            Return CStr(ViewState("InsCoBranchName"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchName") = Value
        End Set
    End Property

    Private Property InsCoName() As String
        Get
            Return CStr(ViewState("InsCoName"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoName") = Value
        End Set
    End Property

    Private Property CurrRate() As String
        Get
            Return CStr(ViewState("CurrRate"))
        End Get
        Set(ByVal Value As String)
            ViewState("CurrRate") = Value
        End Set
    End Property

#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridBranch(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridBranch(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)

        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        'BindGridBranch(Me.SearchBy, "")
        BindGridBranch(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region " BindGridBranch "
    Sub BindGridBranch(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCustomClassInsCoRateAdditional As New Parameter.InsCoRateAdditional


        With oCustomClassInsCoRateAdditional
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort.Trim
        End With

        oCustomClassInsCoRateAdditional = oController.GetInsCoProductList(oCustomClassInsCoRateAdditional)

        dtsEntity = oCustomClassInsCoRateAdditional.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = "Tenor"
        DtgInsCoCP.DataSource = dtvEntity
        Try
            DtgInsCoCP.DataBind()
            DtgInsCoCP.Visible = True
        Catch
            DtgInsCoCP.CurrentPageIndex = 0
            DtgInsCoCP.DataBind()
            DtgInsCoCP.Visible = True
        End Try

        PagingFooter()
    End Sub
#End Region
    Private Sub InitialDefaultPanel()
        PnlMain.Visible = True
        PnlEdit.Visible = False
        PnlAdd.Visible = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            InitialDefaultPanel()

            Me.InsCoID = Request.QueryString("InsCoID")
            Me.BranchID = Request.QueryString("BranchID")
            Me.InsCoBranchName = Request.QueryString("InsCoBranchName")
            Me.InsCoBranchID = Request.QueryString("InsCoBranchID")
            Me.InsCoName = Request.QueryString("InsCoName")
            Me.ProductID = Request.QueryString("ProductID")

            LblMaskapaiAsr.Text = Me.InsCoID.Trim
            lblCabangAsuransi.Text = Me.InsCoBranchName.Trim

            Me.SearchBy = "MaskAssID = '" & Me.InsCoID.Trim & "'  and ProductID='" & Me.ProductID.Trim + "'and MaskAssBranchID='" & Me.InsCoBranchID.Trim + "'"
            Me.SortBy = "MaskAssID"

            BindGridBranch(Me.SearchBy, Me.SortBy)

        End If
    End Sub


    Public Sub Clean()
        txtRate.Text = Nothing
        txtTenor.Text = Nothing
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Clean()
        PnlMain.Visible = False
        PnlAdd.Visible = True
    End Sub

    Protected Sub btnSaveAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAdd.Click
        SaveLoadingRate()
    End Sub
    Private Sub SaveLoadingRate()
        Dim customClass As New Parameter.InsCoRateAdditional

        With customClass
            .strConnection = GetConnectionString()
            .MaskAssId = Me.InsCoID.Trim
            .InsCoBranchID = Me.InsCoBranchID.Trim
            .ProductID = Me.ProductID.Trim
            .BranchId = Me.BranchID.Trim
            .Tenor = txtTenor.Text.Trim
            .Rate = CDbl(txtRate.Text.Trim)
            .IsActive = "True"
        End With

        Try
            Dim msg As String = ""
            msg = oController.InsCoProductSaveAdd(customClass)
            If msg = "" Then
                ShowMessage(lblMessage, "Succes", False)
            Else
                ShowMessage(lblMessage, msg, False)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


        PnlMain.Visible = True
        PnlAdd.Visible = False
        lblMessage.Visible = True

        Me.SearchBy = "MaskAssID = '" & Me.InsCoID.Trim & "'  and ProductID='" & Me.ProductID.Trim + "'and MaskAssBranchID='" & Me.InsCoBranchID.Trim + "'"
        Me.SortBy = "MaskAssID"

        BindGridBranch(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub DtgInsCoBranch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgInsCoCP.ItemCommand

        Select Case e.CommandName
            Case "Edit"
                lblMessage.Visible = False
                PnlMain.Visible = False
                PnlEdit.Visible = True

                TxtTenorEdit.Text = e.Item.Cells(2).Text
                TxtRateEdit.Text = e.Item.Cells(3).Text

                Me.CurrRate = e.Item.Cells(2).Text


            Case "Delete"
                lblMessage.Visible = False
                Dim customClass As New Parameter.InsCoRateAdditional

                With customClass
                    .strConnection = GetConnectionString()
                    .MaskAssId = Me.InsCoID.Trim
                    .InsCoBranchID = Me.InsCoBranchID.Trim
                    .ProductID = Me.ProductID.Trim
                    .BranchId = Me.BranchID.Trim
                    .Tenor = e.Item.Cells(2).Text.Trim
                End With

                Try
                    Dim msg As String = ""
                    msg = oController.InsCoProductDelete(customClass)
                    If msg = "" Then
                        ShowMessage(lblMessage, "Succes", False)
                    End If
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                End Try


                PnlMain.Visible = True
                lblMessage.Visible = True

                Me.SearchBy = "MaskAssID = '" & Me.InsCoID.Trim & "'  and ProductID='" & Me.ProductID.Trim + "'and MaskAssBranchID='" & Me.InsCoBranchID.Trim + "'"
                Me.SortBy = "MaskAssID"

                BindGridBranch(Me.SearchBy, Me.SortBy)
        End Select
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim Target As String = "WFInsuranceCompanyBranch"
        Dim x As String
        x = Target + ".aspx?InsCoID=" + Me.InsCoID + " &InsCoName=" + Me.InsCoName + ""
        Response.Redirect(x.Trim)
    End Sub

    Protected Sub btncanceladd_click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelAdd.Click
        InitialDefaultPanel()
    End Sub

    Private Sub DtgInsCoBranch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgInsCoCP.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub

    Private Sub BtnSaveEdit_Click(sender As Object, e As EventArgs) Handles BtnSaveEdit.Click

        Dim customClass As New Parameter.InsCoRateAdditional

        With customClass
            .strConnection = GetConnectionString()
            .MaskAssId = Me.InsCoID.Trim
            .InsCoBranchID = Me.InsCoBranchID.Trim
            .ProductID = Me.ProductID.Trim
            .BranchId = Me.BranchID.Trim
            .Tenor = TxtTenorEdit.Text.Trim
            .CurrRate = Me.CurrRate.Trim
            .Rate = CDbl(TxtRateEdit.Text.Trim)
            .IsActive = "True"
        End With

        Try
            Dim msg As String = ""
            msg = oController.InsCoProductSaveEdit(customClass)
            If msg = "" Then
                ShowMessage(lblMessage, "Succes", False)
            Else
                ShowMessage(lblMessage, msg, False)
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try


        PnlMain.Visible = True
        PnlEdit.Visible = False
        lblMessage.Visible = True

        Me.SearchBy = "MaskAssID = '" & Me.InsCoID.Trim & "'  and ProductID='" & Me.ProductID.Trim + "'and MaskAssBranchID='" & Me.InsCoBranchID.Trim + "'"
        Me.SortBy = "MaskAssID"

        BindGridBranch(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnCancelEdit_Click(sender As Object, e As EventArgs) Handles BtnCancelEdit.Click
        InitialDefaultPanel()
    End Sub
End Class