﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WFInsuranceMasterSellRate.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.WFInsuranceMasterSellRate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WFInsuranceMasterSellRate</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function CopyPremiumRateConfirm() {
            if (document.forms[0].cboCopyFrom.value == '0') {
                alert("Harap pilih Master Rate Asuransi terlebih Dahulu")
                return false;
            }
            else {
                if (confirm("Apakah yakin mau copy rate dari Master Rate ini ?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        function DeleteRateConfirm() {
            if (confirm("Apakah yakin mau hapus rate ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }

       				
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlListH" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR MASTER SELLING RATE HEADER ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="InsuranceMasterRateID" BorderStyle="None"
                        BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="RATE">
                                <HeaderStyle Width="4%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbRate" runat="server" ImageUrl="../../../images/IconRate.gif"
                                        CausesValidation="False" CommandName="Rate"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle Width="4%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../../../images/IconEdit.gif"
                                        CausesValidation="False" CommandName="EditH"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsuranceMasterRateID" HeaderText="ID">
                                <HeaderStyle HorizontalAlign="Center" Height="20px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Container.DataItem("InsuranceMasterRateID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="InsuranceMasterRateDesc" HeaderText="NAMA MASTER SELLING RATE">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDesc" runat="server" Text='<%# Container.DataItem("InsuranceMasterRateDesc") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsActive" HeaderText="AKTIF">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActiveH" runat="server" Text='<%# Container.DataItem("IsActive") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                        CommandName="First" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                        CommandName="Prev" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                        CommandName="Next" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                        CommandName="Last" OnCommand="NavigationLink_Click"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" >1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtGoPage" ErrorMessage="No Halaman Salah"
                        Type="Integer"  CssClass="validator_general"  MaximumValue="999999999"
                        MinimumValue="1"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtGoPage"
                        ErrorMessage="No Halaman Salah"  CssClass="validator_general" 
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearchH" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI MASTER SELLING RATE HEADER ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchByH" runat="server">
                    <asp:ListItem Value="InsuranceMasterRateID">ID</asp:ListItem>
                    <asp:ListItem Value="InsuranceMasterRateDesc">Nama Master</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchByH" runat="server" Width="213px"  MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEditH" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    MASTER SELLING RATE HEADER ASURANSI -&nbsp;
                    <asp:Label ID="lblAddEditH" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID</label>
                <asp:TextBox ID="txtIdH" runat="server" Width="213px"  MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Master Selling Rate Header</label>
                <asp:TextBox ID="txtDescH" runat="server" Width="100%"  MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Aktif</label>
                <asp:CheckBox ID="chkIsActiveH" runat="server" Text="True" Checked="True"></asp:CheckBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveH" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelH" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearchD" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    MASTER SELLING RATE DETAIL ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID</label>
                <asp:Label ID="lblID1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Master Selling Rate Detail</label>
                <asp:Label ID="lblDesc1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Copy Rate Dari</label>
                <asp:DropDownList ID="cboCopyFrom" runat="server">
                </asp:DropDownList>
                <asp:ImageButton ID="imbCopyD" runat="server" CausesValidation="False" ImageUrl="../../../Images/ButtonCopy.gif">
                </asp:ImageButton>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR MASTER SELLING RATE DETAIL ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPagingD" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" CommandName="EditD"
                                        ImageUrl="../../../Images/iconedit.gif" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                        ImageUrl="../../../Images/iconDelete.gif" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="YearNum" HeaderText="TAHUN KE" SortExpression="YearNum">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RateForGroup" HeaderText="RATE U/ GROUP" SortExpression="RateForGroup">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RateForNonGroup" HeaderText="RATE U/ NON GROUP" SortExpression="RateForNonGroup">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="RateForRO" HeaderText="RATE U/ RO" SortExpression="RateForRO">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundColumn>
                             <asp:BoundColumn DataField="RateLoadingPenggunaan" HeaderText="LOADING PENGGUNAAN" SortExpression="RateLoadingPenggunaan">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SumInsured" HeaderText="PROSENTASE COVER" SortExpression="SumInsured">
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPageD" runat="server" CausesValidation="False" CommandName="First"
                        ImageUrl="../../../Images/grid_navbutton01.png" OnCommand="NavigationLinkD_Click" />
                    <asp:ImageButton ID="imbPrevPageD" runat="server" CausesValidation="False" CommandName="Prev"
                        ImageUrl="../../../Images/grid_navbutton02.png" OnCommand="NavigationLinkD_Click" />
                    <asp:ImageButton ID="imbNextPageD" runat="server" CausesValidation="False" CommandName="Next"
                        ImageUrl="../../../Images/grid_navbutton03.png" OnCommand="NavigationLinkD_Click" />
                    <asp:ImageButton ID="imbLastPageD" runat="server" CausesValidation="False" CommandName="Last"
                        ImageUrl="../../../Images/grid_navbutton04.png" OnCommand="NavigationLinkD_Click" />
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPageD" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPageD" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"> </asp:Button>
                    <asp:RangeValidator ID="rgvGoD" runat="server" ControlToValidate="txtGoPageD" ErrorMessage="No Halaman Salah"
                         CssClass="validator_general"  MaximumValue="999999999"
                        MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGoD" runat="server" ControlToValidate="txtGoPageD"
                        Display="Dynamic" ErrorMessage="No Halaman Salah"  CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPageD" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPageD" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRecD" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddD" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBackD" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEditD" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    MASTER SELLING RATE DETAIL ASURANSI -&nbsp;
                    <asp:Label ID="lblAddEditD" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Tahun Ke</label>
                <asp:TextBox ID="txtYearNum" runat="server"  MaxLength="3" Columns="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="txtYearNum"
                    ErrorMessage="Harap input Tahun Ke" CssClass="validator_general" ></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtYearNum"
                    ErrorMessage="Harap isi dengan Angka" Display="Dynamic" ValidationExpression="\d*" CssClass="validator_general" ></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Rate Untuk Group</label>
                <asp:TextBox ID="txtRateForGroup" runat="server"  MaxLength="9"
                    Columns="10"></asp:TextBox>%
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtRateForGroup"
                    ErrorMessage="Harap isi dengan Angka antara 0 s/d 100" Type="Double" Display="Dynamic"
                    Operator="LessThanEqual" ValueToCompare="100"></asp:CompareValidator>&nbsp;
                <asp:CompareValidator ID="Comparevalidator8" runat="server" ControlToValidate="txtRateForGroup"
                    ErrorMessage="Harap isi dengan Angka antara 0 s/d 100" Type="Double" Display="Dynamic"
                    Operator="GreaterThanEqual" ValueToCompare="0"></asp:CompareValidator>&nbsp;
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Rate Untuk Non Group</label>
                <asp:TextBox ID="txtRateForNonGroup" runat="server"  MaxLength="9"
                    Columns="10"></asp:TextBox>%
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtRateForNonGroup"
                    ErrorMessage="Harap isi dengan Angka antara 0 s/d 100" Type="Double" Operator="LessThanEqual"
                    ValueToCompare="100"></asp:CompareValidator>&nbsp;
                <asp:CompareValidator ID="Comparevalidator9" runat="server" ControlToValidate="txtRateForNonGroup"
                    ErrorMessage="Harap isi dengan Angka antara 0 s/d 100" Type="Double" Display="Dynamic"
                    Operator="GreaterThanEqual" ValueToCompare="0"></asp:CompareValidator>&nbsp;
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Rate Untuk Repeat Order</label>
                <asp:TextBox ID="txtRateRO" runat="server"  MaxLength="9" Columns="10"></asp:TextBox>%
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtRateRO"
                    ErrorMessage="Harap isi dengan Angka antara  0 s/d 100" Type="Double" Operator="LessThanEqual"
                    ValueToCompare="100"></asp:CompareValidator>&nbsp;
                <asp:CompareValidator ID="Comparevalidator10" runat="server" ControlToValidate="txtRateRO"
                    ErrorMessage="Harap isi dengan Angka antara 0 s/d 100" Type="Double" Display="Dynamic"
                    Operator="GreaterThanEqual" ValueToCompare="0"></asp:CompareValidator>&nbsp;
            </div>
        </div>
         <div class="form_box">
            <div class="form_single">
                <label>
                    Loading Penggunaan</label>
                <asp:TextBox ID="txtRateLoadingPenggunaan" runat="server"  MaxLength="9" Columns="10"></asp:TextBox>%
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtRateRO"
                    ErrorMessage="Harap isi dengan Angka antara  0 s/d 100" Type="Double" Operator="LessThanEqual"
                    ValueToCompare="100"></asp:CompareValidator>&nbsp;
                <asp:CompareValidator ID="Comparevalidator5" runat="server" ControlToValidate="txtRateRO"
                    ErrorMessage="Harap isi dengan Angka antara 0 s/d 100" Type="Double" Display="Dynamic"
                    Operator="GreaterThanEqual" ValueToCompare="0"></asp:CompareValidator>&nbsp;
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Prosentase Cover</label>
                <asp:TextBox ID="txtSumIns" runat="server"  MaxLength="9" Columns="10"></asp:TextBox>%
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtSumIns"
                    ErrorMessage="Harap isi dengan Angka antara 0 s/d 100" Type="Double" Operator="LessThanEqual"
                    ValueToCompare="100"></asp:CompareValidator>&nbsp;
                <asp:CompareValidator ID="Comparevalidator14" runat="server" ControlToValidate="txtSumIns"
                    ErrorMessage="Harap isi dengan Angka antara 0 s/d 100" Type="Double" Display="Dynamic"
                    Operator="GreaterThanEqual" ValueToCompare="0"></asp:CompareValidator>&nbsp;
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveD" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelD" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
