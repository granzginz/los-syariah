﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class LoadingRate
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents txtUsiaAsset As ucNumberFormat
    Protected WithEvents txtLoadingRate As ucNumberFormat
#Region " Private Const "
    Dim m_InsCo As New Maxiloan.Controller.cInsuranceCompany

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Dim oCustomClassCoverage As New Parameter.GeneralPaging
    Dim oControllerCoverage As New GeneralPagingController
#End Region

#Region "Property"
    Private Property CoverageType() As String
        Get
            Return CType(ViewState("CoverageType"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("CoverageType") = Value
        End Set
    End Property
    Private Property UsiaAsset() As String
        Get
            Return CType(ViewState("UsiaAsset"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("UsiaAsset") = Value
        End Set
    End Property
    Private Property LoadingRate() As String
        Get
            Return CType(ViewState("LoadingRate"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("LoadingRate") = Value
        End Set
    End Property
    Private Property DtCoverageType() As DataTable
        Get
            Return CType(ViewState("DtCoverageType"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DtCoverageType") = Value
        End Set
    End Property

    Private Property Command() As String
        Get
            Return CStr(ViewState("Command"))
        End Get
        Set(ByVal Value As String)
            ViewState("Command") = Value
        End Set
    End Property

    Private Property InsCoID() As String
        Get
            Return CStr(ViewState("InsCoID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoID") = Value
        End Set
    End Property

    Private Property InsCoName() As String
        Get
            Return CStr(ViewState("InsCoName"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoName") = Value
        End Set
    End Property

    Private Property InsCoBranchID() As String
        Get
            Return CStr(ViewState("InsCoBranchID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchID") = Value
        End Set
    End Property

    Private Property InsCoBranchName() As String
        Get
            Return CStr(ViewState("InsCoBranchName"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchName") = Value
        End Set
    End Property
#End Region

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridBranch(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridBranch(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
      
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        'BindGridBranch(Me.SearchBy, "")
        BindGridBranch(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region " BindGridBranch "
    Sub BindGridBranch(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCustomClassInsCoBranch As New Parameter.GeneralPaging
        Dim oControllerInsCoBranch As New GeneralPagingController

        LblInsCoHOID.Text = Me.InsCoID
        lblCabangAsuransi.Text = Me.InsCoBranchName

        With oCustomClassInsCoBranch
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort.Trim
            .SpName = "spInsuranceLoadingRate"
        End With

        oCustomClassInsCoBranch = oControllerInsCoBranch.GetGeneralPaging(oCustomClassInsCoBranch)


        With oCustomClassInsCoBranch
            lblTotRec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oCustomClassInsCoBranch.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        DtgInsCoBranch.DataSource = dtvEntity
        Try
            DtgInsCoBranch.DataBind()
            DtgInsCoBranch.Visible = True
        Catch
            DtgInsCoBranch.CurrentPageIndex = 0
            DtgInsCoBranch.DataBind()
            DtgInsCoBranch.Visible = True
        End Try

        PagingFooter()
    End Sub
#End Region
    Private Sub InitialDefaultPanel()
        PnlBranch.Visible = True
        PnlInsuranceRate.Visible = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            InitialDefaultPanel()
            'PnlBranch.Visible = True
            'PnlInsuranceRate.Visible = False

            Me.sesBranchId = LTrim(Replace(sesBranchId, "'", ""))
            Me.BranchID = Me.sesBranchId

            Me.FormID = "LOADINGRATE"
            Me.InsCoID = Request.QueryString("InsCoID")
            Me.InsCoBranchName = Request.QueryString("InsCoBranchName")
            Me.InsCoName = Request.QueryString("InsCoName")
            Me.InsCoBranchID = Request.QueryString("InsCoBranchID")

            Me.SearchBy = "MaskAssID = '" & Me.InsCoID.Trim & "'"
            Me.SortBy = ""

            BindGridBranch(Me.SearchBy, "")
            BindComboRate()

            With txtUsiaAsset
                .Text = "0"
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .RangeValidatorMinimumValue = "1"
                .RangeValidatorMaximumValue = "99"
                .TextCssClass = "numberAlign small_text"
                .setErrMsg()
            End With
            With txtLoadingRate
                .Text = "0"
                .RequiredFieldValidatorEnable = True
                .RangeValidatorEnable = True
                .RangeValidatorMinimumValue = "0.01"
                .RangeValidatorMaximumValue = "99"
                .TextCssClass = "numberAlign small_text"
                .setErrMsg()
            End With
        End If
    End Sub

    Private Sub BindComboRate()
        Dim oCustomClassCombo As New Parameter.GeneralPaging
        Dim oControllerCombo As New GeneralPagingController
        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "ALL"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "Description"
            .SpName = "spGetCoverageType"
        End With
        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)
        DtCoverageType = oCustomClassCombo.ListData
        CboCoverageTypeAdd.DataSource = DtCoverageType
        CboCoverageTypeAdd.DataTextField = "description"
        CboCoverageTypeAdd.DataValueField = "ID"
        CboCoverageTypeAdd.DataBind()
    End Sub
    Public Sub Clean()
        CboCoverageTypeAdd.Enabled = True
        CboCoverageTypeAdd.SelectedIndex = -1
        txtUsiaAsset.Text = Nothing
        txtLoadingRate.Text = Nothing
    End Sub
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Me.Command = "Add"
        PnlBranch.Visible = False
        PnlInsuranceRate.Visible = True
        Clean()
    End Sub

    Protected Sub btnSaveAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAdd.Click
        SaveLoadingRate()

    End Sub
    Private Sub SaveLoadingRate()
        Dim customClass As New Parameter.eInsuranceCompany

        If Me.Command = "Add" Then
            With customClass
                .strConnection = GetConnectionString()
                .FormID = Me.FormID
                .Command = Me.Command
                .InsCoID = Me.InsCoID
                .InsCoBranchID = Me.InsCoBranchID
                .BranchId = Me.BranchID
                .CategoryType = CboCoverageTypeAdd.SelectedValue.Trim
                .UsiaAsset = txtUsiaAsset.Text.Trim
                .LoadingRate = txtLoadingRate.Text.Trim
            End With
        Else
            With customClass
                .strConnection = GetConnectionString()
                .FormID = Me.FormID
                .Command = Me.Command
                .InsCoID = Me.InsCoID
                .InsCoBranchID = Me.InsCoBranchID
                .BranchId = Me.BranchID

                .CategoryTypeNew = CboCoverageTypeAdd.SelectedValue.Trim
                .UsiaAssetNew = txtUsiaAsset.Text.Trim
                .LoadingRateNew = txtLoadingRate.Text.Trim

                .CategoryType = Me.CoverageType
                .UsiaAsset = Me.UsiaAsset
                .LoadingRate = Me.LoadingRate
            End With
        End If
        Try
            customClass = m_InsCo.RateSave(customClass)
            If customClass.output = "" Then
                If Me.Command = "Add" Then
                    PnlBranch.Visible = True
                    PnlInsuranceRate.Visible = False
                    ShowMessage(lblMessage, "Tambah Data Berhasil", False)

                Else
                    PnlBranch.Visible = True
                    PnlInsuranceRate.Visible = False
                    PnlInsuranceRate.Visible = False
                    ShowMessage(lblMessage, "Update data Berhasil", False)

                End If
            Else
                ShowMessage(lblMessage, customClass.output, True)
                lblMessage.Visible = True
                Exit Sub
            End If
        Catch ex As Exception
            ShowMessage(lblMessage, ex.Message, True)
        End Try
        

        PnlBranch.Visible = True
        PnlInsuranceRate.Visible = False
        lblMessage.Visible = True
        BindGridBranch(Me.SearchBy, "")
    End Sub

    Private Sub DtgInsCoBranch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgInsCoBranch.ItemCommand

        Select Case e.CommandName
            Case "Edit"
                Dim CoverageTypeAdd As String
                Dim xx As String

                CoverageTypeAdd = CType(e.Item.FindControl("LblCategoryType"), Label).Text.Trim
                CboCoverageTypeAdd.SelectedIndex = CboCoverageTypeAdd.Items.IndexOf(CboCoverageTypeAdd.Items.FindByValue(CoverageTypeAdd.Trim)).ToString
                txtUsiaAsset.Text = CType(e.Item.FindControl("LblUsiaAsset"), Label).Text
                xx = CType(e.Item.FindControl("LblLoadingRate"), Label).Text
                txtLoadingRate.Text = FormatNumber(xx.ToString, 2).Trim


                CboCoverageTypeAdd.Enabled = False
                PnlBranch.Visible = False
                lblMessage.Visible = False
                PnlInsuranceRate.Visible = True
                lblEditAdd.Text = "EDIT"
                Me.Command = "Edit"

                Me.CoverageType = CboCoverageTypeAdd.SelectedValue
                Me.UsiaAsset = CType(e.Item.FindControl("LblUsiaAsset"), Label).Text
                Me.LoadingRate = txtLoadingRate.Text


            Case "Delete"
                lblMessage.Visible = False
                Dim customClass As New Parameter.eInsuranceCompany
                Dim xx As String
                Dim CoverageTypeAdd As String
                CoverageTypeAdd = CType(e.Item.FindControl("LblCategoryType"), Label).Text.Trim
                xx = CType(e.Item.FindControl("LblLoadingRate"), Label).Text
                With customClass
                    .strConnection = GetConnectionString()
                    .FormID = Me.FormID
                    .InsCoID = Me.InsCoID
                    .InsCoBranchID = Me.InsCoBranchID
                    .BranchId = Me.BranchID
                    .CategoryType = CoverageTypeAdd.Trim
                    .UsiaAsset = CType(e.Item.FindControl("LblUsiaAsset"), Label).Text.ToString
                    .LoadingRate = FormatNumber(xx.ToString, 2).Trim.ToString
                End With

                Try
                    m_InsCo.InsCoDelete(customClass)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    lblMessage.Visible = True
                Finally
                    BindGridBranch(Me.SearchBy, "")
                End Try
        End Select
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Dim Target As String = "WFInsuranceCompanyBranch"
        Dim x As String
        x = Target + ".aspx?InsCoID=" + Me.InsCoID + " &InsCoName=" + Me.InsCoName + ""
        Response.Redirect(x.Trim)

    End Sub

    Protected Sub btnCancelAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancelAdd.Click
        InitialDefaultPanel()
    End Sub

    Private Sub DtgInsCoBranch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgInsCoBranch.ItemDataBound
        Dim imbDelete As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")
        End If
    End Sub
End Class