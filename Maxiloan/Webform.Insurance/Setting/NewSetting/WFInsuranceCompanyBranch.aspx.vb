﻿#Region "Imports"
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCookiesHelper
#End Region

Public Class WFInsuranceCompanyBranch
    Inherits Maxiloan.Webform.WebBased
    Dim intLoopGrid As Integer
    Protected WithEvents UcCompanyAddressAdd As UcCompanyAddress
    Protected WithEvents UcContactPersonAdd As UcContactPerson
    Protected WithEvents ucSearchBy As UcSearchBy
    Protected WithEvents UcBankBranchAccountAdd As UcBankAccount
    Protected WithEvents UcBranchIDAdd As UcBranch
    Protected WithEvents TxtAdminFeeAdd As ucNumberFormat
    Protected WithEvents TxtSDFAdd As ucNumberFormat

    Protected WithEvents TxtFeeCp As ucNumberFormat
    Protected WithEvents TxtFeeJk As ucNumberFormat

    Protected WithEvents txtSumInsuredFrom As ucNumberFormat
    Protected WithEvents ucSumInsuredTo As ucNumberFormat
    Protected WithEvents txtKomisi As ucNumberFormat
    Protected WithEvents txtFee As ucNumberFormat
    Protected WithEvents txtPPh23 As ucNumberFormat
    Protected WithEvents txtPPN As ucNumberFormat
    Protected WithEvents TxtTplAmountFromAdd As ucNumberFormat
    Protected WithEvents TxtTplAmountToAdd As ucNumberFormat
    Protected WithEvents txtTPLRate As ucNumberFormat
    Protected WithEvents txtDist As ucNumberFormat
    Protected WithEvents txtBiayaOpex As ucNumberFormat
    Protected WithEvents txtNoClaimBonus As ucNumberFormat
#Region " Private Const "
    Dim m_InsCo As New Maxiloan.Controller.cInsuranceCompany

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1

    Private _ProdId As String

    Dim oCustomClassCoverage As New Parameter.GeneralPaging
    Dim oControllerCoverage As New GeneralPagingController


#End Region
#Region " Property "
    Private Property DtCoverageType() As DataTable
        Get
            Return CType(ViewState("DtCoverageType"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            ViewState("DtCoverageType") = Value
        End Set
    End Property

    Private Property Command() As String
        Get
            Return CStr(ViewState("Command"))
        End Get
        Set(ByVal Value As String)
            ViewState("Command") = Value
        End Set
    End Property

    Private Property InsCoID() As String
        Get
            Return CStr(ViewState("InsCoID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoID") = Value
        End Set
    End Property

    Private Property InsCoName() As String
        Get
            Return CStr(ViewState("InsCoName"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoName") = Value
        End Set
    End Property

    Private Property InsCoBranchID() As String
        Get
            Return CStr(ViewState("InsCoBranchID"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchID") = Value
        End Set
    End Property

    Private Property InsCoBranchName() As String
        Get
            Return CStr(ViewState("InsCoBranchName"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsCoBranchName") = Value
        End Set
    End Property
    'Property untuk TPL and Rate

    Private Property TPLWhereCond() As String
        Get
            Return CStr(ViewState("TPLWhereCond"))
        End Get
        Set(ByVal Value As String)
            ViewState("TPLWhereCond") = Value
        End Set
    End Property

    Private Property TPLCurrentPage() As Int16
        Get
            Return CShort(ViewState("TPLCurrentPage"))
        End Get
        Set(ByVal Value As Int16)
            ViewState("TPLCurrentPage") = Value
        End Set
    End Property

    Private Property TPLTotalPage() As Int32
        Get
            Return CInt(ViewState("TPLTotalPage"))
        End Get
        Set(ByVal Value As Int32)
            ViewState("TPLTotalPage") = Value
        End Set
    End Property

    Private Property InsAssetType() As String
        Get
            Return CStr(ViewState("InsAssetType"))
        End Get
        Set(ByVal Value As String)
            ViewState("InsAssetType") = Value
        End Set
    End Property

    Private Property AssetUsage() As String
        Get
            Return CStr(ViewState("AssetUsage"))
        End Get
        Set(ByVal Value As String)
            ViewState("AssetUsage") = Value
        End Set
    End Property

    Private Property NewUsed() As String
        Get
            Return CStr(ViewState("NewUsed"))
        End Get
        Set(ByVal Value As String)
            ViewState("NewUsed") = Value
        End Set
    End Property

    Private Property CoverageType() As String
        Get
            Return CStr(ViewState("CoverageType"))
        End Get
        Set(ByVal Value As String)
            ViewState("CoverageType") = Value
        End Set
    End Property

    Private Property BankNameState() As String
        Get
            Return CStr(ViewState("BankNameState"))
        End Get
        Set(ByVal Value As String)
            ViewState("BankNameState") = Value
        End Set
    End Property

    Private Property CmdWhereRate() As String
        Get
            Return CStr(ViewState("CmdWhereRate"))
        End Get
        Set(ByVal Value As String)
            ViewState("CmdWhereRate") = Value
        End Set
    End Property

    Private Property SortRate() As String
        Get
            Return CStr(ViewState("SortRate"))
        End Get
        Set(ByVal Value As String)
            ViewState("SortRate") = Value
        End Set
    End Property

    Private Property SortTPL() As String
        Get
            Return CStr(ViewState("SortTPL"))
        End Get
        Set(ByVal Value As String)
            ViewState("SortTPL") = Value
        End Set
    End Property


    Private Property CP() As String
        Get
            Return CStr(ViewState("CreditProtection"))
        End Get
        Set(ByVal Value As String)
            ViewState("CreditProtection") = Value
        End Set
    End Property

    Private Property JK() As String
        Get
            Return CStr(ViewState("JaminanKredit"))
        End Get
        Set(ByVal Value As String)
            ViewState("JaminanKredit") = Value
        End Set
    End Property

    Private Property ProductID() As String
        Get
            Return _ProdId
        End Get
        Set(ByVal Value As String)
            _ProdId = Value
        End Set
    End Property
#End Region

#Region " PanelAllFalse "
    Private Sub PanelAllFalse()

        PnlBranch.Visible = False
        PnlBranchAdd.Visible = False

        PnlTPL.Visible = False
        PnlTPLAdd.Visible = False

        pnlRate.Visible = False
        PnlRateAdd.Visible = False

        pnlCoverage.Visible = False
        lblMessage.Visible = False
        Me.SortTPL = ""
        Me.SortRate = ""

        PnlBranchEdit.Visible = False
    End Sub
#End Region
#Region " Navigation "

    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
            Else
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
            Else
                imbNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridBranch(Me.SearchBy, Me.SortBy)
    End Sub

    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridBranch(Me.SearchBy, Me.SortBy)
            End If
        End If
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Dim sortstr As String
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If

        BindGridBranch(Me.SearchBy, Me.SortBy)
    End Sub

#End Region
#Region " Navigation Edit"
    'Private Sub PagingFooterEdit()
    '    lblPageEdit.Text = currentPage.ToString()
    '    totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
    '    If totalPages = 0 Then

    '        lblTotPageEdit.Text = "1"
    '        rgvGoedit.MaximumValue = "1"
    '    Else
    '        lblTotPageEdit.Text = (System.Math.Ceiling(totalPages)).ToString()
    '        rgvGoedit.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
    '    End If
    '    lblTotRecEdit.Text = recordCount.ToString

    '    If currentPage = 1 Then
    '        imbPrevPageEdit.Enabled = False
    '        If totalPages > 1 Then
    '            imbNextPageEdit.Enabled = True
    '        Else
    '            imbNextPageEdit.Enabled = False
    '            imbLastPageEdit.Enabled = False
    '            imbFirstPageEdit.Enabled = False
    '        End If
    '    Else
    '        imbPrevPageEdit.Enabled = True
    '        If currentPage = totalPages Then
    '            imbNextPageEdit.Enabled = False
    '        Else
    '            imbNextPageEdit.Enabled = True
    '        End If
    '    End If
    'End Sub

    'Protected Sub NavigationLinkEdit_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
    '    Select Case e.CommandName
    '        Case "First" : currentPage = 1
    '        Case "Last" : currentPage = Int32.Parse(lblTotPageEdit.Text)
    '        Case "Next" : currentPage = Int32.Parse(lblPageEdit.Text) + 1
    '        Case "Prev" : currentPage = Int32.Parse(lblPageEdit.Text) - 1
    '    End Select
    '    BindGridBranchEdit(Me.SearchBy, Me.SortBy)
    'End Sub

    'Private Sub imbGoPageEdit_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    If IsNumeric(txtGoPageEdit.Text) Then
    '        If CType(lblTotPageEdit.Text, Integer) > 1 And CType(txtGoPageEdit.Text, Integer) <= CType(lblTotPageEdit.Text, Integer) Then
    '            currentPage = CType(txtGoPageEdit.Text, Int32)
    '            BindGridBranchEdit(Me.SearchBy, Me.SortBy)
    '        End If
    '    End If
    'End Sub

    'Protected Sub SortingEdit(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
    '    Dim sortstr As String
    '    If InStr(Me.SortBy, "DESC") > 0 Then
    '        Me.SortBy = e.SortExpression
    '    Else
    '        Me.SortBy = e.SortExpression + " DESC"
    '    End If

    '    BindGridBranchEdit(Me.SearchBy, Me.SortBy)
    'End Sub
#End Region

#Region " InitialDefaultPanel "
    Private Sub InitialDefaultPanel()
        PanelAllFalse()
        PnlBranch.Visible = True
        PnlBranchAdd.Visible = False
        PnlTPL.Visible = False
        PnlTPLAdd.Visible = False
        pnlCoverage.Visible = False
        pnlRate.Visible = False
        PnlRateAdd.Visible = False
        PnlBranchEdit.Visible = False
    End Sub
#End Region

#Region " BindGridBranch "
    Sub BindGridBranch(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCustomClassInsCoBranch As New Parameter.GeneralPaging
        Dim oControllerInsCoBranch As New GeneralPagingController

        LblInsCoHOID.Text = Me.InsCoID
        LblInsCoHOName.Text = Me.InsCoName

        With oCustomClassInsCoBranch
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmSort.Trim
            .SpName = "spShadowInsuranceCompanyBranchPaging"
        End With

        oCustomClassInsCoBranch = oControllerInsCoBranch.GetGeneralPaging(oCustomClassInsCoBranch)


        With oCustomClassInsCoBranch
            lblTotRec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oCustomClassInsCoBranch.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        DtgInsCoBranch.DataSource = dtvEntity
        Try
            DtgInsCoBranch.DataBind()
            DtgInsCoBranch.Visible = True
        Catch
            DtgInsCoBranch.CurrentPageIndex = 0
            DtgInsCoBranch.DataBind()
            DtgInsCoBranch.Visible = True
        End Try

        PagingFooter()
    End Sub
#End Region
#Region "BindGridBranchEdit"
    Sub BindGridBranchEdit(ByVal cmdWhere As String, ByVal cmSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCustomClassInsCoBranch As New Parameter.GeneralPaging
        Dim oControllerInsCoBranch As New GeneralPagingController

        LblInsCoHOIDEdit.Text = Me.InsCoID
        LblInsCoHONameEdit.Text = Me.InsCoName

        With oCustomClassInsCoBranch
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere
            .CurrentPage = currentPage
            .PageSize = 1000
            .SortBy = cmSort.Trim
            .SpName = "spShadowInsuranceCompanyBranchPaging"
        End With

        oCustomClassInsCoBranch = oControllerInsCoBranch.GetGeneralPaging(oCustomClassInsCoBranch)


        With oCustomClassInsCoBranch
            lblTotRec.Text = CType(.TotalRecords, String)
            lblTotPage.Text = CType(.PageSize, String)
            lblPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oCustomClassInsCoBranch.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = Me.SortBy
        DtgInsCoBranchEdit.DataSource = dtvEntity
        Try
            DtgInsCoBranchEdit.DataBind()
            DtgInsCoBranchEdit.Visible = True
        Catch
            DtgInsCoBranchEdit.CurrentPageIndex = 0
            DtgInsCoBranchEdit.DataBind()
            DtgInsCoBranchEdit.Visible = True
        End Try

        'PagingFooterEdit()
    End Sub
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        Page.Header.DataBind()
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Page.IsPostBack Then

            With TxtTplAmountFromAdd
                .RangeValidatorEnable = True
                .RangeValidatorMaximumValue = "999999999999"
                .RangeValidatorMinimumValue = "0"
                .RequiredFieldValidatorEnable = True
            End With
            With TxtTplAmountToAdd
                .RangeValidatorEnable = True
                .RangeValidatorMaximumValue = "999999999999"
                .RangeValidatorMinimumValue = "0"
                .RequiredFieldValidatorEnable = True
            End With
            With txtTPLRate
                .RangeValidatorEnable = True
                .RangeValidatorMaximumValue = "100"
                .RangeValidatorMinimumValue = "0"
                .RequiredFieldValidatorEnable = True
            End With

            InitialDefaultPanel()

            With UcBankBranchAccountAdd
                'TODO Bank Account
                '.setValidatorLookup()
                .Style = "Insurance"
                .BindBankAccount()
            End With


            Me.InsCoID = Request.QueryString("InsCoID")
            Me.InsCoName = Request.QueryString("InsCoName")

            Me.SearchBy = "InsuranceComBranch.MaskAssID = '" & Me.InsCoID.Trim & "'"
            Me.SortBy = ""

            BindGridBranch(Me.SearchBy, "")
        End If
    End Sub
#End Region
#Region "DtgInsCoBranch_ItemCommand"

    Private Sub DtgInsCoBranch_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DtgInsCoBranch.ItemCommand

        'Me.InsCoBranchID = e.Item.Cells(12).Text
        'Me.BranchID = e.Item.Cells(13).Text
        'Me.BranchName = e.Item.Cells(10).Text
        'Me.InsCoBranchName = e.Item.Cells(7).Text
        'Me.BankNameState = e.Item.Cells(14).Text

        Me.InsCoBranchID = CType(e.Item.FindControl("hpInsCoBranchID"), HyperLink).Text


        Me.BranchID = CType(e.Item.FindControl("lblBranchID"), Label).Text
        Me.BranchName = CType(e.Item.FindControl("lblBranchName"), Label).Text
        Me.InsCoBranchName = CType(e.Item.FindControl("lblMaskAssBranchName"), Label).Text
        Me.BankNameState = CType(e.Item.FindControl("lblBankName"), Label).Text

        Context.Trace.Write("Me.BankNameState = '" & Me.BankNameState & "'")

        Dim sesBID As String = Me.sesBranchId

        Me.FormID = "InsCoBranch"

        Select Case e.CommandName

            Case "Edit"

                PanelAllFalse()
                PnlBranchAdd.Visible = True

                ClearBranchAddForm()
                lblAddEditBranch.Text = "EDIT"
                lblInsCoIDAdd.Text = Me.InsCoID
                lblInsCoNameAdd.Text = Me.InsCoName
                Me.Command = "E"

                txtKomisi.RangeValidatorMaximumValue = 100
                txtKomisi.RangeValidatorEnable = True
                txtFee.RangeValidatorMaximumValue = 100
                txtFee.RangeValidatorEnable = True
                txtPPh23.RangeValidatorMaximumValue = 100
                txtPPh23.RangeValidatorEnable = True
                txtPPN.RangeValidatorMaximumValue = 100
                txtPPN.RangeValidatorEnable = True
                txtDist.RangeValidatorMaximumValue = 100
                txtDist.RangeValidatorEnable = True
                txtBiayaOpex.RangeValidatorMaximumValue = 100
                txtBiayaOpex.RangeValidatorEnable = True
                txtNoClaimBonus.RangeValidatorMaximumValue = 100
                txtBiayaOpex.RangeValidatorEnable = True
                viewbyID()



            Case "Delete"

                lblMessage.Visible = False

                Dim customClass As New Parameter.eInsuranceCompany
                With customClass
                    .strConnection = GetConnectionString()
                    .InsCoID = Me.InsCoID
                    .InsCoBranchID = Me.InsCoBranchID
                    .BranchId = Me.BranchID
                End With

                Try
                    m_InsCo.InsCoBranchDelete(customClass)

                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    lblMessage.Visible = True

                Finally
                    Me.SearchBy = "InsuranceComBranch.MaskAssID = '" & Me.InsCoID.Trim & "'"
                    Me.SortBy = ""

                    BindGridBranch(Me.SearchBy, "")
                End Try

            Case "Rate"

                currentPage = 1
                pageSize = 10
                currentPageNumber = 1
                totalPages = 1
                recordCount = 1

                PanelAllFalse()
                lblMessage.Visible = False

                Me.CmdWhereRate = "n.MaskAssID = '" & Me.InsCoID.Trim & "'"
                Me.CmdWhereRate += "AND n.MaskAssBranchID = '" & Me.InsCoBranchID.Trim & "'"
                Me.CmdWhereRate += "AND n.BranchId = '" & Me.BranchID.Trim & "'"
                Me.SortRate = ""
                BindGridRate(Me.CmdWhereRate, Me.SortRate)
                BindComboCopySource()

                Dim hpRate As HyperLink
                hpRate = CType(Me.FindControl("hpInsCoBranchIDRate"), HyperLink)
                Me.InsCoBranchID = CType(Me.FindControl("hpInsCoBranchIDRate"), HyperLink).Text
                hpRate.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Me.InsCoID.Trim & "','" & hpRate.Text.Trim & "','Insurance')"

                pnlRate.Visible = True

                BindComboRate()

            Case "TPL"

                currentPage = 1
                pageSize = 10
                currentPageNumber = 1
                totalPages = 1
                recordCount = 1

                PanelAllFalse()
                lblMessage.Visible = False
                BindTPLInsAssetType()
                BindGridTPL(Me.SortTPL)
                PnlTPL.Visible = True

            Case "COVERAGE"

                PanelAllFalse()
                lblMessage.Visible = False
                Me.Command = "L"
                BindGridCov()

                pnlCoverage.Visible = True
                btnCoverageEdit.Visible = True
                btnCoverageBack.Visible = True
                btnCoverageCancel.Visible = False
                btnCoverageSave.Visible = False

            Case "Loading"
                Dim Target As String = "LoadingRate"
                Dim x As String
                InsCoBranchID = CType(e.Item.FindControl("hpInsCoBranchID"), HyperLink).Text
                InsCoBranchName = CType(e.Item.FindControl("lblMaskAssBranchName"), Label).Text
                x = Target + ".aspx?InsCoID=" + Me.InsCoID + " &InsCoBranchName=" + InsCoBranchName + "&InsCoName=" + Me.InsCoName + "&InsCoBranchID=" + InsCoBranchID + ""
                Response.Redirect(x.Trim)
            Case "CP"
                Dim Target As String = "CreditProtection"
                Dim x As String
                InsCoID = CType(e.Item.FindControl("lblMaskAssID"), Label).Text
                InsCoBranchID = CType(e.Item.FindControl("hpInsCoBranchID"), HyperLink).Text
                BranchID = CType(e.Item.FindControl("lblBranchID"), Label).Text
                InsCoBranchName = CType(e.Item.FindControl("lblMaskAssBranchName"), Label).Text
                ProductID = "CP"
                x = Target + ".aspx?InsCoID=" + Me.InsCoID + "&BranchID=" + BranchID + "&InsCoBranchID=" + InsCoBranchID + "&InsCoBranchName=" + InsCoBranchName + "&ProductID=" + ProductID + "&InsCoName=" + Me.InsCoName
                Response.Redirect(x.Trim)
            Case "JK"
                Dim Target As String = "JaminanKredit"
                Dim x As String
                InsCoID = CType(e.Item.FindControl("lblMaskAssID"), Label).Text
                InsCoBranchID = CType(e.Item.FindControl("hpInsCoBranchID"), HyperLink).Text
                BranchID = CType(e.Item.FindControl("lblBranchID"), Label).Text
                InsCoBranchName = CType(e.Item.FindControl("lblMaskAssBranchName"), Label).Text
                ProductID = "JK"
                x = Target + ".aspx?InsCoID=" + Me.InsCoID + "&BranchID=" + BranchID + "&InsCoBranchID=" + InsCoBranchID + "&InsCoBranchName=" + InsCoBranchName + "&ProductID=" + ProductID + "&InsCoName=" + Me.InsCoName
                Response.Redirect(x.Trim)
        End Select
    End Sub

#End Region
#Region "DtgInsCoBranch_ItemDataBound"
    Private Sub DtgInsCoBranch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DtgInsCoBranch.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete As ImageButton
            Dim hp As HyperLink
            imbDelete = CType(e.Item.FindControl("ImbDelete"), ImageButton)
            imbDelete.Attributes.Add("onclick", "return fConfirm()")

            hp = CType(e.Item.FindControl("hpInsCoBranchID"), HyperLink)
            hp.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Me.InsCoID.Trim & "','" & hp.Text.Trim & "','Insurance')"
        End If
    End Sub
#End Region
#Region "SaveInsCoBranch"


    Private Sub SaveInsCoBranch()

        Dim customClass As New Parameter.eInsuranceCompany
        Dim oClassAddress As New Parameter.Address
        Dim oClassPersonal As New Parameter.Personal
        Dim oClassBankAccount As New Parameter.BankAccount

        If TxtTOPAdd.Text = "" Then TxtTOPAdd.Text = "0"
        If TxtAdminFeeAdd.Text = "" Then TxtAdminFeeAdd.Text = "0"
        If TxtSDFAdd.Text = "" Then TxtSDFAdd.Text = "0"

        With customClass
            .Command = Me.Command.Trim
            .strConnection = GetConnectionString()
            .MaskAssID = lblInsCoIDAdd.Text
            If Me.Command = "A" Then
                .BranchId = UcBranchIDAdd.BranchID.Trim
            Else
                .BranchId = Me.BranchID.Trim
            End If
            .MaskAssBranchID = TxtInsCoBranchIDAdd.Text
            .MaskAssBranchName = TxtInsCoBranchNameAdd.Text

            .TermOfPayment = TxtTOPAdd.Text
            .AdminFee = CDbl(TxtAdminFeeAdd.Text)
            .StampDutyFee = CDbl(TxtSDFAdd.Text)

            .PolisFeeCp = CDbl(TxtFeeCp.Text)
            .PolisFeeJk = CDbl(TxtFeeJk.Text)

            .LoadingFeeAllRisk = CInt(txtLoadingFeeAllRisk.Text)
            .LoadingFeeTLO = CInt(txtLoadingFeeTLO.Text)

            .Komisi = CDbl(txtKomisi.Text)
            .Fee = CDbl(txtFee.Text)
            .PPh23 = CDbl(txtPPh23.Text)
            .PPN = CDbl(txtPPN.Text)
            .PPh23CertifiedByIns = chkPPH23.Checked
            .PPNCertifiedByIns = chkPPN.Checked
            .AllRiskOptions = cboAllRiskOptions.SelectedValue
            .TLOOptions = cboTLOOptions.SelectedValue
            .Dist = CDbl(txtDist.Text)
            .BiayaOpex = CDbl(txtBiayaOpex.Text)
            .NoClaimBonus = CDbl(txtNoClaimBonus.Text)

            '.Dist = CDbl(txtDist.Text)

            .DtmUpd = Now()
        End With

        With oClassAddress
            .Address = UcCompanyAddressAdd.Address
            .RT = UcCompanyAddressAdd.RT
            .RW = UcCompanyAddressAdd.RW
            .Kecamatan = UcCompanyAddressAdd.Kecamatan
            .Kelurahan = UcCompanyAddressAdd.Kelurahan
            .City = UcCompanyAddressAdd.City
            .ZipCode = UcCompanyAddressAdd.ZipCode
            .AreaPhone1 = UcCompanyAddressAdd.AreaPhone1
            .Phone1 = UcCompanyAddressAdd.Phone1
            .AreaPhone2 = UcCompanyAddressAdd.AreaPhone2
            .Phone2 = UcCompanyAddressAdd.Phone2
            .AreaFax = UcCompanyAddressAdd.AreaFax
            .Fax = UcCompanyAddressAdd.Fax
        End With

        With oClassPersonal
            .PersonName = UcContactPersonAdd.ContactPerson
            .PersonTitle = UcContactPersonAdd.ContactPersonTitle
            .MobilePhone = UcContactPersonAdd.MobilePhone
            .Email = UcContactPersonAdd.Email
        End With

        With oClassBankAccount
            .BankBranchId = UcBankBranchAccountAdd.BankBranchId
            .BankName = UcBankBranchAccountAdd.BankID
            .BankBranch = UcBankBranchAccountAdd.BankBranch
            .BankAccountID = UcBankBranchAccountAdd.AccountNo
            .BankAccountName = UcBankBranchAccountAdd.AccountName
        End With

        customClass = m_InsCo.InsCoBranchSave(customClass, oClassAddress, oClassPersonal, oClassBankAccount)

        If customClass.output = "" Then
            If Me.Command = "A" Then
                InitialDefaultPanel()
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)

            Else
                InitialDefaultPanel()
                ShowMessage(lblMessage, "Update data Berhasil", False)

            End If
        Else
            ShowMessage(lblMessage, customClass.output, True)
            lblMessage.Visible = True
            Exit Sub
        End If
        lblMessage.Visible = True

    End Sub
#End Region

#Region "viewbyID"
    Private Sub viewbyID()
        Dim dtInsCo As New DataTable

        Dim oCustomClassInsCoBranchView As New Parameter.GeneralPaging
        Dim oControllerInsCoBranchView As New GeneralPagingController

        LblInsCoHOID.Text = Me.InsCoID
        LblInsCoHOName.Text = Me.InsCoName

        Dim where As String
        where = "InsuranceComBranch.MaskAssID = '" & Me.InsCoID.Trim & "'"
        where += " AND InsuranceComBranch.MaskAssBranchID = '" & Me.InsCoBranchID & "'"
        where += " AND InsuranceComBranch.BranchID = '" & Me.BranchID & "'"

        With oCustomClassInsCoBranchView
            .strConnection = GetConnectionString()
            .WhereCond = where
            .CurrentPage = 1
            .PageSize = 1
            .SortBy = ""
            .SpName = "spShadowInsuranceCompanyBranchPaging"
        End With

        oCustomClassInsCoBranchView = oControllerInsCoBranchView.GetGeneralPaging(oCustomClassInsCoBranchView)


        dtInsCo = oCustomClassInsCoBranchView.ListData

        'Isi semua field Edit Action
        lblInsCoIDAdd.Text = InsCoID
        If lblInsCoNameAdd.Text <> "" Then lblInsCoNameAdd.Text = InsCoName.Trim
        UcBranchIDAdd.Visible = False
        lblBranchNameAdd.Visible = True
        lblBranchNameAdd.Text = Me.BranchName
        TxtInsCoBranchIDAdd.Text = Me.InsCoBranchID
        TxtInsCoBranchIDAdd.Enabled = False
        TxtInsCoBranchNameAdd.Text = Me.InsCoBranchName

        TxtFeeCp.Text = FormatNumber(dtInsCo.Rows(0).Item("PolisFeeCreditProtection"), 0)
        TxtFeeJk.Text = FormatNumber(dtInsCo.Rows(0).Item("PolisFeeJaminanKredit"), 0)

        txtKomisi.Text = FormatNumber(dtInsCo.Rows(0).Item("Komisi"), 0)
        txtFee.Text = FormatNumber(dtInsCo.Rows(0).Item("ImbalanJasa"), 0)
        txtPPh23.Text = FormatNumber(dtInsCo.Rows(0).Item("PPh23"), 0)
        txtPPN.Text = FormatNumber(dtInsCo.Rows(0).Item("PPN"), 0)
        txtDist.Text = FormatNumber(dtInsCo.Rows(0).Item("Discount"), 0)
        txtBiayaOpex.Text = FormatNumber(dtInsCo.Rows(0).Item("BiayaOpex"), 0)
        txtNoClaimBonus.Text = FormatNumber(dtInsCo.Rows(0).Item("NoClaimBonus"), 0)
        chkPPH23.Checked = CBool(dtInsCo.Rows(0).Item("PPh23CertifiedByIns"))
        chkPPN.Checked = CBool(dtInsCo.Rows(0).Item("PPNCertifiedByIns"))
        cboAllRiskOptions.SelectedValue = CStr(dtInsCo.Rows(0).Item("AllRiskOptions"))
        cboTLOOptions.SelectedValue = CStr(dtInsCo.Rows(0).Item("TLOOptions"))


        With UcCompanyAddressAdd
            .Address = CStr(dtInsCo.Rows(0).Item("Address")).Trim
            .RT = CStr(dtInsCo.Rows(0).Item("RT")).Trim
            .RW = CStr(dtInsCo.Rows(0).Item("RW")).Trim
            .Kelurahan = CStr(dtInsCo.Rows(0).Item("Kelurahan")).Trim
            .Kecamatan = CStr(dtInsCo.Rows(0).Item("Kecamatan")).Trim
            .City = CStr(dtInsCo.Rows(0).Item("City")).Trim
            .ZipCode = CStr(dtInsCo.Rows(0).Item("ZipCode")).Trim
            .AreaPhone1 = CStr(dtInsCo.Rows(0).Item("AreaPhone1")).Trim
            .Phone1 = CStr(dtInsCo.Rows(0).Item("Phone1")).Trim
            .AreaPhone2 = CStr(dtInsCo.Rows(0).Item("AreaPhone2")).Trim
            .Phone2 = CStr(dtInsCo.Rows(0).Item("Phone2")).Trim
            .AreaFax = CStr(dtInsCo.Rows(0).Item("AreaFax1")).Trim
            .Fax = CStr(dtInsCo.Rows(0).Item("Fax1")).Trim
            .Style = "Insurance"
            .BindAddress()
        End With

        With UcContactPersonAdd
            .ContactPerson = CStr(dtInsCo.Rows(0).Item("ContactPersonName")).Trim
            .ContactPersonTitle = CStr(dtInsCo.Rows(0).Item("ContactPersonTitle")).Trim
            .MobilePhone = CStr(dtInsCo.Rows(0).Item("MobilePhone")).Trim
            .Email = CStr(dtInsCo.Rows(0).Item("Email")).Trim
            .BindContacPerson()
        End With


        SendCookiesBankName()
        With UcBankBranchAccountAdd
            .BindBankAccount()
            .BankBranchId = CStr(dtInsCo.Rows(0).Item("BankBranchID")).Trim
            .BankID = CStr(dtInsCo.Rows(0).Item("BankID")).Trim
            .BankName = CStr(dtInsCo.Rows(0).Item("BankName")).Trim
            .BankBranch = CStr(dtInsCo.Rows(0).Item("BankBranch"))
            .AccountNo = CStr(dtInsCo.Rows(0).Item("AccountNo")).Trim
            .AccountName = CStr(dtInsCo.Rows(0).Item("AccountName")).Trim
            'TODO Bank Account
            '.setValidatorLookup()
        End With

        TxtTOPAdd.Text = CStr(dtInsCo.Rows(0).Item("TermOfPayment")).Trim
        TxtAdminFeeAdd.Text = FormatNumber(dtInsCo.Rows(0).Item("AdminFee"), 0)
        TxtSDFAdd.Text = FormatNumber(dtInsCo.Rows(0).Item("StampDutyFee"))
        txtLoadingFeeAllRisk.Text = CStr(dtInsCo.Rows(0).Item("LoadingFeeAllRisk")).Trim
        txtLoadingFeeTLO.Text = CStr(dtInsCo.Rows(0).Item("LoadingFeeTLO")).Trim

    End Sub
#End Region
#Region "ClearBranchAddForm"
    Private Sub ClearBranchAddForm()

        TxtInsCoBranchIDAdd.Text = ""
        TxtInsCoBranchNameAdd.Text = ""
        TxtTOPAdd.Text = "0"
        TxtAdminFeeAdd.Text = "0"
        TxtSDFAdd.Text = "0"
        txtLoadingFeeAllRisk.Text = "0"
        txtLoadingFeeTLO.Text = "0"

        With UcCompanyAddressAdd
            .Address = ""
            .RT = ""
            .RW = ""
            .Kecamatan = ""
            .Kelurahan = ""
            .City = ""
            .ZipCode = ""
            .AreaPhone1 = ""
            .Phone1 = ""
            .AreaPhone2 = ""
            .Phone2 = ""
            .AreaFax = ""
            .Fax = ""
            .Style = "Insurance"
            .BindAddress()
        End With

        With UcContactPersonAdd
            .ContactPerson = ""
            .ContactPersonTitle = ""
            .MobilePhone = ""
            .Email = ""
            .BindContacPerson()
        End With
        With UcBankBranchAccountAdd
            .BankName = ""
            .BankBranch = ""
            .AccountNo = ""
            .AccountName = ""
            .BindBankAccount()
            '.Style = "Insurance"
        End With

    End Sub
#End Region

#Region "ImbSaveAdd_Click"

    Private Sub btnSaveAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAdd.Click
        If TxtAdminFeeAdd.Text = "" Then TxtAdminFeeAdd.Text = "0"
        If TxtSDFAdd.Text = "" Then TxtSDFAdd.Text = "0"
        'If UcBankBranchAccountAdd.BankBranchID.Trim = "0" Then
        '    ShowMessage(lblMessage, "Please update Bank Branch !!!!"
        '    lblMessage.Visible = True
        '    Exit Sub
        'End If
        SaveInsCoBranch()
        PanelAllFalse()
        lblMessage.Visible = True
        ClearBranchAddForm()
        BindGridBranch(Me.SearchBy, Me.SortBy)
        PnlBranch.Visible = True
    End Sub
#End Region
#Region "ImbAdd_Click"

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Me.Command = "A"
        PanelAllFalse()
        lblInsCoIDAdd.Text = Me.InsCoID

        lblInsCoIDAdd.Visible = False
        HpInsCoIDAdd.Text = lblInsCoIDAdd.Text

        Context.Trace.Write("HpInsCoIDAdd.Text = " & HpInsCoIDAdd.Text)

        'Dim hpRate As HyperLink
        'hpRate = CType(Me.FindControl("hpInsCoBranchIDRate"), HyperLink)
        'HpInsCoIDAdd.NavigateUrl = "javascript:OpenViewInsCoBranch('" & Me.InsCoID.Trim & "','" & hpRate.Text.Trim & "','Insurance')"
        lblAddEditBranch.Text = "ADD"
        lblInsCoNameAdd.Text = Me.InsCoName

        UcBranchIDAdd.Visible = True
        lblBranchNameAdd.Visible = False

        ClearBranchAddForm()
        PnlBranchAdd.Visible = True
    End Sub
#End Region
#Region "Edit Is Active"
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        PnlBranch.Visible = False
        PnlBranchEdit.Visible = True

        InsCoID = LblInsCoHOID.Text
        InsCoName = LblInsCoHOName.Text
        SearchBy = "InsuranceComBranch.MaskAssID = '" & Me.InsCoID.Trim & "'"
        SortBy = ""

        BindGridBranchEdit(Me.SearchBy, "")
    End Sub
#End Region
#Region "Save Is Active"
    Private Sub btnSaveEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveEdit.Click
        Dim strState As String
        Dim strID As String
        Dim strBranch As String

        Dim chkStateBranch As CheckBox
        Dim lblchecked As String
        Dim lblID As Label
        Dim lblBranchIDEdit As Label

        For intLoopGrid = 0 To DtgInsCoBranchEdit.Items.Count - 1
            chkStateBranch = CType(DtgInsCoBranchEdit.Items(intLoopGrid).Cells(0).FindControl("CheckSelect"), CheckBox)
            lblID = CType(DtgInsCoBranchEdit.Items(intLoopGrid).Cells(0).FindControl("HpInsCoyID"), Label)
            lblBranchIDEdit = CType(DtgInsCoBranchEdit.Items(intLoopGrid).Cells(0).FindControl("lblBranchID"), Label)
            If chkStateBranch.Checked = True Then
                lblchecked = "1"
            Else
                lblchecked = "0"
            End If
            strState &= CStr(IIf(strState = "", "", ",")) & lblchecked.Replace("'", "")
            strID &= CStr(IIf(strID = "", "", ",")) & lblID.Text.Trim.Replace("'", "")
            strBranch &= CStr(IIf(strBranch = "", "", ",")) & lblBranchIDEdit.Text.Replace("'", "")
        Next

        Dim oClassEdit As New Parameter.eInsuranceCompany

        With oClassEdit
            .strConnection = GetConnectionString()
            .InsCoID = LblInsCoHOIDEdit.Text
            .strID = strID.Trim
            .strBranch = strBranch.Trim
            .strState = strState.Trim
            .NumRows = DtgInsCoBranchEdit.Items.Count

        End With

        oClassEdit = m_InsCo.InsCoBranchSaveEdit(oClassEdit)
        If oClassEdit.output = "" Then
            ShowMessage(lblMessage, "Update data Berhasil", False)
        Else
            ShowMessage(lblMessage, oClassEdit.output, True)
            lblMessage.Visible = True
            Exit Sub
        End If

        PanelAllFalse()
        lblMessage.Visible = True
        PnlBranchEdit.Visible = False
        PnlBranch.Visible = True

        InsCoID = LblInsCoHOIDEdit.Text
        InsCoName = LblInsCoHONameEdit.Text
        SearchBy = "InsuranceComBranch.MaskAssID = '" & Me.InsCoID.Trim & "'"
        SortBy = ""

        BindGridBranch(Me.SearchBy, "")
    End Sub
#End Region
#Region "ImbBackEdit"
    Private Sub btnBackEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackEdit.Click
        PanelAllFalse()
        lblMessage.Visible = True
        PnlBranchEdit.Visible = False
        PnlBranch.Visible = True

        InsCoID = LblInsCoHOIDEdit.Text
        InsCoName = LblInsCoHONameEdit.Text
        SearchBy = "InsuranceComBranch.MaskAssID = '" & Me.InsCoID.Trim & "'"
        SortBy = ""

        BindGridBranch(Me.SearchBy, "")
    End Sub
#End Region
#Region "ImbBack_Click"
    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("WFInsuranceCompanySetting.aspx")
    End Sub
#End Region
#Region "imgCancelAdd_Click"

    Private Sub btnCancelAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelAdd.Click
        PanelAllFalse()
        PnlBranch.Visible = True
    End Sub
#End Region
#Region "RATE"

    Private Sub BindGridRate(ByVal cmdWhere As String, ByVal cmdSort As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oRateList As New Parameter.eInsuranceRateCard
        Dim oCustomClassInsCoBranchRate As New Parameter.GeneralPaging
        Dim oControllerInsCoBranchRate As New GeneralPagingController

        LblInsCoNameRate.Text = Me.InsCoName
        hpInsCoBranchIDRate.Text = Me.InsCoBranchID
        LblInsCoBranchNameRate.Text = Me.InsCoBranchName
        LblTFSBranchRate.Text = Me.BranchName

        With oCustomClassInsCoBranchRate
            .strConnection = GetConnectionString()
            .WhereCond = cmdWhere.Trim
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmdSort.Trim
            .SpName = "spShadowInsuranceCompanyBranchRatePaging"
        End With

        oCustomClassInsCoBranchRate = oControllerInsCoBranchRate.GetGeneralPaging(oCustomClassInsCoBranchRate)


        With oCustomClassInsCoBranchRate
            lblRateTotRec.Text = CType(.TotalRecords, String)
            lblRateTotPage.Text = CType(.PageSize, String)
            lblRatePage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oCustomClassInsCoBranchRate.ListData
        dtvEntity = dtsEntity.DefaultView
        dtvEntity.Sort = cmdSort.Trim
        dtgRate.DataSource = dtvEntity
        Try
            dtgRate.DataBind()
            dtgRate.Visible = True
        Catch
            dtgRate.CurrentPageIndex = 0
            dtgRate.DataBind()
            dtgRate.Visible = True
        End Try

        PagingFooterRate()
    End Sub

    Private Sub BtnRateBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRateBack.Click
        PanelAllFalse()
        PnlBranch.Visible = True
    End Sub

#Region " Navigation Rate"

    Private Sub PagingFooterRate()
        lblRatePage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblRateTotPage.Text = "1"
            rgvRateGo.MaximumValue = "1"
        Else
            lblRateTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvRateGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblRateTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbRatePrevPage.Enabled = False
            If totalPages > 1 Then
                imbRateNextPage.Enabled = True
            Else
                imbRateNextPage.Enabled = False
                imbRateLastPage.Enabled = False
                imbRateFirstPage.Enabled = False
            End If
        Else
            imbRatePrevPage.Enabled = True
            If currentPage = totalPages Then
                imbRateNextPage.Enabled = False
            Else
                imbRateNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLinkRate_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblRateTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblRatePage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblRatePage.Text) - 1
        End Select

        BindGridRate(Me.CmdWhereRate, Me.SortRate)
    End Sub

    Private Sub BtnRateGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRateGoPage.Click
        If IsNumeric(txtRateGoPage.Text) Then
            If CType(lblRateTotPage.Text, Integer) > 1 And CType(txtRateGoPage.Text, Integer) <= CType(lblRateTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)

                BindGridRate(Me.CmdWhereRate, Me.SortRate)
            End If
        End If
    End Sub

    Protected Sub SortingRate(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Dim sortstr As String
        If InStr(Me.SortRate, "DESC") > 0 Then
            Me.SortRate = e.SortExpression
        Else
            Me.SortRate = e.SortExpression + " DESC"
        End If

        BindGridRate(Me.CmdWhereRate, Me.SortRate)

    End Sub

#End Region

    Private Sub BtnRateAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRateAdd.Click
        PanelAllFalse()

        LblInsCoBranchIDRateAdd.Text = Me.InsCoBranchID
        LblInsCoBranchNameRateAdd.Text = Me.InsCoBranchName
        lblTFSBranchRateAdd.Text = Me.BranchName
        ClearRateAddForm()
        PnlRateAdd.Visible = True
        lblAddEditRate.Text = "ADD"
        Me.Command = "A"
    End Sub

    Private Sub BindComboCopySource()

        Dim oCustomClassCombo As New Parameter.GeneralPaging
        Dim oControllerCombo As New GeneralPagingController

        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "" '"Where icb.MaskAssID = '" & Me.InsCoID.Trim & "'" u/ copy cross insco
            .CurrentPage = 1
            .PageSize = 0
            .SortBy = "MaskAssBranchName"
            .SpName = "spShadowInsuranceCompanyBranchComboSource"
        End With

        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)

        If oCustomClassCombo.TotalRecords = 0 Then
            cboInsCoBranchCopy.Items.Insert(0, "No Source")
            cboInsCoBranchCopy.Enabled = False
            btnCopy.Enabled = False
            Exit Sub
        End If

        Dim dtComboCopy As New DataTable
        dtComboCopy = oCustomClassCombo.ListData
        cboInsCoBranchCopy.Enabled = True
        cboInsCoBranchCopy.DataSource = dtComboCopy
        cboInsCoBranchCopy.DataTextField = "MaskAssBranchName"
        cboInsCoBranchCopy.DataValueField = "MaskAssBranchID"
        cboInsCoBranchCopy.DataBind()
        cboInsCoBranchCopy.Items.Insert(0, "Select One")
        cboInsCoBranchCopy.Items(0).Value = "0"

        btnCopy.Enabled = True
        btnCopy.Attributes.Add("OnClick", "return CopyRateConfirm()")

    End Sub

    Private Sub BindComboRate()

        Dim oCustomClassCombo As New Parameter.GeneralPaging
        Dim oControllerCombo As New GeneralPagingController

        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "ALL"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "Description"
            .SpName = "spInsAssetCatPaging"
        End With

        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)

        Dim dtInsAssetType As New DataTable
        dtInsAssetType = oCustomClassCombo.ListData

        CboInsAssetTypeAdd.DataSource = dtInsAssetType
        CboInsAssetTypeAdd.DataTextField = "Description"
        CboInsAssetTypeAdd.DataValueField = "InsRateCategoryID"
        CboInsAssetTypeAdd.DataBind()


        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "ALL"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "Description"
            .SpName = "spGetCoverageType"
        End With

        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)

        DtCoverageType = oCustomClassCombo.ListData

        CboCoverageTypeAdd.DataSource = DtCoverageType
        CboCoverageTypeAdd.DataTextField = "description"
        CboCoverageTypeAdd.DataValueField = "ID"
        CboCoverageTypeAdd.DataBind()


        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "ALL"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "Description"
            .SpName = "spGetAssetUsage"
        End With

        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)

        Dim dtAssetUsage As New DataTable
        dtAssetUsage = oCustomClassCombo.ListData

        CboAssetUssageAdd.DataSource = dtAssetUsage
        CboAssetUssageAdd.DataTextField = "description"
        CboAssetUssageAdd.DataValueField = "ID"
        CboAssetUssageAdd.DataBind()


        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "IsActive = 1"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "InsuranceMasterHPPRateID"
            .SpName = "spShadowInsuranceMasterHPPRateHPaging"
        End With

        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)

        Dim dtRateCard As New DataTable
        dtRateCard = oCustomClassCombo.ListData

        cboRateCardType.DataSource = dtRateCard
        cboRateCardType.DataTextField = "InsuranceMasterHPPRateDesc"
        cboRateCardType.DataValueField = "InsuranceMasterHPPRateID"
        cboRateCardType.DataBind()

    End Sub

    Private Sub ClearRateAddForm()

        CboInsAssetTypeAdd.Enabled = True
        CboAssetUssageAdd.Enabled = True
        CboNewUsedAdd.Enabled = True
        CboCoverageTypeAdd.Enabled = True
        cboRateCardType.Enabled = True

        CboInsAssetTypeAdd.ClearSelection()
        CboAssetUssageAdd.ClearSelection()
        CboNewUsedAdd.ClearSelection()
        CboCoverageTypeAdd.ClearSelection()
        cboRateCardType.ClearSelection()

        txtSumInsuredFrom.Text = ""
        ucSumInsuredTo.Text = ""

    End Sub

    Private Sub btnCancelRate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelRate.Click
        PanelAllFalse()
        pnlRate.Visible = True
    End Sub

    Private Sub btnSaveRate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveRate.Click
        SaveRate()
        pnlRate.Visible = True
    End Sub

    Private Sub SaveRate()

        Dim customClass As New Parameter.eInsuranceCompany

        With customClass
            .strConnection = GetConnectionString()
            .Command = Me.Command
            .InsCoID = Me.InsCoID.Trim
            .InsCoBranchID = Me.InsCoBranchID.Trim
            .InsCoBranchName = LblInsCoBranchNameRateAdd.Text.Trim
            .BranchId = Me.BranchID.Trim
            .insAssettype = CboInsAssetTypeAdd.SelectedItem.Value.Trim
            .AssetUssage = CboAssetUssageAdd.SelectedItem.Value.Trim
            .newused = CboNewUsedAdd.SelectedItem.Value.Trim
            .CoverageType = CboCoverageTypeAdd.SelectedItem.Value.Trim
            .RateCardType = cboRateCardType.SelectedValue.Trim
            .SumInsuredFrom = CType(txtSumInsuredFrom.Text, Decimal)
            .SumInsuredTo = CType(ucSumInsuredTo.Text, Decimal)
        End With

        customClass = m_InsCo.RateSave(customClass)
        If customClass.output = "" Then
            If Me.Command = "A" Then
                PanelAllFalse()
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
            Else
                PanelAllFalse()
                ShowMessage(lblMessage, "Update data Berhasil", False)

            End If
        Else
            ShowMessage(lblMessage, customClass.output, True)
            lblMessage.Visible = True
            Exit Sub
        End If
        Me.CmdWhereRate = "n.MaskAssID = '" & Me.InsCoID.Trim & "'"
        Me.CmdWhereRate += "AND n.MaskAssBranchID = '" & Me.InsCoBranchID.Trim & "'"
        Me.CmdWhereRate += "AND n.BranchId = '" & Me.BranchID.Trim & "'"
        Me.SortRate = ""
        BindGridRate(Me.CmdWhereRate, Me.SortRate)
        PanelAllFalse()
        pnlRate.Visible = True
        lblMessage.Visible = True

    End Sub

    Private Sub dtgRate_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgRate.ItemCommand
        Select Case e.CommandName
            Case "Edit"

                ClearRateAddForm()
                lblAddEditRate.Text = "EDIT"
                Me.Command = "E"
                LblInsCoBranchIDRateAdd.Text = Me.InsCoBranchID
                LblInsCoBranchNameRateAdd.Text = Me.InsCoBranchName
                lblTFSBranchRateAdd.Text = Me.BranchName

                CboInsAssetTypeAdd.SelectedValue = e.Item.Cells(2).Text.Trim
                CboAssetUssageAdd.SelectedValue = e.Item.Cells(4).Text.Trim
                CboNewUsedAdd.SelectedValue = e.Item.Cells(6).Text.Trim
                CboCoverageTypeAdd.SelectedValue = e.Item.Cells(8).Text.Trim
                cboRateCardType.SelectedValue = e.Item.Cells(10).Text.Trim
                txtSumInsuredFrom.Text = CDec(e.Item.Cells(12).Text.Trim).ToString
                ucSumInsuredTo.Text = CDec(e.Item.Cells(13).Text.Trim).ToString

                PanelAllFalse()

                PnlRateAdd.Visible = True

                CboInsAssetTypeAdd.Enabled = False
                CboAssetUssageAdd.Enabled = False
                CboNewUsedAdd.Enabled = False
                CboCoverageTypeAdd.Enabled = False
                cboRateCardType.Enabled = False

            Case "Delete"

                lblMessage.Visible = False

                Dim InscoRate As New Parameter.eInsuranceCompany
                With InscoRate
                    .strConnection = GetConnectionString()
                    .InsCoID = Me.InsCoID
                    .InsCoBranchID = Me.InsCoBranchID
                    .BranchId = Me.BranchID
                    .insAssettype = e.Item.Cells(2).Text.Trim
                    .AssetUssage = e.Item.Cells(4).Text.Trim
                    .newused = e.Item.Cells(6).Text.Trim
                    .CoverageType = e.Item.Cells(8).Text.Trim
                    .RateCardType = e.Item.Cells(10).Text.Trim
                End With

                Try
                    m_InsCo.RateDelete(InscoRate)
                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    lblMessage.Visible = True
                End Try

                Me.CmdWhereRate = "n.MaskAssID = '" & Me.InsCoID.Trim & "'"
                Me.CmdWhereRate += "AND n.MaskAssBranchID = '" & Me.InsCoBranchID.Trim & "'"
                Me.CmdWhereRate += "AND n.BranchId = '" & Me.BranchID.Trim & "'"
                Me.SortRate = ""
                BindGridRate(Me.CmdWhereRate, Me.SortRate)

                pnlRate.Visible = True

        End Select
    End Sub

    Private Sub dtgRate_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgRate.ItemDataBound
        Dim imbRateDel As ImageButton
        If e.Item.ItemIndex >= 0 Then
            imbRateDel = CType(e.Item.FindControl("imbRateDelete"), ImageButton)
            imbRateDel.Attributes.Add("OnClick", "return fConfirm()")
        End If
    End Sub

#Region "ImbSearch_Click"
    Private Sub BtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim StrSearchBy As String = cboSearchBy.SelectedItem.Value
        Dim StrSearchByValue As String = TxtSearchByValue.Text.Trim

        If StrSearchByValue = "" Then
            Me.CmdWhereRate = "n.MaskAssID = '" & Me.InsCoID.Trim & "'"
            Me.CmdWhereRate += "AND n.MaskAssBranchID = '" & Me.InsCoBranchID.Trim & "'"
            Me.CmdWhereRate += "AND n.BranchId = '" & Me.BranchID.Trim & "'"
            Me.SortRate = ""

        Else
            Me.CmdWhereRate = "n.MaskAssID = '" & Me.InsCoID.Trim & "'"
            Me.CmdWhereRate += "AND n.MaskAssBranchID = '" & Me.InsCoBranchID.Trim & "'"
            Me.CmdWhereRate += "AND n.BranchId = '" & Me.BranchID.Trim & "'"
            Me.SortRate = ""

            If StrSearchByValue.Trim = "new" Then StrSearchByValue = "n"
            If StrSearchByValue.Trim = "used" Then StrSearchByValue = "u"

            Me.CmdWhereRate += "AND " + StrSearchBy + " like '%" + StrSearchByValue + "%'"
            Me.SortRate = ""
        End If

        PanelAllFalse()
        BindGridRate(Me.CmdWhereRate, Me.SortRate)
        pnlRate.Visible = True
    End Sub
#End Region

#Region "Copy Standard Rate"

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCopy.Click

        Dim oInsCoRate As New Parameter.eInsuranceCompany

        Dim strInsCoBranchIDFrom As String
        Dim strBranchIDFrom As String

        strInsCoBranchIDFrom = Split(cboInsCoBranchCopy.SelectedValue.Trim, "&&")(1)
        strBranchIDFrom = Split(cboInsCoBranchCopy.SelectedValue.Trim, "&&")(0)

        If strInsCoBranchIDFrom.Trim = Me.InsCoBranchID And strBranchIDFrom.Trim = Me.BranchID Then
            ShowMessage(lblMessage, "Tidak bisa copy dari sumber yang sama, harap pilih yang lain", True)
            lblMessage.Visible = True
            Exit Sub
        End If

        With oInsCoRate
            .InsCoID = Me.InsCoID
            .InsCoBranchIDFrom = strInsCoBranchIDFrom
            .BranchIDFrom = strBranchIDFrom.Trim
            .InsCoBranchIDTo = Me.InsCoBranchID.Trim
            .BranchIDTo = Me.BranchID
            .strConnection = GetConnectionString()

        End With

        Try

            m_InsCo.RateInsCoBranchCopy(oInsCoRate)
            ShowMessage(lblMessage, "Copy Rate dari Cabang Perusahaan Asuransi Berhasil", False)
            lblMessage.Visible = True

        Catch ex As Exception
            Response.Write(ex.Message)
            ShowMessage(lblMessage, "Copy Rate dari Cabang Perusahaan Asuransi Gagal", True)
        End Try

        Me.CmdWhereRate = "n.MaskAssID = '" & Me.InsCoID.Trim & "'"
        Me.CmdWhereRate += "AND n.MaskAssBranchID = '" & Me.InsCoBranchID.Trim & "'"
        Me.CmdWhereRate += "AND n.BranchId = '" & Me.BranchID.Trim & "'"
        Me.SortRate = ""
        BindComboCopySource()
        BindGridRate(Me.CmdWhereRate, Me.SortRate)
    End Sub

#End Region

#End Region

#Region "TPL"

    Private Sub BindGridTPL(ByVal cmdsortTPL As String)
        Dim dtvEntity As DataView
        Dim dtsEntity As DataTable
        Dim oCustomClassCombo As New Parameter.GeneralPaging
        Dim oControllerCombo As New GeneralPagingController

        LblInsCoNameTPL.Text = Me.InsCoName
        LblInsCoBranchIDTPL.Text = Me.InsCoBranchID
        LblInsCoBranchNameTPL.Text = Me.InsCoBranchName
        lblTFSBranchTPL.Text = Me.BranchName

        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "nit.MaskAssID = '" & Me.InsCoID.Trim & "' AND nit.MaskAssBranchID = '" & Me.InsCoBranchID & "' AND nit.BranchID = '" & Me.BranchID & "'"
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = cmdsortTPL
            .SpName = "spShadowInsuranceCompanyBranchTPLPaging"
        End With

        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)

        With oCustomClassCombo
            lblTPLTotRec.Text = CType(.TotalRecords, String)
            lblTPLTotPage.Text = CType(.PageSize, String)
            lblTPLPage.Text = CType(.CurrentPage, String)
            recordCount = .TotalRecords
        End With

        dtsEntity = oCustomClassCombo.ListData
        dtvEntity = dtsEntity.DefaultView

        dtvEntity.Sort = cmdsortTPL
        dtgTPL.DataSource = dtvEntity
        Try
            dtgTPL.DataBind()
            dtgTPL.Visible = True
        Catch
            dtgTPL.CurrentPageIndex = 0
            dtgTPL.DataBind()
            dtgTPL.Visible = True
        End Try

        PagingFooterTPL()
    End Sub

#Region " Navigation TPL"

    Private Sub PagingFooterTPL()
        lblTPLPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then

            lblTPLTotPage.Text = "1"
            rgvTPLGo.MaximumValue = "1"
        Else
            lblTPLTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvTPLGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTPLTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbTPLPrevPage.Enabled = False
            If totalPages > 1 Then
                imbTPLNextPage.Enabled = True
            Else
                imbTPLNextPage.Enabled = False
                imbTPLLastPage.Enabled = False
                imbTPLFirstPage.Enabled = False
            End If
        Else
            imbTPLPrevPage.Enabled = True
            If currentPage = totalPages Then
                imbTPLNextPage.Enabled = False
            Else
                imbTPLNextPage.Enabled = True
            End If
        End If
    End Sub

    Protected Sub NavigationLinkTPL_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTPLTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblTPLPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblTPLPage.Text) - 1
        End Select

        BindGridTPL(Me.SortTPL)
    End Sub

    Private Sub BtnTPLGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnTPLGoPage.Click
        If IsNumeric(txtTPLGoPage.Text) Then
            If CType(lblTPLTotPage.Text, Integer) > 1 And CType(txtTPLGoPage.Text, Integer) <= CType(lblTPLTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)

                BindGridTPL(Me.SortTPL)

            End If
        End If
    End Sub

    Protected Sub SortingTPL(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Dim sortstr As String
        If InStr(Me.SortTPL, "DESC") > 0 Then
            Me.SortTPL = e.SortExpression
        Else
            Me.SortTPL = e.SortExpression + " DESC"
        End If

        BindGridTPL(Me.SortTPL)

    End Sub

#End Region
    Sub BindTPLInsAssetType()
        Dim oCustomClassCombo As New Parameter.GeneralPaging
        Dim oControllerCombo As New GeneralPagingController

        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "ALL"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "Description"
            .SpName = "spInsAssetCatPaging"
        End With

        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)

        Dim dtInsAssetType As New DataTable
        dtInsAssetType = oCustomClassCombo.ListData

        cboTPLJenisAssetAsuransi.DataSource = dtInsAssetType
        cboTPLJenisAssetAsuransi.DataTextField = "Description"
        cboTPLJenisAssetAsuransi.DataValueField = "InsRateCategoryID"
        cboTPLJenisAssetAsuransi.DataBind()

    End Sub
    Private Sub dtgTPL_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgTPL.ItemCommand

        Select Case e.CommandName

            Case "Edit"

                PanelAllFalse()

                LblInsCOBranchIdTPLAdd.Text = Me.InsCoBranchID
                LblInsCoIdTPLAdd.Text = Me.InsCoID
                LblInsCoBranchNameTPLAdd.Text = Me.InsCoBranchName
                LblTFSBranchTPLAdd.Text = Me.BranchName
                TxtTplAmountFromAdd.Text = CDec(e.Item.Cells(2).Text).ToString
                TxtTplAmountToAdd.Text = CDec(e.Item.Cells(3).Text).ToString
                txtTPLRate.Text = CDec(e.Item.Cells(4).Text).ToString
                cboTPLJenisAssetAsuransi.SelectedIndex = cboTPLJenisAssetAsuransi.Items.IndexOf(cboTPLJenisAssetAsuransi.Items.FindByValue(e.Item.Cells(6).Text))

                lblAddEditTPL.Text = "EDIT"
                cboTPLJenisAssetAsuransi.Enabled = False
                TxtTplAmountFromAdd.Enabled = False
                TxtTplAmountToAdd.Enabled = False

                PnlTPLAdd.Visible = True
                Me.Command = "E"

            Case "Delete"
                lblMessage.Visible = False

                Dim InsCoTPL As New Parameter.eInsuranceCompany


                With InsCoTPL
                    .strConnection = GetConnectionString()
                    .InsCoID = Me.InsCoID.Trim
                    .BranchId = Me.BranchID.Trim
                    .InsCoBranchID = Me.InsCoBranchID.Trim
                    .TPLAmount = CDbl(e.Item.Cells(2).Text)
                    .TPLPremium = CDbl(e.Item.Cells(3).Text)
                End With


                Try
                    m_InsCo.TPLDelete(InsCoTPL)

                    PanelAllFalse()
                    BindGridTPL(Me.SortTPL)
                    PnlTPL.Visible = True

                Catch ex As Exception
                    ShowMessage(lblMessage, ex.Message, True)
                    lblMessage.Visible = True

                Finally
                    Me.SortTPL = ""

                    BindGridTPL(Me.SortTPL)
                    PnlTPL.Visible = True
                End Try
        End Select
    End Sub

    Private Sub dtgTPL_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTPL.ItemDataBound

        If e.Item.ItemIndex >= 0 Then
            Dim imbTPLDelete As ImageButton

            imbTPLDelete = CType(e.Item.FindControl("ImbTPLDelete"), ImageButton)
            imbTPLDelete.Attributes.Add("onclick", "return fConfirm()")
        End If

    End Sub

    Private Sub BtnBackTPL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackTPL.Click
        PanelAllFalse()
        PnlBranch.Visible = True
    End Sub

    Private Sub btnAddTPL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddTPL.Click
        PanelAllFalse()
        LblInsCOBranchIdTPLAdd.Text = Me.InsCoBranchID
        LblInsCoIdTPLAdd.Text = Me.InsCoID
        LblInsCoBranchNameTPLAdd.Text = Me.InsCoBranchName
        LblTFSBranchTPLAdd.Text = Me.BranchName
        lblAddEditTPL.Text = "ADD"
        TxtTplAmountFromAdd.Text = "0"
        TxtTplAmountToAdd.Text = "0"
        txtTPLRate.Text = "0"
        cboTPLJenisAssetAsuransi.Enabled = True
        TxtTplAmountFromAdd.Enabled = True
        TxtTplAmountToAdd.Enabled = True

        PnlTPLAdd.Visible = True
        Me.Command = "A"
    End Sub

    Private Sub BtnTPLCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTPLCancel.Click
        PanelAllFalse()
        PnlTPL.Visible = True
    End Sub

    Private Sub BtnTPLSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTPLSave.Click
        SaveTPL()
    End Sub

    Private Sub SaveTPL()

        Dim customClass As New Parameter.eInsuranceCompany

        With customClass
            .strConnection = GetConnectionString()
            .Command = Me.Command
            .InsCoID = LblInsCoIdTPLAdd.Text
            .InsCoBranchID = LblInsCOBranchIdTPLAdd.Text
            .InsCoBranchName = LblInsCoBranchNameTPLAdd.Text
            .BranchId = Me.BranchID
            .AmountFrom = CDbl(TxtTplAmountFromAdd.Text)
            .AmountTo = CDbl(TxtTplAmountToAdd.Text)
            .Rate = CDbl(txtTPLRate.Text)
            .InsRateCategory = cboTPLJenisAssetAsuransi.SelectedValue
        End With

        customClass = m_InsCo.TPLSave(customClass)
        If customClass.output = "" Then
            If Me.Command = "A" Then
                PanelAllFalse()
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
            Else
                PanelAllFalse()
                ShowMessage(lblMessage, "Update data Berhasil", False)

            End If
        Else
            ShowMessage(lblMessage, customClass.output, True)
            lblMessage.Visible = True
            Exit Sub
        End If

        PanelAllFalse()
        lblMessage.Visible = True
        BindGridTPL(Me.SortTPL)
        PnlTPL.Visible = True
    End Sub

#End Region

#Region "COVERAGE"

    Private Sub BindGridCov()

        Dim oCustomClassCombo As New Parameter.GeneralPaging
        Dim oControllerCombo As New GeneralPagingController
        Dim dtvEntity As DataView
        Dim dtbl As DataTable
        lblCoverageInsCo.Text = Me.InsCoName
        lblCoverageInsCoBranchID.Text = Me.InsCoBranchID
        lblCoverageInsCoBranchName.Text = Me.InsCoBranchName
        lblCoverageTFSBranch.Text = Me.BranchName

        With oCustomClassCombo
            .strConnection = GetConnectionString()
            .WhereCond = "ALL"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = "Description"
            .SpName = "spGetCoverageType"
        End With

        oCustomClassCombo = oControllerCombo.GetGeneralPaging(oCustomClassCombo)

        DtCoverageType = oCustomClassCombo.ListData



        With oCustomClassCoverage
            .strConnection = GetConnectionString()
            .WhereCond = "coalesce(nit.MaskAssID,'" & Me.InsCoID.Trim & "') = '" & Me.InsCoID.Trim & "' AND coalesce(nit.MaskAssBranchID,'" & Me.InsCoBranchID & "') = '" & Me.InsCoBranchID & "' AND coalesce(nit.BranchID,'" & Me.BranchID & "') = '" & Me.BranchID & "'"
            .CurrentPage = 1
            .PageSize = 0
            .SortBy = ""
            .SpName = "spShadowInsuranceCompanyBranchCoveragePaging"
        End With

        oCustomClassCoverage = oControllerCoverage.GetGeneralPaging(oCustomClassCoverage)

        dtbl = oCustomClassCoverage.ListData
        dtvEntity = dtbl.DefaultView

        dtgCoverage.DataSource = dtvEntity
        Try
            dtgCoverage.DataBind()
            dtgCoverage.Visible = True
        Catch
            dtgCoverage.CurrentPageIndex = 0
            dtgCoverage.DataBind()
            dtgCoverage.Visible = True
        End Try

    End Sub

    Private Sub dtgCoverage_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgCoverage.ItemDataBound
        Dim txtCoverageRate, txtRateTop, txtRateBottom As TextBox
        Dim lblCoverageRate, lblCoverageType, lblCoverageTypeId, lblWilayah, lblWilayahId, lblRateBottom, lblRateTop As Label


        If e.Item.ItemIndex >= 0 Then
            txtCoverageRate = CType(e.Item.FindControl("txtCoverageRate"), TextBox)
            lblCoverageRate = CType(e.Item.FindControl("lblCoverageRate"), Label)

            txtRateTop = CType(e.Item.FindControl("txtRateTop"), TextBox)
            txtRateBottom = CType(e.Item.FindControl("txtRateBottom"), TextBox)
            lblCoverageType = CType(e.Item.FindControl("lblCoverageType"), Label)
            lblCoverageTypeId = CType(e.Item.FindControl("lblCoverageTypeId"), Label)
            lblWilayah = CType(e.Item.FindControl("lblWilayah"), Label)
            lblWilayahId = CType(e.Item.FindControl("lblWilayahId"), Label)
            lblRateBottom = CType(e.Item.FindControl("lblRateBottom"), Label)
            lblRateTop = CType(e.Item.FindControl("lblRateTop"), Label)






            If Me.Command = "L" Then
                txtCoverageRate.Visible = False
                txtRateTop.Visible = False
                txtRateBottom.Visible = False

                lblRateBottom.Visible = True
                lblRateTop.Visible = True
                lblCoverageRate.Visible = True
            Else
                txtCoverageRate.Visible = True
                txtRateTop.Visible = True
                txtRateBottom.Visible = True

                lblCoverageRate.Visible = False
                lblRateBottom.Visible = False
                lblRateTop.Visible = False
            End If
        End If
    End Sub

    Private Sub btnCoverageBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCoverageBack.Click
        PanelAllFalse()
        PnlBranch.Visible = True
    End Sub

    Private Sub btnCoverageEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCoverageEdit.Click


        Me.Command = "E"
        BindGridCov()
        btnCoverageEdit.Visible = False
        btnCoverageBack.Visible = False
        btnCoverageCancel.Visible = True
        btnCoverageSave.Visible = True





    End Sub

    Private Sub btnCoverageCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCoverageCancel.Click
        Me.Command = "L"
        BindGridCov()
        btnCoverageEdit.Visible = True
        btnCoverageBack.Visible = True
        btnCoverageCancel.Visible = False
        btnCoverageSave.Visible = False
    End Sub

    Private Sub btnCoverageSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCoverageSave.Click
        Dim strID As String
        Dim strRate As String
        Dim txtCoverageRate, txtRateBottom, txtRateTop As TextBox
        Dim lblID As Label
        Dim lblCoverageTypeId, lblWilayahId As Label


        Dim strCoverageType, strInsuranceAreaId, strRateBottom, strRateTop As String


        For intLoopGrid = 0 To dtgCoverage.Items.Count - 1
            txtCoverageRate = CType(dtgCoverage.Items(intLoopGrid).Cells(0).FindControl("txtCoverageRate"), TextBox)
            txtRateBottom = CType(dtgCoverage.Items(intLoopGrid).FindControl("txtRateBottom"), TextBox)
            txtRateTop = CType(dtgCoverage.Items(intLoopGrid).FindControl("txtRateTop"), TextBox)
            lblID = CType(dtgCoverage.Items(intLoopGrid).Cells(0).FindControl("lblID"), Label)
            lblWilayahId = CType(dtgCoverage.Items(intLoopGrid).FindControl("lblWilayahId"), Label)
            lblCoverageTypeId = CType(dtgCoverage.Items(intLoopGrid).FindControl("lblCoverageTypeId"), Label)
            strRateBottom &= CStr(IIf(strRateBottom = "", "", ",")) & txtRateBottom.Text.Replace("'", "")
            strRateTop &= CStr(IIf(strRateTop = "", "", ",")) & txtRateTop.Text.Replace("'", "")

            strInsuranceAreaId &= CStr(IIf(strInsuranceAreaId = "", "", ",")) & lblWilayahId.Text.Replace("'", "")
            strCoverageType &= CStr(IIf(strCoverageType = "", "", ",")) & lblCoverageTypeId.Text.Replace("'", "")
            strRate &= CStr(IIf(strRate = "", "", ",")) & txtCoverageRate.Text.Replace("'", "")
            strID &= CStr(IIf(strID = "", "", ",")) & lblID.Text.Trim.Replace("'", "")
        Next

        Dim customClass As New Parameter.eInsuranceCompany


        With customClass
            .strConnection = GetConnectionString()
            .strCoverageID = strID
            .strCoverageRate = strRate
            .strCoverageType = strCoverageType
            .strInsuranceAreaId = strInsuranceAreaId
            .strRateBottom = strRateBottom
            .strRateTop = strRateTop
            .InsCoID = Me.InsCoID.Trim
            .InsCoBranchID = Me.InsCoBranchID.Trim
            .BranchId = Me.BranchID.Trim
            .RowCount = dtgCoverage.Items.Count
        End With

        customClass = m_InsCo.CoverageSave(customClass)
        If customClass.output = "" Then
            ShowMessage(lblMessage, "Update data Berhasil", False)
        Else
            ShowMessage(lblMessage, customClass.output, True)
            lblMessage.Visible = True
            Exit Sub
        End If

        PanelAllFalse()
        lblMessage.Visible = True
        Me.Command = "L"
        BindGridCov()
        pnlCoverage.Visible = True

        btnCoverageEdit.Visible = True
        btnCoverageBack.Visible = True
        btnCoverageCancel.Visible = False
        btnCoverageSave.Visible = False
    End Sub

#End Region

#Region "SendCookies"

    Sub SendCookiesBankName()
        Dim cookie As HttpCookie = Request.Cookies(CommonCookiesHelper.COOKIES_BANK_NAME)
        If Not cookie Is Nothing Then
            cookie.Values("InsCoBranchID") = Me.InsCoBranchID
            cookie.Values("BankName") = Me.BankNameState
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie(CommonCookiesHelper.COOKIES_BANK_NAME)
            cookieNew.Values.Add("InsCoBranchID", Me.InsCoBranchID)
            cookieNew.Values.Add("BankName", Me.BankNameState)
            Response.AppendCookie(cookieNew)
        End If
    End Sub

    Sub SendCookiesTPL()
        Dim cookie As HttpCookie = Request.Cookies("TPL")
        If Not cookie Is Nothing Then
            cookie.Values("BranchID") = Me.BranchID
            cookie.Values("BranchName") = Me.BranchName
            cookie.Values("Page") = "RateCust"
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("TPL")
            cookieNew.Values.Add("BranchID", Me.BranchID)
            cookieNew.Values.Add("BranchName", Me.BranchName)
            cookieNew.Values.Add("Page", "RateCust")
            Response.AppendCookie(cookieNew)
        End If
    End Sub

    Sub SendCookiesRate()
        Dim cookie As HttpCookie = Request.Cookies("Rate")
        If Not cookie Is Nothing Then
            cookie.Values("BranchID") = Me.BranchID
            cookie.Values("BranchName") = Me.BranchName
            cookie.Values("Page") = "RateCust"
            Response.AppendCookie(cookie)
        Else
            Dim cookieNew As New HttpCookie("Rate")
            cookieNew.Values.Add("BranchID", Me.BranchID)
            cookieNew.Values.Add("BranchName", Me.BranchName)
            cookieNew.Values.Add("Page", "RateCust")
            Response.AppendCookie(cookieNew)
        End If
    End Sub

#End Region

    '#Region "Copy Standard Rate"

    '    Private Sub ImgCopyButton_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgCopyButton.Click
    '        ImgCopyButton.Attributes.Add("OnClick", "return CopyRateConfirm()")
    '        Context.Trace.Write("CheckFeature is on the way ")
    '        If CheckFeature(Me.Loginid, Me.FormID, "Copy", Me.AppId) Then
    '            If sessioninvalid() Then
    '                Exit Sub
    '            End If
    '        End If
    '        Context.Trace.Write("CheckFeature is passed ")


    '        Dim oInsCoRate As New Entities.InsCoRate
    '        Dim oInsCoRateList As New Entities.InsCoRate

    '        With oInsCoRate
    '            .InsCoIDFrom = oInscoBranch.InscoCoyID
    '            .InsCoBranchIDFrom = oInscoBranch.InscoCoyBranchID
    '            .BranchIDFrom = oInscoBranch.BranchIDFrom

    '            .InsCoBranchIDTo = Me.InsCoBranchID
    '            .InsCoIDTo = Me.InsCoID
    '            .BranchIDTo = Me.BranchID
    '            '.InsCoID = Me.InsCoID
    '            '.BranchId = Me.BranchID
    '            .strConnection = GetConnectionString
    '            'context.Trace.Write("UcInsCoBranchCopy.InsCoBranchID = " & oInscoBranch.InscoCoyBranchID)
    '            'context.Trace.Write("Me.InsCoBranchID = " & Me.InsCoBranchID)
    '            'context.Trace.Write("Me.InsCoID = " & Me.InsCoID)
    '            'context.Trace.Write("Me.BranchID= " & Me.BranchID)

    '        End With

    '        Try

    '            If oInscoBranch.BranchIDFrom = Me.BranchID Then
    '                ShowMessage(lblMessage, "Please select different Branch to be copied"
    '                lblMessage.Visible = True
    '                Exit Sub
    '            End If

    '            m_InsCo.RateInsCoBranchCopy(oInsCoRate)
    '            ShowMessage(lblMessage, "Copy Rate from InsCo Branch success"
    '            lblMessage.Visible = True

    '        Catch ex As Exception
    '            Response.Write(ex.Message)
    '            ShowMessage(lblMessage, "Copy Rate from InsCo Branch failed"
    '        End Try

    '        BindGridRate(oInsCoRate)
    '    End Sub

    '#End Region


End Class