﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class WFNewInsuranceStandardPremium
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oBranchAll As ucBranchAll
    Protected WithEvents txtTplPremium As ucNumberFormat
    Protected WithEvents txtTplAmount As ucNumberFormat
    Protected WithEvents txtSumFrom As ucNumberFormat
    Protected WithEvents txtSumTo As ucNumberFormat




    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            InitialPanelBranch()
            Me.BranchID = Me.sesBranchId
            Me.CmdWhere = " BranchID = '" & Me.BranchID.Replace("'", "") & "' "
            BindGridH(Me.CmdWhere)
        End If
    End Sub

#Region "Property"
    Private oCustomClassH As New Parameter.GeneralPaging
    Private oControllerH As New GeneralPagingController
    Private oCustomClassD As New Parameter.GeneralPaging
    Private oControllerD As New GeneralPagingController
    Private oCustomClassTPL As New Parameter.GeneralPaging
    Private oControllerTPL As New GeneralPagingController
    Private oControllerDetail As New cInsuranceRateCard
    Private oCustomClassDetail As New Parameter.eInsuranceRateCard

    Private currentPageD As Integer = 1
    Private pageSizeD As Int16 = 10
    Private currentPageNumberD As Int32 = 1
    Private totalPagesD As Double = 1
    Private recordCountD As Int64 = 1
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere") = Value
        End Set
    End Property
    Private Property SearchByD() As String
        Get
            Return CType(viewstate("SearchByD"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SearchByD") = Value
        End Set
    End Property
    Private Property SortD() As String
        Get
            Return CType(viewstate("SortD"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SortD") = Value
        End Set
    End Property
    Private Property Command() As String
        Get
            Return CType(viewstate("Command"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Command") = Value
        End Set
    End Property
#End Region

#Region "Panel"
    Sub InitialPanelBranch()
        PnlBranch.Visible = True
        PnlList.Visible = True
        pnlSearchD.Visible = False
        pnlAddEditD.Visible = False
        pnltpl.Visible = False
        pnlAddEdittpl.Visible = False
    End Sub
    Sub InitialPanelRate()
        PnlBranch.Visible = False
        PnlList.Visible = False
        pnlSearchD.Visible = True
        pnlAddEditD.Visible = False
        pnltpl.Visible = False
        pnlAddEdittpl.Visible = False
    End Sub
    Sub InitialPanelTPL()
        PnlBranch.Visible = False
        PnlList.Visible = False
        pnlSearchD.Visible = False
        pnlAddEditD.Visible = False
        pnltpl.Visible = True
        pnlAddEdittpl.Visible = False
    End Sub
    Sub InitialPanelTPLAddEdit()
        PnlBranch.Visible = False
        PnlList.Visible = False
        pnlSearchD.Visible = False
        pnlAddEditD.Visible = False
        pnltpl.Visible = False
        pnlAddEdittpl.Visible = True
    End Sub
#End Region

#Region " Navigation Detail"
    Private Sub PagingFooterD()
        lblPageD.Text = currentPageD.ToString()
        totalPagesD = Math.Ceiling(CType((recordCountD / CType(pageSizeD, Integer)), Double))
        If totalPagesD = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            lblTotPageD.Text = "1"
            rgvGoD.MaximumValue = "1"
        Else
            lblTotPageD.Text = (System.Math.Ceiling(totalPagesD)).ToString()
            rgvGoD.MaximumValue = (System.Math.Ceiling(totalPagesD)).ToString()
        End If
        lblTotRecD.Text = recordCountD.ToString

        If currentPageD = 1 Then
            imbPrevPageD.Enabled = False
            imbFirstPageD.Enabled = False
            If totalPagesD > 1 Then
                imbNextPageD.Enabled = True
                imbLastPageD.Enabled = True
            Else
                imbPrevPageD.Enabled = False
                imbNextPageD.Enabled = False
                imbLastPageD.Enabled = False
                imbFirstPageD.Enabled = False
            End If
        Else
            imbPrevPageD.Enabled = True
            imbFirstPageD.Enabled = True
            If currentPageD = totalPagesD Then
                imbNextPageD.Enabled = False
                imbLastPageD.Enabled = False
            Else
                imbLastPageD.Enabled = True
                imbNextPageD.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLinkD_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPageD = 1
            Case "Last" : currentPageD = Int32.Parse(lblTotPageD.Text)
            Case "Next" : currentPageD = Int32.Parse(lblPageD.Text) + 1
            Case "Prev" : currentPageD = Int32.Parse(lblPageD.Text) - 1
        End Select
        BindGridD(Me.SortD, Me.SearchByD)
    End Sub
    Private Sub BtnGoPageD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPageD.Click
        If IsNumeric(txtGoPageD.Text) Then
            If CType(lblTotPageD.Text, Integer) > 1 And CType(txtGoPageD.Text, Integer) <= CType(lblTotPageD.Text, Integer) Then
                currentPageD = CType(txtGoPageD.Text, Int32)
                BindGridD(Me.SortD, Me.SearchByD)
            End If
        End If
    End Sub
    Public Sub SortGridD(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortD = e.SortExpression
        Else
            Me.SortD = e.SortExpression + " DESC"
        End If
        BindGridD(Me.SortD, Me.SearchByD)
    End Sub

#End Region

#Region "Bind"

    Sub BindGridH(ByVal CmdWhere As String)
        Dim dtModel As DataTable
        With oCustomClassH
            .strConnection = GetConnectionString
            .WhereCond = Me.CmdWhere
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowNewInsuranceStdRateHPaging"
        End With
        oCustomClassH = oControllerH.GetGeneralPaging(oCustomClassH)

        If Not oCustomClassH Is Nothing Then
            dtModel = oCustomClassH.ListData
        End If
        dtgPaging.DataSource = dtModel.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
    End Sub

    Sub BindGridD(ByVal SortBy As String, ByVal SearchByD As String)
        Dim dtRate As DataTable

        With oCustomClassD
            .strConnection = GetConnectionString
            .WhereCond = SearchByD
            .CurrentPage = currentPageD
            .PageSize = pageSizeD
            .SortBy = SortBy.Trim
            .SpName = "spShadowNewInsuranceStdRateDPaging"
        End With
        oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)

        If Not oCustomClassD Is Nothing Then
            dtRate = oCustomClassD.ListData
            recordCountD = oCustomClassD.TotalRecords
            lblTotRecD.Text = recordCountD.ToString
        Else
            recordCountD = 0
            lblTotRecD.Text = recordCountD.ToString
        End If
        If recordCountD = 0 Then
            cboCopyFrom.Enabled = True
            imbCopyD.Visible = True
        Else
            cboCopyFrom.Enabled = False
            imbCopyD.Visible = False
        End If
        dtgPagingD.DataSource = dtRate.DefaultView
        dtgPagingD.CurrentPageIndex = 0
        dtgPagingD.DataBind()
        PagingFooterD()
    End Sub

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Select Case e.CommandName
            Case "Rate"
                InitialPanelRate()
                lblID1.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(3).Text
                lblDesc1.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(4).Text
                Me.SearchByD = "BranchID = '" & lblID1.Text.Trim & "'"
                Me.SortD = ""
                BindGridD(Me.SortD, Me.SearchByD)
                fillcbo()
                imbCopyD.Attributes.Add("OnClick", "return CopyPremiumRateConfirm()")
            Case "TPL"
                InitialPanelTPL()
                lblBranchIDH.Text = dtgPaging.Items(e.Item.ItemIndex).Cells(3).Text
                Me.CmdWhere = "branchid = '" & lblBranchIDH.Text.Trim & "'"
                BindGridEntity(Me.CmdWhere)
        End Select
    End Sub

    Private Sub fillcbo()
        Dim dtCopyFrom As DataTable
        With oCustomClassD
            .strConnection = GetConnectionString
            .WhereCond = "BranchID <> '" & lblID1.Text.Trim & "'"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowNewInsuranceStdRateHPaging"
        End With
        oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)
        dtCopyFrom = oCustomClassD.ListData

        With cboCopyFrom
            .DataSource = dtCopyFrom.DefaultView
            .DataValueField = "BranchID"
            .DataTextField = "BranchFullName"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With

        Dim dtInsuranceType As DataTable
        With oCustomClassD
            .strConnection = GetConnectionString
            .WhereCond = ""
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowInsuranceRateCategory"
        End With
        oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)
        dtInsuranceType = oCustomClassD.ListData

        With cboInsType
            .DataSource = dtInsuranceType.DefaultView
            .DataValueField = "InsRateCategoryID"
            .DataTextField = "Description"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With

        Dim dtmasterrate As DataTable
        With oCustomClassD
            .strConnection = GetConnectionString
            .WhereCond = "isactive = 1"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowInsuranceMasterHPPRateHPaging"
        End With
        oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)
        dtmasterrate = oCustomClassD.ListData

        With cbomasterrate
            .DataSource = dtmasterrate.DefaultView
            .DataValueField = "InsuranceMasterHPPRateID"
            .DataTextField = "InsuranceMasterHPPRateDesc"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"
        End With
    End Sub

    Private Sub dtgPagingD_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPagingD.ItemCommand
        Select Case e.CommandName
            Case "EditD"

                pnlSearchD.Visible = False
                pnlAddEditD.Visible = True
                Me.Command = "E1"

                lblAddEditD.Text = "EDIT"

                cboInsType.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(2).Text
                cboUsage.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(3).Text
                cboNewUsed.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(4).Text
                cboCoverageType.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(5).Text
                txtSumFrom.Text = CDec(dtgPagingD.Items(e.Item.ItemIndex).Cells(10).Text).ToString
                txtSumTo.Text = CDec(dtgPagingD.Items(e.Item.ItemIndex).Cells(11).Text).ToString
                cbomasterrate.SelectedValue = dtgPagingD.Items(e.Item.ItemIndex).Cells(13).Text
                cboInsType.Enabled = False
                cboUsage.Enabled = False
                cboNewUsed.Enabled = False
                cboCoverageType.Enabled = False
                cbomasterrate.Enabled = False

            Case "Delete"
                Me.Command = "D"
                Dim oReturnValue As New Parameter.eInsuranceRateCard

                With oCustomClassDetail
                    .strConnection = GetConnectionString
                    .CardID = lblID1.Text.Trim
                    .InsType = dtgPagingD.Items(e.Item.ItemIndex).Cells(2).Text
                    .UsageID = dtgPagingD.Items(e.Item.ItemIndex).Cells(3).Text
                    .NewUsed = dtgPagingD.Items(e.Item.ItemIndex).Cells(4).Text
                    .CoverageType = dtgPagingD.Items(e.Item.ItemIndex).Cells(5).Text
                    .SumFrom = CDbl(dtgPagingD.Items(e.Item.ItemIndex).Cells(10).Text)
                    .SumTo = CDbl(dtgPagingD.Items(e.Item.ItemIndex).Cells(11).Text)
                    .MasterRate = dtgPagingD.Items(e.Item.ItemIndex).Cells(13).Text
                    .Command = Me.Command
                End With

                oCustomClassDetail = oControllerDetail.GetNewInsuranceRateCardDSave(oCustomClassDetail)

                If oCustomClassDetail.output = "" Then
                    ShowMessage(lblMessage, "Hapus Data Berhasil", False)
                    InitialPanelRate()
                    Me.SortD = ""
                    Me.SearchByD = "BranchID = '" & lblID1.Text.Trim & "'"
                    BindGridD(Me.SortD, Me.SearchByD)
                Else
                    ShowMessage(lblMessage, oCustomClassDetail.output, True)
                End If
        End Select
    End Sub

    Private Sub dtgPaging_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPagingD.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete1 As ImageButton
            imbDelete1 = CType(e.Item.FindControl("imbdelete"), ImageButton)
            imbDelete1.Attributes.Add("Onclick", "return DeleteRateConfirm()")
        End If
    End Sub

#End Region

#Region "Click"
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        PnlList.Visible = True
        Me.BranchID = oBranchAll.BranchID
        Me.CmdWhere = "BranchID = '" & oBranchAll.BranchID & "'"
        BindGridH(Me.CmdWhere)
    End Sub
    Private Sub btnBackD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackD.Click
        InitialPanelBranch()
    End Sub
    Private Sub btnSearchD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchD.Click
        If cboSearchByD.SelectedValue.Trim <> "" And txtSearchByD.Text.Trim <> "" Then
            Me.SearchByD = "BranchID = '" & lblID1.Text.Trim & "' And " & cboSearchByD.SelectedValue.Trim & " = '" & txtSearchByD.Text.Trim & "'"
        Else
            Me.SearchByD = "BranchID = '" & lblID1.Text.Trim & "'"
        End If
        Me.SortD = ""
        BindGridD(Me.SortD, Me.SearchByD)
    End Sub
    Private Sub btnCancelD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelD.Click
        InitialPanelRate()
    End Sub
    Private Sub imbCopyD_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCopyD.Click
        Me.Command = "C4"
        Dim oReturnValue As New Parameter.eInsuranceRateCard
        With oCustomClassDetail
            .CardID = lblID1.Text.Trim
            .CardIDSource = cboCopyFrom.SelectedValue.Trim
            .strConnection = GetConnectionString
            .Command = Me.Command.Trim
        End With

        Dim strError As String
        strError = oControllerDetail.GetInsuranceRateCardDCopy(oCustomClassDetail)

        If strError = "" Then
            ShowMessage(lblMessage, "Copy Data Berhasil", False)
            Me.SortD = ""
            Me.SearchByD = "BranchID = '" & lblID1.Text.Trim & "'"
            BindGridD(Me.SortD, Me.SearchByD)
        Else
            ShowMessage(lblMessage, strError, True)
        End If

    End Sub
    Private Sub btnAddD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddD.Click
        pnlSearchD.Visible = False
        pnlAddEditD.Visible = True
        Me.Command = "A1"
        txtSumFrom.Text = ""
        txtSumTo.Text = ""
        cbomasterrate.ClearSelection()
        cboInsType.ClearSelection()
        cboUsage.ClearSelection()
        cboNewUsed.ClearSelection()
        cboCoverageType.ClearSelection()

        cboInsType.Enabled = True
        cboUsage.Enabled = True
        cboNewUsed.Enabled = True
        cboCoverageType.Enabled = True
        cbomasterrate.Enabled = True
        lblAddEditD.Text = "ADD"
    End Sub
    Private Sub btnSaveD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveD.Click

        With oCustomClassDetail
            .CardID = lblID1.Text.Trim
            .InsType = cboInsType.SelectedValue
            .UsageID = cboUsage.SelectedValue
            .NewUsed = cboNewUsed.SelectedValue
            .CoverageType = cboCoverageType.SelectedValue
            .MasterRate = cbomasterrate.SelectedValue
            .SumFrom = CDbl(txtSumFrom.Text.Trim)
            .SumTo = CDbl(txtSumTo.Text.Trim)
            .Command = Me.Command.Trim
            .strConnection = GetConnectionString()
        End With

        oCustomClassDetail = oControllerDetail.GetNewInsuranceRateCardDSave(oCustomClassDetail)

        If oCustomClassDetail.output = "" Then
            If Me.Command = "A1" Then
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
            Else
                ShowMessage(lblMessage, "Update data Berhasil", False)
            End If
            InitialPanelRate()
            Me.SortD = ""
            Me.SearchByD = "BranchID = '" & lblID1.Text.Trim & "'"
            BindGridD(Me.SortD, Me.SearchByD)
        Else
            ShowMessage(lblMessage, oCustomClassDetail.output, True)
        End If

    End Sub

#End Region

#Region "TPL Bind"
    Sub BindGridEntity(ByVal cmdWhere As String)
        Dim dtTPL As DataTable
        With oCustomClassTPL
            .strConnection = GetConnectionString
            .WhereCond = "branchid = '" & lblBranchIDH.Text.Trim & "'"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowNewInsuranceStdRateTPLPaging"
        End With
        oCustomClassTPL = oControllerTPL.GetGeneralPaging(oCustomClassTPL)

        If Not oCustomClassTPL Is Nothing Then
            dtTPL = oCustomClassTPL.ListData
        End If

        dtgTPL.DataSource = dtTPL.DefaultView
        dtgTPL.CurrentPageIndex = 0
        dtgTPL.DataBind()
    End Sub
    Private Sub dtgTPL_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgTPL.ItemCommand
        Select Case e.CommandName
            Case "EditTPL"
                InitialPanelTPLAddEdit()
                lblTitleAddEdit.Text = "EDIT"
                LblBranchIDD.Text = lblBranchIDH.Text
                txtTplAmount.Text = CDec(dtgTPL.Items(e.Item.ItemIndex).Cells(2).Text).ToString()
                txtTplPremium.Text = CDec(dtgTPL.Items(e.Item.ItemIndex).Cells(3).Text).ToString()
                txtTplAmount.Enabled = False
                Me.Command = "E1"

            Case "DeleteTPL"
                Dim oReturnValue As New Parameter.eInsuranceRateCard
                Me.Command = "D1"
                With oCustomClassDetail
                    .strConnection = GetConnectionString
                    .CardID = lblBranchIDH.Text.Trim
                    .TPLAmount = CDbl(dtgTPL.Items(e.Item.ItemIndex).Cells(2).Text)
                    .TPLPremium = CDbl(dtgTPL.Items(e.Item.ItemIndex).Cells(3).Text)
                End With

                Dim strError As String
                strError = oControllerDetail.GetTPLToCustDelete(oCustomClassDetail)

                If strError = "" Then
                    ShowMessage(lblMessage, "Data Sudah diHapus", True)
                    BindGridEntity(Me.CmdWhere)
                Else
                    ShowMessage(lblMessage, strError, True)
                End If
        End Select
    End Sub
    Private Sub dtgTPL_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTPL.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete2 As ImageButton
            imbDelete2 = CType(e.Item.FindControl("imbdeletetpl"), ImageButton)
            imbDelete2.Attributes.Add("Onclick", "return DeleteRateConfirm()")
        End If
    End Sub
#End Region

#Region "Click TPL"
    Private Sub btnBackPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackPage.Click
        InitialPanelBranch()
    End Sub
    Private Sub btnAddTPL1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTPL1.Click
        InitialPanelTPLAddEdit()
        txtTplAmount.Enabled = True
        lblTitleAddEdit.Text = "ADD"
        LblBranchIDD.Text = lblBranchIDH.Text
        txtTplAmount.Text = ""
        txtTplPremium.Text = ""
        Me.Command = "A1"
    End Sub
    Private Sub btnSaveTPL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveTPL.Click
        With oCustomClassDetail
            .strConnection = GetConnectionString()
            .CardID = LblBranchIDD.Text.Trim
            .TPLAmount = CDbl(txtTplAmount.Text.Trim)
            .TPLPremium = CDbl(txtTplPremium.Text.Trim)
            .Command = Me.Command
        End With

        oCustomClassDetail = oControllerDetail.GetTPLToCustSave(oCustomClassDetail)

        If oCustomClassDetail.output = "" Then
            If Me.Command = "A1" Then
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
            Else
                ShowMessage(lblMessage, "Update data Berhasil", False)
            End If
            InitialPanelTPL()
            BindGridEntity(Me.CmdWhere)
        Else
            ShowMessage(lblMessage, oCustomClassDetail.output, True)
        End If
    End Sub
    Private Sub btnCancelTPL_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelTPL.Click
        InitialPanelTPL()
    End Sub
#End Region

End Class