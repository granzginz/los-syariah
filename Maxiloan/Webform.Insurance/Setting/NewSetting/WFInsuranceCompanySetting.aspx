﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WFInsuranceCompanySetting.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.WFInsuranceCompanySetting" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcCompanyAdress" Src="../../../webform.UserController/ucCompanyAddress.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcContactPerson" Src="../../../webform.UserController/UcContactPerson.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBankAccount" Src="../../../webform.UserController/UcBankAccount.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ucLookUpTransaction" Src="../../../Webform.UserController/ucLookUpTransaction.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WFInsuranceCompanySetting</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    
    <link href="../../../include/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <script src="../../../js/jquery-1.9.1.min.js"></script>
    <script src="../../../js/jquery-ui-1.10.3.custom.min.js"></script>
    <script language="javascript" type="text/javascript">
        function fConfirm() {
            if (window.confirm("Apakah yakin mau hapus data ini ? "))
                return true;
            else
                return false;
        }
        function fback() {
            history.go(-1);
            return false;
        }	
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlList" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR PERUSAHAAN ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgInsCo" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="CABANG">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgBranch" runat="server" CommandName="ViewBranch" ImageUrl="../../../Images/iconbranch.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn HeaderText="PRODUCT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgProduct" runat="server" CommandName="ViewProduct" ImageUrl="../../../Images/normal.gif">
                                    </asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbEdit" runat="server" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="Edit"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImbDelete" runat="server" ImageUrl="../../../Images/icondelete.gif"
                                        CommandName="Delete" CausesValidation="False"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="MaskAssID" SortExpression="MaskAssID" HeaderText="ID PERS ASURANSI">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InsuranceComDescription" SortExpression="InsuranceComDescription"
                                HeaderText="NAMA PERUSAHAAN ASURANSI">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ContactPersonName" SortExpression="ContactPersonName"
                                HeaderText="KONTAK">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Phone" SortExpression="Phone" HeaderText="NO TELEPON">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsUtama" SortExpression="IsUtama" Visible="false">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" CommandName="First"
                        OnCommand="NavigationLink_Click" ImageUrl="../../../Images/grid_navbutton01.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" CommandName="Prev"
                        OnCommand="NavigationLink_Click" ImageUrl="../../../Images/grid_navbutton02.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" CommandName="Next"
                        OnCommand="NavigationLink_Click" ImageUrl="../../../Images/grid_navbutton03.png">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" CommandName="Last"
                        OnCommand="NavigationLink_Click" ImageUrl="../../../Images/grid_navbutton04.png">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" ControlToValidate="txtGoPage"
                        MinimumValue="1" ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        ControlToValidate="txtGoPage" ErrorMessage="No Halaman Salah" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddAsr" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI PERUSAHAAN ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchBy" runat="server" Width="144px" Font-Names="Verdana">
                    <asp:ListItem Value="Description" Selected="True">Nama Perusahaan</asp:ListItem>
                    <asp:ListItem Value="ContactPersonName">Kontak</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="TxtSearchByValue" runat="server" Width="168px"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="BtnCariAsr" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnResetAsr" runat="server" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAdd" runat="server">
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    PERUSAHAAN ASURANSI -
                    <asp:Label ID="lblEditAdd" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Perusahaan Asuransi</label>
                <asp:TextBox ID="txtInsCoIDAdd" runat="server" Width="100px" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Width="184px"
                    ControlToValidate="txtInsCoIDAdd" ErrorMessage="Harap isi ID Perusahaan Asuransi"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Perusahaan Asuransi</label>
                <asp:TextBox ID="txtInsCoNameAdd" runat="server" Width="291px" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv1" runat="server" Width="184px" ControlToValidate="txtInsCoNameAdd"
                    ErrorMessage="Harap isi Nama Perusahaan Asuransi" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Rekanan Utama</label>
                <asp:CheckBox ID="chkRekanan" runat="server" Text="Rekanan Utama"></asp:CheckBox>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    ALAMAT</h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccompanyadress id="UcCompanyAddress" runat="server"></uc1:uccompanyadress>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    KONTAK</h4>
            </div>
        </div>
        <div class="form_box_uc">
            <uc1:uccontactperson id="UcContactPerson" runat="server"></uc1:uccontactperson>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <h4>
                    LAINNYA</h4>
            </div>
        </div>
       
        <div class="form_box_uc">
            <uc1:uclookuptransaction id="oTrans" runat="server"></uc1:uclookuptransaction>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveAdd" runat="server" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelAdd" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
