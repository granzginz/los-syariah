﻿#Region "Imports"
Imports Maxiloan.Webform.UserController
Imports Maxiloan.Controller
#End Region

Public Class WFInsuranceMasterRate
    Inherits Maxiloan.Webform.WebBased

#Region "Constanta"

    Private currentPage As Integer = 1
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Dim oRow As DataRow

    Private currentPageD As Integer = 1
    Private pageSizeD As Int16 = 1000
    Private currentPageNumberD As Int32 = 1
    Private totalPagesD As Double = 1
    Private recordCountD As Int64 = 1
    Dim oRowD As DataRow

    Private oCustomClassH As New Parameter.GeneralPaging
    Private oControllerH As New GeneralPagingController
    Private oCustomClassD As New Parameter.GeneralPaging
    Private oControllerD As New GeneralPagingController

    Private oControllerHeader As New cInsuranceRateCard
    Private oCustomClassHeader As New Parameter.eInsuranceRateCard
    Private oControllerDetail As New cInsuranceRateCard
    Private oCustomClassDetail As New Parameter.eInsuranceRateCard


#End Region

#Region "Property"
    Private Property Command() As String
        Get
            Return CType(viewstate("Command"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Command") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(viewstate("CmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("CmdWhere") = Value
        End Set
    End Property
    Private Property SearchByH() As String
        Get
            Return CType(viewstate("SearchByH"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SearchByH") = Value
        End Set
    End Property
    Private Property SearchByD() As String
        Get
            Return CType(viewstate("SearchByD"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SearchByD") = Value
        End Set
    End Property
    Private Property SortD() As String
        Get
            Return CType(viewstate("SortD"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("SortD") = Value
        End Set
    End Property
    Private Property Flag() As String
        Get
            Return CType(viewstate("Flag"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("Flag") = Value
        End Set
    End Property

    Private Property SeqNo() As Integer
        Get
            Return CInt(ViewState("SeqNo"))
        End Get
        Set(ByVal Value As Integer)
            ViewState("SeqNo") = Value
        End Set
    End Property
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMessage.Visible = False
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not Me.IsPostBack Then
            InitialPanelH()
            Me.SearchByH = ""
            Me.SortBy = ""
            BindGridH(Me.SortBy, Me.SearchByH)
        End If
    End Sub

    Sub InitialPanelH()
        pnlHeader.Visible = True
        pnlSearchH.Visible = True
        pnlListH.Visible = True
        pnlAddEditH.Visible = False
        pnlDetail.Visible = False

        txtSearchByH.Text = ""
        txtIdH.Enabled = True
        chkIsActiveH.Checked = True
    End Sub
    Sub InitialPanelD()
        pnlHeader.Visible = False
        pnlDetail.Visible = True
        pnlSearchD.Visible = True
        pnlAddEditD.Visible = False
        Me.Flag = "N"

    End Sub

    Sub BindGridH(ByVal SortBy As String, ByVal SearchByH As String)
        Dim dtModel As DataTable

        If SearchByH = "" Then
            Me.CmdWhere = ""
        Else
            Me.CmdWhere = SearchByH
        End If
        With oCustomClassH
            .strConnection = GetConnectionString
            .WhereCond = Me.CmdWhere
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = Me.SortBy.Trim
            .SpName = "spShadowInsuranceMasterHPPRateHPaging"
        End With
        oCustomClassH = oControllerH.GetGeneralPaging(oCustomClassH)

        If Not oCustomClassH Is Nothing Then
            dtModel = oCustomClassH.ListData
            recordCount = oCustomClassH.TotalRecords
            lblTotRec.Text = recordCount.ToString
        Else
            recordCount = 0
            lblTotRec.Text = recordCount.ToString
        End If
        dtgPaging.DataSource = dtModel.DefaultView
        dtgPaging.CurrentPageIndex = 0
        dtgPaging.DataBind()
        PagingFooter()
    End Sub

#Region " Navigation "
    Private Sub PagingFooter()
        lblPage.Text = currentPage.ToString()
        totalPages = Math.Ceiling(CType((recordCount / CType(pageSize, Integer)), Double))
        If totalPages = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True)
            lblTotPage.Text = "1"
            rgvGo.MaximumValue = "1"
        Else
            lblTotPage.Text = (System.Math.Ceiling(totalPages)).ToString()
            rgvGo.MaximumValue = (System.Math.Ceiling(totalPages)).ToString()
        End If
        lblTotRec.Text = recordCount.ToString

        If currentPage = 1 Then
            imbPrevPage.Enabled = False
            imbFirstPage.Enabled = False
            If totalPages > 1 Then
                imbNextPage.Enabled = True
                imbLastPage.Enabled = True
            Else
                imbPrevPage.Enabled = False
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
                imbFirstPage.Enabled = False
            End If
        Else
            imbPrevPage.Enabled = True
            imbFirstPage.Enabled = True
            If currentPage = totalPages Then
                imbNextPage.Enabled = False
                imbLastPage.Enabled = False
            Else
                imbLastPage.Enabled = True
                imbNextPage.Enabled = True
            End If
        End If
    End Sub
    Protected Sub NavigationLink_Click(ByVal sender As Object, ByVal e As CommandEventArgs)
        Select Case e.CommandName
            Case "First" : currentPage = 1
            Case "Last" : currentPage = Int32.Parse(lblTotPage.Text)
            Case "Next" : currentPage = Int32.Parse(lblPage.Text) + 1
            Case "Prev" : currentPage = Int32.Parse(lblPage.Text) - 1
        End Select
        BindGridH(Me.SortBy, Me.SearchByH)
    End Sub
    Private Sub BtnGoPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnGoPage.Click
        If IsNumeric(txtGoPage.Text) Then
            If CType(lblTotPage.Text, Integer) > 1 And CType(txtGoPage.Text, Integer) <= CType(lblTotPage.Text, Integer) Then
                currentPage = CType(txtGoPage.Text, Int32)
                BindGridH(Me.SortBy, Me.SearchByH)
            End If
        End If
    End Sub
    Public Sub SortGrid(ByVal obj As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        If InStr(Me.SortBy, "DESC") > 0 Then
            Me.SortBy = e.SortExpression
        Else
            Me.SortBy = e.SortExpression + " DESC"
        End If
        BindGridH(Me.SortBy, Me.SearchByH)
    End Sub

#End Region

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pnlSearchH.Visible = True
        pnlListH.Visible = True

        txtGoPage.Text = ""
        Me.SortBy = ""
        If txtSearchByH.Text = "" Then
            Me.SearchByH = ""
        Else
            Me.SearchByH = cboSearchByH.SelectedValue.Trim + " like '%" & txtSearchByH.Text.Trim & "%'"
        End If
        BindGridH(Me.SortBy, Me.SearchByH)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        pnlSearchH.Visible = False
        pnlListH.Visible = False
        pnlAddEditH.Visible = True
        lblAddEditH.Text = "ADD"
        Me.Command = "A2"
        txtIdH.Text = ""
        txtDescH.Text = ""

    End Sub

    Private Sub btnSaveH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveH.Click

        With oCustomClassHeader
            .strConnection = GetConnectionString()
            .CardID = txtIdH.Text.Trim
            .CardDesc = txtDescH.Text.Trim
            .IsActive = CBool(chkIsActiveH.Checked)
            .Command = Me.Command.Trim
        End With
        oCustomClassHeader = oControllerHeader.GetInsuranceRateCardHSave(oCustomClassHeader)

        If oCustomClassHeader.output = "" Then
            If Me.Command = "A2" Then
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
                InitialPanelH()

            Else
                ShowMessage(lblMessage, "Update data Berhasil", False)
                InitialPanelH()

            End If
        Else
            ShowMessage(lblMessage, oCustomClassHeader.output, True)
        End If
    End Sub

    Private Sub dtgPaging_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPaging.ItemCommand
        Select Case e.CommandName
            Case "EditH"

                pnlSearchH.Visible = False
                pnlListH.Visible = False
                pnlAddEditH.Visible = True
                lblAddEditH.Text = "EDIT"
                Me.Command = "E2"

                txtIdH.Enabled = False
                txtIdH.Text = CType(e.Item.FindControl("lblID"), Label).Text
                txtDescH.Text = CType(e.Item.FindControl("lblDesc"), Label).Text

                chkIsActiveH.Checked = CBool(CType(e.Item.FindControl("lblIsActiveH"), Label).Text)


            Case "Rate"
                InitialPanelD()
                lblID1.Text = CType(e.Item.FindControl("lblID"), Label).Text
                lblDesc1.Text = CType(e.Item.FindControl("lblDesc"), Label).Text

                Me.SearchByD = "InsuranceMasterHPPRateID = '" & lblID1.Text.Trim & "'"

                BindGridD(Me.SearchByD)
                fillcbo()
                imbCopyD.Attributes.Add("OnClick", "return CopyPremiumRateConfirm()")

            Case "DeleteH"
                lblID1.Text = CType(e.Item.FindControl("lblID"), Label).Text
                With oCustomClassHeader
                    .strConnection = GetConnectionString()
                    .CardID = lblID1.Text.Trim
                End With

                Dim strError As String
                strError = oControllerHeader.GetInsuranceRateCardHDelete(oCustomClassHeader)

                If strError = "" Then
                    ShowMessage(lblMessage, "Hapus Data Berhasil", False)
                    BindGridH(Me.SortBy, Me.SearchByH)
                Else
                    ShowMessage(lblMessage, strError, True)
                End If
        End Select
    End Sub

    Private Sub btnCancelH_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelH.Click
        InitialPanelH()
    End Sub

    Private Sub fillcbo()
        Dim dtCopyFrom As DataTable
        With oCustomClassD
            .strConnection = GetConnectionString
            .WhereCond = "d.InsuranceMasterHPPRateID <> '" & lblID1.Text.Trim & "'"
            .CurrentPage = 1
            .PageSize = 1000
            .SortBy = ""
            .SpName = "spShadowInsuranceMasterHPPRateDSource"
        End With
        oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)
        dtCopyFrom = oCustomClassD.ListData

        With cboCopyFrom
            .DataSource = dtCopyFrom.DefaultView
            .DataValueField = "InsuranceMasterHPPRateID"
            .DataTextField = "InsuranceMasterHPPRateDesc"
            .DataBind()
            .Items.Insert(0, "Select One")
            .Items(0).Value = "0"

        End With

    End Sub

    Sub BindGridD(ByVal SearchByD As String)
        Dim dtRate As DataTable

        With oCustomClassD
            .strConnection = GetConnectionString
            .WhereCond = SearchByD
            .CurrentPage = currentPageD
            .PageSize = pageSizeD
            .SortBy = ""
            .SpName = "spShadowInsuranceMasterHPPRateDPaging"
        End With
        oCustomClassD = oControllerD.GetGeneralPaging(oCustomClassD)

        If Not oCustomClassD Is Nothing Then
            dtRate = oCustomClassD.ListData
            recordCountD = oCustomClassD.TotalRecords

        Else
            recordCountD = 0

        End If
        If recordCountD = 0 Then
            cboCopyFrom.Enabled = True
            imbCopyD.Visible = True
        Else
            cboCopyFrom.Enabled = False
            imbCopyD.Visible = False
        End If
        dtgPagingD.DataSource = dtRate.DefaultView
        dtgPagingD.CurrentPageIndex = 0
        dtgPagingD.DataBind()

    End Sub

    Private Sub imbCopyD_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imbCopyD.Click
        Dim oReturnValue As New Parameter.eInsuranceRateCard
        With oCustomClassDetail
            .CardID = lblID1.Text.Trim
            .CardIDSource = cboCopyFrom.SelectedValue.Trim
            .Command = "C2"
            .strConnection = GetConnectionString
        End With

        Dim strError As String
        strError = oControllerDetail.GetInsuranceRateCardDCopy(oCustomClassDetail)

        If strError = "" Then
            ShowMessage(lblMessage, "Copy Data Berhasil", False)

            BindGridD(Me.SearchByD)
        Else
            ShowMessage(lblMessage, strError, True)
        End If

    End Sub


    Private Sub btnAddD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddD.Click
        pnlSearchD.Visible = False
        pnlAddEditD.Visible = True
        lblAddEditD.Text = "ADD"

        txtYearNum.Enabled = True
        txtYearNum.Text = ""
        txtInsuranceRate.Text = "0"
        txtSumIns.Text = ""
        txtRateButtom.Text = "0"
        txtRateTop.Text = "0"

        Me.Command = "A2"
    End Sub

    Private Sub btnSaveD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveD.Click
        With oCustomClassDetail
            .strConnection = GetConnectionString()
            .CardID = lblID1.Text.Trim
            .RateGroup = CDbl(txtInsuranceRate.Text)
            .YearNum = CInt(txtYearNum.Text)
            .SumIns = CDbl(IIf(txtSumIns.Text.Trim = "", "0", txtSumIns.Text.Trim))
            .Command = Me.Command.Trim
            .InsuranceAreaId = cboWilayah.SelectedValue
            .OJKSumInsuredFrom = CDbl(txtOJKSumInsFrom.Text)
            .OJKSumInsuredTo = CDbl(txtOJKSumInsTo.Text)
            .RateBottom = CDbl(txtRateButtom.Text)
            .RateTop = CDbl(txtRateTop.Text)
            .SeqNo = Me.SeqNo
        End With
        oCustomClassDetail = oControllerDetail.GetInsuranceMasterRateDSave(oCustomClassDetail)

        If oCustomClassDetail.output = "" Then
            If Me.Command = "A2" Then
                ShowMessage(lblMessage, "Tambah Data Berhasil", False)
                InitialPanelD()
                Me.SearchByD = "InsuranceMasterHPPRateID = '" & lblID1.Text.Trim & "'"

                BindGridD(Me.SearchByD)
            Else
                ShowMessage(lblMessage, "Update data Berhasil", False)
                InitialPanelD()
                Me.SearchByD = "InsuranceMasterHPPRateID = '" & lblID1.Text.Trim & "'"

                BindGridD(Me.SearchByD)
            End If
        Else
            ShowMessage(lblMessage, oCustomClassDetail.output, True)
        End If
    End Sub

    Private Sub dtgPagingD_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgPagingD.ItemCommand
        Select Case e.CommandName
            Case "EditD"

                pnlSearchD.Visible = False
                pnlAddEditD.Visible = True
                Me.Command = "E2"

                lblAddEditD.Text = "EDIT"

                txtYearNum.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(2).Text
                txtInsuranceRate.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(3).Text
                txtSumIns.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(4).Text

                txtRateButtom.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(8).Text
                txtRateTop.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(9).Text

                CompareValidator1.ErrorMessage = "Harap isi dengan Angka Antara " & txtRateButtom.Text & " s/d " & txtRateTop.Text
                Comparevalidator8.ErrorMessage = "Harap isi dengan Angka Antara " & txtRateButtom.Text & " s/d " & txtRateTop.Text

                CompareValidator1.ValueToCompare = txtRateTop.Text
                Comparevalidator8.ValueToCompare = txtRateButtom.Text


                txtYearNum.Enabled = False
                cboWilayah.SelectedIndex = cboWilayah.Items.IndexOf(cboWilayah.Items.FindByValue(dtgPagingD.Items(e.Item.ItemIndex).Cells(7).Text.Trim))
                txtOJKSumInsFrom.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(5).Text
                txtOJKSumInsTo.Text = dtgPagingD.Items(e.Item.ItemIndex).Cells(6).Text
                Me.SeqNo = dtgPagingD.Items(e.Item.ItemIndex).Cells(11).Text
            Case "Delete"

                Dim oReturnValue As New Parameter.eInsuranceRateCard
                With oCustomClassDetail
                    .CardID = lblID1.Text.Trim
                    .YearNum = CInt(dtgPagingD.Items(e.Item.ItemIndex).Cells(2).Text)
                    .Command = "D2"
                    .InsuranceAreaId = CChar(dtgPagingD.Items(e.Item.ItemIndex).Cells(10).Text)
                    .SumFrom = CDbl(dtgPagingD.Items(e.Item.ItemIndex).Cells(5).Text)
                    .SumTo = CDbl(dtgPagingD.Items(e.Item.ItemIndex).Cells(6).Text)
                    .strConnection = GetConnectionString()
                End With

                Dim strError As String
                strError = oControllerDetail.GetInsuranceMasterRateDDelete(oCustomClassDetail)

                If strError = "" Then
                    'ShowMessage(lblMessage, "Hapus Data Berhasil", True)
                    ShowMessage(lblMessage, "Hapus Data Berhasil", False)
                    BindGridD(Me.SearchByD)
                Else
                    ShowMessage(lblMessage, strError, True)
                End If

        End Select
    End Sub

    Private Sub btnBackD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackD.Click
        InitialPanelH()
    End Sub

    Private Sub btnCancelD_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelD.Click
        InitialPanelD()
    End Sub

    Private Sub dtgPagingD_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPagingD.ItemDataBound
        If e.Item.ItemIndex >= 0 Then
            Dim imbDelete As ImageButton
            imbDelete = CType(e.Item.FindControl("imbDelete"), ImageButton)
            imbDelete.Attributes.Add("Onclick", "return DeleteRateConfirm()")
        End If
    End Sub

    Private Sub txtRateButtom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRateButtom.TextChanged
        If IsNumeric(txtRateButtom.Text) AndAlso IsNumeric(txtRateTop.Text) Then
            CompareValidator1.ErrorMessage = "Harap isi dengan Angka Antara " & txtRateButtom.Text & " s/d " & txtRateTop.Text
            Comparevalidator8.ErrorMessage = "Harap isi dengan Angka Antara " & txtRateButtom.Text & " s/d " & txtRateTop.Text

            CompareValidator1.ValueToCompare = txtRateTop.Text
            Comparevalidator8.ValueToCompare = txtRateButtom.Text
        End If
    End Sub

    Private Sub txtRateTop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRateTop.TextChanged
        If IsNumeric(txtRateButtom.Text) AndAlso IsNumeric(txtRateTop.Text) Then
            CompareValidator1.ErrorMessage = "Harap isi dengan Angka Antara " & txtRateButtom.Text & " s/d " & txtRateTop.Text
            Comparevalidator8.ErrorMessage = "Harap isi dengan Angka Antara " & txtRateButtom.Text & " s/d " & txtRateTop.Text

            CompareValidator1.ValueToCompare = txtRateTop.Text
            Comparevalidator8.ValueToCompare = txtRateButtom.Text
        End If

    End Sub
End Class