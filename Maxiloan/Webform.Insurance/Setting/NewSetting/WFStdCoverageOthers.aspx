﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WFStdCoverageOthers.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.WFStdCoverageOthers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WFStdCoverageOthers</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function DeleteRateConfirm() {
            if (confirm("Apakah yakin mau hapus rate ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
										
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlrate" runat="server" Width="100%" HorizontalAlign="center">
        <div class="form_title">
            <div class="form_single">
                <h3>
                    STANDARD RATE COVER LAINNYA
                </h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Cabang</label>
                <asp:Label ID="lblBranchIDH" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR STANDARD RATE COVER LAINNYA
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgRate" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        DataKeyField="CoverageID" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:BoundColumn DataField="CoverageID" HeaderText="COVERAGE ID" ItemStyle-HorizontalAlign="Left"
                                SortExpression="CoverageID" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Description" HeaderText="NAMA COVER" ItemStyle-HorizontalAlign="Left"
                                SortExpression="Description"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CoverageRate" DataFormatString="{0:N2}" HeaderText="RATE"
                                ItemStyle-HorizontalAlign="Right" SortExpression="CoverageRate"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbeditrate" runat="server" CausesValidation="False" CommandName="Edit"
                                        ImageUrl="../../../Images/iconedit.gif" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbdeleterate" runat="server" CausesValidation="False" CommandName="Delete"
                                        ImageUrl="../../../Images/iconDelete.gif" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd1" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBackPage" runat="server" CausesValidation="False" Text="Back"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEditrate" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    STANDARD RATE COVER LAINNYA -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Cabang</label>
                <asp:Label ID="lblBranchIDD" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                 <label class ="label_req">
                    Jenis Cover</label>
                <asp:DropDownList ID="cboCovType" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" InitialValue="0"
                    Display="Dynamic" ErrorMessage="Harap pilih Jenis Cover" ControlToValidate="cboCovType" CssClass="validator_general" ></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
                    Rate Cover</label>
                <asp:TextBox ID="txtrate" runat="server" Width="74px"  Columns="14"
                    MaxLength="12"></asp:TextBox>% <font color="red">*) </font>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ErrorMessage="Harap input Rate"
                    ControlToValidate="txtrate" CssClass="validator_general" ></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator3" runat="server" ErrorMessage="Rate Salah"
                    ControlToValidate="txtrate" MaximumValue="100" MinimumValue="0" Type="Double"></asp:RangeValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
