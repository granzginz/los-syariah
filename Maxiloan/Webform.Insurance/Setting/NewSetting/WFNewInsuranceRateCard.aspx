﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WFNewInsuranceRateCard.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.WFNewInsuranceRateCard" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../../../webform.UserController/ucNumberFormat.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WFNewInsuranceRateCard</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function CopyPremiumRateConfirm() {
            if (document.forms[0].cboCopyFrom.value == '0') {
                alert("Harap pilih Rate Card Asuransi terlebih Dahulu")
                return false;
            }
            else {
                if (confirm("Apakah yakin mau copy rate dari Master Rate ini?")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        function DeleteRateConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }

      				
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="lblMessage" ForeColor="red" runat="server"></asp:Label>
    <asp:Panel ID="pnlListH" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR RATE CARD HEADER ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="RateCardID" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="RATE">
                                <HeaderStyle Width="4%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbRate" runat="server" ImageUrl="../../../images/IconRate.gif"
                                        CausesValidation="False" CommandName="Rate"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="TPL">
                                <HeaderStyle Width="4%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgTPL" ImageUrl="../../../images/iconrate.gif" runat="server"
                                        CausesValidation="False" CommandName="TPL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="COVER LAIN">
                                <HeaderStyle Width="4%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <a href='WFCoverageOtherstoCust.aspx?RateCardID=<%# DataBinder.eval(Container,"DataItem.RateCardID") %>'>
                                        <asp:Image ID="imgOther" ImageUrl="../../../images/iconrate.gif" runat="server">
                                        </asp:Image>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <HeaderStyle Width="4%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="Imagebutton1" runat="server" ImageUrl="../../../images/IconEdit.gif"
                                        CausesValidation="False" CommandName="EditH"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RateCardID" HeaderText="ID">
                                <HeaderStyle HorizontalAlign="Center" Height="20px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Container.DataItem("RateCardID") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="RateCardDesc" HeaderText="NAMA RATE CARD">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblDesc" runat="server" Text='<%# Container.DataItem("RateCardDesc") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="IsActive" HeaderText="AKTIF">
                                <ItemTemplate>
                                    <asp:Label ID="lblIsActiveH" runat="server" Text='<%# Container.DataItem("IsActive") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage"
                        CssClass="validator_general" Type="Integer" MaximumValue="999999999" MinimumValue="1"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtGoPage" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAdd" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearchH" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    CARI RATE CARD HEADER ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari Berdasarkan</label>
                <asp:DropDownList ID="cboSearchByH" runat="server">
                    <asp:ListItem Value="RateCardID">ID</asp:ListItem>
                    <asp:ListItem Value="RateCardDesc">Nama Rate Card</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchByH" runat="server" Width="213px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEditH" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    RATE CARD HEADER ASURANSI -&nbsp;
                    <asp:Label ID="lblAddEditH" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID</label>
                <asp:TextBox ID="txtIdH" runat="server" Width="213px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Rate Card Header</label>
                <asp:TextBox ID="txtDescH" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Aktif</label>
                <asp:CheckBox ID="chkIsActiveH" runat="server" Checked="True" Text="True"></asp:CheckBox>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveH" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelH" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlSearchD" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR RATE CARD ASURANSI DETAIL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPagingD" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="EditD"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbDelete" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="InsuranceType" SortExpression="InsuranceType"
                                HeaderText="INS. ASSET TYPE">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="UsageId" SortExpression="UsageId" HeaderText="ASSET USAGE">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="NewUsed" SortExpression="NewUsed" HeaderText="NEW / USED">
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="CoverageType" SortExpression="CoverageType"
                                HeaderText="COVERAGE TYPE"></asp:BoundColumn>
                            <asp:BoundColumn DataField="InsuranceTypeVT" SortExpression="InsuranceTypeVT" HeaderText="JENIS ASSET ASR">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="UsageIdVT" SortExpression="UsageIdVT" HeaderText="PENGGUNAAN">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="NewUsedVT" SortExpression="NewUsedVT" HeaderText="BARU / BEKAS">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CoverageTypeVT" SortExpression="CoverageTypeVT" HeaderText="JENIS COVER">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SumInsuredFrom" SortExpression="SumInsuredFrom" HeaderText="NILAI COVER DARI">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="SumInsuredTo" SortExpression="SumInsuredTo" HeaderText="NILAI COVER SAMPAI">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="InsuranceMasterRateDesc" SortExpression="InsuranceMasterRateDesc"
                                HeaderText="MASTER ASURANSI">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="InsuranceMasterRateID" SortExpression="InsuranceMasterRateID"
                                HeaderText="INSURANCE MASTER"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPageD" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton01.png"
                        OnCommand="NavigationLinkD_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPageD" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton02.png"
                        OnCommand="NavigationLinkD_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPageD" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton03.png"
                        OnCommand="NavigationLinkD_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPageD" runat="server" CausesValidation="False" ImageUrl="../../../Images/grid_navbutton04.png"
                        OnCommand="NavigationLinkD_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPageD" runat="server" Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPageD" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGoD" runat="server" ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPageD"
                        CssClass="validator_general" Type="Integer" MaximumValue="999999999" MinimumValue="1"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGoD" runat="server" ErrorMessage="No Halaman Salah"
                        ControlToValidate="txtGoPageD" CssClass="validator_general" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPageD" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPageD" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRecD" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddD" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBackD" runat="server" CausesValidation="False" Text="Back" CssClass="small button gray">
            </asp:Button>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    RATE CARD DETAIL ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID</label>
                <asp:Label ID="lblID1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nama Rate Card Detail Asuransi</label>
                <asp:Label ID="lblDesc1" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Cari berdasarkan</label>
                <asp:DropDownList ID="cboSearchByD" runat="server">
                    <asp:ListItem Value="InsuranceTypeVT">Jenis Asset Asr</asp:ListItem>
                    <asp:ListItem Value="UsageIdVT">Penggunaan</asp:ListItem>
                    <asp:ListItem Value="NewUsedVT">Baru / Bekas</asp:ListItem>
                    <asp:ListItem Value="CoverageTypeVT">Jenis Cover</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtSearchByD" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Copy Rate Dari</label>
                <asp:DropDownList ID="cboCopyFrom" runat="server">
                </asp:DropDownList>
                <asp:ImageButton ID="imbCopyD" runat="server" CausesValidation="False" ImageUrl="../../../Images/ButtonCopy.gif">
                </asp:ImageButton>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSearchD" runat="server" CausesValidation="False" Text="Find" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEditD" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    RATE CARD DETAIL ASURANSI -&nbsp;
                    <asp:Label ID="lblAddEditD" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Asset Asuransi</label>
                <asp:DropDownList ID="cboInsType" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic"
                    ControlToValidate="cboInsType" ErrorMessage="Harap pilih Jenis Asset Asuransi"
                    InitialValue="0" CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Penggunaan</label>
                <asp:DropDownList ID="cboUsage" runat="server">
                    <%--<asp:ListItem Value="N" Selected="True">Non-Commercial</asp:ListItem>
                    <asp:ListItem Value="C">Commercial</asp:ListItem>--%>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                    ControlToValidate="cboUsage" ErrorMessage="Harap pilih Jenis Penggunaan" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Asset : Baru/Bekas</label>
                <asp:DropDownList ID="cboNewUsed" runat="server">
                    <asp:ListItem Value="N">Baru</asp:ListItem>
                    <asp:ListItem Value="U">Bekas</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jenis Cover</label>
                <asp:DropDownList ID="cboCoverageType" runat="server">
                    <asp:ListItem Value="ARK">All Risk</asp:ListItem>
                    <asp:ListItem Value="TLO">Total Lost Only</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Master Rate</label>
                <asp:DropDownList ID="cbomasterrate" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" runat="server" Display="Dynamic"
                    ControlToValidate="cbomasterrate" ErrorMessage="Harap Pilih Master Rate" InitialValue="0"
                    CssClass="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nilai Cover Dari</label>
                <%-- <asp:TextBox ID="txtSumFrom" runat="server" Width="129px" Columns="10" MaxLength="20"
                    ></asp:TextBox>
                <asp:RequiredFieldValidator ID="REQUIREDFIELDVALIDATOR1" runat="server" ErrorMessage="Harap isi Nilai Cover Dari"
                    ControlToValidate="txtSumFrom" font-name="Verdana" Font-Size="11px" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator1" runat="server" ControlToValidate="txtSumFrom"
                    ErrorMessage="Nilai Cover Dari  Salah" Type="Double" MinimumValue="0" MaximumValue="999999999999" CssClass="validator_general" ></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtSumFrom" runat="server" />
                </uc1:ucNumberFormat>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Nilai Cover Sampai</label>
                <%-- <asp:TextBox ID="txtSumTo" runat="server" Width="129px" Columns="10" MaxLength="20"
                    ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ErrorMessage="Harap isi Nilai Cover Sampai"
                    ControlToValidate="txtSumTo" font-name="Verdana" Font-Size="11px" Display="Dynamic" CssClass="validator_general" ></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator2" runat="server" ControlToValidate="txtSumTo"
                    ErrorMessage="Nilai Cover Sampai Salah" Type="Double" MinimumValue="0" MaximumValue="999999999999" CssClass="validator_general" ></asp:RangeValidator>--%>
                <uc1:ucnumberformat id="txtSumTo" runat="server" />
                </uc1:ucNumberFormat>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveD" runat="server" CausesValidation="True" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelD" runat="server" CausesValidation="False" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnltpl" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    THIRD PARTY LIABILITIES (TPL)
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Rate Card</label>
                <asp:Label ID="lblRateCardID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR TPL
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgTPL" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="TPLAmount" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="EDIT">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbedittpl" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                        CommandName="EditTPL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="DELETE">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imbdeletetpl" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                        CommandName="DeleteTPL"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="TPLAmount" SortExpression="TPLAmount" HeaderText="JUMLAH TPL"
                                DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            <asp:BoundColumn DataField="TPLPremium" SortExpression="TPLPremium" HeaderText="PREMI TPL"
                                DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnAddTPL1" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnBackPage" runat="server" CausesValidation="False" Text="Back"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlAddEdittpl" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    TPL -&nbsp;
                    <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                </h4>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    ID Rate Card</label>
                <asp:Label ID="lblRateID" runat="server"></asp:Label>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Jumlah TPL</label>
                <asp:TextBox ID="txtTplAmount" runat="server" Width="154px" MaxLength="12" Columns="14"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" ControlToValidate="txtTplAmount"
                    ErrorMessage="Harap isi Jumlah TPL" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator4" runat="server" ControlToValidate="txtTplAmount"
                    ErrorMessage="Jumlah TPL Salah" Type="Double" MinimumValue="0" MaximumValue="999999999999"
                    CssClass="validator_general"></asp:RangeValidator>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label>
                    Premi TPL</label>
                <asp:TextBox ID="txtTplPremium" runat="server" Width="154px" MaxLength="12" Columns="14"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ControlToValidate="txtTplPremium"
                    ErrorMessage="Harap isi Premi TPL" CssClass="validator_general"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="Rangevalidator3" runat="server" ControlToValidate="txtTplPremium"
                    ErrorMessage="Premi TPL Salah" Type="Double" MinimumValue="0" MaximumValue="999999999999"
                    CssClass="validator_general"></asp:RangeValidator>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSaveTPL" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="btnCancelTPL" runat="server" CausesValidation="false" Text="Cancel"
                CssClass="small button gray"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
