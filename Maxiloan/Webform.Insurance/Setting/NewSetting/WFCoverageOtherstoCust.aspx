﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WFCoverageOtherstoCust.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.WFCoverageOtherstoCust" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WFCoverageOtherstoCust</title>
    <link href="../../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../../../Maxiloan.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';

        function DeleteRateConfirm() {
            if (confirm("Apakah yakin mau hapus rate ini  ?")) {
                return true;
            }
            else {
                return false;
            }
        }
										
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <asp:Label ID="lblMessage" ForeColor="red" runat="server"></asp:Label>
        <asp:Panel ID="pnlrate" runat="server">
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        RATE COVER LAIN CUSTOMER
                    </h3>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        ID Rate Card</label>
                    <asp:Label ID="lblRateCardID" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        DAFTAR RATE COVER LAIN CUSTOMER
                    </h4>
                </div>
            </div>
            <div class="form_box_header">
                <div class="form_single">
                    <div class="grid_wrapper_ns">
                        <asp:DataGrid ID="dtgRate" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            DataKeyField="CoverageID" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                            <HeaderStyle CssClass="th" />
                            <ItemStyle CssClass="item_grid" />
                            <Columns>
                                <asp:TemplateColumn HeaderText="EDIT">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbeditrate" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconedit.gif"
                                            CommandName="Edit"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imbdeleterate" runat="server" CausesValidation="False" ImageUrl="../../../Images/iconDelete.gif"
                                            CommandName="Delete"></asp:ImageButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="CoverageID" SortExpression="CoverageID" HeaderText="COVERAGE ID"
                                    ItemStyle-HorizontalAlign="Left" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Description" SortExpression="Description" HeaderText="NAMA COVER LAIN CUSTOMER"
                                    ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CoverageRate" SortExpression="CoverageRate" HeaderText="RATE"
                                    DataFormatString="{0:N2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnAddCover" runat="server" CausesValidation="False" Text="Add" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnBackPage" runat="server" CausesValidation="False" Text="Back"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlAddEditrate" runat="server" Width="100%" HorizontalAlign="Center">
            <div class="form_title">
                <div class="form_single">
                    <h4>
                        RATE COVER LAIN CUSTOMER -&nbsp;
                        <asp:Label ID="lblTitleAddEdit" runat="server"></asp:Label>
                    </h4>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label>
                        ID Rate Card</label>
                    <asp:Label ID="lblRateID" runat="server"></asp:Label>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                   <label class ="label_req">
                        Jenis Cover</label>
                    <asp:DropDownList ID="cboCovType" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" InitialValue="0"
                        Display="Dynamic" ErrorMessage="Harap pilih Jenis Cover" ControlToValidate="cboCovType" CssClass="validator_general" ></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form_box">
                <div class="form_single">
                    <label class ="label_req">
                        Rate</label>
                    <asp:TextBox ID="txtrate" runat="server" Width="74px"  Columns="14"
                        MaxLength="12"></asp:TextBox>%
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" runat="server" ErrorMessage="Harap Input Rate"
                        ControlToValidate="txtrate" CssClass="validator_general" ></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="Rangevalidator3" runat="server" ErrorMessage="Rate Salah"
                        ControlToValidate="txtrate" MaximumValue="100" MinimumValue="0" Type="Double"></asp:RangeValidator>
                </div>
            </div>
            <div class="form_button">
                <asp:Button ID="btnSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel"
                    CssClass="small button gray"></asp:Button>
            </div>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
