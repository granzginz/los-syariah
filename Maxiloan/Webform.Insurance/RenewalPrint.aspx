﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RenewalPrint.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.RenewalPrint" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../webform.UserController/UcSearchByWithNoTable.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcBranchAll" Src="../webform.UserController/UcBranchAll.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>RenewalPrint</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="LblErrorMessages" runat="server"></asp:Label>
    <asp:panel ID="PnlSearch" runat = "server">
    <div class="form_title">
        <div class="form_single">
            <h4>
                CARI COVER UNTUK PERPANJANGAN
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cabang</label>
<%--            <asp:Label ID="LblBranchName" runat="server"></asp:Label>--%>
            <uc1:ucbranchall id="oBranch" runat="server">
                </uc1:ucbranchall>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan</label>
            <uc1:ucsearchbywithnotable id="oSearchBy" runat="server">
                </uc1:ucsearchbywithnotable>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Surat DiBuat</label>
            <asp:TextBox runat="server" ID="txtCreateDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtCreateDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue"
            CausesValidation="False"></asp:Button>
        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray"
            CausesValidation="False"></asp:Button>
    </div>
    </asp:panel>
    
    <asp:Panel ID="PnlGrid" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK SURAT PERPANJANGAN
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="3%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderTemplate>
                                       <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkStatus_CheckedChanged"
                                            AutoPostBack="True"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkStatusAgreementNo" runat="server" Width="67px" Font-Size="Smaller">
                                    </asp:CheckBox>
                                    <asp:Label ID="LblGridBranchIDHidden" runat="server" Text='<%#  container.dataitem("BranchID") %>'
                                        Visible="False">
                                    </asp:Label><br />
                                    <asp:Label ID="LblAgreementNo" runat="server" Font-Size="Smaller" Visible="False"
                                        Text='<%#  container.dataitem("AgreementNo") %>'>
                                    </asp:Label>
                                    <asp:Label ID="lblApplicationIDHidden" runat="server" Visible="False" Text='<%#  container.dataitem("ApplicationID") %>'>
                                    </asp:Label>
                                    <asp:Label ID="LblRenewalDocNoHidden" runat="server" Text='<%#  container.dataitem("RenewalDocNo") %>'
                                        Visible="False">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="EndDate" SortExpression="EndDate" HeaderText="TGL SELESAI ASR"
                                DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CoverPeriod" SortExpression="CoverPeriod" HeaderText="PERIODE COVER">
                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="" HeaderText="CUSTOMER ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnPrint" CausesValidation="False" runat="server" Text="Print" CssClass="small button blue">
            </asp:Button>
        </div>
    </asp:Panel>
    
    </form>
</body>
</html>
