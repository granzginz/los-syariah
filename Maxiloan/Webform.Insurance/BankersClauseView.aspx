﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BankersClauseView.aspx.vb" Inherits="Maxiloan.Webform.Insurance.BankersClauseView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BANKER'S CLAUSE VIEW</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <asp:Panel ID="pnlSearch" runat="server">
        <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    CETAK BANKER'S CLAUSE</h3>
            </div>
        </div>
        <div class="form_box">
            <div class="form_single">
                <label class ="label_req">
				Perusahaan Asuransi</label>            
                <asp:DropDownList ID="cboInsuranceBranch" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ReqValField" runat="server" ErrorMessage="Please Select Insurance Branch"
                ControlToValidate="cboInsuranceBranch" Display="Dynamic" CssClass ="validator_general"></asp:RequiredFieldValidator>
            </div>
        </div>        
        <div class="form_button">
            <asp:Button ID="Btnsearch" runat="server" Text="Find" CssClass="small button blue">
            </asp:Button>
            <asp:Button ID="BtnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
            </asp:Button>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDtGrid" runat="server">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR BANKER'S CLAUSE</h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="SortGrid" DataKeyField="NoClause" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="PILIH"> 
                                <ItemStyle CssClass="command_col"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgPrint" runat="server" CausesValidation="False" ImageUrl="../Images/IconPrinter.gif"
                                        CommandName="Print"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>                
                            <asp:BoundColumn DataField="TglClause" SortExpression="TglClause" HeaderText="TANGGAL">
                            </asp:BoundColumn>        
                            <asp:TemplateColumn HeaderText="No">                                
                                <ItemTemplate>                                    
                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="MaskAssBranchName" SortExpression="MaskAssBranchName" HeaderText="ASURANSI">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="Address" SortExpression="Address" HeaderText="BANK">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="JumlahKontrak" SortExpression="JumlahKontrak" HeaderText="KONTRAK">
                            </asp:BoundColumn>
                            <%--<asp:BoundColumn DataField="InsuranceComBranchID" Visible="false">--%>
                            <asp:BoundColumn DataField="MaskAssBranchID" Visible="false">
                            </asp:BoundColumn>
                        </Columns>                        
                    </asp:DataGrid>
                </div>
            </div>
        </div>        
    </asp:Panel>
    </form>
</body>
</html>
