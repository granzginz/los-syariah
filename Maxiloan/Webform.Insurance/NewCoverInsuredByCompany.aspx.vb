﻿#Region "Imports"
Imports System.Data
Imports System.Data.SqlClient
Imports System.Math
Imports System.Text
Imports Maxiloan.General
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
Imports Maxiloan.General.CommonCacheHelper
Imports Maxiloan.General.CommonSimpleRuleHelper
Imports Maxiloan.General.CommonVariableHelper
#End Region

Public Class NewCoverInsuredByCompany
    Inherits Maxiloan.Webform.WebBased

    Protected WithEvents TxtAmountCoverage As ucNumberFormat
    Protected WithEvents txtInsAdminFee As ucNumberFormat
    Protected WithEvents TxtInsStampDutyFee As ucNumberFormat






#Region "Property"
    Private Property Paid() As String
        Get
            Return (CType(viewstate("Paid"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Paid") = Value
        End Set
    End Property
    Private Property InsuranceComBranchID_lbl() As String
        Get
            Return (CType(viewstate("InsuranceComBranchID_lbl"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceComBranchID_lbl") = Value
        End Set
    End Property
    Private Property CustomerID() As String
        Get
            Return (CType(viewstate("CustomerID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerID") = Value
        End Set
    End Property
    Private Property SupplierGroupID() As String
        Get
            Return (CType(viewstate("SupplierGroupID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("SupplierGroupID") = Value
        End Set
    End Property
    Private Property CustomerName() As String
        Get
            Return (CType(viewstate("CustomerName"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("CustomerName") = Value
        End Set
    End Property
    Private Property PageSource() As String
        Get
            Return (CType(viewstate("PageSource"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PageSource") = Value
        End Set
    End Property
    Private Property AdminFeeBehavior() As String
        Get
            Return (CType(viewstate("AdminFeeBehavior"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("AdminFeeBehavior") = Value
        End Set
    End Property
    Private Property NilaiAdminFeeAwal() As Double
        Get
            Return (CType(viewstate("NilaiAdminFeeAwal"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("NilaiAdminFeeAwal") = Value
        End Set
    End Property
    Private Property StampDutyFeeBehavior() As String
        Get
            Return (CType(viewstate("StampDutyFeeBehavior"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("StampDutyFeeBehavior") = Value
        End Set
    End Property
    Private Property NilaiStampDutyFeeAwal() As Double
        Get
            Return (CType(viewstate("NilaiStampDutyFeeAwal"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("NilaiStampDutyFeeAwal") = Value
        End Set
    End Property
    Private Property Prepaid() As Double
        Get
            Return (CType(viewstate("Prepaid"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("Prepaid") = Value
        End Set
    End Property
    Private Property MaximumTenor() As String
        Get
            Return (CType(viewstate("MaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("MaximumTenor") = Value
        End Set
    End Property
    Private Property MinimumTenor() As String
        Get
            Return (CType(viewstate("MinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("MinimumTenor") = Value
        End Set
    End Property
    Private Property PBMaximumTenor() As String
        Get
            Return (CType(viewstate("PBMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PBMaximumTenor") = Value
        End Set
    End Property
    Private Property PBMinimumTenor() As String
        Get
            Return (CType(viewstate("PBMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PBMinimumTenor") = Value
        End Set
    End Property
    Private Property PMaximumTenor() As String
        Get
            Return (CType(viewstate("PMaximumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PMaximumTenor") = Value
        End Set
    End Property
    Private Property PMinimumTenor() As String
        Get
            Return (CType(viewstate("PMinimumTenor"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("PMinimumTenor") = Value
        End Set
    End Property
    Private Property ModuleID() As String
        Get
            Return (CType(viewstate("ModuleID"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ModuleID") = Value
        End Set
    End Property
    Private Property Mode() As String
        Get
            Return (CType(viewstate("Mode"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("Mode") = Value
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Private Property ApplicationTypeID() As String
        Get
            Return CType(viewstate("ApplicationTypeID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationTypeID") = Value
        End Set
    End Property
    Private Property CoverageTypeMs() As DataTable
        Get
            Return CType(viewstate("CoverageTypeMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("CoverageTypeMs") = Value
        End Set
    End Property

    Private Property PaidByCustMs() As DataTable
        Get
            Return CType(viewstate("PaidByCustMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("PaidByCustMs") = Value
        End Set
    End Property

    Private Property SRCCMs() As DataTable
        Get
            Return CType(viewstate("SRCCMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("SRCCMs") = Value
        End Set
    End Property

    Private Property TPLMs() As DataTable
        Get
            Return CType(viewstate("TPLMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("TPLMs") = Value
        End Set
    End Property
    Private Property FloodMs() As DataTable
        Get
            Return CType(viewstate("FloodMs"), DataTable)
        End Get
        Set(ByVal Value As DataTable)
            viewstate("FloodMs") = Value
        End Set
    End Property
    Private Property InsuranceComBranchID() As String
        Get
            Return CType(viewstate("InsuranceComBranchID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceComBranchID") = Value
        End Set
    End Property
    Private Property InsuranceType() As String
        Get
            Return CType(viewstate("InsuranceType"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("InsuranceType") = Value
        End Set
    End Property
    Private Property AssetUsageID() As String
        Get
            Return CType(viewstate("AssetUsageID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetUsageID") = Value
        End Set
    End Property
    Private Property AssetNewUsed() As String
        Get
            Return CType(viewstate("AssetNewUsed"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("AssetNewUsed") = Value
        End Set
    End Property
    Private Property ManufacturingYear() As String
        Get
            Return (CType(viewstate("ManufacturingYear"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("ManufacturingYear") = Value
        End Set
    End Property
    Public Property InterestType() As String
        Get
            Return (CType(viewstate("InterestType"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InterestType") = Value
        End Set
    End Property
    Public Property InstallmentScheme() As String
        Get
            Return (CType(viewstate("InstallmentScheme"), String))
        End Get
        Set(ByVal Value As String)
            viewstate("InstallmentScheme") = Value
        End Set
    End Property

    Public Property AmountCoverage() As Double
        Get
            Return (CType(viewstate("AmountCoverage"), Double))
        End Get
        Set(ByVal Value As Double)
            viewstate("AmountCoverage") = Value
        End Set
    End Property


    Private Property PremiumAmountByCustBeforeDisc() As Double
        Get
            Return CType(viewstate("PremiumAmountByCustBeforeDisc"), Double)
        End Get
        Set(ByVal Value As Double)
            viewstate("PremiumAmountByCustBeforeDisc") = Value
        End Set
    End Property

    Private Property InsLength() As Integer
        Get
            Return CType(viewstate("InsLength"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsLength") = Value
        End Set
    End Property
    Private Property MaturityDate() As DateTime
        Get
            Return CType(viewstate("MaturityDate"), DateTime)
        End Get
        Set(ByVal Value As DateTime)
            viewstate("MaturityDate") = Value
        End Set
    End Property
    Private Property IsPageSource() As Boolean
        Get
            Return CType(viewstate("IsPageSource"), Boolean)
        End Get
        Set(ByVal Value As Boolean)
            viewstate("IsPageSource") = Value
        End Set
    End Property





#End Region
#Region "Constanta"

    Private CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE As String = CommonCacheHelper.CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE
    Private oCustomClass As New Parameter.InsNewCover
    Private oController As New InsNewCoverController
    Dim m_controller As New DataUserControlController
    Private oInsAppController As New InsuranceApplicationController
    Private oCustomClassResult As New Parameter.InsuranceCalculationResult
    Private oInsCalResultController As New InsuranceCalculationResultController
    Private Const COMPANY_CUSTOMER As String = "CompanyCustomer"
    Protected WithEvents oApplicationType As UcApplicationType
    Private Const CACHE_APPLICATION_TYPE As String = "CacheApplicationType"
    '  Dim InterestType As String
    ' Dim InstallmentScheme As String

#End Region
#Region "LinkTo"
    Function LinkTo(ByVal strApplicationID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinApplication('" & strApplicationID & "','" & strStyle & "')"
    End Function
    Function LinkToCustomer(ByVal strCustomerID As String, ByVal strStyle As String) As String
        Return "javascript:OpenWinCustomer('" & strCustomerID & "','" & strStyle & "')"
    End Function

#End Region
#Region "Default Panel"

    Sub InitialDefatultPanel()
        PnlGrid.Visible = False
        PnlEntry.Visible = False
        LblApplicationType.Visible = False
        PnlDGrid2Insurance.Visible = False
        btnOK.Visible = True

    End Sub
#End Region
#Region "Page Load"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If sessioninvalid() Then
            Exit Sub
        End If
        Me.FormID = "InsNewCover"
        'If Not CheckForm(Me.Loginid, CommonVariableHelper.FORM_NAME_NEW_APPLICATION_BY_COMPANY, CommonVariableHelper.APPLICATION_NAME) Then
        '    Exit Sub
        'End If

        If Not IsPostBack Then
            If IsSingleBranch() And CInt(Replace(Me.sesBranchId, "'", "")) >= 1 And Me.IsHoBranch = False Then

                InitialDefatultPanel()
                Me.ApplicationID = Request.QueryString("ApplicationID")
                Me.Paid = Request.QueryString("Paid")
                If Me.Paid = "Customer" Then
                    Me.IsPageSource = True
                Else
                    Me.IsPageSource = False
                End If
                'Me.BranchID = Request.QueryString("BranchID")
                'Me.PageSource = Request.QueryString("PageSource")
                'Me.SupplierGroupID = Request.QueryString("SupplierGroupID")

                'lblApplicationID.NavigateUrl = LinkTo(Me.ApplicationID, "Insurance")
                HplinkApplicationID.Text = Me.ApplicationID
                HplinkApplicationID.NavigateUrl = LinkTo(Me.ApplicationID, "Insurance")
                ' ---- value = 'CompanyCustomer'
                ' ----- value = 'CompanyAtCost

                'oApplicationType.SupplierGroupID = Me.SupplierGroupID

                Dim oNewAppInsuranceByCompany As New Parameter.InsNewCover
                txtdiscountpremium.Value = "0"
                With oNewAppInsuranceByCompany
                    .ApplicationID = Me.ApplicationID.Trim
                    .BusinessDate = Me.BusinessDate
                    .strConnection = GetConnectionString()
                End With
                Try
                    oNewAppInsuranceByCompany = oController.GetInsuranceEntryStep1List(oNewAppInsuranceByCompany)
                Catch ex As Exception
                    LblErrorMessages.Text = MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND
                End Try


                With oNewAppInsuranceByCompany

                    lblCustomerName.Text = .CustomerName
                    HpLinkCustName.Text = .CustomerName
                    Me.CustomerName = lblCustomerName.Text
                    Me.CustomerID = .CustomerId
                    HpLinkCustName.NavigateUrl = LinkToCustomer(Me.CustomerID, "Insurance")
                    LblAssetNewused.Text = .AssetUsageNewUsed
                    Me.AssetNewUsed = LblAssetNewused.Text
                    LblAssetDescr.Text = .AssetMasterDescr
                    lblInsuranceAssetType.Text = .InsuranceAssetDescr
                    LblInsuredBy.Text = .InsAssetInsuredByName
                    LblPaidBy.Text = Me.Paid
                    LblApplicationID.Text = .ApplicationID
                    Me.Prepaid = CDbl(.Prepaid)
                    Me.InsLength = .InsLength
                    Me.MaturityDate = CType(.MaturityDate, DateTime)
                    Try
                        LblAssetUsageID.Text = .AssetUsageID.Trim
                    Catch ex As Exception
                        LblErrorMessages.Text = MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND & MessageHelper.MESSAGE_DATA_DETAIL_NOT_FOUND_ASSETUSAGE & " for this applicationID = " & Me.ApplicationID
                        btnOK.Visible = False
                        Exit Sub
                    End Try


                    Me.AssetUsageID = LblAssetUsageID.Text.Trim
                    LblAssetUsageDescr.Text = .AssetUsageDescr

                    TxtAmountCoverage.Text = CType(.TotalOTR, String)
                    Me.AmountCoverage = CType(TxtAmountCoverage.Text, Double)
                    LblMinimumTenor.Text = CType(.MinimumTenor, String)
                    LblMaximumTenor.Text = CType(.MaximumTenor, String)
                    LblBranchID.Text = .BranchId
                    'Me.MinimumTenor = LblMinimumTenor.Text
                    'Me.MaximumTenor = LblMaximumTenor.Text
                    'Me.PMaximumTenor = CStr(.PMaximumTenor)
                    'Me.PMinimumTenor = CStr(.PMinimumTenor)
                    'Me.PBMaximumTenor = CStr(.PBMaximumTenor)
                    'Me.PBMinimumTenor = CStr(.PBMinimumTenor)

                    Me.BranchID = .BranchId
                    txtInsAdminFee.Text = CType(.InsAdminFee, String)
                    TxtInsStampDutyFee.Text = CType(.InsStampDutyFee, String)
                    LblInsAdminFeeBehaviour.Text = .InsAdminFeeBehaviour
                    LblInsStampDutyBehavior.Text = .InsStampDutyFeeBehaviour
                    lblPrepaid.Text = FormatNumber(CType(.Prepaid, String), 2)
                    If LblInsAdminFeeBehaviour.Text.Trim.ToUpper = "L" Then
                        txtInsAdminFee.Enabled = False
                    Else
                        txtInsAdminFee.Enabled = True
                    End If


                    If LblInsStampDutyBehavior.Text.Trim.ToUpper = "L" Then
                        TxtInsStampDutyFee.Enabled = False
                    Else
                        TxtInsStampDutyFee.Enabled = True
                    End If

                    Me.AdminFeeBehavior = LblInsAdminFeeBehaviour.Text.Trim.ToUpper
                    Me.StampDutyFeeBehavior = LblInsStampDutyBehavior.Text.Trim.ToUpper
                    Me.NilaiAdminFeeAwal = .InsAdminFee
                    Me.NilaiStampDutyFeeAwal = .InsStampDutyFee

                    LblInsuranceType.Text = .InsuranceType
                    Me.InsuranceType = LblInsuranceType.Text
                    LblManufacturingYear.Text = .ManufacturingYear
                    Me.ManufacturingYear = LblManufacturingYear.Text

                End With
                CheckProspect()
            End If
        End If
    End Sub
#End Region
#Region "Check Prospect "

    Public Function CheckProspect() As Boolean
        Dim oNewAppInsuranceByCompany As New Parameter.InsNewCover
        With oNewAppInsuranceByCompany
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString
            .SpName = "spNewCoverCheckProspectSelect"
        End With

        oNewAppInsuranceByCompany = oController.CheckProspect(oNewAppInsuranceByCompany)

        If oNewAppInsuranceByCompany.MaskAssID <> "" And oNewAppInsuranceByCompany.InsuranceComBranchID <> "" Then
            'Bila prospectnya ada
            cmbInsuranceComBranch.Visible = False
            LblFocusInsuranceComBranchName.Text = oNewAppInsuranceByCompany.InsuranceComBranchName
            LblFocusInsuranceComBranchName.Visible = True
            Me.InsuranceComBranchID = LblFocusMaskAssID.Text
        Else
            'Bila prospectnya tidak ada
            cmbInsuranceComBranch.Visible = True
            LblFocusInsuranceComBranchName.Visible = False
            FillInsuranceComBranch()
            Me.InsuranceComBranchID = cmbInsuranceComBranch.SelectedValue.Trim

        End If

    End Function
#End Region
#Region "FillInsuranceComBranch"



    Public Sub FillInsuranceComBranch()

        Dim oNewAppInsuranceByCompany As New Parameter.InsNewCover
        Dim oData As New DataTable
        oNewAppInsuranceByCompany.strConnection = GetConnectionString
        oNewAppInsuranceByCompany.BranchId = Me.BranchID

        Dim DtFillInsurance As New DataTable
        DtFillInsurance = CType(Me.Cache.Item(CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE & Me.BranchID), DataTable)

        If DtFillInsurance Is Nothing Then
            Dim DtFillInsuranceCache As New DataTable
            DtFillInsuranceCache = oController.FillInsuranceComBranch(oNewAppInsuranceByCompany)
            Me.Cache.Insert(CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE & Me.BranchID, DtFillInsuranceCache, Nothing, DateTime.Now.AddHours(CommonCacheHelper.DEFAULT_TIME_DURATION_CACHE_HOUR), TimeSpan.Zero)
            DtFillInsurance = CType(Me.Cache.Item(CACHE_INSURANCE_COYBRANCH_NEWAPPINSURANCE & Me.BranchID), DataTable)
        End If

        If Not DtFillInsurance Is Nothing Then
            'oData = oNewAppInsuranceByCompany.ListData
            oData = DtFillInsurance
            cmbInsuranceComBranch.DataSource = oData.DefaultView
            cmbInsuranceComBranch.DataValueField = "ID"
            cmbInsuranceComBranch.DataTextField = "Name"
            cmbInsuranceComBranch.DataBind()
            cmbInsuranceComBranch.Items.Insert(0, "Select One")
            cmbInsuranceComBranch.Items(0).Value = "0"

        End If

    End Sub

#End Region
#Region "Reset"


    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("NewCoverForExistingApp.aspx")
    End Sub
#End Region
#Region "Check Validasi Step 1 "
    Function CheckValidasiStep1() As Boolean
        If CDbl(TxtAmountCoverage.Text) < Me.Prepaid Then
            LblErrorMessages.Text = " Jumlah harus lebih besar dari " & Me.Prepaid
            Return False
        Else
            Return True
        End If

    End Function
#End Region
#Region "Ketika Tombol OK di Klik "
    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim CheckStep1 As Boolean
        CheckStep1 = CheckValidasiStep1()

        If CheckStep1 = False Then
            Exit Sub
        End If

        'Tombol Save Dibawah dihilangin dulu
        btnSave.Visible = False


        Me.InsuranceComBranchID_lbl = cmbInsuranceComBranch.SelectedValue.Trim

        LblErrorMessages.Text = ""
        Me.ApplicationTypeID = oApplicationType.AppTypeID.Trim
        LblApplicationType.Text = oApplicationType.AppTypeName.Trim
        LblApplicationType.Visible = True
        PnlGrid.Visible = True
        PnlEntry.Visible = True
        btnOK.Visible = False
        btnReset.Visible = False


        Me.AmountCoverage = CType(TxtAmountCoverage.Text, Double)
        TxtAmountCoverage.Enabled = False
        'TxtTenorCredit.Enabled = False
        cmbInsuranceComBranch.Enabled = False
        oApplicationType.Visible = False

        'Dim jmlgrid As Double
        'jmlgrid = CInt(CType(LblTenorCredit.Text, Int16) / 12)
        'LblJmlGrid.Text = jmlgrid.ToString
        'Me.InsLength = CType(jmlgrid * 12, Int16)

        Dim oSetJmlGrid As Int16
        Dim oSimpleRule As New CommonSimpleRuleHelper
        'oSetJmlGrid = oSimpleRule.SetJumlahGrid(jmltenor)
        'Me.InsLength = CType(TxtTenorCredit.Text, Int16)
        'Context.Trace.Write("Tenor = " + TxtTenorCredit.Text)

        Dim oNewAppInsuranceByCompany As New Parameter.InsNewCover
        With oNewAppInsuranceByCompany
            '.JmlGrid = CType(LblJmlGrid.Text, Int16)
            .MaturityDate = Me.MaturityDate
            .BranchId = Me.BranchID.Trim
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
        End With

        oNewAppInsuranceByCompany = oController.GetInsuranceEntryStep2List(oNewAppInsuranceByCompany)
        Dim dtEntity As New DataTable

        If Not oNewAppInsuranceByCompany Is Nothing Then
            dtEntity = oNewAppInsuranceByCompany.ListData
        End If

        DgridInsurance.DataSource = dtEntity
        DgridInsurance.DataBind()
    End Sub

#End Region
#Region "Item DataBound DgridInsurance "

    Private Sub DgridInsurance_DataBinding(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DgridInsurance.ItemDataBound

        Dim CboCoverageTypeDT As New DataTable("CoverageTypeMs")
        Dim DDLCoverageTypePilihan As DropDownList
        Dim CboBooleanSRCCDT As New DataTable("BolSRCC")
        Dim DDLBooleanSRCCPilihan As DropDownList
        Dim CboTPLPDT As New DataTable("TPLMs")
        Dim DDLTPLPilihan As New DropDownList
        Dim CboBooleanFloodDT As New DataTable("BolFlood")
        Dim DDLBooleanFloodPilihan As DropDownList
        Dim CboPaidByCustDT As New DataTable("PaidByCustMs")
        Dim DDLPaidByCustPilihan As DropDownList

        Dim LblHiddenCoverageTypeDGrid As Label
        Dim LblHiddenSRCC As Label
        Dim LblHiddenTPL As Label
        Dim LblHiddenFlood As Label
        Dim LblHiddenPaidByCust As Label

        If e.Item.ItemIndex > -1 Then
            '============= Handle CoverageType =============
            DDLCoverageTypePilihan = CType(e.Item.Cells(2).FindControl("DDLCoverageTypeDgrid"), DropDownList)
            LblHiddenCoverageTypeDGrid = CType(e.Item.Cells(3).FindControl("LblHiddenCoverageTypeDGrid"), Label)
            CboCoverageTypeDT = Me.CoverageTypeMs
            Dim cbodvCoverageType As DataView
            cbodvCoverageType = CboCoverageTypeDT.DefaultView
            DDLCoverageTypePilihan.DataSource = cbodvCoverageType
            DDLCoverageTypePilihan.DataTextField = "Description"
            DDLCoverageTypePilihan.DataValueField = "ID"
            DDLCoverageTypePilihan.DataBind()
            DDLCoverageTypePilihan.ClearSelection()
            DDLCoverageTypePilihan.Items.FindByValue(Trim(LblHiddenCoverageTypeDGrid.Text)).Selected = True

            '============= Handle SRCC =============

            DDLBooleanSRCCPilihan = CType(e.Item.Cells(5).FindControl("DDLBolSRCCPilihan"), DropDownList)
            LblHiddenSRCC = CType(e.Item.Cells(6).FindControl("LblHiddenSRCCDGrid"), Label)
            CboBooleanSRCCDT = Me.SRCCMs
            Dim CbodvSRCC As DataView
            CbodvSRCC = CboBooleanSRCCDT.DefaultView
            DDLBooleanSRCCPilihan.DataSource = CbodvSRCC
            DDLBooleanSRCCPilihan.DataTextField = "Description"
            DDLBooleanSRCCPilihan.DataValueField = "ID"
            DDLBooleanSRCCPilihan.DataBind()
            DDLBooleanSRCCPilihan.ClearSelection()
            Dim strBol As Int16
            If LblHiddenSRCC.Text.ToUpper.Trim = "TRUE" Then
                strBol = 1
            Else
                strBol = 0
            End If
            DDLBooleanSRCCPilihan.Items.FindByValue(Trim(strBol.ToString)).Selected = True

            '================= Handle TPL =======================

            DDLTPLPilihan = CType(e.Item.Cells(3).FindControl("DDLTPLPilihan"), DropDownList)
            LblHiddenTPL = CType(e.Item.Cells(4).FindControl("LblHiddenTPLDgrid"), Label)
            CboCoverageTypeDT = Me.TPLMs
            Dim CbodvTPL As DataView
            CbodvTPL = CboCoverageTypeDT.DefaultView
            DDLTPLPilihan.DataSource = CbodvTPL
            DDLTPLPilihan.DataTextField = "TPLAmount"
            DDLTPLPilihan.DataValueField = "TPLPremium"
            DDLTPLPilihan.DataBind()
            DDLTPLPilihan.ClearSelection()

            '

            '============= Handle FLOOD =============
            DDLBooleanFloodPilihan = CType(e.Item.Cells(7).FindControl("DDLFloodPilihan"), DropDownList)
            LblHiddenFlood = CType(e.Item.Cells(8).FindControl("LblHiddenFloodDGrid"), Label)
            CboBooleanFloodDT = Me.FloodMs
            Dim CboDvFlood As DataView
            CboDvFlood = CboBooleanFloodDT.DefaultView
            DDLBooleanFloodPilihan.DataSource = CboDvFlood
            DDLBooleanFloodPilihan.DataTextField = "Description"
            DDLBooleanFloodPilihan.DataValueField = "ID"
            DDLBooleanFloodPilihan.DataBind()
            DDLBooleanFloodPilihan.ClearSelection()
            Dim strBol2 As Int16
            If LblHiddenSRCC.Text.ToUpper.Trim = "TRUE" Then
                strBol2 = 1
            Else
                strBol2 = 0
            End If
            DDLBooleanFloodPilihan.Items.FindByValue(Trim(strBol2.ToString)).Selected = True


            '============= Handle PaidByCust =============
            DDLPaidByCustPilihan = CType(e.Item.Cells(9).FindControl("DDLPaidByCustDGrid"), DropDownList)
            LblHiddenPaidByCust = CType(e.Item.Cells(10).FindControl("LblHiddenPaidByCust"), Label)
            CboPaidByCustDT = Me.PaidByCustMs

            Dim CbodvPaidByCust As DataView
            CbodvPaidByCust = CboPaidByCustDT.DefaultView
            DDLPaidByCustPilihan.DataSource = CbodvPaidByCust
            DDLPaidByCustPilihan.DataTextField = "Description"
            DDLPaidByCustPilihan.DataValueField = "ID"
            DDLPaidByCustPilihan.DataBind()
            DDLPaidByCustPilihan.ClearSelection()
            'DDLPaidByCustPilihan.Items.FindByValue(Trim(LblHiddenPaidByCust.Text.Trim)).Selected = True

        Else
            '============= Handle CoverageType =============

            Dim strsqlQuery As String
            strsqlQuery = CommonVariableHelper.SQL_QUERY_COVERAGE_TYPEMS
            Dim objAdapter As New SqlDataAdapter(strsqlQuery, GetConnectionString)
            objAdapter.Fill(CboCoverageTypeDT)
            Me.CoverageTypeMs = CboCoverageTypeDT

            '============= Handle SRCC =============
            Dim strsqlQuerySRCC As String
            Dim SBuilder As New StringBuilder
            SBuilder.Append("create table #TempTable (")
            SBuilder.Append("ID  int ,")
            SBuilder.Append("Description char(8) ) ")
            SBuilder.Append("EXEC ('")
            SBuilder.Append("INSERT INTO #TEMPTABLE Select ''1'' as ID ,''Yes'' as Description ")
            SBuilder.Append("INSERT INTO #TEMPTABLE Select ''0'' as ID ,''No'' as Description ")
            SBuilder.Append("Select ID,Description From #TempTable ')")
            strsqlQuerySRCC = SBuilder.ToString
            Dim objAdapterSRCC As New SqlDataAdapter(strsqlQuerySRCC, GetConnectionString)
            objAdapterSRCC.Fill(CboBooleanSRCCDT)
            Me.SRCCMs = CboBooleanSRCCDT


            '============= Handle TPL =============
            Dim strQueryTPL As String
            strQueryTPL = CommonVariableHelper.SQL_QUERY_TPL & " Where BranchID= '" & Me.BranchID & "'"
            Dim ObjAdapterTPL As New SqlDataAdapter(strQueryTPL, GetConnectionString)
            ObjAdapterTPL.Fill(CboTPLPDT)
            Me.TPLMs = CboTPLPDT

            '============= Handle Flood =============
            Dim strsqlQueryFlood As String
            Dim SBuilder2 As New StringBuilder
            SBuilder2.Append("create table #TempTable (")
            SBuilder2.Append("ID  int ,")
            SBuilder2.Append("Description char(8) ) ")
            SBuilder2.Append("EXEC ('")
            SBuilder2.Append("INSERT INTO #TEMPTABLE Select ''1'' as ID ,''Yes'' as Description ")
            SBuilder2.Append("INSERT INTO #TEMPTABLE Select ''0'' as ID ,''No'' as Description ")
            SBuilder2.Append("Select ID,Description From #TempTable ')")
            strsqlQueryFlood = SBuilder2.ToString
            Dim objAdapterFlood As New SqlDataAdapter(strsqlQueryFlood, GetConnectionString)
            objAdapterFlood.Fill(CboBooleanFloodDT)
            Me.FloodMs = CboBooleanFloodDT


            '============= Handle PaidByCust =============

            Dim strsqlquery2 As String
            strsqlquery2 = CommonVariableHelper.SQL_QUERY_PAIDBYCUST_MS


            Dim objAdapter2 As New SqlDataAdapter(strsqlquery2, GetConnectionString)
            objAdapter2.Fill(CboPaidByCustDT)
            Me.PaidByCustMs = CboPaidByCustDT

        End If
    End Sub



#End Region
#Region "Ketika Tombol Calculate di click !"


    Private Sub btnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculate.Click

        btnSave.Visible = True
        LblErrorMessages.Text = ""

        Dim BolValidasiStep1 As Boolean
        BolValidasiStep1 = True


        '============= Validasi dulu bila companycustomer =====
        Dim loopCheckPaid As Int16

        For loopCheckPaid = 0 To CType(DgridInsurance.Items.Count - 1, Int16)

            Dim DDLPaid As New DropDownList
            DDLPaid = CType(DgridInsurance.Items(loopCheckPaid).Cells(8).FindControl("DDLPaidByCustDGrid"), DropDownList)
            Dim ParamPaidCustStatus As String
            ParamPaidCustStatus = DDLPaid.SelectedItem.Value.ToUpper

            If DDLPaid.SelectedItem.Value.ToUpper.Trim <> "PAID" And loopCheckPaid = 0 Then

                BolValidasiStep1 = False

                'Bila Validasi Tidak Ok maka...Tombol Calculate masih boleh dipijit
                btnCalculate.Enabled = True
                Exit For
            End If

        Next



        If BolValidasiStep1 = False Then
            LblErrorMessages.Text = "Premi Tahun Pertama harus dibayar Customer "
            LblErrorMessages.Visible = True
            Exit Sub
        Else
            'Bila Validasi Ok maka...proses lanjut
            'Tombol Calculate di freeze !
            btnCalculate.Enabled = False
            PnlDGrid2Insurance.Visible = True
            PnlGrid.Visible = False

            ' ============================== Proses Kalkulasi di Grid ====================
            LblErrorMessages.Text = ""
            LblErrorMessages2.Text = ""
            Dim LoopCalculate As Int16
            Dim customClass As New Parameter.InsuranceCalculation

            Dim strcompanycustomer As Boolean
            'If Me.PageSource.Trim = COMPANY_CUSTOMER Then
            '    strcompanycustomer = True
            'Else
            '    strcompanycustomer = False
            'End If
            ' -------=========||| Untuk InsuranceAsset |||===========---------------------
            With customClass
                .InsuranceComBranchID = Me.InsuranceComBranchID
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .ApplicationTypeID = Me.ApplicationTypeID
                .UsageID = Me.AssetUsageID
                .NewUsed = Me.AssetNewUsed
                .strConnection = GetConnectionString()
            End With

            Try
                oInsAppController.ProcessNewAppInsuranceByCompanySaveAddTemporary(customClass)
            Catch ex As Exception
                Dim err As New MaxiloanExceptions
                Response.Write(ex.Message)
                err.WriteLog("NewAppInsuranceByCompany.aspx.vb", "sub Calculate", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                btnSave.Visible = False
                Exit Sub
            End Try
            Dim LblYearInsurance As New Label
            Dim CoverageTpye As DropDownList
            Dim SRCC As DropDownList
            Dim TPLSelection As DropDownList
            Dim ParamSRCC As String
            Dim Flood As DropDownList
            Dim PaidByCust As DropDownList
            For LoopCalculate = 0 To CType(DgridInsurance.Items.Count - 1, Int16)
                LblYearInsurance = CType(DgridInsurance.Items(LoopCalculate).FindControl("LblYearInsurance"), Label)
                CoverageTpye = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLCoverageTypeDgrid"), DropDownList)
                SRCC = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLBolSRCCPilihan"), DropDownList)
                TPLSelection = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLTPLPilihan"), DropDownList)
                Flood = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLFloodPilihan"), DropDownList)
                PaidByCust = CType(DgridInsurance.Items(LoopCalculate).FindControl("DDLPaidByCustDGrid"), DropDownList)
                ' -------=========||| Untuk InsuranceAssetDetail  |||===========---------------------
                Dim strdatemanufacturing As DateTime = CType("01/01/" & Me.ManufacturingYear, DateTime)
                If strdatemanufacturing < CType("01/01/1900", DateTime) Then strdatemanufacturing = CType("01/01/1900", DateTime)

                With customClass
                    .YearNum = CType(LblYearInsurance.Text, Int16)
                    .CoverageType = CoverageTpye.SelectedItem.Value.Trim
                    .BolSRCC = CType(SRCC.SelectedItem.Value.Trim, Boolean)
                    If TPLSelection.SelectedIndex > 0 Then
                        .TPL = CType(TPLSelection.SelectedItem.Value.Trim, Double)
                    Else
                        .TPL = 0
                    End If
                    .BolFlood = CType(Flood.SelectedItem.Value.Trim, Boolean)
                    .PaidByCustStatus = PaidByCust.SelectedItem.Value.Trim
                    .AmountCoverage = Me.AmountCoverage
                    .InsuranceType = Me.InsuranceType
                    .UsageID = Me.AssetUsageID
                    .NewUsed = Me.AssetNewUsed
                    .BranchId = Me.BranchID
                    .InsLength = CType(Me.InsLength, Short)
                    Context.Trace.Write("Ketika Tombol Calculate diclik -> Inslength = " + CType(Me.InsLength, String))
                    .BusinessDate = Me.BusinessDate
                    .DateMonthYearManufacturingYear = strdatemanufacturing
                    .IsPageSourceCompanyCustomer = Me.IsPageSource
                    .strConnection = GetConnectionString()
                End With

                Try
                    oInsAppController.ProcessNewAppInsuranceByCompanyDetailSaveAddTemporary(customClass)
                Catch ex As Exception
                    Dim err As New MaxiloanExceptions
                    err.WriteLog("NewAppInsuranceByCompany.aspx", "Calculate", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
                    Response.Write(ex.Message)
                    btnSave.Visible = False
                    Exit For
                End Try
            Next
            ReBindGridInsurance()
            DisplayResult1()
        End If

    End Sub


#End Region
#Region "ReBindGridInsurance"

    Public Sub ReBindGridInsurance()

        Dim oGrid2 As New Parameter.InsuranceCalculationResult
        With oGrid2
            .BranchId = Me.BranchID.Trim
            .ApplicationID = Me.ApplicationID.Trim
            .strConnection = GetConnectionString
        End With

        oGrid2 = oInsCalResultController.DisplayResultOnGrid(oGrid2)
        Dim dtEntity As New DataTable

        If Not oGrid2 Is Nothing Then
            dtEntity = oGrid2.ListData
        End If

        Dgrid2Insurance.DataSource = dtEntity
        Dgrid2Insurance.DataBind()

    End Sub

#End Region
#Region "Proses Menampilkan hasil perhitungan ke layar "

    Public Sub DisplayResult1()


        Dim oResultInsCalculationResult As New Parameter.InsuranceCalculationResult

        With oCustomClassResult
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .strConnection = GetConnectionString
        End With

        Try
            oResultInsCalculationResult = oInsCalResultController.DisplayResultInsCalculationStep1(oCustomClassResult)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        With oResultInsCalculationResult

            LblTotalSRCCPremium.Text = FormatNumber(.TotalSRCPremiumToCust, 2, , , )
            LblTotalFloodPremium.Text = FormatNumber(.TotalFloodPremiumToCust, 2, , , )
            LblTotalTPLPremium.Text = FormatNumber(.TotalTPLPremiumToCust, 2, , , )
            'LblTotalLoadingFee.Text = FormatNumber(.TotalLoadingFeeToCust, 2, , , )
            If Me.IsPageSource = True Then
                LblTotalStandardPremium.Text = FormatNumber(.TotalStdPremium, 2, , , )
            Else
                LblTotalStandardPremium.Text = FormatNumber(0, 2, , , )
            End If
            LblPremiumToCust.Text = CType(.TotalPremiumToCustBeforeDiscount, String)
            LblPremiumToCustFormat.Text = CType(.TotalPremiumToCustBeforeDiscount, String) 'diremark sejak tgl 30 september 2003 jam 13:25
            LblPremiumToCustFormat.Text = FormatNumber(.TotalPremiumToCustBeforeDiscount, 2, , , )
            LbTotalPremiumByCustformat.Text = CType(.TotalPremiumToCustBeforeDiscount, String)
            LbTotalPremiumByCustformat.Text = FormatNumber(.TotalPremiumToCustBeforeDiscount, 2, , , )
            Me.PremiumAmountByCustBeforeDisc = .TotalPremiumToCustBeforeDiscount
            Me.InterestType = .InterestType
            Me.InstallmentScheme = .InstallmentScheme
            Me.CustomerID = .CustomerID

            ' tambahan untuk javascript tgl 30 juni 2004
            LblPremiumToCustFormat.Text = FormatNumber(.TotalPremiumToCustBeforeDiscount, 2, , , )
            LbTotalPremiumByCustformat.Text = FormatNumber(.TotalPremiumToCustBeforeDiscount, 2, , , )

            'LblAmountCapitalized.Text = "0"
            'TxtPremiumBase.Text = LblPremiumToCust.Text
            '    TxtPremiumBase.Text = "0"
            '    TxtPaidAmountByCustomer.Value = CType(.TotalPremiumToCustBeforeDiscount, String)
            '    If TxtAdditionalCap.Text = "" Then TxtAdditionalCap.Text = "0"
            ' jika prepaid < Total PremiumToCust
            'If Me.Prepaid < CDbl(.TotalPremiumToCustBeforeDiscount) Then
            '    ImbSave.Visible = False
            'End If
        End With

        'Tombol  save dibawah diaktifkan
        ' ImbSave.Visible = True dimatikan 




    End Sub
#End Region
#Region "Tombol Cancel Paling Bawah !"

    Private Sub btnCancelBawah_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelBawah.Click
        With oCustomClass
            .strConnection = GetConnectionString()
            .ApplicationID = Me.ApplicationID
            .BranchId = Me.BranchID
        End With
        Try
            oController.InsNewCoverInsuredByCustDelete(oCustomClass)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Response.Redirect("NewCoverForExistingApp.aspx")
    End Sub
#End Region
#Region "Sequence Validasi'"

    Public Function SequenceValidasi() As Boolean
        'Dim premiumbycustamount As Double = Me.PremiumAmountByCustBeforeDisc - CType(TxtDiscountPremium.Value, Double)
        'bila typenya company at cust maka kasih return true aja
        '03 oktober 2003 koreksi nilai javascriptnya...

        'Dim refreshamountcapitalized As Double = premiumbycustamount - CType(TxtPaidAmountByCustomer.Value, Double)



        Context.Trace.Write("TxtDiscountPremium = " + TxtDiscountPremium.Value)
        'Context.Trace.Write("TxtPaidAmountByCustomer = " + TxtPaidAmountByCustomer.Value)
        'Context.Trace.Write("Total Premium By Cust = " & premiumbycustamount & "")
        'Context.Trace.Write("Paid Amount By Cust = " & TxtPaidAmountByCustomer.Value & "")
        'Context.Trace.Write("Amount Capitalized = " & refreshamountcapitalized & "")


        'If CType(TxtPaidAmountByCustomer.Value, Decimal) > (CType(LblPremiumToCust.Text, Decimal) - CType(TxtDiscountPremium.Value, Decimal)) Then
        '    LblErrorMessages2.Text = "Amount Capitalized should be larger or equal to 0"
        '    LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
        '    LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
        '    Return False
        'End If

        'If Not ValidasiAngkaPertama(CType(LblPremiumToCust.Text, Decimal), CType(TxtPremiumBase.Text, Decimal)) Then
        '    LblErrorMessages2.Text = "Premium Base For Refund Showrom Cannot be Greater Than Premium To Customer "
        '    LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
        '    LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
        '    Return False
        'End If

        If Not ValidasiAngkaPertama(CType(LblPremiumToCustFormat.Text, Decimal), CType(TxtDiscountPremium.Value, Decimal)) Then
            LblErrorMessages2.Text = "Jumlah Discount Harus <= Jumlah Premi ke Customer"
            'LbTotalPremiumByCustformat.Text = CType(premiumbycustamount, String)
            'LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
            Return False
        End If

        If Not ValidasiAngkaPertama(CType(lblPrepaid.Text, Decimal), (CType(LblPremiumToCustFormat.Text, Decimal) - CType(TxtDiscountPremium.Value, Decimal))) Then
            LblErrorMessages2.Text = "Jumlah Prepaid tidak cukup untuk Total Premi ke Customer"
            'LbTotalPremiumByCustformat.Text = CType(premiumbycustamount, String)
            'LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
            Return False
        End If

        'LblTotalPremiumByCust.Text  remark 03 oktober 2003 
        'If Not ValidasiAngkaPertama(CType(premiumbycustamount, Decimal), CType(TxtPaidAmountByCustomer.Value, Decimal)) Then
        '    LblErrorMessages2.Text = "Paid Amount By Customer Cannot Be Greater Than TotalPremium By Customer "
        '    'Context.Trace.Write("Total Premium By Cust = " & LblTotalPremiumByCust.Text & "")
        '    LblTotalPremiumByCust.Text = CType(premiumbycustamount, String)
        '    LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
        '    Return False
        'End If

        If Me.AdminFeeBehavior = "N" Then
            'Minimum , Admin Fee Bisa diedit menjadi lebih besar
            'tapi tidak boleh kurang dari nilai awal
            If Not ValidasiAngkaPertama(CType(txtInsAdminFee.Text, Decimal), CType(Me.NilaiAdminFeeAwal, Decimal)) Then
                LblErrorMessages2.Text = "Biaya Admin Tidak boleh <  " & Me.NilaiAdminFeeAwal
                'LbTotalPremiumByCustformat.Text = CType(premiumbycustamount, String)
                'LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
                Return False
            End If
        End If

        If Me.AdminFeeBehavior = "X" Then
            'Maximum, Admin Fee bisa diedit menjadi lebih kecil
            'Tapi tidak boleh lebik dari nilai awal
            If ValidasiAngkaPertama(CType(txtInsAdminFee.Text, Decimal), CType(Me.NilaiAdminFeeAwal, Decimal)) Then
                LblErrorMessages2.Text = "Biaya Admin Tidak boleh >  " & Me.NilaiAdminFeeAwal
                'LbTotalPremiumByCustformat.Text = CType(premiumbycustamount, String)
                'LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
                Return False
            End If
        End If

        If Me.StampDutyFeeBehavior = "N" Then
            'Minimum , Stamp duty fee Bisa diedit menjadi lebih besar
            'tapi tidak boleh kurang dari nilai awal
            If Not ValidasiAngkaPertama(CType(TxtInsStampDutyFee.Text, Decimal), CType(Me.NilaiAdminFeeAwal, Decimal)) Then
                LblErrorMessages2.Text = "Biaya Materai Tidak Boleh < " & Me.NilaiStampDutyFeeAwal
                'LbTotalPremiumByCustformat.Text = CType(premiumbycustamount, String)
                'LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
                Return False
            End If
        End If

        If Me.StampDutyFeeBehavior = "X" Then
            'Maximum, Stamp duty  Fee bisa diedit menjadi lebih kecil
            'Tapi tidak boleh lebik dari nilai awal
            If ValidasiAngkaPertama(CType(TxtInsStampDutyFee.Text, Decimal), CType(Me.NilaiAdminFeeAwal, Decimal)) Then
                LblErrorMessages2.Text = "Biaya Materai Tidak Boleh >  " & Me.NilaiStampDutyFeeAwal
                'LbTotalPremiumByCustformat.Text = CType(premiumbycustamount, String)
                'LblAmountCapitalized.Text = CType(refreshamountcapitalized, String)
                Return False
            End If
        End If

        LblErrorMessages2.Text = ""
        Return True

    End Function

#End Region
#Region "Ketika Tombol Save Paling Bawah Dipijit "
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim isProcessSuccess As Boolean
        Dim premiumbycustamount As Double = Me.PremiumAmountByCustBeforeDisc - CType(TxtDiscountPremium.Value, Double)
        LbTotalPremiumByCustformat.Text = FormatNumber(premiumbycustamount, 2)
        If Not SequenceValidasi() Then
            Exit Sub
        End If
        'Dim premiumbycustamount As Double = Me.PremiumAmountByCustBeforeDisc - CType(TxtDiscountPremium.Value, Double)
        'LbTotalPremiumByCustformat.Text = FormatNumber(premiumbycustamount, 2)
        'Dim checktotal As Integer
        'Try

        '    Dim oCheck As New Parameter.InsuranceCalculation
        '    With oCheck
        '        .ApplicationID = Me.ApplicationID
        '        .strConnection = GetConnectionString
        '    End With

        '    Dim OcontrollerCheck As New InsuranceApplicationController
        '    oCheck = OcontrollerCheck.GetDateEntryInsuranceData(oCheck)


        '    'With oCheck
        '    '    checktotal = .TotalRecord
        '    '    Context.Trace.Write("TotalRecord  = " & .TotalRecord & "")
        '    'End With

        '    'If checktotal > 0 Then
        '    '    LblErrorMessages2.Text = " Data Already Exists !"
        '    '    ImbSave.Visible = False
        '    '    Exit Sub
        '    'End If

        'Catch ex As Exception
        '    Dim err As New MaxiloanExceptions
        '    err.WriteLog("NewAppInsuranceByCompany.aspx", "Save-check dateinsurancedata", ex.Source, ex.TargetSite.Name, ex.Message, ex.StackTrace)
        'End Try


        'Context.Trace.Write("Recordnya  = " & checktotal & "")
        'ProcessSaveInsuranceApplicationLastProcess
        Dim oCustomClass As New Parameter.InsNewCover

        If Me.InsuranceComBranchID_lbl = "" Then
            Me.InsuranceComBranchID_lbl = "0"
        End If

        With oCustomClass
            .BranchId = Me.BranchID
            .ApplicationID = Me.ApplicationID
            .AmountCoverage = CType(Me.AmountCoverage, Decimal)
            .AdminFeeToCust = CType(txtInsAdminFee.Text, Decimal)
            .MeteraiFeeToCust = CType(TxtInsStampDutyFee.Text, Decimal)
            .DiscToCustAmount = CType(Replace(TxtDiscountPremium.Value, ",", ""), Decimal)
            '.PaidAmountByCust = CType(Replace(TxtPaidAmountByCustomer.Value, ",", ""), Double)
            '.PremiumBaseForRefundSupp = CType(TxtPremiumBase.Text, Double)
            .AccNotes = TxtAccNotes.Text.Trim
            .InsNotes = TxtInsNotes.Text.Trim
            .InsLength = Me.InsLength
            .BusinessDate = Me.BusinessDate
            .strConnection = GetConnectionString()
            .PremiumToCustAmount = CType(premiumbycustamount, Decimal)
            .InsuranceComBranchID = Me.InsuranceComBranchID_lbl
            .IsPageSource = Me.IsPageSource
            '.AdditionalCapitalized = CDbl(TxtAdditionalCap.Text)
            '.AdditionalInsurancePremium = CDbl(TxtAdditionalInsurancePremium.Value)
            '.RefundToSupplier = CDbl(txtRfnSupplier.Text.Trim)
        End With
        Try
            oController.ProcessSaveInsuranceApplicationLastProcess(oCustomClass)
            isProcessSuccess = True
        Catch ex As Exception
            Response.Write(ex.Message)
            isProcessSuccess = False
        End Try
        If isProcessSuccess = True Then
            Response.Redirect("NewCoverForExistingApp.aspx")
        End If

    End Sub
#End Region
#Region "SendCookies"
    Sub SendCookies()

        Dim cookie As New HttpCookie("Financial")
        cookie.Values.Add("id", Me.ApplicationID)
        cookie.Values.Add("custid", Me.CustomerID)
        cookie.Values.Add("name", Me.CustomerName)
        cookie.Values.Add("EmployeeID", "")
        cookie.Values.Add("InterestType", Me.InterestType)
        cookie.Values.Add("InstallmentScheme", Me.InstallmentScheme)
        Response.AppendCookie(cookie)
    End Sub
#End Region
#Region "Function Validasi ketika tombol save dipencet"

    Public Function ValidasiAngkaPertama(ByVal angkapertama As Decimal, ByVal angkakedua As Decimal) As Boolean

        If angkapertama >= angkakedua Then
            'bila angka pertama lebih besar dari angka kedua maka BENAR
            Return True
        Else
            'bila angka pertama lebih kecil dari angka kedua maka Salah
            Return False
        End If


    End Function

#End Region


End Class