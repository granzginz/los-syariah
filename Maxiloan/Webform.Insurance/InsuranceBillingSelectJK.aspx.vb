﻿#Region "Imports"
Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController
#End Region

Public Class InsuranceBillingSelectJK
    Inherits Maxiloan.Webform.WebBased
    'Protected WithEvents TxtAdminFee As ucNumberFormat
    'Protected WithEvents TxtPolicyFee As ucNumberFormat
    Protected WithEvents txtTglRekap As ucDateCE
    Protected WithEvents txtTglRencanaBayar As ucDateCE

#Region "Constanta"
    Dim currentPage As Integer = CommonVariableHelper.DEFAULT_CURRENT_PAGE
    Dim PageSize As Int16 = CommonVariableHelper.DEFAULT_PAGE_SIZE
    Dim currentPageNumber As Int32 = CommonVariableHelper.DEFAULT_CURRENT_PAGE_NUMBER
    Dim totalPages As Double = CommonVariableHelper.DEFAULT_TOTALPAGES
    Dim recordCount As Int64 = CommonVariableHelper.DEFAULT_RECORD_COUNT
    Private Const SP_NAME_PAGING As String = ""
    Private m_controller As New GeneralPagingController
    Private oController As New SPPAController
    Private oSaveController As New InsuranceBillingSelectController
    Private m_Doc As New DocReceiveController
    Dim SumAmountPremium As Double

    Dim paramInsCo As String
    Dim paramWhereAggNo As String
    Dim paramInsCoName As String
    Dim totalinvoiceamount As Double
    Dim temptotaladminfee As Double
    Dim temptotalstampfee As Double
    Dim ParamsApplicationId As String

#End Region
#Region "Property "

    Private Property JmlRecorddiCheck() As Int64
        Get
            Return CType(ViewState("JmlRecord"), Int64)
        End Get
        Set(ByVal Value As Int64)
            ViewState("JmlRecord") = Value
        End Set
    End Property
    Private Property CmdWhere() As String
        Get
            Return CType(ViewState("vwsCmdWhere"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("vwsCmdWhere") = Value
        End Set
    End Property
    Private Property Sort() As String
        Get
            Return CType(ViewState("Sort"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("Sort") = Value
        End Set
    End Property
    Private Property TotInvoiceAmount() As Double
        Get
            Return CType(ViewState("TotInvoiceAmount"), Integer)
        End Get
        Set(ByVal Value As Double)
            ViewState("TotInvoiceAmount") = Value
        End Set
    End Property
    Private Property InsCo() As String
        Get
            Return CType(ViewState("InsCo"), String)
        End Get
        Set(ByVal Value As String)
            ViewState("InsCo") = Value
        End Set
    End Property
    Private Property StatusPage() As String
        Get
            Return CType(ViewState("StatusPage"), String)
        End Get
        Set(ByVal StatusPage As String)
            ViewState("StatusPage") = StatusPage
        End Set
    End Property
    Private Property ApplicationID() As String
        Get
            Return CType(ViewState("ApplicationID"), String)
        End Get
        Set(ByVal ApplicationID As String)
            ViewState("ApplicationID") = ApplicationID
        End Set
    End Property
    Private Property InsuranceComBranchID() As String
        Get
            Return CType(ViewState("InsuranceComBranchID"), String)
        End Get
        Set(ByVal InsuranceComBranchID As String)
            ViewState("InsuranceComBranchID") = InsuranceComBranchID
        End Set
    End Property
    Private TotalPremiGross As Decimal
    Private TotalKomisi As Decimal
    Private TotalBungaNett As Decimal
    Private TotalPremiNett As Decimal
    Private TotalPPn As Decimal
    Private TotalPPH As Decimal
    Private TotalBiayaPolis As Decimal
    Private TotalBiayaMaterai As Decimal
    Private TotalTotalTagihan As Decimal
#End Region
#Region "PageLoad"


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If SessionInvalid() Then
            Exit Sub
        End If

        If Not IsPostBack Then
            Try
                Me.StatusPage = Request.QueryString("StatusPage")
                Me.InsuranceComBranchID = CType(HttpContext.Current.Items("InsuranceBilling"), Parameter.SPPA).MaskAssBranchID

                If Me.StatusPage.Trim = "TerimaTagihan" Then
                    txtTglRekap.IsRequired = True
                    txtTglRencanaBayar.IsRequired = True
                    pnlTagihanInsurance.Visible = False
                    pnlTerimaTagihan.Visible = True
                    Dim oData As New DataTable
                    Dim oDoc As New DocRec
                    Dim dtEntity As New DataTable

                    Dim oCustomClassHTTPContext As New Parameter.SPPA
                    oCustomClassHTTPContext = CType(HttpContext.Current.Items("InsuranceBilling"), Parameter.SPPA)
                    dtEntity = oCustomClassHTTPContext.ListData

                    If dtEntity.Rows.Count > 0 Then
                        Me.ApplicationID = dtEntity.Rows(0).Item("ApplicationId").ToString
                    End If


                    oDoc.strConnection = GetConnectionString()
                    oDoc.WhereCond = " InsuranceAsset.ApplicationId = '" + Me.ApplicationID.Trim + "'"
                    oDoc.SpName = "spGetAllNotaAsuransi"
                    oDoc = m_Doc.GetSPReport(oDoc)
                    oData = oDoc.ListDataReport.Tables(0)
                    dtgTerimaTagihan.DataSource = oData
                    dtgTerimaTagihan.DataBind()
                Else
                    pnlTagihanInsurance.Visible = True
                    pnlTerimaTagihan.Visible = False
                    paramInsCo = CType(HttpContext.Current.Items("InsuranceBilling"), Parameter.SPPA).MaskAssBranchID
                    Me.InsCo = paramInsCo
                    paramInsCoName = CType(HttpContext.Current.Items("InsuranceBilling"), Parameter.SPPA).MaskAssBranchName
                    Dim termofpayment As String = CType(HttpContext.Current.Items("InsuranceBilling"), Parameter.SPPA).TermOfPayment

                    Context.Trace.Write("termofpayment = " & termofpayment)
                    Context.Trace.Write("paramWhereAggNo = " & paramWhereAggNo)
                    Context.Trace.Write("paramInsCo = " & paramInsCo)
                    Context.Trace.Write("paramInsCoName = " & paramInsCoName)

                    LblInsuranceCompany.Text = paramInsCoName

                    Dim processdate As New CommonSimpleRuleHelper

                    txtInvoiceDate.Text = processdate.ConvertDateReturnAsString(CType(Me.BusinessDate, String))

                    Dim defaultduedate As Date
                    defaultduedate = DateAdd(DateInterval.Day, CType(termofpayment, Double), Me.BusinessDate)
                    Context.Trace.Write("defaultduedate = " & defaultduedate)
                    Dim stringdefaultduedate As String
                    stringdefaultduedate = CType(defaultduedate, String)
                    Context.Trace.Write("stringdefaultduedate = " & stringdefaultduedate)

                    txtDueDate.Text = processdate.ConvertDateReturnAsString(stringdefaultduedate)
                    Context.Trace.Write("txtDueDate.text = " & txtDueDate.Text)

                    Dim dtEntity As New DataTable
                    Dim oCustomClassHTTPContext As New Parameter.SPPA
                    oCustomClassHTTPContext = CType(HttpContext.Current.Items("InsuranceBilling"), Parameter.SPPA)
                    dtEntity = oCustomClassHTTPContext.ListData
                    dtgPaging.DataSource = dtEntity
                    dtgPaging.DataBind()
                    Me.JmlRecorddiCheck = CType(dtgPaging.Items.Count.ToString(), Long)
                    LblTotalInvoiceAmount.Text = FormatNumber(Me.TotInvoiceAmount, 2, , , )
                    TxtAdminFee.Text = "0"
                    TxtPolicyFee.Text = "0"
                End If
            Catch ex As Exception
                ShowMessage(lblMessages, ex.Message, True)
            End Try

        End If


    End Sub

#End Region
#Region "Cancel"
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("InsuranceBilling.aspx?InsuranceComBranchID=" + Me.InsuranceComBranchID + "&StatusPage=" + StatusPage + "")
    End Sub
#End Region
#Region "Sorting"
    Public Sub Sorting(ByVal Sender As Object, ByVal e As DataGridSortCommandEventArgs) Handles dtgPaging.SortCommand
        If InStr(Me.Sort, "DESC") > 0 Then
            Me.Sort = e.SortExpression
        Else
            Me.Sort = e.SortExpression + " DESC"
        End If
        'BindGridEntity(Me.SearchBy)
    End Sub
#End Region
#Region "Hitung Total Premium To Be Paid"
    Private Sub dtgPaging_DataBinding(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgPaging.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim hyPolicyNo As HyperLink

        If e.Item.ItemIndex > -1 Then
            Dim rowPremiumToBePaid As TextBox = CType(e.Item.FindControl("TxtPremiumToBePaid"), TextBox)
            Dim result As Double = Jumlahkan(CType(rowPremiumToBePaid.Text, Double))
            'Response.Write(result)
            '  LblTotalInvoiceAmount.Text = CType(result, String)
            Me.TotInvoiceAmount = result
            '*** Customer Link
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"
            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"
            '*** Policy No link
            hyPolicyNo = CType(e.Item.FindControl("hypolicyNo"), HyperLink)
            hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & hyTemp.Text.Trim & "', '" & Replace(Me.sesBranchId, "'", "") & "')"

            'Dim txtPremiumTobePaid As TextBox
            'Dim lblTotal As Label
            'Dim lblSelisihPendapatan As Label
            'Dim strJSFunction As String

            'txtPremiumTobePaid = CType(e.Item.FindControl("txtPremiumTobePaid"), TextBox)
            'txtPremiumTobePaid.Text = FormatNumber(txtPremiumTobePaid.Text)
            'lblTotal = CType(e.Item.FindControl("lblTotal"), Label)
            'lblSelisihPendapatan = CType(e.Item.FindControl("lblSelisihPendapatan"), Label)


            'strJSFunction = "calculateSelisih" & e.Item.ItemIndex.ToString & "()"
            'ScriptManager.RegisterStartupScript(txtPremiumTobePaid, txtPremiumTobePaid.GetType(), txtPremiumTobePaid.ClientID, _
            '                                    calculateSelisihJS(strJSFunction, lblTotal, txtPremiumTobePaid, lblSelisihPendapatan), True)
            'txtPremiumTobePaid.Attributes.Add("onblur", strJSFunction & ";")
        End If
    End Sub

    Private Sub dtgTerimaTagihan_DataBinding(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgTerimaTagihan.ItemDataBound
        Dim lblTemp As Label
        Dim hyTemp As HyperLink
        Dim hyPolicyNo As HyperLink

        If e.Item.ItemIndex > -1 Then
            lblTemp = CType(e.Item.FindControl("lblCustomerId"), Label)
            hyTemp = CType(e.Item.FindControl("hyCustomerName"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(lblTemp.Text.Trim) & "')"

            '*** Agreement No link
            Dim lblApplicationId As Label
            lblApplicationId = CType(e.Item.FindControl("lblApplicationId"), Label)
            hyTemp = CType(e.Item.FindControl("hyAgreementNo"), HyperLink)
            hyTemp.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(lblApplicationId.Text.Trim) & "')"

            '*** Policy No link
            hyPolicyNo = CType(e.Item.FindControl("hypolicyNo"), HyperLink)
            hyPolicyNo.NavigateUrl = "javascript:OpenWinViewPolicyNo('" & hyTemp.Text.Trim & "', '" & Replace(Me.sesBranchId, "'", "") & "')"


            TotalPremiGross = TotalPremiGross + CDbl(CType(e.Item.FindControl("lblPremiGross"), Label).Text)
            TotalKomisi = TotalKomisi + CDbl(CType(e.Item.FindControl("lblKomisi"), Label).Text)
            TotalPremiNett = TotalPremiNett + CDbl(CType(e.Item.FindControl("lblPremiNett"), Label).Text)
            TotalPPn = TotalPPn + CDbl(CType(e.Item.FindControl("lblPPn"), Label).Text)
            TotalPPH = TotalPPH + CDbl(CType(e.Item.FindControl("lblPPH"), Label).Text)
            TotalBiayaPolis = TotalBiayaPolis + CDbl(CType(e.Item.FindControl("lblBiayaPolis"), Label).Text)
            TotalBiayaMaterai = TotalBiayaMaterai + CDbl(CType(e.Item.FindControl("lblBiayaMaterai"), Label).Text)
            TotalTotalTagihan = TotalTotalTagihan + CDbl(CType(e.Item.FindControl("lblTotalTagihan"), Label).Text)
            CType(e.Item.FindControl("lblTotalTagihanAll"), Label).Text = FormatNumber(TotalTotalTagihan, 0)
        End If
        If e.Item.ItemType = ListItemType.Footer Then
            CType(e.Item.FindControl("lblTotalPremiGross"), Label).Text = FormatNumber(TotalPremiGross, 0)
            CType(e.Item.FindControl("lblTotalKomisi"), Label).Text = FormatNumber(TotalKomisi, 0)
            CType(e.Item.FindControl("lblTotalPremiNett"), Label).Text = FormatNumber(TotalPremiNett, 0)
            CType(e.Item.FindControl("lblTotalPPn"), Label).Text = FormatNumber(TotalPPn, 0)
            CType(e.Item.FindControl("lblTotalPPH"), Label).Text = FormatNumber(TotalPPH, 0)
            CType(e.Item.FindControl("lblTotalBiayaPolis"), Label).Text = FormatNumber(TotalBiayaPolis, 0)
            CType(e.Item.FindControl("lblTotalBiayaMaterai"), Label).Text = FormatNumber(TotalBiayaMaterai, 0)
            CType(e.Item.FindControl("lblTotalTotalTagihan"), Label).Text = FormatNumber(TotalTotalTagihan, 0)
        End If
    End Sub

    Private Function calculateSelisihJS(ByVal strJSFunction As String, ByVal lblTotal As Label, ByVal txtPremiumTobePaid As TextBox, ByVal lblSelisihPendapatan As Label) As String
        Dim strBuilder As New StringBuilder
        strBuilder.AppendLine("function " & strJSFunction & "{")
        strBuilder.AppendLine("var oTotal = document.getElementById('" & lblTotal.ClientID & "');")
        strBuilder.AppendLine("var oBayar = document.getElementById('" & txtPremiumTobePaid.ClientID & "');")
        strBuilder.AppendLine("var oSelisih = document.getElementById('" & lblSelisihPendapatan.ClientID & "');")
        strBuilder.AppendLine("oSelisih.innerText = Number(oTotal.innerText.replace(/,/g,"""")) - Number(oBayar.value);")
        strBuilder.AppendLine("}")
        Return strBuilder.ToString
    End Function

    'TODO fixed onTextChanged
    Public Sub calculateSelisih(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim txtPremiumTobePaid As TextBox
        Dim lblTotal As Label
        Dim lblSelisihPendapatan As Label

        Dim dtItem As DataGridItem = CType(sender, TextBox).Parent.Parent

        If dtItem.ItemType = ListItemType.Item Then
            txtPremiumTobePaid = CType(dtItem.FindControl("txtPremiumTobePaid"), TextBox)
            lblTotal = CType(dtItem.FindControl("lblTotal"), Label)
            lblSelisihPendapatan = CType(dtItem.FindControl("lblSelisihPendapatan"), Label)

            lblSelisihPendapatan.Text = FormatNumber(CDbl(lblTotal.Text) - CDbl(txtPremiumTobePaid.Text), 2)
        End If
    End Sub
#End Region
#Region "Jumlahkan"
    Public Function Jumlahkan(ByVal AmountPremium As Double) As Double
        SumAmountPremium = SumAmountPremium + AmountPremium
        Return SumAmountPremium
    End Function
#End Region
    Private Sub btnCalculate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        ProcessTempTable(True)
    End Sub
#Region "Tombol click !"

    Private Sub ButtonSaveTerima_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSaveTerima.Click
        Dim oCustomList As New List(Of Parameter.InsCoAllocationDetailList)
        Dim oCustom As New Parameter.InsCoAllocationDetailList

        Try
            With oCustom
                .strConnection = GetConnectionString()
                .InvoiceNo = txtNoRekap.Text.Trim
                .InvoiceDate = ConvertDate2(txtTglRekap.Text)
                .DueDateEntry = ConvertDate2(txtTglRencanaBayar.Text)
                .BusinessDate = Me.BusinessDate
            End With

            For index = 0 To dtgTerimaTagihan.Items.Count - 1
                If index = 0 Then
                    oCustom.BranchId = CType(dtgTerimaTagihan.Items(index).FindControl("lblBranchId"), Label).Text.Trim
                    oCustom.MaskAssID = CType(dtgTerimaTagihan.Items(index).FindControl("lblMaskAssID"), Label).Text.Trim
                    oCustom.InsuranceComBranchID = CType(dtgTerimaTagihan.Items(index).FindControl("lblInsuranceComBranchID"), Label).Text.Trim
                    oCustom.TotalTagihanAll = CDec(CType(dtgTerimaTagihan.Items(index).FindControl("lblTotalTagihanAll"), Label).Text.Trim)
                End If
                Dim _Custom As New Parameter.InsCoAllocationDetailList
                _Custom.ApplicationID = CType(dtgTerimaTagihan.Items(index).FindControl("lblApplicationId"), Label).Text.Trim
                _Custom.AssetseqNo = CShort(CType(dtgTerimaTagihan.Items(index).FindControl("lblAssetSeqNo"), Label).Text.Trim)
                _Custom.InsSequenceNo = CShort(CType(dtgTerimaTagihan.Items(index).FindControl("lblInsSequenceNo"), Label).Text.Trim)
                _Custom.AccountPayableNo = CType(dtgTerimaTagihan.Items(index).FindControl("lblAccountPayableNo"), Label).Text.Trim
                _Custom.PolicyNumber = CType(dtgTerimaTagihan.Items(index).FindControl("lblBiayaPolis"), Label).Text.Trim
                _Custom.TotalTagihanItem = CDec(CType(dtgTerimaTagihan.Items(index).FindControl("lblTotalTagihan"), Label).Text.Trim)

                oCustomList.Add(_Custom)
            Next

            oSaveController.SaveInsuranceBillingSave(oCustom, oCustomList)
            Response.Redirect("InsuranceBilling.aspx?InsuranceComBranchID=" + Me.InsuranceComBranchID + "&StatusPage=" + StatusPage + "")
        Catch ex As Exception
            Exit Sub
            ShowMessage(lblMessages, ex.Message, True)
        End Try
    End Sub
    Private Sub ButtonCancelTerima_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancelTerima.Click
        Response.Redirect("InsuranceBilling.aspx?InsuranceComBranchID=" + Me.InsuranceComBranchID + "&StatusPage=" + StatusPage + "")
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Sub Process dan save
        'Check Dahulu apakah invoicenya pernah ada atau belum 
        Dim oCheck As New Parameter.InsCoAllocationDetailList
        Dim oCheckController As New InsuranceBillingSelectController
        With oCheck
            .InvoiceNo = TxtInvoiceNo.Text
            .InsuranceComBranchID = Me.InsCo
            .BranchId = Replace(Me.sesBranchId, "'", "")
            .strConnection = GetConnectionString()
            .spName = "spInsCheckInvoiceNo"
        End With

        Dim result As String
        result = oCheckController.CheckInvoiceNo(oCheck)
        If result <> "0" Then
            ShowMessage(lblMessages, "No Invoice :  '" & TxtInvoiceNo.Text & "' Sudah Ada", True)
            Exit Sub
        End If
        ProcessTempTable(False)
    End Sub

#End Region
    Public Sub ProcessTempTable(ByVal iscalculate As Boolean)
        Dim i As Integer
        Dim strparameter As HyperLink
        Dim txtGridPaid As TextBox
        Dim resulttemp As Double
        Dim dblAdminFee As Double
        Dim dblMateraiFee As Double
        For i = 0 To dtgPaging.Items.Count - 1
            strparameter = CType(dtgPaging.Items(i).FindControl("hyAccountPayableNo"), HyperLink)
            txtGridPaid = CType(dtgPaging.Items(i).FindControl("TxtPremiumToBePaid"), TextBox)
            resulttemp = Jumlahkan(CType(txtGridPaid.Text, Double))
            dblAdminFee = CDbl(CType(dtgPaging.Items(i).FindControl("lblAdminFee"), Label).Text)
            dblMateraiFee = CDbl(CType(dtgPaging.Items(i).FindControl("lblMateraiFee"), Label).Text)
            temptotaladminfee += dblAdminFee
            temptotalstampfee += dblMateraiFee
        Next
        'total admin sudah ada ditiap row
        'totalinvoiceamount = resulttemp + temptotaladminfee + temptotalstampfee
        totalinvoiceamount = resulttemp
        TxtAdminFee.Text = temptotaladminfee.ToString
        TxtPolicyFee.Text = temptotalstampfee.ToString
        LblTotalInvoiceAmount.Text = FormatNumber(totalinvoiceamount, 2)
        If iscalculate = False Then
            If Bandingkan(Math.Round(totalinvoiceamount), Math.Round(Me.TotInvoiceAmount)) Then
                SaveInsuranceBillingSelect()
            Else
                ShowMessage(lblMessages, "Nilai invoice (" & FormatNumber(totalinvoiceamount, 2) & ") berubah sejak perhitungan terakhir (" &
                    FormatNumber(Me.TotInvoiceAmount, 2) & "). Hitung ulang!", True)
            End If
        Else
            Me.TotInvoiceAmount = Math.Round(totalinvoiceamount, 2)
        End If
    End Sub
#Region "Bandingkan Nilai Total Invoice Amount Sebelum modifikasi"

    Public Function Bandingkan(ByVal CurrentTotalInvoiceAmount As Double, ByVal totalinvoicesebelummodifikasi As Double) As Boolean
        If CurrentTotalInvoiceAmount <> totalinvoicesebelummodifikasi Then
            Return False
        Else
            Return True
        End If

    End Function

#End Region
#Region "Proses Save"
    Public Sub SaveInsuranceBillingSelect()
        Dim LoopSaveInsuranceBillingSelect As Integer
        Dim strparameter As HyperLink
        Dim strparameterAccountPayableNo As HyperLink
        Dim strPolicyNo As HyperLink
        Dim strPremiumEndorsmentToInsco As Label
        Dim txtGridPaid As TextBox
        Dim CtrAPDescriptionEndorsmentPremium As Integer
        Dim hyAPDescription As HyperLink
        CtrAPDescriptionEndorsmentPremium = 0
        Dim oEntities As New Parameter.InsCoAllocationDetailList

        Try

            With oEntities
                .InsuranceComBranchID = Me.InsCo
                .CoyID = Replace(Me.SesCompanyID, "'", "")
                '.BranchId = Replace(Me.sesBranchId, "'", "")
                .BranchId = Right(Me.InsCo, 3)
                .BusinessDate = Me.BusinessDate
                .InvoiceNo = TxtInvoiceNo.Text
                .InvoiceDate = ConvertDate2(txtInvoiceDate.Text)
                .TotalInvoiceAmount = CType(totalinvoiceamount, Double)
                .TotalAdminFee = temptotaladminfee
                .TotalStampDutyFee = temptotalstampfee
                .LoginId = Me.Loginid
                .strConnection = GetConnectionString()
            End With

            For LoopSaveInsuranceBillingSelect = 0 To dtgPaging.Items.Count - 1
                hyAPDescription = CType(dtgPaging.Items(LoopSaveInsuranceBillingSelect).FindControl("hyAPDescription"), HyperLink)
                If Left(hyAPDescription.Text.Trim, 18) = "Endorsment Premium" Then
                    CtrAPDescriptionEndorsmentPremium = CtrAPDescriptionEndorsmentPremium + 1
                End If
            Next
            LoopSaveInsuranceBillingSelect = 0


            Dim jumlahgrid As Integer = dtgPaging.Items.Count
            jumlahgrid = jumlahgrid - CtrAPDescriptionEndorsmentPremium

            Dim partialadminfee As Double = temptotaladminfee / jumlahgrid
            Dim partialstampfee As Double = temptotalstampfee / jumlahgrid
            Dim dtdetail As New DataTable
            Dim drdetail As DataRow
            dtdetail.Columns.Add("AgreementNo")
            dtdetail.Columns.Add("AccountPayableNo")
            dtdetail.Columns.Add("PolicyNumber")
            dtdetail.Columns.Add("PremiumEndorsmentToInsco")
            dtdetail.Columns.Add("PremiumToBePaid")
            dtdetail.Columns.Add("BranchId")
            dtdetail.Columns.Add("CoyID")
            dtdetail.Columns.Add("BusinessDate")
            dtdetail.Columns.Add("AdminFee")
            dtdetail.Columns.Add("MeteraiFee")
            dtdetail.Columns.Add("InvoiceNo")
            dtdetail.Columns.Add("InvoiceDate")
            dtdetail.Columns.Add("DueDate")
            dtdetail.Columns.Add("InsuranceComBranchID")


            For LoopSaveInsuranceBillingSelect = 0 To dtgPaging.Items.Count - 1
                strparameter = CType(dtgPaging.Items(LoopSaveInsuranceBillingSelect).FindControl("hyAgreementNo"), HyperLink)
                strparameterAccountPayableNo = CType(dtgPaging.Items(LoopSaveInsuranceBillingSelect).FindControl("hyAccountPayableNo"), HyperLink)
                txtGridPaid = CType(dtgPaging.Items(LoopSaveInsuranceBillingSelect).FindControl("TxtPremiumToBePaid"), TextBox)
                strPolicyNo = CType(dtgPaging.Items(LoopSaveInsuranceBillingSelect).FindControl("hyPOLICYNO"), HyperLink)
                strPremiumEndorsmentToInsco = CType(dtgPaging.Items(LoopSaveInsuranceBillingSelect).FindControl("GridPremiumEndorsmentToInsco"), Label)
                hyAPDescription = CType(dtgPaging.Items(LoopSaveInsuranceBillingSelect).FindControl("hyAPDescription"), HyperLink)
                'Save Detail

                'Dim oEntitesDetail As New Parameter.InsCoAllocationDetailList
                drdetail = dtdetail.NewRow
                With oEntities
                    drdetail("AgreementNo") = strparameter.Text
                    drdetail("PolicyNumber") = strPolicyNo.Text
                    drdetail("PremiumEndorsmentToInsco") = CType(strPremiumEndorsmentToInsco.Text, Double)
                    drdetail("PremiumToBePaid") = CType(txtGridPaid.Text, Double)
                    'drdetail("BranchId") = Replace(Me.sesBranchId, "'", "")
                    drdetail("BranchId") = Right(Replace(Me.InsCo, "'", ""), 3)
                    drdetail("CoyID") = Replace(Me.SesCompanyID, "'", "")
                    drdetail("BusinessDate") = Me.BusinessDate
                    If Left(hyAPDescription.Text.Trim, 18) = "Endorsment Premium" Then
                        drdetail("AdminFee") = 0
                        drdetail("MeteraiFee") = 0
                    Else
                        drdetail("AdminFee") = partialadminfee
                        drdetail("MeteraiFee") = partialstampfee
                    End If
                    drdetail("InvoiceNo") = TxtInvoiceNo.Text
                    drdetail("InvoiceDate") = ConvertDate2(txtInvoiceDate.Text)
                    drdetail("DueDate") = ConvertDate2(txtDueDate.Text)
                    drdetail("InsuranceComBranchID") = Me.InsCo
                    drdetail("AccountPayableNo") = strparameterAccountPayableNo.Text
                End With
                dtdetail.Rows.Add(drdetail)
            Next
            oEntities.ListData = dtdetail
            oSaveController.SaveInsuranceBillingSelectDetail(oEntities)
            Response.Redirect("InsuranceBilling.aspx")
        Catch ex As Exception
            ShowMessage(lblMessages, ex.Message, True)
        End Try
    End Sub
#End Region

End Class