﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceBillingSelect.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceBillingSelect" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagName="ucDateCE" TagPrefix="uc1" Src="../Webform.UserController/ucDateCE.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Insurance Billing Selection</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script language="javascript" type="text/javascript">
        function OpenWinViewPolicyNo(pAgreementNo, pBranch) {
            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.Insurance/ViewPolicyDetail.aspx?AgreementNo=' + pAgreementNo + '&BranchID=' + pBranch, 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0,scrollbars=1')
        }
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
    <asp:Label ID="lblMessages" runat="server"></asp:Label>
    <asp:UpdatePanel>
        <ContentTemplate>
            <asp:Panel runat="server" ID="pnlTagihanInsurance">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            TAGIHAN ASURANSI
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Perusahaan Asuransi</label>
                        <asp:Label ID="LblInsuranceCompany" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Invoice</label>
                        <asp:TextBox ID="TxtInvoiceNo" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Harap isi No Invoice"
                            ControlToValidate="TxtInvoiceNo" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Invoice</label>
                        <asp:TextBox runat="server" ID="txtInvoiceDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtInvoiceDate"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Jatuh Tempo</label>
                        <asp:TextBox runat="server" ID="txtDueDate"></asp:TextBox>
                        <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtDueDate"
                            Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TAGIHAN ASURANSI</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn SortExpression="AccountPayableNo" HeaderText="NO A/P">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyAccountPayableNo" runat="server" Text='<%#Container.DataItem("AccountPayableNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA CUSTOMER">
                                        <HeaderStyle HorizontalAlign="Center" Width="15%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Left" Width="15%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="PolicyNumber" HeaderText="NO POLIS">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px" Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyPOLICYNO" runat="server" Text='<%#Container.DataItem("PolicyNumber")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="APDescription" HeaderText="KETERANGAN A/P" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Height="30px" Width="20%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" Width="20%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyAPDescription" runat="server" Text='<%#Container.DataItem("APDescription")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DIBAYAR CUSTOMER" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblGridPAIDAMOUNTTOCUST" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PremiumPaidByCUst"),2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI STANDARD" Visible="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Right" Width="10%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="GridPremiumEndorsmentToInsco" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PremiumEndorsmentAmountToInsco"), 0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI KE ASURANSI">
                                        <HeaderStyle CssClass="th_right"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPremiumAmountToInsco" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PremiumAmountToInsco"),2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="PPh" HeaderText="PPH 23" DataFormatString="{0:N0}"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PPN" HeaderText="PPN" DataFormatString="{0:N0}"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA POLIS">
                                        <HeaderStyle CssClass="th_right"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAdminFee" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.AdminFee"),2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="MATERAI FEE">
                                        <HeaderStyle CssClass="th_right"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMateraiFee" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.MateraiFee"), 0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TOTAL">
                                        <HeaderStyle CssClass="th_right"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotal" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PremiumAmountToInsco") - DataBinder.Eval(Container, "DataItem.PPN") + DataBinder.Eval(Container, "DataItem.PPh") + DataBinder.Eval(Container, "DataItem.AdminFee") + DataBinder.Eval(Container, "DataItem.MateraiFee"), 0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI DIBAYAR">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TxtPremiumToBePaid" runat="server" AutoPostBack="true" CssClass="numberAlign"
                                                Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PremiumAmountToInsco") - DataBinder.Eval(Container, "DataItem.PPN") + DataBinder.Eval(Container, "DataItem.PPh") + DataBinder.Eval(Container, "DataItem.AdminFee") + DataBinder.Eval(Container, "DataItem.MateraiFee"), 0) %>'
                                                OnTextChanged="calculateSelisih" >
                                            </asp:TextBox>
                                            <asp:Label ID="LblSelisih" runat="server" Text='<%#  container.dataitem("SelisihToleransi") %>'
                                                Visible="False">
                                            </asp:Label>
                                            <br />
                                           
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="SELISIH">
                                        <HeaderStyle CssClass="th_right"></HeaderStyle>
                                        <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSelisihPendapatan" runat="server" Text='<%# FormatNumber(0, 2) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" SortExpression="" HeaderText="CUSTOMER ID">
                                        <HeaderStyle HorizontalAlign="Center" Width="1%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="1%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                        <HeaderStyle HorizontalAlign="Center" Width="1%"></HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" Width="1%"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_box_title_nobg">
                    <div class="form_single">
                        <%--<label> Total Biaya Admin</label>--%>
                        <label> Total Biaya Polis</label>
                        <%-- <asp:TextBox ID="TxtAdminFee" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Harap Isi dengan Angka"
                ControlToValidate="TxtAdminFee" ValidationExpression="\d*" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                        <%--<uc1:ucnumberformat id="TxtAdminFee" runat="server"/>--%>
                        <%-- <asp:Label ID="TxtAdminFee" runat="server"></asp:Label>--%>
                         <asp:Label ID="TxtPolicyFee" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Total Biaya Materai</label>
                        <%--<asp:TextBox ID="TxtPolicyFee" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Harap Isi dengan Angka"
                ControlToValidate="TxtPolicyFee" ValidationExpression="\d*" Display="Dynamic"></asp:RegularExpressionValidator>--%>
                       <%-- <uc1:ucnumberformat id="TxtPolicyFee" runat="server" ReadOnly="True"/>
                        </uc1:ucNumberFormat>--%>
                        <%--<asp:Label ID="TxtPolicyFee" runat="server"></asp:Label>--%>
                        <asp:Label ID="TxtAdminFee" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Total Nilai Invoice</label>
                        <asp:Label ID="LblTotalInvoiceAmount" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="btnCalculate" runat="server" Text="Calculate" CssClass="small button green">
                    </asp:Button>
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlTerimaTagihan">
                <div class="form_title">
                    <div class="title_strip">
                    </div>
                    <div class="form_single">
                        <h3>
                            TERIMA TAGIHAN
                        </h3>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Rekap</label>
                        <asp:TextBox ID="txtNoRekap" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Rekap</label>
                        <uc1:ucdatece id="txtTglRekap" runat="server"></uc1:ucdatece>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Tanggal Rencana Bayar</label>
                        <uc1:ucdatece id="txtTglRencanaBayar" runat="server"></uc1:ucdatece>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            DAFTAR TAGIHAN ASURANSI</h4>
                    </div>
                </div>
                <div class="form_box_header">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgTerimaTagihan" runat="server" CssClass="grid_general" OnSortCommand="Sorting"
                                BorderWidth="0" AutoGenerateColumns="False" AllowSorting="True" ShowFooter="True">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <FooterStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA CUSTOMER">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="PolicyNumber" HeaderText="NO POLIS">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hyPOLICYNO" runat="server" Text='<%#Container.DataItem("PolicyNumber")%>'>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            Total
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI GROSS">                                        
                                        <ItemStyle CssClass="item_grid_right" />
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblPremiGross" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PremiGross"),0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="lblTotalPremiGross"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="KOMISI">
                                        <ItemStyle CssClass="item_grid_right" />
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblKomisi" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Komisi"),0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="lblTotalKomisi"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PREMI NET">
                                        <ItemStyle CssClass="item_grid_right" />
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblPremiNett" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PremiNett"),0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="lblTotalPremiNett"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PPN">
                                        <ItemStyle CssClass="item_grid_right" />
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblPPn" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PPn"),0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="lblTotalPPn"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="PPH">
                                        <ItemStyle CssClass="item_grid_right" />
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblPPH" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.PPH"),0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="lblTotalPPH"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA POLIS">
                                        <ItemStyle CssClass="item_grid_right" />
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblBiayaPolis" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.BiayaPolis"),0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="lblTotalBiayaPolis"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="BIAYA MATERAI">
                                        <ItemStyle CssClass="item_grid_right" />
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblBiayaMaterai" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.BiayaMaterai"),0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="lblTotalBiayaMaterai"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="TOTAL TAGIHAN">
                                        <ItemStyle CssClass="item_grid_right" />
                                        <FooterStyle CssClass="item_grid_right" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalTagihan" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.TotalTagihan"),0) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label runat="server" ID="lblTotalTotalTagihan"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn Visible="False">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTotalTagihanAll"></asp:Label>
                                            <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblBranchId" runat="server" Text='<%#Container.DataItem("BranchId")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblAssetSeqNo" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblInsSequenceNo" runat="server" Text='<%#Container.DataItem("InsSequenceNo")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblMaskAssID" runat="server" Text='<%#Container.DataItem("MaskAssID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblInsuranceComBranchID" runat="server" Text='<%#Container.DataItem("InsuranceComBranchID")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblAccountPayableNo" runat="server" Text='<%#Container.DataItem("AccountPayableNo")%>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSaveTerima" runat="server" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancelTerima" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
