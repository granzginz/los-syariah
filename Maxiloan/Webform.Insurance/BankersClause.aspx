﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="BankersClause.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.BankersClause" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ENDORS NAMA JOINT FINANCING</title>
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
        var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%#Request.servervariables("SERVER_NAME")%>/';

        function fBack() {
            history.back(-1);
            return false;
        }
        function DeleteConfirm() {
            if (confirm("Apakah yakin mau hapus data ini ?")) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updatePanel1">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        ENDORSMENT NAMA TERTANGGUNG BANKER"S CLAUSE
                    </h3>
                </div>
            </div>
            <asp:Panel ID="pnlList" runat="server">
                <div class="form_box">
                    <div class="form_single">
                       <label class ="label_req">
                            Tanggal</label>
                        <asp:TextBox ID="TglClause" runat="server" CssClass ="small_text"></asp:TextBox>
                        <asp:CalendarExtender ID="TglClause_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="TglClause" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="TglClause"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                       <label class ="label_req">
                            No</label>
                        <asp:TextBox ID="txtNoClause" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtNoClause"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                      <label class ="label_req">
                            Perusahaan Asuransi</label>
                        <asp:DropDownList OnTextChanged="OnTextChanged_InsuranceBranch" AutoPostBack="true"
                            ID="cboInsuranceBranch" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ReqValField" runat="server" ErrorMessage="Please Select Insurance Branch"
                            ControlToValidate="cboInsuranceBranch" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                      <label class ="label_req">
                            Contact Person</label>
                        <asp:TextBox ID="txtContactPerson" runat="server" CssClass="long_text"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            CssClass="validator_general" ControlToValidate="txtContactPerson"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <h4>
                            CARI KONTRAK
                        </h4>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            Cari Berdasarkan</label>
                        <asp:DropDownList ID="cboSearch" runat="server">
                            <asp:ListItem Value="ApplicationID">No Kontrak</asp:ListItem>
                            <asp:ListItem Value="Name">Nama Konsumen</asp:ListItem>
                            <asp:ListItem Value="AttributeContent">No Polisi</asp:ListItem>
                            <asp:ListItem Value="PolicyNumber">No Polis</asp:ListItem>
                        </asp:DropDownList>
                        <asp:TextBox ID="txtSearch" runat="server" Width="20%" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSearch" runat="server" CausesValidation="False" Text="Search"
                        CssClass="small button blue"></asp:Button>&nbsp;
                    <asp:Button ID="ButtonReset" runat="server" CausesValidation="False" Text="Reset"
                        CssClass="small button gray"></asp:Button>
                </div>
                <div class="form_box_title">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="dtgPaging" runat="server" Width="100%" AllowSorting="True" AutoGenerateColumns="False"
                                OnSortCommand="Sorting" DataKeyField="ApplicationID" BorderStyle="None" BorderWidth="0"
                                CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PILIH">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="check" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lblName" Text='<%#Container.Dataitem("Name")%>' />
                                            <asp:HiddenField ID="hdnCustomerID" Value='<%#Container.Dataitem("CustomerID")%>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NAMA KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lblApplicationID" Text='<%#Container.Dataitem("ApplicationID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="FundingCoyName" SortExpression="FundingCoyName" HeaderText="BANK">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AttributeContent" SortExpression="AttributeContent" HeaderText="NO POLISI">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PolicyNumber" SortExpression="PolicyNumber" HeaderText="NO POLIS">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div class="button_gridnavigation">
                                <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../Images/grid_navbutton01.png"
                                    CommandName="First" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../Images/grid_navbutton02.png"
                                    CommandName="Prev" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../Images/grid_navbutton03.png"
                                    CommandName="Next" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../Images/grid_navbutton04.png"
                                    CommandName="Last" OnCommand="NavigationLink_Click" CausesValidation="False">
                                </asp:ImageButton>
                                <asp:TextBox ID="txtPage" runat="server">1</asp:TextBox>
                                <asp:Button ID="btnPageNumb" runat="server" Text="Go" CssClass="small buttongo blue"
                                    EnableViewState="False"></asp:Button>
                                <asp:RangeValidator ID="rgvGo" runat="server" ControlToValidate="txtPage" MinimumValue="1"
                                    ErrorMessage="No Halaman Salah" MaximumValue="999999999" Type="Integer" CssClass="validator_general"></asp:RangeValidator>
                                <asp:RequiredFieldValidator ID="rfvGo" runat="server" ControlToValidate="txtPage"
                                    ErrorMessage="No Halaman Salah" Display="Dynamic" CssClass="validator_general"></asp:RequiredFieldValidator>
                            </div>
                            <div class="label_gridnavigation">
                                <asp:Label ID="lblPage" runat="server"></asp:Label>of
                                <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total
                                <asp:Label ID="lblrecord" runat="server"></asp:Label>record(s)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSelect" runat="server" CausesValidation="true" Text="Select"
                        CssClass="small button blue"></asp:Button>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlAddEdit" runat="server">
                <div class="form_box_title">
                    <div class="form_single">
                        <div class="grid_wrapper_ns">
                            <asp:DataGrid ID="DataGridEndorsment" runat="server" Width="100%" AllowSorting="True"
                                AutoGenerateColumns="False" OnSortCommand="Sorting" DataKeyField="ApplicationID"
                                BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                                <HeaderStyle CssClass="th" />
                                <ItemStyle CssClass="item_grid" />
                                <Columns>
                                    <asp:TemplateColumn HeaderText="No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNo" Text='<%#Container.Dataitem("No")%>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="Name" HeaderText="NAMA KONSUMEN">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lblName" Text='<%#Container.Dataitem("Name")%>' />
                                            <asp:HiddenField ID="hdnCustomerID" Value='<%#Container.Dataitem("CustomerID")%>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn SortExpression="ApplicationID" HeaderText="NAMA KONTRAK">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lblApplicationID" Text='<%#Container.Dataitem("ApplicationID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="FundingCoyName" SortExpression="FundingCoyName" HeaderText="BANK">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AttributeContent" SortExpression="AttributeContent" HeaderText="NO POLISI">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="PolicyNumber" SortExpression="PolicyNumber" HeaderText="NO POLIS">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <div class="form_button">
                    <asp:Button ID="ButtonSave" runat="server" CausesValidation="true" Text="Save" CssClass="small button blue">
                    </asp:Button>
                    <asp:Button ID="ButtonCancel" runat="server" CausesValidation="true" Text="Cancel"
                        CssClass="small button gray"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
