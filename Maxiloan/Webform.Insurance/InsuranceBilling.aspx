﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceBilling.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceBilling" %>

<%@ Register TagPrefix="uc1" TagName="UcInsuranceBranchName" Src="../Webform.UserController/UcInsuranceBranchName.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Insurance Billing</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var AppInfo = '<%= Request.servervariables("PATH_INFO")%>';
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1)
        var ServerName = 'http://<%=Request.servervariables("SERVER_NAME")%>/';
        function OpenWinViewPolicyNo(BranchID, AgreementNo, strasset, strins, strApplication) {
            var x = screen.width; var y = screen.height - 100; window.open('ViewPolicyDetail.aspx?BranchID=' + BranchID + '&AgreementNo=' + AgreementNo + '&AssetSeqNo=' + strasset + '&InsSeqNo=' + strins + '&ApplicationID=' + strApplication + '&Back=Billing' + '&style=Insurance', 'UserLookup', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="LblErrorMessages" runat="server" ForeColor="Red"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h4>
                CARI TAGIHAN ASURANSI
            </h4>
        </div>
    </div>
    <div class="form_box">
        <div class="form_left">
            <label>
                Cabang
            </label>
            <asp:Label ID="LblBranchName" runat="server"></asp:Label>
        </div>
        <div class="form_right">
            <label>
                Perusahaan Asuransi
            </label>
            <uc1:ucinsurancebranchname id="oInsuranceBranchName" runat="server">
                </uc1:ucinsurancebranchname>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan</label>
            <uc1:ucsearchbywithnotable id="oSearchBy" runat="server">
                </uc1:ucsearchbywithnotable>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
               Product Asuransi</label>
            <asp:DropDownList ID="cboProductAsuransi" runat="server">
                <%--<asp:ListItem Value="CP">Credit Protection</asp:ListItem>
                <asp:ListItem Value="JK">Jaminan Kredit</asp:ListItem>
                <asp:ListItem Value="KB">Kendaraan Bermotor</asp:ListItem>--%>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    <asp:Panel ID="PnlGrid" runat="server">
         <div class="form_title">
            <div class="title_strip">
            </div>
            <div class="form_single">
                <h3>
                    DAFTAR TAGIHAN ASURANSI
                </h3>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid  ID="dtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0"
                        CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkStatus_CheckedChanged"
                                        AutoPostBack="True" CssClass="checkbox_general"></asp:CheckBox>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="ChkStatusAccountPayableNo" runat="server" Width="30px" Font-Size="Smaller">
                                    </asp:CheckBox>
                                    <asp:Label ID="LblAgreementNo" runat="server" Text='<%#  container.dataitem("AgreementNo") %>'
                                        Visible="False" Font-Size="Smaller">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AccountPayableNo" HeaderText="NO A/P" Visible="false">
                                <HeaderStyle Width="135px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAccountPayableNo" runat="server" Text='<%#Container.DataItem("AccountPayableNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle Width="110px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementGrid" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA CUSTOMER">
                                <HeaderStyle Width="220"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AssetDesc" HeaderText="KENDARAAN">
                                <HeaderStyle Width="350px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAssetDesc" runat="server" Text='<%#Container.DataItem("AssetDesc")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                          <%-- <asp:TemplateColumn SortExpression="NoNotaAsuransi" HeaderText="NO NOTA">
                                <ItemTemplate>
                                    <asp:Label ID="lblNoNotaAsuransi" runat="server" Text='<%#Container.DataItem("NoNotaAsuransi")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>--%>
                            <asp:TemplateColumn SortExpression="PolicyNumber" HeaderText="NO POLIS">
                                <HeaderStyle Width="140px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyPOLICYNO" runat="server" Text='<%#Container.DataItem("PolicyNumber")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn Visible="False" DataField="PolicyReceiveDate" SortExpression="PolicyReceiveDate"
                                HeaderText="POLICY RECEIVE DATE" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="CustomerId" HeaderText="CUSTOMER ID">
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="TERMOFPAYMENT" HeaderText="TERMIN">
                                <HeaderStyle Width="20px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblTermOfPayment" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TERMOFPAYMENT") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="APAMOUNT" HeaderText="JUMLAH">
                                <HeaderStyle CssClass="th_right" Width="60px"></HeaderStyle>
                                <ItemStyle CssClass="item_grid_right"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="LblAPAmount" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.APAmount"),0) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="ContractStatus" HeaderText="STS">
                                <HeaderStyle Width="40px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblContractStatus" runat="server" Text='<%#Container.DataItem("ContractStatus")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                             <asp:TemplateColumn SortExpression="ProductAsuransi" HeaderText="Product Asuransi">
                                <HeaderStyle Width="40px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblProductAsuransi" runat="server" Text='<%#Container.DataItem("ProductAsuransi")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="APDESCRIPTION" HeaderText="KETERANGAN A/P" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="LblAPDescription" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.APDescription") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="AssetSeqNo">
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblAsset" runat="server" Text='<%#Container.DataItem("AssetSeqNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="InsSequenceNo">
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblIns" runat="server" Text='<%#Container.DataItem("InsSequenceNo")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>                            
                            <%--Tambahin by Wira 2016-02-2--%>
                            <asp:TemplateColumn Visible="False" HeaderText="BranchIdIns">
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblBranchIdIns" runat="server" Text='<%#Container.DataItem("BranchID")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>                            
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton01.png"
                        OnCommand="NavigationLink_Click" CommandName="First"></asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton02.png"
                        OnCommand="NavigationLink_Click" CommandName="Prev"></asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton03.png"
                        OnCommand="NavigationLink_Click" CommandName="Next"></asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" CausesValidation="False" ImageUrl="../Images/grid_navbutton04.png"
                        OnCommand="NavigationLink_Click" CommandName="Last"></asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server"  Width="34px">1</asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False"></asp:Button>
                    <asp:RangeValidator ID="rgvGo" runat="server" CssClass="validator_general" Type="Integer"
                        MaximumValue="999999999" ErrorMessage="No Halaman Salah" MinimumValue="1" ControlToValidate="txtGoPage"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvGo" runat="server" CssClass="validator_general"
                        ErrorMessage="No Halaman Salah" ControlToValidate="txtGoPage" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
        <div class="form_button">
            <asp:Button ID="btnSelect" runat="server" CausesValidation="False" Text="Select"
                CssClass="small button blue"></asp:Button>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
