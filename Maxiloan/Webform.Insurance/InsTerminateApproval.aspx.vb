﻿Imports Maxiloan.Parameter
Imports Maxiloan.Exceptions
Imports Maxiloan.General
Imports Maxiloan.Controller
Imports Maxiloan.Webform.UserController

Public Class InsTerminateApproval
    Inherits Maxiloan.Webform.WebBased
    Protected WithEvents oReason As UCReason
    Protected WithEvents txtDeductAmount As ucNumberFormat
    Protected WithEvents txtRefundAmountToCust As ucNumberFormat
    Protected WithEvents GridNavigator As ucGridNav

    Dim m_InsTerminate As New InsTerminateController
    Private oInsTerminate As New Parameter.InsTerminate
    Private pageSize As Int16 = 10
    Private currentPageNumber As Int32 = 1
    Private totalPages As Double = 1
    Private recordCount As Int64 = 1
    Private currentPage As Integer = 1

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddHandler GridNavigator.PageChanged, AddressOf PageNavigation

        If Not Page.IsPostBack Then
            fillcbo() 
            Me.FormID = "INSTERMINATEAPR" 
            If CheckForm(Me.Loginid, Me.FormID, Me.AppId) Then
                InitialDefaultPanel()
                oInsTerminate.TerminationDate = Me.BusinessDate 
            Else 
                Dim strHTTPServer = Request.ServerVariables("PATH_INFO")
                Dim strNameServer = Request.ServerVariables("SERVER_NAME")
                Dim strHTTPApp = strHTTPServer.Substring(1, strHTTPServer.IndexOf("/", 1) - 1)
                Response.Redirect("http://" & strNameServer & "/" & strHTTPApp & "/NotAuthorized.html")
            End If
        End If
    End Sub
    Private Sub InitialDefaultPanel()
        pnlDtGrid.Visible = False
        pnlsearch.Visible = True
        pnlView.Visible = False
        pnlCalculate.Visible = False

    End Sub

    Sub Bindgrid(Optional isFrNav As Boolean = False)
        Dim dtvEntity As New DataView
        Dim dtsEntity As New DataTable

        With oInsTerminate
            .strConnection = GetConnectionString()
            .WhereCond = SearchBy
            .CurrentPage = currentPage
            .PageSize = pageSize
            .SortBy = SortBy
        End With

        oInsTerminate = m_InsTerminate.InsTerminateList(oInsTerminate)
        dtsEntity = oInsTerminate.ListData
        dtsEntity.TableName = "Table"
        dtvEntity = dtsEntity.DefaultView
        dtgClaimRequestList.DataSource = dtvEntity

        If Not dtvEntity Is Nothing Then
            dtgClaimRequestList.DataBind()
        End If
        If (isFrNav = False) Then
            GridNavigator.Initialize(recordCount, pageSize)
        End If

        pnlDtGrid.Visible = True
        pnlCalculate.Visible = False
        pnlView.Visible = False
    End Sub


    Protected Sub PageNavigation(ByVal sender As Object, ByVal e As PageChangedEventArgs)
        currentPage = e.CurrentPage
        Bindgrid(True)
        GridNavigator.ReInitialize(e.CurrentPage, recordCount, e.TotalPage)
    End Sub

    Protected Sub Sorting(ByVal Sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs)
        Me.SortBy = String.Format("{0} {1}", e.SortExpression, IIf(InStr(Me.SortBy, "DESC") > 0, "", " DESC"))
        Bindgrid()
    End Sub
    Sub fillcbo()
        Dim m_controller As New DataUserControlController
        With ddlBranch


            If Me.IsHoBranch Then
                .DataSource = m_controller.GetBranchAll(GetConnectionString)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")

                .Items(0).Value = "0"
                .Items.Insert(1, "ALL")
                .Items(0).Value = "ALL"
            Else
                .DataSource = m_controller.GetBranchName(GetConnectionString, Me.sesBranchId)
                .DataValueField = "ID"
                .DataTextField = "Name"
                .DataBind()
                .Items.Insert(0, "Select One")
                .Items(0).Value = "0"


                Dim strBranch() As String
                strBranch = Split(Me.sesBranchId, ",")
                If UBound(strBranch) > 0 Then
                    .Items.Insert(1, "ALL")
                    .Items(1).Value = "ALL"
                End If
            End If
        End With
    End Sub

    Private Sub btnsearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        SearchBy = ""
        SortBy = "" 
        If ddlBranch.SelectedValue <> "" And ddlBranch.SelectedValue <> "ALL" Then
            SearchBy = "BranchID='" & ddlBranch.SelectedValue & "'"
        End If 
        If cboSearchBy.SelectedIndex <> 0 And txtSearchBy.Text <> "" Then 
            SearchBy = String.Format(" {0} {1} {2} like '%{3}%'", SearchBy, IIf(SearchBy = "", "", "and"), cboSearchBy.SelectedItem.Value.Trim, txtSearchBy.Text.Trim)
        End If 
        SearchBy = String.Format(" {0} {1} InsuranceClaim.ClaimStatus='R' ", SearchBy, IIf(SearchBy = "", "", "and"))
        Bindgrid()
    End Sub

    Public Property ApplicationID() As String
        Get
            Return CType(viewstate("ApplicationID"), String)
        End Get
        Set(ByVal Value As String)
            viewstate("ApplicationID") = Value
        End Set
    End Property
    Public Property InsSequenceNo() As Integer
        Get
            Return CType(viewstate("InsSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("InsSequenceNo") = Value
        End Set
    End Property
    Public Property AssetSequenceNo() As Integer
        Get
            Return CType(viewstate("AssetSequenceNo"), Integer)
        End Get
        Set(ByVal Value As Integer)
            viewstate("AssetSequenceNo") = Value
        End Set
    End Property
    Private Sub dtgClaimRequestList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgClaimRequestList.ItemCommand

        Select Case e.CommandName
            Case "Terminate" 
                BranchID = e.Item.Cells(1).Text.Trim
                ApplicationId = e.Item.Cells(2).Text.Trim
                Dim custId = e.Item.Cells(3).Text.Trim
                AssetSequenceNo = CInt(e.Item.Cells(11).Text.Trim)
                InsSequenceNo = CInt(e.Item.Cells(12).Text.Trim)
                viewReportDetail(BranchID, ApplicationID, AssetSequenceNo, InsSequenceNo, custId)
        End Select
    End Sub


    Private Sub viewReportDetail(ByVal BranchID As String, ByVal ApplicationID As String, ByVal AssetSeqNo As Integer, ByVal InsSeqNo As Integer, custid As String)
        lblAgreementNo.Text = ""
        lblAsset.Text = ""
        lblCustomerName.Text = ""
        lblEngineNo.Text = ""
        lblInsCo.Text = ""
        lblContractStatus.Text = ""
        lblTenor.Text = ""
        lblChassisNo.Text = ""
        lblPolicyNo.Text = ""
        lblInsuranceLength.Text = ""
        lblStartDate.Text = ""
        lblEndDate.Text = ""
        lblMainPremiumToCust.Text = ""
        lblPremiumToCust.Text = ""
        lblMainPremiumToInsCo.Text = ""
        lblPremiumToInsCo.Text = ""
        lblisrefund.Text = ""
        lblTerminateDate.Text = ""
        txtNotes.Text = ""


        pnlDtGrid.Visible = False
        pnlsearch.Visible = False
        pnlView.Visible = True
         
        dtgMainPremium.Visible = True
        pnlCalculate.Visible = True
        Dim dtsEntity As New DataTable
        Dim dtvEntity As New DataView

        Dim dt As New DataTable

        With oInsTerminate
            .strConnection = GetConnectionString()
            .BranchId = BranchID
            .ApplicationID = ApplicationID
            .AssetSequenceNo = AssetSeqNo
            .InsSequenceNo = InsSeqNo
            .TerminationDate = BusinessDate
        End With

        oInsTerminate = m_InsTerminate.InsTerminateDetail(oInsTerminate)
        dt = oInsTerminate.ListData
   
        If dt.Rows.Count = 0 Then
            ShowMessage(lblMessage, "Data Tidak ditemukan!", True) 
            Exit Sub
        End If
        Dim InsuranceClaimClaimAmountByCust As Double = 0
        With dt.Rows(0)
            lblAgreementNo.Text = CStr(IIf(IsDBNull(.Item("AgreementNo")), "", .Item("AgreementNo")))
            lblAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(ApplicationID.Trim) & "')"
            lblCustomerName.Text = CStr(IIf(IsDBNull(.Item("Name")), "", .Item("Name")))
            lblCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(custid.Trim) & "')"
            lblAsset.Text = CStr(IIf(IsDBNull(.Item("Description")), "", .Item("Description")))
            lblInsCo.Text = CStr(IIf(IsDBNull(.Item("InsCo")), "", .Item("InsCo")))
            lblPolicyNo.Text = CStr(IIf(IsDBNull(.Item("PolicyNumber")), "", .Item("PolicyNumber")))
            lblContractStatus.Text = CStr(IIf(IsDBNull(.Item("ContractStatus")), "", .Item("ContractStatus")))
            lblTenor.Text = CStr(IIf(IsDBNull(.Item("Tenor")), "", .Item("Tenor")))
            lblChassisNo.Text = CStr(IIf(IsDBNull(.Item("ChassisNo")), "", .Item("ChassisNo")))
            lblEngineNo.Text = CStr(IIf(IsDBNull(.Item("EngineNo")), "", .Item("EngineNo")))
            lblisrefund.Text = CStr(IIf(IsDBNull(.Item("IsRefundable")), "", .Item("IsRefundable")))
            lblInsuranceLength.Text = CStr(IIf(IsDBNull(.Item("InsuranceLength")), "", .Item("InsuranceLength")))
            lblStartDate.Text = CDate(CStr(IIf(IsDBNull(.Item("StartDate")), "", .Item("StartDate")))).ToString("dd/MM/yyyy")
            lblEndDate.Text = CDate(CStr(IIf(IsDBNull(.Item("EndDate")), "", .Item("EndDate")))).ToString("dd/MM/yyyy")

            If lblisrefund.Text = "1" Then
                lblisrefund.Text = "Yes"
            Else
                lblisrefund.Text = "No"
            End If
            lblMainPremiumToCust.Text = FormatNumber(IIf(IsDBNull(.Item("MainPremiumToCustomer")), "", .Item("MainPremiumToCustomer")), 2)
            lblPremiumToCust.Text = FormatNumber(IIf(IsDBNull(.Item("PremiumAmountToCustomer")), "", .Item("PremiumAmountToCustomer")), 2)
            lblMainPremiumToInsCo.Text = FormatNumber(IIf(IsDBNull(.Item("MainPremiumToInsCo")), "", .Item("MainPremiumToInsCo")), 2)
            lblPremiumToInsCo.Text = FormatNumber(IIf(IsDBNull(.Item("PremiumAmountToInsCo")), "", .Item("PremiumAmountToInsCo")), 2)
            lblCoverage.Text = CStr(IIf(IsDBNull(.Item("MainCoverage")), "", .Item("MainCoverage")))

            lblTerminateDate.Text = CDate(CStr(IIf(IsDBNull(.Item("InsuranceClaimClaimDate")), "", .Item("InsuranceClaimClaimDate")))).ToString("dd/MM/yyyy")

            lbloReason.Text = CStr(IIf(IsDBNull(.Item("InsuranceClaimReasonId")), "", .Item("InsuranceClaimReasonId")))
            txtNotes.Text = CStr(IIf(IsDBNull(.Item("InsuranceClaimNotes")), "", .Item("InsuranceClaimNotes")))


            'Me.StartDate = CDate(IIf(IsDBNull(.Item("StartDate")), "", .Item("StartDate")))
            'Me.EndDate = CDate(IIf(IsDBNull(.Item("EndDate")), "", .Item("EndDate")))



            lblRefundAmountToCust.Text = FormatNumber(IIf(IsDBNull(.Item("InsuranceClaimClaimAmount")), "", .Item("InsuranceClaimClaimAmount")), 2)
            lblDeductAmount.Text = FormatNumber(IIf(IsDBNull(.Item("InsuranceClaimRefundDeductionAmount")), "", .Item("InsuranceClaimRefundDeductionAmount")), 2)
            lblTtl.Text = FormatNumber(IIf(IsDBNull(.Item("InsuranceClaimClaimAmountByCust")), "", .Item("InsuranceClaimClaimAmountByCust")), 2)
            InsuranceClaimClaimAmountByCust = IIf(IsDBNull(.Item("InsuranceClaimClaimAmountByCust")), 0, .Item("InsuranceClaimClaimAmountByCust"))


        End With
        dtgMainPremium.DataSource = dt.DefaultView
        dtgMainPremium.DataBind()
        '==============================================================
         
        oInsTerminate = m_InsTerminate.InsCalculate(oInsTerminate)
        dt = oInsTerminate.ListData
        lblRestInsToInsCo.Text = "0"
        lblRefundAmountFromInsCo.Text = "0"
        If dt.Rows.Count > 0 Then 
            With dt.Rows(0) 
                lblRestInsToInsCo.Text = CStr(IIf(IsDBNull(.Item("RestOfPeriodToInsCo")), "", .Item("RestOfPeriodToInsCo")))
                Dim refAmount = IIf(IsDBNull(.Item("RefundFromInsCo")), 0, .Item("RefundFromInsCo"))
                lblRefundAmountFromInsCo.Text = FormatNumber(refAmount, 0)
                lblGain.Text = FormatNumber(refAmount - InsuranceClaimClaimAmountByCust)
            End With
        End If
    End Sub





    Private Sub dtgClaimRequestList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgClaimRequestList.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
             
            Dim lnkAgreementNo = CType(e.Item.FindControl("lnkAgreementNo"), HyperLink)
            Dim strApplicationID = e.Item.Cells(4).Text.Trim
            If lnkAgreementNo.Text.Trim.Length > 0 Then
                lnkAgreementNo.NavigateUrl = "javascript:OpenAgreementNo('" & "Insurance" & "', '" & Server.UrlEncode(strApplicationID.Trim) & "')"
            End If

            Dim strCustomerID = e.Item.Cells(5).Text.Trim
            Dim lnkCustomerName = CType(e.Item.FindControl("lnkCustomerName"), HyperLink)

            If strCustomerID.Trim.Length > 0 Then
                lnkCustomerName.NavigateUrl = "javascript:OpenCustomer('" & "Insurance" & "', '" & Server.UrlEncode(strCustomerID.Trim) & "')"
            End If
        End If
    End Sub
    Private Sub btnSave2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave2.Click
        Try
            With oInsTerminate
                .strConnection = GetConnectionString() 
                .BranchId = Me.BranchID
                .ApplicationID = Me.ApplicationID
                .AssetSequenceNo = Me.AssetSequenceNo
                .InsSequenceNo = Me.InsSequenceNo
                .ClaimStatus = ddlApprove.SelectedValue
                .Notes = txtApprNotes.Text
            End With
            Dim result = m_InsTerminate.InsApproveRejectCalculate(oInsTerminate)
            If result = "OK" Then 
                btnsearch_Click(Nothing, Nothing)
                ShowMessage(lblMessage, "Simpan Data Berhasil", False)
            Else
                ShowMessage(lblMessage, result, True)
            End If
        Catch exp As Exception
            ShowMessage(lblMessage, exp.Message, True)
        End Try

    End Sub

    Private Sub btnCancel2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel2.Click
        Response.Redirect("InsTerminateApproval.aspx")
    End Sub
End Class