﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewCoverInsuredByCust.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.NewCoverInsuredByCust" %>

<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="UcApplicationType" Src="../webform.UserController/UcApplicationType.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcCoverageType" Src="../webform.UserController/UcCoverageType.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NewCoverInsuredByCust</title>
    <link href="../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script src="../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Label ID="LblErrorMessages" runat="server"></asp:Label>
    <div class="form_title">
        <div class="form_single">
            <h3>
                COVER BARU - DIASURANSI OLEH CUSTOMER
            </h3>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                No Aplikasi</label>
            <asp:Label ID="LblApplicationID" runat="server"></asp:Label><asp:Label ID="LblBranchID"
                runat="server" Visible="False"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Nama Customer</label>
            <asp:Label ID="LblCustomerName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Diasuransi Oleh</label>
            <asp:Label ID="LblInsuredByName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                DiBayar Oleh</label>
            <asp:Label ID="LblPaidByName" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="mandatory_container">
                <label>
                    Perusahaan Asuransi</label>
                <label class="mandatory">
                    *</label></div>
            <asp:TextBox ID="TxtInsuranceCompany" runat="server" Width="272px" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator
                ID="rfvInsco" runat="server" ErrorMessage="Harap diisi Perusahaan Asuransi" ControlToValidate="TxtInsuranceCompany"
                Display="Dynamic">Must Be Filled</asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="mandatory_container">
                <label>
                    Nilai Pertanggungan</label>
                <label class="mandatory">
                    *</label></div>
            <%--<asp:TextBox ID="TxtAmountCoverage" runat="server" ></asp:TextBox><asp:RegularExpressionValidator
                ID="revAmount" runat="server" ErrorMessage="Harap diisi dengan Angka" ControlToValidate="TxtAmountCoverage"
                ValidationExpression="\d*"></asp:RegularExpressionValidator><asp:RequiredFieldValidator
                    ID="rfvAmount" runat="server" ErrorMessage="Harap diisi dengan Angka" ControlToValidate="TxtAmountCoverage">Must Be Filled</asp:RequiredFieldValidator>--%>
            <uc1:ucnumberformat id="TxtAmountCoverage" runat="server" /></uc1:ucNumberFormat>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jenis Cover</label>
            <uc1:uccoveragetype id="oCoverageType" runat="server">
            </uc1:uccoveragetype>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <div class="mandatory_container">
                <label>
                    No Polis</label>
                <label class="mandatory">
                    *</label></div>
            <asp:TextBox ID="TxtPolicyNumber" runat="server" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator
                ID="rfvPolicyNo" runat="server" ErrorMessage="Harap Diisi No Polis" ControlToValidate="TxtPolicyNumber">Must Be Filled</asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Jatuh Tempo</label>
            <asp:Label ID="lblMaturityDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal Expired</label>
            <asp:TextBox runat="server" ID="txtExpiredDate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtExpiredDate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Jenis Aplikasi</label>
            <uc1:ucapplicationtype id="oApplicationType" runat="server">
            </uc1:ucapplicationtype>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Catatan Asuransi</label>
            <asp:TextBox ID="txtinsurancenotes" runat="server" Width="392px" TextMode="MultiLine"
                MaxLength="500" Height="112px"></asp:TextBox>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="small button blue"></asp:Button>
        <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel"
            CssClass="small button gray"></asp:Button>
    </div>
    </form>
</body>
</html>
