﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="PolicyBillingsJK.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.PolicyBillingsJK" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagName="ucNumberFormat" TagPrefix="uc1" Src="../webform.UserController/ucNumberFormat.ascx" %>
<%@ Register TagName="UcNotaAsuransi" TagPrefix="uc1" Src="../webform.UserController/UcNotaAsuransi.ascx" %>
<%@ Register  TagName="ucDateCE" TagPrefix="uc1"Src="../Webform.UserController/ucDateCE.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>policybillings</title>
    <script src="../Maxiloan.js" type="text/javascript"></script>
      <script src="../js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        // var AppInfo = '<%# Request.servervariables("PATH_INFO")%>';
        //  var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);

        function OpenWinAgreementNo(pApplicationId, pStyle) {

            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewStatementOfAccount.aspx?ApplicationId=' + pApplicationId + '&style=' + pStyle, 'AgreementNo', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinCustomer(pID, pStyle) {

            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.LoanOrg/Credit/ViewPersonalCustomer.aspx?CustomerID=' + pID + '&style=' + pStyle, 'Customer', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
        function OpenWinAsset(pID, pStyle) {

            var x = screen.width; var y = screen.height - 100; window.open(ServerName + App + '/Webform.LoanOrg/Credit/CreditProcess/NewApplication/ViewAssetData.aspx?ApplicationID=' + pID + '&style=' + pStyle, 'Customer', 'left=0, top=0,width=' + x + ', height=' + y + ', menubar=0, scrollbars=yes');
        }
    </script>
    <link rel="Stylesheet" href="../Include/Buttons.css" type="text/css" />
    <link rel="Stylesheet" href="../Include/General.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">--%>
    <asp:Panel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close"
                onclick="hideMessage();"></asp:Label>
            <div class="form_title">
                <div class="title_strip">
                </div>
                <div class="form_single">
                    <h3>
                        TERIMA POLIS JK
                    </h3>
                </div>
            </div>
            <asp:Panel ID="Panel1" runat="server">
                <div class="form_box">
                    <div class="form_single">
                        <label>
                            No Kontrak</label>
                        <asp:HyperLink ID="hplAgreement" runat="server"></asp:HyperLink>
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nama Customer</label>
                        <asp:HyperLink ID="hplCustomerName" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkName" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Nama Asset</label>
                        <asp:HyperLink ID="lnkAsset" runat="server"></asp:HyperLink>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="chkAsset" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">
                    <div class="form_left">
                        <label>
                            Alamat</label>
                        <asp:Label ID="LblAddress" runat="server"></asp:Label>
                    </div>
                    <div class="form_right">
                        <asp:CheckBox ID="ChkAddress" Checked="true" runat="server" />
                    </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label>
                            Tanggal SPPA</label>
                        <asp:Label ID="lblSPPADate" runat="server"></asp:Label>
		            </div>
		            <div class="form_right">			
                        <asp:CheckBox ID="chkSPPADate" Checked="true" runat="server" />
		            </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label class="label_req">
                            No Polis</label>
                        <asp:TextBox ID="txtNoPolis" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtNoPolis"
                            ErrorMessage="Harap diisi No Polis" CssClass="validator_general"></asp:RequiredFieldValidator>
		            </div>
		            <div class="form_right">	
                        
		            </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label>
                            Tanggal Terima Polis</label>
                        <uc1:ucdatece id="txtValidDate" runat="server" />                        
		            </div>
		            <div class="form_right">	
                        	
		            </div>
                </div>
                <div class="form_box">                    
                    <div class="form_left">
                        <label>
                            Nilai Pertanggungan</label>                        
                        <asp:Label ID="lblSumInsured" runat="server"></asp:Label>
		            </div>
		            <div class="form_right">		
                        <asp:CheckBox ID="chkSumInsured" Checked="true" runat="server" />			
		            </div>
                </div>           
                <div class="form_button">
                    <asp:Button ID="ButtonValidate" runat="server" CausesValidation="False" Text="Next"
                        CssClass="small button blue" />
                    <asp:Button ID="ButtonExitPalingAtas" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlNotaAsuransi" Visible="false">
                <uc1:ucnotaasuransi id="UcNotaAsuransi1" runat="server" />
                <div class="form_button">
                    <asp:Button ID="ButtonSaveNota" runat="server" Text="Save" CssClass="small button blue" OnClientClick="if (!Page_ClientValidate()){ return false; } this.disabled = true; this.value = 'Saving...';" UseSubmitBehavior="false"/>
                    <asp:Button ID="ButtonCancelNota" runat="server" CausesValidation="False" Text="Cancel"
                        CssClass="small button gray" />
                </div>
            </asp:Panel>
            <asp:Panel ID="PnlValid" runat="server">
               
               
                <asp:Panel ID="pnlValidationDiv" runat="server">
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="PnlValid2" runat="server" Visible="False">
              
                <asp:Panel ID="pnlValidationDiv2" runat="server">
                    <div class="form_box_title">
                        <div class="form_eighten">
                            <h5>
                                Tahun Ke</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                Jenis Cover</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                TPL</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                SRCC</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                Flood</h5>
                        </div>
                        <div class="form_eighten">
                            <h5>
                                Loading Fee</h5>
                        </div>
                        <div class="form_eighten">
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlBilling" runat="server" Visible="False">
 
                <asp:Panel ID="pnlValidationDiv3" runat="server">
                    <div class="form_box_title">
                        <div class="form_left">
                            <h5>
                                Data Dari Perusahaan Asuransi</h5>
                        </div>
                        <div class="form_right">
                            <h5>
                                Data Benar</h5>
                        </div>
                    </div>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="PnlBtn" runat="server" Visible="False">
                <div class="form_button">
                    <asp:Button ID="ButtonEndors" runat="server" Text="Next" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                    <asp:Button ID="ButtonEdit" runat="server" Text="Edit" CssClass="small button blue"
                        CausesValidation="False"></asp:Button>
                    <asp:Button ID="ButtonExitBawah" runat="server" Text="Cancel" CssClass="small button gray"
                        CausesValidation="False"></asp:Button>
                </div>
            </asp:Panel>
        </ContentTemplate>
    <%--</asp:UpdatePanel>--%>
    </asp:Panel>
    </form>
</body>
</html>