﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="InsuranceCoveringInq.aspx.vb"
    Inherits="Maxiloan.Webform.Insurance.InsuranceCoveringInq" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uc1" TagName="ucComboBranchAndInsurance" Src="../../Webform.UserController/ucComboBranchAndInsurance.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UcSearchByWithNoTable" Src="../../Webform.UserController/UcSearchByWithNoTable.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ValidDate" Src="../../Webform.UserController/ValidDate.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Insurance Covering Inq</title>
    <link href="../../Include/General.css" rel="stylesheet" type="text/css" />
    <link href="../../Include/Buttons.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var AppInfo = window.location.pathname;
        var App = AppInfo.substr(1, AppInfo.indexOf('/', 1) - 1);
        var ServerName = window.location.protocol + "//" + window.location.host + '/'; 
    </script>
    <script src="../../Maxiloan.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1">
    </asp:ScriptManager>
      <asp:Label ID="lblMessage" runat="server" Visible="false" ToolTip="Click to close" onclick="hideMessage();"></asp:Label>
    <div class="form_title">
        <div class="title_strip">
        </div>
        <div class="form_single">
            <h3>
                CARI COVER ASURANSI
            </h3>
        </div>
    </div>
    <div class="form_box_uc">
        <uc1:uccombobranchandinsurance id="oBranchInsurance" runat="server"></uc1:uccombobranchandinsurance>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Cari Berdasarkan</label>
            <uc1:ucsearchbywithnotable id="oSearchBy" runat="server"></uc1:ucsearchbywithnotable>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Kriteria</label>
            <asp:DropDownList ID="DDLCriteria" runat="server">
                <asp:ListItem Value="0">Semua</asp:ListItem>
                <asp:ListItem Value="1">Kontrak Belum diCover</asp:ListItem>
                <asp:ListItem Value="2">Kontrak Belum Cetak SPPA</asp:ListItem>
                <asp:ListItem Value="3">Polis Belum diterima</asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="form_box">
        <div class="form_single">
            <label>
                Tanggal SPPA</label>
            <asp:TextBox runat="server" ID="txtStartSPPADate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtStartSPPADate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
            &nbsp;S/D&nbsp;
            <asp:TextBox runat="server" ID="txtEndSPPADate"></asp:TextBox>
            <asp:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtEndSPPADate"
                Format="dd/MM/yyyy">
            </asp:CalendarExtender>
        </div>
    </div>
    <div class="form_button">
        <asp:Button ID="btnSearch" runat="server" Text="Find" CssClass="small button blue">
        </asp:Button>
        <asp:Button ID="btnReset" runat="server" CausesValidation="False" Text="Reset" CssClass="small button gray">
        </asp:Button>
    </div>
    <asp:Panel ID="PnlGrid" runat="server" Font-Names="Tahoma" Font-Size="X-Small">
        <div class="form_title">
            <div class="form_single">
                <h4>
                    DAFTAR COVER ASURANSI
                </h4>
            </div>
        </div>
        <div class="form_box_header">
            <div class="form_single">
                <div class="grid_wrapper_ns">
                    <asp:DataGrid ID="DtgPaging" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        OnSortCommand="Sorting" BorderStyle="None" BorderWidth="0" CssClass="grid_general">
                        <HeaderStyle CssClass="th" />
                        <ItemStyle CssClass="item_grid" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="DETAIL">
                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                                <ItemTemplate>
                                    <a href='../ViewInsuranceDetail.aspx?pagesource=covering&ApplicationID=<%# DataBinder.eval(Container,"DataItem.ApplicationID") %>&BranchId=<%# DataBinder.eval(Container,"DataItem.BranchID") %>'>
                                        <asp:Image ID="imgRate" ImageUrl="../../images/iconinsurance.gif" runat="server">
                                        </asp:Image>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="AgreementNo" HeaderText="NO KONTRAK">
                                <HeaderStyle HorizontalAlign="Center" Width="12%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="12%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyAgreementNo" runat="server" Text='<%#Container.DataItem("AgreementNo")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn SortExpression="customername" HeaderText="NAMA KONSUMEN">
                                <HeaderStyle HorizontalAlign="Center" Width="18%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Left" Width="18%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:HyperLink ID="hyCustomerName" runat="server" Text='<%#Container.DataItem("customername")%>'>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="golivedate" SortExpression="golivedate" HeaderText="TGL AKT"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="InscoSelectionDate" SortExpression="InscoSelectionDate"
                                HeaderText="TGL PILIH" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MaskAssBranchname" SortExpression="MaskAssBranchname"
                                HeaderText="MASKAPAI ASURANSI"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="DARI TGL">
                                <ItemTemplate>
                                    <asp:Label ID="lblstartdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate")  %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="S/D TGL">
                                <ItemTemplate>
                                    <asp:Label ID="lblenddate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndDate")  %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="sppadate" SortExpression="sppadate" HeaderText="TGL SPPA"
                                DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="premiumamountbycust" SortExpression="premiumamountbycust"
                                HeaderText="PREMI CUSTOMER" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn Visible="False" DataField="stdpremium" SortExpression="stdpremium" HeaderText="PREMI STANDARD"
                                DataFormatString="{0:N2}">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="premiumamounttoinsco" SortExpression="premiumamounttoinsco"
                                HeaderText="PREMI MASKAPAI" DataFormatString="{0:N0}">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                            <asp:TemplateColumn Visible="False" SortExpression="" HeaderText="CUSTOMER ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerId" runat="server" Text='<%#Container.DataItem("CustomerId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn Visible="False" HeaderText="APPLICATION ID">
                                <HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center" Width="8%"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblApplicationId" runat="server" Text='<%#Container.DataItem("ApplicationId")%>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Coverage" SortExpression="Coverage" HeaderText="JENIS COVER">
                                <ItemStyle HorizontalAlign="Right"></ItemStyle>
                            </asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
                <div class="button_gridnavigation">
                    <asp:ImageButton ID="imbFirstPage" runat="server" ImageUrl="../../Images/grid_navbutton01.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="First">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbPrevPage" runat="server" ImageUrl="../../Images/grid_navbutton02.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Prev">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbNextPage" runat="server" ImageUrl="../../Images/grid_navbutton03.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Next">
                    </asp:ImageButton>
                    <asp:ImageButton ID="imbLastPage" runat="server" ImageUrl="../../Images/grid_navbutton04.png"
                        CausesValidation="False" OnCommand="NavigationLink_Click" CommandName="Last">
                    </asp:ImageButton>
                    Page&nbsp;
                    <asp:TextBox ID="txtGoPage" runat="server" Width="34px" MaxLength="3"></asp:TextBox>
                    <asp:Button ID="BtnGoPage" runat="server" Text="Go" CssClass="small buttongo blue"
                        EnableViewState="False" ImageAlign="AbsBottom"></asp:Button>
                </div>
                <div class="label_gridnavigation">
                    <asp:Label ID="lblPage" runat="server"></asp:Label>&nbsp;of
                    <asp:Label ID="lblTotPage" runat="server"></asp:Label>, Total&nbsp;
                    <asp:Label ID="lblTotRec" runat="server"></asp:Label>&nbsp;record(s)
                </div>
            </div>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
